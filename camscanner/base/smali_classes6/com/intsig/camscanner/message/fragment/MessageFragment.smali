.class public Lcom/intsig/camscanner/message/fragment/MessageFragment;
.super Lcom/intsig/mvp/fragment/BaseChangeFragment;
.source "MessageFragment.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/message/fragment/MessageFragment$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field static final synthetic OO〇00〇8oO:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final o8〇OO0〇0o:Ljava/lang/String;

.field public static final oOo0:Lcom/intsig/camscanner/message/fragment/MessageFragment$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field protected OO:Lcom/intsig/camscanner/message/viewmodel/MessageViewModel;

.field private final o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oOo〇8o008:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/developer/lib_message/SocketConnectionCmd$MsgTaskPair;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:I

.field private 〇080OO8〇0:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

.field private final 〇08O〇00〇o:Lcom/intsig/camscanner/message/MessageClient;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇0O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇OOo8〇0:Lcom/intsig/camscanner/message/adapter/MessageAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/FragmentMessageBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/message/fragment/MessageFragment;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->OO〇00〇8oO:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/message/fragment/MessageFragment$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/message/fragment/MessageFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->oOo0:Lcom/intsig/camscanner/message/fragment/MessageFragment$Companion;

    .line 31
    .line 32
    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    sput-object v0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->o8〇OO0〇0o:Ljava/lang/String;

    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v6, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/FragmentMessageBinding;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x4

    .line 10
    const/4 v5, 0x0

    .line 11
    move-object v0, v6

    .line 12
    move-object v2, p0

    .line 13
    invoke-direct/range {v0 .. v5}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object v6, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 17
    .line 18
    sget-object v0, Lcom/intsig/camscanner/message/MessageClient;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/message/MessageClient$Companion;

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/intsig/camscanner/message/MessageClient$Companion;->〇080()Lcom/intsig/camscanner/message/MessageClient;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    iput-object v0, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/message/MessageClient;

    .line 25
    .line 26
    const-string v0, "CSInformationCenter"

    .line 27
    .line 28
    iput-object v0, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->O8o08O8O:Ljava/lang/String;

    .line 29
    .line 30
    sget-object v0, Lcom/intsig/camscanner/message/fragment/MessageFragment$recycledViewPool$2;->o0:Lcom/intsig/camscanner/message/fragment/MessageFragment$recycledViewPool$2;

    .line 31
    .line 32
    invoke-static {v0}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    iput-object v0, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇0O:Lkotlin/Lazy;

    .line 37
    .line 38
    new-instance v0, Ljava/util/ArrayList;

    .line 39
    .line 40
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 41
    .line 42
    .line 43
    iput-object v0, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->oOo〇8o008:Ljava/util/List;

    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic O0〇0(Lcom/intsig/camscanner/message/fragment/MessageFragment;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇〇O80〇0o(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final Ooo8o(Lcom/intsig/camscanner/message/fragment/MessageFragment;Lcom/intsig/camscanner/message/viewmodel/MessageRepository;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$messageRepository"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇o〇88〇8()Lcom/intsig/camscanner/message/viewmodel/MessageViewModel;

    .line 12
    .line 13
    .line 14
    move-result-object p0

    .line 15
    invoke-virtual {p0}, Lcom/intsig/camscanner/message/viewmodel/MessageViewModel;->〇80〇808〇O()Landroidx/lifecycle/MutableLiveData;

    .line 16
    .line 17
    .line 18
    move-result-object p0

    .line 19
    invoke-virtual {p1}, Lcom/intsig/camscanner/message/viewmodel/MessageRepository;->〇080()Ljava/util/List;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    invoke-virtual {p0, p1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic o00〇88〇08()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->o8〇OO0〇0o:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic o880(Lcom/intsig/camscanner/message/fragment/MessageFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic oO〇oo(Lcom/intsig/camscanner/message/fragment/MessageFragment;Lcom/intsig/camscanner/message/viewmodel/MessageRepository;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->Ooo8o(Lcom/intsig/camscanner/message/fragment/MessageFragment;Lcom/intsig/camscanner/message/viewmodel/MessageRepository;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oooO888(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇0〇0(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇0ooOOo(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇0〇0(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇8〇80o()Landroidx/recyclerview/widget/RecyclerView$RecycledViewPool;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇0O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Landroidx/recyclerview/widget/RecyclerView$RecycledViewPool;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇8〇OOoooo()Lcom/intsig/camscanner/databinding/FragmentMessageBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/message/fragment/MessageFragment;->OO〇00〇8oO:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/FragmentMessageBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇O8oOo0(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇0ooOOo(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final varargs 〇o08(Ljava/lang/String;[Landroid/net/Uri;)Z
    .locals 7

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-nez v0, :cond_3

    .line 7
    .line 8
    array-length v0, p2

    .line 9
    const/4 v2, 0x1

    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    const/4 v0, 0x1

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    :goto_0
    if-eqz v0, :cond_1

    .line 16
    .line 17
    goto :goto_2

    .line 18
    :cond_1
    array-length v0, p2

    .line 19
    const/4 v3, 0x0

    .line 20
    :goto_1
    if-ge v3, v0, :cond_3

    .line 21
    .line 22
    aget-object v4, p2, v3

    .line 23
    .line 24
    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v4

    .line 28
    const-string v5, "uri.toString()"

    .line 29
    .line 30
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    const/4 v5, 0x2

    .line 34
    const/4 v6, 0x0

    .line 35
    invoke-static {p1, v4, v1, v5, v6}, Lkotlin/text/StringsKt;->Oo8Oo00oo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    move-result v4

    .line 39
    if-eqz v4, :cond_2

    .line 40
    .line 41
    return v2

    .line 42
    :cond_2
    add-int/lit8 v3, v3, 0x1

    .line 43
    .line 44
    goto :goto_1

    .line 45
    :cond_3
    :goto_2
    return v1
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final 〇〇O80〇0o(Z)V
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    const/16 v1, 0x8

    .line 3
    .line 4
    const/4 v2, 0x0

    .line 5
    if-eqz p1, :cond_16

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇8〇OOoooo()Lcom/intsig/camscanner/databinding/FragmentMessageBinding;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    if-eqz p1, :cond_0

    .line 12
    .line 13
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentMessageBinding;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    move-object p1, v2

    .line 17
    :goto_0
    if-nez p1, :cond_1

    .line 18
    .line 19
    goto :goto_1

    .line 20
    :cond_1
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 21
    .line 22
    .line 23
    :goto_1
    sget-object p1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    invoke-static {p1}, Lcom/intsig/camscanner/util/Util;->O0(Landroid/content/Context;)Z

    .line 30
    .line 31
    .line 32
    move-result p1

    .line 33
    if-nez p1, :cond_8

    .line 34
    .line 35
    invoke-direct {p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇8〇OOoooo()Lcom/intsig/camscanner/databinding/FragmentMessageBinding;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    if-eqz p1, :cond_2

    .line 40
    .line 41
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentMessageBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/IncludeTipsNetworkMsgBinding;

    .line 42
    .line 43
    if-eqz p1, :cond_2

    .line 44
    .line 45
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/IncludeTipsNetworkMsgBinding;->〇080()Landroid/widget/LinearLayout;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    goto :goto_2

    .line 50
    :cond_2
    move-object p1, v2

    .line 51
    :goto_2
    if-nez p1, :cond_3

    .line 52
    .line 53
    goto :goto_3

    .line 54
    :cond_3
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 55
    .line 56
    .line 57
    :goto_3
    invoke-direct {p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇8〇OOoooo()Lcom/intsig/camscanner/databinding/FragmentMessageBinding;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    if-eqz p1, :cond_4

    .line 62
    .line 63
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentMessageBinding;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/IncludeEmptyMsgBinding;

    .line 64
    .line 65
    if-eqz p1, :cond_4

    .line 66
    .line 67
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/IncludeEmptyMsgBinding;->〇080()Landroid/widget/LinearLayout;

    .line 68
    .line 69
    .line 70
    move-result-object p1

    .line 71
    goto :goto_4

    .line 72
    :cond_4
    move-object p1, v2

    .line 73
    :goto_4
    if-nez p1, :cond_5

    .line 74
    .line 75
    goto :goto_5

    .line 76
    :cond_5
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 77
    .line 78
    .line 79
    :goto_5
    invoke-direct {p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇8〇OOoooo()Lcom/intsig/camscanner/databinding/FragmentMessageBinding;

    .line 80
    .line 81
    .line 82
    move-result-object p1

    .line 83
    if-eqz p1, :cond_6

    .line 84
    .line 85
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentMessageBinding;->OO:Lcom/intsig/camscanner/databinding/IncludeFailLoadMsgBinding;

    .line 86
    .line 87
    if-eqz p1, :cond_6

    .line 88
    .line 89
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/IncludeFailLoadMsgBinding;->〇080()Landroid/widget/LinearLayout;

    .line 90
    .line 91
    .line 92
    move-result-object v2

    .line 93
    :cond_6
    if-nez v2, :cond_7

    .line 94
    .line 95
    goto/16 :goto_14

    .line 96
    .line 97
    :cond_7
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 98
    .line 99
    .line 100
    goto/16 :goto_14

    .line 101
    .line 102
    :cond_8
    iget-object p1, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/message/MessageClient;

    .line 103
    .line 104
    invoke-virtual {p1}, Lcom/intsig/camscanner/message/MessageClient;->o〇O8〇〇o()Z

    .line 105
    .line 106
    .line 107
    move-result p1

    .line 108
    if-eqz p1, :cond_f

    .line 109
    .line 110
    invoke-direct {p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇8〇OOoooo()Lcom/intsig/camscanner/databinding/FragmentMessageBinding;

    .line 111
    .line 112
    .line 113
    move-result-object p1

    .line 114
    if-eqz p1, :cond_9

    .line 115
    .line 116
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentMessageBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/IncludeTipsNetworkMsgBinding;

    .line 117
    .line 118
    if-eqz p1, :cond_9

    .line 119
    .line 120
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/IncludeTipsNetworkMsgBinding;->〇080()Landroid/widget/LinearLayout;

    .line 121
    .line 122
    .line 123
    move-result-object p1

    .line 124
    goto :goto_6

    .line 125
    :cond_9
    move-object p1, v2

    .line 126
    :goto_6
    if-nez p1, :cond_a

    .line 127
    .line 128
    goto :goto_7

    .line 129
    :cond_a
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 130
    .line 131
    .line 132
    :goto_7
    invoke-direct {p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇8〇OOoooo()Lcom/intsig/camscanner/databinding/FragmentMessageBinding;

    .line 133
    .line 134
    .line 135
    move-result-object p1

    .line 136
    if-eqz p1, :cond_b

    .line 137
    .line 138
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentMessageBinding;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/IncludeEmptyMsgBinding;

    .line 139
    .line 140
    if-eqz p1, :cond_b

    .line 141
    .line 142
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/IncludeEmptyMsgBinding;->〇080()Landroid/widget/LinearLayout;

    .line 143
    .line 144
    .line 145
    move-result-object p1

    .line 146
    goto :goto_8

    .line 147
    :cond_b
    move-object p1, v2

    .line 148
    :goto_8
    if-nez p1, :cond_c

    .line 149
    .line 150
    goto :goto_9

    .line 151
    :cond_c
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 152
    .line 153
    .line 154
    :goto_9
    invoke-direct {p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇8〇OOoooo()Lcom/intsig/camscanner/databinding/FragmentMessageBinding;

    .line 155
    .line 156
    .line 157
    move-result-object p1

    .line 158
    if-eqz p1, :cond_d

    .line 159
    .line 160
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentMessageBinding;->OO:Lcom/intsig/camscanner/databinding/IncludeFailLoadMsgBinding;

    .line 161
    .line 162
    if-eqz p1, :cond_d

    .line 163
    .line 164
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/IncludeFailLoadMsgBinding;->〇080()Landroid/widget/LinearLayout;

    .line 165
    .line 166
    .line 167
    move-result-object v2

    .line 168
    :cond_d
    if-nez v2, :cond_e

    .line 169
    .line 170
    goto/16 :goto_14

    .line 171
    .line 172
    :cond_e
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 173
    .line 174
    .line 175
    goto/16 :goto_14

    .line 176
    .line 177
    :cond_f
    invoke-direct {p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇8〇OOoooo()Lcom/intsig/camscanner/databinding/FragmentMessageBinding;

    .line 178
    .line 179
    .line 180
    move-result-object p1

    .line 181
    if-eqz p1, :cond_10

    .line 182
    .line 183
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentMessageBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/IncludeTipsNetworkMsgBinding;

    .line 184
    .line 185
    if-eqz p1, :cond_10

    .line 186
    .line 187
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/IncludeTipsNetworkMsgBinding;->〇080()Landroid/widget/LinearLayout;

    .line 188
    .line 189
    .line 190
    move-result-object p1

    .line 191
    goto :goto_a

    .line 192
    :cond_10
    move-object p1, v2

    .line 193
    :goto_a
    if-nez p1, :cond_11

    .line 194
    .line 195
    goto :goto_b

    .line 196
    :cond_11
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 197
    .line 198
    .line 199
    :goto_b
    invoke-direct {p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇8〇OOoooo()Lcom/intsig/camscanner/databinding/FragmentMessageBinding;

    .line 200
    .line 201
    .line 202
    move-result-object p1

    .line 203
    if-eqz p1, :cond_12

    .line 204
    .line 205
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentMessageBinding;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/IncludeEmptyMsgBinding;

    .line 206
    .line 207
    if-eqz p1, :cond_12

    .line 208
    .line 209
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/IncludeEmptyMsgBinding;->〇080()Landroid/widget/LinearLayout;

    .line 210
    .line 211
    .line 212
    move-result-object p1

    .line 213
    goto :goto_c

    .line 214
    :cond_12
    move-object p1, v2

    .line 215
    :goto_c
    if-nez p1, :cond_13

    .line 216
    .line 217
    goto :goto_d

    .line 218
    :cond_13
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 219
    .line 220
    .line 221
    :goto_d
    invoke-direct {p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇8〇OOoooo()Lcom/intsig/camscanner/databinding/FragmentMessageBinding;

    .line 222
    .line 223
    .line 224
    move-result-object p1

    .line 225
    if-eqz p1, :cond_14

    .line 226
    .line 227
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentMessageBinding;->OO:Lcom/intsig/camscanner/databinding/IncludeFailLoadMsgBinding;

    .line 228
    .line 229
    if-eqz p1, :cond_14

    .line 230
    .line 231
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/IncludeFailLoadMsgBinding;->〇080()Landroid/widget/LinearLayout;

    .line 232
    .line 233
    .line 234
    move-result-object v2

    .line 235
    :cond_14
    if-nez v2, :cond_15

    .line 236
    .line 237
    goto :goto_14

    .line 238
    :cond_15
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 239
    .line 240
    .line 241
    goto :goto_14

    .line 242
    :cond_16
    invoke-direct {p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇8〇OOoooo()Lcom/intsig/camscanner/databinding/FragmentMessageBinding;

    .line 243
    .line 244
    .line 245
    move-result-object p1

    .line 246
    if-eqz p1, :cond_17

    .line 247
    .line 248
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentMessageBinding;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 249
    .line 250
    goto :goto_e

    .line 251
    :cond_17
    move-object p1, v2

    .line 252
    :goto_e
    if-nez p1, :cond_18

    .line 253
    .line 254
    goto :goto_f

    .line 255
    :cond_18
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 256
    .line 257
    .line 258
    :goto_f
    invoke-direct {p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇8〇OOoooo()Lcom/intsig/camscanner/databinding/FragmentMessageBinding;

    .line 259
    .line 260
    .line 261
    move-result-object p1

    .line 262
    if-eqz p1, :cond_19

    .line 263
    .line 264
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentMessageBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/IncludeTipsNetworkMsgBinding;

    .line 265
    .line 266
    if-eqz p1, :cond_19

    .line 267
    .line 268
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/IncludeTipsNetworkMsgBinding;->〇080()Landroid/widget/LinearLayout;

    .line 269
    .line 270
    .line 271
    move-result-object p1

    .line 272
    goto :goto_10

    .line 273
    :cond_19
    move-object p1, v2

    .line 274
    :goto_10
    if-nez p1, :cond_1a

    .line 275
    .line 276
    goto :goto_11

    .line 277
    :cond_1a
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 278
    .line 279
    .line 280
    :goto_11
    invoke-direct {p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇8〇OOoooo()Lcom/intsig/camscanner/databinding/FragmentMessageBinding;

    .line 281
    .line 282
    .line 283
    move-result-object p1

    .line 284
    if-eqz p1, :cond_1b

    .line 285
    .line 286
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentMessageBinding;->〇OOo8〇0:Lcom/intsig/camscanner/databinding/IncludeEmptyMsgBinding;

    .line 287
    .line 288
    if-eqz p1, :cond_1b

    .line 289
    .line 290
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/IncludeEmptyMsgBinding;->〇080()Landroid/widget/LinearLayout;

    .line 291
    .line 292
    .line 293
    move-result-object p1

    .line 294
    goto :goto_12

    .line 295
    :cond_1b
    move-object p1, v2

    .line 296
    :goto_12
    if-nez p1, :cond_1c

    .line 297
    .line 298
    goto :goto_13

    .line 299
    :cond_1c
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 300
    .line 301
    .line 302
    :goto_13
    invoke-direct {p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇8〇OOoooo()Lcom/intsig/camscanner/databinding/FragmentMessageBinding;

    .line 303
    .line 304
    .line 305
    move-result-object p1

    .line 306
    if-eqz p1, :cond_1d

    .line 307
    .line 308
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentMessageBinding;->OO:Lcom/intsig/camscanner/databinding/IncludeFailLoadMsgBinding;

    .line 309
    .line 310
    if-eqz p1, :cond_1d

    .line 311
    .line 312
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/IncludeFailLoadMsgBinding;->〇080()Landroid/widget/LinearLayout;

    .line 313
    .line 314
    .line 315
    move-result-object v2

    .line 316
    :cond_1d
    if-nez v2, :cond_1e

    .line 317
    .line 318
    goto :goto_14

    .line 319
    :cond_1e
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 320
    .line 321
    .line 322
    :goto_14
    return-void
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public static final varargs synthetic 〇〇o0〇8(Lcom/intsig/camscanner/message/fragment/MessageFragment;Ljava/lang/String;[Landroid/net/Uri;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇o08(Ljava/lang/String;[Landroid/net/Uri;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public dealClickAction(Landroid/view/View;)V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    move-object p1, v0

    .line 14
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇8〇OOoooo()Lcom/intsig/camscanner/databinding/FragmentMessageBinding;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    if-eqz v1, :cond_1

    .line 19
    .line 20
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentMessageBinding;->OO:Lcom/intsig/camscanner/databinding/IncludeFailLoadMsgBinding;

    .line 21
    .line 22
    if-eqz v1, :cond_1

    .line 23
    .line 24
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/IncludeFailLoadMsgBinding;->〇OOo8〇0:Landroid/widget/TextView;

    .line 25
    .line 26
    if-eqz v1, :cond_1

    .line 27
    .line 28
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    goto :goto_1

    .line 37
    :cond_1
    move-object v1, v0

    .line 38
    :goto_1
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    if-eqz v1, :cond_2

    .line 43
    .line 44
    sget-object p1, Lcom/intsig/camscanner/message/fragment/MessageFragment;->o8〇OO0〇0o:Ljava/lang/String;

    .line 45
    .line 46
    const-string v0, "Reload"

    .line 47
    .line 48
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    iget-object p1, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/message/MessageClient;

    .line 52
    .line 53
    invoke-virtual {p1}, Lcom/intsig/camscanner/message/MessageClient;->〇oOO8O8()V

    .line 54
    .line 55
    .line 56
    goto :goto_2

    .line 57
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇8〇OOoooo()Lcom/intsig/camscanner/databinding/FragmentMessageBinding;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    if-eqz v1, :cond_3

    .line 62
    .line 63
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentMessageBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/IncludeTipsNetworkMsgBinding;

    .line 64
    .line 65
    if-eqz v1, :cond_3

    .line 66
    .line 67
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/IncludeTipsNetworkMsgBinding;->〇OOo8〇0:Landroid/widget/TextView;

    .line 68
    .line 69
    if-eqz v1, :cond_3

    .line 70
    .line 71
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    .line 72
    .line 73
    .line 74
    move-result v0

    .line 75
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 76
    .line 77
    .line 78
    move-result-object v0

    .line 79
    :cond_3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 80
    .line 81
    .line 82
    move-result p1

    .line 83
    if-eqz p1, :cond_4

    .line 84
    .line 85
    sget-object p1, Lcom/intsig/camscanner/message/fragment/MessageFragment;->o8〇OO0〇0o:Ljava/lang/String;

    .line 86
    .line 87
    const-string v0, "tips network"

    .line 88
    .line 89
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    .line 91
    .line 92
    new-instance p1, Landroid/content/Intent;

    .line 93
    .line 94
    const-string v0, "android.settings.WIRELESS_SETTINGS"

    .line 95
    .line 96
    invoke-direct {p1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 97
    .line 98
    .line 99
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 100
    .line 101
    .line 102
    :cond_4
    :goto_2
    return-void
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 6

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    const/4 v0, 0x0

    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    const-string v1, "subType"

    .line 9
    .line 10
    invoke-virtual {p1, v1}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 p1, 0x0

    .line 16
    :goto_0
    iput p1, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->o〇00O:I

    .line 17
    .line 18
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    const/4 v1, 0x0

    .line 23
    if-eqz p1, :cond_1

    .line 24
    .line 25
    const-string v2, "keyLogAgent"

    .line 26
    .line 27
    invoke-virtual {p1, v2}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    goto :goto_1

    .line 32
    :cond_1
    move-object p1, v1

    .line 33
    :goto_1
    if-nez p1, :cond_2

    .line 34
    .line 35
    const-string p1, "CSInformationCenter"

    .line 36
    .line 37
    :cond_2
    iput-object p1, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->O8o08O8O:Ljava/lang/String;

    .line 38
    .line 39
    sget-object p1, Lcom/intsig/camscanner/message/fragment/MessageFragment;->o8〇OO0〇0o:Ljava/lang/String;

    .line 40
    .line 41
    iget v2, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->o〇00O:I

    .line 42
    .line 43
    new-instance v3, Ljava/lang/StringBuilder;

    .line 44
    .line 45
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 46
    .line 47
    .line 48
    const-string v4, "subType:"

    .line 49
    .line 50
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v2

    .line 60
    invoke-static {p1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    new-instance p1, Lcom/intsig/camscanner/message/viewmodel/MessageRepository;

    .line 64
    .line 65
    iget v2, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->o〇00O:I

    .line 66
    .line 67
    invoke-direct {p1, v2}, Lcom/intsig/camscanner/message/viewmodel/MessageRepository;-><init>(I)V

    .line 68
    .line 69
    .line 70
    new-instance v2, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 71
    .line 72
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 73
    .line 74
    .line 75
    move-result-object v3

    .line 76
    new-instance v4, Ljava/lang/StringBuilder;

    .line 77
    .line 78
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 79
    .line 80
    .line 81
    const-string v5, "message:"

    .line 82
    .line 83
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 87
    .line 88
    .line 89
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 90
    .line 91
    .line 92
    move-result-object v4

    .line 93
    invoke-direct {v2, v3, v4}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;-><init>(Landroidx/lifecycle/LifecycleOwner;Ljava/lang/String;)V

    .line 94
    .line 95
    .line 96
    const-wide/16 v3, 0x7d0

    .line 97
    .line 98
    invoke-virtual {v2, v3, v4}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->〇8o8o〇(J)V

    .line 99
    .line 100
    .line 101
    new-instance v3, L〇00O0/O8;

    .line 102
    .line 103
    invoke-direct {v3, p0, p1}, L〇00O0/O8;-><init>(Lcom/intsig/camscanner/message/fragment/MessageFragment;Lcom/intsig/camscanner/message/viewmodel/MessageRepository;)V

    .line 104
    .line 105
    .line 106
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->OO0o〇〇(Ljava/lang/Runnable;)V

    .line 107
    .line 108
    .line 109
    iput-object v2, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇080OO8〇0:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 110
    .line 111
    invoke-virtual {p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->oOoO8OO〇()V

    .line 112
    .line 113
    .line 114
    new-instance p1, Lcom/intsig/camscanner/message/adapter/MessageAdapter;

    .line 115
    .line 116
    iget-object v2, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->O8o08O8O:Ljava/lang/String;

    .line 117
    .line 118
    invoke-direct {p1, v1, p0, v2}, Lcom/intsig/camscanner/message/adapter/MessageAdapter;-><init>(Ljava/util/List;Landroidx/fragment/app/Fragment;Ljava/lang/String;)V

    .line 119
    .line 120
    .line 121
    iput-object p1, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇OOo8〇0:Lcom/intsig/camscanner/message/adapter/MessageAdapter;

    .line 122
    .line 123
    invoke-direct {p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇8〇OOoooo()Lcom/intsig/camscanner/databinding/FragmentMessageBinding;

    .line 124
    .line 125
    .line 126
    move-result-object p1

    .line 127
    const/4 v2, 0x1

    .line 128
    if-eqz p1, :cond_3

    .line 129
    .line 130
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentMessageBinding;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 131
    .line 132
    if-eqz p1, :cond_3

    .line 133
    .line 134
    invoke-direct {p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇8〇80o()Landroidx/recyclerview/widget/RecyclerView$RecycledViewPool;

    .line 135
    .line 136
    .line 137
    move-result-object v3

    .line 138
    invoke-virtual {p1, v3}, Landroidx/recyclerview/widget/RecyclerView;->setRecycledViewPool(Landroidx/recyclerview/widget/RecyclerView$RecycledViewPool;)V

    .line 139
    .line 140
    .line 141
    invoke-virtual {p1, v2}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 142
    .line 143
    .line 144
    new-instance v3, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 145
    .line 146
    iget-object v4, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 147
    .line 148
    invoke-direct {v3, v4}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 149
    .line 150
    .line 151
    invoke-virtual {p1, v3}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 152
    .line 153
    .line 154
    iget-object v3, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇OOo8〇0:Lcom/intsig/camscanner/message/adapter/MessageAdapter;

    .line 155
    .line 156
    invoke-virtual {p1, v3}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 157
    .line 158
    .line 159
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->o〇O8OO()V

    .line 160
    .line 161
    .line 162
    const/4 p1, 0x2

    .line 163
    new-array p1, p1, [Landroid/view/View;

    .line 164
    .line 165
    invoke-direct {p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇8〇OOoooo()Lcom/intsig/camscanner/databinding/FragmentMessageBinding;

    .line 166
    .line 167
    .line 168
    move-result-object v3

    .line 169
    if-eqz v3, :cond_4

    .line 170
    .line 171
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/FragmentMessageBinding;->OO:Lcom/intsig/camscanner/databinding/IncludeFailLoadMsgBinding;

    .line 172
    .line 173
    if-eqz v3, :cond_4

    .line 174
    .line 175
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/IncludeFailLoadMsgBinding;->〇OOo8〇0:Landroid/widget/TextView;

    .line 176
    .line 177
    goto :goto_2

    .line 178
    :cond_4
    move-object v3, v1

    .line 179
    :goto_2
    aput-object v3, p1, v0

    .line 180
    .line 181
    invoke-direct {p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇8〇OOoooo()Lcom/intsig/camscanner/databinding/FragmentMessageBinding;

    .line 182
    .line 183
    .line 184
    move-result-object v0

    .line 185
    if-eqz v0, :cond_5

    .line 186
    .line 187
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentMessageBinding;->〇08O〇00〇o:Lcom/intsig/camscanner/databinding/IncludeTipsNetworkMsgBinding;

    .line 188
    .line 189
    if-eqz v0, :cond_5

    .line 190
    .line 191
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/IncludeTipsNetworkMsgBinding;->〇OOo8〇0:Landroid/widget/TextView;

    .line 192
    .line 193
    :cond_5
    aput-object v1, p1, v2

    .line 194
    .line 195
    invoke-virtual {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 196
    .line 197
    .line 198
    return-void
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method protected final oOoO8OO〇()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    const v1, 0x7f0a1599

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, v1}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    check-cast v0, Landroid/widget/TextView;

    .line 11
    .line 12
    new-instance v1, Lcom/intsig/camscanner/message/viewmodel/MessageViewModelFactory;

    .line 13
    .line 14
    invoke-direct {v1}, Lcom/intsig/camscanner/message/viewmodel/MessageViewModelFactory;-><init>()V

    .line 15
    .line 16
    .line 17
    new-instance v2, Landroidx/lifecycle/ViewModelProvider;

    .line 18
    .line 19
    invoke-direct {v2, p0, v1}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    .line 20
    .line 21
    .line 22
    const-class v1, Lcom/intsig/camscanner/message/viewmodel/MessageViewModel;

    .line 23
    .line 24
    invoke-virtual {v2, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    check-cast v1, Lcom/intsig/camscanner/message/viewmodel/MessageViewModel;

    .line 29
    .line 30
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇〇〇0(Lcom/intsig/camscanner/message/viewmodel/MessageViewModel;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇o〇88〇8()Lcom/intsig/camscanner/message/viewmodel/MessageViewModel;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    invoke-virtual {v1}, Lcom/intsig/camscanner/message/viewmodel/MessageViewModel;->〇80〇808〇O()Landroidx/lifecycle/MutableLiveData;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    new-instance v2, Lcom/intsig/camscanner/message/fragment/MessageFragment$initMessageLoader$1;

    .line 42
    .line 43
    invoke-direct {v2, p0, v0}, Lcom/intsig/camscanner/message/fragment/MessageFragment$initMessageLoader$1;-><init>(Lcom/intsig/camscanner/message/fragment/MessageFragment;Landroid/widget/TextView;)V

    .line 44
    .line 45
    .line 46
    new-instance v0, L〇00O0/Oo08;

    .line 47
    .line 48
    invoke-direct {v0, v2}, L〇00O0/Oo08;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {v1, p0, v0}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇088O()Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    invoke-virtual {v0}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->o〇0()V

    .line 59
    .line 60
    .line 61
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public onResume()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->O8o08O8O:Ljava/lang/String;

    .line 5
    .line 6
    invoke-static {v0}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected final o〇0〇o()Lcom/intsig/camscanner/message/adapter/MessageAdapter;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇OOo8〇0:Lcom/intsig/camscanner/message/adapter/MessageAdapter;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected final o〇O8OO()V
    .locals 3

    .line 1
    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/camscanner/multiimageedit/factory/NewInstanceFactoryImpl;->〇080()Landroidx/lifecycle/ViewModelProvider$NewInstanceFactory;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const-string v2, "getInstance()"

    .line 8
    .line 9
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    invoke-direct {v0, p0, v1}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    .line 13
    .line 14
    .line 15
    const-class v1, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel;

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel;

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel;->〇80〇808〇O()Landroidx/lifecycle/MutableLiveData;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    new-instance v1, Lcom/intsig/camscanner/message/fragment/MessageFragment$initDatabaseCallbackViewModel$1;

    .line 28
    .line 29
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/message/fragment/MessageFragment$initDatabaseCallbackViewModel$1;-><init>(Lcom/intsig/camscanner/message/fragment/MessageFragment;)V

    .line 30
    .line 31
    .line 32
    new-instance v2, L〇00O0/o〇0;

    .line 33
    .line 34
    invoke-direct {v2, v1}, L〇00O0/o〇0;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0, p0, v2}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 38
    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d030b

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇088O()Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇080OO8〇0:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-object v0

    .line 6
    :cond_0
    const-string v0, "messageLoader"

    .line 7
    .line 8
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected final 〇o〇88〇8()Lcom/intsig/camscanner/message/viewmodel/MessageViewModel;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->OO:Lcom/intsig/camscanner/message/viewmodel/MessageViewModel;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-object v0

    .line 6
    :cond_0
    const-string v0, "messageViewModel"

    .line 7
    .line 8
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    return-object v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected final 〇〇〇0(Lcom/intsig/camscanner/message/viewmodel/MessageViewModel;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/message/viewmodel/MessageViewModel;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->OO:Lcom/intsig/camscanner/message/viewmodel/MessageViewModel;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇〇〇00()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->〇OOo8〇0:Lcom/intsig/camscanner/message/adapter/MessageAdapter;

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    iget-object v2, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->oOo〇8o008:Ljava/util/List;

    .line 7
    .line 8
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O8〇o()Ljava/util/List;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Ljava/lang/Iterable;

    .line 16
    .line 17
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    if-eqz v2, :cond_1

    .line 26
    .line 27
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    check-cast v2, Lcom/intsig/camscanner/message/adapter/item/MessageItem;

    .line 32
    .line 33
    invoke-virtual {v2}, Lcom/intsig/camscanner/message/adapter/item/MessageItem;->O8()Lcom/intsig/camscanner/message/entity/CsSocketMsg;

    .line 34
    .line 35
    .line 36
    move-result-object v3

    .line 37
    invoke-virtual {v3}, Lcom/intsig/camscanner/message/entity/CsSocketMsg;->o〇0()Ljava/lang/Long;

    .line 38
    .line 39
    .line 40
    move-result-object v3

    .line 41
    if-eqz v3, :cond_0

    .line 42
    .line 43
    invoke-virtual {v2}, Lcom/intsig/camscanner/message/adapter/item/MessageItem;->O8()Lcom/intsig/camscanner/message/entity/CsSocketMsg;

    .line 44
    .line 45
    .line 46
    move-result-object v3

    .line 47
    invoke-virtual {v3}, Lcom/intsig/camscanner/message/entity/CsSocketMsg;->〇〇888()I

    .line 48
    .line 49
    .line 50
    move-result v3

    .line 51
    if-eq v3, v1, :cond_0

    .line 52
    .line 53
    invoke-static {}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$MsgTaskPair;->newBuilder()Lcom/intsig/developer/lib_message/SocketConnectionCmd$MsgTaskPair$Builder;

    .line 54
    .line 55
    .line 56
    move-result-object v3

    .line 57
    invoke-virtual {v2}, Lcom/intsig/camscanner/message/adapter/item/MessageItem;->O8()Lcom/intsig/camscanner/message/entity/CsSocketMsg;

    .line 58
    .line 59
    .line 60
    move-result-object v4

    .line 61
    invoke-virtual {v4}, Lcom/intsig/camscanner/message/entity/CsSocketMsg;->o〇0()Ljava/lang/Long;

    .line 62
    .line 63
    .line 64
    move-result-object v4

    .line 65
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    .line 66
    .line 67
    .line 68
    move-result-wide v4

    .line 69
    invoke-virtual {v3, v4, v5}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$MsgTaskPair$Builder;->〇080(J)Lcom/intsig/developer/lib_message/SocketConnectionCmd$MsgTaskPair$Builder;

    .line 70
    .line 71
    .line 72
    move-result-object v3

    .line 73
    invoke-virtual {v2}, Lcom/intsig/camscanner/message/adapter/item/MessageItem;->O8()Lcom/intsig/camscanner/message/entity/CsSocketMsg;

    .line 74
    .line 75
    .line 76
    move-result-object v2

    .line 77
    invoke-virtual {v2}, Lcom/intsig/camscanner/message/entity/CsSocketMsg;->oO80()Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object v2

    .line 81
    invoke-virtual {v3, v2}, Lcom/intsig/developer/lib_message/SocketConnectionCmd$MsgTaskPair$Builder;->〇o00〇〇Oo(Ljava/lang/String;)Lcom/intsig/developer/lib_message/SocketConnectionCmd$MsgTaskPair$Builder;

    .line 82
    .line 83
    .line 84
    move-result-object v2

    .line 85
    invoke-virtual {v2}, Lcom/google/protobuf/GeneratedMessageLite$Builder;->build()Lcom/google/protobuf/GeneratedMessageLite;

    .line 86
    .line 87
    .line 88
    move-result-object v2

    .line 89
    check-cast v2, Lcom/intsig/developer/lib_message/SocketConnectionCmd$MsgTaskPair;

    .line 90
    .line 91
    iget-object v3, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->oOo〇8o008:Ljava/util/List;

    .line 92
    .line 93
    const-string v4, "pair"

    .line 94
    .line 95
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    .line 97
    .line 98
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    .line 100
    .line 101
    goto :goto_0

    .line 102
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/message/entity/dao/MessageDbDao;->〇080:Lcom/intsig/camscanner/message/entity/dao/MessageDbDao;

    .line 103
    .line 104
    iget-object v2, p0, Lcom/intsig/camscanner/message/fragment/MessageFragment;->oOo〇8o008:Ljava/util/List;

    .line 105
    .line 106
    const/4 v3, 0x0

    .line 107
    const/4 v4, 0x0

    .line 108
    invoke-static {v0, v2, v4, v1, v3}, Lcom/intsig/camscanner/message/entity/dao/MessageDbDao;->Oooo8o0〇(Lcom/intsig/camscanner/message/entity/dao/MessageDbDao;Ljava/util/List;ZILjava/lang/Object;)V

    .line 109
    .line 110
    .line 111
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 112
    .line 113
    const v1, 0x7f0a1599

    .line 114
    .line 115
    .line 116
    invoke-virtual {v0, v1}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 117
    .line 118
    .line 119
    move-result-object v0

    .line 120
    check-cast v0, Landroid/widget/TextView;

    .line 121
    .line 122
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 123
    .line 124
    .line 125
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 126
    .line 127
    const v2, 0x7f060205

    .line 128
    .line 129
    .line 130
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 131
    .line 132
    .line 133
    move-result v1

    .line 134
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 135
    .line 136
    .line 137
    return-void
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

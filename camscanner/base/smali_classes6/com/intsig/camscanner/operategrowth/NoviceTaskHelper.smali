.class public Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;
.super Ljava/lang/Object;
.source "NoviceTaskHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceInterfaceListener;,
        Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$ClassHolder;,
        Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;
    }
.end annotation


# instance fields
.field private O8:Ljava/lang/String;

.field private Oo08:Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceInterfaceListener;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final o〇0:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/operategrowth/OperateGuideModel;",
            ">;"
        }
    .end annotation
.end field

.field private final 〇080:Ljava/lang/String;

.field private volatile 〇o00〇〇Oo:Z

.field private 〇o〇:Landroid/app/Application;

.field private final 〇〇888:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "NoviceTaskHelper"

    .line 3
    iput-object v0, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇080:Ljava/lang/String;

    .line 4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->o〇0:Ljava/util/List;

    .line 5
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇〇888:Ljava/util/HashMap;

    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->oO80()V

    return-void
.end method

.method synthetic constructor <init>(Lo0〇〇00/〇080;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;-><init>()V

    return-void
.end method

.method private O8(Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$4;->〇080:[I

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    aget p1, v0, p1

    .line 8
    .line 9
    const-string v0, "NoviceTaskHelper"

    .line 10
    .line 11
    const/4 v1, 0x1

    .line 12
    if-eq p1, v1, :cond_2

    .line 13
    .line 14
    const/4 v2, 0x2

    .line 15
    if-eq p1, v2, :cond_1

    .line 16
    .line 17
    const/4 v2, 0x3

    .line 18
    if-eq p1, v2, :cond_0

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const-string p1, "setShowFirstFinishNewTask = _CERTIFICATE"

    .line 22
    .line 23
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 27
    .line 28
    .line 29
    move-result p1

    .line 30
    if-nez p1, :cond_3

    .line 31
    .line 32
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o〇8oOO88()Z

    .line 33
    .line 34
    .line 35
    move-result p1

    .line 36
    if-nez p1, :cond_3

    .line 37
    .line 38
    const-string p1, "key_show_first_finish_certificate"

    .line 39
    .line 40
    invoke-static {p1, v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->OOOOOOo8(Ljava/lang/String;Z)V

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_1
    const-string p1, "setShowFirstFinishNewTask = _SHARE"

    .line 45
    .line 46
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 50
    .line 51
    .line 52
    move-result p1

    .line 53
    if-nez p1, :cond_3

    .line 54
    .line 55
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o〇8oOO88()Z

    .line 56
    .line 57
    .line 58
    move-result p1

    .line 59
    if-nez p1, :cond_3

    .line 60
    .line 61
    const-string p1, "key_show_first_finish_share"

    .line 62
    .line 63
    invoke-static {p1, v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->OOOOOOo8(Ljava/lang/String;Z)V

    .line 64
    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_2
    const-string p1, "setShowFirstFinishNewTask = _OCR"

    .line 68
    .line 69
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 73
    .line 74
    .line 75
    move-result p1

    .line 76
    if-nez p1, :cond_3

    .line 77
    .line 78
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o〇8oOO88()Z

    .line 79
    .line 80
    .line 81
    move-result p1

    .line 82
    if-nez p1, :cond_3

    .line 83
    .line 84
    const-string p1, "key_show_first_finish_ocr"

    .line 85
    .line 86
    invoke-static {p1, v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->OOOOOOo8(Ljava/lang/String;Z)V

    .line 87
    .line 88
    .line 89
    :cond_3
    :goto_0
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private OO0o〇〇〇〇0(Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;)Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->o〇0:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_1

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Lcom/intsig/camscanner/operategrowth/OperateGuideModel;

    .line 18
    .line 19
    iget-object v2, v1, Lcom/intsig/camscanner/operategrowth/OperateGuideModel;->act_id:Ljava/lang/String;

    .line 20
    .line 21
    iget-object v3, p1, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;->actType:Ljava/lang/String;

    .line 22
    .line 23
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    if-eqz v2, :cond_0

    .line 28
    .line 29
    invoke-virtual {v1}, Lcom/intsig/camscanner/operategrowth/OperateGuideModel;->hasDone()Z

    .line 30
    .line 31
    .line 32
    move-result p1

    .line 33
    return p1

    .line 34
    :cond_1
    const/4 p1, 0x1

    .line 35
    return p1
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇〇888(Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static 〇o〇()Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$ClassHolder;->〇080()Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇〇808〇(Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;)V
    .locals 4

    .line 1
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->O0〇OO8()Lcom/intsig/tianshu/UserInfo;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/tianshu/UserInfo;->getUserID()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz p1, :cond_2

    .line 10
    .line 11
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_0

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->O8(Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;)V

    .line 19
    .line 20
    .line 21
    iget-boolean v0, p1, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;->isCnNew:Z

    .line 22
    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    const-string v0, "cs_task"

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_1
    const-string v0, "cs_storage"

    .line 29
    .line 30
    :goto_0
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->O8()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    iget-object v2, p1, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;->actType:Ljava/lang/String;

    .line 35
    .line 36
    new-instance v3, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$3;

    .line 37
    .line 38
    invoke-direct {v3, p0, p1}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$3;-><init>(Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;)V

    .line 39
    .line 40
    .line 41
    invoke-static {v1, v0, v2, v3}, Lcom/intsig/tianshu/TianShuAPI;->oO80(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/okgo/callback/CustomStringCallback;)V

    .line 42
    .line 43
    .line 44
    return-void

    .line 45
    :cond_2
    :goto_1
    new-instance p1, Ljava/lang/StringBuilder;

    .line 46
    .line 47
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 48
    .line 49
    .line 50
    const-string v1, "updateNoviceTask noviceTaskType == null  ||   uid : "

    .line 51
    .line 52
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    const-string v0, "NoviceTaskHelper"

    .line 63
    .line 64
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    return-void
.end method

.method private 〇〇888(Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;)V
    .locals 5

    .line 1
    sget-object v0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;->NOVICE_NEW_OCR:Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;

    .line 2
    .line 3
    const-string v1, "NoviceTaskHelper"

    .line 4
    .line 5
    if-eq p1, v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;->NOVICE_NEW_SHARE:Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;

    .line 8
    .line 9
    if-eq p1, v0, :cond_0

    .line 10
    .line 11
    sget-object v0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;->NOVICE_NEW_CERTIFICATE:Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;

    .line 12
    .line 13
    if-eq p1, v0, :cond_0

    .line 14
    .line 15
    new-instance v0, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v2, "handleUpdateTaskSuccess default "

    .line 21
    .line 22
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o〇:Landroid/app/Application;

    .line 40
    .line 41
    iget v2, p1, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;->showRes:I

    .line 42
    .line 43
    invoke-static {v0, v2}, Lcom/intsig/utils/ToastUtils;->oO80(Landroid/content/Context;I)V

    .line 44
    .line 45
    .line 46
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->o〇0:Ljava/util/List;

    .line 47
    .line 48
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 53
    .line 54
    .line 55
    move-result v2

    .line 56
    if-eqz v2, :cond_2

    .line 57
    .line 58
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 59
    .line 60
    .line 61
    move-result-object v2

    .line 62
    check-cast v2, Lcom/intsig/camscanner/operategrowth/OperateGuideModel;

    .line 63
    .line 64
    iget-object v3, v2, Lcom/intsig/camscanner/operategrowth/OperateGuideModel;->act_id:Ljava/lang/String;

    .line 65
    .line 66
    iget-object v4, p1, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;->actType:Ljava/lang/String;

    .line 67
    .line 68
    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 69
    .line 70
    .line 71
    move-result v3

    .line 72
    if-eqz v3, :cond_1

    .line 73
    .line 74
    invoke-virtual {v2}, Lcom/intsig/camscanner/operategrowth/OperateGuideModel;->setHasDone()V

    .line 75
    .line 76
    .line 77
    new-instance p1, Ljava/lang/StringBuilder;

    .line 78
    .line 79
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 80
    .line 81
    .line 82
    const-string v0, "current mTaskGiftModels = "

    .line 83
    .line 84
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    iget-object v0, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->o〇0:Ljava/util/List;

    .line 88
    .line 89
    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    .line 90
    .line 91
    .line 92
    move-result-object v0

    .line 93
    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    .line 99
    .line 100
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 101
    .line 102
    .line 103
    move-result-object p1

    .line 104
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    .line 106
    .line 107
    :cond_2
    return-void
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method


# virtual methods
.method public OO0o〇〇(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/operategrowth/OperateGuideModel;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇O〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz p1, :cond_8

    .line 6
    .line 7
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-lez v1, :cond_8

    .line 12
    .line 13
    new-instance v1, Ljava/lang/StringBuilder;

    .line 14
    .line 15
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 16
    .line 17
    .line 18
    const-string v2, " oriData: "

    .line 19
    .line 20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-interface {p1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    invoke-static {v2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    const-string v2, "NoviceTaskHelper"

    .line 39
    .line 40
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    iget-object v1, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->o〇0:Ljava/util/List;

    .line 44
    .line 45
    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 46
    .line 47
    .line 48
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 53
    .line 54
    .line 55
    move-result v1

    .line 56
    const/4 v3, 0x0

    .line 57
    const/4 v4, 0x1

    .line 58
    if-eqz v1, :cond_5

    .line 59
    .line 60
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    check-cast v1, Lcom/intsig/camscanner/operategrowth/OperateGuideModel;

    .line 65
    .line 66
    invoke-static {}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;->values()[Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;

    .line 67
    .line 68
    .line 69
    move-result-object v5

    .line 70
    array-length v6, v5

    .line 71
    :goto_1
    if-ge v3, v6, :cond_2

    .line 72
    .line 73
    aget-object v7, v5, v3

    .line 74
    .line 75
    iget v8, v1, Lcom/intsig/camscanner/operategrowth/OperateGuideModel;->done:I

    .line 76
    .line 77
    if-nez v8, :cond_1

    .line 78
    .line 79
    iget-object v8, v1, Lcom/intsig/camscanner/operategrowth/OperateGuideModel;->act_id:Ljava/lang/String;

    .line 80
    .line 81
    iget-object v9, v7, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;->actType:Ljava/lang/String;

    .line 82
    .line 83
    invoke-static {v8, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 84
    .line 85
    .line 86
    move-result v8

    .line 87
    if-eqz v8, :cond_1

    .line 88
    .line 89
    iget-boolean v7, v7, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;->isCnNew:Z

    .line 90
    .line 91
    if-ne v0, v7, :cond_1

    .line 92
    .line 93
    iget-object v7, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->o〇0:Ljava/util/List;

    .line 94
    .line 95
    invoke-interface {v7, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    .line 97
    .line 98
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 99
    .line 100
    goto :goto_1

    .line 101
    :cond_2
    iget-object v3, v1, Lcom/intsig/camscanner/operategrowth/OperateGuideModel;->act_id:Ljava/lang/String;

    .line 102
    .line 103
    const-string v5, "cs_task_vip"

    .line 104
    .line 105
    invoke-static {v3, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 106
    .line 107
    .line 108
    move-result v3

    .line 109
    if-eqz v3, :cond_0

    .line 110
    .line 111
    iget v3, v1, Lcom/intsig/camscanner/operategrowth/OperateGuideModel;->done:I

    .line 112
    .line 113
    const/4 v5, -0x1

    .line 114
    if-nez v3, :cond_3

    .line 115
    .line 116
    invoke-static {v5}, Lcom/intsig/camscanner/util/PreferenceHelper;->oooo0o(I)V

    .line 117
    .line 118
    .line 119
    goto :goto_2

    .line 120
    :cond_3
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇OO0O8〇o()I

    .line 121
    .line 122
    .line 123
    move-result v3

    .line 124
    if-ne v3, v5, :cond_4

    .line 125
    .line 126
    invoke-static {v4}, Lcom/intsig/camscanner/util/PreferenceHelper;->oooo0o(I)V

    .line 127
    .line 128
    .line 129
    new-instance v3, Lcom/intsig/camscanner/eventbus/NewbieTaskVipMsgEvent;

    .line 130
    .line 131
    invoke-direct {v3}, Lcom/intsig/camscanner/eventbus/NewbieTaskVipMsgEvent;-><init>()V

    .line 132
    .line 133
    .line 134
    invoke-static {v3}, Lcom/intsig/camscanner/eventbus/CsEventBus;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 135
    .line 136
    .line 137
    :cond_4
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    .line 138
    .line 139
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 140
    .line 141
    .line 142
    const-string v4, "cs_task_vip done = "

    .line 143
    .line 144
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    .line 146
    .line 147
    iget v1, v1, Lcom/intsig/camscanner/operategrowth/OperateGuideModel;->done:I

    .line 148
    .line 149
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 150
    .line 151
    .line 152
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 153
    .line 154
    .line 155
    move-result-object v1

    .line 156
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    .line 158
    .line 159
    goto :goto_0

    .line 160
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->o〇0:Ljava/util/List;

    .line 161
    .line 162
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 163
    .line 164
    .line 165
    move-result p1

    .line 166
    if-lez p1, :cond_6

    .line 167
    .line 168
    const/4 v3, 0x1

    .line 169
    :cond_6
    iput-boolean v3, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o00〇〇Oo:Z

    .line 170
    .line 171
    iget-object p1, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->O8:Ljava/lang/String;

    .line 172
    .line 173
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 174
    .line 175
    .line 176
    move-result p1

    .line 177
    if-nez p1, :cond_7

    .line 178
    .line 179
    iget-object p1, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇〇888:Ljava/util/HashMap;

    .line 180
    .line 181
    iget-object v0, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->O8:Ljava/lang/String;

    .line 182
    .line 183
    iget-boolean v1, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o00〇〇Oo:Z

    .line 184
    .line 185
    xor-int/2addr v1, v4

    .line 186
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 187
    .line 188
    .line 189
    move-result-object v1

    .line 190
    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 191
    .line 192
    .line 193
    :cond_7
    new-instance p1, Ljava/lang/StringBuilder;

    .line 194
    .line 195
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 196
    .line 197
    .line 198
    const-string v0, "init novice success: "

    .line 199
    .line 200
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    .line 202
    .line 203
    iget-boolean v0, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o00〇〇Oo:Z

    .line 204
    .line 205
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 206
    .line 207
    .line 208
    const-string v0, "  data: "

    .line 209
    .line 210
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    .line 212
    .line 213
    iget-object v0, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->o〇0:Ljava/util/List;

    .line 214
    .line 215
    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    .line 216
    .line 217
    .line 218
    move-result-object v0

    .line 219
    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    .line 220
    .line 221
    .line 222
    move-result-object v0

    .line 223
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    .line 225
    .line 226
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 227
    .line 228
    .line 229
    move-result-object p1

    .line 230
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    .line 232
    .line 233
    :cond_8
    return-void
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public Oo08(Lcom/intsig/camscanner/web/UrlEntity;)Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->Oo08:Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceInterfaceListener;

    .line 2
    .line 3
    const-string v1, "NoviceTaskHelper"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-eqz v0, :cond_7

    .line 7
    .line 8
    invoke-interface {v0}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceInterfaceListener;->〇080()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    goto :goto_2

    .line 15
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/web/UrlEntity;->O8()Lcom/intsig/camscanner/web/FUNCTION;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    if-nez p1, :cond_1

    .line 20
    .line 21
    return v2

    .line 22
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 23
    .line 24
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 25
    .line 26
    .line 27
    const-string v3, "handleInnerUrl function: "

    .line 28
    .line 29
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    sget-object v0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$4;->〇o00〇〇Oo:[I

    .line 43
    .line 44
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    .line 45
    .line 46
    .line 47
    move-result p1

    .line 48
    aget p1, v0, p1

    .line 49
    .line 50
    const/4 v0, 0x1

    .line 51
    if-eq p1, v0, :cond_5

    .line 52
    .line 53
    const/4 v1, 0x2

    .line 54
    if-eq p1, v1, :cond_4

    .line 55
    .line 56
    const/4 v1, 0x3

    .line 57
    if-eq p1, v1, :cond_3

    .line 58
    .line 59
    const/4 v1, 0x4

    .line 60
    if-eq p1, v1, :cond_2

    .line 61
    .line 62
    const/4 p1, 0x0

    .line 63
    goto :goto_1

    .line 64
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->Oo08:Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceInterfaceListener;

    .line 65
    .line 66
    invoke-interface {p1}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceInterfaceListener;->〇o〇()V

    .line 67
    .line 68
    .line 69
    sget-object p1, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;->NOVICE_SCAN:Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;

    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->Oo08:Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceInterfaceListener;

    .line 73
    .line 74
    invoke-interface {p1}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceInterfaceListener;->Oo08()V

    .line 75
    .line 76
    .line 77
    sget-object p1, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;->NOVICE_CERTIFICATE:Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;

    .line 78
    .line 79
    goto :goto_0

    .line 80
    :cond_4
    iget-object p1, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->Oo08:Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceInterfaceListener;

    .line 81
    .line 82
    invoke-interface {p1}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceInterfaceListener;->O8()V

    .line 83
    .line 84
    .line 85
    sget-object p1, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;->NOVICE_SHARE:Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;

    .line 86
    .line 87
    goto :goto_0

    .line 88
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->Oo08:Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceInterfaceListener;

    .line 89
    .line 90
    invoke-interface {p1}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceInterfaceListener;->〇o00〇〇Oo()V

    .line 91
    .line 92
    .line 93
    sget-object p1, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;->NOVICE_OCR:Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;

    .line 94
    .line 95
    :goto_0
    const/4 v2, 0x1

    .line 96
    :goto_1
    if-eqz p1, :cond_6

    .line 97
    .line 98
    iget-object v0, p1, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;->clickPoint:Ljava/lang/String;

    .line 99
    .line 100
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 101
    .line 102
    .line 103
    move-result v0

    .line 104
    if-nez v0, :cond_6

    .line 105
    .line 106
    const-string v0, "CSReferearn"

    .line 107
    .line 108
    iget-object p1, p1, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;->clickPoint:Ljava/lang/String;

    .line 109
    .line 110
    invoke-static {v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    .line 112
    .line 113
    :cond_6
    return v2

    .line 114
    :cond_7
    :goto_2
    new-instance p1, Ljava/lang/StringBuilder;

    .line 115
    .line 116
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 117
    .line 118
    .line 119
    const-string v0, "handleInnerUrl fail: "

    .line 120
    .line 121
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    .line 123
    .line 124
    iget-object v0, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->Oo08:Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceInterfaceListener;

    .line 125
    .line 126
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 127
    .line 128
    .line 129
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 130
    .line 131
    .line 132
    move-result-object p1

    .line 133
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    .line 135
    .line 136
    return v2
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public Oooo8o0〇(Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceInterfaceListener;)V
    .locals 0
    .param p1    # Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceInterfaceListener;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->Oo08:Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceInterfaceListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public oO80()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iput-object v0, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o〇:Landroid/app/Application;

    .line 6
    .line 7
    new-instance v0, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v1, "init isNovice : "

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    iget-boolean v1, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o00〇〇Oo:Z

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const-string v1, "NoviceTaskHelper"

    .line 27
    .line 28
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o〇0(Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "handleTask : "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "NoviceTaskHelper"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-eqz v0, :cond_0

    .line 32
    .line 33
    invoke-virtual {p0}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇80〇808〇O()Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-eqz v0, :cond_0

    .line 38
    .line 39
    iget-boolean v0, p1, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;->isCnNew:Z

    .line 40
    .line 41
    invoke-virtual {p0}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇O〇()Z

    .line 42
    .line 43
    .line 44
    move-result v1

    .line 45
    if-ne v0, v1, :cond_0

    .line 46
    .line 47
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;)Z

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    if-nez v0, :cond_0

    .line 52
    .line 53
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇〇808〇(Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;)V

    .line 54
    .line 55
    .line 56
    :cond_0
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇80〇808〇O()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o00〇〇Oo:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇8o8o〇(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/operategrowth/OperateGuideModel;",
            ">;"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    :try_start_0
    iput-object p1, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->O8:Ljava/lang/String;

    .line 3
    .line 4
    invoke-static {p1, p2}, Lcom/intsig/tianshu/TianShuAPI;->ooOO(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    invoke-static {p1}, Lcom/lzy/okgo/OkGo;->get(Ljava/lang/String;)Lcom/lzy/okgo/request/GetRequest;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    invoke-virtual {p1}, Lcom/lzy/okgo/request/base/Request;->execute()Lokhttp3/Response;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    invoke-virtual {p1}, Lokhttp3/Response;->Oo08()Lokhttp3/ResponseBody;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    if-eqz p1, :cond_0

    .line 21
    .line 22
    new-instance p2, Lcom/google/gson/stream/JsonReader;

    .line 23
    .line 24
    invoke-virtual {p1}, Lokhttp3/ResponseBody;->charStream()Ljava/io/Reader;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    invoke-direct {p2, p1}, Lcom/google/gson/stream/JsonReader;-><init>(Ljava/io/Reader;)V

    .line 29
    .line 30
    .line 31
    new-instance p1, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$1;

    .line 32
    .line 33
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$1;-><init>(Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {p1}, Lcom/google/gson/reflect/TypeToken;->getType()Ljava/lang/reflect/Type;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    invoke-static {p2, p1}, Lcom/intsig/okgo/utils/GsonUtils;->〇080(Lcom/google/gson/stream/JsonReader;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    check-cast p1, Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    .line 46
    return-object p1

    .line 47
    :cond_0
    return-object v0

    .line 48
    :catch_0
    move-exception p1

    .line 49
    const-string p2, "NoviceTaskHelper"

    .line 50
    .line 51
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 52
    .line 53
    .line 54
    return-object v0
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public 〇O8o08O(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇〇888:Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Boolean;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    return-void

    .line 18
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$2;

    .line 19
    .line 20
    invoke-direct {v0, p0, p1, p2}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$2;-><init>(Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    const-string p1, "NoviceTaskHelper"

    .line 24
    .line 25
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->Oooo8o0〇(Ljava/lang/String;)Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->o〇0()V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
.end method

.method public 〇O〇()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->〇00O0O0()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o00〇〇Oo()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o00〇〇Oo:Z

    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->o〇0:Ljava/util/List;

    .line 5
    .line 6
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;
.super Landroidx/recyclerview/widget/RecyclerView$Adapter;
.source "ScanDoneOperationsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;,
        Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;,
        Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;,
        Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;,
        Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/RecyclerView$Adapter<",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private O8o08O8O:Z

.field private final OO:Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private final o0:Landroid/content/Context;

.field private oOo0:Z

.field private oOo〇8o008:I

.field private o〇00O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/advertisement/interfaces/RealRequestAbs;",
            ">;"
        }
    .end annotation
.end field

.field private 〇080OO8〇0:Z

.field private 〇08O〇00〇o:Lcom/intsig/advertisement/interfaces/RealRequestAbs;

.field private 〇0O:Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;

.field private final 〇OOo8〇0:Lcom/intsig/camscanner/scandone/DonePresenter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/intsig/camscanner/scandone/DonePresenter;Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;Z)V
    .locals 2
    .param p3    # Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/ArrayList;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    iput-boolean v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->O8o08O8O:Z

    .line 13
    .line 14
    iput-boolean v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇080OO8〇0:Z

    .line 15
    .line 16
    const/4 v1, 0x0

    .line 17
    iput-object v1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇0O:Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;

    .line 18
    .line 19
    iput v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->oOo〇8o008:I

    .line 20
    .line 21
    iput-boolean v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->oOo0:Z

    .line 22
    .line 23
    iput-object p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    .line 24
    .line 25
    iput-object p2, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇OOo8〇0:Lcom/intsig/camscanner/scandone/DonePresenter;

    .line 26
    .line 27
    iput-object p3, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->OO:Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 28
    .line 29
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    iget p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson;->completion_page:I

    .line 34
    .line 35
    const/4 p2, 0x3

    .line 36
    const/4 p3, 0x1

    .line 37
    if-ne p1, p2, :cond_0

    .line 38
    .line 39
    if-nez p4, :cond_0

    .line 40
    .line 41
    iput-boolean p3, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->O8o08O8O:Z

    .line 42
    .line 43
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    invoke-virtual {p1}, Lcom/intsig/tsapp/sync/AppConfigJson;->isImageDiscernTagOpen()Z

    .line 48
    .line 49
    .line 50
    move-result p1

    .line 51
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 52
    .line 53
    .line 54
    move-result-object p2

    .line 55
    invoke-virtual {p2}, Lcom/intsig/tsapp/sync/AppConfigJson;->isImageDiscernTagTest2()Z

    .line 56
    .line 57
    .line 58
    move-result p2

    .line 59
    if-eqz p1, :cond_1

    .line 60
    .line 61
    if-eqz p2, :cond_1

    .line 62
    .line 63
    iput-boolean p3, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇080OO8〇0:Z

    .line 64
    .line 65
    :cond_1
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private synthetic O08000(Landroid/widget/LinearLayout;Landroid/view/View;)V
    .locals 1

    .line 1
    const/4 p2, 0x1

    .line 2
    new-array p2, p2, [Landroid/view/View;

    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    aput-object p1, p2, v0

    .line 6
    .line 7
    const/16 p1, 0x8

    .line 8
    .line 9
    invoke-static {p1, p2}, Lcom/intsig/utils/CustomViewUtils;->〇o〇(I[Landroid/view/View;)V

    .line 10
    .line 11
    .line 12
    invoke-static {}, Lcom/intsig/advertisement/logagent/LogAgentManager;->〇8o8o〇()Lcom/intsig/advertisement/logagent/LogAgentManager;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    iget-object p2, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇08O〇00〇o:Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 17
    .line 18
    invoke-virtual {p1, p2}, Lcom/intsig/advertisement/logagent/LogAgentManager;->〇0〇O0088o(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V

    .line 19
    .line 20
    .line 21
    const/4 p1, 0x0

    .line 22
    iput-object p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇08O〇00〇o:Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O8〇o(Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;)Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-direct {v0, v1, v2, v2}, Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    if-eqz p1, :cond_4

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getShow_icon()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    const/4 v2, 0x0

    .line 16
    const/4 v3, 0x1

    .line 17
    if-ne v1, v3, :cond_0

    .line 18
    .line 19
    const/4 v1, 0x1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v1, 0x0

    .line 22
    :goto_0
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;->〇〇8O0〇8(Z)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getUrl()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;->o〇O8〇〇o(Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getDptrackers()Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/DpLinkTrackers;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;->OoO8(Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/DpLinkTrackers;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getUploadGeneralParam()I

    .line 40
    .line 41
    .line 42
    move-result v1

    .line 43
    if-ne v1, v3, :cond_1

    .line 44
    .line 45
    const/4 v1, 0x1

    .line 46
    goto :goto_1

    .line 47
    :cond_1
    const/4 v1, 0x0

    .line 48
    :goto_1
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;->〇〇808〇(Z)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getMacro()Ljava/util/HashMap;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;->〇O00(Ljava/util/HashMap;)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getDeeplink_url()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v1

    .line 62
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;->〇0〇O0088o(Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getClickTrakers()[Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object v1

    .line 69
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;->〇O〇([Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getImpressionTrakers()[Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v1

    .line 76
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;->oo88o8O([Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getJumpAlert()I

    .line 80
    .line 81
    .line 82
    move-result v1

    .line 83
    if-ne v1, v3, :cond_2

    .line 84
    .line 85
    const/4 v1, 0x1

    .line 86
    goto :goto_2

    .line 87
    :cond_2
    const/4 v1, 0x0

    .line 88
    :goto_2
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;->o800o8O(Z)V

    .line 89
    .line 90
    .line 91
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getIsWebFullScreen()I

    .line 92
    .line 93
    .line 94
    move-result p1

    .line 95
    if-ne p1, v3, :cond_3

    .line 96
    .line 97
    const/4 v2, 0x1

    .line 98
    :cond_3
    invoke-virtual {v0, v2}, Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;->O〇8O8〇008(Z)V

    .line 99
    .line 100
    .line 101
    :cond_4
    return-object v0
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private OOO〇O0(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 6
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    instance-of v0, p1, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v1, "convertVipTaskBanner but get a "

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    const-string v0, "ScanDoneOperationsAdapter"

    .line 23
    .line 24
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    return-void

    .line 28
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇0O:Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;

    .line 29
    .line 30
    const/16 v1, 0x8

    .line 31
    .line 32
    if-nez v0, :cond_1

    .line 33
    .line 34
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 35
    .line 36
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 37
    .line 38
    .line 39
    return-void

    .line 40
    :cond_1
    check-cast p1, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;

    .line 41
    .line 42
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;->〇o00〇〇Oo()Z

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    if-eqz v0, :cond_2

    .line 47
    .line 48
    invoke-static {p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;->O8ooOoo〇(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;)Landroid/widget/ImageView;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    const v2, 0x7f080bbe

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 56
    .line 57
    .line 58
    invoke-static {p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;->〇00(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;)Landroid/widget/ImageView;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    const/4 v2, 0x0

    .line 63
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 64
    .line 65
    .line 66
    iget-object v0, p1, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;->O8o08O8O:Landroid/widget/TextView;

    .line 67
    .line 68
    const v2, 0x7f080bba

    .line 69
    .line 70
    .line 71
    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 72
    .line 73
    .line 74
    goto :goto_0

    .line 75
    :cond_2
    invoke-static {p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;->O8ooOoo〇(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;)Landroid/widget/ImageView;

    .line 76
    .line 77
    .line 78
    move-result-object v0

    .line 79
    const v2, 0x7f080bbf

    .line 80
    .line 81
    .line 82
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 83
    .line 84
    .line 85
    invoke-static {p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;->〇00(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;)Landroid/widget/ImageView;

    .line 86
    .line 87
    .line 88
    move-result-object v0

    .line 89
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 90
    .line 91
    .line 92
    iget-object v0, p1, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;->O8o08O8O:Landroid/widget/TextView;

    .line 93
    .line 94
    const v2, 0x7f080bbb

    .line 95
    .line 96
    .line 97
    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 98
    .line 99
    .line 100
    :goto_0
    invoke-static {p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;->〇oOO8O8(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;)Landroid/widget/TextView;

    .line 101
    .line 102
    .line 103
    move-result-object v0

    .line 104
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 105
    .line 106
    .line 107
    move-result-object v0

    .line 108
    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 109
    .line 110
    iget-object v2, p1, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;->O8o08O8O:Landroid/widget/TextView;

    .line 111
    .line 112
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 113
    .line 114
    .line 115
    move-result-object v2

    .line 116
    check-cast v2, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 117
    .line 118
    iget-object v3, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇0O:Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;

    .line 119
    .line 120
    invoke-virtual {v3}, Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;->〇o〇()I

    .line 121
    .line 122
    .line 123
    move-result v3

    .line 124
    const/4 v4, 0x1

    .line 125
    const/4 v5, 0x6

    .line 126
    if-ne v3, v4, :cond_3

    .line 127
    .line 128
    invoke-static {p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;->O〇8O8〇008(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;)Landroid/widget/ImageView;

    .line 129
    .line 130
    .line 131
    move-result-object v1

    .line 132
    const v3, 0x7f080bb9

    .line 133
    .line 134
    .line 135
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 136
    .line 137
    .line 138
    invoke-static {p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;->〇0000OOO(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;)Landroid/widget/TextView;

    .line 139
    .line 140
    .line 141
    move-result-object v1

    .line 142
    const v3, 0x7f131663

    .line 143
    .line 144
    .line 145
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 146
    .line 147
    .line 148
    iget-object v1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    .line 149
    .line 150
    invoke-static {v1, v5}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 151
    .line 152
    .line 153
    move-result v1

    .line 154
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 155
    .line 156
    invoke-static {p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;->〇oOO8O8(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;)Landroid/widget/TextView;

    .line 157
    .line 158
    .line 159
    move-result-object v1

    .line 160
    iget-object v3, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇0O:Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;

    .line 161
    .line 162
    invoke-static {v3}, Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneVipTaskManager;->〇〇888(Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;)Ljava/lang/CharSequence;

    .line 163
    .line 164
    .line 165
    move-result-object v3

    .line 166
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    .line 168
    .line 169
    iget-object v1, p1, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;->O8o08O8O:Landroid/widget/TextView;

    .line 170
    .line 171
    const v3, 0x7f131660

    .line 172
    .line 173
    .line 174
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 175
    .line 176
    .line 177
    iget-object v1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    .line 178
    .line 179
    const/16 v3, 0x55

    .line 180
    .line 181
    invoke-static {v1, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 182
    .line 183
    .line 184
    move-result v1

    .line 185
    iput v1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 186
    .line 187
    goto :goto_3

    .line 188
    :cond_3
    iget-object v3, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇0O:Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;

    .line 189
    .line 190
    invoke-virtual {v3}, Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;->〇o〇()I

    .line 191
    .line 192
    .line 193
    move-result v3

    .line 194
    const/4 v4, 0x2

    .line 195
    if-ne v3, v4, :cond_6

    .line 196
    .line 197
    invoke-static {p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;->O〇8O8〇008(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;)Landroid/widget/ImageView;

    .line 198
    .line 199
    .line 200
    move-result-object v3

    .line 201
    const v4, 0x7f080bc1

    .line 202
    .line 203
    .line 204
    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 205
    .line 206
    .line 207
    invoke-static {p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;->〇0000OOO(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;)Landroid/widget/TextView;

    .line 208
    .line 209
    .line 210
    move-result-object v3

    .line 211
    const v4, 0x7f131664

    .line 212
    .line 213
    .line 214
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 215
    .line 216
    .line 217
    iget-object v3, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇0O:Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;

    .line 218
    .line 219
    invoke-virtual {v3}, Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;->〇080()I

    .line 220
    .line 221
    .line 222
    move-result v3

    .line 223
    const/4 v4, 0x5

    .line 224
    if-eq v3, v4, :cond_5

    .line 225
    .line 226
    iget-object v3, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇0O:Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;

    .line 227
    .line 228
    invoke-virtual {v3}, Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;->〇080()I

    .line 229
    .line 230
    .line 231
    move-result v3

    .line 232
    if-eq v3, v5, :cond_5

    .line 233
    .line 234
    iget-object v3, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇0O:Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;

    .line 235
    .line 236
    invoke-virtual {v3}, Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;->〇080()I

    .line 237
    .line 238
    .line 239
    move-result v3

    .line 240
    const/4 v4, 0x7

    .line 241
    if-ne v3, v4, :cond_4

    .line 242
    .line 243
    goto :goto_1

    .line 244
    :cond_4
    iget-object v1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    .line 245
    .line 246
    const/4 v3, 0x4

    .line 247
    invoke-static {v1, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 248
    .line 249
    .line 250
    move-result v1

    .line 251
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 252
    .line 253
    goto :goto_2

    .line 254
    :cond_5
    :goto_1
    iget-object v3, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    .line 255
    .line 256
    invoke-static {v3, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 257
    .line 258
    .line 259
    move-result v1

    .line 260
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 261
    .line 262
    :goto_2
    invoke-static {p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;->〇oOO8O8(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;)Landroid/widget/TextView;

    .line 263
    .line 264
    .line 265
    move-result-object v1

    .line 266
    iget-object v3, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇0O:Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;

    .line 267
    .line 268
    invoke-static {v3}, Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneVipTaskManager;->〇〇888(Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;)Ljava/lang/CharSequence;

    .line 269
    .line 270
    .line 271
    move-result-object v3

    .line 272
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 273
    .line 274
    .line 275
    iget-object v1, p1, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;->O8o08O8O:Landroid/widget/TextView;

    .line 276
    .line 277
    iget-object v3, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇0O:Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;

    .line 278
    .line 279
    invoke-static {v3}, Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneVipTaskManager;->oO80(Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;)Ljava/lang/String;

    .line 280
    .line 281
    .line 282
    move-result-object v3

    .line 283
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 284
    .line 285
    .line 286
    iget-object v1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    .line 287
    .line 288
    const/16 v3, 0x4c

    .line 289
    .line 290
    invoke-static {v1, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 291
    .line 292
    .line 293
    move-result v1

    .line 294
    iput v1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 295
    .line 296
    :cond_6
    :goto_3
    invoke-static {p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;->〇oOO8O8(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;)Landroid/widget/TextView;

    .line 297
    .line 298
    .line 299
    move-result-object v1

    .line 300
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 301
    .line 302
    .line 303
    iget-object p1, p1, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;->O8o08O8O:Landroid/widget/TextView;

    .line 304
    .line 305
    invoke-virtual {p1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 306
    .line 307
    .line 308
    return-void
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private Oo8Oo00oo(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)Z
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    instance-of v1, p1, Lcom/intsig/advertisement/interfaces/NativeRequest;

    .line 5
    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    check-cast p1, Lcom/intsig/advertisement/interfaces/NativeRequest;

    .line 9
    .line 10
    invoke-virtual {p1}, Lcom/intsig/advertisement/interfaces/NativeRequest;->getOriginalType()Lcom/intsig/advertisement/enums/SourceType;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    sget-object v1, Lcom/intsig/advertisement/enums/SourceType;->TouTiao:Lcom/intsig/advertisement/enums/SourceType;

    .line 15
    .line 16
    if-ne p1, v1, :cond_0

    .line 17
    .line 18
    const/4 v0, 0x1

    .line 19
    :cond_0
    return v0
    .line 20
.end method

.method public static synthetic OoO8(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;Landroid/widget/TextView;Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->oO(Landroid/widget/TextView;Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private Ooo(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 2
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "ScanDoneOperationsAdapter"

    .line 2
    .line 3
    const-string v1, "updateVipMonthRedEnvelope"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    instance-of v0, p1, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;

    .line 9
    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    check-cast p1, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;

    .line 14
    .line 15
    iget v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->oOo〇8o008:I

    .line 16
    .line 17
    const/4 v1, 0x1

    .line 18
    if-ne v0, v1, :cond_2

    .line 19
    .line 20
    iget-object v0, p1, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;->o0:Lcom/intsig/camscanner/databinding/LayoutScanDoneVipMonthCard1Binding;

    .line 21
    .line 22
    if-eqz v0, :cond_2

    .line 23
    .line 24
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutScanDoneVipMonthCard1Binding;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 25
    .line 26
    iget-boolean v1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->oOo0:Z

    .line 27
    .line 28
    if-eqz v1, :cond_1

    .line 29
    .line 30
    const/4 v1, 0x0

    .line 31
    goto :goto_0

    .line 32
    :cond_1
    const/4 v1, 0x4

    .line 33
    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 34
    .line 35
    .line 36
    iget-boolean v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->oOo0:Z

    .line 37
    .line 38
    if-eqz v0, :cond_2

    .line 39
    .line 40
    iget-object p1, p1, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;->o0:Lcom/intsig/camscanner/databinding/LayoutScanDoneVipMonthCard1Binding;

    .line 41
    .line 42
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LayoutScanDoneVipMonthCard1Binding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 43
    .line 44
    invoke-static {p1}, Lcom/intsig/camscanner/purchase/scandone/vip_month/ScanDoneVipMonthManager;->OO0o〇〇〇〇0(Landroid/view/View;)V

    .line 45
    .line 46
    .line 47
    :cond_2
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic O〇8O8〇008(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic O〇O〇oO(Landroid/view/View;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    check-cast p1, Ljava/lang/Integer;

    .line 6
    .line 7
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 12
    .line 13
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-lt p1, v0, :cond_0

    .line 18
    .line 19
    return-void

    .line 20
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 21
    .line 22
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    check-cast p1, Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 27
    .line 28
    invoke-static {}, Lcom/intsig/advertisement/logagent/LogAgentManager;->〇8o8o〇()Lcom/intsig/advertisement/logagent/LogAgentManager;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-virtual {v0, p1}, Lcom/intsig/advertisement/logagent/LogAgentManager;->〇0〇O0088o(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 36
    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    .line 39
    .line 40
    instance-of v0, v0, Landroidx/fragment/app/FragmentActivity;

    .line 41
    .line 42
    if-eqz v0, :cond_1

    .line 43
    .line 44
    sget-object v0, Lcom/intsig/advertisement/enums/PositionType;->ScanDone:Lcom/intsig/advertisement/enums/PositionType;

    .line 45
    .line 46
    invoke-static {v0}, Lcom/intsig/camscanner/ads/dialog/FreeAdsTipsDialog;->〇0ooOOo(Lcom/intsig/advertisement/enums/PositionType;)Z

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    if-eqz v0, :cond_1

    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    .line 53
    .line 54
    check-cast v0, Landroidx/fragment/app/FragmentActivity;

    .line 55
    .line 56
    invoke-virtual {p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 57
    .line 58
    .line 59
    move-result-object p1

    .line 60
    invoke-static {v0, p1}, Lcom/intsig/camscanner/ads/dialog/FreeAdsTipsDialog;->〇〇〇00(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/advertisement/params/RequestParam;)V

    .line 61
    .line 62
    .line 63
    :cond_1
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private o8(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)Z
    .locals 1

    .line 1
    instance-of v0, p1, Lcom/intsig/advertisement/adapters/sources/cs/CsNative;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast p1, Lcom/intsig/advertisement/adapters/sources/cs/CsNative;

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/cs/CsNative;->isShowAdTag()Z

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    xor-int/lit8 p1, p1, 0x1

    .line 12
    .line 13
    return p1

    .line 14
    :cond_0
    const/4 p1, 0x0

    .line 15
    return p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o800o8O(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇〇0o(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic o8oO〇()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0ooO(I)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private synthetic oO(Landroid/widget/TextView;Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;)V
    .locals 0

    .line 1
    invoke-virtual {p2}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getTitle()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p2

    .line 5
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->oO00OOO(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private oO00OOO(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 4

    .line 1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    .line 9
    .line 10
    const v1, 0x7f1305da

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->oo〇(Landroid/widget/TextView;Ljava/lang/String;)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object p2

    .line 21
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    new-instance v2, Landroid/text/SpannableString;

    .line 26
    .line 27
    invoke-direct {v2, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 28
    .line 29
    .line 30
    new-instance p2, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$3;

    .line 31
    .line 32
    invoke-direct {p2, p0, v0}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$3;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    sub-int v0, v1, v0

    .line 40
    .line 41
    const/16 v3, 0x21

    .line 42
    .line 43
    invoke-virtual {v2, p2, v0, v1, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic oo88o8O(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇8〇0〇o〇O(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private oo〇(Landroid/widget/TextView;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "  "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    .line 12
    .line 13
    const v2, 0x7f1305da

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    sub-int/2addr v1, v2

    .line 36
    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    .line 37
    .line 38
    .line 39
    move-result v2

    .line 40
    sub-int/2addr v1, v2

    .line 41
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    new-instance v2, Ljava/lang/StringBuilder;

    .line 46
    .line 47
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 48
    .line 49
    .line 50
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v2

    .line 60
    invoke-virtual {p1, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 61
    .line 62
    .line 63
    move-result v3

    .line 64
    mul-int/lit8 v1, v1, 0x2

    .line 65
    .line 66
    int-to-float v1, v1

    .line 67
    cmpg-float v3, v3, v1

    .line 68
    .line 69
    if-gez v3, :cond_0

    .line 70
    .line 71
    return-object v2

    .line 72
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    .line 73
    .line 74
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 75
    .line 76
    .line 77
    const-string v3, "..."

    .line 78
    .line 79
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v2

    .line 89
    invoke-virtual {p1, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    .line 90
    .line 91
    .line 92
    move-result v2

    .line 93
    sub-float/2addr v1, v2

    .line 94
    float-to-int v1, v1

    .line 95
    int-to-float v1, v1

    .line 96
    const/4 v2, 0x0

    .line 97
    const/4 v4, 0x1

    .line 98
    invoke-virtual {p1, p2, v4, v1, v2}, Landroid/graphics/Paint;->breakText(Ljava/lang/String;ZF[F)I

    .line 99
    .line 100
    .line 101
    move-result p1

    .line 102
    if-le p1, v4, :cond_1

    .line 103
    .line 104
    add-int/lit8 p1, p1, -0x1

    .line 105
    .line 106
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    .line 107
    .line 108
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 109
    .line 110
    .line 111
    const/4 v2, 0x0

    .line 112
    invoke-virtual {p2, v2, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 113
    .line 114
    .line 115
    move-result-object p1

    .line 116
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 117
    .line 118
    .line 119
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    .line 121
    .line 122
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    .line 124
    .line 125
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 126
    .line 127
    .line 128
    move-result-object p1

    .line 129
    return-object p1
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private synthetic o〇0OOo〇0(Landroid/view/View;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    .line 2
    .line 3
    invoke-static {p1}, Lcom/intsig/utils/WebUrlUtils;->OoO8(Landroid/content/Context;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {p1, v0}, Lcom/intsig/webview/util/WebUtil;->〇8o8o〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o〇8(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)Z
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_1

    .line 3
    .line 4
    invoke-virtual {p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    invoke-virtual {p1}, Lcom/intsig/advertisement/params/RequestParam;->〇〇8O0〇8()Lcom/intsig/advertisement/enums/SourceType;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    sget-object v1, Lcom/intsig/advertisement/enums/SourceType;->Admob:Lcom/intsig/advertisement/enums/SourceType;

    .line 13
    .line 14
    if-eq p1, v1, :cond_0

    .line 15
    .line 16
    sget-object v1, Lcom/intsig/advertisement/enums/SourceType;->Facebook:Lcom/intsig/advertisement/enums/SourceType;

    .line 17
    .line 18
    if-ne p1, v1, :cond_1

    .line 19
    .line 20
    :cond_0
    const/4 v0, 0x1

    .line 21
    :cond_1
    return v0
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private o〇8oOO88()V
    .locals 4

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    iget v1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->oOo〇8o008:I

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    const-string v1, ""

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "CSScanDone"

    .line 21
    .line 22
    const-string v2, "card_click"

    .line 23
    .line 24
    const-string v3, "type"

    .line 25
    .line 26
    invoke-static {v1, v2, v3, v0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    .line 30
    .line 31
    new-instance v1, Loo8〇〇/〇0;

    .line 32
    .line 33
    invoke-direct {v1, p0}, Loo8〇〇/〇0;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;)V

    .line 34
    .line 35
    .line 36
    invoke-static {v0, v1}, Lcom/intsig/camscanner/purchase/scandone/vip_month/ScanDoneVipMonthManager;->〇0〇O0088o(Landroid/content/Context;Lcom/intsig/camscanner/purchase/scandone/vip_month/ScanDoneVipMonthManager$IPurchaseSuccessCallback;)V

    .line 37
    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic o〇O8〇〇o(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o8oO〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o〇〇0〇(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 17
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    instance-of v2, v1, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;

    .line 6
    .line 7
    const-string v3, "ScanDoneOperationsAdapter"

    .line 8
    .line 9
    if-nez v2, :cond_0

    .line 10
    .line 11
    new-instance v2, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v4, "convertNewContent but get a "

    .line 17
    .line 18
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-static {v3, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    return-void

    .line 32
    :cond_0
    check-cast v1, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;

    .line 33
    .line 34
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇o(Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneAdView;)V

    .line 35
    .line 36
    .line 37
    iget-object v2, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->OO:Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 38
    .line 39
    if-eqz v2, :cond_1

    .line 40
    .line 41
    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;->o8(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;)Landroid/view/View;

    .line 42
    .line 43
    .line 44
    move-result-object v2

    .line 45
    if-eqz v2, :cond_1

    .line 46
    .line 47
    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;->〇oOO8O8(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;)Landroid/widget/ImageView;

    .line 48
    .line 49
    .line 50
    move-result-object v2

    .line 51
    if-eqz v2, :cond_1

    .line 52
    .line 53
    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;->〇0000OOO(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;)Landroid/widget/TextView;

    .line 54
    .line 55
    .line 56
    move-result-object v2

    .line 57
    if-eqz v2, :cond_1

    .line 58
    .line 59
    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;->O8ooOoo〇(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;)Landroid/widget/ImageView;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    if-eqz v2, :cond_1

    .line 64
    .line 65
    sget-object v4, Lcom/intsig/camscanner/scandone/ScanDoneUtil;->〇080:Lcom/intsig/camscanner/scandone/ScanDoneUtil;

    .line 66
    .line 67
    iget-object v5, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->OO:Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 68
    .line 69
    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;->o8(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;)Landroid/view/View;

    .line 70
    .line 71
    .line 72
    move-result-object v6

    .line 73
    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;->〇oOO8O8(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;)Landroid/widget/ImageView;

    .line 74
    .line 75
    .line 76
    move-result-object v7

    .line 77
    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;->〇0000OOO(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;)Landroid/widget/TextView;

    .line 78
    .line 79
    .line 80
    move-result-object v8

    .line 81
    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;->O8ooOoo〇(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;)Landroid/widget/ImageView;

    .line 82
    .line 83
    .line 84
    move-result-object v9

    .line 85
    invoke-virtual/range {v4 .. v9}, Lcom/intsig/camscanner/scandone/ScanDoneUtil;->〇o〇(Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;Landroid/view/View;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/ImageView;)V

    .line 86
    .line 87
    .line 88
    :cond_1
    iget-object v10, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇OOo8〇0:Lcom/intsig/camscanner/scandone/DonePresenter;

    .line 89
    .line 90
    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;->OOO〇O0(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;)Landroidx/appcompat/widget/AppCompatImageView;

    .line 91
    .line 92
    .line 93
    move-result-object v11

    .line 94
    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;->o〇8(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;)Landroid/view/View;

    .line 95
    .line 96
    .line 97
    move-result-object v12

    .line 98
    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;->OOO〇O0(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;)Landroidx/appcompat/widget/AppCompatImageView;

    .line 99
    .line 100
    .line 101
    move-result-object v13

    .line 102
    const/4 v14, 0x0

    .line 103
    const/4 v15, 0x0

    .line 104
    const-string v16, "CSScanDone"

    .line 105
    .line 106
    invoke-virtual/range {v10 .. v16}, Lcom/intsig/camscanner/scandone/DonePresenter;->o0ooO(Landroidx/appcompat/widget/AppCompatImageView;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;Ljava/lang/String;)V

    .line 107
    .line 108
    .line 109
    iget-object v2, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇OOo8〇0:Lcom/intsig/camscanner/scandone/DonePresenter;

    .line 110
    .line 111
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/scandone/DonePresenter;->o〇0OOo〇0(Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneShareView;)V

    .line 112
    .line 113
    .line 114
    iget-object v2, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇OOo8〇0:Lcom/intsig/camscanner/scandone/DonePresenter;

    .line 115
    .line 116
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/scandone/DonePresenter;->Oo8Oo00oo(Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneFeatureView;)V

    .line 117
    .line 118
    .line 119
    iget-object v2, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇OOo8〇0:Lcom/intsig/camscanner/scandone/DonePresenter;

    .line 120
    .line 121
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/scandone/DonePresenter;->o8(Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneRecommendForCamExam;)V

    .line 122
    .line 123
    .line 124
    iget-object v2, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->OO:Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 125
    .line 126
    if-eqz v2, :cond_2

    .line 127
    .line 128
    iget-object v4, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    .line 129
    .line 130
    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;->oo〇(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;)Landroidx/recyclerview/widget/RecyclerView;

    .line 131
    .line 132
    .line 133
    move-result-object v5

    .line 134
    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;->o〇〇0〇(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;)Lcom/intsig/view/FlowLayout;

    .line 135
    .line 136
    .line 137
    move-result-object v6

    .line 138
    invoke-virtual {v2, v4, v5, v6}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;->o〇O(Landroid/content/Context;Landroidx/recyclerview/widget/RecyclerView;Lcom/intsig/view/FlowLayout;)V

    .line 139
    .line 140
    .line 141
    :cond_2
    iget-boolean v2, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇080OO8〇0:Z

    .line 142
    .line 143
    if-eqz v2, :cond_3

    .line 144
    .line 145
    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;->o0ooO(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;)Landroid/widget/TextView;

    .line 146
    .line 147
    .line 148
    move-result-object v2

    .line 149
    const v4, 0x7f130f84

    .line 150
    .line 151
    .line 152
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    .line 153
    .line 154
    .line 155
    goto :goto_0

    .line 156
    :cond_3
    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;->o0ooO(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;)Landroid/widget/TextView;

    .line 157
    .line 158
    .line 159
    move-result-object v2

    .line 160
    const v4, 0x7f130b4c

    .line 161
    .line 162
    .line 163
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    .line 164
    .line 165
    .line 166
    :goto_0
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/folder/PreferenceFolderHelper;->〇O〇()Z

    .line 167
    .line 168
    .line 169
    move-result v2

    .line 170
    if-eqz v2, :cond_9

    .line 171
    .line 172
    iget-object v2, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->OO:Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 173
    .line 174
    if-eqz v2, :cond_9

    .line 175
    .line 176
    invoke-virtual {v2}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;->o08〇〇0O()Lcom/intsig/camscanner/scandone/ScanDoneModel;

    .line 177
    .line 178
    .line 179
    move-result-object v2

    .line 180
    if-nez v2, :cond_4

    .line 181
    .line 182
    goto/16 :goto_2

    .line 183
    .line 184
    :cond_4
    iget-object v2, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇OOo8〇0:Lcom/intsig/camscanner/scandone/DonePresenter;

    .line 185
    .line 186
    iget-object v4, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->OO:Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 187
    .line 188
    invoke-virtual {v2, v4, v1}, Lcom/intsig/camscanner/scandone/DonePresenter;->〇〇〇0〇〇0(Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneRecommendView;)V

    .line 189
    .line 190
    .line 191
    iget-object v2, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->OO:Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 192
    .line 193
    invoke-virtual {v2}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;->oO8008O()Landroidx/lifecycle/MutableLiveData;

    .line 194
    .line 195
    .line 196
    move-result-object v2

    .line 197
    invoke-virtual {v2}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 198
    .line 199
    .line 200
    move-result-object v2

    .line 201
    check-cast v2, Ljava/lang/String;

    .line 202
    .line 203
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 204
    .line 205
    .line 206
    move-result v4

    .line 207
    if-nez v4, :cond_5

    .line 208
    .line 209
    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;->〇o(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;)Landroid/widget/TextView;

    .line 210
    .line 211
    .line 212
    move-result-object v4

    .line 213
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    .line 215
    .line 216
    :cond_5
    iget-object v2, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->OO:Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 217
    .line 218
    invoke-virtual {v2}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;->〇8O0O808〇()I

    .line 219
    .line 220
    .line 221
    move-result v2

    .line 222
    if-gez v2, :cond_6

    .line 223
    .line 224
    return-void

    .line 225
    :cond_6
    new-instance v2, Ljava/lang/StringBuilder;

    .line 226
    .line 227
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 228
    .line 229
    .line 230
    const-string v4, "scanDone recommend dir type "

    .line 231
    .line 232
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 233
    .line 234
    .line 235
    iget-object v4, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->OO:Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 236
    .line 237
    invoke-virtual {v4}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;->〇8O0O808〇()I

    .line 238
    .line 239
    .line 240
    move-result v4

    .line 241
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 242
    .line 243
    .line 244
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 245
    .line 246
    .line 247
    move-result-object v2

    .line 248
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    .line 250
    .line 251
    iget-object v2, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->OO:Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 252
    .line 253
    invoke-virtual {v2}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;->O8888()Ljava/lang/String;

    .line 254
    .line 255
    .line 256
    move-result-object v2

    .line 257
    iget-object v3, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->OO:Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 258
    .line 259
    invoke-virtual {v3}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;->OO〇()Z

    .line 260
    .line 261
    .line 262
    move-result v3

    .line 263
    if-eqz v3, :cond_7

    .line 264
    .line 265
    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;->〇00(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;)Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 266
    .line 267
    .line 268
    move-result-object v2

    .line 269
    const/16 v3, 0x8

    .line 270
    .line 271
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 272
    .line 273
    .line 274
    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;->O〇8O8〇008(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;)Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 275
    .line 276
    .line 277
    move-result-object v1

    .line 278
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 279
    .line 280
    .line 281
    return-void

    .line 282
    :cond_7
    iget-object v3, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->OO:Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 283
    .line 284
    invoke-virtual {v3}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;->〇8O0O808〇()I

    .line 285
    .line 286
    .line 287
    move-result v3

    .line 288
    const/16 v4, 0x69

    .line 289
    .line 290
    const/4 v5, 0x0

    .line 291
    if-ne v3, v4, :cond_8

    .line 292
    .line 293
    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;->〇00(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;)Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 294
    .line 295
    .line 296
    move-result-object v3

    .line 297
    invoke-virtual {v3, v5}, Landroid/view/View;->setVisibility(I)V

    .line 298
    .line 299
    .line 300
    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;->O8〇o(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;)Landroid/widget/TextView;

    .line 301
    .line 302
    .line 303
    move-result-object v1

    .line 304
    iget-object v3, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    .line 305
    .line 306
    const/4 v4, 0x1

    .line 307
    new-array v4, v4, [Ljava/lang/Object;

    .line 308
    .line 309
    aput-object v2, v4, v5

    .line 310
    .line 311
    const v2, 0x7f131084

    .line 312
    .line 313
    .line 314
    invoke-virtual {v3, v2, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 315
    .line 316
    .line 317
    move-result-object v2

    .line 318
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 319
    .line 320
    .line 321
    goto :goto_1

    .line 322
    :cond_8
    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;->O〇8O8〇008(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;)Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 323
    .line 324
    .line 325
    move-result-object v2

    .line 326
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 327
    .line 328
    .line 329
    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;->〇00〇8(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;)Landroid/widget/TextView;

    .line 330
    .line 331
    .line 332
    move-result-object v1

    .line 333
    iget-object v2, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->OO:Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 334
    .line 335
    invoke-virtual {v2}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;->〇〇00O〇0o()I

    .line 336
    .line 337
    .line 338
    move-result v2

    .line 339
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 340
    .line 341
    .line 342
    :goto_1
    iget-object v1, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->OO:Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 343
    .line 344
    invoke-virtual {v1}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;->〇8O0O808〇()I

    .line 345
    .line 346
    .line 347
    move-result v1

    .line 348
    sget-object v2, Lcom/intsig/camscanner/scenariodir/logagent/ScenarioLogDirAgent;->〇080:Lcom/intsig/camscanner/scenariodir/logagent/ScenarioLogDirAgent;

    .line 349
    .line 350
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/scenariodir/logagent/ScenarioLogDirAgent;->O8(I)V

    .line 351
    .line 352
    .line 353
    :cond_9
    :goto_2
    return-void
    .line 354
.end method

.method static bridge synthetic 〇00(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇0000OOO(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V
    .locals 3
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "bindViewMonthItem"

    .line 2
    .line 3
    const-string v1, "ScanDoneOperationsAdapter"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    instance-of v0, p1, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;

    .line 9
    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    new-instance v0, Ljava/lang/StringBuilder;

    .line 13
    .line 14
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 15
    .line 16
    .line 17
    const-string v2, "bindViewMonthItem but get a "

    .line 18
    .line 19
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    return-void

    .line 33
    :cond_0
    check-cast p1, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;

    .line 34
    .line 35
    iget v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->oOo〇8o008:I

    .line 36
    .line 37
    if-nez v0, :cond_1

    .line 38
    .line 39
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 40
    .line 41
    const/16 v0, 0x8

    .line 42
    .line 43
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 44
    .line 45
    .line 46
    return-void

    .line 47
    :cond_1
    const/4 v1, 0x1

    .line 48
    if-ne v0, v1, :cond_2

    .line 49
    .line 50
    iget-object v0, p1, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;->o0:Lcom/intsig/camscanner/databinding/LayoutScanDoneVipMonthCard1Binding;

    .line 51
    .line 52
    if-eqz v0, :cond_3

    .line 53
    .line 54
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->Ooo(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    .line 55
    .line 56
    .line 57
    iget-object v0, p1, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;->o0:Lcom/intsig/camscanner/databinding/LayoutScanDoneVipMonthCard1Binding;

    .line 58
    .line 59
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutScanDoneVipMonthCard1Binding;->〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 60
    .line 61
    invoke-static {}, Lcom/intsig/camscanner/purchase/scandone/vip_month/ScanDoneVipMonthManager;->〇O〇()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v1

    .line 65
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    .line 67
    .line 68
    iget-object v0, p1, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;->o0:Lcom/intsig/camscanner/databinding/LayoutScanDoneVipMonthCard1Binding;

    .line 69
    .line 70
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutScanDoneVipMonthCard1Binding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 71
    .line 72
    invoke-static {}, Lcom/intsig/camscanner/purchase/scandone/vip_month/ScanDoneVipMonthManager;->Oooo8o0〇()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v1

    .line 76
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    .line 78
    .line 79
    iget-object v0, p1, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;->o0:Lcom/intsig/camscanner/databinding/LayoutScanDoneVipMonthCard1Binding;

    .line 80
    .line 81
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutScanDoneVipMonthCard1Binding;->oOo〇8o008:Landroidx/appcompat/widget/AppCompatTextView;

    .line 82
    .line 83
    new-instance v1, Loo8〇〇/ooo〇8oO;

    .line 84
    .line 85
    invoke-direct {v1, p0}, Loo8〇〇/ooo〇8oO;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;)V

    .line 86
    .line 87
    .line 88
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    .line 90
    .line 91
    iget-object v0, p1, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;->o0:Lcom/intsig/camscanner/databinding/LayoutScanDoneVipMonthCard1Binding;

    .line 92
    .line 93
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutScanDoneVipMonthCard1Binding;->〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 94
    .line 95
    new-instance v1, Loo8〇〇/O0o〇〇Oo;

    .line 96
    .line 97
    invoke-direct {v1, p0}, Loo8〇〇/O0o〇〇Oo;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;)V

    .line 98
    .line 99
    .line 100
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    .line 102
    .line 103
    goto :goto_0

    .line 104
    :cond_2
    const/4 v1, 0x2

    .line 105
    if-ne v0, v1, :cond_3

    .line 106
    .line 107
    invoke-static {p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;->〇00(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;)Landroid/widget/ImageView;

    .line 108
    .line 109
    .line 110
    move-result-object v0

    .line 111
    if-eqz v0, :cond_3

    .line 112
    .line 113
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    .line 114
    .line 115
    invoke-static {v0}, Lcom/bumptech/glide/Glide;->OoO8(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    .line 116
    .line 117
    .line 118
    move-result-object v0

    .line 119
    invoke-static {}, Lcom/intsig/camscanner/purchase/scandone/vip_month/ScanDoneVipMonthManager;->〇〇808〇()Ljava/lang/String;

    .line 120
    .line 121
    .line 122
    move-result-object v1

    .line 123
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/RequestManager;->〇〇808〇(Ljava/lang/String;)Lcom/bumptech/glide/RequestBuilder;

    .line 124
    .line 125
    .line 126
    move-result-object v0

    .line 127
    invoke-static {p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;->〇00(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;)Landroid/widget/ImageView;

    .line 128
    .line 129
    .line 130
    move-result-object v1

    .line 131
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/RequestBuilder;->Oo〇o(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/ViewTarget;

    .line 132
    .line 133
    .line 134
    invoke-static {p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;->〇00(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;)Landroid/widget/ImageView;

    .line 135
    .line 136
    .line 137
    move-result-object v0

    .line 138
    new-instance v1, Loo8〇〇/OO8oO0o〇;

    .line 139
    .line 140
    invoke-direct {v1, p0}, Loo8〇〇/OO8oO0o〇;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;)V

    .line 141
    .line 142
    .line 143
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    .line 145
    .line 146
    :cond_3
    :goto_0
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 147
    .line 148
    const/4 v0, 0x0

    .line 149
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 150
    .line 151
    .line 152
    return-void
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private 〇00〇8(Lcom/intsig/advertisement/interfaces/RealRequestAbs;Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;)V
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->Oo8Oo00oo(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-static {p2}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;->O8ooOoo〇(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;)Landroid/widget/TextView;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    const/16 v1, 0x8

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 14
    .line 15
    .line 16
    invoke-static {p2}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;->〇oOO8O8(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;)Landroid/widget/ImageView;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 21
    .line 22
    .line 23
    invoke-static {p2}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;->〇oOO8O8(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;)Landroid/widget/ImageView;

    .line 24
    .line 25
    .line 26
    move-result-object p2

    .line 27
    invoke-virtual {p1, p2}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->setTag(Ljava/lang/Object;)V

    .line 28
    .line 29
    .line 30
    new-instance p2, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$1;

    .line 31
    .line 32
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$1;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p1, p2}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->addOnAdShowListener(Lcom/intsig/advertisement/listener/OnAdShowListener;)V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    invoke-static {p2}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;->〇oOO8O8(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;)Landroid/widget/ImageView;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    const/4 p2, 0x0

    .line 44
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 45
    .line 46
    .line 47
    :goto_0
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private synthetic 〇08O8o〇0(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o〇8oOO88()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇0〇O0088o(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->O〇O〇oO(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic 〇8(Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;Landroid/view/View;)V
    .locals 3

    .line 1
    iget-object p3, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇08O〇00〇o:Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 2
    .line 3
    invoke-virtual {p3}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyOnClick()V

    .line 4
    .line 5
    .line 6
    iget-object p3, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇OOo8〇0:Lcom/intsig/camscanner/scandone/DonePresenter;

    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getUrl()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {p3, v0}, Lcom/intsig/camscanner/scandone/DonePresenter;->〇08O8o〇0(Ljava/lang/String;)Z

    .line 13
    .line 14
    .line 15
    move-result p3

    .line 16
    if-eqz p3, :cond_0

    .line 17
    .line 18
    iget-object p3, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    .line 19
    .line 20
    instance-of v0, p3, Landroid/app/Activity;

    .line 21
    .line 22
    if-eqz v0, :cond_0

    .line 23
    .line 24
    iget-object p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇OOo8〇0:Lcom/intsig/camscanner/scandone/DonePresenter;

    .line 25
    .line 26
    check-cast p3, Landroid/app/Activity;

    .line 27
    .line 28
    invoke-virtual {p1, p3}, Lcom/intsig/camscanner/scandone/DonePresenter;->o0O0(Landroid/app/Activity;)V

    .line 29
    .line 30
    .line 31
    return-void

    .line 32
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getUrl()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object p3

    .line 36
    invoke-static {p3}, Lcom/intsig/camscanner/web/UrlAnalyzeUtil;->〇080(Ljava/lang/String;)Lcom/intsig/camscanner/web/UrlEntity;

    .line 37
    .line 38
    .line 39
    move-result-object p3

    .line 40
    invoke-virtual {p3}, Lcom/intsig/camscanner/web/UrlEntity;->〇〇888()Ljava/util/HashMap;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    .line 45
    .line 46
    .line 47
    move-result v0

    .line 48
    if-nez v0, :cond_1

    .line 49
    .line 50
    invoke-virtual {p3}, Lcom/intsig/camscanner/web/UrlEntity;->〇〇888()Ljava/util/HashMap;

    .line 51
    .line 52
    .line 53
    move-result-object p3

    .line 54
    sget-object v0, Lcom/intsig/camscanner/web/PARAMATER_KEY;->subMode:Lcom/intsig/camscanner/web/PARAMATER_KEY;

    .line 55
    .line 56
    invoke-virtual {p3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    .line 58
    .line 59
    move-result-object p3

    .line 60
    check-cast p3, Ljava/lang/String;

    .line 61
    .line 62
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 63
    .line 64
    .line 65
    move-result v0

    .line 66
    if-nez v0, :cond_1

    .line 67
    .line 68
    sget-object v0, Lcom/intsig/camscanner/web/PARAMATER_VALUE;->idCard:Lcom/intsig/camscanner/web/PARAMATER_VALUE;

    .line 69
    .line 70
    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    invoke-virtual {p3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 75
    .line 76
    .line 77
    move-result p3

    .line 78
    if-eqz p3, :cond_1

    .line 79
    .line 80
    iget-object p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇OOo8〇0:Lcom/intsig/camscanner/scandone/DonePresenter;

    .line 81
    .line 82
    invoke-virtual {p1}, Lcom/intsig/camscanner/scandone/DonePresenter;->〇0()V

    .line 83
    .line 84
    .line 85
    return-void

    .line 86
    :cond_1
    new-instance p3, Lcom/intsig/advertisement/bean/AdClickInfo;

    .line 87
    .line 88
    sget-object v0, Lcom/intsig/advertisement/enums/PositionType;->ScanDoneTop:Lcom/intsig/advertisement/enums/PositionType;

    .line 89
    .line 90
    sget-object v1, Lcom/intsig/advertisement/enums/SourceType;->CS:Lcom/intsig/advertisement/enums/SourceType;

    .line 91
    .line 92
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getId()Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object v2

    .line 96
    invoke-direct {p3, v0, v1, v2}, Lcom/intsig/advertisement/bean/AdClickInfo;-><init>(Lcom/intsig/advertisement/enums/PositionType;Lcom/intsig/advertisement/enums/SourceType;Ljava/lang/String;)V

    .line 97
    .line 98
    .line 99
    sput-object p3, Lcom/intsig/advertisement/control/AdConfigManager;->〇O8o08O:Lcom/intsig/advertisement/bean/AdClickInfo;

    .line 100
    .line 101
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getDptrackers()Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/DpLinkTrackers;

    .line 102
    .line 103
    .line 104
    move-result-object v0

    .line 105
    invoke-virtual {p3, v0}, Lcom/intsig/advertisement/bean/AdClickInfo;->oO80(Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/DpLinkTrackers;)V

    .line 106
    .line 107
    .line 108
    sget-object p3, Lcom/intsig/advertisement/control/AdConfigManager;->〇O8o08O:Lcom/intsig/advertisement/bean/AdClickInfo;

    .line 109
    .line 110
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getMacro()Ljava/util/HashMap;

    .line 111
    .line 112
    .line 113
    move-result-object p1

    .line 114
    invoke-virtual {p3, p1}, Lcom/intsig/advertisement/bean/AdClickInfo;->〇〇888(Ljava/util/HashMap;)V

    .line 115
    .line 116
    .line 117
    invoke-virtual {p2}, Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;->〇〇888()V

    .line 118
    .line 119
    .line 120
    return-void
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private synthetic 〇8〇0〇o〇O(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇O00(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;Landroid/widget/LinearLayout;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->O08000(Landroid/widget/LinearLayout;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇O888o0o(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇8(Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic 〇O〇(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o〇0OOo〇0(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇o(Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneAdView;)V
    .locals 6
    .param p1    # Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneAdView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-interface {p1}, Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneAdView;->〇80〇808〇O()Landroid/widget/LinearLayout;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const-string p1, "ScanDoneOperationsAdapter"

    .line 8
    .line 9
    const-string v0, "initTopAd get error! no topAdContainer exists"

    .line 10
    .line 11
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇08O〇00〇o:Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 16
    .line 17
    const/4 v2, 0x0

    .line 18
    const/4 v3, 0x1

    .line 19
    if-eqz v1, :cond_6

    .line 20
    .line 21
    invoke-virtual {v1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getData()Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    instance-of v1, v1, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;

    .line 26
    .line 27
    if-eqz v1, :cond_6

    .line 28
    .line 29
    iget-object v1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇08O〇00〇o:Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 30
    .line 31
    invoke-virtual {v1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getData()Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    check-cast v1, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;

    .line 36
    .line 37
    new-array v4, v3, [Landroid/view/View;

    .line 38
    .line 39
    aput-object v0, v4, v2

    .line 40
    .line 41
    invoke-static {v2, v4}, Lcom/intsig/utils/CustomViewUtils;->〇o〇(I[Landroid/view/View;)V

    .line 42
    .line 43
    .line 44
    instance-of v2, v1, Lcom/intsig/advertisement/adapters/sources/cs/ScanDoneTopIntervalBean;

    .line 45
    .line 46
    if-eqz v2, :cond_1

    .line 47
    .line 48
    check-cast v1, Lcom/intsig/advertisement/adapters/sources/cs/ScanDoneTopIntervalBean;

    .line 49
    .line 50
    invoke-direct {p0, p1, v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇oOO8O8(Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneAdView;Lcom/intsig/advertisement/adapters/sources/cs/ScanDoneTopIntervalBean;)V

    .line 51
    .line 52
    .line 53
    goto/16 :goto_1

    .line 54
    .line 55
    :cond_1
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->O8〇o(Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;)Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    invoke-interface {p1}, Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneAdView;->〇0〇O0088o()Landroid/widget/TextView;

    .line 60
    .line 61
    .line 62
    move-result-object v4

    .line 63
    if-eqz v4, :cond_3

    .line 64
    .line 65
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getShow_icon()I

    .line 66
    .line 67
    .line 68
    move-result v5

    .line 69
    if-ne v5, v3, :cond_2

    .line 70
    .line 71
    new-instance v5, Loo8〇〇/〇80;

    .line 72
    .line 73
    invoke-direct {v5, p0, v4, v1}, Loo8〇〇/〇80;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;Landroid/widget/TextView;Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;)V

    .line 74
    .line 75
    .line 76
    invoke-virtual {v4, v5}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 77
    .line 78
    .line 79
    goto :goto_0

    .line 80
    :cond_2
    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    .line 81
    .line 82
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 83
    .line 84
    .line 85
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getTitle()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v5

    .line 89
    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    .line 91
    .line 92
    :cond_3
    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 93
    .line 94
    .line 95
    new-instance v4, Loo8〇〇/Ooo;

    .line 96
    .line 97
    invoke-direct {v4, p0, v1, v2}, Loo8〇〇/Ooo;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;)V

    .line 98
    .line 99
    .line 100
    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    .line 102
    .line 103
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getShow_close()I

    .line 104
    .line 105
    .line 106
    move-result v4

    .line 107
    if-ne v4, v3, :cond_4

    .line 108
    .line 109
    iget-object v3, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    .line 110
    .line 111
    const/4 v4, 0x2

    .line 112
    invoke-static {v3, v4}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 113
    .line 114
    .line 115
    move-result v3

    .line 116
    invoke-interface {p1}, Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneAdView;->o〇O8〇〇o()Landroidx/appcompat/widget/AppCompatImageView;

    .line 117
    .line 118
    .line 119
    move-result-object v4

    .line 120
    if-eqz v4, :cond_4

    .line 121
    .line 122
    invoke-virtual {v4, v3, v3, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 123
    .line 124
    .line 125
    iget-object v3, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    .line 126
    .line 127
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 128
    .line 129
    .line 130
    move-result-object v3

    .line 131
    const v5, 0x7f08063f

    .line 132
    .line 133
    .line 134
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    .line 135
    .line 136
    .line 137
    move-result-object v3

    .line 138
    invoke-virtual {v4, v3}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 139
    .line 140
    .line 141
    new-instance v3, Loo8〇〇/〇O〇80o08O;

    .line 142
    .line 143
    invoke-direct {v3, p0, v0}, Loo8〇〇/〇O〇80o08O;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;Landroid/widget/LinearLayout;)V

    .line 144
    .line 145
    .line 146
    invoke-virtual {v4, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    .line 148
    .line 149
    :cond_4
    invoke-interface {p1}, Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneAdView;->o800o8O()Landroidx/appcompat/widget/AppCompatImageView;

    .line 150
    .line 151
    .line 152
    move-result-object v0

    .line 153
    if-eqz v0, :cond_5

    .line 154
    .line 155
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    .line 156
    .line 157
    invoke-static {v0}, Lcom/bumptech/glide/Glide;->OoO8(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    .line 158
    .line 159
    .line 160
    move-result-object v0

    .line 161
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getPic()Ljava/lang/String;

    .line 162
    .line 163
    .line 164
    move-result-object v1

    .line 165
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/RequestManager;->〇〇808〇(Ljava/lang/String;)Lcom/bumptech/glide/RequestBuilder;

    .line 166
    .line 167
    .line 168
    move-result-object v0

    .line 169
    invoke-interface {p1}, Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneAdView;->o800o8O()Landroidx/appcompat/widget/AppCompatImageView;

    .line 170
    .line 171
    .line 172
    move-result-object p1

    .line 173
    invoke-virtual {v0, p1}, Lcom/bumptech/glide/RequestBuilder;->Oo〇o(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/ViewTarget;

    .line 174
    .line 175
    .line 176
    :cond_5
    invoke-virtual {v2}, Lcom/intsig/advertisement/adapters/sources/api/sdk/AdEventHandler;->〇80〇808〇O()V

    .line 177
    .line 178
    .line 179
    iget-object p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇08O〇00〇o:Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 180
    .line 181
    invoke-virtual {p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyOnShowSucceed()V

    .line 182
    .line 183
    .line 184
    goto :goto_1

    .line 185
    :cond_6
    new-array p1, v3, [Landroid/view/View;

    .line 186
    .line 187
    aput-object v0, p1, v2

    .line 188
    .line 189
    const/16 v0, 0x8

    .line 190
    .line 191
    invoke-static {v0, p1}, Lcom/intsig/utils/CustomViewUtils;->〇o〇(I[Landroid/view/View;)V

    .line 192
    .line 193
    .line 194
    :goto_1
    return-void
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private 〇oOO8O8(Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneAdView;Lcom/intsig/advertisement/adapters/sources/cs/ScanDoneTopIntervalBean;)V
    .locals 6
    .param p1    # Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneAdView;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-virtual {p2}, Lcom/intsig/advertisement/adapters/sources/cs/ScanDoneTopIntervalBean;->getHasReportShow()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const-string v1, "CSScanDone"

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    invoke-virtual {p2, v0}, Lcom/intsig/advertisement/adapters/sources/cs/ScanDoneTopIntervalBean;->setHasReportShow(Z)V

    .line 11
    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/camscanner/ads/csAd/CsAdUtil;->〇O888o0o()Lcom/intsig/advertisement/record/operation/AdIdRecord;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-static {v0}, Lcom/intsig/camscanner/ads/csAd/CsAdUtil;->〇00〇8(Lcom/intsig/advertisement/record/operation/AdIdRecord;)V

    .line 18
    .line 19
    .line 20
    const-string v0, "point_task_pop_show"

    .line 21
    .line 22
    invoke-static {v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    :cond_0
    invoke-interface {p1}, Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneAdView;->o800o8O()Landroidx/appcompat/widget/AppCompatImageView;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-virtual {p2}, Lcom/intsig/advertisement/adapters/sources/cs/ScanDoneTopIntervalBean;->getIconId()I

    .line 30
    .line 31
    .line 32
    move-result v2

    .line 33
    invoke-virtual {v0, v2}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 34
    .line 35
    .line 36
    invoke-virtual {p2}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getTitle()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    invoke-virtual {p2}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getDescription()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v2

    .line 48
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 49
    .line 50
    .line 51
    move-result v2

    .line 52
    add-int/2addr v2, v0

    .line 53
    new-instance v3, Landroid/text/SpannableString;

    .line 54
    .line 55
    new-instance v4, Ljava/lang/StringBuilder;

    .line 56
    .line 57
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p2}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getTitle()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v5

    .line 64
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {p2}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getDescription()Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object v5

    .line 71
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object v4

    .line 78
    invoke-direct {v3, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 79
    .line 80
    .line 81
    new-instance v4, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$2;

    .line 82
    .line 83
    invoke-direct {v4, p0, v1, p2}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$2;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;Ljava/lang/String;Lcom/intsig/advertisement/adapters/sources/cs/ScanDoneTopIntervalBean;)V

    .line 84
    .line 85
    .line 86
    const/16 p2, 0x21

    .line 87
    .line 88
    invoke-virtual {v3, v4, v0, v2, p2}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 89
    .line 90
    .line 91
    invoke-interface {p1}, Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneAdView;->〇0〇O0088o()Landroid/widget/TextView;

    .line 92
    .line 93
    .line 94
    move-result-object p2

    .line 95
    invoke-virtual {p2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    .line 97
    .line 98
    invoke-interface {p1}, Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneAdView;->〇0〇O0088o()Landroid/widget/TextView;

    .line 99
    .line 100
    .line 101
    move-result-object p2

    .line 102
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    .line 103
    .line 104
    .line 105
    move-result-object v0

    .line 106
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 107
    .line 108
    .line 109
    invoke-interface {p1}, Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneAdView;->o〇O8〇〇o()Landroidx/appcompat/widget/AppCompatImageView;

    .line 110
    .line 111
    .line 112
    move-result-object p2

    .line 113
    if-eqz p2, :cond_1

    .line 114
    .line 115
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    .line 116
    .line 117
    const/4 v2, 0x2

    .line 118
    invoke-static {v0, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    iget-object v2, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    .line 123
    .line 124
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 125
    .line 126
    .line 127
    move-result-object v2

    .line 128
    const v3, 0x7f06009a

    .line 129
    .line 130
    .line 131
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    .line 132
    .line 133
    .line 134
    move-result v2

    .line 135
    invoke-static {v2}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    .line 136
    .line 137
    .line 138
    move-result-object v2

    .line 139
    invoke-virtual {p2, v2}, Landroid/widget/ImageView;->setImageTintList(Landroid/content/res/ColorStateList;)V

    .line 140
    .line 141
    .line 142
    invoke-virtual {p2, v0, v0, v0, v0}, Landroid/view/View;->setPadding(IIII)V

    .line 143
    .line 144
    .line 145
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    .line 146
    .line 147
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 148
    .line 149
    .line 150
    move-result-object v0

    .line 151
    const v2, 0x7f08063f

    .line 152
    .line 153
    .line 154
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    .line 155
    .line 156
    .line 157
    move-result-object v0

    .line 158
    invoke-virtual {p2, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 159
    .line 160
    .line 161
    new-instance v0, Loo8〇〇/o0O0;

    .line 162
    .line 163
    invoke-direct {v0, v1, p1}, Loo8〇〇/o0O0;-><init>(Ljava/lang/String;Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneAdView;)V

    .line 164
    .line 165
    .line 166
    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    .line 168
    .line 169
    :cond_1
    return-void
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method public static synthetic 〇oo〇(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇08O8o〇0(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic 〇〇0o(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o〇8oOO88()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇〇8O0〇8(Ljava/lang/String;Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneAdView;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇〇〇0〇〇0(Ljava/lang/String;Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneAdView;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static synthetic 〇〇〇0〇〇0(Ljava/lang/String;Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneAdView;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p2, "point_task_pop_close"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 p0, 0x1

    .line 7
    new-array p0, p0, [Landroid/view/View;

    .line 8
    .line 9
    const/4 p2, 0x0

    .line 10
    invoke-interface {p1}, Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneAdView;->〇80〇808〇O()Landroid/widget/LinearLayout;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    aput-object p1, p0, p2

    .line 15
    .line 16
    const/16 p1, 0x8

    .line 17
    .line 18
    invoke-static {p1, p0}, Lcom/intsig/utils/CustomViewUtils;->〇o〇(I[Landroid/view/View;)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public O000(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇08O〇00〇o:Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 2
    .line 3
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public O8ooOoo〇(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 7
    .line 8
    new-instance v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$4;

    .line 9
    .line 10
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$4;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;)V

    .line 11
    .line 12
    .line 13
    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
.end method

.method public getItemCount()I
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->oOo〇8o008:I

    .line 2
    .line 3
    if-lez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x2

    .line 6
    return v0

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇0O:Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    if-nez v0, :cond_1

    .line 11
    .line 12
    const/4 v0, 0x0

    .line 13
    goto :goto_0

    .line 14
    :cond_1
    const/4 v0, 0x1

    .line 15
    :goto_0
    iget-object v2, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 16
    .line 17
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    add-int/2addr v2, v1

    .line 22
    add-int/2addr v2, v0

    .line 23
    return v2
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public getItemViewType(I)I
    .locals 2

    .line 1
    if-nez p1, :cond_1

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O〇o0()Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    const p1, 0x7f0d04a7

    .line 10
    .line 11
    .line 12
    return p1

    .line 13
    :cond_0
    const p1, 0x7f0d04a5

    .line 14
    .line 15
    .line 16
    return p1

    .line 17
    :cond_1
    const/4 v0, 0x1

    .line 18
    if-ne p1, v0, :cond_2

    .line 19
    .line 20
    iget v1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->oOo〇8o008:I

    .line 21
    .line 22
    if-lez v1, :cond_2

    .line 23
    .line 24
    const p1, 0x7f0d049e

    .line 25
    .line 26
    .line 27
    return p1

    .line 28
    :cond_2
    if-ne p1, v0, :cond_3

    .line 29
    .line 30
    iget-object p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇0O:Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;

    .line 31
    .line 32
    if-eqz p1, :cond_3

    .line 33
    .line 34
    const p1, 0x7f0d049f

    .line 35
    .line 36
    .line 37
    return p1

    .line 38
    :cond_3
    const p1, 0x7f0d04a4

    .line 39
    .line 40
    .line 41
    return p1
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public o0ooO(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->oOo〇8o008:I

    .line 2
    .line 3
    if-lez p1, :cond_0

    .line 4
    .line 5
    iget-object p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o〇00O:Ljava/util/ArrayList;

    .line 6
    .line 7
    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    .line 8
    .line 9
    .line 10
    :cond_0
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 17
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    .line 1
    invoke-virtual/range {p1 .. p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v2

    const v3, 0x7f0d04a7

    const-string v4, "ScanDoneOperationsAdapter"

    if-ne v2, v3, :cond_0

    const-string v2, "onBindViewHolder header use new version from 650"

    .line 2
    invoke-static {v4, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 3
    invoke-direct/range {p0 .. p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o〇〇0〇(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    return-void

    .line 4
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v2

    const v3, 0x7f0d049f

    if-ne v2, v3, :cond_1

    const-string v2, "onBindViewHolder vip task"

    .line 5
    invoke-static {v4, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    invoke-direct/range {p0 .. p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->OOO〇O0(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    return-void

    .line 7
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->getItemViewType()I

    move-result v2

    const/16 v3, 0x8

    const/4 v5, 0x0

    sparse-switch v2, :sswitch_data_0

    goto/16 :goto_2

    :sswitch_0
    const-string v2, "onBindViewHolder header"

    .line 8
    invoke-static {v4, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    instance-of v2, v1, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;

    if-eqz v2, :cond_9

    .line 10
    check-cast v1, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;

    .line 11
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇o(Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneAdView;)V

    .line 12
    iget-boolean v2, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->O8o08O8O:Z

    if-eqz v2, :cond_2

    .line 13
    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->O〇8O8〇008(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;)Landroid/widget/LinearLayout;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 14
    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->〇00(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 15
    iget-object v6, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇OOo8〇0:Lcom/intsig/camscanner/scandone/DonePresenter;

    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->O8ooOoo〇(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;)Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object v7

    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->oo〇(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;)Landroidx/appcompat/widget/AppCompatTextView;

    move-result-object v8

    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->O8〇o(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;)Landroidx/appcompat/widget/AppCompatTextView;

    move-result-object v9

    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->〇o(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;)Landroid/view/View;

    move-result-object v10

    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->〇00〇8(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;)Landroid/view/View;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual/range {v6 .. v12}, Lcom/intsig/camscanner/scandone/DonePresenter;->o0ooO(Landroidx/appcompat/widget/AppCompatImageView;Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;Ljava/lang/String;)V

    .line 16
    iget-object v2, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇OOo8〇0:Lcom/intsig/camscanner/scandone/DonePresenter;

    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/scandone/DonePresenter;->o〇0OOo〇0(Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneShareView;)V

    .line 17
    iget-object v2, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇OOo8〇0:Lcom/intsig/camscanner/scandone/DonePresenter;

    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/scandone/DonePresenter;->Oo8Oo00oo(Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneFeatureView;)V

    goto/16 :goto_2

    .line 18
    :cond_2
    iget-object v2, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇OOo8〇0:Lcom/intsig/camscanner/scandone/DonePresenter;

    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->O8ooOoo〇(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;)Landroidx/appcompat/widget/AppCompatImageView;

    move-result-object v3

    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->〇oOO8O8(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;)Landroid/widget/TextView;

    move-result-object v4

    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->〇0000OOO(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/intsig/camscanner/scandone/DonePresenter;->〇00〇8(Landroidx/appcompat/widget/AppCompatImageView;Landroid/widget/TextView;Landroid/widget/TextView;)V

    .line 19
    iget-object v2, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇OOo8〇0:Lcom/intsig/camscanner/scandone/DonePresenter;

    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->OOO〇O0(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;)Landroidx/recyclerview/widget/RecyclerView;

    move-result-object v3

    invoke-static {v1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->o〇〇0〇(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;)Lcom/intsig/camscanner/view/HorizontalProgressView;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lcom/intsig/camscanner/scandone/DonePresenter;->〇〇00OO(Landroidx/recyclerview/widget/RecyclerView;Lcom/intsig/camscanner/view/HorizontalProgressView;)V

    goto/16 :goto_2

    .line 20
    :sswitch_1
    move-object v2, v1

    check-cast v2, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;

    .line 21
    iget-object v6, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇0O:Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;

    const/4 v7, 0x1

    if-nez v6, :cond_3

    const/4 v6, 0x0

    goto :goto_0

    :cond_3
    const/4 v6, 0x1

    :goto_0
    add-int/lit8 v8, p2, -0x1

    sub-int/2addr v8, v6

    .line 22
    iget-object v6, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o〇00O:Ljava/util/ArrayList;

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 23
    invoke-static {v2}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;->O〇8O8〇008(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;)Landroid/widget/TextView;

    move-result-object v9

    invoke-static {v9, v6}, Lcom/intsig/advertisement/view/AdPolicyWrapper;->〇o〇(Landroid/widget/TextView;Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V

    .line 24
    invoke-direct {v0, v6}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o8(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)Z

    move-result v9

    if-nez v9, :cond_4

    invoke-direct {v0, v6}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o〇8(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 25
    :cond_4
    invoke-static {v2}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;->O8ooOoo〇(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;)Landroid/widget/TextView;

    move-result-object v9

    invoke-virtual {v9, v3}, Landroid/view/View;->setVisibility(I)V

    .line 26
    :cond_5
    invoke-direct {v0, v6, v2}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇00〇8(Lcom/intsig/advertisement/interfaces/RealRequestAbs;Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;)V

    .line 27
    iget-object v1, v1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    instance-of v1, v1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_8

    .line 28
    invoke-static {v2}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;->〇00(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;)Landroid/widget/RelativeLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 29
    iget-object v1, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    const/16 v9, 0xa

    invoke-static {v1, v9}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    move-result v1

    .line 30
    iget-object v9, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o〇00O:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    sub-int/2addr v9, v7

    if-ne v8, v9, :cond_6

    .line 31
    invoke-static {v2}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;->〇0000OOO(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_1

    .line 32
    :cond_6
    invoke-static {v2}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;->〇0000OOO(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7, v1, v1, v1, v5}, Landroid/view/View;->setPadding(IIII)V

    .line 33
    :goto_1
    invoke-static {}, Lcom/intsig/advertisement/adapters/positions/ScanDoneManager;->o8oO〇()Lcom/intsig/advertisement/adapters/positions/ScanDoneManager;

    move-result-object v9

    iget-object v10, v0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    invoke-static {v2}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;->〇00(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;)Landroid/widget/RelativeLayout;

    move-result-object v11

    const/4 v12, -0x1

    const/4 v13, -0x2

    invoke-virtual {v6}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    move-result-object v1

    invoke-virtual {v1}, Lcom/intsig/advertisement/params/RequestParam;->oO80()I

    move-result v14

    invoke-static {v2}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;->o〇〇0〇(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;)Landroid/widget/TextView;

    move-result-object v15

    new-instance v1, Loo8〇〇/oO00OOO;

    invoke-direct {v1, v0}, Loo8〇〇/oO00OOO;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;)V

    move-object/from16 v16, v1

    invoke-virtual/range {v9 .. v16}, Lcom/intsig/advertisement/adapters/positions/ScanDoneManager;->O〇O〇oO(Landroid/content/Context;Landroid/view/ViewGroup;IIILandroid/widget/TextView;Lcom/intsig/advertisement/listener/OnFeedBackListener;)Z

    move-result v1

    .line 34
    invoke-static {v2}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;->〇oOO8O8(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;)Landroid/widget/ImageView;

    move-result-object v5

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 35
    invoke-static {v2}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;->〇oOO8O8(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;)Landroid/widget/ImageView;

    move-result-object v5

    new-instance v6, Loo8〇〇/O000;

    invoke-direct {v6, v0}, Loo8〇〇/O000;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    invoke-static {}, Lcom/intsig/advertisement/adapters/positions/ScanDoneManager;->o8oO〇()Lcom/intsig/advertisement/adapters/positions/ScanDoneManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/intsig/advertisement/adapters/positions/ScanDoneManager;->o〇O()Z

    move-result v5

    if-nez v5, :cond_7

    .line 37
    invoke-static {v2}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;->〇oOO8O8(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_7
    if-nez v1, :cond_9

    const-string v1, "holder.itemView bind fail"

    .line 38
    invoke-static {v4, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_8
    const-string v1, "holder.itemView is not  ViewGroup"

    .line 39
    invoke-static {v4, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 40
    :sswitch_2
    invoke-direct/range {p0 .. p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇0000OOO(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    :cond_9
    :goto_2
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0d049e -> :sswitch_2
        0x7f0d04a4 -> :sswitch_1
        0x7f0d04a5 -> :sswitch_0
    .end sparse-switch
.end method

.method public onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;ILjava/util/List;)V
    .locals 1
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
            "I",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 41
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    .line 42
    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p3

    .line 43
    instance-of v0, p3, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 44
    check-cast p3, Ljava/lang/String;

    goto :goto_0

    :cond_0
    const-string p3, ""

    .line 45
    :goto_0
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 46
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V

    return-void

    :cond_1
    const-string v0, "PAYLOAD_UPDATE_VIP_MONTH_CARD_RED_ENVELOPE"

    .line 47
    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_2

    .line 48
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->Ooo(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    goto :goto_1

    .line 49
    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V

    :goto_1
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0:Landroid/content/Context;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    invoke-virtual {v0, p2, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    const v0, 0x7f0d04a5

    .line 13
    .line 14
    .line 15
    if-ne p2, v0, :cond_0

    .line 16
    .line 17
    new-instance p2, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;

    .line 18
    .line 19
    invoke-direct {p2, p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;-><init>(Landroid/view/View;)V

    .line 20
    .line 21
    .line 22
    return-object p2

    .line 23
    :cond_0
    const v0, 0x7f0d04a7

    .line 24
    .line 25
    .line 26
    if-ne p2, v0, :cond_1

    .line 27
    .line 28
    new-instance p2, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;

    .line 29
    .line 30
    invoke-direct {p2, p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneNewHeadViewHolder;-><init>(Landroid/view/View;)V

    .line 31
    .line 32
    .line 33
    return-object p2

    .line 34
    :cond_1
    const v0, 0x7f0d049f

    .line 35
    .line 36
    .line 37
    if-ne p2, v0, :cond_2

    .line 38
    .line 39
    new-instance p2, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;

    .line 40
    .line 41
    invoke-direct {p2, p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipTaskViewHolder;-><init>(Landroid/view/View;)V

    .line 42
    .line 43
    .line 44
    return-object p2

    .line 45
    :cond_2
    const v0, 0x7f0d049e

    .line 46
    .line 47
    .line 48
    if-ne p2, v0, :cond_3

    .line 49
    .line 50
    new-instance p2, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;

    .line 51
    .line 52
    iget v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->oOo〇8o008:I

    .line 53
    .line 54
    invoke-direct {p2, p1, v0}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;-><init>(Landroid/view/View;I)V

    .line 55
    .line 56
    .line 57
    return-object p2

    .line 58
    :cond_3
    new-instance p2, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;

    .line 59
    .line 60
    invoke-direct {p2, p1}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneAdItem;-><init>(Landroid/view/View;)V

    .line 61
    .line 62
    .line 63
    return-object p2
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public o〇O(Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇0O:Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;

    .line 2
    .line 3
    if-eq p1, v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v1, "setTaskBannerModel, model: "

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {p1}, Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;->toString()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const-string v1, "ScanDoneOperationsAdapter"

    .line 27
    .line 28
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    iput-object p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇0O:Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;

    .line 32
    .line 33
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 34
    .line 35
    .line 36
    :cond_0
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇80()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->〇0O:Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v1, 0x1

    .line 6
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;->O8(Z)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0, v1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O〇80o08O()V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->oOo〇8o008:I

    .line 2
    .line 3
    if-lez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    iput-boolean v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->oOo0:Z

    .line 7
    .line 8
    const-string v1, "PAYLOAD_UPDATE_VIP_MONTH_CARD_RED_ENVELOPE"

    .line 9
    .line 10
    invoke-virtual {p0, v0, v1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(ILjava/lang/Object;)V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/scandone/ScanDoneNewActivity$Companion;
.super Ljava/lang/Object;
.source "ScanDoneNewActivity.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/scandone/ScanDoneNewActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewActivity$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final startActivityForResult(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/scandone/ScanDoneModel;I)V
    .locals 3

    const-string v0, "ScanDoneNewActivity"

    if-eqz p1, :cond_0

    .line 9
    sget-object v1, Lcom/intsig/camscanner/scandone/ScanDoneNewActivity;->O0O:Lcom/intsig/camscanner/scandone/ScanDoneNewActivity$Companion;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p1, v2}, Lcom/intsig/camscanner/scandone/ScanDoneNewActivity$Companion;->〇080(Lcom/intsig/camscanner/scandone/ScanDoneModel;Landroidx/appcompat/app/AppCompatActivity;Z)Landroid/content/Intent;

    move-result-object p2

    .line 10
    invoke-virtual {p1, p2, p3}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    const-string p1, "startActivityForResult"

    .line 11
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-nez p1, :cond_1

    const-string p1, "startActivityForResult, get null activity"

    .line 13
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public final startActivityForResult(Landroidx/fragment/app/Fragment;Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/scandone/ScanDoneModel;IZ)V
    .locals 2

    const/4 v0, 0x0

    const-string v1, "ScanDoneNewActivity"

    if-eqz p1, :cond_2

    if-eqz p2, :cond_0

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scandone/ScanDoneNewActivity;->O0O:Lcom/intsig/camscanner/scandone/ScanDoneNewActivity$Companion;

    invoke-virtual {v0, p3, p2, p5}, Lcom/intsig/camscanner/scandone/ScanDoneNewActivity$Companion;->〇080(Lcom/intsig/camscanner/scandone/ScanDoneModel;Landroidx/appcompat/app/AppCompatActivity;Z)Landroid/content/Intent;

    move-result-object p2

    .line 2
    invoke-virtual {p1, p2, p4}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    const-string p1, "startActivityForResult"

    .line 3
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 4
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    :cond_0
    if-nez v0, :cond_1

    .line 5
    sget-object p1, Lcom/intsig/camscanner/scandone/ScanDoneNewActivity;->O0O:Lcom/intsig/camscanner/scandone/ScanDoneNewActivity$Companion;

    const-string p1, "startActivityForResult, get null activity"

    .line 6
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 7
    :cond_1
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    :cond_2
    if-nez v0, :cond_3

    const-string p1, "startActivityForResult, get null fragment"

    .line 8
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    return-void
.end method

.method public final 〇080(Lcom/intsig/camscanner/scandone/ScanDoneModel;Landroidx/appcompat/app/AppCompatActivity;Z)Landroid/content/Intent;
    .locals 2
    .param p2    # Landroidx/appcompat/app/AppCompatActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Landroid/content/Intent;

    .line 7
    .line 8
    const-class v1, Lcom/intsig/camscanner/scandone/ScanDoneNewActivity;

    .line 9
    .line 10
    invoke-direct {v0, p2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 11
    .line 12
    .line 13
    const-string p2, "import"

    .line 14
    .line 15
    invoke-virtual {v0, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 16
    .line 17
    .line 18
    const-string p2, "extra_key_scan_model"

    .line 19
    .line 20
    invoke-virtual {v0, p2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 21
    .line 22
    .line 23
    return-object v0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

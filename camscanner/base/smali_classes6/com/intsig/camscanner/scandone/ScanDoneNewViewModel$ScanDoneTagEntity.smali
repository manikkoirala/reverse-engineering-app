.class public final Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;
.super Ljava/lang/Object;
.source "ScanDoneNewViewModel.kt"

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/chad/library/adapter/base/entity/MultiItemEntity;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ScanDoneTagEntity"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final O8o08O8O:Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final OO:Ljava/lang/String;

.field private o0:J

.field private o〇00O:I

.field private 〇08O〇00〇o:Z

.field private final 〇OOo8〇0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->O8o08O8O:Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity$Companion;

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity$Creator;

    .line 10
    .line 11
    invoke-direct {v0}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity$Creator;-><init>()V

    .line 12
    .line 13
    .line 14
    sput-object v0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;ZI)V
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "tagName"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-wide p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->o0:J

    iput-object p3, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->〇OOo8〇0:Ljava/lang/String;

    iput-object p4, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->OO:Ljava/lang/String;

    .line 3
    iput-boolean p5, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->〇08O〇00〇o:Z

    .line 4
    iput p6, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->o〇00O:I

    return-void
.end method

.method public synthetic constructor <init>(JLjava/lang/String;Ljava/lang/String;ZIILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p8, p7, 0x4

    if-eqz p8, :cond_0

    const/4 p4, 0x0

    :cond_0
    move-object v4, p4

    and-int/lit8 p4, p7, 0x8

    const/4 p8, 0x0

    if-eqz p4, :cond_1

    const/4 v5, 0x0

    goto :goto_0

    :cond_1
    move v5, p5

    :goto_0
    and-int/lit8 p4, p7, 0x10

    if-eqz p4, :cond_2

    const/4 v6, 0x0

    goto :goto_1

    :cond_2
    move v6, p6

    :goto_1
    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    .line 5
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;-><init>(JLjava/lang/String;Ljava/lang/String;ZI)V

    return-void
.end method


# virtual methods
.method public final O8()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->〇OOo8〇0:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final OO0o〇〇〇〇0(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->o0:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final Oo08()Z
    .locals 3

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->o〇00O:I

    .line 2
    .line 3
    const/4 v1, 0x3

    .line 4
    const/4 v2, 0x0

    .line 5
    if-gt v1, v0, :cond_0

    .line 6
    .line 7
    const/4 v1, 0x6

    .line 8
    if-ge v0, v1, :cond_0

    .line 9
    .line 10
    const/4 v2, 0x1

    .line 11
    :cond_0
    return v2
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public describeContents()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    if-ne v1, p1, :cond_0

    .line 13
    .line 14
    const/4 v0, 0x1

    .line 15
    :cond_0
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public hashCode()I
    .locals 8

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->o〇00O:I

    .line 2
    .line 3
    const/4 v1, 0x7

    .line 4
    const/4 v2, 0x1

    .line 5
    const/4 v3, 0x0

    .line 6
    const/4 v4, 0x3

    .line 7
    const/4 v5, 0x2

    .line 8
    if-eq v0, v1, :cond_1

    .line 9
    .line 10
    if-ne v0, v5, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    new-array v0, v4, [Ljava/lang/Object;

    .line 14
    .line 15
    iget-wide v6, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->o0:J

    .line 16
    .line 17
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    aput-object v1, v0, v3

    .line 22
    .line 23
    iget-object v1, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->OO:Ljava/lang/String;

    .line 24
    .line 25
    aput-object v1, v0, v2

    .line 26
    .line 27
    iget v1, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->o〇00O:I

    .line 28
    .line 29
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    aput-object v1, v0, v5

    .line 34
    .line 35
    invoke-static {v0}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    return v0

    .line 40
    :cond_1
    :goto_0
    new-array v1, v4, [Ljava/lang/Object;

    .line 41
    .line 42
    iget-object v4, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->〇OOo8〇0:Ljava/lang/String;

    .line 43
    .line 44
    aput-object v4, v1, v3

    .line 45
    .line 46
    iget-object v3, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->OO:Ljava/lang/String;

    .line 47
    .line 48
    aput-object v3, v1, v2

    .line 49
    .line 50
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    aput-object v0, v1, v5

    .line 55
    .line 56
    invoke-static {v1}, Ljava/util/Objects;->hash([Ljava/lang/Object;)I

    .line 57
    .line 58
    .line 59
    move-result v0

    .line 60
    return v0
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final o〇0()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->〇08O〇00〇o:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public toString()Ljava/lang/String;
    .locals 8
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->o0:J

    .line 2
    .line 3
    iget-object v2, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->〇OOo8〇0:Ljava/lang/String;

    .line 4
    .line 5
    iget-object v3, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->OO:Ljava/lang/String;

    .line 6
    .line 7
    iget-boolean v4, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->〇08O〇00〇o:Z

    .line 8
    .line 9
    iget v5, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->o〇00O:I

    .line 10
    .line 11
    new-instance v6, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v7, "ScanDoneTagEntity(tagId="

    .line 17
    .line 18
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    const-string v0, ", tagName="

    .line 25
    .line 26
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    const-string v0, ", tagGroup="

    .line 33
    .line 34
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    const-string v0, ", isSelected="

    .line 41
    .line 42
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    const-string v0, ", entityType="

    .line 49
    .line 50
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    const-string v0, ")"

    .line 57
    .line 58
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    return-object v0
    .line 66
    .line 67
    .line 68
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string p2, "out"

    .line 2
    .line 3
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-wide v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->o0:J

    .line 7
    .line 8
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 9
    .line 10
    .line 11
    iget-object p2, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->〇OOo8〇0:Ljava/lang/String;

    .line 12
    .line 13
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iget-object p2, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->OO:Ljava/lang/String;

    .line 17
    .line 18
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    iget-boolean p2, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->〇08O〇00〇o:Z

    .line 22
    .line 23
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 24
    .line 25
    .line 26
    iget p2, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->o〇00O:I

    .line 27
    .line 28
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
.end method

.method public 〇080()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->o〇00O:I

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇80〇808〇O(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->〇08O〇00〇o:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇o00〇〇Oo()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->o〇00O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o〇()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->o0:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;
.super Lcom/intsig/mvp/fragment/BaseChangeFragment;
.source "ScanDoneNewFragment.kt"

# interfaces
.implements Lcom/intsig/camscanner/scandone/ScanDoneView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080OO8〇0:Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field static final synthetic 〇0O:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final O8o08O8O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO:Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;

.field private final o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:Z

.field private 〇08O〇00〇o:Lcom/intsig/camscanner/scandone/DonePresenter;

.field private final 〇OOo8〇0:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/FragmentScanDoneNewBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇0O:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇080OO8〇0:Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$Companion;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v6, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/FragmentScanDoneNewBinding;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x4

    .line 10
    const/4 v5, 0x0

    .line 11
    move-object v0, v6

    .line 12
    move-object v2, p0

    .line 13
    invoke-direct/range {v0 .. v5}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object v6, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 17
    .line 18
    new-instance v0, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$mViewModel$2;

    .line 19
    .line 20
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$mViewModel$2;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;)V

    .line 21
    .line 22
    .line 23
    invoke-static {v0}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇OOo8〇0:Lkotlin/Lazy;

    .line 28
    .line 29
    sget-object v0, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 30
    .line 31
    new-instance v1, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$mDocLifecycleDataChangeManager$2;

    .line 32
    .line 33
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$mDocLifecycleDataChangeManager$2;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;)V

    .line 34
    .line 35
    .line 36
    invoke-static {v0, v1}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->O8o08O8O:Lkotlin/Lazy;

    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final O0O0〇(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic O0〇0(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇Oo〇O(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final O8〇8〇O80(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final Ooo8o()Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->O8o08O8O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final O〇8〇008(I)V
    .locals 3

    .line 1
    const-string v0, "type"

    .line 2
    .line 3
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    const-string v1, "CSScanDone"

    .line 8
    .line 9
    const-string v2, "card_show"

    .line 10
    .line 11
    invoke-static {v1, v2, v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o00〇88〇08(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->O0O0〇(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final o88(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o880(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->O8〇8〇O80(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final oOoO8OO〇()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇0ooOOo()Lcom/intsig/camscanner/databinding/FragmentScanDoneNewBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentScanDoneNewBinding;->o〇00O:Landroid/view/ViewStub;

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-static {v0}, Lcom/intsig/camscanner/util/ViewExtKt;->〇O8o08O(Landroid/view/ViewStub;)Landroid/view/View;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    move-object v0, v1

    .line 18
    :goto_0
    sget-object v2, Lcom/intsig/camscanner/purchase/scandone/vip_month/ScanDoneVipMonthManager;->〇080:Lcom/intsig/camscanner/purchase/scandone/vip_month/ScanDoneVipMonthManager;

    .line 19
    .line 20
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇0ooOOo()Lcom/intsig/camscanner/databinding/FragmentScanDoneNewBinding;

    .line 21
    .line 22
    .line 23
    move-result-object v3

    .line 24
    if-eqz v3, :cond_1

    .line 25
    .line 26
    iget-object v1, v3, Lcom/intsig/camscanner/databinding/FragmentScanDoneNewBinding;->〇08O〇00〇o:Landroidx/recyclerview/widget/RecyclerView;

    .line 27
    .line 28
    :cond_1
    new-instance v3, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$doVipMonthAnimation$1;

    .line 29
    .line 30
    invoke-direct {v3, p0, v0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$doVipMonthAnimation$1;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;Landroid/view/View;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {v2, p0, v0, v1, v3}, Lcom/intsig/camscanner/purchase/scandone/vip_month/ScanDoneVipMonthManager;->〇〇888(Lcom/intsig/mvp/fragment/BaseChangeFragment;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;Lkotlin/jvm/functions/Function0;)V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic oO〇oo(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->o88(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oooO888(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇O0o〇〇o(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic o〇0〇o(Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;)Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->OO:Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o〇O8OO(Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;)Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇o08()Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final o〇oo(Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;Ljava/lang/Object;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/scandone/DonePresenter;

    .line 7
    .line 8
    if-eqz p1, :cond_1

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇o08()Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 11
    .line 12
    .line 13
    move-result-object p0

    .line 14
    invoke-virtual {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;->o08〇〇0O()Lcom/intsig/camscanner/scandone/ScanDoneModel;

    .line 15
    .line 16
    .line 17
    move-result-object p0

    .line 18
    if-eqz p0, :cond_0

    .line 19
    .line 20
    iget-object p0, p0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->title:Ljava/lang/String;

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    const/4 p0, 0x0

    .line 24
    :goto_0
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/scandone/DonePresenter;->O8O〇88oO0(Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    :cond_1
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇088O(Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;)Lcom/intsig/camscanner/databinding/FragmentScanDoneNewBinding;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇0ooOOo()Lcom/intsig/camscanner/databinding/FragmentScanDoneNewBinding;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇08O()V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NotifyDataSetChanged"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇o08()Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;->OoOOo8()Landroidx/lifecycle/MutableLiveData;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    new-instance v1, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$subscribeLiveData$1;

    .line 10
    .line 11
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$subscribeLiveData$1;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;)V

    .line 12
    .line 13
    .line 14
    new-instance v2, Loo8〇〇/〇〇〇0〇〇0;

    .line 15
    .line 16
    invoke-direct {v2, v1}, Loo8〇〇/〇〇〇0〇〇0;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p0, v2}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 20
    .line 21
    .line 22
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇o08()Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-virtual {v0}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;->〇008〇oo()Landroidx/lifecycle/MutableLiveData;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    new-instance v1, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$subscribeLiveData$2;

    .line 31
    .line 32
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$subscribeLiveData$2;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;)V

    .line 33
    .line 34
    .line 35
    new-instance v2, Loo8〇〇/o〇0OOo〇0;

    .line 36
    .line 37
    invoke-direct {v2, v1}, Loo8〇〇/o〇0OOo〇0;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, p0, v2}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 41
    .line 42
    .line 43
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇o08()Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;->o08O()Landroidx/lifecycle/MutableLiveData;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    new-instance v1, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$subscribeLiveData$3;

    .line 52
    .line 53
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$subscribeLiveData$3;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;)V

    .line 54
    .line 55
    .line 56
    new-instance v2, Loo8〇〇/〇〇0o;

    .line 57
    .line 58
    invoke-direct {v2, v1}, Loo8〇〇/〇〇0o;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {v0, p0, v2}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 62
    .line 63
    .line 64
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇o08()Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    invoke-virtual {v0}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;->OOo88OOo()Landroidx/lifecycle/MutableLiveData;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    new-instance v1, Loo8〇〇/〇08O8o〇0;

    .line 73
    .line 74
    invoke-direct {v1, p0}, Loo8〇〇/〇08O8o〇0;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;)V

    .line 75
    .line 76
    .line 77
    invoke-virtual {v0, p0, v1}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 78
    .line 79
    .line 80
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇o08()Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 81
    .line 82
    .line 83
    move-result-object v0

    .line 84
    invoke-virtual {v0}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;->oO8008O()Landroidx/lifecycle/MutableLiveData;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 89
    .line 90
    .line 91
    move-result-object v1

    .line 92
    new-instance v2, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$subscribeLiveData$5;

    .line 93
    .line 94
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$subscribeLiveData$5;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;)V

    .line 95
    .line 96
    .line 97
    new-instance v3, Loo8〇〇/oO;

    .line 98
    .line 99
    invoke-direct {v3, v2}, Loo8〇〇/oO;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 100
    .line 101
    .line 102
    invoke-virtual {v0, v1, v3}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 103
    .line 104
    .line 105
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇o08()Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 106
    .line 107
    .line 108
    move-result-object v0

    .line 109
    invoke-virtual {v0}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;->O〇oO〇oo8o()Landroidx/lifecycle/MutableLiveData;

    .line 110
    .line 111
    .line 112
    move-result-object v0

    .line 113
    new-instance v1, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$subscribeLiveData$6;

    .line 114
    .line 115
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$subscribeLiveData$6;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;)V

    .line 116
    .line 117
    .line 118
    new-instance v2, Loo8〇〇/〇8;

    .line 119
    .line 120
    invoke-direct {v2, v1}, Loo8〇〇/〇8;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 121
    .line 122
    .line 123
    invoke-virtual {v0, p0, v2}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 124
    .line 125
    .line 126
    return-void
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final 〇0oO〇oo00()V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/purchase/scandone/vip_month/ScanDoneVipMonthManager;->〇O00()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    new-instance v1, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v2, "initVipMonth, style: "

    .line 11
    .line 12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    const-string v2, "ScanDoneNewFragment"

    .line 23
    .line 24
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->O〇8〇008(I)V

    .line 28
    .line 29
    .line 30
    if-nez v0, :cond_0

    .line 31
    .line 32
    return-void

    .line 33
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->OO:Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;

    .line 34
    .line 35
    if-eqz v1, :cond_1

    .line 36
    .line 37
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;->o0ooO(I)V

    .line 38
    .line 39
    .line 40
    :cond_1
    const/4 v1, 0x1

    .line 41
    if-ne v0, v1, :cond_2

    .line 42
    .line 43
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->oOoO8OO〇()V

    .line 44
    .line 45
    .line 46
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/purchase/scandone/vip_month/ScanDoneVipMonthManager;->〇080:Lcom/intsig/camscanner/purchase/scandone/vip_month/ScanDoneVipMonthManager;

    .line 47
    .line 48
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/scandone/vip_month/ScanDoneVipMonthManager;->〇〇8O0〇8()V

    .line 49
    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇0ooOOo()Lcom/intsig/camscanner/databinding/FragmentScanDoneNewBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇0O:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/FragmentScanDoneNewBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇0〇0(Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇8O0880(Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇8O0880(Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;)V
    .locals 6

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    invoke-virtual {p1}, Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;->〇o00〇〇Oo()Z

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    move-object v1, v0

    .line 14
    :goto_0
    sget-object v2, Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneVipTaskManager;->〇080:Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneVipTaskManager;

    .line 15
    .line 16
    invoke-virtual {v2}, Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneVipTaskManager;->〇80〇808〇O()Z

    .line 17
    .line 18
    .line 19
    move-result v3

    .line 20
    new-instance v4, Ljava/lang/StringBuilder;

    .line 21
    .line 22
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 23
    .line 24
    .line 25
    const-string v5, "showVipTaskToast, status: "

    .line 26
    .line 27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    const-string v1, ", show: "

    .line 34
    .line 35
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    const-string v3, "ScanDoneNewFragment"

    .line 46
    .line 47
    invoke-static {v3, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    const/4 v1, 0x0

    .line 51
    if-eqz p1, :cond_1

    .line 52
    .line 53
    invoke-virtual {p1}, Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneTaskBannerModel;->〇o00〇〇Oo()Z

    .line 54
    .line 55
    .line 56
    move-result p1

    .line 57
    if-nez p1, :cond_1

    .line 58
    .line 59
    const/4 v1, 0x1

    .line 60
    :cond_1
    if-eqz v1, :cond_5

    .line 61
    .line 62
    invoke-virtual {v2}, Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneVipTaskManager;->〇80〇808〇O()Z

    .line 63
    .line 64
    .line 65
    move-result p1

    .line 66
    if-nez p1, :cond_2

    .line 67
    .line 68
    goto :goto_2

    .line 69
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇0ooOOo()Lcom/intsig/camscanner/databinding/FragmentScanDoneNewBinding;

    .line 70
    .line 71
    .line 72
    move-result-object p1

    .line 73
    if-eqz p1, :cond_3

    .line 74
    .line 75
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentScanDoneNewBinding;->O8o08O8O:Landroid/view/ViewStub;

    .line 76
    .line 77
    if-eqz p1, :cond_3

    .line 78
    .line 79
    invoke-static {p1}, Lcom/intsig/camscanner/util/ViewExtKt;->〇O8o08O(Landroid/view/ViewStub;)Landroid/view/View;

    .line 80
    .line 81
    .line 82
    move-result-object p1

    .line 83
    goto :goto_1

    .line 84
    :cond_3
    move-object p1, v0

    .line 85
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇0ooOOo()Lcom/intsig/camscanner/databinding/FragmentScanDoneNewBinding;

    .line 86
    .line 87
    .line 88
    move-result-object v1

    .line 89
    if-eqz v1, :cond_4

    .line 90
    .line 91
    iget-object v0, v1, Lcom/intsig/camscanner/databinding/FragmentScanDoneNewBinding;->〇08O〇00〇o:Landroidx/recyclerview/widget/RecyclerView;

    .line 92
    .line 93
    :cond_4
    new-instance v1, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$showVipTaskToast$1;

    .line 94
    .line 95
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$showVipTaskToast$1;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;)V

    .line 96
    .line 97
    .line 98
    invoke-virtual {v2, p0, p1, v0, v1}, Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneVipTaskManager;->O8(Lcom/intsig/mvp/fragment/BaseChangeFragment;Landroid/view/View;Landroidx/recyclerview/widget/RecyclerView;Lkotlin/jvm/functions/Function0;)V

    .line 99
    .line 100
    .line 101
    :cond_5
    :goto_2
    return-void
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static final synthetic 〇8〇80o(Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->o〇00O:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->o〇oo(Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇O0o〇〇o(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇O8oOo0(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇〇〇0(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇O8〇8000(Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/scandone/DonePresenter;

    .line 7
    .line 8
    if-eqz p0, :cond_0

    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/intsig/camscanner/scandone/DonePresenter;->OO0〇〇8()V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇O8〇8O0oO()Z
    .locals 4

    .line 1
    invoke-static {}, Lcom/intsig/tsapp/account/exp/ShowLoginGuideBubbleExp;->Oo08()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    iput-boolean v1, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->o〇00O:Z

    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->O8O〇()V

    .line 12
    .line 13
    .line 14
    sget-object v0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->OO0o〇〇〇〇0:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion;

    .line 15
    .line 16
    const/4 v1, 0x1

    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion;->〇o〇(Z)V

    .line 18
    .line 19
    .line 20
    sget-object v0, Lcom/intsig/tsapp/account/login/LoginBottomDialog;->O0O:Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;

    .line 21
    .line 22
    const-string v2, "cs_scandone"

    .line 23
    .line 24
    invoke-virtual {v0, v2}, Lcom/intsig/tsapp/account/login/LoginBottomDialog$Companion;->〇80〇808〇O(Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 28
    .line 29
    const-string v2, "mActivity"

    .line 30
    .line 31
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    const/4 v2, 0x2

    .line 35
    const/4 v3, 0x0

    .line 36
    invoke-static {v0, v3, v2, v3}, Lcom/intsig/tsapp/account/helper/LoginHelper;->〇O〇(Landroid/content/Context;Lcom/intsig/tsapp/account/login_task/LoginFinishListener;ILjava/lang/Object;)V

    .line 37
    .line 38
    .line 39
    return v1
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final 〇Oo〇O(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇o08()Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇OOo8〇0:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇o〇88〇8(Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;)Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->Ooo8o()Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇O80〇0o()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/scandone/DonePresenter;

    .line 2
    .line 3
    if-eqz v0, :cond_3

    .line 4
    .line 5
    new-instance v1, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;

    .line 6
    .line 7
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇o08()Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 10
    .line 11
    .line 12
    move-result-object v3

    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇o08()Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 14
    .line 15
    .line 16
    move-result-object v4

    .line 17
    invoke-virtual {v4}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;->o08〇〇0O()Lcom/intsig/camscanner/scandone/ScanDoneModel;

    .line 18
    .line 19
    .line 20
    move-result-object v4

    .line 21
    const/4 v5, 0x0

    .line 22
    if-eqz v4, :cond_0

    .line 23
    .line 24
    iget-boolean v4, v4, Lcom/intsig/camscanner/scandone/ScanDoneModel;->isTeamDoc:Z

    .line 25
    .line 26
    const/4 v6, 0x1

    .line 27
    if-ne v4, v6, :cond_0

    .line 28
    .line 29
    const/4 v5, 0x1

    .line 30
    :cond_0
    invoke-direct {v1, v2, v0, v3, v5}, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;-><init>(Landroid/content/Context;Lcom/intsig/camscanner/scandone/DonePresenter;Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;Z)V

    .line 31
    .line 32
    .line 33
    iput-object v1, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->OO:Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;

    .line 34
    .line 35
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇0ooOOo()Lcom/intsig/camscanner/databinding/FragmentScanDoneNewBinding;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    if-eqz v0, :cond_1

    .line 40
    .line 41
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentScanDoneNewBinding;->〇08O〇00〇o:Landroidx/recyclerview/widget/RecyclerView;

    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_1
    const/4 v0, 0x0

    .line 45
    :goto_0
    if-nez v0, :cond_2

    .line 46
    .line 47
    goto :goto_1

    .line 48
    :cond_2
    iget-object v1, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->OO:Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;

    .line 49
    .line 50
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 51
    .line 52
    .line 53
    goto :goto_1

    .line 54
    :cond_3
    const-string v0, "ScanDoneNewFragment"

    .line 55
    .line 56
    const-string v1, "initView error, get mPresenter NULL! now finish Activity"

    .line 57
    .line 58
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 62
    .line 63
    if-eqz v0, :cond_4

    .line 64
    .line 65
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 66
    .line 67
    .line 68
    :cond_4
    :goto_1
    return-void
.end method

.method public static synthetic 〇〇o0〇8(Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇O8〇8000(Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇〇〇0(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇〇〇00()V
    .locals 4

    .line 1
    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/camscanner/multiimageedit/factory/NewInstanceFactoryImpl;->〇080()Landroidx/lifecycle/ViewModelProvider$NewInstanceFactory;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const-string v2, "getInstance()"

    .line 8
    .line 9
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    invoke-direct {v0, p0, v1}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    .line 13
    .line 14
    .line 15
    const-class v1, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel;

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel;

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel;->〇80〇808〇O()Landroidx/lifecycle/MutableLiveData;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    new-instance v2, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$initDatabaseCallbackViewModel$1;

    .line 32
    .line 33
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$initDatabaseCallbackViewModel$1;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;)V

    .line 34
    .line 35
    .line 36
    new-instance v3, Loo8〇〇/Oo8Oo00oo;

    .line 37
    .line 38
    invoke-direct {v3, v2}, Loo8〇〇/Oo8Oo00oo;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0, v1, v3}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇〇〇O〇()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-static {}, Lcom/intsig/camscanner/ipo/IPOCheck;->o800o8O()Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    sget-object v2, Lcom/intsig/camscanner/ipo/IPOCheck;->〇080:Lcom/intsig/camscanner/ipo/IPOCheck;

    .line 10
    .line 11
    invoke-virtual {v2}, Lcom/intsig/camscanner/ipo/IPOCheck;->oo88o8O()Z

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    xor-int/lit8 v2, v2, 0x1

    .line 16
    .line 17
    new-instance v3, Ljava/lang/StringBuilder;

    .line 18
    .line 19
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 20
    .line 21
    .line 22
    const-string v4, "isEnable = "

    .line 23
    .line 24
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    const-string v4, " isNotOverLimit = "

    .line 31
    .line 32
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v3

    .line 42
    const-string v4, "ScanDoneNewFragment"

    .line 43
    .line 44
    invoke-static {v4, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    if-nez v1, :cond_0

    .line 48
    .line 49
    if-eqz v2, :cond_0

    .line 50
    .line 51
    const/4 v1, 0x0

    .line 52
    iput-boolean v1, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->o〇00O:Z

    .line 53
    .line 54
    sget-object v2, Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneVipTaskManager;->〇080:Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneVipTaskManager;

    .line 55
    .line 56
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/purchase/scandone/task/ScanDoneVipTaskManager;->〇〇808〇(Z)V

    .line 57
    .line 58
    .line 59
    sget-object v1, Lcom/intsig/camscanner/scandone/ScanDoneCloudDocSyncDialog;->OO:Lcom/intsig/camscanner/scandone/ScanDoneCloudDocSyncDialog$Companion;

    .line 60
    .line 61
    invoke-virtual {v1}, Lcom/intsig/camscanner/scandone/ScanDoneCloudDocSyncDialog$Companion;->〇080()Lcom/intsig/camscanner/scandone/ScanDoneCloudDocSyncDialog;

    .line 62
    .line 63
    .line 64
    move-result-object v1

    .line 65
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    const-string v2, "ScanDoneCloudDocSyncDialog"

    .line 70
    .line 71
    invoke-virtual {v1, v0, v2}, Landroidx/fragment/app/DialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    :cond_0
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method


# virtual methods
.method public initialize(Landroid/os/Bundle;)V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇o08()Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p1}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;->〇o〇o()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    new-instance v0, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v1, "initialize, title = "

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    const-string v0, "ScanDoneNewFragment"

    .line 27
    .line 28
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 32
    .line 33
    const/4 v0, 0x0

    .line 34
    if-eqz p1, :cond_3

    .line 35
    .line 36
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇o08()Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    invoke-virtual {v1}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;->o08〇〇0O()Lcom/intsig/camscanner/scandone/ScanDoneModel;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    if-eqz v1, :cond_0

    .line 45
    .line 46
    iget-boolean v1, v1, Lcom/intsig/camscanner/scandone/ScanDoneModel;->checkShowAd:Z

    .line 47
    .line 48
    const/4 v2, 0x1

    .line 49
    if-ne v1, v2, :cond_0

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_0
    const/4 v2, 0x0

    .line 53
    :goto_0
    if-eqz v2, :cond_1

    .line 54
    .line 55
    sget-object v1, Lcom/intsig/advertisement/adapters/positions/ShotDoneManager;->OO0o〇〇:Lcom/intsig/advertisement/adapters/positions/ShotDoneManager$Companion;

    .line 56
    .line 57
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/positions/ShotDoneManager$Companion;->〇080()Lcom/intsig/advertisement/adapters/positions/ShotDoneManager;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    invoke-virtual {v1, p1}, Lcom/intsig/advertisement/adapters/positions/ShotDoneManager;->o8oO〇(Landroid/app/Activity;)V

    .line 62
    .line 63
    .line 64
    :cond_1
    new-instance v1, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$initialize$1$callback$1;

    .line 65
    .line 66
    invoke-direct {v1, p1, p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment$initialize$1$callback$1;-><init>(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;)V

    .line 67
    .line 68
    .line 69
    new-instance p1, Lcom/intsig/camscanner/scandone/DoneDefaultPresenter;

    .line 70
    .line 71
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇o08()Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 72
    .line 73
    .line 74
    move-result-object v2

    .line 75
    invoke-virtual {v2}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;->o08〇〇0O()Lcom/intsig/camscanner/scandone/ScanDoneModel;

    .line 76
    .line 77
    .line 78
    move-result-object v2

    .line 79
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇o08()Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 80
    .line 81
    .line 82
    move-result-object v3

    .line 83
    invoke-virtual {v3}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;->o08oOO()Lorg/json/JSONObject;

    .line 84
    .line 85
    .line 86
    move-result-object v3

    .line 87
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇0ooOOo()Lcom/intsig/camscanner/databinding/FragmentScanDoneNewBinding;

    .line 88
    .line 89
    .line 90
    move-result-object v4

    .line 91
    if-eqz v4, :cond_2

    .line 92
    .line 93
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/FragmentScanDoneNewBinding;->〇OOo8〇0:Landroid/widget/RelativeLayout;

    .line 94
    .line 95
    goto :goto_1

    .line 96
    :cond_2
    const/4 v4, 0x0

    .line 97
    :goto_1
    invoke-direct {p1, v1, v2, v3, v4}, Lcom/intsig/camscanner/scandone/DoneDefaultPresenter;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneOfflineCallback;Lcom/intsig/camscanner/scandone/ScanDoneModel;Lorg/json/JSONObject;Landroid/view/View;)V

    .line 98
    .line 99
    .line 100
    iput-object p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/scandone/DonePresenter;

    .line 101
    .line 102
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇o08()Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 103
    .line 104
    .line 105
    move-result-object p1

    .line 106
    iget-object v1, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/scandone/DonePresenter;

    .line 107
    .line 108
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;->O8O〇8oo08(Lcom/intsig/camscanner/scandone/DonePresenter;)V

    .line 109
    .line 110
    .line 111
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇〇O80〇0o()V

    .line 112
    .line 113
    .line 114
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇o08()Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 115
    .line 116
    .line 117
    move-result-object p1

    .line 118
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 119
    .line 120
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;->Ooo8(Landroid/content/Context;)V

    .line 121
    .line 122
    .line 123
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇08O()V

    .line 124
    .line 125
    .line 126
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇〇〇00()V

    .line 127
    .line 128
    .line 129
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mHandler:Landroid/os/Handler;

    .line 130
    .line 131
    new-instance v1, Loo8〇〇/o〇8;

    .line 132
    .line 133
    invoke-direct {v1, p0}, Loo8〇〇/o〇8;-><init>(Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;)V

    .line 134
    .line 135
    .line 136
    const-wide/16 v2, 0x1f4

    .line 137
    .line 138
    invoke-virtual {p1, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 139
    .line 140
    .line 141
    sget-object p1, Lcom/intsig/camscanner/purchase/scandone/vip_month/ScanDoneVipMonthManager;->〇080:Lcom/intsig/camscanner/purchase/scandone/vip_month/ScanDoneVipMonthManager;

    .line 142
    .line 143
    invoke-virtual {p1}, Lcom/intsig/camscanner/purchase/scandone/vip_month/ScanDoneVipMonthManager;->o〇0()Z

    .line 144
    .line 145
    .line 146
    move-result p1

    .line 147
    iput-boolean p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->o〇00O:Z

    .line 148
    .line 149
    invoke-static {}, Lcom/intsig/tsapp/account/exp/ShowLoginGuideBubbleExp;->O8()Z

    .line 150
    .line 151
    .line 152
    move-result p1

    .line 153
    if-nez p1, :cond_4

    .line 154
    .line 155
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇〇〇O〇()V

    .line 156
    .line 157
    .line 158
    goto :goto_2

    .line 159
    :cond_4
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇O8〇8O0oO()Z

    .line 160
    .line 161
    .line 162
    :goto_2
    iget-boolean p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->o〇00O:Z

    .line 163
    .line 164
    if-eqz p1, :cond_5

    .line 165
    .line 166
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇0oO〇oo00()V

    .line 167
    .line 168
    .line 169
    goto :goto_3

    .line 170
    :cond_5
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->O〇8〇008(I)V

    .line 171
    .line 172
    .line 173
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇o08()Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;

    .line 174
    .line 175
    .line 176
    move-result-object p1

    .line 177
    invoke-virtual {p1}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel;->〇〇〇()V

    .line 178
    .line 179
    .line 180
    :goto_3
    return-void
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public onDetach()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDetach()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory;->OO0o〇〇〇〇0:Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/tsapp/account/login/onelogin/OneLoginThemeFactory$Companion;->〇o〇(Z)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onResume()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneNewFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/scandone/DonePresenter;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/scandone/DonePresenter;->o80ooO()V

    .line 9
    .line 10
    .line 11
    :cond_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d0339

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

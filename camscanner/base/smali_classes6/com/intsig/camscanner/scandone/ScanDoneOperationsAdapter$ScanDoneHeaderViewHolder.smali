.class Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "ScanDoneOperationsAdapter.java"

# interfaces
.implements Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneAdView;
.implements Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneShareView;
.implements Lcom/intsig/camscanner/scandone/ScanDoneUtil$ScanDoneFeatureView;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ScanDoneHeaderViewHolder"
.end annotation


# instance fields
.field public final O0O:Landroidx/appcompat/widget/AppCompatImageView;

.field public final O88O:Landroidx/appcompat/widget/AppCompatImageView;

.field private final O8o08O8O:Landroid/widget/TextView;

.field private final OO:Landroid/widget/TextView;

.field private final OO〇00〇8oO:Landroid/view/View;

.field private final o0:Landroid/widget/LinearLayout;

.field public final o8o:Landroidx/appcompat/widget/AppCompatTextView;

.field public final o8oOOo:Landroidx/appcompat/widget/AppCompatImageView;

.field private final o8〇OO0〇0o:Landroidx/appcompat/widget/AppCompatTextView;

.field public final oOO〇〇:Landroidx/appcompat/widget/AppCompatImageView;

.field private final oOo0:Landroid/widget/LinearLayout;

.field private final oOo〇8o008:Lcom/intsig/camscanner/view/HorizontalProgressView;

.field public final oo8ooo8O:Lcom/intsig/view/CompleteButton;

.field private final ooo0〇〇O:Landroid/view/View;

.field private final o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

.field public final o〇oO:Lcom/intsig/view/CompleteButton;

.field private final 〇080OO8〇0:Landroid/widget/TextView;

.field private final 〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatImageView;

.field public final 〇08〇o0O:Lcom/intsig/view/CompleteButton;

.field private final 〇0O:Landroidx/recyclerview/widget/RecyclerView;

.field private final 〇8〇oO〇〇8o:Landroidx/appcompat/widget/AppCompatTextView;

.field private final 〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

.field public final 〇O〇〇O8:Landroidx/appcompat/widget/AppCompatTextView;

.field public final 〇o0O:Landroidx/appcompat/widget/AppCompatTextView;

.field private final 〇〇08O:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    const v0, 0x7f0a0c1c

    .line 5
    .line 6
    .line 7
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    check-cast v0, Landroid/widget/LinearLayout;

    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->o0:Landroid/widget/LinearLayout;

    .line 14
    .line 15
    const v0, 0x7f0a00c9

    .line 16
    .line 17
    .line 18
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    check-cast v0, Landroidx/appcompat/widget/AppCompatImageView;

    .line 23
    .line 24
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 25
    .line 26
    const v0, 0x7f0a14d6

    .line 27
    .line 28
    .line 29
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    check-cast v0, Landroid/widget/TextView;

    .line 34
    .line 35
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->OO:Landroid/widget/TextView;

    .line 36
    .line 37
    const v0, 0x7f0a00c8

    .line 38
    .line 39
    .line 40
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    check-cast v0, Landroidx/appcompat/widget/AppCompatImageView;

    .line 45
    .line 46
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 47
    .line 48
    const v0, 0x7f0a00c7

    .line 49
    .line 50
    .line 51
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    check-cast v0, Landroidx/appcompat/widget/AppCompatImageView;

    .line 56
    .line 57
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 58
    .line 59
    const v0, 0x7f0a0264

    .line 60
    .line 61
    .line 62
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    check-cast v0, Landroid/widget/TextView;

    .line 67
    .line 68
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->O8o08O8O:Landroid/widget/TextView;

    .line 69
    .line 70
    const v0, 0x7f0a0265

    .line 71
    .line 72
    .line 73
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    check-cast v0, Landroid/widget/TextView;

    .line 78
    .line 79
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->〇080OO8〇0:Landroid/widget/TextView;

    .line 80
    .line 81
    const v0, 0x7f0a1028

    .line 82
    .line 83
    .line 84
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    .line 89
    .line 90
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->〇0O:Landroidx/recyclerview/widget/RecyclerView;

    .line 91
    .line 92
    const v0, 0x7f0a0710

    .line 93
    .line 94
    .line 95
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 96
    .line 97
    .line 98
    move-result-object v0

    .line 99
    check-cast v0, Lcom/intsig/camscanner/view/HorizontalProgressView;

    .line 100
    .line 101
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->oOo〇8o008:Lcom/intsig/camscanner/view/HorizontalProgressView;

    .line 102
    .line 103
    const v0, 0x7f0a0b1c

    .line 104
    .line 105
    .line 106
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 107
    .line 108
    .line 109
    move-result-object v0

    .line 110
    check-cast v0, Landroid/widget/LinearLayout;

    .line 111
    .line 112
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->oOo0:Landroid/widget/LinearLayout;

    .line 113
    .line 114
    const v0, 0x7f0a0825

    .line 115
    .line 116
    .line 117
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 118
    .line 119
    .line 120
    move-result-object p1

    .line 121
    iput-object p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->OO〇00〇8oO:Landroid/view/View;

    .line 122
    .line 123
    const v0, 0x7f0a132b

    .line 124
    .line 125
    .line 126
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 127
    .line 128
    .line 129
    move-result-object v0

    .line 130
    check-cast v0, Landroidx/appcompat/widget/AppCompatTextView;

    .line 131
    .line 132
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->o8〇OO0〇0o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 133
    .line 134
    const v0, 0x7f0a1688

    .line 135
    .line 136
    .line 137
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 138
    .line 139
    .line 140
    move-result-object v0

    .line 141
    check-cast v0, Landroidx/appcompat/widget/AppCompatTextView;

    .line 142
    .line 143
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->〇8〇oO〇〇8o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 144
    .line 145
    const v0, 0x7f0a1a2e

    .line 146
    .line 147
    .line 148
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 149
    .line 150
    .line 151
    move-result-object v0

    .line 152
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->ooo0〇〇O:Landroid/view/View;

    .line 153
    .line 154
    const v0, 0x7f0a1a08

    .line 155
    .line 156
    .line 157
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 158
    .line 159
    .line 160
    move-result-object v0

    .line 161
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->〇〇08O:Landroid/view/View;

    .line 162
    .line 163
    const v0, 0x7f0a0a32

    .line 164
    .line 165
    .line 166
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 167
    .line 168
    .line 169
    move-result-object v0

    .line 170
    check-cast v0, Landroidx/appcompat/widget/AppCompatImageView;

    .line 171
    .line 172
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->O0O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 173
    .line 174
    const v0, 0x7f0a0a3a

    .line 175
    .line 176
    .line 177
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 178
    .line 179
    .line 180
    move-result-object v0

    .line 181
    check-cast v0, Landroidx/appcompat/widget/AppCompatImageView;

    .line 182
    .line 183
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->o8oOOo:Landroidx/appcompat/widget/AppCompatImageView;

    .line 184
    .line 185
    const v0, 0x7f0a17a7

    .line 186
    .line 187
    .line 188
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 189
    .line 190
    .line 191
    move-result-object v0

    .line 192
    check-cast v0, Landroidx/appcompat/widget/AppCompatTextView;

    .line 193
    .line 194
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->〇O〇〇O8:Landroidx/appcompat/widget/AppCompatTextView;

    .line 195
    .line 196
    const v0, 0x7f0a17a8

    .line 197
    .line 198
    .line 199
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 200
    .line 201
    .line 202
    move-result-object v0

    .line 203
    check-cast v0, Landroidx/appcompat/widget/AppCompatTextView;

    .line 204
    .line 205
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->〇o0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 206
    .line 207
    const v0, 0x7f0a0a33

    .line 208
    .line 209
    .line 210
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 211
    .line 212
    .line 213
    move-result-object v0

    .line 214
    check-cast v0, Landroidx/appcompat/widget/AppCompatImageView;

    .line 215
    .line 216
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->O88O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 217
    .line 218
    const v0, 0x7f0a095f

    .line 219
    .line 220
    .line 221
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 222
    .line 223
    .line 224
    move-result-object v0

    .line 225
    check-cast v0, Landroidx/appcompat/widget/AppCompatImageView;

    .line 226
    .line 227
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->oOO〇〇:Landroidx/appcompat/widget/AppCompatImageView;

    .line 228
    .line 229
    const v0, 0x7f0a1556

    .line 230
    .line 231
    .line 232
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 233
    .line 234
    .line 235
    move-result-object v0

    .line 236
    check-cast v0, Landroidx/appcompat/widget/AppCompatTextView;

    .line 237
    .line 238
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->o8o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 239
    .line 240
    const v0, 0x7f0a030b

    .line 241
    .line 242
    .line 243
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 244
    .line 245
    .line 246
    move-result-object v0

    .line 247
    check-cast v0, Lcom/intsig/view/CompleteButton;

    .line 248
    .line 249
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->oo8ooo8O:Lcom/intsig/view/CompleteButton;

    .line 250
    .line 251
    const v0, 0x7f0a030c

    .line 252
    .line 253
    .line 254
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 255
    .line 256
    .line 257
    move-result-object v0

    .line 258
    check-cast v0, Lcom/intsig/view/CompleteButton;

    .line 259
    .line 260
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->o〇oO:Lcom/intsig/view/CompleteButton;

    .line 261
    .line 262
    const v0, 0x7f0a030d

    .line 263
    .line 264
    .line 265
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 266
    .line 267
    .line 268
    move-result-object p1

    .line 269
    check-cast p1, Lcom/intsig/view/CompleteButton;

    .line 270
    .line 271
    iput-object p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->〇08〇o0O:Lcom/intsig/view/CompleteButton;

    .line 272
    .line 273
    return-void
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method static bridge synthetic O8ooOoo〇(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;)Landroidx/appcompat/widget/AppCompatImageView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O8〇o(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;)Landroidx/appcompat/widget/AppCompatTextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->〇8〇oO〇〇8o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OOO〇O0(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;)Landroidx/recyclerview/widget/RecyclerView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->〇0O:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O〇8O8〇008(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;)Landroid/widget/LinearLayout;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->oOo0:Landroid/widget/LinearLayout;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic oo〇(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;)Landroidx/appcompat/widget/AppCompatTextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->o8〇OO0〇0o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o〇〇0〇(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;)Lcom/intsig/camscanner/view/HorizontalProgressView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->oOo〇8o008:Lcom/intsig/camscanner/view/HorizontalProgressView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇00(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->OO〇00〇8oO:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇0000OOO(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;)Landroid/widget/TextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->〇080OO8〇0:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇00〇8(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->〇〇08O:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇o(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->ooo0〇〇O:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇oOO8O8(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;)Landroid/widget/TextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->O8o08O8O:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public OO0o〇〇()Landroid/view/View;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->〇08〇o0O:Lcom/intsig/view/CompleteButton;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OoO8()Landroid/widget/TextView;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o800o8O()Landroidx/appcompat/widget/AppCompatImageView;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oo88o8O()Landroidx/appcompat/widget/AppCompatTextView;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->〇O〇〇O8:Landroidx/appcompat/widget/AppCompatTextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇0()Landroidx/appcompat/widget/AppCompatImageView;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇O8〇〇o()Landroidx/appcompat/widget/AppCompatImageView;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇080()Landroid/view/View;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇0〇O0088o()Landroid/widget/TextView;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->OO:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇80〇808〇O()Landroid/widget/LinearLayout;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->o0:Landroid/widget/LinearLayout;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O888o0o()Landroid/widget/ImageView;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O8o08O()Landroid/view/View;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->O0O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o00〇〇Oo()Landroidx/appcompat/widget/AppCompatImageView;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->O0O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇oo〇()Landroid/view/View;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->o〇oO:Lcom/intsig/view/CompleteButton;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇()Landroid/view/View;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇808〇()Landroid/view/View;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->O88O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇888()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    return-object v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇8O0〇8()Landroid/view/View;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneHeaderViewHolder;->oo8ooo8O:Lcom/intsig/view/CompleteButton;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

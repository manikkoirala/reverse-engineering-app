.class public final Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;
.super Lcom/intsig/mvp/activity/BaseChangeActivity;
.source "ViewLargerImageActivity.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/scandone/ViewLargerImageActivity$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field static final synthetic O88O:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final 〇o0O:Lcom/intsig/camscanner/scandone/ViewLargerImageActivity$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O0O:I

.field private o8oOOo:I

.field private final ooo0〇〇O:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇O〇〇O8:Ljava/lang/String;

.field private final 〇〇08O:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/ActivityViewLargerImageLayoutBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;->O88O:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;->〇o0O:Lcom/intsig/camscanner/scandone/ViewLargerImageActivity$Companion;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/ActivityViewLargerImageLayoutBinding;

    .line 7
    .line 8
    invoke-direct {v0, v1, p0}, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;-><init>(Ljava/lang/Class;Landroid/app/Activity;)V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;->ooo0〇〇O:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    iput-boolean v0, p0, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;->〇〇08O:Z

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic O0〇(Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;->〇oO88o(Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O〇080〇o0(IILjava/lang/String;Z)Lcom/bumptech/glide/request/RequestOptions;
    .locals 2

    .line 1
    new-instance v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/bumptech/glide/request/RequestOptions;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/bumptech/glide/load/engine/DiskCacheStrategy;->〇o00〇〇Oo:Lcom/bumptech/glide/load/engine/DiskCacheStrategy;

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->oO80(Lcom/bumptech/glide/load/engine/DiskCacheStrategy;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/bumptech/glide/request/RequestOptions;

    .line 13
    .line 14
    new-instance v1, Lcom/intsig/camscanner/multiimageedit/adapter/GlideImageFileDataExtKey;

    .line 15
    .line 16
    invoke-direct {v1, p3}, Lcom/intsig/camscanner/multiimageedit/adapter/GlideImageFileDataExtKey;-><init>(Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇0(Lcom/bumptech/glide/load/Key;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 20
    .line 21
    .line 22
    move-result-object p3

    .line 23
    const-string v0, "RequestOptions()\n       \u2026ileDataExtKey(imagePath))"

    .line 24
    .line 25
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    check-cast p3, Lcom/bumptech/glide/request/RequestOptions;

    .line 29
    .line 30
    if-nez p4, :cond_0

    .line 31
    .line 32
    if-lez p1, :cond_0

    .line 33
    .line 34
    if-lez p2, :cond_0

    .line 35
    .line 36
    invoke-virtual {p3, p1, p2}, Lcom/bumptech/glide/request/BaseRequestOptions;->O000(II)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    const-string p2, "requestOptions.override(targetWidth, targetHeight)"

    .line 41
    .line 42
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    move-object p3, p1

    .line 46
    check-cast p3, Lcom/bumptech/glide/request/RequestOptions;

    .line 47
    .line 48
    :cond_0
    return-object p3
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final O〇0O〇Oo〇o()Lcom/intsig/camscanner/databinding/ActivityViewLargerImageLayoutBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;->ooo0〇〇O:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;->O88O:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;->〇〇888(Landroid/app/Activity;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/ActivityViewLargerImageLayoutBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final O〇〇O80o8(Ljava/lang/String;)[I
    .locals 13

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;->〇〇08O:Z

    .line 2
    .line 3
    const/high16 v1, 0x3f800000    # 1.0f

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const/high16 v0, 0x3fc00000    # 1.5f

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    .line 11
    .line 12
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;->O〇0O〇Oo〇o()Lcom/intsig/camscanner/databinding/ActivityViewLargerImageLayoutBinding;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    const/4 v3, 0x0

    .line 17
    if-eqz v2, :cond_1

    .line 18
    .line 19
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/ActivityViewLargerImageLayoutBinding;->OO:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 20
    .line 21
    if-eqz v2, :cond_1

    .line 22
    .line 23
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    goto :goto_1

    .line 28
    :cond_1
    const/4 v2, 0x0

    .line 29
    :goto_1
    if-gtz v2, :cond_2

    .line 30
    .line 31
    iget v2, p0, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;->O0O:I

    .line 32
    .line 33
    :cond_2
    const/16 v4, 0x7d0

    .line 34
    .line 35
    if-le v2, v4, :cond_3

    .line 36
    .line 37
    const/16 v2, 0x7d0

    .line 38
    .line 39
    :cond_3
    int-to-float v2, v2

    .line 40
    mul-float v2, v2, v0

    .line 41
    .line 42
    float-to-int v2, v2

    .line 43
    invoke-static {p1, v3}, Lcom/intsig/utils/ImageUtil;->Oooo8o0〇(Ljava/lang/String;Z)[I

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    const/4 v4, 0x1

    .line 48
    if-eqz p1, :cond_4

    .line 49
    .line 50
    int-to-float v5, v2

    .line 51
    mul-float v5, v5, v1

    .line 52
    .line 53
    aget v1, p1, v4

    .line 54
    .line 55
    int-to-float v1, v1

    .line 56
    mul-float v5, v5, v1

    .line 57
    .line 58
    aget p1, p1, v3

    .line 59
    .line 60
    int-to-float p1, p1

    .line 61
    div-float/2addr v5, p1

    .line 62
    float-to-int p1, v5

    .line 63
    goto :goto_2

    .line 64
    :cond_4
    const/4 p1, 0x0

    .line 65
    :goto_2
    iget v1, p0, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;->o8oOOo:I

    .line 66
    .line 67
    if-lez v1, :cond_5

    .line 68
    .line 69
    int-to-double v5, v1

    .line 70
    const-wide v7, 0x3fe6666666666666L    # 0.7

    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    mul-double v5, v5, v7

    .line 76
    .line 77
    float-to-double v9, v0

    .line 78
    mul-double v5, v5, v9

    .line 79
    .line 80
    int-to-double v11, p1

    .line 81
    cmpg-double v0, v5, v11

    .line 82
    .line 83
    if-gez v0, :cond_5

    .line 84
    .line 85
    int-to-double v0, v1

    .line 86
    mul-double v0, v0, v7

    .line 87
    .line 88
    mul-double v0, v0, v9

    .line 89
    .line 90
    double-to-int p1, v0

    .line 91
    :cond_5
    const/4 v0, 0x2

    .line 92
    new-array v0, v0, [I

    .line 93
    .line 94
    aput v2, v0, v3

    .line 95
    .line 96
    aput p1, v0, v4

    .line 97
    .line 98
    return-object v0
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final o0Oo()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;->〇O〇〇O8:Ljava/lang/String;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "mDocId"

    .line 6
    .line 7
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    const/4 v1, 0x1

    .line 12
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;->o808o8o08(Ljava/lang/String;Z)V

    .line 13
    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;->O〇0O〇Oo〇o()Lcom/intsig/camscanner/databinding/ActivityViewLargerImageLayoutBinding;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    if-eqz v0, :cond_1

    .line 20
    .line 21
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityViewLargerImageLayoutBinding;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 22
    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    new-instance v1, Loo8〇〇/O0O8OO088;

    .line 26
    .line 27
    invoke-direct {v1, p0}, Loo8〇〇/O0O8OO088;-><init>(Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31
    .line 32
    .line 33
    :cond_1
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final o808o8o08(Ljava/lang/String;Z)V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;->O〇0O〇Oo〇o()Lcom/intsig/camscanner/databinding/ActivityViewLargerImageLayoutBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityViewLargerImageLayoutBinding;->OO:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 8
    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ZoomImageView;->O〇O〇oO()Z

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    if-nez v1, :cond_2

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ZoomImageView;->o8oO〇()Z

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    if-eqz v1, :cond_1

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    .line 26
    .line 27
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 28
    .line 29
    .line 30
    const-string v2, "loadImage"

    .line 31
    .line 32
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    const-string v2, "ViewLargerImageActivity"

    .line 43
    .line 44
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;->O〇〇O80o8(Ljava/lang/String;)[I

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    invoke-virtual {v0, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 52
    .line 53
    .line 54
    invoke-static {p0}, Lcom/bumptech/glide/Glide;->oo88o8O(Landroidx/fragment/app/FragmentActivity;)Lcom/bumptech/glide/RequestManager;

    .line 55
    .line 56
    .line 57
    move-result-object v2

    .line 58
    invoke-virtual {v2}, Lcom/bumptech/glide/RequestManager;->〇o00〇〇Oo()Lcom/bumptech/glide/RequestBuilder;

    .line 59
    .line 60
    .line 61
    move-result-object v2

    .line 62
    invoke-virtual {v2, p1}, Lcom/bumptech/glide/RequestBuilder;->ooO〇00O(Ljava/lang/String;)Lcom/bumptech/glide/RequestBuilder;

    .line 63
    .line 64
    .line 65
    move-result-object v2

    .line 66
    const/4 v3, 0x0

    .line 67
    aget v3, v1, v3

    .line 68
    .line 69
    const/4 v4, 0x1

    .line 70
    aget v1, v1, v4

    .line 71
    .line 72
    invoke-direct {p0, v3, v1, p1, p2}, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;->O〇080〇o0(IILjava/lang/String;Z)Lcom/bumptech/glide/request/RequestOptions;

    .line 73
    .line 74
    .line 75
    move-result-object p2

    .line 76
    invoke-virtual {v2, p2}, Lcom/bumptech/glide/RequestBuilder;->〇O(Lcom/bumptech/glide/request/BaseRequestOptions;)Lcom/bumptech/glide/RequestBuilder;

    .line 77
    .line 78
    .line 79
    move-result-object p2

    .line 80
    new-instance v1, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity$loadImage$1;

    .line 81
    .line 82
    invoke-direct {v1, v0, p1}, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity$loadImage$1;-><init>(Lcom/intsig/camscanner/view/ZoomImageView;Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    invoke-virtual {p2, v1}, Lcom/bumptech/glide/RequestBuilder;->O〇0(Lcom/bumptech/glide/request/target/Target;)Lcom/bumptech/glide/request/target/Target;

    .line 86
    .line 87
    .line 88
    :cond_2
    :goto_0
    return-void
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static final startActivity(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p0    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;->〇o0O:Lcom/intsig/camscanner/scandone/ViewLargerImageActivity$Companion;

    .line 2
    .line 3
    invoke-virtual {v0, p0, p1}, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity$Companion;->startActivity(Landroid/content/Context;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇oO88o(Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public bridge synthetic dealClickAction(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->〇080(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    const-string p1, "ViewLargerImageActivity"

    .line 2
    .line 3
    const-string v0, "onCreate"

    .line 4
    .line 5
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {p0}, Lcom/intsig/camscanner/app/AppUtil;->〇〇o8(Landroid/app/Activity;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getSupportActionBar()Landroidx/appcompat/app/ActionBar;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    if-eqz p1, :cond_0

    .line 16
    .line 17
    invoke-virtual {p1}, Landroidx/appcompat/app/ActionBar;->hide()V

    .line 18
    .line 19
    .line 20
    :cond_0
    new-instance p1, Landroid/util/DisplayMetrics;

    .line 21
    .line 22
    invoke-direct {p1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-virtual {v0, p1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 34
    .line 35
    .line 36
    iget v0, p1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 37
    .line 38
    iput v0, p0, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;->O0O:I

    .line 39
    .line 40
    iget p1, p1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 41
    .line 42
    iput p1, p0, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;->o8oOOo:I

    .line 43
    .line 44
    invoke-direct {p0}, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;->o0Oo()V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public bridge synthetic onToolbarTitleClick(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->Oo08(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇oo()I
    .locals 1

    .line 1
    const v0, 0x7f0d00cb

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇0ooOOo(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇0ooOOo(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    const-string v0, "extra_doc_id"

    .line 7
    .line 8
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    const-string v0, "null cannot be cast to non-null type kotlin.String"

    .line 13
    .line 14
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    check-cast p1, Ljava/lang/String;

    .line 18
    .line 19
    iput-object p1, p0, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;->〇O〇〇O8:Ljava/lang/String;

    .line 20
    .line 21
    :cond_0
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇0〇0()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

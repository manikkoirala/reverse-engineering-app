.class public Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;
.super Ljava/lang/Object;
.source "ScanDoneCompleteEntity.java"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation


# static fields
.field public static final FLAG_RES_NO_RESOURCE:I = -0x1


# instance fields
.field private clickListener:Landroid/view/View$OnClickListener;

.field private dotNum:J

.field private enable:Z

.field private flagRes:I

.field private iconRes:I

.field private isPlaceholder:Z

.field private showDot:Z

.field private titleRes:I

.field private useSquareFlag:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 16
    iput-boolean v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->enable:Z

    .line 17
    iput-boolean v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->isPlaceholder:Z

    return-void
.end method

.method public constructor <init>(IILandroid/view/View$OnClickListener;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->enable:Z

    const/4 v0, -0x1

    .line 3
    iput v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->flagRes:I

    const/4 v0, 0x0

    .line 4
    iput-boolean v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->useSquareFlag:Z

    .line 5
    iput p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->iconRes:I

    .line 6
    iput p2, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->titleRes:I

    .line 7
    iput-object p3, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->clickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public constructor <init>(IZIILandroid/view/View$OnClickListener;)V
    .locals 1

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 9
    iput-boolean v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->enable:Z

    .line 10
    iput p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->flagRes:I

    .line 11
    iput-boolean p2, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->useSquareFlag:Z

    .line 12
    iput p3, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->iconRes:I

    .line 13
    iput p4, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->titleRes:I

    .line 14
    iput-object p5, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->clickListener:Landroid/view/View$OnClickListener;

    return-void
.end method


# virtual methods
.method public getClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->clickListener:Landroid/view/View$OnClickListener;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDotNum()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->dotNum:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFlagRes()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->flagRes:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getIconRes()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->iconRes:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getTitleRes()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->titleRes:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isEnable()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->enable:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isPlaceholder()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->isPlaceholder:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isShowDot()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->showDot:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public isUseSquareFlag()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->useSquareFlag:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->clickListener:Landroid/view/View$OnClickListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setDotNum(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->dotNum:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setEnable(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->enable:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setFlagRes(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->flagRes:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setIconRes(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->iconRes:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setPlaceholder(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->isPlaceholder:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setShowDot(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->showDot:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setTitleRes(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->titleRes:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setUseSquareFlag(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneCompleteEntity;->useSquareFlag:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

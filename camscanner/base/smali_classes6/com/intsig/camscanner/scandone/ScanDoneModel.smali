.class public final Lcom/intsig/camscanner/scandone/ScanDoneModel;
.super Ljava/lang/Object;
.source "ScanDoneModel.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/intsig/camscanner/scandone/ScanDoneModel;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field public checkShowAd:Z

.field public detectedIdCardFlag:Z

.field public docId:J

.field public docType:I

.field public finishPageType:Ljava/lang/String;

.field public functionEntrance:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public isOfflineDoc:Z

.field public isTeamDoc:Z

.field public newDocType:I

.field public pageNum:I

.field public parentSyncId:Ljava/lang/String;

.field public preCheckTagEntity:Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;

.field public tagId:J

.field public title:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/scandone/ScanDoneModel$Creator;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/scandone/ScanDoneModel$Creator;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 19

    move-object/from16 v0, p0

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const-wide/16 v12, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x3000

    const/16 v18, 0x0

    .line 17
    invoke-direct/range {v0 .. v18}, Lcom/intsig/camscanner/scandone/ScanDoneModel;-><init>(Ljava/lang/String;JZLjava/lang/String;ZIZLcom/intsig/camscanner/purchase/track/FunctionEntrance;ILjava/lang/String;JZLcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JZLjava/lang/String;ZIZLcom/intsig/camscanner/purchase/track/FunctionEntrance;ILjava/lang/String;JZLcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;I)V
    .locals 3

    move-object v0, p0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move-object v1, p1

    .line 2
    iput-object v1, v0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->title:Ljava/lang/String;

    move-wide v1, p2

    .line 3
    iput-wide v1, v0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->docId:J

    move v1, p4

    .line 4
    iput-boolean v1, v0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->isOfflineDoc:Z

    move-object v1, p5

    .line 5
    iput-object v1, v0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->finishPageType:Ljava/lang/String;

    move v1, p6

    .line 6
    iput-boolean v1, v0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->isTeamDoc:Z

    move v1, p7

    .line 7
    iput v1, v0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->docType:I

    move v1, p8

    .line 8
    iput-boolean v1, v0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->detectedIdCardFlag:Z

    move-object v1, p9

    .line 9
    iput-object v1, v0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->functionEntrance:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    move v1, p10

    .line 10
    iput v1, v0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->pageNum:I

    move-object v1, p11

    .line 11
    iput-object v1, v0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->parentSyncId:Ljava/lang/String;

    move-wide v1, p12

    .line 12
    iput-wide v1, v0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->tagId:J

    move/from16 v1, p14

    .line 13
    iput-boolean v1, v0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->checkShowAd:Z

    move-object/from16 v1, p15

    .line 14
    iput-object v1, v0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->preCheckTagEntity:Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;

    move/from16 v1, p16

    .line 15
    iput v1, v0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->newDocType:I

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;JZLjava/lang/String;ZIZLcom/intsig/camscanner/purchase/track/FunctionEntrance;ILjava/lang/String;JZLcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 16

    move/from16 v0, p17

    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    move-object/from16 v1, p1

    :goto_0
    and-int/lit8 v3, v0, 0x2

    const-wide/16 v4, 0x0

    if-eqz v3, :cond_1

    move-wide v6, v4

    goto :goto_1

    :cond_1
    move-wide/from16 v6, p2

    :goto_1
    and-int/lit8 v3, v0, 0x4

    if-eqz v3, :cond_2

    const/4 v3, 0x0

    goto :goto_2

    :cond_2
    move/from16 v3, p4

    :goto_2
    and-int/lit8 v9, v0, 0x8

    if-eqz v9, :cond_3

    const/4 v9, 0x0

    goto :goto_3

    :cond_3
    move-object/from16 v9, p5

    :goto_3
    and-int/lit8 v10, v0, 0x10

    if-eqz v10, :cond_4

    const/4 v10, 0x0

    goto :goto_4

    :cond_4
    move/from16 v10, p6

    :goto_4
    and-int/lit8 v11, v0, 0x20

    if-eqz v11, :cond_5

    const/4 v11, 0x0

    goto :goto_5

    :cond_5
    move/from16 v11, p7

    :goto_5
    and-int/lit8 v12, v0, 0x40

    if-eqz v12, :cond_6

    const/4 v12, 0x0

    goto :goto_6

    :cond_6
    move/from16 v12, p8

    :goto_6
    and-int/lit16 v13, v0, 0x80

    if-eqz v13, :cond_7

    const/4 v13, 0x0

    goto :goto_7

    :cond_7
    move-object/from16 v13, p9

    :goto_7
    and-int/lit16 v14, v0, 0x100

    if-eqz v14, :cond_8

    const/4 v14, 0x0

    goto :goto_8

    :cond_8
    move/from16 v14, p10

    :goto_8
    and-int/lit16 v15, v0, 0x200

    if-eqz v15, :cond_9

    const/4 v15, 0x0

    goto :goto_9

    :cond_9
    move-object/from16 v15, p11

    :goto_9
    and-int/lit16 v2, v0, 0x400

    if-eqz v2, :cond_a

    goto :goto_a

    :cond_a
    move-wide/from16 v4, p12

    :goto_a
    and-int/lit16 v2, v0, 0x800

    if-eqz v2, :cond_b

    const/4 v2, 0x0

    goto :goto_b

    :cond_b
    move/from16 v2, p14

    :goto_b
    and-int/lit16 v8, v0, 0x1000

    if-eqz v8, :cond_c

    const/4 v8, 0x0

    goto :goto_c

    :cond_c
    move-object/from16 v8, p15

    :goto_c
    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_d

    const/4 v0, 0x0

    goto :goto_d

    :cond_d
    move/from16 v0, p16

    :goto_d
    move-object/from16 p1, p0

    move-object/from16 p2, v1

    move-wide/from16 p3, v6

    move/from16 p5, v3

    move-object/from16 p6, v9

    move/from16 p7, v10

    move/from16 p8, v11

    move/from16 p9, v12

    move-object/from16 p10, v13

    move/from16 p11, v14

    move-object/from16 p12, v15

    move-wide/from16 p13, v4

    move/from16 p15, v2

    move-object/from16 p16, v8

    move/from16 p17, v0

    .line 16
    invoke-direct/range {p1 .. p17}, Lcom/intsig/camscanner/scandone/ScanDoneModel;-><init>(Ljava/lang/String;JZLjava/lang/String;ZIZLcom/intsig/camscanner/purchase/track/FunctionEntrance;ILjava/lang/String;JZLcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;I)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 5
    .param p1    # Landroid/os/Parcel;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "out"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->title:Ljava/lang/String;

    .line 7
    .line 8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-wide v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->docId:J

    .line 12
    .line 13
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 14
    .line 15
    .line 16
    iget-boolean v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->isOfflineDoc:Z

    .line 17
    .line 18
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 19
    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->finishPageType:Ljava/lang/String;

    .line 22
    .line 23
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    iget-boolean v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->isTeamDoc:Z

    .line 27
    .line 28
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 29
    .line 30
    .line 31
    iget v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->docType:I

    .line 32
    .line 33
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 34
    .line 35
    .line 36
    iget-boolean v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->detectedIdCardFlag:Z

    .line 37
    .line 38
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 39
    .line 40
    .line 41
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->functionEntrance:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 42
    .line 43
    const/4 v1, 0x0

    .line 44
    const/4 v2, 0x1

    .line 45
    if-nez v0, :cond_0

    .line 46
    .line 47
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_0
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    :goto_0
    iget v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->pageNum:I

    .line 62
    .line 63
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 64
    .line 65
    .line 66
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->parentSyncId:Ljava/lang/String;

    .line 67
    .line 68
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    iget-wide v3, p0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->tagId:J

    .line 72
    .line 73
    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    .line 74
    .line 75
    .line 76
    iget-boolean v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->checkShowAd:Z

    .line 77
    .line 78
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 79
    .line 80
    .line 81
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->preCheckTagEntity:Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;

    .line 82
    .line 83
    if-nez v0, :cond_1

    .line 84
    .line 85
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 86
    .line 87
    .line 88
    goto :goto_1

    .line 89
    :cond_1
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 90
    .line 91
    .line 92
    invoke-virtual {v0, p1, p2}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;->writeToParcel(Landroid/os/Parcel;I)V

    .line 93
    .line 94
    .line 95
    :goto_1
    iget p2, p0, Lcom/intsig/camscanner/scandone/ScanDoneModel;->newDocType:I

    .line 96
    .line 97
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 98
    .line 99
    .line 100
    return-void
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

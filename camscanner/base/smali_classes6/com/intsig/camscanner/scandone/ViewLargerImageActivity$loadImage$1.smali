.class public final Lcom/intsig/camscanner/scandone/ViewLargerImageActivity$loadImage$1;
.super Lcom/bumptech/glide/request/target/CustomTarget;
.source "ViewLargerImageActivity.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/intsig/camscanner/scandone/ViewLargerImageActivity;->o808o8o08(Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/bumptech/glide/request/target/CustomTarget<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field final synthetic o0:Lcom/intsig/camscanner/view/ZoomImageView;

.field final synthetic 〇OOo8〇0:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/intsig/camscanner/view/ZoomImageView;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity$loadImage$1;->o0:Lcom/intsig/camscanner/view/ZoomImageView;

    .line 2
    .line 3
    iput-object p2, p0, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity$loadImage$1;->〇OOo8〇0:Ljava/lang/String;

    .line 4
    .line 5
    invoke-direct {p0}, Lcom/bumptech/glide/request/target/CustomTarget;-><init>()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public onLoadCleared(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onResourceReady(Landroid/graphics/Bitmap;Lcom/bumptech/glide/request/transition/Transition;)V
    .locals 2
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/Bitmap;",
            "Lcom/bumptech/glide/request/transition/Transition<",
            "-",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    const-string p2, "resource"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-static {p2, v0}, Lkotlin/ranges/RangesKt;->〇o〇(II)I

    move-result p2

    sget v0, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇8o8o〇:I

    const/4 v1, 0x1

    if-le p2, v0, :cond_0

    .line 3
    iget-object p2, p0, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity$loadImage$1;->o0:Lcom/intsig/camscanner/view/ZoomImageView;

    const/4 v0, 0x0

    invoke-virtual {p2, v1, v0}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 4
    :cond_0
    iget-object p2, p0, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity$loadImage$1;->o0:Lcom/intsig/camscanner/view/ZoomImageView;

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object p2

    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity$loadImage$1;->〇OOo8〇0:Ljava/lang/String;

    if-eq p2, v0, :cond_1

    return-void

    .line 6
    :cond_1
    iget-object p2, p0, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity$loadImage$1;->o0:Lcom/intsig/camscanner/view/ZoomImageView;

    new-instance v0, Lcom/intsig/camscanner/loadimage/RotateBitmap;

    invoke-direct {v0, p1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {p2, v0, v1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o800o8O(Lcom/intsig/camscanner/loadimage/RotateBitmap;Z)V

    return-void
.end method

.method public bridge synthetic onResourceReady(Ljava/lang/Object;Lcom/bumptech/glide/request/transition/Transition;)V
    .locals 0

    .line 1
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/scandone/ViewLargerImageActivity$loadImage$1;->onResourceReady(Landroid/graphics/Bitmap;Lcom/bumptech/glide/request/transition/Transition;)V

    return-void
.end method

.class public Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "ScanDoneOperationsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ScanDoneVipMonthViewHolder"
.end annotation


# instance fields
.field public o0:Lcom/intsig/camscanner/databinding/LayoutScanDoneVipMonthCard1Binding;

.field private 〇OOo8〇0:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/view/View;I)V
    .locals 3
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    if-ne p2, v0, :cond_0

    .line 6
    .line 7
    const p2, 0x7f0a1a8f

    .line 8
    .line 9
    .line 10
    :try_start_0
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    check-cast p1, Landroid/view/ViewStub;

    .line 15
    .line 16
    if-eqz p1, :cond_1

    .line 17
    .line 18
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/LayoutScanDoneVipMonthCard1Binding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/LayoutScanDoneVipMonthCard1Binding;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    iput-object p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;->o0:Lcom/intsig/camscanner/databinding/LayoutScanDoneVipMonthCard1Binding;

    .line 27
    .line 28
    goto :goto_1

    .line 29
    :catch_0
    move-exception p1

    .line 30
    goto :goto_0

    .line 31
    :cond_0
    const/4 v0, 0x2

    .line 32
    if-ne p2, v0, :cond_1

    .line 33
    .line 34
    const p2, 0x7f0a1a90

    .line 35
    .line 36
    .line 37
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 38
    .line 39
    .line 40
    move-result-object p2

    .line 41
    check-cast p2, Landroid/view/ViewStub;

    .line 42
    .line 43
    if-eqz p2, :cond_1

    .line 44
    .line 45
    invoke-virtual {p2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 46
    .line 47
    .line 48
    move-result-object p2

    .line 49
    instance-of v1, p2, Landroid/widget/ImageView;

    .line 50
    .line 51
    if-eqz v1, :cond_1

    .line 52
    .line 53
    check-cast p2, Landroid/widget/ImageView;

    .line 54
    .line 55
    iput-object p2, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 56
    .line 57
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 58
    .line 59
    .line 60
    move-result-object p2

    .line 61
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 62
    .line 63
    .line 64
    move-result-object v1

    .line 65
    invoke-static {v1}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 66
    .line 67
    .line 68
    move-result v1

    .line 69
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 70
    .line 71
    .line 72
    move-result-object p1

    .line 73
    const/16 v2, 0x10

    .line 74
    .line 75
    invoke-static {p1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 76
    .line 77
    .line 78
    move-result p1

    .line 79
    mul-int/lit8 p1, p1, 0x2

    .line 80
    .line 81
    sub-int/2addr v1, p1

    .line 82
    int-to-float p1, v1

    .line 83
    const v0, 0x43ab8000    # 343.0f

    .line 84
    .line 85
    .line 86
    div-float/2addr p1, v0

    .line 87
    const/high16 v0, 0x430e0000    # 142.0f

    .line 88
    .line 89
    mul-float p1, p1, v0

    .line 90
    .line 91
    float-to-int p1, p1

    .line 92
    iput p1, p2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 93
    .line 94
    iget-object p1, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 95
    .line 96
    invoke-virtual {p1, p2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    .line 98
    .line 99
    goto :goto_1

    .line 100
    :goto_0
    const-string p2, "ScanDoneOperationsAdapter"

    .line 101
    .line 102
    const-string v0, "ScanDoneVipMonthViewHolder create holder"

    .line 103
    .line 104
    invoke-static {p2, v0, p1}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 105
    .line 106
    .line 107
    :cond_1
    :goto_1
    return-void
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method static bridge synthetic 〇00(Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;)Landroid/widget/ImageView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/scandone/ScanDoneOperationsAdapter$ScanDoneVipMonthViewHolder;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

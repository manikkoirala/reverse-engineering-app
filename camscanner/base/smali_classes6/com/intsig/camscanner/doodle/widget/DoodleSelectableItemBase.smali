.class public abstract Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;
.super Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;
.source "DoodleSelectableItemBase.java"

# interfaces
.implements Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;


# instance fields
.field private OoO8:Landroid/graphics/Rect;

.field private o800o8O:Landroid/graphics/Paint;

.field private oo88o8O:Z

.field private 〇0〇O0088o:Landroid/graphics/Rect;

.field public 〇O00:I

.field private 〇O888o0o:Landroid/graphics/PointF;

.field private 〇oo〇:Z

.field public 〇〇8O0〇8:I


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/doodle/widget/core/IDoodle;IFF)V
    .locals 6

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move v5, p4

    .line 1
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;-><init>(Lcom/intsig/camscanner/doodle/widget/core/IDoodle;Lcom/intsig/camscanner/doodle/widget/DoodlePaintAttrs;IFF)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/camscanner/doodle/widget/core/IDoodle;Lcom/intsig/camscanner/doodle/widget/DoodlePaintAttrs;IFF)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;-><init>(Lcom/intsig/camscanner/doodle/widget/core/IDoodle;Lcom/intsig/camscanner/doodle/widget/DoodlePaintAttrs;)V

    const/4 p1, 0x0

    .line 3
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->〇O00:I

    .line 4
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->〇〇8O0〇8:I

    .line 5
    new-instance p2, Landroid/graphics/Rect;

    invoke-direct {p2}, Landroid/graphics/Rect;-><init>()V

    iput-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->〇0〇O0088o:Landroid/graphics/Rect;

    .line 6
    new-instance p2, Landroid/graphics/Rect;

    invoke-direct {p2}, Landroid/graphics/Rect;-><init>()V

    iput-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->OoO8:Landroid/graphics/Rect;

    .line 7
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    iput-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->o800o8O:Landroid/graphics/Paint;

    .line 8
    new-instance p2, Landroid/graphics/PointF;

    invoke-direct {p2}, Landroid/graphics/PointF;-><init>()V

    iput-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->〇O888o0o:Landroid/graphics/PointF;

    .line 9
    iput-boolean p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->oo88o8O:Z

    .line 10
    invoke-virtual {p0, p4, p5}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇80〇808〇O(FF)V

    int-to-float p1, p3

    .line 11
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇o〇(F)V

    return-void
.end method


# virtual methods
.method public O8〇o(F)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->O8〇o(F)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->〇00〇8()Landroid/graphics/Rect;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->o〇8(Landroid/graphics/Rect;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->O8()F

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->〇00〇8()Landroid/graphics/Rect;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    int-to-float v0, v0

    .line 24
    const/high16 v1, 0x40000000    # 2.0f

    .line 25
    .line 26
    div-float/2addr v0, v1

    .line 27
    sub-float/2addr p1, v0

    .line 28
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->Oo08()F

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->〇00〇8()Landroid/graphics/Rect;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    .line 37
    .line 38
    .line 39
    move-result v2

    .line 40
    int-to-float v2, v2

    .line 41
    div-float/2addr v2, v1

    .line 42
    sub-float/2addr v0, v2

    .line 43
    const/4 v1, 0x0

    .line 44
    invoke-virtual {p0, p1, v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->O8ooOoo〇(FFZ)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->〇00〇8()Landroid/graphics/Rect;

    .line 48
    .line 49
    .line 50
    move-result-object p1

    .line 51
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->Oo8Oo00oo(Landroid/graphics/Rect;)V

    .line 52
    .line 53
    .line 54
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public OO0o〇〇〇〇0(FF)Z
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->〇0〇O0088o:Landroid/graphics/Rect;

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->Oo8Oo00oo(Landroid/graphics/Rect;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getLocation()Landroid/graphics/PointF;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    iget v1, v0, Landroid/graphics/PointF;->x:F

    .line 11
    .line 12
    sub-float v4, p1, v1

    .line 13
    .line 14
    iget p1, v0, Landroid/graphics/PointF;->y:F

    .line 15
    .line 16
    sub-float v5, p2, p1

    .line 17
    .line 18
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->〇O888o0o:Landroid/graphics/PointF;

    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇〇808〇()F

    .line 21
    .line 22
    .line 23
    move-result p1

    .line 24
    neg-float p1, p1

    .line 25
    float-to-int p1, p1

    .line 26
    int-to-float v3, p1

    .line 27
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->O8()F

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    iget p2, v0, Landroid/graphics/PointF;->x:F

    .line 32
    .line 33
    sub-float v6, p1, p2

    .line 34
    .line 35
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->Oo08()F

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    iget p2, v0, Landroid/graphics/PointF;->y:F

    .line 40
    .line 41
    sub-float v7, p1, p2

    .line 42
    .line 43
    invoke-static/range {v2 .. v7}, Lcom/intsig/camscanner/doodle/util/DrawUtil;->〇o〇(Landroid/graphics/PointF;FFFFF)Landroid/graphics/PointF;

    .line 44
    .line 45
    .line 46
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->OoO8:Landroid/graphics/Rect;

    .line 47
    .line 48
    iget-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->〇0〇O0088o:Landroid/graphics/Rect;

    .line 49
    .line 50
    invoke-virtual {p1, p2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 51
    .line 52
    .line 53
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->OoO8:Landroid/graphics/Rect;

    .line 54
    .line 55
    iget p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->〇〇8O0〇8:I

    .line 56
    .line 57
    neg-int p2, p2

    .line 58
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->〇O00:I

    .line 59
    .line 60
    neg-int v0, v0

    .line 61
    invoke-virtual {p1, p2, v0}, Landroid/graphics/Rect;->inset(II)V

    .line 62
    .line 63
    .line 64
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->OoO8:Landroid/graphics/Rect;

    .line 65
    .line 66
    iget-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->〇O888o0o:Landroid/graphics/PointF;

    .line 67
    .line 68
    iget v0, p2, Landroid/graphics/PointF;->x:F

    .line 69
    .line 70
    float-to-int v0, v0

    .line 71
    iget p2, p2, Landroid/graphics/PointF;->y:F

    .line 72
    .line 73
    float-to-int p2, p2

    .line 74
    invoke-virtual {p1, v0, p2}, Landroid/graphics/Rect;->contains(II)Z

    .line 75
    .line 76
    .line 77
    move-result p1

    .line 78
    return p1
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method protected Oo8Oo00oo(Landroid/graphics/Rect;)V
    .locals 3

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->o〇8(Landroid/graphics/Rect;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->O8()F

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getLocation()Landroid/graphics/PointF;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    iget v1, v1, Landroid/graphics/PointF;->x:F

    .line 13
    .line 14
    sub-float/2addr v0, v1

    .line 15
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->Oo08()F

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getLocation()Landroid/graphics/PointF;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    iget v2, v2, Landroid/graphics/PointF;->y:F

    .line 24
    .line 25
    sub-float/2addr v1, v2

    .line 26
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getScale()F

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    invoke-static {p1, v2, v0, v1}, Lcom/intsig/camscanner/doodle/util/DrawUtil;->Oo08(Landroid/graphics/Rect;FFF)V

    .line 31
    .line 32
    .line 33
    const/16 p1, 0x8

    .line 34
    .line 35
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇O00(I)V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public o0ooO()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->oo88o8O:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o8()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->〇0〇O0088o:Landroid/graphics/Rect;

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->Oo8Oo00oo(Landroid/graphics/Rect;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇00()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇0(F)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->o〇0(F)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->〇0〇O0088o:Landroid/graphics/Rect;

    .line 5
    .line 6
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->Oo8Oo00oo(Landroid/graphics/Rect;)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇00()V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected abstract o〇8(Landroid/graphics/Rect;)V
.end method

.method public 〇00〇8()Landroid/graphics/Rect;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->〇0〇O0088o:Landroid/graphics/Rect;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O888o0o(Landroid/graphics/Canvas;)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇O8o08O()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->〇oo〇:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o00〇〇Oo(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->oo88o8O:Z

    .line 2
    .line 3
    xor-int/lit8 p1, p1, 0x1

    .line 4
    .line 5
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇oOO8O8(Z)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇00()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇〇〇0〇〇0(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->〇oo〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

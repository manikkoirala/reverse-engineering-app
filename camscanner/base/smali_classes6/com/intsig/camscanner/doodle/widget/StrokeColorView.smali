.class Lcom/intsig/camscanner/doodle/widget/StrokeColorView;
.super Landroid/view/View;
.source "StrokeColorView.java"


# static fields
.field private static final oOo〇8o008:I

.field private static final 〇0O:I


# instance fields
.field private O8o08O8O:F

.field private OO:F

.field private o0:Landroid/graphics/Paint;

.field private o〇00O:F

.field private 〇080OO8〇0:F

.field private 〇08O〇00〇o:F

.field private 〇OOo8〇0:Landroid/graphics/Paint;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    const/high16 v0, 0x40000000    # 2.0f

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/util/DoodleUtils;->〇080(F)I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    sput v1, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->〇0O:I

    .line 8
    .line 9
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/util/DoodleUtils;->〇080(F)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    sput v0, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->oOo〇8o008:I

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 3
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 4
    sget p1, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->〇0O:I

    int-to-float p1, p1

    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->〇080OO8〇0:F

    .line 5
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->〇080()V

    return-void
.end method

.method private 〇080()V
    .locals 5

    .line 1
    new-instance v0, Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->o0:Landroid/graphics/Paint;

    .line 7
    .line 8
    const/4 v1, 0x1

    .line 9
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->o0:Landroid/graphics/Paint;

    .line 13
    .line 14
    const/4 v2, -0x1

    .line 15
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->o0:Landroid/graphics/Paint;

    .line 19
    .line 20
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 21
    .line 22
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->o0:Landroid/graphics/Paint;

    .line 26
    .line 27
    sget v3, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->〇0O:I

    .line 28
    .line 29
    int-to-float v3, v3

    .line 30
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 31
    .line 32
    .line 33
    new-instance v0, Landroid/graphics/Paint;

    .line 34
    .line 35
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 36
    .line 37
    .line 38
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->〇OOo8〇0:Landroid/graphics/Paint;

    .line 39
    .line 40
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 41
    .line 42
    .line 43
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->〇OOo8〇0:Landroid/graphics/Paint;

    .line 44
    .line 45
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 46
    .line 47
    .line 48
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->〇OOo8〇0:Landroid/graphics/Paint;

    .line 49
    .line 50
    sget v2, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->oOo〇8o008:I

    .line 51
    .line 52
    int-to-float v2, v2

    .line 53
    const/4 v3, 0x0

    .line 54
    const/high16 v4, 0x30000000

    .line 55
    .line 56
    invoke-virtual {v0, v2, v3, v3, v4}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 57
    .line 58
    .line 59
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->〇OOo8〇0:Landroid/graphics/Paint;

    .line 60
    .line 61
    invoke-virtual {p0, v1, v0}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 62
    .line 63
    .line 64
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2
    .line 3
    .line 4
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->OO:F

    .line 5
    .line 6
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->〇08O〇00〇o:F

    .line 7
    .line 8
    iget v2, p0, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->O8o08O8O:F

    .line 9
    .line 10
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->〇OOo8〇0:Landroid/graphics/Paint;

    .line 11
    .line 12
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 13
    .line 14
    .line 15
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->OO:F

    .line 16
    .line 17
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->〇08O〇00〇o:F

    .line 18
    .line 19
    iget v2, p0, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->o〇00O:F

    .line 20
    .line 21
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->o0:Landroid/graphics/Paint;

    .line 22
    .line 23
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 2
    .line 3
    .line 4
    int-to-float p1, p1

    .line 5
    const/high16 p3, 0x40000000    # 2.0f

    .line 6
    .line 7
    div-float/2addr p1, p3

    .line 8
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->OO:F

    .line 9
    .line 10
    int-to-float p2, p2

    .line 11
    div-float/2addr p2, p3

    .line 12
    iput p2, p0, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->〇08O〇00〇o:F

    .line 13
    .line 14
    invoke-static {p1, p2}, Ljava/lang/Math;->min(FF)F

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    sget p2, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->oOo〇8o008:I

    .line 19
    .line 20
    int-to-float p2, p2

    .line 21
    sub-float/2addr p1, p2

    .line 22
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->O8o08O8O:F

    .line 23
    .line 24
    iget p2, p0, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->〇080OO8〇0:F

    .line 25
    .line 26
    div-float/2addr p2, p3

    .line 27
    sub-float/2addr p1, p2

    .line 28
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->o〇00O:F

    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public 〇o00〇〇Oo(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->〇OOo8〇0:Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

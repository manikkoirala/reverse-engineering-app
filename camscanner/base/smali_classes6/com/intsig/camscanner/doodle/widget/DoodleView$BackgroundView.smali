.class Lcom/intsig/camscanner/doodle/widget/DoodleView$BackgroundView;
.super Landroid/view/View;
.source "DoodleView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/doodle/widget/DoodleView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BackgroundView"
.end annotation


# instance fields
.field final synthetic o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/doodle/widget/DoodleView;Landroid/content/Context;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$BackgroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 2
    .line 3
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇080(Landroid/graphics/Canvas;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$BackgroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllTranX()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$BackgroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllTranY()F

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$BackgroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllScale()F

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    invoke-virtual {p1, v0, v0}, Landroid/graphics/Canvas;->scale(FF)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$BackgroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 26
    .line 27
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇o00〇〇Oo(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Z

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    const/4 v1, 0x0

    .line 32
    const/4 v2, 0x0

    .line 33
    if-eqz v0, :cond_0

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$BackgroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 36
    .line 37
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇o〇(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Landroid/graphics/Bitmap;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 42
    .line 43
    .line 44
    return-void

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$BackgroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 46
    .line 47
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8o8o〇(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Z

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    if-eqz v0, :cond_1

    .line 52
    .line 53
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$BackgroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 54
    .line 55
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->Oo08(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Landroid/graphics/Bitmap;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    goto :goto_0

    .line 60
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$BackgroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 61
    .line 62
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇o〇(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Landroid/graphics/Bitmap;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    :goto_0
    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 67
    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$BackgroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 6
    .line 7
    invoke-static {v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇888(Lcom/intsig/camscanner/doodle/widget/DoodleView;)I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    int-to-float v1, v1

    .line 12
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    int-to-float v2, v2

    .line 17
    const/high16 v3, 0x40000000    # 2.0f

    .line 18
    .line 19
    div-float/2addr v2, v3

    .line 20
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 21
    .line 22
    .line 23
    move-result v4

    .line 24
    int-to-float v4, v4

    .line 25
    div-float/2addr v4, v3

    .line 26
    invoke-virtual {p1, v1, v2, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 27
    .line 28
    .line 29
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView$BackgroundView;->〇080(Landroid/graphics/Canvas;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

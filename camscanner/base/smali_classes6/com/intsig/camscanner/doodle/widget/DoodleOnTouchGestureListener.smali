.class public Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;
.super Lcom/intsig/camscanner/doodle/widget/TouchGestureDetector$OnTouchGestureListener;
.source "DoodleOnTouchGestureListener.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$ISelectionListener;
    }
.end annotation


# instance fields
.field private O0O:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

.field private O88O:F

.field private O8o08O8O:F

.field private OO:F

.field private OO〇00〇8oO:F

.field private Oo80:F

.field private O〇o88o08〇:F

.field private o0:F

.field private o8o:F

.field private o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

.field private o8〇OO0〇0o:F

.field private oOO〇〇:Landroid/animation/ValueAnimator;

.field private oOo0:F

.field private oOo〇8o008:F

.field private oo8ooo8O:F

.field private ooo0〇〇O:Landroid/graphics/Path;

.field private o〇00O:F

.field private o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

.field private 〇00O0:F

.field private 〇080OO8〇0:Ljava/lang/Float;

.field private 〇08O〇00〇o:F

.field private 〇08〇o0O:Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$ISelectionListener;

.field private 〇0O:Ljava/lang/Float;

.field private 〇8〇oO〇〇8o:F

.field private 〇OOo8〇0:F

.field private 〇O〇〇O8:Landroid/animation/ValueAnimator;

.field private 〇o0O:F

.field private 〇〇08O:Lcom/intsig/camscanner/doodle/widget/DoodlePath;

.field private 〇〇o〇:Z


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/doodle/widget/DoodleView;Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$ISelectionListener;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/widget/TouchGestureDetector$OnTouchGestureListener;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇〇o〇:Z

    .line 6
    .line 7
    const/high16 v0, 0x3f800000    # 1.0f

    .line 8
    .line 9
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇00O0:F

    .line 10
    .line 11
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 12
    .line 13
    sget-object v0, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->COPY:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->getCopyLocation()Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O0O:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/CopyLocation;->OO0o〇〇〇〇0()V

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O0O:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 25
    .line 26
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getBitmap()Landroid/graphics/Bitmap;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    int-to-float v1, v1

    .line 35
    const/high16 v2, 0x40000000    # 2.0f

    .line 36
    .line 37
    div-float/2addr v1, v2

    .line 38
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getBitmap()Landroid/graphics/Bitmap;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    int-to-float p1, p1

    .line 47
    div-float/2addr p1, v2

    .line 48
    invoke-virtual {v0, v1, p1}, Lcom/intsig/camscanner/doodle/widget/CopyLocation;->〇〇808〇(FF)V

    .line 49
    .line 50
    .line 51
    iput-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇08〇o0O:Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$ISelectionListener;

    .line 52
    .line 53
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method static bridge synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->oOo0:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic oO80(Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇o0O:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o〇0(Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;)Lcom/intsig/camscanner/doodle/widget/DoodleView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇80〇808〇O(Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->oOo〇8o008:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇8o8o〇(Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8o:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇O00(Lcom/intsig/camscanner/doodle/widget/DoodleRotatableItemBase;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)V
    .locals 5

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->O8()F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->Oo08()F

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 10
    .line 11
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    .line 12
    .line 13
    .line 14
    move-result v3

    .line 15
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    sub-float/2addr v2, v0

    .line 20
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 21
    .line 22
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    .line 23
    .line 24
    .line 25
    move-result p2

    .line 26
    invoke-virtual {v3, p2}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 27
    .line 28
    .line 29
    move-result p2

    .line 30
    sub-float/2addr p2, v1

    .line 31
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 32
    .line 33
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    .line 34
    .line 35
    .line 36
    move-result v4

    .line 37
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 38
    .line 39
    .line 40
    move-result v3

    .line 41
    sub-float/2addr v3, v0

    .line 42
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 43
    .line 44
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    .line 45
    .line 46
    .line 47
    move-result p3

    .line 48
    invoke-virtual {v0, p3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 49
    .line 50
    .line 51
    move-result p3

    .line 52
    sub-float/2addr p3, v1

    .line 53
    mul-float v2, v2, v2

    .line 54
    .line 55
    mul-float p2, p2, p2

    .line 56
    .line 57
    add-float/2addr v2, p2

    .line 58
    float-to-double v0, v2

    .line 59
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    .line 60
    .line 61
    .line 62
    move-result-wide v0

    .line 63
    double-to-float p2, v0

    .line 64
    mul-float v3, v3, v3

    .line 65
    .line 66
    mul-float p3, p3, p3

    .line 67
    .line 68
    add-float/2addr v3, p3

    .line 69
    float-to-double v0, v3

    .line 70
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    .line 71
    .line 72
    .line 73
    move-result-wide v0

    .line 74
    double-to-float p3, v0

    .line 75
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleRotatableItemBase;->o8oO〇()F

    .line 76
    .line 77
    .line 78
    move-result v0

    .line 79
    div-float/2addr p3, p2

    .line 80
    mul-float v0, v0, p3

    .line 81
    .line 82
    const/high16 p2, 0x3f000000    # 0.5f

    .line 83
    .line 84
    cmpg-float p3, v0, p2

    .line 85
    .line 86
    if-gez p3, :cond_0

    .line 87
    .line 88
    const/high16 v0, 0x3f000000    # 0.5f

    .line 89
    .line 90
    :cond_0
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->o〇0(F)V

    .line 91
    .line 92
    .line 93
    return-void
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method static bridge synthetic 〇O8o08O(Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->oo8ooo8O:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇〇808〇(Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sget-object v1, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->TEXT:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 8
    .line 9
    if-ne v0, v1, :cond_0

    .line 10
    .line 11
    if-eq p1, v1, :cond_1

    .line 12
    .line 13
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    sget-object v1, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->BITMAP:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 20
    .line 21
    if-ne v0, v1, :cond_2

    .line 22
    .line 23
    if-ne p1, v1, :cond_2

    .line 24
    .line 25
    :cond_1
    const/4 p1, 0x1

    .line 26
    goto :goto_0

    .line 27
    :cond_2
    const/4 p1, 0x0

    .line 28
    :goto_0
    return p1
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic 〇〇888(Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;)F
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O88O:F

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public O8(Lcom/intsig/camscanner/doodle/widget/ScaleGestureDetectorApi27;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8ooOoo〇()Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    if-nez p1, :cond_1

    .line 8
    .line 9
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0000OOO()Z

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    if-eqz p1, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->OO0o〇〇()V

    .line 19
    .line 20
    .line 21
    return-void

    .line 22
    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 23
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇O〇(Z)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public OO0o〇〇()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleScale()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x1

    .line 8
    const/high16 v2, 0x3f800000    # 1.0f

    .line 9
    .line 10
    cmpg-float v0, v0, v2

    .line 11
    .line 12
    if-gez v0, :cond_1

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇O〇〇O8:Landroid/animation/ValueAnimator;

    .line 15
    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    new-instance v0, Landroid/animation/ValueAnimator;

    .line 19
    .line 20
    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇O〇〇O8:Landroid/animation/ValueAnimator;

    .line 24
    .line 25
    const-wide/16 v3, 0x64

    .line 26
    .line 27
    invoke-virtual {v0, v3, v4}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 28
    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇O〇〇O8:Landroid/animation/ValueAnimator;

    .line 31
    .line 32
    new-instance v3, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$1;

    .line 33
    .line 34
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$1;-><init>(Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 38
    .line 39
    .line 40
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇O〇〇O8:Landroid/animation/ValueAnimator;

    .line 41
    .line 42
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 43
    .line 44
    .line 45
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 46
    .line 47
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleTranslationX()F

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇o0O:F

    .line 52
    .line 53
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 54
    .line 55
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleTranslationY()F

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O88O:F

    .line 60
    .line 61
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇O〇〇O8:Landroid/animation/ValueAnimator;

    .line 62
    .line 63
    const/4 v3, 0x2

    .line 64
    new-array v3, v3, [F

    .line 65
    .line 66
    iget-object v4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 67
    .line 68
    invoke-virtual {v4}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleScale()F

    .line 69
    .line 70
    .line 71
    move-result v4

    .line 72
    const/4 v5, 0x0

    .line 73
    aput v4, v3, v5

    .line 74
    .line 75
    aput v2, v3, v1

    .line 76
    .line 77
    invoke-virtual {v0, v3}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 78
    .line 79
    .line 80
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇O〇〇O8:Landroid/animation/ValueAnimator;

    .line 81
    .line 82
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 83
    .line 84
    .line 85
    goto :goto_0

    .line 86
    :cond_1
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇O〇(Z)V

    .line 87
    .line 88
    .line 89
    :goto_0
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public Oo08(Lcom/intsig/camscanner/doodle/widget/ScaleGestureDetectorApi27;)Z
    .locals 0

    .line 1
    const/4 p1, 0x0

    .line 2
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇080OO8〇0:Ljava/lang/Float;

    .line 3
    .line 4
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇0O:Ljava/lang/Float;

    .line 5
    .line 6
    const/4 p1, 0x1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public Oooo8o0〇()Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 4

    .line 1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇00O:F

    .line 6
    .line 7
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o0:F

    .line 8
    .line 9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O8o08O8O:F

    .line 14
    .line 15
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇OOo8〇0:F

    .line 16
    .line 17
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 18
    .line 19
    instance-of v0, p1, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;

    .line 20
    .line 21
    const/4 v1, 0x1

    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    check-cast p1, Lcom/intsig/camscanner/doodle/widget/DoodleRotatableItemBase;

    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 27
    .line 28
    iget v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o0:F

    .line 29
    .line 30
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 35
    .line 36
    iget v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇OOo8〇0:F

    .line 37
    .line 38
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 39
    .line 40
    .line 41
    move-result v2

    .line 42
    invoke-virtual {p1, v0, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleRotatableItemBase;->〇〇0o(FF)Z

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    const/4 v2, 0x0

    .line 47
    if-eqz v0, :cond_0

    .line 48
    .line 49
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleRotatableItemBase;->O000(Z)V

    .line 50
    .line 51
    .line 52
    return v2

    .line 53
    :cond_0
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleRotatableItemBase;->O000(Z)V

    .line 54
    .line 55
    .line 56
    :cond_1
    return v1
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 7

    .line 1
    iget p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o0:F

    .line 2
    .line 3
    iput p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->OO:F

    .line 4
    .line 5
    iget p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇OOo8〇0:F

    .line 6
    .line 7
    iput p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇08O〇00〇o:F

    .line 8
    .line 9
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    .line 10
    .line 11
    .line 12
    move-result p3

    .line 13
    iput p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o0:F

    .line 14
    .line 15
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    .line 16
    .line 17
    .line 18
    move-result p3

    .line 19
    iput p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇OOo8〇0:F

    .line 20
    .line 21
    iget-object p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 22
    .line 23
    invoke-virtual {p3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O〇8O8〇008()Z

    .line 24
    .line 25
    .line 26
    move-result p3

    .line 27
    if-eqz p3, :cond_0

    .line 28
    .line 29
    goto/16 :goto_3

    .line 30
    .line 31
    :cond_0
    iget-object p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 32
    .line 33
    invoke-virtual {p3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8ooOoo〇()Z

    .line 34
    .line 35
    .line 36
    move-result p3

    .line 37
    const/4 p4, 0x0

    .line 38
    if-nez p3, :cond_7

    .line 39
    .line 40
    iget-object p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 41
    .line 42
    invoke-virtual {p3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 43
    .line 44
    .line 45
    move-result-object p3

    .line 46
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇〇808〇(Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;)Z

    .line 47
    .line 48
    .line 49
    move-result p3

    .line 50
    if-nez p3, :cond_7

    .line 51
    .line 52
    iget-object p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 53
    .line 54
    invoke-virtual {p3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0000OOO()Z

    .line 55
    .line 56
    .line 57
    move-result p3

    .line 58
    if-eqz p3, :cond_1

    .line 59
    .line 60
    goto/16 :goto_1

    .line 61
    .line 62
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 63
    .line 64
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 65
    .line 66
    .line 67
    move-result-object p1

    .line 68
    sget-object p2, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->COPY:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 69
    .line 70
    if-ne p1, p2, :cond_2

    .line 71
    .line 72
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O0O:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 73
    .line 74
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/CopyLocation;->〇80〇808〇O()Z

    .line 75
    .line 76
    .line 77
    move-result p1

    .line 78
    if-eqz p1, :cond_2

    .line 79
    .line 80
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O0O:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 81
    .line 82
    iget-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 83
    .line 84
    iget p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o0:F

    .line 85
    .line 86
    invoke-virtual {p2, p3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 87
    .line 88
    .line 89
    move-result p2

    .line 90
    iget-object p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 91
    .line 92
    iget p4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇OOo8〇0:F

    .line 93
    .line 94
    invoke-virtual {p3, p4}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 95
    .line 96
    .line 97
    move-result p3

    .line 98
    invoke-virtual {p1, p2, p3}, Lcom/intsig/camscanner/doodle/widget/CopyLocation;->〇〇808〇(FF)V

    .line 99
    .line 100
    .line 101
    goto/16 :goto_3

    .line 102
    .line 103
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 104
    .line 105
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 106
    .line 107
    .line 108
    move-result-object p1

    .line 109
    if-ne p1, p2, :cond_3

    .line 110
    .line 111
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O0O:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 112
    .line 113
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/CopyLocation;->O8()F

    .line 114
    .line 115
    .line 116
    move-result p2

    .line 117
    iget-object p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 118
    .line 119
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o0:F

    .line 120
    .line 121
    invoke-virtual {p3, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 122
    .line 123
    .line 124
    move-result p3

    .line 125
    add-float/2addr p2, p3

    .line 126
    iget-object p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O0O:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 127
    .line 128
    invoke-virtual {p3}, Lcom/intsig/camscanner/doodle/widget/CopyLocation;->o〇0()F

    .line 129
    .line 130
    .line 131
    move-result p3

    .line 132
    sub-float/2addr p2, p3

    .line 133
    iget-object p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O0O:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 134
    .line 135
    invoke-virtual {p3}, Lcom/intsig/camscanner/doodle/widget/CopyLocation;->Oo08()F

    .line 136
    .line 137
    .line 138
    move-result p3

    .line 139
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 140
    .line 141
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇OOo8〇0:F

    .line 142
    .line 143
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 144
    .line 145
    .line 146
    move-result v0

    .line 147
    add-float/2addr p3, v0

    .line 148
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O0O:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 149
    .line 150
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/CopyLocation;->〇〇888()F

    .line 151
    .line 152
    .line 153
    move-result v0

    .line 154
    sub-float/2addr p3, v0

    .line 155
    invoke-virtual {p1, p2, p3}, Lcom/intsig/camscanner/doodle/widget/CopyLocation;->〇〇808〇(FF)V

    .line 156
    .line 157
    .line 158
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->ooo0〇〇O:Landroid/graphics/Path;

    .line 159
    .line 160
    if-eqz p1, :cond_6

    .line 161
    .line 162
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇〇08O:Lcom/intsig/camscanner/doodle/widget/DoodlePath;

    .line 163
    .line 164
    if-nez p1, :cond_4

    .line 165
    .line 166
    goto :goto_0

    .line 167
    :cond_4
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 168
    .line 169
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getShape()Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;

    .line 170
    .line 171
    .line 172
    move-result-object p1

    .line 173
    sget-object p2, Lcom/intsig/camscanner/doodle/widget/DoodleShape;->HAND_WRITE:Lcom/intsig/camscanner/doodle/widget/DoodleShape;

    .line 174
    .line 175
    if-ne p1, p2, :cond_5

    .line 176
    .line 177
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->ooo0〇〇O:Landroid/graphics/Path;

    .line 178
    .line 179
    iget-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 180
    .line 181
    iget p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->OO:F

    .line 182
    .line 183
    invoke-virtual {p2, p3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 184
    .line 185
    .line 186
    move-result p2

    .line 187
    iget-object p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 188
    .line 189
    iget p4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇08O〇00〇o:F

    .line 190
    .line 191
    invoke-virtual {p3, p4}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 192
    .line 193
    .line 194
    move-result p3

    .line 195
    iget-object p4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 196
    .line 197
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o0:F

    .line 198
    .line 199
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->OO:F

    .line 200
    .line 201
    add-float/2addr v0, v1

    .line 202
    const/high16 v1, 0x40000000    # 2.0f

    .line 203
    .line 204
    div-float/2addr v0, v1

    .line 205
    invoke-virtual {p4, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 206
    .line 207
    .line 208
    move-result p4

    .line 209
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 210
    .line 211
    iget v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇OOo8〇0:F

    .line 212
    .line 213
    iget v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇08O〇00〇o:F

    .line 214
    .line 215
    add-float/2addr v2, v3

    .line 216
    div-float/2addr v2, v1

    .line 217
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 218
    .line 219
    .line 220
    move-result v0

    .line 221
    invoke-virtual {p1, p2, p3, p4, v0}, Landroid/graphics/Path;->quadTo(FFFF)V

    .line 222
    .line 223
    .line 224
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇〇08O:Lcom/intsig/camscanner/doodle/widget/DoodlePath;

    .line 225
    .line 226
    iget-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->ooo0〇〇O:Landroid/graphics/Path;

    .line 227
    .line 228
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇〇o8(Landroid/graphics/Path;)V

    .line 229
    .line 230
    .line 231
    goto/16 :goto_3

    .line 232
    .line 233
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇〇08O:Lcom/intsig/camscanner/doodle/widget/DoodlePath;

    .line 234
    .line 235
    iget-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 236
    .line 237
    iget p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇00O:F

    .line 238
    .line 239
    invoke-virtual {p2, p3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 240
    .line 241
    .line 242
    move-result p2

    .line 243
    iget-object p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 244
    .line 245
    iget p4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O8o08O8O:F

    .line 246
    .line 247
    invoke-virtual {p3, p4}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 248
    .line 249
    .line 250
    move-result p3

    .line 251
    iget-object p4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 252
    .line 253
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o0:F

    .line 254
    .line 255
    invoke-virtual {p4, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 256
    .line 257
    .line 258
    move-result p4

    .line 259
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 260
    .line 261
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇OOo8〇0:F

    .line 262
    .line 263
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 264
    .line 265
    .line 266
    move-result v0

    .line 267
    invoke-virtual {p1, p2, p3, p4, v0}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o8O〇(FFFF)V

    .line 268
    .line 269
    .line 270
    goto/16 :goto_3

    .line 271
    .line 272
    :cond_6
    :goto_0
    return p4

    .line 273
    :cond_7
    :goto_1
    iget-object p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 274
    .line 275
    if-eqz p3, :cond_b

    .line 276
    .line 277
    instance-of v0, p3, Lcom/intsig/camscanner/doodle/widget/DoodleRotatableItemBase;

    .line 278
    .line 279
    if-eqz v0, :cond_a

    .line 280
    .line 281
    check-cast p3, Lcom/intsig/camscanner/doodle/widget/DoodleRotatableItemBase;

    .line 282
    .line 283
    invoke-virtual {p3}, Lcom/intsig/camscanner/doodle/widget/DoodleRotatableItemBase;->〇〇8O0〇8()Z

    .line 284
    .line 285
    .line 286
    move-result v0

    .line 287
    if-eqz v0, :cond_8

    .line 288
    .line 289
    return p4

    .line 290
    :cond_8
    invoke-virtual {p3}, Lcom/intsig/camscanner/doodle/widget/DoodleRotatableItemBase;->o〇O()Z

    .line 291
    .line 292
    .line 293
    move-result v0

    .line 294
    if-eqz v0, :cond_9

    .line 295
    .line 296
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 297
    .line 298
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇8〇oO〇〇8o:F

    .line 299
    .line 300
    invoke-interface {v0}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->O8()F

    .line 301
    .line 302
    .line 303
    move-result v2

    .line 304
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 305
    .line 306
    invoke-interface {v3}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->Oo08()F

    .line 307
    .line 308
    .line 309
    move-result v3

    .line 310
    iget-object v4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 311
    .line 312
    iget v5, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o0:F

    .line 313
    .line 314
    invoke-virtual {v4, v5}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 315
    .line 316
    .line 317
    move-result v4

    .line 318
    iget-object v5, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 319
    .line 320
    iget v6, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇OOo8〇0:F

    .line 321
    .line 322
    invoke-virtual {v5, v6}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 323
    .line 324
    .line 325
    move-result v5

    .line 326
    invoke-static {v2, v3, v4, v5}, Lcom/intsig/camscanner/doodle/util/DrawUtil;->〇080(FFFF)F

    .line 327
    .line 328
    .line 329
    move-result v2

    .line 330
    add-float/2addr v1, v2

    .line 331
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->〇o〇(F)V

    .line 332
    .line 333
    .line 334
    invoke-direct {p0, p3, p1, p2}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇O00(Lcom/intsig/camscanner/doodle/widget/DoodleRotatableItemBase;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)V

    .line 335
    .line 336
    .line 337
    goto :goto_2

    .line 338
    :cond_9
    invoke-virtual {p3}, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->〇o()Z

    .line 339
    .line 340
    .line 341
    move-result p4

    .line 342
    :cond_a
    :goto_2
    if-eqz p4, :cond_d

    .line 343
    .line 344
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 345
    .line 346
    iget p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->OO〇00〇8oO:F

    .line 347
    .line 348
    iget-object p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 349
    .line 350
    iget p4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o0:F

    .line 351
    .line 352
    invoke-virtual {p3, p4}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 353
    .line 354
    .line 355
    move-result p3

    .line 356
    add-float/2addr p2, p3

    .line 357
    iget-object p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 358
    .line 359
    iget p4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇00O:F

    .line 360
    .line 361
    invoke-virtual {p3, p4}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 362
    .line 363
    .line 364
    move-result p3

    .line 365
    sub-float/2addr p2, p3

    .line 366
    iget p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8〇OO0〇0o:F

    .line 367
    .line 368
    iget-object p4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 369
    .line 370
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇OOo8〇0:F

    .line 371
    .line 372
    invoke-virtual {p4, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 373
    .line 374
    .line 375
    move-result p4

    .line 376
    add-float/2addr p3, p4

    .line 377
    iget-object p4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 378
    .line 379
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O8o08O8O:F

    .line 380
    .line 381
    invoke-virtual {p4, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 382
    .line 383
    .line 384
    move-result p4

    .line 385
    sub-float/2addr p3, p4

    .line 386
    invoke-interface {p1, p2, p3}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->〇80〇808〇O(FF)V

    .line 387
    .line 388
    .line 389
    goto :goto_3

    .line 390
    :cond_b
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 391
    .line 392
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8ooOoo〇()Z

    .line 393
    .line 394
    .line 395
    move-result p1

    .line 396
    if-nez p1, :cond_c

    .line 397
    .line 398
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 399
    .line 400
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0000OOO()Z

    .line 401
    .line 402
    .line 403
    move-result p1

    .line 404
    if-eqz p1, :cond_d

    .line 405
    .line 406
    :cond_c
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 407
    .line 408
    iget p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->OO〇00〇8oO:F

    .line 409
    .line 410
    iget p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o0:F

    .line 411
    .line 412
    add-float/2addr p2, p3

    .line 413
    iget p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇00O:F

    .line 414
    .line 415
    sub-float/2addr p2, p3

    .line 416
    iget p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8〇OO0〇0o:F

    .line 417
    .line 418
    iget p4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇OOo8〇0:F

    .line 419
    .line 420
    add-float/2addr p3, p4

    .line 421
    iget p4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O8o08O8O:F

    .line 422
    .line 423
    sub-float/2addr p3, p4

    .line 424
    invoke-virtual {p1, p2, p3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->Oo8Oo00oo(FF)V

    .line 425
    .line 426
    .line 427
    :cond_d
    :goto_3
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 428
    .line 429
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->refresh()V

    .line 430
    .line 431
    .line 432
    const/4 p1, 0x1

    .line 433
    return p1
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
.end method

.method public onScrollEnd(Landroid/view/MotionEvent;)V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o0:F

    .line 2
    .line 3
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->OO:F

    .line 4
    .line 5
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇OOo8〇0:F

    .line 6
    .line 7
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇08O〇00〇o:F

    .line 8
    .line 9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o0:F

    .line 14
    .line 15
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇OOo8〇0:F

    .line 20
    .line 21
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 22
    .line 23
    const/4 v0, 0x0

    .line 24
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->setScrollingDoodle(Z)V

    .line 25
    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 28
    .line 29
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O〇8O8〇008()Z

    .line 30
    .line 31
    .line 32
    move-result p1

    .line 33
    if-eqz p1, :cond_0

    .line 34
    .line 35
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 36
    .line 37
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDropperTouchListener()Lcom/intsig/camscanner/doodle/widget/core/IDropperTouchListener;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    if-eqz p1, :cond_4

    .line 42
    .line 43
    invoke-interface {p1}, Lcom/intsig/camscanner/doodle/widget/core/IDropperTouchListener;->〇o00〇〇Oo()V

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 48
    .line 49
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8ooOoo〇()Z

    .line 50
    .line 51
    .line 52
    move-result p1

    .line 53
    if-nez p1, :cond_1

    .line 54
    .line 55
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 56
    .line 57
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇〇808〇(Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;)Z

    .line 62
    .line 63
    .line 64
    move-result p1

    .line 65
    if-nez p1, :cond_1

    .line 66
    .line 67
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 68
    .line 69
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0000OOO()Z

    .line 70
    .line 71
    .line 72
    move-result p1

    .line 73
    if-eqz p1, :cond_4

    .line 74
    .line 75
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 76
    .line 77
    instance-of v1, p1, Lcom/intsig/camscanner/doodle/widget/DoodleRotatableItemBase;

    .line 78
    .line 79
    if-eqz v1, :cond_2

    .line 80
    .line 81
    check-cast p1, Lcom/intsig/camscanner/doodle/widget/DoodleRotatableItemBase;

    .line 82
    .line 83
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleRotatableItemBase;->〇80(Z)V

    .line 84
    .line 85
    .line 86
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 87
    .line 88
    check-cast p1, Lcom/intsig/camscanner/doodle/widget/DoodleRotatableItemBase;

    .line 89
    .line 90
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleRotatableItemBase;->O000(Z)V

    .line 91
    .line 92
    .line 93
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 94
    .line 95
    check-cast p1, Lcom/intsig/camscanner/doodle/widget/DoodleRotatableItemBase;

    .line 96
    .line 97
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->〇〇〇0〇〇0(Z)V

    .line 98
    .line 99
    .line 100
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 101
    .line 102
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8ooOoo〇()Z

    .line 103
    .line 104
    .line 105
    move-result p1

    .line 106
    if-nez p1, :cond_3

    .line 107
    .line 108
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 109
    .line 110
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0000OOO()Z

    .line 111
    .line 112
    .line 113
    move-result p1

    .line 114
    if-eqz p1, :cond_4

    .line 115
    .line 116
    :cond_3
    const/4 p1, 0x1

    .line 117
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇O〇(Z)V

    .line 118
    .line 119
    .line 120
    :cond_4
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇〇08O:Lcom/intsig/camscanner/doodle/widget/DoodlePath;

    .line 121
    .line 122
    if-eqz p1, :cond_7

    .line 123
    .line 124
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 125
    .line 126
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 127
    .line 128
    .line 129
    move-result-object p1

    .line 130
    sget-object v0, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->BRUSH:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 131
    .line 132
    if-ne p1, v0, :cond_5

    .line 133
    .line 134
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇〇08O:Lcom/intsig/camscanner/doodle/widget/DoodlePath;

    .line 135
    .line 136
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getColor()Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;

    .line 137
    .line 138
    .line 139
    move-result-object p1

    .line 140
    check-cast p1, Lcom/intsig/camscanner/doodle/widget/DoodleColor;

    .line 141
    .line 142
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 143
    .line 144
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getColor()Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;

    .line 145
    .line 146
    .line 147
    move-result-object v0

    .line 148
    invoke-interface {v0}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;->〇o00〇〇Oo()I

    .line 149
    .line 150
    .line 151
    move-result v0

    .line 152
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->〇080(I)V

    .line 153
    .line 154
    .line 155
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 156
    .line 157
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇oOO8O8()Z

    .line 158
    .line 159
    .line 160
    move-result p1

    .line 161
    if-eqz p1, :cond_6

    .line 162
    .line 163
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 164
    .line 165
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇〇08O:Lcom/intsig/camscanner/doodle/widget/DoodlePath;

    .line 166
    .line 167
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OOO〇O0(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;)V

    .line 168
    .line 169
    .line 170
    :cond_6
    const/4 p1, 0x0

    .line 171
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇〇08O:Lcom/intsig/camscanner/doodle/widget/DoodlePath;

    .line 172
    .line 173
    :cond_7
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 174
    .line 175
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->refresh()V

    .line 176
    .line 177
    .line 178
    return-void
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 8

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o0:F

    .line 2
    .line 3
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->OO:F

    .line 4
    .line 5
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇OOo8〇0:F

    .line 6
    .line 7
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇08O〇00〇o:F

    .line 8
    .line 9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o0:F

    .line 14
    .line 15
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇OOo8〇0:F

    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O〇8O8〇008()Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    const/4 v1, 0x1

    .line 28
    if-eqz v0, :cond_0

    .line 29
    .line 30
    goto/16 :goto_3

    .line 31
    .line 32
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 33
    .line 34
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0000OOO()Z

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    const/4 v2, 0x0

    .line 39
    const/4 v3, 0x0

    .line 40
    if-eqz v0, :cond_2

    .line 41
    .line 42
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 43
    .line 44
    if-eqz p1, :cond_1

    .line 45
    .line 46
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇08〇o0O:Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$ISelectionListener;

    .line 47
    .line 48
    if-eqz v0, :cond_1

    .line 49
    .line 50
    invoke-interface {p1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;->〇〇8O0〇8()Z

    .line 51
    .line 52
    .line 53
    move-result p1

    .line 54
    if-eqz p1, :cond_1

    .line 55
    .line 56
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇08〇o0O:Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$ISelectionListener;

    .line 57
    .line 58
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 59
    .line 60
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$ISelectionListener;->〇o〇(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;)V

    .line 61
    .line 62
    .line 63
    return v1

    .line 64
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 65
    .line 66
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllItem()Ljava/util/List;

    .line 67
    .line 68
    .line 69
    move-result-object p1

    .line 70
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    sub-int/2addr v0, v1

    .line 75
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 76
    .line 77
    .line 78
    move-result-object p1

    .line 79
    check-cast p1, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;

    .line 80
    .line 81
    instance-of v0, p1, Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 82
    .line 83
    if-eqz v0, :cond_b

    .line 84
    .line 85
    check-cast p1, Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 86
    .line 87
    invoke-interface {p1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    sget-object v4, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->RECTANGLE:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 92
    .line 93
    if-ne v0, v4, :cond_b

    .line 94
    .line 95
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 96
    .line 97
    iget v4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o0:F

    .line 98
    .line 99
    invoke-virtual {v0, v4}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 100
    .line 101
    .line 102
    move-result v0

    .line 103
    iget-object v4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 104
    .line 105
    iget v5, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇OOo8〇0:F

    .line 106
    .line 107
    invoke-virtual {v4, v5}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 108
    .line 109
    .line 110
    move-result v4

    .line 111
    invoke-interface {p1, v0, v4}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;->OO0o〇〇〇〇0(FF)Z

    .line 112
    .line 113
    .line 114
    move-result p1

    .line 115
    if-nez p1, :cond_b

    .line 116
    .line 117
    invoke-virtual {p0, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇〇8O0〇8(Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;)V

    .line 118
    .line 119
    .line 120
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 121
    .line 122
    invoke-virtual {p1, v3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->setRectangleMode(Z)V

    .line 123
    .line 124
    .line 125
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 126
    .line 127
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->refresh()V

    .line 128
    .line 129
    .line 130
    return v1

    .line 131
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 132
    .line 133
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8ooOoo〇()Z

    .line 134
    .line 135
    .line 136
    move-result v0

    .line 137
    if-eqz v0, :cond_9

    .line 138
    .line 139
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 140
    .line 141
    if-eqz p1, :cond_3

    .line 142
    .line 143
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇08〇o0O:Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$ISelectionListener;

    .line 144
    .line 145
    if-eqz v0, :cond_3

    .line 146
    .line 147
    invoke-interface {p1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;->〇〇8O0〇8()Z

    .line 148
    .line 149
    .line 150
    move-result p1

    .line 151
    if-eqz p1, :cond_3

    .line 152
    .line 153
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇08〇o0O:Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$ISelectionListener;

    .line 154
    .line 155
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 156
    .line 157
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$ISelectionListener;->〇o〇(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;)V

    .line 158
    .line 159
    .line 160
    return v1

    .line 161
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 162
    .line 163
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllItem()Ljava/util/List;

    .line 164
    .line 165
    .line 166
    move-result-object p1

    .line 167
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 168
    .line 169
    .line 170
    move-result v0

    .line 171
    sub-int/2addr v0, v1

    .line 172
    :goto_0
    if-ltz v0, :cond_7

    .line 173
    .line 174
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 175
    .line 176
    .line 177
    move-result-object v4

    .line 178
    check-cast v4, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;

    .line 179
    .line 180
    invoke-interface {v4}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->〇O8o08O()Z

    .line 181
    .line 182
    .line 183
    move-result v5

    .line 184
    if-nez v5, :cond_4

    .line 185
    .line 186
    goto :goto_1

    .line 187
    :cond_4
    instance-of v5, v4, Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 188
    .line 189
    if-nez v5, :cond_5

    .line 190
    .line 191
    goto :goto_1

    .line 192
    :cond_5
    check-cast v4, Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 193
    .line 194
    iget-object v5, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 195
    .line 196
    iget v6, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o0:F

    .line 197
    .line 198
    invoke-virtual {v5, v6}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 199
    .line 200
    .line 201
    move-result v5

    .line 202
    iget-object v6, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 203
    .line 204
    iget v7, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇OOo8〇0:F

    .line 205
    .line 206
    invoke-virtual {v6, v7}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 207
    .line 208
    .line 209
    move-result v6

    .line 210
    invoke-interface {v4, v5, v6}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;->OO0o〇〇〇〇0(FF)Z

    .line 211
    .line 212
    .line 213
    move-result v5

    .line 214
    if-eqz v5, :cond_6

    .line 215
    .line 216
    invoke-virtual {p0, v4}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇〇8O0〇8(Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;)V

    .line 217
    .line 218
    .line 219
    invoke-interface {v4}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->getLocation()Landroid/graphics/PointF;

    .line 220
    .line 221
    .line 222
    move-result-object p1

    .line 223
    iget v0, p1, Landroid/graphics/PointF;->x:F

    .line 224
    .line 225
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->OO〇00〇8oO:F

    .line 226
    .line 227
    iget p1, p1, Landroid/graphics/PointF;->y:F

    .line 228
    .line 229
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8〇OO0〇0o:F

    .line 230
    .line 231
    const/4 v3, 0x1

    .line 232
    goto :goto_2

    .line 233
    :cond_6
    :goto_1
    add-int/lit8 v0, v0, -0x1

    .line 234
    .line 235
    goto :goto_0

    .line 236
    :cond_7
    :goto_2
    if-nez v3, :cond_b

    .line 237
    .line 238
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 239
    .line 240
    if-eqz p1, :cond_8

    .line 241
    .line 242
    invoke-virtual {p0, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇〇8O0〇8(Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;)V

    .line 243
    .line 244
    .line 245
    goto :goto_3

    .line 246
    :cond_8
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇08〇o0O:Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$ISelectionListener;

    .line 247
    .line 248
    if-eqz p1, :cond_b

    .line 249
    .line 250
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 251
    .line 252
    iget v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o0:F

    .line 253
    .line 254
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 255
    .line 256
    .line 257
    move-result v2

    .line 258
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 259
    .line 260
    iget v4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇OOo8〇0:F

    .line 261
    .line 262
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 263
    .line 264
    .line 265
    move-result v3

    .line 266
    invoke-interface {p1, v0, v2, v3}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$ISelectionListener;->〇080(Lcom/intsig/camscanner/doodle/widget/core/IDoodle;FF)V

    .line 267
    .line 268
    .line 269
    goto :goto_3

    .line 270
    :cond_9
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 271
    .line 272
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 273
    .line 274
    .line 275
    move-result-object v0

    .line 276
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇〇808〇(Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;)Z

    .line 277
    .line 278
    .line 279
    move-result v0

    .line 280
    if-eqz v0, :cond_a

    .line 281
    .line 282
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇08〇o0O:Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$ISelectionListener;

    .line 283
    .line 284
    if-eqz p1, :cond_b

    .line 285
    .line 286
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 287
    .line 288
    iget v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o0:F

    .line 289
    .line 290
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 291
    .line 292
    .line 293
    move-result v2

    .line 294
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 295
    .line 296
    iget v4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇OOo8〇0:F

    .line 297
    .line 298
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 299
    .line 300
    .line 301
    move-result v3

    .line 302
    invoke-interface {p1, v0, v2, v3}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$ISelectionListener;->〇080(Lcom/intsig/camscanner/doodle/widget/core/IDoodle;FF)V

    .line 303
    .line 304
    .line 305
    goto :goto_3

    .line 306
    :cond_a
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇o〇(Landroid/view/MotionEvent;)V

    .line 307
    .line 308
    .line 309
    const/high16 v0, 0x3f800000    # 1.0f

    .line 310
    .line 311
    invoke-virtual {p1, v0, v0}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    .line 312
    .line 313
    .line 314
    invoke-virtual {p0, p1, p1, v0, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    .line 315
    .line 316
    .line 317
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->onScrollEnd(Landroid/view/MotionEvent;)V

    .line 318
    .line 319
    .line 320
    :cond_b
    :goto_3
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 321
    .line 322
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->refresh()V

    .line 323
    .line 324
    .line 325
    return v1
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public 〇080(Lcom/intsig/camscanner/doodle/widget/ScaleGestureDetectorApi27;)Z
    .locals 5

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/ScaleGestureDetectorApi27;->O8()F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->oOo〇8o008:F

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/ScaleGestureDetectorApi27;->Oo08()F

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->oOo0:F

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇080OO8〇0:Ljava/lang/Float;

    .line 14
    .line 15
    const/high16 v1, 0x3f800000    # 1.0f

    .line 16
    .line 17
    if-eqz v0, :cond_4

    .line 18
    .line 19
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇0O:Ljava/lang/Float;

    .line 20
    .line 21
    if-eqz v2, :cond_4

    .line 22
    .line 23
    iget v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->oOo〇8o008:F

    .line 24
    .line 25
    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    sub-float/2addr v2, v0

    .line 30
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->oOo0:F

    .line 31
    .line 32
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇0O:Ljava/lang/Float;

    .line 33
    .line 34
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    .line 35
    .line 36
    .line 37
    move-result v3

    .line 38
    sub-float/2addr v0, v3

    .line 39
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    .line 40
    .line 41
    .line 42
    move-result v3

    .line 43
    cmpl-float v3, v3, v1

    .line 44
    .line 45
    if-gtz v3, :cond_1

    .line 46
    .line 47
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    .line 48
    .line 49
    .line 50
    move-result v3

    .line 51
    cmpl-float v3, v3, v1

    .line 52
    .line 53
    if-lez v3, :cond_0

    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_0
    iget v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->Oo80:F

    .line 57
    .line 58
    add-float/2addr v3, v2

    .line 59
    iput v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->Oo80:F

    .line 60
    .line 61
    iget v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O〇o88o08〇:F

    .line 62
    .line 63
    add-float/2addr v2, v0

    .line 64
    iput v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O〇o88o08〇:F

    .line 65
    .line 66
    goto :goto_1

    .line 67
    :cond_1
    :goto_0
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 68
    .line 69
    if-eqz v3, :cond_2

    .line 70
    .line 71
    iget-boolean v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇〇o〇:Z

    .line 72
    .line 73
    if-nez v3, :cond_3

    .line 74
    .line 75
    :cond_2
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 76
    .line 77
    invoke-virtual {v3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleTranslationX()F

    .line 78
    .line 79
    .line 80
    move-result v4

    .line 81
    add-float/2addr v4, v2

    .line 82
    iget v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->Oo80:F

    .line 83
    .line 84
    add-float/2addr v4, v2

    .line 85
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->setDoodleTranslationX(F)V

    .line 86
    .line 87
    .line 88
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 89
    .line 90
    invoke-virtual {v2}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleTranslationY()F

    .line 91
    .line 92
    .line 93
    move-result v3

    .line 94
    add-float/2addr v3, v0

    .line 95
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O〇o88o08〇:F

    .line 96
    .line 97
    add-float/2addr v3, v0

    .line 98
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->setDoodleTranslationY(F)V

    .line 99
    .line 100
    .line 101
    :cond_3
    const/4 v0, 0x0

    .line 102
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O〇o88o08〇:F

    .line 103
    .line 104
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->Oo80:F

    .line 105
    .line 106
    :cond_4
    :goto_1
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/ScaleGestureDetectorApi27;->o〇0()F

    .line 107
    .line 108
    .line 109
    move-result v0

    .line 110
    sub-float v0, v1, v0

    .line 111
    .line 112
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    .line 113
    .line 114
    .line 115
    move-result v0

    .line 116
    const v2, 0x3ba3d70a    # 0.005f

    .line 117
    .line 118
    .line 119
    cmpl-float v0, v0, v2

    .line 120
    .line 121
    if-lez v0, :cond_7

    .line 122
    .line 123
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 124
    .line 125
    if-eqz v0, :cond_6

    .line 126
    .line 127
    iget-boolean v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇〇o〇:Z

    .line 128
    .line 129
    if-nez v2, :cond_5

    .line 130
    .line 131
    goto :goto_2

    .line 132
    :cond_5
    invoke-interface {v0}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->getScale()F

    .line 133
    .line 134
    .line 135
    move-result v2

    .line 136
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/ScaleGestureDetectorApi27;->o〇0()F

    .line 137
    .line 138
    .line 139
    move-result p1

    .line 140
    mul-float v2, v2, p1

    .line 141
    .line 142
    iget p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇00O0:F

    .line 143
    .line 144
    mul-float v2, v2, p1

    .line 145
    .line 146
    invoke-interface {v0, v2}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->o〇0(F)V

    .line 147
    .line 148
    .line 149
    goto :goto_3

    .line 150
    :cond_6
    :goto_2
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 151
    .line 152
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleScale()F

    .line 153
    .line 154
    .line 155
    move-result v0

    .line 156
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/ScaleGestureDetectorApi27;->o〇0()F

    .line 157
    .line 158
    .line 159
    move-result p1

    .line 160
    mul-float v0, v0, p1

    .line 161
    .line 162
    iget p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇00O0:F

    .line 163
    .line 164
    mul-float v0, v0, p1

    .line 165
    .line 166
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 167
    .line 168
    iget v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->oOo〇8o008:F

    .line 169
    .line 170
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 171
    .line 172
    .line 173
    move-result v2

    .line 174
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 175
    .line 176
    iget v4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->oOo0:F

    .line 177
    .line 178
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 179
    .line 180
    .line 181
    move-result v3

    .line 182
    invoke-virtual {p1, v0, v2, v3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8(FFF)V

    .line 183
    .line 184
    .line 185
    :goto_3
    iput v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇00O0:F

    .line 186
    .line 187
    goto :goto_4

    .line 188
    :cond_7
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇00O0:F

    .line 189
    .line 190
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/ScaleGestureDetectorApi27;->o〇0()F

    .line 191
    .line 192
    .line 193
    move-result p1

    .line 194
    mul-float v0, v0, p1

    .line 195
    .line 196
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇00O0:F

    .line 197
    .line 198
    :goto_4
    iget p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->oOo〇8o008:F

    .line 199
    .line 200
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 201
    .line 202
    .line 203
    move-result-object p1

    .line 204
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇080OO8〇0:Ljava/lang/Float;

    .line 205
    .line 206
    iget p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->oOo0:F

    .line 207
    .line 208
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 209
    .line 210
    .line 211
    move-result-object p1

    .line 212
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇0O:Ljava/lang/Float;

    .line 213
    .line 214
    const/4 p1, 0x1

    .line 215
    return p1
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public 〇O〇(Z)V
    .locals 14

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleRotation()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/16 v1, 0x5a

    .line 8
    .line 9
    rem-int/2addr v0, v1

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleTranslationX()F

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 20
    .line 21
    invoke-virtual {v2}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleTranslationY()F

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 26
    .line 27
    invoke-virtual {v3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleBound()Landroid/graphics/RectF;

    .line 28
    .line 29
    .line 30
    move-result-object v3

    .line 31
    iget-object v4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 32
    .line 33
    invoke-virtual {v4}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleTranslationX()F

    .line 34
    .line 35
    .line 36
    move-result v4

    .line 37
    iget-object v5, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 38
    .line 39
    invoke-virtual {v5}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleTranslationY()F

    .line 40
    .line 41
    .line 42
    move-result v5

    .line 43
    iget-object v6, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 44
    .line 45
    invoke-virtual {v6}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getCenterWidth()I

    .line 46
    .line 47
    .line 48
    move-result v6

    .line 49
    int-to-float v6, v6

    .line 50
    iget-object v7, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 51
    .line 52
    invoke-virtual {v7}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getRotateScale()F

    .line 53
    .line 54
    .line 55
    move-result v7

    .line 56
    mul-float v6, v6, v7

    .line 57
    .line 58
    iget-object v7, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 59
    .line 60
    invoke-virtual {v7}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getCenterHeight()I

    .line 61
    .line 62
    .line 63
    move-result v7

    .line 64
    int-to-float v7, v7

    .line 65
    iget-object v8, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 66
    .line 67
    invoke-virtual {v8}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getRotateScale()F

    .line 68
    .line 69
    .line 70
    move-result v8

    .line 71
    mul-float v7, v7, v8

    .line 72
    .line 73
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    .line 74
    .line 75
    .line 76
    move-result v8

    .line 77
    iget-object v9, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 78
    .line 79
    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    .line 80
    .line 81
    .line 82
    move-result v9

    .line 83
    int-to-float v9, v9

    .line 84
    const/high16 v10, 0x40000000    # 2.0f

    .line 85
    .line 86
    const/4 v11, 0x0

    .line 87
    const/16 v12, 0xb4

    .line 88
    .line 89
    cmpg-float v8, v8, v9

    .line 90
    .line 91
    if-gtz v8, :cond_3

    .line 92
    .line 93
    iget-object v8, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 94
    .line 95
    invoke-virtual {v8}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleRotation()I

    .line 96
    .line 97
    .line 98
    move-result v8

    .line 99
    if-eqz v8, :cond_2

    .line 100
    .line 101
    iget-object v8, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 102
    .line 103
    invoke-virtual {v8}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleRotation()I

    .line 104
    .line 105
    .line 106
    move-result v8

    .line 107
    if-ne v8, v12, :cond_1

    .line 108
    .line 109
    goto :goto_0

    .line 110
    :cond_1
    iget-object v4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 111
    .line 112
    invoke-virtual {v4}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleScale()F

    .line 113
    .line 114
    .line 115
    move-result v4

    .line 116
    mul-float v4, v4, v6

    .line 117
    .line 118
    sub-float v4, v6, v4

    .line 119
    .line 120
    div-float/2addr v4, v10

    .line 121
    goto/16 :goto_5

    .line 122
    .line 123
    :cond_2
    :goto_0
    iget-object v5, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 124
    .line 125
    invoke-virtual {v5}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleScale()F

    .line 126
    .line 127
    .line 128
    move-result v5

    .line 129
    mul-float v5, v5, v7

    .line 130
    .line 131
    sub-float v5, v7, v5

    .line 132
    .line 133
    div-float/2addr v5, v10

    .line 134
    goto/16 :goto_5

    .line 135
    .line 136
    :cond_3
    iget v8, v3, Landroid/graphics/RectF;->top:F

    .line 137
    .line 138
    cmpl-float v9, v8, v11

    .line 139
    .line 140
    if-lez v9, :cond_a

    .line 141
    .line 142
    iget v9, v3, Landroid/graphics/RectF;->bottom:F

    .line 143
    .line 144
    iget-object v13, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 145
    .line 146
    invoke-virtual {v13}, Landroid/view/View;->getHeight()I

    .line 147
    .line 148
    .line 149
    move-result v13

    .line 150
    int-to-float v13, v13

    .line 151
    cmpl-float v9, v9, v13

    .line 152
    .line 153
    if-ltz v9, :cond_a

    .line 154
    .line 155
    iget-object v9, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 156
    .line 157
    invoke-virtual {v9}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleRotation()I

    .line 158
    .line 159
    .line 160
    move-result v9

    .line 161
    if-eqz v9, :cond_7

    .line 162
    .line 163
    iget-object v9, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 164
    .line 165
    invoke-virtual {v9}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleRotation()I

    .line 166
    .line 167
    .line 168
    move-result v9

    .line 169
    if-ne v9, v12, :cond_4

    .line 170
    .line 171
    goto :goto_2

    .line 172
    :cond_4
    iget-object v9, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 173
    .line 174
    invoke-virtual {v9}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleRotation()I

    .line 175
    .line 176
    .line 177
    move-result v9

    .line 178
    if-ne v9, v1, :cond_6

    .line 179
    .line 180
    :cond_5
    sub-float/2addr v4, v8

    .line 181
    goto :goto_5

    .line 182
    :cond_6
    :goto_1
    add-float/2addr v4, v8

    .line 183
    goto :goto_5

    .line 184
    :cond_7
    :goto_2
    iget-object v9, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 185
    .line 186
    invoke-virtual {v9}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleRotation()I

    .line 187
    .line 188
    .line 189
    move-result v9

    .line 190
    if-nez v9, :cond_9

    .line 191
    .line 192
    :cond_8
    sub-float/2addr v5, v8

    .line 193
    goto :goto_5

    .line 194
    :cond_9
    :goto_3
    add-float/2addr v5, v8

    .line 195
    goto :goto_5

    .line 196
    :cond_a
    iget v8, v3, Landroid/graphics/RectF;->bottom:F

    .line 197
    .line 198
    iget-object v9, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 199
    .line 200
    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    .line 201
    .line 202
    .line 203
    move-result v9

    .line 204
    int-to-float v9, v9

    .line 205
    cmpg-float v8, v8, v9

    .line 206
    .line 207
    if-gez v8, :cond_d

    .line 208
    .line 209
    iget v8, v3, Landroid/graphics/RectF;->top:F

    .line 210
    .line 211
    cmpg-float v8, v8, v11

    .line 212
    .line 213
    if-gtz v8, :cond_d

    .line 214
    .line 215
    iget-object v8, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 216
    .line 217
    invoke-virtual {v8}, Landroid/view/View;->getHeight()I

    .line 218
    .line 219
    .line 220
    move-result v8

    .line 221
    int-to-float v8, v8

    .line 222
    iget v9, v3, Landroid/graphics/RectF;->bottom:F

    .line 223
    .line 224
    sub-float/2addr v8, v9

    .line 225
    iget-object v9, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 226
    .line 227
    invoke-virtual {v9}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleRotation()I

    .line 228
    .line 229
    .line 230
    move-result v9

    .line 231
    if-eqz v9, :cond_c

    .line 232
    .line 233
    iget-object v9, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 234
    .line 235
    invoke-virtual {v9}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleRotation()I

    .line 236
    .line 237
    .line 238
    move-result v9

    .line 239
    if-ne v9, v12, :cond_b

    .line 240
    .line 241
    goto :goto_4

    .line 242
    :cond_b
    iget-object v9, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 243
    .line 244
    invoke-virtual {v9}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleRotation()I

    .line 245
    .line 246
    .line 247
    move-result v9

    .line 248
    if-ne v9, v1, :cond_5

    .line 249
    .line 250
    goto :goto_1

    .line 251
    :cond_c
    :goto_4
    iget-object v9, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 252
    .line 253
    invoke-virtual {v9}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleRotation()I

    .line 254
    .line 255
    .line 256
    move-result v9

    .line 257
    if-nez v9, :cond_8

    .line 258
    .line 259
    goto :goto_3

    .line 260
    :cond_d
    :goto_5
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    .line 261
    .line 262
    .line 263
    move-result v8

    .line 264
    iget-object v9, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 265
    .line 266
    invoke-virtual {v9}, Landroid/view/View;->getWidth()I

    .line 267
    .line 268
    .line 269
    move-result v9

    .line 270
    int-to-float v9, v9

    .line 271
    cmpg-float v8, v8, v9

    .line 272
    .line 273
    if-gtz v8, :cond_10

    .line 274
    .line 275
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 276
    .line 277
    invoke-virtual {v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleRotation()I

    .line 278
    .line 279
    .line 280
    move-result v1

    .line 281
    if-eqz v1, :cond_f

    .line 282
    .line 283
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 284
    .line 285
    invoke-virtual {v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleRotation()I

    .line 286
    .line 287
    .line 288
    move-result v1

    .line 289
    if-ne v1, v12, :cond_e

    .line 290
    .line 291
    goto :goto_6

    .line 292
    :cond_e
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 293
    .line 294
    invoke-virtual {v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleScale()F

    .line 295
    .line 296
    .line 297
    move-result v1

    .line 298
    mul-float v1, v1, v7

    .line 299
    .line 300
    sub-float/2addr v7, v1

    .line 301
    div-float v5, v7, v10

    .line 302
    .line 303
    goto/16 :goto_b

    .line 304
    .line 305
    :cond_f
    :goto_6
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 306
    .line 307
    invoke-virtual {v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleScale()F

    .line 308
    .line 309
    .line 310
    move-result v1

    .line 311
    mul-float v1, v1, v6

    .line 312
    .line 313
    sub-float/2addr v6, v1

    .line 314
    div-float v4, v6, v10

    .line 315
    .line 316
    goto/16 :goto_b

    .line 317
    .line 318
    :cond_10
    iget v6, v3, Landroid/graphics/RectF;->left:F

    .line 319
    .line 320
    cmpl-float v7, v6, v11

    .line 321
    .line 322
    if-lez v7, :cond_17

    .line 323
    .line 324
    iget v7, v3, Landroid/graphics/RectF;->right:F

    .line 325
    .line 326
    iget-object v8, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 327
    .line 328
    invoke-virtual {v8}, Landroid/view/View;->getWidth()I

    .line 329
    .line 330
    .line 331
    move-result v8

    .line 332
    int-to-float v8, v8

    .line 333
    cmpl-float v7, v7, v8

    .line 334
    .line 335
    if-ltz v7, :cond_17

    .line 336
    .line 337
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 338
    .line 339
    invoke-virtual {v3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleRotation()I

    .line 340
    .line 341
    .line 342
    move-result v3

    .line 343
    if-eqz v3, :cond_14

    .line 344
    .line 345
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 346
    .line 347
    invoke-virtual {v3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleRotation()I

    .line 348
    .line 349
    .line 350
    move-result v3

    .line 351
    if-ne v3, v12, :cond_11

    .line 352
    .line 353
    goto :goto_8

    .line 354
    :cond_11
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 355
    .line 356
    invoke-virtual {v3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleRotation()I

    .line 357
    .line 358
    .line 359
    move-result v3

    .line 360
    if-ne v3, v1, :cond_13

    .line 361
    .line 362
    :cond_12
    add-float/2addr v5, v6

    .line 363
    goto :goto_b

    .line 364
    :cond_13
    :goto_7
    sub-float/2addr v5, v6

    .line 365
    goto :goto_b

    .line 366
    :cond_14
    :goto_8
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 367
    .line 368
    invoke-virtual {v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleRotation()I

    .line 369
    .line 370
    .line 371
    move-result v1

    .line 372
    if-nez v1, :cond_16

    .line 373
    .line 374
    :cond_15
    sub-float/2addr v4, v6

    .line 375
    goto :goto_b

    .line 376
    :cond_16
    :goto_9
    add-float/2addr v4, v6

    .line 377
    goto :goto_b

    .line 378
    :cond_17
    iget v6, v3, Landroid/graphics/RectF;->right:F

    .line 379
    .line 380
    iget-object v7, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 381
    .line 382
    invoke-virtual {v7}, Landroid/view/View;->getWidth()I

    .line 383
    .line 384
    .line 385
    move-result v7

    .line 386
    int-to-float v7, v7

    .line 387
    cmpg-float v6, v6, v7

    .line 388
    .line 389
    if-gez v6, :cond_1a

    .line 390
    .line 391
    iget v6, v3, Landroid/graphics/RectF;->left:F

    .line 392
    .line 393
    cmpg-float v6, v6, v11

    .line 394
    .line 395
    if-gtz v6, :cond_1a

    .line 396
    .line 397
    iget-object v6, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 398
    .line 399
    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    .line 400
    .line 401
    .line 402
    move-result v6

    .line 403
    int-to-float v6, v6

    .line 404
    iget v3, v3, Landroid/graphics/RectF;->right:F

    .line 405
    .line 406
    sub-float/2addr v6, v3

    .line 407
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 408
    .line 409
    invoke-virtual {v3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleRotation()I

    .line 410
    .line 411
    .line 412
    move-result v3

    .line 413
    if-eqz v3, :cond_19

    .line 414
    .line 415
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 416
    .line 417
    invoke-virtual {v3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleRotation()I

    .line 418
    .line 419
    .line 420
    move-result v3

    .line 421
    if-ne v3, v12, :cond_18

    .line 422
    .line 423
    goto :goto_a

    .line 424
    :cond_18
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 425
    .line 426
    invoke-virtual {v3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleRotation()I

    .line 427
    .line 428
    .line 429
    move-result v3

    .line 430
    if-ne v3, v1, :cond_12

    .line 431
    .line 432
    goto :goto_7

    .line 433
    :cond_19
    :goto_a
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 434
    .line 435
    invoke-virtual {v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleRotation()I

    .line 436
    .line 437
    .line 438
    move-result v1

    .line 439
    if-nez v1, :cond_15

    .line 440
    .line 441
    goto :goto_9

    .line 442
    :cond_1a
    :goto_b
    if-eqz p1, :cond_1c

    .line 443
    .line 444
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->oOO〇〇:Landroid/animation/ValueAnimator;

    .line 445
    .line 446
    if-nez p1, :cond_1b

    .line 447
    .line 448
    new-instance p1, Landroid/animation/ValueAnimator;

    .line 449
    .line 450
    invoke-direct {p1}, Landroid/animation/ValueAnimator;-><init>()V

    .line 451
    .line 452
    .line 453
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->oOO〇〇:Landroid/animation/ValueAnimator;

    .line 454
    .line 455
    const-wide/16 v6, 0x64

    .line 456
    .line 457
    invoke-virtual {p1, v6, v7}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 458
    .line 459
    .line 460
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->oOO〇〇:Landroid/animation/ValueAnimator;

    .line 461
    .line 462
    new-instance v1, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$2;

    .line 463
    .line 464
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$2;-><init>(Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;)V

    .line 465
    .line 466
    .line 467
    invoke-virtual {p1, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 468
    .line 469
    .line 470
    :cond_1b
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->oOO〇〇:Landroid/animation/ValueAnimator;

    .line 471
    .line 472
    const/4 v1, 0x2

    .line 473
    new-array v1, v1, [F

    .line 474
    .line 475
    const/4 v3, 0x0

    .line 476
    aput v0, v1, v3

    .line 477
    .line 478
    const/4 v0, 0x1

    .line 479
    aput v4, v1, v0

    .line 480
    .line 481
    invoke-virtual {p1, v1}, Landroid/animation/ValueAnimator;->setFloatValues([F)V

    .line 482
    .line 483
    .line 484
    iput v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8o:F

    .line 485
    .line 486
    iput v5, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->oo8ooo8O:F

    .line 487
    .line 488
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->oOO〇〇:Landroid/animation/ValueAnimator;

    .line 489
    .line 490
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->start()V

    .line 491
    .line 492
    .line 493
    goto :goto_c

    .line 494
    :cond_1c
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 495
    .line 496
    invoke-virtual {p1, v4, v5}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->Oo8Oo00oo(FF)V

    .line 497
    .line 498
    .line 499
    :goto_c
    return-void
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
.end method

.method public 〇o〇(Landroid/view/MotionEvent;)V
    .locals 6

    .line 1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o0:F

    .line 6
    .line 7
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->OO:F

    .line 8
    .line 9
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇OOo8〇0:F

    .line 14
    .line 15
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇08O〇00〇o:F

    .line 16
    .line 17
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 18
    .line 19
    const/4 v0, 0x1

    .line 20
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->setScrollingDoodle(Z)V

    .line 21
    .line 22
    .line 23
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O〇8O8〇008()Z

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    if-eqz p1, :cond_0

    .line 30
    .line 31
    goto/16 :goto_2

    .line 32
    .line 33
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 34
    .line 35
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8ooOoo〇()Z

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    if-nez p1, :cond_6

    .line 40
    .line 41
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 42
    .line 43
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0000OOO()Z

    .line 44
    .line 45
    .line 46
    move-result p1

    .line 47
    if-nez p1, :cond_6

    .line 48
    .line 49
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 50
    .line 51
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇〇808〇(Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;)Z

    .line 56
    .line 57
    .line 58
    move-result p1

    .line 59
    if-eqz p1, :cond_1

    .line 60
    .line 61
    goto/16 :goto_1

    .line 62
    .line 63
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 64
    .line 65
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 66
    .line 67
    .line 68
    move-result-object p1

    .line 69
    sget-object v1, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->COPY:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 70
    .line 71
    const/4 v2, 0x0

    .line 72
    if-ne p1, v1, :cond_2

    .line 73
    .line 74
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O0O:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 75
    .line 76
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 77
    .line 78
    iget v4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o0:F

    .line 79
    .line 80
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 81
    .line 82
    .line 83
    move-result v3

    .line 84
    iget-object v4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 85
    .line 86
    iget v5, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇OOo8〇0:F

    .line 87
    .line 88
    invoke-virtual {v4, v5}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 89
    .line 90
    .line 91
    move-result v4

    .line 92
    iget-object v5, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 93
    .line 94
    invoke-virtual {v5}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getSize()F

    .line 95
    .line 96
    .line 97
    move-result v5

    .line 98
    invoke-virtual {p1, v3, v4, v5}, Lcom/intsig/camscanner/doodle/widget/CopyLocation;->〇080(FFF)Z

    .line 99
    .line 100
    .line 101
    move-result p1

    .line 102
    if-eqz p1, :cond_2

    .line 103
    .line 104
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O0O:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 105
    .line 106
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/CopyLocation;->〇O8o08O(Z)V

    .line 107
    .line 108
    .line 109
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O0O:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 110
    .line 111
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/doodle/widget/CopyLocation;->〇8o8o〇(Z)V

    .line 112
    .line 113
    .line 114
    goto/16 :goto_2

    .line 115
    .line 116
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 117
    .line 118
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 119
    .line 120
    .line 121
    move-result-object p1

    .line 122
    if-ne p1, v1, :cond_3

    .line 123
    .line 124
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O0O:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 125
    .line 126
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/doodle/widget/CopyLocation;->〇O8o08O(Z)V

    .line 127
    .line 128
    .line 129
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O0O:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 130
    .line 131
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/CopyLocation;->oO80()Z

    .line 132
    .line 133
    .line 134
    move-result p1

    .line 135
    if-nez p1, :cond_3

    .line 136
    .line 137
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O0O:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 138
    .line 139
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/CopyLocation;->〇8o8o〇(Z)V

    .line 140
    .line 141
    .line 142
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O0O:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 143
    .line 144
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 145
    .line 146
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o0:F

    .line 147
    .line 148
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 149
    .line 150
    .line 151
    move-result v0

    .line 152
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 153
    .line 154
    iget v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇OOo8〇0:F

    .line 155
    .line 156
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 157
    .line 158
    .line 159
    move-result v1

    .line 160
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/doodle/widget/CopyLocation;->OO0o〇〇(FF)V

    .line 161
    .line 162
    .line 163
    :cond_3
    new-instance p1, Landroid/graphics/Path;

    .line 164
    .line 165
    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    .line 166
    .line 167
    .line 168
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->ooo0〇〇O:Landroid/graphics/Path;

    .line 169
    .line 170
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 171
    .line 172
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o0:F

    .line 173
    .line 174
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 175
    .line 176
    .line 177
    move-result v0

    .line 178
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 179
    .line 180
    iget v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇OOo8〇0:F

    .line 181
    .line 182
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 183
    .line 184
    .line 185
    move-result v1

    .line 186
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 187
    .line 188
    .line 189
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 190
    .line 191
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getShape()Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;

    .line 192
    .line 193
    .line 194
    move-result-object p1

    .line 195
    sget-object v0, Lcom/intsig/camscanner/doodle/widget/DoodleShape;->HAND_WRITE:Lcom/intsig/camscanner/doodle/widget/DoodleShape;

    .line 196
    .line 197
    if-ne p1, v0, :cond_4

    .line 198
    .line 199
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 200
    .line 201
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->ooo0〇〇O:Landroid/graphics/Path;

    .line 202
    .line 203
    invoke-static {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇0(Lcom/intsig/camscanner/doodle/widget/core/IDoodle;Landroid/graphics/Path;)Lcom/intsig/camscanner/doodle/widget/DoodlePath;

    .line 204
    .line 205
    .line 206
    move-result-object p1

    .line 207
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇〇08O:Lcom/intsig/camscanner/doodle/widget/DoodlePath;

    .line 208
    .line 209
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getColor()Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;

    .line 210
    .line 211
    .line 212
    move-result-object p1

    .line 213
    const v0, -0x66e64364

    .line 214
    .line 215
    .line 216
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;->〇080(I)V

    .line 217
    .line 218
    .line 219
    goto :goto_0

    .line 220
    :cond_4
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 221
    .line 222
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇00O:F

    .line 223
    .line 224
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 225
    .line 226
    .line 227
    move-result v0

    .line 228
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 229
    .line 230
    iget v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->O8o08O8O:F

    .line 231
    .line 232
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 233
    .line 234
    .line 235
    move-result v1

    .line 236
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 237
    .line 238
    iget v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o0:F

    .line 239
    .line 240
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 241
    .line 242
    .line 243
    move-result v2

    .line 244
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 245
    .line 246
    iget v4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇OOo8〇0:F

    .line 247
    .line 248
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 249
    .line 250
    .line 251
    move-result v3

    .line 252
    invoke-static {p1, v0, v1, v2, v3}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o88〇OO08〇(Lcom/intsig/camscanner/doodle/widget/core/IDoodle;FFFF)Lcom/intsig/camscanner/doodle/widget/DoodlePath;

    .line 253
    .line 254
    .line 255
    move-result-object p1

    .line 256
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇〇08O:Lcom/intsig/camscanner/doodle/widget/DoodlePath;

    .line 257
    .line 258
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 259
    .line 260
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇oOO8O8()Z

    .line 261
    .line 262
    .line 263
    move-result p1

    .line 264
    if-eqz p1, :cond_5

    .line 265
    .line 266
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 267
    .line 268
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇〇08O:Lcom/intsig/camscanner/doodle/widget/DoodlePath;

    .line 269
    .line 270
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇〇0〇(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;)V

    .line 271
    .line 272
    .line 273
    goto/16 :goto_2

    .line 274
    .line 275
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 276
    .line 277
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇〇08O:Lcom/intsig/camscanner/doodle/widget/DoodlePath;

    .line 278
    .line 279
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇O00(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;)V

    .line 280
    .line 281
    .line 282
    goto/16 :goto_2

    .line 283
    .line 284
    :cond_6
    :goto_1
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 285
    .line 286
    if-eqz p1, :cond_8

    .line 287
    .line 288
    invoke-interface {p1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->getLocation()Landroid/graphics/PointF;

    .line 289
    .line 290
    .line 291
    move-result-object p1

    .line 292
    iget v1, p1, Landroid/graphics/PointF;->x:F

    .line 293
    .line 294
    iput v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->OO〇00〇8oO:F

    .line 295
    .line 296
    iget p1, p1, Landroid/graphics/PointF;->y:F

    .line 297
    .line 298
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8〇OO0〇0o:F

    .line 299
    .line 300
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 301
    .line 302
    instance-of v1, p1, Lcom/intsig/camscanner/doodle/widget/DoodleRotatableItemBase;

    .line 303
    .line 304
    if-eqz v1, :cond_a

    .line 305
    .line 306
    check-cast p1, Lcom/intsig/camscanner/doodle/widget/DoodleRotatableItemBase;

    .line 307
    .line 308
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 309
    .line 310
    iget v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o0:F

    .line 311
    .line 312
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 313
    .line 314
    .line 315
    move-result v1

    .line 316
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 317
    .line 318
    iget v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇OOo8〇0:F

    .line 319
    .line 320
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 321
    .line 322
    .line 323
    move-result v2

    .line 324
    invoke-virtual {p1, v1, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleRotatableItemBase;->oO(FF)Z

    .line 325
    .line 326
    .line 327
    move-result v3

    .line 328
    if-eqz v3, :cond_7

    .line 329
    .line 330
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleRotatableItemBase;->〇80(Z)V

    .line 331
    .line 332
    .line 333
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 334
    .line 335
    invoke-interface {p1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->〇〇808〇()F

    .line 336
    .line 337
    .line 338
    move-result p1

    .line 339
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 340
    .line 341
    invoke-interface {v0}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->O8()F

    .line 342
    .line 343
    .line 344
    move-result v0

    .line 345
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 346
    .line 347
    invoke-interface {v3}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->Oo08()F

    .line 348
    .line 349
    .line 350
    move-result v3

    .line 351
    invoke-static {v0, v3, v1, v2}, Lcom/intsig/camscanner/doodle/util/DrawUtil;->〇080(FFFF)F

    .line 352
    .line 353
    .line 354
    move-result v0

    .line 355
    sub-float/2addr p1, v0

    .line 356
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇8〇oO〇〇8o:F

    .line 357
    .line 358
    goto :goto_2

    .line 359
    :cond_7
    invoke-virtual {p1, v1, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleRotatableItemBase;->〇08O8o〇0(FF)Z

    .line 360
    .line 361
    .line 362
    move-result v1

    .line 363
    if-eqz v1, :cond_a

    .line 364
    .line 365
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->〇〇〇0〇〇0(Z)V

    .line 366
    .line 367
    .line 368
    goto :goto_2

    .line 369
    :cond_8
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 370
    .line 371
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8ooOoo〇()Z

    .line 372
    .line 373
    .line 374
    move-result p1

    .line 375
    if-nez p1, :cond_9

    .line 376
    .line 377
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 378
    .line 379
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0000OOO()Z

    .line 380
    .line 381
    .line 382
    move-result p1

    .line 383
    if-eqz p1, :cond_a

    .line 384
    .line 385
    :cond_9
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 386
    .line 387
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleTranslationX()F

    .line 388
    .line 389
    .line 390
    move-result p1

    .line 391
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->OO〇00〇8oO:F

    .line 392
    .line 393
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 394
    .line 395
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleTranslationY()F

    .line 396
    .line 397
    .line 398
    move-result p1

    .line 399
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8〇OO0〇0o:F

    .line 400
    .line 401
    :cond_a
    :goto_2
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 402
    .line 403
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->refresh()V

    .line 404
    .line 405
    .line 406
    return-void
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
.end method

.method public 〇〇8O0〇8(Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 2
    .line 3
    instance-of v1, v0, Lcom/intsig/camscanner/doodle/widget/DoodleText;

    .line 4
    .line 5
    const/4 v2, 0x1

    .line 6
    const/4 v3, 0x0

    .line 7
    if-eqz v1, :cond_3

    .line 8
    .line 9
    move-object v1, v0

    .line 10
    check-cast v1, Lcom/intsig/camscanner/doodle/widget/DoodleText;

    .line 11
    .line 12
    if-ne v0, p1, :cond_1

    .line 13
    .line 14
    invoke-virtual {v1}, Lcom/intsig/camscanner/doodle/widget/DoodleText;->O0o〇〇Oo()Z

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    if-nez p1, :cond_0

    .line 19
    .line 20
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleText;->OO8oO0o〇(Z)V

    .line 21
    .line 22
    .line 23
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇08〇o0O:Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$ISelectionListener;

    .line 24
    .line 25
    if-eqz p1, :cond_0

    .line 26
    .line 27
    invoke-interface {p1, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$ISelectionListener;->O8(Lcom/intsig/camscanner/doodle/widget/DoodleText;)V

    .line 28
    .line 29
    .line 30
    :cond_0
    return-void

    .line 31
    :cond_1
    invoke-virtual {v1}, Lcom/intsig/camscanner/doodle/widget/DoodleText;->O0o〇〇Oo()Z

    .line 32
    .line 33
    .line 34
    move-result v4

    .line 35
    if-eqz v4, :cond_3

    .line 36
    .line 37
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/doodle/widget/DoodleText;->OO8oO0o〇(Z)V

    .line 38
    .line 39
    .line 40
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇08〇o0O:Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$ISelectionListener;

    .line 41
    .line 42
    if-eqz p1, :cond_2

    .line 43
    .line 44
    invoke-interface {p1, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$ISelectionListener;->O8(Lcom/intsig/camscanner/doodle/widget/DoodleText;)V

    .line 45
    .line 46
    .line 47
    :cond_2
    return-void

    .line 48
    :cond_3
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 49
    .line 50
    if-eqz v0, :cond_5

    .line 51
    .line 52
    invoke-interface {v0, v3}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;->〇o00〇〇Oo(Z)V

    .line 53
    .line 54
    .line 55
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇08〇o0O:Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$ISelectionListener;

    .line 56
    .line 57
    if-eqz p1, :cond_4

    .line 58
    .line 59
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 60
    .line 61
    invoke-interface {p1, v1, v0, v3}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$ISelectionListener;->〇o00〇〇Oo(Lcom/intsig/camscanner/doodle/widget/core/IDoodle;Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;Z)V

    .line 62
    .line 63
    .line 64
    :cond_4
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 65
    .line 66
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OOO〇O0(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;)V

    .line 67
    .line 68
    .line 69
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 70
    .line 71
    if-eqz p1, :cond_7

    .line 72
    .line 73
    invoke-interface {p1, v2}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;->〇o00〇〇Oo(Z)V

    .line 74
    .line 75
    .line 76
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇08〇o0O:Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$ISelectionListener;

    .line 77
    .line 78
    if-eqz p1, :cond_6

    .line 79
    .line 80
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 81
    .line 82
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 83
    .line 84
    invoke-interface {p1, v0, v1, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$ISelectionListener;->〇o00〇〇Oo(Lcom/intsig/camscanner/doodle/widget/core/IDoodle;Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;Z)V

    .line 85
    .line 86
    .line 87
    :cond_6
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 88
    .line 89
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 90
    .line 91
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇〇0〇(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;)V

    .line 92
    .line 93
    .line 94
    :cond_7
    return-void
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

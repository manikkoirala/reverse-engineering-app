.class public interface abstract Lcom/intsig/camscanner/doodle/widget/core/IDoodle;
.super Ljava/lang/Object;
.source "IDoodle.java"


# virtual methods
.method public abstract getBitmap()Landroid/graphics/Bitmap;
.end method

.method public abstract getColor()Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;
.end method

.method public abstract getDoodleRotation()I
.end method

.method public abstract getDoodleScale()F
.end method

.method public abstract getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;
.end method

.method public abstract getShape()Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;
.end method

.method public abstract getSize()F
.end method

.method public abstract getUnitSize()F
.end method

.method public abstract refresh()V
.end method

.method public abstract setPen(Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;)V
.end method

.method public abstract setSize(F)V
.end method

.method public abstract 〇080()Z
.end method

.class public final enum Lcom/intsig/camscanner/doodle/widget/DoodlePen;
.super Ljava/lang/Enum;
.source "DoodlePen.java"

# interfaces
.implements Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/camscanner/doodle/widget/DoodlePen;",
        ">;",
        "Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/camscanner/doodle/widget/DoodlePen;

.field public static final enum BITMAP:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

.field public static final enum BRUSH:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

.field public static final enum COPY:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

.field public static final enum ERASER:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

.field public static final enum MOSAIC:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

.field public static final enum RECTANGLE:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

.field public static final enum TEXT:Lcom/intsig/camscanner/doodle/widget/DoodlePen;


# instance fields
.field private volatile mCopyLocation:Lcom/intsig/camscanner/doodle/widget/CopyLocation;


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .line 1
    new-instance v0, Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 2
    .line 3
    const-string v1, "BRUSH"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/doodle/widget/DoodlePen;-><init>(Ljava/lang/String;I)V

    .line 7
    .line 8
    .line 9
    sput-object v0, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->BRUSH:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 10
    .line 11
    new-instance v1, Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 12
    .line 13
    const-string v3, "COPY"

    .line 14
    .line 15
    const/4 v4, 0x1

    .line 16
    invoke-direct {v1, v3, v4}, Lcom/intsig/camscanner/doodle/widget/DoodlePen;-><init>(Ljava/lang/String;I)V

    .line 17
    .line 18
    .line 19
    sput-object v1, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->COPY:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 20
    .line 21
    new-instance v3, Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 22
    .line 23
    const-string v5, "ERASER"

    .line 24
    .line 25
    const/4 v6, 0x2

    .line 26
    invoke-direct {v3, v5, v6}, Lcom/intsig/camscanner/doodle/widget/DoodlePen;-><init>(Ljava/lang/String;I)V

    .line 27
    .line 28
    .line 29
    sput-object v3, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->ERASER:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 30
    .line 31
    new-instance v5, Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 32
    .line 33
    const-string v7, "TEXT"

    .line 34
    .line 35
    const/4 v8, 0x3

    .line 36
    invoke-direct {v5, v7, v8}, Lcom/intsig/camscanner/doodle/widget/DoodlePen;-><init>(Ljava/lang/String;I)V

    .line 37
    .line 38
    .line 39
    sput-object v5, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->TEXT:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 40
    .line 41
    new-instance v7, Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 42
    .line 43
    const-string v9, "BITMAP"

    .line 44
    .line 45
    const/4 v10, 0x4

    .line 46
    invoke-direct {v7, v9, v10}, Lcom/intsig/camscanner/doodle/widget/DoodlePen;-><init>(Ljava/lang/String;I)V

    .line 47
    .line 48
    .line 49
    sput-object v7, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->BITMAP:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 50
    .line 51
    new-instance v9, Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 52
    .line 53
    const-string v11, "MOSAIC"

    .line 54
    .line 55
    const/4 v12, 0x5

    .line 56
    invoke-direct {v9, v11, v12}, Lcom/intsig/camscanner/doodle/widget/DoodlePen;-><init>(Ljava/lang/String;I)V

    .line 57
    .line 58
    .line 59
    sput-object v9, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->MOSAIC:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 60
    .line 61
    new-instance v11, Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 62
    .line 63
    const-string v13, "RECTANGLE"

    .line 64
    .line 65
    const/4 v14, 0x6

    .line 66
    invoke-direct {v11, v13, v14}, Lcom/intsig/camscanner/doodle/widget/DoodlePen;-><init>(Ljava/lang/String;I)V

    .line 67
    .line 68
    .line 69
    sput-object v11, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->RECTANGLE:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 70
    .line 71
    const/4 v13, 0x7

    .line 72
    new-array v13, v13, [Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 73
    .line 74
    aput-object v0, v13, v2

    .line 75
    .line 76
    aput-object v1, v13, v4

    .line 77
    .line 78
    aput-object v3, v13, v6

    .line 79
    .line 80
    aput-object v5, v13, v8

    .line 81
    .line 82
    aput-object v7, v13, v10

    .line 83
    .line 84
    aput-object v9, v13, v12

    .line 85
    .line 86
    aput-object v11, v13, v14

    .line 87
    .line 88
    sput-object v13, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->$VALUES:[Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 89
    .line 90
    return-void
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/camscanner/doodle/widget/DoodlePen;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static values()[Lcom/intsig/camscanner/doodle/widget/DoodlePen;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->$VALUES:[Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/intsig/camscanner/doodle/widget/DoodlePen;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public config(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;Landroid/graphics/Paint;)V
    .locals 2

    .line 1
    sget-object p2, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->COPY:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 2
    .line 3
    if-eq p0, p2, :cond_0

    .line 4
    .line 5
    sget-object p2, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->ERASER:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 6
    .line 7
    if-ne p0, p2, :cond_2

    .line 8
    .line 9
    :cond_0
    invoke-interface {p1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->〇〇888()Lcom/intsig/camscanner/doodle/widget/core/IDoodle;

    .line 10
    .line 11
    .line 12
    move-result-object p2

    .line 13
    invoke-interface {p1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->getColor()Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    instance-of v0, v0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;

    .line 18
    .line 19
    if-eqz v0, :cond_1

    .line 20
    .line 21
    invoke-interface {p1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->getColor()Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    check-cast v0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;

    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->〇o〇()Landroid/graphics/Bitmap;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-interface {p2}, Lcom/intsig/camscanner/doodle/widget/core/IDoodle;->getBitmap()Landroid/graphics/Bitmap;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    if-ne v0, v1, :cond_1

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_1
    new-instance v0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;

    .line 39
    .line 40
    invoke-interface {p2}, Lcom/intsig/camscanner/doodle/widget/core/IDoodle;->getBitmap()Landroid/graphics/Bitmap;

    .line 41
    .line 42
    .line 43
    move-result-object p2

    .line 44
    invoke-direct {v0, p2}, Lcom/intsig/camscanner/doodle/widget/DoodleColor;-><init>(Landroid/graphics/Bitmap;)V

    .line 45
    .line 46
    .line 47
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->〇O〇(Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;)V

    .line 48
    .line 49
    .line 50
    :cond_2
    :goto_0
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public copy()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;
    .locals 0

    .line 1
    return-object p0
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public drawHelpers(Landroid/graphics/Canvas;Lcom/intsig/camscanner/doodle/widget/core/IDoodle;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->COPY:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 2
    .line 3
    if-ne p0, v0, :cond_0

    .line 4
    .line 5
    instance-of v0, p2, Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    move-object v0, p2

    .line 10
    check-cast v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8ooOoo〇()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->mCopyLocation:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 19
    .line 20
    invoke-interface {p2}, Lcom/intsig/camscanner/doodle/widget/core/IDoodle;->getSize()F

    .line 21
    .line 22
    .line 23
    move-result p2

    .line 24
    invoke-virtual {v0, p1, p2}, Lcom/intsig/camscanner/doodle/widget/CopyLocation;->〇o〇(Landroid/graphics/Canvas;F)V

    .line 25
    .line 26
    .line 27
    :cond_0
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public getCopyLocation()Lcom/intsig/camscanner/doodle/widget/CopyLocation;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->COPY:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 2
    .line 3
    if-eq p0, v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return-object v0

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->mCopyLocation:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 8
    .line 9
    if-nez v0, :cond_2

    .line 10
    .line 11
    monitor-enter p0

    .line 12
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->mCopyLocation:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 13
    .line 14
    if-nez v0, :cond_1

    .line 15
    .line 16
    new-instance v0, Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 17
    .line 18
    invoke-direct {v0}, Lcom/intsig/camscanner/doodle/widget/CopyLocation;-><init>()V

    .line 19
    .line 20
    .line 21
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->mCopyLocation:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 22
    .line 23
    :cond_1
    monitor-exit p0

    .line 24
    goto :goto_0

    .line 25
    :catchall_0
    move-exception v0

    .line 26
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 27
    throw v0

    .line 28
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->mCopyLocation:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 29
    .line 30
    return-object v0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

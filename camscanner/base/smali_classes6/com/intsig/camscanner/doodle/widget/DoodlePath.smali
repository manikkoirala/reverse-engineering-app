.class public Lcom/intsig/camscanner/doodle/widget/DoodlePath;
.super Lcom/intsig/camscanner/doodle/widget/DoodleRotatableItemBase;
.source "DoodlePath.java"


# static fields
.field private static o〇O:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap<",
            "Lcom/intsig/camscanner/doodle/widget/core/IDoodle;",
            "Ljava/util/HashMap<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Bitmap;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private final O08000:Landroid/graphics/Matrix;

.field private O〇O〇oO:Landroid/graphics/Matrix;

.field private o8oO〇:Landroid/graphics/RectF;

.field private oO:Landroid/graphics/Paint;

.field private final o〇0OOo〇0:Landroid/graphics/Path;

.field private o〇8oOO88:Landroid/graphics/Path;

.field private 〇08O8o〇0:Landroid/graphics/PointF;

.field private 〇8:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

.field private 〇8〇0〇o〇O:Landroid/graphics/Rect;

.field private 〇〇0o:Landroid/graphics/PointF;

.field private final 〇〇〇0〇〇0:Landroid/graphics/Path;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Ljava/util/WeakHashMap;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o〇O:Ljava/util/WeakHashMap;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/intsig/camscanner/doodle/widget/core/IDoodle;)V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    invoke-direct {p0, p1, v0, v1, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleRotatableItemBase;-><init>(Lcom/intsig/camscanner/doodle/widget/core/IDoodle;IFF)V

    .line 4
    .line 5
    .line 6
    new-instance p1, Landroid/graphics/Path;

    .line 7
    .line 8
    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    .line 9
    .line 10
    .line 11
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇〇〇0〇〇0:Landroid/graphics/Path;

    .line 12
    .line 13
    new-instance p1, Landroid/graphics/Path;

    .line 14
    .line 15
    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    .line 16
    .line 17
    .line 18
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o〇0OOo〇0:Landroid/graphics/Path;

    .line 19
    .line 20
    new-instance p1, Landroid/graphics/PointF;

    .line 21
    .line 22
    invoke-direct {p1}, Landroid/graphics/PointF;-><init>()V

    .line 23
    .line 24
    .line 25
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇〇0o:Landroid/graphics/PointF;

    .line 26
    .line 27
    new-instance p1, Landroid/graphics/PointF;

    .line 28
    .line 29
    invoke-direct {p1}, Landroid/graphics/PointF;-><init>()V

    .line 30
    .line 31
    .line 32
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇08O8o〇0:Landroid/graphics/PointF;

    .line 33
    .line 34
    new-instance p1, Landroid/graphics/Paint;

    .line 35
    .line 36
    invoke-direct {p1}, Landroid/graphics/Paint;-><init>()V

    .line 37
    .line 38
    .line 39
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->oO:Landroid/graphics/Paint;

    .line 40
    .line 41
    new-instance p1, Landroid/graphics/Matrix;

    .line 42
    .line 43
    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    .line 44
    .line 45
    .line 46
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->O08000:Landroid/graphics/Matrix;

    .line 47
    .line 48
    new-instance p1, Landroid/graphics/Rect;

    .line 49
    .line 50
    invoke-direct {p1}, Landroid/graphics/Rect;-><init>()V

    .line 51
    .line 52
    .line 53
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇8〇0〇o〇O:Landroid/graphics/Rect;

    .line 54
    .line 55
    new-instance p1, Landroid/graphics/Matrix;

    .line 56
    .line 57
    invoke-direct {p1}, Landroid/graphics/Matrix;-><init>()V

    .line 58
    .line 59
    .line 60
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->O〇O〇oO:Landroid/graphics/Matrix;

    .line 61
    .line 62
    new-instance p1, Landroid/graphics/RectF;

    .line 63
    .line 64
    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    .line 65
    .line 66
    .line 67
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o8oO〇:Landroid/graphics/RectF;

    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private O0O8OO088(Landroid/graphics/Path;FFFFF)V
    .locals 0

    .line 1
    sub-float p4, p2, p4

    .line 2
    .line 3
    mul-float p4, p4, p4

    .line 4
    .line 5
    sub-float p5, p3, p5

    .line 6
    .line 7
    mul-float p5, p5, p5

    .line 8
    .line 9
    add-float/2addr p4, p5

    .line 10
    float-to-double p4, p4

    .line 11
    invoke-static {p4, p5}, Ljava/lang/Math;->sqrt(D)D

    .line 12
    .line 13
    .line 14
    move-result-wide p4

    .line 15
    double-to-float p4, p4

    .line 16
    sget-object p5, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    .line 17
    .line 18
    invoke-virtual {p1, p2, p3, p4, p5}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method private O8O〇(Landroid/graphics/Path;FFFFF)V
    .locals 24

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p1

    .line 4
    .line 5
    move/from16 v2, p4

    .line 6
    .line 7
    move/from16 v3, p5

    .line 8
    .line 9
    move/from16 v4, p6

    .line 10
    .line 11
    float-to-double v5, v4

    .line 12
    const/high16 v7, 0x40000000    # 2.0f

    .line 13
    .line 14
    div-float/2addr v4, v7

    .line 15
    float-to-double v7, v4

    .line 16
    const-wide/high16 v9, 0x4000000000000000L    # 2.0

    .line 17
    .line 18
    div-double v11, v7, v9

    .line 19
    .line 20
    div-double v13, v11, v5

    .line 21
    .line 22
    invoke-static {v13, v14}, Ljava/lang/Math;->atan(D)D

    .line 23
    .line 24
    .line 25
    move-result-wide v13

    .line 26
    mul-double v11, v11, v7

    .line 27
    .line 28
    div-double/2addr v11, v9

    .line 29
    mul-double v9, v5, v5

    .line 30
    .line 31
    add-double/2addr v11, v9

    .line 32
    invoke-static {v11, v12}, Ljava/lang/Math;->sqrt(D)D

    .line 33
    .line 34
    .line 35
    move-result-wide v11

    .line 36
    const-wide/high16 v15, 0x4014000000000000L    # 5.0

    .line 37
    .line 38
    sub-double v22, v11, v15

    .line 39
    .line 40
    sub-float v4, v2, p2

    .line 41
    .line 42
    sub-float v11, v3, p3

    .line 43
    .line 44
    const/16 v19, 0x1

    .line 45
    .line 46
    move v15, v4

    .line 47
    move/from16 v16, v11

    .line 48
    .line 49
    move-wide/from16 v17, v13

    .line 50
    .line 51
    move-wide/from16 v20, v22

    .line 52
    .line 53
    invoke-static/range {v15 .. v21}, Lcom/intsig/camscanner/doodle/util/DrawUtil;->O8(FFDZD)[D

    .line 54
    .line 55
    .line 56
    move-result-object v12

    .line 57
    neg-double v13, v13

    .line 58
    const/16 v21, 0x1

    .line 59
    .line 60
    move/from16 v17, v4

    .line 61
    .line 62
    move/from16 v18, v11

    .line 63
    .line 64
    move-wide/from16 v19, v13

    .line 65
    .line 66
    invoke-static/range {v17 .. v23}, Lcom/intsig/camscanner/doodle/util/DrawUtil;->O8(FFDZD)[D

    .line 67
    .line 68
    .line 69
    move-result-object v13

    .line 70
    float-to-double v14, v2

    .line 71
    const/16 v22, 0x0

    .line 72
    .line 73
    aget-wide v16, v12, v22

    .line 74
    .line 75
    move-wide/from16 v18, v9

    .line 76
    .line 77
    sub-double v9, v14, v16

    .line 78
    .line 79
    double-to-float v9, v9

    .line 80
    move/from16 p6, v11

    .line 81
    .line 82
    float-to-double v10, v3

    .line 83
    const/16 v23, 0x1

    .line 84
    .line 85
    aget-wide v16, v12, v23

    .line 86
    .line 87
    sub-double v2, v10, v16

    .line 88
    .line 89
    double-to-float v2, v2

    .line 90
    aget-wide v16, v13, v22

    .line 91
    .line 92
    move v12, v4

    .line 93
    sub-double v3, v14, v16

    .line 94
    .line 95
    double-to-float v3, v3

    .line 96
    aget-wide v16, v13, v23

    .line 97
    .line 98
    move-wide/from16 v20, v14

    .line 99
    .line 100
    sub-double v13, v10, v16

    .line 101
    .line 102
    double-to-float v4, v13

    .line 103
    invoke-virtual/range {p1 .. p3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 104
    .line 105
    .line 106
    invoke-virtual {v1, v9, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 107
    .line 108
    .line 109
    invoke-virtual {v1, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 110
    .line 111
    .line 112
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Path;->close()V

    .line 113
    .line 114
    .line 115
    div-double v2, v7, v5

    .line 116
    .line 117
    invoke-static {v2, v3}, Ljava/lang/Math;->atan(D)D

    .line 118
    .line 119
    .line 120
    move-result-wide v2

    .line 121
    mul-double v7, v7, v7

    .line 122
    .line 123
    add-double v7, v7, v18

    .line 124
    .line 125
    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    .line 126
    .line 127
    .line 128
    move-result-wide v4

    .line 129
    const/16 v19, 0x1

    .line 130
    .line 131
    move-wide/from16 v6, v20

    .line 132
    .line 133
    move v15, v12

    .line 134
    move/from16 v16, p6

    .line 135
    .line 136
    move-wide/from16 v17, v2

    .line 137
    .line 138
    move-wide/from16 v20, v4

    .line 139
    .line 140
    invoke-static/range {v15 .. v21}, Lcom/intsig/camscanner/doodle/util/DrawUtil;->O8(FFDZD)[D

    .line 141
    .line 142
    .line 143
    move-result-object v8

    .line 144
    neg-double v2, v2

    .line 145
    move-wide/from16 v17, v2

    .line 146
    .line 147
    invoke-static/range {v15 .. v21}, Lcom/intsig/camscanner/doodle/util/DrawUtil;->O8(FFDZD)[D

    .line 148
    .line 149
    .line 150
    move-result-object v2

    .line 151
    aget-wide v3, v8, v22

    .line 152
    .line 153
    sub-double v14, v6, v3

    .line 154
    .line 155
    double-to-float v3, v14

    .line 156
    aget-wide v4, v8, v23

    .line 157
    .line 158
    sub-double v4, v10, v4

    .line 159
    .line 160
    double-to-float v4, v4

    .line 161
    aget-wide v8, v2, v22

    .line 162
    .line 163
    sub-double v14, v6, v8

    .line 164
    .line 165
    double-to-float v5, v14

    .line 166
    aget-wide v6, v2, v23

    .line 167
    .line 168
    sub-double/2addr v10, v6

    .line 169
    double-to-float v2, v10

    .line 170
    iget-object v6, v0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o〇8oOO88:Landroid/graphics/Path;

    .line 171
    .line 172
    if-nez v6, :cond_0

    .line 173
    .line 174
    new-instance v6, Landroid/graphics/Path;

    .line 175
    .line 176
    invoke-direct {v6}, Landroid/graphics/Path;-><init>()V

    .line 177
    .line 178
    .line 179
    iput-object v6, v0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o〇8oOO88:Landroid/graphics/Path;

    .line 180
    .line 181
    :cond_0
    iget-object v6, v0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o〇8oOO88:Landroid/graphics/Path;

    .line 182
    .line 183
    invoke-virtual {v6}, Landroid/graphics/Path;->reset()V

    .line 184
    .line 185
    .line 186
    iget-object v6, v0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o〇8oOO88:Landroid/graphics/Path;

    .line 187
    .line 188
    move/from16 v7, p4

    .line 189
    .line 190
    move/from16 v8, p5

    .line 191
    .line 192
    invoke-virtual {v6, v7, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 193
    .line 194
    .line 195
    iget-object v6, v0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o〇8oOO88:Landroid/graphics/Path;

    .line 196
    .line 197
    invoke-virtual {v6, v5, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 198
    .line 199
    .line 200
    iget-object v2, v0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o〇8oOO88:Landroid/graphics/Path;

    .line 201
    .line 202
    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 203
    .line 204
    .line 205
    iget-object v2, v0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o〇8oOO88:Landroid/graphics/Path;

    .line 206
    .line 207
    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    .line 208
    .line 209
    .line 210
    iget-object v2, v0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o〇8oOO88:Landroid/graphics/Path;

    .line 211
    .line 212
    invoke-virtual {v1, v2}, Landroid/graphics/Path;->addPath(Landroid/graphics/Path;)V

    .line 213
    .line 214
    .line 215
    return-void
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method private Ooo()V
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    sget-object v1, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->MOSAIC:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 6
    .line 7
    if-ne v0, v1, :cond_0

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getColor()Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    instance-of v0, v0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;

    .line 14
    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getColor()Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;

    .line 22
    .line 23
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->Oo08()Landroid/graphics/Matrix;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 28
    .line 29
    .line 30
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getScale()F

    .line 31
    .line 32
    .line 33
    move-result v2

    .line 34
    const/high16 v3, 0x3f800000    # 1.0f

    .line 35
    .line 36
    div-float v2, v3, v2

    .line 37
    .line 38
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getScale()F

    .line 39
    .line 40
    .line 41
    move-result v4

    .line 42
    div-float/2addr v3, v4

    .line 43
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->O8()F

    .line 44
    .line 45
    .line 46
    move-result v4

    .line 47
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->Oo08()F

    .line 48
    .line 49
    .line 50
    move-result v5

    .line 51
    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Matrix;->preScale(FFFF)Z

    .line 52
    .line 53
    .line 54
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getLocation()Landroid/graphics/PointF;

    .line 55
    .line 56
    .line 57
    move-result-object v2

    .line 58
    iget v2, v2, Landroid/graphics/PointF;->x:F

    .line 59
    .line 60
    neg-float v2, v2

    .line 61
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getScale()F

    .line 62
    .line 63
    .line 64
    move-result v3

    .line 65
    mul-float v2, v2, v3

    .line 66
    .line 67
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getLocation()Landroid/graphics/PointF;

    .line 68
    .line 69
    .line 70
    move-result-object v3

    .line 71
    iget v3, v3, Landroid/graphics/PointF;->y:F

    .line 72
    .line 73
    neg-float v3, v3

    .line 74
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getScale()F

    .line 75
    .line 76
    .line 77
    move-result v4

    .line 78
    mul-float v3, v3, v4

    .line 79
    .line 80
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    .line 81
    .line 82
    .line 83
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇〇808〇()F

    .line 84
    .line 85
    .line 86
    move-result v2

    .line 87
    neg-float v2, v2

    .line 88
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->O8()F

    .line 89
    .line 90
    .line 91
    move-result v3

    .line 92
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->Oo08()F

    .line 93
    .line 94
    .line 95
    move-result v4

    .line 96
    invoke-virtual {v1, v2, v3, v4}, Landroid/graphics/Matrix;->preRotate(FFF)Z

    .line 97
    .line 98
    .line 99
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->O8()I

    .line 100
    .line 101
    .line 102
    move-result v2

    .line 103
    int-to-float v2, v2

    .line 104
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->O8()I

    .line 105
    .line 106
    .line 107
    move-result v3

    .line 108
    int-to-float v3, v3

    .line 109
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 110
    .line 111
    .line 112
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->o〇0(Landroid/graphics/Matrix;)V

    .line 113
    .line 114
    .line 115
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇00()V

    .line 116
    .line 117
    .line 118
    :cond_0
    return-void
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private Oo〇O(Landroid/graphics/Path;FFFFF)V
    .locals 6

    .line 1
    cmpg-float v0, p2, p4

    .line 2
    .line 3
    if-gez v0, :cond_1

    .line 4
    .line 5
    cmpg-float v0, p3, p5

    .line 6
    .line 7
    if-gez v0, :cond_0

    .line 8
    .line 9
    sget-object v5, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    .line 10
    .line 11
    move-object v0, p1

    .line 12
    move v1, p2

    .line 13
    move v2, p3

    .line 14
    move v3, p4

    .line 15
    move v4, p5

    .line 16
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 17
    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    sget-object v5, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    .line 21
    .line 22
    move-object v0, p1

    .line 23
    move v1, p2

    .line 24
    move v2, p5

    .line 25
    move v3, p4

    .line 26
    move v4, p3

    .line 27
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 28
    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_1
    cmpg-float v0, p3, p5

    .line 32
    .line 33
    if-gez v0, :cond_2

    .line 34
    .line 35
    sget-object v5, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    .line 36
    .line 37
    move-object v0, p1

    .line 38
    move v1, p4

    .line 39
    move v2, p3

    .line 40
    move v3, p2

    .line 41
    move v4, p5

    .line 42
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 43
    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_2
    sget-object v5, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    .line 47
    .line 48
    move-object v0, p1

    .line 49
    move v1, p4

    .line 50
    move v2, p5

    .line 51
    move v3, p2

    .line 52
    move v4, p3

    .line 53
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Path;->addRect(FFFFLandroid/graphics/Path$Direction;)V

    .line 54
    .line 55
    .line 56
    :goto_0
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method private o0O0(Landroid/graphics/Rect;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o〇0OOo〇0:Landroid/graphics/Path;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇oo〇()F

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/high16 v1, 0x40000000    # 2.0f

    .line 11
    .line 12
    div-float/2addr v0, v1

    .line 13
    const/high16 v1, 0x3f000000    # 0.5f

    .line 14
    .line 15
    add-float/2addr v0, v1

    .line 16
    float-to-int v0, v0

    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o〇0OOo〇0:Landroid/graphics/Path;

    .line 18
    .line 19
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o8oO〇:Landroid/graphics/RectF;

    .line 20
    .line 21
    const/4 v3, 0x0

    .line 22
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getShape()Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    sget-object v2, Lcom/intsig/camscanner/doodle/widget/DoodleShape;->ARROW:Lcom/intsig/camscanner/doodle/widget/DoodleShape;

    .line 30
    .line 31
    if-eq v1, v2, :cond_1

    .line 32
    .line 33
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getShape()Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    sget-object v2, Lcom/intsig/camscanner/doodle/widget/DoodleShape;->FILL_CIRCLE:Lcom/intsig/camscanner/doodle/widget/DoodleShape;

    .line 38
    .line 39
    if-eq v1, v2, :cond_1

    .line 40
    .line 41
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getShape()Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    sget-object v2, Lcom/intsig/camscanner/doodle/widget/DoodleShape;->FILL_RECT:Lcom/intsig/camscanner/doodle/widget/DoodleShape;

    .line 46
    .line 47
    if-ne v1, v2, :cond_2

    .line 48
    .line 49
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇〇888()Lcom/intsig/camscanner/doodle/widget/core/IDoodle;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    invoke-interface {v0}, Lcom/intsig/camscanner/doodle/widget/core/IDoodle;->getUnitSize()F

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    float-to-int v0, v0

    .line 58
    :cond_2
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o8oO〇:Landroid/graphics/RectF;

    .line 59
    .line 60
    iget v2, v1, Landroid/graphics/RectF;->left:F

    .line 61
    .line 62
    int-to-float v0, v0

    .line 63
    sub-float/2addr v2, v0

    .line 64
    float-to-int v2, v2

    .line 65
    iget v3, v1, Landroid/graphics/RectF;->top:F

    .line 66
    .line 67
    sub-float/2addr v3, v0

    .line 68
    float-to-int v3, v3

    .line 69
    iget v4, v1, Landroid/graphics/RectF;->right:F

    .line 70
    .line 71
    add-float/2addr v4, v0

    .line 72
    float-to-int v4, v4

    .line 73
    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    .line 74
    .line 75
    add-float/2addr v1, v0

    .line 76
    float-to-int v0, v1

    .line 77
    invoke-virtual {p1, v2, v3, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 78
    .line 79
    .line 80
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static o88〇OO08〇(Lcom/intsig/camscanner/doodle/widget/core/IDoodle;FFFF)Lcom/intsig/camscanner/doodle/widget/DoodlePath;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;-><init>(Lcom/intsig/camscanner/doodle/widget/core/IDoodle;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0}, Lcom/intsig/camscanner/doodle/widget/core/IDoodle;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-interface {v1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;->copy()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇0000OOO(Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;)V

    .line 15
    .line 16
    .line 17
    invoke-interface {p0}, Lcom/intsig/camscanner/doodle/widget/core/IDoodle;->getShape()Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-interface {v1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;->copy()Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->oo〇(Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;)V

    .line 26
    .line 27
    .line 28
    invoke-interface {p0}, Lcom/intsig/camscanner/doodle/widget/core/IDoodle;->getSize()F

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->O8〇o(F)V

    .line 33
    .line 34
    .line 35
    invoke-interface {p0}, Lcom/intsig/camscanner/doodle/widget/core/IDoodle;->getColor()Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    invoke-interface {v1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;->copy()Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇O〇(Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o8O〇(FFFF)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    sget-object p2, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->COPY:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 54
    .line 55
    if-ne p1, p2, :cond_0

    .line 56
    .line 57
    instance-of p0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 58
    .line 59
    if-eqz p0, :cond_0

    .line 60
    .line 61
    invoke-virtual {p2}, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->getCopyLocation()Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 62
    .line 63
    .line 64
    move-result-object p0

    .line 65
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/CopyLocation;->〇o00〇〇Oo()Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 66
    .line 67
    .line 68
    move-result-object p0

    .line 69
    iput-object p0, v0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇8:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 70
    .line 71
    :cond_0
    return-object v0
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method public static 〇0(Lcom/intsig/camscanner/doodle/widget/core/IDoodle;Landroid/graphics/Path;)Lcom/intsig/camscanner/doodle/widget/DoodlePath;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;-><init>(Lcom/intsig/camscanner/doodle/widget/core/IDoodle;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0}, Lcom/intsig/camscanner/doodle/widget/core/IDoodle;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-interface {v1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;->copy()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇0000OOO(Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;)V

    .line 15
    .line 16
    .line 17
    invoke-interface {p0}, Lcom/intsig/camscanner/doodle/widget/core/IDoodle;->getShape()Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-interface {v1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;->copy()Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->oo〇(Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;)V

    .line 26
    .line 27
    .line 28
    invoke-interface {p0}, Lcom/intsig/camscanner/doodle/widget/core/IDoodle;->getSize()F

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->O8〇o(F)V

    .line 33
    .line 34
    .line 35
    invoke-interface {p0}, Lcom/intsig/camscanner/doodle/widget/core/IDoodle;->getColor()Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    invoke-interface {v1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;->copy()Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇O〇(Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇〇o8(Landroid/graphics/Path;)V

    .line 47
    .line 48
    .line 49
    instance-of p0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 50
    .line 51
    if-eqz p0, :cond_0

    .line 52
    .line 53
    sget-object p0, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->COPY:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 54
    .line 55
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->getCopyLocation()Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 56
    .line 57
    .line 58
    move-result-object p0

    .line 59
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/CopyLocation;->〇o00〇〇Oo()Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 60
    .line 61
    .line 62
    move-result-object p0

    .line 63
    iput-object p0, v0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇8:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_0
    const/4 p0, 0x0

    .line 67
    iput-object p0, v0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇8:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 68
    .line 69
    :goto_0
    return-object v0
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private 〇O〇80o08O(Z)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇8〇0〇o〇O:Landroid/graphics/Rect;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o0O0(Landroid/graphics/Rect;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇〇〇0〇〇0:Landroid/graphics/Path;

    .line 7
    .line 8
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇〇〇0〇〇0:Landroid/graphics/Path;

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o〇0OOo〇0:Landroid/graphics/Path;

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Landroid/graphics/Path;->addPath(Landroid/graphics/Path;)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->O08000:Landroid/graphics/Matrix;

    .line 19
    .line 20
    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->O08000:Landroid/graphics/Matrix;

    .line 24
    .line 25
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇8〇0〇o〇O:Landroid/graphics/Rect;

    .line 26
    .line 27
    iget v2, v1, Landroid/graphics/Rect;->left:I

    .line 28
    .line 29
    neg-int v2, v2

    .line 30
    int-to-float v2, v2

    .line 31
    iget v1, v1, Landroid/graphics/Rect;->top:I

    .line 32
    .line 33
    neg-int v1, v1

    .line 34
    int-to-float v1, v1

    .line 35
    invoke-virtual {v0, v2, v1}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 36
    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇〇〇0〇〇0:Landroid/graphics/Path;

    .line 39
    .line 40
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->O08000:Landroid/graphics/Matrix;

    .line 41
    .line 42
    invoke-virtual {v0, v1}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    .line 43
    .line 44
    .line 45
    if-eqz p1, :cond_0

    .line 46
    .line 47
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇8〇0〇o〇O:Landroid/graphics/Rect;

    .line 48
    .line 49
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 50
    .line 51
    int-to-float v0, v0

    .line 52
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 53
    .line 54
    .line 55
    move-result p1

    .line 56
    int-to-float p1, p1

    .line 57
    const/high16 v1, 0x40000000    # 2.0f

    .line 58
    .line 59
    div-float/2addr p1, v1

    .line 60
    add-float/2addr v0, p1

    .line 61
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->o〇〇0〇(F)V

    .line 62
    .line 63
    .line 64
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇8〇0〇o〇O:Landroid/graphics/Rect;

    .line 65
    .line 66
    iget v0, p1, Landroid/graphics/Rect;->top:I

    .line 67
    .line 68
    int-to-float v0, v0

    .line 69
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 70
    .line 71
    .line 72
    move-result p1

    .line 73
    int-to-float p1, p1

    .line 74
    div-float/2addr p1, v1

    .line 75
    add-float/2addr v0, p1

    .line 76
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->OOO〇O0(F)V

    .line 77
    .line 78
    .line 79
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇8〇0〇o〇O:Landroid/graphics/Rect;

    .line 80
    .line 81
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 82
    .line 83
    int-to-float v0, v0

    .line 84
    iget p1, p1, Landroid/graphics/Rect;->top:I

    .line 85
    .line 86
    int-to-float p1, p1

    .line 87
    const/4 v1, 0x0

    .line 88
    invoke-virtual {p0, v0, p1, v1}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->O8ooOoo〇(FFZ)V

    .line 89
    .line 90
    .line 91
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getColor()Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;

    .line 92
    .line 93
    .line 94
    move-result-object p1

    .line 95
    instance-of p1, p1, Lcom/intsig/camscanner/doodle/widget/DoodleColor;

    .line 96
    .line 97
    if-eqz p1, :cond_4

    .line 98
    .line 99
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getColor()Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;

    .line 100
    .line 101
    .line 102
    move-result-object p1

    .line 103
    check-cast p1, Lcom/intsig/camscanner/doodle/widget/DoodleColor;

    .line 104
    .line 105
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->getType()Lcom/intsig/camscanner/doodle/widget/DoodleColor$Type;

    .line 106
    .line 107
    .line 108
    move-result-object v0

    .line 109
    sget-object v1, Lcom/intsig/camscanner/doodle/widget/DoodleColor$Type;->BITMAP:Lcom/intsig/camscanner/doodle/widget/DoodleColor$Type;

    .line 110
    .line 111
    if-ne v0, v1, :cond_4

    .line 112
    .line 113
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->〇o〇()Landroid/graphics/Bitmap;

    .line 114
    .line 115
    .line 116
    move-result-object v0

    .line 117
    if-eqz v0, :cond_4

    .line 118
    .line 119
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->O〇O〇oO:Landroid/graphics/Matrix;

    .line 120
    .line 121
    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 122
    .line 123
    .line 124
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 125
    .line 126
    .line 127
    move-result-object v0

    .line 128
    sget-object v1, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->MOSAIC:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 129
    .line 130
    if-ne v0, v1, :cond_1

    .line 131
    .line 132
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->Ooo()V

    .line 133
    .line 134
    .line 135
    goto :goto_2

    .line 136
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 137
    .line 138
    .line 139
    move-result-object v0

    .line 140
    sget-object v1, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->COPY:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 141
    .line 142
    if-ne v0, v1, :cond_3

    .line 143
    .line 144
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->OOO()Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 145
    .line 146
    .line 147
    move-result-object v0

    .line 148
    if-eqz v0, :cond_2

    .line 149
    .line 150
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/CopyLocation;->o〇0()F

    .line 151
    .line 152
    .line 153
    move-result v1

    .line 154
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/CopyLocation;->O8()F

    .line 155
    .line 156
    .line 157
    move-result v2

    .line 158
    sub-float/2addr v1, v2

    .line 159
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/CopyLocation;->〇〇888()F

    .line 160
    .line 161
    .line 162
    move-result v2

    .line 163
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/CopyLocation;->Oo08()F

    .line 164
    .line 165
    .line 166
    move-result v0

    .line 167
    sub-float/2addr v2, v0

    .line 168
    goto :goto_0

    .line 169
    :cond_2
    const/4 v1, 0x0

    .line 170
    const/4 v2, 0x0

    .line 171
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇8〇0〇o〇O:Landroid/graphics/Rect;

    .line 172
    .line 173
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o0O0(Landroid/graphics/Rect;)V

    .line 174
    .line 175
    .line 176
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->O〇O〇oO:Landroid/graphics/Matrix;

    .line 177
    .line 178
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇8〇0〇o〇O:Landroid/graphics/Rect;

    .line 179
    .line 180
    iget v4, v3, Landroid/graphics/Rect;->left:I

    .line 181
    .line 182
    int-to-float v4, v4

    .line 183
    sub-float/2addr v1, v4

    .line 184
    iget v3, v3, Landroid/graphics/Rect;->top:I

    .line 185
    .line 186
    int-to-float v3, v3

    .line 187
    sub-float/2addr v2, v3

    .line 188
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 189
    .line 190
    .line 191
    goto :goto_1

    .line 192
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->O〇O〇oO:Landroid/graphics/Matrix;

    .line 193
    .line 194
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇8〇0〇o〇O:Landroid/graphics/Rect;

    .line 195
    .line 196
    iget v2, v1, Landroid/graphics/Rect;->left:I

    .line 197
    .line 198
    neg-int v2, v2

    .line 199
    int-to-float v2, v2

    .line 200
    iget v1, v1, Landroid/graphics/Rect;->top:I

    .line 201
    .line 202
    neg-int v1, v1

    .line 203
    int-to-float v1, v1

    .line 204
    invoke-virtual {v0, v2, v1}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 205
    .line 206
    .line 207
    :goto_1
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->O8()I

    .line 208
    .line 209
    .line 210
    move-result v0

    .line 211
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->O〇O〇oO:Landroid/graphics/Matrix;

    .line 212
    .line 213
    int-to-float v0, v0

    .line 214
    invoke-virtual {v1, v0, v0}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 215
    .line 216
    .line 217
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->O〇O〇oO:Landroid/graphics/Matrix;

    .line 218
    .line 219
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->o〇0(Landroid/graphics/Matrix;)V

    .line 220
    .line 221
    .line 222
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇00()V

    .line 223
    .line 224
    .line 225
    :cond_4
    :goto_2
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇00()V

    .line 226
    .line 227
    .line 228
    return-void
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private 〇o0O0O8(Landroid/graphics/Path;FFFFF)V
    .locals 0

    .line 1
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p1, p4, p5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method


# virtual methods
.method public O0o〇〇Oo()Landroid/graphics/Path;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇〇〇0〇〇0:Landroid/graphics/Path;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O8ooOoo〇(FFZ)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->O8ooOoo〇(FFZ)V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->Ooo()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public O8〇o(F)V
    .locals 7

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->O8〇o(F)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->O08000:Landroid/graphics/Matrix;

    .line 5
    .line 6
    if-nez p1, :cond_0

    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    sget-object p1, Lcom/intsig/camscanner/doodle/widget/DoodleShape;->ARROW:Lcom/intsig/camscanner/doodle/widget/DoodleShape;

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getShape()Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    if-eqz p1, :cond_1

    .line 20
    .line 21
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o〇0OOo〇0:Landroid/graphics/Path;

    .line 22
    .line 23
    invoke-virtual {p1}, Landroid/graphics/Path;->reset()V

    .line 24
    .line 25
    .line 26
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o〇0OOo〇0:Landroid/graphics/Path;

    .line 27
    .line 28
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇〇0o:Landroid/graphics/PointF;

    .line 29
    .line 30
    iget v2, p1, Landroid/graphics/PointF;->x:F

    .line 31
    .line 32
    iget v3, p1, Landroid/graphics/PointF;->y:F

    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇08O8o〇0:Landroid/graphics/PointF;

    .line 35
    .line 36
    iget v4, p1, Landroid/graphics/PointF;->x:F

    .line 37
    .line 38
    iget v5, p1, Landroid/graphics/PointF;->y:F

    .line 39
    .line 40
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇oo〇()F

    .line 41
    .line 42
    .line 43
    move-result v6

    .line 44
    move-object v0, p0

    .line 45
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->O8O〇(Landroid/graphics/Path;FFFFF)V

    .line 46
    .line 47
    .line 48
    :cond_1
    const/4 p1, 0x0

    .line 49
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇O〇80o08O(Z)V

    .line 50
    .line 51
    .line 52
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public OO8oO0o〇()Landroid/graphics/PointF;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇〇0o:Landroid/graphics/PointF;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OOO()Lcom/intsig/camscanner/doodle/widget/CopyLocation;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇8:Lcom/intsig/camscanner/doodle/widget/CopyLocation;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected OoO8(Landroid/graphics/Canvas;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->oO:Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Paint;->reset()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->oO:Landroid/graphics/Paint;

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇oo〇()F

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->oO:Landroid/graphics/Paint;

    .line 16
    .line 17
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->oO:Landroid/graphics/Paint;

    .line 23
    .line 24
    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    .line 25
    .line 26
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 27
    .line 28
    .line 29
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->oO:Landroid/graphics/Paint;

    .line 30
    .line 31
    const/4 v1, 0x1

    .line 32
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->oO:Landroid/graphics/Paint;

    .line 40
    .line 41
    invoke-interface {v0, p0, v1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;->config(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;Landroid/graphics/Paint;)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getColor()Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->oO:Landroid/graphics/Paint;

    .line 49
    .line 50
    invoke-interface {v0, p0, v1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;->config(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;Landroid/graphics/Paint;)V

    .line 51
    .line 52
    .line 53
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getShape()Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->oO:Landroid/graphics/Paint;

    .line 58
    .line 59
    invoke-interface {v0, p0, v1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;->config(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;Landroid/graphics/Paint;)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->O0o〇〇Oo()Landroid/graphics/Path;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->oO:Landroid/graphics/Paint;

    .line 67
    .line 68
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 69
    .line 70
    .line 71
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public o8O〇(FFFF)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇〇0o:Landroid/graphics/PointF;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->set(FF)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇08O8o〇0:Landroid/graphics/PointF;

    .line 7
    .line 8
    invoke-virtual {v0, p3, p4}, Landroid/graphics/PointF;->set(FF)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o〇0OOo〇0:Landroid/graphics/Path;

    .line 12
    .line 13
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 14
    .line 15
    .line 16
    sget-object v0, Lcom/intsig/camscanner/doodle/widget/DoodleShape;->ARROW:Lcom/intsig/camscanner/doodle/widget/DoodleShape;

    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getShape()Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-eqz v0, :cond_0

    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o〇0OOo〇0:Landroid/graphics/Path;

    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇〇0o:Landroid/graphics/PointF;

    .line 31
    .line 32
    iget v2, v0, Landroid/graphics/PointF;->x:F

    .line 33
    .line 34
    iget v3, v0, Landroid/graphics/PointF;->y:F

    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇08O8o〇0:Landroid/graphics/PointF;

    .line 37
    .line 38
    iget v4, v0, Landroid/graphics/PointF;->x:F

    .line 39
    .line 40
    iget v5, v0, Landroid/graphics/PointF;->y:F

    .line 41
    .line 42
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇oo〇()F

    .line 43
    .line 44
    .line 45
    move-result v6

    .line 46
    move-object v0, p0

    .line 47
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->O8O〇(Landroid/graphics/Path;FFFFF)V

    .line 48
    .line 49
    .line 50
    goto/16 :goto_1

    .line 51
    .line 52
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/doodle/widget/DoodleShape;->LINE:Lcom/intsig/camscanner/doodle/widget/DoodleShape;

    .line 53
    .line 54
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getShape()Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;

    .line 55
    .line 56
    .line 57
    move-result-object v1

    .line 58
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 59
    .line 60
    .line 61
    move-result v0

    .line 62
    if-eqz v0, :cond_1

    .line 63
    .line 64
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o〇0OOo〇0:Landroid/graphics/Path;

    .line 65
    .line 66
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇〇0o:Landroid/graphics/PointF;

    .line 67
    .line 68
    iget v2, v0, Landroid/graphics/PointF;->x:F

    .line 69
    .line 70
    iget v3, v0, Landroid/graphics/PointF;->y:F

    .line 71
    .line 72
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇08O8o〇0:Landroid/graphics/PointF;

    .line 73
    .line 74
    iget v4, v0, Landroid/graphics/PointF;->x:F

    .line 75
    .line 76
    iget v5, v0, Landroid/graphics/PointF;->y:F

    .line 77
    .line 78
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇oo〇()F

    .line 79
    .line 80
    .line 81
    move-result v6

    .line 82
    move-object v0, p0

    .line 83
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇o0O0O8(Landroid/graphics/Path;FFFFF)V

    .line 84
    .line 85
    .line 86
    goto :goto_1

    .line 87
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/doodle/widget/DoodleShape;->FILL_CIRCLE:Lcom/intsig/camscanner/doodle/widget/DoodleShape;

    .line 88
    .line 89
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getShape()Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;

    .line 90
    .line 91
    .line 92
    move-result-object v1

    .line 93
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 94
    .line 95
    .line 96
    move-result v0

    .line 97
    if-nez v0, :cond_4

    .line 98
    .line 99
    sget-object v0, Lcom/intsig/camscanner/doodle/widget/DoodleShape;->HOLLOW_CIRCLE:Lcom/intsig/camscanner/doodle/widget/DoodleShape;

    .line 100
    .line 101
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getShape()Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;

    .line 102
    .line 103
    .line 104
    move-result-object v1

    .line 105
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 106
    .line 107
    .line 108
    move-result v0

    .line 109
    if-eqz v0, :cond_2

    .line 110
    .line 111
    goto :goto_0

    .line 112
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/doodle/widget/DoodleShape;->FILL_RECT:Lcom/intsig/camscanner/doodle/widget/DoodleShape;

    .line 113
    .line 114
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getShape()Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;

    .line 115
    .line 116
    .line 117
    move-result-object v1

    .line 118
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    if-nez v0, :cond_3

    .line 123
    .line 124
    sget-object v0, Lcom/intsig/camscanner/doodle/widget/DoodleShape;->HOLLOW_RECT:Lcom/intsig/camscanner/doodle/widget/DoodleShape;

    .line 125
    .line 126
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getShape()Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;

    .line 127
    .line 128
    .line 129
    move-result-object v1

    .line 130
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    .line 131
    .line 132
    .line 133
    move-result v0

    .line 134
    if-eqz v0, :cond_5

    .line 135
    .line 136
    :cond_3
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o〇0OOo〇0:Landroid/graphics/Path;

    .line 137
    .line 138
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇〇0o:Landroid/graphics/PointF;

    .line 139
    .line 140
    iget v2, v0, Landroid/graphics/PointF;->x:F

    .line 141
    .line 142
    iget v3, v0, Landroid/graphics/PointF;->y:F

    .line 143
    .line 144
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇08O8o〇0:Landroid/graphics/PointF;

    .line 145
    .line 146
    iget v4, v0, Landroid/graphics/PointF;->x:F

    .line 147
    .line 148
    iget v5, v0, Landroid/graphics/PointF;->y:F

    .line 149
    .line 150
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇oo〇()F

    .line 151
    .line 152
    .line 153
    move-result v6

    .line 154
    move-object v0, p0

    .line 155
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->Oo〇O(Landroid/graphics/Path;FFFFF)V

    .line 156
    .line 157
    .line 158
    goto :goto_1

    .line 159
    :cond_4
    :goto_0
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o〇0OOo〇0:Landroid/graphics/Path;

    .line 160
    .line 161
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇〇0o:Landroid/graphics/PointF;

    .line 162
    .line 163
    iget v2, v0, Landroid/graphics/PointF;->x:F

    .line 164
    .line 165
    iget v3, v0, Landroid/graphics/PointF;->y:F

    .line 166
    .line 167
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇08O8o〇0:Landroid/graphics/PointF;

    .line 168
    .line 169
    iget v4, v0, Landroid/graphics/PointF;->x:F

    .line 170
    .line 171
    iget v5, v0, Landroid/graphics/PointF;->y:F

    .line 172
    .line 173
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇oo〇()F

    .line 174
    .line 175
    .line 176
    move-result v6

    .line 177
    move-object v0, p0

    .line 178
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->O0O8OO088(Landroid/graphics/Path;FFFFF)V

    .line 179
    .line 180
    .line 181
    :cond_5
    :goto_1
    const/4 v0, 0x1

    .line 182
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇O〇80o08O(Z)V

    .line 183
    .line 184
    .line 185
    return-void
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
.end method

.method public ooo〇8oO()Landroid/graphics/PointF;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇08O8o〇0:Landroid/graphics/PointF;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇0(F)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->o〇0(F)V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->Ooo()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected o〇8(Landroid/graphics/Rect;)V
    .locals 3

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o0O0(Landroid/graphics/Rect;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    const/4 v2, 0x0

    .line 13
    invoke-virtual {p1, v2, v2, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇O8o08O()Z
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    sget-object v1, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->ERASER:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    if-ne v0, v1, :cond_0

    .line 9
    .line 10
    return v2

    .line 11
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    sget-object v1, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->RECTANGLE:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 16
    .line 17
    if-ne v0, v1, :cond_1

    .line 18
    .line 19
    return v2

    .line 20
    :cond_1
    invoke-super {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->〇O8o08O()Z

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    return v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇O〇(Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇O〇(Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    sget-object v0, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->MOSAIC:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    if-ne p1, v0, :cond_0

    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getLocation()Landroid/graphics/PointF;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    iget p1, p1, Landroid/graphics/PointF;->x:F

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getLocation()Landroid/graphics/PointF;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    iget v0, v0, Landroid/graphics/PointF;->y:F

    .line 24
    .line 25
    invoke-virtual {p0, p1, v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->O8ooOoo〇(FFZ)V

    .line 26
    .line 27
    .line 28
    :cond_0
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇O〇80o08O(Z)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇o〇(F)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleRotatableItemBase;->〇o〇(F)V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->Ooo()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇〇o8(Landroid/graphics/Path;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o〇0OOo〇0:Landroid/graphics/Path;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->o〇0OOo〇0:Landroid/graphics/Path;

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Landroid/graphics/Path;->addPath(Landroid/graphics/Path;)V

    .line 9
    .line 10
    .line 11
    const/4 p1, 0x1

    .line 12
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodlePath;->〇O〇80o08O(Z)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

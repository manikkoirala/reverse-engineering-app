.class public Lcom/intsig/camscanner/doodle/widget/DoodleView;
.super Landroid/widget/FrameLayout;
.source "DoodleView.java"

# interfaces
.implements Lcom/intsig/camscanner/doodle/widget/core/IDoodle;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;,
        Lcom/intsig/camscanner/doodle/widget/DoodleView$BackgroundView;
    }
.end annotation


# static fields
.field private static final o00〇88〇08:F

.field private static final o880:F

.field private static final oooO888:I


# instance fields
.field private O0O:F

.field private O88O:Z

.field private O8o08O8O:F

.field private O8o〇O0:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;",
            ">;"
        }
    .end annotation
.end field

.field private O8〇o〇88:I

.field private OO:F

.field private OO〇00〇8oO:F

.field private OO〇OOo:Z

.field private Oo0O0o8:Z

.field private Oo0〇Ooo:Z

.field private Oo80:Z

.field private Ooo08:F

.field private O〇08oOOO0:F

.field private O〇O:Landroid/graphics/Canvas;

.field private O〇o88o08〇:Z

.field private o0:Lcom/intsig/camscanner/doodle/widget/IDoodleListener;

.field private o0OoOOo0:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;",
            "Lcom/intsig/camscanner/doodle/widget/core/IDoodleTouchDetector;",
            ">;"
        }
    .end annotation
.end field

.field private o8O:Landroid/graphics/Matrix;

.field private o8o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;",
            ">;"
        }
    .end annotation
.end field

.field private o8oOOo:Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;

.field private o8〇OO:Landroid/graphics/Path;

.field private o8〇OO0〇0o:F

.field private oO00〇o:Landroid/graphics/RectF;

.field private oOO0880O:Landroid/graphics/PointF;

.field private oOO8:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;",
            ">;"
        }
    .end annotation
.end field

.field private oOO〇〇:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;",
            ">;"
        }
    .end annotation
.end field

.field private oOo0:F

.field private oOoo80oO:Z

.field private oOo〇08〇:Z

.field private oOo〇8o008:F

.field private oO〇8O8oOo:I

.field private oO〇oo:Lcom/intsig/camscanner/doodle/widget/core/IDropperTouchListener;

.field private oo8ooo8O:Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

.field private ooO:Landroid/graphics/Paint;

.field private ooo0〇〇O:F

.field private o〇00O:I

.field private o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;

.field private o〇o〇Oo88:Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;

.field private 〇00O0:F

.field private 〇080OO8〇0:F

.field private 〇08O〇00〇o:I

.field private 〇08〇o0O:F

.field private 〇0O:F

.field private 〇0O〇O00O:Lcom/intsig/camscanner/doodle/widget/core/IDoodleTouchDetector;

.field private final 〇800OO〇0O:Z

.field private 〇80O8o8O〇:Landroid/view/View$OnTouchListener;

.field private 〇8〇o88:Lcom/intsig/camscanner/doodle/widget/DoodleView$BackgroundView;

.field private 〇8〇oO〇〇8o:F

.field private 〇O8oOo0:Z

.field private 〇OO8ooO8〇:Landroid/graphics/Paint;

.field private final 〇OOo8〇0:Landroid/graphics/Bitmap;

.field private 〇OO〇00〇0O:I

.field private 〇O〇〇O8:Z

.field private 〇o0O:Z

.field private 〇oo〇O〇80:Landroid/graphics/Bitmap;

.field private 〇〇08O:F

.field private 〇〇o〇:F

.field private 〇〇〇0o〇〇0:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    const/high16 v0, 0x40800000    # 4.0f

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/util/DoodleUtils;->〇080(F)I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    sput v1, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oooO888:I

    .line 8
    .line 9
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/util/DoodleUtils;->〇080(F)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    int-to-float v0, v0

    .line 14
    sput v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o880:F

    .line 15
    .line 16
    const/high16 v0, 0x41100000    # 9.0f

    .line 17
    .line 18
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/util/DoodleUtils;->〇080(F)I

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    int-to-float v0, v0

    .line 23
    sput v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o00〇88〇08:F

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/graphics/Bitmap;Lcom/intsig/camscanner/doodle/widget/IDoodleListener;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    .line 1
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/doodle/widget/DoodleView;-><init>(Landroid/content/Context;Landroid/graphics/Bitmap;ZLcom/intsig/camscanner/doodle/widget/IDoodleListener;Lcom/intsig/camscanner/doodle/widget/core/IDoodleTouchDetector;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/graphics/Bitmap;ZLcom/intsig/camscanner/doodle/widget/IDoodleListener;Lcom/intsig/camscanner/doodle/widget/core/IDoodleTouchDetector;)V
    .locals 4

    .line 2
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/high16 v0, 0x3f800000    # 1.0f

    .line 3
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0O:F

    .line 4
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OO〇00〇8oO:F

    const/4 v1, 0x0

    .line 5
    iput v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8〇OO0〇0o:F

    iput v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8〇oO〇〇8o:F

    const/high16 v2, 0x3e800000    # 0.25f

    .line 6
    iput v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->ooo0〇〇O:F

    const/high16 v2, 0x40a00000    # 5.0f

    .line 7
    iput v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇08O:F

    const/4 v2, 0x0

    .line 8
    iput-boolean v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇o0O:Z

    .line 9
    iput-boolean v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O88O:Z

    .line 10
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO〇〇:Ljava/util/List;

    .line 11
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8o:Ljava/util/List;

    .line 12
    iput-boolean v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->Oo80:Z

    const/4 v3, 0x1

    .line 13
    iput-boolean v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O〇o88o08〇:Z

    .line 14
    iput v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->Ooo08:F

    .line 15
    sget v1, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oooO888:I

    iput v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OO〇00〇0O:I

    .line 16
    iput-boolean v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->Oo0〇Ooo:Z

    .line 17
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇〇0o〇〇0:F

    .line 18
    iput v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO〇8O8oOo:I

    .line 19
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o0OoOOo0:Ljava/util/Map;

    .line 20
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iput-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO00〇o:Landroid/graphics/RectF;

    .line 21
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    iput-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO0880O:Landroid/graphics/PointF;

    .line 22
    iput-boolean v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOoo80oO:Z

    .line 23
    iput-boolean v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->Oo0O0o8:Z

    .line 24
    iput-boolean v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OO〇OOo:Z

    .line 25
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8o〇O0:Ljava/util/List;

    .line 26
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO8:Ljava/util/List;

    .line 27
    iput v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8〇o〇88:I

    .line 28
    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iput-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8O:Landroid/graphics/Matrix;

    .line 29
    iput-boolean v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOo〇08〇:Z

    .line 30
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 31
    iput-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OOo8〇0:Landroid/graphics/Bitmap;

    .line 32
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object p2

    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    if-eq p2, v1, :cond_0

    const-string p2, "DoodleView"

    const-string v1, "the bitmap may contain alpha, which will cause eraser don\'t work well."

    .line 33
    invoke-static {p2, v1}, Lcom/intsig/log/LogUtils;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    :cond_0
    iput-object p4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o0:Lcom/intsig/camscanner/doodle/widget/IDoodleListener;

    if-eqz p4, :cond_1

    .line 35
    iput-boolean p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇800OO〇0O:Z

    .line 36
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OO〇00〇8oO:F

    .line 37
    new-instance p2, Lcom/intsig/camscanner/doodle/widget/DoodleColor;

    const/high16 p3, -0x10000

    invoke-direct {p2, p3}, Lcom/intsig/camscanner/doodle/widget/DoodleColor;-><init>(I)V

    iput-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;

    .line 38
    sget-object p2, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->BRUSH:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    iput-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oo8ooo8O:Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 39
    sget-object p2, Lcom/intsig/camscanner/doodle/widget/DoodleShape;->HAND_WRITE:Lcom/intsig/camscanner/doodle/widget/DoodleShape;

    iput-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;

    .line 40
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    iput-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OO8ooO8〇:Landroid/graphics/Paint;

    const p3, -0x55000001

    .line 41
    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setColor(I)V

    .line 42
    iget-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OO8ooO8〇:Landroid/graphics/Paint;

    sget-object p3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 43
    iget-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OO8ooO8〇:Landroid/graphics/Paint;

    invoke-virtual {p2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 44
    iget-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OO8ooO8〇:Landroid/graphics/Paint;

    sget-object p3, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 45
    iget-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OO8ooO8〇:Landroid/graphics/Paint;

    sget-object p3, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 46
    iget-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OO8ooO8〇:Landroid/graphics/Paint;

    sget p3, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o880:F

    invoke-static {p3}, Lcom/intsig/camscanner/doodle/util/DoodleUtils;->〇080(F)I

    move-result p3

    int-to-float p3, p3

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 47
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    iput-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->ooO:Landroid/graphics/Paint;

    .line 48
    sget-object p3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 49
    iget-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->ooO:Landroid/graphics/Paint;

    invoke-virtual {p2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 50
    iget-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->ooO:Landroid/graphics/Paint;

    sget-object p3, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 51
    iget-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->ooO:Landroid/graphics/Paint;

    sget-object p3, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 52
    iget-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->ooO:Landroid/graphics/Paint;

    const/high16 p3, 0x3fc00000    # 1.5f

    invoke-static {p3}, Lcom/intsig/camscanner/doodle/util/DoodleUtils;->〇080(F)I

    move-result p3

    int-to-float p3, p3

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 53
    iput-object p5, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0O〇O00O:Lcom/intsig/camscanner/doodle/widget/core/IDoodleTouchDetector;

    .line 54
    new-instance p2, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;

    invoke-direct {p2, p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;-><init>(Lcom/intsig/camscanner/doodle/widget/DoodleView;Landroid/content/Context;)V

    iput-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇o〇Oo88:Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;

    .line 55
    new-instance p2, Lcom/intsig/camscanner/doodle/widget/DoodleView$BackgroundView;

    invoke-direct {p2, p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView$BackgroundView;-><init>(Lcom/intsig/camscanner/doodle/widget/DoodleView;Landroid/content/Context;)V

    iput-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8〇o88:Lcom/intsig/camscanner/doodle/widget/DoodleView$BackgroundView;

    .line 56
    new-instance p1, Landroid/view/ViewGroup$LayoutParams;

    const/4 p3, -0x1

    invoke-direct {p1, p3, p3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p2, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 57
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇o〇Oo88:Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;

    new-instance p2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {p2, p3, p3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    .line 58
    :cond_1
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "IDoodleListener is null!!!"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method static bridge synthetic O8(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Lcom/intsig/camscanner/doodle/widget/core/IDoodleTouchDetector;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0O〇O00O:Lcom/intsig/camscanner/doodle/widget/core/IDoodleTouchDetector;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OO0o〇〇(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Ljava/util/List;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8o〇O0:Ljava/util/List;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic Oo08(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Landroid/graphics/Bitmap;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇oo〇O〇80:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private OoO8(Landroid/graphics/Canvas;)V
    .locals 9

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇O8oOo0:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->Oo0〇Ooo:Z

    .line 7
    .line 8
    if-eqz v0, :cond_2

    .line 9
    .line 10
    iget-boolean v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->Oo80:Z

    .line 11
    .line 12
    if-eqz v0, :cond_2

    .line 13
    .line 14
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->Ooo08:F

    .line 15
    .line 16
    const/4 v1, 0x0

    .line 17
    cmpl-float v0, v0, v1

    .line 18
    .line 19
    if-lez v0, :cond_2

    .line 20
    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇00()V

    .line 22
    .line 23
    .line 24
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 25
    .line 26
    .line 27
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O〇08oOOO0:F

    .line 28
    .line 29
    const/high16 v6, 0x40000000    # 2.0f

    .line 30
    .line 31
    mul-float v0, v0, v6

    .line 32
    .line 33
    sget v1, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oooO888:I

    .line 34
    .line 35
    int-to-float v2, v1

    .line 36
    add-float/2addr v0, v2

    .line 37
    iget v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇o〇:F

    .line 38
    .line 39
    cmpg-float v2, v2, v0

    .line 40
    .line 41
    if-gtz v2, :cond_1

    .line 42
    .line 43
    iget v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇08〇o0O:F

    .line 44
    .line 45
    cmpg-float v2, v2, v0

    .line 46
    .line 47
    if-gtz v2, :cond_1

    .line 48
    .line 49
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    int-to-float v1, v1

    .line 54
    sub-float/2addr v1, v0

    .line 55
    iput v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇00O0:F

    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_1
    int-to-float v0, v1

    .line 59
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇00O0:F

    .line 60
    .line 61
    :goto_0
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇00O0:F

    .line 62
    .line 63
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OO〇00〇0O:I

    .line 64
    .line 65
    int-to-float v1, v1

    .line 66
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 67
    .line 68
    .line 69
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8〇OO:Landroid/graphics/Path;

    .line 70
    .line 71
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 72
    .line 73
    .line 74
    const/high16 v0, -0x1000000

    .line 75
    .line 76
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 80
    .line 81
    .line 82
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->Ooo08:F

    .line 83
    .line 84
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OO〇00〇8oO:F

    .line 85
    .line 86
    div-float/2addr v0, v1

    .line 87
    invoke-virtual {p1, v0, v0}, Landroid/graphics/Canvas;->scale(FF)V

    .line 88
    .line 89
    .line 90
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇08〇o0O:F

    .line 91
    .line 92
    neg-float v1, v1

    .line 93
    iget v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O〇08oOOO0:F

    .line 94
    .line 95
    div-float v3, v2, v0

    .line 96
    .line 97
    add-float/2addr v1, v3

    .line 98
    iget v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇o〇:F

    .line 99
    .line 100
    neg-float v3, v3

    .line 101
    div-float/2addr v2, v0

    .line 102
    add-float/2addr v3, v2

    .line 103
    invoke-virtual {p1, v1, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 104
    .line 105
    .line 106
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 107
    .line 108
    .line 109
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 110
    .line 111
    .line 112
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OOo8〇0:Landroid/graphics/Bitmap;

    .line 113
    .line 114
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇08〇o0O:F

    .line 115
    .line 116
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 117
    .line 118
    .line 119
    move-result v1

    .line 120
    float-to-int v1, v1

    .line 121
    iget v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇o〇:F

    .line 122
    .line 123
    invoke-virtual {p0, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 124
    .line 125
    .line 126
    move-result v2

    .line 127
    float-to-int v2, v2

    .line 128
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/doodle/util/DoodleImageUtils;->〇o00〇〇Oo(Landroid/graphics/Bitmap;II)I

    .line 129
    .line 130
    .line 131
    move-result v7

    .line 132
    invoke-static {v7}, Lcom/intsig/camscanner/doodle/util/DoodleImageUtils;->〇080(I)I

    .line 133
    .line 134
    .line 135
    move-result v0

    .line 136
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->ooO:Landroid/graphics/Paint;

    .line 137
    .line 138
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 139
    .line 140
    .line 141
    iget v4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O〇08oOOO0:F

    .line 142
    .line 143
    sget v8, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o00〇88〇08:F

    .line 144
    .line 145
    div-float v0, v8, v6

    .line 146
    .line 147
    sub-float v1, v4, v0

    .line 148
    .line 149
    add-float v3, v1, v8

    .line 150
    .line 151
    iget-object v5, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->ooO:Landroid/graphics/Paint;

    .line 152
    .line 153
    move-object v0, p1

    .line 154
    move v2, v4

    .line 155
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 156
    .line 157
    .line 158
    iget v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O〇08oOOO0:F

    .line 159
    .line 160
    div-float v0, v8, v6

    .line 161
    .line 162
    sub-float v2, v3, v0

    .line 163
    .line 164
    add-float v4, v2, v8

    .line 165
    .line 166
    iget-object v5, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->ooO:Landroid/graphics/Paint;

    .line 167
    .line 168
    move-object v0, p1

    .line 169
    move v1, v3

    .line 170
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 171
    .line 172
    .line 173
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OO8ooO8〇:Landroid/graphics/Paint;

    .line 174
    .line 175
    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 176
    .line 177
    .line 178
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O〇08oOOO0:F

    .line 179
    .line 180
    sget v1, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o880:F

    .line 181
    .line 182
    div-float/2addr v1, v6

    .line 183
    sub-float v1, v0, v1

    .line 184
    .line 185
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OO8ooO8〇:Landroid/graphics/Paint;

    .line 186
    .line 187
    invoke-static {p1, v0, v0, v1, v2}, Lcom/intsig/camscanner/doodle/util/DrawUtil;->〇o00〇〇Oo(Landroid/graphics/Canvas;FFFLandroid/graphics/Paint;)V

    .line 188
    .line 189
    .line 190
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 191
    .line 192
    .line 193
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oo〇()V

    .line 194
    .line 195
    .line 196
    :cond_2
    return-void
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method static bridge synthetic Oooo8o0〇(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Ljava/util/Map;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o0OoOOo0:Ljava/util/Map;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o800o8O(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇800OO〇0O:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    check-cast v0, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;

    .line 21
    .line 22
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O〇O:Landroid/graphics/Canvas;

    .line 23
    .line 24
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->draw(Landroid/graphics/Canvas;)V

    .line 25
    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_1
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic oO80(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇o0O:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private oo88o8O(I)Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8〇o〇88:I

    .line 2
    .line 3
    and-int/2addr p1, v0

    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    const/4 p1, 0x1

    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 p1, 0x0

    .line 9
    :goto_0
    return p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o〇0(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Lcom/intsig/camscanner/doodle/widget/IDoodleListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o0:Lcom/intsig/camscanner/doodle/widget/IDoodleListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o〇O8〇〇o()V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇800OO〇0O:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇oo〇O〇80:Landroid/graphics/Bitmap;

    .line 7
    .line 8
    if-eqz v0, :cond_1

    .line 9
    .line 10
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 11
    .line 12
    .line 13
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OOo8〇0:Landroid/graphics/Bitmap;

    .line 14
    .line 15
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    const/4 v2, 0x1

    .line 20
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇oo〇O〇80:Landroid/graphics/Bitmap;

    .line 25
    .line 26
    new-instance v0, Landroid/graphics/Canvas;

    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇oo〇O〇80:Landroid/graphics/Bitmap;

    .line 29
    .line 30
    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 31
    .line 32
    .line 33
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O〇O:Landroid/graphics/Canvas;

    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇00()V
    .locals 5

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOo〇08〇:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/4 v0, 0x1

    .line 7
    iput-boolean v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOo〇08〇:Z

    .line 8
    .line 9
    const/high16 v1, 0x40800000    # 4.0f

    .line 10
    .line 11
    invoke-static {v1}, Lcom/intsig/camscanner/doodle/util/DoodleUtils;->〇080(F)I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    int-to-float v1, v1

    .line 16
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OO8ooO8〇:Landroid/graphics/Paint;

    .line 17
    .line 18
    const/high16 v3, 0x33000000

    .line 19
    .line 20
    const/4 v4, 0x0

    .line 21
    invoke-virtual {v2, v1, v4, v4, v3}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 22
    .line 23
    .line 24
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OO8ooO8〇:Landroid/graphics/Paint;

    .line 25
    .line 26
    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇00〇8(Z)V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇800OO〇0O:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇O8〇〇o()V

    .line 7
    .line 8
    .line 9
    if-eqz p1, :cond_1

    .line 10
    .line 11
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO〇〇:Ljava/util/List;

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_1
    new-instance p1, Ljava/util/ArrayList;

    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO〇〇:Ljava/util/List;

    .line 17
    .line 18
    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 19
    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8o〇O0:Ljava/util/List;

    .line 22
    .line 23
    invoke-interface {p1, v0}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 24
    .line 25
    .line 26
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    if-eqz v0, :cond_2

    .line 35
    .line 36
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    check-cast v0, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;

    .line 41
    .line 42
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O〇O:Landroid/graphics/Canvas;

    .line 43
    .line 44
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->draw(Landroid/graphics/Canvas;)V

    .line 45
    .line 46
    .line 47
    goto :goto_1

    .line 48
    :cond_2
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private 〇0〇O0088o(I)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8〇o〇88:I

    .line 2
    .line 3
    not-int p1, p1

    .line 4
    and-int/2addr p1, v0

    .line 5
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8〇o〇88:I

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇80〇808〇O(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Ljava/util/List;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO〇〇:Ljava/util/List;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇8o8o〇(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇800OO〇0O:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇O8o08O(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oo8ooo8O:Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇O〇(I)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8〇o〇88:I

    .line 2
    .line 3
    or-int/2addr p1, v0

    .line 4
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8〇o〇88:I

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇o()V
    .locals 1

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇O〇(I)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->refresh()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇O〇〇O8:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇oo〇()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OOo8〇0:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OOo8〇0:Landroid/graphics/Bitmap;

    .line 8
    .line 9
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    int-to-float v0, v0

    .line 14
    const/high16 v2, 0x3f800000    # 1.0f

    .line 15
    .line 16
    mul-float v3, v0, v2

    .line 17
    .line 18
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 19
    .line 20
    .line 21
    move-result v4

    .line 22
    int-to-float v4, v4

    .line 23
    div-float/2addr v3, v4

    .line 24
    int-to-float v1, v1

    .line 25
    mul-float v4, v1, v2

    .line 26
    .line 27
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 28
    .line 29
    .line 30
    move-result v5

    .line 31
    int-to-float v5, v5

    .line 32
    div-float/2addr v4, v5

    .line 33
    cmpl-float v5, v3, v4

    .line 34
    .line 35
    if-lez v5, :cond_0

    .line 36
    .line 37
    div-float v0, v2, v3

    .line 38
    .line 39
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OO:F

    .line 40
    .line 41
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇00O:I

    .line 46
    .line 47
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OO:F

    .line 48
    .line 49
    mul-float v1, v1, v0

    .line 50
    .line 51
    float-to-int v0, v1

    .line 52
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇08O〇00〇o:I

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_0
    div-float v1, v2, v4

    .line 56
    .line 57
    iput v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OO:F

    .line 58
    .line 59
    mul-float v0, v0, v1

    .line 60
    .line 61
    float-to-int v0, v0

    .line 62
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇00O:I

    .line 63
    .line 64
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 65
    .line 66
    .line 67
    move-result v0

    .line 68
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇08O〇00〇o:I

    .line 69
    .line 70
    :goto_0
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇00O:I

    .line 75
    .line 76
    sub-int/2addr v0, v1

    .line 77
    int-to-float v0, v0

    .line 78
    const/high16 v1, 0x40000000    # 2.0f

    .line 79
    .line 80
    div-float/2addr v0, v1

    .line 81
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8o08O8O:F

    .line 82
    .line 83
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 84
    .line 85
    .line 86
    move-result v0

    .line 87
    iget v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇08O〇00〇o:I

    .line 88
    .line 89
    sub-int/2addr v0, v3

    .line 90
    int-to-float v0, v0

    .line 91
    div-float/2addr v0, v1

    .line 92
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇080OO8〇0:F

    .line 93
    .line 94
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 95
    .line 96
    .line 97
    move-result v0

    .line 98
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 99
    .line 100
    .line 101
    move-result v1

    .line 102
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    .line 103
    .line 104
    .line 105
    move-result v0

    .line 106
    int-to-float v0, v0

    .line 107
    const/high16 v1, 0x40800000    # 4.0f

    .line 108
    .line 109
    div-float/2addr v0, v1

    .line 110
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O〇08oOOO0:F

    .line 111
    .line 112
    const/high16 v1, 0x42700000    # 60.0f

    .line 113
    .line 114
    invoke-static {v1}, Lcom/intsig/camscanner/doodle/util/DoodleUtils;->〇080(F)I

    .line 115
    .line 116
    .line 117
    move-result v1

    .line 118
    int-to-float v1, v1

    .line 119
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    .line 120
    .line 121
    .line 122
    move-result v0

    .line 123
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O〇08oOOO0:F

    .line 124
    .line 125
    new-instance v0, Landroid/graphics/Path;

    .line 126
    .line 127
    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 128
    .line 129
    .line 130
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8〇OO:Landroid/graphics/Path;

    .line 131
    .line 132
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O〇08oOOO0:F

    .line 133
    .line 134
    sget-object v3, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    .line 135
    .line 136
    invoke-virtual {v0, v1, v1, v1, v3}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 137
    .line 138
    .line 139
    invoke-static {v2}, Lcom/intsig/camscanner/doodle/util/DoodleUtils;->〇080(F)I

    .line 140
    .line 141
    .line 142
    move-result v0

    .line 143
    int-to-float v0, v0

    .line 144
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OO:F

    .line 145
    .line 146
    div-float/2addr v0, v1

    .line 147
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇〇0o〇〇0:F

    .line 148
    .line 149
    iget-boolean v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O88O:Z

    .line 150
    .line 151
    if-nez v1, :cond_1

    .line 152
    .line 153
    const/high16 v1, 0x40c00000    # 6.0f

    .line 154
    .line 155
    mul-float v0, v0, v1

    .line 156
    .line 157
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O0O:F

    .line 158
    .line 159
    :cond_1
    const/4 v0, 0x0

    .line 160
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8〇oO〇〇8o:F

    .line 161
    .line 162
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8〇OO0〇0o:F

    .line 163
    .line 164
    iput v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OO〇00〇8oO:F

    .line 165
    .line 166
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇O8〇〇o()V

    .line 167
    .line 168
    .line 169
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇o()V

    .line 170
    .line 171
    .line 172
    return-void
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Landroid/graphics/Bitmap;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OOo8〇0:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇〇808〇(Lcom/intsig/camscanner/doodle/widget/DoodleView;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇00〇8(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇〇888(Lcom/intsig/camscanner/doodle/widget/DoodleView;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO〇8O8oOo:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇〇8O0〇8(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_2

    .line 2
    .line 3
    invoke-interface {p1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->〇〇888()Lcom/intsig/camscanner/doodle/widget/core/IDoodle;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-ne p0, v0, :cond_1

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO〇〇:Ljava/util/List;

    .line 10
    .line 11
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO〇〇:Ljava/util/List;

    .line 18
    .line 19
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 20
    .line 21
    .line 22
    invoke-interface {p1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->OO0o〇〇()V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO8:Ljava/util/List;

    .line 26
    .line 27
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28
    .line 29
    .line 30
    const/4 p1, 0x4

    .line 31
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇O〇(I)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->refresh()V

    .line 35
    .line 36
    .line 37
    return-void

    .line 38
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    .line 39
    .line 40
    const-string v0, "the item has been added"

    .line 41
    .line 42
    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    throw p1

    .line 46
    :cond_1
    new-instance p1, Ljava/lang/RuntimeException;

    .line 47
    .line 48
    const-string v0, "the object Doodle is illegal"

    .line 49
    .line 50
    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    throw p1

    .line 54
    :cond_2
    new-instance p1, Ljava/lang/RuntimeException;

    .line 55
    .line 56
    const-string v0, "item is null"

    .line 57
    .line 58
    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    throw p1
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method


# virtual methods
.method public O08000()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8〇0〇o〇O(I)Z

    .line 3
    .line 4
    .line 5
    move-result v0

    .line 6
    return v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O8ooOoo〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOoo80oO:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O8〇o(I)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8o:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    return v1

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    :goto_0
    if-ge v0, p1, :cond_1

    .line 13
    .line 14
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8o:Ljava/util/List;

    .line 15
    .line 16
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    if-nez v2, :cond_1

    .line 21
    .line 22
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8o:Ljava/util/List;

    .line 23
    .line 24
    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    check-cast v2, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;

    .line 29
    .line 30
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇8O0〇8(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;)V

    .line 31
    .line 32
    .line 33
    add-int/lit8 v0, v0, 0x1

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    const/4 p1, 0x1

    .line 37
    return p1
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public OOO〇O0(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇800OO〇0O:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8o〇O0:Ljava/util/List;

    .line 7
    .line 8
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_2

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO〇〇:Ljava/util/List;

    .line 15
    .line 16
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    const/4 p1, 0x2

    .line 23
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇O〇(I)V

    .line 24
    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇O00(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;)V

    .line 28
    .line 29
    .line 30
    :cond_2
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->refresh()V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public Oo8Oo00oo(FF)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8〇OO0〇0o:F

    .line 2
    .line 3
    iput p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8〇oO〇〇8o:F

    .line 4
    .line 5
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇o()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public O〇8O8〇008()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇O8oOo0:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OOo8〇0:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    const/4 v0, 0x2

    .line 11
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oo88o8O(I)Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    const/4 v2, 0x4

    .line 16
    const/16 v3, 0x8

    .line 17
    .line 18
    if-eqz v1, :cond_1

    .line 19
    .line 20
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0〇O0088o(I)V

    .line 21
    .line 22
    .line 23
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0〇O0088o(I)V

    .line 24
    .line 25
    .line 26
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0〇O0088o(I)V

    .line 27
    .line 28
    .line 29
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇00〇8(Z)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO8:Ljava/util/List;

    .line 34
    .line 35
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 36
    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8〇o88:Lcom/intsig/camscanner/doodle/widget/DoodleView$BackgroundView;

    .line 39
    .line 40
    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_1
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oo88o8O(I)Z

    .line 45
    .line 46
    .line 47
    move-result v0

    .line 48
    if-eqz v0, :cond_2

    .line 49
    .line 50
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0〇O0088o(I)V

    .line 51
    .line 52
    .line 53
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0〇O0088o(I)V

    .line 54
    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO8:Ljava/util/List;

    .line 57
    .line 58
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o800o8O(Ljava/util/List;)V

    .line 59
    .line 60
    .line 61
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO8:Ljava/util/List;

    .line 62
    .line 63
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 64
    .line 65
    .line 66
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8〇o88:Lcom/intsig/camscanner/doodle/widget/DoodleView$BackgroundView;

    .line 67
    .line 68
    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 69
    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_2
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oo88o8O(I)Z

    .line 73
    .line 74
    .line 75
    move-result v0

    .line 76
    if-eqz v0, :cond_3

    .line 77
    .line 78
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0〇O0088o(I)V

    .line 79
    .line 80
    .line 81
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8〇o88:Lcom/intsig/camscanner/doodle/widget/DoodleView$BackgroundView;

    .line 82
    .line 83
    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 84
    .line 85
    .line 86
    :cond_3
    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 87
    .line 88
    .line 89
    move-result v0

    .line 90
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 91
    .line 92
    .line 93
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 94
    .line 95
    .line 96
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OoO8(Landroid/graphics/Canvas;)V

    .line 97
    .line 98
    .line 99
    return-void
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇80O8o8O〇:Landroid/view/View$OnTouchListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0, p0, p1}, Landroid/view/View$OnTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x1

    .line 12
    return p1

    .line 13
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇08〇o0O:F

    .line 18
    .line 19
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇o〇:F

    .line 24
    .line 25
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8O:Landroid/graphics/Matrix;

    .line 30
    .line 31
    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 32
    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8O:Landroid/graphics/Matrix;

    .line 35
    .line 36
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO〇8O8oOo:I

    .line 37
    .line 38
    neg-int v1, v1

    .line 39
    int-to-float v1, v1

    .line 40
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    int-to-float v2, v2

    .line 45
    const/high16 v3, 0x40000000    # 2.0f

    .line 46
    .line 47
    div-float/2addr v2, v3

    .line 48
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 49
    .line 50
    .line 51
    move-result v4

    .line 52
    int-to-float v4, v4

    .line 53
    div-float/2addr v4, v3

    .line 54
    invoke-virtual {v0, v1, v2, v4}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 55
    .line 56
    .line 57
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8O:Landroid/graphics/Matrix;

    .line 58
    .line 59
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->transform(Landroid/graphics/Matrix;)V

    .line 60
    .line 61
    .line 62
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇o〇Oo88:Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;

    .line 63
    .line 64
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 65
    .line 66
    .line 67
    move-result v0

    .line 68
    invoke-virtual {p1}, Landroid/view/MotionEvent;->recycle()V

    .line 69
    .line 70
    .line 71
    return v0
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public getAllItem()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO〇〇:Ljava/util/List;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 6
    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getAllRedoItem()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8o:Ljava/util/List;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 6
    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getAllScale()F
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OO:F

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0O:F

    .line 4
    .line 5
    mul-float v0, v0, v1

    .line 6
    .line 7
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OO〇00〇8oO:F

    .line 8
    .line 9
    mul-float v0, v0, v1

    .line 10
    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getAllTranX()F
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8o08O8O:F

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOo〇8o008:F

    .line 4
    .line 5
    add-float/2addr v0, v1

    .line 6
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8〇OO0〇0o:F

    .line 7
    .line 8
    add-float/2addr v0, v1

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getAllTranY()F
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇080OO8〇0:F

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOo0:F

    .line 4
    .line 5
    add-float/2addr v0, v1

    .line 6
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8〇oO〇〇8o:F

    .line 7
    .line 8
    add-float/2addr v0, v1

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OOo8〇0:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCenterHeight()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇08O〇00〇o:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCenterScale()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OO:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCenterWidth()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇00O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCentreTranX()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8o08O8O:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getCentreTranY()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇080OO8〇0:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getColor()Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDefaultTouchDetector()Lcom/intsig/camscanner/doodle/widget/core/IDoodleTouchDetector;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0O〇O00O:Lcom/intsig/camscanner/doodle/widget/core/IDoodleTouchDetector;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDoodleBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OOo8〇0:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDoodleBound()Landroid/graphics/RectF;
    .locals 17

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    iget v1, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇00O:I

    .line 4
    .line 5
    int-to-float v1, v1

    .line 6
    iget v2, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0O:F

    .line 7
    .line 8
    mul-float v1, v1, v2

    .line 9
    .line 10
    iget v3, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OO〇00〇8oO:F

    .line 11
    .line 12
    mul-float v1, v1, v3

    .line 13
    .line 14
    iget v4, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇08O〇00〇o:I

    .line 15
    .line 16
    int-to-float v4, v4

    .line 17
    mul-float v4, v4, v2

    .line 18
    .line 19
    mul-float v4, v4, v3

    .line 20
    .line 21
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getWidth()I

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    int-to-float v2, v2

    .line 26
    const/high16 v3, 0x40000000    # 2.0f

    .line 27
    .line 28
    div-float/2addr v2, v3

    .line 29
    invoke-virtual/range {p0 .. p0}, Landroid/view/View;->getHeight()I

    .line 30
    .line 31
    .line 32
    move-result v5

    .line 33
    int-to-float v5, v5

    .line 34
    div-float v3, v5, v3

    .line 35
    .line 36
    iget v5, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO〇8O8oOo:I

    .line 37
    .line 38
    rem-int/lit8 v6, v5, 0x5a

    .line 39
    .line 40
    const/4 v7, 0x0

    .line 41
    if-nez v6, :cond_4

    .line 42
    .line 43
    if-nez v5, :cond_0

    .line 44
    .line 45
    iget-object v5, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO0880O:Landroid/graphics/PointF;

    .line 46
    .line 47
    invoke-virtual {v0, v7}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇〇0〇〇0(F)F

    .line 48
    .line 49
    .line 50
    move-result v6

    .line 51
    iput v6, v5, Landroid/graphics/PointF;->x:F

    .line 52
    .line 53
    iget-object v5, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO0880O:Landroid/graphics/PointF;

    .line 54
    .line 55
    invoke-virtual {v0, v7}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇0OOo〇0(F)F

    .line 56
    .line 57
    .line 58
    move-result v6

    .line 59
    iput v6, v5, Landroid/graphics/PointF;->y:F

    .line 60
    .line 61
    goto :goto_1

    .line 62
    :cond_0
    const/16 v6, 0x5a

    .line 63
    .line 64
    if-ne v5, v6, :cond_1

    .line 65
    .line 66
    iget-object v5, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO0880O:Landroid/graphics/PointF;

    .line 67
    .line 68
    invoke-virtual {v0, v7}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇〇0〇〇0(F)F

    .line 69
    .line 70
    .line 71
    move-result v6

    .line 72
    iput v6, v5, Landroid/graphics/PointF;->x:F

    .line 73
    .line 74
    iget-object v5, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO0880O:Landroid/graphics/PointF;

    .line 75
    .line 76
    iget-object v6, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OOo8〇0:Landroid/graphics/Bitmap;

    .line 77
    .line 78
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    .line 79
    .line 80
    .line 81
    move-result v6

    .line 82
    int-to-float v6, v6

    .line 83
    invoke-virtual {v0, v6}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇0OOo〇0(F)F

    .line 84
    .line 85
    .line 86
    move-result v6

    .line 87
    iput v6, v5, Landroid/graphics/PointF;->y:F

    .line 88
    .line 89
    goto :goto_0

    .line 90
    :cond_1
    const/16 v6, 0xb4

    .line 91
    .line 92
    if-ne v5, v6, :cond_2

    .line 93
    .line 94
    iget-object v5, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO0880O:Landroid/graphics/PointF;

    .line 95
    .line 96
    iget-object v6, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OOo8〇0:Landroid/graphics/Bitmap;

    .line 97
    .line 98
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    .line 99
    .line 100
    .line 101
    move-result v6

    .line 102
    int-to-float v6, v6

    .line 103
    invoke-virtual {v0, v6}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇〇0〇〇0(F)F

    .line 104
    .line 105
    .line 106
    move-result v6

    .line 107
    iput v6, v5, Landroid/graphics/PointF;->x:F

    .line 108
    .line 109
    iget-object v5, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO0880O:Landroid/graphics/PointF;

    .line 110
    .line 111
    iget-object v6, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OOo8〇0:Landroid/graphics/Bitmap;

    .line 112
    .line 113
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    .line 114
    .line 115
    .line 116
    move-result v6

    .line 117
    int-to-float v6, v6

    .line 118
    invoke-virtual {v0, v6}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇0OOo〇0(F)F

    .line 119
    .line 120
    .line 121
    move-result v6

    .line 122
    iput v6, v5, Landroid/graphics/PointF;->y:F

    .line 123
    .line 124
    goto :goto_1

    .line 125
    :cond_2
    const/16 v6, 0x10e

    .line 126
    .line 127
    if-ne v5, v6, :cond_3

    .line 128
    .line 129
    iget-object v5, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO0880O:Landroid/graphics/PointF;

    .line 130
    .line 131
    iget-object v6, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OOo8〇0:Landroid/graphics/Bitmap;

    .line 132
    .line 133
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    .line 134
    .line 135
    .line 136
    move-result v6

    .line 137
    int-to-float v6, v6

    .line 138
    invoke-virtual {v0, v6}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇〇0〇〇0(F)F

    .line 139
    .line 140
    .line 141
    move-result v6

    .line 142
    iput v6, v5, Landroid/graphics/PointF;->x:F

    .line 143
    .line 144
    iget-object v5, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO0880O:Landroid/graphics/PointF;

    .line 145
    .line 146
    invoke-virtual {v0, v7}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇0OOo〇0(F)F

    .line 147
    .line 148
    .line 149
    move-result v6

    .line 150
    iput v6, v5, Landroid/graphics/PointF;->y:F

    .line 151
    .line 152
    :goto_0
    move/from16 v16, v4

    .line 153
    .line 154
    move v4, v1

    .line 155
    move/from16 v1, v16

    .line 156
    .line 157
    :cond_3
    :goto_1
    iget-object v5, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO0880O:Landroid/graphics/PointF;

    .line 158
    .line 159
    iget v6, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO〇8O8oOo:I

    .line 160
    .line 161
    int-to-float v6, v6

    .line 162
    iget v7, v5, Landroid/graphics/PointF;->x:F

    .line 163
    .line 164
    iget v8, v5, Landroid/graphics/PointF;->y:F

    .line 165
    .line 166
    move v9, v2

    .line 167
    move v10, v3

    .line 168
    invoke-static/range {v5 .. v10}, Lcom/intsig/camscanner/doodle/util/DrawUtil;->〇o〇(Landroid/graphics/PointF;FFFFF)Landroid/graphics/PointF;

    .line 169
    .line 170
    .line 171
    iget-object v2, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO00〇o:Landroid/graphics/RectF;

    .line 172
    .line 173
    iget-object v3, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO0880O:Landroid/graphics/PointF;

    .line 174
    .line 175
    iget v5, v3, Landroid/graphics/PointF;->x:F

    .line 176
    .line 177
    iget v3, v3, Landroid/graphics/PointF;->y:F

    .line 178
    .line 179
    add-float/2addr v1, v5

    .line 180
    add-float/2addr v4, v3

    .line 181
    invoke-virtual {v2, v5, v3, v1, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 182
    .line 183
    .line 184
    goto/16 :goto_2

    .line 185
    .line 186
    :cond_4
    invoke-virtual {v0, v7}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇〇0〇〇0(F)F

    .line 187
    .line 188
    .line 189
    move-result v1

    .line 190
    invoke-virtual {v0, v7}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇0OOo〇0(F)F

    .line 191
    .line 192
    .line 193
    move-result v8

    .line 194
    iget-object v4, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OOo8〇0:Landroid/graphics/Bitmap;

    .line 195
    .line 196
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    .line 197
    .line 198
    .line 199
    move-result v4

    .line 200
    int-to-float v4, v4

    .line 201
    invoke-virtual {v0, v4}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇〇0〇〇0(F)F

    .line 202
    .line 203
    .line 204
    move-result v4

    .line 205
    iget-object v5, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OOo8〇0:Landroid/graphics/Bitmap;

    .line 206
    .line 207
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    .line 208
    .line 209
    .line 210
    move-result v5

    .line 211
    int-to-float v5, v5

    .line 212
    invoke-virtual {v0, v5}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇0OOo〇0(F)F

    .line 213
    .line 214
    .line 215
    move-result v11

    .line 216
    invoke-virtual {v0, v7}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇〇0〇〇0(F)F

    .line 217
    .line 218
    .line 219
    move-result v12

    .line 220
    iget-object v5, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OOo8〇0:Landroid/graphics/Bitmap;

    .line 221
    .line 222
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    .line 223
    .line 224
    .line 225
    move-result v5

    .line 226
    int-to-float v5, v5

    .line 227
    invoke-virtual {v0, v5}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇0OOo〇0(F)F

    .line 228
    .line 229
    .line 230
    move-result v13

    .line 231
    iget-object v5, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OOo8〇0:Landroid/graphics/Bitmap;

    .line 232
    .line 233
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    .line 234
    .line 235
    .line 236
    move-result v5

    .line 237
    int-to-float v5, v5

    .line 238
    invoke-virtual {v0, v5}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇〇0〇〇0(F)F

    .line 239
    .line 240
    .line 241
    move-result v14

    .line 242
    invoke-virtual {v0, v7}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇0OOo〇0(F)F

    .line 243
    .line 244
    .line 245
    move-result v15

    .line 246
    iget-object v5, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO0880O:Landroid/graphics/PointF;

    .line 247
    .line 248
    iget v6, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO〇8O8oOo:I

    .line 249
    .line 250
    int-to-float v6, v6

    .line 251
    move v7, v1

    .line 252
    move v9, v2

    .line 253
    move v10, v3

    .line 254
    invoke-static/range {v5 .. v10}, Lcom/intsig/camscanner/doodle/util/DrawUtil;->〇o〇(Landroid/graphics/PointF;FFFFF)Landroid/graphics/PointF;

    .line 255
    .line 256
    .line 257
    iget-object v5, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO0880O:Landroid/graphics/PointF;

    .line 258
    .line 259
    iget v1, v5, Landroid/graphics/PointF;->x:F

    .line 260
    .line 261
    iget v10, v5, Landroid/graphics/PointF;->y:F

    .line 262
    .line 263
    iget v6, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO〇8O8oOo:I

    .line 264
    .line 265
    int-to-float v6, v6

    .line 266
    move v7, v4

    .line 267
    move v8, v11

    .line 268
    move v4, v10

    .line 269
    move v10, v3

    .line 270
    invoke-static/range {v5 .. v10}, Lcom/intsig/camscanner/doodle/util/DrawUtil;->〇o〇(Landroid/graphics/PointF;FFFFF)Landroid/graphics/PointF;

    .line 271
    .line 272
    .line 273
    iget-object v5, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO0880O:Landroid/graphics/PointF;

    .line 274
    .line 275
    iget v11, v5, Landroid/graphics/PointF;->x:F

    .line 276
    .line 277
    iget v10, v5, Landroid/graphics/PointF;->y:F

    .line 278
    .line 279
    iget v6, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO〇8O8oOo:I

    .line 280
    .line 281
    int-to-float v6, v6

    .line 282
    move v7, v12

    .line 283
    move v8, v13

    .line 284
    move v12, v10

    .line 285
    move v10, v3

    .line 286
    invoke-static/range {v5 .. v10}, Lcom/intsig/camscanner/doodle/util/DrawUtil;->〇o〇(Landroid/graphics/PointF;FFFFF)Landroid/graphics/PointF;

    .line 287
    .line 288
    .line 289
    iget-object v5, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO0880O:Landroid/graphics/PointF;

    .line 290
    .line 291
    iget v13, v5, Landroid/graphics/PointF;->x:F

    .line 292
    .line 293
    iget v10, v5, Landroid/graphics/PointF;->y:F

    .line 294
    .line 295
    iget v6, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO〇8O8oOo:I

    .line 296
    .line 297
    int-to-float v6, v6

    .line 298
    move v7, v14

    .line 299
    move v8, v15

    .line 300
    move v2, v10

    .line 301
    move v10, v3

    .line 302
    invoke-static/range {v5 .. v10}, Lcom/intsig/camscanner/doodle/util/DrawUtil;->〇o〇(Landroid/graphics/PointF;FFFFF)Landroid/graphics/PointF;

    .line 303
    .line 304
    .line 305
    iget-object v3, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO0880O:Landroid/graphics/PointF;

    .line 306
    .line 307
    iget v5, v3, Landroid/graphics/PointF;->x:F

    .line 308
    .line 309
    iget v3, v3, Landroid/graphics/PointF;->y:F

    .line 310
    .line 311
    iget-object v6, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO00〇o:Landroid/graphics/RectF;

    .line 312
    .line 313
    invoke-static {v1, v11}, Ljava/lang/Math;->min(FF)F

    .line 314
    .line 315
    .line 316
    move-result v7

    .line 317
    invoke-static {v13, v5}, Ljava/lang/Math;->min(FF)F

    .line 318
    .line 319
    .line 320
    move-result v8

    .line 321
    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    .line 322
    .line 323
    .line 324
    move-result v7

    .line 325
    iput v7, v6, Landroid/graphics/RectF;->left:F

    .line 326
    .line 327
    iget-object v6, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO00〇o:Landroid/graphics/RectF;

    .line 328
    .line 329
    invoke-static {v4, v12}, Ljava/lang/Math;->min(FF)F

    .line 330
    .line 331
    .line 332
    move-result v7

    .line 333
    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    .line 334
    .line 335
    .line 336
    move-result v8

    .line 337
    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    .line 338
    .line 339
    .line 340
    move-result v7

    .line 341
    iput v7, v6, Landroid/graphics/RectF;->top:F

    .line 342
    .line 343
    iget-object v6, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO00〇o:Landroid/graphics/RectF;

    .line 344
    .line 345
    invoke-static {v1, v11}, Ljava/lang/Math;->max(FF)F

    .line 346
    .line 347
    .line 348
    move-result v1

    .line 349
    invoke-static {v13, v5}, Ljava/lang/Math;->max(FF)F

    .line 350
    .line 351
    .line 352
    move-result v5

    .line 353
    invoke-static {v1, v5}, Ljava/lang/Math;->max(FF)F

    .line 354
    .line 355
    .line 356
    move-result v1

    .line 357
    iput v1, v6, Landroid/graphics/RectF;->right:F

    .line 358
    .line 359
    iget-object v1, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO00〇o:Landroid/graphics/RectF;

    .line 360
    .line 361
    invoke-static {v4, v12}, Ljava/lang/Math;->max(FF)F

    .line 362
    .line 363
    .line 364
    move-result v4

    .line 365
    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    .line 366
    .line 367
    .line 368
    move-result v2

    .line 369
    invoke-static {v4, v2}, Ljava/lang/Math;->max(FF)F

    .line 370
    .line 371
    .line 372
    move-result v2

    .line 373
    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    .line 374
    .line 375
    :goto_2
    iget-object v1, v0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO00〇o:Landroid/graphics/RectF;

    .line 376
    .line 377
    return-object v1
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public getDoodleMaxScale()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇08O:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDoodleMinScale()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->ooo0〇〇O:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDoodleRotation()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO〇8O8oOo:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDoodleScale()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OO〇00〇8oO:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDoodleTranslationX()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8〇OO0〇0o:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getDoodleTranslationY()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8〇oO〇〇8o:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method getDropperTouchListener()Lcom/intsig/camscanner/doodle/widget/core/IDropperTouchListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO〇oo:Lcom/intsig/camscanner/doodle/widget/core/IDropperTouchListener;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getItemCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO〇〇:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oo8ooo8O:Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRedoItemCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8o:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRotateScale()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0O:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRotateTranX()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOo〇8o008:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRotateTranY()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOo0:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getShape()Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getSize()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O0O:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getUnitSize()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇〇0o〇〇0:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getZoomerScale()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->Ooo08:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public invalidate()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->refresh()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o0ooO(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO〇〇:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8o〇O0:Ljava/util/List;

    .line 11
    .line 12
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO8:Ljava/util/List;

    .line 16
    .line 17
    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 18
    .line 19
    .line 20
    invoke-interface {p1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->〇080()V

    .line 21
    .line 22
    .line 23
    const/4 p1, 0x2

    .line 24
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇O〇(I)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->refresh()V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public o8(FFF)V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->ooo0〇〇O:F

    .line 2
    .line 3
    cmpg-float v1, p1, v0

    .line 4
    .line 5
    if-gez v1, :cond_0

    .line 6
    .line 7
    :goto_0
    move p1, v0

    .line 8
    goto :goto_1

    .line 9
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇08O:F

    .line 10
    .line 11
    cmpl-float v1, p1, v0

    .line 12
    .line 13
    if-lez v1, :cond_1

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_1
    :goto_1
    invoke-virtual {p0, p2}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇〇0〇〇0(F)F

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    invoke-virtual {p0, p3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇0OOo〇0(F)F

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OO〇00〇8oO:F

    .line 25
    .line 26
    invoke-virtual {p0, v0, p2}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇0o(FF)F

    .line 27
    .line 28
    .line 29
    move-result p1

    .line 30
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8〇OO0〇0o:F

    .line 31
    .line 32
    invoke-virtual {p0, v1, p3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇08O8o〇0(FF)F

    .line 33
    .line 34
    .line 35
    move-result p1

    .line 36
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8〇oO〇〇8o:F

    .line 37
    .line 38
    const/16 p1, 0x8

    .line 39
    .line 40
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇O〇(I)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->refresh()V

    .line 44
    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public final oO(F)F
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllTranX()F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    sub-float/2addr p1, v0

    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllScale()F

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    div-float/2addr p1, v0

    .line 11
    return p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇oo〇()V

    .line 5
    .line 6
    .line 7
    iget-boolean p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O88O:Z

    .line 8
    .line 9
    if-nez p1, :cond_0

    .line 10
    .line 11
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o0:Lcom/intsig/camscanner/doodle/widget/IDoodleListener;

    .line 12
    .line 13
    invoke-interface {p1, p0}, Lcom/intsig/camscanner/doodle/widget/IDoodleListener;->OO8〇(Lcom/intsig/camscanner/doodle/widget/core/IDoodle;)V

    .line 14
    .line 15
    .line 16
    const/4 p1, 0x1

    .line 17
    iput-boolean p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O88O:Z

    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public oo〇()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OOo8〇0:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇08〇o0O:F

    .line 4
    .line 5
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    float-to-int v1, v1

    .line 10
    iget v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇o〇:F

    .line 11
    .line 12
    invoke-virtual {p0, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    float-to-int v2, v2

    .line 17
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/doodle/util/DoodleImageUtils;->〇o00〇〇Oo(Landroid/graphics/Bitmap;II)I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO〇oo:Lcom/intsig/camscanner/doodle/widget/core/IDropperTouchListener;

    .line 22
    .line 23
    if-eqz v1, :cond_0

    .line 24
    .line 25
    iget v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇08〇o0O:F

    .line 26
    .line 27
    iget v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇o〇:F

    .line 28
    .line 29
    invoke-interface {v1, v2, v3, v0}, Lcom/intsig/camscanner/doodle/widget/core/IDropperTouchListener;->〇080(FFI)V

    .line 30
    .line 31
    .line 32
    :cond_0
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final o〇0OOo〇0(F)F
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllScale()F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    mul-float p1, p1, v0

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllTranY()F

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    add-float/2addr p1, v0

    .line 12
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇8(Ljava/lang/String;)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "StaticFieldLeak"
        }
    .end annotation

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OO〇OOo:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/4 v0, 0x1

    .line 7
    iput-boolean v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OO〇OOo:Z

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/camscanner/doodle/widget/DoodleView$1;

    .line 10
    .line 11
    invoke-direct {v0, p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView$1;-><init>(Lcom/intsig/camscanner/doodle/widget/DoodleView;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->oo88o8O()Ljava/util/concurrent/ExecutorService;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    const/4 v1, 0x0

    .line 19
    new-array v1, v1, [Ljava/lang/Void;

    .line 20
    .line 21
    invoke-virtual {v0, p1, v1}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public o〇〇0〇(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇800OO〇0O:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8o〇O0:Ljava/util/List;

    .line 7
    .line 8
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-nez v0, :cond_2

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8o〇O0:Ljava/util/List;

    .line 15
    .line 16
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO〇〇:Ljava/util/List;

    .line 20
    .line 21
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    if-eqz p1, :cond_1

    .line 26
    .line 27
    const/4 p1, 0x2

    .line 28
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇O〇(I)V

    .line 29
    .line 30
    .line 31
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->refresh()V

    .line 32
    .line 33
    .line 34
    return-void

    .line 35
    :cond_2
    new-instance p1, Ljava/lang/RuntimeException;

    .line 36
    .line 37
    const-string v0, "The item has been added"

    .line 38
    .line 39
    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    throw p1
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public refresh()V
    .locals 2

    .line 1
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    if-ne v0, v1, :cond_0

    .line 10
    .line 11
    invoke-super {p0}, Landroid/widget/FrameLayout;->invalidate()V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇o〇Oo88:Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;

    .line 15
    .line 16
    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 17
    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    invoke-super {p0}, Landroid/widget/FrameLayout;->postInvalidate()V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇o〇Oo88:Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;

    .line 24
    .line 25
    invoke-virtual {v0}, Landroid/view/View;->postInvalidate()V

    .line 26
    .line 27
    .line 28
    :goto_0
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public setColor(Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8oOOo:Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->refresh()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setDefaultTouchDetector(Lcom/intsig/camscanner/doodle/widget/core/IDoodleTouchDetector;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0O〇O00O:Lcom/intsig/camscanner/doodle/widget/core/IDoodleTouchDetector;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setDoodleMaxScale(F)V
    .locals 1

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇08O:F

    .line 2
    .line 3
    iget p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OO〇00〇8oO:F

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    invoke-virtual {p0, p1, v0, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8(FFF)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setDoodleMinScale(F)V
    .locals 1

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->ooo0〇〇O:F

    .line 2
    .line 3
    iget p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OO〇00〇8oO:F

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    invoke-virtual {p0, p1, v0, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8(FFF)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setDoodleRotation(I)V
    .locals 5

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO〇8O8oOo:I

    .line 2
    .line 3
    rem-int/lit16 p1, p1, 0x168

    .line 4
    .line 5
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO〇8O8oOo:I

    .line 6
    .line 7
    if-gez p1, :cond_0

    .line 8
    .line 9
    add-int/lit16 p1, p1, 0x168

    .line 10
    .line 11
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO〇8O8oOo:I

    .line 12
    .line 13
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getDoodleBound()Landroid/graphics/RectF;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllScale()F

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    div-float/2addr v0, v1

    .line 26
    float-to-int v0, v0

    .line 27
    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllScale()F

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    div-float/2addr p1, v1

    .line 36
    float-to-int p1, p1

    .line 37
    int-to-float v0, v0

    .line 38
    const/high16 v1, 0x3f800000    # 1.0f

    .line 39
    .line 40
    mul-float v0, v0, v1

    .line 41
    .line 42
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 43
    .line 44
    .line 45
    move-result v2

    .line 46
    int-to-float v2, v2

    .line 47
    div-float/2addr v0, v2

    .line 48
    int-to-float p1, p1

    .line 49
    mul-float p1, p1, v1

    .line 50
    .line 51
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 52
    .line 53
    .line 54
    move-result v2

    .line 55
    int-to-float v2, v2

    .line 56
    div-float/2addr p1, v2

    .line 57
    cmpl-float v2, v0, p1

    .line 58
    .line 59
    if-lez v2, :cond_1

    .line 60
    .line 61
    div-float p1, v1, v0

    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_1
    div-float p1, v1, p1

    .line 65
    .line 66
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OOo8〇0:Landroid/graphics/Bitmap;

    .line 67
    .line 68
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 69
    .line 70
    .line 71
    move-result v0

    .line 72
    div-int/lit8 v0, v0, 0x2

    .line 73
    .line 74
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇OOo8〇0:Landroid/graphics/Bitmap;

    .line 75
    .line 76
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    .line 77
    .line 78
    .line 79
    move-result v2

    .line 80
    div-int/lit8 v2, v2, 0x2

    .line 81
    .line 82
    const/4 v3, 0x0

    .line 83
    iput v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8〇oO〇〇8o:F

    .line 84
    .line 85
    iput v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8〇OO0〇0o:F

    .line 86
    .line 87
    iput v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOo0:F

    .line 88
    .line 89
    iput v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOo〇8o008:F

    .line 90
    .line 91
    iput v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OO〇00〇8oO:F

    .line 92
    .line 93
    iput v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0O:F

    .line 94
    .line 95
    int-to-float v0, v0

    .line 96
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇〇0〇〇0(F)F

    .line 97
    .line 98
    .line 99
    move-result v1

    .line 100
    int-to-float v2, v2

    .line 101
    invoke-virtual {p0, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇0OOo〇0(F)F

    .line 102
    .line 103
    .line 104
    move-result v3

    .line 105
    iget v4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OO:F

    .line 106
    .line 107
    div-float/2addr p1, v4

    .line 108
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0O:F

    .line 109
    .line 110
    invoke-virtual {p0, v1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇0o(FF)F

    .line 111
    .line 112
    .line 113
    move-result p1

    .line 114
    invoke-virtual {p0, v3, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇08O8o〇0(FF)F

    .line 115
    .line 116
    .line 117
    move-result v0

    .line 118
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOo〇8o008:F

    .line 119
    .line 120
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOo0:F

    .line 121
    .line 122
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇o()V

    .line 123
    .line 124
    .line 125
    return-void
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public setDoodleTranslationX(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8〇OO0〇0o:F

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇o()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setDoodleTranslationY(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8〇oO〇〇8o:F

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇o()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setDropperMode(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇O8oOo0:Z

    .line 2
    .line 3
    xor-int/lit8 p1, p1, 0x1

    .line 4
    .line 5
    invoke-virtual {p0, p1}, Landroid/view/View;->setWillNotDraw(Z)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->invalidate()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setDropperTouchListener(Lcom/intsig/camscanner/doodle/widget/core/IDropperTouchListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO〇oo:Lcom/intsig/camscanner/doodle/widget/core/IDropperTouchListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setEditMode(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOoo80oO:Z

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->refresh()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setIsDrawableOutside(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇o0O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇80O8o8O〇:Landroid/view/View$OnTouchListener;

    .line 2
    .line 3
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setPen(Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oo8ooo8O:Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->refresh()V

    .line 6
    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    .line 10
    .line 11
    const-string v0, "Pen can\'t be null"

    .line 12
    .line 13
    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    throw p1
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setRectangleMode(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->Oo0O0o8:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setScrollingDoodle(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->Oo0〇Ooo:Z

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->refresh()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setShape(Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇oO:Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->refresh()V

    .line 6
    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    .line 10
    .line 11
    const-string v0, "Shape can\'t be null"

    .line 12
    .line 13
    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    throw p1
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setShowOriginal(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇O〇〇O8:Z

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇o()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setSize(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O0O:F

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->refresh()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setZoomerScale(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->Ooo08:F

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->refresh()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇0000OOO()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->Oo0O0o8:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇080()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OO〇OOo:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇08O8o〇0(FF)F
    .locals 1

    .line 1
    neg-float p2, p2

    .line 2
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllScale()F

    .line 3
    .line 4
    .line 5
    move-result v0

    .line 6
    mul-float p2, p2, v0

    .line 7
    .line 8
    add-float/2addr p2, p1

    .line 9
    iget p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇080OO8〇0:F

    .line 10
    .line 11
    sub-float/2addr p2, p1

    .line 12
    iget p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOo0:F

    .line 13
    .line 14
    sub-float/2addr p2, p1

    .line 15
    return p2
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇8(F)F
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllTranY()F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    sub-float/2addr p1, v0

    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllScale()F

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    div-float/2addr p1, v0

    .line 11
    return p1
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇8〇0〇o〇O(I)Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO〇〇:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-lez v0, :cond_1

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO〇〇:Ljava/util/List;

    .line 11
    .line 12
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    .line 21
    .line 22
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO〇〇:Ljava/util/List;

    .line 23
    .line 24
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 25
    .line 26
    .line 27
    move-result v3

    .line 28
    sub-int/2addr v3, p1

    .line 29
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOO〇〇:Ljava/util/List;

    .line 30
    .line 31
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 32
    .line 33
    .line 34
    move-result p1

    .line 35
    invoke-interface {v2, v3, p1}, Ljava/util/List;->subList(II)Ljava/util/List;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 40
    .line 41
    .line 42
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    if-eqz v0, :cond_0

    .line 51
    .line 52
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    check-cast v0, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;

    .line 57
    .line 58
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o0ooO(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;)V

    .line 59
    .line 60
    .line 61
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8o:Ljava/util/List;

    .line 62
    .line 63
    invoke-interface {v2, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 64
    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_0
    const/4 p1, 0x1

    .line 68
    return p1

    .line 69
    :cond_1
    return v1
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public 〇O00(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇8O0〇8(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o8o:Ljava/util/List;

    .line 5
    .line 6
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇O888o0o(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->Oo80:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇oOO8O8()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇800OO〇0O:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇〇0o(FF)F
    .locals 1

    .line 1
    neg-float p2, p2

    .line 2
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllScale()F

    .line 3
    .line 4
    .line 5
    move-result v0

    .line 6
    mul-float p2, p2, v0

    .line 7
    .line 8
    add-float/2addr p2, p1

    .line 9
    iget p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8o08O8O:F

    .line 10
    .line 11
    sub-float/2addr p2, p1

    .line 12
    iget p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oOo〇8o008:F

    .line 13
    .line 14
    sub-float/2addr p2, p1

    .line 15
    return p2
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇〇〇0〇〇0(F)F
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllScale()F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    mul-float p1, p1, v0

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllTranX()F

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    add-float/2addr p1, v0

    .line 12
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.class public Lcom/intsig/camscanner/doodle/widget/DropperTouchView;
.super Landroid/widget/FrameLayout;
.source "DropperTouchView.java"


# static fields
.field private static final oOo0:I

.field private static final oOo〇8o008:I

.field private static final 〇0O:I


# instance fields
.field private O8o08O8O:I

.field private OO:I

.field private o0:Lcom/intsig/camscanner/doodle/widget/StrokeColorView;

.field private o〇00O:I

.field private 〇080OO8〇0:I

.field private 〇08O〇00〇o:I

.field private 〇OOo8〇0:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const/high16 v0, 0x42300000    # 44.0f

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/util/DoodleUtils;->〇080(F)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    sput v0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇0O:I

    .line 8
    .line 9
    div-int/lit8 v0, v0, 0x2

    .line 10
    .line 11
    sput v0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->oOo〇8o008:I

    .line 12
    .line 13
    const/high16 v0, 0x41000000    # 8.0f

    .line 14
    .line 15
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/util/DoodleUtils;->〇080(F)I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    sput v0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->oOo0:I

    .line 20
    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 2
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, -0x1

    .line 3
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇080OO8〇0:I

    .line 4
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->Oo08()V

    return-void
.end method

.method static bridge synthetic O8(Lcom/intsig/camscanner/doodle/widget/DropperTouchView;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->o〇0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o〇0()V
    .locals 5

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->o〇00O:I

    .line 2
    .line 3
    if-lez v0, :cond_4

    .line 4
    .line 5
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->O8o08O8O:I

    .line 6
    .line 7
    if-gtz v0, :cond_0

    .line 8
    .line 9
    goto :goto_3

    .line 10
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->OO:I

    .line 11
    .line 12
    int-to-float v0, v0

    .line 13
    sget v1, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇0O:I

    .line 14
    .line 15
    int-to-float v2, v1

    .line 16
    const/high16 v3, 0x40000000    # 2.0f

    .line 17
    .line 18
    div-float/2addr v2, v3

    .line 19
    add-float/2addr v0, v2

    .line 20
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    int-to-float v2, v2

    .line 25
    div-float/2addr v2, v3

    .line 26
    cmpl-float v0, v0, v2

    .line 27
    .line 28
    if-lez v0, :cond_2

    .line 29
    .line 30
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->OO:I

    .line 31
    .line 32
    add-int/2addr v0, v1

    .line 33
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 34
    .line 35
    .line 36
    move-result v2

    .line 37
    sget v3, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->oOo0:I

    .line 38
    .line 39
    sub-int/2addr v2, v3

    .line 40
    if-le v0, v2, :cond_1

    .line 41
    .line 42
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    sub-int/2addr v0, v3

    .line 47
    iget v2, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->o〇00O:I

    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_1
    iget v2, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->o〇00O:I

    .line 51
    .line 52
    :goto_0
    sub-int/2addr v0, v2

    .line 53
    goto :goto_1

    .line 54
    :cond_2
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->OO:I

    .line 55
    .line 56
    sget v2, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->oOo0:I

    .line 57
    .line 58
    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    .line 59
    .line 60
    .line 61
    move-result v0

    .line 62
    :goto_1
    iget v2, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇08O〇00〇o:I

    .line 63
    .line 64
    add-int/2addr v2, v1

    .line 65
    sget v3, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->oOo0:I

    .line 66
    .line 67
    add-int/2addr v2, v3

    .line 68
    iget v4, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->O8o08O8O:I

    .line 69
    .line 70
    add-int/2addr v2, v4

    .line 71
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 72
    .line 73
    .line 74
    move-result v4

    .line 75
    if-le v2, v4, :cond_3

    .line 76
    .line 77
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇08O〇00〇o:I

    .line 78
    .line 79
    sub-int/2addr v1, v3

    .line 80
    iget v2, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->O8o08O8O:I

    .line 81
    .line 82
    sub-int/2addr v1, v2

    .line 83
    int-to-float v1, v1

    .line 84
    goto :goto_2

    .line 85
    :cond_3
    iget v2, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇08O〇00〇o:I

    .line 86
    .line 87
    add-int/2addr v2, v1

    .line 88
    add-int/2addr v2, v3

    .line 89
    int-to-float v1, v2

    .line 90
    :goto_2
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇OOo8〇0:Landroid/widget/TextView;

    .line 91
    .line 92
    int-to-float v0, v0

    .line 93
    invoke-virtual {v2, v0}, Landroid/view/View;->setX(F)V

    .line 94
    .line 95
    .line 96
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇OOo8〇0:Landroid/widget/TextView;

    .line 97
    .line 98
    invoke-virtual {v0, v1}, Landroid/view/View;->setY(F)V

    .line 99
    .line 100
    .line 101
    :cond_4
    :goto_3
    return-void
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/doodle/widget/DropperTouchView;)Landroid/widget/TextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇OOo8〇0:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/doodle/widget/DropperTouchView;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->O8o08O8O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/camscanner/doodle/widget/DropperTouchView;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->o〇00O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public Oo08()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;

    .line 6
    .line 7
    invoke-direct {v1, v0}, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;-><init>(Landroid/content/Context;)V

    .line 8
    .line 9
    .line 10
    iput-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->o0:Lcom/intsig/camscanner/doodle/widget/StrokeColorView;

    .line 11
    .line 12
    sget v1, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇0O:I

    .line 13
    .line 14
    new-instance v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 15
    .line 16
    invoke-direct {v2, v1, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 17
    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->o0:Lcom/intsig/camscanner/doodle/widget/StrokeColorView;

    .line 20
    .line 21
    invoke-virtual {p0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 22
    .line 23
    .line 24
    new-instance v1, Landroid/widget/TextView;

    .line 25
    .line 26
    invoke-direct {v1, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 27
    .line 28
    .line 29
    iput-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇OOo8〇0:Landroid/widget/TextView;

    .line 30
    .line 31
    const/16 v0, 0x10

    .line 32
    .line 33
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 34
    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇OOo8〇0:Landroid/widget/TextView;

    .line 37
    .line 38
    const/high16 v1, 0x41600000    # 14.0f

    .line 39
    .line 40
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 41
    .line 42
    .line 43
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇OOo8〇0:Landroid/widget/TextView;

    .line 44
    .line 45
    const v1, -0xd03d5b

    .line 46
    .line 47
    .line 48
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 49
    .line 50
    .line 51
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇OOo8〇0:Landroid/widget/TextView;

    .line 52
    .line 53
    const/high16 v1, 0x41e00000    # 28.0f

    .line 54
    .line 55
    invoke-static {v1}, Lcom/intsig/camscanner/doodle/util/DoodleUtils;->〇080(F)I

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMinHeight(I)V

    .line 60
    .line 61
    .line 62
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇OOo8〇0:Landroid/widget/TextView;

    .line 63
    .line 64
    sget v1, Lcom/intsig/camscanner/doodle/R$string;->cs_521_button_use_color:I

    .line 65
    .line 66
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 67
    .line 68
    .line 69
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇OOo8〇0:Landroid/widget/TextView;

    .line 70
    .line 71
    sget v1, Lcom/intsig/camscanner/doodle/R$drawable;->doodle_ic_touch_complete:I

    .line 72
    .line 73
    const/4 v2, 0x0

    .line 74
    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 75
    .line 76
    .line 77
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇OOo8〇0:Landroid/widget/TextView;

    .line 78
    .line 79
    const/high16 v1, 0x40c00000    # 6.0f

    .line 80
    .line 81
    invoke-static {v1}, Lcom/intsig/camscanner/doodle/util/DoodleUtils;->〇080(F)I

    .line 82
    .line 83
    .line 84
    move-result v1

    .line 85
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 86
    .line 87
    .line 88
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇OOo8〇0:Landroid/widget/TextView;

    .line 89
    .line 90
    sget v1, Lcom/intsig/camscanner/doodle/R$drawable;->doodle_bg_dropper_done:I

    .line 91
    .line 92
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 93
    .line 94
    .line 95
    const/high16 v0, 0x41000000    # 8.0f

    .line 96
    .line 97
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/util/DoodleUtils;->〇080(F)I

    .line 98
    .line 99
    .line 100
    move-result v0

    .line 101
    const/high16 v1, 0x40800000    # 4.0f

    .line 102
    .line 103
    invoke-static {v1}, Lcom/intsig/camscanner/doodle/util/DoodleUtils;->〇080(F)I

    .line 104
    .line 105
    .line 106
    move-result v1

    .line 107
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇OOo8〇0:Landroid/widget/TextView;

    .line 108
    .line 109
    invoke-virtual {v2, v0, v1, v0, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 110
    .line 111
    .line 112
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 113
    .line 114
    const/4 v1, -0x2

    .line 115
    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 116
    .line 117
    .line 118
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇OOo8〇0:Landroid/widget/TextView;

    .line 119
    .line 120
    invoke-virtual {p0, v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 121
    .line 122
    .line 123
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇OOo8〇0:Landroid/widget/TextView;

    .line 124
    .line 125
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 126
    .line 127
    .line 128
    move-result-object v0

    .line 129
    new-instance v1, Lcom/intsig/camscanner/doodle/widget/DropperTouchView$1;

    .line 130
    .line 131
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/doodle/widget/DropperTouchView$1;-><init>(Lcom/intsig/camscanner/doodle/widget/DropperTouchView;)V

    .line 132
    .line 133
    .line 134
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 135
    .line 136
    .line 137
    return-void
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public getColor()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇080OO8〇0:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setDoneClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇OOo8〇0:Landroid/widget/TextView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setDoneVisible(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇OOo8〇0:Landroid/widget/TextView;

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/16 p1, 0x8

    .line 8
    .line 9
    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 10
    .line 11
    .line 12
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->o〇0()V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public update(FFI)V
    .locals 3

    .line 1
    iput p3, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇080OO8〇0:I

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->setDoneVisible(Z)V

    .line 5
    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->o0:Lcom/intsig/camscanner/doodle/widget/StrokeColorView;

    .line 8
    .line 9
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 10
    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->o0:Lcom/intsig/camscanner/doodle/widget/StrokeColorView;

    .line 13
    .line 14
    invoke-virtual {v1, p3}, Lcom/intsig/camscanner/doodle/widget/StrokeColorView;->〇o00〇〇Oo(I)V

    .line 15
    .line 16
    .line 17
    sget p3, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->oOo〇8o008:I

    .line 18
    .line 19
    int-to-float v1, p3

    .line 20
    sub-float/2addr p1, v1

    .line 21
    float-to-int p1, p1

    .line 22
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->OO:I

    .line 23
    .line 24
    if-gez p1, :cond_0

    .line 25
    .line 26
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->OO:I

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    sget v1, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇0O:I

    .line 30
    .line 31
    add-int/2addr p1, v1

    .line 32
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 33
    .line 34
    .line 35
    move-result v2

    .line 36
    if-le p1, v2, :cond_1

    .line 37
    .line 38
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 39
    .line 40
    .line 41
    move-result p1

    .line 42
    sub-int/2addr p1, v1

    .line 43
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->OO:I

    .line 44
    .line 45
    :cond_1
    :goto_0
    int-to-float p1, p3

    .line 46
    sub-float/2addr p2, p1

    .line 47
    float-to-int p1, p2

    .line 48
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇08O〇00〇o:I

    .line 49
    .line 50
    if-gez p1, :cond_2

    .line 51
    .line 52
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇08O〇00〇o:I

    .line 53
    .line 54
    goto :goto_1

    .line 55
    :cond_2
    sget p2, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇0O:I

    .line 56
    .line 57
    add-int/2addr p1, p2

    .line 58
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 59
    .line 60
    .line 61
    move-result p3

    .line 62
    if-le p1, p3, :cond_3

    .line 63
    .line 64
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 65
    .line 66
    .line 67
    move-result p1

    .line 68
    sub-int/2addr p1, p2

    .line 69
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇08O〇00〇o:I

    .line 70
    .line 71
    :cond_3
    :goto_1
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->o0:Lcom/intsig/camscanner/doodle/widget/StrokeColorView;

    .line 72
    .line 73
    iget p2, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->OO:I

    .line 74
    .line 75
    int-to-float p2, p2

    .line 76
    invoke-virtual {p1, p2}, Landroid/view/View;->setX(F)V

    .line 77
    .line 78
    .line 79
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->o0:Lcom/intsig/camscanner/doodle/widget/StrokeColorView;

    .line 80
    .line 81
    iget p2, p0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->〇08O〇00〇o:I

    .line 82
    .line 83
    int-to-float p2, p2

    .line 84
    invoke-virtual {p1, p2}, Landroid/view/View;->setY(F)V

    .line 85
    .line 86
    .line 87
    return-void
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

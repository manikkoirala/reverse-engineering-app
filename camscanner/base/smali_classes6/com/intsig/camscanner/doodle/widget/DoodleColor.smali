.class public Lcom/intsig/camscanner/doodle/widget/DoodleColor;
.super Ljava/lang/Object;
.source "DoodleColor.java"

# interfaces
.implements Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/doodle/widget/DoodleColor$Type;
    }
.end annotation


# instance fields
.field private O8:Landroid/graphics/Matrix;

.field private Oo08:I

.field private o〇0:Landroid/graphics/Shader$TileMode;

.field private 〇080:I

.field private 〇o00〇〇Oo:Landroid/graphics/Bitmap;

.field private 〇o〇:Lcom/intsig/camscanner/doodle/widget/DoodleColor$Type;

.field private 〇〇888:Landroid/graphics/Shader$TileMode;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 2
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->Oo08:I

    .line 3
    sget-object v0, Landroid/graphics/Shader$TileMode;->MIRROR:Landroid/graphics/Shader$TileMode;

    iput-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->o〇0:Landroid/graphics/Shader$TileMode;

    .line 4
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->〇〇888:Landroid/graphics/Shader$TileMode;

    .line 5
    sget-object v0, Lcom/intsig/camscanner/doodle/widget/DoodleColor$Type;->COLOR:Lcom/intsig/camscanner/doodle/widget/DoodleColor$Type;

    iput-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->〇o〇:Lcom/intsig/camscanner/doodle/widget/DoodleColor$Type;

    .line 6
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->〇080:I

    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 1

    const/4 v0, 0x0

    .line 7
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleColor;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)V

    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)V
    .locals 1

    .line 8
    sget-object v0, Landroid/graphics/Shader$TileMode;->MIRROR:Landroid/graphics/Shader$TileMode;

    invoke-direct {p0, p1, p2, v0, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleColor;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V
    .locals 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    .line 10
    iput v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->Oo08:I

    .line 11
    sget-object v0, Landroid/graphics/Shader$TileMode;->MIRROR:Landroid/graphics/Shader$TileMode;

    iput-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->o〇0:Landroid/graphics/Shader$TileMode;

    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->〇〇888:Landroid/graphics/Shader$TileMode;

    .line 13
    sget-object v0, Lcom/intsig/camscanner/doodle/widget/DoodleColor$Type;->BITMAP:Lcom/intsig/camscanner/doodle/widget/DoodleColor$Type;

    iput-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->〇o〇:Lcom/intsig/camscanner/doodle/widget/DoodleColor$Type;

    .line 14
    iput-object p2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->O8:Landroid/graphics/Matrix;

    .line 15
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    .line 16
    iput-object p3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->o〇0:Landroid/graphics/Shader$TileMode;

    .line 17
    iput-object p4, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->〇〇888:Landroid/graphics/Shader$TileMode;

    return-void
.end method


# virtual methods
.method public O8()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->Oo08:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Oo08()Landroid/graphics/Matrix;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->O8:Landroid/graphics/Matrix;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public config(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;Landroid/graphics/Paint;)V
    .locals 3

    .line 1
    check-cast p1, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->〇o〇:Lcom/intsig/camscanner/doodle/widget/DoodleColor$Type;

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/camscanner/doodle/widget/DoodleColor$Type;->COLOR:Lcom/intsig/camscanner/doodle/widget/DoodleColor$Type;

    .line 6
    .line 7
    if-ne p1, v0, :cond_0

    .line 8
    .line 9
    iget p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->〇080:I

    .line 10
    .line 11
    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 12
    .line 13
    .line 14
    const/4 p1, 0x0

    .line 15
    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 16
    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/doodle/widget/DoodleColor$Type;->BITMAP:Lcom/intsig/camscanner/doodle/widget/DoodleColor$Type;

    .line 20
    .line 21
    if-ne p1, v0, :cond_1

    .line 22
    .line 23
    new-instance p1, Landroid/graphics/BitmapShader;

    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    .line 26
    .line 27
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->o〇0:Landroid/graphics/Shader$TileMode;

    .line 28
    .line 29
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->〇〇888:Landroid/graphics/Shader$TileMode;

    .line 30
    .line 31
    invoke-direct {p1, v0, v1, v2}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    .line 32
    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->O8:Landroid/graphics/Matrix;

    .line 35
    .line 36
    invoke-virtual {p1, v0}, Landroid/graphics/Shader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p2, p1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 40
    .line 41
    .line 42
    :cond_1
    :goto_0
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public copy()Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->〇o〇:Lcom/intsig/camscanner/doodle/widget/DoodleColor$Type;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/doodle/widget/DoodleColor$Type;->COLOR:Lcom/intsig/camscanner/doodle/widget/DoodleColor$Type;

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    new-instance v0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;

    .line 8
    .line 9
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->〇080:I

    .line 10
    .line 11
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleColor;-><init>(I)V

    .line 12
    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    .line 18
    .line 19
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleColor;-><init>(Landroid/graphics/Bitmap;)V

    .line 20
    .line 21
    .line 22
    :goto_0
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->o〇0:Landroid/graphics/Shader$TileMode;

    .line 23
    .line 24
    iput-object v1, v0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->o〇0:Landroid/graphics/Shader$TileMode;

    .line 25
    .line 26
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->〇〇888:Landroid/graphics/Shader$TileMode;

    .line 27
    .line 28
    iput-object v1, v0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->〇〇888:Landroid/graphics/Shader$TileMode;

    .line 29
    .line 30
    new-instance v1, Landroid/graphics/Matrix;

    .line 31
    .line 32
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->O8:Landroid/graphics/Matrix;

    .line 33
    .line 34
    invoke-direct {v1, v2}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 35
    .line 36
    .line 37
    iput-object v1, v0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->O8:Landroid/graphics/Matrix;

    .line 38
    .line 39
    iget v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->Oo08:I

    .line 40
    .line 41
    iput v1, v0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->Oo08:I

    .line 42
    .line 43
    return-object v0
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public getType()Lcom/intsig/camscanner/doodle/widget/DoodleColor$Type;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->〇o〇:Lcom/intsig/camscanner/doodle/widget/DoodleColor$Type;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇0(Landroid/graphics/Matrix;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->O8:Landroid/graphics/Matrix;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇080(I)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/doodle/widget/DoodleColor$Type;->COLOR:Lcom/intsig/camscanner/doodle/widget/DoodleColor$Type;

    .line 2
    .line 3
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->〇o〇:Lcom/intsig/camscanner/doodle/widget/DoodleColor$Type;

    .line 4
    .line 5
    iput p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->〇080:I

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇o00〇〇Oo()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->〇080:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇()Landroid/graphics/Bitmap;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;->〇o00〇〇Oo:Landroid/graphics/Bitmap;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

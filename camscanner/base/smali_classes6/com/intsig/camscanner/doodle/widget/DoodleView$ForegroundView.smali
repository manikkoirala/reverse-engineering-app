.class Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;
.super Landroid/view/View;
.source "DoodleView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/doodle/widget/DoodleView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ForegroundView"
.end annotation


# instance fields
.field final synthetic o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/doodle/widget/DoodleView;Landroid/content/Context;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 2
    .line 3
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 4
    .line 5
    .line 6
    const/4 p1, 0x1

    .line 7
    const/4 p2, 0x0

    .line 8
    invoke-virtual {p0, p1, p2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇080(Landroid/graphics/Canvas;)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇o00〇〇Oo(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllTranX()F

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 17
    .line 18
    invoke-virtual {v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllTranY()F

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllScale()F

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    invoke-virtual {p1, v0, v0}, Landroid/graphics/Canvas;->scale(FF)V

    .line 32
    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 35
    .line 36
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8o8o〇(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Z

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    if-eqz v0, :cond_1

    .line 41
    .line 42
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 43
    .line 44
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->Oo08(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Landroid/graphics/Bitmap;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    goto :goto_0

    .line 49
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 50
    .line 51
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇o〇(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Landroid/graphics/Bitmap;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 60
    .line 61
    invoke-static {v2}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇80〇808〇O(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Ljava/util/List;

    .line 62
    .line 63
    .line 64
    move-result-object v2

    .line 65
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 66
    .line 67
    invoke-static {v3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8o8o〇(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Z

    .line 68
    .line 69
    .line 70
    move-result v3

    .line 71
    if-eqz v3, :cond_2

    .line 72
    .line 73
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 74
    .line 75
    invoke-static {v2}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Ljava/util/List;

    .line 76
    .line 77
    .line 78
    move-result-object v2

    .line 79
    :cond_2
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 80
    .line 81
    invoke-static {v3}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO80(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Z

    .line 82
    .line 83
    .line 84
    move-result v3

    .line 85
    const/4 v4, 0x0

    .line 86
    if-nez v3, :cond_3

    .line 87
    .line 88
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 89
    .line 90
    .line 91
    move-result v3

    .line 92
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 93
    .line 94
    .line 95
    move-result v5

    .line 96
    invoke-virtual {p1, v4, v4, v3, v5}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 97
    .line 98
    .line 99
    const/4 v3, 0x1

    .line 100
    goto :goto_1

    .line 101
    :cond_3
    const/4 v3, 0x0

    .line 102
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 103
    .line 104
    .line 105
    move-result-object v5

    .line 106
    :cond_4
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    .line 107
    .line 108
    .line 109
    move-result v6

    .line 110
    if-eqz v6, :cond_7

    .line 111
    .line 112
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 113
    .line 114
    .line 115
    move-result-object v6

    .line 116
    check-cast v6, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;

    .line 117
    .line 118
    invoke-interface {v6}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->Oooo8o0〇()Z

    .line 119
    .line 120
    .line 121
    move-result v7

    .line 122
    if-nez v7, :cond_6

    .line 123
    .line 124
    if-eqz v3, :cond_5

    .line 125
    .line 126
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 127
    .line 128
    .line 129
    :cond_5
    invoke-interface {v6, p1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->draw(Landroid/graphics/Canvas;)V

    .line 130
    .line 131
    .line 132
    if-eqz v3, :cond_4

    .line 133
    .line 134
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 135
    .line 136
    .line 137
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 138
    .line 139
    .line 140
    move-result v6

    .line 141
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 142
    .line 143
    .line 144
    move-result v7

    .line 145
    invoke-virtual {p1, v4, v4, v6, v7}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 146
    .line 147
    .line 148
    goto :goto_2

    .line 149
    :cond_6
    invoke-interface {v6, p1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->draw(Landroid/graphics/Canvas;)V

    .line 150
    .line 151
    .line 152
    goto :goto_2

    .line 153
    :cond_7
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 154
    .line 155
    .line 156
    move-result-object v2

    .line 157
    :cond_8
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 158
    .line 159
    .line 160
    move-result v5

    .line 161
    if-eqz v5, :cond_b

    .line 162
    .line 163
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 164
    .line 165
    .line 166
    move-result-object v5

    .line 167
    check-cast v5, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;

    .line 168
    .line 169
    invoke-interface {v5}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->Oooo8o0〇()Z

    .line 170
    .line 171
    .line 172
    move-result v6

    .line 173
    if-nez v6, :cond_a

    .line 174
    .line 175
    if-eqz v3, :cond_9

    .line 176
    .line 177
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 178
    .line 179
    .line 180
    :cond_9
    invoke-interface {v5, p1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->〇8o8o〇(Landroid/graphics/Canvas;)V

    .line 181
    .line 182
    .line 183
    if-eqz v3, :cond_8

    .line 184
    .line 185
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 186
    .line 187
    .line 188
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 189
    .line 190
    .line 191
    move-result v5

    .line 192
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 193
    .line 194
    .line 195
    move-result v6

    .line 196
    invoke-virtual {p1, v4, v4, v5, v6}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 197
    .line 198
    .line 199
    goto :goto_3

    .line 200
    :cond_a
    invoke-interface {v5, p1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->〇8o8o〇(Landroid/graphics/Canvas;)V

    .line 201
    .line 202
    .line 203
    goto :goto_3

    .line 204
    :cond_b
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 205
    .line 206
    .line 207
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 208
    .line 209
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇O8o08O(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 210
    .line 211
    .line 212
    move-result-object v0

    .line 213
    if-eqz v0, :cond_c

    .line 214
    .line 215
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 216
    .line 217
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇O8o08O(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 218
    .line 219
    .line 220
    move-result-object v0

    .line 221
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 222
    .line 223
    invoke-interface {v0, p1, v1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;->drawHelpers(Landroid/graphics/Canvas;Lcom/intsig/camscanner/doodle/widget/core/IDoodle;)V

    .line 224
    .line 225
    .line 226
    :cond_c
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 227
    .line 228
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OO0o〇〇(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;

    .line 229
    .line 230
    .line 231
    move-result-object v0

    .line 232
    if-eqz v0, :cond_d

    .line 233
    .line 234
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 235
    .line 236
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->OO0o〇〇(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;

    .line 237
    .line 238
    .line 239
    move-result-object v0

    .line 240
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 241
    .line 242
    invoke-interface {v0, p1, v1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;->drawHelpers(Landroid/graphics/Canvas;Lcom/intsig/camscanner/doodle/widget/core/IDoodle;)V

    .line 243
    .line 244
    .line 245
    :cond_d
    return-void
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 6
    .line 7
    invoke-static {v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇888(Lcom/intsig/camscanner/doodle/widget/DoodleView;)I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    int-to-float v1, v1

    .line 12
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    int-to-float v2, v2

    .line 17
    const/high16 v3, 0x40000000    # 2.0f

    .line 18
    .line 19
    div-float/2addr v2, v3

    .line 20
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 21
    .line 22
    .line 23
    move-result v4

    .line 24
    int-to-float v4, v4

    .line 25
    div-float/2addr v4, v3

    .line 26
    invoke-virtual {p1, v1, v2, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 27
    .line 28
    .line 29
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;->〇080(Landroid/graphics/Canvas;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->Oooo8o0〇(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Ljava/util/Map;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 8
    .line 9
    invoke-static {v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇O8o08O(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    check-cast v0, Lcom/intsig/camscanner/doodle/widget/core/IDoodleTouchDetector;

    .line 18
    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleTouchDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    return p1

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 27
    .line 28
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Lcom/intsig/camscanner/doodle/widget/core/IDoodleTouchDetector;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    if-eqz v0, :cond_1

    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/widget/DoodleView$ForegroundView;->o0:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 35
    .line 36
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8(Lcom/intsig/camscanner/doodle/widget/DoodleView;)Lcom/intsig/camscanner/doodle/widget/core/IDoodleTouchDetector;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleTouchDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 41
    .line 42
    .line 43
    move-result p1

    .line 44
    return p1

    .line 45
    :cond_1
    const/4 p1, 0x0

    .line 46
    return p1
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

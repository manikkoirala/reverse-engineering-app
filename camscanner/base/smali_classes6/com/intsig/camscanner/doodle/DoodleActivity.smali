.class public Lcom/intsig/camscanner/doodle/DoodleActivity;
.super Lcom/intsig/camscanner/doodle/BaseDoodleActivity;
.source "DoodleActivity.java"


# static fields
.field private static final oO00〇o:I


# instance fields
.field private Oo0〇Ooo:Landroid/widget/LinearLayout;

.field private Oo80:Landroid/widget/SeekBar;

.field private Ooo08:Landroid/widget/TextView;

.field private O〇08oOOO0:Landroid/view/View;

.field private O〇o88o08〇:Landroid/widget/SeekBar;

.field private o0OoOOo0:Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;

.field private o8o:Landroid/view/View;

.field private o8〇OO:Landroid/view/View;

.field private oO〇8O8oOo:Lcom/intsig/camscanner/doodle/DoodleConfig;

.field private oo8ooo8O:Lcom/intsig/camscanner/doodle/widget/DropperTouchView;

.field private ooO:Landroidx/appcompat/widget/AppCompatImageView;

.field private o〇oO:[Lcom/intsig/view/color/ColorItemView;

.field private o〇o〇Oo88:Z

.field private 〇00O0:Landroid/view/View;

.field private 〇08〇o0O:[Lcom/intsig/view/color/ColorItemView;

.field private 〇0O〇O00O:I

.field private 〇OO8ooO8〇:Landroid/view/View;

.field private 〇OO〇00〇0O:Landroidx/appcompat/widget/AppCompatImageView;

.field private 〇〇o〇:Landroid/widget/ImageView;

.field private 〇〇〇0o〇〇0:Landroid/widget/RelativeLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const/high16 v0, 0x40a00000    # 5.0f

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/util/DoodleUtils;->〇080(F)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    sput v0, Lcom/intsig/camscanner/doodle/DoodleActivity;->oO00〇o:I

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o〇o〇Oo88:Z

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic O00OoO〇(Lcom/intsig/camscanner/doodle/DoodleActivity;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o〇o〇Oo88:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O088O()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getSize()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllScale()F

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    mul-float v0, v0, v1

    .line 14
    .line 15
    float-to-int v0, v0

    .line 16
    sget v1, Lcom/intsig/camscanner/doodle/DoodleActivity;->oO00〇o:I

    .line 17
    .line 18
    if-ge v0, v1, :cond_0

    .line 19
    .line 20
    move v0, v1

    .line 21
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o8o:Landroid/view/View;

    .line 22
    .line 23
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 28
    .line 29
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o8o:Landroid/view/View;

    .line 32
    .line 33
    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private O08〇oO8〇(Lcom/intsig/camscanner/doodle/widget/DoodlePen;Lcom/intsig/view/color/ColorItemView;)V
    .locals 1
    .param p2    # Lcom/intsig/view/color/ColorItemView;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->setPen(Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇0O8Oo()V

    .line 7
    .line 8
    .line 9
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/doodle/DoodleActivity;->O〇0o8〇(Lcom/intsig/view/color/ColorItemView;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic O0o0(Landroid/view/View;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O08000()Z

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->o88oo〇O()V

    .line 7
    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->O80OO()V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O80OO()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0000OOO()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->setRectangleMode(Z)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o0OoOOo0:Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;

    .line 18
    .line 19
    const/4 v1, 0x0

    .line 20
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇〇8O0〇8(Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;)V

    .line 21
    .line 22
    .line 23
    :cond_0
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic O88(Lcom/intsig/camscanner/doodle/DoodleActivity;)Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o0OoOOo0:Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic O880O〇(Lcom/intsig/camscanner/doodle/DoodleActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇〇〇OOO〇〇(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic O8O(Lcom/intsig/camscanner/doodle/DoodleActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/DoodleActivity;->O〇0o8o8〇(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O8〇o0〇〇8()V
    .locals 10

    .line 1
    new-instance v0, L〇O8〇OO〇/〇O8o08O;

    .line 2
    .line 3
    invoke-direct {v0, p0}, L〇O8〇OO〇/〇O8o08O;-><init>(Lcom/intsig/camscanner/doodle/DoodleActivity;)V

    .line 4
    .line 5
    .line 6
    sget v1, Lcom/intsig/camscanner/doodle/R$id;->l_color:I

    .line 7
    .line 8
    invoke-virtual {p0, v1}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    check-cast v1, Landroid/view/ViewGroup;

    .line 13
    .line 14
    const/4 v2, 0x6

    .line 15
    new-array v3, v2, [Lcom/intsig/view/color/ColorItemView;

    .line 16
    .line 17
    iput-object v3, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o〇oO:[Lcom/intsig/view/color/ColorItemView;

    .line 18
    .line 19
    const/4 v3, 0x3

    .line 20
    new-array v4, v3, [Lcom/intsig/view/color/ColorItemView;

    .line 21
    .line 22
    iput-object v4, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇08〇o0O:[Lcom/intsig/view/color/ColorItemView;

    .line 23
    .line 24
    const/4 v4, 0x0

    .line 25
    const/4 v5, 0x0

    .line 26
    const/4 v6, 0x0

    .line 27
    :goto_0
    if-ge v5, v2, :cond_1

    .line 28
    .line 29
    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 30
    .line 31
    .line 32
    move-result-object v7

    .line 33
    check-cast v7, Lcom/intsig/view/color/ColorItemView;

    .line 34
    .line 35
    invoke-virtual {v7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    .line 37
    .line 38
    iget-object v8, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o〇oO:[Lcom/intsig/view/color/ColorItemView;

    .line 39
    .line 40
    aput-object v7, v8, v5

    .line 41
    .line 42
    if-lt v5, v3, :cond_0

    .line 43
    .line 44
    iget-object v8, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇08〇o0O:[Lcom/intsig/view/color/ColorItemView;

    .line 45
    .line 46
    add-int/lit8 v9, v6, 0x1

    .line 47
    .line 48
    aput-object v7, v8, v6

    .line 49
    .line 50
    move v6, v9

    .line 51
    :cond_0
    add-int/lit8 v5, v5, 0x1

    .line 52
    .line 53
    goto :goto_0

    .line 54
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o〇oO:[Lcom/intsig/view/color/ColorItemView;

    .line 55
    .line 56
    aget-object v0, v0, v4

    .line 57
    .line 58
    const/4 v1, -0x1

    .line 59
    invoke-virtual {v0, v1}, Lcom/intsig/view/color/ColorItemView;->setColor(I)V

    .line 60
    .line 61
    .line 62
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o〇oO:[Lcom/intsig/view/color/ColorItemView;

    .line 63
    .line 64
    const/4 v1, 0x1

    .line 65
    aget-object v0, v0, v1

    .line 66
    .line 67
    const/high16 v1, -0x1000000

    .line 68
    .line 69
    invoke-virtual {v0, v1}, Lcom/intsig/view/color/ColorItemView;->setColor(I)V

    .line 70
    .line 71
    .line 72
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o〇oO:[Lcom/intsig/view/color/ColorItemView;

    .line 73
    .line 74
    const/4 v1, 0x2

    .line 75
    aget-object v0, v0, v1

    .line 76
    .line 77
    const v1, -0x4a494a

    .line 78
    .line 79
    .line 80
    invoke-virtual {v0, v1}, Lcom/intsig/view/color/ColorItemView;->setColor(I)V

    .line 81
    .line 82
    .line 83
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->o8o0o8()V

    .line 84
    .line 85
    .line 86
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->oO〇8O8oOo:Lcom/intsig/camscanner/doodle/DoodleConfig;

    .line 87
    .line 88
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/DoodleConfig;->〇o00〇〇Oo()I

    .line 89
    .line 90
    .line 91
    move-result v0

    .line 92
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o〇oO:[Lcom/intsig/view/color/ColorItemView;

    .line 93
    .line 94
    aget-object v1, v1, v0

    .line 95
    .line 96
    invoke-virtual {v1}, Lcom/intsig/view/color/ColorItemView;->getColor()I

    .line 97
    .line 98
    .line 99
    move-result v1

    .line 100
    iput v1, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇0O〇O00O:I

    .line 101
    .line 102
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o〇oO:[Lcom/intsig/view/color/ColorItemView;

    .line 103
    .line 104
    aget-object v0, v1, v0

    .line 105
    .line 106
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->O〇0o8〇(Lcom/intsig/view/color/ColorItemView;)V

    .line 107
    .line 108
    .line 109
    return-void
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic OO0o(Lcom/intsig/camscanner/doodle/DoodleActivity;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/DoodleActivity;->oO8(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private OO8〇O8()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllItem()Ljava/util/List;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-static {v0}, Lcom/intsig/utils/ListUtils;->〇o〇(Ljava/util/List;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇00()V

    .line 16
    .line 17
    .line 18
    return-void

    .line 19
    :cond_0
    const-string v0, "complete"

    .line 20
    .line 21
    const/4 v1, 0x0

    .line 22
    const-string v2, "CSSmudge"

    .line 23
    .line 24
    invoke-static {v2, v0, v1}, Lcom/intsig/utils/LogMessage;->O8(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 28
    .line 29
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->O0O:Ljava/lang/String;

    .line 30
    .line 31
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇8(Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    :cond_1
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private OOo00(Z)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/4 v0, 0x0

    .line 7
    if-eqz p1, :cond_1

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇OO8ooO8〇:Landroid/view/View;

    .line 10
    .line 11
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 12
    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->oo8ooo8O:Lcom/intsig/camscanner/doodle/widget/DropperTouchView;

    .line 15
    .line 16
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 17
    .line 18
    .line 19
    sget v0, Lcom/intsig/camscanner/doodle/R$id;->l_tools:I

    .line 20
    .line 21
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    const/4 v1, 0x4

    .line 26
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 27
    .line 28
    .line 29
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->O〇o8()V

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇OO8ooO8〇:Landroid/view/View;

    .line 34
    .line 35
    const/16 v2, 0x8

    .line 36
    .line 37
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 38
    .line 39
    .line 40
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->oo8ooo8O:Lcom/intsig/camscanner/doodle/widget/DropperTouchView;

    .line 41
    .line 42
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 43
    .line 44
    .line 45
    sget v1, Lcom/intsig/camscanner/doodle/R$id;->l_tools:I

    .line 46
    .line 47
    invoke-virtual {p0, v1}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 52
    .line 53
    .line 54
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 55
    .line 56
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->setDropperMode(Z)V

    .line 57
    .line 58
    .line 59
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic OO〇〇o0oO(Lcom/intsig/camscanner/doodle/DoodleActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/DoodleActivity;->oO〇O0O(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic OoO〇OOo8o(Lcom/intsig/camscanner/doodle/DoodleActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/DoodleActivity;->O〇00O(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic OooO〇(Lcom/intsig/camscanner/doodle/DoodleActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇o〇88(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic O〇00O(Landroid/view/View;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    check-cast p1, Lcom/intsig/view/color/ColorItemView;

    .line 7
    .line 8
    sget-object v0, Lcom/intsig/camscanner/doodle/DoodleActivity$6;->〇080:[I

    .line 9
    .line 10
    invoke-virtual {p1}, Lcom/intsig/view/color/ColorItemView;->getStatus()Lcom/intsig/view/color/ColorItemView$Status;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    aget v0, v0, v1

    .line 19
    .line 20
    const/4 v1, 0x1

    .line 21
    if-eq v0, v1, :cond_4

    .line 22
    .line 23
    const/4 v2, 0x2

    .line 24
    if-eq v0, v2, :cond_1

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/DoodleActivity;->O〇0o8〇(Lcom/intsig/view/color/ColorItemView;)V

    .line 28
    .line 29
    .line 30
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 31
    .line 32
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    sget-object v0, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->RECTANGLE:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 37
    .line 38
    if-ne p1, v0, :cond_5

    .line 39
    .line 40
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 41
    .line 42
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇0000OOO()Z

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    if-eqz p1, :cond_5

    .line 47
    .line 48
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 49
    .line 50
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllItem()Ljava/util/List;

    .line 51
    .line 52
    .line 53
    move-result-object p1

    .line 54
    invoke-static {p1}, Lcom/intsig/utils/ListUtils;->〇o〇(Ljava/util/List;)Z

    .line 55
    .line 56
    .line 57
    move-result v2

    .line 58
    if-eqz v2, :cond_2

    .line 59
    .line 60
    return-void

    .line 61
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 62
    .line 63
    .line 64
    move-result v2

    .line 65
    sub-int/2addr v2, v1

    .line 66
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 67
    .line 68
    .line 69
    move-result-object p1

    .line 70
    check-cast p1, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;

    .line 71
    .line 72
    instance-of v1, p1, Lcom/intsig/camscanner/doodle/widget/DoodlePath;

    .line 73
    .line 74
    if-eqz v1, :cond_3

    .line 75
    .line 76
    invoke-interface {p1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 77
    .line 78
    .line 79
    move-result-object v1

    .line 80
    if-ne v1, v0, :cond_3

    .line 81
    .line 82
    move-object v0, p1

    .line 83
    check-cast v0, Lcom/intsig/camscanner/doodle/widget/DoodlePath;

    .line 84
    .line 85
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->o0ooO()Z

    .line 86
    .line 87
    .line 88
    move-result v0

    .line 89
    if-eqz v0, :cond_3

    .line 90
    .line 91
    new-instance v0, Lcom/intsig/camscanner/doodle/widget/DoodleColor;

    .line 92
    .line 93
    iget v1, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇0O〇O00O:I

    .line 94
    .line 95
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleColor;-><init>(I)V

    .line 96
    .line 97
    .line 98
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->〇O〇(Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;)V

    .line 99
    .line 100
    .line 101
    :cond_3
    return-void

    .line 102
    :cond_4
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->O80OO()V

    .line 103
    .line 104
    .line 105
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/doodle/DoodleActivity;->OOo00(Z)V

    .line 106
    .line 107
    .line 108
    :cond_5
    :goto_0
    return-void
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private synthetic O〇0o8o8〇(Landroid/view/View;)V
    .locals 9

    .line 1
    const-string p1, "rubber"

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    const-string v1, "CSSmudge"

    .line 5
    .line 6
    invoke-static {v1, p1, v0}, Lcom/intsig/utils/LogMessage;->O8(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 7
    .line 8
    .line 9
    const/16 v3, 0x11

    .line 10
    .line 11
    sget p1, Lcom/intsig/camscanner/doodle/R$string;->a_label_title_eraser:I

    .line 12
    .line 13
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v4

    .line 17
    const/4 v5, 0x0

    .line 18
    const/4 v6, 0x0

    .line 19
    const/4 v7, 0x0

    .line 20
    const/4 v8, 0x1

    .line 21
    move-object v2, p0

    .line 22
    invoke-static/range {v2 .. v8}, Lcom/intsig/utils/ToastUtils;->Oooo8o0〇(Landroid/content/Context;ILjava/lang/String;IIIZ)V

    .line 23
    .line 24
    .line 25
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 26
    .line 27
    sget-object v0, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->ERASER:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 28
    .line 29
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->setPen(Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;)V

    .line 30
    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇0O8Oo()V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private O〇0o8〇(Lcom/intsig/view/color/ColorItemView;)V
    .locals 6
    .param p1    # Lcom/intsig/view/color/ColorItemView;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    sget-object v0, Lcom/intsig/view/color/ColorItemView$Status;->selected:Lcom/intsig/view/color/ColorItemView$Status;

    .line 4
    .line 5
    invoke-virtual {p1, v0}, Lcom/intsig/view/color/ColorItemView;->setStatus(Lcom/intsig/view/color/ColorItemView$Status;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/intsig/view/color/ColorItemView;->getColor()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->o0〇〇00〇o(I)V

    .line 13
    .line 14
    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o〇oO:[Lcom/intsig/view/color/ColorItemView;

    .line 16
    .line 17
    array-length v1, v0

    .line 18
    const/4 v2, 0x0

    .line 19
    :goto_0
    if-ge v2, v1, :cond_2

    .line 20
    .line 21
    aget-object v3, v0, v2

    .line 22
    .line 23
    if-eq v3, p1, :cond_1

    .line 24
    .line 25
    invoke-virtual {v3}, Lcom/intsig/view/color/ColorItemView;->getStatus()Lcom/intsig/view/color/ColorItemView$Status;

    .line 26
    .line 27
    .line 28
    move-result-object v4

    .line 29
    sget-object v5, Lcom/intsig/view/color/ColorItemView$Status;->selected:Lcom/intsig/view/color/ColorItemView$Status;

    .line 30
    .line 31
    if-ne v4, v5, :cond_1

    .line 32
    .line 33
    sget-object v4, Lcom/intsig/view/color/ColorItemView$Status;->normal:Lcom/intsig/view/color/ColorItemView$Status;

    .line 34
    .line 35
    invoke-virtual {v3, v4}, Lcom/intsig/view/color/ColorItemView;->setStatus(Lcom/intsig/view/color/ColorItemView$Status;)V

    .line 36
    .line 37
    .line 38
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_2
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private O〇o8()V
    .locals 10

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o〇o〇Oo88:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 7
    .line 8
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    int-to-float v0, v0

    .line 13
    const/high16 v1, 0x40000000    # 2.0f

    .line 14
    .line 15
    div-float v7, v0, v1

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 18
    .line 19
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    int-to-float v0, v0

    .line 24
    div-float v8, v0, v1

    .line 25
    .line 26
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    .line 27
    .line 28
    .line 29
    move-result-wide v2

    .line 30
    const-wide/16 v0, 0x14

    .line 31
    .line 32
    add-long v4, v2, v0

    .line 33
    .line 34
    const/4 v6, 0x1

    .line 35
    const/4 v9, 0x0

    .line 36
    invoke-static/range {v2 .. v9}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 41
    .line 42
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 43
    .line 44
    .line 45
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 46
    .line 47
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oo〇()V

    .line 48
    .line 49
    .line 50
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic O〇〇o8O(Lcom/intsig/camscanner/doodle/DoodleActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->O80OO()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o088O8800()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o〇oO:[Lcom/intsig/view/color/ColorItemView;

    .line 2
    .line 3
    array-length v0, v0

    .line 4
    const/4 v1, 0x0

    .line 5
    :goto_0
    if-ge v1, v0, :cond_1

    .line 6
    .line 7
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o〇oO:[Lcom/intsig/view/color/ColorItemView;

    .line 8
    .line 9
    aget-object v2, v2, v1

    .line 10
    .line 11
    invoke-virtual {v2}, Lcom/intsig/view/color/ColorItemView;->getStatus()Lcom/intsig/view/color/ColorItemView$Status;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    sget-object v3, Lcom/intsig/view/color/ColorItemView$Status;->selected:Lcom/intsig/view/color/ColorItemView$Status;

    .line 16
    .line 17
    if-ne v2, v3, :cond_0

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->oO〇8O8oOo:Lcom/intsig/camscanner/doodle/DoodleConfig;

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/doodle/DoodleConfig;->o〇0(I)V

    .line 22
    .line 23
    .line 24
    goto :goto_1

    .line 25
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_1
    :goto_1
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic o0O0O〇〇〇0(Lcom/intsig/camscanner/doodle/DoodleActivity;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o〇o〇Oo88:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o0OO(Lcom/intsig/camscanner/doodle/DoodleActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->OO8〇O8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o0〇〇00〇o(I)V
    .locals 2

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇0O〇O00O:I

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    new-instance v1, Lcom/intsig/camscanner/doodle/widget/DoodleColor;

    .line 8
    .line 9
    invoke-direct {v1, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleColor;-><init>(I)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->setColor(Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic o88o88(Landroid/view/View;)V
    .locals 2

    .line 1
    const-string p1, "get_color"

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    const-string v1, "CSSmudge"

    .line 5
    .line 6
    invoke-static {v1, p1, v0}, Lcom/intsig/utils/LogMessage;->O8(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 7
    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->O80OO()V

    .line 10
    .line 11
    .line 12
    const/4 p1, 0x1

    .line 13
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/DoodleActivity;->OOo00(Z)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o88oo〇O()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getItemCount()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getRedoItemCount()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    const/16 v2, 0x8

    .line 14
    .line 15
    const/4 v3, 0x0

    .line 16
    if-gtz v0, :cond_1

    .line 17
    .line 18
    if-lez v1, :cond_0

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->Ooo08:Landroid/widget/TextView;

    .line 22
    .line 23
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇00O0:Landroid/view/View;

    .line 27
    .line 28
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 29
    .line 30
    .line 31
    goto :goto_2

    .line 32
    :cond_1
    :goto_0
    iget-object v4, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->Ooo08:Landroid/widget/TextView;

    .line 33
    .line 34
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 35
    .line 36
    .line 37
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇00O0:Landroid/view/View;

    .line 38
    .line 39
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 40
    .line 41
    .line 42
    const/high16 v2, 0x3f800000    # 1.0f

    .line 43
    .line 44
    const/4 v4, 0x1

    .line 45
    const v5, 0x3e6b851f    # 0.23f

    .line 46
    .line 47
    .line 48
    if-lez v0, :cond_2

    .line 49
    .line 50
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->O〇08oOOO0:Landroid/view/View;

    .line 51
    .line 52
    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 53
    .line 54
    .line 55
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->O〇08oOOO0:Landroid/view/View;

    .line 56
    .line 57
    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 58
    .line 59
    .line 60
    goto :goto_1

    .line 61
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->O〇08oOOO0:Landroid/view/View;

    .line 62
    .line 63
    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 64
    .line 65
    .line 66
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->O〇08oOOO0:Landroid/view/View;

    .line 67
    .line 68
    invoke-virtual {v0, v5}, Landroid/view/View;->setAlpha(F)V

    .line 69
    .line 70
    .line 71
    :goto_1
    if-lez v1, :cond_3

    .line 72
    .line 73
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o8〇OO:Landroid/view/View;

    .line 74
    .line 75
    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 76
    .line 77
    .line 78
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o8〇OO:Landroid/view/View;

    .line 79
    .line 80
    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 81
    .line 82
    .line 83
    goto :goto_2

    .line 84
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o8〇OO:Landroid/view/View;

    .line 85
    .line 86
    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 87
    .line 88
    .line 89
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o8〇OO:Landroid/view/View;

    .line 90
    .line 91
    invoke-virtual {v0, v5}, Landroid/view/View;->setAlpha(F)V

    .line 92
    .line 93
    .line 94
    :goto_2
    return-void
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private synthetic o8O〇008(Landroid/view/View;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 2
    .line 3
    const/4 v0, 0x1

    .line 4
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O8〇o(I)Z

    .line 5
    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->o88oo〇O()V

    .line 8
    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->O80OO()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o8o0o8()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->oO〇8O8oOo:Lcom/intsig/camscanner/doodle/DoodleConfig;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/camscanner/doodle/DoodleConfig;->〇o00〇〇Oo:Ljava/util/List;

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    const/4 v2, 0x0

    .line 10
    :goto_0
    if-ge v2, v1, :cond_1

    .line 11
    .line 12
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 13
    .line 14
    .line 15
    move-result-object v3

    .line 16
    check-cast v3, Ljava/lang/Integer;

    .line 17
    .line 18
    if-nez v3, :cond_0

    .line 19
    .line 20
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇08〇o0O:[Lcom/intsig/view/color/ColorItemView;

    .line 21
    .line 22
    aget-object v3, v3, v2

    .line 23
    .line 24
    sget-object v4, Lcom/intsig/view/color/ColorItemView$Status;->unset:Lcom/intsig/view/color/ColorItemView$Status;

    .line 25
    .line 26
    invoke-virtual {v3, v4}, Lcom/intsig/view/color/ColorItemView;->setStatus(Lcom/intsig/view/color/ColorItemView$Status;)V

    .line 27
    .line 28
    .line 29
    goto :goto_1

    .line 30
    :cond_0
    iget-object v4, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇08〇o0O:[Lcom/intsig/view/color/ColorItemView;

    .line 31
    .line 32
    aget-object v4, v4, v2

    .line 33
    .line 34
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 35
    .line 36
    .line 37
    move-result v3

    .line 38
    invoke-virtual {v4, v3}, Lcom/intsig/view/color/ColorItemView;->setColor(I)V

    .line 39
    .line 40
    .line 41
    :goto_1
    add-int/lit8 v2, v2, 0x1

    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_1
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private oO8(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇OO8ooO8〇:Landroid/view/View;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-ne p1, v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇OO8ooO8〇:Landroid/view/View;

    .line 11
    .line 12
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic oOO8oo0(Lcom/intsig/camscanner/doodle/DoodleActivity;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇00o〇O8(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic oO〇O0O(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇80〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o〇08oO80o(Lcom/intsig/camscanner/doodle/DoodleActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/DoodleActivity;->o8O〇008(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic o〇OoO0(Lcom/intsig/camscanner/doodle/DoodleActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->o88oo〇O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o〇o08〇(Lcom/intsig/camscanner/doodle/DoodleActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇8o0o0(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇00o〇O8(I)V
    .locals 3

    .line 1
    sget v0, Lcom/intsig/camscanner/doodle/DoodleConfig;->O8:I

    .line 2
    .line 3
    int-to-float v1, p1

    .line 4
    invoke-static {v1}, Lcom/intsig/camscanner/doodle/util/DoodleUtils;->〇080(F)I

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    add-int/2addr v0, v1

    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    check-cast v1, Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 16
    .line 17
    if-eqz v1, :cond_0

    .line 18
    .line 19
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->oO〇8O8oOo:Lcom/intsig/camscanner/doodle/DoodleConfig;

    .line 20
    .line 21
    invoke-virtual {v2, v1, p1}, Lcom/intsig/camscanner/doodle/DoodleConfig;->〇〇888(Lcom/intsig/camscanner/doodle/widget/DoodlePen;I)V

    .line 22
    .line 23
    .line 24
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 25
    .line 26
    if-eqz p1, :cond_1

    .line 27
    .line 28
    int-to-float v0, v0

    .line 29
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->setSize(F)V

    .line 30
    .line 31
    .line 32
    :cond_1
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private 〇0O8Oo()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sget-object v1, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->ERASER:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 8
    .line 9
    const/high16 v2, 0x3f800000    # 1.0f

    .line 10
    .line 11
    const/16 v3, 0x8

    .line 12
    .line 13
    const/4 v4, 0x1

    .line 14
    const/4 v5, 0x0

    .line 15
    if-ne v0, v1, :cond_0

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->O〇o88o08〇:Landroid/widget/SeekBar;

    .line 18
    .line 19
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->Oo80:Landroid/widget/SeekBar;

    .line 23
    .line 24
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->Oo80:Landroid/widget/SeekBar;

    .line 28
    .line 29
    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 30
    .line 31
    .line 32
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->O〇o88o08〇:Landroid/widget/SeekBar;

    .line 33
    .line 34
    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 35
    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->O〇o88o08〇:Landroid/widget/SeekBar;

    .line 38
    .line 39
    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getProgress()I

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇00o〇O8(I)V

    .line 44
    .line 45
    .line 46
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇〇o〇:Landroid/widget/ImageView;

    .line 47
    .line 48
    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 49
    .line 50
    .line 51
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇OO〇00〇0O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 52
    .line 53
    invoke-virtual {v0, v5}, Landroid/view/View;->setSelected(Z)V

    .line 54
    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->ooO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 57
    .line 58
    invoke-virtual {v0, v5}, Landroid/view/View;->setSelected(Z)V

    .line 59
    .line 60
    .line 61
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 62
    .line 63
    sget-object v1, Lcom/intsig/camscanner/doodle/widget/DoodleShape;->HAND_WRITE:Lcom/intsig/camscanner/doodle/widget/DoodleShape;

    .line 64
    .line 65
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->setShape(Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;)V

    .line 66
    .line 67
    .line 68
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->Oo0〇Ooo:Landroid/widget/LinearLayout;

    .line 69
    .line 70
    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 71
    .line 72
    .line 73
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇〇〇0o〇〇0:Landroid/widget/RelativeLayout;

    .line 74
    .line 75
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 76
    .line 77
    .line 78
    goto/16 :goto_0

    .line 79
    .line 80
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 81
    .line 82
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    sget-object v1, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->BRUSH:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 87
    .line 88
    if-ne v0, v1, :cond_1

    .line 89
    .line 90
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->Oo80:Landroid/widget/SeekBar;

    .line 91
    .line 92
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 93
    .line 94
    .line 95
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->O〇o88o08〇:Landroid/widget/SeekBar;

    .line 96
    .line 97
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 98
    .line 99
    .line 100
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->Oo80:Landroid/widget/SeekBar;

    .line 101
    .line 102
    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 103
    .line 104
    .line 105
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->O〇o88o08〇:Landroid/widget/SeekBar;

    .line 106
    .line 107
    invoke-virtual {v0, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 108
    .line 109
    .line 110
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->Oo80:Landroid/widget/SeekBar;

    .line 111
    .line 112
    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getProgress()I

    .line 113
    .line 114
    .line 115
    move-result v0

    .line 116
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇00o〇O8(I)V

    .line 117
    .line 118
    .line 119
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇〇o〇:Landroid/widget/ImageView;

    .line 120
    .line 121
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 122
    .line 123
    .line 124
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇OO〇00〇0O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 125
    .line 126
    invoke-virtual {v0, v5}, Landroid/view/View;->setSelected(Z)V

    .line 127
    .line 128
    .line 129
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->ooO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 130
    .line 131
    invoke-virtual {v0, v4}, Landroid/view/View;->setSelected(Z)V

    .line 132
    .line 133
    .line 134
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 135
    .line 136
    sget-object v1, Lcom/intsig/camscanner/doodle/widget/DoodleShape;->HAND_WRITE:Lcom/intsig/camscanner/doodle/widget/DoodleShape;

    .line 137
    .line 138
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->setShape(Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;)V

    .line 139
    .line 140
    .line 141
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->Oo0〇Ooo:Landroid/widget/LinearLayout;

    .line 142
    .line 143
    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 144
    .line 145
    .line 146
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇〇〇0o〇〇0:Landroid/widget/RelativeLayout;

    .line 147
    .line 148
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 149
    .line 150
    .line 151
    goto :goto_0

    .line 152
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 153
    .line 154
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getPen()Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;

    .line 155
    .line 156
    .line 157
    move-result-object v0

    .line 158
    sget-object v1, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->RECTANGLE:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 159
    .line 160
    if-ne v0, v1, :cond_2

    .line 161
    .line 162
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->Oo80:Landroid/widget/SeekBar;

    .line 163
    .line 164
    invoke-virtual {v0, v5}, Landroid/view/View;->setEnabled(Z)V

    .line 165
    .line 166
    .line 167
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->O〇o88o08〇:Landroid/widget/SeekBar;

    .line 168
    .line 169
    invoke-virtual {v0, v5}, Landroid/view/View;->setEnabled(Z)V

    .line 170
    .line 171
    .line 172
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->Oo80:Landroid/widget/SeekBar;

    .line 173
    .line 174
    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getProgress()I

    .line 175
    .line 176
    .line 177
    move-result v0

    .line 178
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇00o〇O8(I)V

    .line 179
    .line 180
    .line 181
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇〇o〇:Landroid/widget/ImageView;

    .line 182
    .line 183
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 184
    .line 185
    .line 186
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇OO〇00〇0O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 187
    .line 188
    invoke-virtual {v0, v4}, Landroid/view/View;->setSelected(Z)V

    .line 189
    .line 190
    .line 191
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->ooO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 192
    .line 193
    invoke-virtual {v0, v5}, Landroid/view/View;->setSelected(Z)V

    .line 194
    .line 195
    .line 196
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 197
    .line 198
    sget-object v1, Lcom/intsig/camscanner/doodle/widget/DoodleShape;->FILL_RECT:Lcom/intsig/camscanner/doodle/widget/DoodleShape;

    .line 199
    .line 200
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->setShape(Lcom/intsig/camscanner/doodle/widget/core/IDoodleShape;)V

    .line 201
    .line 202
    .line 203
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->Oo0〇Ooo:Landroid/widget/LinearLayout;

    .line 204
    .line 205
    const/high16 v1, 0x3f000000    # 0.5f

    .line 206
    .line 207
    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 208
    .line 209
    .line 210
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇〇〇0o〇〇0:Landroid/widget/RelativeLayout;

    .line 211
    .line 212
    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 213
    .line 214
    .line 215
    :cond_2
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->O80OO()V

    .line 216
    .line 217
    .line 218
    return-void
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method static bridge synthetic 〇0o0oO〇〇0(Lcom/intsig/camscanner/doodle/DoodleActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->O088O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇0o88Oo〇(Lcom/intsig/camscanner/doodle/DoodleActivity;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o8o:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇80〇()V
    .locals 3

    .line 1
    const-string v0, "get_color_success"

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const-string v2, "CSSmudge"

    .line 5
    .line 6
    invoke-static {v2, v0, v1}, Lcom/intsig/utils/LogMessage;->O8(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 7
    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->OOo00(Z)V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->oO〇8O8oOo:Lcom/intsig/camscanner/doodle/DoodleConfig;

    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->oo8ooo8O:Lcom/intsig/camscanner/doodle/widget/DropperTouchView;

    .line 16
    .line 17
    invoke-virtual {v1}, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->getColor()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/doodle/DoodleConfig;->〇080(I)V

    .line 22
    .line 23
    .line 24
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->o8o0o8()V

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->oO〇8O8oOo:Lcom/intsig/camscanner/doodle/DoodleConfig;

    .line 28
    .line 29
    iget-object v0, v0, Lcom/intsig/camscanner/doodle/DoodleConfig;->〇o00〇〇Oo:Ljava/util/List;

    .line 30
    .line 31
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    add-int/lit8 v0, v0, -0x1

    .line 36
    .line 37
    sget-object v1, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->BRUSH:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 38
    .line 39
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇08〇o0O:[Lcom/intsig/view/color/ColorItemView;

    .line 40
    .line 41
    aget-object v0, v2, v0

    .line 42
    .line 43
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->O08〇oO8〇(Lcom/intsig/camscanner/doodle/widget/DoodlePen;Lcom/intsig/view/color/ColorItemView;)V

    .line 44
    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic 〇8o0o0(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇8oo8888()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇8oo8888()V
    .locals 3

    .line 1
    const-string v0, "tips"

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const-string v2, "CSSmudge"

    .line 5
    .line 6
    invoke-static {v2, v0, v1}, Lcom/intsig/utils/LogMessage;->O8(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 7
    .line 8
    .line 9
    new-instance v0, Lcom/intsig/camscanner/doodle/EyedropperGuideDialog;

    .line 10
    .line 11
    invoke-direct {v0}, Lcom/intsig/camscanner/doodle/EyedropperGuideDialog;-><init>()V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    const-string v2, "eyedropper"

    .line 19
    .line 20
    invoke-virtual {v0, v1, v2}, Lcom/intsig/app/BaseDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic 〇8〇〇8o(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇80〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇OoO0o0(Lcom/intsig/camscanner/doodle/DoodleActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/DoodleActivity;->o88o88(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇oOO80o(Lcom/intsig/camscanner/doodle/DoodleActivity;)Lcom/intsig/camscanner/doodle/widget/DropperTouchView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->oo8ooo8O:Lcom/intsig/camscanner/doodle/widget/DropperTouchView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇ooO8Ooo〇(Lcom/intsig/camscanner/doodle/DoodleActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇8〇〇8o(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇ooO〇000(Lcom/intsig/camscanner/doodle/DoodleActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/DoodleActivity;->O0o0(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic 〇o〇88(Landroid/view/View;)V
    .locals 0

    .line 1
    const/4 p1, 0x0

    .line 2
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/DoodleActivity;->OOo00(Z)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇o〇OO80oO()V
    .locals 4

    .line 1
    sget v0, Lcom/intsig/camscanner/doodle/R$id;->v_pen_size:I

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o8o:Landroid/view/View;

    .line 8
    .line 9
    sget v0, Lcom/intsig/camscanner/doodle/R$id;->v_dropper_touch:I

    .line 10
    .line 11
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;

    .line 16
    .line 17
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->oo8ooo8O:Lcom/intsig/camscanner/doodle/widget/DropperTouchView;

    .line 18
    .line 19
    sget v0, Lcom/intsig/camscanner/doodle/R$id;->iv_eraser:I

    .line 20
    .line 21
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    check-cast v0, Landroid/widget/ImageView;

    .line 26
    .line 27
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇〇o〇:Landroid/widget/ImageView;

    .line 28
    .line 29
    sget v0, Lcom/intsig/camscanner/doodle/R$id;->sb_brush:I

    .line 30
    .line 31
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    check-cast v0, Landroid/widget/SeekBar;

    .line 36
    .line 37
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->Oo80:Landroid/widget/SeekBar;

    .line 38
    .line 39
    sget v0, Lcom/intsig/camscanner/doodle/R$id;->sb_eraser:I

    .line 40
    .line 41
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    check-cast v0, Landroid/widget/SeekBar;

    .line 46
    .line 47
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->O〇o88o08〇:Landroid/widget/SeekBar;

    .line 48
    .line 49
    sget v0, Lcom/intsig/camscanner/doodle/R$id;->l_undo_redo:I

    .line 50
    .line 51
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇00O0:Landroid/view/View;

    .line 56
    .line 57
    sget v0, Lcom/intsig/camscanner/doodle/R$id;->iv_undo:I

    .line 58
    .line 59
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->O〇08oOOO0:Landroid/view/View;

    .line 64
    .line 65
    sget v0, Lcom/intsig/camscanner/doodle/R$id;->iv_redo:I

    .line 66
    .line 67
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o8〇OO:Landroid/view/View;

    .line 72
    .line 73
    sget v0, Lcom/intsig/camscanner/doodle/R$id;->tv_hint:I

    .line 74
    .line 75
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 76
    .line 77
    .line 78
    move-result-object v0

    .line 79
    check-cast v0, Landroid/widget/TextView;

    .line 80
    .line 81
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->Ooo08:Landroid/widget/TextView;

    .line 82
    .line 83
    sget v0, Lcom/intsig/camscanner/doodle/R$id;->l_dropper_nav:I

    .line 84
    .line 85
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 86
    .line 87
    .line 88
    move-result-object v0

    .line 89
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇OO8ooO8〇:Landroid/view/View;

    .line 90
    .line 91
    sget v0, Lcom/intsig/camscanner/doodle/R$id;->v_brush_mode:I

    .line 92
    .line 93
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    check-cast v0, Landroidx/appcompat/widget/AppCompatImageView;

    .line 98
    .line 99
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->ooO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 100
    .line 101
    sget v0, Lcom/intsig/camscanner/doodle/R$id;->v_rectangle_mode:I

    .line 102
    .line 103
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 104
    .line 105
    .line 106
    move-result-object v0

    .line 107
    check-cast v0, Landroidx/appcompat/widget/AppCompatImageView;

    .line 108
    .line 109
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇OO〇00〇0O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 110
    .line 111
    sget v0, Lcom/intsig/camscanner/doodle/R$id;->l_size:I

    .line 112
    .line 113
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 114
    .line 115
    .line 116
    move-result-object v0

    .line 117
    check-cast v0, Landroid/widget/LinearLayout;

    .line 118
    .line 119
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->Oo0〇Ooo:Landroid/widget/LinearLayout;

    .line 120
    .line 121
    sget v0, Lcom/intsig/camscanner/doodle/R$id;->rl_color_layout:I

    .line 122
    .line 123
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 124
    .line 125
    .line 126
    move-result-object v0

    .line 127
    check-cast v0, Landroid/widget/RelativeLayout;

    .line 128
    .line 129
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇〇〇0o〇〇0:Landroid/widget/RelativeLayout;

    .line 130
    .line 131
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->ooO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 132
    .line 133
    const/4 v1, 0x1

    .line 134
    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 135
    .line 136
    .line 137
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇OO〇00〇0O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 138
    .line 139
    const/4 v2, 0x0

    .line 140
    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 141
    .line 142
    .line 143
    const/4 v0, 0x2

    .line 144
    new-array v0, v0, [Landroid/view/View;

    .line 145
    .line 146
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->ooO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 147
    .line 148
    aput-object v3, v0, v2

    .line 149
    .line 150
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇OO〇00〇0O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 151
    .line 152
    aput-object v3, v0, v1

    .line 153
    .line 154
    invoke-virtual {p0, v0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇O8〇8O0oO([Landroid/view/View;)V

    .line 155
    .line 156
    .line 157
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->O〇o88o08〇:Landroid/widget/SeekBar;

    .line 158
    .line 159
    sget v1, Lcom/intsig/camscanner/doodle/DoodleConfig;->Oo08:I

    .line 160
    .line 161
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 162
    .line 163
    .line 164
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->Oo80:Landroid/widget/SeekBar;

    .line 165
    .line 166
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 167
    .line 168
    .line 169
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇〇o〇:Landroid/widget/ImageView;

    .line 170
    .line 171
    new-instance v1, L〇O8〇OO〇/〇o〇;

    .line 172
    .line 173
    invoke-direct {v1, p0}, L〇O8〇OO〇/〇o〇;-><init>(Lcom/intsig/camscanner/doodle/DoodleActivity;)V

    .line 174
    .line 175
    .line 176
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177
    .line 178
    .line 179
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->O〇08oOOO0:Landroid/view/View;

    .line 180
    .line 181
    new-instance v1, L〇O8〇OO〇/O8;

    .line 182
    .line 183
    invoke-direct {v1, p0}, L〇O8〇OO〇/O8;-><init>(Lcom/intsig/camscanner/doodle/DoodleActivity;)V

    .line 184
    .line 185
    .line 186
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 187
    .line 188
    .line 189
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o8〇OO:Landroid/view/View;

    .line 190
    .line 191
    new-instance v1, L〇O8〇OO〇/Oo08;

    .line 192
    .line 193
    invoke-direct {v1, p0}, L〇O8〇OO〇/Oo08;-><init>(Lcom/intsig/camscanner/doodle/DoodleActivity;)V

    .line 194
    .line 195
    .line 196
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    .line 198
    .line 199
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->O〇o88o08〇:Landroid/widget/SeekBar;

    .line 200
    .line 201
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->oO〇8O8oOo:Lcom/intsig/camscanner/doodle/DoodleConfig;

    .line 202
    .line 203
    sget-object v3, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->ERASER:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 204
    .line 205
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/doodle/DoodleConfig;->〇o〇(Lcom/intsig/camscanner/doodle/widget/DoodlePen;)I

    .line 206
    .line 207
    .line 208
    move-result v1

    .line 209
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 210
    .line 211
    .line 212
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->Oo80:Landroid/widget/SeekBar;

    .line 213
    .line 214
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->oO〇8O8oOo:Lcom/intsig/camscanner/doodle/DoodleConfig;

    .line 215
    .line 216
    sget-object v3, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->BRUSH:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 217
    .line 218
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/doodle/DoodleConfig;->〇o〇(Lcom/intsig/camscanner/doodle/widget/DoodlePen;)I

    .line 219
    .line 220
    .line 221
    move-result v1

    .line 222
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 223
    .line 224
    .line 225
    new-instance v0, Lcom/intsig/camscanner/doodle/DoodleActivity$1;

    .line 226
    .line 227
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/doodle/DoodleActivity$1;-><init>(Lcom/intsig/camscanner/doodle/DoodleActivity;)V

    .line 228
    .line 229
    .line 230
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->O〇o88o08〇:Landroid/widget/SeekBar;

    .line 231
    .line 232
    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 233
    .line 234
    .line 235
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->Oo80:Landroid/widget/SeekBar;

    .line 236
    .line 237
    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 238
    .line 239
    .line 240
    sget v0, Lcom/intsig/camscanner/doodle/R$id;->iv_cancel:I

    .line 241
    .line 242
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 243
    .line 244
    .line 245
    move-result-object v0

    .line 246
    new-instance v1, L〇O8〇OO〇/o〇0;

    .line 247
    .line 248
    invoke-direct {v1, p0}, L〇O8〇OO〇/o〇0;-><init>(Lcom/intsig/camscanner/doodle/DoodleActivity;)V

    .line 249
    .line 250
    .line 251
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 252
    .line 253
    .line 254
    sget v0, Lcom/intsig/camscanner/doodle/R$id;->iv_save:I

    .line 255
    .line 256
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 257
    .line 258
    .line 259
    move-result-object v0

    .line 260
    new-instance v1, Lcom/intsig/camscanner/doodle/DoodleActivity$2;

    .line 261
    .line 262
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/doodle/DoodleActivity$2;-><init>(Lcom/intsig/camscanner/doodle/DoodleActivity;)V

    .line 263
    .line 264
    .line 265
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 266
    .line 267
    .line 268
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->O8〇o0〇〇8()V

    .line 269
    .line 270
    .line 271
    sget v0, Lcom/intsig/camscanner/doodle/R$id;->iv_dropper:I

    .line 272
    .line 273
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 274
    .line 275
    .line 276
    move-result-object v0

    .line 277
    new-instance v1, L〇O8〇OO〇/〇〇888;

    .line 278
    .line 279
    invoke-direct {v1, p0}, L〇O8〇OO〇/〇〇888;-><init>(Lcom/intsig/camscanner/doodle/DoodleActivity;)V

    .line 280
    .line 281
    .line 282
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 283
    .line 284
    .line 285
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->oo8ooo8O:Lcom/intsig/camscanner/doodle/widget/DropperTouchView;

    .line 286
    .line 287
    new-instance v1, L〇O8〇OO〇/oO80;

    .line 288
    .line 289
    invoke-direct {v1, p0}, L〇O8〇OO〇/oO80;-><init>(Lcom/intsig/camscanner/doodle/DoodleActivity;)V

    .line 290
    .line 291
    .line 292
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/doodle/widget/DropperTouchView;->setDoneClickListener(Landroid/view/View$OnClickListener;)V

    .line 293
    .line 294
    .line 295
    sget v0, Lcom/intsig/camscanner/doodle/R$id;->iv_back:I

    .line 296
    .line 297
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 298
    .line 299
    .line 300
    move-result-object v0

    .line 301
    new-instance v1, L〇O8〇OO〇/〇80〇808〇O;

    .line 302
    .line 303
    invoke-direct {v1, p0}, L〇O8〇OO〇/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/doodle/DoodleActivity;)V

    .line 304
    .line 305
    .line 306
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 307
    .line 308
    .line 309
    sget v0, Lcom/intsig/camscanner/doodle/R$id;->iv_confirm:I

    .line 310
    .line 311
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 312
    .line 313
    .line 314
    move-result-object v0

    .line 315
    new-instance v1, L〇O8〇OO〇/OO0o〇〇〇〇0;

    .line 316
    .line 317
    invoke-direct {v1, p0}, L〇O8〇OO〇/OO0o〇〇〇〇0;-><init>(Lcom/intsig/camscanner/doodle/DoodleActivity;)V

    .line 318
    .line 319
    .line 320
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 321
    .line 322
    .line 323
    sget v0, Lcom/intsig/camscanner/doodle/R$id;->tv_help:I

    .line 324
    .line 325
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 326
    .line 327
    .line 328
    move-result-object v0

    .line 329
    new-instance v1, L〇O8〇OO〇/〇8o8o〇;

    .line 330
    .line 331
    invoke-direct {v1, p0}, L〇O8〇OO〇/〇8o8o〇;-><init>(Lcom/intsig/camscanner/doodle/DoodleActivity;)V

    .line 332
    .line 333
    .line 334
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 335
    .line 336
    .line 337
    sget v0, Lcom/intsig/camscanner/doodle/R$id;->v_dropper_free:I

    .line 338
    .line 339
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 340
    .line 341
    .line 342
    move-result-object v0

    .line 343
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 344
    .line 345
    .line 346
    return-void
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private synthetic 〇〇〇OOO〇〇(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇Oo〇O()Z

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-nez p1, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 8
    .line 9
    .line 10
    :cond_0
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public OO8〇(Lcom/intsig/camscanner/doodle/widget/core/IDoodle;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇o〇OO80oO()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->BRUSH:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 5
    .line 6
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/doodle/widget/core/IDoodle;->setPen(Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;)V

    .line 7
    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->oO〇8O8oOo:Lcom/intsig/camscanner/doodle/DoodleConfig;

    .line 10
    .line 11
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/doodle/DoodleConfig;->〇o〇(Lcom/intsig/camscanner/doodle/widget/DoodlePen;)I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    int-to-float v0, v0

    .line 16
    invoke-static {v0}, Lcom/intsig/camscanner/doodle/util/DoodleUtils;->〇080(F)I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    int-to-float v0, v0

    .line 21
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/doodle/widget/core/IDoodle;->setSize(F)V

    .line 22
    .line 23
    .line 24
    iget p1, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇0O〇O00O:I

    .line 25
    .line 26
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/DoodleActivity;->o0〇〇00〇o(I)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method protected Ooo8o()I
    .locals 1

    .line 1
    sget v0, Lcom/intsig/camscanner/doodle/R$color;->cs_black:I

    .line 2
    .line 3
    invoke-static {p0, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected O〇〇O80o8(Landroid/graphics/Bitmap;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->O〇〇O80o8(Landroid/graphics/Bitmap;)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 5
    .line 6
    if-nez p1, :cond_0

    .line 7
    .line 8
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 9
    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    new-instance p1, Lcom/intsig/camscanner/doodle/DoodleActivity$3;

    .line 13
    .line 14
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/doodle/DoodleActivity$3;-><init>(Lcom/intsig/camscanner/doodle/DoodleActivity;)V

    .line 15
    .line 16
    .line 17
    new-instance v0, Lcom/intsig/camscanner/doodle/DoodleActivity$4;

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 20
    .line 21
    invoke-direct {v0, p0, v1, p1}, Lcom/intsig/camscanner/doodle/DoodleActivity$4;-><init>(Lcom/intsig/camscanner/doodle/DoodleActivity;Lcom/intsig/camscanner/doodle/widget/DoodleView;Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$ISelectionListener;)V

    .line 22
    .line 23
    .line 24
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o0OoOOo0:Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;

    .line 25
    .line 26
    new-instance p1, Lcom/intsig/camscanner/doodle/widget/DoodleTouchDetector;

    .line 27
    .line 28
    invoke-direct {p1, p0, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleTouchDetector;-><init>(Landroid/content/Context;Lcom/intsig/camscanner/doodle/widget/TouchGestureDetector$IOnTouchGestureListener;)V

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 32
    .line 33
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->setDefaultTouchDetector(Lcom/intsig/camscanner/doodle/widget/core/IDoodleTouchDetector;)V

    .line 34
    .line 35
    .line 36
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 37
    .line 38
    const/4 v0, 0x1

    .line 39
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇O888o0o(Z)V

    .line 40
    .line 41
    .line 42
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 43
    .line 44
    const/high16 v0, 0x40200000    # 2.5f

    .line 45
    .line 46
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->setZoomerScale(F)V

    .line 47
    .line 48
    .line 49
    sget p1, Lcom/intsig/camscanner/doodle/R$id;->l_doodle:I

    .line 50
    .line 51
    invoke-virtual {p0, p1}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    check-cast p1, Landroid/view/ViewGroup;

    .line 56
    .line 57
    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 58
    .line 59
    const/4 v1, -0x1

    .line 60
    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 61
    .line 62
    .line 63
    const/high16 v1, 0x41a00000    # 20.0f

    .line 64
    .line 65
    invoke-static {v1}, Lcom/intsig/camscanner/doodle/util/DoodleUtils;->〇080(F)I

    .line 66
    .line 67
    .line 68
    move-result v1

    .line 69
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 70
    .line 71
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 72
    .line 73
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 74
    .line 75
    const/4 v2, 0x0

    .line 76
    invoke-virtual {p1, v1, v2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 77
    .line 78
    .line 79
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 80
    .line 81
    new-instance v0, Lcom/intsig/camscanner/doodle/DoodleActivity$5;

    .line 82
    .line 83
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/doodle/DoodleActivity$5;-><init>(Lcom/intsig/camscanner/doodle/DoodleActivity;)V

    .line 84
    .line 85
    .line 86
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->setDropperTouchListener(Lcom/intsig/camscanner/doodle/widget/core/IDropperTouchListener;)V

    .line 87
    .line 88
    .line 89
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public dealClickAction(Landroid/view/View;)V
    .locals 11

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    sget v0, Lcom/intsig/camscanner/doodle/R$id;->v_brush_mode:I

    .line 6
    .line 7
    const-string v1, "type"

    .line 8
    .line 9
    const-string v2, "smudge"

    .line 10
    .line 11
    const-string v3, "CSRevise"

    .line 12
    .line 13
    if-ne p1, v0, :cond_0

    .line 14
    .line 15
    const-string p1, "round"

    .line 16
    .line 17
    invoke-static {v3, v2, v1, p1}, Lcom/intsig/utils/LogMessage;->〇o〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    const/16 v5, 0x11

    .line 21
    .line 22
    sget p1, Lcom/intsig/camscanner/doodle/R$string;->cs_531_smear_brush:I

    .line 23
    .line 24
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v6

    .line 28
    const/4 v7, 0x0

    .line 29
    const/4 v8, 0x0

    .line 30
    const/4 v9, 0x0

    .line 31
    const/4 v10, 0x1

    .line 32
    move-object v4, p0

    .line 33
    invoke-static/range {v4 .. v10}, Lcom/intsig/utils/ToastUtils;->Oooo8o0〇(Landroid/content/Context;ILjava/lang/String;IIIZ)V

    .line 34
    .line 35
    .line 36
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 37
    .line 38
    sget-object v0, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->BRUSH:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 39
    .line 40
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->setPen(Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;)V

    .line 41
    .line 42
    .line 43
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇0O8Oo()V

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_0
    sget v0, Lcom/intsig/camscanner/doodle/R$id;->v_rectangle_mode:I

    .line 48
    .line 49
    if-ne p1, v0, :cond_1

    .line 50
    .line 51
    const-string p1, "rectangle"

    .line 52
    .line 53
    invoke-static {v3, v2, v1, p1}, Lcom/intsig/utils/LogMessage;->〇o〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    const/16 v5, 0x11

    .line 57
    .line 58
    sget p1, Lcom/intsig/camscanner/doodle/R$string;->cs_531_smear_rectangle:I

    .line 59
    .line 60
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v6

    .line 64
    const/4 v7, 0x0

    .line 65
    const/4 v8, 0x0

    .line 66
    const/4 v9, 0x0

    .line 67
    const/4 v10, 0x1

    .line 68
    move-object v4, p0

    .line 69
    invoke-static/range {v4 .. v10}, Lcom/intsig/utils/ToastUtils;->Oooo8o0〇(Landroid/content/Context;ILjava/lang/String;IIIZ)V

    .line 70
    .line 71
    .line 72
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 73
    .line 74
    sget-object v0, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->RECTANGLE:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 75
    .line 76
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->setPen(Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;)V

    .line 77
    .line 78
    .line 79
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->〇0O8Oo()V

    .line 80
    .line 81
    .line 82
    :cond_1
    :goto_0
    return-void
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->O0O:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    if-nez p1, :cond_0

    .line 8
    .line 9
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/doodle/Doodle;->Oo08()Lcom/intsig/camscanner/doodle/util/IDoodleProxy;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    if-eqz p1, :cond_1

    .line 18
    .line 19
    invoke-interface {p1, p0}, Lcom/intsig/camscanner/doodle/util/IDoodleProxy;->O8(Landroid/app/Activity;)V

    .line 20
    .line 21
    .line 22
    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    if-eqz p1, :cond_2

    .line 27
    .line 28
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    const/high16 v0, -0x1000000

    .line 33
    .line 34
    invoke-virtual {p1, v0}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 35
    .line 36
    .line 37
    :cond_2
    invoke-static {}, Lcom/intsig/camscanner/doodle/DoodleConfig;->O8()Lcom/intsig/camscanner/doodle/DoodleConfig;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->oO〇8O8oOo:Lcom/intsig/camscanner/doodle/DoodleConfig;

    .line 42
    .line 43
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->o808o8o08()V

    .line 44
    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method protected onPause()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onPause()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->o〇oO:[Lcom/intsig/view/color/ColorItemView;

    .line 5
    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleActivity;->o088O8800()V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleActivity;->oO〇8O8oOo:Lcom/intsig/camscanner/doodle/DoodleConfig;

    .line 13
    .line 14
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/DoodleConfig;->Oo08()V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected onStart()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/appcompat/app/AppCompatActivity;->onStart()V

    .line 2
    .line 3
    .line 4
    const-string v0, "CSSmudge"

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-static {v0, v1}, Lcom/intsig/utils/LogMessage;->Oo08(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic onToolbarTitleClick(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->Oo08(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇oo()I
    .locals 1

    .line 1
    sget v0, Lcom/intsig/camscanner/doodle/R$layout;->doodle_activity_doodle:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇0〇0()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇Oo〇O()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->O〇8O8〇008()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    const/4 v2, 0x1

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/doodle/DoodleActivity;->OOo00(Z)V

    .line 14
    .line 15
    .line 16
    return v2

    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getItemCount()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-lez v0, :cond_1

    .line 24
    .line 25
    invoke-static {}, Lcom/intsig/camscanner/doodle/Doodle;->Oo08()Lcom/intsig/camscanner/doodle/util/IDoodleProxy;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-interface {v0, p0}, Lcom/intsig/camscanner/doodle/util/IDoodleProxy;->〇080(Landroid/app/Activity;)V

    .line 30
    .line 31
    .line 32
    return v2

    .line 33
    :cond_1
    return v1
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

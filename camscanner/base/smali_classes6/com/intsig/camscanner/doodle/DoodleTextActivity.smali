.class public Lcom/intsig/camscanner/doodle/DoodleTextActivity;
.super Lcom/intsig/camscanner/doodle/BaseDoodleActivity;
.source "DoodleTextActivity.java"


# instance fields
.field private Oo80:Lcom/intsig/view/ScrollColorPickerView$OnColorSelectedListener;

.field private O〇o88o08〇:Landroid/text/TextWatcher;

.field private o8o:Landroid/view/ViewGroup;

.field private oo8ooo8O:Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;

.field private o〇oO:Landroid/widget/EditText;

.field private 〇00O0:Lcom/intsig/camscanner/doodle/widget/core/IDoodleItemListener;

.field private 〇08〇o0O:Lcom/intsig/view/ScrollColorPickerView;

.field private 〇〇o〇:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/camscanner/doodle/DoodleTextActivity$3;

    .line 5
    .line 6
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/doodle/DoodleTextActivity$3;-><init>(Lcom/intsig/camscanner/doodle/DoodleTextActivity;)V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->Oo80:Lcom/intsig/view/ScrollColorPickerView$OnColorSelectedListener;

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/doodle/DoodleTextActivity$4;

    .line 12
    .line 13
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/doodle/DoodleTextActivity$4;-><init>(Lcom/intsig/camscanner/doodle/DoodleTextActivity;)V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->O〇o88o08〇:Landroid/text/TextWatcher;

    .line 17
    .line 18
    new-instance v0, L〇O8〇OO〇/OO0o〇〇;

    .line 19
    .line 20
    invoke-direct {v0, p0}, L〇O8〇OO〇/OO0o〇〇;-><init>(Lcom/intsig/camscanner/doodle/DoodleTextActivity;)V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->〇00O0:Lcom/intsig/camscanner/doodle/widget/core/IDoodleItemListener;

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic O00OoO〇(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->o〇OoO0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O80OO(Lcom/intsig/camscanner/doodle/widget/DoodleText;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->o〇oO:Landroid/widget/EditText;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->o〇oO:Landroid/widget/EditText;

    .line 8
    .line 9
    invoke-static {v0}, Lcom/intsig/utils/KeyboardUtils;->〇8o8o〇(Landroid/view/View;)V

    .line 10
    .line 11
    .line 12
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->〇0o0oO〇〇0(Lcom/intsig/camscanner/doodle/widget/DoodleText;)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->o〇oO:Landroid/widget/EditText;

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->O〇o88o08〇:Landroid/text/TextWatcher;

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->o〇oO:Landroid/widget/EditText;

    .line 23
    .line 24
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleText;->OOO()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->o〇oO:Landroid/widget/EditText;

    .line 32
    .line 33
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getColor()Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    invoke-interface {p1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;->〇o00〇〇Oo()I

    .line 38
    .line 39
    .line 40
    move-result p1

    .line 41
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 42
    .line 43
    .line 44
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->o〇oO:Landroid/widget/EditText;

    .line 45
    .line 46
    invoke-virtual {p1}, Landroid/widget/TextView;->length()I

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setSelection(I)V

    .line 51
    .line 52
    .line 53
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->o〇oO:Landroid/widget/EditText;

    .line 54
    .line 55
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->O〇o88o08〇:Landroid/text/TextWatcher;

    .line 56
    .line 57
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 58
    .line 59
    .line 60
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private O88(FF)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->〇08〇o0O:Lcom/intsig/view/ScrollColorPickerView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/view/ScrollColorPickerView;->getSelectedColor()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    new-instance v7, Lcom/intsig/camscanner/doodle/widget/DoodleText;

    .line 8
    .line 9
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 10
    .line 11
    const-string v3, ""

    .line 12
    .line 13
    new-instance v4, Lcom/intsig/camscanner/doodle/widget/DoodleColor;

    .line 14
    .line 15
    invoke-direct {v4, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleColor;-><init>(I)V

    .line 16
    .line 17
    .line 18
    move-object v1, v7

    .line 19
    move v5, p1

    .line 20
    move v6, p2

    .line 21
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/doodle/widget/DoodleText;-><init>(Lcom/intsig/camscanner/doodle/widget/core/IDoodle;Ljava/lang/String;Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;FF)V

    .line 22
    .line 23
    .line 24
    const/4 p1, 0x1

    .line 25
    invoke-virtual {v7, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleText;->OO8oO0o〇(Z)V

    .line 26
    .line 27
    .line 28
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 29
    .line 30
    invoke-virtual {p1, v7}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇O00(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;)V

    .line 31
    .line 32
    .line 33
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->oo8ooo8O:Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;

    .line 34
    .line 35
    invoke-virtual {p1, v7}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇〇8O0〇8(Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;)V

    .line 36
    .line 37
    .line 38
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->〇00O0:Lcom/intsig/camscanner/doodle/widget/core/IDoodleItemListener;

    .line 39
    .line 40
    invoke-virtual {v7, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇0〇O0088o(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItemListener;)V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method static bridge synthetic O880O〇(Lcom/intsig/camscanner/doodle/DoodleTextActivity;)Lcom/intsig/view/ScrollColorPickerView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->〇08〇o0O:Lcom/intsig/view/ScrollColorPickerView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O8O(Lcom/intsig/camscanner/doodle/DoodleTextActivity;)Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->oo8ooo8O:Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O8〇8〇O80()V
    .locals 3

    .line 1
    sget v0, Lcom/intsig/camscanner/doodle/R$string;->cs_522_add_text:I

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Landroid/app/Activity;->setTitle(I)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->O8o08O8O:Landroidx/appcompat/widget/Toolbar;

    .line 7
    .line 8
    const/high16 v1, 0x41200000    # 10.0f

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Landroid/view/View;->setElevation(F)V

    .line 11
    .line 12
    .line 13
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    sget v1, Lcom/intsig/camscanner/doodle/R$layout;->doodle_action_layout_save:I

    .line 18
    .line 19
    const/4 v2, 0x0

    .line 20
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    sget v1, Lcom/intsig/camscanner/doodle/R$id;->action_save:I

    .line 25
    .line 26
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    new-instance v2, L〇O8〇OO〇/〇〇808〇;

    .line 31
    .line 32
    invoke-direct {v2, p0}, L〇O8〇OO〇/〇〇808〇;-><init>(Lcom/intsig/camscanner/doodle/DoodleTextActivity;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p0, v0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->setToolbarWrapMenu(Landroid/view/View;)V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private OO0o()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->o8o:Landroid/view/ViewGroup;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    int-to-float v0, v0

    .line 8
    const/high16 v1, 0x40000000    # 2.0f

    .line 9
    .line 10
    div-float/2addr v0, v1

    .line 11
    iget-object v2, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->o8o:Landroid/view/ViewGroup;

    .line 12
    .line 13
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    int-to-float v2, v2

    .line 18
    div-float/2addr v2, v1

    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 20
    .line 21
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->oO(F)F

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 26
    .line 27
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇8(F)F

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->O88(FF)V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic OO〇〇o0oO(Lcom/intsig/camscanner/doodle/DoodleTextActivity;)Landroid/widget/EditText;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->o〇oO:Landroid/widget/EditText;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OoO〇OOo8o(Lcom/intsig/camscanner/doodle/DoodleTextActivity;Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->〇oOO80o(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic OooO〇(Lcom/intsig/camscanner/doodle/DoodleTextActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->OO0o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic O〇〇o8O(I)V
    .locals 1

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    if-eq p1, v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x7

    .line 6
    if-ne p1, v0, :cond_1

    .line 7
    .line 8
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->oo8ooo8O:Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;

    .line 9
    .line 10
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->Oooo8o0〇()Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    check-cast p1, Lcom/intsig/camscanner/doodle/widget/DoodleText;

    .line 15
    .line 16
    if-eqz p1, :cond_1

    .line 17
    .line 18
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->〇0o0oO〇〇0(Lcom/intsig/camscanner/doodle/widget/DoodleText;)V

    .line 19
    .line 20
    .line 21
    :cond_1
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic o0O0O〇〇〇0(Lcom/intsig/camscanner/doodle/DoodleTextActivity;Lcom/intsig/camscanner/doodle/widget/DoodleText;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->O80OO(Lcom/intsig/camscanner/doodle/widget/DoodleText;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private o0OO()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getItemCount()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-lez v0, :cond_0

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllItem()Ljava/util/List;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    add-int/lit8 v2, v0, -0x1

    .line 16
    .line 17
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    check-cast v1, Lcom/intsig/camscanner/doodle/widget/DoodleText;

    .line 22
    .line 23
    invoke-virtual {v1}, Lcom/intsig/camscanner/doodle/widget/DoodleText;->OOO()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    if-eqz v1, :cond_0

    .line 32
    .line 33
    add-int/lit8 v0, v0, -0x1

    .line 34
    .line 35
    :cond_0
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    .line 36
    .line 37
    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 38
    .line 39
    .line 40
    const-string v2, "num"

    .line 41
    .line 42
    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 43
    .line 44
    .line 45
    const-string v0, "CSInsertTextbox"

    .line 46
    .line 47
    const-string v2, "save_textbox"

    .line 48
    .line 49
    invoke-static {v0, v2, v1}, Lcom/intsig/utils/LogMessage;->O8(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    .line 51
    .line 52
    goto :goto_0

    .line 53
    :catch_0
    move-exception v0

    .line 54
    const-string v1, "DoodleTextActivity"

    .line 55
    .line 56
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 57
    .line 58
    .line 59
    :goto_0
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private oOO8oo0(Lcom/intsig/camscanner/doodle/widget/DoodleText;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->〇〇o〇:Landroid/widget/TextView;

    .line 2
    .line 3
    const/16 v1, 0x8

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->〇08〇o0O:Lcom/intsig/view/ScrollColorPickerView;

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->〇08〇o0O:Lcom/intsig/view/ScrollColorPickerView;

    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getColor()Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    invoke-interface {p1}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleColor;->〇o00〇〇Oo()I

    .line 21
    .line 22
    .line 23
    move-result p1

    .line 24
    invoke-virtual {v0, p1}, Lcom/intsig/view/ScrollColorPickerView;->setSelectColor(I)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic o〇08oO80o(Lcom/intsig/camscanner/doodle/DoodleTextActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->O00OoO〇(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private o〇OoO0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-static {p0}, Lcom/intsig/utils/KeyboardUtils;->o〇0(Landroid/app/Activity;)V

    .line 7
    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->o0OO()V

    .line 10
    .line 11
    .line 12
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->〇0o88Oo〇()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-nez v0, :cond_1

    .line 17
    .line 18
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 19
    .line 20
    .line 21
    return-void

    .line 22
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 23
    .line 24
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->O0O:Ljava/lang/String;

    .line 25
    .line 26
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇8(Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic o〇o08〇(Lcom/intsig/camscanner/doodle/DoodleTextActivity;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->O〇〇o8O(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇0o0oO〇〇0(Lcom/intsig/camscanner/doodle/widget/DoodleText;)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllScale()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getScale()F

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleSelectableItemBase;->〇00〇8()Landroid/graphics/Rect;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->getLocation()Landroid/graphics/PointF;

    .line 16
    .line 17
    .line 18
    move-result-object v3

    .line 19
    iget v4, v3, Landroid/graphics/PointF;->x:F

    .line 20
    .line 21
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->O8()F

    .line 22
    .line 23
    .line 24
    move-result v5

    .line 25
    iget v6, v3, Landroid/graphics/PointF;->x:F

    .line 26
    .line 27
    sub-float/2addr v5, v6

    .line 28
    const/high16 v6, 0x3f800000    # 1.0f

    .line 29
    .line 30
    sub-float v6, v1, v6

    .line 31
    .line 32
    mul-float v5, v5, v6

    .line 33
    .line 34
    sub-float/2addr v4, v5

    .line 35
    iget v5, v3, Landroid/graphics/PointF;->y:F

    .line 36
    .line 37
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->Oo08()F

    .line 38
    .line 39
    .line 40
    move-result v7

    .line 41
    iget v3, v3, Landroid/graphics/PointF;->y:F

    .line 42
    .line 43
    sub-float/2addr v7, v3

    .line 44
    mul-float v7, v7, v6

    .line 45
    .line 46
    sub-float/2addr v5, v7

    .line 47
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->o〇oO:Landroid/widget/EditText;

    .line 48
    .line 49
    iget-object v6, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 50
    .line 51
    invoke-virtual {v6, v4}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->〇〇〇0〇〇0(F)F

    .line 52
    .line 53
    .line 54
    move-result v4

    .line 55
    invoke-virtual {v3, v4}, Landroid/view/View;->setX(F)V

    .line 56
    .line 57
    .line 58
    iget-object v3, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->o〇oO:Landroid/widget/EditText;

    .line 59
    .line 60
    iget-object v4, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 61
    .line 62
    invoke-virtual {v4, v5}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o〇0OOo〇0(F)F

    .line 63
    .line 64
    .line 65
    move-result v4

    .line 66
    invoke-virtual {v3, v4}, Landroid/view/View;->setY(F)V

    .line 67
    .line 68
    .line 69
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇oo〇()F

    .line 70
    .line 71
    .line 72
    move-result v3

    .line 73
    mul-float v3, v3, v1

    .line 74
    .line 75
    mul-float v3, v3, v0

    .line 76
    .line 77
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->o〇oO:Landroid/widget/EditText;

    .line 78
    .line 79
    invoke-virtual {v1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    .line 80
    .line 81
    .line 82
    move-result-object v1

    .line 83
    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 84
    .line 85
    .line 86
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->o〇oO:Landroid/widget/EditText;

    .line 87
    .line 88
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 89
    .line 90
    .line 91
    move-result-object v1

    .line 92
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    .line 93
    .line 94
    .line 95
    move-result v3

    .line 96
    int-to-float v3, v3

    .line 97
    mul-float v3, v3, v0

    .line 98
    .line 99
    float-to-double v3, v3

    .line 100
    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    .line 101
    .line 102
    .line 103
    move-result-wide v3

    .line 104
    double-to-int v3, v3

    .line 105
    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 106
    .line 107
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    .line 108
    .line 109
    .line 110
    move-result v2

    .line 111
    int-to-float v2, v2

    .line 112
    mul-float v2, v2, v0

    .line 113
    .line 114
    float-to-double v2, v2

    .line 115
    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    .line 116
    .line 117
    .line 118
    move-result-wide v2

    .line 119
    double-to-int v0, v2

    .line 120
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 121
    .line 122
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->o〇oO:Landroid/widget/EditText;

    .line 123
    .line 124
    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 125
    .line 126
    .line 127
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->o〇oO:Landroid/widget/EditText;

    .line 128
    .line 129
    invoke-virtual {p1}, Lcom/intsig/camscanner/doodle/widget/DoodleItemBase;->〇〇808〇()F

    .line 130
    .line 131
    .line 132
    move-result p1

    .line 133
    invoke-virtual {v0, p1}, Landroid/view/View;->setRotation(F)V

    .line 134
    .line 135
    .line 136
    return-void
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private 〇0o88Oo〇()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getItemCount()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-gtz v0, :cond_1

    .line 12
    .line 13
    return v1

    .line 14
    :cond_1
    const/4 v2, 0x1

    .line 15
    if-ne v0, v2, :cond_2

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->getAllItem()Ljava/util/List;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    check-cast v0, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;

    .line 28
    .line 29
    instance-of v1, v0, Lcom/intsig/camscanner/doodle/widget/DoodleText;

    .line 30
    .line 31
    if-eqz v1, :cond_2

    .line 32
    .line 33
    check-cast v0, Lcom/intsig/camscanner/doodle/widget/DoodleText;

    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/camscanner/doodle/widget/DoodleText;->OOO()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    xor-int/2addr v0, v2

    .line 44
    return v0

    .line 45
    :cond_2
    return v2
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic 〇OoO0o0(Lcom/intsig/camscanner/doodle/DoodleTextActivity;Lcom/intsig/camscanner/doodle/widget/DoodleText;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->oOO8oo0(Lcom/intsig/camscanner/doodle/widget/DoodleText;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇oOO80o(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->〇00O0:Lcom/intsig/camscanner/doodle/widget/core/IDoodleItemListener;

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;->oO80(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItemListener;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 7
    .line 8
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->o0ooO(Lcom/intsig/camscanner/doodle/widget/core/IDoodleItem;)V

    .line 9
    .line 10
    .line 11
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->oo8ooo8O:Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;

    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;->〇〇8O0〇8(Lcom/intsig/camscanner/doodle/widget/core/IDoodleSelectableItem;)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇ooO8Ooo〇(Lcom/intsig/camscanner/doodle/DoodleTextActivity;FF)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->O88(FF)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic 〇ooO〇000(Lcom/intsig/camscanner/doodle/DoodleTextActivity;)Landroid/widget/TextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->〇〇o〇:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public OO8〇(Lcom/intsig/camscanner/doodle/widget/core/IDoodle;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/doodle/widget/DoodlePen;->TEXT:Lcom/intsig/camscanner/doodle/widget/DoodlePen;

    .line 2
    .line 3
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/doodle/widget/core/IDoodle;->setPen(Lcom/intsig/camscanner/doodle/widget/core/IDoodlePen;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected O〇〇O80o8(Landroid/graphics/Bitmap;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->O〇〇O80o8(Landroid/graphics/Bitmap;)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->setEditMode(Z)V

    .line 8
    .line 9
    .line 10
    new-instance p1, Lcom/intsig/camscanner/doodle/DoodleTextActivity$1;

    .line 11
    .line 12
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/doodle/DoodleTextActivity$1;-><init>(Lcom/intsig/camscanner/doodle/DoodleTextActivity;)V

    .line 13
    .line 14
    .line 15
    new-instance v0, Lcom/intsig/camscanner/doodle/DoodleTextActivity$2;

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 18
    .line 19
    invoke-direct {v0, p0, v1, p1}, Lcom/intsig/camscanner/doodle/DoodleTextActivity$2;-><init>(Lcom/intsig/camscanner/doodle/DoodleTextActivity;Lcom/intsig/camscanner/doodle/widget/DoodleView;Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener$ISelectionListener;)V

    .line 20
    .line 21
    .line 22
    iput-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->oo8ooo8O:Lcom/intsig/camscanner/doodle/widget/DoodleOnTouchGestureListener;

    .line 23
    .line 24
    new-instance p1, Lcom/intsig/camscanner/doodle/widget/DoodleTouchDetector;

    .line 25
    .line 26
    invoke-direct {p1, p0, v0}, Lcom/intsig/camscanner/doodle/widget/DoodleTouchDetector;-><init>(Landroid/content/Context;Lcom/intsig/camscanner/doodle/widget/TouchGestureDetector$IOnTouchGestureListener;)V

    .line 27
    .line 28
    .line 29
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 30
    .line 31
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/doodle/widget/DoodleView;->setDefaultTouchDetector(Lcom/intsig/camscanner/doodle/widget/core/IDoodleTouchDetector;)V

    .line 32
    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->o8o:Landroid/view/ViewGroup;

    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->ooo0〇〇O:Lcom/intsig/camscanner/doodle/widget/DoodleView;

    .line 37
    .line 38
    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    .line 39
    .line 40
    const/4 v2, -0x1

    .line 41
    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 42
    .line 43
    .line 44
    const/4 v2, 0x0

    .line 45
    invoke-virtual {p1, v0, v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 46
    .line 47
    .line 48
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->o〇oO:Landroid/widget/EditText;

    .line 49
    .line 50
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->O〇o88o08〇:Landroid/text/TextWatcher;

    .line 51
    .line 52
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 53
    .line 54
    .line 55
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->〇08〇o0O:Lcom/intsig/view/ScrollColorPickerView;

    .line 56
    .line 57
    iget-object v0, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->Oo80:Lcom/intsig/view/ScrollColorPickerView$OnColorSelectedListener;

    .line 58
    .line 59
    invoke-virtual {p1, v0}, Lcom/intsig/view/ScrollColorPickerView;->setOnColorSelectedListener(Lcom/intsig/view/ScrollColorPickerView$OnColorSelectedListener;)V

    .line 60
    .line 61
    .line 62
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->o〇oO:Landroid/widget/EditText;

    .line 63
    .line 64
    invoke-static {p0, p1}, Lcom/intsig/camscanner/doodle/util/KeyboardFitHelper;->〇o00〇〇Oo(Landroidx/fragment/app/FragmentActivity;Landroid/view/View;)V

    .line 65
    .line 66
    .line 67
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->o〇oO:Landroid/widget/EditText;

    .line 68
    .line 69
    new-instance v0, L〇O8〇OO〇/Oooo8o0〇;

    .line 70
    .line 71
    invoke-direct {v0, p0}, L〇O8〇OO〇/Oooo8o0〇;-><init>(Lcom/intsig/camscanner/doodle/DoodleTextActivity;)V

    .line 72
    .line 73
    .line 74
    const-wide/16 v1, 0x96

    .line 75
    .line 76
    invoke-virtual {p1, v0, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 77
    .line 78
    .line 79
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public bridge synthetic dealClickAction(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->〇080(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->O0O:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    if-nez p1, :cond_0

    .line 8
    .line 9
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->O8〇8〇O80()V

    .line 14
    .line 15
    .line 16
    sget p1, Lcom/intsig/camscanner/doodle/R$id;->l_doodle:I

    .line 17
    .line 18
    invoke-virtual {p0, p1}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    check-cast p1, Landroid/view/ViewGroup;

    .line 23
    .line 24
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->o8o:Landroid/view/ViewGroup;

    .line 25
    .line 26
    sget p1, Lcom/intsig/camscanner/doodle/R$id;->et_text:I

    .line 27
    .line 28
    invoke-virtual {p0, p1}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    check-cast p1, Landroid/widget/EditText;

    .line 33
    .line 34
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->o〇oO:Landroid/widget/EditText;

    .line 35
    .line 36
    sget p1, Lcom/intsig/camscanner/doodle/R$id;->tv_prompt:I

    .line 37
    .line 38
    invoke-virtual {p0, p1}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    check-cast p1, Landroid/widget/TextView;

    .line 43
    .line 44
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->〇〇o〇:Landroid/widget/TextView;

    .line 45
    .line 46
    sget p1, Lcom/intsig/camscanner/doodle/R$id;->color_picker:I

    .line 47
    .line 48
    invoke-virtual {p0, p1}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    check-cast p1, Lcom/intsig/view/ScrollColorPickerView;

    .line 53
    .line 54
    iput-object p1, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->〇08〇o0O:Lcom/intsig/view/ScrollColorPickerView;

    .line 55
    .line 56
    invoke-virtual {p0}, Lcom/intsig/camscanner/doodle/BaseDoodleActivity;->o808o8o08()V

    .line 57
    .line 58
    .line 59
    invoke-static {}, Lcom/intsig/base/ToolbarThemeGet;->Oo08()Z

    .line 60
    .line 61
    .line 62
    move-result p1

    .line 63
    if-nez p1, :cond_1

    .line 64
    .line 65
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->〇〇o〇:Landroid/widget/TextView;

    .line 66
    .line 67
    const/4 v0, -0x1

    .line 68
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 69
    .line 70
    .line 71
    iget-object p1, p0, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->〇08〇o0O:Lcom/intsig/view/ScrollColorPickerView;

    .line 72
    .line 73
    const v0, -0xe1d5c9

    .line 74
    .line 75
    .line 76
    invoke-virtual {p1, v0}, Lcom/intsig/view/ScrollColorPickerView;->setSelectedBg(I)V

    .line 77
    .line 78
    .line 79
    :cond_1
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method protected onStart()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/appcompat/app/AppCompatActivity;->onStart()V

    .line 2
    .line 3
    .line 4
    const-string v0, "CSInsertTextbox"

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    invoke-static {v0, v1}, Lcom/intsig/utils/LogMessage;->Oo08(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic onToolbarTitleClick(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->Oo08(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇O8OO()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇oo()I
    .locals 1

    .line 1
    sget v0, Lcom/intsig/camscanner/doodle/R$layout;->doodle_activity_text:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇Oo〇O()Z
    .locals 1

    .line 1
    invoke-static {p0}, Lcom/intsig/utils/KeyboardUtils;->o〇0(Landroid/app/Activity;)V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/doodle/DoodleTextActivity;->〇0o88Oo〇()Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    invoke-static {}, Lcom/intsig/camscanner/doodle/Doodle;->Oo08()Lcom/intsig/camscanner/doodle/util/IDoodleProxy;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-interface {v0, p0}, Lcom/intsig/camscanner/doodle/util/IDoodleProxy;->〇080(Landroid/app/Activity;)V

    .line 15
    .line 16
    .line 17
    const/4 v0, 0x1

    .line 18
    return v0

    .line 19
    :cond_0
    const/4 v0, 0x0

    .line 20
    return v0
    .line 21
.end method

.class public Lcom/intsig/camscanner/question/QuestionDialog;
.super Lcom/intsig/app/BaseDialog;
.source "QuestionDialog.java"

# interfaces
.implements Lcom/intsig/camscanner/question/ILogAgentData;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/question/QuestionDialog$BaseQuestionNavigation;,
        Lcom/intsig/camscanner/question/QuestionDialog$CommitQuestionNavigation;,
        Lcom/intsig/camscanner/question/QuestionDialog$NegativeQuestionNavigation;,
        Lcom/intsig/camscanner/question/QuestionDialog$QuestionDialogHome;
    }
.end annotation


# static fields
.field private static final o〇00O:Ljava/lang/String; = "QuestionDialog"


# instance fields
.field private OO:Ljava/lang/String;

.field private o0:Landroid/widget/FrameLayout;

.field private 〇08O〇00〇o:Lcom/intsig/camscanner/question/mode/QuestionHomeMode;

.field private 〇OOo8〇0:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    const v0, 0x7f140004

    .line 2
    .line 3
    .line 4
    invoke-direct {p0, p1, v0}, Lcom/intsig/app/BaseDialog;-><init>(Landroid/content/Context;I)V

    .line 5
    .line 6
    .line 7
    const-string p1, "CSSatisfactionRatingPop"

    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/question/QuestionDialog;->〇OOo8〇0:Ljava/lang/String;

    .line 10
    .line 11
    sget-object p1, Lcom/intsig/comm/tracker/TrackPara$Scan;->〇080:Ljava/lang/String;

    .line 12
    .line 13
    iput-object p1, p0, Lcom/intsig/camscanner/question/QuestionDialog;->OO:Ljava/lang/String;

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private oO80(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 2

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    new-instance p1, Lorg/json/JSONObject;

    .line 4
    .line 5
    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    .line 6
    .line 7
    .line 8
    :cond_0
    :try_start_0
    const-string v0, "from"

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/question/QuestionDialog;->OO:Ljava/lang/String;

    .line 11
    .line 12
    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 13
    .line 14
    .line 15
    goto :goto_0

    .line 16
    :catch_0
    move-exception v0

    .line 17
    sget-object v1, Lcom/intsig/camscanner/question/QuestionDialog;->o〇00O:Ljava/lang/String;

    .line 18
    .line 19
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 20
    .line 21
    .line 22
    :goto_0
    return-object p1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private 〇O8o08O()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/QuestionDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/question/mode/QuestionHomeMode;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/camscanner/question/QuestionDialog;->o〇00O:Ljava/lang/String;

    .line 6
    .line 7
    const-string v1, "questionMenuMode == null)"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    :try_start_0
    invoke-virtual {p0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 13
    .line 14
    .line 15
    goto :goto_0

    .line 16
    :catch_0
    move-exception v0

    .line 17
    sget-object v1, Lcom/intsig/camscanner/question/QuestionDialog;->o〇00O:Ljava/lang/String;

    .line 18
    .line 19
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 20
    .line 21
    .line 22
    :goto_0
    return-void

    .line 23
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/question/QuestionDialog;->〇OOo8〇0:Ljava/lang/String;

    .line 24
    .line 25
    const/4 v1, 0x0

    .line 26
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/question/QuestionDialog;->oO80(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇O00(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 31
    .line 32
    .line 33
    new-instance v0, Lcom/intsig/camscanner/question/QuestionDialog$QuestionDialogHome;

    .line 34
    .line 35
    iget-object v1, p0, Lcom/intsig/camscanner/question/QuestionDialog;->o0:Landroid/widget/FrameLayout;

    .line 36
    .line 37
    iget-object v2, p0, Lcom/intsig/camscanner/question/QuestionDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/question/mode/QuestionHomeMode;

    .line 38
    .line 39
    invoke-direct {v0, p0, v1, v2, p0}, Lcom/intsig/camscanner/question/QuestionDialog$QuestionDialogHome;-><init>(Landroid/app/Dialog;Landroid/view/ViewGroup;Lcom/intsig/camscanner/question/mode/QuestionHomeMode;Lcom/intsig/camscanner/question/ILogAgentData;)V

    .line 40
    .line 41
    .line 42
    invoke-virtual {v0}, Lcom/intsig/camscanner/question/QuestionDialog$QuestionDialogHome;->〇o〇()V

    .line 43
    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic 〇〇888()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/question/QuestionDialog;->o〇00O:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public O8()I
    .locals 1

    .line 1
    const/16 v0, 0x50

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method OO0o〇〇〇〇0(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/question/QuestionDialog;->〇OOo8〇0:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public Oo08()Landroid/view/View;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/QuestionDialog;->o0:Landroid/widget/FrameLayout;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const v1, 0x7f0d0671

    .line 14
    .line 15
    .line 16
    const/4 v2, 0x0

    .line 17
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, Landroid/widget/FrameLayout;

    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/question/QuestionDialog;->o0:Landroid/widget/FrameLayout;

    .line 24
    .line 25
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/question/QuestionDialog;->o0:Landroid/widget/FrameLayout;

    .line 26
    .line 27
    return-object v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p2    # Landroid/view/KeyEvent;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    const/4 v0, 0x4

    .line 2
    if-ne p1, v0, :cond_0

    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/question/QuestionDialog;->oO80(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const-string v1, "cancel"

    .line 10
    .line 11
    invoke-virtual {p0, v1, v0}, Lcom/intsig/camscanner/question/QuestionDialog;->〇080(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 12
    .line 13
    .line 14
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Dialog;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    return p1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public show()V
    .locals 0

    .line 1
    invoke-super {p0}, Landroid/app/Dialog;->show()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/question/QuestionDialog;->〇O8o08O()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇080(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/QuestionDialog;->〇OOo8〇0:Ljava/lang/String;

    .line 2
    .line 3
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/question/QuestionDialog;->oO80(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    .line 4
    .line 5
    .line 6
    move-result-object p2

    .line 7
    invoke-static {v0, p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method 〇80〇808〇O(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/question/QuestionDialog;->OO:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method 〇8o8o〇(Lcom/intsig/camscanner/question/mode/QuestionHomeMode;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/question/QuestionDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/question/mode/QuestionHomeMode;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇o00〇〇Oo()I
    .locals 1

    .line 1
    const/4 v0, -0x2

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇()I
    .locals 1

    .line 1
    const/4 v0, -0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

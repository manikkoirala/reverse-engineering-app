.class public final Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;
.super Ljava/lang/Object;
.source "NpsCommonMultiChoiseViewManager.kt"

# interfaces
.implements Lcom/intsig/camscanner/question/nps/NPSDialogActivity$INpsDialogViewManager;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇8〇oO〇〇8o:Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Landroid/view/View;

.field private final OO:Lcom/intsig/camscanner/question/nps/NPSDialogActivity$Attitude;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO〇00〇8oO:Landroid/widget/Toast;

.field private final o0:Lcom/intsig/camscanner/question/mode/NpsMultiChoiceQuestion;

.field private final o8〇OO0〇0o:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oOo0:Landroid/view/View$OnClickListener;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOo〇8o008:Landroid/view/View;

.field private o〇00O:Landroid/view/View;

.field private final 〇080OO8〇0:Lcom/intsig/utils/ClickLimit;

.field private final 〇08O〇00〇o:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇0O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Lcom/intsig/camscanner/question/nps/NPSDialogActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/intsig/camscanner/question/mode/NpsMultiChoiceQuestion;Lcom/intsig/camscanner/question/nps/NPSDialogActivity;Lcom/intsig/camscanner/question/nps/NPSDialogActivity$Attitude;Ljava/lang/String;)V
    .locals 1
    .param p3    # Lcom/intsig/camscanner/question/nps/NPSDialogActivity$Attitude;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "attitude"

    .line 2
    .line 3
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "mFromTrack"

    .line 7
    .line 8
    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o0:Lcom/intsig/camscanner/question/mode/NpsMultiChoiceQuestion;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇OOo8〇0:Lcom/intsig/camscanner/question/nps/NPSDialogActivity;

    .line 17
    .line 18
    iput-object p3, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->OO:Lcom/intsig/camscanner/question/nps/NPSDialogActivity$Attitude;

    .line 19
    .line 20
    iput-object p4, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇08O〇00〇o:Ljava/lang/String;

    .line 21
    .line 22
    invoke-static {}, Lcom/intsig/utils/ClickLimit;->〇o〇()Lcom/intsig/utils/ClickLimit;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    iput-object p1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇080OO8〇0:Lcom/intsig/utils/ClickLimit;

    .line 27
    .line 28
    const-string p1, "other"

    .line 29
    .line 30
    iput-object p1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇0O:Ljava/lang/String;

    .line 31
    .line 32
    new-instance p1, LO088O/〇O8o08O;

    .line 33
    .line 34
    invoke-direct {p1, p0}, LO088O/〇O8o08O;-><init>(Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;)V

    .line 35
    .line 36
    .line 37
    iput-object p1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->oOo0:Landroid/view/View$OnClickListener;

    .line 38
    .line 39
    new-instance p1, Ljava/util/HashSet;

    .line 40
    .line 41
    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    .line 42
    .line 43
    .line 44
    iput-object p1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o8〇OO0〇0o:Ljava/util/HashSet;

    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final O8()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇OOo8〇0:Lcom/intsig/camscanner/question/nps/NPSDialogActivity;

    .line 2
    .line 3
    const/16 v1, 0x40

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 6
    .line 7
    .line 8
    move-result v4

    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o〇00O:Landroid/view/View;

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    const v2, 0x7f0a04a8

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    move-object v6, v0

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    move-object v6, v1

    .line 24
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o〇00O:Landroid/view/View;

    .line 25
    .line 26
    if-eqz v0, :cond_1

    .line 27
    .line 28
    const v2, 0x7f0a05fc

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    move-object v5, v0

    .line 36
    goto :goto_1

    .line 37
    :cond_1
    move-object v5, v1

    .line 38
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o〇00O:Landroid/view/View;

    .line 39
    .line 40
    if-eqz v0, :cond_2

    .line 41
    .line 42
    const v2, 0x7f0a0ada

    .line 43
    .line 44
    .line 45
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    check-cast v0, Lcom/intsig/camscanner/view/KeyboardLayout;

    .line 50
    .line 51
    goto :goto_2

    .line 52
    :cond_2
    move-object v0, v1

    .line 53
    :goto_2
    iget-object v2, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o〇00O:Landroid/view/View;

    .line 54
    .line 55
    if-eqz v2, :cond_3

    .line 56
    .line 57
    const v1, 0x7f0a109d

    .line 58
    .line 59
    .line 60
    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    check-cast v1, Lcom/intsig/camscanner/view/MaxHeightLimitScrollView;

    .line 65
    .line 66
    :cond_3
    move-object v7, v1

    .line 67
    if-eqz v0, :cond_4

    .line 68
    .line 69
    new-instance v1, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager$adjustOptionMaxHeight$1;

    .line 70
    .line 71
    move-object v2, v1

    .line 72
    move-object v3, p0

    .line 73
    invoke-direct/range {v2 .. v7}, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager$adjustOptionMaxHeight$1;-><init>(Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;ILandroid/view/View;Landroid/view/View;Lcom/intsig/camscanner/view/MaxHeightLimitScrollView;)V

    .line 74
    .line 75
    .line 76
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/KeyboardLayout;->setStateCallback(Lcom/intsig/camscanner/view/KeyboardLayout$StateCallback;)V

    .line 77
    .line 78
    .line 79
    :cond_4
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final OO0o〇〇〇〇0()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->OO:Lcom/intsig/camscanner/question/nps/NPSDialogActivity$Attitude;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/question/nps/NPSDialogActivity$Attitude;->SUPPORTER:Lcom/intsig/camscanner/question/nps/NPSDialogActivity$Attitude;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-ne v0, v1, :cond_0

    .line 7
    .line 8
    return v2

    .line 9
    :cond_0
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-nez v0, :cond_1

    .line 20
    .line 21
    return v2

    .line 22
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇OOo8〇0:Lcom/intsig/camscanner/question/nps/NPSDialogActivity;

    .line 23
    .line 24
    if-eqz v0, :cond_5

    .line 25
    .line 26
    iget-object v0, v0, Lcom/intsig/camscanner/question/nps/NPSDialogActivity;->OO:Lcom/intsig/camscanner/gift/GiftQueryManager;

    .line 27
    .line 28
    if-eqz v0, :cond_5

    .line 29
    .line 30
    invoke-virtual {v0}, Lcom/intsig/camscanner/gift/GiftQueryManager;->O8()Ljava/util/concurrent/ConcurrentHashMap;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    if-eqz v0, :cond_5

    .line 35
    .line 36
    const-string v1, "nps_plus_Q1_100M"

    .line 37
    .line 38
    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    check-cast v1, Ljava/lang/Integer;

    .line 43
    .line 44
    if-nez v1, :cond_2

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_2
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    .line 48
    .line 49
    .line 50
    move-result v1

    .line 51
    if-nez v1, :cond_4

    .line 52
    .line 53
    const-string v1, "nps_plus_Q2_100M"

    .line 54
    .line 55
    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    check-cast v0, Ljava/lang/Integer;

    .line 60
    .line 61
    if-nez v0, :cond_3

    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 65
    .line 66
    .line 67
    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    if-nez v0, :cond_4

    .line 69
    .line 70
    const/4 v2, 0x1

    .line 71
    :cond_4
    :goto_0
    return v2

    .line 72
    :catchall_0
    move-exception v0

    .line 73
    new-instance v1, Ljava/lang/StringBuilder;

    .line 74
    .line 75
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 76
    .line 77
    .line 78
    const-string v3, "isCurrentPageNeedAddGift, t="

    .line 79
    .line 80
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    const-string v1, "NpsCommonMultiChoiceViewManager"

    .line 91
    .line 92
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    .line 94
    .line 95
    :cond_5
    return v2
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final Oo08()V
    .locals 9

    .line 1
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->oOo〇8o008:Landroid/view/View;

    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    const v3, 0x7f0a197c

    .line 12
    .line 13
    .line 14
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    check-cast v1, Landroid/widget/EditText;

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    move-object v1, v2

    .line 22
    :goto_0
    const/4 v3, 0x0

    .line 23
    const/4 v4, 0x1

    .line 24
    if-eqz v1, :cond_7

    .line 25
    .line 26
    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    if-eqz v1, :cond_7

    .line 31
    .line 32
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    if-eqz v1, :cond_7

    .line 37
    .line 38
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    .line 39
    .line 40
    .line 41
    move-result v2

    .line 42
    sub-int/2addr v2, v4

    .line 43
    const/4 v5, 0x0

    .line 44
    const/4 v6, 0x0

    .line 45
    :goto_1
    if-gt v5, v2, :cond_6

    .line 46
    .line 47
    if-nez v6, :cond_1

    .line 48
    .line 49
    move v7, v5

    .line 50
    goto :goto_2

    .line 51
    :cond_1
    move v7, v2

    .line 52
    :goto_2
    invoke-interface {v1, v7}, Ljava/lang/CharSequence;->charAt(I)C

    .line 53
    .line 54
    .line 55
    move-result v7

    .line 56
    const/16 v8, 0x20

    .line 57
    .line 58
    invoke-static {v7, v8}, Lkotlin/jvm/internal/Intrinsics;->oO80(II)I

    .line 59
    .line 60
    .line 61
    move-result v7

    .line 62
    if-gtz v7, :cond_2

    .line 63
    .line 64
    const/4 v7, 0x1

    .line 65
    goto :goto_3

    .line 66
    :cond_2
    const/4 v7, 0x0

    .line 67
    :goto_3
    if-nez v6, :cond_4

    .line 68
    .line 69
    if-nez v7, :cond_3

    .line 70
    .line 71
    const/4 v6, 0x1

    .line 72
    goto :goto_1

    .line 73
    :cond_3
    add-int/lit8 v5, v5, 0x1

    .line 74
    .line 75
    goto :goto_1

    .line 76
    :cond_4
    if-nez v7, :cond_5

    .line 77
    .line 78
    goto :goto_4

    .line 79
    :cond_5
    add-int/lit8 v2, v2, -0x1

    .line 80
    .line 81
    goto :goto_1

    .line 82
    :cond_6
    :goto_4
    add-int/2addr v2, v4

    .line 83
    invoke-interface {v1, v5, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    .line 84
    .line 85
    .line 86
    move-result-object v1

    .line 87
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v2

    .line 91
    :cond_7
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 92
    .line 93
    .line 94
    move-result v1

    .line 95
    if-nez v1, :cond_8

    .line 96
    .line 97
    const-string v1, "comment"

    .line 98
    .line 99
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 100
    .line 101
    .line 102
    :cond_8
    new-instance v1, Ljava/lang/StringBuilder;

    .line 103
    .line 104
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 105
    .line 106
    .line 107
    iget-object v2, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o8〇OO0〇0o:Ljava/util/HashSet;

    .line 108
    .line 109
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    .line 110
    .line 111
    .line 112
    move-result-object v2

    .line 113
    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 114
    .line 115
    .line 116
    move-result v5

    .line 117
    if-eqz v5, :cond_b

    .line 118
    .line 119
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 120
    .line 121
    .line 122
    move-result-object v5

    .line 123
    check-cast v5, Ljava/lang/String;

    .line 124
    .line 125
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    .line 126
    .line 127
    .line 128
    move-result v6

    .line 129
    if-lez v6, :cond_9

    .line 130
    .line 131
    const/4 v6, 0x1

    .line 132
    goto :goto_6

    .line 133
    :cond_9
    const/4 v6, 0x0

    .line 134
    :goto_6
    if-eqz v6, :cond_a

    .line 135
    .line 136
    const-string v6, ","

    .line 137
    .line 138
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    .line 140
    .line 141
    :cond_a
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    .line 143
    .line 144
    goto :goto_5

    .line 145
    :cond_b
    const-string v2, "type"

    .line 146
    .line 147
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 148
    .line 149
    .line 150
    move-result-object v1

    .line 151
    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 152
    .line 153
    .line 154
    iget-object v1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇08O〇00〇o:Ljava/lang/String;

    .line 155
    .line 156
    invoke-static {v1}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 157
    .line 158
    .line 159
    move-result v1

    .line 160
    xor-int/2addr v1, v4

    .line 161
    if-eqz v1, :cond_c

    .line 162
    .line 163
    const-string v1, "from"

    .line 164
    .line 165
    iget-object v2, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇08O〇00〇o:Ljava/lang/String;

    .line 166
    .line 167
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 168
    .line 169
    .line 170
    :cond_c
    invoke-direct {p0}, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇〇888()Ljava/lang/String;

    .line 171
    .line 172
    .line 173
    move-result-object v1

    .line 174
    const-string v2, "click_score"

    .line 175
    .line 176
    invoke-static {v1, v2, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    .line 178
    .line 179
    goto :goto_7

    .line 180
    :catch_0
    move-exception v0

    .line 181
    const-string v1, "NpsCommonMultiChoiceViewManager"

    .line 182
    .line 183
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 184
    .line 185
    .line 186
    :goto_7
    return-void
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private final oO80()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇OOo8〇0:Lcom/intsig/camscanner/question/nps/NPSDialogActivity;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->oOo〇8o008:Landroid/view/View;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-eqz v1, :cond_0

    .line 7
    .line 8
    const v3, 0x7f0a197c

    .line 9
    .line 10
    .line 11
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    check-cast v1, Landroid/widget/EditText;

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    move-object v1, v2

    .line 19
    :goto_0
    invoke-static {v0, v1}, Lcom/intsig/utils/SoftKeyboardUtils;->〇o00〇〇Oo(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 20
    .line 21
    .line 22
    new-instance v0, Ljava/util/ArrayList;

    .line 23
    .line 24
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 25
    .line 26
    .line 27
    iget-object v1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o8〇OO0〇0o:Ljava/util/HashSet;

    .line 28
    .line 29
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 30
    .line 31
    .line 32
    iget-object v1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇0O:Ljava/lang/String;

    .line 33
    .line 34
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 35
    .line 36
    .line 37
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    .line 38
    .line 39
    .line 40
    move-result v1

    .line 41
    xor-int/lit8 v1, v1, 0x1

    .line 42
    .line 43
    if-eqz v1, :cond_1

    .line 44
    .line 45
    goto :goto_1

    .line 46
    :cond_1
    move-object v0, v2

    .line 47
    :goto_1
    if-eqz v0, :cond_2

    .line 48
    .line 49
    sget-object v1, Lkotlin/random/Random;->Default:Lkotlin/random/Random$Default;

    .line 50
    .line 51
    invoke-static {v0, v1}, Lkotlin/collections/CollectionsKt;->o0O0(Ljava/util/Collection;Lkotlin/random/Random;)Ljava/lang/Object;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    move-object v2, v0

    .line 56
    check-cast v2, Ljava/lang/String;

    .line 57
    .line 58
    :cond_2
    invoke-static {}, Lcom/intsig/camscanner/question/nps/NPSActionClient;->Oo08()Lcom/intsig/camscanner/question/nps/NPSActionClient;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/question/nps/NPSActionClient;->〇80〇808〇O(Ljava/lang/String;)Lcom/intsig/camscanner/question/mode/NpsMultiChoiceQuestion;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    if-eqz v0, :cond_4

    .line 67
    .line 68
    new-instance v1, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;

    .line 69
    .line 70
    iget-object v3, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇OOo8〇0:Lcom/intsig/camscanner/question/nps/NPSDialogActivity;

    .line 71
    .line 72
    iget-object v4, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->OO:Lcom/intsig/camscanner/question/nps/NPSDialogActivity$Attitude;

    .line 73
    .line 74
    if-nez v2, :cond_3

    .line 75
    .line 76
    const-string v5, ""

    .line 77
    .line 78
    goto :goto_2

    .line 79
    :cond_3
    move-object v5, v2

    .line 80
    :goto_2
    invoke-direct {v1, v0, v3, v4, v5}, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;-><init>(Lcom/intsig/camscanner/question/mode/NpsMultiChoiceQuestion;Lcom/intsig/camscanner/question/nps/NPSDialogActivity;Lcom/intsig/camscanner/question/nps/NPSDialogActivity$Attitude;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    goto :goto_3

    .line 84
    :cond_4
    new-instance v1, Lcom/intsig/camscanner/question/nps/NPSSatisfiedViewManager;

    .line 85
    .line 86
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇OOo8〇0:Lcom/intsig/camscanner/question/nps/NPSDialogActivity;

    .line 87
    .line 88
    iget-object v3, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->OO:Lcom/intsig/camscanner/question/nps/NPSDialogActivity$Attitude;

    .line 89
    .line 90
    invoke-direct {v1, v0, v3}, Lcom/intsig/camscanner/question/nps/NPSSatisfiedViewManager;-><init>(Lcom/intsig/camscanner/question/nps/NPSDialogActivity;Lcom/intsig/camscanner/question/nps/NPSDialogActivity$Attitude;)V

    .line 91
    .line 92
    .line 93
    :goto_3
    new-instance v0, Ljava/lang/StringBuilder;

    .line 94
    .line 95
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 96
    .line 97
    .line 98
    const-string v3, "goToEndPage, randomOpt="

    .line 99
    .line 100
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    .line 102
    .line 103
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    .line 105
    .line 106
    const-string v2, ", and nextPage="

    .line 107
    .line 108
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object v0

    .line 118
    const-string v2, "NpsCommonMultiChoiceViewManager"

    .line 119
    .line 120
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    .line 122
    .line 123
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇OOo8〇0:Lcom/intsig/camscanner/question/nps/NPSDialogActivity;

    .line 124
    .line 125
    if-eqz v0, :cond_5

    .line 126
    .line 127
    invoke-interface {v1}, Lcom/intsig/camscanner/question/nps/NPSDialogActivity$INpsDialogViewManager;->〇o00〇〇Oo()Landroid/view/View;

    .line 128
    .line 129
    .line 130
    move-result-object v1

    .line 131
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/question/nps/NPSDialogActivity;->addView(Landroid/view/View;)V

    .line 132
    .line 133
    .line 134
    :cond_5
    return-void
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final o〇0()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o0:Lcom/intsig/camscanner/question/mode/NpsMultiChoiceQuestion;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/question/mode/NpsMultiChoiceQuestion;->〇080()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    const-string v1, "satisfied_sheet_1"

    .line 12
    .line 13
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-eqz v1, :cond_1

    .line 18
    .line 19
    const/4 v0, 0x1

    .line 20
    goto :goto_1

    .line 21
    :cond_1
    const-string v1, "unsatisfied_sheet_2"

    .line 22
    .line 23
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    :goto_1
    if-eqz v0, :cond_2

    .line 28
    .line 29
    const-string v0, "nps_plus_Q1_100M"

    .line 30
    .line 31
    goto :goto_2

    .line 32
    :cond_2
    const-string v0, "nps_plus_Q2_100M"

    .line 33
    .line 34
    :goto_2
    return-object v0
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇8o8o〇(Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇80〇808〇O()V
    .locals 15

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o0:Lcom/intsig/camscanner/question/mode/NpsMultiChoiceQuestion;

    .line 2
    .line 3
    const-string v1, "NpsCommonMultiChoiceViewManager"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-eqz v0, :cond_12

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/question/mode/NpsMultiChoiceQuestion;->〇o00〇〇Oo()[Lcom/intsig/camscanner/question/mode/QuestionOption;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    if-eqz v0, :cond_12

    .line 13
    .line 14
    array-length v3, v0

    .line 15
    const/4 v4, 0x1

    .line 16
    const/4 v5, 0x0

    .line 17
    if-nez v3, :cond_0

    .line 18
    .line 19
    const/4 v3, 0x1

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v3, 0x0

    .line 22
    :goto_0
    xor-int/2addr v3, v4

    .line 23
    if-eqz v3, :cond_1

    .line 24
    .line 25
    goto :goto_1

    .line 26
    :cond_1
    move-object v0, v2

    .line 27
    :goto_1
    if-eqz v0, :cond_12

    .line 28
    .line 29
    array-length v3, v0

    .line 30
    add-int/2addr v3, v4

    .line 31
    new-array v6, v3, [Lcom/intsig/camscanner/question/mode/QuestionOption;

    .line 32
    .line 33
    array-length v7, v0

    .line 34
    invoke-static {v0, v5, v6, v5, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 35
    .line 36
    .line 37
    new-instance v0, Lcom/intsig/camscanner/question/mode/QuestionOption;

    .line 38
    .line 39
    iget-object v7, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇0O:Ljava/lang/String;

    .line 40
    .line 41
    iget-object v8, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇OOo8〇0:Lcom/intsig/camscanner/question/nps/NPSDialogActivity;

    .line 42
    .line 43
    if-eqz v8, :cond_2

    .line 44
    .line 45
    const v9, 0x7f130870

    .line 46
    .line 47
    .line 48
    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v8

    .line 52
    goto :goto_2

    .line 53
    :cond_2
    move-object v8, v2

    .line 54
    :goto_2
    invoke-direct {v0, v7, v8}, Lcom/intsig/camscanner/question/mode/QuestionOption;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    add-int/lit8 v7, v3, -0x1

    .line 58
    .line 59
    aput-object v0, v6, v7

    .line 60
    .line 61
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o〇00O:Landroid/view/View;

    .line 62
    .line 63
    if-eqz v0, :cond_3

    .line 64
    .line 65
    const v7, 0x7f0a0c73

    .line 66
    .line 67
    .line 68
    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    check-cast v0, Landroid/widget/LinearLayout;

    .line 73
    .line 74
    goto :goto_3

    .line 75
    :cond_3
    move-object v0, v2

    .line 76
    :goto_3
    const/4 v7, 0x0

    .line 77
    :goto_4
    if-ge v7, v3, :cond_d

    .line 78
    .line 79
    aget-object v8, v6, v7

    .line 80
    .line 81
    iget-object v9, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇OOo8〇0:Lcom/intsig/camscanner/question/nps/NPSDialogActivity;

    .line 82
    .line 83
    invoke-static {v9}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 84
    .line 85
    .line 86
    move-result-object v9

    .line 87
    const v10, 0x7f0d0555

    .line 88
    .line 89
    .line 90
    invoke-virtual {v9, v10, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 91
    .line 92
    .line 93
    move-result-object v9

    .line 94
    const-string v10, "from(npsDialog).inflate(\u2026lse\n                    )"

    .line 95
    .line 96
    invoke-static {v9, v10}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 97
    .line 98
    .line 99
    const v10, 0x7f0a136d

    .line 100
    .line 101
    .line 102
    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 103
    .line 104
    .line 105
    move-result-object v10

    .line 106
    check-cast v10, Landroid/widget/TextView;

    .line 107
    .line 108
    if-eqz v8, :cond_4

    .line 109
    .line 110
    iget-object v11, v8, Lcom/intsig/camscanner/question/mode/QuestionOption;->〇o00〇〇Oo:Ljava/lang/String;

    .line 111
    .line 112
    goto :goto_5

    .line 113
    :cond_4
    move-object v11, v2

    .line 114
    :goto_5
    if-nez v11, :cond_5

    .line 115
    .line 116
    const-string v11, ""

    .line 117
    .line 118
    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    .line 120
    .line 121
    goto :goto_6

    .line 122
    :cond_5
    iget-object v11, v8, Lcom/intsig/camscanner/question/mode/QuestionOption;->〇o00〇〇Oo:Ljava/lang/String;

    .line 123
    .line 124
    invoke-static {v11}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    .line 125
    .line 126
    .line 127
    move-result-object v11

    .line 128
    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    .line 130
    .line 131
    :goto_6
    if-eqz v8, :cond_6

    .line 132
    .line 133
    iget-object v10, v8, Lcom/intsig/camscanner/question/mode/QuestionOption;->〇080:Ljava/lang/String;

    .line 134
    .line 135
    goto :goto_7

    .line 136
    :cond_6
    move-object v10, v2

    .line 137
    :goto_7
    invoke-virtual {v9, v10}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 138
    .line 139
    .line 140
    iget-object v10, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->oOo0:Landroid/view/View$OnClickListener;

    .line 141
    .line 142
    invoke-virtual {v9, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    .line 144
    .line 145
    if-eqz v8, :cond_7

    .line 146
    .line 147
    iget-object v10, v8, Lcom/intsig/camscanner/question/mode/QuestionOption;->〇080:Ljava/lang/String;

    .line 148
    .line 149
    goto :goto_8

    .line 150
    :cond_7
    move-object v10, v2

    .line 151
    :goto_8
    iget-object v11, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇0O:Ljava/lang/String;

    .line 152
    .line 153
    invoke-static {v10, v11}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 154
    .line 155
    .line 156
    move-result v10

    .line 157
    if-eqz v10, :cond_8

    .line 158
    .line 159
    if-eqz v0, :cond_c

    .line 160
    .line 161
    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 162
    .line 163
    .line 164
    goto/16 :goto_c

    .line 165
    .line 166
    :cond_8
    if-eqz v0, :cond_9

    .line 167
    .line 168
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    .line 169
    .line 170
    .line 171
    move-result v10

    .line 172
    goto :goto_9

    .line 173
    :cond_9
    const/4 v10, 0x0

    .line 174
    :goto_9
    new-instance v11, Ljava/util/Random;

    .line 175
    .line 176
    invoke-direct {v11}, Ljava/util/Random;-><init>()V

    .line 177
    .line 178
    .line 179
    add-int/lit8 v12, v10, 0x1

    .line 180
    .line 181
    invoke-virtual {v11, v12}, Ljava/util/Random;->nextInt(I)I

    .line 182
    .line 183
    .line 184
    move-result v11

    .line 185
    if-eqz v8, :cond_a

    .line 186
    .line 187
    iget-object v12, v8, Lcom/intsig/camscanner/question/mode/QuestionOption;->〇080:Ljava/lang/String;

    .line 188
    .line 189
    goto :goto_a

    .line 190
    :cond_a
    move-object v12, v2

    .line 191
    :goto_a
    new-instance v13, Ljava/lang/StringBuilder;

    .line 192
    .line 193
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 194
    .line 195
    .line 196
    const-string v14, "initViewQuestionOptions add "

    .line 197
    .line 198
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 199
    .line 200
    .line 201
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 202
    .line 203
    .line 204
    const-string v12, ", random="

    .line 205
    .line 206
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 207
    .line 208
    .line 209
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 210
    .line 211
    .line 212
    const-string v12, ", currentContainerSize="

    .line 213
    .line 214
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    .line 216
    .line 217
    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 218
    .line 219
    .line 220
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 221
    .line 222
    .line 223
    move-result-object v13

    .line 224
    invoke-static {v1, v13}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    .line 226
    .line 227
    if-eqz v0, :cond_c

    .line 228
    .line 229
    :try_start_0
    invoke-virtual {v0, v9, v11}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 230
    .line 231
    .line 232
    goto :goto_c

    .line 233
    :catchall_0
    move-exception v9

    .line 234
    if-eqz v8, :cond_b

    .line 235
    .line 236
    iget-object v8, v8, Lcom/intsig/camscanner/question/mode/QuestionOption;->〇080:Ljava/lang/String;

    .line 237
    .line 238
    goto :goto_b

    .line 239
    :cond_b
    move-object v8, v2

    .line 240
    :goto_b
    new-instance v13, Ljava/lang/StringBuilder;

    .line 241
    .line 242
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 243
    .line 244
    .line 245
    const-string v14, "initViewQuestionOptions get error! while "

    .line 246
    .line 247
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    .line 249
    .line 250
    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    .line 252
    .line 253
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    .line 255
    .line 256
    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 257
    .line 258
    .line 259
    const-string v8, ", randomPos="

    .line 260
    .line 261
    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    .line 263
    .line 264
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 265
    .line 266
    .line 267
    const-string v8, ", \n "

    .line 268
    .line 269
    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    .line 271
    .line 272
    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 273
    .line 274
    .line 275
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 276
    .line 277
    .line 278
    move-result-object v8

    .line 279
    invoke-static {v1, v8}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    .line 281
    .line 282
    :cond_c
    :goto_c
    add-int/lit8 v7, v7, 0x1

    .line 283
    .line 284
    goto/16 :goto_4

    .line 285
    .line 286
    :cond_d
    iget-object v3, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇OOo8〇0:Lcom/intsig/camscanner/question/nps/NPSDialogActivity;

    .line 287
    .line 288
    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 289
    .line 290
    .line 291
    move-result-object v3

    .line 292
    const v6, 0x7f0d0554

    .line 293
    .line 294
    .line 295
    invoke-virtual {v3, v6, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 296
    .line 297
    .line 298
    move-result-object v3

    .line 299
    iput-object v3, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->oOo〇8o008:Landroid/view/View;

    .line 300
    .line 301
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    .line 302
    .line 303
    const/4 v5, -0x1

    .line 304
    const/4 v6, -0x2

    .line 305
    invoke-direct {v3, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 306
    .line 307
    .line 308
    iget-object v5, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇OOo8〇0:Lcom/intsig/camscanner/question/nps/NPSDialogActivity;

    .line 309
    .line 310
    const/16 v6, 0xc

    .line 311
    .line 312
    invoke-static {v5, v6}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 313
    .line 314
    .line 315
    move-result v5

    .line 316
    iput v5, v3, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 317
    .line 318
    iget-object v5, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇OOo8〇0:Lcom/intsig/camscanner/question/nps/NPSDialogActivity;

    .line 319
    .line 320
    const/16 v6, 0x8

    .line 321
    .line 322
    invoke-static {v5, v6}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 323
    .line 324
    .line 325
    move-result v5

    .line 326
    iput v5, v3, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 327
    .line 328
    iget-object v5, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->oOo〇8o008:Landroid/view/View;

    .line 329
    .line 330
    if-nez v5, :cond_e

    .line 331
    .line 332
    goto :goto_d

    .line 333
    :cond_e
    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 334
    .line 335
    .line 336
    :goto_d
    iget-object v5, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->oOo〇8o008:Landroid/view/View;

    .line 337
    .line 338
    if-eqz v5, :cond_f

    .line 339
    .line 340
    const v6, 0x7f0a197c

    .line 341
    .line 342
    .line 343
    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 344
    .line 345
    .line 346
    move-result-object v5

    .line 347
    if-eqz v5, :cond_f

    .line 348
    .line 349
    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 350
    .line 351
    .line 352
    :cond_f
    iget-object v5, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->oOo〇8o008:Landroid/view/View;

    .line 353
    .line 354
    if-eqz v5, :cond_10

    .line 355
    .line 356
    const v6, 0x7f0a10a0

    .line 357
    .line 358
    .line 359
    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 360
    .line 361
    .line 362
    move-result-object v5

    .line 363
    check-cast v5, Lcom/intsig/camscanner/view/MaxHeightLimitScrollView;

    .line 364
    .line 365
    goto :goto_e

    .line 366
    :cond_10
    move-object v5, v2

    .line 367
    :goto_e
    if-eqz v5, :cond_11

    .line 368
    .line 369
    invoke-virtual {v5, v4}, Lcom/intsig/camscanner/view/MaxHeightLimitScrollView;->setRequestDisallowInterceptTouchEvent(Z)V

    .line 370
    .line 371
    .line 372
    :cond_11
    if-eqz v0, :cond_12

    .line 373
    .line 374
    iget-object v4, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->oOo〇8o008:Landroid/view/View;

    .line 375
    .line 376
    invoke-virtual {v0, v4, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 377
    .line 378
    .line 379
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 380
    .line 381
    goto :goto_f

    .line 382
    :cond_12
    move-object v0, v2

    .line 383
    :goto_f
    if-nez v0, :cond_14

    .line 384
    .line 385
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o0:Lcom/intsig/camscanner/question/mode/NpsMultiChoiceQuestion;

    .line 386
    .line 387
    if-eqz v0, :cond_13

    .line 388
    .line 389
    invoke-virtual {v0}, Lcom/intsig/camscanner/question/mode/NpsMultiChoiceQuestion;->〇o00〇〇Oo()[Lcom/intsig/camscanner/question/mode/QuestionOption;

    .line 390
    .line 391
    .line 392
    move-result-object v0

    .line 393
    if-eqz v0, :cond_13

    .line 394
    .line 395
    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    .line 396
    .line 397
    .line 398
    move-result-object v2

    .line 399
    const-string v0, "toString(this)"

    .line 400
    .line 401
    invoke-static {v2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 402
    .line 403
    .line 404
    :cond_13
    new-instance v0, Ljava/lang/StringBuilder;

    .line 405
    .line 406
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 407
    .line 408
    .line 409
    const-string v3, "initViewQuestionOptions but get "

    .line 410
    .line 411
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412
    .line 413
    .line 414
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 415
    .line 416
    .line 417
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 418
    .line 419
    .line 420
    move-result-object v0

    .line 421
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    .line 423
    .line 424
    :cond_14
    return-void
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private static final 〇8o8o〇(Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;Landroid/view/View;)V
    .locals 7

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇080OO8〇0:Lcom/intsig/utils/ClickLimit;

    .line 7
    .line 8
    const-wide/16 v1, 0x12c

    .line 9
    .line 10
    invoke-virtual {v0, p1, v1, v2}, Lcom/intsig/utils/ClickLimit;->〇o00〇〇Oo(Landroid/view/View;J)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-nez v0, :cond_0

    .line 15
    .line 16
    const-string p0, "NpsCommonMultiChoiceViewManager"

    .line 17
    .line 18
    const-string p1, "click too fast"

    .line 19
    .line 20
    invoke-static {p0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    return-void

    .line 24
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    instance-of v1, v0, Ljava/lang/String;

    .line 29
    .line 30
    if-eqz v1, :cond_d

    .line 31
    .line 32
    const v1, 0x7f0a0872

    .line 33
    .line 34
    .line 35
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    check-cast p1, Landroid/widget/ImageView;

    .line 40
    .line 41
    iget-object v1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o8〇OO0〇0o:Ljava/util/HashSet;

    .line 42
    .line 43
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    .line 44
    .line 45
    .line 46
    move-result v1

    .line 47
    const v2, 0x7f0a197c

    .line 48
    .line 49
    .line 50
    const/4 v3, 0x0

    .line 51
    const/4 v4, 0x0

    .line 52
    const/4 v5, 0x1

    .line 53
    if-eqz v1, :cond_3

    .line 54
    .line 55
    iget-object v1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o8〇OO0〇0o:Ljava/util/HashSet;

    .line 56
    .line 57
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 58
    .line 59
    .line 60
    const v1, 0x7f0803b5

    .line 61
    .line 62
    .line 63
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 64
    .line 65
    .line 66
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇0O:Ljava/lang/String;

    .line 67
    .line 68
    check-cast v0, Ljava/lang/CharSequence;

    .line 69
    .line 70
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 71
    .line 72
    .line 73
    move-result p1

    .line 74
    if-eqz p1, :cond_9

    .line 75
    .line 76
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->oOo〇8o008:Landroid/view/View;

    .line 77
    .line 78
    if-nez p1, :cond_1

    .line 79
    .line 80
    goto :goto_0

    .line 81
    :cond_1
    const/16 v0, 0x8

    .line 82
    .line 83
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 84
    .line 85
    .line 86
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇OOo8〇0:Lcom/intsig/camscanner/question/nps/NPSDialogActivity;

    .line 87
    .line 88
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->oOo〇8o008:Landroid/view/View;

    .line 89
    .line 90
    if-eqz v0, :cond_2

    .line 91
    .line 92
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    move-object v3, v0

    .line 97
    check-cast v3, Landroid/widget/EditText;

    .line 98
    .line 99
    :cond_2
    invoke-static {p1, v3}, Lcom/intsig/utils/SoftKeyboardUtils;->〇o00〇〇Oo(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 100
    .line 101
    .line 102
    goto :goto_2

    .line 103
    :cond_3
    iget-object v1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o8〇OO0〇0o:Ljava/util/HashSet;

    .line 104
    .line 105
    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    .line 106
    .line 107
    .line 108
    move-result v1

    .line 109
    const/4 v6, 0x3

    .line 110
    if-ge v1, v6, :cond_7

    .line 111
    .line 112
    iget-object v1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o8〇OO0〇0o:Ljava/util/HashSet;

    .line 113
    .line 114
    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 115
    .line 116
    .line 117
    const v1, 0x7f0803b4

    .line 118
    .line 119
    .line 120
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 121
    .line 122
    .line 123
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇0O:Ljava/lang/String;

    .line 124
    .line 125
    check-cast v0, Ljava/lang/CharSequence;

    .line 126
    .line 127
    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 128
    .line 129
    .line 130
    move-result p1

    .line 131
    if-eqz p1, :cond_9

    .line 132
    .line 133
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->oOo〇8o008:Landroid/view/View;

    .line 134
    .line 135
    if-nez p1, :cond_4

    .line 136
    .line 137
    goto :goto_1

    .line 138
    :cond_4
    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 139
    .line 140
    .line 141
    :goto_1
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->oOo〇8o008:Landroid/view/View;

    .line 142
    .line 143
    if-eqz p1, :cond_5

    .line 144
    .line 145
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 146
    .line 147
    .line 148
    move-result-object p1

    .line 149
    move-object v3, p1

    .line 150
    check-cast v3, Landroid/widget/EditText;

    .line 151
    .line 152
    :cond_5
    if-eqz v3, :cond_6

    .line 153
    .line 154
    invoke-virtual {v3}, Landroid/widget/TextView;->length()I

    .line 155
    .line 156
    .line 157
    move-result p1

    .line 158
    invoke-virtual {v3, p1}, Landroid/widget/EditText;->setSelection(I)V

    .line 159
    .line 160
    .line 161
    :cond_6
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇OOo8〇0:Lcom/intsig/camscanner/question/nps/NPSDialogActivity;

    .line 162
    .line 163
    invoke-static {p1, v3}, Lcom/intsig/utils/SoftKeyboardUtils;->O8(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 164
    .line 165
    .line 166
    goto :goto_2

    .line 167
    :cond_7
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->OO〇00〇8oO:Landroid/widget/Toast;

    .line 168
    .line 169
    if-eqz p1, :cond_8

    .line 170
    .line 171
    if-eqz p1, :cond_8

    .line 172
    .line 173
    invoke-virtual {p1}, Landroid/widget/Toast;->cancel()V

    .line 174
    .line 175
    .line 176
    :cond_8
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇OOo8〇0:Lcom/intsig/camscanner/question/nps/NPSDialogActivity;

    .line 177
    .line 178
    const v0, 0x7f130864

    .line 179
    .line 180
    .line 181
    invoke-static {p1, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    .line 182
    .line 183
    .line 184
    move-result-object p1

    .line 185
    iput-object p1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->OO〇00〇8oO:Landroid/widget/Toast;

    .line 186
    .line 187
    if-eqz p1, :cond_9

    .line 188
    .line 189
    invoke-virtual {p1}, Landroid/widget/Toast;->show()V

    .line 190
    .line 191
    .line 192
    :cond_9
    :goto_2
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o8〇OO0〇0o:Ljava/util/HashSet;

    .line 193
    .line 194
    invoke-virtual {p1}, Ljava/util/HashSet;->size()I

    .line 195
    .line 196
    .line 197
    move-result p1

    .line 198
    if-lez p1, :cond_b

    .line 199
    .line 200
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->O8o08O8O:Landroid/view/View;

    .line 201
    .line 202
    if-nez p1, :cond_a

    .line 203
    .line 204
    goto :goto_3

    .line 205
    :cond_a
    invoke-virtual {p1, v5}, Landroid/view/View;->setClickable(Z)V

    .line 206
    .line 207
    .line 208
    :goto_3
    iget-object p0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->O8o08O8O:Landroid/view/View;

    .line 209
    .line 210
    if-eqz p0, :cond_d

    .line 211
    .line 212
    const p1, 0x7f0801cb

    .line 213
    .line 214
    .line 215
    invoke-virtual {p0, p1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 216
    .line 217
    .line 218
    goto :goto_5

    .line 219
    :cond_b
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->O8o08O8O:Landroid/view/View;

    .line 220
    .line 221
    if-nez p1, :cond_c

    .line 222
    .line 223
    goto :goto_4

    .line 224
    :cond_c
    invoke-virtual {p1, v4}, Landroid/view/View;->setClickable(Z)V

    .line 225
    .line 226
    .line 227
    :goto_4
    iget-object p0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->O8o08O8O:Landroid/view/View;

    .line 228
    .line 229
    if-eqz p0, :cond_d

    .line 230
    .line 231
    const p1, 0x7f0801cc

    .line 232
    .line 233
    .line 234
    invoke-virtual {p0, p1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 235
    .line 236
    .line 237
    :cond_d
    :goto_5
    return-void
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o〇00O:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇888()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o0:Lcom/intsig/camscanner/question/mode/NpsMultiChoiceQuestion;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/question/mode/NpsMultiChoiceQuestion;->〇080()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    const-string v1, "satisfied_sheet_1"

    .line 12
    .line 13
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-eqz v1, :cond_1

    .line 18
    .line 19
    const/4 v0, 0x1

    .line 20
    goto :goto_1

    .line 21
    :cond_1
    const-string v1, "unsatisfied_sheet_2"

    .line 22
    .line 23
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    :goto_1
    if-eqz v0, :cond_2

    .line 28
    .line 29
    const-string v0, "CSFunctionNPSPop"

    .line 30
    .line 31
    goto :goto_2

    .line 32
    :cond_2
    const-string v0, "CSNPSReasonPop"

    .line 33
    .line 34
    :goto_2
    return-object v0
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "view"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇080OO8〇0:Lcom/intsig/utils/ClickLimit;

    .line 11
    .line 12
    const-wide/16 v2, 0x12c

    .line 13
    .line 14
    invoke-virtual {v1, p1, v2, v3}, Lcom/intsig/utils/ClickLimit;->〇o00〇〇Oo(Landroid/view/View;J)Z

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    const-string v1, "NpsCommonMultiChoiceViewManager"

    .line 19
    .line 20
    if-nez p1, :cond_0

    .line 21
    .line 22
    const-string p1, "click too fast"

    .line 23
    .line 24
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    return-void

    .line 28
    :cond_0
    const p1, 0x7f0a087d

    .line 29
    .line 30
    .line 31
    if-eq v0, p1, :cond_3

    .line 32
    .line 33
    const p1, 0x7f0a1310

    .line 34
    .line 35
    .line 36
    if-eq v0, p1, :cond_1

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_1
    const-string p1, "click commit"

    .line 40
    .line 41
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    invoke-direct {p0}, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->Oo08()V

    .line 45
    .line 46
    .line 47
    invoke-direct {p0}, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->OO0o〇〇〇〇0()Z

    .line 48
    .line 49
    .line 50
    move-result p1

    .line 51
    if-eqz p1, :cond_2

    .line 52
    .line 53
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇OOo8〇0:Lcom/intsig/camscanner/question/nps/NPSDialogActivity;

    .line 54
    .line 55
    if-eqz p1, :cond_2

    .line 56
    .line 57
    iget-object v0, p1, Lcom/intsig/camscanner/question/nps/NPSDialogActivity;->OO:Lcom/intsig/camscanner/gift/GiftQueryManager;

    .line 58
    .line 59
    if-eqz v0, :cond_2

    .line 60
    .line 61
    const-string v1, "nps_plus"

    .line 62
    .line 63
    invoke-direct {p0}, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o〇0()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object v2

    .line 67
    const/4 v3, 0x0

    .line 68
    const/4 v4, 0x4

    .line 69
    const/4 v5, 0x0

    .line 70
    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/gift/GiftQueryManager;->〇o〇(Lcom/intsig/camscanner/gift/GiftQueryManager;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    .line 71
    .line 72
    .line 73
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->oO80()V

    .line 74
    .line 75
    .line 76
    goto :goto_0

    .line 77
    :cond_3
    const-string p1, "click close"

    .line 78
    .line 79
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    .line 81
    .line 82
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇OOo8〇0:Lcom/intsig/camscanner/question/nps/NPSDialogActivity;

    .line 83
    .line 84
    if-eqz p1, :cond_4

    .line 85
    .line 86
    invoke-virtual {p1}, Lcom/intsig/camscanner/question/nps/NPSDialogActivity;->〇00()V

    .line 87
    .line 88
    .line 89
    :cond_4
    :goto_0
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public 〇o00〇〇Oo()Landroid/view/View;
    .locals 4

    .line 1
    const-string v0, "NpsCommonMultiChoiceViewManager"

    .line 2
    .line 3
    const-string v1, "show"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇〇888()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const-string v1, "from"

    .line 13
    .line 14
    iget-object v2, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇08O〇00〇o:Ljava/lang/String;

    .line 15
    .line 16
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/log/LogAgentData;->Oooo8o0〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇OOo8〇0:Lcom/intsig/camscanner/question/nps/NPSDialogActivity;

    .line 20
    .line 21
    const/4 v1, 0x0

    .line 22
    if-eqz v0, :cond_0

    .line 23
    .line 24
    const v2, 0x7f0d01ed

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/question/nps/NPSDialogActivity;->oooO888(I)Landroid/view/View;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    goto :goto_0

    .line 32
    :cond_0
    move-object v0, v1

    .line 33
    :goto_0
    iput-object v0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o〇00O:Landroid/view/View;

    .line 34
    .line 35
    if-eqz v0, :cond_1

    .line 36
    .line 37
    const v2, 0x7f0a187b

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    check-cast v0, Landroid/widget/TextView;

    .line 45
    .line 46
    goto :goto_1

    .line 47
    :cond_1
    move-object v0, v1

    .line 48
    :goto_1
    iget-object v2, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o0:Lcom/intsig/camscanner/question/mode/NpsMultiChoiceQuestion;

    .line 49
    .line 50
    if-eqz v2, :cond_2

    .line 51
    .line 52
    invoke-virtual {v2}, Lcom/intsig/camscanner/question/mode/NpsMultiChoiceQuestion;->〇o〇()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v2

    .line 56
    goto :goto_2

    .line 57
    :cond_2
    move-object v2, v1

    .line 58
    :goto_2
    if-eqz v2, :cond_4

    .line 59
    .line 60
    invoke-static {v2}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 61
    .line 62
    .line 63
    move-result v3

    .line 64
    xor-int/lit8 v3, v3, 0x1

    .line 65
    .line 66
    if-eqz v3, :cond_3

    .line 67
    .line 68
    goto :goto_3

    .line 69
    :cond_3
    move-object v2, v1

    .line 70
    :goto_3
    if-nez v2, :cond_5

    .line 71
    .line 72
    :cond_4
    const-string v2, ""

    .line 73
    .line 74
    :cond_5
    if-nez v0, :cond_6

    .line 75
    .line 76
    goto :goto_4

    .line 77
    :cond_6
    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    .line 78
    .line 79
    .line 80
    move-result-object v2

    .line 81
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    .line 83
    .line 84
    :goto_4
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o〇00O:Landroid/view/View;

    .line 85
    .line 86
    if-eqz v0, :cond_7

    .line 87
    .line 88
    const v2, 0x7f0a087d

    .line 89
    .line 90
    .line 91
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    if-eqz v0, :cond_7

    .line 96
    .line 97
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    .line 99
    .line 100
    :cond_7
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o〇00O:Landroid/view/View;

    .line 101
    .line 102
    if-eqz v0, :cond_8

    .line 103
    .line 104
    const v2, 0x7f0a1310

    .line 105
    .line 106
    .line 107
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 108
    .line 109
    .line 110
    move-result-object v0

    .line 111
    goto :goto_5

    .line 112
    :cond_8
    move-object v0, v1

    .line 113
    :goto_5
    iput-object v0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->O8o08O8O:Landroid/view/View;

    .line 114
    .line 115
    if-eqz v0, :cond_9

    .line 116
    .line 117
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    .line 119
    .line 120
    :cond_9
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->O8o08O8O:Landroid/view/View;

    .line 121
    .line 122
    const/4 v2, 0x0

    .line 123
    if-nez v0, :cond_a

    .line 124
    .line 125
    goto :goto_6

    .line 126
    :cond_a
    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 127
    .line 128
    .line 129
    :goto_6
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->O8o08O8O:Landroid/view/View;

    .line 130
    .line 131
    if-eqz v0, :cond_b

    .line 132
    .line 133
    const v3, 0x7f0801cc

    .line 134
    .line 135
    .line 136
    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 137
    .line 138
    .line 139
    :cond_b
    invoke-direct {p0}, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->〇80〇808〇O()V

    .line 140
    .line 141
    .line 142
    invoke-direct {p0}, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->O8()V

    .line 143
    .line 144
    .line 145
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o〇00O:Landroid/view/View;

    .line 146
    .line 147
    if-eqz v0, :cond_c

    .line 148
    .line 149
    const v1, 0x7f0a14db

    .line 150
    .line 151
    .line 152
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 153
    .line 154
    .line 155
    move-result-object v0

    .line 156
    move-object v1, v0

    .line 157
    check-cast v1, Landroid/widget/TextView;

    .line 158
    .line 159
    :cond_c
    if-nez v1, :cond_d

    .line 160
    .line 161
    goto :goto_8

    .line 162
    :cond_d
    invoke-direct {p0}, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->OO0o〇〇〇〇0()Z

    .line 163
    .line 164
    .line 165
    move-result v0

    .line 166
    if-eqz v0, :cond_e

    .line 167
    .line 168
    goto :goto_7

    .line 169
    :cond_e
    const/16 v2, 0x8

    .line 170
    .line 171
    :goto_7
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 172
    .line 173
    .line 174
    :goto_8
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NpsCommonMultiChoiceViewManager;->o〇00O:Landroid/view/View;

    .line 175
    .line 176
    return-object v0
    .line 177
.end method

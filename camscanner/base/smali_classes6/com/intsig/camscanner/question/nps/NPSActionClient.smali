.class public Lcom/intsig/camscanner/question/nps/NPSActionClient;
.super Ljava/lang/Object;
.source "NPSActionClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/question/nps/NPSActionClient$RecordActionData;,
        Lcom/intsig/camscanner/question/nps/NPSActionClient$NPSActionClientImpl;
    }
.end annotation


# instance fields
.field private O8:Lcom/intsig/camscanner/question/NPSCheckData;

.field private final Oo08:Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;

.field private final 〇080:[B

.field private volatile 〇o00〇〇Oo:Landroid/os/HandlerThread;

.field private 〇o〇:Landroid/os/Handler;


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    new-array v0, v0, [B

    .line 3
    iput-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->〇080:[B

    .line 4
    new-instance v0, Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;

    invoke-direct {v0}, Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->Oo08:Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;

    return-void
.end method

.method synthetic constructor <init>(LO088O/〇o00〇〇Oo;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/question/nps/NPSActionClient;-><init>()V

    return-void
.end method

.method private synthetic OO0o〇〇(Landroid/os/Message;)Z
    .locals 3

    .line 1
    iget-object p1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 2
    .line 3
    instance-of v0, p1, Lcom/intsig/camscanner/question/nps/NPSActionClient$RecordActionData;

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    check-cast p1, Lcom/intsig/camscanner/question/nps/NPSActionClient$RecordActionData;

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->Oo08:Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;

    .line 10
    .line 11
    invoke-static {p1}, Lcom/intsig/camscanner/question/nps/NPSActionClient$RecordActionData;->〇o00〇〇Oo(Lcom/intsig/camscanner/question/nps/NPSActionClient$RecordActionData;)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-static {p1}, Lcom/intsig/camscanner/question/nps/NPSActionClient$RecordActionData;->〇080(Lcom/intsig/camscanner/question/nps/NPSActionClient$RecordActionData;)Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    invoke-static {p1}, Lcom/intsig/camscanner/question/nps/NPSActionClient$RecordActionData;->〇o〇(Lcom/intsig/camscanner/question/nps/NPSActionClient$RecordActionData;)Lorg/json/JSONObject;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    invoke-virtual {v0, v1, v2, p1}, Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;->〇〇808〇(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 24
    .line 25
    .line 26
    const/4 p1, 0x1

    .line 27
    return p1

    .line 28
    :cond_0
    const/4 p1, 0x0

    .line 29
    return p1
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static Oo08()Lcom/intsig/camscanner/question/nps/NPSActionClient;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/question/nps/NPSActionClient$NPSActionClientImpl;->〇080:Lcom/intsig/camscanner/question/nps/NPSActionClient;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/question/nps/NPSActionClient;Landroid/os/Message;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/question/nps/NPSActionClient;->OO0o〇〇(Landroid/os/Message;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇O8o08O()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->〇o00〇〇Oo:Landroid/os/HandlerThread;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->〇o〇:Landroid/os/Handler;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->〇080:[B

    .line 11
    .line 12
    monitor-enter v0

    .line 13
    :try_start_0
    iget-object v1, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->〇o00〇〇Oo:Landroid/os/HandlerThread;

    .line 14
    .line 15
    if-nez v1, :cond_1

    .line 16
    .line 17
    new-instance v1, Landroid/os/HandlerThread;

    .line 18
    .line 19
    const-string v2, "NPSAction"

    .line 20
    .line 21
    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    iput-object v1, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->〇o00〇〇Oo:Landroid/os/HandlerThread;

    .line 25
    .line 26
    iget-object v1, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->〇o00〇〇Oo:Landroid/os/HandlerThread;

    .line 27
    .line 28
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 29
    .line 30
    .line 31
    new-instance v1, Landroid/os/Handler;

    .line 32
    .line 33
    iget-object v2, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->〇o00〇〇Oo:Landroid/os/HandlerThread;

    .line 34
    .line 35
    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    invoke-direct {p0}, Lcom/intsig/camscanner/question/nps/NPSActionClient;->〇o〇()Landroid/os/Handler$Callback;

    .line 40
    .line 41
    .line 42
    move-result-object v3

    .line 43
    invoke-direct {v1, v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    .line 44
    .line 45
    .line 46
    iput-object v1, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->〇o〇:Landroid/os/Handler;

    .line 47
    .line 48
    :cond_1
    monitor-exit v0

    .line 49
    return-void

    .line 50
    :catchall_0
    move-exception v1

    .line 51
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 52
    throw v1
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇o〇()Landroid/os/Handler$Callback;
    .locals 1

    .line 1
    new-instance v0, LO088O/〇080;

    .line 2
    .line 3
    invoke-direct {v0, p0}, LO088O/〇080;-><init>(Lcom/intsig/camscanner/question/nps/NPSActionClient;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public O8()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/question/mode/NPSActionDataGroup;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->Oo08:Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;->〇O〇()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OO0o〇〇〇〇0()Lcom/intsig/camscanner/question/mode/NpsMultiChoiceQuestion;
    .locals 2
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->Oo08:Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;->〇〇8O0〇8()Ljava/util/Map;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "unsatisfied_sheet_2"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, Lcom/intsig/camscanner/question/mode/NpsMultiChoiceQuestion;

    .line 14
    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OoO8(Lcom/intsig/camscanner/question/mode/NPSActionDataOriginGroup;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->Oo08:Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;->o〇O8〇〇o(Lcom/intsig/camscanner/question/mode/NPSActionDataOriginGroup;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public Oooo8o0〇(Lcom/intsig/tsapp/sync/AppConfigJson;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->Oo08:Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;->OoO8(Lcom/intsig/tsapp/sync/AppConfigJson;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o800o8O(Lcom/intsig/camscanner/question/mode/NPSActionDataGroup;)Lcom/intsig/camscanner/question/nps/NPSActionClient;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->Oo08:Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;->〇00(Lcom/intsig/camscanner/question/mode/NPSActionDataGroup;)V

    .line 4
    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public oO80()Lcom/intsig/camscanner/question/mode/NpsMultiChoiceQuestion;
    .locals 2
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->Oo08:Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;->〇〇8O0〇8()Ljava/util/Map;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "satisfied_sheet_1"

    .line 8
    .line 9
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, Lcom/intsig/camscanner/question/mode/NpsMultiChoiceQuestion;

    .line 14
    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇0()Lcom/intsig/camscanner/question/mode/NPSActionDataGroup;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->Oo08:Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;->〇O00()Lcom/intsig/camscanner/question/mode/NPSActionDataGroup;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇0〇O0088o()Lcom/intsig/camscanner/question/nps/NPSActionClient;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->O8:Lcom/intsig/camscanner/question/NPSCheckData;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-object p0

    .line 6
    :cond_0
    invoke-static {v0}, Lcom/intsig/okgo/utils/GsonUtils;->Oo08(Ljava/lang/Object;)Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇o0O0(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    return-object p0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇80〇808〇O(Ljava/lang/String;)Lcom/intsig/camscanner/question/mode/NpsMultiChoiceQuestion;
    .locals 3
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const/4 p1, 0x0

    .line 8
    return-object p1

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->Oo08:Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;->〇〇8O0〇8()Ljava/util/Map;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    new-instance v1, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v2, "unsatisfied_sheet_"

    .line 21
    .line 22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    check-cast p1, Lcom/intsig/camscanner/question/mode/NpsMultiChoiceQuestion;

    .line 37
    .line 38
    return-object p1
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇8o8o〇()I
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->O8:Lcom/intsig/camscanner/question/NPSCheckData;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇〇888(Landroid/content/Context;Z)I

    .line 9
    .line 10
    .line 11
    move-result v6

    .line 12
    new-instance v0, Lcom/intsig/camscanner/question/NPSCheckData;

    .line 13
    .line 14
    sget-object v2, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 15
    .line 16
    const v3, 0x7f1304ee

    .line 17
    .line 18
    .line 19
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v3

    .line 23
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 24
    .line 25
    .line 26
    move-result-wide v4

    .line 27
    const/4 v7, 0x0

    .line 28
    move-object v2, v0

    .line 29
    invoke-direct/range {v2 .. v7}, Lcom/intsig/camscanner/question/NPSCheckData;-><init>(Ljava/lang/String;JII)V

    .line 30
    .line 31
    .line 32
    iput-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->O8:Lcom/intsig/camscanner/question/NPSCheckData;

    .line 33
    .line 34
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->O8:Lcom/intsig/camscanner/question/NPSCheckData;

    .line 35
    .line 36
    if-nez v0, :cond_1

    .line 37
    .line 38
    return v1

    .line 39
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/camscanner/question/NPSCheckData;->getNpsTimes()I

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    const/4 v1, 0x1

    .line 44
    if-gtz v0, :cond_2

    .line 45
    .line 46
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->O8:Lcom/intsig/camscanner/question/NPSCheckData;

    .line 47
    .line 48
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/question/NPSCheckData;->setNpsTimes(I)V

    .line 49
    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_2
    const v2, 0x7fffffff

    .line 53
    .line 54
    .line 55
    if-ge v0, v2, :cond_3

    .line 56
    .line 57
    iget-object v2, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->O8:Lcom/intsig/camscanner/question/NPSCheckData;

    .line 58
    .line 59
    add-int/2addr v0, v1

    .line 60
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/question/NPSCheckData;->setNpsTimes(I)V

    .line 61
    .line 62
    .line 63
    :cond_3
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->O8:Lcom/intsig/camscanner/question/NPSCheckData;

    .line 64
    .line 65
    invoke-virtual {v0}, Lcom/intsig/camscanner/question/NPSCheckData;->getNpsTimes()I

    .line 66
    .line 67
    .line 68
    move-result v0

    .line 69
    return v0
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public 〇O00(Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider$RandomNPSActionDataGroupCallback;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->Oo08:Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;->oo88o8O(Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider$RandomNPSActionDataGroupCallback;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇O888o0o(Lcom/intsig/camscanner/question/NPSCheckData;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->O8:Lcom/intsig/camscanner/question/NPSCheckData;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇O〇(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/question/nps/NPSActionClient;->〇O8o08O()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->〇o〇:Landroid/os/Handler;

    .line 5
    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    const-string p1, "NPSActionClient"

    .line 9
    .line 10
    const-string p2, "handler == null"

    .line 11
    .line 12
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    return-void

    .line 16
    :cond_0
    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    new-instance v1, Lcom/intsig/camscanner/question/nps/NPSActionClient$RecordActionData;

    .line 21
    .line 22
    invoke-direct {v1, p1, p2, p3}, Lcom/intsig/camscanner/question/nps/NPSActionClient$RecordActionData;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 23
    .line 24
    .line 25
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->〇o〇:Landroid/os/Handler;

    .line 28
    .line 29
    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public 〇o00〇〇Oo(Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider$RandomNPSActionDataGroupCallback;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->Oo08:Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;->〇080(Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider$RandomNPSActionDataGroupCallback;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇〇808〇(Z)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->Oo08:Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;->〇O888o0o(Z)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->O8:Lcom/intsig/camscanner/question/NPSCheckData;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return v0

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/question/NPSCheckData;->getNpsTimes()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇8O0〇8()Lcom/intsig/camscanner/question/nps/NPSActionClient;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSActionClient;->Oo08:Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider;->〇oo〇()V

    .line 4
    .line 5
    .line 6
    return-object p0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

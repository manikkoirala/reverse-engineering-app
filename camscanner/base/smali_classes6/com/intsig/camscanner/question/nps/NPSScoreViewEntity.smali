.class public Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity;
.super Ljava/lang/Object;
.source "NPSScoreViewEntity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$Builder;
    }
.end annotation


# instance fields
.field O8:I

.field OO0o〇〇〇〇0:I

.field Oo08:I

.field oO80:I

.field o〇0:I

.field 〇080:I
    .annotation build Landroidx/annotation/DrawableRes;
    .end annotation
.end field

.field 〇80〇808〇O:I

.field 〇8o8o〇:I

.field 〇O8o08O:Z

.field 〇o00〇〇Oo:I

.field 〇o〇:I

.field 〇〇888:I


# direct methods
.method private constructor <init>(Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$Builder;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$Builder;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    invoke-static {p1}, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$Builder;->Oo08(Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$Builder;)I

    move-result v0

    iput v0, p0, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity;->〇080:I

    .line 4
    invoke-static {p1}, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$Builder;->〇〇888(Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$Builder;)I

    move-result v0

    iput v0, p0, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity;->〇o00〇〇Oo:I

    .line 5
    invoke-static {p1}, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$Builder;->〇080(Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$Builder;)I

    move-result v0

    iput v0, p0, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity;->〇o〇:I

    .line 6
    invoke-static {p1}, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$Builder;->o〇0(Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$Builder;)I

    move-result v0

    iput v0, p0, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity;->O8:I

    .line 7
    invoke-static {p1}, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$Builder;->O8(Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$Builder;)I

    move-result v0

    iput v0, p0, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity;->Oo08:I

    .line 8
    invoke-static {p1}, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$Builder;->〇o00〇〇Oo(Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$Builder;)I

    move-result v0

    iput v0, p0, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity;->o〇0:I

    .line 9
    invoke-static {p1}, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$Builder;->〇o〇(Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity;->〇O8o08O:Z

    .line 10
    iget v0, p1, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$Builder;->〇〇888:I

    iput v0, p0, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity;->〇〇888:I

    .line 11
    iget v0, p1, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$Builder;->oO80:I

    iput v0, p0, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity;->oO80:I

    .line 12
    iget v0, p1, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$Builder;->〇80〇808〇O:I

    iput v0, p0, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity;->〇80〇808〇O:I

    .line 13
    iget v0, p1, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$Builder;->OO0o〇〇〇〇0:I

    iput v0, p0, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity;->OO0o〇〇〇〇0:I

    .line 14
    iget p1, p1, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$Builder;->〇8o8o〇:I

    iput p1, p0, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity;->〇8o8o〇:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$Builder;LO088O/oO80;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity;-><init>(Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$Builder;)V

    return-void
.end method


# virtual methods
.method public 〇080()Lcom/intsig/camscanner/question/nps/NPSScoreViewManager$IScoreViewGenerator;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$1;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity$1;-><init>(Lcom/intsig/camscanner/question/nps/NPSScoreViewEntity;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;
.super Ljava/lang/Object;
.source "NPSDetectionLifecycleObserver.java"

# interfaces
.implements Landroidx/lifecycle/DefaultLifecycleObserver;


# instance fields
.field private final O8o08O8O:Lcom/intsig/camscanner/question/nps/NPSActionClient;

.field private final OO:I

.field private final o0:Landroid/app/Activity;

.field private o〇00O:Landroid/os/Handler;

.field private volatile 〇080OO8〇0:Z

.field private 〇08O〇00〇o:Landroid/os/HandlerThread;

.field private final 〇0O:Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider$RandomNPSActionDataGroupCallback;

.field private final 〇OOo8〇0:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider$RandomNPSActionDataGroupCallback;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x3e9

    .line 5
    .line 6
    iput v0, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->OO:I

    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    iput-boolean v0, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->〇080OO8〇0:Z

    .line 10
    .line 11
    iput-object p1, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->o0:Landroid/app/Activity;

    .line 12
    .line 13
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    iput-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->〇OOo8〇0:Landroid/content/Context;

    .line 18
    .line 19
    invoke-static {}, Lcom/intsig/camscanner/question/nps/NPSActionClient;->Oo08()Lcom/intsig/camscanner/question/nps/NPSActionClient;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->O8o08O8O:Lcom/intsig/camscanner/question/nps/NPSActionClient;

    .line 24
    .line 25
    new-instance v0, LO088O/O8;

    .line 26
    .line 27
    invoke-direct {v0, p0, p1, p2}, LO088O/O8;-><init>(Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;Landroid/app/Activity;Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider$RandomNPSActionDataGroupCallback;)V

    .line 28
    .line 29
    .line 30
    iput-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->〇0O:Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider$RandomNPSActionDataGroupCallback;

    .line 31
    .line 32
    return-void
    .line 33
.end method

.method private O8()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->〇08O〇00〇o:Landroid/os/HandlerThread;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->o〇00O:Landroid/os/Handler;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    new-instance v0, Landroid/os/HandlerThread;

    .line 11
    .line 12
    const-string v1, "NPSDetectionLifecycleObserver"

    .line 13
    .line 14
    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    iput-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->〇08O〇00〇o:Landroid/os/HandlerThread;

    .line 18
    .line 19
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 20
    .line 21
    .line 22
    new-instance v0, Landroid/os/Handler;

    .line 23
    .line 24
    iget-object v1, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->〇08O〇00〇o:Landroid/os/HandlerThread;

    .line 25
    .line 26
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    new-instance v2, LO088O/〇o〇;

    .line 31
    .line 32
    invoke-direct {v2, p0}, LO088O/〇o〇;-><init>(Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;)V

    .line 33
    .line 34
    .line 35
    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    .line 36
    .line 37
    .line 38
    iput-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->o〇00O:Landroid/os/Handler;

    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic Oo08(Landroid/os/Message;)Z
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->〇080OO8〇0:Z

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_5

    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->o0:Landroid/app/Activity;

    .line 7
    .line 8
    if-eqz v0, :cond_5

    .line 9
    .line 10
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->O8o08O8O:Lcom/intsig/camscanner/question/nps/NPSActionClient;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/question/nps/NPSActionClient;->o〇0()Lcom/intsig/camscanner/question/mode/NPSActionDataGroup;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    const-string p1, "NPSDetectionLifecycleObserver"

    .line 26
    .line 27
    const-string v0, "initHandlerThread npsActionClient.getNpsActionDataGroup() != null "

    .line 28
    .line 29
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->〇0O:Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider$RandomNPSActionDataGroupCallback;

    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->O8o08O8O:Lcom/intsig/camscanner/question/nps/NPSActionClient;

    .line 35
    .line 36
    invoke-virtual {v0}, Lcom/intsig/camscanner/question/nps/NPSActionClient;->o〇0()Lcom/intsig/camscanner/question/mode/NPSActionDataGroup;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider$RandomNPSActionDataGroupCallback;->〇080(Lcom/intsig/camscanner/question/mode/NPSActionDataGroup;)V

    .line 41
    .line 42
    .line 43
    return v1

    .line 44
    :cond_1
    iget p1, p1, Landroid/os/Message;->what:I

    .line 45
    .line 46
    const/16 v0, 0x3e9

    .line 47
    .line 48
    const/4 v2, 0x1

    .line 49
    if-ne p1, v0, :cond_4

    .line 50
    .line 51
    invoke-direct {p0}, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->oO80()V

    .line 52
    .line 53
    .line 54
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇8O8〇008()Z

    .line 55
    .line 56
    .line 57
    move-result p1

    .line 58
    if-nez p1, :cond_2

    .line 59
    .line 60
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->O8o08O8O:Lcom/intsig/camscanner/question/nps/NPSActionClient;

    .line 61
    .line 62
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/question/nps/NPSActionClient;->〇〇808〇(Z)Z

    .line 63
    .line 64
    .line 65
    return v1

    .line 66
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->o0:Landroid/app/Activity;

    .line 67
    .line 68
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    invoke-static {p1}, Lcom/intsig/camscanner/question/nps/NPSDialogUtil;->〇o00〇〇Oo(Landroid/content/Context;)I

    .line 73
    .line 74
    .line 75
    move-result p1

    .line 76
    if-gtz p1, :cond_3

    .line 77
    .line 78
    return v1

    .line 79
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->O8o08O8O:Lcom/intsig/camscanner/question/nps/NPSActionClient;

    .line 80
    .line 81
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/question/nps/NPSActionClient;->〇〇808〇(Z)Z

    .line 82
    .line 83
    .line 84
    :cond_4
    return v2

    .line 85
    :cond_5
    :goto_0
    return v1
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private oO80()V
    .locals 7

    .line 1
    invoke-static {}, Lcom/intsig/utils/PreferenceUtil;->oO80()Lcom/intsig/utils/PreferenceUtil;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "key_has_print_temp_track"

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-virtual {v0, v1, v2}, Lcom/intsig/utils/PreferenceUtil;->Oo08(Ljava/lang/String;Z)Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 16
    .line 17
    .line 18
    move-result-wide v3

    .line 19
    const-wide v5, 0x17a5da60800L

    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    cmp-long v0, v3, v5

    .line 25
    .line 26
    if-lez v0, :cond_1

    .line 27
    .line 28
    return-void

    .line 29
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇0〇()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-nez v0, :cond_2

    .line 38
    .line 39
    return-void

    .line 40
    :cond_2
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    iget v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson;->nps_popup_style:I

    .line 45
    .line 46
    if-lez v0, :cond_3

    .line 47
    .line 48
    return-void

    .line 49
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->〇OOo8〇0:Landroid/content/Context;

    .line 50
    .line 51
    invoke-static {v0, v2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇〇888(Landroid/content/Context;Z)I

    .line 52
    .line 53
    .line 54
    move-result v0

    .line 55
    const/4 v3, 0x2

    .line 56
    if-ge v0, v3, :cond_4

    .line 57
    .line 58
    return-void

    .line 59
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->O8o08O8O:Lcom/intsig/camscanner/question/nps/NPSActionClient;

    .line 60
    .line 61
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/question/nps/NPSActionClient;->〇〇808〇(Z)Z

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    if-eqz v0, :cond_5

    .line 66
    .line 67
    const-string v0, "NPSDetectionLifecycleObserver"

    .line 68
    .line 69
    const-string v2, "printTrack find "

    .line 70
    .line 71
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    const-string v0, "CSNpsRequireRecord"

    .line 75
    .line 76
    const-string v2, "support"

    .line 77
    .line 78
    invoke-static {v0, v2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    invoke-static {}, Lcom/intsig/utils/PreferenceUtil;->oO80()Lcom/intsig/utils/PreferenceUtil;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    const/4 v2, 0x1

    .line 86
    invoke-virtual {v0, v1, v2}, Lcom/intsig/utils/PreferenceUtil;->〇O00(Ljava/lang/String;Z)V

    .line 87
    .line 88
    .line 89
    :cond_5
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private synthetic o〇0(Lcom/intsig/camscanner/question/mode/NPSActionDataGroup;Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider$RandomNPSActionDataGroupCallback;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->O8o08O8O:Lcom/intsig/camscanner/question/nps/NPSActionClient;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/question/nps/NPSActionClient;->o800o8O(Lcom/intsig/camscanner/question/mode/NPSActionDataGroup;)Lcom/intsig/camscanner/question/nps/NPSActionClient;

    .line 4
    .line 5
    .line 6
    if-eqz p2, :cond_0

    .line 7
    .line 8
    iget-boolean v0, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->〇080OO8〇0:Z

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-interface {p2, p1}, Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider$RandomNPSActionDataGroupCallback;->〇080(Lcom/intsig/camscanner/question/mode/NPSActionDataGroup;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;Landroid/app/Activity;Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider$RandomNPSActionDataGroupCallback;Lcom/intsig/camscanner/question/mode/NPSActionDataGroup;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->〇〇888(Landroid/app/Activity;Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider$RandomNPSActionDataGroupCallback;Lcom/intsig/camscanner/question/mode/NPSActionDataGroup;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;Lcom/intsig/camscanner/question/mode/NPSActionDataGroup;Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider$RandomNPSActionDataGroupCallback;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->o〇0(Lcom/intsig/camscanner/question/mode/NPSActionDataGroup;Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider$RandomNPSActionDataGroupCallback;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇o〇(Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;Landroid/os/Message;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->Oo08(Landroid/os/Message;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic 〇〇888(Landroid/app/Activity;Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider$RandomNPSActionDataGroupCallback;Lcom/intsig/camscanner/question/mode/NPSActionDataGroup;)V
    .locals 2

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_1

    .line 8
    .line 9
    if-nez p3, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 13
    .line 14
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 15
    .line 16
    .line 17
    const-string v1, " npsActionDataGroup="

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {p3}, Lcom/intsig/camscanner/question/mode/NPSActionDataGroup;->〇o〇()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    const-string v1, "NPSDetectionLifecycleObserver"

    .line 34
    .line 35
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    new-instance v0, LO088O/Oo08;

    .line 39
    .line 40
    invoke-direct {v0, p0, p3, p2}, LO088O/Oo08;-><init>(Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;Lcom/intsig/camscanner/question/mode/NPSActionDataGroup;Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider$RandomNPSActionDataGroupCallback;)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {p1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 44
    .line 45
    .line 46
    :cond_1
    :goto_0
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public synthetic onCreate(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Landroidx/lifecycle/〇o00〇〇Oo;->〇080(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onDestroy(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 2
    .param p1    # Landroidx/lifecycle/LifecycleOwner;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "NPSDetectionLifecycleObserver"

    .line 2
    .line 3
    const-string v1, "onDestroy"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-interface {p1}, Landroidx/lifecycle/LifecycleOwner;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    invoke-virtual {p1, p0}, Landroidx/lifecycle/Lifecycle;->removeObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 13
    .line 14
    .line 15
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->〇08O〇00〇o:Landroid/os/HandlerThread;

    .line 16
    .line 17
    const/4 v0, 0x0

    .line 18
    if-eqz p1, :cond_0

    .line 19
    .line 20
    invoke-virtual {p1}, Landroid/os/HandlerThread;->quitSafely()Z

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->〇08O〇00〇o:Landroid/os/HandlerThread;

    .line 24
    .line 25
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->o〇00O:Landroid/os/Handler;

    .line 26
    .line 27
    if-eqz p1, :cond_1

    .line 28
    .line 29
    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 30
    .line 31
    .line 32
    :cond_1
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onPause(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 1
    .param p1    # Landroidx/lifecycle/LifecycleOwner;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    new-instance p1, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v0, "onPause "

    .line 7
    .line 8
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->o0:Landroid/app/Activity;

    .line 12
    .line 13
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    const-string v0, "NPSDetectionLifecycleObserver"

    .line 21
    .line 22
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    const/4 p1, 0x0

    .line 26
    iput-boolean p1, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->〇080OO8〇0:Z

    .line 27
    .line 28
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->o〇00O:Landroid/os/Handler;

    .line 29
    .line 30
    if-eqz p1, :cond_0

    .line 31
    .line 32
    const/16 v0, 0x3e9

    .line 33
    .line 34
    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 35
    .line 36
    .line 37
    :cond_0
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onResume(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 3
    .param p1    # Landroidx/lifecycle/LifecycleOwner;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    new-instance p1, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v0, "onResume "

    .line 7
    .line 8
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->o0:Landroid/app/Activity;

    .line 12
    .line 13
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    const-string v0, "NPSDetectionLifecycleObserver"

    .line 21
    .line 22
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    const/4 p1, 0x1

    .line 26
    iput-boolean p1, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->〇080OO8〇0:Z

    .line 27
    .line 28
    invoke-direct {p0}, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->O8()V

    .line 29
    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->o〇00O:Landroid/os/Handler;

    .line 32
    .line 33
    const/16 v0, 0x3e9

    .line 34
    .line 35
    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeMessages(I)V

    .line 36
    .line 37
    .line 38
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->o〇00O:Landroid/os/Handler;

    .line 39
    .line 40
    const-wide/16 v1, 0x3e8

    .line 41
    .line 42
    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 43
    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onStart(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 1
    .param p1    # Landroidx/lifecycle/LifecycleOwner;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    new-instance p1, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v0, "onStart "

    .line 7
    .line 8
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->o0:Landroid/app/Activity;

    .line 12
    .line 13
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    const-string v0, "NPSDetectionLifecycleObserver"

    .line 21
    .line 22
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->O8o08O8O:Lcom/intsig/camscanner/question/nps/NPSActionClient;

    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->〇0O:Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider$RandomNPSActionDataGroupCallback;

    .line 28
    .line 29
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/question/nps/NPSActionClient;->〇o00〇〇Oo(Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider$RandomNPSActionDataGroupCallback;)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onStop(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 1
    .param p1    # Landroidx/lifecycle/LifecycleOwner;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    new-instance p1, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v0, "onStop "

    .line 7
    .line 8
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->o0:Landroid/app/Activity;

    .line 12
    .line 13
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    const-string v0, "NPSDetectionLifecycleObserver"

    .line 21
    .line 22
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    iget-object p1, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->O8o08O8O:Lcom/intsig/camscanner/question/nps/NPSActionClient;

    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;->〇0O:Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider$RandomNPSActionDataGroupCallback;

    .line 28
    .line 29
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/question/nps/NPSActionClient;->〇O00(Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider$RandomNPSActionDataGroupCallback;)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

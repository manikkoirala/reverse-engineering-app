.class public Lcom/intsig/camscanner/preference/CustomViewPreference;
.super Landroid/preference/Preference;
.source "CustomViewPreference.java"


# instance fields
.field private OO:Landroid/view/View;

.field private o0:Z

.field private o〇00O:Z

.field private 〇08O〇00〇o:Landroid/view/View;

.field private 〇OOo8〇0:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    .line 1
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/preference/CustomViewPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 2
    invoke-direct {p0, p1, p2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 3
    sget-object v0, Lcom/intsig/camscanner/R$styleable;->PreferenceAttrs:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    const/4 p2, 0x1

    .line 4
    invoke-virtual {p1, p2, p2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/intsig/camscanner/preference/CustomViewPreference;->o0:Z

    const/4 p2, 0x0

    .line 5
    invoke-virtual {p1, p2, p2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/intsig/camscanner/preference/CustomViewPreference;->o〇00O:Z

    .line 6
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private 〇o00〇〇Oo(Z)V
    .locals 1

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/preference/CustomViewPreference;->o〇00O:Z

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/preference/CustomViewPreference;->〇08O〇00〇o:Landroid/view/View;

    .line 4
    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    const/4 p1, 0x0

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/16 p1, 0x8

    .line 12
    .line 13
    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 14
    .line 15
    .line 16
    :cond_1
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method protected onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .line 1
    const v0, 0x7f0d06f7

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Landroid/preference/Preference;->setLayoutResource(I)V

    .line 5
    .line 6
    .line 7
    invoke-super {p0, p1}, Landroid/preference/Preference;->onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    iput-object p1, p0, Lcom/intsig/camscanner/preference/CustomViewPreference;->〇OOo8〇0:Landroid/view/View;

    .line 12
    .line 13
    const v0, 0x7f0a1a43

    .line 14
    .line 15
    .line 16
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    iput-object p1, p0, Lcom/intsig/camscanner/preference/CustomViewPreference;->OO:Landroid/view/View;

    .line 21
    .line 22
    iget-object p1, p0, Lcom/intsig/camscanner/preference/CustomViewPreference;->〇OOo8〇0:Landroid/view/View;

    .line 23
    .line 24
    const v0, 0x7f0a0abd

    .line 25
    .line 26
    .line 27
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    iput-object p1, p0, Lcom/intsig/camscanner/preference/CustomViewPreference;->〇08O〇00〇o:Landroid/view/View;

    .line 32
    .line 33
    iget-boolean p1, p0, Lcom/intsig/camscanner/preference/CustomViewPreference;->o0:Z

    .line 34
    .line 35
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/preference/CustomViewPreference;->〇080(Z)V

    .line 36
    .line 37
    .line 38
    iget-boolean p1, p0, Lcom/intsig/camscanner/preference/CustomViewPreference;->o〇00O:Z

    .line 39
    .line 40
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/preference/CustomViewPreference;->〇o00〇〇Oo(Z)V

    .line 41
    .line 42
    .line 43
    iget-object p1, p0, Lcom/intsig/camscanner/preference/CustomViewPreference;->〇OOo8〇0:Landroid/view/View;

    .line 44
    .line 45
    return-object p1
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇080(Z)V
    .locals 1

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/preference/CustomViewPreference;->o0:Z

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/preference/CustomViewPreference;->OO:Landroid/view/View;

    .line 4
    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    const/4 p1, 0x0

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/16 p1, 0x8

    .line 12
    .line 13
    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 14
    .line 15
    .line 16
    :cond_1
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.class public abstract Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;
.super Landroid/preference/CheckBoxPreference;
.source "AbstractSwitchCompatPreference.java"


# instance fields
.field private O8o08O8O:Landroid/view/View;

.field private OO:Landroidx/appcompat/widget/SwitchCompat;

.field private final o0:Ljava/lang/String;

.field private o〇00O:Landroid/view/View;

.field private 〇08O〇00〇o:Landroid/preference/Preference$OnPreferenceChangeListener;

.field private 〇OOo8〇0:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .line 9
    invoke-direct {p0, p1}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    const-string p1, "AbstractSwitchCompatPreference"

    .line 10
    iput-object p1, p0, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->o0:Ljava/lang/String;

    const/4 p1, 0x0

    .line 11
    iput-object p1, p0, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->〇08O〇00〇o:Landroid/preference/Preference$OnPreferenceChangeListener;

    .line 12
    iput-object p1, p0, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->o〇00O:Landroid/view/View;

    .line 13
    iput-object p1, p0, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->O8o08O8O:Landroid/view/View;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1, p2}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string v0, "AbstractSwitchCompatPreference"

    .line 2
    iput-object v0, p0, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->o0:Ljava/lang/String;

    const/4 v0, 0x0

    .line 3
    iput-object v0, p0, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->〇08O〇00〇o:Landroid/preference/Preference$OnPreferenceChangeListener;

    .line 4
    iput-object v0, p0, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->o〇00O:Landroid/view/View;

    .line 5
    iput-object v0, p0, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->O8o08O8O:Landroid/view/View;

    .line 6
    sget-object v0, Lcom/intsig/camscanner/R$styleable;->PreferenceAttrs:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object p1

    const/4 p2, 0x1

    .line 7
    invoke-virtual {p1, p2, p2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result p2

    iput-boolean p2, p0, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->〇OOo8〇0:Z

    .line 8
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;)Landroidx/appcompat/widget/SwitchCompat;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->OO:Landroidx/appcompat/widget/SwitchCompat;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method protected onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->〇o00〇〇Oo()Landroid/view/View;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iput-object p1, p0, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->o〇00O:Landroid/view/View;

    .line 6
    .line 7
    const v0, 0x1020001

    .line 8
    .line 9
    .line 10
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    check-cast p1, Landroidx/appcompat/widget/SwitchCompat;

    .line 15
    .line 16
    iput-object p1, p0, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->OO:Landroidx/appcompat/widget/SwitchCompat;

    .line 17
    .line 18
    const/4 v0, 0x0

    .line 19
    invoke-virtual {p1, v0}, Landroid/view/View;->setSelected(Z)V

    .line 20
    .line 21
    .line 22
    iget-object p1, p0, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->OO:Landroidx/appcompat/widget/SwitchCompat;

    .line 23
    .line 24
    invoke-virtual {p1, v0}, Landroid/view/View;->setClickable(Z)V

    .line 25
    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->OO:Landroidx/appcompat/widget/SwitchCompat;

    .line 28
    .line 29
    invoke-virtual {p1, v0}, Landroid/view/View;->setFocusable(Z)V

    .line 30
    .line 31
    .line 32
    iget-object p1, p0, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->OO:Landroidx/appcompat/widget/SwitchCompat;

    .line 33
    .line 34
    const/4 v1, 0x0

    .line 35
    invoke-virtual {p1, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p0, v0}, Landroid/preference/Preference;->getPersistedBoolean(Z)Z

    .line 39
    .line 40
    .line 41
    move-result p1

    .line 42
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    .line 47
    .line 48
    .line 49
    move-result p1

    .line 50
    if-eqz p1, :cond_0

    .line 51
    .line 52
    iget-object p1, p0, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->OO:Landroidx/appcompat/widget/SwitchCompat;

    .line 53
    .line 54
    const/4 v0, 0x1

    .line 55
    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/SwitchCompat;->setChecked(Z)V

    .line 56
    .line 57
    .line 58
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->o〇00O:Landroid/view/View;

    .line 59
    .line 60
    const v0, 0x7f0a1a43

    .line 61
    .line 62
    .line 63
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    iput-object p1, p0, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->O8o08O8O:Landroid/view/View;

    .line 68
    .line 69
    iget-boolean p1, p0, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->〇OOo8〇0:Z

    .line 70
    .line 71
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->〇o〇(Z)V

    .line 72
    .line 73
    .line 74
    iget-object p1, p0, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->〇08O〇00〇o:Landroid/preference/Preference$OnPreferenceChangeListener;

    .line 75
    .line 76
    if-nez p1, :cond_1

    .line 77
    .line 78
    new-instance p1, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference$1;

    .line 79
    .line 80
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference$1;-><init>(Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;)V

    .line 81
    .line 82
    .line 83
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 84
    .line 85
    .line 86
    goto :goto_0

    .line 87
    :cond_1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 88
    .line 89
    .line 90
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->o〇00O:Landroid/view/View;

    .line 91
    .line 92
    return-object p1
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->〇08O〇00〇o:Landroid/preference/Preference$OnPreferenceChangeListener;

    .line 2
    .line 3
    invoke-super {p0, p1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public abstract 〇o00〇〇Oo()Landroid/view/View;
.end method

.method public 〇o〇(Z)V
    .locals 1

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->〇OOo8〇0:Z

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/preference/AbstractSwitchCompatPreference;->O8o08O8O:Landroid/view/View;

    .line 4
    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    if-eqz p1, :cond_0

    .line 8
    .line 9
    const/4 p1, 0x0

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/16 p1, 0x8

    .line 12
    .line 13
    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 14
    .line 15
    .line 16
    :cond_1
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

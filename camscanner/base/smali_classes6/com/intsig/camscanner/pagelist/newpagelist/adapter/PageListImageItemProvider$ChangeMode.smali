.class public final Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$ChangeMode;
.super Ljava/lang/Object;
.source "PageListImageItemProvider.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ChangeMode"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private 〇080:Ljava/lang/String;

.field private 〇o00〇〇Oo:J

.field private 〇o〇:F


# direct methods
.method public constructor <init>()V
    .locals 7

    .line 1
    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x7

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$ChangeMode;-><init>(Ljava/lang/String;JFILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JF)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$ChangeMode;->〇080:Ljava/lang/String;

    .line 4
    iput-wide p2, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$ChangeMode;->〇o00〇〇Oo:J

    .line 5
    iput p4, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$ChangeMode;->〇o〇:F

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;JFILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p6, p5, 0x1

    if-eqz p6, :cond_0

    const/4 p1, 0x0

    :cond_0
    and-int/lit8 p6, p5, 0x2

    if-eqz p6, :cond_1

    const-wide/16 p2, -0x1

    :cond_1
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_2

    const/high16 p4, -0x40800000    # -1.0f

    .line 6
    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$ChangeMode;-><init>(Ljava/lang/String;JF)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 7

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p0, p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    instance-of v1, p1, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$ChangeMode;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    if-nez v1, :cond_1

    .line 9
    .line 10
    return v2

    .line 11
    :cond_1
    check-cast p1, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$ChangeMode;

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$ChangeMode;->〇080:Ljava/lang/String;

    .line 14
    .line 15
    iget-object v3, p1, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$ChangeMode;->〇080:Ljava/lang/String;

    .line 16
    .line 17
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-nez v1, :cond_2

    .line 22
    .line 23
    return v2

    .line 24
    :cond_2
    iget-wide v3, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$ChangeMode;->〇o00〇〇Oo:J

    .line 25
    .line 26
    iget-wide v5, p1, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$ChangeMode;->〇o00〇〇Oo:J

    .line 27
    .line 28
    cmp-long v1, v3, v5

    .line 29
    .line 30
    if-eqz v1, :cond_3

    .line 31
    .line 32
    return v2

    .line 33
    :cond_3
    iget v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$ChangeMode;->〇o〇:F

    .line 34
    .line 35
    iget p1, p1, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$ChangeMode;->〇o〇:F

    .line 36
    .line 37
    invoke-static {v1, p1}, Ljava/lang/Float;->compare(FF)I

    .line 38
    .line 39
    .line 40
    move-result p1

    .line 41
    if-eqz p1, :cond_4

    .line 42
    .line 43
    return v2

    .line 44
    :cond_4
    return v0
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public hashCode()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$ChangeMode;->〇080:Ljava/lang/String;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 12
    .line 13
    iget-wide v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$ChangeMode;->〇o00〇〇Oo:J

    .line 14
    .line 15
    invoke-static {v1, v2}, Landroidx/privacysandbox/ads/adservices/adselection/〇080;->〇080(J)I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    add-int/2addr v0, v1

    .line 20
    mul-int/lit8 v0, v0, 0x1f

    .line 21
    .line 22
    iget v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$ChangeMode;->〇o〇:F

    .line 23
    .line 24
    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    add-int/2addr v0, v1

    .line 29
    return v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public toString()Ljava/lang/String;
    .locals 6
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$ChangeMode;->〇080:Ljava/lang/String;

    .line 2
    .line 3
    iget-wide v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$ChangeMode;->〇o00〇〇Oo:J

    .line 4
    .line 5
    iget v3, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$ChangeMode;->〇o〇:F

    .line 6
    .line 7
    new-instance v4, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v5, "ChangeMode(tag="

    .line 13
    .line 14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    const-string v0, ", pageId="

    .line 21
    .line 22
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v4, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    const-string v0, ", progress="

    .line 29
    .line 30
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    const-string v0, ")"

    .line 37
    .line 38
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    return-object v0
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇080()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$ChangeMode;->〇080:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

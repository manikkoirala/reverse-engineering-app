.class public final enum Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;
.super Ljava/lang/Enum;
.source "FuncData.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

.field public static final enum ALL_ROUND:Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

.field private static final ARRAY_SIZE:I

.field public static final enum BOTTOM_ROUND:Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

.field public static final Companion:Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final DEFAULT_RADIUS:F

.field public static final enum NONE_ROUND:Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

.field private static final ROUND_RADIUS:F

.field public static final enum TOP_ROUND:Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;


# direct methods
.method private static final synthetic $values()[Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;
    .locals 3

    .line 1
    const/4 v0, 0x4

    .line 2
    new-array v0, v0, [Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    sget-object v2, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->TOP_ROUND:Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 6
    .line 7
    aput-object v2, v0, v1

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    sget-object v2, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->BOTTOM_ROUND:Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 11
    .line 12
    aput-object v2, v0, v1

    .line 13
    .line 14
    const/4 v1, 0x2

    .line 15
    sget-object v2, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->ALL_ROUND:Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 16
    .line 17
    aput-object v2, v0, v1

    .line 18
    .line 19
    const/4 v1, 0x3

    .line 20
    sget-object v2, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->NONE_ROUND:Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 21
    .line 22
    aput-object v2, v0, v1

    .line 23
    .line 24
    return-object v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 2
    .line 3
    const-string v1, "TOP_ROUND"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;-><init>(Ljava/lang/String;I)V

    .line 7
    .line 8
    .line 9
    sput-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->TOP_ROUND:Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 12
    .line 13
    const-string v1, "BOTTOM_ROUND"

    .line 14
    .line 15
    const/4 v2, 0x1

    .line 16
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;-><init>(Ljava/lang/String;I)V

    .line 17
    .line 18
    .line 19
    sput-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->BOTTOM_ROUND:Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 20
    .line 21
    new-instance v0, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 22
    .line 23
    const-string v1, "ALL_ROUND"

    .line 24
    .line 25
    const/4 v2, 0x2

    .line 26
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;-><init>(Ljava/lang/String;I)V

    .line 27
    .line 28
    .line 29
    sput-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->ALL_ROUND:Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 30
    .line 31
    new-instance v0, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 32
    .line 33
    const-string v1, "NONE_ROUND"

    .line 34
    .line 35
    const/4 v2, 0x3

    .line 36
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;-><init>(Ljava/lang/String;I)V

    .line 37
    .line 38
    .line 39
    sput-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->NONE_ROUND:Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 40
    .line 41
    invoke-static {}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->$values()[Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    sput-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->$VALUES:[Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 46
    .line 47
    new-instance v0, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;

    .line 48
    .line 49
    const/4 v1, 0x0

    .line 50
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 51
    .line 52
    .line 53
    sput-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->Companion:Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;

    .line 54
    .line 55
    const/16 v0, 0x8

    .line 56
    .line 57
    sput v0, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->ARRAY_SIZE:I

    .line 58
    .line 59
    const/high16 v0, 0x41000000    # 8.0f

    .line 60
    .line 61
    invoke-static {v0}, Lcom/intsig/utils/ext/FloatExtKt;->〇080(F)F

    .line 62
    .line 63
    .line 64
    move-result v0

    .line 65
    sput v0, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->ROUND_RADIUS:F

    .line 66
    .line 67
    return-void
    .line 68
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic access$getARRAY_SIZE$cp()I
    .locals 1

    .line 1
    sget v0, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->ARRAY_SIZE:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic access$getDEFAULT_RADIUS$cp()F
    .locals 1

    .line 1
    sget v0, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->DEFAULT_RADIUS:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic access$getROUND_RADIUS$cp()F
    .locals 1

    .line 1
    sget v0, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->ROUND_RADIUS:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final getRadius(Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;)[F
    .locals 1
    .param p0    # Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->Companion:Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;

    .line 2
    .line 3
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;->O8(Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;)[F

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    return-object p0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static values()[Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->$VALUES:[Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 2
    .line 3
    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

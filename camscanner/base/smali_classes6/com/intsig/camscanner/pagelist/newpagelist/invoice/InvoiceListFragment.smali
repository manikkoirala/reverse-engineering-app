.class public final Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;
.super Lcom/intsig/mvp/fragment/BaseChangeFragment;
.source "InvoiceListFragment.kt"

# interfaces
.implements Lcom/chad/library/adapter/base/listener/OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field static final synthetic ooo0〇〇O:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final 〇8〇oO〇〇8o:Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Ljava/lang/String;

.field private final OO:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO〇00〇8oO:I

.field private final o0:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o8〇OO0〇0o:Ljava/lang/String;

.field private oOo0:Z

.field private oOo〇8o008:Z

.field private o〇00O:J

.field private 〇080OO8〇0:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerFragment;

.field private 〇08O〇00〇o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇0O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "vb"

    .line 7
    .line 8
    const-string v3, "getVb()Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->ooo0〇〇O:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$Companion;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>()V
    .locals 10

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    const-class v0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;

    .line 5
    .line 6
    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->〇o00〇〇Oo(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    new-instance v1, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$special$$inlined$activityViewModels$default$1;

    .line 11
    .line 12
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$special$$inlined$activityViewModels$default$1;-><init>(Landroidx/fragment/app/Fragment;)V

    .line 13
    .line 14
    .line 15
    new-instance v2, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$special$$inlined$activityViewModels$default$2;

    .line 16
    .line 17
    const/4 v3, 0x0

    .line 18
    invoke-direct {v2, v3, p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$special$$inlined$activityViewModels$default$2;-><init>(Lkotlin/jvm/functions/Function0;Landroidx/fragment/app/Fragment;)V

    .line 19
    .line 20
    .line 21
    new-instance v4, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$special$$inlined$activityViewModels$default$3;

    .line 22
    .line 23
    invoke-direct {v4, p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$special$$inlined$activityViewModels$default$3;-><init>(Landroidx/fragment/app/Fragment;)V

    .line 24
    .line 25
    .line 26
    invoke-static {p0, v0, v1, v2, v4}, Landroidx/fragment/app/FragmentViewModelLazyKt;->createViewModelLazy(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->o0:Lkotlin/Lazy;

    .line 31
    .line 32
    const-class v0, Lcom/intsig/camscanner/capture/invoice/InvoiceViewModel;

    .line 33
    .line 34
    invoke-static {v0}, Lkotlin/jvm/internal/Reflection;->〇o00〇〇Oo(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    new-instance v1, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$special$$inlined$activityViewModels$default$4;

    .line 39
    .line 40
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$special$$inlined$activityViewModels$default$4;-><init>(Landroidx/fragment/app/Fragment;)V

    .line 41
    .line 42
    .line 43
    new-instance v2, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$special$$inlined$activityViewModels$default$5;

    .line 44
    .line 45
    invoke-direct {v2, v3, p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$special$$inlined$activityViewModels$default$5;-><init>(Lkotlin/jvm/functions/Function0;Landroidx/fragment/app/Fragment;)V

    .line 46
    .line 47
    .line 48
    new-instance v4, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$special$$inlined$activityViewModels$default$6;

    .line 49
    .line 50
    invoke-direct {v4, p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$special$$inlined$activityViewModels$default$6;-><init>(Landroidx/fragment/app/Fragment;)V

    .line 51
    .line 52
    .line 53
    invoke-static {p0, v0, v1, v2, v4}, Landroidx/fragment/app/FragmentViewModelLazyKt;->createViewModelLazy(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇OOo8〇0:Lkotlin/Lazy;

    .line 58
    .line 59
    new-instance v0, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 60
    .line 61
    const-class v5, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;

    .line 62
    .line 63
    const/4 v7, 0x0

    .line 64
    const/4 v8, 0x4

    .line 65
    const/4 v9, 0x0

    .line 66
    move-object v4, v0

    .line 67
    move-object v6, p0

    .line 68
    invoke-direct/range {v4 .. v9}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 69
    .line 70
    .line 71
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->OO:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 72
    .line 73
    new-instance v0, Ljava/util/ArrayList;

    .line 74
    .line 75
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 76
    .line 77
    .line 78
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇08O〇00〇o:Ljava/util/List;

    .line 79
    .line 80
    new-instance v0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$parentViewModel$2;

    .line 81
    .line 82
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$parentViewModel$2;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;)V

    .line 83
    .line 84
    .line 85
    sget-object v1, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 86
    .line 87
    new-instance v2, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$special$$inlined$viewModels$default$1;

    .line 88
    .line 89
    invoke-direct {v2, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$special$$inlined$viewModels$default$1;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 90
    .line 91
    .line 92
    invoke-static {v1, v2}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    const-class v1, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerViewModel;

    .line 97
    .line 98
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->〇o00〇〇Oo(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    .line 99
    .line 100
    .line 101
    move-result-object v1

    .line 102
    new-instance v2, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$special$$inlined$viewModels$default$2;

    .line 103
    .line 104
    invoke-direct {v2, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$special$$inlined$viewModels$default$2;-><init>(Lkotlin/Lazy;)V

    .line 105
    .line 106
    .line 107
    new-instance v4, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$special$$inlined$viewModels$default$3;

    .line 108
    .line 109
    invoke-direct {v4, v3, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$special$$inlined$viewModels$default$3;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/Lazy;)V

    .line 110
    .line 111
    .line 112
    new-instance v3, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$special$$inlined$viewModels$default$4;

    .line 113
    .line 114
    invoke-direct {v3, p0, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$special$$inlined$viewModels$default$4;-><init>(Landroidx/fragment/app/Fragment;Lkotlin/Lazy;)V

    .line 115
    .line 116
    .line 117
    invoke-static {p0, v1, v2, v4, v3}, Landroidx/fragment/app/FragmentViewModelLazyKt;->createViewModelLazy(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 118
    .line 119
    .line 120
    move-result-object v0

    .line 121
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇0O:Lkotlin/Lazy;

    .line 122
    .line 123
    return-void
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final O08〇(Lcom/intsig/camscanner/capture/invoice/InvoiceUIState;)V
    .locals 2

    .line 1
    instance-of v0, p1, Lcom/intsig/camscanner/capture/invoice/InvoiceUIState$RefreshInvoiceResult;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    new-instance v0, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v1, "handleInvoiceState state "

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    const-string v1, "InvoiceListFragment"

    .line 23
    .line 24
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    check-cast p1, Lcom/intsig/camscanner/capture/invoice/InvoiceUIState$RefreshInvoiceResult;

    .line 28
    .line 29
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/invoice/InvoiceUIState$RefreshInvoiceResult;->O8()Z

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    if-eqz v0, :cond_0

    .line 34
    .line 35
    const/4 p1, 0x1

    .line 36
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇oO〇08o(Z)V

    .line 37
    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/invoice/InvoiceUIState$RefreshInvoiceResult;->〇080()Ljava/util/List;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    check-cast v0, Ljava/util/Collection;

    .line 45
    .line 46
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->〇00O0O0(Ljava/util/Collection;)Ljava/util/List;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    invoke-static {v0}, Lcom/intsig/camscanner/capture/invoice/InvoiceUtils;->o〇0(Ljava/util/List;)V

    .line 51
    .line 52
    .line 53
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/invoice/InvoiceUIState$RefreshInvoiceResult;->〇080()Ljava/util/List;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O880O〇(Ljava/util/List;)V

    .line 58
    .line 59
    .line 60
    :cond_1
    :goto_0
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final O0O0〇()V
    .locals 7

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇08O〇00〇o:Ljava/util/List;

    .line 7
    .line 8
    check-cast v1, Ljava/lang/Iterable;

    .line 9
    .line 10
    new-instance v2, Ljava/util/ArrayList;

    .line 11
    .line 12
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 13
    .line 14
    .line 15
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 20
    .line 21
    .line 22
    move-result v3

    .line 23
    if-eqz v3, :cond_1

    .line 24
    .line 25
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    move-result-object v3

    .line 29
    move-object v4, v3

    .line 30
    check-cast v4, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;

    .line 31
    .line 32
    invoke-virtual {v4}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;->〇o00〇〇Oo()Z

    .line 33
    .line 34
    .line 35
    move-result v4

    .line 36
    if-eqz v4, :cond_0

    .line 37
    .line 38
    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    .line 43
    .line 44
    const/16 v3, 0xa

    .line 45
    .line 46
    invoke-static {v2, v3}, Lkotlin/collections/CollectionsKt;->〇0〇O0088o(Ljava/lang/Iterable;I)I

    .line 47
    .line 48
    .line 49
    move-result v4

    .line 50
    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 51
    .line 52
    .line 53
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 54
    .line 55
    .line 56
    move-result-object v2

    .line 57
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 58
    .line 59
    .line 60
    move-result v4

    .line 61
    if-eqz v4, :cond_2

    .line 62
    .line 63
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 64
    .line 65
    .line 66
    move-result-object v4

    .line 67
    check-cast v4, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;

    .line 68
    .line 69
    invoke-virtual {v4}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;->〇080()Lcom/intsig/camscanner/capture/invoice/data/Bills;

    .line 70
    .line 71
    .line 72
    move-result-object v4

    .line 73
    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 74
    .line 75
    .line 76
    goto :goto_1

    .line 77
    :cond_2
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 78
    .line 79
    .line 80
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 81
    .line 82
    const/16 v2, 0x18

    .line 83
    .line 84
    if-lt v1, v2, :cond_3

    .line 85
    .line 86
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇08O〇00〇o:Ljava/util/List;

    .line 87
    .line 88
    sget-object v2, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$deleteSelectItems$3;->o0:Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$deleteSelectItems$3;

    .line 89
    .line 90
    new-instance v4, L〇ooO〇000/〇o00〇〇Oo;

    .line 91
    .line 92
    invoke-direct {v4, v2}, L〇ooO〇000/〇o00〇〇Oo;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 93
    .line 94
    .line 95
    invoke-static {v1, v4}, L〇ooO〇000/〇080;->〇080(Ljava/util/List;Ljava/util/function/Predicate;)Z

    .line 96
    .line 97
    .line 98
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O〇8〇008()Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;

    .line 99
    .line 100
    .line 101
    move-result-object v1

    .line 102
    new-instance v2, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$DeleteInvoiceAction;

    .line 103
    .line 104
    iget-wide v4, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->o〇00O:J

    .line 105
    .line 106
    iget-object v6, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇08O〇00〇o:Ljava/util/List;

    .line 107
    .line 108
    invoke-interface {v6}, Ljava/util/List;->size()I

    .line 109
    .line 110
    .line 111
    move-result v6

    .line 112
    if-nez v6, :cond_4

    .line 113
    .line 114
    const/4 v6, 0x1

    .line 115
    goto :goto_2

    .line 116
    :cond_4
    const/4 v6, 0x0

    .line 117
    :goto_2
    invoke-direct {v2, v0, v4, v5, v6}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$DeleteInvoiceAction;-><init>(Ljava/util/List;JZ)V

    .line 118
    .line 119
    .line 120
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->O8ooOoo〇(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction;)V

    .line 121
    .line 122
    .line 123
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇08O〇00〇o:Ljava/util/List;

    .line 124
    .line 125
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 126
    .line 127
    .line 128
    move-result v0

    .line 129
    if-nez v0, :cond_5

    .line 130
    .line 131
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 132
    .line 133
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 134
    .line 135
    .line 136
    goto :goto_4

    .line 137
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇O0o〇〇o()V

    .line 138
    .line 139
    .line 140
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇08O〇00〇o:Ljava/util/List;

    .line 141
    .line 142
    check-cast v0, Ljava/lang/Iterable;

    .line 143
    .line 144
    new-instance v1, Ljava/util/ArrayList;

    .line 145
    .line 146
    invoke-static {v0, v3}, Lkotlin/collections/CollectionsKt;->〇0〇O0088o(Ljava/lang/Iterable;I)I

    .line 147
    .line 148
    .line 149
    move-result v2

    .line 150
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 151
    .line 152
    .line 153
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 154
    .line 155
    .line 156
    move-result-object v0

    .line 157
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 158
    .line 159
    .line 160
    move-result v2

    .line 161
    if-eqz v2, :cond_6

    .line 162
    .line 163
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 164
    .line 165
    .line 166
    move-result-object v2

    .line 167
    check-cast v2, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;

    .line 168
    .line 169
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;->〇080()Lcom/intsig/camscanner/capture/invoice/data/Bills;

    .line 170
    .line 171
    .line 172
    move-result-object v2

    .line 173
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 174
    .line 175
    .line 176
    goto :goto_3

    .line 177
    :cond_6
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O880O〇(Ljava/util/List;)V

    .line 178
    .line 179
    .line 180
    :goto_4
    return-void
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private final O0〇()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    iget-wide v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->o〇00O:J

    .line 4
    .line 5
    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-static {v0, v1}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇00(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->o8〇OO0〇0o:Ljava/lang/String;

    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇〇()Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;->〇OOo8〇0:Lcom/intsig/view/ImageTextButton;

    .line 22
    .line 23
    new-instance v2, L〇ooO〇000/〇o〇;

    .line 24
    .line 25
    invoke-direct {v2, p0}, L〇ooO〇000/〇o〇;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 29
    .line 30
    .line 31
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;->OO:Lcom/intsig/view/ImageTextButton;

    .line 32
    .line 33
    new-instance v2, L〇ooO〇000/O8;

    .line 34
    .line 35
    invoke-direct {v2, p0}, L〇ooO〇000/O8;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    .line 40
    .line 41
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;->〇08O〇00〇o:Lcom/intsig/view/ImageTextButton;

    .line 42
    .line 43
    new-instance v2, L〇ooO〇000/Oo08;

    .line 44
    .line 45
    invoke-direct {v2, p0}, L〇ooO〇000/Oo08;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    .line 50
    .line 51
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;->ooo0〇〇O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 52
    .line 53
    new-instance v2, L〇ooO〇000/o〇0;

    .line 54
    .line 55
    invoke-direct {v2, p0}, L〇ooO〇000/o〇0;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;)V

    .line 56
    .line 57
    .line 58
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    .line 60
    .line 61
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;->〇8〇oO〇〇8o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 62
    .line 63
    new-instance v2, L〇ooO〇000/〇〇888;

    .line 64
    .line 65
    invoke-direct {v2, p0}, L〇ooO〇000/〇〇888;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;)V

    .line 66
    .line 67
    .line 68
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    .line 70
    .line 71
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;->OO〇00〇8oO:Landroid/widget/RelativeLayout;

    .line 72
    .line 73
    new-instance v2, L〇ooO〇000/oO80;

    .line 74
    .line 75
    invoke-direct {v2, p0}, L〇ooO〇000/oO80;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;)V

    .line 76
    .line 77
    .line 78
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    .line 80
    .line 81
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;->O0O:Landroid/view/View;

    .line 82
    .line 83
    new-instance v2, L〇ooO〇000/〇80〇808〇O;

    .line 84
    .line 85
    invoke-direct {v2}, L〇ooO〇000/〇80〇808〇O;-><init>()V

    .line 86
    .line 87
    .line 88
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 89
    .line 90
    .line 91
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 92
    .line 93
    invoke-static {v1}, Lcom/bumptech/glide/Glide;->oo88o8O(Landroidx/fragment/app/FragmentActivity;)Lcom/bumptech/glide/RequestManager;

    .line 94
    .line 95
    .line 96
    move-result-object v1

    .line 97
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->o8〇OO0〇0o:Ljava/lang/String;

    .line 98
    .line 99
    invoke-virtual {v1, v2}, Lcom/bumptech/glide/RequestManager;->〇〇808〇(Ljava/lang/String;)Lcom/bumptech/glide/RequestBuilder;

    .line 100
    .line 101
    .line 102
    move-result-object v1

    .line 103
    invoke-virtual {v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->〇80〇808〇O()Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 104
    .line 105
    .line 106
    move-result-object v1

    .line 107
    check-cast v1, Lcom/bumptech/glide/RequestBuilder;

    .line 108
    .line 109
    new-instance v2, Ljp/wasabeef/glide/transformations/BlurTransformation;

    .line 110
    .line 111
    const/16 v3, 0x1e

    .line 112
    .line 113
    invoke-direct {v2, v3}, Ljp/wasabeef/glide/transformations/BlurTransformation;-><init>(I)V

    .line 114
    .line 115
    .line 116
    invoke-virtual {v1, v2}, Lcom/bumptech/glide/request/BaseRequestOptions;->O0O8OO088(Lcom/bumptech/glide/load/Transformation;)Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 117
    .line 118
    .line 119
    move-result-object v1

    .line 120
    check-cast v1, Lcom/bumptech/glide/RequestBuilder;

    .line 121
    .line 122
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 123
    .line 124
    invoke-virtual {v1, v0}, Lcom/bumptech/glide/RequestBuilder;->Oo〇o(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/ViewTarget;

    .line 125
    .line 126
    .line 127
    :cond_0
    return-void
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic O0〇0(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇oO88o(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O8()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇〇()Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;->OO〇00〇8oO:Landroid/widget/RelativeLayout;

    .line 8
    .line 9
    const-string v2, "rlFail"

    .line 10
    .line 11
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    const/4 v2, 0x0

    .line 15
    invoke-static {v1, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 16
    .line 17
    .line 18
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;->〇0O:Landroid/widget/LinearLayout;

    .line 19
    .line 20
    const-string v3, "llLoading"

    .line 21
    .line 22
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    const/4 v3, 0x1

    .line 26
    invoke-static {v1, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 27
    .line 28
    .line 29
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;->oOo0:Landroid/widget/RelativeLayout;

    .line 30
    .line 31
    const-string v1, "rlContent"

    .line 32
    .line 33
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    invoke-static {v0, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 37
    .line 38
    .line 39
    :cond_0
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final O880O〇(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/capture/invoice/data/Bills;",
            ">;)V"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇oO〇08o(Z)V

    .line 3
    .line 4
    .line 5
    new-instance v1, Ljava/util/ArrayList;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 8
    .line 9
    .line 10
    check-cast p1, Ljava/lang/Iterable;

    .line 11
    .line 12
    new-instance v2, Ljava/util/ArrayList;

    .line 13
    .line 14
    const/16 v3, 0xa

    .line 15
    .line 16
    invoke-static {p1, v3}, Lkotlin/collections/CollectionsKt;->〇0〇O0088o(Ljava/lang/Iterable;I)I

    .line 17
    .line 18
    .line 19
    move-result v3

    .line 20
    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 21
    .line 22
    .line 23
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 28
    .line 29
    .line 30
    move-result v3

    .line 31
    if-eqz v3, :cond_0

    .line 32
    .line 33
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object v3

    .line 37
    check-cast v3, Lcom/intsig/camscanner/capture/invoice/data/Bills;

    .line 38
    .line 39
    new-instance v4, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;

    .line 40
    .line 41
    invoke-direct {v4, v3, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;-><init>(Lcom/intsig/camscanner/capture/invoice/data/Bills;Z)V

    .line 42
    .line 43
    .line 44
    invoke-interface {v2, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 45
    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_0
    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 49
    .line 50
    .line 51
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇08O〇00〇o:Ljava/util/List;

    .line 52
    .line 53
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇〇()Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    if-eqz p1, :cond_1

    .line 58
    .line 59
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;->o8〇OO0〇0o:Landroidx/recyclerview/widget/RecyclerView;

    .line 60
    .line 61
    if-eqz p1, :cond_1

    .line 62
    .line 63
    new-instance v0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAdapter;

    .line 64
    .line 65
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O〇8〇008()Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;

    .line 66
    .line 67
    .line 68
    move-result-object v1

    .line 69
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAdapter;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;)V

    .line 70
    .line 71
    .line 72
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇08O〇00〇o:Ljava/util/List;

    .line 73
    .line 74
    check-cast v1, Ljava/util/Collection;

    .line 75
    .line 76
    invoke-virtual {v0, v1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇o(Ljava/util/Collection;)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {v0, p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O〇Oooo〇〇(Lcom/chad/library/adapter/base/listener/OnItemLongClickListener;)V

    .line 80
    .line 81
    .line 82
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 83
    .line 84
    .line 85
    :cond_1
    const/4 p1, 0x1

    .line 86
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇ooO8Ooo〇(Z)V

    .line 87
    .line 88
    .line 89
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final O8O(JLjava/util/List;Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/capture/invoice/data/Bills;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/bankcardjournal/dialog/ShareBankCardJournalDialog;->〇0O:Lcom/intsig/camscanner/bankcardjournal/dialog/ShareBankCardJournalDialog$Companion;

    .line 2
    .line 3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const-string v2, "childFragmentManager"

    .line 8
    .line 9
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    new-instance v2, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$showExportDialog$1;

    .line 13
    .line 14
    move-object v3, v2

    .line 15
    move-object v4, p0

    .line 16
    move-object v5, p3

    .line 17
    move-wide v6, p1

    .line 18
    move-object v8, p4

    .line 19
    invoke-direct/range {v3 .. v8}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$showExportDialog$1;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Ljava/util/List;JLjava/util/List;)V

    .line 20
    .line 21
    .line 22
    const/4 p1, 0x1

    .line 23
    const/4 p2, 0x0

    .line 24
    invoke-virtual {v0, v1, p2, v2, p1}, Lcom/intsig/camscanner/bankcardjournal/dialog/ShareBankCardJournalDialog$Companion;->〇〇888(Landroidx/fragment/app/FragmentManager;ZLcom/intsig/camscanner/bankcardjournal/dialog/ShareBankCardJournalDialogListener;Z)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final O8〇8〇O80()V
    .locals 9

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->oOo〇8o008:Z

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    new-array v0, v1, [Ljava/lang/Object;

    .line 7
    .line 8
    iget v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->OO〇00〇8oO:I

    .line 9
    .line 10
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    const/4 v2, 0x0

    .line 15
    aput-object v1, v0, v2

    .line 16
    .line 17
    const v1, 0x7f130164

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0, v1, v0}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    const-string v1, "getString(R.string.a_lab\u2026selected, \"$selectCount\")"

    .line 25
    .line 26
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇080OO8〇0:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerFragment;

    .line 30
    .line 31
    if-eqz v1, :cond_1

    .line 32
    .line 33
    invoke-virtual {v1, v0, v2}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerFragment;->o0〇〇00〇o(Ljava/lang/String;Z)V

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    if-nez v0, :cond_1

    .line 38
    .line 39
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 40
    .line 41
    .line 42
    move-result-object v3

    .line 43
    const/4 v4, 0x0

    .line 44
    const/4 v5, 0x0

    .line 45
    new-instance v6, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$checkRefreshTitle$1;

    .line 46
    .line 47
    const/4 v0, 0x0

    .line 48
    invoke-direct {v6, p0, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$checkRefreshTitle$1;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Lkotlin/coroutines/Continuation;)V

    .line 49
    .line 50
    .line 51
    const/4 v7, 0x3

    .line 52
    const/4 v8, 0x0

    .line 53
    invoke-static/range {v3 .. v8}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 54
    .line 55
    .line 56
    :cond_1
    :goto_0
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final OO0O(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Landroid/view/View;)V
    .locals 3

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O〇8〇008()Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    new-instance v0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$ExportInvoiceAction;

    .line 11
    .line 12
    iget-wide v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->o〇00O:J

    .line 13
    .line 14
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇08O〇00〇o:Ljava/util/List;

    .line 15
    .line 16
    invoke-direct {v0, v1, v2, p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$ExportInvoiceAction;-><init>(JLjava/util/List;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->O8ooOoo〇(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction;)V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final OO〇〇o0oO()V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NotifyDataSetChanged"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇〇()Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;->o8〇OO0〇0o:Landroidx/recyclerview/widget/RecyclerView;

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    move-object v0, v1

    .line 18
    :goto_0
    instance-of v2, v0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAdapter;

    .line 19
    .line 20
    if-eqz v2, :cond_1

    .line 21
    .line 22
    move-object v1, v0

    .line 23
    check-cast v1, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAdapter;

    .line 24
    .line 25
    :cond_1
    if-eqz v1, :cond_3

    .line 26
    .line 27
    invoke-virtual {v1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O8〇o()Ljava/util/List;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    check-cast v0, Ljava/lang/Iterable;

    .line 32
    .line 33
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 38
    .line 39
    .line 40
    move-result v2

    .line 41
    const/4 v3, 0x1

    .line 42
    if-eqz v2, :cond_2

    .line 43
    .line 44
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 45
    .line 46
    .line 47
    move-result-object v2

    .line 48
    check-cast v2, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;

    .line 49
    .line 50
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;->〇o〇(Z)V

    .line 51
    .line 52
    .line 53
    goto :goto_1

    .line 54
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O〇8〇008()Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    new-instance v2, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$UpdateTitleAction;

    .line 59
    .line 60
    const/4 v4, 0x0

    .line 61
    invoke-direct {v2, v4, v3, v4}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$UpdateTitleAction;-><init>(ZZZ)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->O8ooOoo〇(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction;)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 68
    .line 69
    .line 70
    :cond_3
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic Ooo8o(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;)Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerFragment;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇080OO8〇0:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerFragment;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final OooO〇(I)V
    .locals 4

    .line 1
    const v0, 0x7f0a084c

    .line 2
    .line 3
    .line 4
    const-string v1, "InvoiceListFragment"

    .line 5
    .line 6
    const/4 v2, 0x1

    .line 7
    if-eq p1, v0, :cond_3

    .line 8
    .line 9
    const v0, 0x7f0a177e

    .line 10
    .line 11
    .line 12
    if-eq p1, v0, :cond_0

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const-string p1, "click select all or unselect all"

    .line 16
    .line 17
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    iget-boolean p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->oOo0:Z

    .line 21
    .line 22
    xor-int/2addr p1, v2

    .line 23
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->oOo0:Z

    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇080OO8〇0:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerFragment;

    .line 26
    .line 27
    if-eqz v0, :cond_1

    .line 28
    .line 29
    xor-int/2addr p1, v2

    .line 30
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerFragment;->O08〇oO8〇(Z)V

    .line 31
    .line 32
    .line 33
    :cond_1
    iget-boolean p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->oOo0:Z

    .line 34
    .line 35
    if-eqz p1, :cond_2

    .line 36
    .line 37
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->OO〇〇o0oO()V

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇ooO〇000()V

    .line 42
    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_3
    const-string p1, "enter edit mode"

    .line 46
    .line 47
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    const-string p1, "view_type"

    .line 51
    .line 52
    const-string v0, "invoice"

    .line 53
    .line 54
    const-string v1, "CSList"

    .line 55
    .line 56
    const-string v3, "edit"

    .line 57
    .line 58
    invoke-static {v1, v3, p1, v0}, Lcom/intsig/log/LogAgentHelper;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    const/4 p1, -0x1

    .line 62
    iput p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->OO〇00〇8oO:I

    .line 63
    .line 64
    const/4 p1, 0x0

    .line 65
    const/4 v0, 0x0

    .line 66
    invoke-static {p0, p1, v2, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇08O(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;IILjava/lang/Object;)V

    .line 67
    .line 68
    .line 69
    :goto_0
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private static final O〇080〇o0(Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p0, "InvoiceListFragment"

    .line 2
    .line 3
    const-string v0, "click view mask"

    .line 4
    .line 5
    invoke-static {p0, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final O〇0O〇Oo〇o(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Landroid/view/View;)V
    .locals 3

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "view_type"

    .line 7
    .line 8
    const-string v0, "invoice"

    .line 9
    .line 10
    const-string v1, "CSList"

    .line 11
    .line 12
    const-string v2, "add"

    .line 13
    .line 14
    invoke-static {v1, v2, p1, v0}, Lcom/intsig/log/LogAgentHelper;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇080OO8〇0:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerFragment;

    .line 18
    .line 19
    if-eqz p0, :cond_0

    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerFragment;->O8〇o0〇〇8()V

    .line 22
    .line 23
    .line 24
    :cond_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O〇8〇008()Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->o0:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final O〇〇O80o8(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    const-string v0, "invoice"

    .line 11
    .line 12
    invoke-static {v0}, Lcom/intsig/utils/WebUrlUtils;->〇〇888(Ljava/lang/String;)Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-static {p1, v0}, Lcom/intsig/webview/util/WebUtil;->〇8o8o〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇O0o〇〇o()V

    .line 20
    .line 21
    .line 22
    :cond_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o00〇88〇08(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O〇〇O80o8(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final o0Oo(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Landroid/view/View;)V
    .locals 5

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇08O〇00〇o:Ljava/util/List;

    .line 7
    .line 8
    check-cast p1, Ljava/lang/Iterable;

    .line 9
    .line 10
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    const/4 v1, 0x1

    .line 19
    if-eqz v0, :cond_1

    .line 20
    .line 21
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    move-object v2, v0

    .line 26
    check-cast v2, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;

    .line 27
    .line 28
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;->〇o00〇〇Oo()Z

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    xor-int/2addr v2, v1

    .line 33
    if-eqz v2, :cond_0

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_1
    const/4 v0, 0x0

    .line 37
    :goto_0
    if-nez v0, :cond_2

    .line 38
    .line 39
    const-string p1, "select_all"

    .line 40
    .line 41
    goto :goto_1

    .line 42
    :cond_2
    const-string p1, "not_select_all"

    .line 43
    .line 44
    :goto_1
    const/4 v0, 0x2

    .line 45
    new-array v0, v0, [Landroid/util/Pair;

    .line 46
    .line 47
    new-instance v2, Landroid/util/Pair;

    .line 48
    .line 49
    const-string v3, "view_type"

    .line 50
    .line 51
    const-string v4, "invoice"

    .line 52
    .line 53
    invoke-direct {v2, v3, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 54
    .line 55
    .line 56
    const/4 v3, 0x0

    .line 57
    aput-object v2, v0, v3

    .line 58
    .line 59
    new-instance v2, Landroid/util/Pair;

    .line 60
    .line 61
    const-string v3, "type"

    .line 62
    .line 63
    invoke-direct {v2, v3, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 64
    .line 65
    .line 66
    aput-object v2, v0, v1

    .line 67
    .line 68
    const-string p1, "CSList"

    .line 69
    .line 70
    const-string v1, "delete"

    .line 71
    .line 72
    invoke-static {p1, v1, v0}, Lcom/intsig/log/LogAgentHelper;->〇O8o08O(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 73
    .line 74
    .line 75
    new-instance p1, Lcom/intsig/app/AlertDialog$Builder;

    .line 76
    .line 77
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 78
    .line 79
    .line 80
    move-result-object v0

    .line 81
    invoke-direct {p1, v0}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 82
    .line 83
    .line 84
    const v0, 0x7f130ffe

    .line 85
    .line 86
    .line 87
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    invoke-virtual {p1, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇〇〇0〇〇0(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 92
    .line 93
    .line 94
    move-result-object p1

    .line 95
    const v0, 0x7f13176b

    .line 96
    .line 97
    .line 98
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 99
    .line 100
    .line 101
    move-result-object v0

    .line 102
    const v1, 0x7f060207

    .line 103
    .line 104
    .line 105
    invoke-virtual {p1, v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇〇8O0〇8(Ljava/lang/CharSequence;I)Lcom/intsig/app/AlertDialog$Builder;

    .line 106
    .line 107
    .line 108
    move-result-object p1

    .line 109
    const v0, 0x7f1316a3

    .line 110
    .line 111
    .line 112
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 113
    .line 114
    .line 115
    move-result-object v0

    .line 116
    new-instance v2, L〇ooO〇000/OO0o〇〇〇〇0;

    .line 117
    .line 118
    invoke-direct {v2}, L〇ooO〇000/OO0o〇〇〇〇0;-><init>()V

    .line 119
    .line 120
    .line 121
    invoke-virtual {p1, v0, v1, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇O888o0o(Ljava/lang/String;ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 122
    .line 123
    .line 124
    move-result-object p1

    .line 125
    const v0, 0x7f131769

    .line 126
    .line 127
    .line 128
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 129
    .line 130
    .line 131
    move-result-object v0

    .line 132
    new-instance v1, L〇ooO〇000/〇8o8o〇;

    .line 133
    .line 134
    invoke-direct {v1, p0}, L〇ooO〇000/〇8o8o〇;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;)V

    .line 135
    .line 136
    .line 137
    const p0, 0x7f0601f2

    .line 138
    .line 139
    .line 140
    invoke-virtual {p1, v0, p0, v1}, Lcom/intsig/app/AlertDialog$Builder;->OOO〇O0(Ljava/lang/String;ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 141
    .line 142
    .line 143
    move-result-object p0

    .line 144
    invoke-virtual {p0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 145
    .line 146
    .line 147
    move-result-object p0

    .line 148
    invoke-virtual {p0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 149
    .line 150
    .line 151
    return-void
    .line 152
    .line 153
    .line 154
.end method

.method private final o0〇〇00(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState;)V
    .locals 9

    .line 1
    instance-of v0, p1, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$RefreshInvoiceListState;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/4 v2, 0x1

    .line 5
    if-eqz v0, :cond_3

    .line 6
    .line 7
    check-cast p1, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$RefreshInvoiceListState;

    .line 8
    .line 9
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$RefreshInvoiceListState;->〇080()Ljava/util/List;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, Ljava/util/Collection;

    .line 14
    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-eqz v0, :cond_1

    .line 22
    .line 23
    :cond_0
    const/4 v1, 0x1

    .line 24
    :cond_1
    if-eqz v1, :cond_2

    .line 25
    .line 26
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->o88()Lcom/intsig/camscanner/capture/invoice/InvoiceViewModel;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    new-instance v0, Lcom/intsig/camscanner/capture/invoice/InvoiceAction$RequestInvoice;

    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇O8〇8O0oO()Ljava/util/List;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->o〇oo()Ljava/util/ArrayList;

    .line 37
    .line 38
    .line 39
    move-result-object v2

    .line 40
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/capture/invoice/InvoiceAction$RequestInvoice;-><init>(Ljava/util/List;Ljava/util/List;)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/capture/invoice/InvoiceViewModel;->O8〇o(Lcom/intsig/camscanner/capture/invoice/InvoiceAction;)V

    .line 44
    .line 45
    .line 46
    goto/16 :goto_b

    .line 47
    .line 48
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$RefreshInvoiceListState;->〇080()Ljava/util/List;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O880O〇(Ljava/util/List;)V

    .line 53
    .line 54
    .line 55
    goto/16 :goto_b

    .line 56
    .line 57
    :cond_3
    instance-of v0, p1, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$UpdateTitle;

    .line 58
    .line 59
    if-eqz v0, :cond_c

    .line 60
    .line 61
    check-cast p1, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$UpdateTitle;

    .line 62
    .line 63
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$UpdateTitle;->〇080()Z

    .line 64
    .line 65
    .line 66
    move-result v0

    .line 67
    if-eqz v0, :cond_4

    .line 68
    .line 69
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇08O〇00〇o:Ljava/util/List;

    .line 70
    .line 71
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 72
    .line 73
    .line 74
    move-result p1

    .line 75
    iput p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->OO〇00〇8oO:I

    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_4
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$UpdateTitle;->〇o00〇〇Oo()Z

    .line 79
    .line 80
    .line 81
    move-result v0

    .line 82
    if-eqz v0, :cond_5

    .line 83
    .line 84
    iput v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->OO〇00〇8oO:I

    .line 85
    .line 86
    goto :goto_0

    .line 87
    :cond_5
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$UpdateTitle;->〇o〇()Z

    .line 88
    .line 89
    .line 90
    move-result p1

    .line 91
    if-eqz p1, :cond_6

    .line 92
    .line 93
    iget p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->OO〇00〇8oO:I

    .line 94
    .line 95
    add-int/2addr p1, v2

    .line 96
    iput p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->OO〇00〇8oO:I

    .line 97
    .line 98
    goto :goto_0

    .line 99
    :cond_6
    iget p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->OO〇00〇8oO:I

    .line 100
    .line 101
    sub-int/2addr p1, v2

    .line 102
    iput p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->OO〇00〇8oO:I

    .line 103
    .line 104
    :goto_0
    iget p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->OO〇00〇8oO:I

    .line 105
    .line 106
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇08O〇00〇o:Ljava/util/List;

    .line 107
    .line 108
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 109
    .line 110
    .line 111
    move-result v0

    .line 112
    if-ne p1, v0, :cond_7

    .line 113
    .line 114
    const/4 p1, 0x1

    .line 115
    goto :goto_1

    .line 116
    :cond_7
    const/4 p1, 0x0

    .line 117
    :goto_1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->oOo0:Z

    .line 118
    .line 119
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇080OO8〇0:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerFragment;

    .line 120
    .line 121
    if-eqz p1, :cond_9

    .line 122
    .line 123
    iget v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->OO〇00〇8oO:I

    .line 124
    .line 125
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇08O〇00〇o:Ljava/util/List;

    .line 126
    .line 127
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 128
    .line 129
    .line 130
    move-result v3

    .line 131
    if-eq v0, v3, :cond_8

    .line 132
    .line 133
    const/4 v0, 0x1

    .line 134
    goto :goto_2

    .line 135
    :cond_8
    const/4 v0, 0x0

    .line 136
    :goto_2
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerFragment;->O08〇oO8〇(Z)V

    .line 137
    .line 138
    .line 139
    :cond_9
    iget p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->OO〇00〇8oO:I

    .line 140
    .line 141
    new-instance v0, Ljava/lang/StringBuilder;

    .line 142
    .line 143
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 144
    .line 145
    .line 146
    const-string v3, "UpdateTitle selectCount "

    .line 147
    .line 148
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    .line 150
    .line 151
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 152
    .line 153
    .line 154
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 155
    .line 156
    .line 157
    move-result-object p1

    .line 158
    const-string v0, "InvoiceListFragment"

    .line 159
    .line 160
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    .line 162
    .line 163
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇〇()Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;

    .line 164
    .line 165
    .line 166
    move-result-object p1

    .line 167
    if-eqz p1, :cond_b

    .line 168
    .line 169
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;->O0O:Landroid/view/View;

    .line 170
    .line 171
    if-eqz p1, :cond_b

    .line 172
    .line 173
    iget v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->OO〇00〇8oO:I

    .line 174
    .line 175
    if-nez v0, :cond_a

    .line 176
    .line 177
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->oOo〇8o008:Z

    .line 178
    .line 179
    if-eqz v0, :cond_a

    .line 180
    .line 181
    goto :goto_3

    .line 182
    :cond_a
    const/4 v2, 0x0

    .line 183
    :goto_3
    invoke-static {p1, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 184
    .line 185
    .line 186
    :cond_b
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O8〇8〇O80()V

    .line 187
    .line 188
    .line 189
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇ooO8Ooo〇(Z)V

    .line 190
    .line 191
    .line 192
    goto/16 :goto_b

    .line 193
    .line 194
    :cond_c
    instance-of v0, p1, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$ExportInvoiceState;

    .line 195
    .line 196
    if-eqz v0, :cond_17

    .line 197
    .line 198
    new-instance v0, Ljava/util/ArrayList;

    .line 199
    .line 200
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 201
    .line 202
    .line 203
    new-instance v3, Ljava/util/ArrayList;

    .line 204
    .line 205
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 206
    .line 207
    .line 208
    iget-boolean v4, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->oOo〇8o008:Z

    .line 209
    .line 210
    const/16 v5, 0xa

    .line 211
    .line 212
    if-eqz v4, :cond_12

    .line 213
    .line 214
    move-object v4, p1

    .line 215
    check-cast v4, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$ExportInvoiceState;

    .line 216
    .line 217
    invoke-virtual {v4}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$ExportInvoiceState;->〇o00〇〇Oo()Ljava/util/List;

    .line 218
    .line 219
    .line 220
    move-result-object v4

    .line 221
    check-cast v4, Ljava/lang/Iterable;

    .line 222
    .line 223
    new-instance v6, Ljava/util/ArrayList;

    .line 224
    .line 225
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 226
    .line 227
    .line 228
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 229
    .line 230
    .line 231
    move-result-object v4

    .line 232
    :cond_d
    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    .line 233
    .line 234
    .line 235
    move-result v7

    .line 236
    if-eqz v7, :cond_e

    .line 237
    .line 238
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 239
    .line 240
    .line 241
    move-result-object v7

    .line 242
    move-object v8, v7

    .line 243
    check-cast v8, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;

    .line 244
    .line 245
    invoke-virtual {v8}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;->〇o00〇〇Oo()Z

    .line 246
    .line 247
    .line 248
    move-result v8

    .line 249
    if-eqz v8, :cond_d

    .line 250
    .line 251
    invoke-interface {v6, v7}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 252
    .line 253
    .line 254
    goto :goto_4

    .line 255
    :cond_e
    new-instance v4, Ljava/util/ArrayList;

    .line 256
    .line 257
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 258
    .line 259
    .line 260
    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 261
    .line 262
    .line 263
    move-result-object v7

    .line 264
    :cond_f
    :goto_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    .line 265
    .line 266
    .line 267
    move-result v8

    .line 268
    if-eqz v8, :cond_10

    .line 269
    .line 270
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 271
    .line 272
    .line 273
    move-result-object v8

    .line 274
    check-cast v8, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;

    .line 275
    .line 276
    invoke-virtual {v8}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;->〇080()Lcom/intsig/camscanner/capture/invoice/data/Bills;

    .line 277
    .line 278
    .line 279
    move-result-object v8

    .line 280
    invoke-virtual {v8}, Lcom/intsig/camscanner/capture/invoice/data/Bills;->getPageSyncId()Ljava/lang/String;

    .line 281
    .line 282
    .line 283
    move-result-object v8

    .line 284
    if-eqz v8, :cond_f

    .line 285
    .line 286
    invoke-interface {v4, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 287
    .line 288
    .line 289
    goto :goto_5

    .line 290
    :cond_10
    invoke-interface {v0, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 291
    .line 292
    .line 293
    new-instance v4, Ljava/util/ArrayList;

    .line 294
    .line 295
    invoke-static {v6, v5}, Lkotlin/collections/CollectionsKt;->〇0〇O0088o(Ljava/lang/Iterable;I)I

    .line 296
    .line 297
    .line 298
    move-result v5

    .line 299
    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 300
    .line 301
    .line 302
    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 303
    .line 304
    .line 305
    move-result-object v5

    .line 306
    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    .line 307
    .line 308
    .line 309
    move-result v6

    .line 310
    if-eqz v6, :cond_11

    .line 311
    .line 312
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 313
    .line 314
    .line 315
    move-result-object v6

    .line 316
    check-cast v6, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;

    .line 317
    .line 318
    invoke-virtual {v6}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;->〇080()Lcom/intsig/camscanner/capture/invoice/data/Bills;

    .line 319
    .line 320
    .line 321
    move-result-object v6

    .line 322
    invoke-interface {v4, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 323
    .line 324
    .line 325
    goto :goto_6

    .line 326
    :cond_11
    invoke-interface {v3, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 327
    .line 328
    .line 329
    goto :goto_9

    .line 330
    :cond_12
    move-object v4, p1

    .line 331
    check-cast v4, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$ExportInvoiceState;

    .line 332
    .line 333
    invoke-virtual {v4}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$ExportInvoiceState;->〇o00〇〇Oo()Ljava/util/List;

    .line 334
    .line 335
    .line 336
    move-result-object v6

    .line 337
    check-cast v6, Ljava/lang/Iterable;

    .line 338
    .line 339
    new-instance v7, Ljava/util/ArrayList;

    .line 340
    .line 341
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 342
    .line 343
    .line 344
    invoke-interface {v6}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 345
    .line 346
    .line 347
    move-result-object v6

    .line 348
    :cond_13
    :goto_7
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    .line 349
    .line 350
    .line 351
    move-result v8

    .line 352
    if-eqz v8, :cond_14

    .line 353
    .line 354
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 355
    .line 356
    .line 357
    move-result-object v8

    .line 358
    check-cast v8, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;

    .line 359
    .line 360
    invoke-virtual {v8}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;->〇080()Lcom/intsig/camscanner/capture/invoice/data/Bills;

    .line 361
    .line 362
    .line 363
    move-result-object v8

    .line 364
    invoke-virtual {v8}, Lcom/intsig/camscanner/capture/invoice/data/Bills;->getPageSyncId()Ljava/lang/String;

    .line 365
    .line 366
    .line 367
    move-result-object v8

    .line 368
    if-eqz v8, :cond_13

    .line 369
    .line 370
    invoke-interface {v7, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 371
    .line 372
    .line 373
    goto :goto_7

    .line 374
    :cond_14
    invoke-interface {v0, v7}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 375
    .line 376
    .line 377
    invoke-virtual {v4}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$ExportInvoiceState;->〇o00〇〇Oo()Ljava/util/List;

    .line 378
    .line 379
    .line 380
    move-result-object v4

    .line 381
    check-cast v4, Ljava/lang/Iterable;

    .line 382
    .line 383
    new-instance v6, Ljava/util/ArrayList;

    .line 384
    .line 385
    invoke-static {v4, v5}, Lkotlin/collections/CollectionsKt;->〇0〇O0088o(Ljava/lang/Iterable;I)I

    .line 386
    .line 387
    .line 388
    move-result v5

    .line 389
    invoke-direct {v6, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 390
    .line 391
    .line 392
    invoke-interface {v4}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 393
    .line 394
    .line 395
    move-result-object v4

    .line 396
    :goto_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    .line 397
    .line 398
    .line 399
    move-result v5

    .line 400
    if-eqz v5, :cond_15

    .line 401
    .line 402
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 403
    .line 404
    .line 405
    move-result-object v5

    .line 406
    check-cast v5, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;

    .line 407
    .line 408
    invoke-virtual {v5}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;->〇080()Lcom/intsig/camscanner/capture/invoice/data/Bills;

    .line 409
    .line 410
    .line 411
    move-result-object v5

    .line 412
    invoke-interface {v6, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 413
    .line 414
    .line 415
    goto :goto_8

    .line 416
    :cond_15
    invoke-interface {v3, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 417
    .line 418
    .line 419
    :goto_9
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 420
    .line 421
    .line 422
    move-result v4

    .line 423
    check-cast p1, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$ExportInvoiceState;

    .line 424
    .line 425
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$ExportInvoiceState;->〇o00〇〇Oo()Ljava/util/List;

    .line 426
    .line 427
    .line 428
    move-result-object v5

    .line 429
    invoke-interface {v5}, Ljava/util/List;->size()I

    .line 430
    .line 431
    .line 432
    move-result v5

    .line 433
    if-ne v4, v5, :cond_16

    .line 434
    .line 435
    const-string v4, "select_all"

    .line 436
    .line 437
    goto :goto_a

    .line 438
    :cond_16
    const-string v4, "not_select_all"

    .line 439
    .line 440
    :goto_a
    const/4 v5, 0x2

    .line 441
    new-array v5, v5, [Landroid/util/Pair;

    .line 442
    .line 443
    new-instance v6, Landroid/util/Pair;

    .line 444
    .line 445
    const-string v7, "view_type"

    .line 446
    .line 447
    const-string v8, "invoice"

    .line 448
    .line 449
    invoke-direct {v6, v7, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 450
    .line 451
    .line 452
    aput-object v6, v5, v1

    .line 453
    .line 454
    new-instance v1, Landroid/util/Pair;

    .line 455
    .line 456
    const-string v6, "type"

    .line 457
    .line 458
    invoke-direct {v1, v6, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 459
    .line 460
    .line 461
    aput-object v1, v5, v2

    .line 462
    .line 463
    const-string v1, "CSList"

    .line 464
    .line 465
    const-string v2, "export"

    .line 466
    .line 467
    invoke-static {v1, v2, v5}, Lcom/intsig/log/LogAgentHelper;->〇O8o08O(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 468
    .line 469
    .line 470
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$ExportInvoiceState;->〇080()J

    .line 471
    .line 472
    .line 473
    move-result-wide v1

    .line 474
    invoke-direct {p0, v1, v2, v0, v3}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O8O(JLjava/util/List;Ljava/util/List;)V

    .line 475
    .line 476
    .line 477
    goto :goto_b

    .line 478
    :cond_17
    instance-of v0, p1, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$Go2DetailState;

    .line 479
    .line 480
    if-eqz v0, :cond_18

    .line 481
    .line 482
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 483
    .line 484
    const-string v1, "mActivity"

    .line 485
    .line 486
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 487
    .line 488
    .line 489
    check-cast p1, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$Go2DetailState;

    .line 490
    .line 491
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$Go2DetailState;->〇o00〇〇Oo()Lcom/intsig/camscanner/capture/invoice/data/BillsOcrData;

    .line 492
    .line 493
    .line 494
    move-result-object v1

    .line 495
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$Go2DetailState;->〇080()Lcom/intsig/camscanner/capture/invoice/data/InvoiceResultBundle;

    .line 496
    .line 497
    .line 498
    move-result-object v2

    .line 499
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$Go2DetailState;->〇o〇()I

    .line 500
    .line 501
    .line 502
    move-result p1

    .line 503
    invoke-static {v0, v1, v2, p1}, Lcom/intsig/camscanner/capture/invoice/InvoiceUtils;->oO80(Landroid/app/Activity;Lcom/intsig/camscanner/capture/invoice/data/BillsOcrData;Lcom/intsig/camscanner/capture/invoice/data/InvoiceResultBundle;I)V

    .line 504
    .line 505
    .line 506
    :cond_18
    :goto_b
    return-void
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
.end method

.method private static final o808o8o08(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Landroid/content/DialogInterface;I)V
    .locals 2

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "from_part"

    .line 7
    .line 8
    const-string p2, "cs_list"

    .line 9
    .line 10
    const-string v0, "CSInvoiceDeletePop"

    .line 11
    .line 12
    const-string v1, "delete"

    .line 13
    .line 14
    invoke-static {v0, v1, p1, p2}, Lcom/intsig/log/LogAgentHelper;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O0O0〇()V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final o88()Lcom/intsig/camscanner/capture/invoice/InvoiceViewModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇OOo8〇0:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/capture/invoice/InvoiceViewModel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic o880(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->o〇08oO80o(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic oOoO8OO〇(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;)Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O〇8〇008()Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic oO〇oo(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O〇0O〇Oo〇o(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oooO888(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->o0Oo(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final o〇08oO80o(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 p1, 0x1

    .line 7
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇oO〇08o(Z)V

    .line 8
    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->o88()Lcom/intsig/camscanner/capture/invoice/InvoiceViewModel;

    .line 11
    .line 12
    .line 13
    move-result-object p0

    .line 14
    sget-object p1, Lcom/intsig/camscanner/capture/invoice/InvoiceAction$StopRequest;->〇080:Lcom/intsig/camscanner/capture/invoice/InvoiceAction$StopRequest;

    .line 15
    .line 16
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/capture/invoice/InvoiceViewModel;->O8〇o(Lcom/intsig/camscanner/capture/invoice/InvoiceAction;)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o〇0〇o(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O〇080〇o0(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o〇O8OO(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O8o08O8O:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final o〇o08〇(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Landroid/view/View;)V
    .locals 3

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "CSList"

    .line 7
    .line 8
    const-string v0, "click_recognize"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogAgentHelper;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O8()V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O〇8〇008()Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    new-instance v0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$RequestInvoiceListAction;

    .line 21
    .line 22
    iget-wide v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->o〇00O:J

    .line 23
    .line 24
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 25
    .line 26
    .line 27
    move-result-object p0

    .line 28
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$RequestInvoiceListAction;-><init>(Ljava/lang/Long;)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->O8ooOoo〇(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction;)V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final o〇oo()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    iget-wide v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->o〇00O:J

    .line 4
    .line 5
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/db/dao/ImageDao;->OO8oO0o〇(Landroid/content/Context;J)Ljava/util/ArrayList;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇088O(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)Z
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇Oo〇O(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static synthetic 〇08O(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;IILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p2, p2, 0x1

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    const/4 p1, -0x1

    .line 6
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇8O0880(I)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static final synthetic 〇0oO〇oo00(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->OooO〇(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇0ooOOo(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇0〇0(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;)Lcom/intsig/camscanner/capture/invoice/InvoiceViewModel;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->o88()Lcom/intsig/camscanner/capture/invoice/InvoiceViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇8O0880(I)V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NotifyDataSetChanged"
        }
    .end annotation

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->oOo〇8o008:Z

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇080OO8〇0:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerFragment;

    .line 5
    .line 6
    const/4 v2, 0x0

    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    invoke-virtual {v1, v0, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerFragment;->〇8oo〇〇oO(ZZ)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerFragment;->〇8〇〇8o(Z)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerFragment;->〇o〇OO80oO(Z)V

    .line 16
    .line 17
    .line 18
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇〇()Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    if-eqz v1, :cond_2

    .line 23
    .line 24
    iget-object v3, v1, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;->〇08O〇00〇o:Lcom/intsig/view/ImageTextButton;

    .line 25
    .line 26
    const-string v4, "itbDelete"

    .line 27
    .line 28
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-static {v3, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 32
    .line 33
    .line 34
    iget-object v3, v1, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;->〇OOo8〇0:Lcom/intsig/view/ImageTextButton;

    .line 35
    .line 36
    const-string v4, "itbAdd"

    .line 37
    .line 38
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    invoke-static {v3, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 42
    .line 43
    .line 44
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;->o8〇OO0〇0o:Landroidx/recyclerview/widget/RecyclerView;

    .line 45
    .line 46
    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    const-string v3, "null cannot be cast to non-null type com.intsig.camscanner.pagelist.newpagelist.invoice.InvoiceListAdapter"

    .line 51
    .line 52
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    check-cast v1, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAdapter;

    .line 56
    .line 57
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAdapter;->o8O0(Z)V

    .line 58
    .line 59
    .line 60
    const/4 v3, -0x1

    .line 61
    if-eq p1, v3, :cond_1

    .line 62
    .line 63
    invoke-virtual {v1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O8〇o()Ljava/util/List;

    .line 64
    .line 65
    .line 66
    move-result-object v3

    .line 67
    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 68
    .line 69
    .line 70
    move-result-object p1

    .line 71
    check-cast p1, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;

    .line 72
    .line 73
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;->〇o〇(Z)V

    .line 74
    .line 75
    .line 76
    :cond_1
    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 77
    .line 78
    .line 79
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O〇8〇008()Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;

    .line 80
    .line 81
    .line 82
    move-result-object p1

    .line 83
    new-instance v1, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$UpdateTitleAction;

    .line 84
    .line 85
    invoke-direct {v1, v0, v2, v2}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$UpdateTitleAction;-><init>(ZZZ)V

    .line 86
    .line 87
    .line 88
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->O8ooOoo〇(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction;)V

    .line 89
    .line 90
    .line 91
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇〇〇O〇()Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerViewModel;

    .line 92
    .line 93
    .line 94
    move-result-object p1

    .line 95
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerViewModel;->〇8o8o〇()Landroidx/lifecycle/MutableLiveData;

    .line 96
    .line 97
    .line 98
    move-result-object p1

    .line 99
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 100
    .line 101
    invoke-virtual {p1, v0}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 102
    .line 103
    .line 104
    return-void
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static final synthetic 〇8〇80o(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->o〇00O:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->o808o8o08(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇O0o〇〇o()V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NotifyDataSetChanged"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->oOo〇8o008:Z

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇080OO8〇0:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerFragment;

    .line 5
    .line 6
    const/4 v2, 0x1

    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    invoke-virtual {v1, v0, v2}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerFragment;->〇8oo〇〇oO(ZZ)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerFragment;->〇8〇〇8o(Z)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerFragment;->〇o〇OO80oO(Z)V

    .line 16
    .line 17
    .line 18
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇〇()Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    if-eqz v1, :cond_1

    .line 23
    .line 24
    iget-object v3, v1, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;->〇08O〇00〇o:Lcom/intsig/view/ImageTextButton;

    .line 25
    .line 26
    const-string v4, "itbDelete"

    .line 27
    .line 28
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-static {v3, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 32
    .line 33
    .line 34
    iget-object v3, v1, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;->〇OOo8〇0:Lcom/intsig/view/ImageTextButton;

    .line 35
    .line 36
    const-string v4, "itbAdd"

    .line 37
    .line 38
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    invoke-static {v3, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 42
    .line 43
    .line 44
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;->o8〇OO0〇0o:Landroidx/recyclerview/widget/RecyclerView;

    .line 45
    .line 46
    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    const-string v3, "null cannot be cast to non-null type com.intsig.camscanner.pagelist.newpagelist.invoice.InvoiceListAdapter"

    .line 51
    .line 52
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    check-cast v1, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAdapter;

    .line 56
    .line 57
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAdapter;->o8O0(Z)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 61
    .line 62
    .line 63
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇ooO〇000()V

    .line 64
    .line 65
    .line 66
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O8〇8〇O80()V

    .line 67
    .line 68
    .line 69
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇ooO8Ooo〇(Z)V

    .line 70
    .line 71
    .line 72
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇〇〇O〇()Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerViewModel;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerViewModel;->〇8o8o〇()Landroidx/lifecycle/MutableLiveData;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 81
    .line 82
    invoke-virtual {v0, v1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 83
    .line 84
    .line 85
    return-void
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->OO0O(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇O8〇8000(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O8o08O8O:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇O8〇8O0oO()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    iget-wide v2, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->o〇00O:J

    .line 9
    .line 10
    const/4 v4, 0x0

    .line 11
    invoke-static {v1, v2, v3, v4}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇O〇80o08O(Landroid/content/Context;JLjava/lang/String;)Ljava/util/ArrayList;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 16
    .line 17
    .line 18
    return-object v0
    .line 19
    .line 20
    .line 21
.end method

.method private static final 〇Oo〇O(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)Z
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    move-result-object p0

    .line 10
    check-cast p0, Ljava/lang/Boolean;

    .line 11
    .line 12
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 13
    .line 14
    .line 15
    move-result p0

    .line 16
    return p0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇o08(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;)Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerViewModel;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇〇〇O〇()Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇oO88o(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-interface {p0}, Landroid/content/DialogInterface;->dismiss()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇oO〇08o(Z)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇〇()Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;->OO〇00〇8oO:Landroid/widget/RelativeLayout;

    .line 8
    .line 9
    const-string v2, "rlFail"

    .line 10
    .line 11
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    invoke-static {v1, p1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 15
    .line 16
    .line 17
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;->〇0O:Landroid/widget/LinearLayout;

    .line 18
    .line 19
    const-string v2, "llLoading"

    .line 20
    .line 21
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    const/4 v2, 0x0

    .line 25
    invoke-static {v1, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 26
    .line 27
    .line 28
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;->oOo0:Landroid/widget/RelativeLayout;

    .line 29
    .line 30
    const-string v1, "rlContent"

    .line 31
    .line 32
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    xor-int/lit8 p1, p1, 0x1

    .line 36
    .line 37
    invoke-static {v0, p1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 38
    .line 39
    .line 40
    :cond_0
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇ooO8Ooo〇(Z)V
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetTextI18n"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇08O〇00〇o:Ljava/util/List;

    .line 2
    .line 3
    check-cast v0, Ljava/lang/Iterable;

    .line 4
    .line 5
    new-instance v1, Ljava/util/ArrayList;

    .line 6
    .line 7
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 8
    .line 9
    .line 10
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    const/4 v3, 0x1

    .line 19
    const/4 v4, 0x0

    .line 20
    if-eqz v2, :cond_5

    .line 21
    .line 22
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    move-object v5, v2

    .line 27
    check-cast v5, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;

    .line 28
    .line 29
    invoke-virtual {v5}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;->〇o00〇〇Oo()Z

    .line 30
    .line 31
    .line 32
    move-result v6

    .line 33
    if-nez v6, :cond_1

    .line 34
    .line 35
    if-eqz p1, :cond_4

    .line 36
    .line 37
    :cond_1
    invoke-virtual {v5}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;->〇080()Lcom/intsig/camscanner/capture/invoice/data/Bills;

    .line 38
    .line 39
    .line 40
    move-result-object v5

    .line 41
    invoke-virtual {v5}, Lcom/intsig/camscanner/capture/invoice/data/Bills;->getInvoiceTaxRate()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v5

    .line 45
    if-eqz v5, :cond_3

    .line 46
    .line 47
    invoke-interface {v5}, Ljava/lang/CharSequence;->length()I

    .line 48
    .line 49
    .line 50
    move-result v5

    .line 51
    if-lez v5, :cond_2

    .line 52
    .line 53
    const/4 v5, 0x1

    .line 54
    goto :goto_1

    .line 55
    :cond_2
    const/4 v5, 0x0

    .line 56
    :goto_1
    if-ne v5, v3, :cond_3

    .line 57
    .line 58
    const/4 v5, 0x1

    .line 59
    goto :goto_2

    .line 60
    :cond_3
    const/4 v5, 0x0

    .line 61
    :goto_2
    if-eqz v5, :cond_4

    .line 62
    .line 63
    goto :goto_3

    .line 64
    :cond_4
    const/4 v3, 0x0

    .line 65
    :goto_3
    if-eqz v3, :cond_0

    .line 66
    .line 67
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 68
    .line 69
    .line 70
    goto :goto_0

    .line 71
    :cond_5
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 72
    .line 73
    .line 74
    move-result-object p1

    .line 75
    const-wide/16 v0, 0x0

    .line 76
    .line 77
    move-wide v5, v0

    .line 78
    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 79
    .line 80
    .line 81
    move-result v2

    .line 82
    if-eqz v2, :cond_7

    .line 83
    .line 84
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 85
    .line 86
    .line 87
    move-result-object v2

    .line 88
    check-cast v2, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;

    .line 89
    .line 90
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;->〇080()Lcom/intsig/camscanner/capture/invoice/data/Bills;

    .line 91
    .line 92
    .line 93
    move-result-object v2

    .line 94
    invoke-virtual {v2}, Lcom/intsig/camscanner/capture/invoice/data/Bills;->getInvoiceTaxRate()Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object v2

    .line 98
    if-eqz v2, :cond_6

    .line 99
    .line 100
    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    .line 101
    .line 102
    .line 103
    move-result-wide v7

    .line 104
    goto :goto_5

    .line 105
    :cond_6
    move-wide v7, v0

    .line 106
    :goto_5
    add-double/2addr v5, v7

    .line 107
    goto :goto_4

    .line 108
    :cond_7
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇〇()Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;

    .line 109
    .line 110
    .line 111
    move-result-object p1

    .line 112
    if-eqz p1, :cond_8

    .line 113
    .line 114
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;->〇〇08O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 115
    .line 116
    goto :goto_6

    .line 117
    :cond_8
    const/4 p1, 0x0

    .line 118
    :goto_6
    if-nez p1, :cond_9

    .line 119
    .line 120
    goto :goto_7

    .line 121
    :cond_9
    const v0, 0x7f13107c

    .line 122
    .line 123
    .line 124
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 125
    .line 126
    .line 127
    move-result-object v0

    .line 128
    sget-object v1, Lkotlin/jvm/internal/StringCompanionObject;->〇080:Lkotlin/jvm/internal/StringCompanionObject;

    .line 129
    .line 130
    new-array v1, v3, [Ljava/lang/Object;

    .line 131
    .line 132
    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    .line 133
    .line 134
    .line 135
    move-result-object v2

    .line 136
    aput-object v2, v1, v4

    .line 137
    .line 138
    invoke-static {v1, v3}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    .line 139
    .line 140
    .line 141
    move-result-object v1

    .line 142
    const-string v2, "%.2f"

    .line 143
    .line 144
    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 145
    .line 146
    .line 147
    move-result-object v1

    .line 148
    const-string v2, "format(format, *args)"

    .line 149
    .line 150
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    .line 152
    .line 153
    new-instance v2, Ljava/lang/StringBuilder;

    .line 154
    .line 155
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 156
    .line 157
    .line 158
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    .line 160
    .line 161
    const-string v0, ": \uffe5 "

    .line 162
    .line 163
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    .line 165
    .line 166
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    .line 168
    .line 169
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 170
    .line 171
    .line 172
    move-result-object v0

    .line 173
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    .line 175
    .line 176
    :goto_7
    return-void
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private final 〇ooO〇000()V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NotifyDataSetChanged"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇〇()Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;->o8〇OO0〇0o:Landroidx/recyclerview/widget/RecyclerView;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 v0, 0x0

    .line 17
    :goto_0
    const-string v1, "null cannot be cast to non-null type com.intsig.camscanner.pagelist.newpagelist.invoice.InvoiceListAdapter"

    .line 18
    .line 19
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    check-cast v0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAdapter;

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O8〇o()Ljava/util/List;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    check-cast v1, Ljava/lang/Iterable;

    .line 29
    .line 30
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 35
    .line 36
    .line 37
    move-result v2

    .line 38
    const/4 v3, 0x0

    .line 39
    if-eqz v2, :cond_1

    .line 40
    .line 41
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 42
    .line 43
    .line 44
    move-result-object v2

    .line 45
    check-cast v2, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;

    .line 46
    .line 47
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;->〇o〇(Z)V

    .line 48
    .line 49
    .line 50
    goto :goto_1

    .line 51
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O〇8〇008()Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    new-instance v2, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$UpdateTitleAction;

    .line 56
    .line 57
    const/4 v4, 0x1

    .line 58
    invoke-direct {v2, v3, v3, v4}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$UpdateTitleAction;-><init>(ZZZ)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->O8ooOoo〇(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction;)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 65
    .line 66
    .line 67
    return-void
    .line 68
.end method

.method public static final synthetic 〇o〇88〇8(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇O0o〇〇o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇()Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->OO:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->ooo0〇〇O:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/FragmentInvoiceListBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇〇O80〇0o(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->oOo〇8o008:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇〇o0〇8(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->o〇o08〇(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇〇〇0(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->o0〇〇00(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇〇〇00(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Lcom/intsig/camscanner/capture/invoice/InvoiceUIState;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O08〇(Lcom/intsig/camscanner/capture/invoice/InvoiceUIState;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇〇〇O〇()Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerViewModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇0O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerViewModel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public O0O8OO088(Lcom/chad/library/adapter/base/BaseQuickAdapter;Landroid/view/View;I)Z
    .locals 1
    .param p1    # Lcom/chad/library/adapter/base/BaseQuickAdapter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chad/library/adapter/base/BaseQuickAdapter<",
            "**>;",
            "Landroid/view/View;",
            "I)Z"
        }
    .end annotation

    .line 1
    const-string v0, "adapter"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "view"

    .line 7
    .line 8
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    iput p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->OO〇00〇8oO:I

    .line 13
    .line 14
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇8O0880(I)V

    .line 15
    .line 16
    .line 17
    const/4 p1, 0x1

    .line 18
    return p1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public addEvents()V
    .locals 13

    .line 1
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x0

    .line 7
    new-instance v3, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$addEvents$1;

    .line 8
    .line 9
    const/4 v6, 0x0

    .line 10
    invoke-direct {v3, p0, v6}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$addEvents$1;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Lkotlin/coroutines/Continuation;)V

    .line 11
    .line 12
    .line 13
    const/4 v4, 0x3

    .line 14
    const/4 v5, 0x0

    .line 15
    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 16
    .line 17
    .line 18
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 19
    .line 20
    .line 21
    move-result-object v7

    .line 22
    const/4 v8, 0x0

    .line 23
    const/4 v9, 0x0

    .line 24
    new-instance v10, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$addEvents$2;

    .line 25
    .line 26
    invoke-direct {v10, p0, v6}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$addEvents$2;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Lkotlin/coroutines/Continuation;)V

    .line 27
    .line 28
    .line 29
    const/4 v11, 0x3

    .line 30
    const/4 v12, 0x0

    .line 31
    invoke-static/range {v7 .. v12}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 32
    .line 33
    .line 34
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    new-instance v3, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$addEvents$3;

    .line 39
    .line 40
    invoke-direct {v3, p0, v6}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment$addEvents$3;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;Lkotlin/coroutines/Continuation;)V

    .line 41
    .line 42
    .line 43
    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 44
    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public getIntentData(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->getIntentData(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    const-string v0, "invoice_doc_id"

    .line 7
    .line 8
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getLong(Ljava/lang/String;)J

    .line 9
    .line 10
    .line 11
    move-result-wide v0

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const-wide/16 v0, 0x0

    .line 14
    .line 15
    :goto_0
    iput-wide v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->o〇00O:J

    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O〇8〇008()Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    iget-wide v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->o〇00O:J

    .line 22
    .line 23
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->〇o〇(J)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O8()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getParentFragment()Landroidx/fragment/app/Fragment;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    instance-of v0, p1, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerFragment;

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    check-cast p1, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerFragment;

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 p1, 0x0

    .line 16
    :goto_0
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇080OO8〇0:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerFragment;

    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->addEvents()V

    .line 19
    .line 20
    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O0〇()V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public interceptBackPressed()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->oOo〇8o008:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->〇O0o〇〇o()V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x1

    .line 9
    return v0

    .line 10
    :cond_0
    invoke-super {p0}, Lcom/intsig/fragmentBackHandler/BackHandledFragment;->interceptBackPressed()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .line 1
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    const/16 v0, 0x2712

    .line 5
    .line 6
    if-ne p1, v0, :cond_1

    .line 7
    .line 8
    const/4 p1, -0x1

    .line 9
    if-ne p2, p1, :cond_1

    .line 10
    .line 11
    const/4 p1, 0x1

    .line 12
    const/4 p2, 0x0

    .line 13
    if-eqz p3, :cond_0

    .line 14
    .line 15
    const-string v0, "result_code_delete_doc"

    .line 16
    .line 17
    invoke-virtual {p3, v0, p2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 18
    .line 19
    .line 20
    move-result p3

    .line 21
    if-ne p3, p1, :cond_0

    .line 22
    .line 23
    const/4 p2, 0x1

    .line 24
    :cond_0
    if-eqz p2, :cond_1

    .line 25
    .line 26
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O〇8〇008()Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;

    .line 27
    .line 28
    .line 29
    move-result-object p2

    .line 30
    new-instance p3, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$DeleteInvoiceAction;

    .line 31
    .line 32
    new-instance v0, Ljava/util/ArrayList;

    .line 33
    .line 34
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 35
    .line 36
    .line 37
    iget-wide v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->o〇00O:J

    .line 38
    .line 39
    invoke-direct {p3, v0, v1, v2, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$DeleteInvoiceAction;-><init>(Ljava/util/List;JZ)V

    .line 40
    .line 41
    .line 42
    invoke-virtual {p2, p3}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->O8ooOoo〇(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction;)V

    .line 43
    .line 44
    .line 45
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 46
    .line 47
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 48
    .line 49
    .line 50
    :cond_1
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public onResume()V
    .locals 4

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->O〇8〇008()Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    new-instance v1, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$RequestInvoiceListAction;

    .line 9
    .line 10
    iget-wide v2, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListFragment;->o〇00O:J

    .line 11
    .line 12
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 13
    .line 14
    .line 15
    move-result-object v2

    .line 16
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$RequestInvoiceListAction;-><init>(Ljava/lang/Long;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->O8ooOoo〇(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction;)V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d02f2

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

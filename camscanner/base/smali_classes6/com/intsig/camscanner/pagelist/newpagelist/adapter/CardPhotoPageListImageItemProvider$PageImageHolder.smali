.class public final Lcom/intsig/camscanner/pagelist/newpagelist/adapter/CardPhotoPageListImageItemProvider$PageImageHolder;
.super Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
.source "CardPhotoPageListImageItemProvider.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pagelist/newpagelist/adapter/CardPhotoPageListImageItemProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "PageImageHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final OO:Landroid/widget/TextView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o0:Landroidx/constraintlayout/widget/ConstraintLayout;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field final synthetic 〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/newpagelist/adapter/CardPhotoPageListImageItemProvider;

.field private final 〇OOo8〇0:Landroid/widget/ImageView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/pagelist/newpagelist/adapter/CardPhotoPageListImageItemProvider;Landroid/view/View;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/pagelist/newpagelist/adapter/CardPhotoPageListImageItemProvider;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 1
    const-string v0, "convertView"

    .line 2
    .line 3
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/CardPhotoPageListImageItemProvider$PageImageHolder;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/newpagelist/adapter/CardPhotoPageListImageItemProvider;

    .line 7
    .line 8
    invoke-direct {p0, p2}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;-><init>(Landroid/view/View;)V

    .line 9
    .line 10
    .line 11
    invoke-static {p2}, Lcom/intsig/camscanner/databinding/ItemCardPhotoPageListImageListBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ItemCardPhotoPageListImageListBinding;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    const-string p2, "bind(convertView)"

    .line 16
    .line 17
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemCardPhotoPageListImageListBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 21
    .line 22
    const-string v0, "binding.clCardPhoto"

    .line 23
    .line 24
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    iput-object p2, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/CardPhotoPageListImageItemProvider$PageImageHolder;->o0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 28
    .line 29
    iget-object p2, p1, Lcom/intsig/camscanner/databinding/ItemCardPhotoPageListImageListBinding;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 30
    .line 31
    const-string v0, "binding.tvPageTag"

    .line 32
    .line 33
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    iput-object p2, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/CardPhotoPageListImageItemProvider$PageImageHolder;->OO:Landroid/widget/TextView;

    .line 37
    .line 38
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemCardPhotoPageListImageListBinding;->OO:Landroid/widget/ImageView;

    .line 39
    .line 40
    const-string p2, "binding.ivCardPhoto"

    .line 41
    .line 42
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/CardPhotoPageListImageItemProvider$PageImageHolder;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method


# virtual methods
.method public final 〇00()Landroid/widget/ImageView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/CardPhotoPageListImageItemProvider$PageImageHolder;->〇OOo8〇0:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

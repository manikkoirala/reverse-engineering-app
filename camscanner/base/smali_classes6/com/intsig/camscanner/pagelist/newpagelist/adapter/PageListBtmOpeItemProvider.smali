.class public final Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;
.super Lcom/chad/library/adapter/base/provider/BaseItemProvider;
.source "PageListBtmOpeItemProvider.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider$BtmOpeViewHolder;,
        Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/provider/BaseItemProvider<",
        "Lcom/intsig/camscanner/pagelist/model/PageTypeItem;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080OO8〇0:Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇0O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Z

.field private final o〇00O:Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;->〇080OO8〇0:Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider$Companion;

    .line 8
    .line 9
    const-class v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "PageListBtmOpeItemProvider::class.java.simpleName"

    .line 16
    .line 17
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;->〇0O:Ljava/lang/String;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>(Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mAdapter"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;->o〇00O:Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;

    .line 10
    .line 11
    const/4 p1, 0x1

    .line 12
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;->O8o08O8O:Z

    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O8ooOoo〇(I)Z
    .locals 2

    .line 1
    const/16 v0, 0xbb8

    .line 2
    .line 3
    if-eq p1, v0, :cond_1

    .line 4
    .line 5
    const/4 v1, 0x2

    .line 6
    if-eq p1, v1, :cond_1

    .line 7
    .line 8
    const/4 v1, 0x3

    .line 9
    if-eq p1, v1, :cond_1

    .line 10
    .line 11
    const/16 v1, 0x73

    .line 12
    .line 13
    if-eq p1, v1, :cond_1

    .line 14
    .line 15
    const/16 v1, 0xbba

    .line 16
    .line 17
    if-eq p1, v1, :cond_1

    .line 18
    .line 19
    const/4 v1, 0x5

    .line 20
    if-eq p1, v1, :cond_1

    .line 21
    .line 22
    if-eq p1, v0, :cond_1

    .line 23
    .line 24
    const/4 v1, 0x4

    .line 25
    if-eq p1, v1, :cond_1

    .line 26
    .line 27
    const/16 v1, 0x10

    .line 28
    .line 29
    if-eq p1, v1, :cond_1

    .line 30
    .line 31
    const/16 v1, 0x71

    .line 32
    .line 33
    if-eq p1, v1, :cond_1

    .line 34
    .line 35
    const/16 v1, 0x72

    .line 36
    .line 37
    if-eq p1, v1, :cond_1

    .line 38
    .line 39
    const/16 v1, 0x9

    .line 40
    .line 41
    if-eq p1, v1, :cond_1

    .line 42
    .line 43
    const/16 v1, 0xbb9

    .line 44
    .line 45
    if-eq p1, v1, :cond_1

    .line 46
    .line 47
    const/16 v1, 0xd

    .line 48
    .line 49
    if-eq p1, v1, :cond_1

    .line 50
    .line 51
    const/16 v1, 0x77

    .line 52
    .line 53
    if-eq p1, v1, :cond_1

    .line 54
    .line 55
    if-ne p1, v0, :cond_0

    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_0
    const/4 p1, 0x0

    .line 59
    goto :goto_1

    .line 60
    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 61
    :goto_1
    return p1
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private static final O〇8O8〇008(Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;Lcom/intsig/camscanner/scandone/ScanDoneUtil$DynamicFeatureResult;Landroid/view/View;)V
    .locals 6

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object v0, Lcom/intsig/camscanner/scandone/ScanDoneUtil;->〇080:Lcom/intsig/camscanner/scandone/ScanDoneUtil;

    .line 7
    .line 8
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;->o〇00O:Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;

    .line 9
    .line 10
    invoke-virtual {p2}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;->〇〇0o8O〇〇()Landroid/app/Activity;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;->o〇00O:Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;

    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;->〇Oo〇o8()J

    .line 17
    .line 18
    .line 19
    move-result-wide v3

    .line 20
    sget-object v5, Lcom/intsig/comm/tracker/TrackParaSealed$DynamicFeature$CsListRecommend;->〇o00〇〇Oo:Lcom/intsig/comm/tracker/TrackParaSealed$DynamicFeature$CsListRecommend;

    .line 21
    .line 22
    move-object v2, p1

    .line 23
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/camscanner/scandone/ScanDoneUtil;->Oooo8o0〇(Landroid/app/Activity;Lcom/intsig/camscanner/scandone/ScanDoneUtil$DynamicFeatureResult;JLcom/intsig/comm/tracker/TrackParaSealed$DynamicFeature;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic o800o8O(Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider$BtmOpeViewHolder;Lcom/intsig/camscanner/scandone/ScanDoneUtil$DynamicFeatureResult;Ljava/lang/Boolean;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;->〇00(Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider$BtmOpeViewHolder;Lcom/intsig/camscanner/scandone/ScanDoneUtil$DynamicFeatureResult;Ljava/lang/Boolean;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic oo88o8O(Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;Lcom/intsig/camscanner/scandone/ScanDoneUtil$DynamicFeatureResult;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;->O〇8O8〇008(Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;Lcom/intsig/camscanner/scandone/ScanDoneUtil$DynamicFeatureResult;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇00(Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider$BtmOpeViewHolder;Lcom/intsig/camscanner/scandone/ScanDoneUtil$DynamicFeatureResult;Ljava/lang/Boolean;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$viewHolder"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 14
    .line 15
    .line 16
    move-result p3

    .line 17
    if-eqz p3, :cond_0

    .line 18
    .line 19
    const/4 p3, 0x0

    .line 20
    iput-boolean p3, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;->O8o08O8O:Z

    .line 21
    .line 22
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider$BtmOpeViewHolder;->O〇8O8〇008()Landroid/widget/LinearLayout;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-virtual {v0, p3}, Landroid/view/View;->setVisibility(I)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider$BtmOpeViewHolder;->O〇8O8〇008()Landroid/widget/LinearLayout;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    new-instance p3, Lo〇o08〇/〇〇888;

    .line 34
    .line 35
    invoke-direct {p3, p0, p2}, Lo〇o08〇/〇〇888;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;Lcom/intsig/camscanner/scandone/ScanDoneUtil$DynamicFeatureResult;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p1, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider$BtmOpeViewHolder;->O〇8O8〇008()Landroid/widget/LinearLayout;

    .line 43
    .line 44
    .line 45
    move-result-object p0

    .line 46
    const/16 p1, 0x8

    .line 47
    .line 48
    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 49
    .line 50
    .line 51
    :goto_0
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final 〇oOO8O8()Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/vendor/VendorHelper;->O8()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_3

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/utils/LanguageUtil;->〇O〇()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    goto :goto_1

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;->o〇00O:Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;->〇8o〇〇8080()I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    const/16 v1, 0xb

    .line 21
    .line 22
    if-eq v0, v1, :cond_2

    .line 23
    .line 24
    const/16 v1, 0x95

    .line 25
    .line 26
    if-eq v0, v1, :cond_2

    .line 27
    .line 28
    const/16 v1, 0x3e8

    .line 29
    .line 30
    if-eq v0, v1, :cond_1

    .line 31
    .line 32
    const/16 v1, 0xfa1

    .line 33
    .line 34
    if-eq v0, v1, :cond_2

    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;->o〇00O:Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;

    .line 37
    .line 38
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;->〇8o〇〇8080()I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;->O8ooOoo〇(I)Z

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    goto :goto_0

    .line 47
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/paper/PaperUtil;->〇080:Lcom/intsig/camscanner/paper/PaperUtil;

    .line 48
    .line 49
    invoke-virtual {v0}, Lcom/intsig/camscanner/paper/PaperUtil;->OO0o〇〇〇〇0()Z

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    goto :goto_0

    .line 54
    :cond_2
    const/4 v0, 0x1

    .line 55
    :goto_0
    return v0

    .line 56
    :cond_3
    :goto_1
    const/4 v0, 0x0

    .line 57
    return v0
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇oo〇(Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider$BtmOpeViewHolder;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider$BtmOpeViewHolder;->〇oOO8O8()Landroid/widget/TextView;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;->o〇00O:Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;->〇8o〇〇8080()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    const/16 v1, 0xb

    .line 12
    .line 13
    if-eq v0, v1, :cond_4

    .line 14
    .line 15
    const/16 v1, 0x95

    .line 16
    .line 17
    const v2, 0x7f1314e9

    .line 18
    .line 19
    .line 20
    if-eq v0, v1, :cond_3

    .line 21
    .line 22
    const/16 v1, 0x3e8

    .line 23
    .line 24
    if-eq v0, v1, :cond_2

    .line 25
    .line 26
    const/16 v1, 0xfa1

    .line 27
    .line 28
    if-eq v0, v1, :cond_1

    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;->o〇00O:Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;

    .line 31
    .line 32
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;->〇8o〇〇8080()I

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;->O8ooOoo〇(I)Z

    .line 37
    .line 38
    .line 39
    move-result v0

    .line 40
    if-eqz v0, :cond_0

    .line 41
    .line 42
    const-string v0, "\u6253\u5370\u8bc1\u4ef6"

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_0
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    const-string v1, "context.getString(R.string.cs_632_newmore_print)"

    .line 54
    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_1
    const-string v0, "\u6253\u5370\u767d\u677f"

    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_2
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    const v1, 0x7f130d8c

    .line 67
    .line 68
    .line 69
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    goto :goto_0

    .line 74
    :cond_3
    invoke-virtual {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->getContext()Landroid/content/Context;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object v0

    .line 82
    goto :goto_0

    .line 83
    :cond_4
    const-string v0, "\u6253\u5370\u4e66\u7c4d"

    .line 84
    .line 85
    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    .line 87
    .line 88
    return-void
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method


# virtual methods
.method public Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "parent"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1, p2}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    new-instance p2, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider$BtmOpeViewHolder;

    .line 11
    .line 12
    iget-object p1, p1, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 13
    .line 14
    const-string v0, "baseViewHolder.itemView"

    .line 15
    .line 16
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    invoke-direct {p2, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider$BtmOpeViewHolder;-><init>(Landroid/view/View;)V

    .line 20
    .line 21
    .line 22
    return-object p2
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public oO80()I
    .locals 1

    .line 1
    const v0, 0x7f0d046d

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇O8〇〇o(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/pagelist/model/PageTypeItem;)V
    .locals 8
    .param p1    # Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/pagelist/model/PageTypeItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "helper"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "item"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    check-cast p1, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider$BtmOpeViewHolder;

    .line 12
    .line 13
    sget-object v0, Lcom/intsig/camscanner/scandone/ScanDoneUtil;->〇080:Lcom/intsig/camscanner/scandone/ScanDoneUtil;

    .line 14
    .line 15
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;->o〇00O:Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;

    .line 16
    .line 17
    invoke-virtual {p2}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;->〇Oo〇o8()J

    .line 18
    .line 19
    .line 20
    move-result-wide v1

    .line 21
    sget-object v5, Lcom/intsig/comm/tracker/TrackParaSealed$DynamicFeature$CsListRecommend;->〇o00〇〇Oo:Lcom/intsig/comm/tracker/TrackParaSealed$DynamicFeature$CsListRecommend;

    .line 22
    .line 23
    invoke-virtual {v0, v1, v2, v5}, Lcom/intsig/camscanner/scandone/ScanDoneUtil;->〇O8o08O(JLcom/intsig/comm/tracker/TrackParaSealed$DynamicFeature;)Lcom/intsig/camscanner/scandone/ScanDoneUtil$DynamicFeatureResult;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    new-instance p2, Lo〇o08〇/o〇0;

    .line 28
    .line 29
    invoke-direct {p2, p0, p1, v1}, Lo〇o08〇/o〇0;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider$BtmOpeViewHolder;Lcom/intsig/camscanner/scandone/ScanDoneUtil$DynamicFeatureResult;)V

    .line 30
    .line 31
    .line 32
    if-eqz v1, :cond_0

    .line 33
    .line 34
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider$BtmOpeViewHolder;->O〇8O8〇008()Landroid/widget/LinearLayout;

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    const v3, 0x7f0a00c0

    .line 39
    .line 40
    .line 41
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 42
    .line 43
    .line 44
    move-result-object v2

    .line 45
    const-string v3, "viewHolder.llDynamicFeat\u2026R.id.aiv_dynamic_feature)"

    .line 46
    .line 47
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    check-cast v2, Landroid/widget/ImageView;

    .line 51
    .line 52
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider$BtmOpeViewHolder;->O〇8O8〇008()Landroid/widget/LinearLayout;

    .line 53
    .line 54
    .line 55
    move-result-object v3

    .line 56
    const v4, 0x7f0a13cd

    .line 57
    .line 58
    .line 59
    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 60
    .line 61
    .line 62
    move-result-object v3

    .line 63
    const-string v4, "viewHolder.llDynamicFeat\u2026(R.id.tv_dynamic_feature)"

    .line 64
    .line 65
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    check-cast v3, Landroid/widget/TextView;

    .line 69
    .line 70
    const/4 v4, 0x0

    .line 71
    iget-boolean v6, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;->O8o08O8O:Z

    .line 72
    .line 73
    move-object v7, p2

    .line 74
    invoke-virtual/range {v0 .. v7}, Lcom/intsig/camscanner/scandone/ScanDoneUtil;->o〇0(Lcom/intsig/camscanner/scandone/ScanDoneUtil$DynamicFeatureResult;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/ImageView;Lcom/intsig/comm/tracker/TrackParaSealed$DynamicFeature;ZLcom/intsig/callback/Callback;)V

    .line 75
    .line 76
    .line 77
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 78
    .line 79
    goto :goto_0

    .line 80
    :cond_0
    const/4 v0, 0x0

    .line 81
    :goto_0
    if-nez v0, :cond_1

    .line 82
    .line 83
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    .line 84
    .line 85
    invoke-interface {p2, v0}, Lcom/intsig/callback/Callback;->call(Ljava/lang/Object;)V

    .line 86
    .line 87
    .line 88
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;->〇oOO8O8()Z

    .line 89
    .line 90
    .line 91
    move-result p2

    .line 92
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;->〇oOO8O8()Z

    .line 93
    .line 94
    .line 95
    move-result v0

    .line 96
    if-eqz v0, :cond_2

    .line 97
    .line 98
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;->〇oo〇(Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider$BtmOpeViewHolder;)V

    .line 99
    .line 100
    .line 101
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider$BtmOpeViewHolder;->〇00()Landroid/widget/LinearLayout;

    .line 102
    .line 103
    .line 104
    move-result-object v0

    .line 105
    const/4 v1, 0x0

    .line 106
    if-nez p2, :cond_3

    .line 107
    .line 108
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;->o〇00O:Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;

    .line 109
    .line 110
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;->o8o〇〇0O()Ljava/util/List;

    .line 111
    .line 112
    .line 113
    move-result-object v2

    .line 114
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 115
    .line 116
    .line 117
    move-result v2

    .line 118
    const/4 v3, 0x1

    .line 119
    if-le v2, v3, :cond_3

    .line 120
    .line 121
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;->o〇00O:Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;

    .line 122
    .line 123
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;->oo0O〇0〇〇〇()Z

    .line 124
    .line 125
    .line 126
    move-result v2

    .line 127
    if-nez v2, :cond_3

    .line 128
    .line 129
    goto :goto_1

    .line 130
    :cond_3
    const/4 v3, 0x0

    .line 131
    :goto_1
    const/16 v2, 0x8

    .line 132
    .line 133
    if-eqz v3, :cond_4

    .line 134
    .line 135
    const/4 v3, 0x0

    .line 136
    goto :goto_2

    .line 137
    :cond_4
    const/16 v3, 0x8

    .line 138
    .line 139
    :goto_2
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 140
    .line 141
    .line 142
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider$BtmOpeViewHolder;->O8ooOoo〇()Landroid/widget/LinearLayout;

    .line 143
    .line 144
    .line 145
    move-result-object p1

    .line 146
    if-eqz p2, :cond_5

    .line 147
    .line 148
    goto :goto_3

    .line 149
    :cond_5
    const/16 v1, 0x8

    .line 150
    .line 151
    :goto_3
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 152
    .line 153
    .line 154
    return-void
.end method

.method public bridge synthetic 〇080(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListBtmOpeItemProvider;->o〇O8〇〇o(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/pagelist/model/PageTypeItem;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;->BTM_OPE:Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;->getType()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;
.super Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
.source "PageListImageItemProvider.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "PageImageHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O0O:Landroid/widget/ImageView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final O88O:Landroid/widget/ImageView;

.field private final O8o08O8O:Landroid/widget/TextView;

.field private final OO:Landroid/view/View;

.field private final OO〇00〇8oO:Landroid/widget/ImageView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o0:Landroidx/constraintlayout/widget/ConstraintLayout;

.field private final o8o:Landroid/widget/TextView;

.field private final o8oOOo:Landroid/widget/TextView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o8〇OO0〇0o:Landroid/widget/ImageView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oOO〇〇:Lcom/intsig/camscanner/view/CircleProgressView;

.field private final oOo0:Landroid/widget/LinearLayout;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oOo〇8o008:Landroid/widget/TextView;

.field private final oo8ooo8O:Landroid/view/ViewStub;

.field private final ooo0〇〇O:Landroid/widget/TextView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇00O:Landroid/widget/CheckBox;

.field final synthetic o〇oO:Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider;

.field private final 〇080OO8〇0:Landroid/widget/ImageView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08O〇00〇o:Landroid/view/View;

.field private final 〇0O:Landroid/widget/TextView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇8〇oO〇〇8o:Landroid/widget/LinearLayout;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Landroid/view/View;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇O〇〇O8:Landroid/widget/ImageView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o0O:Landroid/widget/LinearLayout;

.field private final 〇〇08O:Landroid/widget/TextView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider;Landroid/view/View;)V
    .locals 16
    .param p1    # Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p2

    .line 4
    .line 5
    const-string v2, "convertView"

    .line 6
    .line 7
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    move-object/from16 v2, p1

    .line 11
    .line 12
    iput-object v2, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->o〇oO:Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider;

    .line 13
    .line 14
    invoke-direct {v0, v1}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;-><init>(Landroid/view/View;)V

    .line 15
    .line 16
    .line 17
    invoke-static/range {p1 .. p1}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider;->O〇8O8〇008(Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider;)Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;

    .line 18
    .line 19
    .line 20
    move-result-object v2

    .line 21
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;->ooo8o〇o〇()Z

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    const-string v3, "binding.recognizedTag"

    .line 26
    .line 27
    const-string v4, "binding.viewCheckedBorder"

    .line 28
    .line 29
    const-string v5, "binding.txtPageitemModifiedTime"

    .line 30
    .line 31
    const-string v6, "binding.txtPageitemImgSize"

    .line 32
    .line 33
    const-string v7, "binding.llPageListBottomInfo"

    .line 34
    .line 35
    const-string v8, "binding.imageViewNote"

    .line 36
    .line 37
    const-string v9, "binding.syncState"

    .line 38
    .line 39
    const-string v10, "binding.statusView"

    .line 40
    .line 41
    const-string v11, "binding.statusViewBackground"

    .line 42
    .line 43
    const-string v12, "binding.pageImage"

    .line 44
    .line 45
    const-string v13, "binding.txtPagelistPageName"

    .line 46
    .line 47
    const-string v14, "binding.textViewPageNote"

    .line 48
    .line 49
    const-string v15, "bind(convertView)"

    .line 50
    .line 51
    const/4 v1, 0x0

    .line 52
    if-eqz v2, :cond_0

    .line 53
    .line 54
    invoke-static/range {p2 .. p2}, Lcom/intsig/camscanner/databinding/ItemPageListImageGridBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ItemPageListImageGridBinding;

    .line 55
    .line 56
    .line 57
    move-result-object v2

    .line 58
    invoke-static {v2, v15}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    iget-object v15, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageGridBinding;->O0O:Landroid/widget/TextView;

    .line 62
    .line 63
    invoke-static {v15, v14}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    iput-object v15, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->o8oOOo:Landroid/widget/TextView;

    .line 67
    .line 68
    iget-object v14, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageGridBinding;->oOO〇〇:Landroid/widget/TextView;

    .line 69
    .line 70
    invoke-static {v14, v13}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    iput-object v14, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->〇0O:Landroid/widget/TextView;

    .line 74
    .line 75
    iget-object v13, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageGridBinding;->〇0O:Lcom/intsig/view/SafeImageView;

    .line 76
    .line 77
    invoke-static {v13, v12}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    .line 79
    .line 80
    iput-object v13, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 81
    .line 82
    iget-object v12, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageGridBinding;->〇〇08O:Landroid/widget/TextView;

    .line 83
    .line 84
    iput-object v12, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->oOo〇8o008:Landroid/widget/TextView;

    .line 85
    .line 86
    iget-object v12, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageGridBinding;->o8〇OO0〇0o:Landroid/widget/LinearLayout;

    .line 87
    .line 88
    invoke-static {v12, v11}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    iput-object v12, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->oOo0:Landroid/widget/LinearLayout;

    .line 92
    .line 93
    iget-object v11, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageGridBinding;->OO〇00〇8oO:Landroid/widget/ImageView;

    .line 94
    .line 95
    invoke-static {v11, v10}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 96
    .line 97
    .line 98
    iput-object v11, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->OO〇00〇8oO:Landroid/widget/ImageView;

    .line 99
    .line 100
    iget-object v10, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageGridBinding;->〇8〇oO〇〇8o:Landroid/widget/ImageView;

    .line 101
    .line 102
    invoke-static {v10, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    iput-object v10, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->o8〇OO0〇0o:Landroid/widget/ImageView;

    .line 106
    .line 107
    iget-object v9, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageGridBinding;->o〇00O:Landroid/widget/ImageView;

    .line 108
    .line 109
    invoke-static {v9, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    .line 111
    .line 112
    iput-object v9, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->O0O:Landroid/widget/ImageView;

    .line 113
    .line 114
    iget-object v8, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageGridBinding;->O8o08O8O:Landroid/widget/LinearLayout;

    .line 115
    .line 116
    invoke-static {v8, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 117
    .line 118
    .line 119
    iput-object v8, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->〇8〇oO〇〇8o:Landroid/widget/LinearLayout;

    .line 120
    .line 121
    iget-object v7, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageGridBinding;->〇o0O:Landroid/widget/TextView;

    .line 122
    .line 123
    invoke-static {v7, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    .line 125
    .line 126
    iput-object v7, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->ooo0〇〇O:Landroid/widget/TextView;

    .line 127
    .line 128
    iget-object v6, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageGridBinding;->O88O:Landroid/widget/TextView;

    .line 129
    .line 130
    invoke-static {v6, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    .line 132
    .line 133
    iput-object v6, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->〇〇08O:Landroid/widget/TextView;

    .line 134
    .line 135
    iget-object v5, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageGridBinding;->〇OOo8〇0:Landroid/widget/CheckBox;

    .line 136
    .line 137
    iput-object v5, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->o〇00O:Landroid/widget/CheckBox;

    .line 138
    .line 139
    iget-object v5, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageGridBinding;->〇O〇〇O8:Landroid/widget/TextView;

    .line 140
    .line 141
    iput-object v5, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->O8o08O8O:Landroid/widget/TextView;

    .line 142
    .line 143
    iget-object v5, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageGridBinding;->o8o:Landroid/view/View;

    .line 144
    .line 145
    invoke-static {v5, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 146
    .line 147
    .line 148
    iput-object v5, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->〇OOo8〇0:Landroid/view/View;

    .line 149
    .line 150
    iget-object v4, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageGridBinding;->oOo〇8o008:Landroid/widget/ImageView;

    .line 151
    .line 152
    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    .line 154
    .line 155
    iput-object v4, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->〇O〇〇O8:Landroid/widget/ImageView;

    .line 156
    .line 157
    iget-object v3, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageGridBinding;->oo8ooo8O:Landroid/view/View;

    .line 158
    .line 159
    iput-object v3, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->OO:Landroid/view/View;

    .line 160
    .line 161
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageGridBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 162
    .line 163
    iput-object v2, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->〇08O〇00〇o:Landroid/view/View;

    .line 164
    .line 165
    iput-object v1, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->〇o0O:Landroid/widget/LinearLayout;

    .line 166
    .line 167
    iput-object v1, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->O88O:Landroid/widget/ImageView;

    .line 168
    .line 169
    iput-object v1, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->oOO〇〇:Lcom/intsig/camscanner/view/CircleProgressView;

    .line 170
    .line 171
    iput-object v1, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->o8o:Landroid/widget/TextView;

    .line 172
    .line 173
    iput-object v1, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->oo8ooo8O:Landroid/view/ViewStub;

    .line 174
    .line 175
    goto/16 :goto_0

    .line 176
    .line 177
    :cond_0
    invoke-static/range {p2 .. p2}, Lcom/intsig/camscanner/databinding/ItemPageListImageListBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ItemPageListImageListBinding;

    .line 178
    .line 179
    .line 180
    move-result-object v2

    .line 181
    invoke-static {v2, v15}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    .line 183
    .line 184
    iget-object v15, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageListBinding;->〇0O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 185
    .line 186
    iput-object v15, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->o0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 187
    .line 188
    iget-object v15, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageListBinding;->〇8〇oO〇〇8o:Landroid/widget/TextView;

    .line 189
    .line 190
    invoke-static {v15, v14}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    .line 192
    .line 193
    iput-object v15, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->o8oOOo:Landroid/widget/TextView;

    .line 194
    .line 195
    iget-object v14, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageListBinding;->o8oOOo:Landroid/widget/TextView;

    .line 196
    .line 197
    invoke-static {v14, v13}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 198
    .line 199
    .line 200
    iput-object v14, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->〇0O:Landroid/widget/TextView;

    .line 201
    .line 202
    iget-object v13, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageListBinding;->O8o08O8O:Landroid/widget/ImageView;

    .line 203
    .line 204
    invoke-static {v13, v12}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 205
    .line 206
    .line 207
    iput-object v13, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 208
    .line 209
    iput-object v1, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->oOo〇8o008:Landroid/widget/TextView;

    .line 210
    .line 211
    iget-object v12, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageListBinding;->oOo0:Landroid/widget/LinearLayout;

    .line 212
    .line 213
    invoke-static {v12, v11}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 214
    .line 215
    .line 216
    iput-object v12, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->oOo0:Landroid/widget/LinearLayout;

    .line 217
    .line 218
    iget-object v11, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageListBinding;->oOo〇8o008:Landroid/widget/ImageView;

    .line 219
    .line 220
    invoke-static {v11, v10}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 221
    .line 222
    .line 223
    iput-object v11, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->OO〇00〇8oO:Landroid/widget/ImageView;

    .line 224
    .line 225
    iget-object v10, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageListBinding;->OO〇00〇8oO:Landroid/widget/ImageView;

    .line 226
    .line 227
    invoke-static {v10, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 228
    .line 229
    .line 230
    iput-object v10, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->o8〇OO0〇0o:Landroid/widget/ImageView;

    .line 231
    .line 232
    iget-object v9, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageListBinding;->OO:Landroid/widget/ImageView;

    .line 233
    .line 234
    invoke-static {v9, v8}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 235
    .line 236
    .line 237
    iput-object v9, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->O0O:Landroid/widget/ImageView;

    .line 238
    .line 239
    iget-object v8, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageListBinding;->o〇00O:Landroid/widget/LinearLayout;

    .line 240
    .line 241
    invoke-static {v8, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 242
    .line 243
    .line 244
    iput-object v8, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->〇8〇oO〇〇8o:Landroid/widget/LinearLayout;

    .line 245
    .line 246
    iget-object v7, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageListBinding;->〇〇08O:Landroid/widget/TextView;

    .line 247
    .line 248
    invoke-static {v7, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 249
    .line 250
    .line 251
    iput-object v7, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->ooo0〇〇O:Landroid/widget/TextView;

    .line 252
    .line 253
    iget-object v6, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageListBinding;->O0O:Landroid/widget/TextView;

    .line 254
    .line 255
    invoke-static {v6, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 256
    .line 257
    .line 258
    iput-object v6, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->〇〇08O:Landroid/widget/TextView;

    .line 259
    .line 260
    iput-object v1, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->o〇00O:Landroid/widget/CheckBox;

    .line 261
    .line 262
    iput-object v1, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->O8o08O8O:Landroid/widget/TextView;

    .line 263
    .line 264
    iget-object v5, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageListBinding;->〇O〇〇O8:Landroid/view/View;

    .line 265
    .line 266
    invoke-static {v5, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 267
    .line 268
    .line 269
    iput-object v5, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->〇OOo8〇0:Landroid/view/View;

    .line 270
    .line 271
    iget-object v4, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageListBinding;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 272
    .line 273
    invoke-static {v4, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 274
    .line 275
    .line 276
    iput-object v4, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->〇O〇〇O8:Landroid/widget/ImageView;

    .line 277
    .line 278
    iget-object v3, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageListBinding;->o8〇OO0〇0o:Landroid/widget/LinearLayout;

    .line 279
    .line 280
    iput-object v3, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->〇o0O:Landroid/widget/LinearLayout;

    .line 281
    .line 282
    iput-object v1, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->OO:Landroid/view/View;

    .line 283
    .line 284
    iput-object v1, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->〇08O〇00〇o:Landroid/view/View;

    .line 285
    .line 286
    iget-object v1, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageListBinding;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 287
    .line 288
    iput-object v1, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->O88O:Landroid/widget/ImageView;

    .line 289
    .line 290
    iget-object v1, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageListBinding;->〇OOo8〇0:Lcom/intsig/camscanner/view/CircleProgressView;

    .line 291
    .line 292
    iput-object v1, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->oOO〇〇:Lcom/intsig/camscanner/view/CircleProgressView;

    .line 293
    .line 294
    iget-object v1, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageListBinding;->ooo0〇〇O:Landroid/widget/TextView;

    .line 295
    .line 296
    iput-object v1, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->o8o:Landroid/widget/TextView;

    .line 297
    .line 298
    iget-object v1, v2, Lcom/intsig/camscanner/databinding/ItemPageListImageListBinding;->〇o0O:Landroid/view/ViewStub;

    .line 299
    .line 300
    iput-object v1, v0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->oo8ooo8O:Landroid/view/ViewStub;

    .line 301
    .line 302
    :goto_0
    return-void
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method


# virtual methods
.method public final O8ooOoo〇()Landroid/widget/ImageView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->O0O:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O8〇o()Landroid/widget/LinearLayout;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->〇8〇oO〇〇8o:Landroid/widget/LinearLayout;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final OOO〇O0()Landroid/widget/ImageView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->OO〇00〇8oO:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final Oo8Oo00oo()Landroid/widget/TextView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->〇0O:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O〇8O8〇008()Lcom/intsig/camscanner/view/CircleProgressView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->oOO〇〇:Lcom/intsig/camscanner/view/CircleProgressView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o0ooO()Landroid/widget/TextView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->〇〇08O:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o8()Landroid/widget/TextView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->o8oOOo:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oO()Landroid/view/View;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->〇08O〇00〇o:Landroid/view/View;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oo〇()Landroid/widget/ImageView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->o8〇OO0〇0o:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇0OOo〇0()Landroid/widget/TextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->O8o08O8O:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇8()Landroid/widget/TextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->oOo〇8o008:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇〇0〇()Landroid/widget/ImageView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->O88O:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇00()Landroid/widget/CheckBox;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->o〇00O:Landroid/widget/CheckBox;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇0000OOO()Landroid/widget/ImageView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->〇080OO8〇0:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇00〇8()Landroid/widget/LinearLayout;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->oOo0:Landroid/widget/LinearLayout;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇08O8o〇0()Landroid/view/View;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->OO:Landroid/view/View;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇8(I)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->o〇oO:Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider;->O〇8O8〇008(Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider;)Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;->ooo8o〇o〇()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_8

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->o〇oO:Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider;

    .line 14
    .line 15
    invoke-static {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider;->O8ooOoo〇(Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider;)Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇O00()Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-eqz v0, :cond_0

    .line 24
    .line 25
    goto :goto_6

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->O8o08O8O:Landroid/widget/TextView;

    .line 27
    .line 28
    if-eqz v0, :cond_7

    .line 29
    .line 30
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->o〇00O:Landroid/widget/CheckBox;

    .line 31
    .line 32
    if-nez v1, :cond_1

    .line 33
    .line 34
    goto :goto_5

    .line 35
    :cond_1
    const/16 v1, 0x63

    .line 36
    .line 37
    if-gt p1, v1, :cond_2

    .line 38
    .line 39
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    goto :goto_0

    .line 44
    :cond_2
    const-string v1, "99+"

    .line 45
    .line 46
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->O8o08O8O:Landroid/widget/TextView;

    .line 50
    .line 51
    const/4 v1, 0x1

    .line 52
    const/4 v2, 0x0

    .line 53
    if-lez p1, :cond_3

    .line 54
    .line 55
    const/4 v3, 0x1

    .line 56
    goto :goto_1

    .line 57
    :cond_3
    const/4 v3, 0x0

    .line 58
    :goto_1
    const/16 v4, 0x8

    .line 59
    .line 60
    if-eqz v3, :cond_4

    .line 61
    .line 62
    const/4 v3, 0x0

    .line 63
    goto :goto_2

    .line 64
    :cond_4
    const/16 v3, 0x8

    .line 65
    .line 66
    :goto_2
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 67
    .line 68
    .line 69
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->o〇00O:Landroid/widget/CheckBox;

    .line 70
    .line 71
    if-gtz p1, :cond_5

    .line 72
    .line 73
    goto :goto_3

    .line 74
    :cond_5
    const/4 v1, 0x0

    .line 75
    :goto_3
    if-eqz v1, :cond_6

    .line 76
    .line 77
    goto :goto_4

    .line 78
    :cond_6
    const/16 v2, 0x8

    .line 79
    .line 80
    :goto_4
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 81
    .line 82
    .line 83
    :cond_7
    :goto_5
    return-void

    .line 84
    :cond_8
    :goto_6
    sget-object p1, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider;->o8〇OO0〇0o:Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$Companion;

    .line 85
    .line 86
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$Companion;->〇080()Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object p1

    .line 90
    const-string v0, "pageList drag forbidden on list mode"

    .line 91
    .line 92
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    .line 94
    .line 95
    return-void
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public final 〇o()Landroid/widget/TextView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->ooo0〇〇O:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇oOO8O8()Landroid/widget/ImageView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->〇O〇〇O8:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇〇0o()Landroid/view/View;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->〇OOo8〇0:Landroid/view/View;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇〇〇0〇〇0()Landroid/widget/TextView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListImageItemProvider$PageImageHolder;->o8o:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;
.super Landroidx/lifecycle/ViewModel;
.source "InvoiceListViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final O8o08O8O:Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇080OO8〇0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final OO:Lkotlinx/coroutines/channels/Channel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/channels/Channel<",
            "Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o0:Lkotlinx/coroutines/channels/Channel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/channels/Channel<",
            "Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇00O:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇08O〇00〇o:J

.field private final 〇OOo8〇0:Lkotlinx/coroutines/flow/Flow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/flow/Flow<",
            "Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->O8o08O8O:Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel$Companion;

    .line 8
    .line 9
    const-string v0, "InvoiceListViewModel"

    .line 10
    .line 11
    sput-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->〇080OO8〇0:Ljava/lang/String;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 5

    .line 1
    invoke-direct {p0}, Landroidx/lifecycle/ViewModel;-><init>()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lkotlinx/coroutines/channels/BufferOverflow;->SUSPEND:Lkotlinx/coroutines/channels/BufferOverflow;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    const/4 v2, 0x0

    .line 8
    const/4 v3, 0x5

    .line 9
    invoke-static {v1, v0, v2, v3, v2}, Lkotlinx/coroutines/channels/ChannelKt;->〇o00〇〇Oo(ILkotlinx/coroutines/channels/BufferOverflow;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lkotlinx/coroutines/channels/Channel;

    .line 10
    .line 11
    .line 12
    move-result-object v4

    .line 13
    iput-object v4, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->o0:Lkotlinx/coroutines/channels/Channel;

    .line 14
    .line 15
    invoke-static {v4}, Lkotlinx/coroutines/flow/FlowKt;->〇oo〇(Lkotlinx/coroutines/channels/ReceiveChannel;)Lkotlinx/coroutines/flow/Flow;

    .line 16
    .line 17
    .line 18
    move-result-object v4

    .line 19
    iput-object v4, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->〇OOo8〇0:Lkotlinx/coroutines/flow/Flow;

    .line 20
    .line 21
    invoke-static {v1, v0, v2, v3, v2}, Lkotlinx/coroutines/channels/ChannelKt;->〇o00〇〇Oo(ILkotlinx/coroutines/channels/BufferOverflow;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lkotlinx/coroutines/channels/Channel;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->OO:Lkotlinx/coroutines/channels/Channel;

    .line 26
    .line 27
    const-wide/16 v0, -0x1

    .line 28
    .line 29
    iput-wide v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->〇08O〇00〇o:J

    .line 30
    .line 31
    new-instance v0, Landroidx/lifecycle/MutableLiveData;

    .line 32
    .line 33
    invoke-direct {v0}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 34
    .line 35
    .line 36
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->o〇00O:Landroidx/lifecycle/MutableLiveData;

    .line 37
    .line 38
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->〇00()V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic Oooo8o0〇(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->〇oo〇(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final oo〇(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState;)V
    .locals 6

    .line 1
    invoke-static {p0}, Landroidx/lifecycle/ViewModelKt;->getViewModelScope(Landroidx/lifecycle/ViewModel;)Lkotlinx/coroutines/CoroutineScope;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x0

    .line 7
    new-instance v3, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel$sendUIState$1;

    .line 8
    .line 9
    const/4 v4, 0x0

    .line 10
    invoke-direct {v3, p0, p1, v4}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel$sendUIState$1;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState;Lkotlin/coroutines/Continuation;)V

    .line 11
    .line 12
    .line 13
    const/4 v4, 0x3

    .line 14
    const/4 v5, 0x0

    .line 15
    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
.end method

.method private final 〇00()V
    .locals 6

    .line 1
    invoke-static {p0}, Landroidx/lifecycle/ViewModelKt;->getViewModelScope(Landroidx/lifecycle/ViewModel;)Lkotlinx/coroutines/CoroutineScope;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x0

    .line 7
    new-instance v3, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel$observeAction$1;

    .line 8
    .line 9
    const/4 v4, 0x0

    .line 10
    invoke-direct {v3, p0, v4}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel$observeAction$1;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;Lkotlin/coroutines/Continuation;)V

    .line 11
    .line 12
    .line 13
    const/4 v4, 0x3

    .line 14
    const/4 v5, 0x0

    .line 15
    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇80〇808〇O(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;)Lkotlinx/coroutines/channels/Channel;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->OO:Lkotlinx/coroutines/channels/Channel;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇8o8o〇(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;)Lkotlinx/coroutines/channels/Channel;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->o0:Lkotlinx/coroutines/channels/Channel;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇O00(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->oo〇(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇oo〇(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction;)V
    .locals 11

    .line 1
    instance-of v0, p1, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$UpdateTitleAction;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$UpdateTitle;

    .line 6
    .line 7
    check-cast p1, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$UpdateTitleAction;

    .line 8
    .line 9
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$UpdateTitleAction;->〇o〇()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$UpdateTitleAction;->〇080()Z

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$UpdateTitleAction;->〇o00〇〇Oo()Z

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    invoke-direct {v0, v1, v2, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$UpdateTitle;-><init>(ZZZ)V

    .line 22
    .line 23
    .line 24
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->oo〇(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState;)V

    .line 25
    .line 26
    .line 27
    goto/16 :goto_2

    .line 28
    .line 29
    :cond_0
    instance-of v0, p1, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$RequestInvoiceListAction;

    .line 30
    .line 31
    if-eqz v0, :cond_1

    .line 32
    .line 33
    check-cast p1, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$RequestInvoiceListAction;

    .line 34
    .line 35
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$RequestInvoiceListAction;->〇080()Ljava/lang/Long;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    if-eqz p1, :cond_7

    .line 40
    .line 41
    invoke-virtual {p1}, Ljava/lang/Number;->longValue()J

    .line 42
    .line 43
    .line 44
    move-result-wide v0

    .line 45
    invoke-static {v0, v1}, Lcom/intsig/camscanner/capture/invoice/InvoiceUtils;->O8(J)Ljava/util/List;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    sget-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->〇080OO8〇0:Ljava/lang/String;

    .line 50
    .line 51
    new-instance v1, Ljava/lang/StringBuilder;

    .line 52
    .line 53
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 54
    .line 55
    .line 56
    const-string v2, "RequestInvoiceListAction "

    .line 57
    .line 58
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object v1

    .line 68
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    new-instance v0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$RefreshInvoiceListState;

    .line 72
    .line 73
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$RefreshInvoiceListState;-><init>(Ljava/util/List;)V

    .line 74
    .line 75
    .line 76
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->oo〇(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState;)V

    .line 77
    .line 78
    .line 79
    goto/16 :goto_2

    .line 80
    .line 81
    :cond_1
    instance-of v0, p1, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$ExportInvoiceAction;

    .line 82
    .line 83
    if-eqz v0, :cond_4

    .line 84
    .line 85
    sget-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->〇080OO8〇0:Ljava/lang/String;

    .line 86
    .line 87
    check-cast p1, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$ExportInvoiceAction;

    .line 88
    .line 89
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$ExportInvoiceAction;->〇o00〇〇Oo()Ljava/util/List;

    .line 90
    .line 91
    .line 92
    move-result-object v1

    .line 93
    check-cast v1, Ljava/lang/Iterable;

    .line 94
    .line 95
    new-instance v2, Ljava/util/ArrayList;

    .line 96
    .line 97
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 98
    .line 99
    .line 100
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 101
    .line 102
    .line 103
    move-result-object v1

    .line 104
    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 105
    .line 106
    .line 107
    move-result v3

    .line 108
    if-eqz v3, :cond_3

    .line 109
    .line 110
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 111
    .line 112
    .line 113
    move-result-object v3

    .line 114
    move-object v4, v3

    .line 115
    check-cast v4, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;

    .line 116
    .line 117
    invoke-virtual {v4}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListItem;->〇o00〇〇Oo()Z

    .line 118
    .line 119
    .line 120
    move-result v4

    .line 121
    if-eqz v4, :cond_2

    .line 122
    .line 123
    invoke-interface {v2, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 124
    .line 125
    .line 126
    goto :goto_0

    .line 127
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 128
    .line 129
    .line 130
    move-result v1

    .line 131
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 132
    .line 133
    .line 134
    move-result-object v1

    .line 135
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    .line 137
    .line 138
    new-instance v0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$ExportInvoiceState;

    .line 139
    .line 140
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$ExportInvoiceAction;->〇080()J

    .line 141
    .line 142
    .line 143
    move-result-wide v1

    .line 144
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$ExportInvoiceAction;->〇o00〇〇Oo()Ljava/util/List;

    .line 145
    .line 146
    .line 147
    move-result-object p1

    .line 148
    invoke-direct {v0, v1, v2, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState$ExportInvoiceState;-><init>(JLjava/util/List;)V

    .line 149
    .line 150
    .line 151
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->oo〇(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState;)V

    .line 152
    .line 153
    .line 154
    goto :goto_2

    .line 155
    :cond_4
    instance-of v0, p1, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$DeleteInvoiceAction;

    .line 156
    .line 157
    const/4 v1, 0x0

    .line 158
    if-eqz v0, :cond_6

    .line 159
    .line 160
    move-object v0, p1

    .line 161
    check-cast v0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$DeleteInvoiceAction;

    .line 162
    .line 163
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$DeleteInvoiceAction;->〇o00〇〇Oo()Ljava/util/List;

    .line 164
    .line 165
    .line 166
    move-result-object v2

    .line 167
    invoke-static {v2}, Lcom/intsig/camscanner/capture/invoice/InvoiceUtils;->〇080(Ljava/util/List;)V

    .line 168
    .line 169
    .line 170
    new-instance v2, Ljava/util/ArrayList;

    .line 171
    .line 172
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 173
    .line 174
    .line 175
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$DeleteInvoiceAction;->〇o00〇〇Oo()Ljava/util/List;

    .line 176
    .line 177
    .line 178
    move-result-object v0

    .line 179
    check-cast v0, Ljava/lang/Iterable;

    .line 180
    .line 181
    new-instance v3, Ljava/util/ArrayList;

    .line 182
    .line 183
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 184
    .line 185
    .line 186
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 187
    .line 188
    .line 189
    move-result-object v0

    .line 190
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 191
    .line 192
    .line 193
    move-result v4

    .line 194
    if-eqz v4, :cond_5

    .line 195
    .line 196
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 197
    .line 198
    .line 199
    move-result-object v4

    .line 200
    check-cast v4, Lcom/intsig/camscanner/capture/invoice/data/Bills;

    .line 201
    .line 202
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 203
    .line 204
    .line 205
    move-result-object v5

    .line 206
    invoke-virtual {v4}, Lcom/intsig/camscanner/capture/invoice/data/Bills;->getPageSyncId()Ljava/lang/String;

    .line 207
    .line 208
    .line 209
    move-result-object v4

    .line 210
    invoke-static {v5, v4}, Lcom/intsig/camscanner/db/dao/ImageDao;->O〇O〇oO(Landroid/content/Context;Ljava/lang/String;)J

    .line 211
    .line 212
    .line 213
    move-result-wide v4

    .line 214
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 215
    .line 216
    .line 217
    move-result-object v4

    .line 218
    invoke-interface {v3, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 219
    .line 220
    .line 221
    goto :goto_1

    .line 222
    :cond_5
    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 223
    .line 224
    .line 225
    invoke-static {p0}, Landroidx/lifecycle/ViewModelKt;->getViewModelScope(Landroidx/lifecycle/ViewModel;)Lkotlinx/coroutines/CoroutineScope;

    .line 226
    .line 227
    .line 228
    move-result-object v5

    .line 229
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 230
    .line 231
    .line 232
    move-result-object v6

    .line 233
    const/4 v7, 0x0

    .line 234
    new-instance v8, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel$handleAction$4;

    .line 235
    .line 236
    invoke-direct {v8, p1, v2, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel$handleAction$4;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction;Ljava/util/List;Lkotlin/coroutines/Continuation;)V

    .line 237
    .line 238
    .line 239
    const/4 v9, 0x2

    .line 240
    const/4 v10, 0x0

    .line 241
    invoke-static/range {v5 .. v10}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 242
    .line 243
    .line 244
    goto :goto_2

    .line 245
    :cond_6
    instance-of v0, p1, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction$Go2DetailAction;

    .line 246
    .line 247
    if-eqz v0, :cond_7

    .line 248
    .line 249
    invoke-static {p0}, Landroidx/lifecycle/ViewModelKt;->getViewModelScope(Landroidx/lifecycle/ViewModel;)Lkotlinx/coroutines/CoroutineScope;

    .line 250
    .line 251
    .line 252
    move-result-object v2

    .line 253
    const/4 v3, 0x0

    .line 254
    const/4 v4, 0x0

    .line 255
    new-instance v5, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel$handleAction$5;

    .line 256
    .line 257
    invoke-direct {v5, p1, p0, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel$handleAction$5;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction;Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;Lkotlin/coroutines/Continuation;)V

    .line 258
    .line 259
    .line 260
    const/4 v6, 0x3

    .line 261
    const/4 v7, 0x0

    .line 262
    invoke-static/range {v2 .. v7}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 263
    .line 264
    .line 265
    :cond_7
    :goto_2
    return-void
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method


# virtual methods
.method public final O8ooOoo〇(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction;)V
    .locals 7
    .param p1    # Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "action"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {p0}, Landroidx/lifecycle/ViewModelKt;->getViewModelScope(Landroidx/lifecycle/ViewModel;)Lkotlinx/coroutines/CoroutineScope;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    const/4 v2, 0x0

    .line 11
    const/4 v3, 0x0

    .line 12
    new-instance v4, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel$sendAction$1;

    .line 13
    .line 14
    const/4 v0, 0x0

    .line 15
    invoke-direct {v4, p0, p1, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel$sendAction$1;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListAction;Lkotlin/coroutines/Continuation;)V

    .line 16
    .line 17
    .line 18
    const/4 v5, 0x3

    .line 19
    const/4 v6, 0x0

    .line 20
    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final oo88o8O()Lkotlinx/coroutines/flow/Flow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/flow/Flow<",
            "Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListUIState;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->〇OOo8〇0:Lkotlinx/coroutines/flow/Flow;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇O8o08O()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->〇08O〇00〇o:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o〇(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/invoice/InvoiceListViewModel;->〇08O〇00〇o:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

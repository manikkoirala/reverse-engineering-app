.class public final Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew$PageListDiffCallBack;
.super Landroidx/recyclerview/widget/DiffUtil$ItemCallback;
.source "PageListAdapterNew.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "PageListDiffCallBack"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/DiffUtil$ItemCallback<",
        "Lcom/intsig/camscanner/pagelist/model/PageTypeItem;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field final synthetic 〇080:Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew$PageListDiffCallBack;->〇080:Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew;

    .line 2
    .line 3
    invoke-direct {p0}, Landroidx/recyclerview/widget/DiffUtil$ItemCallback;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public bridge synthetic areContentsTheSame(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 0

    .line 1
    check-cast p1, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;

    .line 2
    .line 3
    check-cast p2, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;

    .line 4
    .line 5
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew$PageListDiffCallBack;->〇080(Lcom/intsig/camscanner/pagelist/model/PageTypeItem;Lcom/intsig/camscanner/pagelist/model/PageTypeItem;)Z

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    return p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public bridge synthetic areItemsTheSame(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 0

    .line 1
    check-cast p1, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;

    .line 2
    .line 3
    check-cast p2, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;

    .line 4
    .line 5
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageListAdapterNew$PageListDiffCallBack;->〇o00〇〇Oo(Lcom/intsig/camscanner/pagelist/model/PageTypeItem;Lcom/intsig/camscanner/pagelist/model/PageTypeItem;)Z

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    return p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇080(Lcom/intsig/camscanner/pagelist/model/PageTypeItem;Lcom/intsig/camscanner/pagelist/model/PageTypeItem;)Z
    .locals 5
    .param p1    # Lcom/intsig/camscanner/pagelist/model/PageTypeItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/pagelist/model/PageTypeItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "oldItem"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "newItem"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    instance-of v0, p1, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    instance-of v0, p2, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 16
    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    check-cast p1, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 20
    .line 21
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageImageItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/pagelist/model/PageItem;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    check-cast p2, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 26
    .line 27
    invoke-virtual {p2}, Lcom/intsig/camscanner/pagelist/model/PageImageItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/pagelist/model/PageItem;

    .line 28
    .line 29
    .line 30
    move-result-object p2

    .line 31
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 32
    .line 33
    .line 34
    move-result p1

    .line 35
    goto :goto_1

    .line 36
    :cond_0
    instance-of v0, p1, Lcom/intsig/camscanner/pagelist/model/PageOperationItem;

    .line 37
    .line 38
    const/4 v1, 0x1

    .line 39
    const/4 v2, 0x0

    .line 40
    if-eqz v0, :cond_1

    .line 41
    .line 42
    instance-of v0, p2, Lcom/intsig/camscanner/pagelist/model/PageOperationItem;

    .line 43
    .line 44
    if-eqz v0, :cond_1

    .line 45
    .line 46
    check-cast p1, Lcom/intsig/camscanner/pagelist/model/PageOperationItem;

    .line 47
    .line 48
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageOperationItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/business/operation/OperateContent;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    invoke-interface {p1}, Lcom/intsig/camscanner/business/operation/OperateContent;->〇080()I

    .line 53
    .line 54
    .line 55
    move-result p1

    .line 56
    check-cast p2, Lcom/intsig/camscanner/pagelist/model/PageOperationItem;

    .line 57
    .line 58
    invoke-virtual {p2}, Lcom/intsig/camscanner/pagelist/model/PageOperationItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/business/operation/OperateContent;

    .line 59
    .line 60
    .line 61
    move-result-object p2

    .line 62
    invoke-interface {p2}, Lcom/intsig/camscanner/business/operation/OperateContent;->〇080()I

    .line 63
    .line 64
    .line 65
    move-result p2

    .line 66
    if-ne p1, p2, :cond_3

    .line 67
    .line 68
    :goto_0
    const/4 p1, 0x1

    .line 69
    goto :goto_1

    .line 70
    :cond_1
    instance-of v0, p1, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;

    .line 71
    .line 72
    if-eqz v0, :cond_2

    .line 73
    .line 74
    instance-of v0, p2, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;

    .line 75
    .line 76
    if-eqz v0, :cond_2

    .line 77
    .line 78
    check-cast p1, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;

    .line 79
    .line 80
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;->Oo08()J

    .line 81
    .line 82
    .line 83
    move-result-wide v3

    .line 84
    check-cast p2, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;

    .line 85
    .line 86
    invoke-virtual {p2}, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;->Oo08()J

    .line 87
    .line 88
    .line 89
    move-result-wide p1

    .line 90
    cmp-long v0, v3, p1

    .line 91
    .line 92
    if-nez v0, :cond_3

    .line 93
    .line 94
    goto :goto_0

    .line 95
    :cond_2
    instance-of p1, p1, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageBtmOpeTypeItem;

    .line 96
    .line 97
    if-eqz p1, :cond_3

    .line 98
    .line 99
    instance-of p1, p2, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageBtmOpeTypeItem;

    .line 100
    .line 101
    if-eqz p1, :cond_3

    .line 102
    .line 103
    goto :goto_0

    .line 104
    :cond_3
    const/4 p1, 0x0

    .line 105
    :goto_1
    return p1
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public 〇o00〇〇Oo(Lcom/intsig/camscanner/pagelist/model/PageTypeItem;Lcom/intsig/camscanner/pagelist/model/PageTypeItem;)Z
    .locals 5
    .param p1    # Lcom/intsig/camscanner/pagelist/model/PageTypeItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/pagelist/model/PageTypeItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "oldItem"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "newItem"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    instance-of v0, p1, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 12
    .line 13
    const/4 v1, 0x1

    .line 14
    const/4 v2, 0x0

    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    instance-of v0, p2, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 18
    .line 19
    if-eqz v0, :cond_1

    .line 20
    .line 21
    check-cast p1, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 22
    .line 23
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageImageItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/pagelist/model/PageItem;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    iget-wide v3, p1, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇080:J

    .line 28
    .line 29
    check-cast p2, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 30
    .line 31
    invoke-virtual {p2}, Lcom/intsig/camscanner/pagelist/model/PageImageItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/pagelist/model/PageItem;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    iget-wide p1, p1, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇080:J

    .line 36
    .line 37
    cmp-long v0, v3, p1

    .line 38
    .line 39
    if-nez v0, :cond_0

    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    const/4 v1, 0x0

    .line 43
    goto :goto_0

    .line 44
    :cond_1
    instance-of v0, p1, Lcom/intsig/camscanner/pagelist/model/PageOperationItem;

    .line 45
    .line 46
    if-eqz v0, :cond_2

    .line 47
    .line 48
    instance-of v0, p2, Lcom/intsig/camscanner/pagelist/model/PageOperationItem;

    .line 49
    .line 50
    if-eqz v0, :cond_2

    .line 51
    .line 52
    check-cast p1, Lcom/intsig/camscanner/pagelist/model/PageOperationItem;

    .line 53
    .line 54
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageOperationItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/business/operation/OperateContent;

    .line 55
    .line 56
    .line 57
    move-result-object p1

    .line 58
    invoke-interface {p1}, Lcom/intsig/camscanner/business/operation/OperateContent;->〇080()I

    .line 59
    .line 60
    .line 61
    move-result p1

    .line 62
    check-cast p2, Lcom/intsig/camscanner/pagelist/model/PageOperationItem;

    .line 63
    .line 64
    invoke-virtual {p2}, Lcom/intsig/camscanner/pagelist/model/PageOperationItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/business/operation/OperateContent;

    .line 65
    .line 66
    .line 67
    move-result-object p2

    .line 68
    invoke-interface {p2}, Lcom/intsig/camscanner/business/operation/OperateContent;->〇080()I

    .line 69
    .line 70
    .line 71
    move-result p2

    .line 72
    if-ne p1, p2, :cond_0

    .line 73
    .line 74
    goto :goto_0

    .line 75
    :cond_2
    instance-of v0, p1, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;

    .line 76
    .line 77
    if-eqz v0, :cond_3

    .line 78
    .line 79
    instance-of v0, p2, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;

    .line 80
    .line 81
    if-eqz v0, :cond_3

    .line 82
    .line 83
    check-cast p1, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;

    .line 84
    .line 85
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;->Oo08()J

    .line 86
    .line 87
    .line 88
    move-result-wide v3

    .line 89
    check-cast p2, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;

    .line 90
    .line 91
    invoke-virtual {p2}, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;->Oo08()J

    .line 92
    .line 93
    .line 94
    move-result-wide p1

    .line 95
    cmp-long v0, v3, p1

    .line 96
    .line 97
    if-nez v0, :cond_0

    .line 98
    .line 99
    goto :goto_0

    .line 100
    :cond_3
    instance-of p1, p1, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageBtmOpeTypeItem;

    .line 101
    .line 102
    if-eqz p1, :cond_0

    .line 103
    .line 104
    instance-of p1, p2, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageBtmOpeTypeItem;

    .line 105
    .line 106
    if-eqz p1, :cond_0

    .line 107
    .line 108
    :goto_0
    return v1
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

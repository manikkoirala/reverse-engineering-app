.class public final Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;
.super Ljava/lang/Object;
.source "FuncData.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion$WhenMappings;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final O8(Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;)[F
    .locals 9
    .param p1    # Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "corner"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion$WhenMappings;->〇080:[I

    .line 7
    .line 8
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    aget p1, v0, p1

    .line 13
    .line 14
    const/4 v0, 0x7

    .line 15
    const/4 v1, 0x6

    .line 16
    const/4 v2, 0x5

    .line 17
    const/16 v3, 0x8

    .line 18
    .line 19
    const/4 v4, 0x0

    .line 20
    const/4 v5, 0x4

    .line 21
    const/4 v6, 0x3

    .line 22
    const/4 v7, 0x2

    .line 23
    const/4 v8, 0x1

    .line 24
    if-eq p1, v8, :cond_4

    .line 25
    .line 26
    if-eq p1, v7, :cond_3

    .line 27
    .line 28
    if-eq p1, v6, :cond_1

    .line 29
    .line 30
    if-ne p1, v5, :cond_0

    .line 31
    .line 32
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;->〇080()I

    .line 33
    .line 34
    .line 35
    move-result p1

    .line 36
    new-array p1, p1, [F

    .line 37
    .line 38
    goto/16 :goto_1

    .line 39
    .line 40
    :cond_0
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    .line 41
    .line 42
    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 43
    .line 44
    .line 45
    throw p1

    .line 46
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;->〇080()I

    .line 47
    .line 48
    .line 49
    move-result p1

    .line 50
    new-array v0, p1, [F

    .line 51
    .line 52
    :goto_0
    if-ge v4, p1, :cond_2

    .line 53
    .line 54
    sget-object v1, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->Companion:Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;

    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;->〇o〇()F

    .line 57
    .line 58
    .line 59
    move-result v1

    .line 60
    aput v1, v0, v4

    .line 61
    .line 62
    add-int/lit8 v4, v4, 0x1

    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_2
    move-object p1, v0

    .line 66
    goto :goto_1

    .line 67
    :cond_3
    new-array p1, v3, [F

    .line 68
    .line 69
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;->〇o00〇〇Oo()F

    .line 70
    .line 71
    .line 72
    move-result v3

    .line 73
    aput v3, p1, v4

    .line 74
    .line 75
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;->〇o00〇〇Oo()F

    .line 76
    .line 77
    .line 78
    move-result v3

    .line 79
    aput v3, p1, v8

    .line 80
    .line 81
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;->〇o00〇〇Oo()F

    .line 82
    .line 83
    .line 84
    move-result v3

    .line 85
    aput v3, p1, v7

    .line 86
    .line 87
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;->〇o00〇〇Oo()F

    .line 88
    .line 89
    .line 90
    move-result v3

    .line 91
    aput v3, p1, v6

    .line 92
    .line 93
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;->〇o〇()F

    .line 94
    .line 95
    .line 96
    move-result v3

    .line 97
    aput v3, p1, v5

    .line 98
    .line 99
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;->〇o〇()F

    .line 100
    .line 101
    .line 102
    move-result v3

    .line 103
    aput v3, p1, v2

    .line 104
    .line 105
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;->〇o〇()F

    .line 106
    .line 107
    .line 108
    move-result v2

    .line 109
    aput v2, p1, v1

    .line 110
    .line 111
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;->〇o〇()F

    .line 112
    .line 113
    .line 114
    move-result v1

    .line 115
    aput v1, p1, v0

    .line 116
    .line 117
    goto :goto_1

    .line 118
    :cond_4
    new-array p1, v3, [F

    .line 119
    .line 120
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;->〇o〇()F

    .line 121
    .line 122
    .line 123
    move-result v3

    .line 124
    aput v3, p1, v4

    .line 125
    .line 126
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;->〇o〇()F

    .line 127
    .line 128
    .line 129
    move-result v3

    .line 130
    aput v3, p1, v8

    .line 131
    .line 132
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;->〇o〇()F

    .line 133
    .line 134
    .line 135
    move-result v3

    .line 136
    aput v3, p1, v7

    .line 137
    .line 138
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;->〇o〇()F

    .line 139
    .line 140
    .line 141
    move-result v3

    .line 142
    aput v3, p1, v6

    .line 143
    .line 144
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;->〇o00〇〇Oo()F

    .line 145
    .line 146
    .line 147
    move-result v3

    .line 148
    aput v3, p1, v5

    .line 149
    .line 150
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;->〇o00〇〇Oo()F

    .line 151
    .line 152
    .line 153
    move-result v3

    .line 154
    aput v3, p1, v2

    .line 155
    .line 156
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;->〇o00〇〇Oo()F

    .line 157
    .line 158
    .line 159
    move-result v2

    .line 160
    aput v2, p1, v1

    .line 161
    .line 162
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner$Companion;->〇o00〇〇Oo()F

    .line 163
    .line 164
    .line 165
    move-result v1

    .line 166
    aput v1, p1, v0

    .line 167
    .line 168
    :goto_1
    return-object p1
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public final 〇080()I
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->access$getARRAY_SIZE$cp()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o00〇〇Oo()F
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->access$getDEFAULT_RADIUS$cp()F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o〇()F
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/pagelist/newpagelist/more_dialog_new/data/RoundCorner;->access$getROUND_RADIUS$cp()F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

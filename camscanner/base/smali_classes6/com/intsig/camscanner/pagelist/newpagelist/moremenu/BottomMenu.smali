.class public final Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;
.super Ljava/lang/Object;
.source "BottomMenu.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O8:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final Oo08:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oO80:Z

.field private o〇0:Z

.field private final 〇080:Landroidx/fragment/app/FragmentActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o〇:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇〇888:Z


# direct methods
.method public constructor <init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;)V
    .locals 1
    .param p1    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mActivity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "fragment"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 17
    .line 18
    sget-object p1, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 19
    .line 20
    new-instance p2, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu$mEditBarMenuItems$2;

    .line 21
    .line 22
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu$mEditBarMenuItems$2;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;)V

    .line 23
    .line 24
    .line 25
    invoke-static {p1, p2}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 26
    .line 27
    .line 28
    move-result-object p2

    .line 29
    iput-object p2, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o〇:Lkotlin/Lazy;

    .line 30
    .line 31
    new-instance p2, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu$mBottomMorePopupListMenu$2;

    .line 32
    .line 33
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu$mBottomMorePopupListMenu$2;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;)V

    .line 34
    .line 35
    .line 36
    invoke-static {p1, p2}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->O8:Lkotlin/Lazy;

    .line 41
    .line 42
    new-instance p1, Ljava/util/ArrayList;

    .line 43
    .line 44
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 45
    .line 46
    .line 47
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->Oo08:Ljava/util/List;

    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final o〇0()Lcom/intsig/menu/PopupListMenu;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->O8:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/menu/PopupListMenu;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇080(Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;)Lcom/intsig/menu/PopupMenuItems;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇〇888()Lcom/intsig/menu/PopupMenuItems;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇80〇808〇O(I)V
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇〇888()Lcom/intsig/menu/PopupMenuItems;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/menu/PopupMenuItems;->O8()V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->Oo8oo()Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListViewModel;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListViewModel;->oo0O〇0〇〇〇()Landroidx/lifecycle/MutableLiveData;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-virtual {v0}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    check-cast v0, Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;

    .line 23
    .line 24
    new-instance v1, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu$setMoreMenuItem$1;

    .line 25
    .line 26
    invoke-direct {v1, p0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu$setMoreMenuItem$1;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;I)V

    .line 27
    .line 28
    .line 29
    invoke-static {v0, v1}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker;->〇80〇808〇O(Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;Lkotlin/jvm/functions/Function0;)V

    .line 30
    .line 31
    .line 32
    new-instance p1, Lcom/intsig/menu/MenuItem;

    .line 33
    .line 34
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 35
    .line 36
    const v2, 0x7f130d48

    .line 37
    .line 38
    .line 39
    invoke-virtual {v1, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    const/16 v2, 0xa

    .line 44
    .line 45
    invoke-direct {p1, v2, v1}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;)V

    .line 46
    .line 47
    .line 48
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇〇888()Lcom/intsig/menu/PopupMenuItems;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    invoke-virtual {v1, p1}, Lcom/intsig/menu/PopupMenuItems;->〇o00〇〇Oo(Lcom/intsig/menu/MenuItem;)V

    .line 53
    .line 54
    .line 55
    new-instance p1, Lcom/intsig/menu/MenuItem;

    .line 56
    .line 57
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 58
    .line 59
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    invoke-virtual {v2}, Lcom/intsig/tsapp/sync/AppConfigJson;->isSendFaxOn()Z

    .line 64
    .line 65
    .line 66
    move-result v2

    .line 67
    if-eqz v2, :cond_0

    .line 68
    .line 69
    const v2, 0x7f13022a

    .line 70
    .line 71
    .line 72
    goto :goto_0

    .line 73
    :cond_0
    const v2, 0x7f131f87

    .line 74
    .line 75
    .line 76
    :goto_0
    invoke-virtual {v1, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object v1

    .line 80
    const/4 v2, 0x5

    .line 81
    invoke-direct {p1, v2, v1}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;)V

    .line 82
    .line 83
    .line 84
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇〇888()Lcom/intsig/menu/PopupMenuItems;

    .line 85
    .line 86
    .line 87
    move-result-object v1

    .line 88
    invoke-virtual {v1, p1}, Lcom/intsig/menu/PopupMenuItems;->〇o00〇〇Oo(Lcom/intsig/menu/MenuItem;)V

    .line 89
    .line 90
    .line 91
    invoke-static {}, Lcom/intsig/camscanner/tsapp/account/util/AccountUtil;->〇80〇808〇O()Z

    .line 92
    .line 93
    .line 94
    move-result p1

    .line 95
    if-eqz p1, :cond_1

    .line 96
    .line 97
    sget-object p1, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->〇80〇:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew$Companion;

    .line 98
    .line 99
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew$Companion;->〇080()Ljava/lang/String;

    .line 100
    .line 101
    .line 102
    move-result-object p1

    .line 103
    const-string v0, "addCompositeItem>>> is illegal !!!"

    .line 104
    .line 105
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    goto :goto_1

    .line 109
    :cond_1
    invoke-static {v0}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker;->oO80(Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;)Z

    .line 110
    .line 111
    .line 112
    move-result p1

    .line 113
    if-nez p1, :cond_2

    .line 114
    .line 115
    new-instance p1, Lcom/intsig/menu/MenuItem;

    .line 116
    .line 117
    const/4 v1, 0x6

    .line 118
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 119
    .line 120
    const v2, 0x7f130113

    .line 121
    .line 122
    .line 123
    invoke-virtual {v0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 124
    .line 125
    .line 126
    move-result-object v2

    .line 127
    const/4 v3, -0x1

    .line 128
    const/4 v4, 0x0

    .line 129
    const v5, 0x7f080d9f

    .line 130
    .line 131
    .line 132
    move-object v0, p1

    .line 133
    invoke-direct/range {v0 .. v5}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;IZI)V

    .line 134
    .line 135
    .line 136
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇〇888()Lcom/intsig/menu/PopupMenuItems;

    .line 137
    .line 138
    .line 139
    move-result-object v0

    .line 140
    invoke-virtual {v0, p1}, Lcom/intsig/menu/PopupMenuItems;->〇o00〇〇Oo(Lcom/intsig/menu/MenuItem;)V

    .line 141
    .line 142
    .line 143
    :cond_2
    :goto_1
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->o〇0()Z

    .line 144
    .line 145
    .line 146
    move-result p1

    .line 147
    if-eqz p1, :cond_3

    .line 148
    .line 149
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇80〇808〇O()Z

    .line 150
    .line 151
    .line 152
    move-result p1

    .line 153
    if-nez p1, :cond_3

    .line 154
    .line 155
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->OO〇0008O8()Z

    .line 156
    .line 157
    .line 158
    move-result p1

    .line 159
    if-eqz p1, :cond_3

    .line 160
    .line 161
    new-instance p1, Lcom/intsig/menu/MenuItem;

    .line 162
    .line 163
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 164
    .line 165
    const v1, 0x7f130221

    .line 166
    .line 167
    .line 168
    invoke-virtual {v0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 169
    .line 170
    .line 171
    move-result-object v0

    .line 172
    const/4 v1, 0x7

    .line 173
    invoke-direct {p1, v1, v0}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;)V

    .line 174
    .line 175
    .line 176
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇〇888()Lcom/intsig/menu/PopupMenuItems;

    .line 177
    .line 178
    .line 179
    move-result-object v0

    .line 180
    invoke-virtual {v0, p1}, Lcom/intsig/menu/PopupMenuItems;->〇o00〇〇Oo(Lcom/intsig/menu/MenuItem;)V

    .line 181
    .line 182
    .line 183
    :cond_3
    return-void
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private final 〇〇888()Lcom/intsig/menu/PopupMenuItems;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o〇:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/menu/PopupMenuItems;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final O8()Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final OO0o〇〇(Z)V
    .locals 4

    .line 1
    const-string v0, "fragment.mBottomToolbarBinding.root"

    .line 2
    .line 3
    if-eqz p1, :cond_2

    .line 4
    .line 5
    iget-boolean p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇〇888:Z

    .line 6
    .line 7
    if-nez p1, :cond_0

    .line 8
    .line 9
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->o〇O0ooo()Lcom/intsig/camscanner/databinding/FragmentPageListNewBinding;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/FragmentPageListNewBinding;->〇080()Landroid/widget/RelativeLayout;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    const v1, 0x7f0a005d

    .line 20
    .line 21
    .line 22
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 27
    .line 28
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    const-string v2, "bind(view)"

    .line 33
    .line 34
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;->〇080OO8〇0:Lcom/intsig/view/ImageTextButton;

    .line 38
    .line 39
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 40
    .line 41
    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    .line 43
    .line 44
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;->O8o08O8O:Lcom/intsig/view/ImageTextButton;

    .line 45
    .line 46
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 47
    .line 48
    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    .line 50
    .line 51
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;->o〇00O:Lcom/intsig/view/ImageTextButton;

    .line 52
    .line 53
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 54
    .line 55
    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    .line 57
    .line 58
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;->OO:Lcom/intsig/view/ImageTextButton;

    .line 59
    .line 60
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 61
    .line 62
    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    .line 64
    .line 65
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;->〇08O〇00〇o:Lcom/intsig/view/ImageTextButton;

    .line 66
    .line 67
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 68
    .line 69
    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    .line 71
    .line 72
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;->〇080OO8〇0:Lcom/intsig/view/ImageTextButton;

    .line 73
    .line 74
    invoke-virtual {v2}, Lcom/intsig/view/ImageTextButton;->OO0o〇〇()V

    .line 75
    .line 76
    .line 77
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;->O8o08O8O:Lcom/intsig/view/ImageTextButton;

    .line 78
    .line 79
    invoke-virtual {v2}, Lcom/intsig/view/ImageTextButton;->OO0o〇〇()V

    .line 80
    .line 81
    .line 82
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;->o〇00O:Lcom/intsig/view/ImageTextButton;

    .line 83
    .line 84
    invoke-virtual {v2}, Lcom/intsig/view/ImageTextButton;->OO0o〇〇()V

    .line 85
    .line 86
    .line 87
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;->OO:Lcom/intsig/view/ImageTextButton;

    .line 88
    .line 89
    invoke-virtual {v2}, Lcom/intsig/view/ImageTextButton;->OO0o〇〇()V

    .line 90
    .line 91
    .line 92
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;->〇08O〇00〇o:Lcom/intsig/view/ImageTextButton;

    .line 93
    .line 94
    invoke-virtual {v2}, Lcom/intsig/view/ImageTextButton;->OO0o〇〇()V

    .line 95
    .line 96
    .line 97
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->〇88〇o(Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;)V

    .line 98
    .line 99
    .line 100
    const/4 p1, 0x1

    .line 101
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇〇888:Z

    .line 102
    .line 103
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 104
    .line 105
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->〇〇Ooo0o()Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;

    .line 106
    .line 107
    .line 108
    move-result-object v1

    .line 109
    invoke-virtual {v1}, Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;->〇080()Landroid/widget/LinearLayout;

    .line 110
    .line 111
    .line 112
    move-result-object v1

    .line 113
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    .line 115
    .line 116
    invoke-virtual {p1, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->OoO888(Landroid/view/View;)V

    .line 117
    .line 118
    .line 119
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->Oo08:Ljava/util/List;

    .line 120
    .line 121
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 122
    .line 123
    .line 124
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->Oo08:Ljava/util/List;

    .line 125
    .line 126
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 127
    .line 128
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->〇〇Ooo0o()Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;

    .line 129
    .line 130
    .line 131
    move-result-object v0

    .line 132
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;->〇080OO8〇0:Lcom/intsig/view/ImageTextButton;

    .line 133
    .line 134
    const-string v1, "fragment.mBottomToolbarBinding.itbBottomShare"

    .line 135
    .line 136
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 137
    .line 138
    .line 139
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 140
    .line 141
    .line 142
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->Oo08:Ljava/util/List;

    .line 143
    .line 144
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 145
    .line 146
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->〇〇Ooo0o()Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;

    .line 147
    .line 148
    .line 149
    move-result-object v0

    .line 150
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;->O8o08O8O:Lcom/intsig/view/ImageTextButton;

    .line 151
    .line 152
    const-string v1, "fragment.mBottomToolbarB\u2026ding.itbBottomSaveGallery"

    .line 153
    .line 154
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    .line 156
    .line 157
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    .line 159
    .line 160
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->Oo08:Ljava/util/List;

    .line 161
    .line 162
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 163
    .line 164
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->〇〇Ooo0o()Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;

    .line 165
    .line 166
    .line 167
    move-result-object v0

    .line 168
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;->o〇00O:Lcom/intsig/view/ImageTextButton;

    .line 169
    .line 170
    const-string v1, "fragment.mBottomToolbarBinding.itbBottomMove"

    .line 171
    .line 172
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 173
    .line 174
    .line 175
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    .line 177
    .line 178
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->Oo08:Ljava/util/List;

    .line 179
    .line 180
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 181
    .line 182
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->〇〇Ooo0o()Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;

    .line 183
    .line 184
    .line 185
    move-result-object v0

    .line 186
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;->OO:Lcom/intsig/view/ImageTextButton;

    .line 187
    .line 188
    const-string v1, "fragment.mBottomToolbarBinding.itbBottomDelete"

    .line 189
    .line 190
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    .line 192
    .line 193
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    .line 195
    .line 196
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->Oo08:Ljava/util/List;

    .line 197
    .line 198
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 199
    .line 200
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->〇〇Ooo0o()Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;

    .line 201
    .line 202
    .line 203
    move-result-object v0

    .line 204
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;->〇08O〇00〇o:Lcom/intsig/view/ImageTextButton;

    .line 205
    .line 206
    const-string v1, "fragment.mBottomToolbarBinding.itbBottomMore"

    .line 207
    .line 208
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 209
    .line 210
    .line 211
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    .line 213
    .line 214
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 215
    .line 216
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->Oo8oo()Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListViewModel;

    .line 217
    .line 218
    .line 219
    move-result-object p1

    .line 220
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListViewModel;->oo0O〇0〇〇〇()Landroidx/lifecycle/MutableLiveData;

    .line 221
    .line 222
    .line 223
    move-result-object p1

    .line 224
    invoke-virtual {p1}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 225
    .line 226
    .line 227
    move-result-object p1

    .line 228
    check-cast p1, Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;

    .line 229
    .line 230
    invoke-static {p1}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker;->〇〇888(Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;)Z

    .line 231
    .line 232
    .line 233
    move-result v0

    .line 234
    if-eqz v0, :cond_1

    .line 235
    .line 236
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 237
    .line 238
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->〇〇Ooo0o()Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;

    .line 239
    .line 240
    .line 241
    move-result-object v0

    .line 242
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;->o〇00O:Lcom/intsig/view/ImageTextButton;

    .line 243
    .line 244
    invoke-virtual {v0}, Lcom/intsig/view/ImageTextButton;->〇8o8o〇()V

    .line 245
    .line 246
    .line 247
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 248
    .line 249
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->Oo8oo()Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListViewModel;

    .line 250
    .line 251
    .line 252
    move-result-object v0

    .line 253
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListViewModel;->o〇〇0〇88()Z

    .line 254
    .line 255
    .line 256
    move-result v0

    .line 257
    invoke-static {p1, v0}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker;->o〇0(Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;Z)Z

    .line 258
    .line 259
    .line 260
    move-result p1

    .line 261
    if-eqz p1, :cond_3

    .line 262
    .line 263
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 264
    .line 265
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->〇〇Ooo0o()Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;

    .line 266
    .line 267
    .line 268
    move-result-object p1

    .line 269
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;->OO:Lcom/intsig/view/ImageTextButton;

    .line 270
    .line 271
    invoke-virtual {p1}, Lcom/intsig/view/ImageTextButton;->〇8o8o〇()V

    .line 272
    .line 273
    .line 274
    goto :goto_0

    .line 275
    :cond_2
    iget-boolean p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇〇888:Z

    .line 276
    .line 277
    if-eqz p1, :cond_3

    .line 278
    .line 279
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 280
    .line 281
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->〇〇Ooo0o()Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;

    .line 282
    .line 283
    .line 284
    move-result-object p1

    .line 285
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/DocBottombarPrivateBinding;->〇080()Landroid/widget/LinearLayout;

    .line 286
    .line 287
    .line 288
    move-result-object p1

    .line 289
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 290
    .line 291
    .line 292
    const/16 v0, 0x8

    .line 293
    .line 294
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 295
    .line 296
    .line 297
    :cond_3
    :goto_0
    return-void
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public final OO0o〇〇〇〇0(Landroid/view/View;I)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "anchorView"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇80〇808〇O(I)V

    .line 7
    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->o〇0()Lcom/intsig/menu/PopupListMenu;

    .line 10
    .line 11
    .line 12
    move-result-object p2

    .line 13
    invoke-virtual {p2, p1}, Lcom/intsig/menu/PopupListMenu;->〇00(Landroid/view/View;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final Oo08()Landroidx/fragment/app/FragmentActivity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oO80(I)V
    .locals 1

    .line 1
    const-string v0, "CSList"

    .line 2
    .line 3
    packed-switch p1, :pswitch_data_0

    .line 4
    .line 5
    .line 6
    :pswitch_0
    goto :goto_0

    .line 7
    :pswitch_1
    const-string p1, "smart_print"

    .line 8
    .line 9
    invoke-static {v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    goto :goto_0

    .line 13
    :pswitch_2
    const-string p1, "CSMore"

    .line 14
    .line 15
    const-string v0, "batch_process_image"

    .line 16
    .line 17
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :pswitch_3
    const-string p1, "document_more_evidence"

    .line 22
    .line 23
    invoke-static {v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    goto :goto_0

    .line 27
    :pswitch_4
    const-string p1, "document_more_collage"

    .line 28
    .line 29
    invoke-static {v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :pswitch_5
    const-string p1, "document_more_upload/print/fax"

    .line 34
    .line 35
    invoke-static {v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :pswitch_6
    const-string p1, "document_delete"

    .line 40
    .line 41
    invoke-static {v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    goto :goto_0

    .line 45
    :pswitch_7
    const-string p1, "document_more_copy"

    .line 46
    .line 47
    invoke-static {v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :pswitch_8
    const-string p1, "document_move"

    .line 52
    .line 53
    invoke-static {v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :pswitch_9
    const-string p1, " document_save"

    .line 58
    .line 59
    invoke-static {v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    goto :goto_0

    .line 63
    :pswitch_a
    const-string p1, "document_share"

    .line 64
    .line 65
    invoke-static {v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    :goto_0
    return-void

    .line 69
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public final 〇8o8o〇(Z)V
    .locals 3

    .line 1
    const-string v0, "fragment.mBottomExtractToolbarBinding.root"

    .line 2
    .line 3
    if-eqz p1, :cond_1

    .line 4
    .line 5
    iget-boolean p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->oO80:Z

    .line 6
    .line 7
    if-nez p1, :cond_0

    .line 8
    .line 9
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->o〇O0ooo()Lcom/intsig/camscanner/databinding/FragmentPageListNewBinding;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/FragmentPageListNewBinding;->〇080()Landroid/widget/RelativeLayout;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    const v1, 0x7f0a005e

    .line 20
    .line 21
    .line 22
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 27
    .line 28
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/DocBottombarPrivateExtractBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/DocBottombarPrivateExtractBinding;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    const-string v2, "bind(view)"

    .line 33
    .line 34
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->O8〇O〇O0(Lcom/intsig/camscanner/databinding/DocBottombarPrivateExtractBinding;)V

    .line 38
    .line 39
    .line 40
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 41
    .line 42
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->Oo088O〇8〇()Lcom/intsig/camscanner/databinding/DocBottombarPrivateExtractBinding;

    .line 43
    .line 44
    .line 45
    move-result-object p1

    .line 46
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DocBottombarPrivateExtractBinding;->OO:Landroid/widget/TextView;

    .line 47
    .line 48
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 49
    .line 50
    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    .line 52
    .line 53
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 54
    .line 55
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->Oo088O〇8〇()Lcom/intsig/camscanner/databinding/DocBottombarPrivateExtractBinding;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DocBottombarPrivateExtractBinding;->OO:Landroid/widget/TextView;

    .line 60
    .line 61
    invoke-virtual {p1}, Landroid/widget/TextView;->setSingleLine()V

    .line 62
    .line 63
    .line 64
    const/4 p1, 0x1

    .line 65
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->oO80:Z

    .line 66
    .line 67
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 68
    .line 69
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->Oo088O〇8〇()Lcom/intsig/camscanner/databinding/DocBottombarPrivateExtractBinding;

    .line 70
    .line 71
    .line 72
    move-result-object p1

    .line 73
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/DocBottombarPrivateExtractBinding;->〇080()Landroid/widget/LinearLayout;

    .line 74
    .line 75
    .line 76
    move-result-object p1

    .line 77
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 78
    .line 79
    .line 80
    const/4 v0, 0x0

    .line 81
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 82
    .line 83
    .line 84
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->Oo08:Ljava/util/List;

    .line 85
    .line 86
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 87
    .line 88
    .line 89
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->Oo08:Ljava/util/List;

    .line 90
    .line 91
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 92
    .line 93
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->Oo088O〇8〇()Lcom/intsig/camscanner/databinding/DocBottombarPrivateExtractBinding;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DocBottombarPrivateExtractBinding;->OO:Landroid/widget/TextView;

    .line 98
    .line 99
    const-string v1, "fragment.mBottomExtractToolbarBinding.tvPdfExtract"

    .line 100
    .line 101
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 102
    .line 103
    .line 104
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    .line 106
    .line 107
    goto :goto_0

    .line 108
    :cond_1
    iget-boolean p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->oO80:Z

    .line 109
    .line 110
    if-eqz p1, :cond_2

    .line 111
    .line 112
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 113
    .line 114
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->Oo088O〇8〇()Lcom/intsig/camscanner/databinding/DocBottombarPrivateExtractBinding;

    .line 115
    .line 116
    .line 117
    move-result-object p1

    .line 118
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/DocBottombarPrivateExtractBinding;->〇080()Landroid/widget/LinearLayout;

    .line 119
    .line 120
    .line 121
    move-result-object p1

    .line 122
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 123
    .line 124
    .line 125
    const/16 v0, 0x8

    .line 126
    .line 127
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 128
    .line 129
    .line 130
    :cond_2
    :goto_0
    return-void
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public final 〇O8o08O(Z)V
    .locals 4

    .line 1
    const-string v0, "fragment.mBottomMoveToolbarBinding.root"

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    iget-boolean p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->o〇0:Z

    .line 6
    .line 7
    if-nez p1, :cond_1

    .line 8
    .line 9
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->o〇O0ooo()Lcom/intsig/camscanner/databinding/FragmentPageListNewBinding;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/FragmentPageListNewBinding;->〇080()Landroid/widget/RelativeLayout;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    const v1, 0x7f0a005f

    .line 20
    .line 21
    .line 22
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 27
    .line 28
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    const-string v2, "bind(view)"

    .line 33
    .line 34
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;->O8o08O8O:Lcom/intsig/view/ImageTextButton;

    .line 38
    .line 39
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 40
    .line 41
    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    .line 43
    .line 44
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;->OO:Lcom/intsig/view/ImageTextButton;

    .line 45
    .line 46
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 47
    .line 48
    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    .line 50
    .line 51
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;->o〇00O:Lcom/intsig/view/ImageTextButton;

    .line 52
    .line 53
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 54
    .line 55
    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    .line 57
    .line 58
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;->〇08O〇00〇o:Lcom/intsig/view/ImageTextButton;

    .line 59
    .line 60
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 61
    .line 62
    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    .line 64
    .line 65
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 66
    .line 67
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 68
    .line 69
    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    .line 71
    .line 72
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;->O8o08O8O:Lcom/intsig/view/ImageTextButton;

    .line 73
    .line 74
    invoke-virtual {v2}, Lcom/intsig/view/ImageTextButton;->OO0o〇〇()V

    .line 75
    .line 76
    .line 77
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;->OO:Lcom/intsig/view/ImageTextButton;

    .line 78
    .line 79
    invoke-virtual {v2}, Lcom/intsig/view/ImageTextButton;->OO0o〇〇()V

    .line 80
    .line 81
    .line 82
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;->o〇00O:Lcom/intsig/view/ImageTextButton;

    .line 83
    .line 84
    invoke-virtual {v2}, Lcom/intsig/view/ImageTextButton;->OO0o〇〇()V

    .line 85
    .line 86
    .line 87
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;->〇08O〇00〇o:Lcom/intsig/view/ImageTextButton;

    .line 88
    .line 89
    invoke-virtual {v2}, Lcom/intsig/view/ImageTextButton;->OO0o〇〇()V

    .line 90
    .line 91
    .line 92
    const/4 v2, 0x1

    .line 93
    iput-boolean v2, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->o〇0:Z

    .line 94
    .line 95
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->O8〇8O(Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;)V

    .line 96
    .line 97
    .line 98
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 99
    .line 100
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->O8080〇O8o()Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;

    .line 101
    .line 102
    .line 103
    move-result-object p1

    .line 104
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;->〇080()Landroid/widget/LinearLayout;

    .line 105
    .line 106
    .line 107
    move-result-object p1

    .line 108
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    .line 110
    .line 111
    const/4 v0, 0x0

    .line 112
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 113
    .line 114
    .line 115
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->Oo08:Ljava/util/List;

    .line 116
    .line 117
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 118
    .line 119
    .line 120
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->Oo08:Ljava/util/List;

    .line 121
    .line 122
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 123
    .line 124
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->O8080〇O8o()Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;

    .line 125
    .line 126
    .line 127
    move-result-object v0

    .line 128
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;->O8o08O8O:Lcom/intsig/view/ImageTextButton;

    .line 129
    .line 130
    const-string v1, "fragment.mBottomMoveTool\u2026ding.itbBottomPdfKitShare"

    .line 131
    .line 132
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 133
    .line 134
    .line 135
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    .line 137
    .line 138
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->Oo08:Ljava/util/List;

    .line 139
    .line 140
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 141
    .line 142
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->O8080〇O8o()Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;

    .line 143
    .line 144
    .line 145
    move-result-object v0

    .line 146
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;->OO:Lcom/intsig/view/ImageTextButton;

    .line 147
    .line 148
    const-string v1, "fragment.mBottomMoveTool\u2026inding.itbBottomPdfKitAdd"

    .line 149
    .line 150
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 151
    .line 152
    .line 153
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 154
    .line 155
    .line 156
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->Oo08:Ljava/util/List;

    .line 157
    .line 158
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 159
    .line 160
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->O8080〇O8o()Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;

    .line 161
    .line 162
    .line 163
    move-result-object v0

    .line 164
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;->o〇00O:Lcom/intsig/view/ImageTextButton;

    .line 165
    .line 166
    const-string v1, "fragment.mBottomMoveTool\u2026ng.itbBottomPdfKitExtract"

    .line 167
    .line 168
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 169
    .line 170
    .line 171
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    .line 173
    .line 174
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->Oo08:Ljava/util/List;

    .line 175
    .line 176
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 177
    .line 178
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->O8080〇O8o()Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;

    .line 179
    .line 180
    .line 181
    move-result-object v0

    .line 182
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;->〇08O〇00〇o:Lcom/intsig/view/ImageTextButton;

    .line 183
    .line 184
    const-string v1, "fragment.mBottomMoveTool\u2026ing.itbBottomPdfKitDelete"

    .line 185
    .line 186
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 187
    .line 188
    .line 189
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 190
    .line 191
    .line 192
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->Oo08:Ljava/util/List;

    .line 193
    .line 194
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 195
    .line 196
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->O8080〇O8o()Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;

    .line 197
    .line 198
    .line 199
    move-result-object v0

    .line 200
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 201
    .line 202
    const-string v1, "fragment.mBottomMoveTool\u2026ing.itbBottomPdfKitSubmit"

    .line 203
    .line 204
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 205
    .line 206
    .line 207
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    .line 209
    .line 210
    goto :goto_0

    .line 211
    :cond_0
    iget-boolean p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->o〇0:Z

    .line 212
    .line 213
    if-eqz p1, :cond_1

    .line 214
    .line 215
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 216
    .line 217
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->O8080〇O8o()Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;

    .line 218
    .line 219
    .line 220
    move-result-object p1

    .line 221
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/DocBottombarPrivateMoveBinding;->〇080()Landroid/widget/LinearLayout;

    .line 222
    .line 223
    .line 224
    move-result-object p1

    .line 225
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 226
    .line 227
    .line 228
    const/16 v0, 0x8

    .line 229
    .line 230
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 231
    .line 232
    .line 233
    :cond_1
    :goto_0
    return-void
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public final 〇o00〇〇Oo()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->Oo08:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o〇(Z)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/base/ToolbarThemeGet;->〇080:Lcom/intsig/base/ToolbarThemeGet;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/intsig/base/ToolbarThemeGet;->〇o〇()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 14
    .line 15
    const v2, 0x7f06007e

    .line 16
    .line 17
    .line 18
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/newpagelist/moremenu/BottomMenu;->Oo08:Ljava/util/List;

    .line 23
    .line 24
    check-cast v2, Ljava/lang/Iterable;

    .line 25
    .line 26
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 31
    .line 32
    .line 33
    move-result v3

    .line 34
    if-eqz v3, :cond_2

    .line 35
    .line 36
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    move-result-object v3

    .line 40
    check-cast v3, Landroid/view/View;

    .line 41
    .line 42
    invoke-virtual {v3, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 43
    .line 44
    .line 45
    instance-of v4, v3, Lcom/intsig/view/ImageTextButton;

    .line 46
    .line 47
    if-eqz v4, :cond_0

    .line 48
    .line 49
    if-eqz p1, :cond_1

    .line 50
    .line 51
    move v4, v0

    .line 52
    goto :goto_1

    .line 53
    :cond_1
    move v4, v1

    .line 54
    :goto_1
    check-cast v3, Lcom/intsig/view/ImageTextButton;

    .line 55
    .line 56
    invoke-virtual {v3, v4}, Lcom/intsig/view/ImageTextButton;->setIconAndTextColor(I)V

    .line 57
    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_2
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

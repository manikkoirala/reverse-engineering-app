.class public final Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;
.super Ljava/lang/Object;
.source "DialogManager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final oO80:Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇80〇808〇O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8:Landroidx/fragment/app/FragmentActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final Oo08:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray<",
            "Landroid/app/Dialog;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇0:Z

.field private final 〇080:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o〇:Z

.field private 〇〇888:Landroidx/fragment/app/DialogFragment;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->oO80:Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager$Companion;

    .line 8
    .line 9
    const-class v0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "DialogManager::class.java.simpleName"

    .line 16
    .line 17
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇80〇808〇O:Ljava/lang/String;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>(Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;Landroidx/fragment/app/FragmentManager;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroidx/fragment/app/FragmentManager;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mFragment"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "mFragmentManager"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇080:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 17
    .line 18
    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    .line 19
    .line 20
    .line 21
    move-result-object p2

    .line 22
    const-string v0, "mFragment.requireActivity()"

    .line 23
    .line 24
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    iput-object p2, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->O8:Landroidx/fragment/app/FragmentActivity;

    .line 28
    .line 29
    new-instance p2, Landroid/util/SparseArray;

    .line 30
    .line 31
    invoke-direct {p2}, Landroid/util/SparseArray;-><init>()V

    .line 32
    .line 33
    .line 34
    iput-object p2, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->Oo08:Landroid/util/SparseArray;

    .line 35
    .line 36
    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    invoke-interface {p1}, Landroidx/lifecycle/LifecycleOwner;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    new-instance p2, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager$1;

    .line 45
    .line 46
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager$1;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {p1, p2}, Landroidx/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 50
    .line 51
    .line 52
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic O8(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;Lcom/intsig/camscanner/question/mode/NPSActionDataGroup;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->oo88o8O(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;Lcom/intsig/camscanner/question/mode/NPSActionDataGroup;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final O8ooOoo〇(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;Lkotlin/jvm/functions/Function0;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 p2, 0x0

    .line 7
    invoke-virtual {p0, p2}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇oo〇(Z)V

    .line 8
    .line 9
    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    invoke-interface {p1}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 13
    .line 14
    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇oOO8O8(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic Oo08(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇〇0o(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final o8(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇oo〇(Z)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic oO80(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇8(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final oo88o8O(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;Lcom/intsig/camscanner/question/mode/NPSActionDataGroup;)V
    .locals 2

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-boolean p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->o〇0:Z

    .line 7
    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇O00()Z

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    if-eqz p1, :cond_1

    .line 16
    .line 17
    return-void

    .line 18
    :cond_1
    new-instance p1, Landroid/os/Bundle;

    .line 19
    .line 20
    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 21
    .line 22
    .line 23
    const-string v0, "extra_from_import"

    .line 24
    .line 25
    const-string v1, "cs_list"

    .line 26
    .line 27
    invoke-virtual {p1, v0, v1}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    new-instance v0, Lcom/intsig/utils/IntentBuilder;

    .line 31
    .line 32
    invoke-direct {v0}, Lcom/intsig/utils/IntentBuilder;-><init>()V

    .line 33
    .line 34
    .line 35
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇080:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 36
    .line 37
    invoke-virtual {v0, v1}, Lcom/intsig/utils/IntentBuilder;->〇O8o08O(Landroidx/fragment/app/Fragment;)Lcom/intsig/utils/IntentBuilder;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    const-class v1, Lcom/intsig/camscanner/question/nps/NPSDialogActivity;

    .line 42
    .line 43
    invoke-virtual {v0, v1}, Lcom/intsig/utils/IntentBuilder;->〇〇888(Ljava/lang/Class;)Lcom/intsig/utils/IntentBuilder;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-virtual {v0, p1}, Lcom/intsig/utils/IntentBuilder;->o〇0(Landroid/os/Bundle;)Lcom/intsig/utils/IntentBuilder;

    .line 48
    .line 49
    .line 50
    move-result-object p1

    .line 51
    const/16 v0, 0x3f8

    .line 52
    .line 53
    invoke-virtual {p1, v0}, Lcom/intsig/utils/IntentBuilder;->oO80(I)Lcom/intsig/utils/IntentBuilder;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    invoke-virtual {p1}, Lcom/intsig/utils/IntentBuilder;->〇80〇808〇O()V

    .line 58
    .line 59
    .line 60
    const/4 p1, 0x1

    .line 61
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇oo〇(Z)V

    .line 62
    .line 63
    .line 64
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic oo〇(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;Ljava/lang/String;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p3, p3, 0x2

    .line 2
    .line 3
    if-eqz p3, :cond_0

    .line 4
    .line 5
    const/4 p2, 0x0

    .line 6
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->OOO〇O0(Ljava/lang/String;Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method public static synthetic o〇0(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇〇808〇(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final o〇〇0〇()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇80〇808〇O:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "showCNRenewRecallDialog on dismiss"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final 〇00()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇80〇808〇O:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "showAnnualPremiumDialog on dismiss"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->o8(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇8(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇80〇808〇O:Ljava/lang/String;

    .line 7
    .line 8
    const-string v1, "showVipActivityPurchaseDialog on dismiss"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->o〇0:Z

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇80〇808〇O(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇8〇0〇o〇O(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇8o8o〇(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)Landroid/util/SparseArray;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->Oo08:Landroid/util/SparseArray;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇8〇0〇o〇O(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇80〇808〇O:Ljava/lang/String;

    .line 7
    .line 8
    const-string v1, "showVipMonthPromotionDialog on dismiss"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->o〇0:Z

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇O00()Z
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇o〇:Z

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    return v1

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 8
    .line 9
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->getFragments()Ljava/util/List;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const-string v2, "mFragmentManager.fragments"

    .line 14
    .line 15
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    check-cast v0, Ljava/lang/Iterable;

    .line 19
    .line 20
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    if-eqz v2, :cond_2

    .line 29
    .line 30
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    check-cast v2, Landroidx/fragment/app/Fragment;

    .line 35
    .line 36
    instance-of v2, v2, Landroidx/fragment/app/DialogFragment;

    .line 37
    .line 38
    if-eqz v2, :cond_1

    .line 39
    .line 40
    return v1

    .line 41
    :cond_2
    const/4 v0, 0x0

    .line 42
    return v0
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic 〇O8o08O()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇80〇808〇O:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇O〇(I)Landroid/app/Dialog;
    .locals 3

    .line 1
    const/4 v0, -0x2

    .line 2
    const/4 v1, 0x0

    .line 3
    if-eq p1, v0, :cond_3

    .line 4
    .line 5
    const/16 v0, 0x64

    .line 6
    .line 7
    if-eq p1, v0, :cond_2

    .line 8
    .line 9
    const/16 v0, 0x6c

    .line 10
    .line 11
    const/4 v2, 0x0

    .line 12
    if-eq p1, v0, :cond_1

    .line 13
    .line 14
    const/16 v0, 0x458

    .line 15
    .line 16
    if-eq p1, v0, :cond_0

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->O8:Landroidx/fragment/app/FragmentActivity;

    .line 20
    .line 21
    const v0, 0x7f131cfa

    .line 22
    .line 23
    .line 24
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-static {p1, v0, v1, v1}, Lcom/intsig/camscanner/app/AppUtil;->OOO〇O0(Landroid/content/Context;Ljava/lang/String;ZI)Lcom/intsig/app/BaseProgressDialog;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    goto :goto_0

    .line 33
    :cond_1
    new-instance p1, Lcom/intsig/app/AlertDialog$Builder;

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->O8:Landroidx/fragment/app/FragmentActivity;

    .line 36
    .line 37
    invoke-direct {p1, v0}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 38
    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->O8:Landroidx/fragment/app/FragmentActivity;

    .line 41
    .line 42
    const v1, 0x7f131d1a

    .line 43
    .line 44
    .line 45
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    invoke-virtual {p1, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇〇〇0〇〇0(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->O8:Landroidx/fragment/app/FragmentActivity;

    .line 54
    .line 55
    const v1, 0x7f131e4e

    .line 56
    .line 57
    .line 58
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    invoke-virtual {p1, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇O00(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 63
    .line 64
    .line 65
    move-result-object p1

    .line 66
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->O8:Landroidx/fragment/app/FragmentActivity;

    .line 67
    .line 68
    const v1, 0x7f131e36

    .line 69
    .line 70
    .line 71
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    invoke-virtual {p1, v0, v2}, Lcom/intsig/app/AlertDialog$Builder;->oo〇(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 76
    .line 77
    .line 78
    move-result-object p1

    .line 79
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 80
    .line 81
    .line 82
    move-result-object v2

    .line 83
    goto :goto_0

    .line 84
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->O8:Landroidx/fragment/app/FragmentActivity;

    .line 85
    .line 86
    const v0, 0x7f1305ba

    .line 87
    .line 88
    .line 89
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 90
    .line 91
    .line 92
    move-result-object v0

    .line 93
    const/4 v2, 0x1

    .line 94
    invoke-static {p1, v0, v1, v2}, Lcom/intsig/camscanner/app/AppUtil;->OOO〇O0(Landroid/content/Context;Ljava/lang/String;ZI)Lcom/intsig/app/BaseProgressDialog;

    .line 95
    .line 96
    .line 97
    move-result-object v2

    .line 98
    goto :goto_0

    .line 99
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->O8:Landroidx/fragment/app/FragmentActivity;

    .line 100
    .line 101
    const v0, 0x7f1300a3

    .line 102
    .line 103
    .line 104
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 105
    .line 106
    .line 107
    move-result-object v0

    .line 108
    invoke-static {p1, v0, v1, v1}, Lcom/intsig/camscanner/app/AppUtil;->OOO〇O0(Landroid/content/Context;Ljava/lang/String;ZI)Lcom/intsig/app/BaseProgressDialog;

    .line 109
    .line 110
    .line 111
    move-result-object v2

    .line 112
    :goto_0
    return-object v2
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;Lkotlin/jvm/functions/Function0;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->O8ooOoo〇(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;Lkotlin/jvm/functions/Function0;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final 〇oOO8O8(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇oo〇(Z)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇o〇()V
    .locals 0

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->o〇〇0〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final 〇〇0o(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇oo〇(Z)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇〇808〇(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "AreaFreeActivityManager"

    .line 7
    .line 8
    const-string v1, "area free share dialog dismiss"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇oo〇(Z)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇〇888()V
    .locals 0

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇00()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final O08000()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇O00()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇80〇808〇O:Ljava/lang/String;

    .line 8
    .line 9
    const-string v1, "showVipMonthPromotionDialog is showing now, can not show again"

    .line 10
    .line 11
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    const/4 v0, 0x1

    .line 16
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->o〇0:Z

    .line 17
    .line 18
    sget-object v0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/VipMonthPromotionManager;->〇080:Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/VipMonthPromotionManager;

    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 21
    .line 22
    new-instance v2, LO880O〇/oO80;

    .line 23
    .line 24
    invoke-direct {v2, p0}, LO880O〇/oO80;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/VipMonthPromotionManager;->〇O00(Landroidx/fragment/app/FragmentManager;Lcom/intsig/callback/DialogDismissListener;)V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final O8〇o(I)Landroid/app/Dialog;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->Oo08:Landroid/util/SparseArray;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Landroid/app/Dialog;

    .line 8
    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇O〇(I)Landroid/app/Dialog;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->Oo08:Landroid/util/SparseArray;

    .line 18
    .line 19
    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 20
    .line 21
    .line 22
    :cond_0
    if-eqz v0, :cond_1

    .line 23
    .line 24
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 25
    .line 26
    .line 27
    move-result p1

    .line 28
    if-nez p1, :cond_1

    .line 29
    .line 30
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 31
    .line 32
    .line 33
    :cond_1
    return-object v0
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final OO0o〇〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager$checkShowOneTrialRenew$1;

    .line 4
    .line 5
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager$checkShowOneTrialRenew$1;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V

    .line 6
    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/camscanner/marketing/trialrenew/OneTrialRenewConfiguration;->〇080(Landroidx/fragment/app/FragmentManager;Lkotlin/jvm/functions/Function0;)Z

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final OOO〇O0(Ljava/lang/String;Z)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "msg"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->Oo08:Landroid/util/SparseArray;

    .line 7
    .line 8
    const/4 v1, -0x2

    .line 9
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    instance-of v2, v0, Lcom/intsig/app/ProgressDialog;

    .line 14
    .line 15
    const/4 v3, 0x0

    .line 16
    if-eqz v2, :cond_0

    .line 17
    .line 18
    check-cast v0, Lcom/intsig/app/ProgressDialog;

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    move-object v0, v3

    .line 22
    :goto_0
    if-nez v0, :cond_3

    .line 23
    .line 24
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇O〇(I)Landroid/app/Dialog;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    instance-of v2, v0, Lcom/intsig/app/ProgressDialog;

    .line 29
    .line 30
    if-eqz v2, :cond_1

    .line 31
    .line 32
    move-object v3, v0

    .line 33
    check-cast v3, Lcom/intsig/app/ProgressDialog;

    .line 34
    .line 35
    :cond_1
    if-eqz v3, :cond_2

    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->Oo08:Landroid/util/SparseArray;

    .line 38
    .line 39
    invoke-virtual {v0, v1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 40
    .line 41
    .line 42
    :cond_2
    move-object v0, v3

    .line 43
    :cond_3
    if-eqz v0, :cond_4

    .line 44
    .line 45
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 46
    .line 47
    .line 48
    move-result v1

    .line 49
    if-nez v1, :cond_4

    .line 50
    .line 51
    invoke-virtual {v0, p1}, Lcom/intsig/app/ProgressDialog;->o〇O8〇〇o(Ljava/lang/CharSequence;)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {v0, p2}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 58
    .line 59
    .line 60
    :cond_4
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public final Oo8Oo00oo(ILjava/lang/String;)V
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇O00()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    const/4 v1, 0x1

    .line 10
    if-eqz p2, :cond_2

    .line 11
    .line 12
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    if-nez v2, :cond_1

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_1
    const/4 v2, 0x0

    .line 20
    goto :goto_1

    .line 21
    :cond_2
    :goto_0
    const/4 v2, 0x1

    .line 22
    :goto_1
    if-eqz v2, :cond_3

    .line 23
    .line 24
    sget-object p1, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇80〇808〇O:Ljava/lang/String;

    .line 25
    .line 26
    const-string p2, "appId is empty"

    .line 27
    .line 28
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    return-void

    .line 32
    :cond_3
    :try_start_0
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->O8:Landroidx/fragment/app/FragmentActivity;

    .line 33
    .line 34
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    invoke-virtual {v2, p2, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 39
    .line 40
    .line 41
    move-result-object p2

    .line 42
    iget-object p2, p2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 43
    .line 44
    invoke-virtual {p2, v2}, Landroid/content/pm/PackageItemInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    .line 45
    .line 46
    .line 47
    move-result-object p2

    .line 48
    const-string v2, "pm.getPackageInfo(appId,\u2026icationInfo.loadLabel(pm)"

    .line 49
    .line 50
    invoke-static {p2, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->O8:Landroidx/fragment/app/FragmentActivity;

    .line 54
    .line 55
    const/4 v3, 0x4

    .line 56
    new-array v3, v3, [Ljava/lang/Object;

    .line 57
    .line 58
    invoke-static {p1}, Lcom/intsig/camscanner/openapi/ReturnCode;->〇080(I)Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v4

    .line 62
    new-instance v5, Ljava/lang/StringBuilder;

    .line 63
    .line 64
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 65
    .line 66
    .line 67
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    const-string p1, ", "

    .line 71
    .line 72
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 73
    .line 74
    .line 75
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object p1

    .line 82
    aput-object p1, v3, v0

    .line 83
    .line 84
    aput-object p2, v3, v1

    .line 85
    .line 86
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->O8:Landroidx/fragment/app/FragmentActivity;

    .line 87
    .line 88
    const v0, 0x7f1304ed

    .line 89
    .line 90
    .line 91
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 92
    .line 93
    .line 94
    move-result-object p1

    .line 95
    const/4 v0, 0x2

    .line 96
    aput-object p1, v3, v0

    .line 97
    .line 98
    const/4 p1, 0x3

    .line 99
    aput-object p2, v3, p1

    .line 100
    .line 101
    const p1, 0x7f13008f

    .line 102
    .line 103
    .line 104
    invoke-virtual {v2, p1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 105
    .line 106
    .line 107
    move-result-object p1

    .line 108
    const-string v0, "mActivity.getString(\n   \u2026, loadLabel\n            )"

    .line 109
    .line 110
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    .line 112
    .line 113
    sget-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/OpenApiErrorDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/newpagelist/dialog/OpenApiErrorDialog$Companion;

    .line 114
    .line 115
    invoke-virtual {v0, p2, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/OpenApiErrorDialog$Companion;->〇080(Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/intsig/camscanner/pagelist/newpagelist/dialog/OpenApiErrorDialog;

    .line 116
    .line 117
    .line 118
    move-result-object p1

    .line 119
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 120
    .line 121
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/OpenApiErrorDialog$Companion;->〇o00〇〇Oo()Ljava/lang/String;

    .line 122
    .line 123
    .line 124
    move-result-object v0

    .line 125
    invoke-virtual {p1, p2, v0}, Lcom/intsig/app/BaseDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 126
    .line 127
    .line 128
    goto :goto_2

    .line 129
    :catch_0
    move-exception p1

    .line 130
    sget-object p2, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇80〇808〇O:Ljava/lang/String;

    .line 131
    .line 132
    const-string v0, "NameNotFoundException"

    .line 133
    .line 134
    invoke-static {p2, v0, p1}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 135
    .line 136
    .line 137
    :goto_2
    return-void
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public final OoO8()Landroidx/fragment/app/FragmentActivity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->O8:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final Oooo8o0〇()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇O00()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇80〇808〇O:Ljava/lang/String;

    .line 8
    .line 9
    const-string v1, "checkToShowAreaFreeShareDialog is showing now, can not show again"

    .line 10
    .line 11
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->O8:Landroidx/fragment/app/FragmentActivity;

    .line 16
    .line 17
    invoke-static {v0}, Lcom/intsig/camscanner/purchase/spread/AreaFreeActivityManager;->O8(Landroid/content/Context;)Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-eqz v0, :cond_1

    .line 22
    .line 23
    const/4 v0, 0x1

    .line 24
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇oo〇(Z)V

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 28
    .line 29
    new-instance v1, LO880O〇/〇080;

    .line 30
    .line 31
    invoke-direct {v1, p0}, LO880O〇/〇080;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V

    .line 32
    .line 33
    .line 34
    invoke-static {v0, v1}, Lcom/intsig/camscanner/purchase/spread/AreaFreeActivityManager;->〇〇808〇(Landroidx/fragment/app/FragmentManager;Lcom/intsig/callback/DialogDismissListener;)V

    .line 35
    .line 36
    .line 37
    :cond_1
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final O〇8O8〇008(Lkotlin/jvm/functions/Function0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇O00()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇〇888:Landroidx/fragment/app/DialogFragment;

    .line 9
    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    return-void

    .line 13
    :cond_1
    new-instance v0, Lcom/intsig/camscanner/batch/BatchImageProcessTipsDialog;

    .line 14
    .line 15
    invoke-direct {v0}, Lcom/intsig/camscanner/batch/BatchImageProcessTipsDialog;-><init>()V

    .line 16
    .line 17
    .line 18
    new-instance v1, LO880O〇/〇80〇808〇O;

    .line 19
    .line 20
    invoke-direct {v1, p0, p1}, LO880O〇/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;Lkotlin/jvm/functions/Function0;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/batch/BatchImageProcessTipsDialog;->o00〇88〇08(Landroid/view/View$OnClickListener;)V

    .line 24
    .line 25
    .line 26
    new-instance p1, LO880O〇/OO0o〇〇〇〇0;

    .line 27
    .line 28
    invoke-direct {p1, p0}, LO880O〇/OO0o〇〇〇〇0;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/batch/BatchImageProcessTipsDialog;->setDialogDismissListener(Lcom/intsig/callback/DialogDismissListener;)V

    .line 32
    .line 33
    .line 34
    const/4 p1, 0x1

    .line 35
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇oo〇(Z)V

    .line 36
    .line 37
    .line 38
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 39
    .line 40
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    const-string v1, "BatchImageProcessTipsDialog"

    .line 45
    .line 46
    invoke-virtual {p1, v0, v1}, Landroidx/fragment/app/FragmentTransaction;->add(Landroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentTransaction;->commitNowAllowingStateLoss()V

    .line 51
    .line 52
    .line 53
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇〇888:Landroidx/fragment/app/DialogFragment;

    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final o0ooO()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇O00()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇80〇808〇O:Ljava/lang/String;

    .line 8
    .line 9
    const-string v1, "showGpGuidePostPayDialog is showing now, can not show again"

    .line 10
    .line 11
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/guide/gppostpay/GPGuidePostPayConfiguration;->〇080:Lcom/intsig/camscanner/guide/gppostpay/GPGuidePostPayConfiguration;

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 18
    .line 19
    sget-object v2, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager$showGpGuidePostPayDialog$1;->o0:Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager$showGpGuidePostPayDialog$1;

    .line 20
    .line 21
    const/4 v3, 0x1

    .line 22
    invoke-virtual {v0, v3, v1, v2}, Lcom/intsig/camscanner/guide/gppostpay/GPGuidePostPayConfiguration;->〇〇8O0〇8(ILandroidx/fragment/app/FragmentManager;Lkotlin/jvm/functions/Function0;)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final o800o8O()Landroidx/fragment/app/FragmentManager;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oO()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇O00()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇80〇808〇O:Ljava/lang/String;

    .line 8
    .line 9
    const-string v1, "showVipActivityPurchaseDialog is showing now, can not show again"

    .line 10
    .line 11
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    const/4 v0, 0x1

    .line 16
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->o〇0:Z

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 19
    .line 20
    new-instance v1, LO880O〇/〇〇888;

    .line 21
    .line 22
    invoke-direct {v1, p0}, LO880O〇/〇〇888;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V

    .line 23
    .line 24
    .line 25
    invoke-static {v0, v1}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseManager;->〇〇888(Landroidx/fragment/app/FragmentManager;Lcom/intsig/callback/DialogDismissListener;)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final o〇0OOo〇0()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇O00()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇80〇808〇O:Ljava/lang/String;

    .line 8
    .line 9
    const-string v1, "is showing now, can not show again"

    .line 10
    .line 11
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    const/4 v0, 0x1

    .line 16
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇oo〇(Z)V

    .line 17
    .line 18
    .line 19
    sget-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/SetPdfImportTypeDialog;->o〇00O:Lcom/intsig/camscanner/pagelist/newpagelist/dialog/SetPdfImportTypeDialog$Companion;

    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 22
    .line 23
    new-instance v2, LO880O〇/〇o〇;

    .line 24
    .line 25
    invoke-direct {v2, p0}, LO880O〇/〇o〇;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/SetPdfImportTypeDialog$Companion;->〇o00〇〇Oo(Landroidx/fragment/app/FragmentManager;Lcom/intsig/callback/DialogDismissListener;)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final o〇8()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇O00()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇80〇808〇O:Ljava/lang/String;

    .line 8
    .line 9
    const-string v1, "is showing now, can not show again"

    .line 10
    .line 11
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    const/4 v0, 0x1

    .line 16
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇oo〇(Z)V

    .line 17
    .line 18
    .line 19
    sget-object v0, Lcom/intsig/camscanner/image_progress/image_editing/guide/ImageEditGuideDialogFragment;->O8o08O8O:Lcom/intsig/camscanner/image_progress/image_editing/guide/ImageEditGuideDialogFragment$Companion;

    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 22
    .line 23
    new-instance v2, LO880O〇/O8;

    .line 24
    .line 25
    invoke-direct {v2, p0}, LO880O〇/O8;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/image_progress/image_editing/guide/ImageEditGuideDialogFragment$Companion;->〇O8o08O(Landroidx/fragment/app/FragmentManager;Lcom/intsig/callback/DialogDismissListener;)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final o〇O8〇〇o()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇O00()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇80〇808〇O:Ljava/lang/String;

    .line 8
    .line 9
    const-string v1, "showAnnualPremiumDialog is showing now, can not show again"

    .line 10
    .line 11
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialog/annualpremium/AnnualPremiumConfig;->〇080:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialog/annualpremium/AnnualPremiumConfig;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialog/annualpremium/AnnualPremiumConfig;->〇〇8O0〇8()V

    .line 18
    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 21
    .line 22
    new-instance v2, LO880O〇/〇o00〇〇Oo;

    .line 23
    .line 24
    invoke-direct {v2}, LO880O〇/〇o00〇〇Oo;-><init>()V

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialog/annualpremium/AnnualPremiumConfig;->o800o8O(Landroidx/fragment/app/FragmentManager;Lcom/intsig/callback/DialogDismissListener;)V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇0000OOO()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇O00()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇80〇808〇O:Ljava/lang/String;

    .line 8
    .line 9
    const-string v1, "showCNRenewRecallDialog is showing now, can not show again"

    .line 10
    .line 11
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialog/cnrenewrecall/CNRenewRecallConfig;->〇080:Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialog/cnrenewrecall/CNRenewRecallConfig;

    .line 16
    .line 17
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 18
    .line 19
    .line 20
    move-result-wide v1

    .line 21
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialog/cnrenewrecall/CNRenewRecallConfig;->oO80(J)V

    .line 22
    .line 23
    .line 24
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 25
    .line 26
    new-instance v2, LO880O〇/o〇0;

    .line 27
    .line 28
    invoke-direct {v2}, LO880O〇/o〇0;-><init>()V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/common/newdialogs/mainpage/dialog/cnrenewrecall/CNRenewRecallConfig;->〇80〇808〇O(Landroidx/fragment/app/FragmentManager;Lcom/intsig/callback/DialogDismissListener;)V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇00〇8(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇O00()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    if-eqz p1, :cond_1

    .line 9
    .line 10
    if-eqz p2, :cond_1

    .line 11
    .line 12
    sget-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/EmptyDocDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/newpagelist/dialog/EmptyDocDialog$Companion;

    .line 13
    .line 14
    invoke-virtual {v0, p1, p2}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/EmptyDocDialog$Companion;->〇080(Ljava/lang/String;Landroid/net/Uri;)Lcom/intsig/camscanner/pagelist/newpagelist/dialog/EmptyDocDialog;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/EmptyDocDialog$Companion;->〇o00〇〇Oo()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-virtual {p1, p2, v0}, Lcom/intsig/app/BaseDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    :cond_1
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇08O8o〇0()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇O00()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/purchase/dialog/CloudOverrunDialog;

    .line 9
    .line 10
    invoke-direct {v0}, Lcom/intsig/camscanner/purchase/dialog/CloudOverrunDialog;-><init>()V

    .line 11
    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 14
    .line 15
    const-string v2, "CloudOverrunDialog"

    .line 16
    .line 17
    invoke-virtual {v0, v1, v2}, Lcom/intsig/app/BaseDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
.end method

.method public final 〇0〇O0088o(I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->Oo08:Landroid/util/SparseArray;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Landroid/app/Dialog;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 12
    .line 13
    .line 14
    sget-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇80〇808〇O:Ljava/lang/String;

    .line 15
    .line 16
    new-instance v1, Ljava/lang/StringBuilder;

    .line 17
    .line 18
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 19
    .line 20
    .line 21
    const-string v2, "dialog dismiss, id == "

    .line 22
    .line 23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    :cond_0
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final 〇O888o0o()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇O00()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇80〇808〇O:Ljava/lang/String;

    .line 9
    .line 10
    const-string v1, "initNPSDetectionLifecycleObserver"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    new-instance v0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->O8:Landroidx/fragment/app/FragmentActivity;

    .line 18
    .line 19
    new-instance v2, LO880O〇/Oo08;

    .line 20
    .line 21
    invoke-direct {v2, p0}, LO880O〇/Oo08;-><init>(Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;)V

    .line 22
    .line 23
    .line 24
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider$RandomNPSActionDataGroupCallback;)V

    .line 25
    .line 26
    .line 27
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇080:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 28
    .line 29
    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    invoke-virtual {v1, v0}, Landroidx/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇o()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇O00()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o〇()Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇O〇()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eqz v0, :cond_3

    .line 17
    .line 18
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-nez v0, :cond_3

    .line 23
    .line 24
    sget-object v0, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 25
    .line 26
    invoke-virtual {v0}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->〇00()Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-eqz v0, :cond_1

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_1
    const-string v0, "key_show_first_finish_certificate"

    .line 34
    .line 35
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇8O0〇(Ljava/lang/String;)Z

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    if-eqz v1, :cond_2

    .line 40
    .line 41
    sget-object v1, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/NewBieTaskDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/newpagelist/dialog/NewBieTaskDialog$Companion;

    .line 42
    .line 43
    const-string v2, "id_mode"

    .line 44
    .line 45
    invoke-virtual {v1, v0, v2}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/NewBieTaskDialog$Companion;->〇080(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/pagelist/newpagelist/dialog/NewBieTaskDialog;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 50
    .line 51
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/NewBieTaskDialog$Companion;->〇o00〇〇Oo()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/BaseDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_2
    const-string v0, "key_show_first_finish_ocr"

    .line 60
    .line 61
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇8O0〇(Ljava/lang/String;)Z

    .line 62
    .line 63
    .line 64
    move-result v1

    .line 65
    if-eqz v1, :cond_3

    .line 66
    .line 67
    sget-object v1, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/NewBieTaskDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/newpagelist/dialog/NewBieTaskDialog$Companion;

    .line 68
    .line 69
    const-string v2, "ocr"

    .line 70
    .line 71
    invoke-virtual {v1, v0, v2}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/NewBieTaskDialog$Companion;->〇080(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/pagelist/newpagelist/dialog/NewBieTaskDialog;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 76
    .line 77
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/NewBieTaskDialog$Companion;->〇o00〇〇Oo()Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object v1

    .line 81
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/BaseDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 82
    .line 83
    .line 84
    :cond_3
    :goto_0
    return-void
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public final 〇oo〇(Z)V
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇080:Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListFragmentNew;->Oo8O()Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerViewModel;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x1

    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/newpagelist/fragment/PageListContainerViewModel;->o〇0OOo〇0(Z)V

    .line 11
    .line 12
    .line 13
    :cond_0
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇o〇:Z

    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇〇8O0〇8()V
    .locals 1

    .line 1
    const/4 v0, -0x2

    .line 2
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇0〇O0088o(I)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇〇〇0〇〇0()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇O00()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/pdf/PdfKitMoveTipsDialog;

    .line 9
    .line 10
    invoke-direct {v0}, Lcom/intsig/camscanner/pdf/PdfKitMoveTipsDialog;-><init>()V

    .line 11
    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/DialogManager;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 14
    .line 15
    const-string v2, "PdfMoveDialog"

    .line 16
    .line 17
    invoke-virtual {v0, v1, v2}, Landroidx/fragment/app/DialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    const/4 v0, 0x0

    .line 21
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇〇O8(Z)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

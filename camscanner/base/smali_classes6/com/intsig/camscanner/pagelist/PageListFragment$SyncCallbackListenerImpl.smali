.class Lcom/intsig/camscanner/pagelist/PageListFragment$SyncCallbackListenerImpl;
.super Ljava/lang/Object;
.source "PageListFragment.java"

# interfaces
.implements Lcom/intsig/camscanner/tsapp/SyncCallbackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pagelist/PageListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SyncCallbackListenerImpl"
.end annotation


# instance fields
.field private final 〇080:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/intsig/camscanner/pagelist/PageListFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment$SyncCallbackListenerImpl;->〇080:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method synthetic constructor <init>(Lcom/intsig/camscanner/pagelist/PageListFragment;Lo0Oo/O8O〇88oO0;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment$SyncCallbackListenerImpl;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    return-void
.end method


# virtual methods
.method public O8()Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment$SyncCallbackListenerImpl;->〇080:Ljava/lang/ref/WeakReference;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Oo08(JJJIZ)V
    .locals 11

    .line 1
    move-wide v1, p1

    .line 2
    move-wide/from16 v3, p5

    .line 3
    .line 4
    move-object v7, p0

    .line 5
    iget-object v0, v7, Lcom/intsig/camscanner/pagelist/PageListFragment$SyncCallbackListenerImpl;->〇080:Ljava/lang/ref/WeakReference;

    .line 6
    .line 7
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    move-object v8, v0

    .line 12
    check-cast v8, Lcom/intsig/camscanner/pagelist/PageListFragment;

    .line 13
    .line 14
    if-nez v8, :cond_0

    .line 15
    .line 16
    invoke-static {}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇8()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "weakReference documentFragment == null"

    .line 21
    .line 22
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    return-void

    .line 26
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇8()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    new-instance v5, Ljava/lang/StringBuilder;

    .line 31
    .line 32
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 33
    .line 34
    .line 35
    const-string v6, "contentChange docId="

    .line 36
    .line 37
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    const-string v6, " docId "

    .line 44
    .line 45
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-static {v8}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O0o0(Lcom/intsig/camscanner/pagelist/PageListFragment;)Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 49
    .line 50
    .line 51
    move-result-object v6

    .line 52
    invoke-virtual {v6}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 53
    .line 54
    .line 55
    move-result-wide v9

    .line 56
    invoke-virtual {v5, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    const-string v6, " tagId = "

    .line 60
    .line 61
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object v5

    .line 71
    invoke-static {v0, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    const-wide/16 v5, 0x0

    .line 75
    .line 76
    cmp-long v0, p3, v5

    .line 77
    .line 78
    if-gtz v0, :cond_1

    .line 79
    .line 80
    cmp-long v0, v1, v5

    .line 81
    .line 82
    if-lez v0, :cond_3

    .line 83
    .line 84
    invoke-static {v8}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O0o0(Lcom/intsig/camscanner/pagelist/PageListFragment;)Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 89
    .line 90
    .line 91
    move-result-wide v5

    .line 92
    cmp-long v0, v1, v5

    .line 93
    .line 94
    if-nez v0, :cond_3

    .line 95
    .line 96
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇8()Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object v0

    .line 100
    const-string v5, "contentChange do Change"

    .line 101
    .line 102
    invoke-static {v0, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    const/4 v0, 0x2

    .line 106
    move/from16 v6, p7

    .line 107
    .line 108
    if-ne v6, v0, :cond_2

    .line 109
    .line 110
    const-wide/16 v9, -0x1

    .line 111
    .line 112
    cmp-long v0, p3, v9

    .line 113
    .line 114
    if-nez v0, :cond_2

    .line 115
    .line 116
    cmp-long v0, v3, v9

    .line 117
    .line 118
    if-nez v0, :cond_2

    .line 119
    .line 120
    invoke-static {}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇8()Ljava/lang/String;

    .line 121
    .line 122
    .line 123
    move-result-object v0

    .line 124
    const-string v1, "contentChange do Change Delete doc back"

    .line 125
    .line 126
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    .line 128
    .line 129
    invoke-virtual {v8}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o88O8()V

    .line 130
    .line 131
    .line 132
    return-void

    .line 133
    :cond_2
    new-instance v9, Lcom/intsig/camscanner/fragment/SyncContentChangedInfo;

    .line 134
    .line 135
    move-object v0, v9

    .line 136
    move-wide v1, p1

    .line 137
    move-wide v3, p3

    .line 138
    move/from16 v5, p8

    .line 139
    .line 140
    move/from16 v6, p7

    .line 141
    .line 142
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/fragment/SyncContentChangedInfo;-><init>(JJZI)V

    .line 143
    .line 144
    .line 145
    invoke-static {v8}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o0OO(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/os/Handler;

    .line 146
    .line 147
    .line 148
    move-result-object v0

    .line 149
    invoke-static {v8}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o0OO(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/os/Handler;

    .line 150
    .line 151
    .line 152
    move-result-object v1

    .line 153
    const/16 v2, 0x6a

    .line 154
    .line 155
    invoke-static {v1, v2, v9}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    .line 156
    .line 157
    .line 158
    move-result-object v1

    .line 159
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 160
    .line 161
    .line 162
    :cond_3
    return-void
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

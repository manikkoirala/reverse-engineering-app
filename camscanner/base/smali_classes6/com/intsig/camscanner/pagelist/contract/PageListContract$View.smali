.class public interface abstract Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;
.super Ljava/lang/Object;
.source "PageListContract.java"

# interfaces
.implements Lcom/intsig/mvp/view/IView;


# virtual methods
.method public abstract O0()Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;
.end method

.method public abstract O00()V
.end method

.method public abstract O00O()V
.end method

.method public abstract O0oO008(I)V
.end method

.method public abstract O8O〇(Z)V
.end method

.method public abstract O8o〇O0(I)V
.end method

.method public abstract OO〇()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/pagelist/model/PageTypeItem;",
            ">;"
        }
    .end annotation
.end method

.method public abstract OO〇0008O8()Landroidx/recyclerview/widget/RecyclerView;
.end method

.method public abstract Ooo8(Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;)V
.end method

.method public abstract O〇0()V
.end method

.method public abstract getCurrentActivity()Landroidx/fragment/app/FragmentActivity;
.end method

.method public abstract getFragment()Landroidx/fragment/app/Fragment;
.end method

.method public abstract getHandler()Landroid/os/Handler;
.end method

.method public abstract o08o〇0(ILjava/lang/String;)V
.end method

.method public abstract o88O8()V
.end method

.method public abstract o8O(J)V
.end method

.method public abstract oO0〇〇o8〇()Z
.end method

.method public abstract oO80OOO〇(Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;)V
.end method

.method public abstract oO8o()V
.end method

.method public abstract oO〇8O8oOo()V
.end method

.method public abstract o〇OOo000(I)V
.end method

.method public abstract showDialog(I)V
.end method

.method public abstract updateTitle(Z)V
.end method

.method public abstract 〇0()V
.end method

.method public abstract 〇0OO8(I)V
.end method

.method public abstract 〇8o()V
.end method

.method public abstract 〇O8〇OO〇(Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;)V
.end method

.method public abstract 〇〇00OO()V
.end method

.method public abstract 〇〇0o〇o8(Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;Z)V
.end method

.method public abstract 〇〇O00〇8()V
.end method

.method public abstract 〇〇〇(Ljava/util/List;Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/pagelist/model/PageTypeItem;",
            ">;",
            "Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;",
            ")V"
        }
    .end annotation
.end method

.method public abstract 〇〇〇0880(I)V
    .param p1    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param
.end method

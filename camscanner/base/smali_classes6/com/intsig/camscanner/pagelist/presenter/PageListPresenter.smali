.class public Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;
.super Ljava/lang/Object;
.source "PageListPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$MyPdfListener;
    }
.end annotation


# instance fields
.field public O000:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Lcom/intsig/camscanner/pagelist/newpagelist/data/PageListRecommendEntity;",
            ">;"
        }
    .end annotation
.end field

.field private O08000:Ljava/lang/String;

.field private O0O8OO088:Z

.field private O0o〇〇Oo:Z

.field private O8:I

.field private O8O〇:Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;

.field private volatile O8ooOoo〇:I

.field private O8〇o:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field private OO0o〇〇:Ljava/lang/String;

.field private OO0o〇〇〇〇0:Z

.field private OO8oO0o〇:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

.field private OOO:Lcom/intsig/camscanner/datastruct/FolderItem;

.field private OOO〇O0:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$OnMultipleFunctionResponse;

.field private Oo08:Ljava/lang/String;

.field private Oo8Oo00oo:Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;

.field private OoO8:[Ljava/lang/String;

.field private Ooo:Lcom/intsig/camscanner/datastruct/FolderItem;

.field private Oooo8o0〇:Z

.field Oo〇O:Ljava/lang/String;

.field private O〇8O8〇008:I

.field private O〇O〇oO:Z

.field private o0O0:Z

.field private o0ooO:Z

.field private o8:Ljava/lang/String;

.field private o800o8O:Ljava/lang/String;

.field private o88〇OO08〇:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;

.field private o8O〇:Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private volatile o8oO〇:Z

.field public oO:Z

.field private oO00OOO:Z

.field private oO80:Lcom/intsig/camscanner/pagelist/model/EditType;

.field private oo88o8O:Z

.field private ooo〇8oO:Lcom/intsig/camscanner/datastruct/DocItem;

.field private oo〇:Ljava/lang/String;

.field private o〇0:Lcom/intsig/camscanner/pagelist/model/PageListModel;

.field private o〇0OOo〇0:J

.field private o〇8:Z

.field private o〇8oOO88:Z

.field private o〇O:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

.field private o〇O8〇〇o:I

.field private o〇〇0〇:Lcom/intsig/camscanner/capture/CaptureMode;

.field 〇0:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine;

.field private 〇00:Ljava/lang/String;

.field private 〇0000OOO:Lcom/intsig/camscanner/business/mode/eevidence/commonbiz/impl/EEvidenceProcessControl;

.field private 〇00〇8:Lcom/intsig/camscanner/share/ShareHelper;

.field private final 〇080:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

.field private 〇08O8o〇0:Z

.field private volatile 〇0〇O0088o:I

.field private 〇8:Z

.field public 〇80:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private 〇80〇808〇O:Z

.field private 〇8o8o〇:Z

.field private 〇8〇0〇o〇O:J

.field private 〇O00:Landroid/net/Uri;

.field private 〇O888o0o:Z

.field private 〇O8o08O:Z

.field private 〇O〇:I

.field private 〇O〇80o08O:Lcom/intsig/camscanner/datastruct/FolderItem;

.field private 〇o:Lcom/intsig/camscanner/util/PdfEncryptionUtil;

.field private 〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/presenter/PageListDocPresenter;

.field private 〇o0O0O8:Lcom/intsig/adapter/RecyclerViewMultiTouchHelper$MultiItemTouchHelperCallback;

.field private 〇oOO8O8:Z

.field private 〇oo〇:Z

.field private 〇o〇:Z

.field private 〇〇0o:Z

.field private 〇〇808〇:Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;

.field private 〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

.field private 〇〇8O0〇8:J

.field private 〇〇o8:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private 〇〇〇0〇〇0:Lcom/intsig/advertisement/listener/OnAdPositionListener;


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;)V
    .locals 5

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/camscanner/pagelist/presenter/PageListDocPresenter;

    .line 5
    .line 6
    invoke-direct {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListDocPresenter;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/presenter/PageListDocPresenter;

    .line 10
    .line 11
    sget-object v0, Lcom/intsig/camscanner/pagelist/model/EditType;->DEFAULT:Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO80:Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇80〇808〇O:Z

    .line 17
    .line 18
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO0o〇〇〇〇0:Z

    .line 19
    .line 20
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o8o〇:Z

    .line 21
    .line 22
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O8o08O:Z

    .line 23
    .line 24
    const/4 v1, 0x0

    .line 25
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO0o〇〇:Ljava/lang/String;

    .line 26
    .line 27
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oooo8o0〇:Z

    .line 28
    .line 29
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇808〇:Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;

    .line 30
    .line 31
    const/4 v2, -0x1

    .line 32
    iput v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O〇:I

    .line 33
    .line 34
    const-wide/16 v3, -0x1

    .line 35
    .line 36
    iput-wide v3, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 37
    .line 38
    iput v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0〇O0088o:I

    .line 39
    .line 40
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OoO8:[Ljava/lang/String;

    .line 41
    .line 42
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O888o0o:Z

    .line 43
    .line 44
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oo88o8O:Z

    .line 45
    .line 46
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo〇:Z

    .line 47
    .line 48
    iput v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8ooOoo〇:I

    .line 49
    .line 50
    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->NONE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 51
    .line 52
    iput-object v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8〇o:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 53
    .line 54
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇00〇8:Lcom/intsig/camscanner/share/ShareHelper;

    .line 55
    .line 56
    iput-wide v3, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇0OOo〇0:J

    .line 57
    .line 58
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇0o:Z

    .line 59
    .line 60
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇08O8o〇0:Z

    .line 61
    .line 62
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO:Z

    .line 63
    .line 64
    const-wide/16 v2, 0x0

    .line 65
    .line 66
    iput-wide v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8〇0〇o〇O:J

    .line 67
    .line 68
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇O〇oO:Z

    .line 69
    .line 70
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8oO〇:Z

    .line 71
    .line 72
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇8oOO88:Z

    .line 73
    .line 74
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇O:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 75
    .line 76
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO00OOO:Z

    .line 77
    .line 78
    new-instance v2, Landroidx/lifecycle/MutableLiveData;

    .line 79
    .line 80
    invoke-direct {v2}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 81
    .line 82
    .line 83
    iput-object v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O000:Landroidx/lifecycle/MutableLiveData;

    .line 84
    .line 85
    new-instance v2, Landroidx/lifecycle/MutableLiveData;

    .line 86
    .line 87
    invoke-direct {v2}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 88
    .line 89
    .line 90
    iput-object v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇80:Landroidx/lifecycle/MutableLiveData;

    .line 91
    .line 92
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Ooo:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 93
    .line 94
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O〇80o08O:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 95
    .line 96
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OOO:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 97
    .line 98
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇8oO:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 99
    .line 100
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0o〇〇Oo:Z

    .line 101
    .line 102
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO8oO0o〇:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 103
    .line 104
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o0O0:Z

    .line 105
    .line 106
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine;

    .line 107
    .line 108
    new-instance v2, Lcom/intsig/camscanner/business/operation/OperationShowTraceCallbackImpl;

    .line 109
    .line 110
    invoke-direct {v2}, Lcom/intsig/camscanner/business/operation/OperationShowTraceCallbackImpl;-><init>()V

    .line 111
    .line 112
    .line 113
    iput-object v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8O〇:Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;

    .line 114
    .line 115
    new-instance v2, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$13;

    .line 116
    .line 117
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$13;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 118
    .line 119
    .line 120
    iput-object v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o0O0O8:Lcom/intsig/adapter/RecyclerViewMultiTouchHelper$MultiItemTouchHelperCallback;

    .line 121
    .line 122
    new-instance v2, Ljava/util/HashMap;

    .line 123
    .line 124
    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 125
    .line 126
    .line 127
    iput-object v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇o8:Ljava/util/HashMap;

    .line 128
    .line 129
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oo〇O:Ljava/lang/String;

    .line 130
    .line 131
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8O〇:Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 132
    .line 133
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 134
    .line 135
    new-instance p1, Lcom/intsig/camscanner/pagelist/model/PageListModel;

    .line 136
    .line 137
    invoke-direct {p1}, Lcom/intsig/camscanner/pagelist/model/PageListModel;-><init>()V

    .line 138
    .line 139
    .line 140
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇0:Lcom/intsig/camscanner/pagelist/model/PageListModel;

    .line 141
    .line 142
    new-instance p1, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 143
    .line 144
    invoke-direct {p1, v0}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;-><init>(Z)V

    .line 145
    .line 146
    .line 147
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇080:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 148
    .line 149
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->OO〇80oO〇()Z

    .line 150
    .line 151
    .line 152
    move-result p1

    .line 153
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o〇:Z

    .line 154
    .line 155
    return-void
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method static bridge synthetic O0(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o0ooO:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic O000(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Landroid/app/Activity;Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0O(Landroid/app/Activity;Landroid/content/Context;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private O00O()Lcom/intsig/camscanner/business/mode/eevidence/commonbiz/IEEvidenceProcessParamsGetter;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$2;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$2;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic O08000(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇〇0o〇〇0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic O0O(Landroid/app/Activity;Landroid/content/Context;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-wide v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 9
    .line 10
    invoke-static {p2, v0, v1}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇〇0o(Landroid/content/Context;J)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic O0O8OO088(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO8oO0o〇:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O0OO8〇0()V
    .locals 4

    .line 1
    const-string v0, "checkShowStorageDialog"

    .line 2
    .line 3
    const-string v1, "PageListPresenter"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 9
    .line 10
    invoke-static {v0}, Lcom/intsig/comm/account_data/AccountPreference;->〇〇0o(Landroid/content/Context;)Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-nez v0, :cond_0

    .line 15
    .line 16
    const-string v0, "checkShowStorageDialog is not LoginAccount"

    .line 17
    .line 18
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    return-void

    .line 22
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O()J

    .line 23
    .line 24
    .line 25
    move-result-wide v0

    .line 26
    new-instance v2, Ljava/util/Date;

    .line 27
    .line 28
    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    .line 32
    .line 33
    .line 34
    move-result-wide v2

    .line 35
    invoke-static {v0, v1, v2, v3}, Lcom/intsig/utils/DateTimeUtil;->Oooo8o0〇(JJ)Z

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    if-eqz v0, :cond_1

    .line 40
    .line 41
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 42
    .line 43
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->oO〇8O8oOo()V

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 48
    .line 49
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->〇0()V

    .line 50
    .line 51
    .line 52
    :goto_0
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static O0o(Landroid/app/Activity;JLcom/intsig/camscanner/control/DataChecker$ActionListener;)V
    .locals 1
    .param p0    # Landroid/app/Activity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$5;

    .line 9
    .line 10
    invoke-direct {v0, p0, p3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$5;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/control/DataChecker$ActionListener;)V

    .line 11
    .line 12
    .line 13
    invoke-static {p0, p1, p2, p3, v0}, Lcom/intsig/camscanner/control/DataChecker;->〇80〇808〇O(Landroid/app/Activity;JLcom/intsig/camscanner/control/DataChecker$ActionListener;Lcom/intsig/camscanner/app/DbWaitingListener;)Z

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic O0oO0(Ljava/util/ArrayList;Landroid/app/Activity;I)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 2
    .line 3
    .line 4
    move-result p3

    .line 5
    new-array v0, p3, [J

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    :goto_0
    if-ge v1, p3, :cond_0

    .line 9
    .line 10
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 11
    .line 12
    .line 13
    move-result-object v2

    .line 14
    check-cast v2, Ljava/lang/Long;

    .line 15
    .line 16
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    .line 17
    .line 18
    .line 19
    move-result-wide v2

    .line 20
    aput-wide v2, v0, v1

    .line 21
    .line 22
    add-int/lit8 v1, v1, 0x1

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    new-instance p1, Landroid/content/Intent;

    .line 26
    .line 27
    const-class p3, Lcom/intsig/camscanner/MovePageActivity;

    .line 28
    .line 29
    invoke-direct {p1, p2, p3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 30
    .line 31
    .line 32
    const-string p2, "EXTRA_CUT_DOC_ID"

    .line 33
    .line 34
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 35
    .line 36
    .line 37
    move-result-wide v1

    .line 38
    invoke-virtual {p1, p2, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 39
    .line 40
    .line 41
    const-string p2, "dirSyncId"

    .line 42
    .line 43
    iget-object p3, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO0o〇〇:Ljava/lang/String;

    .line 44
    .line 45
    invoke-virtual {p1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 46
    .line 47
    .line 48
    const-string p2, "EXTRA_CUT_DOC_PDF_PATH"

    .line 49
    .line 50
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇0〇0o8()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object p3

    .line 54
    invoke-virtual {p1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 55
    .line 56
    .line 57
    const-string p2, "EXTRA_CUT_DOC_PAGE_NUM"

    .line 58
    .line 59
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o8O〇O()I

    .line 60
    .line 61
    .line 62
    move-result p3

    .line 63
    invoke-virtual {p1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 64
    .line 65
    .line 66
    const-string p2, "multi_image_id"

    .line 67
    .line 68
    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 69
    .line 70
    .line 71
    const-string p2, "action"

    .line 72
    .line 73
    const/4 p3, 0x1

    .line 74
    invoke-virtual {p1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 75
    .line 76
    .line 77
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 78
    .line 79
    invoke-interface {p2}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getFragment()Landroidx/fragment/app/Fragment;

    .line 80
    .line 81
    .line 82
    move-result-object p2

    .line 83
    invoke-virtual {p2}, Landroidx/fragment/app/Fragment;->isAdded()Z

    .line 84
    .line 85
    .line 86
    move-result p2

    .line 87
    if-eqz p2, :cond_1

    .line 88
    .line 89
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 90
    .line 91
    invoke-interface {p2}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getFragment()Landroidx/fragment/app/Fragment;

    .line 92
    .line 93
    .line 94
    move-result-object p2

    .line 95
    const/16 p3, 0x3ee

    .line 96
    .line 97
    invoke-virtual {p2, p1, p3}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 98
    .line 99
    .line 100
    goto :goto_1

    .line 101
    :cond_1
    const-string p1, "PageListPresenter"

    .line 102
    .line 103
    const-string p2, "activity not Attach when go2MultiCut"

    .line 104
    .line 105
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    :goto_1
    return-void
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private O0oO008(Lcom/intsig/camscanner/capture/SupportCaptureModeOption;Ljava/lang/String;Z)V
    .locals 11

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0O00oO()Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    invoke-static {v1, v0}, Lcom/intsig/camscanner/business/folders/OfflineFolder;->Oooo8o0〇(Landroid/content/Context;Z)Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    return-void

    .line 25
    :cond_1
    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    const-string v2, "KEY_USE_SYS_CAMERA"

    .line 30
    .line 31
    const/4 v3, 0x0

    .line 32
    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    if-eqz v0, :cond_2

    .line 37
    .line 38
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->ooo〇8oO()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oo〇:Ljava/lang/String;

    .line 43
    .line 44
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 45
    .line 46
    check-cast p2, Lcom/intsig/camscanner/pagelist/PageListFragment;

    .line 47
    .line 48
    const/16 p3, 0x3ec

    .line 49
    .line 50
    invoke-static {p2, p3, p1}, Lcom/intsig/camscanner/app/IntentUtil;->oO(Landroidx/fragment/app/Fragment;ILjava/lang/String;)V

    .line 51
    .line 52
    .line 53
    goto/16 :goto_5

    .line 54
    .line 55
    :cond_2
    invoke-static {}, Lcom/intsig/camscanner/app/AppPerformanceInfo;->〇080()Lcom/intsig/camscanner/app/AppPerformanceInfo;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 60
    .line 61
    .line 62
    move-result-wide v4

    .line 63
    iget-boolean v2, v0, Lcom/intsig/camscanner/app/AppPerformanceInfo;->〇o00〇〇Oo:Z

    .line 64
    .line 65
    if-eqz v2, :cond_3

    .line 66
    .line 67
    iput-boolean v3, v0, Lcom/intsig/camscanner/app/AppPerformanceInfo;->〇o00〇〇Oo:Z

    .line 68
    .line 69
    iput-wide v4, v0, Lcom/intsig/camscanner/app/AppPerformanceInfo;->O8:J

    .line 70
    .line 71
    :cond_3
    iput-wide v4, v0, Lcom/intsig/camscanner/app/AppPerformanceInfo;->Oo08:J

    .line 72
    .line 73
    if-eqz p3, :cond_4

    .line 74
    .line 75
    iget-wide v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇0OOo〇0:J

    .line 76
    .line 77
    iget-object v4, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO0o〇〇:Ljava/lang/String;

    .line 78
    .line 79
    const/4 v5, 0x0

    .line 80
    iget-object v6, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇〇0〇:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 81
    .line 82
    iget-boolean v7, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oooo8o0〇:Z

    .line 83
    .line 84
    const/4 v8, 0x0

    .line 85
    const/4 v9, 0x0

    .line 86
    const/4 v10, 0x0

    .line 87
    invoke-static/range {v1 .. v10}, Lcom/intsig/camscanner/capture/util/CaptureActivityRouterUtil;->〇080(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/capture/CaptureMode;ZLjava/lang/String;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;Z)Landroid/content/Intent;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    goto :goto_0

    .line 92
    :cond_4
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o8O〇O()I

    .line 93
    .line 94
    .line 95
    move-result v0

    .line 96
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 97
    .line 98
    .line 99
    move-result-wide v2

    .line 100
    invoke-static {v1, v0, v2, v3}, Lcom/intsig/camscanner/capture/util/CaptureActivityRouterUtil;->〇o00〇〇Oo(Landroid/content/Context;IJ)Landroid/content/Intent;

    .line 101
    .line 102
    .line 103
    move-result-object v0

    .line 104
    const-string v1, "doc_title"

    .line 105
    .line 106
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0〇oo()Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object v2

    .line 110
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    .line 112
    .line 113
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇0008O8()I

    .line 114
    .line 115
    .line 116
    move-result v1

    .line 117
    sget-object v2, Lcom/intsig/camscanner/paper/PaperUtil;->〇080:Lcom/intsig/camscanner/paper/PaperUtil;

    .line 118
    .line 119
    invoke-virtual {v2}, Lcom/intsig/camscanner/paper/PaperUtil;->OO0o〇〇〇〇0()Z

    .line 120
    .line 121
    .line 122
    move-result v3

    .line 123
    const/16 v4, 0x3e8

    .line 124
    .line 125
    const-string v5, "support_mode"

    .line 126
    .line 127
    const-string v6, "capture_mode"

    .line 128
    .line 129
    if-eqz v3, :cond_5

    .line 130
    .line 131
    if-ne v1, v4, :cond_5

    .line 132
    .line 133
    sget-object v3, Lcom/intsig/camscanner/capture/CaptureMode;->TOPIC_PAPER:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 134
    .line 135
    invoke-virtual {v0, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 136
    .line 137
    .line 138
    sget-object v3, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_TOPIC_PAPER:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 139
    .line 140
    invoke-virtual {v0, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 141
    .line 142
    .line 143
    goto :goto_1

    .line 144
    :cond_5
    invoke-virtual {v2}, Lcom/intsig/camscanner/paper/PaperUtil;->OO0o〇〇〇〇0()Z

    .line 145
    .line 146
    .line 147
    move-result v3

    .line 148
    if-eqz v3, :cond_6

    .line 149
    .line 150
    const/16 v3, 0x7d0

    .line 151
    .line 152
    if-ne v1, v3, :cond_6

    .line 153
    .line 154
    sget-object v3, Lcom/intsig/camscanner/capture/CaptureMode;->TOPIC_LEGACY:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 155
    .line 156
    invoke-virtual {v0, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 157
    .line 158
    .line 159
    sget-object v3, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_TOPIC:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 160
    .line 161
    invoke-virtual {v0, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 162
    .line 163
    .line 164
    goto :goto_1

    .line 165
    :cond_6
    const/16 v3, 0x8b

    .line 166
    .line 167
    if-ne v1, v3, :cond_7

    .line 168
    .line 169
    sget-object v3, Lcom/intsig/camscanner/capture/CaptureMode;->BANK_CARD_JOURNAL:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 170
    .line 171
    invoke-virtual {v0, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 172
    .line 173
    .line 174
    sget-object v3, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_BANK_CARD_JOURNAL:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 175
    .line 176
    invoke-virtual {v0, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 177
    .line 178
    .line 179
    goto :goto_1

    .line 180
    :cond_7
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o〇o()Z

    .line 181
    .line 182
    .line 183
    move-result v3

    .line 184
    if-eqz v3, :cond_8

    .line 185
    .line 186
    sget-object v3, Lcom/intsig/camscanner/capture/CaptureMode;->WRITING_PAD:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 187
    .line 188
    invoke-virtual {v0, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 189
    .line 190
    .line 191
    sget-object v3, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ALL:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 192
    .line 193
    invoke-virtual {v0, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 194
    .line 195
    .line 196
    goto :goto_1

    .line 197
    :cond_8
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇o0o()Z

    .line 198
    .line 199
    .line 200
    move-result v3

    .line 201
    if-eqz v3, :cond_9

    .line 202
    .line 203
    sget-object v3, Lcom/intsig/camscanner/capture/CaptureMode;->WHITE_PAD:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 204
    .line 205
    invoke-virtual {v0, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 206
    .line 207
    .line 208
    sget-object v3, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ALL:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 209
    .line 210
    invoke-virtual {v0, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 211
    .line 212
    .line 213
    :cond_9
    :goto_1
    const-string v3, "extra_doc_type_for_load_capture_mode"

    .line 214
    .line 215
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 216
    .line 217
    .line 218
    invoke-virtual {v2}, Lcom/intsig/camscanner/paper/PaperUtil;->OO0o〇〇〇〇0()Z

    .line 219
    .line 220
    .line 221
    move-result v2

    .line 222
    if-eqz v2, :cond_a

    .line 223
    .line 224
    if-ne v1, v4, :cond_a

    .line 225
    .line 226
    const-string v1, "test_paper"

    .line 227
    .line 228
    goto :goto_2

    .line 229
    :cond_a
    const-string v1, "normal"

    .line 230
    .line 231
    :goto_2
    const-string v2, "CSList"

    .line 232
    .line 233
    invoke-static {v2, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    .line 235
    .line 236
    const-string v1, "extra_offline_folder"

    .line 237
    .line 238
    iget-boolean v3, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oooo8o0〇:Z

    .line 239
    .line 240
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 241
    .line 242
    .line 243
    const-string v1, "extra_back_animaiton"

    .line 244
    .line 245
    const/4 v3, 0x1

    .line 246
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 247
    .line 248
    .line 249
    const-string v1, "extra_show_capture_mode_tips"

    .line 250
    .line 251
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 252
    .line 253
    .line 254
    const-string v1, "extra_folder_id"

    .line 255
    .line 256
    iget-object v4, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO0o〇〇:Ljava/lang/String;

    .line 257
    .line 258
    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 259
    .line 260
    .line 261
    const-string v1, "extra_entrance"

    .line 262
    .line 263
    if-eqz p1, :cond_d

    .line 264
    .line 265
    const-string v2, "extra_normal_only_single"

    .line 266
    .line 267
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 268
    .line 269
    .line 270
    invoke-virtual {v0, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 271
    .line 272
    .line 273
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ALL:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 274
    .line 275
    if-ne p1, v2, :cond_b

    .line 276
    .line 277
    const-string p1, "extra_direct_multiple_photo"

    .line 278
    .line 279
    invoke-virtual {v0, p1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 280
    .line 281
    .line 282
    :cond_b
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 283
    .line 284
    .line 285
    move-result p1

    .line 286
    if-nez p1, :cond_c

    .line 287
    .line 288
    const-string p1, "constant_add_spec_action"

    .line 289
    .line 290
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 291
    .line 292
    .line 293
    :cond_c
    sget-object p1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_LIST:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 294
    .line 295
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 296
    .line 297
    .line 298
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0oo0o0〇()Z

    .line 299
    .line 300
    .line 301
    move-result p1

    .line 302
    if-eqz p1, :cond_f

    .line 303
    .line 304
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 305
    .line 306
    .line 307
    move-result p1

    .line 308
    if-eqz p1, :cond_f

    .line 309
    .line 310
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->CERTIFICATE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 311
    .line 312
    invoke-virtual {v0, v6, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 313
    .line 314
    .line 315
    goto :goto_3

    .line 316
    :cond_d
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo8o〇o〇()Ljava/lang/String;

    .line 317
    .line 318
    .line 319
    move-result-object p1

    .line 320
    invoke-static {p1}, Lcom/intsig/camscanner/business/folders/CertificationFolder;->〇080(Ljava/lang/String;)Z

    .line 321
    .line 322
    .line 323
    move-result p1

    .line 324
    if-eqz p1, :cond_e

    .line 325
    .line 326
    const-string p1, "PageListPresenter"

    .line 327
    .line 328
    const-string p2, "click in doc belongs to my certification folder"

    .line 329
    .line 330
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    .line 332
    .line 333
    const-string p1, "cardfolder_click_scan"

    .line 334
    .line 335
    invoke-static {v2, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    .line 337
    .line 338
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->CERTIFICATE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 339
    .line 340
    sget-object p2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_IDCARD_FOLDER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 341
    .line 342
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 343
    .line 344
    .line 345
    invoke-virtual {v0, v6, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 346
    .line 347
    .line 348
    goto :goto_3

    .line 349
    :cond_e
    sget-object p1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_LIST:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 350
    .line 351
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 352
    .line 353
    .line 354
    :cond_f
    :goto_3
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇〇0〇:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 355
    .line 356
    if-eqz p1, :cond_10

    .line 357
    .line 358
    invoke-virtual {v0, v6, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 359
    .line 360
    .line 361
    :cond_10
    if-eqz p3, :cond_11

    .line 362
    .line 363
    const-string p1, "constant_is_add_new_doc"

    .line 364
    .line 365
    invoke-virtual {v0, p1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 366
    .line 367
    .line 368
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 369
    .line 370
    invoke-interface {p1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getFragment()Landroidx/fragment/app/Fragment;

    .line 371
    .line 372
    .line 373
    move-result-object p1

    .line 374
    const/16 p2, 0x400

    .line 375
    .line 376
    invoke-virtual {p1, v0, p2}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 377
    .line 378
    .line 379
    goto :goto_4

    .line 380
    :cond_11
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 381
    .line 382
    invoke-interface {p1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getFragment()Landroidx/fragment/app/Fragment;

    .line 383
    .line 384
    .line 385
    move-result-object p1

    .line 386
    const/16 p2, 0x3e9

    .line 387
    .line 388
    invoke-virtual {p1, v0, p2}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 389
    .line 390
    .line 391
    :goto_4
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 392
    .line 393
    invoke-interface {p1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 394
    .line 395
    .line 396
    move-result-object p1

    .line 397
    const p2, 0x7f01004a

    .line 398
    .line 399
    .line 400
    const p3, 0x7f010049

    .line 401
    .line 402
    .line 403
    invoke-virtual {p1, p2, p3}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 404
    .line 405
    .line 406
    :goto_5
    return-void
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private O0o〇()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->o〇0()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇80〇808〇O()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_1

    .line 12
    .line 13
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇80〇808〇O()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_2

    .line 18
    .line 19
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->〇〇888()Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-eqz v0, :cond_2

    .line 24
    .line 25
    :cond_1
    const/4 v0, 0x1

    .line 26
    goto :goto_0

    .line 27
    :cond_2
    const/4 v0, 0x0

    .line 28
    :goto_0
    return v0
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static O0o〇O0〇(Landroidx/appcompat/app/AppCompatActivity;JLcom/intsig/camscanner/share/ShareHelper;Ljava/lang/String;Z)V
    .locals 2
    .param p0    # Landroidx/appcompat/app/AppCompatActivity;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 5
    .line 6
    .line 7
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 12
    .line 13
    .line 14
    new-instance p1, Lcom/intsig/camscanner/share/type/ShareToWord;

    .line 15
    .line 16
    const/4 p2, 0x0

    .line 17
    invoke-direct {p1, p0, v0, p2}, Lcom/intsig/camscanner/share/type/ShareToWord;-><init>(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1, p4}, Lcom/intsig/camscanner/share/type/ShareToWord;->〇〇0o8O〇〇(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {p1, p5}, Lcom/intsig/camscanner/share/type/ShareToWord;->〇o8OO0(Z)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p3}, Lcom/intsig/camscanner/share/ShareHelper;->Oo〇O8o〇8()Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;

    .line 27
    .line 28
    .line 29
    move-result-object p0

    .line 30
    if-nez p0, :cond_0

    .line 31
    .line 32
    new-instance p0, Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;

    .line 33
    .line 34
    invoke-direct {p0}, Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;-><init>()V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p3, p0}, Lcom/intsig/camscanner/share/ShareHelper;->O〇oO〇oo8o(Lcom/intsig/camscanner/share/data_mode/ShareOtherArguments;)V

    .line 38
    .line 39
    .line 40
    :cond_0
    sget-object p0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_LIST:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 41
    .line 42
    invoke-virtual {p3, p0}, Lcom/intsig/camscanner/share/ShareHelper;->〇〇00O〇0o(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 43
    .line 44
    .line 45
    invoke-virtual {p3, p1}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 46
    .line 47
    .line 48
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method static bridge synthetic O0o〇〇Oo(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇8:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O0〇(Ljava/lang/Long;I)V
    .locals 4

    .line 1
    const-string v0, "showRecommendDirView dirType:$dirType"

    .line 2
    .line 3
    const-string v1, "PageListPresenter"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/16 v0, 0x65

    .line 9
    .line 10
    if-ge p2, v0, :cond_0

    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    invoke-static {p2}, Lcom/intsig/camscanner/scenariodir/util/CertificateUtil;->OO0o〇〇〇〇0(I)Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O〇80o08O:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 18
    .line 19
    const/4 v2, 0x1

    .line 20
    if-nez v0, :cond_2

    .line 21
    .line 22
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 23
    .line 24
    invoke-static {v0, v2}, Lcom/intsig/camscanner/app/DBUtil;->〇8o8O〇O(Landroid/content/Context;Z)I

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O80〇oOo()I

    .line 29
    .line 30
    .line 31
    move-result v3

    .line 32
    if-lt v0, v3, :cond_1

    .line 33
    .line 34
    return-void

    .line 35
    :cond_1
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 36
    .line 37
    const/4 v3, 0x0

    .line 38
    invoke-static {v0, v3, v2, p2}, Lcom/intsig/camscanner/util/Util;->〇O888o0o(Landroid/content/Context;Ljava/lang/String;ZI)Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    new-instance v3, Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 43
    .line 44
    invoke-direct {v3}, Lcom/intsig/camscanner/datastruct/FolderItem;-><init>()V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v3, p2}, Lcom/intsig/camscanner/datastruct/FolderItem;->O0o〇〇Oo(I)V

    .line 48
    .line 49
    .line 50
    invoke-virtual {v3, v0}, Lcom/intsig/camscanner/datastruct/FolderItem;->〇O〇80o08O(Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    iput-object v3, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OOO:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 54
    .line 55
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O000:Landroidx/lifecycle/MutableLiveData;

    .line 56
    .line 57
    invoke-virtual {p2, v3}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 58
    .line 59
    .line 60
    new-instance p2, Ljava/lang/StringBuilder;

    .line 61
    .line 62
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 63
    .line 64
    .line 65
    const-string v3, "showRecommendDirView newFolderItem title:"

    .line 66
    .line 67
    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object p2

    .line 77
    invoke-static {v1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    .line 79
    .line 80
    goto :goto_0

    .line 81
    :cond_2
    new-instance p2, Ljava/lang/StringBuilder;

    .line 82
    .line 83
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 84
    .line 85
    .line 86
    const-string v0, "showRecommendDirView recommendFolder title="

    .line 87
    .line 88
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O〇80o08O:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 92
    .line 93
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/FolderItem;->o800o8O()Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    .line 99
    .line 100
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 101
    .line 102
    .line 103
    move-result-object p2

    .line 104
    invoke-static {v1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    .line 106
    .line 107
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O000:Landroidx/lifecycle/MutableLiveData;

    .line 108
    .line 109
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O〇80o08O:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 110
    .line 111
    invoke-virtual {p2, v0}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 112
    .line 113
    .line 114
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    .line 115
    .line 116
    .line 117
    move-result-wide p1

    .line 118
    invoke-static {p1, p2, v2}, Lcom/intsig/camscanner/scenariodir/util/CertificateDbUtil;->oO80(JZ)Z

    .line 119
    .line 120
    .line 121
    return-void
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private synthetic O0〇oO〇o(Landroid/app/Activity;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;Ljava/lang/String;ZZ)V
    .locals 0

    .line 1
    if-nez p5, :cond_0

    .line 2
    .line 3
    new-instance p2, L〇OoO0o0/o〇〇0〇;

    .line 4
    .line 5
    invoke-direct {p2, p0}, L〇OoO0o0/o〇〇0〇;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 6
    .line 7
    .line 8
    invoke-static {p1, p2}, Lcom/intsig/camscanner/app/DialogUtils;->OOO(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)V

    .line 9
    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    invoke-direct {p0, p2, p3, p4}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0oO008(Lcom/intsig/camscanner/capture/SupportCaptureModeOption;Ljava/lang/String;Z)V

    .line 13
    .line 14
    .line 15
    :goto_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method public static synthetic O8(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Landroid/app/Activity;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0O(Landroid/app/Activity;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic O88O()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->o88O8()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic O8O〇(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o0O0:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic O8o08O8O(Landroid/app/Activity;)V
    .locals 1

    .line 1
    new-instance v0, L〇OoO0o0/o〇O8〇〇o;

    .line 2
    .line 3
    invoke-direct {v0, p0}, L〇OoO0o0/o〇O8〇〇o;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O8oOo80(I)Ljava/lang/String;
    .locals 7

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    new-instance v2, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    new-instance v3, Ljava/lang/StringBuilder;

    .line 17
    .line 18
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 19
    .line 20
    .line 21
    new-instance v4, Ljava/lang/StringBuilder;

    .line 22
    .line 23
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 24
    .line 25
    .line 26
    const/4 v5, 0x0

    .line 27
    :goto_0
    if-ge v5, p1, :cond_5

    .line 28
    .line 29
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    .line 30
    .line 31
    .line 32
    move-result v6

    .line 33
    if-lez v6, :cond_0

    .line 34
    .line 35
    const-string v6, " and note like ? "

    .line 36
    .line 37
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    goto :goto_1

    .line 41
    :cond_0
    const-string v6, " note like ? "

    .line 42
    .line 43
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    :goto_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    .line 47
    .line 48
    .line 49
    move-result v6

    .line 50
    if-lez v6, :cond_1

    .line 51
    .line 52
    const-string v6, " and image_titile like ? "

    .line 53
    .line 54
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    goto :goto_2

    .line 58
    :cond_1
    const-string v6, " image_titile like ? "

    .line 59
    .line 60
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    :goto_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    .line 64
    .line 65
    .line 66
    move-result v6

    .line 67
    if-lez v6, :cond_2

    .line 68
    .line 69
    const-string v6, " and ocr_result like ? "

    .line 70
    .line 71
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    goto :goto_3

    .line 75
    :cond_2
    const-string v6, " ocr_result like ? "

    .line 76
    .line 77
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    :goto_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    .line 81
    .line 82
    .line 83
    move-result v6

    .line 84
    if-lez v6, :cond_3

    .line 85
    .line 86
    const-string v6, " and ocr_result_user like ? "

    .line 87
    .line 88
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    goto :goto_4

    .line 92
    :cond_3
    const-string v6, " ocr_result_user like ? "

    .line 93
    .line 94
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    .line 96
    .line 97
    :goto_4
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    .line 98
    .line 99
    .line 100
    move-result v6

    .line 101
    if-lez v6, :cond_4

    .line 102
    .line 103
    const-string v6, " and ocr_string like ? "

    .line 104
    .line 105
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    .line 107
    .line 108
    goto :goto_5

    .line 109
    :cond_4
    const-string v6, " ocr_string like ? "

    .line 110
    .line 111
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    :goto_5
    add-int/lit8 v5, v5, 0x1

    .line 115
    .line 116
    goto :goto_0

    .line 117
    :cond_5
    new-instance p1, Ljava/lang/StringBuilder;

    .line 118
    .line 119
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 120
    .line 121
    .line 122
    const-string v5, "("

    .line 123
    .line 124
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    .line 126
    .line 127
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object v0

    .line 131
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    .line 133
    .line 134
    const-string v0, ")"

    .line 135
    .line 136
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    .line 138
    .line 139
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 140
    .line 141
    .line 142
    move-result-object p1

    .line 143
    new-instance v6, Ljava/lang/StringBuilder;

    .line 144
    .line 145
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 146
    .line 147
    .line 148
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    .line 150
    .line 151
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 152
    .line 153
    .line 154
    move-result-object v1

    .line 155
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    .line 157
    .line 158
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    .line 160
    .line 161
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 162
    .line 163
    .line 164
    move-result-object v1

    .line 165
    new-instance v6, Ljava/lang/StringBuilder;

    .line 166
    .line 167
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 168
    .line 169
    .line 170
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    .line 172
    .line 173
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 174
    .line 175
    .line 176
    move-result-object v2

    .line 177
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 178
    .line 179
    .line 180
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    .line 182
    .line 183
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 184
    .line 185
    .line 186
    move-result-object v2

    .line 187
    new-instance v6, Ljava/lang/StringBuilder;

    .line 188
    .line 189
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 190
    .line 191
    .line 192
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    .line 194
    .line 195
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 196
    .line 197
    .line 198
    move-result-object v3

    .line 199
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    .line 201
    .line 202
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    .line 204
    .line 205
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 206
    .line 207
    .line 208
    move-result-object v3

    .line 209
    new-instance v6, Ljava/lang/StringBuilder;

    .line 210
    .line 211
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 212
    .line 213
    .line 214
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    .line 216
    .line 217
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 218
    .line 219
    .line 220
    move-result-object v4

    .line 221
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    .line 223
    .line 224
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    .line 226
    .line 227
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 228
    .line 229
    .line 230
    move-result-object v0

    .line 231
    new-instance v4, Ljava/lang/StringBuilder;

    .line 232
    .line 233
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 234
    .line 235
    .line 236
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    .line 238
    .line 239
    const-string p1, " or "

    .line 240
    .line 241
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 242
    .line 243
    .line 244
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    .line 246
    .line 247
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    .line 249
    .line 250
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    .line 252
    .line 253
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    .line 255
    .line 256
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    .line 258
    .line 259
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    .line 261
    .line 262
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    .line 264
    .line 265
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 266
    .line 267
    .line 268
    move-result-object p1

    .line 269
    return-object p1
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public static synthetic O8ooOoo〇(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Ooo08()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O8o〇O0()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    move-object v1, v0

    .line 15
    check-cast v1, Landroidx/appcompat/app/AppCompatActivity;

    .line 16
    .line 17
    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-eqz v0, :cond_1

    .line 22
    .line 23
    return-void

    .line 24
    :cond_1
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oo0O〇0〇〇〇(Landroidx/appcompat/app/AppCompatActivity;)Lcom/intsig/camscanner/share/ShareHelper;

    .line 25
    .line 26
    .line 27
    move-result-object v4

    .line 28
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 29
    .line 30
    .line 31
    move-result-wide v2

    .line 32
    const/4 v5, 0x0

    .line 33
    const/4 v6, 0x1

    .line 34
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0o〇O0〇(Landroidx/appcompat/app/AppCompatActivity;JLcom/intsig/camscanner/share/ShareHelper;Ljava/lang/String;Z)V

    .line 35
    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic O8〇o(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel$UriData;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8〇oO〇〇8o(Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel$UriData;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static synthetic OO(Landroid/app/Activity;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    check-cast p0, Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/printer/PrintNavigation;->〇080(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private OO0O()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o0()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    .line 15
    .line 16
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 20
    .line 21
    .line 22
    move-result-wide v2

    .line 23
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 28
    .line 29
    .line 30
    invoke-static {v0, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->oO0(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 31
    .line 32
    .line 33
    :cond_1
    :goto_0
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic OO0o〇〇(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;II)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oOo〇8o008(II)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Landroid/app/Activity;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8〇OO(Landroid/app/Activity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private OO0〇〇8(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/pagelist/model/PageTypeItem;",
            ">;)",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_2

    .line 7
    .line 8
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    if-eqz v1, :cond_0

    .line 13
    .line 14
    goto :goto_1

    .line 15
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    if-eqz v1, :cond_2

    .line 24
    .line 25
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    check-cast v1, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;

    .line 30
    .line 31
    instance-of v2, v1, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 32
    .line 33
    if-eqz v2, :cond_1

    .line 34
    .line 35
    check-cast v1, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 36
    .line 37
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/model/PageImageItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/pagelist/model/PageItem;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    iget-object v1, v1, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇o〇:Ljava/lang/String;

    .line 42
    .line 43
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_2
    :goto_1
    return-object v0
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic OO8oO0o〇(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OOO(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇O:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OOO8o〇〇(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oo〇O8o〇8(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic OOO〇O0(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Ljava/util/List;Landroid/app/Activity;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o0O(Ljava/util/List;Landroid/app/Activity;Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private synthetic OOo()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇8o8o〇()Ljava/util/ArrayList;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o80ooO(Ljava/util/ArrayList;)V

    .line 14
    .line 15
    .line 16
    new-instance v0, Ljava/lang/StringBuilder;

    .line 17
    .line 18
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 19
    .line 20
    .line 21
    const-string v2, "doPageDeleteConfirm deleteNum="

    .line 22
    .line 23
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    const-string v1, " mPageNum="

    .line 30
    .line 31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o8O〇O()I

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    const-string v1, "PageListPresenter"

    .line 46
    .line 47
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static synthetic OO〇00〇8oO(Lcom/intsig/callback/Callback0;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-interface {p0}, Lcom/intsig/callback/Callback0;->call()V

    .line 2
    .line 3
    .line 4
    const/4 p0, 0x0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic Oo08(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇0O8ooO(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private Oo08OO8oO(Ljava/util/List;)[I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/pagelist/model/PageItem;",
            ">;)[I"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 3
    .line 4
    .line 5
    move-result-object v1

    .line 6
    check-cast v1, Lcom/intsig/camscanner/pagelist/model/PageItem;

    .line 7
    .line 8
    iget v1, v1, Lcom/intsig/camscanner/pagelist/model/PageItem;->o〇0:I

    .line 9
    .line 10
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    move v2, v1

    .line 15
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 16
    .line 17
    .line 18
    move-result v3

    .line 19
    if-eqz v3, :cond_2

    .line 20
    .line 21
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    check-cast v3, Lcom/intsig/camscanner/pagelist/model/PageItem;

    .line 26
    .line 27
    iget v3, v3, Lcom/intsig/camscanner/pagelist/model/PageItem;->o〇0:I

    .line 28
    .line 29
    if-ge v3, v1, :cond_1

    .line 30
    .line 31
    move v1, v3

    .line 32
    :cond_1
    if-le v3, v2, :cond_0

    .line 33
    .line 34
    move v2, v3

    .line 35
    goto :goto_0

    .line 36
    :cond_2
    const/4 p1, 0x2

    .line 37
    new-array p1, p1, [I

    .line 38
    .line 39
    aput v1, p1, v0

    .line 40
    .line 41
    const/4 v0, 0x1

    .line 42
    aput v2, p1, v0

    .line 43
    .line 44
    return-object p1
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private Oo0oOo〇0([Ljava/lang/String;)[Ljava/lang/String;
    .locals 7

    .line 1
    array-length v0, p1

    .line 2
    new-array v1, v0, [Ljava/lang/String;

    .line 3
    .line 4
    const/4 v2, 0x0

    .line 5
    const/4 v3, 0x0

    .line 6
    :goto_0
    if-ge v3, v0, :cond_0

    .line 7
    .line 8
    new-instance v4, Ljava/lang/StringBuilder;

    .line 9
    .line 10
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 11
    .line 12
    .line 13
    const-string v5, "%"

    .line 14
    .line 15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    aget-object v6, p1, v3

    .line 19
    .line 20
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v4

    .line 30
    aput-object v4, v1, v3

    .line 31
    .line 32
    add-int/lit8 v3, v3, 0x1

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_0
    mul-int/lit8 p1, v0, 0x5

    .line 36
    .line 37
    new-array v3, p1, [Ljava/lang/String;

    .line 38
    .line 39
    const/4 v4, 0x0

    .line 40
    :cond_1
    if-ge v4, p1, :cond_2

    .line 41
    .line 42
    const/4 v5, 0x0

    .line 43
    :goto_1
    if-ge v5, v0, :cond_1

    .line 44
    .line 45
    aget-object v6, v1, v5

    .line 46
    .line 47
    aput-object v6, v3, v4

    .line 48
    .line 49
    add-int/lit8 v4, v4, 0x1

    .line 50
    .line 51
    add-int/lit8 v5, v5, 0x1

    .line 52
    .line 53
    goto :goto_1

    .line 54
    :cond_2
    return-object v3
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private Oo0oO〇O〇O()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇()Z

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    if-eqz v1, :cond_1

    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 21
    .line 22
    invoke-interface {v1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->O00O()V

    .line 23
    .line 24
    .line 25
    :cond_1
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    const-string v2, "KEY_SET_OCR_LANGUAGE"

    .line 30
    .line 31
    const/4 v3, 0x1

    .line 32
    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    if-eqz v1, :cond_2

    .line 37
    .line 38
    new-instance v1, L〇OoO0o0/o8oO〇;

    .line 39
    .line 40
    invoke-direct {v1, p0}, L〇OoO0o0/o8oO〇;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 41
    .line 42
    .line 43
    invoke-static {v0, v1}, Lcom/intsig/camscanner/app/DialogUtils;->O0o〇〇Oo(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;)V

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇o〇Oo88()V

    .line 48
    .line 49
    .line 50
    :goto_0
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic Oo0〇Ooo()V
    .locals 5

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 2
    .line 3
    invoke-static {v0, v1}, Lcom/intsig/camscanner/scenariodir/util/CertificateDbUtil;->〇o00〇〇Oo(J)Lcom/intsig/camscanner/datastruct/DocItem;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇8oO:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 8
    .line 9
    const-string v1, "PageListPresenter"

    .line 10
    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    const-string v0, "tryCheckRecommendAbroadScenario docItem == null"

    .line 14
    .line 15
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    return-void

    .line 19
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->Oo8Oo00oo()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oo〇O:Ljava/lang/String;

    .line 24
    .line 25
    if-eqz v0, :cond_1

    .line 26
    .line 27
    invoke-static {v0}, Lcom/intsig/camscanner/scenariodir/util/CertificateDbUtil;->O8(Ljava/lang/String;)Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Ooo:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 32
    .line 33
    if-eqz v0, :cond_1

    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/FolderItem;->oo88o8O()I

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    if-ltz v0, :cond_1

    .line 40
    .line 41
    const-string v0, "tryCheckRecommendAbroadScenario sourceFolderItem  dirType > 0"

    .line 42
    .line 43
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    return-void

    .line 47
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇8oO:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 48
    .line 49
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->〇8()I

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    if-eqz v0, :cond_2

    .line 54
    .line 55
    const-string v0, "tryCheckRecommendAbroadScenario is recommended"

    .line 56
    .line 57
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    return-void

    .line 61
    :cond_2
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 62
    .line 63
    iget-wide v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 64
    .line 65
    const/4 v4, 0x1

    .line 66
    invoke-static {v0, v2, v3, v4}, Lcom/intsig/camscanner/db/dao/MTagDao;->〇080(Landroid/content/Context;JZ)Ljava/util/ArrayList;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    invoke-static {v0}, Lcom/intsig/camscanner/scenariodir/util/CertificateUtil;->OO0o〇〇(Ljava/util/List;)I

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    new-instance v2, Ljava/lang/StringBuilder;

    .line 75
    .line 76
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 77
    .line 78
    .line 79
    const-string v3, "tryCheckRecommendAbroadScenario dirType="

    .line 80
    .line 81
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    .line 83
    .line 84
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v2

    .line 91
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    iget-wide v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 95
    .line 96
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 97
    .line 98
    .line 99
    move-result-object v1

    .line 100
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0〇(Ljava/lang/Long;I)V

    .line 101
    .line 102
    .line 103
    return-void
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private synthetic Oo8(Landroid/app/Activity;)V
    .locals 1

    .line 1
    new-instance v0, L〇OoO0o0/〇oOO8O8;

    .line 2
    .line 3
    invoke-direct {v0, p0}, L〇OoO0o0/〇oOO8O8;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic Oo80(Lcom/intsig/mvp/activity/BaseChangeActivity;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 8
    .line 9
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->O00O()V

    .line 10
    .line 11
    .line 12
    :cond_0
    new-instance v0, L〇OoO0o0/〇o;

    .line 13
    .line 14
    invoke-direct {v0, p0}, L〇OoO0o0/〇o;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 15
    .line 16
    .line 17
    invoke-static {p1, v0}, Lcom/intsig/camscanner/share/ShareSuccessDialog;->〇〇o0〇8(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/share/ShareSuccessDialog$ShareContinue;)V

    .line 18
    .line 19
    .line 20
    return-void
.end method

.method public static synthetic Oo8Oo00oo(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Ljava/util/ArrayList;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o88O〇8(Ljava/util/ArrayList;Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic OoO8(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Landroid/app/Activity;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;Ljava/lang/String;ZZ)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p5}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0〇oO〇o(Landroid/app/Activity;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;Ljava/lang/String;ZZ)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method public static synthetic Ooo(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇〇O(Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic Ooo08()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 8
    .line 9
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->O00O()V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic Ooo8〇〇(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;II)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇0o〇o8(II)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private OooO〇(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/pagelist/model/PageItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oo08OO8oO(Ljava/util/List;)[I

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-boolean v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o〇:Z

    .line 6
    .line 7
    const/4 v2, 0x1

    .line 8
    if-eqz v1, :cond_0

    .line 9
    .line 10
    const/4 v3, 0x1

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v3, -0x1

    .line 13
    :goto_0
    if-eqz v1, :cond_1

    .line 14
    .line 15
    const/4 v1, 0x0

    .line 16
    aget v0, v0, v1

    .line 17
    .line 18
    goto :goto_1

    .line 19
    :cond_1
    aget v0, v0, v2

    .line 20
    .line 21
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    if-eqz v1, :cond_2

    .line 30
    .line 31
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    check-cast v1, Lcom/intsig/camscanner/pagelist/model/PageItem;

    .line 36
    .line 37
    iput v0, v1, Lcom/intsig/camscanner/pagelist/model/PageItem;->o〇0:I

    .line 38
    .line 39
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇o8:Ljava/util/HashMap;

    .line 40
    .line 41
    iget-wide v4, v1, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇080:J

    .line 42
    .line 43
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 44
    .line 45
    .line 46
    move-result-object v4

    .line 47
    iget v1, v1, Lcom/intsig/camscanner/pagelist/model/PageItem;->o〇0:I

    .line 48
    .line 49
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    invoke-virtual {v2, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    .line 55
    .line 56
    add-int/2addr v0, v3

    .line 57
    goto :goto_2

    .line 58
    :cond_2
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic Oooo8o0〇(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;)Lkotlin/Unit;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private Oo〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇o8:Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-lez v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇o8:Ljava/util/HashMap;

    .line 10
    .line 11
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8〇OOoooo(Ljava/util/HashMap;)V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇o8:Ljava/util/HashMap;

    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 17
    .line 18
    .line 19
    iget-wide v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 20
    .line 21
    invoke-static {v0, v1}, Lcom/intsig/camscanner/settings/DeveloperActivity;->o〇o08〇(J)V

    .line 22
    .line 23
    .line 24
    :cond_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic Oo〇O(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇8:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private Oo〇O8o〇8(Ljava/lang/String;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_1

    .line 13
    .line 14
    return-void

    .line 15
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 16
    .line 17
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    new-instance v1, Lcom/intsig/utils/CommonLoadingTask;

    .line 22
    .line 23
    new-instance v2, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$8;

    .line 24
    .line 25
    invoke-direct {v2, p0, v0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$8;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Landroid/app/Activity;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    const p1, 0x7f1300a3

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    invoke-direct {v1, v0, v2, p1}, Lcom/intsig/utils/CommonLoadingTask;-><init>(Landroid/content/Context;Lcom/intsig/utils/CommonLoadingTask$TaskCallback;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {v1}, Lcom/intsig/utils/CommonLoadingTask;->O8()V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private O〇08()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/folder/PreferenceFolderHelper;->Oooo8o0〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0o〇〇Oo:Z

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v0, 0x1

    .line 13
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0o〇〇Oo:Z

    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    new-instance v1, L〇OoO0o0/〇80;

    .line 20
    .line 21
    invoke-direct {v1, p0}, L〇OoO0o0/〇80;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 25
    .line 26
    .line 27
    :cond_1
    :goto_0
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic O〇08oOOO0()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 8
    .line 9
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->O00O()V

    .line 10
    .line 11
    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oO〇08o(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic O〇8O8〇008(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Lcom/intsig/mvp/activity/BaseChangeActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oo80(Lcom/intsig/mvp/activity/BaseChangeActivity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O〇OO()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    instance-of v0, v0, Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 8
    .line 9
    xor-int/lit8 v0, v0, 0x1

    .line 10
    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private O〇Oooo〇〇()Lcom/intsig/camscanner/datastruct/FolderItem;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O〇80o08O:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const-string v0, "PageListPresenter"

    .line 6
    .line 7
    const-string v1, "checkIsNeedCreateFolder recommendFolder"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O〇80o08O:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 13
    .line 14
    return-object v0

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OOO:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 16
    .line 17
    invoke-static {v0}, Lcom/intsig/camscanner/scenariodir/util/CertificateUtil;->〇o〇(Lcom/intsig/camscanner/datastruct/FolderItem;)Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    return-object v0
.end method

.method public static synthetic O〇O〇oO(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Landroid/app/Activity;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oo8ooo8O(Landroid/app/Activity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic O〇o88o08〇(Ljava/lang/String;)Lkotlin/Unit;
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    new-instance v1, Lcom/intsig/camscanner/tools/PrepareDataForComposite;

    .line 8
    .line 9
    new-instance v2, Lcom/intsig/camscanner/tools/DataFromDoc;

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 12
    .line 13
    .line 14
    move-result-wide v3

    .line 15
    invoke-direct {v2, v0, v3, v4, p1}, Lcom/intsig/camscanner/tools/DataFromDoc;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    .line 16
    .line 17
    .line 18
    invoke-direct {v1, v0, v2}, Lcom/intsig/camscanner/tools/PrepareDataForComposite;-><init>(Landroid/content/Context;Lcom/intsig/camscanner/tools/JigsawData;)V

    .line 19
    .line 20
    .line 21
    new-instance p1, L〇OoO0o0/o〇8oOO88;

    .line 22
    .line 23
    invoke-direct {p1, p0}, L〇OoO0o0/o〇8oOO88;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/tools/PrepareDataForComposite;->〇80〇808〇O(Lcom/intsig/camscanner/tools/PrepareDataForComposite$FinishPrepareDataCallback;)V

    .line 27
    .line 28
    .line 29
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->oo88o8O()Ljava/util/concurrent/ExecutorService;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    const/4 v0, 0x0

    .line 34
    new-array v0, v0, [Ljava/lang/Void;

    .line 35
    .line 36
    invoke-virtual {v1, p1, v0}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 37
    .line 38
    .line 39
    const/4 p1, 0x0

    .line 40
    return-object p1
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private O〇〇O80o8()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 8
    .line 9
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oo0O〇0〇〇〇(Landroidx/appcompat/app/AppCompatActivity;)Lcom/intsig/camscanner/share/ShareHelper;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    new-instance v2, Lcom/intsig/camscanner/share/type/ShareBatchOcr;

    .line 14
    .line 15
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 16
    .line 17
    .line 18
    move-result-wide v3

    .line 19
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 20
    .line 21
    .line 22
    move-result-object v5

    .line 23
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 24
    .line 25
    .line 26
    move-result-wide v6

    .line 27
    invoke-static {v5, v6, v7}, Lcom/intsig/camscanner/db/dao/ImageDao;->o8oO〇(Landroid/content/Context;J)Ljava/util/ArrayList;

    .line 28
    .line 29
    .line 30
    move-result-object v5

    .line 31
    invoke-direct {v2, v0, v3, v4, v5}, Lcom/intsig/camscanner/share/type/ShareBatchOcr;-><init>(Landroidx/fragment/app/FragmentActivity;JLjava/util/List;)V

    .line 32
    .line 33
    .line 34
    sget-object v0, Lcom/intsig/camscanner/mode_ocr/PageFromType;->FROM_DOC_MORE:Lcom/intsig/camscanner/mode_ocr/PageFromType;

    .line 35
    .line 36
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/share/type/ShareBatchOcr;->o8O〇(Lcom/intsig/camscanner/mode_ocr/PageFromType;)V

    .line 37
    .line 38
    .line 39
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MORE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 40
    .line 41
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/share/type/ShareBatchOcr;->Oo〇O(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 42
    .line 43
    .line 44
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_LIST:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 45
    .line 46
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/share/ShareHelper;->〇〇00O〇0o(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 50
    .line 51
    .line 52
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private o0()Landroidx/fragment/app/FragmentActivity;
    .locals 2
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    return-object v0

    .line 17
    :cond_1
    :goto_0
    const/4 v0, 0x0

    .line 18
    return-object v0
    .line 19
    .line 20
    .line 21
.end method

.method private synthetic o08o〇0(I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oo0oO〇O〇O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o0O0(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)Lcom/intsig/camscanner/pagelist/model/EditType;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO80:Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o0Oo()Z
    .locals 3

    .line 1
    const-string v0, "PageListPresenter"

    .line 2
    .line 3
    const-string v1, "tryCheckRecommendAbroadScenario: START"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇08O8o8()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    const/4 v1, 0x0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    return v1

    .line 16
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/folder/PreferenceFolderHelper;->Oooo8o0〇()Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-eqz v0, :cond_2

    .line 21
    .line 22
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0o〇〇Oo:Z

    .line 23
    .line 24
    if-eqz v0, :cond_1

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_1
    const/4 v0, 0x1

    .line 28
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0o〇〇Oo:Z

    .line 29
    .line 30
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    new-instance v2, L〇OoO0o0/O000;

    .line 35
    .line 36
    invoke-direct {v2, p0}, L〇OoO0o0/O000;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {v1, v2}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 40
    .line 41
    .line 42
    return v0

    .line 43
    :cond_2
    :goto_0
    return v1
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic o0ooO(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Ljava/lang/String;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇o88o08〇(Ljava/lang/String;)Lkotlin/Unit;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o8(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇08oOOO0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o800o8O(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇oO()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o808o8o08()Z
    .locals 10

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o88o0O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-static {}, Lcom/intsig/camscanner/sharedir/recommed/ShareDirRecommendPreferenceHelper;->〇o00〇〇Oo()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    iget-wide v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 14
    .line 15
    invoke-static {v0, v1}, Lcom/intsig/camscanner/sharedir/data/ShareDirDbUtil;->〇080(J)I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    const/4 v1, 0x2

    .line 20
    if-lt v0, v1, :cond_0

    .line 21
    .line 22
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 23
    .line 24
    iget-wide v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 25
    .line 26
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->oO00OOO(Landroid/content/Context;J)Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-nez v0, :cond_0

    .line 31
    .line 32
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 33
    .line 34
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O000:Landroidx/lifecycle/MutableLiveData;

    .line 35
    .line 36
    new-instance v9, Lcom/intsig/camscanner/sharedir/recommed/RecommendSceneEntity;

    .line 37
    .line 38
    const v2, 0x7f131498

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v3

    .line 45
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v4

    .line 49
    const v2, 0x7f131499

    .line 50
    .line 51
    .line 52
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v5

    .line 56
    const v2, 0x7f1314a4

    .line 57
    .line 58
    .line 59
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v6

    .line 63
    const-string v7, "share_more"

    .line 64
    .line 65
    sget-object v8, Lcom/intsig/camscanner/sharedir/recommed/RecommendSceneEntity$RecommendType$RecommendForShare;->〇o00〇〇Oo:Lcom/intsig/camscanner/sharedir/recommed/RecommendSceneEntity$RecommendType$RecommendForShare;

    .line 66
    .line 67
    move-object v2, v9

    .line 68
    invoke-direct/range {v2 .. v8}, Lcom/intsig/camscanner/sharedir/recommed/RecommendSceneEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/sharedir/recommed/RecommendSceneEntity$RecommendType;)V

    .line 69
    .line 70
    .line 71
    invoke-virtual {v1, v9}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 72
    .line 73
    .line 74
    const/4 v0, 0x1

    .line 75
    invoke-static {v0}, Lcom/intsig/camscanner/sharedir/recommed/ShareDirRecommendPreferenceHelper;->O8(Z)V

    .line 76
    .line 77
    .line 78
    return v0

    .line 79
    :cond_0
    const/4 v0, 0x0

    .line 80
    return v0
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private o80ooO(Ljava/util/ArrayList;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "PageListPresenter"

    .line 2
    .line 3
    if-eqz p1, :cond_a

    .line 4
    .line 5
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-nez v1, :cond_0

    .line 10
    .line 11
    goto/16 :goto_6

    .line 12
    .line 13
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o0()Landroidx/fragment/app/FragmentActivity;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    if-eqz v1, :cond_9

    .line 18
    .line 19
    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    if-eqz v2, :cond_1

    .line 24
    .line 25
    goto/16 :goto_5

    .line 26
    .line 27
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 28
    .line 29
    .line 30
    move-result-wide v9

    .line 31
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    if-eqz v2, :cond_2

    .line 40
    .line 41
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 42
    .line 43
    .line 44
    move-result-object v2

    .line 45
    check-cast v2, Ljava/lang/Long;

    .line 46
    .line 47
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    .line 48
    .line 49
    .line 50
    move-result-wide v11

    .line 51
    invoke-static {v11, v12}, Lcom/intsig/camscanner/loadimage/BitmapLoaderUtil;->〇〇888(J)V

    .line 52
    .line 53
    .line 54
    const/4 v5, 0x2

    .line 55
    const/4 v6, 0x1

    .line 56
    const/4 v7, 0x0

    .line 57
    move-object v2, v1

    .line 58
    move-wide v3, v11

    .line 59
    invoke-static/range {v2 .. v7}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->oOo0(Landroid/content/Context;JIZZ)V

    .line 60
    .line 61
    .line 62
    const/4 v7, 0x1

    .line 63
    const/4 v8, 0x0

    .line 64
    invoke-static/range {v2 .. v8}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o〇00O(Landroid/content/Context;JIZZZ)V

    .line 65
    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_2
    new-instance p1, Ljava/util/ArrayList;

    .line 69
    .line 70
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 71
    .line 72
    .line 73
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 74
    .line 75
    .line 76
    move-result-object v2

    .line 77
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 78
    .line 79
    .line 80
    move-result-wide v3

    .line 81
    invoke-static {v3, v4}, Lcom/intsig/camscanner/provider/Documents$Image;->〇080(J)Landroid/net/Uri;

    .line 82
    .line 83
    .line 84
    move-result-object v3

    .line 85
    const-string v4, "_id"

    .line 86
    .line 87
    const-string v8, "page_num"

    .line 88
    .line 89
    filled-new-array {v4, v8}, [Ljava/lang/String;

    .line 90
    .line 91
    .line 92
    move-result-object v4

    .line 93
    const-string v5, "page_num > 0"

    .line 94
    .line 95
    const/4 v6, 0x0

    .line 96
    const-string v7, "page_num ASC"

    .line 97
    .line 98
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 99
    .line 100
    .line 101
    move-result-object v2

    .line 102
    const/4 v3, 0x0

    .line 103
    const/4 v4, 0x1

    .line 104
    if-eqz v2, :cond_5

    .line 105
    .line 106
    const/4 v5, 0x0

    .line 107
    :cond_3
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    .line 108
    .line 109
    .line 110
    move-result v6

    .line 111
    if-eqz v6, :cond_4

    .line 112
    .line 113
    add-int/lit8 v5, v5, 0x1

    .line 114
    .line 115
    invoke-interface {v2, v4}, Landroid/database/Cursor;->getInt(I)I

    .line 116
    .line 117
    .line 118
    move-result v6

    .line 119
    if-eq v5, v6, :cond_3

    .line 120
    .line 121
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    .line 122
    .line 123
    .line 124
    move-result v6

    .line 125
    new-instance v7, Landroid/content/ContentValues;

    .line 126
    .line 127
    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 128
    .line 129
    .line 130
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 131
    .line 132
    .line 133
    move-result-object v11

    .line 134
    invoke-virtual {v7, v8, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 135
    .line 136
    .line 137
    sget-object v11, Lcom/intsig/camscanner/provider/Documents$Image;->〇080:Landroid/net/Uri;

    .line 138
    .line 139
    int-to-long v12, v6

    .line 140
    invoke-static {v11, v12, v13}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 141
    .line 142
    .line 143
    move-result-object v6

    .line 144
    invoke-static {v6}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    .line 145
    .line 146
    .line 147
    move-result-object v6

    .line 148
    invoke-virtual {v6, v7}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    .line 149
    .line 150
    .line 151
    move-result-object v6

    .line 152
    invoke-virtual {v6}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    .line 153
    .line 154
    .line 155
    move-result-object v6

    .line 156
    invoke-virtual {p1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    .line 158
    .line 159
    goto :goto_1

    .line 160
    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 161
    .line 162
    .line 163
    move v3, v5

    .line 164
    :cond_5
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 165
    .line 166
    .line 167
    move-result v2

    .line 168
    if-lez v2, :cond_6

    .line 169
    .line 170
    :try_start_0
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 171
    .line 172
    .line 173
    move-result-object v2

    .line 174
    sget-object v5, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 175
    .line 176
    invoke-virtual {v2, v5, p1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    .line 178
    .line 179
    goto :goto_3

    .line 180
    :catch_0
    move-exception p1

    .line 181
    goto :goto_2

    .line 182
    :catch_1
    move-exception p1

    .line 183
    goto :goto_2

    .line 184
    :catch_2
    move-exception p1

    .line 185
    goto :goto_2

    .line 186
    :catch_3
    move-exception p1

    .line 187
    :goto_2
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 188
    .line 189
    .line 190
    :cond_6
    :goto_3
    invoke-virtual {p0, v4}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8O0880(Z)V

    .line 191
    .line 192
    .line 193
    new-instance p1, Landroid/content/ContentValues;

    .line 194
    .line 195
    invoke-direct {p1}, Landroid/content/ContentValues;-><init>()V

    .line 196
    .line 197
    .line 198
    const-string v2, "pages"

    .line 199
    .line 200
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 201
    .line 202
    .line 203
    move-result-object v5

    .line 204
    invoke-virtual {p1, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 205
    .line 206
    .line 207
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o()Landroid/net/Uri;

    .line 208
    .line 209
    .line 210
    move-result-object v2

    .line 211
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 212
    .line 213
    .line 214
    move-result-object v5

    .line 215
    const/4 v6, 0x0

    .line 216
    invoke-virtual {v5, v2, p1, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 217
    .line 218
    .line 219
    new-instance p1, Ljava/lang/StringBuilder;

    .line 220
    .line 221
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 222
    .line 223
    .line 224
    const-string v5, "after delete, docPageNum="

    .line 225
    .line 226
    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    .line 228
    .line 229
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 230
    .line 231
    .line 232
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 233
    .line 234
    .line 235
    move-result-object p1

    .line 236
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    .line 238
    .line 239
    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 240
    .line 241
    .line 242
    move-result-wide v11

    .line 243
    if-lez v3, :cond_7

    .line 244
    .line 245
    const/4 v5, 0x3

    .line 246
    const/4 v6, 0x1

    .line 247
    const/4 v7, 0x0

    .line 248
    move-object v2, v1

    .line 249
    move-wide v3, v11

    .line 250
    invoke-static/range {v2 .. v7}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o08o〇0(Landroid/content/Context;JIZZ)V

    .line 251
    .line 252
    .line 253
    invoke-static {v1, v11, v12}, Lcom/intsig/camscanner/tsapp/AutoUploadThread;->〇〇8O0〇8(Landroid/content/Context;J)I

    .line 254
    .line 255
    .line 256
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 257
    .line 258
    const/16 v2, 0x6b

    .line 259
    .line 260
    invoke-interface {p1, v2}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->o〇OOo000(I)V

    .line 261
    .line 262
    .line 263
    goto :goto_4

    .line 264
    :cond_7
    iput-boolean v4, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO:Z

    .line 265
    .line 266
    const/4 v5, 0x2

    .line 267
    const/4 v6, 0x1

    .line 268
    const/4 v7, 0x0

    .line 269
    move-object v2, v1

    .line 270
    move-wide v3, v11

    .line 271
    invoke-static/range {v2 .. v7}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o08o〇0(Landroid/content/Context;JIZZ)V

    .line 272
    .line 273
    .line 274
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO0O()V

    .line 275
    .line 276
    .line 277
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 278
    .line 279
    const/16 v2, 0x6c

    .line 280
    .line 281
    invoke-interface {p1, v2}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->o〇OOo000(I)V

    .line 282
    .line 283
    .line 284
    :goto_4
    const-wide/16 v2, 0x0

    .line 285
    .line 286
    cmp-long p1, v11, v2

    .line 287
    .line 288
    if-lez p1, :cond_8

    .line 289
    .line 290
    invoke-static {v1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o〇OOo000(Landroid/content/Context;)V

    .line 291
    .line 292
    .line 293
    :cond_8
    new-instance p1, Ljava/lang/StringBuilder;

    .line 294
    .line 295
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 296
    .line 297
    .line 298
    const-string v1, "remove cost = "

    .line 299
    .line 300
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301
    .line 302
    .line 303
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 304
    .line 305
    .line 306
    move-result-wide v1

    .line 307
    sub-long/2addr v1, v9

    .line 308
    invoke-virtual {p1, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 309
    .line 310
    .line 311
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 312
    .line 313
    .line 314
    move-result-object p1

    .line 315
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    .line 317
    .line 318
    :cond_9
    :goto_5
    return-void

    .line 319
    :cond_a
    :goto_6
    const-string p1, "onRemoveMutilPage, pageIds is empty"

    .line 320
    .line 321
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    .line 323
    .line 324
    return-void
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private o88O〇8(Ljava/util/ArrayList;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/datastruct/PageProperty;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, " go2AutoCompositePreview"

    .line 2
    .line 3
    const-string v1, "PageListPresenter"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 16
    .line 17
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    :try_start_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO〇()Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    iput-object p2, v2, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->oOo〇8o008:Ljava/util/List;

    .line 26
    .line 27
    const/4 p2, 0x2

    .line 28
    invoke-static {v0, p1, v2, p2}, Lcom/intsig/camscanner/topic/TopicPreviewActivity;->O0〇(Landroid/content/Context;Ljava/util/ArrayList;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;I)Landroid/content/Intent;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    const-string p2, "KEY_TOPIC_FROM_COLLAGE_ENTRANCE"

    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8〇o:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 35
    .line 36
    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 37
    .line 38
    .line 39
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 40
    .line 41
    invoke-interface {p2}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getFragment()Landroidx/fragment/app/Fragment;

    .line 42
    .line 43
    .line 44
    move-result-object p2

    .line 45
    invoke-virtual {p2}, Landroidx/fragment/app/Fragment;->isAdded()Z

    .line 46
    .line 47
    .line 48
    move-result p2

    .line 49
    if-eqz p2, :cond_1

    .line 50
    .line 51
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 52
    .line 53
    invoke-interface {p2}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getFragment()Landroidx/fragment/app/Fragment;

    .line 54
    .line 55
    .line 56
    move-result-object p2

    .line 57
    const/16 v0, 0x38d

    .line 58
    .line 59
    invoke-virtual {p2, p1, v0}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    .line 61
    .line 62
    goto :goto_0

    .line 63
    :catch_0
    move-exception p1

    .line 64
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 65
    .line 66
    .line 67
    :cond_1
    :goto_0
    return-void
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method static bridge synthetic o88〇OO08〇(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO00OOO:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o8O(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8O〇:Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;->Oooo8o0〇()Landroidx/lifecycle/MutableLiveData;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {v0}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    check-cast v0, Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;

    .line 19
    .line 20
    new-instance v1, L〇OoO0o0/O8〇o;

    .line 21
    .line 22
    invoke-direct {v1, p0, p1}, L〇OoO0o0/O8〇o;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    invoke-static {v0, v1}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker;->〇o〇(Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;Lkotlin/jvm/functions/Function0;)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic o8O〇(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oo8Oo00oo:Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static synthetic o8o(Lcom/intsig/mvp/activity/BaseChangeActivity;Lcom/intsig/camscanner/datastruct/FolderItem;Ljava/lang/String;Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 0

    .line 1
    const/4 p2, 0x1

    .line 2
    new-array p2, p2, [Ljava/lang/Object;

    .line 3
    .line 4
    const/4 p3, 0x0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/FolderItem;->o800o8O()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    aput-object p1, p2, p3

    .line 10
    .line 11
    const p1, 0x7f131087

    .line 12
    .line 13
    .line 14
    invoke-virtual {p0, p1, p2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    invoke-static {p0, p1}, Lcom/intsig/utils/ToastUtils;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    const-string p0, "PageListPresenter"

    .line 22
    .line 23
    const-string p1, "moveToRecommendDir move success"

    .line 24
    .line 25
    invoke-static {p0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    const/4 p0, 0x0

    .line 29
    return-object p0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private synthetic o8oOOo(II)V
    .locals 1

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0〇O0088o:I

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇080:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇〇808〇()Z

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    if-nez p1, :cond_0

    .line 10
    .line 11
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇080:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇O00()Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->updateTitle(Z)V

    .line 20
    .line 21
    .line 22
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 23
    .line 24
    iget v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0〇O0088o:I

    .line 25
    .line 26
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->O0oO008(I)V

    .line 27
    .line 28
    .line 29
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 30
    .line 31
    invoke-interface {p1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->O〇0()V

    .line 32
    .line 33
    .line 34
    iget p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0〇O0088o:I

    .line 35
    .line 36
    if-lez p1, :cond_1

    .line 37
    .line 38
    iget-boolean p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8oO〇:Z

    .line 39
    .line 40
    if-eqz p1, :cond_1

    .line 41
    .line 42
    const/4 p1, 0x0

    .line 43
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8oO〇:Z

    .line 44
    .line 45
    :cond_1
    const/4 p1, 0x4

    .line 46
    if-ne p2, p1, :cond_2

    .line 47
    .line 48
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0OO8〇0()V

    .line 49
    .line 50
    .line 51
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 52
    .line 53
    invoke-interface {p1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->oO8o()V

    .line 54
    .line 55
    .line 56
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 57
    .line 58
    invoke-interface {p1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->〇〇O00〇8()V

    .line 59
    .line 60
    .line 61
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic o8oO〇(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Lcom/intsig/camscanner/datastruct/FolderItem;Ljava/lang/String;Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oOO〇〇(Lcom/intsig/camscanner/datastruct/FolderItem;Ljava/lang/String;Ljava/lang/Boolean;)Lkotlin/Unit;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private o8o〇〇0O()Ljava/lang/String;
    .locals 4

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "getPageOrder "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget-boolean v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o〇:Z

    .line 12
    .line 13
    const-string v2, "page_num ASC"

    .line 14
    .line 15
    const-string v3, "page_num DESC"

    .line 16
    .line 17
    if-eqz v1, :cond_0

    .line 18
    .line 19
    move-object v1, v2

    .line 20
    goto :goto_0

    .line 21
    :cond_0
    move-object v1, v3

    .line 22
    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    const-string v1, "PageListPresenter"

    .line 30
    .line 31
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o〇:Z

    .line 35
    .line 36
    if-eqz v0, :cond_1

    .line 37
    .line 38
    goto :goto_1

    .line 39
    :cond_1
    move-object v2, v3

    .line 40
    :goto_1
    return-object v2
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic o8〇OO(Landroid/app/Activity;)V
    .locals 1

    .line 1
    new-instance v0, L〇OoO0o0/O8ooOoo〇;

    .line 2
    .line 3
    invoke-direct {v0, p0}, L〇OoO0o0/O8ooOoo〇;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static synthetic o8〇OO0〇0o(Lcom/intsig/callback/Callback0;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-interface {p0}, Lcom/intsig/callback/Callback0;->call()V

    .line 2
    .line 3
    .line 4
    const/4 p0, 0x0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic oO(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇OO0(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic oO0()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 8
    .line 9
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->O00O()V

    .line 10
    .line 11
    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oO〇08o(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic oO00OOO(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Landroid/app/Activity;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oo8(Landroid/app/Activity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic oO0〇〇o8〇(Lcom/intsig/camscanner/capture/SupportCaptureModeOption;Ljava/lang/String;Z[Ljava/lang/String;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooO〇00O(Lcom/intsig/camscanner/capture/SupportCaptureModeOption;Ljava/lang/String;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method public static synthetic oO80(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇00O(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private oO8o()Lcom/intsig/camscanner/util/PdfEncryptionUtil;
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o:Lcom/intsig/camscanner/util/PdfEncryptionUtil;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/util/PdfEncryptionUtil;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 8
    .line 9
    invoke-interface {v1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o()Landroid/net/Uri;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    new-instance v3, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$7;

    .line 18
    .line 19
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$7;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 20
    .line 21
    .line 22
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/camscanner/util/PdfEncryptionUtil;-><init>(Landroid/app/Activity;Landroid/net/Uri;Lcom/intsig/camscanner/util/PdfEncryptionUtil$PdfEncStatusListener;)V

    .line 23
    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o:Lcom/intsig/camscanner/util/PdfEncryptionUtil;

    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/PdfEncryptionUtil;->Oooo8o0〇()Z

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o0ooO:Z

    .line 32
    .line 33
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o:Lcom/intsig/camscanner/util/PdfEncryptionUtil;

    .line 34
    .line 35
    const-string v2, "CSMorePop"

    .line 36
    .line 37
    const-string v3, "pdf_password"

    .line 38
    .line 39
    const-string v4, "confirm_pdf_password"

    .line 40
    .line 41
    const-string v5, "cancel_pdf_password"

    .line 42
    .line 43
    const-string v6, "reset_pdf_password"

    .line 44
    .line 45
    invoke-virtual/range {v1 .. v6}, Lcom/intsig/camscanner/util/PdfEncryptionUtil;->〇0〇O0088o(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o:Lcom/intsig/camscanner/util/PdfEncryptionUtil;

    .line 49
    .line 50
    return-object v0
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic oOO〇〇(Lcom/intsig/camscanner/datastruct/FolderItem;Ljava/lang/String;Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇80:Landroidx/lifecycle/MutableLiveData;

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/FolderItem;->o〇O8〇〇o()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    invoke-virtual {p2, p1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 10
    .line 11
    .line 12
    :cond_0
    const-string p1, "PageListPresenter"

    .line 13
    .line 14
    const-string p2, "moveToRecommendDir copy success"

    .line 15
    .line 16
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    const/4 p1, 0x0

    .line 20
    return-object p1
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic oOo0(I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8o〇O0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic oOo〇8o008(II)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇800OO〇0O(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private oO〇8O8oOo()V
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "PageListPresenter"

    .line 8
    .line 9
    if-eqz v0, :cond_5

    .line 10
    .line 11
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    if-nez v2, :cond_5

    .line 16
    .line 17
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 18
    .line 19
    invoke-interface {v2}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getFragment()Landroidx/fragment/app/Fragment;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    invoke-static {v2}, Lcom/intsig/camscanner/mainmenu/CsLifecycleUtil;->〇080(Landroidx/lifecycle/LifecycleOwner;)Z

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    if-eqz v2, :cond_0

    .line 28
    .line 29
    goto/16 :goto_1

    .line 30
    .line 31
    :cond_0
    iget-wide v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 32
    .line 33
    const-wide/16 v4, 0x0

    .line 34
    .line 35
    cmp-long v6, v2, v4

    .line 36
    .line 37
    if-gez v6, :cond_1

    .line 38
    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    .line 40
    .line 41
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 42
    .line 43
    .line 44
    const-string v2, "mDocId = "

    .line 45
    .line 46
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    iget-wide v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 50
    .line 51
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    return-void

    .line 62
    :cond_1
    iget-object v4, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 63
    .line 64
    invoke-static {v0, v2, v3}, Lcom/intsig/camscanner/data/dao/ShareDirDao;->Oooo8o0〇(Landroid/content/Context;J)Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;

    .line 65
    .line 66
    .line 67
    move-result-object v2

    .line 68
    invoke-interface {v4, v2}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->oO80OOO〇(Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;)V

    .line 69
    .line 70
    .line 71
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 72
    .line 73
    .line 74
    move-result-object v5

    .line 75
    iget-wide v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 76
    .line 77
    invoke-static {v2, v3}, Lcom/intsig/camscanner/provider/Documents$Image;->〇080(J)Landroid/net/Uri;

    .line 78
    .line 79
    .line 80
    move-result-object v6

    .line 81
    sget-object v7, Lcom/intsig/camscanner/pagelist/model/PageListModel;->〇OOo8〇0:[Ljava/lang/String;

    .line 82
    .line 83
    const/4 v8, 0x0

    .line 84
    const/4 v9, 0x0

    .line 85
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8o〇〇0O()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v10

    .line 89
    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 90
    .line 91
    .line 92
    move-result-object v2

    .line 93
    if-eqz v2, :cond_4

    .line 94
    .line 95
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇0:Lcom/intsig/camscanner/pagelist/model/PageListModel;

    .line 96
    .line 97
    invoke-virtual {v3, v2}, Lcom/intsig/camscanner/pagelist/model/PageListModel;->〇080(Landroid/database/Cursor;)Ljava/util/List;

    .line 98
    .line 99
    .line 100
    move-result-object v3

    .line 101
    invoke-interface {v3}, Ljava/util/List;->size()I

    .line 102
    .line 103
    .line 104
    move-result v4

    .line 105
    if-nez v4, :cond_2

    .line 106
    .line 107
    const-string v0, "pageItemList.size() == 0"

    .line 108
    .line 109
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    .line 111
    .line 112
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 113
    .line 114
    .line 115
    new-instance v0, L〇OoO0o0/〇8〇0〇o〇O;

    .line 116
    .line 117
    invoke-direct {v0, p0}, L〇OoO0o0/〇8〇0〇o〇O;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 118
    .line 119
    .line 120
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇8〇(Ljava/lang/Runnable;)V

    .line 121
    .line 122
    .line 123
    return-void

    .line 124
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    .line 125
    .line 126
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 127
    .line 128
    .line 129
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 130
    .line 131
    .line 132
    move-result-object v5

    .line 133
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    .line 134
    .line 135
    .line 136
    move-result v6

    .line 137
    if-eqz v6, :cond_3

    .line 138
    .line 139
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 140
    .line 141
    .line 142
    move-result-object v6

    .line 143
    check-cast v6, Lcom/intsig/camscanner/pagelist/model/PageItem;

    .line 144
    .line 145
    new-instance v7, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 146
    .line 147
    invoke-direct {v7}, Lcom/intsig/camscanner/pagelist/model/PageImageItem;-><init>()V

    .line 148
    .line 149
    .line 150
    invoke-virtual {v7, v6}, Lcom/intsig/camscanner/pagelist/model/PageImageItem;->〇80〇808〇O(Lcom/intsig/camscanner/pagelist/model/PageItem;)V

    .line 151
    .line 152
    .line 153
    sget-object v6, Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;->IMAGE:Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;

    .line 154
    .line 155
    invoke-virtual {v6}, Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;->getType()I

    .line 156
    .line 157
    .line 158
    move-result v6

    .line 159
    invoke-virtual {v7, v6}, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;->setType(I)V

    .line 160
    .line 161
    .line 162
    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    .line 164
    .line 165
    goto :goto_0

    .line 166
    :cond_3
    iget-object v5, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇080:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 167
    .line 168
    invoke-virtual {v5, v2}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->O8ooOoo〇(Landroid/database/Cursor;)V

    .line 169
    .line 170
    .line 171
    new-instance v5, L〇OoO0o0/O〇O〇oO;

    .line 172
    .line 173
    invoke-direct {v5, p0, v3, v0, v4}, L〇OoO0o0/O〇O〇oO;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Ljava/util/List;Landroid/app/Activity;Ljava/util/List;)V

    .line 174
    .line 175
    .line 176
    invoke-direct {p0, v5}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇8〇(Ljava/lang/Runnable;)V

    .line 177
    .line 178
    .line 179
    const-string v0, "pageLoaderManagerListener>>>"

    .line 180
    .line 181
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    .line 183
    .line 184
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 185
    .line 186
    .line 187
    :cond_4
    return-void

    .line 188
    :cond_5
    :goto_1
    const-string v0, "initPageLifecycleDataChangerManager stop"

    .line 189
    .line 190
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    .line 192
    .line 193
    return-void
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private oo(Lcom/intsig/camscanner/web/UrlEntity;)Z
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/web/UrlEntity;->O8()Lcom/intsig/camscanner/web/FUNCTION;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/camscanner/web/UrlEntity;->〇〇888()Ljava/util/HashMap;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    sget-object v1, Lcom/intsig/camscanner/web/PARAMATER_KEY;->position:Lcom/intsig/camscanner/web/PARAMATER_KEY;

    .line 10
    .line 11
    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    check-cast p1, Ljava/lang/String;

    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    const-string v2, "PageListPresenter"

    .line 19
    .line 20
    if-eqz p1, :cond_1

    .line 21
    .line 22
    sget-object v3, Lcom/intsig/camscanner/web/PARAMATER_VALUE;->pageList:Lcom/intsig/camscanner/web/PARAMATER_VALUE;

    .line 23
    .line 24
    invoke-virtual {v3}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v3

    .line 28
    invoke-virtual {v3, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 29
    .line 30
    .line 31
    move-result p1

    .line 32
    if-nez p1, :cond_0

    .line 33
    .line 34
    goto :goto_2

    .line 35
    :cond_0
    sget-object p1, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$16;->〇080:[I

    .line 36
    .line 37
    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    .line 38
    .line 39
    .line 40
    move-result v3

    .line 41
    aget p1, p1, v3

    .line 42
    .line 43
    packed-switch p1, :pswitch_data_0

    .line 44
    .line 45
    .line 46
    new-instance p1, Ljava/lang/StringBuilder;

    .line 47
    .line 48
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 49
    .line 50
    .line 51
    const-string v3, "function is "

    .line 52
    .line 53
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    goto :goto_1

    .line 71
    :pswitch_0
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->CERTIFICATE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 72
    .line 73
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇〇0〇:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 74
    .line 75
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OOo8o〇O()V

    .line 76
    .line 77
    .line 78
    goto :goto_0

    .line 79
    :pswitch_1
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->CERTIFICATE_PHOTO:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 80
    .line 81
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇〇0〇:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 82
    .line 83
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OOo8o〇O()V

    .line 84
    .line 85
    .line 86
    goto :goto_0

    .line 87
    :pswitch_2
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->OCR:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 88
    .line 89
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇〇0〇:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 90
    .line 91
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OOo8o〇O()V

    .line 92
    .line 93
    .line 94
    goto :goto_0

    .line 95
    :pswitch_3
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->TOPIC:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 96
    .line 97
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇〇0〇:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 98
    .line 99
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OOo8o〇O()V

    .line 100
    .line 101
    .line 102
    goto :goto_0

    .line 103
    :pswitch_4
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->BOOK_SPLITTER:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 104
    .line 105
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇〇0〇:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 106
    .line 107
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OOo8o〇O()V

    .line 108
    .line 109
    .line 110
    goto :goto_0

    .line 111
    :pswitch_5
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->PPT:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 112
    .line 113
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇〇0〇:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 114
    .line 115
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OOo8o〇O()V

    .line 116
    .line 117
    .line 118
    goto :goto_0

    .line 119
    :pswitch_6
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->DOC_TO_EXCEL:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 120
    .line 121
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇〇0〇:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 122
    .line 123
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OOo8o〇O()V

    .line 124
    .line 125
    .line 126
    goto :goto_0

    .line 127
    :pswitch_7
    sget-object p1, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 128
    .line 129
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇〇0〇:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 130
    .line 131
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OOo8o〇O()V

    .line 132
    .line 133
    .line 134
    :goto_0
    const/4 v1, 0x1

    .line 135
    :goto_1
    return v1

    .line 136
    :cond_1
    :goto_2
    const-string p1, "function is null or position`s value is not pageList"

    .line 137
    .line 138
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    .line 140
    .line 141
    return v1

    .line 142
    nop

    .line 143
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private synthetic oo08OO〇0()V
    .locals 3

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 2
    .line 3
    invoke-static {v0, v1}, Lcom/intsig/camscanner/scenariodir/util/CertificateDbUtil;->〇o00〇〇Oo(J)Lcom/intsig/camscanner/datastruct/DocItem;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇8oO:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 8
    .line 9
    const-string v1, "PageListPresenter"

    .line 10
    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    const-string v0, "checkRecommendScenarioDir docItem == null"

    .line 14
    .line 15
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    return-void

    .line 19
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->Oo8Oo00oo()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oo〇O:Ljava/lang/String;

    .line 24
    .line 25
    if-eqz v0, :cond_1

    .line 26
    .line 27
    invoke-static {v0}, Lcom/intsig/camscanner/scenariodir/util/CertificateDbUtil;->O8(Ljava/lang/String;)Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Ooo:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 32
    .line 33
    if-eqz v0, :cond_1

    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/FolderItem;->oo88o8O()I

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    if-ltz v0, :cond_1

    .line 40
    .line 41
    const-string v0, "checkRecommendScenarioDir sourceFolderItem  dirType > 0"

    .line 42
    .line 43
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    return-void

    .line 47
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/util/CertificateUtil;->〇080:Lcom/intsig/camscanner/scenariodir/util/CertificateUtil;

    .line 48
    .line 49
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/util/CertificateUtil;->〇O〇()Z

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    const-string v2, "checkRecommendScenarioDir is recommended"

    .line 54
    .line 55
    if-nez v0, :cond_2

    .line 56
    .line 57
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    return-void

    .line 61
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇8oO:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 62
    .line 63
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->〇8()I

    .line 64
    .line 65
    .line 66
    move-result v0

    .line 67
    if-eqz v0, :cond_3

    .line 68
    .line 69
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    return-void

    .line 73
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇8oO:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 74
    .line 75
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->〇o()I

    .line 76
    .line 77
    .line 78
    move-result v0

    .line 79
    if-lez v0, :cond_4

    .line 80
    .line 81
    const-string v0, "checkRecommendScenarioDir type: $docNewType"

    .line 82
    .line 83
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    iget-wide v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 87
    .line 88
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 89
    .line 90
    .line 91
    move-result-object v0

    .line 92
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇8oO:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 93
    .line 94
    invoke-virtual {v1}, Lcom/intsig/camscanner/datastruct/DocItem;->〇o()I

    .line 95
    .line 96
    .line 97
    move-result v1

    .line 98
    invoke-static {v1}, Lcom/intsig/camscanner/scenariodir/util/CertificateUtil;->〇8o8o〇(I)I

    .line 99
    .line 100
    .line 101
    move-result v1

    .line 102
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0〇(Ljava/lang/Long;I)V

    .line 103
    .line 104
    .line 105
    return-void

    .line 106
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇8oO:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 107
    .line 108
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->Oooo8o0〇()I

    .line 109
    .line 110
    .line 111
    move-result v0

    .line 112
    if-lez v0, :cond_5

    .line 113
    .line 114
    const-string v0, "checkRecommendScenarioDir recognized but not has type"

    .line 115
    .line 116
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    .line 118
    .line 119
    return-void

    .line 120
    :cond_5
    iget-wide v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 121
    .line 122
    new-instance v2, L〇OoO0o0/〇080;

    .line 123
    .line 124
    invoke-direct {v2, p0}, L〇OoO0o0/〇080;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 125
    .line 126
    .line 127
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/scenariodir/util/CertificateUtil;->〇0〇O0088o(JLkotlin/jvm/functions/Function3;)V

    .line 128
    .line 129
    .line 130
    return-void
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic oo88o8O(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇o〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic oo8ooo8O(Landroid/app/Activity;)V
    .locals 7

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "onActionReceived updateDocImages start doc id = "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget-wide v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 12
    .line 13
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "PageListPresenter"

    .line 21
    .line 22
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    iget-wide v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 26
    .line 27
    invoke-static {p1, v0, v1}, Lcom/intsig/camscanner/data/dao/ShareDirDao;->Oooo8o0〇(Landroid/content/Context;J)Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇080()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    if-nez v1, :cond_0

    .line 40
    .line 41
    invoke-virtual {v0}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇o00〇〇Oo()I

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    if-nez v0, :cond_1

    .line 46
    .line 47
    :cond_0
    iget-wide v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 48
    .line 49
    invoke-static {p1, v0, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇o8OO0(Landroid/content/Context;J)Z

    .line 50
    .line 51
    .line 52
    move-result v0

    .line 53
    if-nez v0, :cond_1

    .line 54
    .line 55
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncImgDownloadHelper;->〇080()Lcom/intsig/camscanner/tsapp/sync/SyncImgDownloadHelper;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    iget-wide v3, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 60
    .line 61
    const/4 v5, 0x0

    .line 62
    invoke-static {p1}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->oo〇(Landroid/content/Context;)Lcom/intsig/camscanner/tsapp/sync/SyncThread;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->o〇0OOo〇0()Ljava/util/Vector;

    .line 67
    .line 68
    .line 69
    move-result-object v6

    .line 70
    move-object v2, p1

    .line 71
    invoke-virtual/range {v1 .. v6}, Lcom/intsig/camscanner/tsapp/sync/SyncImgDownloadHelper;->〇o00〇〇Oo(Landroid/content/Context;JLcom/intsig/tianshu/sync/SyncApi$SyncProgress;Ljava/util/Vector;)Ljava/util/concurrent/Future;

    .line 72
    .line 73
    .line 74
    :cond_1
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private synthetic ooO(I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇〇O80o8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic ooOO(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO00OOO:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private ooo0〇O88O()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    goto :goto_1

    .line 14
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 15
    :goto_1
    if-eqz v0, :cond_2

    .line 16
    .line 17
    const-string v1, "PageListPresenter"

    .line 18
    .line 19
    const-string v2, "checkContextNull >>> context is null."

    .line 20
    .line 21
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    :cond_2
    return v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic ooo0〇〇O(Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->〇〇O00〇8()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8O〇:Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 9
    .line 10
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;->〇8o8o〇()Z

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    invoke-interface {v0, p1, v1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->〇〇0o〇o8(Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;Z)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic ooo〇8oO(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)Lcom/intsig/advertisement/listener/OnAdPositionListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇〇0〇〇0:Lcom/intsig/advertisement/listener/OnAdPositionListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic oo〇(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇o0〇8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o〇0(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇08O(Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic o〇00O(I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇o0〇8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic o〇00O0O〇o(Ljava/lang/Runnable;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getFragment()Landroidx/fragment/app/Fragment;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {v0}, Lcom/intsig/camscanner/mainmenu/CsLifecycleUtil;->〇080(Landroidx/lifecycle/LifecycleOwner;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    const-string p1, "PageListPresenter"

    .line 14
    .line 15
    const-string v0, "executeRunnableOnUI CsLifecycleUtil.isDestroyed(this) || CsLifecycleUtil.isDestroyed(mActivity)"

    .line 16
    .line 17
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    return-void

    .line 21
    :cond_0
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private o〇08oO80o()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇O:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->o〇0()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic o〇0OOo〇0(Lcom/intsig/mvp/activity/BaseChangeActivity;Lcom/intsig/camscanner/datastruct/FolderItem;Ljava/lang/String;Ljava/lang/Boolean;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8o(Lcom/intsig/mvp/activity/BaseChangeActivity;Lcom/intsig/camscanner/datastruct/FolderItem;Ljava/lang/String;Ljava/lang/Boolean;)Lkotlin/Unit;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private o〇0o〇〇()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 4
    .line 5
    invoke-interface {v1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getFragment()Landroidx/fragment/app/Fragment;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    const-string v2, "page_list_load_doc"

    .line 10
    .line 11
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;-><init>(Landroidx/lifecycle/LifecycleOwner;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇O:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 15
    .line 16
    const-wide/16 v1, 0x9c4

    .line 17
    .line 18
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->〇80〇808〇O(J)V

    .line 19
    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇O:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 22
    .line 23
    new-instance v1, L〇OoO0o0/oo88o8O;

    .line 24
    .line 25
    invoke-direct {v1, p0}, L〇OoO0o0/oo88o8O;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->OO0o〇〇(Ljava/lang/Runnable;)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic o〇0〇(Landroid/app/Activity;)V
    .locals 1

    .line 1
    new-instance v0, L〇OoO0o0/〇〇〇0〇〇0;

    .line 2
    .line 3
    invoke-direct {v0, p0}, L〇OoO0o0/〇〇〇0〇〇0;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o〇8(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Ljava/util/ArrayList;Landroid/app/Activity;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oOoo〇(Ljava/util/ArrayList;Landroid/app/Activity;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic o〇8oOO88(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO〇8O8oOo()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o〇8〇(Ljava/lang/Runnable;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    if-eqz p1, :cond_1

    .line 10
    .line 11
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    new-instance v1, L〇OoO0o0/〇8;

    .line 19
    .line 20
    invoke-direct {v1, p0, p1}, L〇OoO0o0/〇8;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Ljava/lang/Runnable;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 24
    .line 25
    .line 26
    return-void

    .line 27
    :cond_1
    :goto_0
    const-string p1, "PageListPresenter"

    .line 28
    .line 29
    const-string v0, "executeRunnableOnUI mActivity == null || runnable == null"

    .line 30
    .line 31
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic o〇O(Lcom/intsig/callback/Callback0;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8〇OO0〇0o(Lcom/intsig/callback/Callback0;)Lkotlin/Unit;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o〇O8〇〇o(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇00O0(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private o〇o08〇(Landroid/app/Activity;I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o88〇OO08〇:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;

    .line 6
    .line 7
    invoke-direct {v0}, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o88〇OO08〇:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;

    .line 11
    .line 12
    iput-object p1, v0, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;->〇o〇:Landroid/app/Activity;

    .line 13
    .line 14
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o88〇OO08〇:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;

    .line 15
    .line 16
    add-int/lit8 p2, p2, 0x1

    .line 17
    .line 18
    iput p2, p1, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;->〇080:I

    .line 19
    .line 20
    iget p2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8:I

    .line 21
    .line 22
    iput p2, p1, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;->〇o00〇〇Oo:I

    .line 23
    .line 24
    iget-boolean p2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oOO8O8:Z

    .line 25
    .line 26
    iput-boolean p2, p1, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;->O8:Z

    .line 27
    .line 28
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OOO〇O0:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$OnMultipleFunctionResponse;

    .line 29
    .line 30
    iput-object p2, p1, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;->oO80:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$OnMultipleFunctionResponse;

    .line 31
    .line 32
    iget-wide v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 33
    .line 34
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 35
    .line 36
    .line 37
    move-result-object p2

    .line 38
    invoke-static {p2}, Lcom/intsig/camscanner/paper/CamExamGuideManager;->〇O8o08O(Ljava/lang/Long;)Z

    .line 39
    .line 40
    .line 41
    move-result p2

    .line 42
    iput-boolean p2, p1, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;->〇80〇808〇O:Z

    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private synthetic o〇oO()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo〇O〇80(Z)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic o〇〇0〇(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Landroid/app/Activity;Ljava/util/ArrayList;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇08O〇00〇o(Landroid/app/Activity;Ljava/util/ArrayList;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method static bridge synthetic 〇0(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oo8Oo00oo:Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇00(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OOo()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇0000OOO(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oo08OO〇0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇000O0(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇800OO〇0O(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇000〇〇08(JLjava/util/ArrayList;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/ArrayList<",
            "Landroid/content/ContentProviderOperation;",
            ">;I)V"
        }
    .end annotation

    .line 1
    new-instance v0, Landroid/content/ContentValues;

    .line 2
    .line 3
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "page_num"

    .line 7
    .line 8
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 9
    .line 10
    .line 11
    move-result-object p4

    .line 12
    invoke-virtual {v0, v1, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 13
    .line 14
    .line 15
    sget-object p4, Lcom/intsig/camscanner/provider/Documents$Image;->〇080:Landroid/net/Uri;

    .line 16
    .line 17
    invoke-static {p4, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 18
    .line 19
    .line 20
    move-result-object p4

    .line 21
    invoke-static {p4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    .line 22
    .line 23
    .line 24
    move-result-object p4

    .line 25
    invoke-virtual {p4, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    .line 26
    .line 27
    .line 28
    move-result-object p4

    .line 29
    invoke-virtual {p4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    .line 30
    .line 31
    .line 32
    move-result-object p4

    .line 33
    invoke-virtual {p3, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    .line 35
    .line 36
    new-instance p3, Ljava/lang/StringBuilder;

    .line 37
    .line 38
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 39
    .line 40
    .line 41
    const-string p4, "createPagesUpdateOps at "

    .line 42
    .line 43
    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {p3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    const-string p1, " set to "

    .line 50
    .line 51
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    const-string p2, "PageListPresenter"

    .line 62
    .line 63
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    return-void
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic 〇00O0(I)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇()Z

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 8
    .line 9
    invoke-interface {p1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->O00O()V

    .line 10
    .line 11
    .line 12
    :cond_0
    const/4 p1, 0x0

    .line 13
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oO〇08o(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇00O0O0(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oo〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇00〇8(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Ljava/lang/Runnable;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇00O0O〇o(Ljava/lang/Runnable;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇080(Lcom/intsig/callback/Callback0;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇00〇8oO(Lcom/intsig/callback/Callback0;)Lkotlin/Unit;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇080OO8〇0(Ljava/lang/String;I)V
    .locals 0

    .line 1
    const/4 p1, 0x1

    .line 2
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo〇O〇80(Z)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇08O8o8()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/util/CertificateUtil;->〇080:Lcom/intsig/camscanner/scenariodir/util/CertificateUtil;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/util/CertificateUtil;->〇〇808〇()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const-string v0, "PageListPresenter"

    .line 10
    .line 11
    const-string v1, "isRecommendDirWithAbroadScenarioDir NO NEED"

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const/4 v0, 0x0

    .line 17
    return v0

    .line 18
    :cond_0
    const/4 v0, 0x1

    .line 19
    return v0
    .line 20
    .line 21
.end method

.method public static synthetic 〇08O8o〇0(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Lcom/intsig/mvp/activity/BaseChangeActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇OO〇00〇0O(Lcom/intsig/mvp/activity/BaseChangeActivity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic 〇08O〇00〇o(Landroid/app/Activity;Ljava/util/ArrayList;I)V
    .locals 1

    .line 1
    new-instance p3, Lcom/intsig/utils/CommonLoadingTask;

    .line 2
    .line 3
    new-instance v0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$6;

    .line 4
    .line 5
    invoke-direct {v0, p0, p1, p2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$6;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Landroid/app/Activity;Ljava/util/ArrayList;)V

    .line 6
    .line 7
    .line 8
    const-string p2, ""

    .line 9
    .line 10
    invoke-direct {p3, p1, v0, p2}, Lcom/intsig/utils/CommonLoadingTask;-><init>(Landroid/content/Context;Lcom/intsig/utils/CommonLoadingTask$TaskCallback;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p3}, Lcom/intsig/utils/CommonLoadingTask;->O8()V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic 〇08〇o0O(Lcom/intsig/mvp/activity/BaseChangeActivity;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 8
    .line 9
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->O00O()V

    .line 10
    .line 11
    .line 12
    :cond_0
    new-instance v0, L〇OoO0o0/o0ooO;

    .line 13
    .line 14
    invoke-direct {v0, p0}, L〇OoO0o0/o0ooO;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 15
    .line 16
    .line 17
    invoke-static {p1, v0}, Lcom/intsig/camscanner/share/ShareSuccessDialog;->〇〇o0〇8(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/share/ShareSuccessDialog$ShareContinue;)V

    .line 18
    .line 19
    .line 20
    return-void
.end method

.method private synthetic 〇0O(Landroid/app/Activity;I)V
    .locals 3

    .line 1
    new-instance p2, Landroid/content/Intent;

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    const-class v1, Lcom/intsig/camscanner/uploadfaxprint/UploadFaxPrintActivity;

    .line 5
    .line 6
    const-string v2, "android.intent.action.SEND"

    .line 7
    .line 8
    invoke-direct {p2, v2, v0, p1, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 9
    .line 10
    .line 11
    const-string p1, "SEND_TYPE"

    .line 12
    .line 13
    const/16 v0, 0xa

    .line 14
    .line 15
    invoke-virtual {p2, p1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 16
    .line 17
    .line 18
    const-string p1, "doc_id"

    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 21
    .line 22
    .line 23
    move-result-wide v0

    .line 24
    invoke-virtual {p2, p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 25
    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    const/4 v0, 0x1

    .line 32
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇O8o08O(Z)[I

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    const-string v1, "send_multi_page_pos"

    .line 37
    .line 38
    invoke-virtual {p2, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 39
    .line 40
    .line 41
    const-string p1, "is_need_suffix"

    .line 42
    .line 43
    invoke-virtual {p2, p1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 44
    .line 45
    .line 46
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 47
    .line 48
    invoke-interface {p1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getFragment()Landroidx/fragment/app/Fragment;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    invoke-virtual {p1}, Landroidx/fragment/app/Fragment;->isAdded()Z

    .line 53
    .line 54
    .line 55
    move-result p1

    .line 56
    if-eqz p1, :cond_0

    .line 57
    .line 58
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 59
    .line 60
    invoke-interface {p1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getFragment()Landroidx/fragment/app/Fragment;

    .line 61
    .line 62
    .line 63
    move-result-object p1

    .line 64
    invoke-virtual {p1, p2}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 65
    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_0
    const-string p1, "PageListPresenter"

    .line 69
    .line 70
    const-string p2, "activity not Attach when UploadFaxPrintActivity"

    .line 71
    .line 72
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    :goto_0
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private 〇0OO8(Ljava/util/List;II)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/pagelist/model/PageTypeItem;",
            ">;II)",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/pagelist/model/PageItem;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {p2, p3}, Ljava/lang/Math;->min(II)I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-static {p2, p3}, Ljava/lang/Math;->max(II)I

    .line 6
    .line 7
    .line 8
    move-result p2

    .line 9
    new-instance p3, Ljava/util/ArrayList;

    .line 10
    .line 11
    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    .line 12
    .line 13
    .line 14
    :goto_0
    if-gt v0, p2, :cond_1

    .line 15
    .line 16
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    check-cast v1, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;

    .line 21
    .line 22
    instance-of v2, v1, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 23
    .line 24
    if-eqz v2, :cond_0

    .line 25
    .line 26
    check-cast v1, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 27
    .line 28
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/model/PageImageItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/pagelist/model/PageItem;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    invoke-interface {p3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    .line 34
    .line 35
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_1
    return-object p3
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic 〇0O〇Oo(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Ljava/util/List;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO0〇〇8(Ljava/util/List;)Ljava/util/ArrayList;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇0〇O0088o(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Ljava/lang/String;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇080OO8〇0(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇8(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;II)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8oOOo(II)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇80(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇OOo8〇0(Lcom/intsig/camscanner/datastruct/ParcelDocInfo;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private 〇800OO〇0O(I)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    new-instance v1, Lcom/intsig/utils/CommonLoadingTask;

    .line 15
    .line 16
    new-instance v2, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$10;

    .line 17
    .line 18
    invoke-direct {v2, p0, v0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$10;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Landroid/app/Activity;I)V

    .line 19
    .line 20
    .line 21
    const p1, 0x7f1300a3

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    invoke-direct {v1, v0, v2, p1}, Lcom/intsig/utils/CommonLoadingTask;-><init>(Landroid/content/Context;Lcom/intsig/utils/CommonLoadingTask$TaskCallback;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {v1}, Lcom/intsig/utils/CommonLoadingTask;->O8()V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic 〇80〇808〇O(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Lcom/intsig/camscanner/web/UrlEntity;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O80〇oOo(Lcom/intsig/camscanner/web/UrlEntity;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇8o8o〇(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oo0oO〇O〇O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇8〇0〇o〇O(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooO(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇8〇OOoooo(Ljava/util/HashMap;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/Long;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o0()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    .line 9
    .line 10
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    if-eqz v2, :cond_1

    .line 26
    .line 27
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    check-cast v2, Ljava/util/Map$Entry;

    .line 32
    .line 33
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object v3

    .line 37
    check-cast v3, Ljava/lang/Long;

    .line 38
    .line 39
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    .line 40
    .line 41
    .line 42
    move-result-wide v3

    .line 43
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 44
    .line 45
    .line 46
    move-result-object v2

    .line 47
    check-cast v2, Ljava/lang/Integer;

    .line 48
    .line 49
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 50
    .line 51
    .line 52
    move-result v2

    .line 53
    invoke-direct {p0, v3, v4, v1, v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇000〇〇08(JLjava/util/ArrayList;I)V

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_1
    :try_start_0
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    sget-object v0, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 62
    .line 63
    invoke-virtual {p1, v0, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    .line 65
    .line 66
    goto :goto_2

    .line 67
    :catch_0
    move-exception p1

    .line 68
    goto :goto_1

    .line 69
    :catch_1
    move-exception p1

    .line 70
    :goto_1
    const-string v0, "PageListPresenter"

    .line 71
    .line 72
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 73
    .line 74
    .line 75
    :goto_2
    const/4 p1, 0x1

    .line 76
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0O8OO088:Z

    .line 77
    .line 78
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8O0880(Z)V

    .line 79
    .line 80
    .line 81
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private 〇8〇o88(Lcom/intsig/camscanner/web/UrlEntity;)Z
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/web/UrlEntity;->O8()Lcom/intsig/camscanner/web/FUNCTION;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    sget-object v0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$16;->〇080:[I

    .line 6
    .line 7
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    aget p1, v0, p1

    .line 12
    .line 13
    packed-switch p1, :pswitch_data_0

    .line 14
    .line 15
    .line 16
    const/4 p1, 0x0

    .line 17
    goto :goto_1

    .line 18
    :pswitch_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 19
    .line 20
    const-wide/16 v0, 0x1

    .line 21
    .line 22
    invoke-interface {p1, v0, v1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->o8O(J)V

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :pswitch_1
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 27
    .line 28
    const-wide/16 v0, 0x0

    .line 29
    .line 30
    invoke-interface {p1, v0, v1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->o8O(J)V

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :pswitch_2
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 35
    .line 36
    const-wide/16 v0, 0xb

    .line 37
    .line 38
    invoke-interface {p1, v0, v1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->o8O(J)V

    .line 39
    .line 40
    .line 41
    :goto_0
    const/4 p1, 0x1

    .line 42
    :goto_1
    return p1

    .line 43
    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private synthetic 〇8〇oO〇〇8o(Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel$UriData;)V
    .locals 4

    .line 1
    if-eqz p1, :cond_4

    .line 2
    .line 3
    iget-object p1, p1, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel$UriData;->〇080:Landroid/net/Uri;

    .line 4
    .line 5
    if-nez p1, :cond_0

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    new-instance v0, Ljava/lang/StringBuilder;

    .line 13
    .line 14
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 15
    .line 16
    .line 17
    const-string v1, "initDatabaseCallbackViewModel uriString: "

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    const-string v1, "PageListPresenter"

    .line 30
    .line 31
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    const/4 v0, 0x1

    .line 35
    new-array v1, v0, [Landroid/net/Uri;

    .line 36
    .line 37
    sget-object v2, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 38
    .line 39
    const/4 v3, 0x0

    .line 40
    aput-object v2, v1, v3

    .line 41
    .line 42
    invoke-direct {p0, p1, v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oOo〇(Ljava/lang/String;[Landroid/net/Uri;)Z

    .line 43
    .line 44
    .line 45
    move-result v1

    .line 46
    if-eqz v1, :cond_2

    .line 47
    .line 48
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 49
    .line 50
    invoke-interface {p1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->oO0〇〇o8〇()Z

    .line 51
    .line 52
    .line 53
    move-result p1

    .line 54
    if-eqz p1, :cond_1

    .line 55
    .line 56
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO00OOO:Z

    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇O:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 60
    .line 61
    if-eqz p1, :cond_4

    .line 62
    .line 63
    invoke-virtual {p1}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->〇o00〇〇Oo()V

    .line 64
    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_2
    new-array v1, v0, [Landroid/net/Uri;

    .line 68
    .line 69
    sget-object v2, Lcom/intsig/camscanner/provider/Documents$Image;->〇080:Landroid/net/Uri;

    .line 70
    .line 71
    aput-object v2, v1, v3

    .line 72
    .line 73
    invoke-direct {p0, p1, v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oOo〇(Ljava/lang/String;[Landroid/net/Uri;)Z

    .line 74
    .line 75
    .line 76
    move-result p1

    .line 77
    if-eqz p1, :cond_4

    .line 78
    .line 79
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 80
    .line 81
    invoke-interface {p1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->oO0〇〇o8〇()Z

    .line 82
    .line 83
    .line 84
    move-result p1

    .line 85
    if-eqz p1, :cond_3

    .line 86
    .line 87
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o0O0:Z

    .line 88
    .line 89
    goto :goto_0

    .line 90
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO8oO0o〇:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 91
    .line 92
    if-eqz p1, :cond_4

    .line 93
    .line 94
    invoke-virtual {p1}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->〇o00〇〇Oo()V

    .line 95
    .line 96
    .line 97
    :cond_4
    :goto_0
    return-void
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method static bridge synthetic 〇O(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o0O0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇O00(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Landroid/app/Activity;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8o08O8O(Landroid/app/Activity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic 〇O80〇oOo(Lcom/intsig/camscanner/web/UrlEntity;)Z
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/web/UrlEntity;->o〇0()Lcom/intsig/camscanner/web/MODULE;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    const-string p1, "PageListPresenter"

    .line 9
    .line 10
    const-string v0, "module is null"

    .line 11
    .line 12
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    return v1

    .line 16
    :cond_0
    sget-object v2, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$16;->〇o00〇〇Oo:[I

    .line 17
    .line 18
    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    aget v0, v2, v0

    .line 23
    .line 24
    const/4 v2, 0x1

    .line 25
    if-eq v0, v2, :cond_2

    .line 26
    .line 27
    const/4 v2, 0x2

    .line 28
    if-eq v0, v2, :cond_1

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8〇o88(Lcom/intsig/camscanner/web/UrlEntity;)Z

    .line 32
    .line 33
    .line 34
    move-result v1

    .line 35
    goto :goto_0

    .line 36
    :cond_2
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oo(Lcom/intsig/camscanner/web/UrlEntity;)Z

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    :goto_0
    return v1
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic 〇O888o0o(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Lcom/intsig/mvp/activity/BaseChangeActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇OO8ooO8〇(Lcom/intsig/mvp/activity/BaseChangeActivity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇O8o08O(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O88O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇O8oOo0()V
    .locals 15

    .line 1
    const-string v0, "reeditSinglePageInDoc START"

    .line 2
    .line 3
    const-string v1, "PageListPresenter"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->O0()Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    if-nez v0, :cond_0

    .line 15
    .line 16
    const-string v0, "reeditSinglePageInDoc:adapter NULL"

    .line 17
    .line 18
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    return-void

    .line 22
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->〇O888o0o()Ljava/util/List;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    if-nez v0, :cond_1

    .line 27
    .line 28
    const-string v0, "reeditSinglePageInDoc:list NULL"

    .line 29
    .line 30
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    return-void

    .line 34
    :cond_1
    const/4 v2, 0x0

    .line 35
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    check-cast v0, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;

    .line 40
    .line 41
    instance-of v2, v0, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 42
    .line 43
    if-nez v2, :cond_2

    .line 44
    .line 45
    new-instance v2, Ljava/lang/StringBuilder;

    .line 46
    .line 47
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 48
    .line 49
    .line 50
    const-string v3, "reeditSinglePageInDoc:pageTypeItem="

    .line 51
    .line 52
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    return-void

    .line 66
    :cond_2
    check-cast v0, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 67
    .line 68
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageImageItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/pagelist/model/PageItem;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    if-nez v0, :cond_3

    .line 73
    .line 74
    const-string v0, "reeditSinglePageInDoc:pageItem NULL"

    .line 75
    .line 76
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    return-void

    .line 80
    :cond_3
    new-instance v5, Lcom/intsig/camscanner/loadimage/PageImage;

    .line 81
    .line 82
    iget-wide v2, v0, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇080:J

    .line 83
    .line 84
    iget-object v4, v0, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇o00〇〇Oo:Ljava/lang/String;

    .line 85
    .line 86
    invoke-direct {v5, v2, v3, v4}, Lcom/intsig/camscanner/loadimage/PageImage;-><init>(JLjava/lang/String;)V

    .line 87
    .line 88
    .line 89
    iget-object v2, v0, Lcom/intsig/camscanner/pagelist/model/PageItem;->Oo08:Ljava/lang/String;

    .line 90
    .line 91
    invoke-virtual {v5, v2}, Lcom/intsig/camscanner/loadimage/PageImage;->〇8〇0〇o〇O(Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    sget-object v2, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 95
    .line 96
    iget-wide v3, v0, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇080:J

    .line 97
    .line 98
    invoke-static {v2, v3, v4}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇oOO8O8(Landroid/content/Context;J)I

    .line 99
    .line 100
    .line 101
    move-result v2

    .line 102
    invoke-virtual {v5, v2}, Lcom/intsig/camscanner/loadimage/PageImage;->〇00〇8(I)V

    .line 103
    .line 104
    .line 105
    invoke-virtual {v5}, Lcom/intsig/camscanner/loadimage/PageImage;->〇oOO8O8()Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object v2

    .line 109
    invoke-static {v2}, Lcom/intsig/utils/FileUtil;->〇00〇8(Ljava/lang/String;)Z

    .line 110
    .line 111
    .line 112
    move-result v2

    .line 113
    new-instance v3, Ljava/lang/StringBuilder;

    .line 114
    .line 115
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 116
    .line 117
    .line 118
    const-string v4, "reedit isRaw exist "

    .line 119
    .line 120
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    .line 122
    .line 123
    iget-object v0, v0, Lcom/intsig/camscanner/pagelist/model/PageItem;->Oo08:Ljava/lang/String;

    .line 124
    .line 125
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    .line 127
    .line 128
    const-string v0, " = "

    .line 129
    .line 130
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    .line 132
    .line 133
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 134
    .line 135
    .line 136
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 137
    .line 138
    .line 139
    move-result-object v0

    .line 140
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    .line 142
    .line 143
    if-eqz v2, :cond_4

    .line 144
    .line 145
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o0()Landroidx/fragment/app/FragmentActivity;

    .line 146
    .line 147
    .line 148
    move-result-object v2

    .line 149
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 150
    .line 151
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getFragment()Landroidx/fragment/app/Fragment;

    .line 152
    .line 153
    .line 154
    move-result-object v3

    .line 155
    const/16 v4, 0x402

    .line 156
    .line 157
    invoke-virtual {v5}, Lcom/intsig/camscanner/loadimage/PageImage;->〇oOO8O8()Ljava/lang/String;

    .line 158
    .line 159
    .line 160
    move-result-object v6

    .line 161
    invoke-virtual {v5}, Lcom/intsig/camscanner/loadimage/PageImage;->o800o8O()J

    .line 162
    .line 163
    .line 164
    move-result-wide v7

    .line 165
    invoke-virtual {v5}, Lcom/intsig/camscanner/loadimage/PageImage;->〇O〇()Ljava/lang/String;

    .line 166
    .line 167
    .line 168
    move-result-object v9

    .line 169
    sget-object v0, Lcom/intsig/camscanner/paper/PaperUtil;->〇080:Lcom/intsig/camscanner/paper/PaperUtil;

    .line 170
    .line 171
    invoke-virtual {v0}, Lcom/intsig/camscanner/paper/PaperUtil;->OO0o〇〇〇〇0()Z

    .line 172
    .line 173
    .line 174
    move-result v10

    .line 175
    const/4 v11, 0x0

    .line 176
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO〇()Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 177
    .line 178
    .line 179
    move-result-object v12

    .line 180
    iget-wide v13, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 181
    .line 182
    invoke-static/range {v2 .. v14}, Lcom/intsig/camscanner/pagedetail/PageDetailReeditUtil;->o〇0(Landroidx/fragment/app/FragmentActivity;Landroidx/fragment/app/Fragment;ILcom/intsig/camscanner/loadimage/PageImage;Ljava/lang/String;JLjava/lang/String;ZLjava/util/HashSet;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;J)V

    .line 183
    .line 184
    .line 185
    goto :goto_0

    .line 186
    :cond_4
    const-string v0, "reeditSinglePageInDoc: NO RAW PAGE"

    .line 187
    .line 188
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    .line 190
    .line 191
    :goto_0
    return-void
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private 〇O8〇OO〇()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 4
    .line 5
    invoke-interface {v1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getFragment()Landroidx/fragment/app/Fragment;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    const-string v2, "page_list_load_page"

    .line 10
    .line 11
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;-><init>(Landroidx/lifecycle/LifecycleOwner;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO8oO0o〇:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 15
    .line 16
    const-wide/16 v1, 0x7d0

    .line 17
    .line 18
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->〇80〇808〇O(J)V

    .line 19
    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO8oO0o〇:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 22
    .line 23
    new-instance v1, L〇OoO0o0/o〇8;

    .line 24
    .line 25
    invoke-direct {v1, p0}, L〇OoO0o0/o〇8;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->OO0o〇〇(Ljava/lang/Runnable;)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic 〇OO0(I)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇()Z

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 8
    .line 9
    invoke-interface {p1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->O00O()V

    .line 10
    .line 11
    .line 12
    :cond_0
    const/4 p1, 0x0

    .line 13
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oO〇08o(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇OO8ooO8〇(Lcom/intsig/mvp/activity/BaseChangeActivity;)V
    .locals 2

    .line 1
    new-instance v0, L〇OoO0o0/OOO〇O0;

    .line 2
    .line 3
    invoke-direct {v0, p0}, L〇OoO0o0/OOO〇O0;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 4
    .line 5
    .line 6
    new-instance v1, L〇OoO0o0/oo〇;

    .line 7
    .line 8
    invoke-direct {v1, p0}, L〇OoO0o0/oo〇;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 9
    .line 10
    .line 11
    invoke-static {p1, v0, v1}, Lcom/intsig/camscanner/share/ShareSuccessDialog;->O0〇0(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/share/ShareSuccessDialog$ShareContinue;Lcom/intsig/camscanner/share/ShareSuccessDialog$ShareDismiss;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇OOo8〇0(Lcom/intsig/camscanner/datastruct/ParcelDocInfo;I)V
    .locals 7

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mode_ocr/OcrActivityUtil;->〇080:Lcom/intsig/camscanner/mode_ocr/OcrActivityUtil;

    .line 2
    .line 3
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 4
    .line 5
    invoke-interface {p2}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    new-instance v2, Ljava/util/ArrayList;

    .line 10
    .line 11
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇080:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 12
    .line 13
    invoke-virtual {p2}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->o〇0()Ljava/util/List;

    .line 14
    .line 15
    .line 16
    move-result-object p2

    .line 17
    invoke-direct {v2, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 18
    .line 19
    .line 20
    sget-object v4, Lcom/intsig/camscanner/mode_ocr/PageFromType;->FROM_PAGE_LIST_VIEW_OCR:Lcom/intsig/camscanner/mode_ocr/PageFromType;

    .line 21
    .line 22
    const/4 v5, -0x1

    .line 23
    const/4 v6, 0x0

    .line 24
    move-object v3, p1

    .line 25
    invoke-virtual/range {v0 .. v6}, Lcom/intsig/camscanner/mode_ocr/OcrActivityUtil;->〇o00〇〇Oo(Landroid/content/Context;Ljava/util/ArrayList;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Lcom/intsig/camscanner/mode_ocr/PageFromType;IZ)Landroid/content/Intent;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 30
    .line 31
    invoke-interface {p2}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getFragment()Landroidx/fragment/app/Fragment;

    .line 32
    .line 33
    .line 34
    move-result-object p2

    .line 35
    invoke-virtual {p2, p1}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private synthetic 〇OO〇00〇0O(Lcom/intsig/mvp/activity/BaseChangeActivity;)V
    .locals 1

    .line 1
    new-instance v0, L〇OoO0o0/Oo8Oo00oo;

    .line 2
    .line 3
    invoke-direct {v0, p0}, L〇OoO0o0/Oo8Oo00oo;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇Oo(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 2
    .line 3
    invoke-interface {p1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getFragment()Landroidx/fragment/app/Fragment;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    const/4 p2, 0x0

    .line 8
    const/16 v0, 0x3f2

    .line 9
    .line 10
    invoke-static {p1, p2, v0}, Lcom/intsig/camscanner/ocrapi/OcrIntent;->o〇0(Landroidx/fragment/app/Fragment;II)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇O〇(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oo0〇Ooo()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇O〇80o08O(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oOo0(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic 〇O〇〇O8()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->o88O8()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇o(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Lcom/intsig/mvp/activity/BaseChangeActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇08〇o0O(Lcom/intsig/mvp/activity/BaseChangeActivity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o08o〇0(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic 〇o0O(Ljava/util/List;Landroid/app/Activity;Ljava/util/List;)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oo〇o()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_5

    .line 7
    .line 8
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-lez v0, :cond_5

    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0o〇()Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-eqz v0, :cond_4

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine;

    .line 21
    .line 22
    if-nez v0, :cond_0

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine;

    .line 25
    .line 26
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8O〇:Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;

    .line 27
    .line 28
    invoke-direct {v0, p2, v2}, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;)V

    .line 29
    .line 30
    .line 31
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine;

    .line 32
    .line 33
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 34
    .line 35
    .line 36
    move-result p1

    .line 37
    invoke-direct {p0, p2, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇o08〇(Landroid/app/Activity;I)V

    .line 38
    .line 39
    .line 40
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine;

    .line 41
    .line 42
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o88〇OO08〇:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;

    .line 43
    .line 44
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine;->〇O8o08O(Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;)V

    .line 45
    .line 46
    .line 47
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine;

    .line 48
    .line 49
    invoke-virtual {p1}, Lcom/intsig/camscanner/business/operation/OperateEngine;->〇o00〇〇Oo()Lcom/intsig/camscanner/business/operation/OperateContent;

    .line 50
    .line 51
    .line 52
    move-result-object p1

    .line 53
    if-eqz p1, :cond_4

    .line 54
    .line 55
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8O〇:Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 56
    .line 57
    if-eqz p2, :cond_1

    .line 58
    .line 59
    invoke-virtual {p2}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;->Oooo8o0〇()Landroidx/lifecycle/MutableLiveData;

    .line 60
    .line 61
    .line 62
    move-result-object p2

    .line 63
    invoke-virtual {p2}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 64
    .line 65
    .line 66
    move-result-object p2

    .line 67
    check-cast p2, Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;

    .line 68
    .line 69
    if-eqz p2, :cond_1

    .line 70
    .line 71
    invoke-virtual {p2}, Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;->oO80()Z

    .line 72
    .line 73
    .line 74
    move-result v0

    .line 75
    invoke-virtual {p2}, Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;->〇80〇808〇O()Z

    .line 76
    .line 77
    .line 78
    move-result p2

    .line 79
    goto :goto_0

    .line 80
    :cond_1
    const/4 p2, 0x0

    .line 81
    const/4 v0, 0x0

    .line 82
    :goto_0
    if-nez v0, :cond_3

    .line 83
    .line 84
    if-eqz p2, :cond_2

    .line 85
    .line 86
    goto :goto_1

    .line 87
    :cond_2
    new-instance p1, Lcom/intsig/camscanner/pagelist/model/PageOperationItem;

    .line 88
    .line 89
    invoke-direct {p1}, Lcom/intsig/camscanner/pagelist/model/PageOperationItem;-><init>()V

    .line 90
    .line 91
    .line 92
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o88〇OO08〇:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;

    .line 93
    .line 94
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/pagelist/model/PageOperationItem;->〇o〇(Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;)V

    .line 95
    .line 96
    .line 97
    new-instance p1, Lcom/intsig/camscanner/pagelist/model/PageOperationItem;

    .line 98
    .line 99
    invoke-direct {p1}, Lcom/intsig/camscanner/pagelist/model/PageOperationItem;-><init>()V

    .line 100
    .line 101
    .line 102
    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    .line 104
    .line 105
    goto :goto_2

    .line 106
    :cond_3
    :goto_1
    instance-of p1, p1, Lcom/intsig/camscanner/business/operation/document_page/ODWord;

    .line 107
    .line 108
    if-eqz p1, :cond_4

    .line 109
    .line 110
    new-instance p1, Lcom/intsig/camscanner/pagelist/model/PageOperationItem;

    .line 111
    .line 112
    invoke-direct {p1}, Lcom/intsig/camscanner/pagelist/model/PageOperationItem;-><init>()V

    .line 113
    .line 114
    .line 115
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o88〇OO08〇:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;

    .line 116
    .line 117
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/pagelist/model/PageOperationItem;->〇o〇(Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;)V

    .line 118
    .line 119
    .line 120
    new-instance p1, Lcom/intsig/camscanner/pagelist/model/PageOperationItem;

    .line 121
    .line 122
    invoke-direct {p1}, Lcom/intsig/camscanner/pagelist/model/PageOperationItem;-><init>()V

    .line 123
    .line 124
    .line 125
    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    .line 127
    .line 128
    :cond_4
    :goto_2
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oo8Oo00oo:Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;

    .line 129
    .line 130
    if-eqz p1, :cond_5

    .line 131
    .line 132
    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    .line 134
    .line 135
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 136
    .line 137
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇080:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 138
    .line 139
    invoke-interface {p1, p3, p2}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->〇〇〇(Ljava/util/List;Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;)V

    .line 140
    .line 141
    .line 142
    iget p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0〇O0088o:I

    .line 143
    .line 144
    if-lez p1, :cond_6

    .line 145
    .line 146
    iget-boolean p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8oO〇:Z

    .line 147
    .line 148
    if-eqz p1, :cond_6

    .line 149
    .line 150
    iput-boolean v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8oO〇:Z

    .line 151
    .line 152
    goto :goto_3

    .line 153
    :cond_6
    iget p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8ooOoo〇:I

    .line 154
    .line 155
    if-lez p1, :cond_7

    .line 156
    .line 157
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 158
    .line 159
    iget p2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8ooOoo〇:I

    .line 160
    .line 161
    invoke-interface {p1, p2}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->O8o〇O0(I)V

    .line 162
    .line 163
    .line 164
    const/4 p1, -0x1

    .line 165
    iput p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8ooOoo〇:I

    .line 166
    .line 167
    :cond_7
    :goto_3
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 168
    .line 169
    invoke-interface {p1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->〇8o()V

    .line 170
    .line 171
    .line 172
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇()Z

    .line 173
    .line 174
    .line 175
    move-result p1

    .line 176
    if-eqz p1, :cond_8

    .line 177
    .line 178
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇080:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 179
    .line 180
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇80〇808〇O()I

    .line 181
    .line 182
    .line 183
    move-result p1

    .line 184
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 185
    .line 186
    invoke-interface {p2, p1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->〇0OO8(I)V

    .line 187
    .line 188
    .line 189
    :cond_8
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 190
    .line 191
    invoke-interface {p1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->O00()V

    .line 192
    .line 193
    .line 194
    return-void
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method static bridge synthetic 〇o0O0O8(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇080:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇oO88o()Z
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o88o0O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_3

    .line 7
    .line 8
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 9
    .line 10
    iget-wide v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 11
    .line 12
    invoke-static {v0, v2, v3}, Lcom/intsig/camscanner/db/dao/DocumentDao;->oO00OOO(Landroid/content/Context;J)Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    goto :goto_1

    .line 19
    :cond_0
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 20
    .line 21
    iget-wide v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 22
    .line 23
    invoke-static {v0, v2, v3, v1}, Lcom/intsig/camscanner/db/dao/MTagDao;->〇080(Landroid/content/Context;JZ)Ljava/util/ArrayList;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    if-eqz v2, :cond_3

    .line 36
    .line 37
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    check-cast v2, Ljava/lang/Long;

    .line 42
    .line 43
    if-eqz v2, :cond_1

    .line 44
    .line 45
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    .line 46
    .line 47
    .line 48
    move-result-wide v2

    .line 49
    sget-object v4, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 50
    .line 51
    invoke-static {v4, v2, v3}, Lcom/intsig/camscanner/db/dao/TagDao;->O8(Landroid/content/Context;J)Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v2

    .line 55
    invoke-static {v2}, Lcom/intsig/camscanner/sharedir/recommed/ShareDirRecommendHelper;->〇o00〇〇Oo(Ljava/lang/String;)Lcom/intsig/camscanner/sharedir/recommed/RecommendSceneEntity;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    if-eqz v2, :cond_1

    .line 60
    .line 61
    invoke-virtual {v2}, Lcom/intsig/camscanner/sharedir/recommed/RecommendSceneEntity;->getTitle()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v3

    .line 65
    if-eqz v3, :cond_2

    .line 66
    .line 67
    invoke-virtual {v2}, Lcom/intsig/camscanner/sharedir/recommed/RecommendSceneEntity;->getTitle()Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object v3

    .line 71
    goto :goto_0

    .line 72
    :cond_2
    const-string v3, ""

    .line 73
    .line 74
    :goto_0
    invoke-static {v3}, Lcom/intsig/camscanner/sharedir/recommed/ShareDirRecommendPreferenceHelper;->〇080(Ljava/lang/String;)Z

    .line 75
    .line 76
    .line 77
    move-result v4

    .line 78
    if-nez v4, :cond_1

    .line 79
    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    .line 81
    .line 82
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 83
    .line 84
    .line 85
    const-string v1, "tryShareDirRecommendFromScene: get data="

    .line 86
    .line 87
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    .line 89
    .line 90
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    const-string v1, "PageListPresenter"

    .line 98
    .line 99
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    .line 101
    .line 102
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O000:Landroidx/lifecycle/MutableLiveData;

    .line 103
    .line 104
    invoke-virtual {v0, v2}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 105
    .line 106
    .line 107
    const/4 v0, 0x1

    .line 108
    invoke-static {v3, v0}, Lcom/intsig/camscanner/sharedir/recommed/ShareDirRecommendPreferenceHelper;->〇o〇(Ljava/lang/String;Z)V

    .line 109
    .line 110
    .line 111
    return v0

    .line 112
    :cond_3
    :goto_1
    return v1
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic 〇oOO8O8(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇Oo(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic 〇oOoo〇(Ljava/util/ArrayList;Landroid/app/Activity;I)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 2
    .line 3
    .line 4
    move-result p3

    .line 5
    new-array v0, p3, [J

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    :goto_0
    if-ge v1, p3, :cond_0

    .line 9
    .line 10
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 11
    .line 12
    .line 13
    move-result-object v2

    .line 14
    check-cast v2, Ljava/lang/Long;

    .line 15
    .line 16
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    .line 17
    .line 18
    .line 19
    move-result-wide v2

    .line 20
    aput-wide v2, v0, v1

    .line 21
    .line 22
    add-int/lit8 v1, v1, 0x1

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    new-instance p1, Landroid/content/Intent;

    .line 26
    .line 27
    const-class p3, Lcom/intsig/camscanner/MovePageActivity;

    .line 28
    .line 29
    invoke-direct {p1, p2, p3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 30
    .line 31
    .line 32
    const-string p2, "multi_image_id"

    .line 33
    .line 34
    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 35
    .line 36
    .line 37
    const-string p2, "dirSyncId"

    .line 38
    .line 39
    iget-object p3, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO0o〇〇:Ljava/lang/String;

    .line 40
    .line 41
    invoke-virtual {p1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 42
    .line 43
    .line 44
    const-string p2, "EXTRA_CUT_DOC_ID"

    .line 45
    .line 46
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 47
    .line 48
    .line 49
    move-result-wide v0

    .line 50
    invoke-virtual {p1, p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 51
    .line 52
    .line 53
    const-string p2, "action"

    .line 54
    .line 55
    const/4 p3, 0x2

    .line 56
    invoke-virtual {p1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 57
    .line 58
    .line 59
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 60
    .line 61
    invoke-interface {p2}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getFragment()Landroidx/fragment/app/Fragment;

    .line 62
    .line 63
    .line 64
    move-result-object p2

    .line 65
    invoke-virtual {p2}, Landroidx/fragment/app/Fragment;->isAdded()Z

    .line 66
    .line 67
    .line 68
    move-result p2

    .line 69
    if-eqz p2, :cond_1

    .line 70
    .line 71
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 72
    .line 73
    invoke-interface {p2}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getFragment()Landroidx/fragment/app/Fragment;

    .line 74
    .line 75
    .line 76
    move-result-object p2

    .line 77
    const/16 p3, 0x3ed

    .line 78
    .line 79
    invoke-virtual {p2, p1, p3}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 80
    .line 81
    .line 82
    goto :goto_1

    .line 83
    :cond_1
    const-string p1, "PageListPresenter"

    .line 84
    .line 85
    const-string p2, "activity not Attach when go2MultiCopy"

    .line 86
    .line 87
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    :goto_1
    return-void
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private varargs 〇oOo〇(Ljava/lang/String;[Landroid/net/Uri;)Z
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-nez v0, :cond_2

    .line 7
    .line 8
    if-eqz p2, :cond_2

    .line 9
    .line 10
    array-length v0, p2

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    goto :goto_1

    .line 14
    :cond_0
    array-length v0, p2

    .line 15
    const/4 v2, 0x0

    .line 16
    :goto_0
    if-ge v2, v0, :cond_2

    .line 17
    .line 18
    aget-object v3, p2, v2

    .line 19
    .line 20
    if-eqz v3, :cond_1

    .line 21
    .line 22
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v3

    .line 26
    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    .line 27
    .line 28
    .line 29
    move-result v3

    .line 30
    if-eqz v3, :cond_1

    .line 31
    .line 32
    const/4 p1, 0x1

    .line 33
    return p1

    .line 34
    :cond_1
    add-int/lit8 v2, v2, 0x1

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_2
    :goto_1
    return v1
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic 〇oo〇(Landroid/app/Activity;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO(Landroid/app/Activity;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private 〇oo〇O〇80(Z)V
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇OO()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_1

    .line 13
    .line 14
    return-void

    .line 15
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/sharedir/data/ShareDirDbUtil;->〇080:Lcom/intsig/camscanner/sharedir/data/ShareDirDbUtil;

    .line 16
    .line 17
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 18
    .line 19
    .line 20
    move-result-wide v1

    .line 21
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/sharedir/data/ShareDirDbUtil;->〇o00〇〇Oo(J)I

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 25
    .line 26
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    check-cast v0, Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 31
    .line 32
    new-instance v1, Ljava/util/ArrayList;

    .line 33
    .line 34
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 38
    .line 39
    .line 40
    move-result-wide v2

    .line 41
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 42
    .line 43
    .line 44
    move-result-object v2

    .line 45
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 46
    .line 47
    .line 48
    new-instance v2, Ljava/lang/StringBuilder;

    .line 49
    .line 50
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 51
    .line 52
    .line 53
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o8O〇O()I

    .line 54
    .line 55
    .line 56
    move-result v3

    .line 57
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    const-string v3, ""

    .line 61
    .line 62
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object v2

    .line 69
    const-string v3, "CSList"

    .line 70
    .line 71
    const-string v4, "share"

    .line 72
    .line 73
    const-string v5, "total_page_num"

    .line 74
    .line 75
    invoke-static {v3, v4, v5, v2}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    if-nez p1, :cond_2

    .line 79
    .line 80
    new-instance p1, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$11;

    .line 81
    .line 82
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$11;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 83
    .line 84
    .line 85
    new-instance v2, L〇OoO0o0/〇8o8o〇;

    .line 86
    .line 87
    invoke-direct {v2, p0, v0}, L〇OoO0o0/〇8o8o〇;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Lcom/intsig/mvp/activity/BaseChangeActivity;)V

    .line 88
    .line 89
    .line 90
    const/4 v3, 0x1

    .line 91
    invoke-static {v0, v1, v3, p1, v2}, Lcom/intsig/camscanner/share/ShareHelper;->Oo〇(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;ZLcom/intsig/camscanner/share/listener/SharePreviousInterceptor;Lcom/intsig/camscanner/share/listener/ShareBackListener;)V

    .line 92
    .line 93
    .line 94
    goto :goto_0

    .line 95
    :cond_2
    sget-object p1, Lcom/intsig/camscanner/share/ShareHelper$ShareType;->EMAIL_MYSELF:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 96
    .line 97
    new-instance v2, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$12;

    .line 98
    .line 99
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$12;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 100
    .line 101
    .line 102
    new-instance v3, L〇OoO0o0/OO0o〇〇;

    .line 103
    .line 104
    invoke-direct {v3, p0, v0}, L〇OoO0o0/OO0o〇〇;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Lcom/intsig/mvp/activity/BaseChangeActivity;)V

    .line 105
    .line 106
    .line 107
    invoke-static {v0, v1, p1, v2, v3}, Lcom/intsig/camscanner/share/ShareHelper;->OoOOo8(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Lcom/intsig/camscanner/share/ShareHelper$ShareType;Lcom/intsig/camscanner/share/listener/SharePreviousInterceptor;Lcom/intsig/camscanner/share/listener/ShareBackListener;)V

    .line 108
    .line 109
    .line 110
    :goto_0
    return-void
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic 〇o〇(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;Ljava/lang/String;Z[Ljava/lang/String;Z)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p5}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO0〇〇o8〇(Lcom/intsig/camscanner/capture/SupportCaptureModeOption;Ljava/lang/String;Z[Ljava/lang/String;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method private 〇o〇o()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-wide v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 8
    .line 9
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    const/16 v2, 0xfa0

    .line 14
    .line 15
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/db/dao/ImageDao;->Oo〇o(Landroid/content/Context;Ljava/lang/Long;I)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    return v0
    .line 20
    .line 21
.end method

.method private synthetic 〇〇08O(Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8O〇:Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;->〇80〇808〇O()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oo〇O:Ljava/lang/String;

    .line 8
    .line 9
    new-instance v0, L〇OoO0o0/o〇0OOo〇0;

    .line 10
    .line 11
    invoke-direct {v0, p0, p1}, L〇OoO0o0/o〇0OOo〇0;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;)V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇8〇(Ljava/lang/Runnable;)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇〇0O8ooO(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 2
    .line 3
    check-cast p1, Lcom/intsig/camscanner/pagelist/PageListFragment;

    .line 4
    .line 5
    const/16 p2, 0x3ea

    .line 6
    .line 7
    const/4 v0, 0x1

    .line 8
    invoke-static {p1, p2, v0}, Lcom/intsig/camscanner/app/IntentUtil;->O〇8O8〇008(Landroidx/fragment/app/Fragment;IZ)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇〇0o(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Ljava/util/ArrayList;Landroid/app/Activity;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0oO0(Ljava/util/ArrayList;Landroid/app/Activity;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private 〇〇0o8O〇〇()Lcom/intsig/advertisement/listener/OnAdPositionListener;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$9;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$9;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇〇0o〇o8(II)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->O0()Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->〇O888o0o()Ljava/util/List;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-ltz p1, :cond_2

    .line 16
    .line 17
    if-ltz p2, :cond_2

    .line 18
    .line 19
    if-ge p1, v1, :cond_2

    .line 20
    .line 21
    if-lt p2, v1, :cond_0

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    invoke-direct {p0, v0, p1, p2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0OO8(Ljava/util/List;II)Ljava/util/List;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 29
    .line 30
    .line 31
    move-result p2

    .line 32
    const/4 v0, 0x1

    .line 33
    if-ge p2, v0, :cond_1

    .line 34
    .line 35
    return-void

    .line 36
    :cond_1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OooO〇(Ljava/util/List;)V

    .line 37
    .line 38
    .line 39
    :cond_2
    :goto_0
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private synthetic 〇〇8(Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    .line 2
    .line 3
    .line 4
    move-result p2

    .line 5
    invoke-static {p2}, Lcom/intsig/camscanner/scenariodir/util/CertificateUtil;->〇8o8o〇(I)I

    .line 6
    .line 7
    .line 8
    move-result p2

    .line 9
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0〇(Ljava/lang/Long;I)V

    .line 10
    .line 11
    .line 12
    const-string p1, "PageListPresenter"

    .line 13
    .line 14
    const-string p2, "checkRecommendScenarioDir recognized but not has type"

    .line 15
    .line 16
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    const/4 p1, 0x0

    .line 20
    return-object p1
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇〇808〇(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O〇〇O8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇〇888(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Landroid/app/Activity;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇0〇(Landroid/app/Activity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇〇8O0〇8(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇〇O80o8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇〇o0o()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-wide v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 8
    .line 9
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    const/16 v2, 0xfa1

    .line 14
    .line 15
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/db/dao/ImageDao;->Oo〇o(Landroid/content/Context;Ljava/lang/Long;I)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    return v0
    .line 20
    .line 21
.end method

.method private 〇〇o0〇8()V
    .locals 5

    .line 1
    const-string v0, "saveDocToGallery"

    .line 2
    .line 3
    const-string v1, "PageListPresenter"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 19
    .line 20
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-nez v0, :cond_0

    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 31
    .line 32
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 37
    .line 38
    .line 39
    move-result-wide v1

    .line 40
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0〇oo()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v3

    .line 44
    const/4 v4, 0x0

    .line 45
    invoke-static {v0, v1, v2, v3, v4}, Lcom/intsig/camscanner/control/ShareControl;->Oo〇O(Landroid/app/Activity;JLjava/lang/String;Z)V

    .line 46
    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_0
    const-string v0, "saveDocToGallery error occur"

    .line 50
    .line 51
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    :goto_0
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic 〇〇o8(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇〇o〇()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo〇O〇80(Z)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇〇〇()V
    .locals 3

    .line 1
    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 4
    .line 5
    invoke-interface {v1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getFragment()Landroidx/fragment/app/Fragment;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-static {}, Lcom/intsig/camscanner/multiimageedit/factory/NewInstanceFactoryImpl;->〇080()Landroidx/lifecycle/ViewModelProvider$NewInstanceFactory;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    invoke-direct {v0, v1, v2}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    .line 14
    .line 15
    .line 16
    const-class v1, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel;

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    check-cast v0, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel;

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel;->〇80〇808〇O()Landroidx/lifecycle/MutableLiveData;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 29
    .line 30
    invoke-interface {v1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getFragment()Landroidx/fragment/app/Fragment;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    new-instance v2, L〇OoO0o0/〇00;

    .line 39
    .line 40
    invoke-direct {v2, p0}, L〇OoO0o0/〇00;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, v1, v2}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 44
    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇〇〇0o〇〇0()V
    .locals 12

    .line 1
    const-string v0, "loadDocData: "

    .line 2
    .line 3
    const-string v1, "PageListPresenter"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    if-eqz v0, :cond_8

    .line 15
    .line 16
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    if-nez v2, :cond_8

    .line 21
    .line 22
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 23
    .line 24
    invoke-interface {v2}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getFragment()Landroidx/fragment/app/Fragment;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    invoke-static {v2}, Lcom/intsig/camscanner/mainmenu/CsLifecycleUtil;->〇080(Landroidx/lifecycle/LifecycleOwner;)Z

    .line 29
    .line 30
    .line 31
    move-result v2

    .line 32
    if-eqz v2, :cond_0

    .line 33
    .line 34
    goto/16 :goto_3

    .line 35
    .line 36
    :cond_0
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O00:Landroid/net/Uri;

    .line 37
    .line 38
    if-nez v2, :cond_1

    .line 39
    .line 40
    const-string v0, "mDocUri == null"

    .line 41
    .line 42
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    return-void

    .line 46
    :cond_1
    :try_start_0
    sget-object v3, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 47
    .line 48
    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 49
    .line 50
    .line 51
    move-result-wide v4

    .line 52
    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 53
    .line 54
    .line 55
    move-result-object v7
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 57
    .line 58
    .line 59
    move-result-object v2

    .line 60
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 61
    .line 62
    .line 63
    move-result-object v6

    .line 64
    sget-object v8, Lcom/intsig/camscanner/pagelist/model/PageListModel;->o0:[Ljava/lang/String;

    .line 65
    .line 66
    const/4 v9, 0x0

    .line 67
    const/4 v10, 0x0

    .line 68
    const/4 v11, 0x0

    .line 69
    invoke-virtual/range {v6 .. v11}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 70
    .line 71
    .line 72
    move-result-object v3

    .line 73
    if-eqz v3, :cond_7

    .line 74
    .line 75
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 76
    .line 77
    .line 78
    move-result v4

    .line 79
    const/4 v5, 0x1

    .line 80
    if-eqz v4, :cond_6

    .line 81
    .line 82
    invoke-interface {v3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 83
    .line 84
    .line 85
    move-result-object v4

    .line 86
    iput-object v4, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o800o8O:Ljava/lang/String;

    .line 87
    .line 88
    iget-boolean v4, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8:Z

    .line 89
    .line 90
    if-nez v4, :cond_3

    .line 91
    .line 92
    const/4 v4, 0x2

    .line 93
    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    .line 94
    .line 95
    .line 96
    move-result v4

    .line 97
    if-ne v4, v5, :cond_2

    .line 98
    .line 99
    goto :goto_0

    .line 100
    :cond_2
    const/4 v4, 0x0

    .line 101
    goto :goto_1

    .line 102
    :cond_3
    :goto_0
    const/4 v4, 0x1

    .line 103
    :goto_1
    iput-boolean v4, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8:Z

    .line 104
    .line 105
    const/4 v4, 0x4

    .line 106
    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object v4

    .line 110
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 111
    .line 112
    .line 113
    move-result v6

    .line 114
    if-nez v6, :cond_4

    .line 115
    .line 116
    iput-object v4, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O08000:Ljava/lang/String;

    .line 117
    .line 118
    :cond_4
    const/4 v4, 0x5

    .line 119
    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    .line 120
    .line 121
    .line 122
    move-result v4

    .line 123
    int-to-long v6, v4

    .line 124
    iput-wide v6, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8〇0〇o〇O:J

    .line 125
    .line 126
    const/4 v4, 0x3

    .line 127
    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    .line 128
    .line 129
    .line 130
    move-result v4

    .line 131
    const/16 v6, 0xa

    .line 132
    .line 133
    invoke-interface {v3, v6}, Landroid/database/Cursor;->getInt(I)I

    .line 134
    .line 135
    .line 136
    move-result v6

    .line 137
    iput v6, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇O8〇〇o:I

    .line 138
    .line 139
    const/16 v6, 0xd

    .line 140
    .line 141
    invoke-interface {v3, v6}, Landroid/database/Cursor;->getInt(I)I

    .line 142
    .line 143
    .line 144
    move-result v6

    .line 145
    if-ne v6, v5, :cond_5

    .line 146
    .line 147
    iget-boolean v6, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇O〇oO:Z

    .line 148
    .line 149
    if-nez v6, :cond_5

    .line 150
    .line 151
    iput-boolean v5, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇O〇oO:Z

    .line 152
    .line 153
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 154
    .line 155
    .line 156
    move-result-object v6

    .line 157
    new-instance v7, L〇OoO0o0/〇〇0o;

    .line 158
    .line 159
    invoke-direct {v7, p0, v0, v2}, L〇OoO0o0/〇〇0o;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Landroid/app/Activity;Landroid/content/Context;)V

    .line 160
    .line 161
    .line 162
    invoke-virtual {v6, v7}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 163
    .line 164
    .line 165
    :cond_5
    const/16 v0, 0xb

    .line 166
    .line 167
    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 168
    .line 169
    .line 170
    move-result-object v0

    .line 171
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇00:Ljava/lang/String;

    .line 172
    .line 173
    const/16 v0, 0xc

    .line 174
    .line 175
    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    .line 176
    .line 177
    .line 178
    move-result v0

    .line 179
    iput v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇8O8〇008:I

    .line 180
    .line 181
    const/16 v0, 0x10

    .line 182
    .line 183
    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    .line 184
    .line 185
    .line 186
    move-result v0

    .line 187
    new-instance v2, L〇OoO0o0/〇08O8o〇0;

    .line 188
    .line 189
    invoke-direct {v2, p0, v4, v0}, L〇OoO0o0/〇08O8o〇0;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;II)V

    .line 190
    .line 191
    .line 192
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇8〇(Ljava/lang/Runnable;)V

    .line 193
    .line 194
    .line 195
    new-instance v0, Ljava/lang/StringBuilder;

    .line 196
    .line 197
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 198
    .line 199
    .line 200
    const-string v2, "onLoadFinished mNeedCreatePdf="

    .line 201
    .line 202
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    .line 204
    .line 205
    iget-boolean v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8:Z

    .line 206
    .line 207
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 208
    .line 209
    .line 210
    const-string v2, ", mTitle="

    .line 211
    .line 212
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    .line 214
    .line 215
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o800o8O:Ljava/lang/String;

    .line 216
    .line 217
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    .line 219
    .line 220
    const-string v2, ",mPdfFile="

    .line 221
    .line 222
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    .line 224
    .line 225
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O08000:Ljava/lang/String;

    .line 226
    .line 227
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 228
    .line 229
    .line 230
    const-string v2, " mBelongState="

    .line 231
    .line 232
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 233
    .line 234
    .line 235
    iget v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇O8〇〇o:I

    .line 236
    .line 237
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 238
    .line 239
    .line 240
    const-string v2, ", mCollaborateToken="

    .line 241
    .line 242
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    .line 244
    .line 245
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇00:Ljava/lang/String;

    .line 246
    .line 247
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    .line 249
    .line 250
    const-string v2, ",mPageNum="

    .line 251
    .line 252
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    .line 254
    .line 255
    iget v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0〇O0088o:I

    .line 256
    .line 257
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 258
    .line 259
    .line 260
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 261
    .line 262
    .line 263
    move-result-object v0

    .line 264
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    .line 266
    .line 267
    goto :goto_2

    .line 268
    :cond_6
    const-string v0, "DocInfoLoader onLoadFinished() doc may be deleted"

    .line 269
    .line 270
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    .line 272
    .line 273
    new-instance v0, L〇OoO0o0/oO;

    .line 274
    .line 275
    invoke-direct {v0, p0}, L〇OoO0o0/oO;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 276
    .line 277
    .line 278
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇8〇(Ljava/lang/Runnable;)V

    .line 279
    .line 280
    .line 281
    :goto_2
    iput-boolean v5, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇8oOO88:Z

    .line 282
    .line 283
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 284
    .line 285
    .line 286
    :cond_7
    return-void

    .line 287
    :catch_0
    move-exception v0

    .line 288
    new-instance v2, Ljava/lang/StringBuilder;

    .line 289
    .line 290
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 291
    .line 292
    .line 293
    const-string v3, "mDocUri:"

    .line 294
    .line 295
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 296
    .line 297
    .line 298
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O00:Landroid/net/Uri;

    .line 299
    .line 300
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 301
    .line 302
    .line 303
    const-string v3, " "

    .line 304
    .line 305
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 306
    .line 307
    .line 308
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 309
    .line 310
    .line 311
    move-result-object v2

    .line 312
    invoke-static {v1, v2, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 313
    .line 314
    .line 315
    return-void

    .line 316
    :cond_8
    :goto_3
    const-string v0, "activity == null || activity.isFinishing()"

    .line 317
    .line 318
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    .line 320
    .line 321
    return-void
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public static synthetic 〇〇〇0〇〇0(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public O00()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o8O〇O()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const-string v1, "PageListPresenter"

    .line 6
    .line 7
    if-lez v0, :cond_0

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇80〇808〇O()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o8O〇O()I

    .line 18
    .line 19
    .line 20
    move-result v2

    .line 21
    if-ge v0, v2, :cond_0

    .line 22
    .line 23
    const-string v0, "doPageDeleteConfirm multi page"

    .line 24
    .line 25
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 29
    .line 30
    const/16 v1, 0x458

    .line 31
    .line 32
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->showDialog(I)V

    .line 33
    .line 34
    .line 35
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    new-instance v1, L〇OoO0o0/O〇8O8〇008;

    .line 40
    .line 41
    invoke-direct {v1, p0}, L〇OoO0o0/O〇8O8〇008;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 45
    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_0
    const-string v0, "doPageDeleteConfirm delete whole doc"

    .line 49
    .line 50
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 54
    .line 55
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 60
    .line 61
    .line 62
    move-result-wide v1

    .line 63
    const/4 v3, 0x2

    .line 64
    const/4 v4, 0x1

    .line 65
    invoke-static {v0, v1, v2, v3, v4}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0oO0(Landroid/content/Context;JIZ)V

    .line 66
    .line 67
    .line 68
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO0O()V

    .line 69
    .line 70
    .line 71
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 72
    .line 73
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 78
    .line 79
    .line 80
    :goto_0
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public O08O0〇O()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o〇:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O08〇(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oo〇:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public O0O0〇(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0〇O0088o:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public O0O〇OOo()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8O〇:Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return v0

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;->Oooo8o0〇()Landroidx/lifecycle/MutableLiveData;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;

    .line 16
    .line 17
    invoke-static {v0}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker;->〇〇888(Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;)Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    return v0
.end method

.method public O0o8〇O()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇080:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇O00()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O0oo0o0〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oOO8O8:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O0〇0(Landroid/content/Intent;I)V
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 2
    .line 3
    invoke-static {p1, p2, v0, v1}, Lcom/intsig/camscanner/pagelist/DocCreateClient;->〇80〇808〇O(Landroid/content/Intent;IJ)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public O0〇OO8(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_3

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-gtz v0, :cond_0

    .line 8
    .line 9
    goto :goto_1

    .line 10
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o0()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    if-eqz v0, :cond_3

    .line 15
    .line 16
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    if-eqz v1, :cond_1

    .line 21
    .line 22
    goto :goto_1

    .line 23
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    new-array v2, v1, [J

    .line 28
    .line 29
    const/4 v3, 0x0

    .line 30
    :goto_0
    if-ge v3, v1, :cond_2

    .line 31
    .line 32
    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    move-result-object v4

    .line 36
    check-cast v4, Ljava/lang/Long;

    .line 37
    .line 38
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    .line 39
    .line 40
    .line 41
    move-result-wide v4

    .line 42
    aput-wide v4, v2, v3

    .line 43
    .line 44
    add-int/lit8 v3, v3, 0x1

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_2
    new-instance p1, Lcom/intsig/utils/CommonLoadingTask;

    .line 48
    .line 49
    new-instance v1, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$1;

    .line 50
    .line 51
    invoke-direct {v1, p0, v0, v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$1;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Landroid/app/Activity;[J)V

    .line 52
    .line 53
    .line 54
    const v2, 0x7f13024d

    .line 55
    .line 56
    .line 57
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object v2

    .line 61
    const/4 v3, 0x1

    .line 62
    invoke-direct {p1, v0, v1, v2, v3}, Lcom/intsig/utils/CommonLoadingTask;-><init>(Landroid/content/Context;Lcom/intsig/utils/CommonLoadingTask$TaskCallback;Ljava/lang/String;Z)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {p1}, Lcom/intsig/utils/CommonLoadingTask;->O8()V

    .line 66
    .line 67
    .line 68
    :cond_3
    :goto_1
    return-void
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public O0〇oo()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o800o8O:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O80〇O〇080(Lcom/intsig/callback/Callback0;)V
    .locals 3
    .param p1    # Lcom/intsig/callback/Callback0;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8O〇:Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-interface {p1}, Lcom/intsig/callback/Callback0;->call()V

    .line 6
    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;->Oooo8o0〇()Landroidx/lifecycle/MutableLiveData;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {v0}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    check-cast v0, Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8O〇:Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 20
    .line 21
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;->〇8o8o〇()Z

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    new-instance v2, L〇OoO0o0/〇O〇80o08O;

    .line 26
    .line 27
    invoke-direct {v2, p1}, L〇OoO0o0/〇O〇80o08O;-><init>(Lcom/intsig/callback/Callback0;)V

    .line 28
    .line 29
    .line 30
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker;->〇080(Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;ZLkotlin/jvm/functions/Function0;)V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public O880O〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO8oO0o〇:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->o〇0()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O880oOO08(Lcom/intsig/camscanner/capture/SupportCaptureModeOption;Ljava/lang/String;Z)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o0()Landroidx/fragment/app/FragmentActivity;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    new-instance v1, L〇OoO0o0/Ooo;

    .line 13
    .line 14
    invoke-direct {v1, p0, p1, p2, p3}, L〇OoO0o0/Ooo;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;Ljava/lang/String;Z)V

    .line 15
    .line 16
    .line 17
    invoke-static {v0, v1}, Lcom/intsig/util/PermissionUtil;->O8(Landroid/content/Context;Lcom/intsig/permission/PermissionCallback;)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public O8888()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇8o8o〇()Ljava/util/ArrayList;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    if-eqz v1, :cond_2

    .line 23
    .line 24
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    if-nez v2, :cond_1

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_1
    new-instance v2, L〇OoO0o0/〇〇808〇;

    .line 32
    .line 33
    invoke-direct {v2, p0, v1, v0}, L〇OoO0o0/〇〇808〇;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Ljava/util/ArrayList;Landroid/app/Activity;)V

    .line 34
    .line 35
    .line 36
    const/4 v3, 0x0

    .line 37
    invoke-static {v0, v1, v3, v2}, Lcom/intsig/camscanner/control/DataChecker;->〇O00(Landroid/app/Activity;Ljava/util/ArrayList;Ljava/lang/String;Lcom/intsig/camscanner/control/DataChecker$ActionListener;)Z

    .line 38
    .line 39
    .line 40
    goto :goto_1

    .line 41
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 42
    .line 43
    const v1, 0x7f130385

    .line 44
    .line 45
    .line 46
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->〇〇〇0880(I)V

    .line 47
    .line 48
    .line 49
    :goto_1
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public O88o〇()V
    .locals 5

    .line 1
    const-string v0, "PageListPresenter"

    .line 2
    .line 3
    const-string v1, "go2SaveToGallery"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇0080o(Z)V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 13
    .line 14
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 19
    .line 20
    .line 21
    move-result-wide v1

    .line 22
    new-instance v3, L〇OoO0o0/〇o〇;

    .line 23
    .line 24
    invoke-direct {v3, p0}, L〇OoO0o0/〇o〇;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 25
    .line 26
    .line 27
    new-instance v4, L〇OoO0o0/O8;

    .line 28
    .line 29
    invoke-direct {v4, p0, v0}, L〇OoO0o0/O8;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Landroid/app/Activity;)V

    .line 30
    .line 31
    .line 32
    invoke-static {v0, v1, v2, v3, v4}, Lcom/intsig/camscanner/control/DataChecker;->〇80〇808〇O(Landroid/app/Activity;JLcom/intsig/camscanner/control/DataChecker$ActionListener;Lcom/intsig/camscanner/app/DbWaitingListener;)Z

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public O88〇〇o0O()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    return-void

    .line 21
    :cond_1
    instance-of v0, v1, Landroidx/appcompat/app/AppCompatActivity;

    .line 22
    .line 23
    if-nez v0, :cond_2

    .line 24
    .line 25
    return-void

    .line 26
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 27
    .line 28
    .line 29
    move-result-wide v2

    .line 30
    const-string v4, "cs_list"

    .line 31
    .line 32
    const-string v5, "doc"

    .line 33
    .line 34
    new-instance v6, L〇OoO0o0/〇O8o08O;

    .line 35
    .line 36
    invoke-direct {v6, v1}, L〇OoO0o0/〇O8o08O;-><init>(Landroid/app/Activity;)V

    .line 37
    .line 38
    .line 39
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/printer/PrintUtil;->〇O〇(Landroid/app/Activity;JLjava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/printer/contract/PreparePrintDataCallback;)V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public O8OO08o()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo〇:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O8O〇88oO0(I)V
    .locals 3

    .line 1
    const/4 v0, 0x2

    .line 2
    const-string v1, "type"

    .line 3
    .line 4
    const-string v2, "CSMorePop"

    .line 5
    .line 6
    if-eq p1, v0, :cond_9

    .line 7
    .line 8
    const/16 v0, 0xb

    .line 9
    .line 10
    if-eq p1, v0, :cond_8

    .line 11
    .line 12
    const/16 v0, 0xd

    .line 13
    .line 14
    if-eq p1, v0, :cond_7

    .line 15
    .line 16
    const/16 v0, 0x10

    .line 17
    .line 18
    if-eq p1, v0, :cond_6

    .line 19
    .line 20
    const/16 v0, 0x12

    .line 21
    .line 22
    if-eq p1, v0, :cond_5

    .line 23
    .line 24
    const/16 v0, 0x15

    .line 25
    .line 26
    if-eq p1, v0, :cond_4

    .line 27
    .line 28
    const/16 v0, 0x18

    .line 29
    .line 30
    if-eq p1, v0, :cond_3

    .line 31
    .line 32
    const/4 v0, 0x6

    .line 33
    if-eq p1, v0, :cond_2

    .line 34
    .line 35
    const/4 v0, 0x7

    .line 36
    if-eq p1, v0, :cond_1

    .line 37
    .line 38
    const/16 v0, 0x8

    .line 39
    .line 40
    if-eq p1, v0, :cond_0

    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_0
    const-string p1, "rename"

    .line 44
    .line 45
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇Oo()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    invoke-static {v2, p1, v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    goto :goto_0

    .line 53
    :cond_1
    const-string p1, "label"

    .line 54
    .line 55
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇Oo()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    invoke-static {v2, p1, v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    goto :goto_0

    .line 63
    :cond_2
    const-string p1, "email_to_myself"

    .line 64
    .line 65
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇Oo()Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    invoke-static {v2, p1, v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    goto :goto_0

    .line 73
    :cond_3
    const-string p1, "smart_print"

    .line 74
    .line 75
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇Oo()Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v0

    .line 79
    invoke-static {v2, p1, v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    .line 81
    .line 82
    goto :goto_0

    .line 83
    :cond_4
    const-string p1, "save_to_gallery"

    .line 84
    .line 85
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇Oo()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v0

    .line 89
    invoke-static {v2, p1, v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    .line 91
    .line 92
    goto :goto_0

    .line 93
    :cond_5
    const-string p1, "transfer_word"

    .line 94
    .line 95
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇Oo()Ljava/lang/String;

    .line 96
    .line 97
    .line 98
    move-result-object v0

    .line 99
    invoke-static {v2, p1, v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    .line 101
    .line 102
    goto :goto_0

    .line 103
    :cond_6
    const-string p1, "document_security_water"

    .line 104
    .line 105
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇Oo()Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object v0

    .line 109
    invoke-static {v2, p1, v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    .line 111
    .line 112
    goto :goto_0

    .line 113
    :cond_7
    const-string p1, "choose"

    .line 114
    .line 115
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇Oo()Ljava/lang/String;

    .line 116
    .line 117
    .line 118
    move-result-object v0

    .line 119
    invoke-static {v2, p1, v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    .line 121
    .line 122
    goto :goto_0

    .line 123
    :cond_8
    const-string p1, "collage"

    .line 124
    .line 125
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇Oo()Ljava/lang/String;

    .line 126
    .line 127
    .line 128
    move-result-object v0

    .line 129
    invoke-static {v2, p1, v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    .line 131
    .line 132
    goto :goto_0

    .line 133
    :cond_9
    const-string p1, "import_gallery"

    .line 134
    .line 135
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇Oo()Ljava/lang/String;

    .line 136
    .line 137
    .line 138
    move-result-object v0

    .line 139
    invoke-static {v2, p1, v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    .line 141
    .line 142
    :goto_0
    return-void
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public O8O〇8oo08()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o8o〇:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O8〇8〇O80(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public O8〇o〇88()V
    .locals 1

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    throw v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OO88o()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇0o:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OO88〇OOO()Z
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/tsapp/sync/AppConfigJson;->isShowGptEntrance()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    return v0

    .line 13
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o88o0O()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OO8〇(Ljava/lang/String;Lcom/intsig/camscanner/sharedir/recommed/RecommendShareDirFromType;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/sharedir/recommed/RecommendShareDirFromType;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "inviteShareDir"

    .line 2
    .line 3
    const-string v1, "PageListPresenter"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o0()Landroidx/fragment/app/FragmentActivity;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    const-string p1, "inviteShareDir: activity is NULL"

    .line 15
    .line 16
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    return-void

    .line 20
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    .line 21
    .line 22
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 23
    .line 24
    .line 25
    iget-wide v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 26
    .line 27
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 32
    .line 33
    .line 34
    invoke-virtual {p2}, Lcom/intsig/camscanner/sharedir/recommed/RecommendShareDirFromType;->〇080()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object p2

    .line 38
    invoke-static {v0, p1, v1, p2}, Lcom/intsig/camscanner/mainmenu/docpage/menu/ShareDirInviteHelper;->〇O〇(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public OOo0O(Z)V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    const-string v1, "PageListPresenter"

    .line 15
    .line 16
    const-string v2, "User Operation: import photo"

    .line 17
    .line 18
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0O00oO()Z

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    invoke-static {v0, v1}, Lcom/intsig/camscanner/business/folders/OfflineFolder;->Oooo8o0〇(Landroid/content/Context;Z)Z

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    if-eqz v1, :cond_1

    .line 30
    .line 31
    return-void

    .line 32
    :cond_1
    invoke-static {v0}, Lcom/intsig/camscanner/util/SDStorageManager;->〇〇888(Landroid/app/Activity;)Z

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    if-eqz v0, :cond_2

    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 39
    .line 40
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getFragment()Landroidx/fragment/app/Fragment;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    const-string v1, "CSList"

    .line 45
    .line 46
    const-string v2, "cs_list"

    .line 47
    .line 48
    const/16 v3, 0x3ea

    .line 49
    .line 50
    invoke-static {v0, v3, p1, v1, v2}, Lcom/intsig/camscanner/app/IntentUtil;->O8ooOoo〇(Landroidx/fragment/app/Fragment;IZLjava/lang/String;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    :cond_2
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public OOo88OOo()V
    .locals 10

    .line 1
    new-instance v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 7
    .line 8
    .line 9
    move-result-wide v1

    .line 10
    iput-wide v1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇8o8o〇()Ljava/util/ArrayList;

    .line 17
    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 20
    .line 21
    invoke-interface {v1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    new-instance v4, Ljava/util/ArrayList;

    .line 26
    .line 27
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 28
    .line 29
    .line 30
    iget-wide v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 31
    .line 32
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    .line 38
    .line 39
    new-instance v1, Lcom/intsig/camscanner/control/DataChecker;

    .line 40
    .line 41
    const-wide/16 v5, -0x1

    .line 42
    .line 43
    const/4 v7, 0x0

    .line 44
    sget v8, Lcom/intsig/camscanner/control/DataChecker;->〇O8o08O:I

    .line 45
    .line 46
    new-instance v9, L〇OoO0o0/O08000;

    .line 47
    .line 48
    invoke-direct {v9, p0, v0}, L〇OoO0o0/O08000;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;)V

    .line 49
    .line 50
    .line 51
    move-object v2, v1

    .line 52
    invoke-direct/range {v2 .. v9}, Lcom/intsig/camscanner/control/DataChecker;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;JLjava/lang/String;ILcom/intsig/camscanner/control/DataChecker$ActionListener;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {v1}, Lcom/intsig/camscanner/control/DataChecker;->o〇0()Z

    .line 56
    .line 57
    .line 58
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public OOo8o〇O()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    invoke-virtual {p0, v0, v0, v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O880oOO08(Lcom/intsig/camscanner/capture/SupportCaptureModeOption;Ljava/lang/String;Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OOoo()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8〇o:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OO〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇080:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇〇808〇()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OO〇0008O8()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OO〇OOo()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 2
    .line 3
    if-eqz v0, :cond_2

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0000OOO:Lcom/intsig/camscanner/business/mode/eevidence/commonbiz/impl/EEvidenceProcessControl;

    .line 13
    .line 14
    if-nez v0, :cond_1

    .line 15
    .line 16
    new-instance v0, Lcom/intsig/camscanner/business/mode/eevidence/commonbiz/impl/EEvidenceProcessControl;

    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 19
    .line 20
    invoke-interface {v1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O00O()Lcom/intsig/camscanner/business/mode/eevidence/commonbiz/IEEvidenceProcessParamsGetter;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/business/mode/eevidence/commonbiz/impl/EEvidenceProcessControl;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/business/mode/eevidence/commonbiz/IEEvidenceProcessParamsGetter;)V

    .line 29
    .line 30
    .line 31
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0000OOO:Lcom/intsig/camscanner/business/mode/eevidence/commonbiz/impl/EEvidenceProcessControl;

    .line 32
    .line 33
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0000OOO:Lcom/intsig/camscanner/business/mode/eevidence/commonbiz/impl/EEvidenceProcessControl;

    .line 34
    .line 35
    invoke-virtual {v0}, Lcom/intsig/camscanner/business/mode/eevidence/commonbiz/impl/EEvidenceProcessControl;->o〇〇0〇()V

    .line 36
    .line 37
    .line 38
    :cond_2
    :goto_0
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public Oo(JLjava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇oo(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇O8OO(J)V

    .line 5
    .line 6
    .line 7
    sget-object p3, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 8
    .line 9
    invoke-static {p3, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oOoO8OO〇(Landroid/net/Uri;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O880O〇()V

    .line 17
    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇08oO80o()V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public Oo0O080()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8O〇:Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return v0

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;->Oooo8o0〇()Landroidx/lifecycle/MutableLiveData;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8O〇:Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;->〇8o8o〇()Z

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    invoke-static {v0, v1}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker;->o〇0(Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;Z)Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    return v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public Oo0O0o8()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/presenter/PageListDocPresenter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListDocPresenter;->〇080()Lcom/intsig/camscanner/pagelist/reader/DocReaderManager;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    const/4 v2, 0x1

    .line 9
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/pagelist/reader/DocReaderManager;->〇o〇(ZZ)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OoOOo8(Z)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    if-eqz p1, :cond_2

    .line 9
    .line 10
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oo08OO〇0()Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    const/4 v0, 0x1

    .line 19
    if-eqz p1, :cond_1

    .line 20
    .line 21
    new-instance p1, L〇OoO0o0/〇oo〇;

    .line 22
    .line 23
    invoke-direct {p1, p0}, L〇OoO0o0/〇oo〇;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 24
    .line 25
    .line 26
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 27
    .line 28
    invoke-interface {v1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    const/4 v2, 0x0

    .line 33
    invoke-static {v1, v0, p1, v2}, Lcom/intsig/camscanner/app/DialogUtils;->O000(Landroid/app/Activity;ZLcom/intsig/camscanner/app/DialogUtils$MailToMeCallback;Landroid/preference/Preference;)V

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_1
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo〇O〇80(Z)V

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_2
    const/4 p1, 0x0

    .line 42
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo〇O〇80(Z)V

    .line 43
    .line 44
    .line 45
    :goto_0
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public OoO〇()Lcom/intsig/camscanner/util/CSInternalResolver$CSInternalActionCallback;
    .locals 1

    .line 1
    new-instance v0, L〇OoO0o0/O0o〇〇Oo;

    .line 2
    .line 3
    invoke-direct {v0, p0}, L〇OoO0o0/O0o〇〇Oo;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Ooo8()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 15
    .line 16
    .line 17
    move-result-wide v1

    .line 18
    new-instance v3, L〇OoO0o0/〇0000OOO;

    .line 19
    .line 20
    invoke-direct {v3, p0}, L〇OoO0o0/〇0000OOO;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 21
    .line 22
    .line 23
    invoke-static {v0, v1, v2, v3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0o(Landroid/app/Activity;JLcom/intsig/camscanner/control/DataChecker$ActionListener;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public Ooo8o(Lcom/intsig/camscanner/pagelist/model/EditType;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO80:Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public Oo〇o()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇080:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇O00()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    return v0
    .line 19
    .line 20
    .line 21
.end method

.method public O〇(Lcom/intsig/callback/Callback0;)V
    .locals 2
    .param p1    # Lcom/intsig/callback/Callback0;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8O〇:Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-interface {p1}, Lcom/intsig/callback/Callback0;->call()V

    .line 6
    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;->Oooo8o0〇()Landroidx/lifecycle/MutableLiveData;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {v0}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    check-cast v0, Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;

    .line 18
    .line 19
    new-instance v1, L〇OoO0o0/ooo〇8oO;

    .line 20
    .line 21
    invoke-direct {v1, p1}, L〇OoO0o0/ooo〇8oO;-><init>(Lcom/intsig/callback/Callback0;)V

    .line 22
    .line 23
    .line 24
    invoke-static {v0, v1}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker;->〇o00〇〇Oo(Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;Lkotlin/jvm/functions/Function0;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public O〇0(I)I
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x2

    .line 8
    if-eqz v0, :cond_2

    .line 9
    .line 10
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    if-eqz v2, :cond_0

    .line 15
    .line 16
    goto/16 :goto_0

    .line 17
    .line 18
    :cond_0
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    new-instance v3, Ljava/lang/StringBuilder;

    .line 23
    .line 24
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 25
    .line 26
    .line 27
    const-string v4, "recyclerViewWidth="

    .line 28
    .line 29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    const-string v4, "screent width="

    .line 36
    .line 37
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v3

    .line 47
    const-string v4, "PageListPresenter"

    .line 48
    .line 49
    invoke-static {v4, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    invoke-static {p1, v2}, Ljava/lang/Math;->max(II)I

    .line 53
    .line 54
    .line 55
    move-result p1

    .line 56
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 57
    .line 58
    .line 59
    move-result-object v2

    .line 60
    const v3, 0x7f070183

    .line 61
    .line 62
    .line 63
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 64
    .line 65
    .line 66
    move-result v2

    .line 67
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 68
    .line 69
    .line 70
    move-result-object v5

    .line 71
    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 72
    .line 73
    .line 74
    move-result v3

    .line 75
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    .line 76
    .line 77
    .line 78
    move-result v5

    .line 79
    const/16 v6, 0x3e8

    .line 80
    .line 81
    if-le v5, v6, :cond_1

    .line 82
    .line 83
    new-instance v5, Ljava/lang/StringBuilder;

    .line 84
    .line 85
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 86
    .line 87
    .line 88
    const-string v6, "Error, margin is too wide, value = "

    .line 89
    .line 90
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object v3

    .line 100
    invoke-static {v4, v3}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    .line 102
    .line 103
    const/4 v3, 0x6

    .line 104
    invoke-static {v0, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 105
    .line 106
    .line 107
    move-result v3

    .line 108
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 109
    .line 110
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 111
    .line 112
    .line 113
    const-string v5, "margin width="

    .line 114
    .line 115
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    .line 117
    .line 118
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 119
    .line 120
    .line 121
    const-string v5, ",marginF = "

    .line 122
    .line 123
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    .line 125
    .line 126
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 127
    .line 128
    .line 129
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 130
    .line 131
    .line 132
    move-result-object v0

    .line 133
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    .line 135
    .line 136
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇080:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 137
    .line 138
    mul-int/lit8 v3, v3, 0x2

    .line 139
    .line 140
    sub-int/2addr p1, v3

    .line 141
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->O〇8O8〇008(I)I

    .line 142
    .line 143
    .line 144
    move-result p1

    .line 145
    return p1

    .line 146
    :cond_2
    :goto_0
    return v1
    .line 147
.end method

.method public O〇080〇o0()V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇OO()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_1

    .line 13
    .line 14
    return-void

    .line 15
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 16
    .line 17
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 22
    .line 23
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 24
    .line 25
    .line 26
    move-result-wide v1

    .line 27
    new-instance v3, L〇OoO0o0/o800o8O;

    .line 28
    .line 29
    invoke-direct {v3, p0}, L〇OoO0o0/o800o8O;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 30
    .line 31
    .line 32
    new-instance v4, L〇OoO0o0/〇O888o0o;

    .line 33
    .line 34
    invoke-direct {v4, p0, v0}, L〇OoO0o0/〇O888o0o;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Lcom/intsig/mvp/activity/BaseChangeActivity;)V

    .line 35
    .line 36
    .line 37
    invoke-static {v0, v1, v2, v3, v4}, Lcom/intsig/camscanner/control/DataChecker;->〇80〇808〇O(Landroid/app/Activity;JLcom/intsig/camscanner/control/DataChecker$ActionListener;Lcom/intsig/camscanner/app/DbWaitingListener;)Z

    .line 38
    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public O〇0O〇Oo〇o()V
    .locals 2

    .line 1
    const-string v0, "showSnackBarWithoutScanDone: START!"

    .line 2
    .line 3
    const-string v1, "PageListPresenter"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o808o8o08()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    const-string v0, "showSnackBarWithoutScanDone: show SHARE-SHARE_DIR"

    .line 15
    .line 16
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    return-void

    .line 20
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oO88o()Z

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    if-eqz v0, :cond_1

    .line 25
    .line 26
    const-string v0, "showSnackBarWithoutScanDone: show SCENE-SHARE_DIR"

    .line 27
    .line 28
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    return-void

    .line 32
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o0Oo()Z

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    if-eqz v0, :cond_2

    .line 37
    .line 38
    const-string v0, "showSnackBarWithoutScanDone: show SCENARIO-ABROAD"

    .line 39
    .line 40
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    return-void

    .line 44
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇08()V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public O〇0〇o808〇()Lcom/intsig/camscanner/pagelist/model/EditType;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO80:Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O〇8oOo8O()Lcom/intsig/camscanner/business/operation/OperateContent;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return-object v0

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/business/operation/OperateEngine;->〇o00〇〇Oo()Lcom/intsig/camscanner/business/operation/OperateContent;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O〇8〇008(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oo08:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public O〇O(J)V
    .locals 1

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 2
    .line 3
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 4
    .line 5
    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O00:Landroid/net/Uri;

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    iput p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0〇O0088o:I

    .line 13
    .line 14
    const/4 p1, 0x1

    .line 15
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8oO〇:Z

    .line 16
    .line 17
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8:Z

    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oO8O0〇〇O()V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public O〇Oo()Ljava/lang/String;
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0〇O0088o:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-le v0, v1, :cond_0

    .line 5
    .line 6
    const-string v0, "multi"

    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const-string v0, "single"

    .line 10
    .line 11
    :goto_0
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O〇oO〇oo8o()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    const-string v1, "PageListPresenter"

    .line 15
    .line 16
    const-string v2, "User Operation: pdf setting"

    .line 17
    .line 18
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    const-string v1, "CSMorePop"

    .line 22
    .line 23
    const-string v2, "pdf_setting"

    .line 24
    .line 25
    invoke-static {v1, v2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    new-instance v1, Landroid/content/Intent;

    .line 29
    .line 30
    const-class v2, Lcom/intsig/camscanner/settings/PdfSettingActivity;

    .line 31
    .line 32
    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o()Landroid/net/Uri;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 40
    .line 41
    .line 42
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 43
    .line 44
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getFragment()Landroidx/fragment/app/Fragment;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    invoke-virtual {v0, v1}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 49
    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public O〇〇()[Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OoO8:[Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o00〇88〇08(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const-string v0, "killed by system"

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇〇O〇(Z)V

    .line 11
    .line 12
    .line 13
    const-string v0, "doc_uri"

    .line 14
    .line 15
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    check-cast v0, Landroid/net/Uri;

    .line 20
    .line 21
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oOoO8OO〇(Landroid/net/Uri;)V

    .line 22
    .line 23
    .line 24
    const-string v0, "doc_pagenum"

    .line 25
    .line 26
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 27
    .line 28
    .line 29
    move-result p1

    .line 30
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0O0〇(I)V

    .line 31
    .line 32
    .line 33
    new-instance p1, Ljava/lang/StringBuilder;

    .line 34
    .line 35
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 36
    .line 37
    .line 38
    const-string v0, "restoreInstanceState()  mPageNum = "

    .line 39
    .line 40
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o8O〇O()I

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    const-string v0, ", mDocUri:"

    .line 51
    .line 52
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o()Landroid/net/Uri;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object p1

    .line 66
    const-string v0, "PageListPresenter"

    .line 67
    .line 68
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    :cond_0
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public o08O()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇8o8o〇()Ljava/util/ArrayList;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    if-eqz v1, :cond_2

    .line 23
    .line 24
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    if-nez v2, :cond_1

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_1
    new-instance v2, L〇OoO0o0/〇0〇O0088o;

    .line 32
    .line 33
    invoke-direct {v2, p0, v0, v1}, L〇OoO0o0/〇0〇O0088o;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Landroid/app/Activity;Ljava/util/ArrayList;)V

    .line 34
    .line 35
    .line 36
    const/4 v3, 0x0

    .line 37
    invoke-static {v0, v1, v3, v2}, Lcom/intsig/camscanner/control/DataChecker;->〇O00(Landroid/app/Activity;Ljava/util/ArrayList;Ljava/lang/String;Lcom/intsig/camscanner/control/DataChecker$ActionListener;)Z

    .line 38
    .line 39
    .line 40
    goto :goto_1

    .line 41
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 42
    .line 43
    const v1, 0x7f130385

    .line 44
    .line 45
    .line 46
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->〇〇〇0880(I)V

    .line 47
    .line 48
    .line 49
    :goto_1
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o08oOO()V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    const-string v0, "PageListPresenter"

    .line 9
    .line 10
    const-string v1, "go2AutoComposite"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 16
    .line 17
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    sget-object v1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MORE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 22
    .line 23
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇〇0(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 27
    .line 28
    .line 29
    move-result-wide v1

    .line 30
    new-instance v3, L〇OoO0o0/o〇0;

    .line 31
    .line 32
    invoke-direct {v3, p0}, L〇OoO0o0/o〇0;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 33
    .line 34
    .line 35
    new-instance v4, L〇OoO0o0/〇〇888;

    .line 36
    .line 37
    invoke-direct {v4, p0, v0}, L〇OoO0o0/〇〇888;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Landroid/app/Activity;)V

    .line 38
    .line 39
    .line 40
    invoke-static {v0, v1, v2, v3, v4}, Lcom/intsig/camscanner/control/DataChecker;->〇80〇808〇O(Landroid/app/Activity;JLcom/intsig/camscanner/control/DataChecker$ActionListener;Lcom/intsig/camscanner/app/DbWaitingListener;)Z

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o08〇〇0O()V
    .locals 2

    .line 1
    const-string v0, "PageListPresenter"

    .line 2
    .line 3
    const-string v1, "go2PdfEnc"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO8o()Lcom/intsig/camscanner/util/PdfEncryptionUtil;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    iget-boolean v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o0ooO:Z

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/util/PdfEncryptionUtil;->OO0o〇〇〇〇0(Z)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o0OoOOo0()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o0O〇8o0O()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_2

    .line 8
    .line 9
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o()Landroid/net/Uri;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    if-eqz v1, :cond_1

    .line 21
    .line 22
    invoke-static {v1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 23
    .line 24
    .line 25
    move-result-wide v1

    .line 26
    const/4 v3, 0x2

    .line 27
    const/4 v4, 0x1

    .line 28
    invoke-static {v0, v1, v2, v3, v4}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0oO0(Landroid/content/Context;JIZ)V

    .line 29
    .line 30
    .line 31
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO0O()V

    .line 32
    .line 33
    .line 34
    :cond_1
    const-string v1, "PageListPresenter"

    .line 35
    .line 36
    const-string v2, "doDelEmptyDoc"

    .line 37
    .line 38
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 42
    .line 43
    .line 44
    :cond_2
    :goto_0
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o0oO()Z
    .locals 6

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/utils/AppHelper;->〇o〇(Landroid/content/Context;)Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    const/4 v2, 0x0

    .line 14
    if-eqz v1, :cond_0

    .line 15
    .line 16
    return v2

    .line 17
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    .line 18
    .line 19
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v3

    .line 26
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    const-string v3, "some_entrance_hide"

    .line 30
    .line 31
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    invoke-static {v1}, Lcom/intsig/utils/AppHelper;->O8([B)Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    const-string v3, "10D13B4AA35BC5B21189CF7CEA331141"

    .line 47
    .line 48
    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 49
    .line 50
    .line 51
    move-result v3

    .line 52
    const/4 v4, 0x1

    .line 53
    if-nez v3, :cond_1

    .line 54
    .line 55
    const-string v3, "2F73B127C13CC300C75348F4AD24E681"

    .line 56
    .line 57
    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 58
    .line 59
    .line 60
    move-result v3

    .line 61
    if-eqz v3, :cond_2

    .line 62
    .line 63
    :cond_1
    const/4 v2, 0x1

    .line 64
    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    .line 65
    .line 66
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 67
    .line 68
    .line 69
    const-string v5, "sig = "

    .line 70
    .line 71
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    const-string v0, "     md5 = "

    .line 78
    .line 79
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v0

    .line 89
    const-string v1, "PageListPresenter"

    .line 90
    .line 91
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    xor-int/lit8 v0, v2, 0x1

    .line 95
    .line 96
    return v0
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public o0〇〇00()V
    .locals 8

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    instance-of v0, v0, Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 15
    .line 16
    if-nez v0, :cond_1

    .line 17
    .line 18
    return-void

    .line 19
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/sharedir/data/ShareDirDbUtil;->〇080:Lcom/intsig/camscanner/sharedir/data/ShareDirDbUtil;

    .line 20
    .line 21
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 22
    .line 23
    .line 24
    move-result-wide v1

    .line 25
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/sharedir/data/ShareDirDbUtil;->〇o00〇〇Oo(J)I

    .line 26
    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 29
    .line 30
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    move-object v1, v0

    .line 35
    check-cast v1, Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 36
    .line 37
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇8o8o〇()Ljava/util/ArrayList;

    .line 42
    .line 43
    .line 44
    move-result-object v4

    .line 45
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 46
    .line 47
    .line 48
    move-result-wide v2

    .line 49
    sget-object v5, Lcom/intsig/camscanner/share/ShareHelper$ShareType;->DEFAULT:Lcom/intsig/camscanner/share/ShareHelper$ShareType;

    .line 50
    .line 51
    new-instance v6, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$3;

    .line 52
    .line 53
    invoke-direct {v6, p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$3;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 54
    .line 55
    .line 56
    new-instance v7, L〇OoO0o0/oO80;

    .line 57
    .line 58
    invoke-direct {v7, p0, v1}, L〇OoO0o0/oO80;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Lcom/intsig/mvp/activity/BaseChangeActivity;)V

    .line 59
    .line 60
    .line 61
    invoke-static/range {v1 .. v7}, Lcom/intsig/camscanner/share/ShareHelper;->o〇0o〇〇(Landroidx/fragment/app/FragmentActivity;JLjava/util/ArrayList;Lcom/intsig/camscanner/share/ShareHelper$ShareType;Lcom/intsig/camscanner/share/listener/SharePreviousInterceptor;Lcom/intsig/camscanner/share/listener/ShareBackListener;)V

    .line 62
    .line 63
    .line 64
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o88(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O08000:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o880(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8〇o:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 17
    .line 18
    .line 19
    move-result-wide v1

    .line 20
    new-instance p1, L〇OoO0o0/〇O00;

    .line 21
    .line 22
    invoke-direct {p1, p0}, L〇OoO0o0/〇O00;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 23
    .line 24
    .line 25
    new-instance v3, L〇OoO0o0/〇〇8O0〇8;

    .line 26
    .line 27
    invoke-direct {v3, p0, v0}, L〇OoO0o0/〇〇8O0〇8;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Landroid/app/Activity;)V

    .line 28
    .line 29
    .line 30
    invoke-static {v0, v1, v2, p1, v3}, Lcom/intsig/camscanner/control/DataChecker;->〇80〇808〇O(Landroid/app/Activity;JLcom/intsig/camscanner/control/DataChecker$ActionListener;Lcom/intsig/camscanner/app/DbWaitingListener;)Z

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public o88O8(Ljava/util/ArrayList;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;Z)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    if-eqz p1, :cond_3

    .line 9
    .line 10
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-gtz v0, :cond_1

    .line 15
    .line 16
    goto :goto_1

    .line 17
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO〇()Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-static {p1}, Lcom/intsig/camscanner/util/Util;->Oo〇o(Ljava/util/List;)[J

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    iput-object v1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o8〇OO0〇0o:[J

    .line 26
    .line 27
    iget v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0〇O0088o:I

    .line 28
    .line 29
    const/4 v2, 0x1

    .line 30
    if-ne v1, v2, :cond_2

    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O8oOo0()V

    .line 33
    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_2
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 37
    .line 38
    invoke-interface {v1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getFragment()Landroidx/fragment/app/Fragment;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    const/16 v2, 0x3fe

    .line 43
    .line 44
    invoke-static {v1, p2, p1, v0, v2}, Lcom/intsig/camscanner/batch/BatchImageReeditActivity;->o〇08oO80o(Landroidx/fragment/app/Fragment;ZLjava/util/ArrayList;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;I)V

    .line 45
    .line 46
    .line 47
    :goto_0
    return-void

    .line 48
    :cond_3
    :goto_1
    const-string p1, "PageListPresenter"

    .line 49
    .line 50
    const-string p2, "pageIds isEmpty"

    .line 51
    .line 52
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public o88o0O()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8O〇:Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    return v0

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;->Oooo8o0〇()Landroidx/lifecycle/MutableLiveData;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;

    .line 16
    .line 17
    invoke-static {v0}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker;->Oo08(Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;)Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    return v0
.end method

.method public o8O0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oo8Oo00oo:Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 6
    .line 7
    invoke-interface {v1, v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->Ooo8(Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oo8Oo00oo:Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;->〇o〇()V

    .line 13
    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oo8Oo00oo:Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;

    .line 17
    .line 18
    :cond_0
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method public o8〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇80〇808〇O:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oO00〇o(Landroid/content/Intent;Ljava/lang/String;)V
    .locals 12

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o0()Landroidx/fragment/app/FragmentActivity;

    move-result-object p2

    if-nez p2, :cond_0

    return-void

    .line 2
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO80:Lcom/intsig/camscanner/pagelist/model/EditType;

    sget-object v2, Lcom/intsig/camscanner/pagelist/model/EditType;->MOVE:Lcom/intsig/camscanner/pagelist/model/EditType;

    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eq v1, v2, :cond_2

    sget-object v2, Lcom/intsig/camscanner/pagelist/model/EditType;->EXTRACT:Lcom/intsig/camscanner/pagelist/model/EditType;

    if-eq v1, v2, :cond_2

    sget-object v2, Lcom/intsig/camscanner/pagelist/model/EditType;->EXTRACT_CS_DOC:Lcom/intsig/camscanner/pagelist/model/EditType;

    if-ne v1, v2, :cond_1

    goto :goto_0

    :cond_1
    const-string v1, "constant_enter_all_selected"

    .line 4
    invoke-virtual {p1, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇80〇808〇O:Z

    goto :goto_1

    .line 5
    :cond_2
    :goto_0
    iput-boolean v3, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇80〇808〇O:Z

    :goto_1
    const-string v1, "extra_image_scanner_activity_engine_classify"

    .line 6
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;

    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇808〇:Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;

    const-string v1, "extra_scan_engine_detect_doc_type"

    const/4 v2, -0x1

    .line 7
    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O〇:I

    const-string v1, "extra_from_widget"

    .line 8
    invoke-virtual {p1, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO0o〇〇〇〇0:Z

    const-string v1, "extra_from_document_short_cut"

    .line 9
    invoke-virtual {p1, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o8o〇:Z

    const-string v1, "extra_start_do_camera"

    .line 10
    invoke-virtual {p1, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O8o08O:Z

    const-string v1, "extra_folder_id"

    .line 11
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO0o〇〇:Ljava/lang/String;

    const-string v1, "extra_offline_folder"

    .line 12
    invoke-virtual {p1, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oooo8o0〇:Z

    const-string v1, "capture_mode_is_now_mode"

    .line 13
    invoke-virtual {p1, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    const-string v1, "extra_is_new_doc_first_view"

    .line 14
    invoke-virtual {p1, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 15
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onActionReceived() action="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ",   from: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Landroid/app/Activity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v6, " ,mParentSyncId"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO0o〇〇:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " ,mIsOfflinedDoc:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v6, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oooo8o0〇:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "PageListPresenter"

    invoke-static {v6, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, "is_to_word_demo"

    .line 16
    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v7, "1"

    invoke-static {v7, v5}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    const-string v7, "uri == null"

    const-wide/16 v8, -0x1

    if-eqz v5, :cond_4

    .line 17
    new-instance v5, Lcom/intsig/camscanner/DocumentPresenter;

    invoke-direct {v5}, Lcom/intsig/camscanner/DocumentPresenter;-><init>()V

    .line 18
    invoke-virtual {v5}, Lcom/intsig/camscanner/DocumentPresenter;->〇080()Landroid/net/Uri;

    move-result-object v10

    if-eqz v10, :cond_3

    .line 19
    invoke-virtual {p1, v10}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 20
    iput-object v10, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O00:Landroid/net/Uri;

    .line 21
    iput-wide v8, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇0OOo〇0:J

    .line 22
    invoke-static {v10}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v10

    iput-wide v10, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 23
    invoke-virtual {v5}, Lcom/intsig/camscanner/DocumentPresenter;->〇o00〇〇Oo()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o800o8O:Ljava/lang/String;

    .line 24
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "onActionReceived mDocUri = "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v10, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O00:Landroid/net/Uri;

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 25
    :cond_3
    invoke-static {v6, v7}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    invoke-interface {p1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->o88O8()V

    return-void

    :cond_4
    :goto_2
    const-string v5, "com.intsig.camscanner.NEW_DOC"

    .line 27
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    const-string v10, "com.intsig.camscanner.NEW_BATOCR_DOC"

    const-string v11, "android.intent.action.VIEW"

    if-eqz v5, :cond_7

    .line 28
    iget-boolean v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O888o0o:Z

    if-eqz v1, :cond_5

    .line 29
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oO8O0〇〇O()V

    return-void

    .line 30
    :cond_5
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_6

    .line 31
    invoke-static {v6, v7}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    invoke-interface {p1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->o88O8()V

    return-void

    :cond_6
    const-string v2, "import"

    .line 33
    invoke-virtual {p1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇08O8o〇0:Z

    .line 34
    new-instance v2, Lcom/intsig/camscanner/pagelist/DocCreateClient;

    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o0()Landroidx/fragment/app/FragmentActivity;

    move-result-object v3

    iget-object v5, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO0o〇〇:Ljava/lang/String;

    iget-boolean v7, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oooo8o0〇:Z

    invoke-direct {v2, v3, v5, v7}, Lcom/intsig/camscanner/pagelist/DocCreateClient;-><init>(Landroid/app/Activity;Ljava/lang/String;Z)V

    .line 35
    iget-boolean v3, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇08O8o〇0:Z

    invoke-virtual {v2, p1, v1, v3}, Lcom/intsig/camscanner/pagelist/DocCreateClient;->〇o00〇〇Oo(Landroid/content/Intent;Landroid/net/Uri;Z)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 36
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/DocCreateClient;->〇〇888()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O00:Landroid/net/Uri;

    .line 37
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActionReceived createNewForSingleScan mDocUri = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O00:Landroid/net/Uri;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/DocCreateClient;->Oo08()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 39
    sget-object v1, Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient;->〇O00:Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient$Companion;

    invoke-virtual {v1}, Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient$Companion;->〇080()Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient;

    move-result-object v1

    iget-wide v5, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    invoke-virtual {v1, v5, v6, v8, v9}, Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient;->〇00(JJ)V

    .line 40
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/DocCreateClient;->oO80()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇0OOo〇0:J

    .line 41
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/DocCreateClient;->o〇0()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_10

    .line 42
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/DocCreateClient;->o〇0()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o800o8O:Ljava/lang/String;

    goto/16 :goto_4

    .line 43
    :cond_7
    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    if-nez v1, :cond_8

    .line 44
    iput-boolean v3, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇0o:Z

    .line 45
    :cond_8
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O00:Landroid/net/Uri;

    const-string v1, "EXTRA_QUERY_STRING"

    .line 46
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OoO8:[Ljava/lang/String;

    .line 47
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇080:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    invoke-virtual {v3, v1}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇00([Ljava/lang/String;)V

    .line 48
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇080:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OoO8:[Ljava/lang/String;

    invoke-static {v3}, Lcom/intsig/camscanner/util/StringUtil;->〇O8o08O([Ljava/lang/String;)[Ljava/util/regex/Pattern;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->o〇O8〇〇o([Ljava/util/regex/Pattern;)V

    .line 49
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "query string = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OoO8:[Ljava/lang/String;

    invoke-static {v3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " mDocUri="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O00:Landroid/net/Uri;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->o88〇OO08〇()Z

    move-result v1

    if-nez v1, :cond_9

    .line 51
    invoke-static {p2}, Lcom/intsig/camscanner/util/SDStorageManager;->oo(Landroid/content/Context;)V

    :cond_9
    const-string v1, "EXTRA_OPEN_API_CREATE"

    .line 52
    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-lez v1, :cond_10

    const-string v2, "EXTRA_OPEN_API_APPID"

    .line 53
    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 54
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    invoke-interface {v3, v1, v2}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->o08o〇0(ILjava/lang/String;)V

    goto/16 :goto_4

    :cond_a
    const-string v1, "com.intsig.camscanner.NEW_DOC_MULTIPLE"

    .line 55
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "doc_id"

    if-eqz v1, :cond_b

    .line 56
    invoke-virtual {p1, v2, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 57
    sget-object v3, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    invoke-static {v3, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O00:Landroid/net/Uri;

    const-string v1, "doc_title"

    .line 58
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o800o8O:Ljava/lang/String;

    .line 59
    sget-object v1, Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient;->〇O00:Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient$Companion;

    invoke-virtual {v1}, Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient$Companion;->〇080()Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient;

    move-result-object v1

    iget-wide v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    invoke-virtual {v1, v2, v3, v8, v9}, Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient;->〇00(JJ)V

    .line 60
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "URI "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O00:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_b
    const-string v1, "com.intsig.camscanner.NEW_DOC_CERTIFICATE"

    .line 61
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_e

    const-string v5, "com.intsig.camscanner.NEW_DOC_CERTIFICATE_PHOTO"

    .line 62
    invoke-static {v5, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_e

    const-string v5, "com.intsig.camscanner.NEW_DOC_EXCEL"

    .line 63
    invoke-static {v5, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_c

    goto :goto_3

    :cond_c
    const-string v1, "com.intsig.camscanner.NEW_DOC_TOPIC"

    .line 64
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    const-string v1, "com.intsig.camscanner.NEW_DOC_BOOK_SPLITTER"

    .line 65
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 66
    invoke-virtual {v10, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    const-string v1, "com.intsig.camscanner.NEW_DOC_IMAGE_RESTORE"

    .line 67
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    const-string v1, "com.intsig.camscanner.NEW_DOC_IMAGE_TRANSLATE"

    .line 68
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 69
    :cond_d
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O00:Landroid/net/Uri;

    .line 70
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mDocUri = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O00:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O00:Landroid/net/Uri;

    if-eqz v1, :cond_10

    .line 72
    invoke-static {v1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 73
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-wide v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    invoke-static {v1, v2, v3}, Lcom/intsig/camscanner/db/dao/DocumentDao;->O〇8O8〇008(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o800o8O:Ljava/lang/String;

    goto :goto_4

    .line 74
    :cond_e
    :goto_3
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 75
    invoke-virtual {p0, v3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇O80〇0o(Z)V

    .line 76
    :cond_f
    invoke-virtual {p1, v2, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 77
    sget-object v3, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    invoke-static {v3, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O00:Landroid/net/Uri;

    .line 78
    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-wide v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    invoke-static {v1, v2, v3}, Lcom/intsig/camscanner/db/dao/DocumentDao;->O〇8O8〇008(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o800o8O:Ljava/lang/String;

    .line 79
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " certificate uri "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O00:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    :cond_10
    :goto_4
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oO8O0〇〇O()V

    .line 81
    iget-wide v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    invoke-static {p2, v1, v2}, Lcom/intsig/camscanner/db/dao/ImageDao;->OO0o〇〇〇〇0(Landroid/content/Context;J)V

    .line 82
    iget-wide v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    const-wide/16 v5, 0x0

    cmp-long v3, v1, v5

    if-lez v3, :cond_11

    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->ooo〇8oO()Z

    move-result v1

    if-nez v1, :cond_11

    .line 83
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    move-result-object v1

    new-instance v2, L〇OoO0o0/OO0o〇〇〇〇0;

    invoke-direct {v2, p0, p2}, L〇OoO0o0/OO0o〇〇〇〇0;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Landroid/app/Activity;)V

    invoke-virtual {v1, v2}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 84
    :cond_11
    invoke-static {p1}, Lcom/intsig/camscanner/app/IntentUtil;->〇O8o08O(Landroid/content/Intent;)Ljava/util/ArrayList;

    move-result-object v1

    const-string v2, "extra_delete_title_res_id"

    .line 85
    invoke-virtual {p1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result p1

    if-eqz v1, :cond_12

    .line 86
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_12

    if-lez p1, :cond_12

    .line 87
    invoke-static {p2, p1, v1}, Lcom/intsig/camscanner/app/DialogUtils;->〇00〇8(Landroid/content/Context;ILjava/util/ArrayList;)V

    .line 88
    :cond_12
    invoke-static {v0, v11}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_13

    .line 89
    invoke-static {v0, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_13

    .line 90
    invoke-static {}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o〇()Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;

    move-result-object p1

    sget-object p2, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;->NOVICE_SCAN:Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;

    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->o〇0(Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;)V

    :cond_13
    return-void
.end method

.method public oO0〇〇O8o()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O8o08O:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oO8008O()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇0OOo〇0:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oO80OOO〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8O〇:Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return v0

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;->Oooo8o0〇()Landroidx/lifecycle/MutableLiveData;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;

    .line 16
    .line 17
    invoke-static {v0}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker;->oO80(Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;)Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    return v0
.end method

.method public oOO0880O()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    throw v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oOO8()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/pagelist/presenter/PageListDocPresenter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListDocPresenter;->〇080()Lcom/intsig/camscanner/pagelist/reader/DocReaderManager;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/reader/DocReaderManager;->〇o00〇〇Oo()V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oOo()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇8O8〇008:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oOoO8OO〇(Landroid/net/Uri;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O00:Landroid/net/Uri;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public oOoo80oO(Landroid/app/Activity;)V
    .locals 0

    .line 1
    const/4 p1, 0x0

    .line 2
    throw p1
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public oOo〇08〇()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO8o()Lcom/intsig/camscanner/util/PdfEncryptionUtil;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/PdfEncryptionUtil;->〇〇808〇()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oO〇()Lcom/intsig/camscanner/datastruct/ParcelDocInfo;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 7
    .line 8
    .line 9
    move-result-wide v1

    .line 10
    iput-wide v1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇0008O8()I

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    iput v1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->oOo0:I

    .line 17
    .line 18
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo8o〇o〇()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    iput-object v1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO:Ljava/lang/String;

    .line 23
    .line 24
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0O00oO()Z

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    iput-boolean v1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇08O〇00〇o:Z

    .line 29
    .line 30
    return-object v0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public oO〇oo()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇0o8O〇〇()Lcom/intsig/advertisement/listener/OnAdPositionListener;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇〇0〇〇0:Lcom/intsig/advertisement/listener/OnAdPositionListener;

    .line 6
    .line 7
    new-instance v0, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 10
    .line 11
    invoke-interface {v1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-direct {v0, v1}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;-><init>(Landroid/content/Context;)V

    .line 16
    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o0()Landroidx/fragment/app/FragmentActivity;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 23
    .line 24
    .line 25
    move-result-wide v2

    .line 26
    invoke-static {v1, v2, v3}, Lcom/intsig/camscanner/ads/csAd/CsAdUtil;->〇O8o08O(Landroid/app/Activity;J)Ljava/util/HashMap;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->〇8o8o〇(Ljava/util/HashMap;)Lcom/intsig/advertisement/params/AdRequestOptions$Builder;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇〇0〇〇0:Lcom/intsig/advertisement/listener/OnAdPositionListener;

    .line 35
    .line 36
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->〇O8o08O(Lcom/intsig/advertisement/listener/OnAdRequestListener;)Lcom/intsig/advertisement/params/AdRequestOptions$Builder;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->〇80〇808〇O()Lcom/intsig/advertisement/params/AdRequestOptions;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    invoke-static {}, Lcom/intsig/advertisement/adapters/positions/ListBannerManager;->o〇8oOO88()Lcom/intsig/advertisement/adapters/positions/ListBannerManager;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    invoke-virtual {v1, v0}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->〇8〇0〇o〇O(Lcom/intsig/advertisement/params/AdRequestOptions;)V

    .line 49
    .line 50
    .line 51
    new-instance v0, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;

    .line 52
    .line 53
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 54
    .line 55
    invoke-interface {v1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    invoke-direct {v0, v1}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;-><init>(Landroid/content/Context;)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v0}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->〇80〇808〇O()Lcom/intsig/advertisement/params/AdRequestOptions;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    sget-object v1, Lcom/intsig/advertisement/adapters/positions/vir/VirListBannerManager;->〇O8o08O:Lcom/intsig/advertisement/adapters/positions/vir/VirListBannerManager$Companion;

    .line 67
    .line 68
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/positions/vir/VirListBannerManager$Companion;->〇080()Lcom/intsig/advertisement/adapters/positions/vir/VirListBannerManager;

    .line 69
    .line 70
    .line 71
    move-result-object v1

    .line 72
    invoke-virtual {v1, v0}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->〇8〇0〇o〇O(Lcom/intsig/advertisement/params/AdRequestOptions;)V

    .line 73
    .line 74
    .line 75
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public oo0O〇0〇〇〇(Landroidx/appcompat/app/AppCompatActivity;)Lcom/intsig/camscanner/share/ShareHelper;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇00〇8:Lcom/intsig/camscanner/share/ShareHelper;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-static {p1}, Lcom/intsig/camscanner/share/ShareHelper;->ooo8o〇o〇(Landroidx/fragment/app/FragmentActivity;)Lcom/intsig/camscanner/share/ShareHelper;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇00〇8:Lcom/intsig/camscanner/share/ShareHelper;

    .line 10
    .line 11
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇00〇8:Lcom/intsig/camscanner/share/ShareHelper;

    .line 12
    .line 13
    return-object p1
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public ooO〇00O(Lcom/intsig/camscanner/capture/SupportCaptureModeOption;Ljava/lang/String;Z)V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v3

    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-static {v0}, Lcom/intsig/camscanner/util/SDStorageManager;->〇〇888(Landroid/app/Activity;)Z

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    if-nez v0, :cond_1

    .line 25
    .line 26
    return-void

    .line 27
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇()Z

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-eqz v0, :cond_2

    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 34
    .line 35
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->O00O()V

    .line 36
    .line 37
    .line 38
    :cond_2
    iget v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8:I

    .line 39
    .line 40
    const/16 v1, 0x8c

    .line 41
    .line 42
    if-ne v0, v1, :cond_3

    .line 43
    .line 44
    sget-object p1, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_COUNT_NUMBER:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 45
    .line 46
    :cond_3
    move-object v4, p1

    .line 47
    new-instance p1, L〇OoO0o0/Oo08;

    .line 48
    .line 49
    move-object v1, p1

    .line 50
    move-object v2, p0

    .line 51
    move-object v5, p2

    .line 52
    move v6, p3

    .line 53
    invoke-direct/range {v1 .. v6}, L〇OoO0o0/Oo08;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Landroid/app/Activity;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;Ljava/lang/String;Z)V

    .line 54
    .line 55
    .line 56
    invoke-static {p1}, Lcom/intsig/camscanner/app/AppUtil;->〇O8o08O(Lcom/intsig/camscanner/app/AppUtil$ICheckCameraListener;)V

    .line 57
    .line 58
    .line 59
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public ooo8o〇o〇()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO0o〇〇:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oooO888()V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o0()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-boolean v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0O8OO088:Z

    .line 9
    .line 10
    if-eqz v1, :cond_1

    .line 11
    .line 12
    iget-wide v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 13
    .line 14
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/app/DBUtil;->Ooo8(Landroid/content/Context;J)V

    .line 15
    .line 16
    .line 17
    iget-wide v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 18
    .line 19
    const/4 v3, 0x3

    .line 20
    const/4 v4, 0x1

    .line 21
    invoke-static {v0, v1, v2, v3, v4}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0oO0(Landroid/content/Context;JIZ)V

    .line 22
    .line 23
    .line 24
    iget-wide v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 25
    .line 26
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/tsapp/AutoUploadThread;->〇〇8O0〇8(Landroid/content/Context;J)I

    .line 27
    .line 28
    .line 29
    const/4 v0, 0x0

    .line 30
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0O8OO088:Z

    .line 31
    .line 32
    :cond_1
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public ooo〇〇O〇()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO0o〇〇〇〇0:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o〇0〇o()V
    .locals 4

    .line 1
    const-string v0, "PageListPresenter"

    .line 2
    .line 3
    const-string v1, "sendToPC"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇OO()Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-eqz v0, :cond_1

    .line 20
    .line 21
    return-void

    .line 22
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 23
    .line 24
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    const-string v1, "CSMorePop"

    .line 29
    .line 30
    const-string v2, "edit_on_pc"

    .line 31
    .line 32
    invoke-static {v1, v2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    new-instance v1, Ljava/util/ArrayList;

    .line 36
    .line 37
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 38
    .line 39
    .line 40
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 41
    .line 42
    .line 43
    move-result-wide v2

    .line 44
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 45
    .line 46
    .line 47
    move-result-object v2

    .line 48
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    .line 50
    .line 51
    invoke-static {v0, v1}, Lcom/intsig/camscanner/share/type/SendToPc;->Oo〇O(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;)Lcom/intsig/camscanner/share/type/SendToPc;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    const v2, 0x138d4

    .line 56
    .line 57
    .line 58
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/share/type/SendToPc;->OOo8o〇O(I)V

    .line 59
    .line 60
    .line 61
    invoke-static {v0}, Lcom/intsig/camscanner/share/ShareHelper;->ooo8o〇o〇(Landroidx/fragment/app/FragmentActivity;)Lcom/intsig/camscanner/share/ShareHelper;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 66
    .line 67
    .line 68
    return-void
.end method

.method public o〇O8OO(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇OOo000()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8O〇:Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return v0

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;->Oooo8o0〇()Landroidx/lifecycle/MutableLiveData;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    check-cast v0, Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;

    .line 16
    .line 17
    invoke-static {v0}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker;->O8(Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;)Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    return v0
.end method

.method public o〇o(Landroid/content/Intent;Landroid/net/Uri;Ljava/lang/String;Z)V
    .locals 19

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    move-object/from16 v0, p1

    .line 4
    .line 5
    move/from16 v7, p4

    .line 6
    .line 7
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o0()Landroidx/fragment/app/FragmentActivity;

    .line 8
    .line 9
    .line 10
    move-result-object v8

    .line 11
    const-string v9, "PageListPresenter"

    .line 12
    .line 13
    if-nez v8, :cond_0

    .line 14
    .line 15
    const-string v0, "activity == null"

    .line 16
    .line 17
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    return-void

    .line 21
    :cond_0
    if-nez v0, :cond_1

    .line 22
    .line 23
    const-string v0, "data == null"

    .line 24
    .line 25
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    return-void

    .line 29
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    if-nez v2, :cond_2

    .line 34
    .line 35
    const-string v0, "data.getData() == null"

    .line 36
    .line 37
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    return-void

    .line 41
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 42
    .line 43
    .line 44
    move-result-wide v10

    .line 45
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    .line 46
    .line 47
    .line 48
    move-result-object v2

    .line 49
    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v2

    .line 53
    const-string v3, "raw_path"

    .line 54
    .line 55
    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v3

    .line 59
    const-string v4, "extra_thumb_path"

    .line 60
    .line 61
    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v4

    .line 65
    invoke-static {v4}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 66
    .line 67
    .line 68
    move-result v5

    .line 69
    if-nez v5, :cond_3

    .line 70
    .line 71
    invoke-static {v2}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇o(Ljava/lang/String;)Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v4

    .line 75
    :cond_3
    new-instance v5, Lcom/intsig/camscanner/datastruct/PageProperty;

    .line 76
    .line 77
    invoke-direct {v5}, Lcom/intsig/camscanner/datastruct/PageProperty;-><init>()V

    .line 78
    .line 79
    .line 80
    iput-object v3, v5, Lcom/intsig/camscanner/datastruct/PageProperty;->OO:Ljava/lang/String;

    .line 81
    .line 82
    iput-object v2, v5, Lcom/intsig/camscanner/datastruct/PageProperty;->〇OOo8〇0:Ljava/lang/String;

    .line 83
    .line 84
    iput-object v4, v5, Lcom/intsig/camscanner/datastruct/PageProperty;->〇08O〇00〇o:Ljava/lang/String;

    .line 85
    .line 86
    invoke-static {v0, v5}, Lcom/intsig/camscanner/app/DBUtil;->〇o0O0O8(Landroid/content/Intent;Lcom/intsig/camscanner/datastruct/PageProperty;)Lcom/intsig/camscanner/datastruct/PageProperty;

    .line 87
    .line 88
    .line 89
    :try_start_0
    invoke-static/range {p2 .. p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 90
    .line 91
    .line 92
    move-result-wide v2

    .line 93
    iput-wide v2, v5, Lcom/intsig/camscanner/datastruct/PageProperty;->o0:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    .line 95
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o0()Landroidx/fragment/app/FragmentActivity;

    .line 96
    .line 97
    .line 98
    move-result-object v2

    .line 99
    iget-wide v3, v5, Lcom/intsig/camscanner/datastruct/PageProperty;->o0:J

    .line 100
    .line 101
    invoke-static {v2, v3, v4}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇〇8O0〇8(Landroid/content/Context;J)I

    .line 102
    .line 103
    .line 104
    move-result v2

    .line 105
    iput v2, v1, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0〇O0088o:I

    .line 106
    .line 107
    iget v2, v1, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0〇O0088o:I

    .line 108
    .line 109
    const/4 v12, 0x1

    .line 110
    add-int/2addr v2, v12

    .line 111
    iput v2, v5, Lcom/intsig/camscanner/datastruct/PageProperty;->o〇00O:I

    .line 112
    .line 113
    const-string v2, "intent_image_quality"

    .line 114
    .line 115
    const/4 v3, -0x1

    .line 116
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 117
    .line 118
    .line 119
    move-result v0

    .line 120
    iput v0, v5, Lcom/intsig/camscanner/datastruct/PageProperty;->oo8ooo8O:I

    .line 121
    .line 122
    sget-object v0, Lcom/intsig/camscanner/app/DBInsertPageUtil;->〇080:Lcom/intsig/camscanner/app/DBInsertPageUtil;

    .line 123
    .line 124
    iget v2, v1, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇O8〇〇o:I

    .line 125
    .line 126
    move-object/from16 v3, p3

    .line 127
    .line 128
    invoke-virtual {v0, v5, v3, v2, v7}, Lcom/intsig/camscanner/app/DBInsertPageUtil;->〇oo〇(Lcom/intsig/camscanner/datastruct/PageProperty;Ljava/lang/String;IZ)Landroid/net/Uri;

    .line 129
    .line 130
    .line 131
    move-result-object v0

    .line 132
    if-eqz v0, :cond_6

    .line 133
    .line 134
    iget v2, v5, Lcom/intsig/camscanner/datastruct/PageProperty;->o8o:I

    .line 135
    .line 136
    const/16 v3, 0xfa0

    .line 137
    .line 138
    if-ne v2, v3, :cond_4

    .line 139
    .line 140
    iget-wide v2, v5, Lcom/intsig/camscanner/datastruct/PageProperty;->o0:J

    .line 141
    .line 142
    sget-object v4, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 143
    .line 144
    const v5, 0x7f13156a

    .line 145
    .line 146
    .line 147
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 148
    .line 149
    .line 150
    move-result-object v4

    .line 151
    const-string v5, "createNewForSingleScan"

    .line 152
    .line 153
    const-string v6, "0CS690TagWritePad20230510"

    .line 154
    .line 155
    invoke-static {v5, v2, v3, v6, v4}, Lcom/intsig/camscanner/app/DBUtil;->o0O〇8o0O(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    .line 156
    .line 157
    .line 158
    :cond_4
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O0oO()Z

    .line 159
    .line 160
    .line 161
    move-result v2

    .line 162
    if-eqz v2, :cond_6

    .line 163
    .line 164
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 165
    .line 166
    .line 167
    move-result-object v13

    .line 168
    const-string v2, "_data"

    .line 169
    .line 170
    filled-new-array {v2}, [Ljava/lang/String;

    .line 171
    .line 172
    .line 173
    move-result-object v15

    .line 174
    const/16 v16, 0x0

    .line 175
    .line 176
    const/16 v17, 0x0

    .line 177
    .line 178
    const/16 v18, 0x0

    .line 179
    .line 180
    move-object v14, v0

    .line 181
    invoke-virtual/range {v13 .. v18}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 182
    .line 183
    .line 184
    move-result-object v2

    .line 185
    if-eqz v2, :cond_6

    .line 186
    .line 187
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 188
    .line 189
    .line 190
    move-result v3

    .line 191
    if-eqz v3, :cond_5

    .line 192
    .line 193
    const/4 v3, 0x0

    .line 194
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 195
    .line 196
    .line 197
    move-result-object v3

    .line 198
    invoke-static {v3}, Lcom/intsig/camscanner/util/SDStorageManager;->〇80(Ljava/lang/String;)Ljava/lang/String;

    .line 199
    .line 200
    .line 201
    move-result-object v3

    .line 202
    invoke-static {v3}, Lcom/intsig/camscanner/app/AppUtil;->〇0〇O0088o(Ljava/lang/String;)V

    .line 203
    .line 204
    .line 205
    :cond_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 206
    .line 207
    .line 208
    :cond_6
    if-eqz v0, :cond_7

    .line 209
    .line 210
    sget-object v2, Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient;->〇O00:Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient$Companion;

    .line 211
    .line 212
    invoke-virtual {v2}, Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient$Companion;->〇080()Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient;

    .line 213
    .line 214
    .line 215
    move-result-object v2

    .line 216
    invoke-static/range {p2 .. p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 217
    .line 218
    .line 219
    move-result-wide v3

    .line 220
    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 221
    .line 222
    .line 223
    move-result-wide v5

    .line 224
    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/intsig/camscanner/ocrapi/SilentLocalOcrClient;->〇00(JJ)V

    .line 225
    .line 226
    .line 227
    :cond_7
    new-instance v2, Ljava/lang/StringBuilder;

    .line 228
    .line 229
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 230
    .line 231
    .line 232
    const-string v3, "after insertOneImage u "

    .line 233
    .line 234
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    .line 236
    .line 237
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 238
    .line 239
    .line 240
    const-string v0, ", issaveready = "

    .line 241
    .line 242
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 243
    .line 244
    .line 245
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 246
    .line 247
    .line 248
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 249
    .line 250
    .line 251
    move-result-object v0

    .line 252
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    .line 254
    .line 255
    iget v0, v1, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0〇O0088o:I

    .line 256
    .line 257
    add-int/2addr v0, v12

    .line 258
    iput v0, v1, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0〇O0088o:I

    .line 259
    .line 260
    new-instance v0, Landroid/content/ContentValues;

    .line 261
    .line 262
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 263
    .line 264
    .line 265
    iget v2, v1, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0〇O0088o:I

    .line 266
    .line 267
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 268
    .line 269
    .line 270
    move-result-object v2

    .line 271
    const-string v3, "pages"

    .line 272
    .line 273
    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 274
    .line 275
    .line 276
    const-string v2, "state"

    .line 277
    .line 278
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 279
    .line 280
    .line 281
    move-result-object v3

    .line 282
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 283
    .line 284
    .line 285
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 286
    .line 287
    .line 288
    move-result-object v2

    .line 289
    const/4 v3, 0x0

    .line 290
    move-object/from16 v13, p2

    .line 291
    .line 292
    invoke-virtual {v2, v13, v0, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 293
    .line 294
    .line 295
    invoke-static/range {p2 .. p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 296
    .line 297
    .line 298
    move-result-wide v3

    .line 299
    const/4 v5, 0x3

    .line 300
    const/4 v6, 0x1

    .line 301
    move-object v2, v8

    .line 302
    move/from16 v7, p4

    .line 303
    .line 304
    invoke-static/range {v2 .. v7}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o08o〇0(Landroid/content/Context;JIZZ)V

    .line 305
    .line 306
    .line 307
    invoke-static/range {p2 .. p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 308
    .line 309
    .line 310
    move-result-wide v2

    .line 311
    invoke-static {v8, v2, v3}, Lcom/intsig/camscanner/tsapp/AutoUploadThread;->〇〇8O0〇8(Landroid/content/Context;J)I

    .line 312
    .line 313
    .line 314
    iput-boolean v12, v1, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8:Z

    .line 315
    .line 316
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 317
    .line 318
    .line 319
    move-result-wide v2

    .line 320
    sub-long/2addr v2, v10

    .line 321
    new-instance v0, Ljava/lang/StringBuilder;

    .line 322
    .line 323
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 324
    .line 325
    .line 326
    const-string v4, "appendOnePage consume "

    .line 327
    .line 328
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 329
    .line 330
    .line 331
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 332
    .line 333
    .line 334
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 335
    .line 336
    .line 337
    move-result-object v0

    .line 338
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    .line 340
    .line 341
    return-void

    .line 342
    :catch_0
    move-exception v0

    .line 343
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 344
    .line 345
    .line 346
    return-void
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
.end method

.method public o〇oo(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO0o〇〇:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇o〇Oo88()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    const-string v1, "PageListPresenter"

    .line 15
    .line 16
    const-string v2, "ocrExport"

    .line 17
    .line 18
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    new-instance v1, Ljava/util/ArrayList;

    .line 22
    .line 23
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 27
    .line 28
    .line 29
    move-result-wide v2

    .line 30
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 35
    .line 36
    .line 37
    invoke-static {}, Lcom/intsig/camscanner/control/ShareControl;->o〇8()Lcom/intsig/camscanner/control/ShareControl;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    const/4 v3, 0x0

    .line 42
    invoke-virtual {v2, v0, v1, v3, v3}, Lcom/intsig/camscanner/control/ShareControl;->o〇8oOO88(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Lcom/intsig/utils/SquareShareDialogControl$ShareListener;)V

    .line 43
    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o〇〇0〇88()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8〇0〇o〇O:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇008〇o0〇〇(I)V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 15
    .line 16
    .line 17
    move-result-wide v1

    .line 18
    new-instance v3, L〇OoO0o0/〇00〇8;

    .line 19
    .line 20
    invoke-direct {v3, p0, p1}, L〇OoO0o0/〇00〇8;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;I)V

    .line 21
    .line 22
    .line 23
    new-instance v4, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$4;

    .line 24
    .line 25
    invoke-direct {v4, p0, v0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter$4;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Landroid/app/Activity;I)V

    .line 26
    .line 27
    .line 28
    invoke-static {v0, v1, v2, v3, v4}, Lcom/intsig/camscanner/control/DataChecker;->〇80〇808〇O(Landroid/app/Activity;JLcom/intsig/camscanner/control/DataChecker$ActionListener;Lcom/intsig/camscanner/app/DbWaitingListener;)Z

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇008〇oo()Ljava/lang/String;
    .locals 1
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oo08:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇080O0()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇8o8o〇()Ljava/util/ArrayList;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    if-eqz v1, :cond_2

    .line 23
    .line 24
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    if-nez v2, :cond_1

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_1
    new-instance v2, L〇OoO0o0/Oooo8o0〇;

    .line 32
    .line 33
    invoke-direct {v2, p0, v0}, L〇OoO0o0/Oooo8o0〇;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Landroid/app/Activity;)V

    .line 34
    .line 35
    .line 36
    const/4 v3, 0x0

    .line 37
    invoke-static {v0, v1, v3, v2}, Lcom/intsig/camscanner/control/DataChecker;->〇O00(Landroid/app/Activity;Ljava/util/ArrayList;Ljava/lang/String;Lcom/intsig/camscanner/control/DataChecker$ActionListener;)Z

    .line 38
    .line 39
    .line 40
    goto :goto_1

    .line 41
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 42
    .line 43
    const v1, 0x7f130385

    .line 44
    .line 45
    .line 46
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->〇〇〇0880(I)V

    .line 47
    .line 48
    .line 49
    :goto_1
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇088O(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oOO8O8:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇08O(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oooo8o0〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇08〇0〇o〇8()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇08O8o〇0:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇0O00oO()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oooo8o0〇:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇0O〇O00O(Lcom/intsig/mvp/activity/BaseChangeActivity;Ljava/lang/Long;)V
    .locals 5

    .line 1
    if-eqz p1, :cond_6

    .line 2
    .line 3
    invoke-virtual {p1}, Landroid/app/Activity;->isDestroyed()Z

    .line 4
    .line 5
    .line 6
    move-result p2

    .line 7
    if-eqz p2, :cond_0

    .line 8
    .line 9
    goto :goto_2

    .line 10
    :cond_0
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇8oO:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 11
    .line 12
    if-nez p2, :cond_1

    .line 13
    .line 14
    return-void

    .line 15
    :cond_1
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O000:Landroidx/lifecycle/MutableLiveData;

    .line 16
    .line 17
    invoke-virtual {p2}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object p2

    .line 21
    check-cast p2, Lcom/intsig/camscanner/pagelist/newpagelist/data/PageListRecommendEntity;

    .line 22
    .line 23
    instance-of v0, p2, Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 24
    .line 25
    if-nez v0, :cond_2

    .line 26
    .line 27
    return-void

    .line 28
    :cond_2
    check-cast p2, Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 29
    .line 30
    const-string v0, "PageListPresenter"

    .line 31
    .line 32
    if-nez p2, :cond_3

    .line 33
    .line 34
    const-string p1, "folderItem == null"

    .line 35
    .line 36
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    return-void

    .line 40
    :cond_3
    const-string v1, "moveToRecommendDir"

    .line 41
    .line 42
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    new-instance v0, Ljava/util/ArrayList;

    .line 46
    .line 47
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 48
    .line 49
    .line 50
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇8oO:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 51
    .line 52
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 53
    .line 54
    .line 55
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/FolderItem;->oo88o8O()I

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    const/16 v2, 0x69

    .line 60
    .line 61
    const/4 v3, 0x0

    .line 62
    if-ne v1, v2, :cond_4

    .line 63
    .line 64
    const/4 v1, 0x1

    .line 65
    goto :goto_0

    .line 66
    :cond_4
    const/4 v1, 0x0

    .line 67
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇Oooo〇〇()Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 68
    .line 69
    .line 70
    move-result-object v2

    .line 71
    if-eqz v1, :cond_5

    .line 72
    .line 73
    new-instance v1, Lcom/intsig/camscanner/scenariodir/util/DocMoveAndCopy;

    .line 74
    .line 75
    new-instance v4, L〇OoO0o0/o〇O;

    .line 76
    .line 77
    invoke-direct {v4, p0, v2}, L〇OoO0o0/o〇O;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Lcom/intsig/camscanner/datastruct/FolderItem;)V

    .line 78
    .line 79
    .line 80
    invoke-direct {v1, p1, v3, v4}, Lcom/intsig/camscanner/scenariodir/util/DocMoveAndCopy;-><init>(Lcom/intsig/mvp/activity/BaseChangeActivity;ZLkotlin/jvm/functions/Function2;)V

    .line 81
    .line 82
    .line 83
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Ooo:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 84
    .line 85
    invoke-virtual {v1, v0, p1, v2}, Lcom/intsig/camscanner/scenariodir/util/DocMoveAndCopy;->OoO8(Ljava/util/ArrayList;Lcom/intsig/camscanner/datastruct/FolderItem;Lcom/intsig/camscanner/datastruct/FolderItem;)V

    .line 86
    .line 87
    .line 88
    goto :goto_1

    .line 89
    :cond_5
    new-instance v1, Lcom/intsig/camscanner/scenariodir/util/DocMoveAndCopy;

    .line 90
    .line 91
    new-instance v4, L〇OoO0o0/oO00OOO;

    .line 92
    .line 93
    invoke-direct {v4, p1, p2}, L〇OoO0o0/oO00OOO;-><init>(Lcom/intsig/mvp/activity/BaseChangeActivity;Lcom/intsig/camscanner/datastruct/FolderItem;)V

    .line 94
    .line 95
    .line 96
    invoke-direct {v1, p1, v3, v4}, Lcom/intsig/camscanner/scenariodir/util/DocMoveAndCopy;-><init>(Lcom/intsig/mvp/activity/BaseChangeActivity;ZLkotlin/jvm/functions/Function2;)V

    .line 97
    .line 98
    .line 99
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Ooo:Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 100
    .line 101
    invoke-virtual {v1, v0, p1, v2}, Lcom/intsig/camscanner/scenariodir/util/DocMoveAndCopy;->O8ooOoo〇(Ljava/util/ArrayList;Lcom/intsig/camscanner/datastruct/FolderItem;Lcom/intsig/camscanner/datastruct/FolderItem;)V

    .line 102
    .line 103
    .line 104
    :goto_1
    sget-object p1, Lcom/intsig/camscanner/scenariodir/logagent/ScenarioLogDirAgent;->〇080:Lcom/intsig/camscanner/scenariodir/logagent/ScenarioLogDirAgent;

    .line 105
    .line 106
    invoke-virtual {p2}, Lcom/intsig/camscanner/datastruct/FolderItem;->oo88o8O()I

    .line 107
    .line 108
    .line 109
    move-result p2

    .line 110
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/scenariodir/logagent/ScenarioLogDirAgent;->oO80(I)V

    .line 111
    .line 112
    .line 113
    :cond_6
    :goto_2
    return-void
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public 〇0oO〇oo00(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇080:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇oo〇(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇0ooOOo(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇080:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->oo88o8O(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇0〇0(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇80O8o8O〇()Ljava/lang/String;
    .locals 7
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    sget-object v2, Lcom/intsig/camscanner/provider/Documents$Mtag;->〇080:Landroid/net/Uri;

    .line 10
    .line 11
    const-string v0, "tag_id"

    .line 12
    .line 13
    filled-new-array {v0}, [Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v3

    .line 17
    new-instance v0, Ljava/lang/StringBuilder;

    .line 18
    .line 19
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 20
    .line 21
    .line 22
    const-string v4, "document_id = "

    .line 23
    .line 24
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    iget-wide v4, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 28
    .line 29
    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v4

    .line 36
    const/4 v5, 0x0

    .line 37
    const/4 v6, 0x0

    .line 38
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    const-wide/16 v1, -0x1

    .line 43
    .line 44
    if-eqz v0, :cond_1

    .line 45
    .line 46
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    .line 47
    .line 48
    .line 49
    move-result v3

    .line 50
    if-eqz v3, :cond_0

    .line 51
    .line 52
    const/4 v1, 0x0

    .line 53
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    .line 54
    .line 55
    .line 56
    move-result-wide v1

    .line 57
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 58
    .line 59
    .line 60
    :cond_1
    const-wide/16 v3, 0x0

    .line 61
    .line 62
    cmp-long v0, v1, v3

    .line 63
    .line 64
    if-lez v0, :cond_2

    .line 65
    .line 66
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/app/DBUtil;->〇00O0O0(Landroid/content/Context;J)Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    return-object v0

    .line 75
    :cond_2
    const/4 v0, 0x0

    .line 76
    return-object v0
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public 〇8O0880(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇8O0O808〇()V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    const-string v0, "PageListPresenter"

    .line 9
    .line 10
    const-string v1, "go2OcrExport"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 16
    .line 17
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    const-string v1, "CSList"

    .line 22
    .line 23
    const-string v2, "share_OCR"

    .line 24
    .line 25
    invoke-static {v1, v2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 29
    .line 30
    .line 31
    move-result v1

    .line 32
    if-eqz v1, :cond_1

    .line 33
    .line 34
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 35
    .line 36
    .line 37
    move-result-wide v1

    .line 38
    new-instance v3, L〇OoO0o0/OO8oO0o〇;

    .line 39
    .line 40
    invoke-direct {v3, p0}, L〇OoO0o0/OO8oO0o〇;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 41
    .line 42
    .line 43
    new-instance v4, L〇OoO0o0/〇o00〇〇Oo;

    .line 44
    .line 45
    invoke-direct {v4, p0, v0}, L〇OoO0o0/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Landroid/app/Activity;)V

    .line 46
    .line 47
    .line 48
    invoke-static {v0, v1, v2, v3, v4}, Lcom/intsig/camscanner/control/DataChecker;->〇80〇808〇O(Landroid/app/Activity;JLcom/intsig/camscanner/control/DataChecker$ActionListener;Lcom/intsig/camscanner/app/DbWaitingListener;)Z

    .line 49
    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_1
    sget-object v1, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_OCR_EXPORT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 53
    .line 54
    invoke-static {v0, v1}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->〇〇8O0〇8(Landroid/content/Context;Lcom/intsig/camscanner/purchase/entity/Function;)V

    .line 55
    .line 56
    .line 57
    :goto_0
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇8o()Landroid/net/Uri;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O00:Landroid/net/Uri;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇8o8O〇O()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0〇O0088o:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇8o〇〇8080()Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8O〇:Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇8〇80o(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇8〇o〇8()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇8oOO88:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O0o〇〇o(Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$OnMultipleFunctionResponse;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OOO〇O0:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$OnMultipleFunctionResponse;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇O8〇8000(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8oO〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇O8〇8O0oO()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    const-string v0, "PageListPresenter"

    .line 9
    .line 10
    const-string v1, "User Operation: menu tag"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    new-array v0, v0, [J

    .line 17
    .line 18
    const/4 v1, 0x0

    .line 19
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 20
    .line 21
    .line 22
    move-result-wide v2

    .line 23
    aput-wide v2, v0, v1

    .line 24
    .line 25
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 26
    .line 27
    invoke-interface {v1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getFragment()Landroidx/fragment/app/Fragment;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-virtual {v1}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    if-eqz v1, :cond_1

    .line 36
    .line 37
    invoke-static {v1, v0}, Lcom/intsig/camscanner/mainmenu/tagsetting/TagManagerRouteUtil;->〇080(Landroidx/fragment/app/FragmentActivity;[J)V

    .line 38
    .line 39
    .line 40
    :cond_1
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇OO8Oo0〇()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oo〇:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇Oo〇O(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8〇0〇o〇O:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇Oo〇o8()Lcom/intsig/adapter/RecyclerViewMultiTouchHelper$MultiItemTouchHelperCallback;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o0O0O8:Lcom/intsig/adapter/RecyclerViewMultiTouchHelper$MultiItemTouchHelperCallback;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o08(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8O〇:Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;->〇o00〇〇Oo(Z)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇o8OO0()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O〇:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o8oO()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "PageListPresenter"

    .line 8
    .line 9
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-nez v0, :cond_2

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 18
    .line 19
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getFragment()Landroidx/fragment/app/Fragment;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-static {v0}, Lcom/intsig/camscanner/mainmenu/CsLifecycleUtil;->〇080(Landroidx/lifecycle/LifecycleOwner;)Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-eqz v0, :cond_0

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_0
    iget-wide v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 31
    .line 32
    const-wide/16 v4, -0x1

    .line 33
    .line 34
    cmp-long v0, v2, v4

    .line 35
    .line 36
    if-nez v0, :cond_1

    .line 37
    .line 38
    const-string v0, "mDocId = -1"

    .line 39
    .line 40
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    return-void

    .line 44
    :cond_1
    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    .line 45
    .line 46
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 47
    .line 48
    invoke-interface {v1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    invoke-direct {v0, v1}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;)V

    .line 53
    .line 54
    .line 55
    const-class v1, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 56
    .line 57
    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    check-cast v0, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 62
    .line 63
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8O〇:Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 64
    .line 65
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;->Oooo8o0〇()Landroidx/lifecycle/MutableLiveData;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 70
    .line 71
    invoke-interface {v1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    new-instance v2, L〇OoO0o0/〇80〇808〇O;

    .line 76
    .line 77
    invoke-direct {v2, p0}, L〇OoO0o0/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 78
    .line 79
    .line 80
    invoke-virtual {v0, v1, v2}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 81
    .line 82
    .line 83
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8O〇:Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 84
    .line 85
    iget-wide v1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 86
    .line 87
    const/4 v3, 0x1

    .line 88
    invoke-virtual {v0, v1, v2, v3}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;->〇oo〇(JZ)V

    .line 89
    .line 90
    .line 91
    return-void

    .line 92
    :cond_2
    :goto_0
    const-string v0, "activity == null || activity.isFinishing()"

    .line 93
    .line 94
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    .line 96
    .line 97
    return-void
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public 〇oO8O0〇〇O()V
    .locals 11

    .line 1
    const-string v0, "initVar"

    .line 2
    .line 3
    const-string v1, "PageListPresenter"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O00:Landroid/net/Uri;

    .line 9
    .line 10
    const/4 v2, 0x1

    .line 11
    const/4 v3, 0x0

    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    const-string v0, "Error: DocUri is null"

    .line 15
    .line 16
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    :goto_0
    const/4 v0, 0x1

    .line 20
    goto :goto_1

    .line 21
    :cond_0
    :try_start_0
    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 22
    .line 23
    .line 24
    move-result-wide v4

    .line 25
    iput-wide v4, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 26
    .line 27
    const/4 v0, 0x0

    .line 28
    goto :goto_1

    .line 29
    :catch_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 30
    .line 31
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    .line 33
    .line 34
    const-string v4, "invliad uri "

    .line 35
    .line 36
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    iget-object v4, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O00:Landroid/net/Uri;

    .line 40
    .line 41
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    const/4 v0, 0x0

    .line 52
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O00:Landroid/net/Uri;

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o0()Landroidx/fragment/app/FragmentActivity;

    .line 56
    .line 57
    .line 58
    move-result-object v4

    .line 59
    if-nez v4, :cond_1

    .line 60
    .line 61
    return-void

    .line 62
    :cond_1
    if-nez v0, :cond_3

    .line 63
    .line 64
    iget-wide v5, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 65
    .line 66
    invoke-static {v4, v5, v6}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇o00〇〇Oo(Landroid/content/Context;J)Z

    .line 67
    .line 68
    .line 69
    move-result v5

    .line 70
    if-nez v5, :cond_2

    .line 71
    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    .line 73
    .line 74
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 75
    .line 76
    .line 77
    const-string v5, "doc not exist "

    .line 78
    .line 79
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    iget-object v5, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O00:Landroid/net/Uri;

    .line 83
    .line 84
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    const/4 v0, 0x1

    .line 95
    :cond_2
    iget-wide v5, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 96
    .line 97
    invoke-static {v4, v5, v6}, Lcom/intsig/camscanner/db/dao/DocumentDao;->o〇8oOO88(Landroid/content/Context;J)Z

    .line 98
    .line 99
    .line 100
    move-result v5

    .line 101
    if-nez v5, :cond_3

    .line 102
    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    .line 104
    .line 105
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 106
    .line 107
    .line 108
    const-string v5, "not current account doc "

    .line 109
    .line 110
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    .line 112
    .line 113
    iget-object v5, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O00:Landroid/net/Uri;

    .line 114
    .line 115
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 116
    .line 117
    .line 118
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 119
    .line 120
    .line 121
    move-result-object v0

    .line 122
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    .line 124
    .line 125
    const/4 v0, 0x1

    .line 126
    :cond_3
    if-eqz v0, :cond_4

    .line 127
    .line 128
    const-string v0, "initVar, doc_does_not_exist"

    .line 129
    .line 130
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    .line 132
    .line 133
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 134
    .line 135
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->o88O8()V

    .line 136
    .line 137
    .line 138
    return-void

    .line 139
    :cond_4
    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 140
    .line 141
    .line 142
    move-result-object v0

    .line 143
    const-string v5, "default_open"

    .line 144
    .line 145
    invoke-virtual {v0, v5, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 146
    .line 147
    .line 148
    move-result v0

    .line 149
    if-eqz v0, :cond_6

    .line 150
    .line 151
    iput-boolean v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo〇:Z

    .line 152
    .line 153
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 154
    .line 155
    .line 156
    move-result-object v5

    .line 157
    sget-object v6, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 158
    .line 159
    iget-wide v7, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 160
    .line 161
    invoke-static {v6, v7, v8}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 162
    .line 163
    .line 164
    move-result-object v6

    .line 165
    const-string v7, "belong_state"

    .line 166
    .line 167
    const-string v8, "co_token"

    .line 168
    .line 169
    filled-new-array {v7, v8}, [Ljava/lang/String;

    .line 170
    .line 171
    .line 172
    move-result-object v7

    .line 173
    const/4 v8, 0x0

    .line 174
    const/4 v9, 0x0

    .line 175
    const/4 v10, 0x0

    .line 176
    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 177
    .line 178
    .line 179
    move-result-object v5

    .line 180
    if-eqz v5, :cond_6

    .line 181
    .line 182
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    .line 183
    .line 184
    .line 185
    move-result v6

    .line 186
    if-eqz v6, :cond_5

    .line 187
    .line 188
    invoke-interface {v5, v3}, Landroid/database/Cursor;->getInt(I)I

    .line 189
    .line 190
    .line 191
    move-result v3

    .line 192
    iput v3, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇O8〇〇o:I

    .line 193
    .line 194
    invoke-interface {v5, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 195
    .line 196
    .line 197
    move-result-object v3

    .line 198
    iput-object v3, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇00:Ljava/lang/String;

    .line 199
    .line 200
    :cond_5
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 201
    .line 202
    .line 203
    :cond_6
    new-instance v3, Ljava/lang/StringBuilder;

    .line 204
    .line 205
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 206
    .line 207
    .line 208
    const-string v5, "init var defaultOpen = "

    .line 209
    .line 210
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    .line 212
    .line 213
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 214
    .line 215
    .line 216
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 217
    .line 218
    .line 219
    move-result-object v0

    .line 220
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    .line 222
    .line 223
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OoO8:[Ljava/lang/String;

    .line 224
    .line 225
    if-eqz v0, :cond_8

    .line 226
    .line 227
    array-length v0, v0

    .line 228
    if-lez v0, :cond_8

    .line 229
    .line 230
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇080:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 231
    .line 232
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇〇808〇()Z

    .line 233
    .line 234
    .line 235
    move-result v0

    .line 236
    if-nez v0, :cond_8

    .line 237
    .line 238
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OoO8:[Ljava/lang/String;

    .line 239
    .line 240
    array-length v0, v0

    .line 241
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8oOo80(I)Ljava/lang/String;

    .line 242
    .line 243
    .line 244
    move-result-object v8

    .line 245
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OoO8:[Ljava/lang/String;

    .line 246
    .line 247
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oo0oOo〇0([Ljava/lang/String;)[Ljava/lang/String;

    .line 248
    .line 249
    .line 250
    move-result-object v9

    .line 251
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 252
    .line 253
    .line 254
    move-result-object v5

    .line 255
    iget-wide v3, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇8O0〇8:J

    .line 256
    .line 257
    invoke-static {v3, v4}, Lcom/intsig/camscanner/provider/Documents$Image;->〇080(J)Landroid/net/Uri;

    .line 258
    .line 259
    .line 260
    move-result-object v6

    .line 261
    const-string v0, "_id"

    .line 262
    .line 263
    const-string v3, "page_num"

    .line 264
    .line 265
    filled-new-array {v0, v3}, [Ljava/lang/String;

    .line 266
    .line 267
    .line 268
    move-result-object v7

    .line 269
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8o〇〇0O()Ljava/lang/String;

    .line 270
    .line 271
    .line 272
    move-result-object v10

    .line 273
    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 274
    .line 275
    .line 276
    move-result-object v0

    .line 277
    if-eqz v0, :cond_8

    .line 278
    .line 279
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 280
    .line 281
    .line 282
    move-result v3

    .line 283
    if-eqz v3, :cond_7

    .line 284
    .line 285
    :try_start_1
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    .line 286
    .line 287
    .line 288
    move-result v2

    .line 289
    iput v2, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8ooOoo〇:I

    .line 290
    .line 291
    new-instance v2, Ljava/lang/StringBuilder;

    .line 292
    .line 293
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 294
    .line 295
    .line 296
    const-string v3, "initVar load unfinish: setSelection by search at: "

    .line 297
    .line 298
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    .line 300
    .line 301
    iget v3, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8ooOoo〇:I

    .line 302
    .line 303
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 304
    .line 305
    .line 306
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 307
    .line 308
    .line 309
    move-result-object v2

    .line 310
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 311
    .line 312
    .line 313
    goto :goto_2

    .line 314
    :catch_1
    move-exception v2

    .line 315
    const-string v3, "Exception"

    .line 316
    .line 317
    invoke-static {v1, v3, v2}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 318
    .line 319
    .line 320
    :cond_7
    :goto_2
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 321
    .line 322
    .line 323
    :cond_8
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O880O〇()V

    .line 324
    .line 325
    .line 326
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇08oO80o()V

    .line 327
    .line 328
    .line 329
    return-void
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public 〇oO〇08o(Ljava/lang/String;)V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o()Landroid/net/Uri;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-static {v0, v1}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇8(Landroid/content/Context;Landroid/net/Uri;)I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    new-instance v2, Ljava/lang/StringBuilder;

    .line 23
    .line 24
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 25
    .line 26
    .line 27
    const-string v3, "comfirGo2AutoComposite pageIdCollection="

    .line 28
    .line 29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    const-string v3, " pageNumber="

    .line 36
    .line 37
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v2

    .line 47
    const-string v3, "PageListPresenter"

    .line 48
    .line 49
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    if-lez v1, :cond_1

    .line 53
    .line 54
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8O(Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_1
    const-string p1, " empty doc "

    .line 59
    .line 60
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    const p1, 0x7f130490

    .line 64
    .line 65
    .line 66
    invoke-static {v0, p1}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 67
    .line 68
    .line 69
    :goto_0
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public 〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇080:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇8()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇8:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇88〇8(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇8:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇o〇Oo0()Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇808〇:Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o800o8O:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇〇00OO()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇O8〇〇o:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇00O〇0o()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo0〇O88O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 9
    .line 10
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇8o8o〇()Ljava/util/ArrayList;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    if-eqz v1, :cond_2

    .line 23
    .line 24
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    if-nez v2, :cond_1

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_1
    new-instance v2, L〇OoO0o0/OoO8;

    .line 32
    .line 33
    invoke-direct {v2, p0, v1, v0}, L〇OoO0o0/OoO8;-><init>(Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;Ljava/util/ArrayList;Landroid/app/Activity;)V

    .line 34
    .line 35
    .line 36
    const/4 v3, 0x0

    .line 37
    invoke-static {v0, v1, v3, v2}, Lcom/intsig/camscanner/control/DataChecker;->〇O00(Landroid/app/Activity;Ljava/util/ArrayList;Ljava/lang/String;Lcom/intsig/camscanner/control/DataChecker$ActionListener;)Z

    .line 38
    .line 39
    .line 40
    goto :goto_1

    .line 41
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇888:Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;

    .line 42
    .line 43
    const v1, 0x7f130385

    .line 44
    .line 45
    .line 46
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;->〇〇〇0880(I)V

    .line 47
    .line 48
    .line 49
    :goto_1
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇〇0〇0o8()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O08000:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇O00〇8()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oo88o8O:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇O80〇0o(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oo88o8O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇〇〇0(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8〇o:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇〇〇00(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇80〇808〇O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇〇〇0880()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇0o〇〇()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O8〇OO〇()V

    .line 5
    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇〇()V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇〇O〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O888o0o:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

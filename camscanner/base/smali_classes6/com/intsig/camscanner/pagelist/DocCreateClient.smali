.class public final Lcom/intsig/camscanner/pagelist/DocCreateClient;
.super Ljava/lang/Object;
.source "DocCreateClient.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pagelist/DocCreateClient$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final oO80:Lcom/intsig/camscanner/pagelist/DocCreateClient$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private Oo08:J

.field private o〇0:Landroid/net/Uri;

.field private final 〇080:Landroid/app/Activity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Ljava/lang/String;

.field private 〇o〇:Z

.field private 〇〇888:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pagelist/DocCreateClient$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pagelist/DocCreateClient$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/pagelist/DocCreateClient;->oO80:Lcom/intsig/camscanner/pagelist/DocCreateClient$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Z)V
    .locals 1
    .param p1    # Landroid/app/Activity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/DocCreateClient;->〇080:Landroid/app/Activity;

    .line 10
    .line 11
    iput-object p2, p0, Lcom/intsig/camscanner/pagelist/DocCreateClient;->〇o00〇〇Oo:Ljava/lang/String;

    .line 12
    .line 13
    iput-boolean p3, p0, Lcom/intsig/camscanner/pagelist/DocCreateClient;->〇o〇:Z

    .line 14
    .line 15
    const-string p1, ""

    .line 16
    .line 17
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/DocCreateClient;->O8:Ljava/lang/String;

    .line 18
    .line 19
    const-wide/16 p1, -0x1

    .line 20
    .line 21
    iput-wide p1, p0, Lcom/intsig/camscanner/pagelist/DocCreateClient;->Oo08:J

    .line 22
    .line 23
    iput-wide p1, p0, Lcom/intsig/camscanner/pagelist/DocCreateClient;->〇〇888:J

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final O8(Lcom/intsig/camscanner/pagelist/DocCreateClient;Landroid/net/Uri;Lkotlin/jvm/internal/Ref$LongRef;Ljava/lang/String;Lcom/intsig/camscanner/datastruct/PageProperty;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JILjava/lang/String;)V
    .locals 15

    move-object v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p4

    move-object/from16 v3, p5

    const-string v4, "this$0"

    invoke-static {p0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "$tempDocUri"

    move-object/from16 v5, p1

    invoke-static {v5, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "$start"

    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v4, "$property"

    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    iget-object v4, v0, Lcom/intsig/camscanner/pagelist/DocCreateClient;->o〇0:Landroid/net/Uri;

    if-eqz v4, :cond_0

    iget-wide v6, v0, Lcom/intsig/camscanner/pagelist/DocCreateClient;->Oo08:J

    const-wide/16 v8, -0x1

    cmp-long v4, v6, v8

    if-eqz v4, :cond_0

    .line 2
    sget-object v4, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    invoke-virtual {v4}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    move-result-object v6

    iget-wide v7, v0, Lcom/intsig/camscanner/pagelist/DocCreateClient;->Oo08:J

    invoke-static {v6, v7, v8}, Lcom/intsig/camscanner/db/dao/TagDao;->〇o00〇〇Oo(Landroid/content/Context;J)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 3
    invoke-static/range {p1 .. p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v5

    iget-wide v7, v0, Lcom/intsig/camscanner/pagelist/DocCreateClient;->Oo08:J

    invoke-virtual {v4}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    move-result-object v4

    invoke-static {v5, v6, v7, v8, v4}, Lcom/intsig/camscanner/util/Util;->o88〇OO08〇(JJLandroid/content/Context;)Landroid/net/Uri;

    .line 4
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, v1, Lkotlin/jvm/internal/Ref$LongRef;->element:J

    sub-long/2addr v4, v6

    iput-wide v4, v1, Lkotlin/jvm/internal/Ref$LongRef;->element:J

    .line 5
    iget-wide v6, v0, Lcom/intsig/camscanner/pagelist/DocCreateClient;->Oo08:J

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SCANNER_ACTION_NEW_DOC consume "

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v4, " tag id="

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v4, " imageUUID "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v4, p3

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v5, "DocCreateClient"

    invoke-static {v5, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    iget v1, v2, Lcom/intsig/camscanner/datastruct/PageProperty;->o8o:I

    const/4 v5, 0x0

    const/4 v14, 0x1

    const/16 v6, 0xfa0

    if-ne v6, v1, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    .line 7
    :goto_0
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    move-result-object v6

    invoke-virtual {v6}, Lcom/intsig/tsapp/sync/AppConfigJson;->isShowTag()Z

    move-result v6

    const-string v7, "custom_from_scene_greet_card"

    if-eqz v6, :cond_2

    invoke-static {v3, v7}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 8
    :cond_2
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    move-result-object v6

    invoke-virtual {v6}, Lcom/intsig/tsapp/sync/AppConfigJson;->isImageDiscernTagTest2()Z

    move-result v6

    if-nez v6, :cond_4

    if-eqz v1, :cond_3

    goto :goto_1

    :cond_3
    const/4 v6, 0x0

    goto :goto_2

    :cond_4
    :goto_1
    const/4 v6, 0x1

    :goto_2
    if-eqz v6, :cond_7

    .line 9
    invoke-static {v3, v7}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 10
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f130101

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "ApplicationHelper.sConte\u2026_capture_mode_greet_card)"

    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    iget-wide v6, v0, Lcom/intsig/camscanner/pagelist/DocCreateClient;->〇〇888:J

    invoke-static {v1}, Lcom/intsig/camscanner/app/DBUtil;->〇0O〇Oo(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Lcom/intsig/camscanner/app/DBUtil;->OOo88OOo(JJ)J

    const/4 v3, 0x3

    new-array v3, v3, [Landroid/util/Pair;

    .line 12
    new-instance v6, Landroid/util/Pair;

    const-string v7, "name"

    invoke-direct {v6, v7, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v6, v3, v5

    .line 13
    new-instance v1, Landroid/util/Pair;

    const-string v5, "type"

    const-string v6, "scene_from"

    invoke-direct {v1, v5, v6}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v1, v3, v14

    .line 14
    new-instance v1, Landroid/util/Pair;

    if-eqz p6, :cond_5

    const-string v5, "CSImport"

    goto :goto_3

    :cond_5
    const-string v5, "CSScan"

    :goto_3
    const-string v6, "from_part"

    invoke-direct {v1, v6, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v5, 0x2

    aput-object v1, v3, v5

    const-string v1, "CSNewDoc"

    const-string v5, "select_identified_label"

    .line 15
    invoke-static {v1, v5, v3}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    goto :goto_4

    :cond_6
    if-eqz v1, :cond_7

    .line 16
    iget-wide v5, v0, Lcom/intsig/camscanner/pagelist/DocCreateClient;->〇〇888:J

    .line 17
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f13156a

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "createNewForSingleScan"

    const-string v7, "0CS690TagWritePad20230510"

    .line 18
    invoke-static {v3, v5, v6, v7, v1}, Lcom/intsig/camscanner/app/DBUtil;->o0O〇8o0O(Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V

    .line 19
    :cond_7
    :goto_4
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    move-result-object v5

    move-object/from16 v6, p3

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-wide/from16 v11, p11

    move/from16 v13, p13

    .line 20
    invoke-static/range {v5 .. v13}, Lcom/intsig/camscanner/app/DBUtil;->o88O8(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JI)V

    .line 21
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇800OO〇0O()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 22
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    move-result-object v3

    iget-wide v4, v0, Lcom/intsig/camscanner/pagelist/DocCreateClient;->〇〇888:J

    iget v2, v2, Lcom/intsig/camscanner/datastruct/PageProperty;->o〇00O:I

    invoke-static {v3, v4, v5, v2}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇8〇0〇o〇O(Landroid/content/Context;JI)J

    move-result-wide v2

    .line 23
    iget-wide v4, v0, Lcom/intsig/camscanner/pagelist/DocCreateClient;->〇〇888:J

    invoke-static {v4, v5, v2, v3}, Lcom/intsig/camscanner/util/PreferenceHelper;->oo80O80〇O(JJ)V

    .line 24
    :cond_8
    sget-object v2, Lcom/intsig/camscanner/pagelist/DocCreateClient;->oO80:Lcom/intsig/camscanner/pagelist/DocCreateClient$Companion;

    iget-wide v3, v0, Lcom/intsig/camscanner/pagelist/DocCreateClient;->〇〇888:J

    move-object/from16 v0, p14

    invoke-virtual {v2, v0, v14, v3, v4}, Lcom/intsig/camscanner/pagelist/DocCreateClient$Companion;->〇o00〇〇Oo(Ljava/lang/String;IJ)V

    .line 25
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o〇OOo000(Landroid/content/Context;)V

    return-void
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/pagelist/DocCreateClient;Landroid/net/Uri;Lkotlin/jvm/internal/Ref$LongRef;Ljava/lang/String;Lcom/intsig/camscanner/datastruct/PageProperty;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JILjava/lang/String;)V
    .locals 0

    .line 1
    invoke-static/range {p0 .. p14}, Lcom/intsig/camscanner/pagelist/DocCreateClient;->O8(Lcom/intsig/camscanner/pagelist/DocCreateClient;Landroid/net/Uri;Lkotlin/jvm/internal/Ref$LongRef;Ljava/lang/String;Lcom/intsig/camscanner/datastruct/PageProperty;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JILjava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
.end method

.method public static final 〇80〇808〇O(Landroid/content/Intent;IJ)V
    .locals 1
    .param p0    # Landroid/content/Intent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/DocCreateClient;->oO80:Lcom/intsig/camscanner/pagelist/DocCreateClient$Companion;

    .line 2
    .line 3
    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/intsig/camscanner/pagelist/DocCreateClient$Companion;->〇080(Landroid/content/Intent;IJ)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇o〇(Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;IIIIIILjava/lang/String;JLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IILandroid/net/Uri;ZI)Z
    .locals 32

    move-object/from16 v15, p0

    move-object/from16 v0, p1

    move/from16 v1, p2

    move-object/from16 v2, p16

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    .line 2
    invoke-virtual/range {p30 .. p30}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v14, "DocCreateClient"

    if-nez v3, :cond_0

    const-string v0, "createNewForSingleScan image == null"

    .line 3
    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return v4

    :cond_0
    if-nez v0, :cond_1

    const-string v0, "createNewForSingleScan raw == null"

    .line 4
    invoke-static {v14, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return v4

    .line 5
    :cond_1
    iget-object v4, v15, Lcom/intsig/camscanner/pagelist/DocCreateClient;->〇080:Landroid/app/Activity;

    iget-object v5, v15, Lcom/intsig/camscanner/pagelist/DocCreateClient;->〇o00〇〇Oo:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/intsig/camscanner/capture/scene/CaptureSceneDataExtKt;->〇o00〇〇Oo(Landroid/content/Context;Ljava/lang/String;)Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 6
    new-instance v4, Lcom/intsig/camscanner/eventbus/AutoArchiveEvent;

    iget-object v5, v15, Lcom/intsig/camscanner/pagelist/DocCreateClient;->〇o00〇〇Oo:Ljava/lang/String;

    invoke-direct {v4, v5}, Lcom/intsig/camscanner/eventbus/AutoArchiveEvent;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/intsig/camscanner/eventbus/CsEventBus;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 7
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "createNewForSingleScan docType:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v14, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    invoke-static/range {p3 .. p3}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 9
    invoke-static {v3}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇o(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_2
    move-object/from16 v4, p3

    .line 10
    :goto_0
    new-instance v5, Lcom/intsig/camscanner/datastruct/PageProperty;

    invoke-direct {v5}, Lcom/intsig/camscanner/datastruct/PageProperty;-><init>()V

    .line 11
    iput-object v0, v5, Lcom/intsig/camscanner/datastruct/PageProperty;->OO:Ljava/lang/String;

    .line 12
    iput-object v3, v5, Lcom/intsig/camscanner/datastruct/PageProperty;->〇OOo8〇0:Ljava/lang/String;

    .line 13
    iput-object v4, v5, Lcom/intsig/camscanner/datastruct/PageProperty;->〇08O〇00〇o:Ljava/lang/String;

    const/4 v12, 0x1

    .line 14
    iput v12, v5, Lcom/intsig/camscanner/datastruct/PageProperty;->o〇00O:I

    move-object/from16 v18, p5

    move/from16 v19, p6

    move/from16 v20, p7

    move/from16 v21, p8

    move/from16 v22, p9

    move/from16 v23, p10

    move/from16 v24, p11

    move-object/from16 v25, p12

    move-wide/from16 v26, p13

    move/from16 v28, p28

    move/from16 v29, p29

    move-object/from16 v30, v5

    .line 15
    invoke-static/range {v18 .. v30}, Lcom/intsig/camscanner/app/DBUtil;->〇〇o8(Ljava/lang/String;IIIIIILjava/lang/String;JIILcom/intsig/camscanner/datastruct/PageProperty;)Lcom/intsig/camscanner/datastruct/PageProperty;

    move-object/from16 v4, p15

    .line 16
    iput-object v4, v5, Lcom/intsig/camscanner/datastruct/PageProperty;->O0O:Ljava/lang/String;

    .line 17
    new-instance v3, Lkotlin/jvm/internal/Ref$LongRef;

    invoke-direct {v3}, Lkotlin/jvm/internal/Ref$LongRef;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iput-wide v6, v3, Lkotlin/jvm/internal/Ref$LongRef;->element:J

    if-eqz v2, :cond_3

    .line 18
    invoke-static/range {p16 .. p16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 19
    iput-object v2, v15, Lcom/intsig/camscanner/pagelist/DocCreateClient;->O8:Ljava/lang/String;

    .line 20
    :cond_3
    new-instance v0, Lcom/intsig/camscanner/datastruct/DocProperty;

    iget-object v6, v15, Lcom/intsig/camscanner/pagelist/DocCreateClient;->〇o00〇〇Oo:Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    iget-boolean v9, v15, Lcom/intsig/camscanner/pagelist/DocCreateClient;->〇o〇:Z

    move-object/from16 p5, v0

    move-object/from16 p6, p16

    move-object/from16 p7, v6

    move-object/from16 p8, v7

    move/from16 p9, v8

    move/from16 p10, p2

    move/from16 p11, v9

    invoke-direct/range {p5 .. p11}, Lcom/intsig/camscanner/datastruct/DocProperty;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZIZ)V

    if-eqz p17, :cond_4

    const-string v2, "preset"

    goto :goto_1

    :cond_4
    const-string v2, "non_preset"

    .line 21
    :goto_1
    iput-object v2, v0, Lcom/intsig/camscanner/datastruct/DocProperty;->〇〇808〇:Ljava/lang/String;

    .line 22
    invoke-static/range {p18 .. p18}, Lcom/intsig/camscanner/capture/scene/CaptureSceneDataExtKt;->〇o〇(Ljava/lang/String;)Lcom/intsig/camscanner/capture/scene/CaptureSceneData;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/datastruct/DocProperty;->〇o00〇〇Oo(Lcom/intsig/camscanner/capture/scene/CaptureSceneData;)V

    move/from16 v2, p32

    .line 23
    iput v2, v0, Lcom/intsig/camscanner/datastruct/DocProperty;->〇o00〇〇Oo:I

    .line 24
    iget-object v2, v15, Lcom/intsig/camscanner/pagelist/DocCreateClient;->〇080:Landroid/app/Activity;

    invoke-static {v5}, Lkotlin/collections/CollectionsKt;->O8(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    move/from16 v7, p4

    invoke-static {v2, v6, v7, v0}, Lcom/intsig/camscanner/app/DBUtil;->OoO8(Landroid/content/Context;Ljava/util/List;ZLcom/intsig/camscanner/datastruct/DocProperty;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v15, Lcom/intsig/camscanner/pagelist/DocCreateClient;->o〇0:Landroid/net/Uri;

    packed-switch v1, :pswitch_data_0

    goto :goto_2

    .line 25
    :pswitch_0
    iget-wide v0, v5, Lcom/intsig/camscanner/datastruct/PageProperty;->o0:J

    invoke-static {v0, v1}, Lcom/intsig/camscanner/app/DBUtil;->O0OO8〇0(J)V

    goto :goto_2

    .line 26
    :pswitch_1
    iget-wide v0, v5, Lcom/intsig/camscanner/datastruct/PageProperty;->o0:J

    invoke-static {v0, v1}, Lcom/intsig/camscanner/app/DBUtil;->Oo〇O8o〇8(J)V

    goto :goto_2

    .line 27
    :pswitch_2
    iget-wide v0, v5, Lcom/intsig/camscanner/datastruct/PageProperty;->o0:J

    invoke-static {v0, v1}, Lcom/intsig/camscanner/app/DBUtil;->O0oO008(J)V

    .line 28
    :goto_2
    iget-object v2, v15, Lcom/intsig/camscanner/pagelist/DocCreateClient;->o〇0:Landroid/net/Uri;

    if-eqz v2, :cond_5

    move-wide/from16 v0, p19

    .line 29
    iput-wide v0, v15, Lcom/intsig/camscanner/pagelist/DocCreateClient;->Oo08:J

    .line 30
    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    iput-wide v0, v15, Lcom/intsig/camscanner/pagelist/DocCreateClient;->〇〇888:J

    .line 31
    new-instance v18, Lo0Oo/〇oOO8O8;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    move-object/from16 v4, p15

    move-object/from16 v6, p21

    move/from16 v7, p31

    move-object/from16 v8, p22

    move-object/from16 v9, p23

    move-object/from16 v10, p24

    move-object/from16 v11, p25

    const/16 v19, 0x1

    move-wide/from16 v12, p13

    move-object/from16 v31, v14

    move/from16 v14, p26

    move-object/from16 v15, p27

    invoke-direct/range {v0 .. v15}, Lo0Oo/〇oOO8O8;-><init>(Lcom/intsig/camscanner/pagelist/DocCreateClient;Landroid/net/Uri;Lkotlin/jvm/internal/Ref$LongRef;Ljava/lang/String;Lcom/intsig/camscanner/datastruct/PageProperty;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JILjava/lang/String;)V

    invoke-static/range {v18 .. v18}, Lcom/intsig/thread/ThreadPoolSingleton;->〇080(Ljava/lang/Runnable;)V

    move-object/from16 v1, v31

    goto :goto_3

    :cond_5
    move-object/from16 v31, v14

    const/16 v19, 0x1

    const-string v0, "createNewForSingleScan create doc fail"

    move-object/from16 v1, v31

    .line 32
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    :goto_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v2, v2, v16

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createNewForSingleScan costTime: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return v19

    :pswitch_data_0
    .packed-switch 0x7b
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final Oo08()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pagelist/DocCreateClient;->〇〇888:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getActivity()Landroid/app/Activity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/DocCreateClient;->〇080:Landroid/app/Activity;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oO80()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pagelist/DocCreateClient;->Oo08:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇0()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/DocCreateClient;->O8:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o00〇〇Oo(Landroid/content/Intent;Landroid/net/Uri;Z)Z
    .locals 38
    .param p1    # Landroid/content/Intent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/net/Uri;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    move-object/from16 v1, p0

    .line 4
    .line 5
    move-object/from16 v31, p2

    .line 6
    .line 7
    move/from16 v32, p3

    .line 8
    .line 9
    const-string v2, "intent"

    .line 10
    .line 11
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    const-string v2, "uri"

    .line 15
    .line 16
    move-object/from16 v3, p2

    .line 17
    .line 18
    invoke-static {v3, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    const-string v2, "isCaptureguide"

    .line 22
    .line 23
    const/4 v14, 0x0

    .line 24
    invoke-virtual {v0, v2, v14}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    move/from16 v18, v2

    .line 29
    .line 30
    invoke-static {v2}, Lcom/intsig/camscanner/mainmenu/mainpage/util/MainHomeNoneDocGuideControl;->〇〇8O0〇8(Z)V

    .line 31
    .line 32
    .line 33
    const-string v2, "raw_path"

    .line 34
    .line 35
    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    const-string v3, "extra_doc_type"

    .line 40
    .line 41
    invoke-virtual {v0, v3, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 42
    .line 43
    .line 44
    move-result v3

    .line 45
    const-string v4, "extra_thumb_path"

    .line 46
    .line 47
    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v4

    .line 51
    const-string v5, "issaveready"

    .line 52
    .line 53
    const/4 v6, 0x1

    .line 54
    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 55
    .line 56
    .line 57
    move-result v5

    .line 58
    const-string v6, "imae_crop_borders"

    .line 59
    .line 60
    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v6

    .line 64
    const-string v7, "image_enhance_mode"

    .line 65
    .line 66
    const/4 v8, -0x1

    .line 67
    invoke-virtual {v0, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 68
    .line 69
    .line 70
    move-result v7

    .line 71
    const-string v8, "image_contrast_index"

    .line 72
    .line 73
    invoke-virtual {v0, v8, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 74
    .line 75
    .line 76
    move-result v8

    .line 77
    const-string v9, "image_brightness_index"

    .line 78
    .line 79
    invoke-virtual {v0, v9, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 80
    .line 81
    .line 82
    move-result v9

    .line 83
    const-string v10, "image_detail_index"

    .line 84
    .line 85
    const/16 v11, 0x64

    .line 86
    .line 87
    invoke-virtual {v0, v10, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 88
    .line 89
    .line 90
    move-result v10

    .line 91
    const-string v11, "image_rotation"

    .line 92
    .line 93
    invoke-virtual {v0, v11, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 94
    .line 95
    .line 96
    move-result v11

    .line 97
    const-string v12, "ori_rotation"

    .line 98
    .line 99
    invoke-virtual {v0, v12, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 100
    .line 101
    .line 102
    move-result v12

    .line 103
    const-string v15, "extra_ocr_paragraph"

    .line 104
    .line 105
    invoke-virtual {v0, v15}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object v13

    .line 109
    const-string v14, "extra_ocr_time"

    .line 110
    .line 111
    move-object/from16 v34, v1

    .line 112
    .line 113
    move-object/from16 p3, v2

    .line 114
    .line 115
    const-wide/16 v1, 0x0

    .line 116
    .line 117
    invoke-virtual {v0, v14, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 118
    .line 119
    .line 120
    move-result-wide v1

    .line 121
    move/from16 v35, v3

    .line 122
    .line 123
    move-object v3, v15

    .line 124
    move-wide v14, v1

    .line 125
    const-string v1, "image_sync_id"

    .line 126
    .line 127
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object v16

    .line 131
    const-string v1, "doc_title"

    .line 132
    .line 133
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 134
    .line 135
    .line 136
    move-result-object v17

    .line 137
    const-string v1, "extra_scene_json"

    .line 138
    .line 139
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 140
    .line 141
    .line 142
    move-result-object v19

    .line 143
    const-string v1, "tag_id"

    .line 144
    .line 145
    move-object/from16 v36, v4

    .line 146
    .line 147
    move/from16 v37, v5

    .line 148
    .line 149
    const-wide/16 v4, -0x1

    .line 150
    .line 151
    invoke-virtual {v0, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 152
    .line 153
    .line 154
    move-result-wide v20

    .line 155
    const-string v1, "extra_custom_from_part"

    .line 156
    .line 157
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 158
    .line 159
    .line 160
    move-result-object v22

    .line 161
    const-string v1, "extra_ocr_result"

    .line 162
    .line 163
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 164
    .line 165
    .line 166
    move-result-object v23

    .line 167
    const-string v1, "extra_ocr_user_result"

    .line 168
    .line 169
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 170
    .line 171
    .line 172
    move-result-object v24

    .line 173
    const-string v1, "extra_ocr_file"

    .line 174
    .line 175
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 176
    .line 177
    .line 178
    move-result-object v25

    .line 179
    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 180
    .line 181
    .line 182
    move-result-object v26

    .line 183
    const-string v1, "extra_ocr_mode"

    .line 184
    .line 185
    const/4 v2, 0x0

    .line 186
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 187
    .line 188
    .line 189
    move-result v27

    .line 190
    const-string v1, "intent_image_quality"

    .line 191
    .line 192
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 193
    .line 194
    .line 195
    move-result v30

    .line 196
    const-string v1, "raw_path_copy"

    .line 197
    .line 198
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 199
    .line 200
    .line 201
    move-result-object v28

    .line 202
    const-string v1, "extra_file_type"

    .line 203
    .line 204
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 205
    .line 206
    .line 207
    move-result v29

    .line 208
    const-string v1, "doc_title_source"

    .line 209
    .line 210
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 211
    .line 212
    .line 213
    move-result v33

    .line 214
    move-object/from16 v2, p3

    .line 215
    .line 216
    move-object/from16 v1, v34

    .line 217
    .line 218
    move/from16 v3, v35

    .line 219
    .line 220
    move-object/from16 v4, v36

    .line 221
    .line 222
    move/from16 v5, v37

    .line 223
    .line 224
    invoke-direct/range {v1 .. v33}, Lcom/intsig/camscanner/pagelist/DocCreateClient;->〇o〇(Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;IIIIIILjava/lang/String;JLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IILandroid/net/Uri;ZI)Z

    .line 225
    .line 226
    .line 227
    move-result v0

    .line 228
    return v0
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public final 〇〇888()Landroid/net/Uri;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/DocCreateClient;->o〇0:Landroid/net/Uri;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

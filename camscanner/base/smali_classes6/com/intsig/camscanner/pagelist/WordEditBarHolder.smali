.class public final Lcom/intsig/camscanner/pagelist/WordEditBarHolder;
.super Ljava/lang/Object;
.source "WordEditBarHolder.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pagelist/WordEditBarHolder$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final OO0o〇〇:Lcom/intsig/camscanner/pagelist/WordEditBarHolder$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8:Landroid/view/View;

.field private final OO0o〇〇〇〇0:Landroid/os/Handler;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final Oo08:Lcom/intsig/camscanner/mode_ocr/SoftKeyBoardListener$OnSoftKeyBoardChangeListener;

.field private oO80:Z

.field private o〇0:I

.field private final 〇080:Landroidx/fragment/app/FragmentActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇80〇808〇O:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇8o8o〇:I

.field private final 〇O8o08O:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o〇:Landroid/view/View;

.field private 〇〇888:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->OO0o〇〇:Lcom/intsig/camscanner/pagelist/WordEditBarHolder$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;Landroid/view/View;Landroid/view/View;Lcom/intsig/camscanner/mode_ocr/SoftKeyBoardListener$OnSoftKeyBoardChangeListener;)V
    .locals 1
    .param p1    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "binding"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;

    .line 17
    .line 18
    iput-object p3, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇o〇:Landroid/view/View;

    .line 19
    .line 20
    iput-object p4, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->O8:Landroid/view/View;

    .line 21
    .line 22
    iput-object p5, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->Oo08:Lcom/intsig/camscanner/mode_ocr/SoftKeyBoardListener$OnSoftKeyBoardChangeListener;

    .line 23
    .line 24
    new-instance p2, Landroidx/lifecycle/ViewModelProvider;

    .line 25
    .line 26
    invoke-direct {p2, p1}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;)V

    .line 27
    .line 28
    .line 29
    const-class p1, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 30
    .line 31
    invoke-virtual {p2, p1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    check-cast p1, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 36
    .line 37
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇80〇808〇O:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 38
    .line 39
    new-instance p1, Landroid/os/Handler;

    .line 40
    .line 41
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 42
    .line 43
    .line 44
    move-result-object p2

    .line 45
    invoke-direct {p1, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 46
    .line 47
    .line 48
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->OO0o〇〇〇〇0:Landroid/os/Handler;

    .line 49
    .line 50
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->o〇〇0〇()V

    .line 51
    .line 52
    .line 53
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->OOO〇O0()V

    .line 54
    .line 55
    .line 56
    new-instance p1, Lcom/intsig/camscanner/pagelist/WordEditBarHolder$mBottomBarShowRunnable$1;

    .line 57
    .line 58
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder$mBottomBarShowRunnable$1;-><init>(Lcom/intsig/camscanner/pagelist/WordEditBarHolder;)V

    .line 59
    .line 60
    .line 61
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇O8o08O:Lkotlin/jvm/functions/Function0;

    .line 62
    .line 63
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method public static synthetic O8(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->O8〇o(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final O8〇o(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic OO0o〇〇(Lcom/intsig/camscanner/pagelist/WordEditBarHolder;)Lcom/intsig/camscanner/pic2word/lr/LrViewModel;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇80〇808〇O:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/pagelist/WordEditBarHolder;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->O8:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final OOO〇O0()V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/viewmodel/LrWordConvertHelper;->〇080:Lcom/intsig/camscanner/pagelist/viewmodel/LrWordConvertHelper;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/viewmodel/LrWordConvertHelper;->OO0o〇〇(Z)V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇80〇808〇O:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->o〇0OOo〇0()Landroidx/lifecycle/MutableLiveData;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 14
    .line 15
    new-instance v2, Lcom/intsig/camscanner/pagelist/WordEditBarHolder$initViewModel$1;

    .line 16
    .line 17
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder$initViewModel$1;-><init>(Lcom/intsig/camscanner/pagelist/WordEditBarHolder;)V

    .line 18
    .line 19
    .line 20
    new-instance v3, Lo0Oo/o80ooO;

    .line 21
    .line 22
    invoke-direct {v3, v2}, Lo0Oo/o80ooO;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, v1, v3}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 26
    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇80〇808〇O:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 29
    .line 30
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->〇O00()Landroidx/lifecycle/MutableLiveData;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 35
    .line 36
    new-instance v2, Lcom/intsig/camscanner/pagelist/WordEditBarHolder$initViewModel$2;

    .line 37
    .line 38
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder$initViewModel$2;-><init>(Lcom/intsig/camscanner/pagelist/WordEditBarHolder;)V

    .line 39
    .line 40
    .line 41
    new-instance v3, Lo0Oo/O00;

    .line 42
    .line 43
    invoke-direct {v3, v2}, Lo0Oo/O00;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {v0, v1, v3}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 47
    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇80〇808〇O:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 50
    .line 51
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->Oooo8o0〇()Landroidx/lifecycle/MutableLiveData;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 56
    .line 57
    new-instance v2, Lcom/intsig/camscanner/pagelist/WordEditBarHolder$initViewModel$3;

    .line 58
    .line 59
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder$initViewModel$3;-><init>(Lcom/intsig/camscanner/pagelist/WordEditBarHolder;)V

    .line 60
    .line 61
    .line 62
    new-instance v3, Lo0Oo/o8O0;

    .line 63
    .line 64
    invoke-direct {v3, v2}, Lo0Oo/o8O0;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 65
    .line 66
    .line 67
    invoke-virtual {v0, v1, v3}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 68
    .line 69
    .line 70
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇80〇808〇O:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 71
    .line 72
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->〇oo〇()Landroidx/lifecycle/MutableLiveData;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 77
    .line 78
    new-instance v2, Lcom/intsig/camscanner/pagelist/WordEditBarHolder$initViewModel$4;

    .line 79
    .line 80
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder$initViewModel$4;-><init>(Lcom/intsig/camscanner/pagelist/WordEditBarHolder;)V

    .line 81
    .line 82
    .line 83
    new-instance v3, Lo0Oo/O0o;

    .line 84
    .line 85
    invoke-direct {v3, v2}, Lo0Oo/O0o;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 86
    .line 87
    .line 88
    invoke-virtual {v0, v1, v3}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 89
    .line 90
    .line 91
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇80〇808〇O:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 92
    .line 93
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->oo88o8O()Landroidx/lifecycle/MutableLiveData;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 98
    .line 99
    new-instance v2, Lcom/intsig/camscanner/pagelist/WordEditBarHolder$initViewModel$5;

    .line 100
    .line 101
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder$initViewModel$5;-><init>(Lcom/intsig/camscanner/pagelist/WordEditBarHolder;)V

    .line 102
    .line 103
    .line 104
    new-instance v3, Lo0Oo/O0o〇O0〇;

    .line 105
    .line 106
    invoke-direct {v3, v2}, Lo0Oo/O0o〇O0〇;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 107
    .line 108
    .line 109
    invoke-virtual {v0, v1, v3}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 110
    .line 111
    .line 112
    return-void
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic Oo08(Lkotlin/jvm/functions/Function0;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇O888o0o(Lkotlin/jvm/functions/Function0;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final OoO8()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->OO0o〇〇〇〇0:Landroid/os/Handler;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇O8o08O:Lkotlin/jvm/functions/Function0;

    .line 4
    .line 5
    new-instance v2, Lo0Oo/Oo0oO〇O〇O;

    .line 6
    .line 7
    invoke-direct {v2, v1}, Lo0Oo/Oo0oO〇O〇O;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->OO0o〇〇〇〇0:Landroid/os/Handler;

    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇O8o08O:Lkotlin/jvm/functions/Function0;

    .line 16
    .line 17
    new-instance v2, Lo0Oo/O0oO008;

    .line 18
    .line 19
    invoke-direct {v2, v1}, Lo0Oo/O0oO008;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 20
    .line 21
    .line 22
    const-wide/16 v3, 0xc8

    .line 23
    .line 24
    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic Oooo8o0〇(Lcom/intsig/camscanner/pagelist/WordEditBarHolder;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->o〇0:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final o0ooO(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o8()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;->O8o08O8O:Lcom/intsig/camscanner/pic2word/view/LrTableEditTextView;

    .line 4
    .line 5
    invoke-virtual {v0}, Landroidx/appcompat/widget/AppCompatEditText;->getText()Landroid/text/Editable;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇80〇808〇O:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 14
    .line 15
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->o〇8oOO88(Ljava/lang/CharSequence;)Z

    .line 16
    .line 17
    .line 18
    invoke-static {v0}, Lcom/intsig/utils/KeyboardUtils;->〇〇888(Landroid/view/View;)V

    .line 19
    .line 20
    .line 21
    return-void
.end method

.method private static final o800o8O(Lkotlin/jvm/functions/Function0;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final oO(ZZ)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 4
    .line 5
    const/high16 v1, 0x3f800000    # 1.0f

    .line 6
    .line 7
    const/high16 v2, 0x3f000000    # 0.5f

    .line 8
    .line 9
    if-eqz p1, :cond_0

    .line 10
    .line 11
    const/high16 p1, 0x3f800000    # 1.0f

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/high16 p1, 0x3f000000    # 0.5f

    .line 15
    .line 16
    :goto_0
    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    .line 17
    .line 18
    .line 19
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;

    .line 20
    .line 21
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 22
    .line 23
    if-eqz p2, :cond_1

    .line 24
    .line 25
    goto :goto_1

    .line 26
    :cond_1
    const/high16 v1, 0x3f000000    # 0.5f

    .line 27
    .line 28
    :goto_1
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
.end method

.method public static synthetic oO80(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇00〇8(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final oo88o8O()V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇〇888:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const-string v0, "WordEditBarHolder"

    .line 7
    .line 8
    const-string v1, "click text menu copy"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇o0〇〇()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-nez v0, :cond_1

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 20
    .line 21
    new-instance v1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 22
    .line 23
    sget-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->WORD_COPY:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 24
    .line 25
    sget-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WORD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 26
    .line 27
    invoke-direct {v1, v2, v3}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 28
    .line 29
    .line 30
    const/16 v2, 0x3fb

    .line 31
    .line 32
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->Oooo8o0〇(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;I)V

    .line 33
    .line 34
    .line 35
    return-void

    .line 36
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇80〇808〇O:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 37
    .line 38
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->o0ooO()Landroidx/lifecycle/MutableLiveData;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    sget-object v1, Lcom/intsig/camscanner/pic2word/lr/LrViewModel$TextMenuEvent;->COPY:Lcom/intsig/camscanner/pic2word/lr/LrViewModel$TextMenuEvent;

    .line 43
    .line 44
    invoke-virtual {v0, v1}, Landroidx/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final oo〇(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o〇0(Lkotlin/jvm/functions/Function0;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇0000OOO(Lkotlin/jvm/functions/Function0;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final o〇0OOo〇0(Lcom/intsig/camscanner/pic2word/lr/LrElement;)V
    .locals 2

    .line 1
    instance-of v0, p1, Lcom/intsig/camscanner/pic2word/lr/LrText;

    .line 2
    .line 3
    const-string v1, "WordEditBarHolder"

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    move-object v0, p1

    .line 8
    check-cast v0, Lcom/intsig/camscanner/pic2word/lr/LrText;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrElement;->o800o8O()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    const-string p1, "lrText focus"

    .line 17
    .line 18
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    const/4 p1, 0x1

    .line 22
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇〇〇0〇〇0(I)V

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    instance-of v0, p1, Lcom/intsig/camscanner/pic2word/lr/LrTable;

    .line 27
    .line 28
    if-eqz v0, :cond_1

    .line 29
    .line 30
    check-cast p1, Lcom/intsig/camscanner/pic2word/lr/LrTable;

    .line 31
    .line 32
    invoke-virtual {p1}, Lcom/intsig/camscanner/pic2word/lr/LrTable;->oO()Z

    .line 33
    .line 34
    .line 35
    move-result p1

    .line 36
    if-eqz p1, :cond_1

    .line 37
    .line 38
    const-string p1, "lrTable focus"

    .line 39
    .line 40
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    const/4 p1, 0x2

    .line 44
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇〇〇0〇〇0(I)V

    .line 45
    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_1
    const-string p1, "layout default"

    .line 49
    .line 50
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    const/4 p1, 0x0

    .line 54
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇〇〇0〇〇0(I)V

    .line 55
    .line 56
    .line 57
    :goto_0
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final o〇O8〇〇o()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->oO80:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const-string v0, "click text menu paste"

    .line 7
    .line 8
    const-string v1, "WordEditBarHolder"

    .line 9
    .line 10
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 14
    .line 15
    invoke-static {v0}, Lcom/intsig/camscanner/app/AppUtil;->o〇8(Landroid/content/Context;)Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    const-string v0, "paste text is empty, return!!"

    .line 26
    .line 27
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    return-void

    .line 31
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇80〇808〇O:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 32
    .line 33
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->o0ooO()Landroidx/lifecycle/MutableLiveData;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    sget-object v1, Lcom/intsig/camscanner/pic2word/lr/LrViewModel$TextMenuEvent;->PASTE:Lcom/intsig/camscanner/pic2word/lr/LrViewModel$TextMenuEvent;

    .line 38
    .line 39
    invoke-virtual {v0, v1}, Landroidx/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final o〇〇0〇()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder$initKeyboardListener$listener$1;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder$initKeyboardListener$listener$1;-><init>(Lcom/intsig/camscanner/pagelist/WordEditBarHolder;)V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 7
    .line 8
    invoke-static {v1, v0}, Lcom/intsig/camscanner/mode_ocr/SoftKeyBoardListener;->〇o〇(Landroid/app/Activity;Lcom/intsig/camscanner/mode_ocr/SoftKeyBoardListener$OnSoftKeyBoardChangeListener;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final 〇0000OOO(Lkotlin/jvm/functions/Function0;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇00〇8(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇080(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->o0ooO(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇08O8o〇0(Z)V
    .locals 2

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇〇888:Z

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;

    .line 4
    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 8
    .line 9
    const/high16 v1, 0x3f800000    # 1.0f

    .line 10
    .line 11
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 12
    .line 13
    .line 14
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;->o〇00O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 15
    .line 16
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 17
    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 21
    .line 22
    const v1, 0x3e99999a    # 0.3f

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 26
    .line 27
    .line 28
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;->o〇00O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 29
    .line 30
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 31
    .line 32
    .line 33
    :goto_0
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic 〇0〇O0088o(Lcom/intsig/camscanner/pagelist/WordEditBarHolder;ZZ)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->oO(ZZ)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic 〇80〇808〇O(Lcom/intsig/camscanner/pagelist/WordEditBarHolder;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇o〇:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇8o8o〇(Lcom/intsig/camscanner/pagelist/WordEditBarHolder;)Lcom/intsig/camscanner/mode_ocr/SoftKeyBoardListener$OnSoftKeyBoardChangeListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->Oo08:Lcom/intsig/camscanner/mode_ocr/SoftKeyBoardListener$OnSoftKeyBoardChangeListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇O00(Lcom/intsig/camscanner/pagelist/WordEditBarHolder;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇〇0o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇O888o0o(Lkotlin/jvm/functions/Function0;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇O8o08O(Lcom/intsig/camscanner/pagelist/WordEditBarHolder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->o〇0:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇O〇(Lcom/intsig/camscanner/pagelist/WordEditBarHolder;Lcom/intsig/camscanner/pic2word/lr/LrElement;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->o〇0OOo〇0(Lcom/intsig/camscanner/pic2word/lr/LrElement;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇o(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇o00〇〇Oo(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->oo〇(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇oOO8O8()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->OO0o〇〇〇〇0:Landroid/os/Handler;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇O8o08O:Lkotlin/jvm/functions/Function0;

    .line 4
    .line 5
    new-instance v2, Lo0Oo/o0O〇8o0O;

    .line 6
    .line 7
    invoke-direct {v2, v1}, Lo0Oo/o0O〇8o0O;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->O8:Landroid/view/View;

    .line 14
    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/16 v1, 0x8

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 21
    .line 22
    .line 23
    :goto_0
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇oo〇()V
    .locals 4

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇〇888:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const-string v0, "WordEditBarHolder"

    .line 7
    .line 8
    const-string v1, "click text menu cut"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇o0〇〇()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-nez v0, :cond_1

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 20
    .line 21
    new-instance v1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 22
    .line 23
    sget-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->WORD_CUT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 24
    .line 25
    sget-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WORD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 26
    .line 27
    invoke-direct {v1, v2, v3}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 28
    .line 29
    .line 30
    const/16 v2, 0x3fb

    .line 31
    .line 32
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->Oooo8o0〇(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;I)V

    .line 33
    .line 34
    .line 35
    return-void

    .line 36
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇80〇808〇O:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 37
    .line 38
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->o0ooO()Landroidx/lifecycle/MutableLiveData;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    sget-object v1, Lcom/intsig/camscanner/pic2word/lr/LrViewModel$TextMenuEvent;->CUT:Lcom/intsig/camscanner/pic2word/lr/LrViewModel$TextMenuEvent;

    .line 43
    .line 44
    invoke-virtual {v0, v1}, Landroidx/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇o〇(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇o(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇〇0o()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/app/AppUtil;->o〇8(Landroid/content/Context;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    xor-int/lit8 v0, v0, 0x1

    .line 12
    .line 13
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->oO80:Z

    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;

    .line 16
    .line 17
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;->o8〇OO0〇0o:Landroid/widget/TextView;

    .line 18
    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    const/high16 v0, 0x3f800000    # 1.0f

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const v0, 0x3e99999a    # 0.3f

    .line 25
    .line 26
    .line 27
    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic 〇〇808〇(Lcom/intsig/camscanner/pagelist/WordEditBarHolder;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇〇〇0〇〇0(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇〇888(Lkotlin/jvm/functions/Function0;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->o800o8O(Lkotlin/jvm/functions/Function0;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇〇8O0〇8(Lcom/intsig/camscanner/pagelist/WordEditBarHolder;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇08O8o〇0(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇〇〇0〇〇0(I)V
    .locals 3

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇8o8o〇:I

    .line 2
    .line 3
    new-instance v0, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v1, "layoutType:"

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "WordEditBarHolder"

    .line 21
    .line 22
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    const/4 v0, 0x1

    .line 26
    const/4 v1, 0x0

    .line 27
    const/16 v2, 0x8

    .line 28
    .line 29
    if-eq p1, v0, :cond_1

    .line 30
    .line 31
    const/4 v0, 0x2

    .line 32
    if-eq p1, v0, :cond_0

    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;

    .line 35
    .line 36
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;->getRoot()Landroid/view/View;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 41
    .line 42
    .line 43
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;->〇0O:Landroid/widget/LinearLayout;

    .line 44
    .line 45
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 46
    .line 47
    .line 48
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;->oOo〇8o008:Landroid/widget/LinearLayout;

    .line 49
    .line 50
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 51
    .line 52
    .line 53
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->OoO8()V

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;

    .line 58
    .line 59
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;->getRoot()Landroid/view/View;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 64
    .line 65
    .line 66
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;->〇0O:Landroid/widget/LinearLayout;

    .line 67
    .line 68
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 69
    .line 70
    .line 71
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;->oOo〇8o008:Landroid/widget/LinearLayout;

    .line 72
    .line 73
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 74
    .line 75
    .line 76
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇oOO8O8()V

    .line 77
    .line 78
    .line 79
    goto :goto_0

    .line 80
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;

    .line 81
    .line 82
    invoke-virtual {p1}, Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;->getRoot()Landroid/view/View;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 87
    .line 88
    .line 89
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;->〇0O:Landroid/widget/LinearLayout;

    .line 90
    .line 91
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 92
    .line 93
    .line 94
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;->oOo〇8o008:Landroid/widget/LinearLayout;

    .line 95
    .line 96
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 97
    .line 98
    .line 99
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇oOO8O8()V

    .line 100
    .line 101
    .line 102
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇〇0o()V

    .line 103
    .line 104
    .line 105
    :goto_0
    return-void
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method


# virtual methods
.method public final O8ooOoo〇()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇8o8o〇:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final Oo8Oo00oo(Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "listener"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;

    .line 7
    .line 8
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 9
    .line 10
    invoke-virtual {v1, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 11
    .line 12
    .line 13
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 14
    .line 15
    invoke-virtual {v1, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 16
    .line 17
    .line 18
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 19
    .line 20
    invoke-virtual {v1, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 21
    .line 22
    .line 23
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 24
    .line 25
    invoke-virtual {v1, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 26
    .line 27
    .line 28
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;->o〇00O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 29
    .line 30
    invoke-virtual {v1, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 31
    .line 32
    .line 33
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;->o8〇OO0〇0o:Landroid/widget/TextView;

    .line 34
    .line 35
    invoke-virtual {v1, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    .line 37
    .line 38
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 39
    .line 40
    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final O〇8O8〇008()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->o〇0:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getActivity()Landroidx/fragment/app/FragmentActivity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇080:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇8(Landroid/view/View;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "v"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    sparse-switch p1, :sswitch_data_0

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :sswitch_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->o〇O8〇〇o()V

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :sswitch_1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->o8()V

    .line 19
    .line 20
    .line 21
    goto :goto_0

    .line 22
    :sswitch_2
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇oo〇()V

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :sswitch_3
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->oo88o8O()V

    .line 27
    .line 28
    .line 29
    goto :goto_0

    .line 30
    :sswitch_4
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇80〇808〇O:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 31
    .line 32
    invoke-virtual {p1}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->o〇0OOo〇0()Landroidx/lifecycle/MutableLiveData;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    invoke-virtual {p1}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    check-cast p1, Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 41
    .line 42
    if-eqz p1, :cond_0

    .line 43
    .line 44
    invoke-virtual {p1}, Lcom/intsig/camscanner/pic2word/lr/LrView;->oO80()Z

    .line 45
    .line 46
    .line 47
    move-result v0

    .line 48
    if-eqz v0, :cond_0

    .line 49
    .line 50
    invoke-virtual {p1}, Lcom/intsig/camscanner/pic2word/lr/LrView;->o8()V

    .line 51
    .line 52
    .line 53
    goto :goto_0

    .line 54
    :sswitch_5
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇80〇808〇O:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 55
    .line 56
    invoke-virtual {p1}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->o〇0OOo〇0()Landroidx/lifecycle/MutableLiveData;

    .line 57
    .line 58
    .line 59
    move-result-object p1

    .line 60
    invoke-virtual {p1}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    move-result-object p1

    .line 64
    check-cast p1, Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 65
    .line 66
    if-eqz p1, :cond_0

    .line 67
    .line 68
    invoke-virtual {p1}, Lcom/intsig/camscanner/pic2word/lr/LrView;->〇80〇808〇O()Z

    .line 69
    .line 70
    .line 71
    move-result v0

    .line 72
    if-eqz v0, :cond_0

    .line 73
    .line 74
    invoke-virtual {p1}, Lcom/intsig/camscanner/pic2word/lr/LrView;->o〇8oOO88()V

    .line 75
    .line 76
    .line 77
    :cond_0
    :goto_0
    return-void

    .line 78
    nop

    .line 79
    :sswitch_data_0
    .sparse-switch
        0x7f0a00d6 -> :sswitch_5
        0x7f0a00ee -> :sswitch_4
        0x7f0a0386 -> :sswitch_3
        0x7f0a0389 -> :sswitch_2
        0x7f0a07da -> :sswitch_1
        0x7f0a1681 -> :sswitch_0
    .end sparse-switch
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public final 〇00()Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/WordEditBarHolder;->〇o00〇〇Oo:Lcom/intsig/camscanner/databinding/IncludeWordEditBarBinding;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

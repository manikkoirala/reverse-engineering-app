.class public final Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;
.super Ljava/lang/Object;
.source "PageListTagTipsWindow.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇8o8o〇:Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇O8o08O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8:Landroid/widget/PopupWindow;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO0o〇〇〇〇0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final Oo08:Landroidx/constraintlayout/widget/ConstraintLayout;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oO80:I

.field private final o〇0:Landroid/widget/TextView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇080:Landroid/view/View;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇80〇808〇O:I

.field private final 〇o00〇〇Oo:J

.field private final 〇o〇:Lcom/intsig/camscanner/pagelist/PageListFragment;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇〇888:Landroid/widget/ImageView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇8o8o〇:Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow$Companion;

    .line 8
    .line 9
    const-class v0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "PageListTagTipsWindow::class.java.simpleName"

    .line 16
    .line 17
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇O8o08O:Ljava/lang/String;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>(Landroid/view/View;JLcom/intsig/camscanner/pagelist/PageListFragment;)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lcom/intsig/camscanner/pagelist/PageListFragment;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "anchorView"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "fragment"

    .line 7
    .line 8
    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇080:Landroid/view/View;

    .line 15
    .line 16
    iput-wide p2, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇o00〇〇Oo:J

    .line 17
    .line 18
    iput-object p4, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇o〇:Lcom/intsig/camscanner/pagelist/PageListFragment;

    .line 19
    .line 20
    const/high16 p2, 0x42000000    # 32.0f

    .line 21
    .line 22
    invoke-static {p2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 23
    .line 24
    .line 25
    move-result p2

    .line 26
    iput p2, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->oO80:I

    .line 27
    .line 28
    const/high16 p3, 0x41800000    # 16.0f

    .line 29
    .line 30
    invoke-static {p3}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 31
    .line 32
    .line 33
    move-result p3

    .line 34
    iput p3, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇80〇808〇O:I

    .line 35
    .line 36
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    invoke-static {}, Lcom/intsig/camscanner/util/StringUtil;->o〇O8〇〇o()Z

    .line 41
    .line 42
    .line 43
    move-result p3

    .line 44
    if-eqz p3, :cond_0

    .line 45
    .line 46
    invoke-static {p1}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 47
    .line 48
    .line 49
    move-result p3

    .line 50
    iget v0, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇80〇808〇O:I

    .line 51
    .line 52
    sub-int/2addr p3, v0

    .line 53
    const/high16 v0, 0x43960000    # 300.0f

    .line 54
    .line 55
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 56
    .line 57
    .line 58
    move-result v0

    .line 59
    add-int/2addr p3, v0

    .line 60
    iput p3, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇80〇808〇O:I

    .line 61
    .line 62
    :cond_0
    const p3, 0x7f0d06ed

    .line 63
    .line 64
    .line 65
    const/4 v0, 0x0

    .line 66
    invoke-static {p1, p3, v0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 67
    .line 68
    .line 69
    move-result-object p1

    .line 70
    const-string p3, "null cannot be cast to non-null type androidx.constraintlayout.widget.ConstraintLayout"

    .line 71
    .line 72
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    check-cast p1, Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 76
    .line 77
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->Oo08:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 78
    .line 79
    new-instance p3, Landroid/widget/PopupWindow;

    .line 80
    .line 81
    const/4 v0, -0x2

    .line 82
    const/4 v1, 0x0

    .line 83
    invoke-direct {p3, p1, v0, p2, v1}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    .line 84
    .line 85
    .line 86
    iput-object p3, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->O8:Landroid/widget/PopupWindow;

    .line 87
    .line 88
    invoke-virtual {p3, v1}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 89
    .line 90
    .line 91
    new-instance p2, Landroid/graphics/drawable/ColorDrawable;

    .line 92
    .line 93
    invoke-direct {p2}, Landroid/graphics/drawable/ColorDrawable;-><init>()V

    .line 94
    .line 95
    .line 96
    invoke-virtual {p3, p2}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 97
    .line 98
    .line 99
    const p2, 0x7f0a1259

    .line 100
    .line 101
    .line 102
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 103
    .line 104
    .line 105
    move-result-object p2

    .line 106
    const-string p3, "contentView.findViewById(R.id.tv_add_tag)"

    .line 107
    .line 108
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 109
    .line 110
    .line 111
    check-cast p2, Landroid/widget/TextView;

    .line 112
    .line 113
    iput-object p2, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->o〇0:Landroid/widget/TextView;

    .line 114
    .line 115
    const p2, 0x7f0a0839

    .line 116
    .line 117
    .line 118
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 119
    .line 120
    .line 121
    move-result-object p1

    .line 122
    const-string p2, "contentView.findViewById(R.id.iv_add_tag)"

    .line 123
    .line 124
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    .line 126
    .line 127
    check-cast p1, Landroid/widget/ImageView;

    .line 128
    .line 129
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇〇888:Landroid/widget/ImageView;

    .line 130
    .line 131
    invoke-virtual {p4}, Landroidx/fragment/app/Fragment;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 132
    .line 133
    .line 134
    move-result-object p1

    .line 135
    new-instance p2, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow$1;

    .line 136
    .line 137
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow$1;-><init>(Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;)V

    .line 138
    .line 139
    .line 140
    invoke-virtual {p1, p2}, Landroidx/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 141
    .line 142
    .line 143
    const-string p1, "unlabeled"

    .line 144
    .line 145
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 146
    .line 147
    return-void
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public static final synthetic O8()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇O8o08O:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final OO0o〇〇〇〇0(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Pair<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow$getDocFirstTagTitle$2;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-direct {v1, p1, p2, v2}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow$getDocFirstTagTitle$2;-><init>(JLkotlin/coroutines/Continuation;)V

    .line 9
    .line 10
    .line 11
    invoke-static {v0, v1, p3}, Lkotlinx/coroutines/BuildersKt;->Oo08(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    return-object p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic Oo08(Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->oO80:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final Oooo8o0〇(Ljava/lang/String;)V
    .locals 2

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    invoke-static {p1}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    goto :goto_1

    .line 12
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 13
    :goto_1
    if-eqz v0, :cond_2

    .line 14
    .line 15
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->o〇0:Landroid/widget/TextView;

    .line 16
    .line 17
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    const v1, 0x7f1319f1

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 31
    .line 32
    .line 33
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇〇888:Landroid/widget/ImageView;

    .line 34
    .line 35
    const v0, 0x7f0804d7

    .line 36
    .line 37
    .line 38
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 39
    .line 40
    .line 41
    const-string p1, "unlabeled"

    .line 42
    .line 43
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 44
    .line 45
    goto :goto_2

    .line 46
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->o〇0:Landroid/widget/TextView;

    .line 47
    .line 48
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 49
    .line 50
    .line 51
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇〇888:Landroid/widget/ImageView;

    .line 52
    .line 53
    const v0, 0x7f080ba3

    .line 54
    .line 55
    .line 56
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 57
    .line 58
    .line 59
    const-string p1, "labeled"

    .line 60
    .line 61
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 62
    .line 63
    :goto_2
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic o〇0(Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;)Landroid/widget/TextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->o〇0:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇080(Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;JLkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->OO0o〇〇〇〇0(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;)Landroid/widget/ImageView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇〇888:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇80〇808〇O:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇〇888(Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->Oooo8o0〇(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public final OO0o〇〇(Landroid/content/Context;ZZ)V
    .locals 7
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string p3, "context"

    .line 2
    .line 3
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    instance-of p3, p1, Landroidx/lifecycle/LifecycleOwner;

    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    if-eqz p3, :cond_0

    .line 10
    .line 11
    check-cast p1, Landroidx/lifecycle/LifecycleOwner;

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    move-object p1, v0

    .line 15
    :goto_0
    if-eqz p1, :cond_1

    .line 16
    .line 17
    invoke-static {p1}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    if-eqz v1, :cond_1

    .line 22
    .line 23
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o〇()Lkotlinx/coroutines/MainCoroutineDispatcher;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    const/4 v3, 0x0

    .line 28
    new-instance v4, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow$searchAndShowTag$1;

    .line 29
    .line 30
    invoke-direct {v4, p0, p2, v0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow$searchAndShowTag$1;-><init>(Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;ZLkotlin/coroutines/Continuation;)V

    .line 31
    .line 32
    .line 33
    const/4 v5, 0x2

    .line 34
    const/4 v6, 0x0

    .line 35
    invoke-static/range {v1 .. v6}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 36
    .line 37
    .line 38
    :cond_1
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public final getType()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final oO80()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->O8:Landroid/widget/PopupWindow;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇80〇808〇O()Landroid/view/View;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇080:Landroid/view/View;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇8o8o〇()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇o00〇〇Oo:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇O8o08O()Landroid/widget/PopupWindow;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->O8:Landroid/widget/PopupWindow;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇O〇(ZLandroidx/fragment/app/FragmentActivity;Z)V
    .locals 8
    .param p2    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇o〇:Lcom/intsig/camscanner/pagelist/PageListFragment;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇08〇0oo0()Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO88〇OOO()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-nez v0, :cond_1

    .line 17
    .line 18
    sget-object p1, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇O8o08O:Ljava/lang/String;

    .line 19
    .line 20
    const-string p2, "do not show it"

    .line 21
    .line 22
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->O8:Landroid/widget/PopupWindow;

    .line 26
    .line 27
    invoke-virtual {p1}, Landroid/widget/PopupWindow;->isShowing()Z

    .line 28
    .line 29
    .line 30
    move-result p1

    .line 31
    if-eqz p1, :cond_0

    .line 32
    .line 33
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->O8:Landroid/widget/PopupWindow;

    .line 34
    .line 35
    invoke-virtual {p1}, Landroid/widget/PopupWindow;->dismiss()V

    .line 36
    .line 37
    .line 38
    :cond_0
    return-void

    .line 39
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    invoke-virtual {v0}, Lcom/intsig/tsapp/sync/AppConfigJson;->isImageDiscernTagOpen()Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    if-nez v0, :cond_2

    .line 48
    .line 49
    return-void

    .line 50
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇080:Landroid/view/View;

    .line 51
    .line 52
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 53
    .line 54
    .line 55
    move-result-object v3

    .line 56
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇080:Landroid/view/View;

    .line 57
    .line 58
    invoke-static {v0}, Landroidx/core/view/ViewCompat;->isLaidOut(Landroid/view/View;)Z

    .line 59
    .line 60
    .line 61
    move-result v1

    .line 62
    if-eqz v1, :cond_a

    .line 63
    .line 64
    invoke-virtual {v0}, Landroid/view/View;->isLayoutRequested()Z

    .line 65
    .line 66
    .line 67
    move-result v1

    .line 68
    if-nez v1, :cond_a

    .line 69
    .line 70
    const/4 v0, 0x2

    .line 71
    new-array v1, v0, [I

    .line 72
    .line 73
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇80〇808〇O()Landroid/view/View;

    .line 74
    .line 75
    .line 76
    move-result-object v2

    .line 77
    invoke-virtual {v2, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 78
    .line 79
    .line 80
    const/4 v2, 0x1

    .line 81
    aget v1, v1, v2

    .line 82
    .line 83
    invoke-static {}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->O8()Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object v2

    .line 87
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇80〇808〇O()Landroid/view/View;

    .line 88
    .line 89
    .line 90
    move-result-object v4

    .line 91
    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    .line 92
    .line 93
    .line 94
    move-result v4

    .line 95
    invoke-static {p0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇o〇(Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;)I

    .line 96
    .line 97
    .line 98
    move-result v5

    .line 99
    new-instance v6, Ljava/lang/StringBuilder;

    .line 100
    .line 101
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 102
    .line 103
    .line 104
    const-string v7, "anchor y = "

    .line 105
    .line 106
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    .line 108
    .line 109
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 110
    .line 111
    .line 112
    const-string v7, " , anchor height = "

    .line 113
    .line 114
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 118
    .line 119
    .line 120
    const-string v4, ", marginStart = "

    .line 121
    .line 122
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 123
    .line 124
    .line 125
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 126
    .line 127
    .line 128
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 129
    .line 130
    .line 131
    move-result-object v4

    .line 132
    invoke-static {v2, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    .line 134
    .line 135
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇80〇808〇O()Landroid/view/View;

    .line 136
    .line 137
    .line 138
    move-result-object v2

    .line 139
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    .line 140
    .line 141
    .line 142
    move-result v2

    .line 143
    const/high16 v4, 0x42000000    # 32.0f

    .line 144
    .line 145
    invoke-static {v4}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 146
    .line 147
    .line 148
    move-result v4

    .line 149
    sub-int/2addr v2, v4

    .line 150
    div-int/2addr v2, v0

    .line 151
    add-int/2addr v1, v2

    .line 152
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇O8o08O()Landroid/widget/PopupWindow;

    .line 153
    .line 154
    .line 155
    move-result-object v2

    .line 156
    invoke-virtual {v2}, Landroid/widget/PopupWindow;->isShowing()Z

    .line 157
    .line 158
    .line 159
    move-result v2

    .line 160
    if-eqz v2, :cond_3

    .line 161
    .line 162
    invoke-static {}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->O8()Ljava/lang/String;

    .line 163
    .line 164
    .line 165
    move-result-object p2

    .line 166
    new-instance v0, Ljava/lang/StringBuilder;

    .line 167
    .line 168
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 169
    .line 170
    .line 171
    const-string v2, "update : "

    .line 172
    .line 173
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    .line 175
    .line 176
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 177
    .line 178
    .line 179
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 180
    .line 181
    .line 182
    move-result-object v0

    .line 183
    invoke-static {p2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    .line 185
    .line 186
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇O8o08O()Landroid/widget/PopupWindow;

    .line 187
    .line 188
    .line 189
    move-result-object p2

    .line 190
    invoke-static {p0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇o〇(Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;)I

    .line 191
    .line 192
    .line 193
    move-result v0

    .line 194
    const/4 v2, -0x2

    .line 195
    invoke-static {p0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->Oo08(Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;)I

    .line 196
    .line 197
    .line 198
    move-result v4

    .line 199
    invoke-virtual {p2, v0, v1, v2, v4}, Landroid/widget/PopupWindow;->update(IIII)V

    .line 200
    .line 201
    .line 202
    goto/16 :goto_3

    .line 203
    .line 204
    :cond_3
    invoke-static {}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->O8()Ljava/lang/String;

    .line 205
    .line 206
    .line 207
    move-result-object v2

    .line 208
    new-instance v4, Ljava/lang/StringBuilder;

    .line 209
    .line 210
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 211
    .line 212
    .line 213
    const-string v5, "show : "

    .line 214
    .line 215
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    .line 217
    .line 218
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 219
    .line 220
    .line 221
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 222
    .line 223
    .line 224
    move-result-object v4

    .line 225
    invoke-static {v2, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    .line 227
    .line 228
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇O8o08O()Landroid/widget/PopupWindow;

    .line 229
    .line 230
    .line 231
    move-result-object v2

    .line 232
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇80〇808〇O()Landroid/view/View;

    .line 233
    .line 234
    .line 235
    move-result-object v4

    .line 236
    invoke-static {p0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇o〇(Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;)I

    .line 237
    .line 238
    .line 239
    move-result v5

    .line 240
    const/4 v6, 0x0

    .line 241
    invoke-virtual {v2, v4, v6, v5, v1}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 242
    .line 243
    .line 244
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇O8o08O()Landroid/widget/PopupWindow;

    .line 245
    .line 246
    .line 247
    move-result-object v1

    .line 248
    invoke-virtual {v1}, Landroid/widget/PopupWindow;->getContentView()Landroid/view/View;

    .line 249
    .line 250
    .line 251
    move-result-object v1

    .line 252
    new-instance v2, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow$showOrUpdate$1$1;

    .line 253
    .line 254
    invoke-direct {v2, p2, p0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow$showOrUpdate$1$1;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;)V

    .line 255
    .line 256
    .line 257
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 258
    .line 259
    .line 260
    invoke-static {}, Lcom/intsig/camscanner/util/StringUtil;->o〇O8〇〇o()Z

    .line 261
    .line 262
    .line 263
    move-result p2

    .line 264
    if-nez p2, :cond_9

    .line 265
    .line 266
    invoke-static {p0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->o〇0(Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;)Landroid/widget/TextView;

    .line 267
    .line 268
    .line 269
    move-result-object p2

    .line 270
    invoke-static {p2}, Landroidx/core/view/ViewCompat;->isLaidOut(Landroid/view/View;)Z

    .line 271
    .line 272
    .line 273
    move-result v1

    .line 274
    if-eqz v1, :cond_8

    .line 275
    .line 276
    invoke-virtual {p2}, Landroid/view/View;->isLayoutRequested()Z

    .line 277
    .line 278
    .line 279
    move-result v1

    .line 280
    if-nez v1, :cond_8

    .line 281
    .line 282
    invoke-static {p0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->o〇0(Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;)Landroid/widget/TextView;

    .line 283
    .line 284
    .line 285
    move-result-object p2

    .line 286
    invoke-static {v3}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 287
    .line 288
    .line 289
    move-result v1

    .line 290
    div-int/lit8 v1, v1, 0x5

    .line 291
    .line 292
    mul-int/lit8 v1, v1, 0x2

    .line 293
    .line 294
    invoke-static {p0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇o00〇〇Oo(Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;)Landroid/widget/ImageView;

    .line 295
    .line 296
    .line 297
    move-result-object v0

    .line 298
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 299
    .line 300
    .line 301
    move-result-object v0

    .line 302
    instance-of v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 303
    .line 304
    if-eqz v2, :cond_4

    .line 305
    .line 306
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 307
    .line 308
    invoke-static {v0}, Landroidx/core/view/MarginLayoutParamsCompat;->getMarginStart(Landroid/view/ViewGroup$MarginLayoutParams;)I

    .line 309
    .line 310
    .line 311
    move-result v0

    .line 312
    goto :goto_0

    .line 313
    :cond_4
    const/4 v0, 0x0

    .line 314
    :goto_0
    sub-int/2addr v1, v0

    .line 315
    invoke-static {p0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇o00〇〇Oo(Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;)Landroid/widget/ImageView;

    .line 316
    .line 317
    .line 318
    move-result-object v0

    .line 319
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 320
    .line 321
    .line 322
    move-result-object v0

    .line 323
    instance-of v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 324
    .line 325
    if-eqz v2, :cond_5

    .line 326
    .line 327
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 328
    .line 329
    invoke-static {v0}, Landroidx/core/view/MarginLayoutParamsCompat;->getMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;)I

    .line 330
    .line 331
    .line 332
    move-result v0

    .line 333
    goto :goto_1

    .line 334
    :cond_5
    const/4 v0, 0x0

    .line 335
    :goto_1
    sub-int/2addr v1, v0

    .line 336
    invoke-static {p0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->o〇0(Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;)Landroid/widget/TextView;

    .line 337
    .line 338
    .line 339
    move-result-object v0

    .line 340
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 341
    .line 342
    .line 343
    move-result-object v0

    .line 344
    instance-of v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 345
    .line 346
    if-eqz v2, :cond_6

    .line 347
    .line 348
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 349
    .line 350
    invoke-static {v0}, Landroidx/core/view/MarginLayoutParamsCompat;->getMarginStart(Landroid/view/ViewGroup$MarginLayoutParams;)I

    .line 351
    .line 352
    .line 353
    move-result v0

    .line 354
    goto :goto_2

    .line 355
    :cond_6
    const/4 v0, 0x0

    .line 356
    :goto_2
    sub-int/2addr v1, v0

    .line 357
    invoke-static {p0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->o〇0(Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;)Landroid/widget/TextView;

    .line 358
    .line 359
    .line 360
    move-result-object v0

    .line 361
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 362
    .line 363
    .line 364
    move-result-object v0

    .line 365
    instance-of v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 366
    .line 367
    if-eqz v2, :cond_7

    .line 368
    .line 369
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 370
    .line 371
    invoke-static {v0}, Landroidx/core/view/MarginLayoutParamsCompat;->getMarginEnd(Landroid/view/ViewGroup$MarginLayoutParams;)I

    .line 372
    .line 373
    .line 374
    move-result v6

    .line 375
    :cond_7
    sub-int/2addr v1, v6

    .line 376
    invoke-static {p0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇o〇(Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;)I

    .line 377
    .line 378
    .line 379
    move-result v0

    .line 380
    sub-int/2addr v1, v0

    .line 381
    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 382
    .line 383
    .line 384
    goto :goto_3

    .line 385
    :cond_8
    new-instance v0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow$showOrUpdate$lambda$1$$inlined$doOnLayout$1;

    .line 386
    .line 387
    invoke-direct {v0, p0, v3}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow$showOrUpdate$lambda$1$$inlined$doOnLayout$1;-><init>(Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;Landroid/content/Context;)V

    .line 388
    .line 389
    .line 390
    invoke-virtual {p2, v0}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 391
    .line 392
    .line 393
    :cond_9
    :goto_3
    const-string p2, "context"

    .line 394
    .line 395
    invoke-static {v3, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 396
    .line 397
    .line 398
    invoke-virtual {p0, v3, p1, p3}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->OO0o〇〇(Landroid/content/Context;ZZ)V

    .line 399
    .line 400
    .line 401
    goto :goto_4

    .line 402
    :cond_a
    new-instance v7, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow$showOrUpdate$$inlined$doOnLayout$1;

    .line 403
    .line 404
    move-object v1, v7

    .line 405
    move-object v2, p0

    .line 406
    move v4, p1

    .line 407
    move v5, p3

    .line 408
    move-object v6, p2

    .line 409
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow$showOrUpdate$$inlined$doOnLayout$1;-><init>(Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;Landroid/content/Context;ZZLandroidx/fragment/app/FragmentActivity;)V

    .line 410
    .line 411
    .line 412
    invoke-virtual {v0, v7}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 413
    .line 414
    .line 415
    :goto_4
    return-void
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public final 〇〇808〇(ZLandroidx/fragment/app/FragmentActivity;)V
    .locals 1
    .param p2    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    invoke-virtual {p0, p1, p2, v0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇O〇(ZLandroidx/fragment/app/FragmentActivity;Z)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

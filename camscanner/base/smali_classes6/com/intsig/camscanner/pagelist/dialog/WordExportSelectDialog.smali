.class public final Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;
.super Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;
.source "WordExportSelectDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final O8o08O8O:[Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final OO:Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final OO〇00〇8oO:[Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o8〇OO0〇0o:[Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oOo0:[Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oOo〇8o008:[Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o〇00O:[Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇080OO8〇0:[Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field static final synthetic 〇08O〇00〇o:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final 〇0O:[Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇8〇oO〇〇8o:[Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Landroid/content/Intent;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/DialogWordExportSelectBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->〇08O〇00〇o:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->OO:Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog$Companion;

    .line 31
    .line 32
    const-string v0, "com.tencent.mm/com.tencent.mm.ui.tools.ShareImgUI"

    .line 33
    .line 34
    filled-new-array {v0}, [Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    sput-object v0, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->o〇00O:[Ljava/lang/String;

    .line 39
    .line 40
    const-string v0, "com.tencent.eim/com.tencent.mobileqq.activity.JumpActivity"

    .line 41
    .line 42
    const-string v1, "com.tencent.mobileqq/com.tencent.mobileqq.activity.JumpActivity"

    .line 43
    .line 44
    filled-new-array {v0, v1}, [Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    sput-object v0, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->O8o08O8O:[Ljava/lang/String;

    .line 49
    .line 50
    const-string v0, "com.tencent.eim/com.tencent.mobileqq.activity.qfileJumpActivity"

    .line 51
    .line 52
    const-string v1, "com.tencent.mobileqq/com.tencent.mobileqq.activity.qfileJumpActivity"

    .line 53
    .line 54
    filled-new-array {v0, v1}, [Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    sput-object v0, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->〇080OO8〇0:[Ljava/lang/String;

    .line 59
    .line 60
    const-string v0, "com.tencent.wework/com.tencent.wework.launch.AppSchemeLaunchActivity"

    .line 61
    .line 62
    filled-new-array {v0}, [Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    sput-object v0, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->〇0O:[Ljava/lang/String;

    .line 67
    .line 68
    const-string v0, "com.alibaba.android.rimet/com.alibaba.android.rimet.biz.BokuiActivity"

    .line 69
    .line 70
    filled-new-array {v0}, [Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    sput-object v0, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->oOo〇8o008:[Ljava/lang/String;

    .line 75
    .line 76
    const-string v0, "com.whatsapp/com.whatsapp.contact.picker.ContactPicker"

    .line 77
    .line 78
    filled-new-array {v0}, [Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object v0

    .line 82
    sput-object v0, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->oOo0:[Ljava/lang/String;

    .line 83
    .line 84
    const-string v0, "com.facebook.orca/com.facebook.messenger.intents.ShareIntentHandler"

    .line 85
    .line 86
    filled-new-array {v0}, [Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    sput-object v0, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->OO〇00〇8oO:[Ljava/lang/String;

    .line 91
    .line 92
    const-string v0, "com.google.android.apps.messaging"

    .line 93
    .line 94
    const-string v1, "com.samsung.android.messaging"

    .line 95
    .line 96
    const-string v2, "com.android.mms"

    .line 97
    .line 98
    filled-new-array {v2, v0, v1}, [Ljava/lang/String;

    .line 99
    .line 100
    .line 101
    move-result-object v0

    .line 102
    sput-object v0, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->o8〇OO0〇0o:[Ljava/lang/String;

    .line 103
    .line 104
    const-string v0, "com.android.email"

    .line 105
    .line 106
    filled-new-array {v0}, [Ljava/lang/String;

    .line 107
    .line 108
    .line 109
    move-result-object v0

    .line 110
    sput-object v0, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->〇8〇oO〇〇8o:[Ljava/lang/String;

    .line 111
    .line 112
    return-void
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v6, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/DialogWordExportSelectBinding;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x4

    .line 10
    const/4 v5, 0x0

    .line 11
    move-object v0, v6

    .line 12
    move-object v2, p0

    .line 13
    invoke-direct/range {v0 .. v5}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object v6, p0, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 17
    .line 18
    new-instance v0, Landroid/content/Intent;

    .line 19
    .line 20
    const-string v1, "android.intent.action.SEND"

    .line 21
    .line 22
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->〇OOo8〇0:Landroid/content/Intent;

    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final O0〇0(Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p2, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p2, "$shareModel"

    .line 7
    .line 8
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->Ooo8o(Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final Ooo8o(Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;)V
    .locals 10

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->〇o〇88〇8()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x1

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x1

    .line 12
    :goto_0
    new-instance v3, Lorg/json/JSONObject;

    .line 13
    .line 14
    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 15
    .line 16
    .line 17
    iget-object v4, p1, Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;->〇08O〇00〇o:Ljava/lang/String;

    .line 18
    .line 19
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 20
    .line 21
    .line 22
    move-result v4

    .line 23
    const-string v5, "type"

    .line 24
    .line 25
    const-string v6, "select_type"

    .line 26
    .line 27
    const-string v7, "word_export_select_result"

    .line 28
    .line 29
    if-nez v4, :cond_1

    .line 30
    .line 31
    new-instance v4, Landroid/content/ComponentName;

    .line 32
    .line 33
    iget-object v8, p1, Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;->o〇00O:Ljava/lang/String;

    .line 34
    .line 35
    iget-object v9, p1, Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;->O8o08O8O:Ljava/lang/String;

    .line 36
    .line 37
    invoke-direct {v4, v8, v9}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    const/4 v8, 0x2

    .line 41
    new-array v8, v8, [Lkotlin/Pair;

    .line 42
    .line 43
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-static {v6, v0}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    aput-object v0, v8, v1

    .line 52
    .line 53
    const-string v0, "select_component"

    .line 54
    .line 55
    invoke-static {v0, v4}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    aput-object v0, v8, v2

    .line 60
    .line 61
    invoke-static {v8}, Landroidx/core/os/BundleKt;->bundleOf([Lkotlin/Pair;)Landroid/os/Bundle;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    invoke-static {p0, v7, v0}, Landroidx/fragment/app/FragmentKt;->setFragmentResult(Landroidx/fragment/app/Fragment;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 66
    .line 67
    .line 68
    iget-object p1, p1, Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;->o〇00O:Ljava/lang/String;

    .line 69
    .line 70
    const-string v0, "innerShareModel.packageName"

    .line 71
    .line 72
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->o〇0〇o(Ljava/lang/String;)Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object p1

    .line 79
    if-eqz p1, :cond_2

    .line 80
    .line 81
    invoke-virtual {v3, v5, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 82
    .line 83
    .line 84
    goto :goto_1

    .line 85
    :cond_1
    new-array p1, v2, [Lkotlin/Pair;

    .line 86
    .line 87
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    invoke-static {v6, v0}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    .line 92
    .line 93
    .line 94
    move-result-object v0

    .line 95
    aput-object v0, p1, v1

    .line 96
    .line 97
    invoke-static {p1}, Landroidx/core/os/BundleKt;->bundleOf([Lkotlin/Pair;)Landroid/os/Bundle;

    .line 98
    .line 99
    .line 100
    move-result-object p1

    .line 101
    invoke-static {p0, v7, p1}, Landroidx/fragment/app/FragmentKt;->setFragmentResult(Landroidx/fragment/app/Fragment;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 102
    .line 103
    .line 104
    const-string p1, "more"

    .line 105
    .line 106
    invoke-virtual {v3, v5, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 107
    .line 108
    .line 109
    :cond_2
    :goto_1
    const-string p1, "CSWordExportPop"

    .line 110
    .line 111
    const-string v0, "send_word"

    .line 112
    .line 113
    invoke-static {p1, v0, v3}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 114
    .line 115
    .line 116
    invoke-virtual {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;->dismiss()V

    .line 117
    .line 118
    .line 119
    return-void
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final o00〇88〇08(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isDetached()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_2

    .line 6
    .line 7
    if-eqz p1, :cond_2

    .line 8
    .line 9
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->〇088O()Lcom/intsig/camscanner/databinding/DialogWordExportSelectBinding;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    if-nez v0, :cond_1

    .line 21
    .line 22
    return-void

    .line 23
    :cond_1
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogWordExportSelectBinding;->OO:Landroid/widget/HorizontalScrollView;

    .line 24
    .line 25
    const-string v2, "vb.horizontalList"

    .line 26
    .line 27
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    new-instance v2, Lo〇08oO80o/〇〇888;

    .line 31
    .line 32
    invoke-direct {v2, v1, p1, p0, v0}, Lo〇08oO80o/〇〇888;-><init>(Landroid/widget/HorizontalScrollView;Ljava/util/List;Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;Lcom/intsig/camscanner/databinding/DialogWordExportSelectBinding;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v1, v2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 36
    .line 37
    .line 38
    :cond_2
    :goto_0
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final synthetic o880(Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->o00〇88〇08(Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final oOoO8OO〇(Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;Landroid/view/View;)V
    .locals 2

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "CSWordExportPop"

    .line 7
    .line 8
    const-string v0, "transfer_pdf"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    const/4 p1, 0x1

    .line 14
    new-array p1, p1, [Lkotlin/Pair;

    .line 15
    .line 16
    const/4 v0, 0x2

    .line 17
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    const-string v1, "select_type"

    .line 22
    .line 23
    invoke-static {v1, v0}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    const/4 v1, 0x0

    .line 28
    aput-object v0, p1, v1

    .line 29
    .line 30
    invoke-static {p1}, Landroidx/core/os/BundleKt;->bundleOf([Lkotlin/Pair;)Landroid/os/Bundle;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    const-string v0, "word_export_select_result"

    .line 35
    .line 36
    invoke-static {p0, v0, p1}, Landroidx/fragment/app/FragmentKt;->setFragmentResult(Landroidx/fragment/app/Fragment;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;->dismiss()V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic oOo〇08〇(Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->〇0ooOOo(Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oO〇oo(Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->〇0〇0(Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oooO888(Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->O0〇0(Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final o〇0〇o(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "com.tencent.mm"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string p1, "wechat"

    .line 10
    .line 11
    return-object p1

    .line 12
    :cond_0
    const-string v0, "com.tencent.eim"

    .line 13
    .line 14
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-nez v0, :cond_4

    .line 19
    .line 20
    const-string v0, "com.tencent.mobileqq"

    .line 21
    .line 22
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-eqz v0, :cond_1

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->o8〇OO0〇0o:[Ljava/lang/String;

    .line 30
    .line 31
    invoke-static {v0, p1}, Lkotlin/collections/ArraysKt;->〇0〇O0088o([Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    if-eqz v0, :cond_2

    .line 36
    .line 37
    const-string p1, "message"

    .line 38
    .line 39
    return-object p1

    .line 40
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->〇8〇oO〇〇8o:[Ljava/lang/String;

    .line 41
    .line 42
    invoke-static {v0, p1}, Lkotlin/collections/ArraysKt;->〇0〇O0088o([Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    if-eqz p1, :cond_3

    .line 47
    .line 48
    const-string p1, "mail"

    .line 49
    .line 50
    return-object p1

    .line 51
    :cond_3
    const/4 p1, 0x0

    .line 52
    return-object p1

    .line 53
    :cond_4
    :goto_0
    const-string p1, "qq"

    .line 54
    .line 55
    return-object p1
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final o〇O8OO()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->〇088O()Lcom/intsig/camscanner/databinding/DialogWordExportSelectBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogWordExportSelectBinding;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 9
    .line 10
    new-instance v2, Lo〇08oO80o/oO80;

    .line 11
    .line 12
    invoke-direct {v2, p0}, Lo〇08oO80o/oO80;-><init>(Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 16
    .line 17
    .line 18
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogWordExportSelectBinding;->〇080OO8〇0:Lcom/intsig/camscanner/mode_ocr/view/ExportRawOtherTypeItem;

    .line 19
    .line 20
    new-instance v2, Lo〇08oO80o/〇80〇808〇O;

    .line 21
    .line 22
    invoke-direct {v2, p0}, Lo〇08oO80o/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 26
    .line 27
    .line 28
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogWordExportSelectBinding;->O8o08O8O:Lcom/intsig/camscanner/mode_ocr/view/ExportRawOtherTypeItem;

    .line 29
    .line 30
    new-instance v1, Lo〇08oO80o/OO0o〇〇〇〇0;

    .line 31
    .line 32
    invoke-direct {v1, p0}, Lo〇08oO80o/OO0o〇〇〇〇0;-><init>(Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇088O()Lcom/intsig/camscanner/databinding/DialogWordExportSelectBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->〇08O〇00〇o:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/DialogWordExportSelectBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final 〇0ooOOo(Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;Landroid/view/View;)V
    .locals 2

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "CSWordExportPop"

    .line 7
    .line 8
    const-string v0, "save_to_gallery"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    const/4 p1, 0x1

    .line 14
    new-array p1, p1, [Lkotlin/Pair;

    .line 15
    .line 16
    const/4 v0, 0x3

    .line 17
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    const-string v1, "select_type"

    .line 22
    .line 23
    invoke-static {v1, v0}, Lkotlin/TuplesKt;->〇080(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    const/4 v1, 0x0

    .line 28
    aput-object v0, p1, v1

    .line 29
    .line 30
    invoke-static {p1}, Landroidx/core/os/BundleKt;->bundleOf([Lkotlin/Pair;)Landroid/os/Bundle;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    const-string v0, "word_export_select_result"

    .line 35
    .line 36
    invoke-static {p0, v0, p1}, Landroidx/fragment/app/FragmentKt;->setFragmentResult(Landroidx/fragment/app/Fragment;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;->dismiss()V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private static final 〇0〇0(Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;->dismiss()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇80O8o8O〇(Landroid/widget/HorizontalScrollView;Ljava/util/List;Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;Lcom/intsig/camscanner/databinding/DialogWordExportSelectBinding;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->〇〇o0〇8(Landroid/widget/HorizontalScrollView;Ljava/util/List;Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;Lcom/intsig/camscanner/databinding/DialogWordExportSelectBinding;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final 〇8〇80o()V
    .locals 6

    .line 1
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x0

    .line 7
    new-instance v3, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog$initShareAppData$1;

    .line 8
    .line 9
    const/4 v4, 0x0

    .line 10
    invoke-direct {v3, p0, v4}, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog$initShareAppData$1;-><init>(Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;Lkotlin/coroutines/Continuation;)V

    .line 11
    .line 12
    .line 13
    const/4 v4, 0x3

    .line 14
    const/4 v5, 0x0

    .line 15
    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->oOoO8OO〇(Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇o〇88〇8()I
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const-string v1, "arg_file_type"

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    :goto_0
    return v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final 〇〇o0〇8(Landroid/widget/HorizontalScrollView;Ljava/util/List;Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;Lcom/intsig/camscanner/databinding/DialogWordExportSelectBinding;)V
    .locals 9

    .line 1
    const-string v0, "$horizontalScrollView"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "this$0"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "$vb"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    .line 21
    .line 22
    .line 23
    move-result p0

    .line 24
    int-to-float v1, v0

    .line 25
    const/high16 v2, 0x40900000    # 4.5f

    .line 26
    .line 27
    div-float/2addr v1, v2

    .line 28
    float-to-int v1, v1

    .line 29
    new-instance v2, Ljava/lang/StringBuilder;

    .line 30
    .line 31
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    .line 33
    .line 34
    const-string v3, "measuredWidth="

    .line 35
    .line 36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    const-string v0, " itemWidth="

    .line 43
    .line 44
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    const-string v2, "WordExportSelectDialog"

    .line 55
    .line 56
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 60
    .line 61
    .line 62
    move-result v0

    .line 63
    const/4 v2, 0x0

    .line 64
    const/4 v3, 0x0

    .line 65
    :goto_0
    if-ge v3, v0, :cond_1

    .line 66
    .line 67
    invoke-virtual {p2}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 68
    .line 69
    .line 70
    move-result-object v4

    .line 71
    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 72
    .line 73
    .line 74
    move-result-object v4

    .line 75
    iget-object v5, p3, Lcom/intsig/camscanner/databinding/DialogWordExportSelectBinding;->o〇00O:Landroid/widget/LinearLayout;

    .line 76
    .line 77
    const v6, 0x7f0d04bb

    .line 78
    .line 79
    .line 80
    invoke-virtual {v4, v6, v5, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 81
    .line 82
    .line 83
    move-result-object v4

    .line 84
    iget-object v5, p3, Lcom/intsig/camscanner/databinding/DialogWordExportSelectBinding;->o〇00O:Landroid/widget/LinearLayout;

    .line 85
    .line 86
    invoke-virtual {v5, v4, v1, p0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 87
    .line 88
    .line 89
    const v5, 0x7f0a00fe

    .line 90
    .line 91
    .line 92
    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 93
    .line 94
    .line 95
    move-result-object v5

    .line 96
    check-cast v5, Landroid/widget/ImageView;

    .line 97
    .line 98
    const v6, 0x7f0a019c

    .line 99
    .line 100
    .line 101
    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 102
    .line 103
    .line 104
    move-result-object v6

    .line 105
    check-cast v6, Landroid/widget/TextView;

    .line 106
    .line 107
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 108
    .line 109
    .line 110
    move-result-object v7

    .line 111
    check-cast v7, Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;

    .line 112
    .line 113
    iget-object v8, v7, Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;->OO:Landroid/graphics/drawable/Drawable;

    .line 114
    .line 115
    if-eqz v8, :cond_0

    .line 116
    .line 117
    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 118
    .line 119
    .line 120
    :cond_0
    iget-object v5, v7, Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;->〇OOo8〇0:Ljava/lang/String;

    .line 121
    .line 122
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 123
    .line 124
    .line 125
    new-instance v5, Lo〇08oO80o/〇8o8o〇;

    .line 126
    .line 127
    invoke-direct {v5, p2, v7}, Lo〇08oO80o/〇8o8o〇;-><init>(Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;)V

    .line 128
    .line 129
    .line 130
    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    .line 132
    .line 133
    add-int/lit8 v3, v3, 0x1

    .line 134
    .line 135
    goto :goto_0

    .line 136
    :cond_1
    return-void
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method


# virtual methods
.method public getTheme()I
    .locals 1

    .line 1
    const v0, 0x7f140193

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->〇OOo8〇0:Landroid/content/Intent;

    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->〇o〇88〇8()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    const-string v0, "application/vnd.openxmlformats-officedocument.wordprocessingml.document"

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const-string v0, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"

    .line 16
    .line 17
    :goto_0
    invoke-virtual {p1, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 18
    .line 19
    .line 20
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/LayoutInflater;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string p3, "inflater"

    .line 2
    .line 3
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const p3, 0x7f0d0254

    .line 7
    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    return-object p1
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "view"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1, p2}, Landroidx/fragment/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 7
    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->o〇O8OO()V

    .line 10
    .line 11
    .line 12
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->〇8〇80o()V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇8〇OOoooo()Ljava/util/List;
    .locals 23
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/utils/LanguageUtil;->Oooo8o0〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/16 v1, 0x8

    .line 6
    .line 7
    const/4 v2, 0x7

    .line 8
    const/4 v3, 0x6

    .line 9
    const/4 v4, 0x5

    .line 10
    const/4 v5, 0x3

    .line 11
    const/16 v6, 0x9

    .line 12
    .line 13
    const/4 v7, 0x4

    .line 14
    const/4 v8, 0x2

    .line 15
    const/4 v9, 0x1

    .line 16
    const/4 v10, 0x0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    new-array v6, v6, [[Ljava/lang/String;

    .line 20
    .line 21
    sget-object v11, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->o〇00O:[Ljava/lang/String;

    .line 22
    .line 23
    aput-object v11, v6, v10

    .line 24
    .line 25
    sget-object v11, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->O8o08O8O:[Ljava/lang/String;

    .line 26
    .line 27
    aput-object v11, v6, v9

    .line 28
    .line 29
    sget-object v11, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->〇080OO8〇0:[Ljava/lang/String;

    .line 30
    .line 31
    aput-object v11, v6, v8

    .line 32
    .line 33
    sget-object v11, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->〇0O:[Ljava/lang/String;

    .line 34
    .line 35
    aput-object v11, v6, v5

    .line 36
    .line 37
    sget-object v5, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->oOo〇8o008:[Ljava/lang/String;

    .line 38
    .line 39
    aput-object v5, v6, v7

    .line 40
    .line 41
    sget-object v5, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->oOo0:[Ljava/lang/String;

    .line 42
    .line 43
    aput-object v5, v6, v4

    .line 44
    .line 45
    sget-object v4, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->OO〇00〇8oO:[Ljava/lang/String;

    .line 46
    .line 47
    aput-object v4, v6, v3

    .line 48
    .line 49
    sget-object v3, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->o8〇OO0〇0o:[Ljava/lang/String;

    .line 50
    .line 51
    aput-object v3, v6, v2

    .line 52
    .line 53
    sget-object v2, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->〇8〇oO〇〇8o:[Ljava/lang/String;

    .line 54
    .line 55
    aput-object v2, v6, v1

    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_0
    new-array v6, v6, [[Ljava/lang/String;

    .line 59
    .line 60
    sget-object v11, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->oOo0:[Ljava/lang/String;

    .line 61
    .line 62
    aput-object v11, v6, v10

    .line 63
    .line 64
    sget-object v11, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->OO〇00〇8oO:[Ljava/lang/String;

    .line 65
    .line 66
    aput-object v11, v6, v9

    .line 67
    .line 68
    sget-object v11, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->o8〇OO0〇0o:[Ljava/lang/String;

    .line 69
    .line 70
    aput-object v11, v6, v8

    .line 71
    .line 72
    sget-object v11, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->〇8〇oO〇〇8o:[Ljava/lang/String;

    .line 73
    .line 74
    aput-object v11, v6, v5

    .line 75
    .line 76
    sget-object v5, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->o〇00O:[Ljava/lang/String;

    .line 77
    .line 78
    aput-object v5, v6, v7

    .line 79
    .line 80
    sget-object v5, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->O8o08O8O:[Ljava/lang/String;

    .line 81
    .line 82
    aput-object v5, v6, v4

    .line 83
    .line 84
    sget-object v4, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->〇080OO8〇0:[Ljava/lang/String;

    .line 85
    .line 86
    aput-object v4, v6, v3

    .line 87
    .line 88
    sget-object v3, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->〇0O:[Ljava/lang/String;

    .line 89
    .line 90
    aput-object v3, v6, v2

    .line 91
    .line 92
    sget-object v2, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->oOo〇8o008:[Ljava/lang/String;

    .line 93
    .line 94
    aput-object v2, v6, v1

    .line 95
    .line 96
    :goto_0
    if-eqz v0, :cond_1

    .line 97
    .line 98
    const-string v11, "wechat"

    .line 99
    .line 100
    const-string v12, "qq"

    .line 101
    .line 102
    const-string v13, "qq_to_pc"

    .line 103
    .line 104
    const-string v14, "business_wechat"

    .line 105
    .line 106
    const-string v15, "ding_talk"

    .line 107
    .line 108
    const-string v16, "whatsapp"

    .line 109
    .line 110
    const-string v17, "facebook_msg"

    .line 111
    .line 112
    const-string v18, "short_msg"

    .line 113
    .line 114
    const-string v19, "email"

    .line 115
    .line 116
    filled-new-array/range {v11 .. v19}, [Ljava/lang/String;

    .line 117
    .line 118
    .line 119
    move-result-object v0

    .line 120
    goto :goto_1

    .line 121
    :cond_1
    const-string v11, "whatsapp"

    .line 122
    .line 123
    const-string v12, "facebook_msg"

    .line 124
    .line 125
    const-string v13, "short_msg"

    .line 126
    .line 127
    const-string v14, "email"

    .line 128
    .line 129
    const-string v15, "wechat"

    .line 130
    .line 131
    const-string v16, "qq"

    .line 132
    .line 133
    const-string v17, "qq_to_pc"

    .line 134
    .line 135
    const-string v18, "business_wechat"

    .line 136
    .line 137
    const-string v19, "ding_talk"

    .line 138
    .line 139
    filled-new-array/range {v11 .. v19}, [Ljava/lang/String;

    .line 140
    .line 141
    .line 142
    move-result-object v0

    .line 143
    :goto_1
    move-object v1, v0

    .line 144
    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 145
    .line 146
    .line 147
    move-result-object v0

    .line 148
    new-instance v2, Ljava/util/ArrayList;

    .line 149
    .line 150
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 151
    .line 152
    .line 153
    if-nez v0, :cond_2

    .line 154
    .line 155
    return-object v2

    .line 156
    :cond_2
    new-instance v3, Lcom/intsig/camscanner/share/ShareDataPresenter;

    .line 157
    .line 158
    invoke-direct {v3, v0}, Lcom/intsig/camscanner/share/ShareDataPresenter;-><init>(Landroid/content/Context;)V

    .line 159
    .line 160
    .line 161
    move-object/from16 v4, p0

    .line 162
    .line 163
    iget-object v5, v4, Lcom/intsig/camscanner/pagelist/dialog/WordExportSelectDialog;->〇OOo8〇0:Landroid/content/Intent;

    .line 164
    .line 165
    invoke-virtual {v3, v5}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇80〇808〇O(Landroid/content/Intent;)Ljava/util/ArrayList;

    .line 166
    .line 167
    .line 168
    move-result-object v5

    .line 169
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    .line 170
    .line 171
    .line 172
    move-result-object v11

    .line 173
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 174
    .line 175
    .line 176
    move-result-object v5

    .line 177
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    .line 178
    .line 179
    .line 180
    move-result v0

    .line 181
    if-eqz v0, :cond_9

    .line 182
    .line 183
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 184
    .line 185
    .line 186
    move-result-object v0

    .line 187
    move-object v12, v0

    .line 188
    check-cast v12, Landroid/content/pm/ResolveInfo;

    .line 189
    .line 190
    invoke-virtual {v3, v12}, Lcom/intsig/camscanner/share/ShareDataPresenter;->〇8o8o〇(Landroid/content/pm/ResolveInfo;)Landroid/content/pm/ComponentInfo;

    .line 191
    .line 192
    .line 193
    move-result-object v0

    .line 194
    if-eqz v0, :cond_8

    .line 195
    .line 196
    iget-object v13, v0, Landroid/content/pm/ComponentInfo;->packageName:Ljava/lang/String;

    .line 197
    .line 198
    iget-object v14, v0, Landroid/content/pm/ComponentInfo;->name:Ljava/lang/String;

    .line 199
    .line 200
    new-instance v0, Ljava/lang/StringBuilder;

    .line 201
    .line 202
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 203
    .line 204
    .line 205
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    .line 207
    .line 208
    const-string v15, "/"

    .line 209
    .line 210
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    .line 212
    .line 213
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    .line 215
    .line 216
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 217
    .line 218
    .line 219
    move-result-object v0

    .line 220
    array-length v7, v6

    .line 221
    const/4 v9, 0x0

    .line 222
    :goto_3
    if-ge v9, v7, :cond_7

    .line 223
    .line 224
    aget-object v8, v6, v9

    .line 225
    .line 226
    array-length v8, v8

    .line 227
    :goto_4
    if-ge v10, v8, :cond_6

    .line 228
    .line 229
    aget-object v20, v6, v9

    .line 230
    .line 231
    move-object/from16 v21, v3

    .line 232
    .line 233
    aget-object v3, v20, v10

    .line 234
    .line 235
    const/4 v4, 0x0

    .line 236
    move-object/from16 v20, v5

    .line 237
    .line 238
    move-object/from16 v18, v6

    .line 239
    .line 240
    const/4 v5, 0x2

    .line 241
    const/4 v6, 0x0

    .line 242
    invoke-static {v3, v15, v6, v5, v4}, Lkotlin/text/StringsKt;->Oo8Oo00oo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    .line 243
    .line 244
    .line 245
    move-result v19

    .line 246
    const/16 v17, 0x1

    .line 247
    .line 248
    xor-int/lit8 v19, v19, 0x1

    .line 249
    .line 250
    invoke-static {v3, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 251
    .line 252
    .line 253
    move-result v22

    .line 254
    if-nez v22, :cond_4

    .line 255
    .line 256
    if-eqz v19, :cond_3

    .line 257
    .line 258
    invoke-static {v3, v13}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 259
    .line 260
    .line 261
    move-result v3

    .line 262
    if-eqz v3, :cond_3

    .line 263
    .line 264
    goto :goto_5

    .line 265
    :cond_3
    add-int/lit8 v10, v10, 0x1

    .line 266
    .line 267
    move-object/from16 v4, p0

    .line 268
    .line 269
    move-object/from16 v6, v18

    .line 270
    .line 271
    move-object/from16 v5, v20

    .line 272
    .line 273
    move-object/from16 v3, v21

    .line 274
    .line 275
    goto :goto_4

    .line 276
    :cond_4
    :goto_5
    if-eqz v19, :cond_5

    .line 277
    .line 278
    move-object v3, v13

    .line 279
    goto :goto_6

    .line 280
    :cond_5
    move-object v3, v0

    .line 281
    :goto_6
    const-string v7, ""

    .line 282
    .line 283
    :try_start_0
    invoke-virtual {v12, v11}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    .line 284
    .line 285
    .line 286
    move-result-object v8

    .line 287
    const-string v0, "resolveInfo.loadLabel(packageManager)"

    .line 288
    .line 289
    invoke-static {v8, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 290
    .line 291
    .line 292
    :try_start_1
    invoke-virtual {v12, v11}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    .line 293
    .line 294
    .line 295
    move-result-object v4
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 296
    goto :goto_8

    .line 297
    :catch_0
    move-exception v0

    .line 298
    move-object v7, v8

    .line 299
    goto :goto_7

    .line 300
    :catch_1
    move-exception v0

    .line 301
    :goto_7
    const-string v8, "WordExportSelectDialog"

    .line 302
    .line 303
    invoke-static {v8, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 304
    .line 305
    .line 306
    move-object v8, v7

    .line 307
    :goto_8
    new-instance v0, Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;

    .line 308
    .line 309
    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 310
    .line 311
    .line 312
    move-result-object v7

    .line 313
    invoke-direct {v0, v12, v7, v4, v3}, Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;-><init>(Landroid/content/pm/ResolveInfo;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V

    .line 314
    .line 315
    .line 316
    invoke-virtual {v0, v13, v14}, Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;->〇o〇(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;

    .line 317
    .line 318
    .line 319
    move-result-object v0

    .line 320
    aget-object v3, v1, v9

    .line 321
    .line 322
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;->〇o00〇〇Oo(Ljava/lang/String;)Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;

    .line 323
    .line 324
    .line 325
    move-result-object v0

    .line 326
    invoke-virtual {v0, v9}, Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;->O8(I)Lcom/intsig/camscanner/mode_ocr/mode/InnerShareModel;

    .line 327
    .line 328
    .line 329
    move-result-object v0

    .line 330
    const-string v3, "innerShareModel"

    .line 331
    .line 332
    invoke-static {v0, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 333
    .line 334
    .line 335
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 336
    .line 337
    .line 338
    move-object/from16 v4, p0

    .line 339
    .line 340
    move-object/from16 v6, v18

    .line 341
    .line 342
    move-object/from16 v5, v20

    .line 343
    .line 344
    move-object/from16 v3, v21

    .line 345
    .line 346
    const/4 v7, 0x4

    .line 347
    const/4 v8, 0x2

    .line 348
    const/4 v9, 0x1

    .line 349
    const/4 v10, 0x0

    .line 350
    goto/16 :goto_2

    .line 351
    .line 352
    :cond_6
    move-object/from16 v21, v3

    .line 353
    .line 354
    move-object/from16 v20, v5

    .line 355
    .line 356
    move-object/from16 v18, v6

    .line 357
    .line 358
    const/4 v5, 0x2

    .line 359
    const/4 v6, 0x0

    .line 360
    const/16 v17, 0x1

    .line 361
    .line 362
    add-int/lit8 v9, v9, 0x1

    .line 363
    .line 364
    move-object/from16 v4, p0

    .line 365
    .line 366
    move-object/from16 v6, v18

    .line 367
    .line 368
    move-object/from16 v5, v20

    .line 369
    .line 370
    const/4 v8, 0x2

    .line 371
    const/4 v10, 0x0

    .line 372
    goto/16 :goto_3

    .line 373
    .line 374
    :cond_7
    const/16 v17, 0x1

    .line 375
    .line 376
    move-object/from16 v4, p0

    .line 377
    .line 378
    const/4 v7, 0x4

    .line 379
    const/4 v9, 0x1

    .line 380
    goto/16 :goto_2

    .line 381
    .line 382
    :cond_8
    move-object/from16 v4, p0

    .line 383
    .line 384
    goto/16 :goto_2

    .line 385
    .line 386
    :cond_9
    const/4 v6, 0x0

    .line 387
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->oo88o8O(Ljava/util/List;)V

    .line 388
    .line 389
    .line 390
    new-instance v0, Ljava/util/ArrayList;

    .line 391
    .line 392
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 393
    .line 394
    .line 395
    const/4 v1, 0x4

    .line 396
    const/4 v10, 0x0

    .line 397
    :goto_9
    if-ge v10, v1, :cond_b

    .line 398
    .line 399
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 400
    .line 401
    .line 402
    move-result v3

    .line 403
    if-le v3, v10, :cond_a

    .line 404
    .line 405
    invoke-interface {v2, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 406
    .line 407
    .line 408
    move-result-object v3

    .line 409
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 410
    .line 411
    .line 412
    :cond_a
    add-int/lit8 v10, v10, 0x1

    .line 413
    .line 414
    goto :goto_9

    .line 415
    :cond_b
    return-object v0
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

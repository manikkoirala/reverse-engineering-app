.class public final Lcom/intsig/camscanner/pagelist/widget/AiEntranceView$DelayInterpolator;
.super Ljava/lang/Object;
.source "AiEntranceView.kt"

# interfaces
.implements Landroid/view/animation/Interpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DelayInterpolator"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final 〇080:J

.field private final 〇o00〇〇Oo:Landroid/view/animation/Interpolator;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o〇:J


# direct methods
.method public constructor <init>(JJLandroid/view/animation/Interpolator;)V
    .locals 1
    .param p5    # Landroid/view/animation/Interpolator;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "interpolator"

    .line 2
    .line 3
    invoke-static {p5, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-wide p3, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView$DelayInterpolator;->〇080:J

    .line 10
    .line 11
    iput-object p5, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView$DelayInterpolator;->〇o00〇〇Oo:Landroid/view/animation/Interpolator;

    .line 12
    .line 13
    add-long/2addr p3, p1

    .line 14
    iput-wide p3, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView$DelayInterpolator;->〇o〇:J

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 7

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView$DelayInterpolator;->〇o〇:J

    .line 2
    .line 3
    long-to-float v2, v0

    .line 4
    mul-float v2, v2, p1

    .line 5
    .line 6
    float-to-long v2, v2

    .line 7
    iget-wide v4, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView$DelayInterpolator;->〇080:J

    .line 8
    .line 9
    cmp-long v6, v2, v4

    .line 10
    .line 11
    if-lez v6, :cond_0

    .line 12
    .line 13
    const/high16 p1, 0x3f800000    # 1.0f

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView$DelayInterpolator;->〇o00〇〇Oo:Landroid/view/animation/Interpolator;

    .line 17
    .line 18
    long-to-float v0, v0

    .line 19
    mul-float p1, p1, v0

    .line 20
    .line 21
    long-to-float v0, v4

    .line 22
    div-float/2addr p1, v0

    .line 23
    invoke-interface {v2, p1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    .line 24
    .line 25
    .line 26
    move-result p1

    .line 27
    :goto_0
    return p1
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

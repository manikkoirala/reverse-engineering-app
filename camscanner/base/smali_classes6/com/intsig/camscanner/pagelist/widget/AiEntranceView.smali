.class public final Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;
.super Lcom/intsig/view/SafeImageView;
.source "AiEntranceView.kt"

# interfaces
.implements Landroidx/lifecycle/DefaultLifecycleObserver;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pagelist/widget/AiEntranceView$DelayInterpolator;,
        Lcom/intsig/camscanner/pagelist/widget/AiEntranceView$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇00O0:Lcom/intsig/camscanner/pagelist/widget/AiEntranceView$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O0O:Landroid/graphics/Bitmap;

.field private O88O:F

.field private O8o08O8O:F

.field private final OO〇00〇8oO:Landroid/graphics/Bitmap;

.field private Oo80:F

.field private O〇o88o08〇:Z

.field private o8o:I

.field private final o8oOOo:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o8〇OO0〇0o:Landroid/graphics/Bitmap;

.field private oOO〇〇:I

.field private final oOo0:Landroid/graphics/RectF;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOo〇8o008:I

.field private oo8ooo8O:I

.field private final ooo0〇〇O:Landroid/graphics/Bitmap;

.field private final o〇00O:Landroid/graphics/Path;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇oO:Landroid/animation/ValueAnimator;

.field private 〇080OO8〇0:F

.field private 〇08〇o0O:Landroid/animation/ValueAnimator;

.field private 〇0O:I

.field private final 〇8〇oO〇〇8o:Landroid/graphics/Bitmap;

.field private final 〇O〇〇O8:Landroid/graphics/RectF;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o0O:F

.field private final 〇〇08O:Landroid/graphics/Bitmap;

.field private 〇〇o〇:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇00O0:Lcom/intsig/camscanner/pagelist/widget/AiEntranceView$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-direct {p0, p1, p2, p3}, Landroidx/appcompat/widget/AppCompatImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 4
    new-instance p2, Landroid/graphics/Path;

    invoke-direct {p2}, Landroid/graphics/Path;-><init>()V

    iput-object p2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o〇00O:Landroid/graphics/Path;

    .line 5
    new-instance p2, Landroid/graphics/RectF;

    invoke-direct {p2}, Landroid/graphics/RectF;-><init>()V

    iput-object p2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->oOo0:Landroid/graphics/RectF;

    .line 6
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f0800b0

    invoke-static {p2, p3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object p2

    iput-object p2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->OO〇00〇8oO:Landroid/graphics/Bitmap;

    .line 7
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f0800b1

    invoke-static {p2, p3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object p2

    iput-object p2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o8〇OO0〇0o:Landroid/graphics/Bitmap;

    .line 8
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f0800b2

    invoke-static {p2, p3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object p2

    iput-object p2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇8〇oO〇〇8o:Landroid/graphics/Bitmap;

    .line 9
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f0800ad

    invoke-static {p2, p3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object p2

    iput-object p2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->ooo0〇〇O:Landroid/graphics/Bitmap;

    .line 10
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const v0, 0x7f0800ae

    invoke-static {p3, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object p3

    iput-object p3, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇〇08O:Landroid/graphics/Bitmap;

    .line 11
    invoke-virtual {p0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const v0, 0x7f0800af

    invoke-static {p3, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object p3

    iput-object p3, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->O0O:Landroid/graphics/Bitmap;

    .line 12
    new-instance p3, Landroid/graphics/Paint;

    const/4 v0, 0x1

    invoke-direct {p3, v0}, Landroid/graphics/Paint;-><init>(I)V

    iput-object p3, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o8oOOo:Landroid/graphics/Paint;

    .line 13
    new-instance p3, Landroid/graphics/RectF;

    invoke-direct {p3}, Landroid/graphics/RectF;-><init>()V

    iput-object p3, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇O〇〇O8:Landroid/graphics/RectF;

    .line 14
    instance-of p3, p1, Landroidx/appcompat/app/AppCompatActivity;

    if-eqz p3, :cond_0

    .line 15
    check-cast p1, Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {p1}, Landroidx/activity/ComponentActivity;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    move-result-object p1

    invoke-virtual {p1, p0}, Landroidx/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 16
    :cond_0
    invoke-virtual {p0, p2}, Lcom/intsig/view/SafeImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 17
    sget-object p1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, p1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 2
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private final OO0o〇〇()V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->oo8ooo8O:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-eq v0, v1, :cond_0

    .line 5
    .line 6
    return-void

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇08〇o0O:Landroid/animation/ValueAnimator;

    .line 8
    .line 9
    if-nez v0, :cond_1

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇〇808〇()V

    .line 12
    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_1
    invoke-virtual {v0}, Landroid/animation/Animator;->isPaused()Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-eqz v1, :cond_2

    .line 20
    .line 21
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->resume()V

    .line 22
    .line 23
    .line 24
    :cond_2
    :goto_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final OO0o〇〇〇〇0(Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->OO0o〇〇()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic Oo08(Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;Landroid/animation/ValueAnimator;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇O〇(Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;Landroid/animation/ValueAnimator;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final oO80()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇00O0:Lcom/intsig/camscanner/pagelist/widget/AiEntranceView$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView$Companion;->〇080()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic o〇0(Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final 〇0〇O0088o()V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇00O0:Lcom/intsig/camscanner/pagelist/widget/AiEntranceView$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView$Companion;->〇o00〇〇Oo()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇80〇808〇O(FF)Z
    .locals 4

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->oo8ooo8O:I

    .line 2
    .line 3
    const/4 v1, 0x2

    .line 4
    const/4 v2, 0x1

    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    return v2

    .line 8
    :cond_0
    const/4 v1, 0x0

    .line 9
    if-ne v0, v2, :cond_1

    .line 10
    .line 11
    new-instance v0, Landroid/graphics/RectF;

    .line 12
    .line 13
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->OO〇00〇8oO:Landroid/graphics/Bitmap;

    .line 14
    .line 15
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    int-to-float v2, v2

    .line 20
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇8〇oO〇〇8o:Landroid/graphics/Bitmap;

    .line 21
    .line 22
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    .line 23
    .line 24
    .line 25
    move-result v3

    .line 26
    int-to-float v3, v3

    .line 27
    add-float/2addr v2, v3

    .line 28
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->OO〇00〇8oO:Landroid/graphics/Bitmap;

    .line 29
    .line 30
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    .line 31
    .line 32
    .line 33
    move-result v3

    .line 34
    int-to-float v3, v3

    .line 35
    invoke-direct {v0, v1, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_1
    new-instance v0, Landroid/graphics/RectF;

    .line 40
    .line 41
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->OO〇00〇8oO:Landroid/graphics/Bitmap;

    .line 42
    .line 43
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o8〇OO0〇0o:Landroid/graphics/Bitmap;

    .line 48
    .line 49
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    .line 50
    .line 51
    .line 52
    move-result v3

    .line 53
    add-int/2addr v2, v3

    .line 54
    int-to-float v2, v2

    .line 55
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇8〇oO〇〇8o:Landroid/graphics/Bitmap;

    .line 56
    .line 57
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    .line 58
    .line 59
    .line 60
    move-result v3

    .line 61
    int-to-float v3, v3

    .line 62
    add-float/2addr v2, v3

    .line 63
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->OO〇00〇8oO:Landroid/graphics/Bitmap;

    .line 64
    .line 65
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    .line 66
    .line 67
    .line 68
    move-result v3

    .line 69
    int-to-float v3, v3

    .line 70
    invoke-direct {v0, v1, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 71
    .line 72
    .line 73
    :goto_0
    invoke-virtual {v0, p1, p2}, Landroid/graphics/RectF;->contains(FF)Z

    .line 74
    .line 75
    .line 76
    move-result p1

    .line 77
    return p1
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final 〇8o8o〇()V
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->oo8ooo8O:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-eq v0, v1, :cond_0

    .line 5
    .line 6
    return-void

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇08〇o0O:Landroid/animation/ValueAnimator;

    .line 8
    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_1

    .line 16
    .line 17
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->pause()V

    .line 18
    .line 19
    .line 20
    :cond_1
    return-void
    .line 21
.end method

.method private static final 〇O〇(Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;Landroid/animation/ValueAnimator;)V
    .locals 5

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "it"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    const-string v0, "null cannot be cast to non-null type kotlin.Float"

    .line 16
    .line 17
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    check-cast p1, Ljava/lang/Float;

    .line 21
    .line 22
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    iget v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->O8o08O8O:F

    .line 27
    .line 28
    add-float v1, p1, v0

    .line 29
    .line 30
    iget v2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->oOo〇8o008:I

    .line 31
    .line 32
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->oOo0:Landroid/graphics/RectF;

    .line 33
    .line 34
    iput v0, v3, Landroid/graphics/RectF;->left:F

    .line 35
    .line 36
    const/4 v0, 0x0

    .line 37
    iput v0, v3, Landroid/graphics/RectF;->top:F

    .line 38
    .line 39
    iput v1, v3, Landroid/graphics/RectF;->right:F

    .line 40
    .line 41
    int-to-float v1, v2

    .line 42
    iput v1, v3, Landroid/graphics/RectF;->bottom:F

    .line 43
    .line 44
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o〇00O:Landroid/graphics/Path;

    .line 45
    .line 46
    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    .line 47
    .line 48
    .line 49
    new-instance v1, Landroid/graphics/RectF;

    .line 50
    .line 51
    iget v2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->oOo〇8o008:I

    .line 52
    .line 53
    int-to-float v3, v2

    .line 54
    int-to-float v2, v2

    .line 55
    invoke-direct {v1, v0, v0, v3, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 56
    .line 57
    .line 58
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o〇00O:Landroid/graphics/Path;

    .line 59
    .line 60
    const/high16 v3, 0x42b40000    # 90.0f

    .line 61
    .line 62
    const/high16 v4, 0x43340000    # 180.0f

    .line 63
    .line 64
    invoke-virtual {v2, v1, v3, v4}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 65
    .line 66
    .line 67
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o〇00O:Landroid/graphics/Path;

    .line 68
    .line 69
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->oOo0:Landroid/graphics/RectF;

    .line 70
    .line 71
    sget-object v3, Landroid/graphics/Path$Direction;->CCW:Landroid/graphics/Path$Direction;

    .line 72
    .line 73
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->addRect(Landroid/graphics/RectF;Landroid/graphics/Path$Direction;)V

    .line 74
    .line 75
    .line 76
    new-instance v1, Landroid/graphics/RectF;

    .line 77
    .line 78
    iget v2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->O8o08O8O:F

    .line 79
    .line 80
    const/4 v3, 0x2

    .line 81
    int-to-float v3, v3

    .line 82
    mul-float v2, v2, v3

    .line 83
    .line 84
    add-float/2addr v2, p1

    .line 85
    iget v3, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->oOo〇8o008:I

    .line 86
    .line 87
    int-to-float v3, v3

    .line 88
    invoke-direct {v1, p1, v0, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 89
    .line 90
    .line 91
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o〇00O:Landroid/graphics/Path;

    .line 92
    .line 93
    const/high16 v3, -0x3d4c0000    # -90.0f

    .line 94
    .line 95
    invoke-virtual {v2, v1, v3, v4}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    .line 96
    .line 97
    .line 98
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o〇00O:Landroid/graphics/Path;

    .line 99
    .line 100
    iget v2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇o0O:F

    .line 101
    .line 102
    iget v3, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->O88O:F

    .line 103
    .line 104
    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->offset(FF)V

    .line 105
    .line 106
    .line 107
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇O〇〇O8:Landroid/graphics/RectF;

    .line 108
    .line 109
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->OO〇00〇8oO:Landroid/graphics/Bitmap;

    .line 110
    .line 111
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    .line 112
    .line 113
    .line 114
    move-result v2

    .line 115
    int-to-float v2, v2

    .line 116
    iput v2, v1, Landroid/graphics/RectF;->left:F

    .line 117
    .line 118
    iput v0, v1, Landroid/graphics/RectF;->top:F

    .line 119
    .line 120
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->OO〇00〇8oO:Landroid/graphics/Bitmap;

    .line 121
    .line 122
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 123
    .line 124
    .line 125
    move-result v0

    .line 126
    int-to-float v0, v0

    .line 127
    add-float/2addr p1, v0

    .line 128
    iput p1, v1, Landroid/graphics/RectF;->right:F

    .line 129
    .line 130
    iget p1, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o8o:I

    .line 131
    .line 132
    int-to-float p1, p1

    .line 133
    iput p1, v1, Landroid/graphics/RectF;->bottom:F

    .line 134
    .line 135
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 136
    .line 137
    .line 138
    return-void
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic 〇〇888(Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;Landroid/animation/ValueAnimator;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇〇8O0〇8(Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;Landroid/animation/ValueAnimator;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇〇8O0〇8(Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;Landroid/animation/ValueAnimator;)V
    .locals 2

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "it"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    const-string v0, "null cannot be cast to non-null type kotlin.Int"

    .line 16
    .line 17
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    check-cast p1, Ljava/lang/Integer;

    .line 21
    .line 22
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    mul-int/lit16 v0, p1, 0xe1

    .line 27
    .line 28
    const/16 v1, 0x64

    .line 29
    .line 30
    div-int/2addr v0, v1

    .line 31
    iput v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇〇o〇:I

    .line 32
    .line 33
    int-to-float p1, p1

    .line 34
    const/high16 v0, 0x43b40000    # 360.0f

    .line 35
    .line 36
    mul-float p1, p1, v0

    .line 37
    .line 38
    int-to-float v0, v1

    .line 39
    div-float/2addr p1, v0

    .line 40
    iput p1, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->Oo80:F

    .line 41
    .line 42
    invoke-virtual {p0}, Landroid/view/View;->invalidate()V

    .line 43
    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method


# virtual methods
.method public final Oooo8o0〇()V
    .locals 1

    .line 1
    const/4 v0, 0x2

    .line 2
    iput v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->oo8ooo8O:I

    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->O0O:Landroid/graphics/Bitmap;

    .line 5
    .line 6
    invoke-virtual {p0, v0}, Lcom/intsig/view/SafeImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇O00()V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public synthetic onCreate(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Landroidx/lifecycle/〇o00〇〇Oo;->〇080(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public synthetic onDestroy(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Landroidx/lifecycle/〇o00〇〇Oo;->〇o00〇〇Oo(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroid/widget/ImageView;->onDetachedFromWindow()V

    .line 2
    .line 3
    .line 4
    const-string v0, "AiEntranceView"

    .line 5
    .line 6
    const-string v1, "onDetachedFromWindow: "

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o〇oO:Landroid/animation/ValueAnimator;

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 16
    .line 17
    .line 18
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o〇oO:Landroid/animation/ValueAnimator;

    .line 19
    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 23
    .line 24
    .line 25
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇08〇o0O:Landroid/animation/ValueAnimator;

    .line 26
    .line 27
    if-eqz v0, :cond_2

    .line 28
    .line 29
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->removeAllUpdateListeners()V

    .line 30
    .line 31
    .line 32
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇08〇o0O:Landroid/animation/ValueAnimator;

    .line 33
    .line 34
    if-eqz v0, :cond_3

    .line 35
    .line 36
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->end()V

    .line 37
    .line 38
    .line 39
    :cond_3
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->oo8ooo8O:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    const/4 v2, 0x0

    .line 5
    if-ne v0, v1, :cond_8

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇08〇o0O:Landroid/animation/ValueAnimator;

    .line 8
    .line 9
    if-nez v0, :cond_3

    .line 10
    .line 11
    if-eqz p1, :cond_0

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->OO〇00〇8oO:Landroid/graphics/Bitmap;

    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o8oOOo:Landroid/graphics/Paint;

    .line 16
    .line 17
    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    if-eqz p1, :cond_1

    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o8〇OO0〇0o:Landroid/graphics/Bitmap;

    .line 23
    .line 24
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->OO〇00〇8oO:Landroid/graphics/Bitmap;

    .line 25
    .line 26
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    int-to-float v1, v1

    .line 31
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o8oOOo:Landroid/graphics/Paint;

    .line 32
    .line 33
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 34
    .line 35
    .line 36
    :cond_1
    if-eqz p1, :cond_2

    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇8〇oO〇〇8o:Landroid/graphics/Bitmap;

    .line 39
    .line 40
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->OO〇00〇8oO:Landroid/graphics/Bitmap;

    .line 41
    .line 42
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 43
    .line 44
    .line 45
    move-result v1

    .line 46
    int-to-float v1, v1

    .line 47
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o8〇OO0〇0o:Landroid/graphics/Bitmap;

    .line 48
    .line 49
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    .line 50
    .line 51
    .line 52
    move-result v3

    .line 53
    int-to-float v3, v3

    .line 54
    add-float/2addr v1, v3

    .line 55
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o8oOOo:Landroid/graphics/Paint;

    .line 56
    .line 57
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 58
    .line 59
    .line 60
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 61
    .line 62
    .line 63
    goto/16 :goto_0

    .line 64
    .line 65
    :cond_3
    if-eqz p1, :cond_4

    .line 66
    .line 67
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->OO〇00〇8oO:Landroid/graphics/Bitmap;

    .line 68
    .line 69
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o8oOOo:Landroid/graphics/Paint;

    .line 70
    .line 71
    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 72
    .line 73
    .line 74
    :cond_4
    if-eqz p1, :cond_5

    .line 75
    .line 76
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o8〇OO0〇0o:Landroid/graphics/Bitmap;

    .line 77
    .line 78
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇O〇〇O8:Landroid/graphics/RectF;

    .line 79
    .line 80
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o8oOOo:Landroid/graphics/Paint;

    .line 81
    .line 82
    const/4 v4, 0x0

    .line 83
    invoke-virtual {p1, v0, v4, v1, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 84
    .line 85
    .line 86
    :cond_5
    if-eqz p1, :cond_6

    .line 87
    .line 88
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇8〇oO〇〇8o:Landroid/graphics/Bitmap;

    .line 89
    .line 90
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->OO〇00〇8oO:Landroid/graphics/Bitmap;

    .line 91
    .line 92
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 93
    .line 94
    .line 95
    move-result v1

    .line 96
    int-to-float v1, v1

    .line 97
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇O〇〇O8:Landroid/graphics/RectF;

    .line 98
    .line 99
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    .line 100
    .line 101
    .line 102
    move-result v3

    .line 103
    add-float/2addr v1, v3

    .line 104
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o8oOOo:Landroid/graphics/Paint;

    .line 105
    .line 106
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 107
    .line 108
    .line 109
    :cond_6
    if-eqz p1, :cond_7

    .line 110
    .line 111
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o〇00O:Landroid/graphics/Path;

    .line 112
    .line 113
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 114
    .line 115
    .line 116
    :cond_7
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 117
    .line 118
    .line 119
    goto :goto_0

    .line 120
    :cond_8
    if-nez v0, :cond_c

    .line 121
    .line 122
    if-eqz p1, :cond_9

    .line 123
    .line 124
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->OO〇00〇8oO:Landroid/graphics/Bitmap;

    .line 125
    .line 126
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o8oOOo:Landroid/graphics/Paint;

    .line 127
    .line 128
    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 129
    .line 130
    .line 131
    :cond_9
    if-eqz p1, :cond_a

    .line 132
    .line 133
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o8〇OO0〇0o:Landroid/graphics/Bitmap;

    .line 134
    .line 135
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->OO〇00〇8oO:Landroid/graphics/Bitmap;

    .line 136
    .line 137
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 138
    .line 139
    .line 140
    move-result v1

    .line 141
    int-to-float v1, v1

    .line 142
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o8oOOo:Landroid/graphics/Paint;

    .line 143
    .line 144
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 145
    .line 146
    .line 147
    :cond_a
    if-eqz p1, :cond_b

    .line 148
    .line 149
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇8〇oO〇〇8o:Landroid/graphics/Bitmap;

    .line 150
    .line 151
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->OO〇00〇8oO:Landroid/graphics/Bitmap;

    .line 152
    .line 153
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 154
    .line 155
    .line 156
    move-result v1

    .line 157
    int-to-float v1, v1

    .line 158
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o8〇OO0〇0o:Landroid/graphics/Bitmap;

    .line 159
    .line 160
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    .line 161
    .line 162
    .line 163
    move-result v3

    .line 164
    int-to-float v3, v3

    .line 165
    add-float/2addr v1, v3

    .line 166
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o8oOOo:Landroid/graphics/Paint;

    .line 167
    .line 168
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 169
    .line 170
    .line 171
    :cond_b
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 172
    .line 173
    .line 174
    goto :goto_0

    .line 175
    :cond_c
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 176
    .line 177
    .line 178
    if-eqz p1, :cond_d

    .line 179
    .line 180
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 181
    .line 182
    .line 183
    :cond_d
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o8oOOo:Landroid/graphics/Paint;

    .line 184
    .line 185
    iget v1, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇〇o〇:I

    .line 186
    .line 187
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 188
    .line 189
    .line 190
    if-eqz p1, :cond_e

    .line 191
    .line 192
    iget v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->Oo80:F

    .line 193
    .line 194
    invoke-virtual {p0}, Landroid/view/View;->getWidth()I

    .line 195
    .line 196
    .line 197
    move-result v1

    .line 198
    int-to-float v1, v1

    .line 199
    const/high16 v3, 0x3f800000    # 1.0f

    .line 200
    .line 201
    mul-float v1, v1, v3

    .line 202
    .line 203
    const/4 v4, 0x2

    .line 204
    int-to-float v4, v4

    .line 205
    div-float/2addr v1, v4

    .line 206
    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    .line 207
    .line 208
    .line 209
    move-result v5

    .line 210
    int-to-float v5, v5

    .line 211
    mul-float v5, v5, v3

    .line 212
    .line 213
    div-float/2addr v5, v4

    .line 214
    invoke-virtual {p1, v0, v1, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 215
    .line 216
    .line 217
    :cond_e
    if-eqz p1, :cond_f

    .line 218
    .line 219
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇〇08O:Landroid/graphics/Bitmap;

    .line 220
    .line 221
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o8oOOo:Landroid/graphics/Paint;

    .line 222
    .line 223
    invoke-virtual {p1, v0, v2, v2, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 224
    .line 225
    .line 226
    :cond_f
    if-eqz p1, :cond_10

    .line 227
    .line 228
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 229
    .line 230
    .line 231
    :cond_10
    :goto_0
    return-void
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method protected onMeasure(II)V
    .locals 4

    .line 1
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onMeasure(II)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->OO〇00〇8oO:Landroid/graphics/Bitmap;

    .line 5
    .line 6
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o8〇OO0〇0o:Landroid/graphics/Bitmap;

    .line 11
    .line 12
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    .line 13
    .line 14
    .line 15
    move-result p2

    .line 16
    add-int/2addr p1, p2

    .line 17
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇8〇oO〇〇8o:Landroid/graphics/Bitmap;

    .line 18
    .line 19
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    .line 20
    .line 21
    .line 22
    move-result p2

    .line 23
    add-int/2addr p1, p2

    .line 24
    iput p1, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->oOO〇〇:I

    .line 25
    .line 26
    iget p2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->oo8ooo8O:I

    .line 27
    .line 28
    const/4 v0, 0x2

    .line 29
    if-ne p2, v0, :cond_0

    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->O0O:Landroid/graphics/Bitmap;

    .line 32
    .line 33
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 34
    .line 35
    .line 36
    move-result p1

    .line 37
    :cond_0
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o8〇OO0〇0o:Landroid/graphics/Bitmap;

    .line 38
    .line 39
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    .line 40
    .line 41
    .line 42
    move-result p2

    .line 43
    iput p2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o8o:I

    .line 44
    .line 45
    iget v1, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->oo8ooo8O:I

    .line 46
    .line 47
    if-ne v1, v0, :cond_1

    .line 48
    .line 49
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->O0O:Landroid/graphics/Bitmap;

    .line 50
    .line 51
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    .line 52
    .line 53
    .line 54
    move-result p2

    .line 55
    :cond_1
    iget v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->oOO〇〇:I

    .line 56
    .line 57
    iget v1, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇0O:I

    .line 58
    .line 59
    sub-int/2addr v0, v1

    .line 60
    int-to-float v0, v0

    .line 61
    const/high16 v1, 0x40000000    # 2.0f

    .line 62
    .line 63
    div-float/2addr v0, v1

    .line 64
    iput v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇o0O:F

    .line 65
    .line 66
    iget v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o8o:I

    .line 67
    .line 68
    iget v2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->oOo〇8o008:I

    .line 69
    .line 70
    sub-int v2, v0, v2

    .line 71
    .line 72
    int-to-float v2, v2

    .line 73
    div-float/2addr v2, v1

    .line 74
    iput v2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->O88O:F

    .line 75
    .line 76
    int-to-float v0, v0

    .line 77
    div-float/2addr v0, v1

    .line 78
    iput v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇080OO8〇0:F

    .line 79
    .line 80
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->ooo0〇〇O:Landroid/graphics/Bitmap;

    .line 81
    .line 82
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 83
    .line 84
    .line 85
    move-result v0

    .line 86
    int-to-float v0, v0

    .line 87
    div-float/2addr v0, v1

    .line 88
    iput v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->O8o08O8O:F

    .line 89
    .line 90
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->ooo0〇〇O:Landroid/graphics/Bitmap;

    .line 91
    .line 92
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    .line 93
    .line 94
    .line 95
    move-result v0

    .line 96
    iput v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇0O:I

    .line 97
    .line 98
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->ooo0〇〇O:Landroid/graphics/Bitmap;

    .line 99
    .line 100
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    .line 101
    .line 102
    .line 103
    move-result v0

    .line 104
    iput v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->oOo〇8o008:I

    .line 105
    .line 106
    iget v1, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇0O:I

    .line 107
    .line 108
    new-instance v2, Ljava/lang/StringBuilder;

    .line 109
    .line 110
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 111
    .line 112
    .line 113
    const-string v3, "onMeasure: bmSize: "

    .line 114
    .line 115
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    .line 117
    .line 118
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 119
    .line 120
    .line 121
    const-string v1, " * "

    .line 122
    .line 123
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    .line 125
    .line 126
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 127
    .line 128
    .line 129
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 130
    .line 131
    .line 132
    move-result-object v0

    .line 133
    const-string v1, "AiEntranceView"

    .line 134
    .line 135
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    .line 137
    .line 138
    invoke-virtual {p0, p1, p2}, Landroid/view/View;->setMeasuredDimension(II)V

    .line 139
    .line 140
    .line 141
    return-void
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public onPause(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 1
    .param p1    # Landroidx/lifecycle/LifecycleOwner;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "owner"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {p0, p1}, Landroidx/lifecycle/〇o00〇〇Oo;->〇o〇(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    .line 7
    .line 8
    .line 9
    const/4 p1, 0x0

    .line 10
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->O〇o88o08〇:Z

    .line 11
    .line 12
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇8o8o〇()V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onResume(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 2
    .param p1    # Landroidx/lifecycle/LifecycleOwner;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "owner"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {p0, p1}, Landroidx/lifecycle/〇o00〇〇Oo;->O8(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    .line 7
    .line 8
    .line 9
    const/4 p1, 0x1

    .line 10
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->O〇o88o08〇:Z

    .line 11
    .line 12
    new-instance p1, Lo0O0O〇〇〇0/〇080;

    .line 13
    .line 14
    invoke-direct {p1, p0}, Lo0O0O〇〇〇0/〇080;-><init>(Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;)V

    .line 15
    .line 16
    .line 17
    const-wide/16 v0, 0x3e8

    .line 18
    .line 19
    invoke-virtual {p0, p1, v0, v1}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public synthetic onStart(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Landroidx/lifecycle/〇o00〇〇Oo;->Oo08(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public synthetic onStop(Landroidx/lifecycle/LifecycleOwner;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Landroidx/lifecycle/〇o00〇〇Oo;->o〇0(Landroidx/lifecycle/DefaultLifecycleObserver;Landroidx/lifecycle/LifecycleOwner;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1

    .line 8
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    const/4 v1, 0x1

    .line 13
    if-ne v0, v1, :cond_2

    .line 14
    .line 15
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    invoke-direct {p0, v0, p1}, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇80〇808〇O(FF)Z

    .line 24
    .line 25
    .line 26
    move-result p1

    .line 27
    if-eqz p1, :cond_1

    .line 28
    .line 29
    invoke-virtual {p0}, Landroid/view/View;->performClick()Z

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_1
    const-string p1, "AiEntranceView"

    .line 34
    .line 35
    const-string v0, "onTouchEvent: click outside"

    .line 36
    .line 37
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    :cond_2
    :goto_0
    return v1
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final 〇O00()V
    .locals 8

    .line 1
    const/4 v0, 0x3

    .line 2
    new-array v0, v0, [I

    .line 3
    .line 4
    fill-array-data v0, :array_0

    .line 5
    .line 6
    .line 7
    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    new-instance v1, Lo0O0O〇〇〇0/〇o〇;

    .line 12
    .line 13
    invoke-direct {v1, p0}, Lo0O0O〇〇〇0/〇o〇;-><init>(Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 17
    .line 18
    .line 19
    const-wide/16 v1, 0x157c

    .line 20
    .line 21
    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 22
    .line 23
    .line 24
    const/4 v1, -0x1

    .line 25
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 26
    .line 27
    .line 28
    const/4 v1, 0x1

    .line 29
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 30
    .line 31
    .line 32
    new-instance v1, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView$DelayInterpolator;

    .line 33
    .line 34
    new-instance v7, Landroid/view/animation/DecelerateInterpolator;

    .line 35
    .line 36
    invoke-direct {v7}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 37
    .line 38
    .line 39
    const-wide/16 v3, 0x9c4

    .line 40
    .line 41
    const-wide/16 v5, 0xbb8

    .line 42
    .line 43
    move-object v2, v1

    .line 44
    invoke-direct/range {v2 .. v7}, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView$DelayInterpolator;-><init>(JJLandroid/view/animation/Interpolator;)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 48
    .line 49
    .line 50
    const-wide/16 v1, 0x1f4

    .line 51
    .line 52
    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 56
    .line 57
    .line 58
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->o〇oO:Landroid/animation/ValueAnimator;

    .line 59
    .line 60
    return-void

    .line 61
    :array_0
    .array-data 4
        0x0
        0x64
        0x0
    .end array-data
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇〇808〇()V
    .locals 5

    .line 1
    const-string v0, "startAnim: "

    .line 2
    .line 3
    const-string v1, "AiEntranceView"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x1

    .line 9
    iput v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->oo8ooo8O:I

    .line 10
    .line 11
    iget-boolean v2, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->O〇o88o08〇:Z

    .line 12
    .line 13
    if-nez v2, :cond_0

    .line 14
    .line 15
    const-string v0, "startAnim: not resume, so ignore"

    .line 16
    .line 17
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    return-void

    .line 21
    :cond_0
    const-string v2, "startAnim: real start"

    .line 22
    .line 23
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    const/4 v1, 0x2

    .line 27
    new-array v2, v1, [F

    .line 28
    .line 29
    iget v3, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇0O:I

    .line 30
    .line 31
    int-to-float v3, v3

    .line 32
    iget v4, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->O8o08O8O:F

    .line 33
    .line 34
    int-to-float v1, v1

    .line 35
    mul-float v4, v4, v1

    .line 36
    .line 37
    sub-float/2addr v3, v4

    .line 38
    const/4 v1, 0x0

    .line 39
    aput v3, v2, v1

    .line 40
    .line 41
    const/4 v1, 0x0

    .line 42
    aput v1, v2, v0

    .line 43
    .line 44
    invoke-static {v2}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    const-wide/16 v1, 0x3e8

    .line 49
    .line 50
    invoke-virtual {v0, v1, v2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 51
    .line 52
    .line 53
    new-instance v1, Landroidx/interpolator/view/animation/FastOutLinearInInterpolator;

    .line 54
    .line 55
    invoke-direct {v1}, Landroidx/interpolator/view/animation/FastOutLinearInInterpolator;-><init>()V

    .line 56
    .line 57
    .line 58
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 59
    .line 60
    .line 61
    new-instance v1, Lo0O0O〇〇〇0/〇o00〇〇Oo;

    .line 62
    .line 63
    invoke-direct {v1, p0}, Lo0O0O〇〇〇0/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;)V

    .line 64
    .line 65
    .line 66
    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 67
    .line 68
    .line 69
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 70
    .line 71
    .line 72
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇08〇o0O:Landroid/animation/ValueAnimator;

    .line 73
    .line 74
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.class public final Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;
.super Landroidx/constraintlayout/widget/ConstraintLayout;
.source "WordContentOpView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pagelist/widget/WordContentOpView$ContentOpDelegate;,
        Lcom/intsig/camscanner/pagelist/widget/WordContentOpView$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final oOo〇8o008:Lcom/intsig/camscanner/pagelist/widget/WordContentOpView$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;

.field private OO:Lcom/intsig/camscanner/pagelist/widget/WordContentOpView$ContentOpDelegate;

.field private final o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:I

.field private 〇080OO8〇0:Z

.field private 〇08O〇00〇o:I

.field private 〇0O:Lcom/intsig/utils/ClickLimit;

.field private 〇OOo8〇0:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->oOo〇8o008:Lcom/intsig/camscanner/pagelist/widget/WordContentOpView$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x6

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 2
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    invoke-direct {p0, p1, p2, p3}, Landroidx/constraintlayout/widget/ConstraintLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p2, -0x1

    .line 5
    iput p2, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇08O〇00〇o:I

    const/16 p2, 0x7b

    .line 6
    iput p2, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o〇00O:I

    const p2, 0x7f0d078c

    .line 7
    invoke-static {p1, p2, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p1

    .line 8
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    move-result-object p1

    const-string p2, "bind(view)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    const/16 p1, 0x20

    .line 9
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇8o8o〇(I)V

    const/16 p1, 0x8

    .line 10
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇8o8o〇(I)V

    const/4 p1, 0x2

    .line 11
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇8o8o〇(I)V

    const/4 p1, 0x1

    const/4 p2, 0x0

    const/4 p3, 0x0

    .line 12
    invoke-static {p0, p3, p1, p2}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇〇8O0〇8(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;IILjava/lang/Object;)V

    .line 13
    invoke-static {}, Lcom/intsig/utils/ClickLimit;->〇o〇()Lcom/intsig/utils/ClickLimit;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇0O:Lcom/intsig/utils/ClickLimit;

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 3
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private final O8ooOoo〇(Landroid/widget/TextView;Z)V
    .locals 5

    .line 1
    const/high16 v0, 0x40800000    # 4.0f

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    const-string p2, "#1A19BCAA"

    .line 6
    .line 7
    invoke-static {p2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 8
    .line 9
    .line 10
    move-result p2

    .line 11
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    int-to-float v0, v0

    .line 16
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    const v2, 0x7f0601ee

    .line 21
    .line 22
    .line 23
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    const/high16 v3, 0x3f800000    # 1.0f

    .line 28
    .line 29
    invoke-static {v3}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 30
    .line 31
    .line 32
    move-result v3

    .line 33
    new-instance v4, Landroid/graphics/drawable/GradientDrawable;

    .line 34
    .line 35
    invoke-direct {v4}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 36
    .line 37
    .line 38
    invoke-virtual {v4, p2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {v4, v3, v1}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {v4, v0}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {p1, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 48
    .line 49
    .line 50
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 51
    .line 52
    .line 53
    move-result-object p2

    .line 54
    invoke-static {p2, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 55
    .line 56
    .line 57
    move-result p2

    .line 58
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 59
    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 63
    .line 64
    .line 65
    move-result-object p2

    .line 66
    const v1, 0x7f0601e3

    .line 67
    .line 68
    .line 69
    invoke-static {p2, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 70
    .line 71
    .line 72
    move-result p2

    .line 73
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 74
    .line 75
    .line 76
    move-result v0

    .line 77
    int-to-float v0, v0

    .line 78
    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    .line 79
    .line 80
    invoke-direct {v1}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 81
    .line 82
    .line 83
    invoke-virtual {v1, p2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 84
    .line 85
    .line 86
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 87
    .line 88
    .line 89
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 90
    .line 91
    .line 92
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 93
    .line 94
    .line 95
    move-result-object p2

    .line 96
    const v0, 0x7f060208

    .line 97
    .line 98
    .line 99
    invoke-static {p2, v0}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 100
    .line 101
    .line 102
    move-result p2

    .line 103
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 104
    .line 105
    .line 106
    :goto_0
    return-void
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final OO0o〇〇(Landroid/view/View;I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇0O:Lcom/intsig/utils/ClickLimit;

    .line 2
    .line 3
    const-wide/16 v1, 0x96

    .line 4
    .line 5
    invoke-virtual {v0, p1, v1, v2}, Lcom/intsig/utils/ClickLimit;->〇o00〇〇Oo(Landroid/view/View;J)Z

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    if-nez p1, :cond_0

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->Oooo8o0〇(I)Z

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    if-eqz p1, :cond_1

    .line 17
    .line 18
    iget v0, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇OOo8〇0:I

    .line 19
    .line 20
    not-int v1, p2

    .line 21
    and-int/2addr v0, v1

    .line 22
    goto :goto_0

    .line 23
    :cond_1
    iget v0, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇OOo8〇0:I

    .line 24
    .line 25
    or-int/2addr v0, p2

    .line 26
    :goto_0
    iput v0, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇OOo8〇0:I

    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->OO:Lcom/intsig/camscanner/pagelist/widget/WordContentOpView$ContentOpDelegate;

    .line 29
    .line 30
    if-eqz v0, :cond_2

    .line 31
    .line 32
    xor-int/lit8 p1, p1, 0x1

    .line 33
    .line 34
    iget v1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇08O〇00〇o:I

    .line 35
    .line 36
    invoke-interface {v0, p2, p1, v1}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView$ContentOpDelegate;->〇o〇(IZI)V

    .line 37
    .line 38
    .line 39
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇oOO8O8()V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇0〇O0088o(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic Oo08(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇oo〇(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;Landroid/widget/CompoundButton;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final OoO8(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "view"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const/16 v0, 0x28

    .line 12
    .line 13
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->OO0o〇〇(Landroid/view/View;I)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final Oooo8o0〇(I)Z
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇OOo8〇0:I

    .line 2
    .line 3
    and-int/2addr p1, v0

    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    const/4 p1, 0x1

    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 p1, 0x0

    .line 9
    :goto_0
    return p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final o800o8O(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->O8o08O8O:Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;

    .line 7
    .line 8
    if-nez p1, :cond_0

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;->setStampHidden(Z)V

    .line 12
    .line 13
    .line 14
    :goto_0
    const/4 p1, 0x2

    .line 15
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇O8o08O(I)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic oO80(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->oo88o8O(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;Landroid/widget/CompoundButton;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static final oo88o8O(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;Landroid/widget/CompoundButton;Z)V
    .locals 2

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->O8o08O8O:Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;

    .line 7
    .line 8
    if-nez p1, :cond_0

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;->setHandwriteRender(I)V

    .line 12
    .line 13
    .line 14
    :goto_0
    if-eqz p2, :cond_1

    .line 15
    .line 16
    const-string p1, "on"

    .line 17
    .line 18
    goto :goto_1

    .line 19
    :cond_1
    const-string p1, "off"

    .line 20
    .line 21
    :goto_1
    iget-boolean p2, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇080OO8〇0:Z

    .line 22
    .line 23
    if-eqz p2, :cond_2

    .line 24
    .line 25
    const-string p2, "CSWordPreview"

    .line 26
    .line 27
    goto :goto_2

    .line 28
    :cond_2
    const-string p2, "CSList"

    .line 29
    .line 30
    :goto_2
    const-string v0, "handwriting_to_print"

    .line 31
    .line 32
    const-string v1, "type"

    .line 33
    .line 34
    invoke-static {p2, v0, v1, p1}, Lcom/intsig/log/LogAgentHelper;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    const/4 p1, 0x0

    .line 38
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇O8o08O(I)V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic o〇0(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->OoO8(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o〇O8〇〇o(I)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇OOo8〇0:I

    .line 2
    .line 3
    not-int p1, p1

    .line 4
    and-int/2addr p1, v0

    .line 5
    iput p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇OOo8〇0:I

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇0〇O0088o(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "view"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x2

    .line 12
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->OO0o〇〇(Landroid/view/View;I)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇80〇808〇O(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇O888o0o(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;Landroid/widget/CompoundButton;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇8o8o〇(I)V
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇OOo8〇0:I

    .line 2
    .line 3
    or-int/2addr p1, v0

    .line 4
    iput p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇OOo8〇0:I

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇O888o0o(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->O8o08O8O:Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;

    .line 7
    .line 8
    if-nez p1, :cond_0

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;->setHandwriteHidden(Z)V

    .line 12
    .line 13
    .line 14
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 15
    .line 16
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->〇OOo8〇0:Landroid/widget/CheckBox;

    .line 17
    .line 18
    xor-int/lit8 p2, p2, 0x1

    .line 19
    .line 20
    invoke-virtual {p1, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 21
    .line 22
    .line 23
    const/16 p1, 0x28

    .line 24
    .line 25
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇O8o08O(I)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final 〇O8o08O(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->O8o08O8O:Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->OO:Lcom/intsig/camscanner/pagelist/widget/WordContentOpView$ContentOpDelegate;

    .line 6
    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    invoke-interface {v1, p1, v0}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView$ContentOpDelegate;->〇o00〇〇Oo(ILcom/intsig/camscanner/pagelist/model/ImageJsonParam;)V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇oOO8O8()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->〇080OO8〇0:Landroid/widget/HorizontalScrollView;

    .line 4
    .line 5
    const-string v1, "mBinding.llFunction"

    .line 6
    .line 7
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    const/4 v1, 0x0

    .line 15
    const/4 v2, 0x1

    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    const/4 v0, 0x1

    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const/4 v0, 0x0

    .line 21
    :goto_0
    if-eqz v0, :cond_4

    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 24
    .line 25
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->o〇00O:Landroid/widget/CheckBox;

    .line 26
    .line 27
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->O8o08O8O:Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;

    .line 28
    .line 29
    if-eqz v3, :cond_1

    .line 30
    .line 31
    invoke-virtual {v3}, Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;->getStampHidden()Z

    .line 32
    .line 33
    .line 34
    move-result v3

    .line 35
    if-ne v3, v2, :cond_1

    .line 36
    .line 37
    const/4 v3, 0x1

    .line 38
    goto :goto_1

    .line 39
    :cond_1
    const/4 v3, 0x0

    .line 40
    :goto_1
    invoke-virtual {v0, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 41
    .line 42
    .line 43
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 44
    .line 45
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->〇08O〇00〇o:Landroid/widget/CheckBox;

    .line 46
    .line 47
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->O8o08O8O:Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;

    .line 48
    .line 49
    if-eqz v3, :cond_2

    .line 50
    .line 51
    invoke-virtual {v3}, Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;->getHandwriteHidden()Z

    .line 52
    .line 53
    .line 54
    move-result v3

    .line 55
    if-ne v3, v2, :cond_2

    .line 56
    .line 57
    const/4 v3, 0x1

    .line 58
    goto :goto_2

    .line 59
    :cond_2
    const/4 v3, 0x0

    .line 60
    :goto_2
    invoke-virtual {v0, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 61
    .line 62
    .line 63
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 64
    .line 65
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->〇OOo8〇0:Landroid/widget/CheckBox;

    .line 66
    .line 67
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->O8o08O8O:Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;

    .line 68
    .line 69
    if-eqz v3, :cond_3

    .line 70
    .line 71
    invoke-virtual {v3}, Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;->getHandwriteRender()I

    .line 72
    .line 73
    .line 74
    move-result v3

    .line 75
    if-ne v3, v2, :cond_3

    .line 76
    .line 77
    const/4 v1, 0x1

    .line 78
    :cond_3
    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 79
    .line 80
    .line 81
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 82
    .line 83
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->〇OOo8〇0:Landroid/widget/CheckBox;

    .line 84
    .line 85
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->〇08O〇00〇o:Landroid/widget/CheckBox;

    .line 86
    .line 87
    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    .line 88
    .line 89
    .line 90
    move-result v0

    .line 91
    xor-int/2addr v0, v2

    .line 92
    invoke-virtual {v1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 93
    .line 94
    .line 95
    goto :goto_3

    .line 96
    :cond_4
    const/4 v0, 0x2

    .line 97
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->Oooo8o0〇(I)Z

    .line 98
    .line 99
    .line 100
    move-result v0

    .line 101
    const/16 v1, 0x28

    .line 102
    .line 103
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->Oooo8o0〇(I)Z

    .line 104
    .line 105
    .line 106
    move-result v1

    .line 107
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 108
    .line 109
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->oOo0:Landroid/widget/TextView;

    .line 110
    .line 111
    const-string v3, "mBinding.tvSeal"

    .line 112
    .line 113
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    .line 115
    .line 116
    invoke-direct {p0, v2, v0}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->O8ooOoo〇(Landroid/widget/TextView;Z)V

    .line 117
    .line 118
    .line 119
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 120
    .line 121
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->oOo〇8o008:Landroid/widget/TextView;

    .line 122
    .line 123
    const-string v2, "mBinding.tvHandwriting"

    .line 124
    .line 125
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    .line 127
    .line 128
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->O8ooOoo〇(Landroid/widget/TextView;Z)V

    .line 129
    .line 130
    .line 131
    :goto_3
    return-void
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private static final 〇oo〇(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;Landroid/widget/CompoundButton;Z)V
    .locals 3

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-boolean p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇080OO8〇0:Z

    .line 7
    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    const-string p1, "CSWordPreview"

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const-string p1, "CSList"

    .line 14
    .line 15
    :goto_0
    if-eqz p2, :cond_1

    .line 16
    .line 17
    const-string v0, "on"

    .line 18
    .line 19
    goto :goto_1

    .line 20
    :cond_1
    const-string v0, "off"

    .line 21
    .line 22
    :goto_1
    const-string v1, "extract_table"

    .line 23
    .line 24
    const-string v2, "type"

    .line 25
    .line 26
    invoke-static {p1, v1, v2, v0}, Lcom/intsig/log/LogAgentHelper;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->OO:Lcom/intsig/camscanner/pagelist/widget/WordContentOpView$ContentOpDelegate;

    .line 30
    .line 31
    if-eqz p0, :cond_2

    .line 32
    .line 33
    invoke-interface {p0, p2}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView$ContentOpDelegate;->〇080(Z)V

    .line 34
    .line 35
    .line 36
    :cond_2
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇〇888(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o800o8O(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;Landroid/widget/CompoundButton;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇〇8O0〇8(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;IILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p2, p2, 0x1

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    const/16 p1, 0x7b

    .line 6
    .line 7
    :cond_0
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇O00(I)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method


# virtual methods
.method public final O〇8O8〇008(Lcom/intsig/camscanner/pagelist/model/ContentOpData;Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;Z)V
    .locals 4
    .param p1    # Lcom/intsig/camscanner/pagelist/model/ContentOpData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "contentOpData"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p2, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->O8o08O8O:Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;

    .line 7
    .line 8
    iput-boolean p3, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇080OO8〇0:Z

    .line 9
    .line 10
    const/4 p3, 0x1

    .line 11
    const/4 v0, 0x0

    .line 12
    if-nez p2, :cond_0

    .line 13
    .line 14
    const/4 p2, 0x1

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 p2, 0x0

    .line 17
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    .line 18
    .line 19
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 20
    .line 21
    .line 22
    const-string v2, "setImageMode imageJsonParam is null: "

    .line 23
    .line 24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object p2

    .line 34
    const-string v1, "WordContentOpView"

    .line 35
    .line 36
    invoke-static {v1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/ContentOpData;->〇080()I

    .line 40
    .line 41
    .line 42
    move-result p2

    .line 43
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 44
    .line 45
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->〇080OO8〇0:Landroid/widget/HorizontalScrollView;

    .line 46
    .line 47
    const-string v2, "mBinding.llFunction"

    .line 48
    .line 49
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    .line 53
    .line 54
    .line 55
    move-result v1

    .line 56
    if-nez v1, :cond_1

    .line 57
    .line 58
    const/4 v1, 0x1

    .line 59
    goto :goto_1

    .line 60
    :cond_1
    const/4 v1, 0x0

    .line 61
    :goto_1
    if-eqz v1, :cond_7

    .line 62
    .line 63
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 64
    .line 65
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->o〇00O:Landroid/widget/CheckBox;

    .line 66
    .line 67
    const-string v2, "mBinding.cbRemoveStamp"

    .line 68
    .line 69
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    and-int/lit8 v2, p2, 0x2

    .line 73
    .line 74
    if-eqz v2, :cond_2

    .line 75
    .line 76
    const/4 v2, 0x1

    .line 77
    goto :goto_2

    .line 78
    :cond_2
    const/4 v2, 0x0

    .line 79
    :goto_2
    invoke-static {v1, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 80
    .line 81
    .line 82
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 83
    .line 84
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->〇08O〇00〇o:Landroid/widget/CheckBox;

    .line 85
    .line 86
    const-string v2, "mBinding.cbRemoveHandwrite"

    .line 87
    .line 88
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    and-int/lit8 v3, p2, 0x8

    .line 92
    .line 93
    if-nez v3, :cond_4

    .line 94
    .line 95
    and-int/lit8 p2, p2, 0x20

    .line 96
    .line 97
    if-eqz p2, :cond_3

    .line 98
    .line 99
    goto :goto_3

    .line 100
    :cond_3
    const/4 p2, 0x0

    .line 101
    goto :goto_4

    .line 102
    :cond_4
    :goto_3
    const/4 p2, 0x1

    .line 103
    :goto_4
    invoke-static {v1, p2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 104
    .line 105
    .line 106
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 107
    .line 108
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->〇OOo8〇0:Landroid/widget/CheckBox;

    .line 109
    .line 110
    const-string v1, "mBinding.cbHandwriteConvert"

    .line 111
    .line 112
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 113
    .line 114
    .line 115
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 116
    .line 117
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->〇08O〇00〇o:Landroid/widget/CheckBox;

    .line 118
    .line 119
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 120
    .line 121
    .line 122
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    .line 123
    .line 124
    .line 125
    move-result v1

    .line 126
    if-nez v1, :cond_5

    .line 127
    .line 128
    const/4 v1, 0x1

    .line 129
    goto :goto_5

    .line 130
    :cond_5
    const/4 v1, 0x0

    .line 131
    :goto_5
    if-eqz v1, :cond_6

    .line 132
    .line 133
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/ContentOpData;->〇o〇()Z

    .line 134
    .line 135
    .line 136
    move-result p1

    .line 137
    if-nez p1, :cond_6

    .line 138
    .line 139
    goto :goto_6

    .line 140
    :cond_6
    const/4 p3, 0x0

    .line 141
    :goto_6
    invoke-static {p2, p3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 142
    .line 143
    .line 144
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇oOO8O8()V

    .line 145
    .line 146
    .line 147
    :cond_7
    return-void
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public final getMContentOpDelegate()Lcom/intsig/camscanner/pagelist/widget/WordContentOpView$ContentOpDelegate;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->OO:Lcom/intsig/camscanner/pagelist/widget/WordContentOpView$ContentOpDelegate;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getMDocType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o〇00O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final setMContentOpDelegate(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView$ContentOpDelegate;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->OO:Lcom/intsig/camscanner/pagelist/widget/WordContentOpView$ContentOpDelegate;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setMDocType(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o〇00O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇00(Lcom/intsig/camscanner/pagelist/model/ContentOpData;I)V
    .locals 4
    .param p1    # Lcom/intsig/camscanner/pagelist/model/ContentOpData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "contentOpData"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v1, "setImageMode: contentOpData: "

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    const-string v1, ", dataPosition: "

    .line 20
    .line 21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    const-string v1, "WordContentOpView"

    .line 32
    .line 33
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    iput p2, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇08O〇00〇o:I

    .line 37
    .line 38
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/ContentOpData;->〇080()I

    .line 39
    .line 40
    .line 41
    move-result p2

    .line 42
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 43
    .line 44
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->oOo0:Landroid/widget/TextView;

    .line 45
    .line 46
    const-string v1, "mBinding.tvSeal"

    .line 47
    .line 48
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    and-int/lit8 v1, p2, 0x2

    .line 52
    .line 53
    const/4 v2, 0x1

    .line 54
    const/4 v3, 0x0

    .line 55
    if-eqz v1, :cond_0

    .line 56
    .line 57
    const/4 v1, 0x1

    .line 58
    goto :goto_0

    .line 59
    :cond_0
    const/4 v1, 0x0

    .line 60
    :goto_0
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 61
    .line 62
    .line 63
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 64
    .line 65
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->oOo〇8o008:Landroid/widget/TextView;

    .line 66
    .line 67
    const-string v1, "mBinding.tvHandwriting"

    .line 68
    .line 69
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    and-int/lit8 v1, p2, 0x8

    .line 73
    .line 74
    if-nez v1, :cond_2

    .line 75
    .line 76
    and-int/lit8 p2, p2, 0x20

    .line 77
    .line 78
    if-eqz p2, :cond_1

    .line 79
    .line 80
    goto :goto_1

    .line 81
    :cond_1
    const/4 v2, 0x0

    .line 82
    :cond_2
    :goto_1
    invoke-static {v0, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 83
    .line 84
    .line 85
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/ContentOpData;->〇o00〇〇Oo()Landroid/util/SparseBooleanArray;

    .line 86
    .line 87
    .line 88
    move-result-object p1

    .line 89
    invoke-virtual {p1}, Landroid/util/SparseBooleanArray;->size()I

    .line 90
    .line 91
    .line 92
    move-result p2

    .line 93
    :goto_2
    if-ge v3, p2, :cond_4

    .line 94
    .line 95
    invoke-virtual {p1, v3}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    .line 96
    .line 97
    .line 98
    move-result v0

    .line 99
    invoke-virtual {p1, v3}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    .line 100
    .line 101
    .line 102
    move-result v1

    .line 103
    if-eqz v1, :cond_3

    .line 104
    .line 105
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o〇O8〇〇o(I)V

    .line 106
    .line 107
    .line 108
    goto :goto_3

    .line 109
    :cond_3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇8o8o〇(I)V

    .line 110
    .line 111
    .line 112
    :goto_3
    add-int/lit8 v3, v3, 0x1

    .line 113
    .line 114
    goto :goto_2

    .line 115
    :cond_4
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇oOO8O8()V

    .line 116
    .line 117
    .line 118
    return-void
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public final 〇O00(I)V
    .locals 7

    .line 1
    const/16 v0, 0x7c

    .line 2
    .line 3
    const-string v1, "mBinding.cbOnlyKeepTable"

    .line 4
    .line 5
    const-string v2, "mBinding.llFunction"

    .line 6
    .line 7
    const-string v3, "mBinding.llContentOp"

    .line 8
    .line 9
    const-string v4, "mBinding.title"

    .line 10
    .line 11
    const/4 v5, 0x1

    .line 12
    const/4 v6, 0x0

    .line 13
    if-ne p1, v0, :cond_0

    .line 14
    .line 15
    invoke-static {}, Lcom/intsig/camscanner/pagelist/WordContentController;->o〇0()Z

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    if-eqz p1, :cond_0

    .line 20
    .line 21
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 22
    .line 23
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->〇0O:Landroid/widget/TextView;

    .line 24
    .line 25
    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    invoke-static {p1, v6}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 29
    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 32
    .line 33
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->O8o08O8O:Landroid/widget/LinearLayout;

    .line 34
    .line 35
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    invoke-static {p1, v6}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 39
    .line 40
    .line 41
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 42
    .line 43
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->〇080OO8〇0:Landroid/widget/HorizontalScrollView;

    .line 44
    .line 45
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    invoke-static {p1, v6}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 49
    .line 50
    .line 51
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 52
    .line 53
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->OO:Landroid/widget/CheckBox;

    .line 54
    .line 55
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    invoke-static {p1, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 59
    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/pagelist/WordContentController;->〇〇888()Z

    .line 63
    .line 64
    .line 65
    move-result p1

    .line 66
    if-eqz p1, :cond_1

    .line 67
    .line 68
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 69
    .line 70
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->〇0O:Landroid/widget/TextView;

    .line 71
    .line 72
    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    invoke-static {p1, v6}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 76
    .line 77
    .line 78
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 79
    .line 80
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->O8o08O8O:Landroid/widget/LinearLayout;

    .line 81
    .line 82
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    invoke-static {p1, v6}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 86
    .line 87
    .line 88
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 89
    .line 90
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->〇080OO8〇0:Landroid/widget/HorizontalScrollView;

    .line 91
    .line 92
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 93
    .line 94
    .line 95
    invoke-static {p1, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 96
    .line 97
    .line 98
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 99
    .line 100
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->OO:Landroid/widget/CheckBox;

    .line 101
    .line 102
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    invoke-static {p1, v6}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 106
    .line 107
    .line 108
    goto :goto_0

    .line 109
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 110
    .line 111
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->〇0O:Landroid/widget/TextView;

    .line 112
    .line 113
    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 114
    .line 115
    .line 116
    invoke-static {p1, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 117
    .line 118
    .line 119
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 120
    .line 121
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->O8o08O8O:Landroid/widget/LinearLayout;

    .line 122
    .line 123
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 124
    .line 125
    .line 126
    invoke-static {p1, v5}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 127
    .line 128
    .line 129
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 130
    .line 131
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->〇080OO8〇0:Landroid/widget/HorizontalScrollView;

    .line 132
    .line 133
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    .line 135
    .line 136
    invoke-static {p1, v6}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 137
    .line 138
    .line 139
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 140
    .line 141
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->OO:Landroid/widget/CheckBox;

    .line 142
    .line 143
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 144
    .line 145
    .line 146
    invoke-static {p1, v6}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 147
    .line 148
    .line 149
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 150
    .line 151
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->OO〇00〇8oO:Landroid/view/View;

    .line 152
    .line 153
    const-string p1, "mBinding.viewLine"

    .line 154
    .line 155
    invoke-static {v0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 156
    .line 157
    .line 158
    const/4 v1, 0x0

    .line 159
    const/16 p1, 0x10

    .line 160
    .line 161
    invoke-static {p1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 162
    .line 163
    .line 164
    move-result p1

    .line 165
    float-to-int v2, p1

    .line 166
    const/4 v3, 0x0

    .line 167
    const/4 v4, 0x0

    .line 168
    const/16 v5, 0xd

    .line 169
    .line 170
    const/4 v6, 0x0

    .line 171
    invoke-static/range {v0 .. v6}, Lcom/intsig/camscanner/util/ViewExtKt;->〇0〇O0088o(Landroid/view/View;IIIIILjava/lang/Object;)V

    .line 172
    .line 173
    .line 174
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->〇oOO8O8()V

    .line 175
    .line 176
    .line 177
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 178
    .line 179
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->oOo0:Landroid/widget/TextView;

    .line 180
    .line 181
    new-instance v0, Lo0O0O〇〇〇0/O8;

    .line 182
    .line 183
    invoke-direct {v0, p0}, Lo0O0O〇〇〇0/O8;-><init>(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;)V

    .line 184
    .line 185
    .line 186
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 187
    .line 188
    .line 189
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 190
    .line 191
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->oOo〇8o008:Landroid/widget/TextView;

    .line 192
    .line 193
    new-instance v0, Lo0O0O〇〇〇0/Oo08;

    .line 194
    .line 195
    invoke-direct {v0, p0}, Lo0O0O〇〇〇0/Oo08;-><init>(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;)V

    .line 196
    .line 197
    .line 198
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 199
    .line 200
    .line 201
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 202
    .line 203
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->o〇00O:Landroid/widget/CheckBox;

    .line 204
    .line 205
    new-instance v0, Lo0O0O〇〇〇0/o〇0;

    .line 206
    .line 207
    invoke-direct {v0, p0}, Lo0O0O〇〇〇0/o〇0;-><init>(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;)V

    .line 208
    .line 209
    .line 210
    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 211
    .line 212
    .line 213
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 214
    .line 215
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->〇08O〇00〇o:Landroid/widget/CheckBox;

    .line 216
    .line 217
    new-instance v0, Lo0O0O〇〇〇0/〇〇888;

    .line 218
    .line 219
    invoke-direct {v0, p0}, Lo0O0O〇〇〇0/〇〇888;-><init>(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;)V

    .line 220
    .line 221
    .line 222
    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 223
    .line 224
    .line 225
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 226
    .line 227
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->〇OOo8〇0:Landroid/widget/CheckBox;

    .line 228
    .line 229
    new-instance v0, Lo0O0O〇〇〇0/oO80;

    .line 230
    .line 231
    invoke-direct {v0, p0}, Lo0O0O〇〇〇0/oO80;-><init>(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;)V

    .line 232
    .line 233
    .line 234
    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 235
    .line 236
    .line 237
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->o0:Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;

    .line 238
    .line 239
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ViewWordContentOpBinding;->OO:Landroid/widget/CheckBox;

    .line 240
    .line 241
    new-instance v0, Lo0O0O〇〇〇0/〇80〇808〇O;

    .line 242
    .line 243
    invoke-direct {v0, p0}, Lo0O0O〇〇〇0/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;)V

    .line 244
    .line 245
    .line 246
    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 247
    .line 248
    .line 249
    return-void
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public final 〇O〇()Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/pagelist/WordContentController;->〇〇888()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->O8o08O8O:Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;

    .line 8
    .line 9
    const/4 v1, 0x0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;->getStampHidden()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    const/4 v1, 0x1

    .line 19
    :cond_0
    return v1

    .line 20
    :cond_1
    const/4 v0, 0x2

    .line 21
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->Oooo8o0〇(I)Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    return v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇〇808〇()Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/pagelist/WordContentController;->〇〇888()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->O8o08O8O:Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;

    .line 8
    .line 9
    const/4 v1, 0x0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;->getHandwriteHidden()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    const/4 v1, 0x1

    .line 19
    :cond_0
    return v1

    .line 20
    :cond_1
    const/16 v0, 0x28

    .line 21
    .line 22
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/widget/WordContentOpView;->Oooo8o0〇(I)Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    return v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

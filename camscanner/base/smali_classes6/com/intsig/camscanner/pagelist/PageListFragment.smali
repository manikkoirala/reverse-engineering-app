.class public Lcom/intsig/camscanner/pagelist/PageListFragment;
.super Lcom/intsig/camscanner/fragment/DocumentAbstractFragment;
.source "PageListFragment.java"

# interfaces
.implements Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;
.implements Lcom/intsig/adapter/BaseRecyclerViewAdapter$OnRecyclerViewItemLongClickListener;
.implements Lcom/intsig/adapter/BaseRecyclerViewAdapter$OnRecyclerViewItemClickListener;
.implements Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OnOcrClickListener;
.implements Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;,
        Lcom/intsig/camscanner/pagelist/PageListFragment$PhoneDevice;,
        Lcom/intsig/camscanner/pagelist/PageListFragment$MyDialogFragment;,
        Lcom/intsig/camscanner/pagelist/PageListFragment$SyncCallbackListenerImpl;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/intsig/camscanner/fragment/DocumentAbstractFragment;",
        "Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;",
        "Lcom/intsig/adapter/BaseRecyclerViewAdapter$OnRecyclerViewItemLongClickListener<",
        "Lcom/intsig/camscanner/pagelist/model/PageTypeItem;",
        ">;",
        "Lcom/intsig/adapter/BaseRecyclerViewAdapter$OnRecyclerViewItemClickListener<",
        "Lcom/intsig/camscanner/pagelist/model/PageTypeItem;",
        ">;",
        "Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OnOcrClickListener;",
        "Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;"
    }
.end annotation


# static fields
.field private static final 〇O8〇8000:Ljava/lang/String; = "PageListFragment"


# instance fields
.field private O0O:Landroid/widget/TextView;
    .annotation build Landroidx/annotation/Nullable;
    .end annotation
.end field

.field private O0〇0:Landroidx/fragment/app/DialogFragment;

.field private O88O:Z

.field private O8o08O8O:Lcom/intsig/view/ImageTextButton;

.field private O8o〇O0:Ljava/lang/String;

.field private O8〇o〇88:Z

.field private OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

.field private OO〇00〇8oO:I

.field private OO〇OOo:Z

.field private Oo0O0o8:Z

.field private Oo0〇Ooo:Z

.field private Oo80:Ljava/lang/String;

.field private Ooo08:I

.field private Ooo8o:Lcom/intsig/menu/MenuItem;

.field private O〇08oOOO0:[I

.field private O〇O:Landroid/view/ViewStub;

.field private O〇o88o08〇:Lcom/intsig/menu/PopupListMenu;

.field private o00〇88〇08:Lcom/intsig/app/ProgressDialog;

.field o0OoOOo0:Z

.field private o880:Landroid/os/Handler;

.field private o8O:Z

.field private o8o:Landroidx/appcompat/app/AppCompatActivity;

.field private final o8oOOo:Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;

.field private o8〇OO:I

.field private o8〇OO0〇0o:Z

.field private oO00〇o:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$OnMultipleFunctionResponse;

.field private oOO0880O:Lcom/intsig/camscanner/batch/BatchImageProcessTipsDialog;

.field private oOO8:Lcom/intsig/camscanner/pagelist/bubble/PageListBubbleControl;

.field private oOO〇〇:Z

.field private oOo0:Lcom/intsig/comm/widget/CustomTextView;

.field private oOoO8OO〇:Lcom/intsig/menu/MenuItem;

.field private oOoo80oO:Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;

.field private oOo〇08〇:Lcom/intsig/camscanner/pdf/PdfKitMoveTipsDialog;

.field private oOo〇8o008:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

.field private oO〇8O8oOo:Lcom/intsig/utils/ClickLimit;

.field private oO〇oo:Lcom/intsig/camscanner/tsapp/SyncCallbackListener;

.field private oo8ooo8O:Lcom/intsig/camscanner/view/PullToSyncRecyclerView;

.field private ooO:Landroid/view/View;

.field private ooo0〇〇O:Z

.field private oooO888:Landroid/widget/EditText;

.field private o〇00O:Landroidx/recyclerview/widget/RecyclerView;

.field private o〇0〇o:Landroid/app/Dialog;

.field private o〇O8OO:Lcom/intsig/menu/MenuItem;

.field private o〇oO:Lcom/intsig/camscanner/view/AbstractPullToSyncView$PullToSyncAssistant;

.field private o〇o〇Oo88:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

.field private 〇00O0:Lcom/intsig/menu/PopupMenuItems;

.field private 〇080OO8〇0:Lcom/intsig/adapter/BaseRecyclerViewTouchHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/adapter/BaseRecyclerViewTouchHelper<",
            "Lcom/intsig/camscanner/pagelist/model/PageTypeItem;",
            ">;"
        }
    .end annotation
.end field

.field private 〇088O:Landroid/view/View;

.field private 〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

.field private 〇08〇o0O:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

.field private 〇0O:Landroidx/recyclerview/widget/GridLayoutManager;

.field private 〇0O〇O00O:Z

.field private 〇0oO〇oo00:I

.field private 〇0ooOOo:Lcom/intsig/menu/MenuItem;

.field private 〇0〇0:Lcom/intsig/menu/MenuItem;

.field private 〇800OO〇0O:Ljava/lang/String;

.field private 〇80O8o8O〇:Landroid/widget/PopupWindow;

.field private 〇8〇80o:Lcom/intsig/menu/PopupMenuItems;

.field 〇8〇OOoooo:Landroid/view/ViewStub;

.field private 〇8〇o88:Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;

.field private 〇8〇oO〇〇8o:I

.field private 〇O8oOo0:Z

.field private 〇OO8ooO8〇:Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;

.field private 〇OO〇00〇0O:Z

.field private 〇O〇〇O8:Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;

.field private 〇o08:Lcom/intsig/menu/MenuItem;

.field private final 〇o0O:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private 〇oo〇O〇80:Z

.field public 〇o〇88〇8:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Ljava/lang/String;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private 〇〇08O:Lcom/intsig/camscanner/pagelist/newpagelist/dialog/TipsManager;

.field private 〇〇O80〇0o:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private 〇〇o0〇8:I

.field private 〇〇o〇:Landroid/view/View;

.field private 〇〇〇0:Landroid/view/View;

.field private 〇〇〇00:Lcom/intsig/menu/MenuItem;

.field private 〇〇〇0o〇〇0:Z


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/DocumentAbstractFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 5
    .line 6
    invoke-direct {v0}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 12
    .line 13
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;-><init>(Lcom/intsig/camscanner/pagelist/contract/PageListContract$View;)V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 17
    .line 18
    const/4 v0, 0x0

    .line 19
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8〇OO0〇0o:Z

    .line 20
    .line 21
    iput v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇oO〇〇8o:I

    .line 22
    .line 23
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->ooo0〇〇O:Z

    .line 24
    .line 25
    new-instance v1, Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;

    .line 26
    .line 27
    invoke-direct {v1}, Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;-><init>()V

    .line 28
    .line 29
    .line 30
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8oOOo:Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;

    .line 31
    .line 32
    new-instance v1, Lcom/intsig/camscanner/pagelist/PageListFragment$2;

    .line 33
    .line 34
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/pagelist/PageListFragment$2;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 35
    .line 36
    .line 37
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇o0O:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 38
    .line 39
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O88O:Z

    .line 40
    .line 41
    const/4 v1, 0x0

    .line 42
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->Oo80:Ljava/lang/String;

    .line 43
    .line 44
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇o88o08〇:Lcom/intsig/menu/PopupListMenu;

    .line 45
    .line 46
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇00O0:Lcom/intsig/menu/PopupMenuItems;

    .line 47
    .line 48
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇08oOOO0:[I

    .line 49
    .line 50
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO8ooO8〇:Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;

    .line 51
    .line 52
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->ooO:Landroid/view/View;

    .line 53
    .line 54
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO〇00〇0O:Z

    .line 55
    .line 56
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->Oo0〇Ooo:Z

    .line 57
    .line 58
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇〇0o〇〇0:Z

    .line 59
    .line 60
    invoke-static {}, Lcom/intsig/utils/ClickLimit;->〇o〇()Lcom/intsig/utils/ClickLimit;

    .line 61
    .line 62
    .line 63
    move-result-object v2

    .line 64
    iput-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oO〇8O8oOo:Lcom/intsig/utils/ClickLimit;

    .line 65
    .line 66
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o0OoOOo0:Z

    .line 67
    .line 68
    sget-object v2, Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;->DOCUMENT_MORE:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 69
    .line 70
    iput-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇o〇Oo88:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 71
    .line 72
    new-instance v2, Lcom/intsig/camscanner/pagelist/PageListFragment$3;

    .line 73
    .line 74
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/pagelist/PageListFragment$3;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 75
    .line 76
    .line 77
    iput-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oO00〇o:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$OnMultipleFunctionResponse;

    .line 78
    .line 79
    const-string v2, "Doc_finish_type_default"

    .line 80
    .line 81
    iput-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O8o〇O0:Ljava/lang/String;

    .line 82
    .line 83
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇oo〇O〇80:Z

    .line 84
    .line 85
    const/4 v2, 0x1

    .line 86
    iput-boolean v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8O:Z

    .line 87
    .line 88
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8oOo0:Z

    .line 89
    .line 90
    new-instance v2, Lcom/intsig/camscanner/pagelist/PageListFragment$SyncCallbackListenerImpl;

    .line 91
    .line 92
    invoke-direct {v2, p0, v1}, Lcom/intsig/camscanner/pagelist/PageListFragment$SyncCallbackListenerImpl;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;Lo0Oo/O8O〇88oO0;)V

    .line 93
    .line 94
    .line 95
    iput-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oO〇oo:Lcom/intsig/camscanner/tsapp/SyncCallbackListener;

    .line 96
    .line 97
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oooO888:Landroid/widget/EditText;

    .line 98
    .line 99
    new-instance v2, Landroid/os/Handler;

    .line 100
    .line 101
    new-instance v3, Lcom/intsig/camscanner/pagelist/PageListFragment$6;

    .line 102
    .line 103
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/pagelist/PageListFragment$6;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 104
    .line 105
    .line 106
    invoke-direct {v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    .line 107
    .line 108
    .line 109
    iput-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o880:Landroid/os/Handler;

    .line 110
    .line 111
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O0〇0:Landroidx/fragment/app/DialogFragment;

    .line 112
    .line 113
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇80o:Lcom/intsig/menu/PopupMenuItems;

    .line 114
    .line 115
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇O8OO:Lcom/intsig/menu/MenuItem;

    .line 116
    .line 117
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0〇0:Lcom/intsig/menu/MenuItem;

    .line 118
    .line 119
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOoO8OO〇:Lcom/intsig/menu/MenuItem;

    .line 120
    .line 121
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0ooOOo:Lcom/intsig/menu/MenuItem;

    .line 122
    .line 123
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->Ooo8o:Lcom/intsig/menu/MenuItem;

    .line 124
    .line 125
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇o08:Lcom/intsig/menu/MenuItem;

    .line 126
    .line 127
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇〇00:Lcom/intsig/menu/MenuItem;

    .line 128
    .line 129
    iput v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0oO〇oo00:I

    .line 130
    .line 131
    return-void
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private O008o8oo()Landroid/text/SpannableString;
    .locals 9

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    invoke-virtual {v1}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    const v2, 0x7f130b84

    .line 13
    .line 14
    .line 15
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    const-string v1, ": "

    .line 23
    .line 24
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    new-instance v3, Ljava/lang/StringBuilder;

    .line 36
    .line 37
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    const-string v0, "+50M"

    .line 44
    .line 45
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 53
    .line 54
    .line 55
    move-result v3

    .line 56
    new-instance v4, Ljava/lang/StringBuilder;

    .line 57
    .line 58
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 59
    .line 60
    .line 61
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    const-string v0, "  "

    .line 65
    .line 66
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 70
    .line 71
    invoke-virtual {v0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    const v5, 0x7f130de7

    .line 76
    .line 77
    .line 78
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object v0

    .line 82
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object v0

    .line 92
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 93
    .line 94
    .line 95
    move-result v1

    .line 96
    new-instance v4, Ljava/lang/StringBuilder;

    .line 97
    .line 98
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 99
    .line 100
    .line 101
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    .line 103
    .line 104
    const-string v0, "+1"

    .line 105
    .line 106
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    .line 108
    .line 109
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 110
    .line 111
    .line 112
    move-result-object v0

    .line 113
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 114
    .line 115
    .line 116
    move-result v4

    .line 117
    new-instance v5, Landroid/text/SpannableString;

    .line 118
    .line 119
    invoke-direct {v5, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 120
    .line 121
    .line 122
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    .line 123
    .line 124
    iget-object v6, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 125
    .line 126
    invoke-virtual {v6}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 127
    .line 128
    .line 129
    move-result-object v6

    .line 130
    const v7, 0x7f060095

    .line 131
    .line 132
    .line 133
    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    .line 134
    .line 135
    .line 136
    move-result v6

    .line 137
    invoke-direct {v0, v6}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 138
    .line 139
    .line 140
    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    .line 141
    .line 142
    iget-object v8, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 143
    .line 144
    invoke-virtual {v8}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 145
    .line 146
    .line 147
    move-result-object v8

    .line 148
    invoke-virtual {v8, v7}, Landroid/content/res/Resources;->getColor(I)I

    .line 149
    .line 150
    .line 151
    move-result v7

    .line 152
    invoke-direct {v6, v7}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 153
    .line 154
    .line 155
    const/16 v7, 0x21

    .line 156
    .line 157
    invoke-virtual {v5, v0, v2, v3, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 158
    .line 159
    .line 160
    invoke-virtual {v5, v6, v1, v4, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 161
    .line 162
    .line 163
    return-object v5
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static synthetic O008oO0(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O00OoO〇(Lcom/intsig/camscanner/pagelist/PageListFragment;)Lcom/intsig/menu/PopupMenuItems;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇80o:Lcom/intsig/menu/PopupMenuItems;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic O00o()V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o0OoOOo0:Z

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o08〇808()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o0OoOOo0:Z

    .line 13
    .line 14
    invoke-static {}, Lcom/intsig/advertisement/logagent/LogAgentManager;->〇8o8o〇()Lcom/intsig/advertisement/logagent/LogAgentManager;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    sget-object v1, Lcom/intsig/advertisement/enums/PositionType;->PageListBanner:Lcom/intsig/advertisement/enums/PositionType;

    .line 19
    .line 20
    const/4 v2, 0x0

    .line 21
    invoke-virtual {v0, v1, v2}, Lcom/intsig/advertisement/logagent/LogAgentManager;->〇O00(Lcom/intsig/advertisement/enums/PositionType;Lorg/json/JSONObject;)V

    .line 22
    .line 23
    .line 24
    :cond_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static synthetic O00〇8(Ljava/lang/String;Lcom/intsig/app/AlertDialog;Landroid/view/View;)V
    .locals 2

    .line 1
    const-string p2, "cancel"

    .line 2
    .line 3
    const-string v0, "type"

    .line 4
    .line 5
    const-string v1, "CSTaskCompletePop"

    .line 6
    .line 7
    invoke-static {v1, p2, v0, p0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->dismiss()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private O00〇o00()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->〇O888o0o()Ljava/util/List;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    invoke-static {v1}, Lcom/intsig/utils/ListUtils;->〇o〇(Ljava/util/List;)Z

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    if-eqz v2, :cond_0

    .line 17
    .line 18
    return-object v0

    .line 19
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    if-eqz v2, :cond_2

    .line 28
    .line 29
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    check-cast v2, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;

    .line 34
    .line 35
    instance-of v3, v2, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 36
    .line 37
    if-eqz v3, :cond_1

    .line 38
    .line 39
    check-cast v2, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 40
    .line 41
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/model/PageImageItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/pagelist/model/PageItem;

    .line 42
    .line 43
    .line 44
    move-result-object v2

    .line 45
    iget-wide v2, v2, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇080:J

    .line 46
    .line 47
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 48
    .line 49
    .line 50
    move-result-object v2

    .line 51
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_2
    return-object v0
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic O088O(Lcom/intsig/camscanner/pagelist/PageListFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O0OO8O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O08o()Lcom/intsig/camscanner/scandone/ScanDoneModel;
    .locals 19

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    new-instance v18, Lcom/intsig/camscanner/scandone/ScanDoneModel;

    .line 4
    .line 5
    move-object/from16 v1, v18

    .line 6
    .line 7
    iget-object v2, v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 8
    .line 9
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0〇oo()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    iget-object v3, v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 14
    .line 15
    invoke-virtual {v3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 16
    .line 17
    .line 18
    move-result-wide v3

    .line 19
    iget-object v5, v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 20
    .line 21
    invoke-virtual {v5}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0O00oO()Z

    .line 22
    .line 23
    .line 24
    move-result v5

    .line 25
    iget-object v6, v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O8o〇O0:Ljava/lang/String;

    .line 26
    .line 27
    const/4 v7, 0x0

    .line 28
    iget-object v8, v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 29
    .line 30
    invoke-virtual {v8}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇0008O8()I

    .line 31
    .line 32
    .line 33
    move-result v8

    .line 34
    iget-object v9, v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 35
    .line 36
    invoke-virtual {v9}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0oo0o0〇()Z

    .line 37
    .line 38
    .line 39
    move-result v9

    .line 40
    iget-object v10, v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 41
    .line 42
    invoke-virtual {v10}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OOoo()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 43
    .line 44
    .line 45
    move-result-object v10

    .line 46
    iget-object v11, v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 47
    .line 48
    invoke-virtual {v11}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o8O〇O()I

    .line 49
    .line 50
    .line 51
    move-result v11

    .line 52
    iget-object v12, v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 53
    .line 54
    invoke-virtual {v12}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo8o〇o〇()Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v12

    .line 58
    iget-object v13, v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 59
    .line 60
    invoke-virtual {v13}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO8008O()J

    .line 61
    .line 62
    .line 63
    move-result-wide v13

    .line 64
    iget-boolean v15, v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->ooo0〇〇O:Z

    .line 65
    .line 66
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇8o00()Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;

    .line 67
    .line 68
    .line 69
    move-result-object v16

    .line 70
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇08〇0oo0()Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 71
    .line 72
    .line 73
    move-result-object v17

    .line 74
    invoke-virtual/range {v17 .. v17}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o8OO0()I

    .line 75
    .line 76
    .line 77
    move-result v17

    .line 78
    invoke-direct/range {v1 .. v17}, Lcom/intsig/camscanner/scandone/ScanDoneModel;-><init>(Ljava/lang/String;JZLjava/lang/String;ZIZLcom/intsig/camscanner/purchase/track/FunctionEntrance;ILjava/lang/String;JZLcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;I)V

    .line 79
    .line 80
    .line 81
    return-object v18
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic O08〇(Lcom/intsig/camscanner/pagelist/PageListFragment;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->ooO888O0〇()Lkotlin/Unit;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O08〇oO8〇(Lcom/intsig/camscanner/pagelist/PageListFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOO〇0o8〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic O0O0〇(Lcom/intsig/camscanner/pagelist/PageListFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O00o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O0Oo〇8()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    return v0

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0O00oO()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic O0o0(Lcom/intsig/camscanner/pagelist/PageListFragment;)Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O0o0〇8o()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇0008O8()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 16
    .line 17
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 18
    .line 19
    .line 20
    move-result-wide v2

    .line 21
    invoke-static {v1, v2, v3}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇O〇80o08O(Landroid/content/Context;J)I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0〇0(I)V

    .line 26
    .line 27
    .line 28
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 29
    .line 30
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇0008O8()I

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    const/16 v1, 0x3e8

    .line 35
    .line 36
    if-ne v0, v1, :cond_1

    .line 37
    .line 38
    sget-object v0, Lcom/intsig/camscanner/paper/PaperUtil;->〇080:Lcom/intsig/camscanner/paper/PaperUtil;

    .line 39
    .line 40
    invoke-virtual {v0}, Lcom/intsig/camscanner/paper/PaperUtil;->OO0o〇〇〇〇0()Z

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    if-eqz v0, :cond_1

    .line 45
    .line 46
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 47
    .line 48
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 49
    .line 50
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 51
    .line 52
    .line 53
    move-result-wide v2

    .line 54
    invoke-static {v1, v2, v3}, Lcom/intsig/camscanner/db/dao/DocumentDao;->ooo〇8oO(Landroid/content/Context;J)Ljava/lang/String;

    .line 55
    .line 56
    .line 57
    move-result-object v1

    .line 58
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇8〇008(Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    :cond_1
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static synthetic O0oO(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic O0oOo()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OoOOo8(Z)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic O0〇(Lcom/intsig/camscanner/pagelist/PageListFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇oo8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic O0〇0(Lcom/intsig/camscanner/pagelist/PageListFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇80oo〇0〇o(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic O0〇0o8O()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O88o〇()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static synthetic O0〇8〇(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroidx/appcompat/widget/AppCompatTextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mToolbarTitle:Landroidx/appcompat/widget/AppCompatTextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O0〇O80ooo()Landroidx/recyclerview/widget/RecyclerView$RecycledViewPool;
    .locals 3

    .line 1
    new-instance v0, Landroidx/recyclerview/widget/RecyclerView$RecycledViewPool;

    .line 2
    .line 3
    invoke-direct {v0}, Landroidx/recyclerview/widget/RecyclerView$RecycledViewPool;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;->BANNER_AD:Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;->getType()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    const/4 v2, 0x1

    .line 13
    invoke-virtual {v0, v1, v2}, Landroidx/recyclerview/widget/RecyclerView$RecycledViewPool;->setMaxRecycledViews(II)V

    .line 14
    .line 15
    .line 16
    sget-object v1, Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;->OPERATION:Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;

    .line 17
    .line 18
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;->getType()I

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    invoke-virtual {v0, v1, v2}, Landroidx/recyclerview/widget/RecyclerView$RecycledViewPool;->setMaxRecycledViews(II)V

    .line 23
    .line 24
    .line 25
    sget-object v1, Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;->IMAGE:Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;

    .line 26
    .line 27
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;->getType()I

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    const/16 v2, 0xa

    .line 32
    .line 33
    invoke-virtual {v0, v1, v2}, Landroidx/recyclerview/widget/RecyclerView$RecycledViewPool;->setMaxRecycledViews(II)V

    .line 34
    .line 35
    .line 36
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private O0〇o8o〇〇()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/pagelist/PageListFragment$11;

    .line 4
    .line 5
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    iget-object v3, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 10
    .line 11
    check-cast v3, Landroid/view/ViewGroup;

    .line 12
    .line 13
    invoke-direct {v1, p0, v2, v3}, Lcom/intsig/camscanner/pagelist/PageListFragment$11;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;Landroid/app/Activity;Landroid/view/ViewGroup;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnDragListener(Landroid/view/View$OnDragListener;)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
.end method

.method private O80(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 8
    .line 9
    const-string v0, "getContext() == null"

    .line 10
    .line 11
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO〇()Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    const/4 v1, 0x0

    .line 22
    iput v1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->oOo0:I

    .line 23
    .line 24
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    new-instance v2, Lo0Oo/Ooo8〇〇;

    .line 29
    .line 30
    invoke-direct {v2, p0}, Lo0Oo/Ooo8〇〇;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 31
    .line 32
    .line 33
    invoke-static {v1, v0, v2, p1}, Lcom/intsig/camscanner/securitymark/SecurityMarkActivity;->O〇080〇o0(Landroid/content/Context;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Lcom/intsig/camscanner/securitymark/SecurityMarkActivity$PrepareIntentCallBack;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private O8080〇O8o(Ljava/lang/String;I)V
    .locals 6

    .line 1
    const-string v0, "CSFileRename"

    .line 2
    .line 3
    const-string v1, "finish"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {p1}, Lcom/intsig/util/WordFilter;->O8(Ljava/lang/String;)Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 21
    .line 22
    .line 23
    move-result-wide v1

    .line 24
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 25
    .line 26
    invoke-virtual {v3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇0〇0o8()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v3

    .line 30
    iget-object v4, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 31
    .line 32
    invoke-static {v1, v2, p1, v3, v4}, Lcom/intsig/camscanner/util/Util;->o8O0(JLjava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o88(Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 40
    .line 41
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇(Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setToolbarTitle(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    sget-object p1, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->〇080:Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;

    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 50
    .line 51
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 52
    .line 53
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 54
    .line 55
    .line 56
    move-result-wide v1

    .line 57
    invoke-virtual {p1, v0, v1, v2, p2}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->〇00〇8(Landroid/content/Context;JI)V

    .line 58
    .line 59
    .line 60
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 61
    .line 62
    sget-object p2, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 63
    .line 64
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 65
    .line 66
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 67
    .line 68
    .line 69
    move-result-wide v0

    .line 70
    invoke-static {p2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 71
    .line 72
    .line 73
    move-result-object p2

    .line 74
    invoke-static {p1, p2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->oo88o8O(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object p1

    .line 78
    sget-object v0, Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;->〇080:Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;

    .line 79
    .line 80
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 81
    .line 82
    const/4 v3, 0x3

    .line 83
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 84
    .line 85
    .line 86
    move-result-wide v4

    .line 87
    move-object v2, p1

    .line 88
    invoke-virtual/range {v0 .. v5}, Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;->OoO8(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;IJ)V

    .line 89
    .line 90
    .line 91
    sget-object p2, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 92
    .line 93
    new-instance v0, Ljava/lang/StringBuilder;

    .line 94
    .line 95
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 96
    .line 97
    .line 98
    const-string v1, "after edit--- doctitle:"

    .line 99
    .line 100
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    .line 102
    .line 103
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 104
    .line 105
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0〇oo()Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object v1

    .line 109
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    .line 111
    .line 112
    const-string v1, ",    pdf path:"

    .line 113
    .line 114
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 118
    .line 119
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇0〇0o8()Ljava/lang/String;

    .line 120
    .line 121
    .line 122
    move-result-object v1

    .line 123
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    .line 125
    .line 126
    const-string v1, "; syncDocId= "

    .line 127
    .line 128
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    .line 130
    .line 131
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 132
    .line 133
    .line 134
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 135
    .line 136
    .line 137
    move-result-object p1

    .line 138
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    .line 140
    .line 141
    return-void
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method static bridge synthetic O80OO(Lcom/intsig/camscanner/pagelist/PageListFragment;)Lcom/intsig/app/ProgressDialog;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o00〇88〇08:Lcom/intsig/app/ProgressDialog;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O80〇()V
    .locals 3

    .line 1
    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    invoke-static {}, Lcom/intsig/camscanner/multiimageedit/factory/NewInstanceFactoryImpl;->〇080()Landroidx/lifecycle/ViewModelProvider$NewInstanceFactory;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    invoke-direct {v0, v1, v2}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    .line 10
    .line 11
    .line 12
    const-class v1, Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    check-cast v0, Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/intsig/camscanner/background_batch/view_model/BatchScanDocViewModel;->〇8o8o〇()Landroidx/lifecycle/MutableLiveData;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    new-instance v1, Lo0Oo/O0o〇〇Oo;

    .line 25
    .line 26
    invoke-direct {v1, p0}, Lo0Oo/O0o〇〇Oo;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0, p0, v1}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic O80〇〇o()V
    .locals 2

    .line 1
    const-string v0, "AreaFreeActivityManager"

    .line 2
    .line 3
    const-string v1, "area free share dialog dismiss"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8oOOo:Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;->〇o00〇〇Oo(Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic O88(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic O880O〇(Lcom/intsig/camscanner/pagelist/PageListFragment;Lcom/intsig/camscanner/question/mode/NPSActionDataGroup;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O88Oo8(Lcom/intsig/camscanner/question/mode/NPSActionDataGroup;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static synthetic O888Oo(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic O888o8(Landroidx/cardview/widget/CardView;Landroid/view/View;)V
    .locals 0

    .line 1
    const/16 p2, 0x8

    .line 2
    .line 3
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 7
    .line 8
    iget-object p1, p1, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O000:Landroidx/lifecycle/MutableLiveData;

    .line 9
    .line 10
    invoke-virtual {p1}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    check-cast p1, Lcom/intsig/camscanner/pagelist/newpagelist/data/PageListRecommendEntity;

    .line 15
    .line 16
    instance-of p2, p1, Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 17
    .line 18
    if-eqz p2, :cond_0

    .line 19
    .line 20
    check-cast p1, Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 21
    .line 22
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/FolderItem;->oo88o8O()I

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    invoke-static {p1}, Lcom/intsig/camscanner/scenariodir/logagent/ScenarioLogDirAgent;->〇〇888(I)V

    .line 27
    .line 28
    .line 29
    :cond_0
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic O88Oo8(Lcom/intsig/camscanner/question/mode/NPSActionDataGroup;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8oOOo:Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;->〇080()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    if-nez p1, :cond_0

    .line 12
    .line 13
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 14
    .line 15
    new-instance v0, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v1, "CurrentHandleDialog="

    .line 21
    .line 22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8oOOo:Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;

    .line 26
    .line 27
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;->〇080()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    return-void

    .line 42
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8oOOo:Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;

    .line 43
    .line 44
    const-string v0, "nps_dialog"

    .line 45
    .line 46
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;->〇o00〇〇Oo(Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    new-instance p1, Landroid/os/Bundle;

    .line 50
    .line 51
    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 52
    .line 53
    .line 54
    const-string v0, "extra_from_import"

    .line 55
    .line 56
    const-string v1, "cs_list"

    .line 57
    .line 58
    invoke-virtual {p1, v0, v1}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    new-instance v0, Lcom/intsig/utils/IntentBuilder;

    .line 62
    .line 63
    invoke-direct {v0}, Lcom/intsig/utils/IntentBuilder;-><init>()V

    .line 64
    .line 65
    .line 66
    invoke-virtual {v0, p0}, Lcom/intsig/utils/IntentBuilder;->〇O8o08O(Landroidx/fragment/app/Fragment;)Lcom/intsig/utils/IntentBuilder;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    const-class v1, Lcom/intsig/camscanner/question/nps/NPSDialogActivity;

    .line 71
    .line 72
    invoke-virtual {v0, v1}, Lcom/intsig/utils/IntentBuilder;->〇〇888(Ljava/lang/Class;)Lcom/intsig/utils/IntentBuilder;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    invoke-virtual {v0, p1}, Lcom/intsig/utils/IntentBuilder;->o〇0(Landroid/os/Bundle;)Lcom/intsig/utils/IntentBuilder;

    .line 77
    .line 78
    .line 79
    move-result-object p1

    .line 80
    const/16 v0, 0x3f8

    .line 81
    .line 82
    invoke-virtual {p1, v0}, Lcom/intsig/utils/IntentBuilder;->oO80(I)Lcom/intsig/utils/IntentBuilder;

    .line 83
    .line 84
    .line 85
    move-result-object p1

    .line 86
    invoke-virtual {p1}, Lcom/intsig/utils/IntentBuilder;->〇80〇808〇O()V

    .line 87
    .line 88
    .line 89
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic O8O(Lcom/intsig/camscanner/pagelist/PageListFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OoOO〇(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic O8O〇8〇〇8〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8oOOo:Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;->〇o00〇〇Oo(Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private O8o0〇(Ljava/lang/String;Lcom/intsig/camscanner/datastruct/FolderItem;)Landroid/view/View;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const v1, 0x7f0d057e

    .line 8
    .line 9
    .line 10
    const/4 v2, 0x0

    .line 11
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const v1, 0x7f0a1717

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    check-cast v1, Landroid/widget/TextView;

    .line 23
    .line 24
    const v2, 0x7f0a191e

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    check-cast v2, Landroid/widget/TextView;

    .line 32
    .line 33
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 34
    .line 35
    .line 36
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    .line 37
    .line 38
    const/4 v1, -0x1

    .line 39
    const/4 v3, -0x2

    .line 40
    invoke-direct {p1, v1, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 44
    .line 45
    .line 46
    new-instance p1, Lo0Oo/〇000O0;

    .line 47
    .line 48
    invoke-direct {p1, p0, p2}, Lo0Oo/〇000O0;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;Lcom/intsig/camscanner/datastruct/FolderItem;)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {v2, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    .line 53
    .line 54
    return-object v0
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private O8oO0()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO8ooO8〇:Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;->〇080()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private O8ooO8o()V
    .locals 5

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/tsapp/sync/AppConfigJson;->isShowGptEntrance()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 13
    .line 14
    const v1, 0x7f0a0c5e

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, Landroid/widget/LinearLayout;

    .line 22
    .line 23
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    instance-of v2, v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 28
    .line 29
    if-eqz v2, :cond_1

    .line 30
    .line 31
    move-object v2, v1

    .line 32
    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 33
    .line 34
    const/4 v3, 0x6

    .line 35
    const v4, 0x7f0a0bff

    .line 36
    .line 37
    .line 38
    invoke-virtual {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 39
    .line 40
    .line 41
    const/16 v3, 0x8

    .line 42
    .line 43
    invoke-virtual {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 44
    .line 45
    .line 46
    const/16 v3, 0x14

    .line 47
    .line 48
    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 49
    .line 50
    .line 51
    const/high16 v3, 0x41f00000    # 30.0f

    .line 52
    .line 53
    invoke-static {v3}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 54
    .line 55
    .line 56
    move-result v3

    .line 57
    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 58
    .line 59
    const/4 v3, 0x0

    .line 60
    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 61
    .line 62
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 63
    .line 64
    .line 65
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOo〇8o008:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 66
    .line 67
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    instance-of v1, v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 72
    .line 73
    if-eqz v1, :cond_2

    .line 74
    .line 75
    move-object v1, v0

    .line 76
    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 77
    .line 78
    const/high16 v2, 0x41800000    # 16.0f

    .line 79
    .line 80
    invoke-static {v2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 81
    .line 82
    .line 83
    move-result v2

    .line 84
    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 85
    .line 86
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOo〇8o008:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 87
    .line 88
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 89
    .line 90
    .line 91
    :cond_2
    return-void
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private O8o〇o(JI)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "id="

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    const/4 v1, 0x1

    .line 30
    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->o800o8O(JIZ)V

    .line 31
    .line 32
    .line 33
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 34
    .line 35
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇80〇808〇O()I

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0OO8(I)V

    .line 44
    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private O8〇()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o0oO()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 10
    .line 11
    const-string v1, "addCopositeItem>>> is illegal !!!"

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO80OOO〇()Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 26
    .line 27
    const-string v1, "isViewOnlyByUploadRole"

    .line 28
    .line 29
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    return-void

    .line 33
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOoO8OO〇:Lcom/intsig/menu/MenuItem;

    .line 34
    .line 35
    if-nez v0, :cond_2

    .line 36
    .line 37
    new-instance v0, Lcom/intsig/menu/MenuItem;

    .line 38
    .line 39
    const/4 v2, 0x6

    .line 40
    const v1, 0x7f130113

    .line 41
    .line 42
    .line 43
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v3

    .line 47
    const/4 v4, -0x1

    .line 48
    const/4 v5, 0x0

    .line 49
    const v6, 0x7f080d9f

    .line 50
    .line 51
    .line 52
    move-object v1, v0

    .line 53
    invoke-direct/range {v1 .. v6}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;IZI)V

    .line 54
    .line 55
    .line 56
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOoO8OO〇:Lcom/intsig/menu/MenuItem;

    .line 57
    .line 58
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇80o:Lcom/intsig/menu/PopupMenuItems;

    .line 59
    .line 60
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOoO8OO〇:Lcom/intsig/menu/MenuItem;

    .line 61
    .line 62
    invoke-virtual {v0, v1}, Lcom/intsig/menu/PopupMenuItems;->〇o00〇〇Oo(Lcom/intsig/menu/MenuItem;)V

    .line 63
    .line 64
    .line 65
    return-void
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic O8〇8〇O80(Lcom/intsig/camscanner/pagelist/PageListFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o8〇o(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private O8〇o0〇〇(Lcom/intsig/callback/Callback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/callback/Callback<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 1
    new-instance v0, Lo0Oo/Ooo;

    .line 2
    .line 3
    invoke-direct {v0, p0, p1}, Lo0Oo/Ooo;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;Lcom/intsig/callback/Callback;)V

    .line 4
    .line 5
    .line 6
    invoke-static {v0}, Lcom/intsig/thread/ThreadPoolSingleton;->〇080(Ljava/lang/Runnable;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O8〇o0〇〇8(Lcom/intsig/camscanner/pagelist/PageListFragment;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO〇00〇8oO:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private OO00〇0o〇〇(I)V
    .locals 16

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oO〇oo()Z

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    const/4 v2, 0x1

    .line 8
    const/4 v3, 0x0

    .line 9
    if-eqz v1, :cond_1

    .line 10
    .line 11
    iget-object v1, v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 12
    .line 13
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->OO0o〇〇〇〇0()Ljava/util/HashSet;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    .line 22
    .line 23
    .line 24
    move-result v4

    .line 25
    new-array v4, v4, [J

    .line 26
    .line 27
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    const/4 v5, 0x0

    .line 32
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 33
    .line 34
    .line 35
    move-result v6

    .line 36
    if-eqz v6, :cond_0

    .line 37
    .line 38
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 39
    .line 40
    .line 41
    move-result-object v6

    .line 42
    check-cast v6, Ljava/lang/Long;

    .line 43
    .line 44
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    .line 45
    .line 46
    .line 47
    move-result-wide v6

    .line 48
    aput-wide v6, v4, v5

    .line 49
    .line 50
    add-int/2addr v5, v2

    .line 51
    goto :goto_0

    .line 52
    :cond_0
    move-object v9, v4

    .line 53
    const/4 v12, 0x1

    .line 54
    goto :goto_1

    .line 55
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O00〇o00()Ljava/util/ArrayList;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    invoke-static {v1}, Lcom/intsig/camscanner/util/Util;->O00(Ljava/util/List;)[J

    .line 60
    .line 61
    .line 62
    move-result-object v4

    .line 63
    move-object v9, v4

    .line 64
    const/4 v12, 0x0

    .line 65
    :goto_1
    array-length v1, v9

    .line 66
    if-lez v1, :cond_4

    .line 67
    .line 68
    const/4 v1, 0x5

    .line 69
    move/from16 v4, p1

    .line 70
    .line 71
    if-ne v4, v1, :cond_2

    .line 72
    .line 73
    const/4 v15, 0x1

    .line 74
    goto :goto_2

    .line 75
    :cond_2
    const/4 v15, 0x0

    .line 76
    :goto_2
    iget-object v5, v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 77
    .line 78
    iget-object v1, v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 79
    .line 80
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 81
    .line 82
    .line 83
    move-result-wide v6

    .line 84
    iget-object v1, v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 85
    .line 86
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0〇oo()Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v8

    .line 90
    iget-boolean v1, v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇〇0o〇〇0:Z

    .line 91
    .line 92
    if-eqz v1, :cond_3

    .line 93
    .line 94
    sget-object v1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->FROM_VIEW_PDF:Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;

    .line 95
    .line 96
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->getEntrance()I

    .line 97
    .line 98
    .line 99
    move-result v1

    .line 100
    goto :goto_3

    .line 101
    :cond_3
    sget-object v1, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->FROM_SHARE:Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;

    .line 102
    .line 103
    invoke-virtual {v1}, Lcom/intsig/camscanner/pdf/preshare/PdfEditingEntrance;->getEntrance()I

    .line 104
    .line 105
    .line 106
    move-result v1

    .line 107
    :goto_3
    move v10, v1

    .line 108
    const-string v11, "cs_list"

    .line 109
    .line 110
    const/4 v13, 0x0

    .line 111
    iget-object v1, v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 112
    .line 113
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇0〇0o8()Ljava/lang/String;

    .line 114
    .line 115
    .line 116
    move-result-object v14

    .line 117
    invoke-static/range {v5 .. v15}, Lcom/intsig/camscanner/app/IntentUtil;->o0ooO(Landroid/app/Activity;JLjava/lang/String;[JILjava/lang/String;ZZLjava/lang/String;Z)V

    .line 118
    .line 119
    .line 120
    iput-boolean v3, v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇〇0o〇〇0:Z

    .line 121
    .line 122
    :cond_4
    return-void
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic OO0O(Lcom/intsig/camscanner/pagelist/PageListFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇00O00o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OO0o(Lcom/intsig/camscanner/pagelist/PageListFragment;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇o0〇8:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private OO0o88()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    const v1, 0x7f0a04fc

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    check-cast v0, Landroidx/cardview/widget/CardView;

    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 13
    .line 14
    const v2, 0x7f0a0865

    .line 15
    .line 16
    .line 17
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    check-cast v1, Landroid/widget/ImageView;

    .line 22
    .line 23
    new-instance v2, Lo0Oo/〇o0O0O8;

    .line 24
    .line 25
    invoke-direct {v2, p0, v0}, Lo0Oo/〇o0O0O8;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;Landroidx/cardview/widget/CardView;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 32
    .line 33
    iget-object v0, v0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O000:Landroidx/lifecycle/MutableLiveData;

    .line 34
    .line 35
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    new-instance v2, Lo0Oo/〇〇o8;

    .line 40
    .line 41
    invoke-direct {v2, p0}, Lo0Oo/〇〇o8;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0, v1, v2}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 45
    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 48
    .line 49
    iget-object v0, v0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇80:Landroidx/lifecycle/MutableLiveData;

    .line 50
    .line 51
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    new-instance v2, Lcom/intsig/camscanner/pagelist/PageListFragment$8;

    .line 56
    .line 57
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/pagelist/PageListFragment$8;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {v0, v1, v2}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 61
    .line 62
    .line 63
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private OO0〇()V
    .locals 1

    .line 1
    const v0, 0x7f130385

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇〇0880(I)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static synthetic OO0〇O(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OO8〇O8(Lcom/intsig/camscanner/pagelist/PageListFragment;Lcom/intsig/menu/PopupListMenu;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇o88o08〇:Lcom/intsig/menu/PopupListMenu;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private OOO0o〇()V
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇o〇:Landroid/view/View;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/16 v1, 0x8

    .line 8
    .line 9
    if-eq v0, v1, :cond_0

    .line 10
    .line 11
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    .line 12
    .line 13
    const/high16 v3, 0x3f800000    # 1.0f

    .line 14
    .line 15
    const/4 v4, 0x0

    .line 16
    const/high16 v5, 0x3f800000    # 1.0f

    .line 17
    .line 18
    const/4 v6, 0x0

    .line 19
    const/4 v7, 0x1

    .line 20
    const/high16 v8, 0x3f000000    # 0.5f

    .line 21
    .line 22
    const/4 v9, 0x1

    .line 23
    const/high16 v10, 0x3f000000    # 0.5f

    .line 24
    .line 25
    move-object v2, v0

    .line 26
    invoke-direct/range {v2 .. v10}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 27
    .line 28
    .line 29
    const-wide/16 v2, 0xc8

    .line 30
    .line 31
    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 32
    .line 33
    .line 34
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇o〇:Landroid/view/View;

    .line 35
    .line 36
    invoke-virtual {v2, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 37
    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇o〇:Landroid/view/View;

    .line 40
    .line 41
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 42
    .line 43
    .line 44
    :cond_0
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic OOO80〇〇88()V
    .locals 1

    .line 1
    const/4 v0, 0x4

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oo〇88(I)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private OOOo〇(Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 8
    .line 9
    .line 10
    move-result-wide v2

    .line 11
    const-wide/16 v4, -0x1

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 14
    .line 15
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo8o〇o〇()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v6

    .line 19
    const/4 v7, 0x0

    .line 20
    const/4 v8, 0x0

    .line 21
    const/4 v9, 0x0

    .line 22
    move-object v1, p1

    .line 23
    invoke-static/range {v0 .. v9}, Lcom/intsig/camscanner/BatchModeActivity;->o88o88(Landroid/content/Context;Ljava/util/ArrayList;JJLjava/lang/String;Ljava/lang/String;ZZ)Landroid/content/Intent;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    const/16 v0, 0x3f0

    .line 28
    .line 29
    invoke-virtual {p0, p1, v0}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private OOO〇()Ljava/lang/Boolean;
    .locals 3

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 9
    .line 10
    .line 11
    move-result-wide v1

    .line 12
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 17
    .line 18
    .line 19
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 20
    .line 21
    invoke-static {v1, v0}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇000O0(Landroid/content/Context;Ljava/util/ArrayList;)Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    return-object v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic OOo00(Lcom/intsig/camscanner/pagelist/PageListFragment;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O00〇o00()Ljava/util/ArrayList;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OO〇000(Lcom/intsig/camscanner/pagelist/PageListFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇o0o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic OO〇80oO〇(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroidx/appcompat/widget/AppCompatTextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mToolbarTitle:Landroidx/appcompat/widget/AppCompatTextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic OO〇〇o0oO(Lcom/intsig/camscanner/pagelist/PageListFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇o8〇〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private Oo0(Z)V
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->Oo80:Ljava/lang/String;

    .line 4
    .line 5
    invoke-virtual {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setToolbarTitle(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇o00〇〇Oo()V

    .line 16
    .line 17
    .line 18
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 19
    .line 20
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0〇oo()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    invoke-virtual {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setToolbarTitle(Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 28
    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    .line 30
    .line 31
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    .line 33
    .line 34
    const-string v1, "updateTitleViewOnEditModeChanged: title = "

    .line 35
    .line 36
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 40
    .line 41
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0〇oo()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    :goto_0
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private Oo088O〇8〇()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇8o8o〇()Ljava/util/ArrayList;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-lez v1, :cond_0

    .line 16
    .line 17
    sget-object v1, Lkotlinx/coroutines/GlobalScope;->o0:Lkotlinx/coroutines/GlobalScope;

    .line 18
    .line 19
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 20
    .line 21
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 22
    .line 23
    invoke-virtual {v3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 24
    .line 25
    .line 26
    move-result-wide v3

    .line 27
    invoke-static {v1, v2, v3, v4, v0}, Lcom/intsig/camscanner/router/AlbumExt;->〇〇888(Lkotlinx/coroutines/CoroutineScope;Landroid/app/Activity;JLjava/util/ArrayList;)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O00O()V

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO0〇()V

    .line 35
    .line 36
    .line 37
    :goto_0
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static synthetic Oo0O〇8800(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private Oo0o0o8(I)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->isComputingLayout()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    :try_start_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 12
    .line 13
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 14
    .line 15
    .line 16
    goto :goto_0

    .line 17
    :catch_0
    move-exception p1

    .line 18
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 19
    .line 20
    new-instance v1, Ljava/lang/StringBuilder;

    .line 21
    .line 22
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 23
    .line 24
    .line 25
    const-string v2, "safeNotifyAdapterDataSetChanged error; "

    .line 26
    .line 27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_0
    const/4 v0, 0x5

    .line 42
    if-lt p1, v0, :cond_1

    .line 43
    .line 44
    return-void

    .line 45
    :cond_1
    add-int/lit8 p1, p1, 0x1

    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o880:Landroid/os/Handler;

    .line 48
    .line 49
    new-instance v1, Lo0Oo/O0;

    .line 50
    .line 51
    invoke-direct {v1, p0, p1}, Lo0Oo/O0;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;I)V

    .line 52
    .line 53
    .line 54
    const-wide/16 v2, 0x3e8

    .line 55
    .line 56
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 57
    .line 58
    .line 59
    :goto_0
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private synthetic Oo8〇〇ooo(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O80(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static synthetic OoO888()V
    .locals 1

    .line 1
    const/4 v0, 0x2

    .line 2
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->O0oo0〇〇〇O(I)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private OoOO〇(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V
    .locals 5

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "TOP_MENU_SECURITY"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 11
    .line 12
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 13
    .line 14
    .line 15
    move-result-wide v1

    .line 16
    new-instance v3, Lo0Oo/〇8〇0〇o〇O;

    .line 17
    .line 18
    invoke-direct {v3, p0, p1}, Lo0Oo/〇8〇0〇o〇O;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 19
    .line 20
    .line 21
    new-instance v4, Lo0Oo/O〇O〇oO;

    .line 22
    .line 23
    invoke-direct {v4, p0, p1}, Lo0Oo/O〇O〇oO;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 24
    .line 25
    .line 26
    invoke-static {v0, v1, v2, v3, v4}, Lcom/intsig/camscanner/control/DataChecker;->〇80〇808〇O(Landroid/app/Activity;JLcom/intsig/camscanner/control/DataChecker$ActionListener;Lcom/intsig/camscanner/app/DbWaitingListener;)Z

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic OoO〇OOo8o(Lcom/intsig/camscanner/pagelist/PageListFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0880O0〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic Ooo8o(Lcom/intsig/camscanner/pagelist/PageListFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇o0o8Oo()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic OooO〇(Lcom/intsig/camscanner/pagelist/PageListFragment;Ljava/lang/String;Lcom/intsig/app/AlertDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/pagelist/PageListFragment;->Oo〇0o(Ljava/lang/String;Lcom/intsig/app/AlertDialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private OooO〇080()V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_LIST:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->OoOO〇(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private synthetic Oo〇0o(Ljava/lang/String;Lcom/intsig/app/AlertDialog;Landroid/view/View;)V
    .locals 3

    .line 1
    const-string p3, "complete"

    .line 2
    .line 3
    const-string v0, "type"

    .line 4
    .line 5
    const-string v1, "CSTaskCompletePop"

    .line 6
    .line 7
    invoke-static {v1, p3, v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 11
    .line 12
    const p3, 0x7f1300ae

    .line 13
    .line 14
    .line 15
    invoke-virtual {p0, p3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object p3

    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 20
    .line 21
    invoke-static {v0}, Lcom/intsig/camscanner/web/UrlUtil;->〇00〇8(Landroid/content/Context;)Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    const/4 v1, 0x1

    .line 26
    const/4 v2, 0x0

    .line 27
    invoke-static {p1, p3, v0, v1, v2}, Lcom/intsig/webview/util/WebUtil;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {p2}, Lcom/intsig/app/AlertDialog;->dismiss()V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private Oo〇〇〇〇([I)Landroid/view/View;
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_2

    .line 3
    .line 4
    array-length v1, p1

    .line 5
    const/4 v2, 0x2

    .line 6
    if-ne v1, v2, :cond_2

    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇〇o8〇()Landroid/view/View;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    if-nez v1, :cond_0

    .line 13
    .line 14
    return-object v0

    .line 15
    :cond_0
    const v0, 0x7f0a0fcd

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    if-nez v0, :cond_1

    .line 23
    .line 24
    return-object v1

    .line 25
    :cond_1
    invoke-virtual {v0, p1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 26
    .line 27
    .line 28
    move-object v0, v1

    .line 29
    :cond_2
    return-object v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic O〇00O(Lcom/intsig/camscanner/pagelist/PageListFragment;)Lcom/intsig/menu/PopupListMenu;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇o88o08〇:Lcom/intsig/menu/PopupListMenu;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O〇00o08(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getItemAnimator()Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    instance-of v0, v0, Landroidx/recyclerview/widget/SimpleItemAnimator;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 12
    .line 13
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getItemAnimator()Landroidx/recyclerview/widget/RecyclerView$ItemAnimator;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    check-cast v0, Landroidx/recyclerview/widget/SimpleItemAnimator;

    .line 18
    .line 19
    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/SimpleItemAnimator;->setSupportsChangeAnimations(Z)V

    .line 20
    .line 21
    .line 22
    :cond_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic O〇080〇o0(Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8〇0o〇(Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O〇0888o()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇80O8o8O〇:Landroid/widget/PopupWindow;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic O〇0O〇Oo〇o(Lcom/intsig/camscanner/pagelist/PageListFragment;Ljava/lang/String;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇o〇〇88〇8(Ljava/lang/String;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic O〇0o8o8〇(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/widget/TextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O0O:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O〇0o8〇(Lcom/intsig/camscanner/pagelist/PageListFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO00〇0o〇〇(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic O〇8()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private O〇88()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇〇0〇88()J

    .line 4
    .line 5
    .line 6
    move-result-wide v0

    .line 7
    const-wide/16 v2, 0x0

    .line 8
    .line 9
    cmp-long v4, v0, v2

    .line 10
    .line 11
    if-eqz v4, :cond_1

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇〇0〇88()J

    .line 16
    .line 17
    .line 18
    move-result-wide v0

    .line 19
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 20
    .line 21
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->checkPdfSizeIdExist(JLandroid/content/Context;)[I

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    const/4 v1, 0x0

    .line 26
    aget v1, v0, v1

    .line 27
    .line 28
    const/4 v2, 0x1

    .line 29
    const/4 v3, -0x1

    .line 30
    if-eq v1, v3, :cond_0

    .line 31
    .line 32
    aget v0, v0, v2

    .line 33
    .line 34
    if-ne v0, v3, :cond_1

    .line 35
    .line 36
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 37
    .line 38
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 39
    .line 40
    invoke-static {v1}, Lcom/intsig/camscanner/provider/ProviderSpHelper;->〇o〇(Landroid/content/Context;)J

    .line 41
    .line 42
    .line 43
    move-result-wide v3

    .line 44
    invoke-virtual {v0, v3, v4}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇Oo〇O(J)V

    .line 45
    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 48
    .line 49
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8O0880(Z)V

    .line 50
    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 53
    .line 54
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o()Landroid/net/Uri;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇00O(Landroid/net/Uri;)V

    .line 59
    .line 60
    .line 61
    :cond_1
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic O〇8O0O80〇(Lcom/intsig/camscanner/pagelist/PageListFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇o008o08O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic O〇8Oo()V
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O8o()Landroid/graphics/Rect;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const/4 v1, 0x2

    .line 8
    new-array v1, v1, [I

    .line 9
    .line 10
    sget-object v2, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 11
    .line 12
    new-instance v3, Ljava/lang/StringBuilder;

    .line 13
    .line 14
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 15
    .line 16
    .line 17
    const-string v4, "positon left="

    .line 18
    .line 19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    const/4 v4, 0x0

    .line 23
    aget v5, v1, v4

    .line 24
    .line 25
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    const-string v5, " top="

    .line 29
    .line 30
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    const/4 v5, 0x1

    .line 34
    aget v6, v1, v5

    .line 35
    .line 36
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v3

    .line 43
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇088O:Landroid/view/View;

    .line 47
    .line 48
    invoke-virtual {v2, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 49
    .line 50
    .line 51
    aget v2, v1, v4

    .line 52
    .line 53
    neg-int v2, v2

    .line 54
    aget v1, v1, v5

    .line 55
    .line 56
    neg-int v1, v1

    .line 57
    invoke-virtual {v0, v2, v1}, Landroid/graphics/Rect;->offset(II)V

    .line 58
    .line 59
    .line 60
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇088O:Landroid/view/View;

    .line 61
    .line 62
    const v2, 0x7f0a0d06

    .line 63
    .line 64
    .line 65
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 66
    .line 67
    .line 68
    move-result-object v1

    .line 69
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 70
    .line 71
    .line 72
    move-result-object v2

    .line 73
    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 74
    .line 75
    iget v3, v0, Landroid/graphics/Rect;->left:I

    .line 76
    .line 77
    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 78
    .line 79
    iget v3, v0, Landroid/graphics/Rect;->top:I

    .line 80
    .line 81
    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 82
    .line 83
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    .line 84
    .line 85
    .line 86
    move-result v3

    .line 87
    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 88
    .line 89
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    .line 90
    .line 91
    .line 92
    move-result v0

    .line 93
    iput v0, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 94
    .line 95
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 96
    .line 97
    .line 98
    :cond_0
    return-void
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private O〇8O〇()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;-><init>(Landroidx/fragment/app/FragmentActivity;)V

    .line 6
    .line 7
    .line 8
    sget-object v1, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->CsMoreOpenPdf:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->O8(Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;)Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    new-instance v1, Lo0Oo/OO8oO0o〇;

    .line 15
    .line 16
    invoke-direct {v1, p0}, Lo0Oo/OO8oO0o〇;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function2;)Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 24
    .line 25
    const v2, 0x7f130b01

    .line 26
    .line 27
    .line 28
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->Oo08(Ljava/lang/String;)Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver$CsPdfRiverBuilder;->〇o〇()Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lcom/intsig/camscanner/pdf/pdfriver/CsPdfRiver;->〇0〇O0088o()V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic O〇8〇008(Lcom/intsig/camscanner/pagelist/PageListFragment;Ljava/lang/Long;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇0〇〇8O(Ljava/lang/Long;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic O〇O800oo(Lcom/intsig/camscanner/pagelist/PageListFragment;II)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oO8O(II)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic O〇O88(Landroidx/fragment/app/FragmentActivity;Lkotlin/jvm/functions/Function1;)Lkotlin/Unit;
    .locals 0

    .line 1
    iput-object p2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇o〇88〇8:Lkotlin/jvm/functions/Function1;

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 4
    .line 5
    const/4 p2, 0x7

    .line 6
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇008〇o0〇〇(I)V

    .line 7
    .line 8
    .line 9
    const/4 p1, 0x0

    .line 10
    return-object p1
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O〇O〇88O8O(Landroid/net/Uri;)V
    .locals 4

    .line 1
    new-instance v0, Landroid/content/Intent;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    const-class v2, Lcom/intsig/camscanner/ImageScannerActivity;

    .line 6
    .line 7
    const-string v3, "com.intsig.camscanner.NEW_PAGE"

    .line 8
    .line 9
    invoke-direct {v0, v3, p1, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 10
    .line 11
    .line 12
    const-string p1, "scanner_image_src"

    .line 13
    .line 14
    const/4 v1, 0x1

    .line 15
    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 16
    .line 17
    .line 18
    const/16 p1, 0x3e9

    .line 19
    .line 20
    invoke-virtual {p0, v0, p1}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic O〇o8(Lcom/intsig/camscanner/pagelist/PageListFragment;Landroid/widget/TextView;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O0O:Landroid/widget/TextView;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static synthetic O〇oo8O80(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O〇〇O()Lcom/intsig/camscanner/pagelist/bubble/PageListBubbleControl;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOO8:Lcom/intsig/camscanner/pagelist/bubble/PageListBubbleControl;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/pagelist/bubble/PageListBubbleControl;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 8
    .line 9
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 10
    .line 11
    invoke-direct {v0, p0, v1, v2}, Lcom/intsig/camscanner/pagelist/bubble/PageListBubbleControl;-><init>(Landroidx/fragment/app/Fragment;Landroid/app/Activity;Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;)V

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOO8:Lcom/intsig/camscanner/pagelist/bubble/PageListBubbleControl;

    .line 15
    .line 16
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOO8:Lcom/intsig/camscanner/pagelist/bubble/PageListBubbleControl;

    .line 17
    .line 18
    return-object v0
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic O〇〇O80o8(Lcom/intsig/camscanner/pagelist/PageListFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->OOO80〇〇88()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O〇〇o8O(Lcom/intsig/camscanner/pagelist/PageListFragment;)Lcom/google/android/material/floatingactionbutton/FloatingActionButton;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOo〇8o008:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic o00(J)V
    .locals 0

    .line 1
    long-to-int p2, p1

    .line 2
    invoke-static {p2}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇〇ooOO(I)V

    .line 3
    .line 4
    .line 5
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇0〇o()V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic o008(Lcom/intsig/camscanner/datastruct/FolderItem;Landroid/view/View;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/FolderItem;->oo88o8O()I

    .line 2
    .line 3
    .line 4
    move-result p2

    .line 5
    invoke-static {p2}, Lcom/intsig/camscanner/scenariodir/logagent/ScenarioLogDirAgent;->〇80〇808〇O(I)V

    .line 6
    .line 7
    .line 8
    new-instance p2, Landroid/content/Intent;

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 11
    .line 12
    const-class v1, Lcom/intsig/camscanner/targetdir/TargetDirActivity;

    .line 13
    .line 14
    invoke-direct {p2, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 15
    .line 16
    .line 17
    const-string v0, "args_parent_folder_item"

    .line 18
    .line 19
    invoke-virtual {p2, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private o00o0O〇〇o()Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOoo80oO:Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08〇o0O:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 8
    .line 9
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 10
    .line 11
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 12
    .line 13
    .line 14
    move-result-wide v2

    .line 15
    invoke-direct {v0, v1, v2, v3, p0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;-><init>(Landroid/view/View;JLcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 16
    .line 17
    .line 18
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOoo80oO:Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;

    .line 19
    .line 20
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOoo80oO:Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;

    .line 21
    .line 22
    return-object v0
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private o00oooo()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    const v1, 0x7f0a0f18

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    check-cast v0, Landroidx/recyclerview/widget/RecyclerView;

    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O0〇O80ooo()Landroidx/recyclerview/widget/RecyclerView$RecycledViewPool;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setRecycledViewPool(Landroidx/recyclerview/widget/RecyclerView$RecycledViewPool;)V

    .line 19
    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 22
    .line 23
    const v1, 0x7f0a0e63

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    check-cast v0, Landroid/widget/ProgressBar;

    .line 31
    .line 32
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 33
    .line 34
    const v2, 0x7f0a0ea1

    .line 35
    .line 36
    .line 37
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    check-cast v1, Lcom/intsig/camscanner/view/PullToSyncRecyclerView;

    .line 42
    .line 43
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oo8ooo8O:Lcom/intsig/camscanner/view/PullToSyncRecyclerView;

    .line 44
    .line 45
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    invoke-virtual {v1}, Lcom/intsig/tsapp/sync/AppConfigJson;->isNewLoading()Z

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    if-eqz v1, :cond_0

    .line 54
    .line 55
    new-instance v1, Lcom/intsig/camscanner/view/SyncLottieRefreshHeaderStrategy;

    .line 56
    .line 57
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 58
    .line 59
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oo8ooo8O:Lcom/intsig/camscanner/view/PullToSyncRecyclerView;

    .line 60
    .line 61
    invoke-direct {v1, p0, v2, v3}, Lcom/intsig/camscanner/view/SyncLottieRefreshHeaderStrategy;-><init>(Ljava/lang/Object;Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 62
    .line 63
    .line 64
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O〇〇O8:Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;

    .line 65
    .line 66
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oo8ooo8O:Lcom/intsig/camscanner/view/PullToSyncRecyclerView;

    .line 67
    .line 68
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->setIHeaderViewStrategy(Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;)V

    .line 69
    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_0
    new-instance v1, Lcom/intsig/camscanner/view/header/DocListHeaderViewStrategy;

    .line 73
    .line 74
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 75
    .line 76
    .line 77
    move-result-object v2

    .line 78
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oo8ooo8O:Lcom/intsig/camscanner/view/PullToSyncRecyclerView;

    .line 79
    .line 80
    invoke-direct {v1, p0, v2, v3}, Lcom/intsig/camscanner/view/header/DocListHeaderViewStrategy;-><init>(Ljava/lang/Object;Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 81
    .line 82
    .line 83
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oo8ooo8O:Lcom/intsig/camscanner/view/PullToSyncRecyclerView;

    .line 84
    .line 85
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->setIHeaderViewStrategy(Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;)V

    .line 86
    .line 87
    .line 88
    const v2, -0xe64356

    .line 89
    .line 90
    .line 91
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/view/header/DocListHeaderViewStrategy;->〇〇808〇(I)V

    .line 92
    .line 93
    .line 94
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O〇〇O8:Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;

    .line 95
    .line 96
    :goto_0
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oo8ooo8O:Lcom/intsig/camscanner/view/PullToSyncRecyclerView;

    .line 97
    .line 98
    const/4 v2, 0x0

    .line 99
    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 100
    .line 101
    .line 102
    new-instance v1, Lcom/intsig/camscanner/view/AbstractPullToSyncView$PullToSyncAssistant;

    .line 103
    .line 104
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 105
    .line 106
    .line 107
    move-result-object v2

    .line 108
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oo8ooo8O:Lcom/intsig/camscanner/view/PullToSyncRecyclerView;

    .line 109
    .line 110
    invoke-direct {v1, v2, v3, v0}, Lcom/intsig/camscanner/view/AbstractPullToSyncView$PullToSyncAssistant;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/view/AbstractPullToSyncView;Landroid/widget/ProgressBar;)V

    .line 111
    .line 112
    .line 113
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇oO:Lcom/intsig/camscanner/view/AbstractPullToSyncView$PullToSyncAssistant;

    .line 114
    .line 115
    invoke-virtual {v1}, Lcom/intsig/camscanner/view/AbstractPullToSyncView$PullToSyncAssistant;->〇〇808〇()V

    .line 116
    .line 117
    .line 118
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 119
    .line 120
    const v1, 0x7f0a09f0

    .line 121
    .line 122
    .line 123
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 124
    .line 125
    .line 126
    move-result-object v0

    .line 127
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇o〇:Landroid/view/View;

    .line 128
    .line 129
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    .line 131
    .line 132
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 133
    .line 134
    const v1, 0x7f0a05d5

    .line 135
    .line 136
    .line 137
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 138
    .line 139
    .line 140
    move-result-object v0

    .line 141
    check-cast v0, Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 142
    .line 143
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08〇o0O:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 144
    .line 145
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 146
    .line 147
    const v1, 0x7f0a05db

    .line 148
    .line 149
    .line 150
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 151
    .line 152
    .line 153
    move-result-object v0

    .line 154
    check-cast v0, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 155
    .line 156
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOo〇8o008:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 157
    .line 158
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 159
    .line 160
    const v1, 0x7f0a1a96

    .line 161
    .line 162
    .line 163
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 164
    .line 165
    .line 166
    move-result-object v0

    .line 167
    check-cast v0, Landroid/view/ViewStub;

    .line 168
    .line 169
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇O:Landroid/view/ViewStub;

    .line 170
    .line 171
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO0o88()V

    .line 172
    .line 173
    .line 174
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 175
    .line 176
    invoke-static {v0}, Lcom/intsig/camscanner/util/DarkModeUtils;->〇080(Landroid/content/Context;)Z

    .line 177
    .line 178
    .line 179
    move-result v0

    .line 180
    if-eqz v0, :cond_1

    .line 181
    .line 182
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOo〇8o008:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 183
    .line 184
    const/4 v1, 0x0

    .line 185
    invoke-virtual {v0, v1}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setElevation(F)V

    .line 186
    .line 187
    .line 188
    :cond_1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 189
    .line 190
    const v1, 0x7f0a04e1

    .line 191
    .line 192
    .line 193
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 194
    .line 195
    .line 196
    move-result-object v0

    .line 197
    check-cast v0, Lcom/intsig/comm/widget/CustomTextView;

    .line 198
    .line 199
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOo0:Lcom/intsig/comm/widget/CustomTextView;

    .line 200
    .line 201
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08〇o0O:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 202
    .line 203
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 204
    .line 205
    .line 206
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 207
    .line 208
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0O〇OOo()Z

    .line 209
    .line 210
    .line 211
    move-result v0

    .line 212
    if-eqz v0, :cond_2

    .line 213
    .line 214
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08〇o0O:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 215
    .line 216
    const/16 v1, 0x8

    .line 217
    .line 218
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;->setVisibility(I)V

    .line 219
    .line 220
    .line 221
    :cond_2
    const/4 v0, 0x1

    .line 222
    new-array v0, v0, [Landroid/view/View;

    .line 223
    .line 224
    const/4 v1, 0x0

    .line 225
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOo〇8o008:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 226
    .line 227
    aput-object v2, v0, v1

    .line 228
    .line 229
    invoke-virtual {p0, v0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 230
    .line 231
    .line 232
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 233
    .line 234
    new-instance v1, Lcom/intsig/camscanner/pagelist/PageListFragment$7;

    .line 235
    .line 236
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/pagelist/PageListFragment$7;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 237
    .line 238
    .line 239
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    .line 240
    .line 241
    .line 242
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8〇OO0〇0o:Z

    .line 243
    .line 244
    if-eqz v0, :cond_3

    .line 245
    .line 246
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8〇8oooO〇()V

    .line 247
    .line 248
    .line 249
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 250
    .line 251
    const/16 v1, 0x60

    .line 252
    .line 253
    invoke-static {v0, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 254
    .line 255
    .line 256
    move-result v0

    .line 257
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8o8o(I)V

    .line 258
    .line 259
    .line 260
    :cond_3
    return-void
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public static synthetic o00〇88〇08(Lcom/intsig/camscanner/pagelist/PageListFragment;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇8OO()Lkotlin/Unit;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic o08(Ljava/lang/String;Ljava/lang/String;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOO〇o〇0O(Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    const/4 p1, 0x0

    .line 5
    return-object p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic o088O8800(Lcom/intsig/camscanner/pagelist/PageListFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->OooO〇080()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o088〇〇()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "F - go2ChangePaperProperty"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    sget-object v0, Lcom/intsig/camscanner/paper/PaperPropertySelectFullScreenActivity;->〇O〇〇O8:Lcom/intsig/camscanner/paper/PaperPropertySelectFullScreenActivity$Companion;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 11
    .line 12
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇008〇oo()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    const/16 v2, 0x401

    .line 17
    .line 18
    invoke-virtual {v0, p0, v2, v1}, Lcom/intsig/camscanner/paper/PaperPropertySelectFullScreenActivity$Companion;->〇o00〇〇Oo(Landroidx/fragment/app/Fragment;ILjava/lang/String;)Landroid/content/Intent;

    .line 19
    .line 20
    .line 21
    return-void
.end method

.method private o08〇808()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    instance-of v1, v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 8
    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    check-cast v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 12
    .line 13
    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->getItemCount()I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    const/4 v2, 0x1

    .line 24
    sub-int/2addr v1, v2

    .line 25
    if-ne v0, v1, :cond_0

    .line 26
    .line 27
    return v2

    .line 28
    :cond_0
    const/4 v0, 0x0

    .line 29
    return v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic o0O(Landroidx/cardview/widget/CardView;Landroid/view/View;)V
    .locals 3

    .line 1
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    check-cast v0, Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 6
    .line 7
    invoke-virtual {p2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 8
    .line 9
    .line 10
    move-result-wide v1

    .line 11
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-virtual {p2, v0, v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0O〇O00O(Lcom/intsig/mvp/activity/BaseChangeActivity;Ljava/lang/Long;)V

    .line 16
    .line 17
    .line 18
    const/16 p2, 0x8

    .line 19
    .line 20
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o0O0O〇〇〇0(Lcom/intsig/camscanner/pagelist/PageListFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O〇o0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o0O8o00(Z)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InflateParams"
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_2

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇0〇o:Landroid/app/Dialog;

    .line 4
    .line 5
    if-nez p1, :cond_0

    .line 6
    .line 7
    new-instance p1, Landroid/app/Dialog;

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 10
    .line 11
    const v1, 0x7f1401d6

    .line 12
    .line 13
    .line 14
    invoke-direct {p1, v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 15
    .line 16
    .line 17
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇0〇o:Landroid/app/Dialog;

    .line 18
    .line 19
    const/4 v0, 0x1

    .line 20
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 21
    .line 22
    .line 23
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇088O:Landroid/view/View;

    .line 24
    .line 25
    if-nez p1, :cond_1

    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 28
    .line 29
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    const v0, 0x7f0d0755

    .line 34
    .line 35
    .line 36
    const/4 v1, 0x0

    .line 37
    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇088O:Landroid/view/View;

    .line 42
    .line 43
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇088O:Landroid/view/View;

    .line 44
    .line 45
    new-instance v0, Lo0Oo/〇0;

    .line 46
    .line 47
    invoke-direct {v0, p0}, Lo0Oo/〇0;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 48
    .line 49
    .line 50
    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 51
    .line 52
    .line 53
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇0〇o:Landroid/app/Dialog;

    .line 54
    .line 55
    invoke-virtual {p1}, Landroid/app/Dialog;->isShowing()Z

    .line 56
    .line 57
    .line 58
    move-result p1

    .line 59
    if-nez p1, :cond_2

    .line 60
    .line 61
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇0〇o:Landroid/app/Dialog;

    .line 62
    .line 63
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇088O:Landroid/view/View;

    .line 64
    .line 65
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 66
    .line 67
    .line 68
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇088O:Landroid/view/View;

    .line 69
    .line 70
    new-instance v0, Lo0Oo/o88〇OO08〇;

    .line 71
    .line 72
    invoke-direct {v0, p0}, Lo0Oo/o88〇OO08〇;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 73
    .line 74
    .line 75
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    .line 77
    .line 78
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇0〇o:Landroid/app/Dialog;

    .line 79
    .line 80
    invoke-virtual {p1}, Landroid/app/Dialog;->show()V

    .line 81
    .line 82
    .line 83
    :cond_2
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method static bridge synthetic o0OO(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/os/Handler;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o880:Landroid/os/Handler;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o0Oo(Lcom/intsig/camscanner/pagelist/PageListFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O8O〇8〇〇8〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o0Oo〇(Lcom/intsig/camscanner/pagelist/newpagelist/data/PageListRecommendEntity;)V
    .locals 11

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/NewUserGuideCleaner;->Oo08()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/mainmenu/NewUserGuideCleaner;->〇080:Lcom/intsig/camscanner/mainmenu/NewUserGuideCleaner;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/NewUserGuideCleaner;->〇o〇()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    return-void

    .line 16
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->ooo0〇080()Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    return-void

    .line 23
    :cond_1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 24
    .line 25
    const v1, 0x7f0a04fc

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    check-cast v0, Landroidx/cardview/widget/CardView;

    .line 33
    .line 34
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 35
    .line 36
    const v2, 0x7f0a08f6

    .line 37
    .line 38
    .line 39
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    move-object v4, v1

    .line 44
    check-cast v4, Landroid/widget/ImageView;

    .line 45
    .line 46
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 47
    .line 48
    const v2, 0x7f0a08f5

    .line 49
    .line 50
    .line 51
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    move-object v5, v1

    .line 56
    check-cast v5, Landroid/widget/ImageView;

    .line 57
    .line 58
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 59
    .line 60
    const v2, 0x7f0a127f

    .line 61
    .line 62
    .line 63
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 64
    .line 65
    .line 66
    move-result-object v1

    .line 67
    check-cast v1, Landroid/widget/TextView;

    .line 68
    .line 69
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 70
    .line 71
    const v3, 0x7f0a1450

    .line 72
    .line 73
    .line 74
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 75
    .line 76
    .line 77
    move-result-object v2

    .line 78
    check-cast v2, Landroid/widget/TextView;

    .line 79
    .line 80
    iget-object v3, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 81
    .line 82
    const v6, 0x7f0a144a

    .line 83
    .line 84
    .line 85
    invoke-virtual {v3, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 86
    .line 87
    .line 88
    move-result-object v3

    .line 89
    move-object v6, v3

    .line 90
    check-cast v6, Landroid/widget/TextView;

    .line 91
    .line 92
    instance-of v3, p1, Lcom/intsig/camscanner/sharedir/recommed/RecommendSceneEntity;

    .line 93
    .line 94
    const/4 v7, 0x0

    .line 95
    if-eqz v3, :cond_3

    .line 96
    .line 97
    sget-object v3, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 98
    .line 99
    const-string v8, "showSnackBarRecommend: is RecommendSceneEntity!"

    .line 100
    .line 101
    invoke-static {v3, v8}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    .line 103
    .line 104
    check-cast p1, Lcom/intsig/camscanner/sharedir/recommed/RecommendSceneEntity;

    .line 105
    .line 106
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 107
    .line 108
    .line 109
    move-result v3

    .line 110
    if-eqz v3, :cond_2

    .line 111
    .line 112
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 113
    .line 114
    .line 115
    const v3, 0x7f0807ff

    .line 116
    .line 117
    .line 118
    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 119
    .line 120
    .line 121
    const/16 v3, 0x8

    .line 122
    .line 123
    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 124
    .line 125
    .line 126
    invoke-virtual {p1}, Lcom/intsig/camscanner/sharedir/recommed/RecommendSceneEntity;->getType()Ljava/lang/String;

    .line 127
    .line 128
    .line 129
    move-result-object v3

    .line 130
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 131
    .line 132
    .line 133
    move-result v3

    .line 134
    if-nez v3, :cond_2

    .line 135
    .line 136
    invoke-virtual {p1}, Lcom/intsig/camscanner/sharedir/recommed/RecommendSceneEntity;->getType()Ljava/lang/String;

    .line 137
    .line 138
    .line 139
    move-result-object v3

    .line 140
    invoke-static {v3}, Lcom/intsig/camscanner/sharedir/recommed/ShareDirLogAgentHelper;->OO0o〇〇(Ljava/lang/String;)V

    .line 141
    .line 142
    .line 143
    :cond_2
    invoke-virtual {p1}, Lcom/intsig/camscanner/sharedir/recommed/RecommendSceneEntity;->getTitle()Ljava/lang/String;

    .line 144
    .line 145
    .line 146
    move-result-object v3

    .line 147
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 148
    .line 149
    .line 150
    invoke-virtual {p1}, Lcom/intsig/camscanner/sharedir/recommed/RecommendSceneEntity;->getSubtitle()Ljava/lang/String;

    .line 151
    .line 152
    .line 153
    move-result-object v2

    .line 154
    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    .line 156
    .line 157
    invoke-virtual {p1}, Lcom/intsig/camscanner/sharedir/recommed/RecommendSceneEntity;->getLocalRecommendType()Lcom/intsig/camscanner/sharedir/recommed/RecommendSceneEntity$RecommendType;

    .line 158
    .line 159
    .line 160
    move-result-object v2

    .line 161
    invoke-virtual {v2}, Lcom/intsig/camscanner/sharedir/recommed/RecommendSceneEntity$RecommendType;->〇080()I

    .line 162
    .line 163
    .line 164
    move-result v2

    .line 165
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 166
    .line 167
    .line 168
    new-instance v2, Lo0Oo/ooOO;

    .line 169
    .line 170
    invoke-direct {v2, p0, p1, v0}, Lo0Oo/ooOO;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;Lcom/intsig/camscanner/sharedir/recommed/RecommendSceneEntity;Landroidx/cardview/widget/CardView;)V

    .line 171
    .line 172
    .line 173
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174
    .line 175
    .line 176
    return-void

    .line 177
    :cond_3
    instance-of v3, p1, Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 178
    .line 179
    if-eqz v3, :cond_5

    .line 180
    .line 181
    check-cast p1, Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 182
    .line 183
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 184
    .line 185
    .line 186
    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 187
    .line 188
    .line 189
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/FolderItem;->oo88o8O()I

    .line 190
    .line 191
    .line 192
    move-result v3

    .line 193
    const/16 v8, 0x69

    .line 194
    .line 195
    const/4 v9, 0x1

    .line 196
    if-ne v3, v8, :cond_4

    .line 197
    .line 198
    const v3, 0x7f1303ad

    .line 199
    .line 200
    .line 201
    invoke-virtual {p0, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 202
    .line 203
    .line 204
    move-result-object v3

    .line 205
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 206
    .line 207
    .line 208
    new-array v3, v9, [Ljava/lang/Object;

    .line 209
    .line 210
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/FolderItem;->o800o8O()Ljava/lang/String;

    .line 211
    .line 212
    .line 213
    move-result-object v8

    .line 214
    aput-object v8, v3, v7

    .line 215
    .line 216
    const v7, 0x7f131085

    .line 217
    .line 218
    .line 219
    invoke-virtual {p0, v7, v3}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 220
    .line 221
    .line 222
    move-result-object v3

    .line 223
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    .line 225
    .line 226
    goto :goto_0

    .line 227
    :cond_4
    const v3, 0x7f131089

    .line 228
    .line 229
    .line 230
    invoke-virtual {p0, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 231
    .line 232
    .line 233
    move-result-object v3

    .line 234
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 235
    .line 236
    .line 237
    new-array v3, v9, [Ljava/lang/Object;

    .line 238
    .line 239
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/FolderItem;->o800o8O()Ljava/lang/String;

    .line 240
    .line 241
    .line 242
    move-result-object v8

    .line 243
    aput-object v8, v3, v7

    .line 244
    .line 245
    const v7, 0x7f131086

    .line 246
    .line 247
    .line 248
    invoke-virtual {p0, v7, v3}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 249
    .line 250
    .line 251
    move-result-object v3

    .line 252
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 253
    .line 254
    .line 255
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/FolderItem;->oo88o8O()I

    .line 256
    .line 257
    .line 258
    move-result v2

    .line 259
    invoke-static {v2}, Lcom/intsig/camscanner/scenariodir/logagent/ScenarioLogDirAgent;->O〇8O8〇008(I)V

    .line 260
    .line 261
    .line 262
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 263
    .line 264
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/FolderItem;->oo88o8O()I

    .line 265
    .line 266
    .line 267
    move-result v3

    .line 268
    const/4 v7, 0x0

    .line 269
    const/4 v8, 0x0

    .line 270
    const/4 v9, 0x0

    .line 271
    const/4 v10, 0x0

    .line 272
    invoke-static/range {v2 .. v10}, Lcom/intsig/camscanner/scenariodir/util/ScenarioDirViewUtil;->〇080(Landroid/content/Context;ILandroid/widget/ImageView;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/TextView;Ljava/lang/Integer;Landroid/widget/ImageView;Z)V

    .line 273
    .line 274
    .line 275
    new-instance p1, Lo0Oo/OOO8o〇〇;

    .line 276
    .line 277
    invoke-direct {p1, p0, v0}, Lo0Oo/OOO8o〇〇;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;Landroidx/cardview/widget/CardView;)V

    .line 278
    .line 279
    .line 280
    invoke-virtual {v1, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 281
    .line 282
    .line 283
    :cond_5
    return-void
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private o0o〇〇〇8o()V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 6
    .line 7
    .line 8
    const v1, 0x7f130423

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇〇〇0〇〇0(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 16
    .line 17
    .line 18
    new-instance v1, Ljava/util/ArrayList;

    .line 19
    .line 20
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 21
    .line 22
    .line 23
    const v2, 0x7f13012b

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 31
    .line 32
    .line 33
    const v2, 0x7f13012c

    .line 34
    .line 35
    .line 36
    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v2

    .line 40
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    .line 42
    .line 43
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    new-array v2, v2, [Ljava/lang/CharSequence;

    .line 48
    .line 49
    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 50
    .line 51
    .line 52
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->OO〇80oO〇()Z

    .line 53
    .line 54
    .line 55
    move-result v1

    .line 56
    xor-int/lit8 v1, v1, 0x1

    .line 57
    .line 58
    new-instance v3, Lo0Oo/〇80;

    .line 59
    .line 60
    invoke-direct {v3, p0}, Lo0Oo/〇80;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 61
    .line 62
    .line 63
    invoke-virtual {v0, v2, v1, v3}, Lcom/intsig/app/AlertDialog$Builder;->o〇8([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 64
    .line 65
    .line 66
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 71
    .line 72
    .line 73
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private synthetic o0〇(Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->isComputingLayout()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    sget-object v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 8
    .line 9
    new-instance v2, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v3, "deleteAdItem isComputingLayout="

    .line 15
    .line 16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    if-nez v0, :cond_0

    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 32
    .line 33
    invoke-virtual {v0, p1}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->〇0〇O0088o(Ljava/lang/Object;)Z

    .line 34
    .line 35
    .line 36
    :cond_0
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private o0〇OO008O()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->getItemCount()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-lez v0, :cond_1

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 10
    .line 11
    add-int/lit8 v0, v0, -0x1

    .line 12
    .line 13
    invoke-virtual {v1, v0}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->getItem(I)Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    instance-of v1, v1, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;

    .line 18
    .line 19
    if-eqz v1, :cond_1

    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 22
    .line 23
    invoke-virtual {v1, v0}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->getItem(I)Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    check-cast v1, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;

    .line 28
    .line 29
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;->O8()I

    .line 30
    .line 31
    .line 32
    move-result v2

    .line 33
    if-gtz v2, :cond_0

    .line 34
    .line 35
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 36
    .line 37
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 38
    .line 39
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    .line 40
    .line 41
    .line 42
    move-result v3

    .line 43
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇0(I)I

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 48
    .line 49
    invoke-virtual {v1, v3, v2}, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;->〇80〇808〇O(Landroid/content/Context;I)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;->O8()I

    .line 53
    .line 54
    .line 55
    move-result v2

    .line 56
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 57
    .line 58
    const/16 v4, 0xf

    .line 59
    .line 60
    invoke-static {v3, v4}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 61
    .line 62
    .line 63
    move-result v3

    .line 64
    add-int/2addr v2, v3

    .line 65
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8o8o(I)V

    .line 66
    .line 67
    .line 68
    :cond_0
    iget v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇oO〇〇8o:I

    .line 69
    .line 70
    invoke-virtual {v1, v0, v2}, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;->〇o00〇〇Oo(II)Ljava/lang/Boolean;

    .line 71
    .line 72
    .line 73
    move-result-object v1

    .line 74
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    .line 75
    .line 76
    .line 77
    move-result v1

    .line 78
    if-eqz v1, :cond_1

    .line 79
    .line 80
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 81
    .line 82
    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 83
    .line 84
    .line 85
    :cond_1
    return-void
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic o0〇〇00(Lcom/intsig/camscanner/pagelist/PageListFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇8Oo()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o0〇〇00〇o(Lcom/intsig/camscanner/pagelist/PageListFragment;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oO8o〇o〇8()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic o80(Ljava/lang/Long;)V
    .locals 3

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 5
    .line 6
    new-instance v1, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v2, "observe imageId "

    .line 12
    .line 13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o880:Landroid/os/Handler;

    .line 27
    .line 28
    invoke-virtual {p1}, Ljava/lang/Long;->intValue()I

    .line 29
    .line 30
    .line 31
    move-result p1

    .line 32
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    const/16 v1, 0x68

    .line 37
    .line 38
    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    invoke-virtual {p1}, Landroid/os/Message;->sendToTarget()V

    .line 43
    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private static synthetic o808Oo(Lcom/intsig/callback/Callback;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-interface {p0, p1}, Lcom/intsig/callback/Callback;->call(Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o808o8o08(Lcom/intsig/camscanner/pagelist/PageListFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O0oOo()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic o80oO(Ljava/lang/String;Ljava/lang/String;)Lkotlin/Unit;
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->〇080:Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 6
    .line 7
    .line 8
    move-result-wide v1

    .line 9
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    const-string v2, "PageListFragment.showRenameDlg"

    .line 14
    .line 15
    invoke-virtual {v0, v2, p1, p2, v1}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->OoO8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 16
    .line 17
    .line 18
    invoke-static {p2, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 19
    .line 20
    .line 21
    move-result p2

    .line 22
    if-eqz p2, :cond_0

    .line 23
    .line 24
    sget-object p2, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->UNDEFINED:Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    sget-object p2, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->CUSTOM:Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;

    .line 28
    .line 29
    :goto_0
    invoke-virtual {p2}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->getType()I

    .line 30
    .line 31
    .line 32
    move-result p2

    .line 33
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O8080〇O8o(Ljava/lang/String;I)V

    .line 34
    .line 35
    .line 36
    const/4 p1, 0x0

    .line 37
    return-object p1
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic o88(Lcom/intsig/camscanner/pagelist/PageListFragment;Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o0〇(Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o880(Lcom/intsig/camscanner/pagelist/PageListFragment;Lcom/intsig/camscanner/pagelist/newpagelist/data/PageListRecommendEntity;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o0Oo〇(Lcom/intsig/camscanner/pagelist/newpagelist/data/PageListRecommendEntity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic o88o88(Lcom/intsig/camscanner/pagelist/PageListFragment;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO〇00〇0O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic o88oo〇O(Lcom/intsig/camscanner/pagelist/PageListFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0〇(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic o8O〇008(Lcom/intsig/camscanner/pagelist/PageListFragment;)Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇o〇Oo88:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic o8o0(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o8o0o8(Lcom/intsig/camscanner/pagelist/PageListFragment;Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇o〇Oo88:Lcom/intsig/camscanner/pdf/office/PdfToOfficeConstant$Entrance;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic o8o8〇o(Landroid/content/DialogInterface;I)V
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p2, v0, :cond_0

    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    :cond_0
    sget-object p2, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 6
    .line 7
    new-instance v1, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v2, "go2ChangeShowMode click menu "

    .line 13
    .line 14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-static {p2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 28
    .line 29
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O08O0〇O()Z

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    if-eq v1, v0, :cond_2

    .line 34
    .line 35
    if-eqz v0, :cond_1

    .line 36
    .line 37
    const-string v1, "User Operation: order asc"

    .line 38
    .line 39
    invoke-static {p2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_1
    const-string v1, "User Operation: order desc"

    .line 44
    .line 45
    invoke-static {p2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    :goto_0
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 49
    .line 50
    invoke-virtual {p2, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8〇8〇O80(Z)V

    .line 51
    .line 52
    .line 53
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->OO0〇〇OO〇(Z)V

    .line 54
    .line 55
    .line 56
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 57
    .line 58
    invoke-virtual {p2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O880O〇()V

    .line 59
    .line 60
    .line 61
    :cond_2
    :try_start_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    .line 63
    .line 64
    goto :goto_1

    .line 65
    :catch_0
    move-exception p1

    .line 66
    sget-object p2, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 67
    .line 68
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 69
    .line 70
    .line 71
    :goto_1
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method static synthetic o8oo0OOO(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroidx/appcompat/widget/AppCompatTextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mToolbarTitle:Landroidx/appcompat/widget/AppCompatTextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic o8o〇8(Landroid/view/View;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇0〇o:Landroid/app/Dialog;

    .line 2
    .line 3
    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static synthetic o8〇0o〇(Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;)V
    .locals 0

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇0〇O0088o()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->〇〇808〇()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o8〇8oooO〇()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    const v1, 0x7f0a1095

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    check-cast v0, Landroid/view/ViewStub;

    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇OOoooo:Landroid/view/ViewStub;

    .line 13
    .line 14
    const/4 v1, 0x0

    .line 15
    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 19
    .line 20
    const v1, 0x7f0a0f37

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    check-cast v0, Landroid/widget/ImageView;

    .line 28
    .line 29
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 30
    .line 31
    .line 32
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 33
    .line 34
    const v1, 0x7f0a0f36

    .line 35
    .line 36
    .line 37
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    check-cast v0, Landroid/widget/TextView;

    .line 42
    .line 43
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 44
    .line 45
    const v2, 0x7f0a0f3a

    .line 46
    .line 47
    .line 48
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    check-cast v1, Landroid/widget/TextView;

    .line 53
    .line 54
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O008o8oo()Landroid/text/SpannableString;

    .line 55
    .line 56
    .line 57
    move-result-object v2

    .line 58
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    .line 63
    .line 64
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 65
    .line 66
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8〇o〇88()V

    .line 67
    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private o8〇O〇0O0〇()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇80〇808〇O()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-lez v0, :cond_0

    .line 12
    .line 13
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 14
    .line 15
    const-string v1, "showDeleteDirDialog"

    .line 16
    .line 17
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    new-instance v0, Lcom/intsig/camscanner/business/DataDeleteLogicalUtil;

    .line 21
    .line 22
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 27
    .line 28
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->OO0o〇〇〇〇0()Ljava/util/HashSet;

    .line 33
    .line 34
    .line 35
    move-result-object v2

    .line 36
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 37
    .line 38
    invoke-virtual {v3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0O00oO()Z

    .line 39
    .line 40
    .line 41
    move-result v3

    .line 42
    const/4 v4, 0x2

    .line 43
    invoke-direct {v0, v1, v4, v2, v3}, Lcom/intsig/camscanner/business/DataDeleteLogicalUtil;-><init>(Landroid/content/Context;ILjava/util/Set;Z)V

    .line 44
    .line 45
    .line 46
    const/4 v1, 0x0

    .line 47
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/business/DataDeleteLogicalUtil;->〇o00〇〇Oo(Z)Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    new-instance v1, Lcom/intsig/app/AlertDialog$Builder;

    .line 52
    .line 53
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 54
    .line 55
    .line 56
    move-result-object v2

    .line 57
    invoke-direct {v1, v2}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 58
    .line 59
    .line 60
    const v2, 0x7f131e42

    .line 61
    .line 62
    .line 63
    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object v2

    .line 67
    invoke-virtual {v1, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇〇〇0〇〇0(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 68
    .line 69
    .line 70
    move-result-object v1

    .line 71
    invoke-virtual {v1, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇O00(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    const v1, 0x7f131e36

    .line 76
    .line 77
    .line 78
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object v1

    .line 82
    new-instance v2, Lo0Oo/oO00OOO;

    .line 83
    .line 84
    invoke-direct {v2, p0}, Lo0Oo/oO00OOO;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 85
    .line 86
    .line 87
    invoke-virtual {v0, v1, v2}, Lcom/intsig/app/AlertDialog$Builder;->oo〇(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    const v1, 0x7f13057e

    .line 92
    .line 93
    .line 94
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object v1

    .line 98
    const/4 v2, 0x0

    .line 99
    invoke-virtual {v0, v1, v2}, Lcom/intsig/app/AlertDialog$Builder;->o800o8O(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 100
    .line 101
    .line 102
    move-result-object v0

    .line 103
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 104
    .line 105
    .line 106
    move-result-object v0

    .line 107
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 108
    .line 109
    .line 110
    goto :goto_0

    .line 111
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO0〇()V

    .line 112
    .line 113
    .line 114
    :goto_0
    return-void
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private o8〇〇〇0O(Landroid/view/View;)V
    .locals 10

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇〇0:Landroid/view/View;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    if-nez v0, :cond_1

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 10
    .line 11
    const v2, 0x7f0a1a64

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    check-cast v0, Landroid/view/ViewStub;

    .line 19
    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 26
    .line 27
    const v2, 0x7f0a0d10

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇〇0:Landroid/view/View;

    .line 35
    .line 36
    :cond_1
    iget-object v4, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇〇0:Landroid/view/View;

    .line 37
    .line 38
    if-nez v4, :cond_2

    .line 39
    .line 40
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 41
    .line 42
    const-string v0, "setupBatchProcessTipView rootMarkupGuide == null"

    .line 43
    .line 44
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    return-void

    .line 48
    :cond_2
    sget-object v2, Lcom/intsig/camscanner/view/NewArrowGuidePopUtil;->〇080:Lcom/intsig/camscanner/view/NewArrowGuidePopUtil;

    .line 49
    .line 50
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 51
    .line 52
    new-instance v5, Lo0Oo/ooO〇00O;

    .line 53
    .line 54
    invoke-direct {v5, p0}, Lo0Oo/ooO〇00O;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 55
    .line 56
    .line 57
    sget-object v6, Lcom/intsig/comm/widget/CustomTextView$ArrowDirection;->TOP:Lcom/intsig/comm/widget/CustomTextView$ArrowDirection;

    .line 58
    .line 59
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 60
    .line 61
    const v7, 0x7f1308be

    .line 62
    .line 63
    .line 64
    invoke-virtual {v0, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object v7

    .line 68
    const/4 v0, 0x1

    .line 69
    new-array v9, v0, [Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 70
    .line 71
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇O80〇0o:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 72
    .line 73
    aput-object v0, v9, v1

    .line 74
    .line 75
    move-object v8, p1

    .line 76
    invoke-virtual/range {v2 .. v9}, Lcom/intsig/camscanner/view/NewArrowGuidePopUtil;->Oo08(Landroid/app/Activity;Landroid/view/View;Lcom/intsig/callback/Callback0;Lcom/intsig/comm/widget/CustomTextView$ArrowDirection;Ljava/lang/String;Landroid/view/View;[Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)Z

    .line 77
    .line 78
    .line 79
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private oO0o(I)Lcom/intsig/camscanner/pagelist/model/PageImageItem;
    .locals 3
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->〇O888o0o()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-eqz v0, :cond_2

    .line 9
    .line 10
    if-ltz p1, :cond_2

    .line 11
    .line 12
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    if-lt p1, v2, :cond_0

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    check-cast p1, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;

    .line 24
    .line 25
    instance-of v0, p1, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 26
    .line 27
    if-nez v0, :cond_1

    .line 28
    .line 29
    return-object v1

    .line 30
    :cond_1
    check-cast p1, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 31
    .line 32
    return-object p1

    .line 33
    :cond_2
    :goto_0
    return-object v1
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic oO8(Lcom/intsig/camscanner/pagelist/PageListFragment;Ljava/lang/String;Lcom/intsig/camscanner/datastruct/FolderItem;)Landroid/view/View;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O8o0〇(Ljava/lang/String;Lcom/intsig/camscanner/datastruct/FolderItem;)Landroid/view/View;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private oO800o()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 6
    .line 7
    const-string v1, "initItemHelper >>> recyclerView is null."

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇080OO8〇0:Lcom/intsig/adapter/BaseRecyclerViewTouchHelper;

    .line 14
    .line 15
    if-nez v0, :cond_1

    .line 16
    .line 17
    new-instance v0, Lcom/intsig/adapter/RecyclerViewMultiTouchHelper;

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 20
    .line 21
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 22
    .line 23
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇Oo〇o8()Lcom/intsig/adapter/RecyclerViewMultiTouchHelper$MultiItemTouchHelperCallback;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    invoke-direct {v0, v1, v2}, Lcom/intsig/adapter/RecyclerViewMultiTouchHelper;-><init>(Landroidx/recyclerview/widget/RecyclerView$Adapter;Lcom/intsig/adapter/RecyclerViewMultiTouchHelper$MultiItemTouchHelperCallback;)V

    .line 28
    .line 29
    .line 30
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇080OO8〇0:Lcom/intsig/adapter/BaseRecyclerViewTouchHelper;

    .line 31
    .line 32
    new-instance v1, Landroidx/recyclerview/widget/ItemTouchHelper;

    .line 33
    .line 34
    invoke-direct {v1, v0}, Landroidx/recyclerview/widget/ItemTouchHelper;-><init>(Landroidx/recyclerview/widget/ItemTouchHelper$Callback;)V

    .line 35
    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 38
    .line 39
    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/ItemTouchHelper;->attachToRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V

    .line 40
    .line 41
    .line 42
    :cond_1
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static synthetic oO88〇0O8O(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private oO8O(II)V
    .locals 3

    .line 1
    :try_start_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-static {p1, p2}, Lcom/intsig/camscanner/pagelist/PageListFragment$MyDialogFragment;->〇O8oOo0(II)Lcom/intsig/camscanner/pagelist/PageListFragment$MyDialogFragment;

    .line 8
    .line 9
    .line 10
    move-result-object p2

    .line 11
    iput-object p2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O0〇0:Landroidx/fragment/app/DialogFragment;

    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    invoke-virtual {p2, p0, v0}, Landroidx/fragment/app/Fragment;->setTargetFragment(Landroidx/fragment/app/Fragment;I)V

    .line 15
    .line 16
    .line 17
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O0〇0:Landroidx/fragment/app/DialogFragment;

    .line 18
    .line 19
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    sget-object v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 24
    .line 25
    invoke-virtual {p2, v0, v1}, Landroidx/fragment/app/DialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :catch_0
    move-exception p2

    .line 30
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 31
    .line 32
    new-instance v1, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    const-string v2, "showDialog id:"

    .line 38
    .line 39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    invoke-static {v0, p1, p2}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 50
    .line 51
    .line 52
    :cond_0
    :goto_0
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method static synthetic oO8o〇08〇(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private oO8o〇o〇8()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇0008O8()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/16 v1, 0x3e8

    .line 8
    .line 9
    if-ne v0, v1, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic oOO8oo0(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->ooO:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private oOOo()V
    .locals 2

    .line 1
    const-string v0, "CSFileRename"

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0〇oo()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const/4 v1, 0x0

    .line 13
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOO〇o〇0O(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private synthetic oOOo8〇o(Landroid/content/res/Configuration;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO8ooO8〇:Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;

    .line 2
    .line 3
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 9
    .line 10
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇0(I)I

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0O:Landroidx/recyclerview/widget/GridLayoutManager;

    .line 19
    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    invoke-virtual {v0}, Landroidx/recyclerview/widget/GridLayoutManager;->getSpanCount()I

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-eq p1, v0, :cond_0

    .line 27
    .line 28
    iput p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO〇00〇8oO:I

    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0O:Landroidx/recyclerview/widget/GridLayoutManager;

    .line 31
    .line 32
    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/GridLayoutManager;->setSpanCount(I)V

    .line 33
    .line 34
    .line 35
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O0OO8O()V

    .line 36
    .line 37
    .line 38
    :cond_1
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private oOO〇0o8〇()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 4
    .line 5
    .line 6
    move-result-wide v1

    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0〇oo()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v3

    .line 13
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 14
    .line 15
    .line 16
    move-result-object v4

    .line 17
    sget-object v5, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 18
    .line 19
    new-instance v6, Lcom/intsig/camscanner/pagelist/PageListFragment$4;

    .line 20
    .line 21
    invoke-direct {v6, p0}, Lcom/intsig/camscanner/pagelist/PageListFragment$4;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 22
    .line 23
    .line 24
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/mainmenu/tagsetting/dialog/TagAndTitleSettingDialog;->O0O0〇(JLjava/lang/String;Landroidx/fragment/app/FragmentManager;Ljava/lang/String;Lcom/intsig/camscanner/mainmenu/tagsetting/interfaces/TagDialogCallback;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic oOO〇OO8(I)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 8
    .line 9
    if-eqz v0, :cond_3

    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 12
    .line 13
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O08O0〇O()Z

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-nez v1, :cond_0

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 20
    .line 21
    invoke-virtual {v1}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->〇O888o0o()Ljava/util/List;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    invoke-interface {v1}, Ljava/util/List;->size()I

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    sub-int/2addr v1, p1

    .line 30
    add-int/lit8 v1, v1, -0x1

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_0
    if-lez p1, :cond_1

    .line 34
    .line 35
    add-int/lit8 p1, p1, -0x1

    .line 36
    .line 37
    :cond_1
    move v1, p1

    .line 38
    :goto_0
    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstCompletelyVisibleItemPosition()I

    .line 39
    .line 40
    .line 41
    move-result p1

    .line 42
    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastCompletelyVisibleItemPosition()I

    .line 43
    .line 44
    .line 45
    move-result v2

    .line 46
    if-lt v1, p1, :cond_2

    .line 47
    .line 48
    if-le v1, v2, :cond_3

    .line 49
    .line 50
    :cond_2
    sget-object v3, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 51
    .line 52
    new-instance v4, Ljava/lang/StringBuilder;

    .line 53
    .line 54
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 55
    .line 56
    .line 57
    const-string v5, "Finally scroll To Search Position, realSearchedPos = "

    .line 58
    .line 59
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    const-string v5, ", firstVisiblePosition = "

    .line 66
    .line 67
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    const-string p1, ", lastVisiblePosition = "

    .line 74
    .line 75
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object p1

    .line 85
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    const/high16 p1, 0x42a00000    # 80.0f

    .line 89
    .line 90
    invoke-static {p1}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 91
    .line 92
    .line 93
    move-result p1

    .line 94
    invoke-virtual {v0, v1, p1}, Landroidx/recyclerview/widget/LinearLayoutManager;->scrollToPositionWithOffset(II)V

    .line 95
    .line 96
    .line 97
    :cond_3
    return-void
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private oOO〇o〇0O(Ljava/lang/String;Ljava/lang/String;)V
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o88o0O()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    xor-int/lit8 v0, v0, 0x1

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 10
    .line 11
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 12
    .line 13
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo8o〇o〇()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    const v3, 0x7f130420

    .line 18
    .line 19
    .line 20
    const/4 v4, 0x0

    .line 21
    new-instance v7, Lo0Oo/Oo〇O;

    .line 22
    .line 23
    invoke-direct {v7, p0, v0, p1}, Lo0Oo/Oo〇O;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;ZLjava/lang/String;)V

    .line 24
    .line 25
    .line 26
    new-instance v8, Lcom/intsig/camscanner/pagelist/PageListFragment$5;

    .line 27
    .line 28
    invoke-direct {v8, p0}, Lcom/intsig/camscanner/pagelist/PageListFragment$5;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 32
    .line 33
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 34
    .line 35
    .line 36
    move-result-wide v9

    .line 37
    move-object v5, p1

    .line 38
    move-object v6, p2

    .line 39
    invoke-static/range {v1 .. v10}, Lcom/intsig/camscanner/app/DialogUtils;->o88〇OO08〇(Landroid/app/Activity;Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/app/DialogUtils$OnDocTitleEditListener;Lcom/intsig/camscanner/app/DialogUtils$OnTemplateSettingsListener;J)V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic oOoO8OO〇(Lcom/intsig/camscanner/pagelist/PageListFragment;Landroidx/fragment/app/FragmentActivity;Lkotlin/jvm/functions/Function1;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇O88(Landroidx/fragment/app/FragmentActivity;Lkotlin/jvm/functions/Function1;)Lkotlin/Unit;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic oOo〇0o8〇8()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    if-eqz v0, :cond_6

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    goto/16 :goto_4

    .line 12
    .line 13
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇0〇o808〇()Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    sget-object v1, Lcom/intsig/camscanner/pagelist/model/EditType;->MOVE:Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 20
    .line 21
    const/4 v2, 0x0

    .line 22
    if-ne v0, v1, :cond_1

    .line 23
    .line 24
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇0ooO()Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-eqz v0, :cond_1

    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOo〇08〇:Lcom/intsig/camscanner/pdf/PdfKitMoveTipsDialog;

    .line 31
    .line 32
    if-nez v0, :cond_1

    .line 33
    .line 34
    :try_start_0
    new-instance v0, Lcom/intsig/camscanner/pdf/PdfKitMoveTipsDialog;

    .line 35
    .line 36
    invoke-direct {v0}, Lcom/intsig/camscanner/pdf/PdfKitMoveTipsDialog;-><init>()V

    .line 37
    .line 38
    .line 39
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOo〇08〇:Lcom/intsig/camscanner/pdf/PdfKitMoveTipsDialog;

    .line 40
    .line 41
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/dialog/BaseReferToEarnDialog;->oO〇oo(Landroidx/fragment/app/FragmentManager;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 46
    .line 47
    .line 48
    goto :goto_0

    .line 49
    :catchall_0
    move-exception v0

    .line 50
    goto :goto_1

    .line 51
    :catch_0
    move-exception v0

    .line 52
    :try_start_1
    sget-object v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 53
    .line 54
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 55
    .line 56
    .line 57
    :goto_0
    invoke-static {v2}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇〇O8(Z)V

    .line 58
    .line 59
    .line 60
    goto/16 :goto_4

    .line 61
    .line 62
    :goto_1
    invoke-static {v2}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇〇O8(Z)V

    .line 63
    .line 64
    .line 65
    throw v0

    .line 66
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 67
    .line 68
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇0〇o808〇()Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    sget-object v1, Lcom/intsig/camscanner/pagelist/model/EditType;->DEFAULT:Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 73
    .line 74
    if-ne v0, v1, :cond_6

    .line 75
    .line 76
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇oo〇O〇80:Z

    .line 77
    .line 78
    if-nez v0, :cond_6

    .line 79
    .line 80
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->Oo0O0o8:Z

    .line 81
    .line 82
    if-eqz v0, :cond_2

    .line 83
    .line 84
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->OO〇8O8()Z

    .line 85
    .line 86
    .line 87
    move-result v0

    .line 88
    if-eqz v0, :cond_2

    .line 89
    .line 90
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOO0880O:Lcom/intsig/camscanner/batch/BatchImageProcessTipsDialog;

    .line 91
    .line 92
    if-nez v0, :cond_6

    .line 93
    .line 94
    :try_start_2
    new-instance v0, Lcom/intsig/camscanner/batch/BatchImageProcessTipsDialog;

    .line 95
    .line 96
    invoke-direct {v0}, Lcom/intsig/camscanner/batch/BatchImageProcessTipsDialog;-><init>()V

    .line 97
    .line 98
    .line 99
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOO0880O:Lcom/intsig/camscanner/batch/BatchImageProcessTipsDialog;

    .line 100
    .line 101
    new-instance v1, Lo0Oo/OOo0O;

    .line 102
    .line 103
    invoke-direct {v1, p0}, Lo0Oo/OOo0O;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 104
    .line 105
    .line 106
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/batch/BatchImageProcessTipsDialog;->o00〇88〇08(Landroid/view/View$OnClickListener;)V

    .line 107
    .line 108
    .line 109
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOO0880O:Lcom/intsig/camscanner/batch/BatchImageProcessTipsDialog;

    .line 110
    .line 111
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 112
    .line 113
    .line 114
    move-result-object v1

    .line 115
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/dialog/BaseReferToEarnDialog;->oO〇oo(Landroidx/fragment/app/FragmentManager;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 116
    .line 117
    .line 118
    goto :goto_4

    .line 119
    :catch_1
    move-exception v0

    .line 120
    sget-object v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 121
    .line 122
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 123
    .line 124
    .line 125
    goto :goto_4

    .line 126
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇80O8o8O〇:Landroid/widget/PopupWindow;

    .line 127
    .line 128
    if-nez v0, :cond_6

    .line 129
    .line 130
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 131
    .line 132
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇0008O8()I

    .line 133
    .line 134
    .line 135
    move-result v0

    .line 136
    const/16 v1, 0x3e8

    .line 137
    .line 138
    if-ne v0, v1, :cond_3

    .line 139
    .line 140
    sget-object v0, Lcom/intsig/camscanner/paper/PaperUtil;->〇080:Lcom/intsig/camscanner/paper/PaperUtil;

    .line 141
    .line 142
    invoke-virtual {v0}, Lcom/intsig/camscanner/paper/PaperUtil;->OO0o〇〇〇〇0()Z

    .line 143
    .line 144
    .line 145
    move-result v0

    .line 146
    if-nez v0, :cond_4

    .line 147
    .line 148
    :cond_3
    invoke-static {}, Lcom/intsig/camscanner/util/CurrentAppInfo;->〇o〇()Lcom/intsig/camscanner/util/CurrentAppInfo;

    .line 149
    .line 150
    .line 151
    move-result-object v0

    .line 152
    invoke-virtual {v0}, Lcom/intsig/camscanner/util/CurrentAppInfo;->oO80()Z

    .line 153
    .line 154
    .line 155
    move-result v0

    .line 156
    if-nez v0, :cond_5

    .line 157
    .line 158
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 159
    .line 160
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇8(Landroid/content/Context;)Z

    .line 161
    .line 162
    .line 163
    move-result v0

    .line 164
    if-eqz v0, :cond_4

    .line 165
    .line 166
    goto :goto_2

    .line 167
    :cond_4
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 168
    .line 169
    const-string v1, "showTipsWhenUpdateImageData but paper on"

    .line 170
    .line 171
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    .line 173
    .line 174
    goto :goto_3

    .line 175
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 176
    .line 177
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 178
    .line 179
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 180
    .line 181
    .line 182
    move-result-object v1

    .line 183
    invoke-static {v0, v1}, Lcom/intsig/camscanner/signature/SignatureUtil;->O〇8O8〇008(Landroid/content/Context;Landroid/view/View;)Landroid/widget/PopupWindow;

    .line 184
    .line 185
    .line 186
    move-result-object v0

    .line 187
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇80O8o8O〇:Landroid/widget/PopupWindow;

    .line 188
    .line 189
    :goto_3
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇80O8o8O〇:Landroid/widget/PopupWindow;

    .line 190
    .line 191
    if-eqz v0, :cond_6

    .line 192
    .line 193
    new-instance v1, Lo0Oo/O〇Oooo〇〇;

    .line 194
    .line 195
    invoke-direct {v1}, Lo0Oo/O〇Oooo〇〇;-><init>()V

    .line 196
    .line 197
    .line 198
    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setOnDismissListener(Landroid/widget/PopupWindow$OnDismissListener;)V

    .line 199
    .line 200
    .line 201
    :cond_6
    :goto_4
    return-void
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method static bridge synthetic oO〇O0O(Lcom/intsig/camscanner/pagelist/PageListFragment;Lcom/intsig/app/ProgressDialog;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o00〇88〇08:Lcom/intsig/app/ProgressDialog;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static synthetic oo0O(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic oo8(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic oo88(Lcom/intsig/camscanner/pagelist/PageListFragment;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->Oo0(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic oo8O8o80(Landroid/content/Intent;)V
    .locals 1

    .line 1
    const/16 v0, 0x3fd

    .line 2
    .line 3
    invoke-virtual {p0, p1, v0}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic oo8〇〇(Lcom/intsig/camscanner/pagelist/PageListFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇80O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic ooO888O0〇()Lkotlin/Unit;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static synthetic ooo008(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroidx/appcompat/widget/AppCompatTextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mToolbarTitle:Landroidx/appcompat/widget/AppCompatTextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private ooo0〇080()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 13
    .line 14
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v2, "constant_is_import_pdf"

    .line 19
    .line 20
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    if-eqz v0, :cond_0

    .line 25
    .line 26
    invoke-static {}, Lcom/intsig/camscanner/pagelist/ShowFloatWindowExp;->〇080()Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-eqz v0, :cond_0

    .line 31
    .line 32
    const/4 v1, 0x1

    .line 33
    :cond_0
    return v1
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic oooO8〇00(Lcom/intsig/camscanner/pagelist/PageListFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇〇〇O0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic oooo800〇〇(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroidx/appcompat/widget/AppCompatTextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mToolbarTitle:Landroidx/appcompat/widget/AppCompatTextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic ooooo0O(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroidx/appcompat/widget/AppCompatTextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mToolbarTitle:Landroidx/appcompat/widget/AppCompatTextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private oo〇88(I)V
    .locals 3

    .line 1
    const-string v0, "CSList"

    .line 2
    .line 3
    packed-switch p1, :pswitch_data_0

    .line 4
    .line 5
    .line 6
    :pswitch_0
    goto/16 :goto_0

    .line 7
    .line 8
    :pswitch_1
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 9
    .line 10
    const-string v1, "BOTTON_MENU_PRINT"

    .line 11
    .line 12
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    const-string p1, "document_more_print"

    .line 16
    .line 17
    invoke-static {v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 21
    .line 22
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o08O()V

    .line 23
    .line 24
    .line 25
    goto/16 :goto_0

    .line 26
    .line 27
    :pswitch_2
    const-string p1, "CSMore"

    .line 28
    .line 29
    const-string v0, "batch_process_image"

    .line 30
    .line 31
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 35
    .line 36
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇8o8o〇()Ljava/util/ArrayList;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    const/4 v1, 0x0

    .line 45
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o88O8(Ljava/util/ArrayList;Z)V

    .line 46
    .line 47
    .line 48
    goto/16 :goto_0

    .line 49
    .line 50
    :pswitch_3
    const-string p1, "document_more_evidence"

    .line 51
    .line 52
    invoke-static {v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 56
    .line 57
    const-string v0, "click bottom e evidence"

    .line 58
    .line 59
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 63
    .line 64
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇OOo()V

    .line 65
    .line 66
    .line 67
    goto/16 :goto_0

    .line 68
    .line 69
    :pswitch_4
    const-string p1, "document_more_collage"

    .line 70
    .line 71
    invoke-static {v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 75
    .line 76
    const-string v0, "click bottom auto"

    .line 77
    .line 78
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 82
    .line 83
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 84
    .line 85
    .line 86
    move-result-object p1

    .line 87
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇8o8o〇()Ljava/util/ArrayList;

    .line 88
    .line 89
    .line 90
    move-result-object p1

    .line 91
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 92
    .line 93
    .line 94
    move-result v0

    .line 95
    if-lez v0, :cond_0

    .line 96
    .line 97
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 98
    .line 99
    new-instance v1, Lo0Oo/Oo8Oo00oo;

    .line 100
    .line 101
    invoke-direct {v1, p0}, Lo0Oo/Oo8Oo00oo;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 102
    .line 103
    .line 104
    const/4 v2, 0x0

    .line 105
    invoke-static {v0, p1, v2, v1}, Lcom/intsig/camscanner/control/DataChecker;->〇O00(Landroid/app/Activity;Ljava/util/ArrayList;Ljava/lang/String;Lcom/intsig/camscanner/control/DataChecker$ActionListener;)Z

    .line 106
    .line 107
    .line 108
    goto :goto_0

    .line 109
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO0〇()V

    .line 110
    .line 111
    .line 112
    goto :goto_0

    .line 113
    :pswitch_5
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 114
    .line 115
    const-string v1, "click bottom upload"

    .line 116
    .line 117
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    .line 119
    .line 120
    const-string p1, "document_more_upload/print/fax"

    .line 121
    .line 122
    invoke-static {v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    .line 124
    .line 125
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 126
    .line 127
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇080O0()V

    .line 128
    .line 129
    .line 130
    goto :goto_0

    .line 131
    :pswitch_6
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 132
    .line 133
    const-string v1, "click bottom delete"

    .line 134
    .line 135
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    .line 137
    .line 138
    const-string p1, "document_delete"

    .line 139
    .line 140
    invoke-static {v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    .line 142
    .line 143
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8〇O〇0O0〇()V

    .line 144
    .line 145
    .line 146
    goto :goto_0

    .line 147
    :pswitch_7
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 148
    .line 149
    const-string v1, "click bottom copy"

    .line 150
    .line 151
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    .line 153
    .line 154
    const-string p1, "document_more_copy"

    .line 155
    .line 156
    invoke-static {v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    .line 158
    .line 159
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 160
    .line 161
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇00O〇0o()V

    .line 162
    .line 163
    .line 164
    goto :goto_0

    .line 165
    :pswitch_8
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 166
    .line 167
    const-string v1, "click bottom move"

    .line 168
    .line 169
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    .line 171
    .line 172
    const-string p1, "document_move"

    .line 173
    .line 174
    invoke-static {v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    .line 176
    .line 177
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 178
    .line 179
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8888()V

    .line 180
    .line 181
    .line 182
    goto :goto_0

    .line 183
    :pswitch_9
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 184
    .line 185
    const-string v1, "click bottom save gallery"

    .line 186
    .line 187
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    .line 189
    .line 190
    const-string p1, " document_save"

    .line 191
    .line 192
    invoke-static {v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    .line 194
    .line 195
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->Oo088O〇8〇()V

    .line 196
    .line 197
    .line 198
    goto :goto_0

    .line 199
    :pswitch_a
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 200
    .line 201
    const-string v1, "click bottom multi page share"

    .line 202
    .line 203
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    .line 205
    .line 206
    const-string p1, "document_share"

    .line 207
    .line 208
    invoke-static {v0, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    .line 210
    .line 211
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 212
    .line 213
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o0〇〇00()V

    .line 214
    .line 215
    .line 216
    :goto_0
    return-void

    .line 217
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private oo〇O0o〇()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "User Operation:  view pdf"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x1

    .line 9
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇〇0o〇〇0:Z

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 12
    .line 13
    const/4 v1, 0x5

    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇008〇o0〇〇(I)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private oo〇O〇o〇8(Z)V
    .locals 11

    .line 1
    sget-object v0, Lcom/intsig/camscanner/paper/PaperUtil;->〇080:Lcom/intsig/camscanner/paper/PaperUtil;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/paper/PaperUtil;->OO0o〇〇〇〇0()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-eqz v0, :cond_1

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oO8o〇o〇8()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 17
    .line 18
    const-string v2, "showPrintBtn isCurrentDocPaper"

    .line 19
    .line 20
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇o〇:Landroid/view/View;

    .line 24
    .line 25
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_2

    .line 30
    .line 31
    if-eqz p1, :cond_0

    .line 32
    .line 33
    new-instance p1, Landroid/view/animation/ScaleAnimation;

    .line 34
    .line 35
    const/4 v3, 0x0

    .line 36
    const/high16 v4, 0x3f800000    # 1.0f

    .line 37
    .line 38
    const/4 v5, 0x0

    .line 39
    const/high16 v6, 0x3f800000    # 1.0f

    .line 40
    .line 41
    const/4 v7, 0x1

    .line 42
    const/high16 v8, 0x3f000000    # 0.5f

    .line 43
    .line 44
    const/4 v9, 0x1

    .line 45
    const/high16 v10, 0x3f000000    # 0.5f

    .line 46
    .line 47
    move-object v2, p1

    .line 48
    invoke-direct/range {v2 .. v10}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 49
    .line 50
    .line 51
    const-wide/16 v2, 0xc8

    .line 52
    .line 53
    invoke-virtual {p1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 54
    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇o〇:Landroid/view/View;

    .line 57
    .line 58
    invoke-virtual {v0, p1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 59
    .line 60
    .line 61
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇o〇:Landroid/view/View;

    .line 62
    .line 63
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 64
    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_1
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 68
    .line 69
    const-string v0, "showPrintBtn not isCurrentDocPaper"

    .line 70
    .line 71
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    const/4 p1, 0x1

    .line 75
    new-array p1, p1, [Landroid/view/View;

    .line 76
    .line 77
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇o〇:Landroid/view/View;

    .line 78
    .line 79
    aput-object v0, p1, v1

    .line 80
    .line 81
    const/16 v0, 0x8

    .line 82
    .line 83
    invoke-static {v0, p1}, Lcom/intsig/utils/CustomViewUtils;->〇o〇(I[Landroid/view/View;)V

    .line 84
    .line 85
    .line 86
    :cond_2
    :goto_0
    return-void
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic o〇08oO80o(Lcom/intsig/camscanner/pagelist/PageListFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o〇0〇o(Lcom/intsig/camscanner/pagelist/PageListFragment;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8Oo8〇8(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private o〇O80o8OO()V
    .locals 5

    .line 1
    new-instance v0, Landroid/content/Intent;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 6
    .line 7
    .line 8
    move-result-wide v1

    .line 9
    invoke-static {v1, v2}, Lcom/intsig/camscanner/provider/Documents$Image;->〇080(J)Landroid/net/Uri;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 14
    .line 15
    const-class v3, Lcom/intsig/camscanner/ImagePageViewActivity;

    .line 16
    .line 17
    const-string v4, "android.intent.action.VIEW"

    .line 18
    .line 19
    invoke-direct {v0, v4, v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 20
    .line 21
    .line 22
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 23
    .line 24
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0〇oo()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    const-string v2, "doc_title"

    .line 29
    .line 30
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 31
    .line 32
    .line 33
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 34
    .line 35
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0O00oO()Z

    .line 36
    .line 37
    .line 38
    move-result v1

    .line 39
    const-string v2, "image_page_view_key_offline_folder"

    .line 40
    .line 41
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 42
    .line 43
    .line 44
    const-string v1, "opennote"

    .line 45
    .line 46
    const/4 v2, 0x0

    .line 47
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 48
    .line 49
    .line 50
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 51
    .line 52
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO〇()Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    const-string v2, "extra_key_doc_info"

    .line 57
    .line 58
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 59
    .line 60
    .line 61
    const-string v1, "constant_add_spec_action"

    .line 62
    .line 63
    const-string v2, "spec_action_show_image_page_view"

    .line 64
    .line 65
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    .line 67
    .line 68
    const-string v1, "constant_add_spec_action_from_part"

    .line 69
    .line 70
    const-string v2, "cs_scan"

    .line 71
    .line 72
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 73
    .line 74
    .line 75
    const/16 v1, 0x67

    .line 76
    .line 77
    invoke-virtual {p0, v0, v1}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 78
    .line 79
    .line 80
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic o〇O8OO(Lcom/intsig/camscanner/pagelist/PageListFragment;Landroidx/cardview/widget/CardView;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O888o8(Landroidx/cardview/widget/CardView;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private o〇Oo(Landroid/content/Intent;)V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "handleResultFromImagePageView data == null is "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    const/4 v2, 0x1

    .line 14
    if-nez p1, :cond_0

    .line 15
    .line 16
    const/4 v3, 0x1

    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/4 v3, 0x0

    .line 19
    :goto_0
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    if-nez p1, :cond_1

    .line 30
    .line 31
    const-string p1, "data == null"

    .line 32
    .line 33
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    goto :goto_1

    .line 37
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    if-eqz v1, :cond_3

    .line 42
    .line 43
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    const-string v3, "finish activity"

    .line 48
    .line 49
    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    if-eqz v1, :cond_2

    .line 54
    .line 55
    const-string p1, "onActivityResult()  finish "

    .line 56
    .line 57
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    iput-boolean v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O88O:Z

    .line 61
    .line 62
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 63
    .line 64
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 65
    .line 66
    .line 67
    return-void

    .line 68
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    const-string v1, "firstpage"

    .line 73
    .line 74
    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    .line 75
    .line 76
    .line 77
    move-result p1

    .line 78
    if-eqz p1, :cond_3

    .line 79
    .line 80
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 84
    .line 85
    invoke-virtual {p1, v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8O0880(Z)V

    .line 86
    .line 87
    .line 88
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇oO〇()V

    .line 89
    .line 90
    .line 91
    :goto_1
    return-void
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method static bridge synthetic o〇OoO0(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇o0O:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o〇o08〇(Lcom/intsig/camscanner/pagelist/PageListFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O80〇〇o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic o〇o0o8Oo()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oo8ooo8O:Lcom/intsig/camscanner/view/PullToSyncRecyclerView;

    .line 13
    .line 14
    const/4 v1, 0x0

    .line 15
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 19
    .line 20
    const v2, 0x7f01002c

    .line 21
    .line 22
    .line 23
    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    new-instance v2, Landroid/view/animation/LayoutAnimationController;

    .line 28
    .line 29
    invoke-direct {v2, v0}, Landroid/view/animation/LayoutAnimationController;-><init>(Landroid/view/animation/Animation;)V

    .line 30
    .line 31
    .line 32
    const v0, 0x3dcccccd    # 0.1f

    .line 33
    .line 34
    .line 35
    invoke-virtual {v2, v0}, Landroid/view/animation/LayoutAnimationController;->setDelay(F)V

    .line 36
    .line 37
    .line 38
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    .line 39
    .line 40
    invoke-direct {v0}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v2, v0}, Landroid/view/animation/LayoutAnimationController;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 44
    .line 45
    .line 46
    invoke-virtual {v2, v1}, Landroid/view/animation/LayoutAnimationController;->setOrder(I)V

    .line 47
    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 50
    .line 51
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setLayoutAnimation(Landroid/view/animation/LayoutAnimationController;)V

    .line 52
    .line 53
    .line 54
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 55
    .line 56
    invoke-virtual {v0}, Landroid/view/ViewGroup;->startLayoutAnimation()V

    .line 57
    .line 58
    .line 59
    :cond_1
    :goto_0
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic o〇o0oOO8(Lcom/intsig/camscanner/pagelist/PageListFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oo〇88(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic o〇o0〇0O〇o(I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->Oo0o0o8(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic o〇o8〇〇O(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic o〇oO08〇o0(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroidx/appcompat/widget/AppCompatTextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mToolbarTitle:Landroidx/appcompat/widget/AppCompatTextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o〇oo(Lcom/intsig/callback/Callback;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o808Oo(Lcom/intsig/callback/Callback;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private o〇〇8〇〇(I)Z
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    if-le p1, v0, :cond_0

    .line 3
    .line 4
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇Oo〇o8()Z

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    if-nez p1, :cond_0

    .line 9
    .line 10
    invoke-static {}, Lcom/intsig/camscanner/signature/SignatureUtil;->〇〇808〇()Z

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    if-nez p1, :cond_0

    .line 15
    .line 16
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇0888o()Z

    .line 17
    .line 18
    .line 19
    move-result p1

    .line 20
    if-nez p1, :cond_0

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    const/4 v0, 0x0

    .line 24
    :goto_0
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 25
    .line 26
    new-instance v1, Ljava/lang/StringBuilder;

    .line 27
    .line 28
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 29
    .line 30
    .line 31
    const-string v2, "checkShowCollageAutoFloatingView>>> isShow = "

    .line 32
    .line 33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    invoke-static {p1, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    return v0
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private 〇00O(Landroid/net/Uri;)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o0OoOOo0()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    new-instance v0, Landroid/content/ContentValues;

    .line 10
    .line 11
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 12
    .line 13
    .line 14
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 15
    .line 16
    .line 17
    move-result-wide v1

    .line 18
    sget-object v3, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 19
    .line 20
    const-string v4, "modified_date savePdfProperty pagelist"

    .line 21
    .line 22
    invoke-static {v3, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    const-string v4, "modified"

    .line 26
    .line 27
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 32
    .line 33
    .line 34
    const-string v1, "state"

    .line 35
    .line 36
    const/4 v2, 0x1

    .line 37
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 38
    .line 39
    .line 40
    move-result-object v4

    .line 41
    invoke-virtual {v0, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 42
    .line 43
    .line 44
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 45
    .line 46
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇〇0〇88()J

    .line 47
    .line 48
    .line 49
    move-result-wide v4

    .line 50
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 51
    .line 52
    .line 53
    move-result-object v1

    .line 54
    const-string v4, "page_size"

    .line 55
    .line 56
    invoke-virtual {v0, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 57
    .line 58
    .line 59
    const-string v1, "savePdfProperty mNeedCreatePdf"

    .line 60
    .line 61
    invoke-static {v3, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    if-eqz p1, :cond_0

    .line 65
    .line 66
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 67
    .line 68
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 69
    .line 70
    .line 71
    move-result-object v1

    .line 72
    const/4 v3, 0x0

    .line 73
    invoke-virtual {v1, p1, v0, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 74
    .line 75
    .line 76
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 77
    .line 78
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 79
    .line 80
    .line 81
    move-result-wide v3

    .line 82
    const/4 p1, 0x3

    .line 83
    invoke-static {v0, v3, v4, p1, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0oO0(Landroid/content/Context;JIZ)V

    .line 84
    .line 85
    .line 86
    :cond_0
    return-void
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private synthetic 〇00O00o()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0oO()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇00o80oo(Landroid/view/View;)Z
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    instance-of v1, v1, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;

    .line 10
    .line 11
    if-eqz v1, :cond_1

    .line 12
    .line 13
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    check-cast p1, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;

    .line 18
    .line 19
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o8()Z

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    return p1

    .line 24
    :cond_1
    return v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic 〇00o〇O8(Lcom/intsig/camscanner/pagelist/PageListFragment;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o08〇808()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic 〇00〇〇〇o〇8(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇0880O0〇()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 4
    .line 5
    const v2, 0x7f0a1002

    .line 6
    .line 7
    .line 8
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 13
    .line 14
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 15
    .line 16
    .line 17
    move-result-wide v2

    .line 18
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/transfer/CsTransferDocUtil;->o800o8O(Landroidx/fragment/app/FragmentActivity;Landroid/view/View;Ljava/lang/Long;)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static synthetic 〇0888(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroidx/appcompat/widget/AppCompatTextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mToolbarTitle:Landroidx/appcompat/widget/AppCompatTextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇088O(Lcom/intsig/camscanner/pagelist/PageListFragment;Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O80O(Landroid/content/DialogInterface;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇08O(Lcom/intsig/camscanner/pagelist/PageListFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O0〇0o8O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇0O0Oo〇()V
    .locals 1

    .line 1
    const/4 v0, 0x2

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oo〇88(I)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇0O8Oo(Lcom/intsig/camscanner/pagelist/PageListFragment;Lcom/intsig/view/ImageTextButton;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O8o08O8O:Lcom/intsig/view/ImageTextButton;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic 〇0OOoO8O0(Lcom/intsig/camscanner/sharedir/recommed/RecommendSceneEntity;Landroidx/cardview/widget/CardView;Landroid/view/View;)V
    .locals 2

    .line 1
    iget-object p3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/sharedir/recommed/RecommendSceneEntity;->getEntry_title()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sget-object v1, Lcom/intsig/camscanner/sharedir/recommed/RecommendShareDirFromType$RecommendFromList;->〇o00〇〇Oo:Lcom/intsig/camscanner/sharedir/recommed/RecommendShareDirFromType$RecommendFromList;

    .line 8
    .line 9
    invoke-virtual {p3, v0, v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO8〇(Ljava/lang/String;Lcom/intsig/camscanner/sharedir/recommed/RecommendShareDirFromType;)V

    .line 10
    .line 11
    .line 12
    const/16 p3, 0x8

    .line 13
    .line 14
    invoke-virtual {p2, p3}, Landroid/view/View;->setVisibility(I)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/intsig/camscanner/sharedir/recommed/RecommendSceneEntity;->getType()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object p2

    .line 21
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 22
    .line 23
    .line 24
    move-result p2

    .line 25
    if-nez p2, :cond_0

    .line 26
    .line 27
    invoke-virtual {p1}, Lcom/intsig/camscanner/sharedir/recommed/RecommendSceneEntity;->getType()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    invoke-static {p1}, Lcom/intsig/camscanner/sharedir/recommed/ShareDirLogAgentHelper;->o〇0(Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    :cond_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private 〇0o(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    return-object v1

    .line 9
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->OoO8()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇〇〇0()Z

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    sget-object v3, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 18
    .line 19
    new-instance v4, Ljava/lang/StringBuilder;

    .line 20
    .line 21
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 22
    .line 23
    .line 24
    const-string v5, "directIntentToPdfEditing = "

    .line 25
    .line 26
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    const-string v5, " , showDocumentFinish = "

    .line 33
    .line 34
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    const-string v5, ",inputSpecAction="

    .line 41
    .line 42
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v4

    .line 52
    invoke-static {v3, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    if-nez v0, :cond_1

    .line 56
    .line 57
    const-string v0, "spec_action_loading_to_pdf_editing"

    .line 58
    .line 59
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 60
    .line 61
    .line 62
    move-result v0

    .line 63
    if-eqz v0, :cond_1

    .line 64
    .line 65
    return-object v1

    .line 66
    :cond_1
    if-nez v2, :cond_2

    .line 67
    .line 68
    const-string v0, "spec_action_show_scan_done"

    .line 69
    .line 70
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    if-eqz v0, :cond_2

    .line 75
    .line 76
    sget-object v0, Lcom/intsig/camscanner/guide/gppostpay/GPGuidePostPayConfiguration;->〇080:Lcom/intsig/camscanner/guide/gppostpay/GPGuidePostPayConfiguration;

    .line 77
    .line 78
    const/4 v2, 0x1

    .line 79
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/guide/gppostpay/GPGuidePostPayConfiguration;->〇o〇(I)Z

    .line 80
    .line 81
    .line 82
    move-result v0

    .line 83
    if-nez v0, :cond_2

    .line 84
    .line 85
    invoke-static {}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseManager;->〇080()Z

    .line 86
    .line 87
    .line 88
    move-result v0

    .line 89
    if-nez v0, :cond_2

    .line 90
    .line 91
    return-object v1

    .line 92
    :cond_2
    return-object p1
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method static bridge synthetic 〇0o0oO〇〇0(Lcom/intsig/camscanner/pagelist/PageListFragment;)Lcom/intsig/comm/widget/CustomTextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOo0:Lcom/intsig/comm/widget/CustomTextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇0o88O()V
    .locals 8

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O〇o0()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    new-instance v0, Landroid/content/Intent;

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 10
    .line 11
    const-class v2, Lcom/intsig/camscanner/ScanDoneActivity;

    .line 12
    .line 13
    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 14
    .line 15
    .line 16
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 17
    .line 18
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0〇oo()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 23
    .line 24
    .line 25
    move-result v1

    .line 26
    if-nez v1, :cond_0

    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 29
    .line 30
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0〇oo()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    const-string v2, "Constant_doc_finish_title"

    .line 35
    .line 36
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 37
    .line 38
    .line 39
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 40
    .line 41
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 42
    .line 43
    .line 44
    move-result-wide v1

    .line 45
    const-string v3, "Constant_doc_finish_doc_id"

    .line 46
    .line 47
    invoke-virtual {v0, v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 48
    .line 49
    .line 50
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 51
    .line 52
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0O00oO()Z

    .line 53
    .line 54
    .line 55
    move-result v1

    .line 56
    const-string v2, "Constant_doc_is_offline_folder"

    .line 57
    .line 58
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 59
    .line 60
    .line 61
    const-string v1, "Constant_doc_finish_page_type"

    .line 62
    .line 63
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O8o〇O0:Ljava/lang/String;

    .line 64
    .line 65
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 66
    .line 67
    .line 68
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 69
    .line 70
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇0008O8()I

    .line 71
    .line 72
    .line 73
    move-result v1

    .line 74
    const-string v2, "extra_doc_type"

    .line 75
    .line 76
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 77
    .line 78
    .line 79
    const-string v1, "Constant_doc_finish_is_team_doc"

    .line 80
    .line 81
    const/4 v2, 0x0

    .line 82
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 83
    .line 84
    .line 85
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 86
    .line 87
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0oo0o0〇()Z

    .line 88
    .line 89
    .line 90
    move-result v1

    .line 91
    const-string v2, "extra_from_detect_idcard_2_a4_paper"

    .line 92
    .line 93
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 94
    .line 95
    .line 96
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 97
    .line 98
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OOoo()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 99
    .line 100
    .line 101
    move-result-object v1

    .line 102
    const-string v2, "extra_entrance"

    .line 103
    .line 104
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 105
    .line 106
    .line 107
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 108
    .line 109
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o8O〇O()I

    .line 110
    .line 111
    .line 112
    move-result v1

    .line 113
    const-string v2, "page_num"

    .line 114
    .line 115
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 116
    .line 117
    .line 118
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 119
    .line 120
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo8o〇o〇()Ljava/lang/String;

    .line 121
    .line 122
    .line 123
    move-result-object v1

    .line 124
    const-string v2, "parent_sync_id"

    .line 125
    .line 126
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    .line 128
    .line 129
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 130
    .line 131
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO8008O()J

    .line 132
    .line 133
    .line 134
    move-result-wide v1

    .line 135
    const-string v3, "tag_id"

    .line 136
    .line 137
    invoke-virtual {v0, v3, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 138
    .line 139
    .line 140
    const-string v1, "intent_extra_check_show_ad"

    .line 141
    .line 142
    iget-boolean v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->ooo0〇〇O:Z

    .line 143
    .line 144
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 145
    .line 146
    .line 147
    const/16 v1, 0x3f7

    .line 148
    .line 149
    invoke-virtual {p0, v0, v1}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 150
    .line 151
    .line 152
    goto :goto_0

    .line 153
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O08o()Lcom/intsig/camscanner/scandone/ScanDoneModel;

    .line 154
    .line 155
    .line 156
    move-result-object v5

    .line 157
    sget-object v2, Lcom/intsig/camscanner/scandone/ScanDoneNewActivity;->O0O:Lcom/intsig/camscanner/scandone/ScanDoneNewActivity$Companion;

    .line 158
    .line 159
    iget-object v4, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 160
    .line 161
    const/16 v6, 0x3f7

    .line 162
    .line 163
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 164
    .line 165
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇08〇0〇o〇8()Z

    .line 166
    .line 167
    .line 168
    move-result v7

    .line 169
    move-object v3, p0

    .line 170
    invoke-virtual/range {v2 .. v7}, Lcom/intsig/camscanner/scandone/ScanDoneNewActivity$Companion;->startActivityForResult(Landroidx/fragment/app/Fragment;Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/scandone/ScanDoneModel;IZ)V

    .line 171
    .line 172
    .line 173
    :goto_0
    return-void
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic 〇0o88Oo〇(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroidx/fragment/app/DialogFragment;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O0〇0:Landroidx/fragment/app/DialogFragment;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇0oO()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇〇0:Landroid/view/View;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/16 v1, 0x8

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇O80〇0o:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 12
    .line 13
    if-nez v0, :cond_1

    .line 14
    .line 15
    return-void

    .line 16
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇〇0:Landroid/view/View;

    .line 17
    .line 18
    const v1, 0x7f0a1212

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    check-cast v0, Lcom/intsig/comm/widget/CustomTextView;

    .line 26
    .line 27
    if-eqz v0, :cond_3

    .line 28
    .line 29
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    if-nez v1, :cond_2

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_2
    sget-object v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 37
    .line 38
    const-string v2, "hideNewPopGuide"

    .line 39
    .line 40
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇O80〇0o:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 48
    .line 49
    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 50
    .line 51
    .line 52
    const/4 v0, 0x0

    .line 53
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇O80〇0o:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 54
    .line 55
    :cond_3
    :goto_0
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇0oO〇oo00(Lcom/intsig/camscanner/pagelist/PageListFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0O0Oo〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇0ooOOo(Lcom/intsig/camscanner/pagelist/PageListFragment;Lcom/intsig/callback/Callback;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0〇〇o0(Lcom/intsig/callback/Callback;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static synthetic 〇0o〇o(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇0〇(I)V
    .locals 4

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O0〇0:Landroidx/fragment/app/DialogFragment;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroidx/fragment/app/DialogFragment;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4
    .line 5
    .line 6
    goto :goto_0

    .line 7
    :catch_0
    move-exception v0

    .line 8
    sget-object v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 9
    .line 10
    new-instance v2, Ljava/lang/StringBuilder;

    .line 11
    .line 12
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    .line 14
    .line 15
    const-string v3, "dismissDialog id:"

    .line 16
    .line 17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    invoke-static {v1, p1, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 28
    .line 29
    .line 30
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o00〇88〇08:Lcom/intsig/app/ProgressDialog;

    .line 31
    .line 32
    if-eqz p1, :cond_0

    .line 33
    .line 34
    const/4 p1, 0x0

    .line 35
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o00〇88〇08:Lcom/intsig/app/ProgressDialog;

    .line 36
    .line 37
    :cond_0
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic 〇0〇0(Lcom/intsig/camscanner/pagelist/PageListFragment;Lcom/intsig/camscanner/datastruct/FolderItem;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o008(Lcom/intsig/camscanner/datastruct/FolderItem;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private 〇0〇8o〇(Z)V
    .locals 6

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8〇OO:I

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->Ooo08:I

    .line 7
    .line 8
    :goto_0
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇08oOOO0:[I

    .line 9
    .line 10
    if-eqz v1, :cond_2

    .line 11
    .line 12
    array-length v2, v1

    .line 13
    const/4 v3, 0x0

    .line 14
    :goto_1
    if-ge v3, v2, :cond_2

    .line 15
    .line 16
    aget v4, v1, v3

    .line 17
    .line 18
    iget-object v5, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO8ooO8〇:Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;

    .line 19
    .line 20
    invoke-interface {v5, v4}, Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;->〇〇888(I)Landroid/view/View;

    .line 21
    .line 22
    .line 23
    move-result-object v4

    .line 24
    if-eqz v4, :cond_1

    .line 25
    .line 26
    invoke-virtual {v4, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 27
    .line 28
    .line 29
    instance-of v5, v4, Lcom/intsig/view/ImageTextButton;

    .line 30
    .line 31
    if-eqz v5, :cond_1

    .line 32
    .line 33
    check-cast v4, Lcom/intsig/view/ImageTextButton;

    .line 34
    .line 35
    invoke-virtual {v4, v0}, Lcom/intsig/view/ImageTextButton;->setIconAndTextColor(I)V

    .line 36
    .line 37
    .line 38
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 39
    .line 40
    goto :goto_1

    .line 41
    :cond_2
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static synthetic 〇0〇o8〇(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇0〇〇o0(Lcom/intsig/callback/Callback;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇80O8o8O〇()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o880:Landroid/os/Handler;

    .line 8
    .line 9
    new-instance v2, Lo0Oo/o0O0;

    .line 10
    .line 11
    invoke-direct {v2, p1, v0}, Lo0Oo/o0O0;-><init>(Lcom/intsig/callback/Callback;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇80O(Lcom/intsig/camscanner/pagelist/PageListFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8〇〇〇0O(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇80O80O〇0()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "changtoManualSort"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oo8ooo8O:Lcom/intsig/camscanner/view/PullToSyncRecyclerView;

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->setLock(Z)V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08〇o0O:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 15
    .line 16
    const/16 v2, 0x8

    .line 17
    .line 18
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;->setVisibility(I)V

    .line 19
    .line 20
    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o00o0O〇〇o()Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->oO80()V

    .line 26
    .line 27
    .line 28
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->OOO0o〇()V

    .line 29
    .line 30
    .line 31
    const v0, 0x7f1301e9

    .line 32
    .line 33
    .line 34
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    invoke-virtual {p0, v0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setToolbarTitle(Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mToolbarMenu:Landroid/widget/FrameLayout;

    .line 42
    .line 43
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 44
    .line 45
    .line 46
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 47
    .line 48
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0oO〇oo00(Z)V

    .line 49
    .line 50
    .line 51
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 52
    .line 53
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o8O〇O()I

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    if-le v0, v1, :cond_0

    .line 58
    .line 59
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇80O()Z

    .line 60
    .line 61
    .line 62
    move-result v0

    .line 63
    xor-int/2addr v0, v1

    .line 64
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o0O8o00(Z)V

    .line 65
    .line 66
    .line 67
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O08o〇8()V

    .line 68
    .line 69
    .line 70
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇〇O()Lcom/intsig/camscanner/pagelist/bubble/PageListBubbleControl;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/bubble/PageListBubbleControl;->Oo08()V

    .line 75
    .line 76
    .line 77
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 78
    .line 79
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O880O〇()V

    .line 80
    .line 81
    .line 82
    return-void
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private synthetic 〇80oo〇0〇o(Landroid/view/View;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO8ooO8〇:Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;

    .line 2
    .line 3
    invoke-interface {p1}, Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;->O8()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇80〇(Lcom/intsig/camscanner/pagelist/PageListFragment;Lcom/intsig/menu/PopupMenuItems;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇00O0:Lcom/intsig/menu/PopupMenuItems;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇88()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0oO()V

    .line 2
    .line 3
    .line 4
    const-wide/16 v0, 0x1

    .line 5
    .line 6
    invoke-virtual {p0, v0, v1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8O(J)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static synthetic 〇8880(Landroid/app/Dialog;Landroid/view/View;)V
    .locals 0

    .line 1
    :try_start_0
    invoke-virtual {p0}, Landroid/app/Dialog;->isShowing()Z

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Landroid/app/Dialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :catch_0
    move-exception p0

    .line 12
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 13
    .line 14
    invoke-static {p1, p0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 15
    .line 16
    .line 17
    :cond_0
    :goto_0
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static synthetic 〇8O(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroidx/appcompat/widget/AppCompatTextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mToolbarTitle:Landroidx/appcompat/widget/AppCompatTextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇8O0880(Lcom/intsig/camscanner/pagelist/PageListFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇o0〇0O〇o(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic 〇8Oo8〇8(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O80(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic 〇8OooO0()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/pdf/PreferenceCsPdfHelper;->〇o00〇〇Oo()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const-wide/16 v0, 0x1a

    .line 8
    .line 9
    invoke-virtual {p0, v0, v1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8O(J)V

    .line 10
    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const-wide/16 v0, 0x0

    .line 14
    .line 15
    invoke-virtual {p0, v0, v1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8O(J)V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method private synthetic 〇8o088〇0(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    new-instance v1, Lo0Oo/O8O〇;

    .line 4
    .line 5
    invoke-direct {v1, p0, p1}, Lo0Oo/O8O〇;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇8o0o0(Lcom/intsig/camscanner/pagelist/PageListFragment;Landroid/widget/EditText;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oooO888:Landroid/widget/EditText;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇8o80O()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "existManualSort"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->ooO:Landroid/view/View;

    .line 9
    .line 10
    invoke-virtual {p0, v0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setToolbarMenu(Landroid/view/View;)V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oo8ooo8O:Lcom/intsig/camscanner/view/PullToSyncRecyclerView;

    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O0Oo〇8()Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->setLock(Z)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 23
    .line 24
    const/4 v1, 0x0

    .line 25
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0oO〇oo00(Z)V

    .line 26
    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 29
    .line 30
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0O〇OOo()Z

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    if-nez v0, :cond_0

    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08〇o0O:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 37
    .line 38
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;->setVisibility(I)V

    .line 39
    .line 40
    .line 41
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o00o0O〇〇o()Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 46
    .line 47
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇〇808〇(ZLandroidx/fragment/app/FragmentActivity;)V

    .line 48
    .line 49
    .line 50
    const/4 v0, 0x1

    .line 51
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oo〇O〇o〇8(Z)V

    .line 52
    .line 53
    .line 54
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 55
    .line 56
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0〇oo()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    invoke-virtual {p0, v0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setToolbarTitle(Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 64
    .line 65
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oooO888()V

    .line 66
    .line 67
    .line 68
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇〇O()Lcom/intsig/camscanner/pagelist/bubble/PageListBubbleControl;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/bubble/PageListBubbleControl;->〇8o8o〇()V

    .line 73
    .line 74
    .line 75
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private 〇8o8o(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08〇o0O:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 8
    .line 9
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 10
    .line 11
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08〇o0O:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 12
    .line 13
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o00o0O〇〇o()Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    const/4 v0, 0x0

    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 22
    .line 23
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇〇808〇(ZLandroidx/fragment/app/FragmentActivity;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic 〇8oo0oO0(Lcom/intsig/camscanner/pagelist/PageListFragment;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o0O8o00(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇8oo8888(Lcom/intsig/camscanner/pagelist/PageListFragment;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->OOOo〇(Ljava/util/ArrayList;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static synthetic 〇8ooOO(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic 〇8ooo(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇8oo〇〇oO(Lcom/intsig/camscanner/pagelist/PageListFragment;Ljava/lang/String;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O8080〇O8o(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic 〇8〇(I)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇o〇()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oO〇08o(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O00O()V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
.end method

.method static synthetic 〇8〇0O〇(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇8〇80o(Lcom/intsig/camscanner/pagelist/PageListFragment;Ljava/lang/String;Ljava/lang/String;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o08(Ljava/lang/String;Ljava/lang/String;)Lkotlin/Unit;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private 〇8〇8o00()Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;
    .locals 12
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "buildScanDoneTagEntity"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 9
    .line 10
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o〇Oo0()Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    sget-object v2, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 15
    .line 16
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 17
    .line 18
    invoke-virtual {v3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 19
    .line 20
    .line 21
    move-result-wide v3

    .line 22
    invoke-static {v2, v3, v4}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇O888o0o(Landroid/content/Context;J)Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    const/4 v3, 0x1

    .line 27
    if-eqz v1, :cond_0

    .line 28
    .line 29
    if-eqz v2, :cond_0

    .line 30
    .line 31
    invoke-virtual {v1, v3, v2}, Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;->tryTriggerLogAgent(ZLjava/lang/String;)V

    .line 32
    .line 33
    .line 34
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇08〇0oo0()Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o8OO0()I

    .line 39
    .line 40
    .line 41
    move-result v2

    .line 42
    const/4 v4, 0x0

    .line 43
    if-lez v2, :cond_1

    .line 44
    .line 45
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 46
    .line 47
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 48
    .line 49
    .line 50
    move-result-wide v5

    .line 51
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇08〇0oo0()Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 52
    .line 53
    .line 54
    move-result-object v2

    .line 55
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o8OO0()I

    .line 56
    .line 57
    .line 58
    move-result v2

    .line 59
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    invoke-static {v5, v6, v2, v4}, Lcom/intsig/camscanner/scenariodir/util/CertificateDbUtil;->o〇0(JLjava/lang/Integer;Ljava/lang/Integer;)Z

    .line 64
    .line 65
    .line 66
    :cond_1
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 67
    .line 68
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO8008O()J

    .line 69
    .line 70
    .line 71
    move-result-wide v6

    .line 72
    const-wide/16 v8, 0x0

    .line 73
    .line 74
    cmp-long v2, v6, v8

    .line 75
    .line 76
    if-ltz v2, :cond_3

    .line 77
    .line 78
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 79
    .line 80
    invoke-static {v1, v6, v7}, Lcom/intsig/camscanner/db/dao/TagDao;->O8(Landroid/content/Context;J)Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object v8

    .line 84
    const-string v1, "buildScanDoneTagEntity------>>>>>> chosenTagId"

    .line 85
    .line 86
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    .line 88
    .line 89
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 90
    .line 91
    .line 92
    move-result v0

    .line 93
    if-eqz v0, :cond_2

    .line 94
    .line 95
    goto :goto_0

    .line 96
    :cond_2
    new-instance v4, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;

    .line 97
    .line 98
    const/4 v9, 0x0

    .line 99
    const/4 v10, 0x1

    .line 100
    const/4 v11, 0x3

    .line 101
    move-object v5, v4

    .line 102
    invoke-direct/range {v5 .. v11}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;-><init>(JLjava/lang/String;Ljava/lang/String;ZI)V

    .line 103
    .line 104
    .line 105
    :goto_0
    return-object v4

    .line 106
    :cond_3
    sget-object v2, Lcom/intsig/camscanner/scanner/pagescene/PageSceneUtil;->Companion:Lcom/intsig/camscanner/scanner/pagescene/PageSceneUtil$Companion;

    .line 107
    .line 108
    invoke-virtual {v2, v1, v3}, Lcom/intsig/camscanner/scanner/pagescene/PageSceneUtil$Companion;->getTagName(Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;Z)Ljava/lang/String;

    .line 109
    .line 110
    .line 111
    move-result-object v8

    .line 112
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 113
    .line 114
    .line 115
    move-result v1

    .line 116
    if-nez v1, :cond_4

    .line 117
    .line 118
    new-instance v1, Ljava/lang/StringBuilder;

    .line 119
    .line 120
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 121
    .line 122
    .line 123
    const-string v2, "buildScanDoneTagEntity------>>>>>> engineClassifyTag="

    .line 124
    .line 125
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    .line 127
    .line 128
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    .line 130
    .line 131
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 132
    .line 133
    .line 134
    move-result-object v1

    .line 135
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    .line 137
    .line 138
    invoke-static {v8}, Lcom/intsig/camscanner/app/DBUtil;->〇0O〇Oo(Ljava/lang/String;)J

    .line 139
    .line 140
    .line 141
    move-result-wide v6

    .line 142
    new-instance v0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;

    .line 143
    .line 144
    const/4 v9, 0x0

    .line 145
    const/4 v10, 0x1

    .line 146
    const/4 v11, 0x4

    .line 147
    move-object v5, v0

    .line 148
    invoke-direct/range {v5 .. v11}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;-><init>(JLjava/lang/String;Ljava/lang/String;ZI)V

    .line 149
    .line 150
    .line 151
    return-object v0

    .line 152
    :cond_4
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 153
    .line 154
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇O00〇8()Z

    .line 155
    .line 156
    .line 157
    move-result v1

    .line 158
    if-eqz v1, :cond_5

    .line 159
    .line 160
    const-string v1, "buildScanDoneTagEntity------>>>>>> isFromCertificate"

    .line 161
    .line 162
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    .line 164
    .line 165
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 166
    .line 167
    const v1, 0x7f1312ac

    .line 168
    .line 169
    .line 170
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 171
    .line 172
    .line 173
    move-result-object v5

    .line 174
    invoke-static {v5}, Lcom/intsig/camscanner/app/DBUtil;->〇0O〇Oo(Ljava/lang/String;)J

    .line 175
    .line 176
    .line 177
    move-result-wide v3

    .line 178
    new-instance v0, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;

    .line 179
    .line 180
    const/4 v6, 0x0

    .line 181
    const/4 v7, 0x1

    .line 182
    const/4 v8, 0x5

    .line 183
    move-object v2, v0

    .line 184
    invoke-direct/range {v2 .. v8}, Lcom/intsig/camscanner/scandone/ScanDoneNewViewModel$ScanDoneTagEntity;-><init>(JLjava/lang/String;Ljava/lang/String;ZI)V

    .line 185
    .line 186
    .line 187
    return-object v0

    .line 188
    :cond_5
    return-object v4
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public static synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/pagelist/PageListFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o〇8(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static synthetic 〇8〇o〇OoO8(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroidx/appcompat/widget/AppCompatTextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mToolbarTitle:Landroidx/appcompat/widget/AppCompatTextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇8〇〇8o(Lcom/intsig/camscanner/pagelist/PageListFragment;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇o0〇8:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static synthetic 〇8〇〇8〇8(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroidx/appcompat/widget/AppCompatTextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mToolbarTitle:Landroidx/appcompat/widget/AppCompatTextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇8〇〇〇O0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O8o08O8O:Lcom/intsig/view/ImageTextButton;

    .line 2
    .line 3
    if-eqz v0, :cond_3

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 6
    .line 7
    if-eqz v0, :cond_3

    .line 8
    .line 9
    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-nez v0, :cond_3

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 16
    .line 17
    if-nez v0, :cond_0

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    iget v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson;->pagedetail_web_login:I

    .line 25
    .line 26
    if-nez v0, :cond_1

    .line 27
    .line 28
    return-void

    .line 29
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/signature/SignatureUtil;->〇〇808〇()Z

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    if-nez v0, :cond_2

    .line 34
    .line 35
    const/4 v0, 0x2

    .line 36
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->O0oo0〇〇〇O(I)V

    .line 37
    .line 38
    .line 39
    :cond_2
    return-void

    .line 40
    :cond_3
    :goto_0
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 41
    .line 42
    const-string v1, "showShareTips() Error --"

    .line 43
    .line 44
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇O0OO8O()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->getItemCount()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0oO〇oo00:I

    .line 8
    .line 9
    if-eq v1, v0, :cond_0

    .line 10
    .line 11
    :try_start_0
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 12
    .line 13
    invoke-virtual {v1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 14
    .line 15
    .line 16
    goto :goto_0

    .line 17
    :catch_0
    move-exception v1

    .line 18
    sget-object v2, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 19
    .line 20
    new-instance v3, Ljava/lang/StringBuilder;

    .line 21
    .line 22
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 23
    .line 24
    .line 25
    const-string v4, "safeNotifyAdapterDataSetChanged error; "

    .line 26
    .line 27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    :goto_0
    iput v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0oO〇oo00:I

    .line 41
    .line 42
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oO0〇〇o8〇()Z

    .line 43
    .line 44
    .line 45
    move-result v0

    .line 46
    if-eqz v0, :cond_1

    .line 47
    .line 48
    return-void

    .line 49
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 50
    .line 51
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    instance-of v0, v0, Landroidx/recyclerview/widget/GridLayoutManager;

    .line 56
    .line 57
    const/4 v1, 0x0

    .line 58
    const/4 v2, 0x1

    .line 59
    if-eqz v0, :cond_2

    .line 60
    .line 61
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 62
    .line 63
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    check-cast v0, Landroidx/recyclerview/widget/GridLayoutManager;

    .line 68
    .line 69
    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    .line 70
    .line 71
    .line 72
    move-result v3

    .line 73
    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    .line 74
    .line 75
    .line 76
    move-result v0

    .line 77
    iget-object v4, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 78
    .line 79
    invoke-virtual {v4}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->getItemCount()I

    .line 80
    .line 81
    .line 82
    move-result v4

    .line 83
    sub-int/2addr v4, v2

    .line 84
    if-ne v4, v0, :cond_2

    .line 85
    .line 86
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 87
    .line 88
    invoke-virtual {v0}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->〇O888o0o()Ljava/util/List;

    .line 89
    .line 90
    .line 91
    move-result-object v0

    .line 92
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    instance-of v0, v0, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;

    .line 97
    .line 98
    if-eqz v0, :cond_2

    .line 99
    .line 100
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇00o08(Z)V

    .line 101
    .line 102
    .line 103
    :try_start_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 104
    .line 105
    sub-int/2addr v4, v2

    .line 106
    invoke-virtual {v0, v3, v4}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemRangeChanged(II)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 107
    .line 108
    .line 109
    goto :goto_1

    .line 110
    :catch_1
    move-exception v0

    .line 111
    sget-object v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 112
    .line 113
    new-instance v2, Ljava/lang/StringBuilder;

    .line 114
    .line 115
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 116
    .line 117
    .line 118
    const-string v3, "notifyItemRangeChanged error; "

    .line 119
    .line 120
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    .line 122
    .line 123
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 124
    .line 125
    .line 126
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 127
    .line 128
    .line 129
    move-result-object v0

    .line 130
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    .line 132
    .line 133
    :goto_1
    return-void

    .line 134
    :cond_2
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇00o08(Z)V

    .line 135
    .line 136
    .line 137
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->Oo0o0o8(I)V

    .line 138
    .line 139
    .line 140
    return-void
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic 〇O0o〇〇o(Landroid/app/Dialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8880(Landroid/app/Dialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic 〇O80O(Landroid/content/DialogInterface;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8oOOo:Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;->〇o00〇〇Oo(Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇O8〇8000(Lcom/intsig/camscanner/pagelist/PageListFragment;J)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o00(J)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇O8〇8O0oO(Lcom/intsig/camscanner/pagelist/PageListFragment;Ljava/lang/Long;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o80(Ljava/lang/Long;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇O8〇〇o8〇()Landroid/view/View;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    instance-of v1, v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 8
    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    check-cast v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 12
    .line 13
    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const/4 v0, 0x0

    .line 19
    :goto_0
    if-ltz v0, :cond_2

    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 22
    .line 23
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    if-lt v0, v1, :cond_1

    .line 28
    .line 29
    goto :goto_1

    .line 30
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 31
    .line 32
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    return-object v0

    .line 37
    :cond_2
    :goto_1
    const/4 v0, 0x0

    .line 38
    return-object v0
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇OO0oO()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    new-instance v0, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 10
    .line 11
    new-instance v2, Lo0Oo/OOO〇O0;

    .line 12
    .line 13
    invoke-direct {v2, p0}, Lo0Oo/OOO〇O0;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 14
    .line 15
    .line 16
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/question/nps/NPSActionDataGroupProvider$RandomNPSActionDataGroupCallback;)V

    .line 17
    .line 18
    .line 19
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇o88:Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;

    .line 20
    .line 21
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇o88:Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Landroidx/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 28
    .line 29
    .line 30
    :cond_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇OOo08()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8oOOo:Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;

    .line 2
    .line 3
    const-string v1, "gp_guide_post_pay_dialog"

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;->〇o00〇〇Oo(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    sget-object v0, Lcom/intsig/camscanner/guide/gppostpay/GPGuidePostPayConfiguration;->〇080:Lcom/intsig/camscanner/guide/gppostpay/GPGuidePostPayConfiguration;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 11
    .line 12
    invoke-virtual {v1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    new-instance v2, Lo0Oo/O0〇OO8;

    .line 17
    .line 18
    invoke-direct {v2, p0}, Lo0Oo/O0〇OO8;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 19
    .line 20
    .line 21
    const/4 v3, 0x1

    .line 22
    invoke-virtual {v0, v3, v1, v2}, Lcom/intsig/camscanner/guide/gppostpay/GPGuidePostPayConfiguration;->〇〇8O0〇8(ILandroidx/fragment/app/FragmentManager;Lkotlin/jvm/functions/Function0;)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇OOo0Oo8O()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8oOOo:Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;

    .line 2
    .line 3
    const-string v1, "vip_activity_purchase_dialog"

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;->〇o00〇〇Oo(Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 9
    .line 10
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    new-instance v1, Lo0Oo/oO;

    .line 15
    .line 16
    invoke-direct {v1, p0}, Lo0Oo/oO;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 17
    .line 18
    .line 19
    invoke-static {v0, v1}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseManager;->〇〇888(Landroidx/fragment/app/FragmentManager;Lcom/intsig/callback/DialogDismissListener;)V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇OoO0o0(Lcom/intsig/camscanner/pagelist/PageListFragment;Ljava/lang/String;Ljava/lang/String;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o80oO(Ljava/lang/String;Ljava/lang/String;)Lkotlin/Unit;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic 〇OoOO〇(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O00()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇OoOo()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 6
    .line 7
    const-string v1, "operationSelectAll mAdapter == null"

    .line 8
    .line 9
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO〇00〇0O:Z

    .line 14
    .line 15
    const/4 v1, 0x1

    .line 16
    const/4 v2, 0x0

    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    iput-boolean v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO〇00〇0O:Z

    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO8ooO8〇:Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;

    .line 22
    .line 23
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;->〇8o8o〇(Z)V

    .line 24
    .line 25
    .line 26
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0〇8o〇(Z)V

    .line 27
    .line 28
    .line 29
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇o00〇〇Oo()V

    .line 36
    .line 37
    .line 38
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O0OO8O()V

    .line 39
    .line 40
    .line 41
    goto :goto_1

    .line 42
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 43
    .line 44
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇80〇808〇O()I

    .line 49
    .line 50
    .line 51
    move-result v0

    .line 52
    iput-boolean v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO〇00〇0O:Z

    .line 53
    .line 54
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO8ooO8〇:Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;

    .line 55
    .line 56
    invoke-interface {v3, v2}, Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;->〇8o8o〇(Z)V

    .line 57
    .line 58
    .line 59
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 60
    .line 61
    invoke-virtual {v3}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->〇O888o0o()Ljava/util/List;

    .line 62
    .line 63
    .line 64
    move-result-object v3

    .line 65
    if-eqz v3, :cond_3

    .line 66
    .line 67
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 68
    .line 69
    .line 70
    move-result-object v3

    .line 71
    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 72
    .line 73
    .line 74
    move-result v4

    .line 75
    if-eqz v4, :cond_3

    .line 76
    .line 77
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 78
    .line 79
    .line 80
    move-result-object v4

    .line 81
    check-cast v4, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;

    .line 82
    .line 83
    instance-of v5, v4, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 84
    .line 85
    if-eqz v5, :cond_2

    .line 86
    .line 87
    check-cast v4, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 88
    .line 89
    iget-object v5, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 90
    .line 91
    invoke-virtual {v5}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 92
    .line 93
    .line 94
    move-result-object v5

    .line 95
    invoke-virtual {v4}, Lcom/intsig/camscanner/pagelist/model/PageImageItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/pagelist/model/PageItem;

    .line 96
    .line 97
    .line 98
    move-result-object v6

    .line 99
    iget-wide v6, v6, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇080:J

    .line 100
    .line 101
    invoke-virtual {v4}, Lcom/intsig/camscanner/pagelist/model/PageImageItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/pagelist/model/PageItem;

    .line 102
    .line 103
    .line 104
    move-result-object v4

    .line 105
    iget v4, v4, Lcom/intsig/camscanner/pagelist/model/PageItem;->o〇0:I

    .line 106
    .line 107
    invoke-virtual {v5, v6, v7, v4, v2}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->o800o8O(JIZ)V

    .line 108
    .line 109
    .line 110
    goto :goto_0

    .line 111
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O0OO8O()V

    .line 112
    .line 113
    .line 114
    if-nez v0, :cond_4

    .line 115
    .line 116
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 117
    .line 118
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 119
    .line 120
    .line 121
    move-result-object v0

    .line 122
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇80〇808〇O()I

    .line 123
    .line 124
    .line 125
    move-result v0

    .line 126
    if-lez v0, :cond_4

    .line 127
    .line 128
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0〇8o〇(Z)V

    .line 129
    .line 130
    .line 131
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 132
    .line 133
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 134
    .line 135
    .line 136
    move-result-object v0

    .line 137
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇80〇808〇O()I

    .line 138
    .line 139
    .line 140
    move-result v0

    .line 141
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇o88〇O(I)V

    .line 142
    .line 143
    .line 144
    return-void
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private 〇Oooo088〇()V
    .locals 4

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o〇()Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇O〇()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_2

    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-nez v0, :cond_2

    .line 16
    .line 17
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o〇8oOO88()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-eqz v0, :cond_0

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const-string v0, "key_show_first_finish_certificate"

    .line 25
    .line 26
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇8O0〇(Ljava/lang/String;)Z

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    const/4 v2, 0x0

    .line 31
    if-eqz v1, :cond_1

    .line 32
    .line 33
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 34
    .line 35
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    const v3, 0x7f0d01eb

    .line 40
    .line 41
    .line 42
    invoke-virtual {v1, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    const-string v2, "id_mode"

    .line 47
    .line 48
    invoke-direct {p0, v1, v0, v2}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O〇0(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_1
    const-string v0, "key_show_first_finish_ocr"

    .line 53
    .line 54
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇8O0〇(Ljava/lang/String;)Z

    .line 55
    .line 56
    .line 57
    move-result v1

    .line 58
    if-eqz v1, :cond_2

    .line 59
    .line 60
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 61
    .line 62
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 63
    .line 64
    .line 65
    move-result-object v1

    .line 66
    const v3, 0x7f0d01ec

    .line 67
    .line 68
    .line 69
    invoke-virtual {v1, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 70
    .line 71
    .line 72
    move-result-object v1

    .line 73
    const-string v2, "ocr"

    .line 74
    .line 75
    invoke-direct {p0, v1, v0, v2}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O〇0(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    :cond_2
    :goto_0
    return-void
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic 〇Oo〇O(Lcom/intsig/camscanner/pagelist/PageListFragment;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->Oo8〇〇ooo(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇O〇0(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8oOOo:Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;->〇080()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 14
    .line 15
    new-instance p2, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string p3, "showNewbieTaskDialog dialogConflictHandling="

    .line 21
    .line 22
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    iget-object p3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8oOOo:Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;

    .line 26
    .line 27
    invoke-virtual {p3}, Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;->〇080()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object p3

    .line 31
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object p2

    .line 38
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    return-void

    .line 42
    :cond_0
    const v0, 0x7f0a087d

    .line 43
    .line 44
    .line 45
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    check-cast v0, Landroid/widget/ImageView;

    .line 50
    .line 51
    new-instance v1, Lcom/intsig/app/AlertDialog$Builder;

    .line 52
    .line 53
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 54
    .line 55
    const v3, 0x7f140153

    .line 56
    .line 57
    .line 58
    invoke-direct {v1, v2, v3}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 59
    .line 60
    .line 61
    invoke-virtual {v1, p1}, Lcom/intsig/app/AlertDialog$Builder;->oO(Landroid/view/View;)Lcom/intsig/app/AlertDialog$Builder;

    .line 62
    .line 63
    .line 64
    move-result-object v1

    .line 65
    const/4 v2, 0x0

    .line 66
    invoke-virtual {v1, v2}, Lcom/intsig/app/AlertDialog$Builder;->O8(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 67
    .line 68
    .line 69
    move-result-object v1

    .line 70
    invoke-virtual {v1, v2}, Lcom/intsig/app/AlertDialog$Builder;->〇〇888(Z)Lcom/intsig/app/AlertDialog$Builder;

    .line 71
    .line 72
    .line 73
    move-result-object v1

    .line 74
    invoke-virtual {v1}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 75
    .line 76
    .line 77
    move-result-object v1

    .line 78
    new-instance v3, Lo0Oo/o8oO〇;

    .line 79
    .line 80
    invoke-direct {v3, p0}, Lo0Oo/o8oO〇;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 81
    .line 82
    .line 83
    invoke-virtual {v1, v3}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 84
    .line 85
    .line 86
    const v3, 0x7f0a0260

    .line 87
    .line 88
    .line 89
    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 90
    .line 91
    .line 92
    move-result-object p1

    .line 93
    new-instance v3, Lo0Oo/o〇8oOO88;

    .line 94
    .line 95
    invoke-direct {v3, p0, p3, v1}, Lo0Oo/o〇8oOO88;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;Ljava/lang/String;Lcom/intsig/app/AlertDialog;)V

    .line 96
    .line 97
    .line 98
    invoke-virtual {p1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    .line 100
    .line 101
    new-instance p1, Lo0Oo/o〇O;

    .line 102
    .line 103
    invoke-direct {p1, p3, v1}, Lo0Oo/o〇O;-><init>(Ljava/lang/String;Lcom/intsig/app/AlertDialog;)V

    .line 104
    .line 105
    .line 106
    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    .line 108
    .line 109
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    .line 110
    .line 111
    .line 112
    move-result-object p1

    .line 113
    if-eqz p1, :cond_1

    .line 114
    .line 115
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    .line 116
    .line 117
    .line 118
    move-result-object p1

    .line 119
    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    .line 120
    .line 121
    .line 122
    move-result-object p1

    .line 123
    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 124
    .line 125
    .line 126
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    .line 127
    .line 128
    .line 129
    move-result-object p1

    .line 130
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 131
    .line 132
    const/16 v3, 0x12c

    .line 133
    .line 134
    invoke-static {v0, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 135
    .line 136
    .line 137
    move-result v0

    .line 138
    const/4 v3, -0x2

    .line 139
    invoke-virtual {p1, v0, v3}, Landroid/view/Window;->setLayout(II)V

    .line 140
    .line 141
    .line 142
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8oOOo:Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;

    .line 143
    .line 144
    const-string v0, "newbie_task"

    .line 145
    .line 146
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;->〇o00〇〇Oo(Ljava/lang/String;)V

    .line 147
    .line 148
    .line 149
    :try_start_0
    invoke-static {p2, v2}, Lcom/intsig/camscanner/util/PreferenceHelper;->OOOOOOo8(Ljava/lang/String;Z)V

    .line 150
    .line 151
    .line 152
    invoke-virtual {v1}, Lcom/intsig/app/AlertDialog;->show()V

    .line 153
    .line 154
    .line 155
    const-string p1, "CSTaskCompletePop"

    .line 156
    .line 157
    const-string p2, "type"

    .line 158
    .line 159
    invoke-static {p1, p2, p3}, Lcom/intsig/camscanner/log/LogAgentData;->Oooo8o0〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 160
    .line 161
    .line 162
    goto :goto_0

    .line 163
    :catch_0
    move-exception p1

    .line 164
    sget-object p2, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 165
    .line 166
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 167
    .line 168
    .line 169
    :goto_0
    return-void
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private synthetic 〇O〇o0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0o8〇O()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 18
    .line 19
    const-string v1, "click title to rename"

    .line 20
    .line 21
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOOo()V

    .line 25
    .line 26
    .line 27
    const-string v0, "CSList"

    .line 28
    .line 29
    const-string v1, "rename"

    .line 30
    .line 31
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    :cond_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇O〇〇〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8oOOo:Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;->〇080()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 10
    .line 11
    const-string v1, "already has dialog showing now"

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 18
    .line 19
    invoke-static {v0}, Lcom/intsig/camscanner/purchase/spread/AreaFreeActivityManager;->O8(Landroid/content/Context;)Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-nez v0, :cond_1

    .line 24
    .line 25
    return-void

    .line 26
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8oOOo:Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;

    .line 27
    .line 28
    const-string v1, "area_free_activity_share_dialog"

    .line 29
    .line 30
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;->〇o00〇〇Oo(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 34
    .line 35
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    new-instance v1, Lo0Oo/o〇8;

    .line 40
    .line 41
    invoke-direct {v1, p0}, Lo0Oo/o〇8;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 42
    .line 43
    .line 44
    invoke-static {v0, v1}, Lcom/intsig/camscanner/purchase/spread/AreaFreeActivityManager;->〇〇808〇(Landroidx/fragment/app/FragmentManager;Lcom/intsig/callback/DialogDismissListener;)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇o008o08O()V
    .locals 1

    .line 1
    new-instance v0, Lo0Oo/oo〇;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lo0Oo/oo〇;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O8〇o0〇〇(Lcom/intsig/callback/Callback;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇o08(Lcom/intsig/camscanner/pagelist/PageListFragment;Landroid/content/res/Configuration;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOOo8〇o(Landroid/content/res/Configuration;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇o0o()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇80o:Lcom/intsig/menu/PopupMenuItems;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/menu/PopupMenuItems;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 8
    .line 9
    invoke-direct {v0, v1}, Lcom/intsig/menu/PopupMenuItems;-><init>(Landroid/content/Context;)V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇80o:Lcom/intsig/menu/PopupMenuItems;

    .line 13
    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇80o:Lcom/intsig/menu/PopupMenuItems;

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/menu/PopupMenuItems;->O8()V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇〇00:Lcom/intsig/menu/MenuItem;

    .line 20
    .line 21
    if-nez v0, :cond_1

    .line 22
    .line 23
    new-instance v0, Lcom/intsig/menu/MenuItem;

    .line 24
    .line 25
    const v1, 0x7f130d48

    .line 26
    .line 27
    .line 28
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    const/16 v2, 0xa

    .line 33
    .line 34
    invoke-direct {v0, v2, v1}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;)V

    .line 35
    .line 36
    .line 37
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇〇00:Lcom/intsig/menu/MenuItem;

    .line 38
    .line 39
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇O8OO:Lcom/intsig/menu/MenuItem;

    .line 40
    .line 41
    if-nez v0, :cond_2

    .line 42
    .line 43
    new-instance v0, Lcom/intsig/menu/MenuItem;

    .line 44
    .line 45
    const v1, 0x7f131db5

    .line 46
    .line 47
    .line 48
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    const/4 v2, 0x3

    .line 53
    invoke-direct {v0, v2, v1}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;)V

    .line 54
    .line 55
    .line 56
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇O8OO:Lcom/intsig/menu/MenuItem;

    .line 57
    .line 58
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0〇0:Lcom/intsig/menu/MenuItem;

    .line 59
    .line 60
    if-nez v0, :cond_4

    .line 61
    .line 62
    new-instance v0, Lcom/intsig/menu/MenuItem;

    .line 63
    .line 64
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 65
    .line 66
    .line 67
    move-result-object v1

    .line 68
    invoke-virtual {v1}, Lcom/intsig/tsapp/sync/AppConfigJson;->isSendFaxOn()Z

    .line 69
    .line 70
    .line 71
    move-result v1

    .line 72
    if-eqz v1, :cond_3

    .line 73
    .line 74
    const v1, 0x7f13022a

    .line 75
    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_3
    const v1, 0x7f131f87

    .line 79
    .line 80
    .line 81
    :goto_0
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object v1

    .line 85
    const/4 v2, 0x5

    .line 86
    invoke-direct {v0, v2, v1}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;)V

    .line 87
    .line 88
    .line 89
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0〇0:Lcom/intsig/menu/MenuItem;

    .line 90
    .line 91
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 92
    .line 93
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇8o8o〇()Ljava/util/ArrayList;

    .line 98
    .line 99
    .line 100
    move-result-object v0

    .line 101
    if-eqz v0, :cond_7

    .line 102
    .line 103
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇o08:Lcom/intsig/menu/MenuItem;

    .line 104
    .line 105
    if-nez v0, :cond_6

    .line 106
    .line 107
    new-instance v0, Lcom/intsig/menu/MenuItem;

    .line 108
    .line 109
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 110
    .line 111
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o8O〇O()I

    .line 112
    .line 113
    .line 114
    move-result v1

    .line 115
    const/4 v2, 0x1

    .line 116
    if-le v1, v2, :cond_5

    .line 117
    .line 118
    const v1, 0x7f13080f

    .line 119
    .line 120
    .line 121
    goto :goto_1

    .line 122
    :cond_5
    const v1, 0x7f130f7c

    .line 123
    .line 124
    .line 125
    :goto_1
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 126
    .line 127
    .line 128
    move-result-object v1

    .line 129
    const/16 v2, 0x9

    .line 130
    .line 131
    invoke-direct {v0, v2, v1}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;)V

    .line 132
    .line 133
    .line 134
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇o08:Lcom/intsig/menu/MenuItem;

    .line 135
    .line 136
    :cond_6
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇80o:Lcom/intsig/menu/PopupMenuItems;

    .line 137
    .line 138
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇o08:Lcom/intsig/menu/MenuItem;

    .line 139
    .line 140
    invoke-virtual {v0, v1}, Lcom/intsig/menu/PopupMenuItems;->〇o00〇〇Oo(Lcom/intsig/menu/MenuItem;)V

    .line 141
    .line 142
    .line 143
    :cond_7
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇80o:Lcom/intsig/menu/PopupMenuItems;

    .line 144
    .line 145
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇O8OO:Lcom/intsig/menu/MenuItem;

    .line 146
    .line 147
    invoke-virtual {v0, v1}, Lcom/intsig/menu/PopupMenuItems;->〇o00〇〇Oo(Lcom/intsig/menu/MenuItem;)V

    .line 148
    .line 149
    .line 150
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇80o:Lcom/intsig/menu/PopupMenuItems;

    .line 151
    .line 152
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇〇00:Lcom/intsig/menu/MenuItem;

    .line 153
    .line 154
    invoke-virtual {v0, v1}, Lcom/intsig/menu/PopupMenuItems;->〇o00〇〇Oo(Lcom/intsig/menu/MenuItem;)V

    .line 155
    .line 156
    .line 157
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇80o:Lcom/intsig/menu/PopupMenuItems;

    .line 158
    .line 159
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0〇0:Lcom/intsig/menu/MenuItem;

    .line 160
    .line 161
    invoke-virtual {v0, v1}, Lcom/intsig/menu/PopupMenuItems;->〇o00〇〇Oo(Lcom/intsig/menu/MenuItem;)V

    .line 162
    .line 163
    .line 164
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O8〇()V

    .line 165
    .line 166
    .line 167
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->o〇0()Z

    .line 168
    .line 169
    .line 170
    move-result v0

    .line 171
    if-eqz v0, :cond_9

    .line 172
    .line 173
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇80〇808〇O()Z

    .line 174
    .line 175
    .line 176
    move-result v0

    .line 177
    if-nez v0, :cond_9

    .line 178
    .line 179
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->OO〇0008O8()Z

    .line 180
    .line 181
    .line 182
    move-result v0

    .line 183
    if-eqz v0, :cond_9

    .line 184
    .line 185
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0ooOOo:Lcom/intsig/menu/MenuItem;

    .line 186
    .line 187
    if-nez v0, :cond_8

    .line 188
    .line 189
    new-instance v0, Lcom/intsig/menu/MenuItem;

    .line 190
    .line 191
    const v1, 0x7f130221

    .line 192
    .line 193
    .line 194
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 195
    .line 196
    .line 197
    move-result-object v1

    .line 198
    const/4 v2, 0x7

    .line 199
    invoke-direct {v0, v2, v1}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;)V

    .line 200
    .line 201
    .line 202
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0ooOOo:Lcom/intsig/menu/MenuItem;

    .line 203
    .line 204
    :cond_8
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇80o:Lcom/intsig/menu/PopupMenuItems;

    .line 205
    .line 206
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0ooOOo:Lcom/intsig/menu/MenuItem;

    .line 207
    .line 208
    invoke-virtual {v0, v1}, Lcom/intsig/menu/PopupMenuItems;->〇o00〇〇Oo(Lcom/intsig/menu/MenuItem;)V

    .line 209
    .line 210
    .line 211
    :cond_9
    return-void
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private 〇o88〇O(I)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "changeSelectNumber, now Selected "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 24
    .line 25
    const/4 v1, 0x1

    .line 26
    new-array v1, v1, [Ljava/lang/Object;

    .line 27
    .line 28
    new-instance v2, Ljava/lang/StringBuilder;

    .line 29
    .line 30
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 31
    .line 32
    .line 33
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    const-string p1, ""

    .line 37
    .line 38
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    const/4 v2, 0x0

    .line 46
    aput-object p1, v1, v2

    .line 47
    .line 48
    const p1, 0x7f130164

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->Oo80:Ljava/lang/String;

    .line 56
    .line 57
    invoke-virtual {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setToolbarTitle(Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static synthetic 〇o8〇8(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇o8〇〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iput v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇oO〇〇8o:I

    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o0〇OO008O()V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇oO88o(Lcom/intsig/camscanner/pagelist/PageListFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOO〇OO8(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇oOO80o(Lcom/intsig/camscanner/pagelist/PageListFragment;)Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇oO〇08o(Lcom/intsig/camscanner/pagelist/PageListFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇o80Oo(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic 〇oo8()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o()Landroid/net/Uri;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 10
    .line 11
    .line 12
    move-result-wide v0

    .line 13
    invoke-static {}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OoO8()Lcom/intsig/camscanner/background_batch/client/BackScanClient;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    invoke-virtual {v2, v0, v1}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->O〇O〇oO(J)Lcom/intsig/camscanner/background_batch/client/BackScanClient;

    .line 18
    .line 19
    .line 20
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 21
    .line 22
    invoke-static {v2, v0, v1}, Lcom/intsig/camscanner/app/DBUtil;->O〇8oOo8O(Landroid/content/Context;J)Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    if-eqz v0, :cond_0

    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 29
    .line 30
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/FolderItem;->Oo08()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇oo(Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 38
    .line 39
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/FolderItem;->〇〇〇0〇〇0()Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇08O(Z)V

    .line 44
    .line 45
    .line 46
    :cond_0
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇oo88〇O〇8()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/tools/GuidePopClient;->oO80(Landroid/app/Activity;)Lcom/intsig/camscanner/tools/GuidePopClient;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    new-instance v1, Lcom/intsig/camscanner/tools/GuidePopClient$GuidPopClientParams;

    .line 8
    .line 9
    invoke-direct {v1}, Lcom/intsig/camscanner/tools/GuidePopClient$GuidPopClientParams;-><init>()V

    .line 10
    .line 11
    .line 12
    sget-object v2, Lcom/intsig/comm/widget/CustomTextView$ArrowDirection;->BOTTOM:Lcom/intsig/comm/widget/CustomTextView$ArrowDirection;

    .line 13
    .line 14
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/tools/GuidePopClient$GuidPopClientParams;->〇〇808〇(Lcom/intsig/comm/widget/CustomTextView$ArrowDirection;)V

    .line 15
    .line 16
    .line 17
    const v2, 0x7f130767

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0, v2}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/tools/GuidePopClient$GuidPopClientParams;->OoO8(Ljava/lang/CharSequence;)V

    .line 25
    .line 26
    .line 27
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 28
    .line 29
    const/16 v3, 0x14

    .line 30
    .line 31
    invoke-static {v2, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 32
    .line 33
    .line 34
    move-result v2

    .line 35
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/tools/GuidePopClient$GuidPopClientParams;->〇O00(I)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tools/GuidePopClient;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/tools/GuidePopClient$GuidPopClientParams;)V

    .line 39
    .line 40
    .line 41
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 42
    .line 43
    const v2, 0x7f0a07bc

    .line 44
    .line 45
    .line 46
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    check-cast v1, Lcom/intsig/view/ImageTextButton;

    .line 51
    .line 52
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 53
    .line 54
    invoke-virtual {v0, v2, v1}, Lcom/intsig/camscanner/tools/GuidePopClient;->〇8o8o〇(Landroid/content/Context;Landroid/view/View;)V

    .line 55
    .line 56
    .line 57
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇ooO8Ooo〇(Lcom/intsig/camscanner/pagelist/PageListFragment;Landroid/content/Intent;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oo8O8o80(Landroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇ooO〇000(Lcom/intsig/camscanner/pagelist/PageListFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOo〇0o8〇8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇o〇88(Lcom/intsig/camscanner/pagelist/PageListFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->ooO:Landroid/view/View;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇o〇88〇8(Ljava/lang/String;Lcom/intsig/app/AlertDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O00〇8(Ljava/lang/String;Lcom/intsig/app/AlertDialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic 〇o〇OO80oO(Lcom/intsig/camscanner/pagelist/PageListFragment;)Lcom/intsig/menu/PopupMenuItems;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇00O0:Lcom/intsig/menu/PopupMenuItems;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇o〇〇88〇8(Ljava/lang/String;Landroid/view/View;)V
    .locals 9

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    new-instance v5, Lcom/intsig/webview/data/WebArgs;

    .line 4
    .line 5
    invoke-direct {v5}, Lcom/intsig/webview/data/WebArgs;-><init>()V

    .line 6
    .line 7
    .line 8
    const/4 p2, 0x0

    .line 9
    invoke-virtual {v5, p2}, Lcom/intsig/webview/data/WebArgs;->oo88o8O(Z)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {v5, p2}, Lcom/intsig/webview/data/WebArgs;->〇0000OOO(Z)V

    .line 13
    .line 14
    .line 15
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 20
    .line 21
    .line 22
    move-result-wide v1

    .line 23
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇00(Landroid/content/Context;J)I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-nez v0, :cond_0

    .line 28
    .line 29
    const/4 p2, 0x1

    .line 30
    :cond_0
    invoke-static {p1, p2}, Lcom/intsig/utils/WebUrlUtils;->〇〇8O0〇8(Ljava/lang/String;I)Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 35
    .line 36
    new-instance p2, Ljava/lang/StringBuilder;

    .line 37
    .line 38
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 39
    .line 40
    .line 41
    const-string v0, "initGptEntrance launch url: "

    .line 42
    .line 43
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object p2

    .line 53
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 57
    .line 58
    const/4 v1, 0x0

    .line 59
    const/4 v3, 0x0

    .line 60
    const/4 v4, 0x0

    .line 61
    const/4 v6, 0x0

    .line 62
    const/4 v7, 0x1

    .line 63
    const/4 v8, 0x1

    .line 64
    invoke-static/range {v0 .. v8}, Lcom/intsig/webview/util/WebUtil;->〇080(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLcom/intsig/webview/data/WebArgs;ZZZ)Landroid/content/Intent;

    .line 65
    .line 66
    .line 67
    move-result-object p1

    .line 68
    const/high16 p2, 0x41000000    # 8.0f

    .line 69
    .line 70
    invoke-static {p2}, Lcom/intsig/utils/DisplayUtil;->O8(F)I

    .line 71
    .line 72
    .line 73
    move-result p2

    .line 74
    const-string v0, "extra_key_top_radius"

    .line 75
    .line 76
    invoke-virtual {p1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 77
    .line 78
    .line 79
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 80
    .line 81
    .line 82
    const-string p1, "CSList"

    .line 83
    .line 84
    const-string p2, "cs_ai_click"

    .line 85
    .line 86
    invoke-static {p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    .line 88
    .line 89
    :cond_1
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic 〇〇(Lcom/intsig/camscanner/pagelist/PageListFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8OooO0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic 〇〇0(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇〇0Oo0880(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 7

    .line 1
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 6
    .line 7
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 8
    .line 9
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo8o〇o〇()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v2

    .line 13
    const/4 v4, 0x0

    .line 14
    new-instance v5, Lo0Oo/〇0O〇Oo;

    .line 15
    .line 16
    invoke-direct {v5, p0, p3}, Lo0Oo/〇0O〇Oo;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    new-instance v6, Lo0Oo/〇00O0O0;

    .line 20
    .line 21
    invoke-direct {v6, p0, p3, p2}, Lo0Oo/〇00O0O0;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    move-object v3, p3

    .line 25
    invoke-static/range {v0 .. v6}, Lcom/intsig/camscanner/mainmenu/SensitiveWordsChecker;->〇080(Ljava/lang/Boolean;Landroidx/appcompat/app/AppCompatActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic 〇〇0〇〇8O(Ljava/lang/Long;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 4
    .line 5
    const v2, 0x7f0a1002

    .line 6
    .line 7
    .line 8
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    invoke-static {v0, v1, p1}, Lcom/intsig/camscanner/transfer/CsTransferDocUtil;->o800o8O(Landroidx/fragment/app/FragmentActivity;Landroid/view/View;Ljava/lang/Long;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇〇8088()V
    .locals 4

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/tsapp/sync/AppConfigJson;->isShowGptEntrance()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_3

    .line 10
    .line 11
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 12
    .line 13
    invoke-static {v0}, Lcom/intsig/camscanner/app/AppUtil;->oO(Landroid/content/Context;)Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    goto :goto_1

    .line 20
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O8ooO8o()V

    .line 21
    .line 22
    .line 23
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 24
    .line 25
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 26
    .line 27
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 28
    .line 29
    .line 30
    move-result-wide v1

    .line 31
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇O888o0o(Landroid/content/Context;J)Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    sget-object v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 36
    .line 37
    new-instance v2, Ljava/lang/StringBuilder;

    .line 38
    .line 39
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 40
    .line 41
    .line 42
    const-string v3, "initGptEntrance syncId: "

    .line 43
    .line 44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v2

    .line 54
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 58
    .line 59
    .line 60
    move-result v1

    .line 61
    if-eqz v1, :cond_1

    .line 62
    .line 63
    return-void

    .line 64
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇O:Landroid/view/ViewStub;

    .line 65
    .line 66
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 67
    .line 68
    .line 69
    move-result-object v1

    .line 70
    const v2, 0x7f0a19fc

    .line 71
    .line 72
    .line 73
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 74
    .line 75
    .line 76
    move-result-object v2

    .line 77
    check-cast v2, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;

    .line 78
    .line 79
    new-instance v3, Lo0Oo/〇O〇80o08O;

    .line 80
    .line 81
    invoke-direct {v3, p0, v0}, Lo0Oo/〇O〇80o08O;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;Ljava/lang/String;)V

    .line 82
    .line 83
    .line 84
    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    .line 86
    .line 87
    invoke-static {}, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->oO80()Z

    .line 88
    .line 89
    .line 90
    move-result v0

    .line 91
    if-eqz v0, :cond_2

    .line 92
    .line 93
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o880:Landroid/os/Handler;

    .line 94
    .line 95
    new-instance v1, Lo0Oo/ooo〇8oO;

    .line 96
    .line 97
    invoke-direct {v1, v2}, Lo0Oo/ooo〇8oO;-><init>(Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;)V

    .line 98
    .line 99
    .line 100
    const-wide/16 v2, 0x3e8

    .line 101
    .line 102
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 103
    .line 104
    .line 105
    goto :goto_0

    .line 106
    :cond_2
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/widget/AiEntranceView;->Oooo8o0〇()V

    .line 107
    .line 108
    .line 109
    :goto_0
    const-string v0, "CSList"

    .line 110
    .line 111
    const-string v1, "cs_ai_icon_show"

    .line 112
    .line 113
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    .line 115
    .line 116
    goto :goto_1

    .line 117
    :catch_0
    move-exception v0

    .line 118
    sget-object v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 119
    .line 120
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 121
    .line 122
    .line 123
    :cond_3
    :goto_1
    return-void
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private 〇〇80O()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇80o:Lcom/intsig/menu/PopupMenuItems;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/menu/PopupMenuItems;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 8
    .line 9
    invoke-direct {v0, v1}, Lcom/intsig/menu/PopupMenuItems;-><init>(Landroid/content/Context;)V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇80o:Lcom/intsig/menu/PopupMenuItems;

    .line 13
    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇80o:Lcom/intsig/menu/PopupMenuItems;

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/menu/PopupMenuItems;->O8()V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇〇00:Lcom/intsig/menu/MenuItem;

    .line 20
    .line 21
    if-nez v0, :cond_1

    .line 22
    .line 23
    new-instance v0, Lcom/intsig/menu/MenuItem;

    .line 24
    .line 25
    const v1, 0x7f130d48

    .line 26
    .line 27
    .line 28
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    const/16 v2, 0xa

    .line 33
    .line 34
    invoke-direct {v0, v2, v1}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;)V

    .line 35
    .line 36
    .line 37
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇〇00:Lcom/intsig/menu/MenuItem;

    .line 38
    .line 39
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0〇0:Lcom/intsig/menu/MenuItem;

    .line 40
    .line 41
    if-nez v0, :cond_3

    .line 42
    .line 43
    new-instance v0, Lcom/intsig/menu/MenuItem;

    .line 44
    .line 45
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    invoke-virtual {v1}, Lcom/intsig/tsapp/sync/AppConfigJson;->isSendFaxOn()Z

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    if-eqz v1, :cond_2

    .line 54
    .line 55
    const v1, 0x7f13022a

    .line 56
    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_2
    const v1, 0x7f131f87

    .line 60
    .line 61
    .line 62
    :goto_0
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v1

    .line 66
    const/4 v2, 0x5

    .line 67
    invoke-direct {v0, v2, v1}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;)V

    .line 68
    .line 69
    .line 70
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0〇0:Lcom/intsig/menu/MenuItem;

    .line 71
    .line 72
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇80o:Lcom/intsig/menu/PopupMenuItems;

    .line 73
    .line 74
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇〇00:Lcom/intsig/menu/MenuItem;

    .line 75
    .line 76
    invoke-virtual {v0, v1}, Lcom/intsig/menu/PopupMenuItems;->〇o00〇〇Oo(Lcom/intsig/menu/MenuItem;)V

    .line 77
    .line 78
    .line 79
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇80o:Lcom/intsig/menu/PopupMenuItems;

    .line 80
    .line 81
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0〇0:Lcom/intsig/menu/MenuItem;

    .line 82
    .line 83
    invoke-virtual {v0, v1}, Lcom/intsig/menu/PopupMenuItems;->〇o00〇〇Oo(Lcom/intsig/menu/MenuItem;)V

    .line 84
    .line 85
    .line 86
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O8〇()V

    .line 87
    .line 88
    .line 89
    invoke-static {}, Lcom/intsig/util/VerifyCountryUtil;->o〇0()Z

    .line 90
    .line 91
    .line 92
    move-result v0

    .line 93
    if-eqz v0, :cond_5

    .line 94
    .line 95
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇80〇808〇O()Z

    .line 96
    .line 97
    .line 98
    move-result v0

    .line 99
    if-nez v0, :cond_5

    .line 100
    .line 101
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->OO〇0008O8()Z

    .line 102
    .line 103
    .line 104
    move-result v0

    .line 105
    if-eqz v0, :cond_5

    .line 106
    .line 107
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0ooOOo:Lcom/intsig/menu/MenuItem;

    .line 108
    .line 109
    if-nez v0, :cond_4

    .line 110
    .line 111
    new-instance v0, Lcom/intsig/menu/MenuItem;

    .line 112
    .line 113
    const v1, 0x7f130221

    .line 114
    .line 115
    .line 116
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 117
    .line 118
    .line 119
    move-result-object v1

    .line 120
    const/4 v2, 0x7

    .line 121
    invoke-direct {v0, v2, v1}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;)V

    .line 122
    .line 123
    .line 124
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0ooOOo:Lcom/intsig/menu/MenuItem;

    .line 125
    .line 126
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇80o:Lcom/intsig/menu/PopupMenuItems;

    .line 127
    .line 128
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0ooOOo:Lcom/intsig/menu/MenuItem;

    .line 129
    .line 130
    invoke-virtual {v0, v1}, Lcom/intsig/menu/PopupMenuItems;->〇o00〇〇Oo(Lcom/intsig/menu/MenuItem;)V

    .line 131
    .line 132
    .line 133
    :cond_5
    return-void
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private 〇〇80o〇o0()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 8
    .line 9
    const-string v2, "extra_offline_folder"

    .line 10
    .line 11
    const/4 v3, 0x0

    .line 12
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇08O(Z)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
.end method

.method private synthetic 〇〇8OO()Lkotlin/Unit;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8oOOo:Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;->〇o00〇〇Oo(Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    return-object v1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static synthetic 〇〇8o0OOOo(Lcom/intsig/camscanner/pagelist/PageListFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇〇8〇Oo0(Ljava/lang/String;)V
    .locals 11

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "updatePaperDocProperty, newPropertyStr="

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 24
    .line 25
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇008〇oo()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 30
    .line 31
    .line 32
    move-result v2

    .line 33
    if-eqz v2, :cond_0

    .line 34
    .line 35
    const-string p1, "updatePaperDocProperty BUT EQUALS"

    .line 36
    .line 37
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    return-void

    .line 41
    :cond_0
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 42
    .line 43
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 44
    .line 45
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 46
    .line 47
    .line 48
    move-result-wide v2

    .line 49
    invoke-static {v0, v2, v3, p1}, Lcom/intsig/camscanner/db/dao/DocumentDao;->oo(Landroid/content/Context;JLjava/lang/String;)Z

    .line 50
    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 53
    .line 54
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇8〇008(Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    new-instance v0, Ljava/util/ArrayList;

    .line 58
    .line 59
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 60
    .line 61
    .line 62
    new-instance v2, Ljava/util/ArrayList;

    .line 63
    .line 64
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 65
    .line 66
    .line 67
    sget-object v3, Lcom/intsig/camscanner/paper/PaperPropertyEntity;->Oo08:Lcom/intsig/camscanner/paper/PaperPropertyEntity$Companion;

    .line 68
    .line 69
    invoke-virtual {v3, v1}, Lcom/intsig/camscanner/paper/PaperPropertyEntity$Companion;->〇080(Ljava/lang/String;)Lcom/intsig/camscanner/paper/PaperPropertyEntity;

    .line 70
    .line 71
    .line 72
    move-result-object v1

    .line 73
    invoke-virtual {v3, p1}, Lcom/intsig/camscanner/paper/PaperPropertyEntity$Companion;->〇080(Ljava/lang/String;)Lcom/intsig/camscanner/paper/PaperPropertyEntity;

    .line 74
    .line 75
    .line 76
    move-result-object p1

    .line 77
    if-eqz p1, :cond_3

    .line 78
    .line 79
    invoke-virtual {p1}, Lcom/intsig/camscanner/paper/PaperPropertyEntity;->〇o〇()Ljava/util/List;

    .line 80
    .line 81
    .line 82
    move-result-object v3

    .line 83
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 84
    .line 85
    .line 86
    move-result-object v3

    .line 87
    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 88
    .line 89
    .line 90
    move-result v4

    .line 91
    if-eqz v4, :cond_3

    .line 92
    .line 93
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 94
    .line 95
    .line 96
    move-result-object v4

    .line 97
    check-cast v4, Ljava/lang/String;

    .line 98
    .line 99
    if-eqz v1, :cond_2

    .line 100
    .line 101
    invoke-virtual {v1}, Lcom/intsig/camscanner/paper/PaperPropertyEntity;->〇o〇()Ljava/util/List;

    .line 102
    .line 103
    .line 104
    move-result-object v5

    .line 105
    invoke-interface {v5, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 106
    .line 107
    .line 108
    move-result v5

    .line 109
    if-nez v5, :cond_1

    .line 110
    .line 111
    :cond_2
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    .line 113
    .line 114
    goto :goto_0

    .line 115
    :cond_3
    if-eqz v1, :cond_6

    .line 116
    .line 117
    invoke-virtual {v1}, Lcom/intsig/camscanner/paper/PaperPropertyEntity;->〇o〇()Ljava/util/List;

    .line 118
    .line 119
    .line 120
    move-result-object v1

    .line 121
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 122
    .line 123
    .line 124
    move-result-object v1

    .line 125
    :cond_4
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 126
    .line 127
    .line 128
    move-result v3

    .line 129
    if-eqz v3, :cond_6

    .line 130
    .line 131
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 132
    .line 133
    .line 134
    move-result-object v3

    .line 135
    check-cast v3, Ljava/lang/String;

    .line 136
    .line 137
    if-eqz p1, :cond_5

    .line 138
    .line 139
    invoke-virtual {p1}, Lcom/intsig/camscanner/paper/PaperPropertyEntity;->〇o〇()Ljava/util/List;

    .line 140
    .line 141
    .line 142
    move-result-object v4

    .line 143
    invoke-interface {v4, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 144
    .line 145
    .line 146
    move-result v4

    .line 147
    if-nez v4, :cond_4

    .line 148
    .line 149
    :cond_5
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    .line 151
    .line 152
    goto :goto_1

    .line 153
    :cond_6
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 154
    .line 155
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 156
    .line 157
    .line 158
    move-result-wide v3

    .line 159
    invoke-static {v3, v4, v0, v2}, Lcom/intsig/camscanner/app/DBUtil;->〇〇〇(JLjava/util/List;Ljava/util/List;)V

    .line 160
    .line 161
    .line 162
    iget-object v5, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 163
    .line 164
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 165
    .line 166
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 167
    .line 168
    .line 169
    move-result-wide v6

    .line 170
    const/4 v8, 0x3

    .line 171
    const/4 v9, 0x1

    .line 172
    const/4 v10, 0x1

    .line 173
    invoke-static/range {v5 .. v10}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o08o〇0(Landroid/content/Context;JIZZ)V

    .line 174
    .line 175
    .line 176
    return-void
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private 〇〇O(JI)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O00O()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O8o〇o(JI)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇〇O80〇0o()V
    .locals 0

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/pagelist/PageListFragment;->OoO888()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇〇Ooo0o(I)V
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oO0o(I)Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    if-nez p1, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageImageItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/pagelist/model/PageItem;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iget-wide v0, v0, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇080:J

    .line 13
    .line 14
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageImageItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/pagelist/model/PageItem;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    iget p1, p1, Lcom/intsig/camscanner/pagelist/model/PageItem;->o〇0:I

    .line 19
    .line 20
    invoke-direct {p0, v0, v1, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O8o〇o(JI)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private 〇〇Oo〇0O〇8(IJLandroid/view/View;)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-static {v0, p2, p3}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇〇〇0〇〇0(Landroid/content/Context;J)I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, -0x1

    .line 8
    if-ne v1, v0, :cond_0

    .line 9
    .line 10
    sget-object v2, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 11
    .line 12
    const-string v3, "EVENT_CLICK_UNDOWNLOAD_DOCUMENT"

    .line 13
    .line 14
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    :cond_0
    sget-object v2, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 18
    .line 19
    new-instance v3, Ljava/lang/StringBuilder;

    .line 20
    .line 21
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 22
    .line 23
    .line 24
    const-string v4, "getJpgStatus jpgStatus="

    .line 25
    .line 26
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v3

    .line 36
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 40
    .line 41
    invoke-static {v3, p2, p3}, Lcom/intsig/camscanner/app/DBUtil;->o0(Landroid/content/Context;J)Z

    .line 42
    .line 43
    .line 44
    move-result v3

    .line 45
    const/4 v4, 0x0

    .line 46
    const/4 v5, 0x1

    .line 47
    if-eqz v3, :cond_2

    .line 48
    .line 49
    const-string v3, "InternalJpg"

    .line 50
    .line 51
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    if-eq v1, v0, :cond_1

    .line 55
    .line 56
    goto :goto_1

    .line 57
    :cond_1
    const-string v0, "getJpgStatus STATUS_PRE_ADD donoting"

    .line 58
    .line 59
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    goto :goto_0

    .line 63
    :cond_2
    const-string v3, "ExternalJpg"

    .line 64
    .line 65
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 69
    .line 70
    invoke-static {v3}, Lcom/intsig/camscanner/util/SDStorageManager;->o8O〇(Landroid/content/Context;)Z

    .line 71
    .line 72
    .line 73
    move-result v3

    .line 74
    if-eqz v3, :cond_3

    .line 75
    .line 76
    if-ne v5, v0, :cond_3

    .line 77
    .line 78
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 79
    .line 80
    invoke-static {v0}, Lcom/intsig/camscanner/app/DialogUtils;->〇o0O0O8(Landroid/content/Context;)V

    .line 81
    .line 82
    .line 83
    goto :goto_0

    .line 84
    :cond_3
    if-eq v1, v0, :cond_4

    .line 85
    .line 86
    goto :goto_1

    .line 87
    :cond_4
    :goto_0
    const/4 v5, 0x0

    .line 88
    :goto_1
    if-eqz v5, :cond_8

    .line 89
    .line 90
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 91
    .line 92
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 93
    .line 94
    .line 95
    move-result-wide v0

    .line 96
    invoke-static {v0, v1}, Lcom/intsig/camscanner/provider/Documents$Image;->〇080(J)Landroid/net/Uri;

    .line 97
    .line 98
    .line 99
    move-result-object v0

    .line 100
    new-instance v1, Landroid/content/Intent;

    .line 101
    .line 102
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 103
    .line 104
    const-class v5, Lcom/intsig/camscanner/ImagePageViewActivity;

    .line 105
    .line 106
    const-string v6, "android.intent.action.VIEW"

    .line 107
    .line 108
    invoke-direct {v1, v6, v0, v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 109
    .line 110
    .line 111
    const-string v0, "current position"

    .line 112
    .line 113
    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 114
    .line 115
    .line 116
    const-string p1, "image_id"

    .line 117
    .line 118
    invoke-virtual {v1, p1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 119
    .line 120
    .line 121
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 122
    .line 123
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO〇()Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 124
    .line 125
    .line 126
    move-result-object p1

    .line 127
    const-string p2, "extra_key_doc_info"

    .line 128
    .line 129
    invoke-virtual {v1, p2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 130
    .line 131
    .line 132
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 133
    .line 134
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0〇oo()Ljava/lang/String;

    .line 135
    .line 136
    .line 137
    move-result-object p1

    .line 138
    const-string p2, "doc_title"

    .line 139
    .line 140
    invoke-virtual {v1, p2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    .line 142
    .line 143
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 144
    .line 145
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0O00oO()Z

    .line 146
    .line 147
    .line 148
    move-result p1

    .line 149
    const-string p2, "image_page_view_key_offline_folder"

    .line 150
    .line 151
    invoke-virtual {v1, p2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 152
    .line 153
    .line 154
    const-string p1, "opennote"

    .line 155
    .line 156
    invoke-direct {p0, p4}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇00o80oo(Landroid/view/View;)Z

    .line 157
    .line 158
    .line 159
    move-result p2

    .line 160
    invoke-virtual {v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 161
    .line 162
    .line 163
    const-string p1, "constant_add_spec_action_from_part"

    .line 164
    .line 165
    const-string p2, "cs_detail"

    .line 166
    .line 167
    invoke-virtual {v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 168
    .line 169
    .line 170
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 171
    .line 172
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇0008O8()I

    .line 173
    .line 174
    .line 175
    move-result p1

    .line 176
    const-string p2, "extra_doc_type"

    .line 177
    .line 178
    invoke-virtual {v1, p2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 179
    .line 180
    .line 181
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 182
    .line 183
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇〇()[Ljava/lang/String;

    .line 184
    .line 185
    .line 186
    move-result-object p1

    .line 187
    if-eqz p1, :cond_5

    .line 188
    .line 189
    array-length p2, p1

    .line 190
    if-lez p2, :cond_5

    .line 191
    .line 192
    const-string p2, "EXTRA_QUERY_STRING"

    .line 193
    .line 194
    invoke-virtual {v1, p2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 195
    .line 196
    .line 197
    :cond_5
    instance-of p2, p4, Landroid/widget/ImageView;

    .line 198
    .line 199
    if-eqz p2, :cond_6

    .line 200
    .line 201
    move-object p2, p4

    .line 202
    check-cast p2, Landroid/widget/ImageView;

    .line 203
    .line 204
    invoke-virtual {p2}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    .line 205
    .line 206
    .line 207
    move-result-object p2

    .line 208
    instance-of p3, p2, Landroid/graphics/drawable/BitmapDrawable;

    .line 209
    .line 210
    if-eqz p3, :cond_6

    .line 211
    .line 212
    check-cast p2, Landroid/graphics/drawable/BitmapDrawable;

    .line 213
    .line 214
    invoke-virtual {p2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    .line 215
    .line 216
    .line 217
    move-result-object p2

    .line 218
    if-eqz p2, :cond_6

    .line 219
    .line 220
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->isRecycled()Z

    .line 221
    .line 222
    .line 223
    move-result v4

    .line 224
    :cond_6
    new-instance p2, Ljava/lang/StringBuilder;

    .line 225
    .line 226
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 227
    .line 228
    .line 229
    const-string p3, "viewPages: isRecycled: "

    .line 230
    .line 231
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232
    .line 233
    .line 234
    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 235
    .line 236
    .line 237
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 238
    .line 239
    .line 240
    move-result-object p2

    .line 241
    invoke-static {v2, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    .line 243
    .line 244
    const/16 p2, 0x67

    .line 245
    .line 246
    if-nez p1, :cond_7

    .line 247
    .line 248
    if-eqz p4, :cond_7

    .line 249
    .line 250
    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 251
    .line 252
    sget p3, Lcom/intsig/camscanner/app/AppSwitch;->〇80〇808〇O:I

    .line 253
    .line 254
    if-lt p1, p3, :cond_7

    .line 255
    .line 256
    const-string p1, "nubia"

    .line 257
    .line 258
    sget-object p3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    .line 259
    .line 260
    invoke-virtual {p1, p3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 261
    .line 262
    .line 263
    move-result p1

    .line 264
    if-nez p1, :cond_7

    .line 265
    .line 266
    if-nez v4, :cond_7

    .line 267
    .line 268
    :try_start_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 269
    .line 270
    const p3, 0x7f0a0e43

    .line 271
    .line 272
    .line 273
    invoke-virtual {p4, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 274
    .line 275
    .line 276
    move-result-object p3

    .line 277
    const p4, 0x7f131ee6

    .line 278
    .line 279
    .line 280
    invoke-virtual {p0, p4}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 281
    .line 282
    .line 283
    move-result-object p4

    .line 284
    invoke-static {p1, p3, p4}, Landroid/app/ActivityOptions;->makeSceneTransitionAnimation(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;)Landroid/app/ActivityOptions;

    .line 285
    .line 286
    .line 287
    move-result-object p1

    .line 288
    iget-object p3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 289
    .line 290
    invoke-virtual {p1}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    .line 291
    .line 292
    .line 293
    move-result-object p1

    .line 294
    invoke-virtual {p3, v1, p2, p1}, Landroidx/activity/ComponentActivity;->startActivityForResult(Landroid/content/Intent;ILandroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 295
    .line 296
    .line 297
    goto :goto_2

    .line 298
    :catch_0
    move-exception p1

    .line 299
    sget-object p3, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 300
    .line 301
    invoke-static {p3, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 302
    .line 303
    .line 304
    invoke-virtual {p0, v1, p2}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 305
    .line 306
    .line 307
    goto :goto_2

    .line 308
    :cond_7
    invoke-virtual {p0, v1, p2}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 309
    .line 310
    .line 311
    goto :goto_2

    .line 312
    :cond_8
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 313
    .line 314
    invoke-static {p1, p2, p3}, Lcom/intsig/camscanner/tsapp/sync/SyncClient;->o〇O8〇〇o(Landroid/app/Activity;J)V

    .line 315
    .line 316
    .line 317
    const-string p1, "viewPages, enableToOpen = false"

    .line 318
    .line 319
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    .line 321
    .line 322
    :goto_2
    return-void
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public static synthetic 〇〇o0〇8(Lcom/intsig/camscanner/pagelist/PageListFragment;Lcom/intsig/camscanner/sharedir/recommed/RecommendSceneEntity;Landroidx/cardview/widget/CardView;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0OOoO8O0(Lcom/intsig/camscanner/sharedir/recommed/RecommendSceneEntity;Landroidx/cardview/widget/CardView;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private synthetic 〇〇o80Oo(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O0O:Landroid/widget/TextView;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O0O:Landroid/widget/TextView;

    .line 13
    .line 14
    const v0, 0x7f130133

    .line 15
    .line 16
    .line 17
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 18
    .line 19
    .line 20
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O0O:Landroid/widget/TextView;

    .line 21
    .line 22
    const v0, 0x7f080e35

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1, v1, v1, v0, v1}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O0O:Landroid/widget/TextView;

    .line 30
    .line 31
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 32
    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O0O:Landroid/widget/TextView;

    .line 35
    .line 36
    invoke-virtual {p1, v1, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V

    .line 37
    .line 38
    .line 39
    :cond_1
    :goto_0
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private 〇〇o〇0O(Landroid/view/View;)V
    .locals 8

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8OO08o()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_1

    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇Oo〇o8()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 18
    .line 19
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->O088O(Landroid/content/Context;)Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-nez v0, :cond_1

    .line 24
    .line 25
    new-instance v7, Landroid/app/Dialog;

    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 28
    .line 29
    const v1, 0x7f1401d6

    .line 30
    .line 31
    .line 32
    invoke-direct {v7, v0, v1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 33
    .line 34
    .line 35
    const/4 v0, 0x1

    .line 36
    invoke-virtual {v7, v0}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 37
    .line 38
    .line 39
    new-instance v0, Lcom/intsig/camscanner/view/ImagePageTipsLayout;

    .line 40
    .line 41
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/view/ImagePageTipsLayout;-><init>(Landroid/content/Context;)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {v7, v0}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 49
    .line 50
    .line 51
    const v1, 0x7f130dce

    .line 52
    .line 53
    .line 54
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/ImagePageTipsLayout;->setText(I)V

    .line 55
    .line 56
    .line 57
    new-instance v1, Lo0Oo/O08000;

    .line 58
    .line 59
    invoke-direct {v1, v7}, Lo0Oo/O08000;-><init>(Landroid/app/Dialog;)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {v7}, Landroid/app/Dialog;->isShowing()Z

    .line 66
    .line 67
    .line 68
    move-result v1

    .line 69
    if-eqz v1, :cond_0

    .line 70
    .line 71
    const/4 v1, 0x4

    .line 72
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/ImagePageTipsLayout;->setParentVisibility(I)V

    .line 73
    .line 74
    .line 75
    goto :goto_0

    .line 76
    :cond_0
    :try_start_0
    invoke-virtual {v7}, Landroid/app/Dialog;->show()V

    .line 77
    .line 78
    .line 79
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 80
    .line 81
    invoke-static {v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->O8〇880〇8(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    .line 83
    .line 84
    goto :goto_0

    .line 85
    :catch_0
    move-exception v1

    .line 86
    sget-object v2, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 87
    .line 88
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 89
    .line 90
    .line 91
    :goto_0
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 92
    .line 93
    invoke-virtual {v0, p1, v1}, Lcom/intsig/camscanner/view/ImagePageTipsLayout;->Oo08(Landroid/view/View;Landroid/view/View;)V

    .line 94
    .line 95
    .line 96
    new-instance p1, Lcom/intsig/camscanner/pagelist/PageListFragment$10;

    .line 97
    .line 98
    const-wide/16 v3, 0x91d

    .line 99
    .line 100
    const-wide/16 v5, 0x3e8

    .line 101
    .line 102
    move-object v1, p1

    .line 103
    move-object v2, p0

    .line 104
    invoke-direct/range {v1 .. v7}, Lcom/intsig/camscanner/pagelist/PageListFragment$10;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;JJLandroid/app/Dialog;)V

    .line 105
    .line 106
    .line 107
    invoke-virtual {p1}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 108
    .line 109
    .line 110
    :cond_1
    return-void
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic 〇〇〇0(Lcom/intsig/camscanner/pagelist/PageListFragment;Landroidx/cardview/widget/CardView;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o0O(Landroidx/cardview/widget/CardView;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇〇〇00(Lcom/intsig/camscanner/pagelist/PageListFragment;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇0Oo0880(ZLjava/lang/String;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method static bridge synthetic 〇〇〇OOO〇〇(Lcom/intsig/camscanner/pagelist/PageListFragment;[I)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇08oOOO0:[I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇〇〇O〇(Lcom/intsig/camscanner/pagelist/PageListFragment;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8o088〇0(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public O0()Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O00()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8〇()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_2

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇0〇o808〇()Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    sget-object v1, Lcom/intsig/camscanner/pagelist/model/EditType;->DEFAULT:Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 16
    .line 17
    if-ne v0, v1, :cond_0

    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇oo88〇O〇8()V

    .line 20
    .line 21
    .line 22
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O00O()V

    .line 23
    .line 24
    .line 25
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment$12;->〇080:[I

    .line 26
    .line 27
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 28
    .line 29
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇0〇o808〇()Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    .line 34
    .line 35
    .line 36
    move-result v1

    .line 37
    aget v0, v0, v1

    .line 38
    .line 39
    const/4 v1, 0x1

    .line 40
    const/4 v2, 0x0

    .line 41
    if-eq v0, v1, :cond_1

    .line 42
    .line 43
    const/4 v1, 0x2

    .line 44
    if-eq v0, v1, :cond_1

    .line 45
    .line 46
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OoOo()V

    .line 47
    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_1
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇Ooo0o(I)V

    .line 51
    .line 52
    .line 53
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 54
    .line 55
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇〇00(Z)V

    .line 56
    .line 57
    .line 58
    :cond_2
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public O00O()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇()Z

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    const/4 v2, 0x1

    .line 8
    xor-int/2addr v1, v2

    .line 9
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0ooOOo(Z)V

    .line 10
    .line 11
    .line 12
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 13
    .line 14
    new-instance v1, Ljava/lang/StringBuilder;

    .line 15
    .line 16
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 17
    .line 18
    .line 19
    const-string v3, "onEditModeChanged()  editmode="

    .line 20
    .line 21
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 25
    .line 26
    invoke-virtual {v3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇()Z

    .line 27
    .line 28
    .line 29
    move-result v3

    .line 30
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇〇O()Lcom/intsig/camscanner/pagelist/bubble/PageListBubbleControl;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/bubble/PageListBubbleControl;->oO80()V

    .line 45
    .line 46
    .line 47
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 48
    .line 49
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇()Z

    .line 50
    .line 51
    .line 52
    move-result v1

    .line 53
    const/4 v3, 0x0

    .line 54
    if-eqz v1, :cond_0

    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oo8ooo8O:Lcom/intsig/camscanner/view/PullToSyncRecyclerView;

    .line 57
    .line 58
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->setLock(Z)V

    .line 59
    .line 60
    .line 61
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08〇o0O:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 62
    .line 63
    const/16 v1, 0x8

    .line 64
    .line 65
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;->setVisibility(I)V

    .line 66
    .line 67
    .line 68
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o00o0O〇〇o()Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->oO80()V

    .line 73
    .line 74
    .line 75
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->OOO0o〇()V

    .line 76
    .line 77
    .line 78
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO8ooO8〇:Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;

    .line 79
    .line 80
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;->〇o〇()V

    .line 81
    .line 82
    .line 83
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO8ooO8〇:Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;

    .line 84
    .line 85
    invoke-interface {v0, v2}, Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;->〇8o8o〇(Z)V

    .line 86
    .line 87
    .line 88
    iput-boolean v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO〇00〇0O:Z

    .line 89
    .line 90
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 91
    .line 92
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇o00〇〇Oo()V

    .line 97
    .line 98
    .line 99
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 100
    .line 101
    new-array v1, v2, [Ljava/lang/Object;

    .line 102
    .line 103
    const-string v2, "0"

    .line 104
    .line 105
    aput-object v2, v1, v3

    .line 106
    .line 107
    const v2, 0x7f130164

    .line 108
    .line 109
    .line 110
    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 111
    .line 112
    .line 113
    move-result-object v0

    .line 114
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->Oo80:Ljava/lang/String;

    .line 115
    .line 116
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 117
    .line 118
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇()Z

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->Oo0(Z)V

    .line 123
    .line 124
    .line 125
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0〇8o〇(Z)V

    .line 126
    .line 127
    .line 128
    goto :goto_0

    .line 129
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oo8ooo8O:Lcom/intsig/camscanner/view/PullToSyncRecyclerView;

    .line 130
    .line 131
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O0Oo〇8()Z

    .line 132
    .line 133
    .line 134
    move-result v4

    .line 135
    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->setLock(Z)V

    .line 136
    .line 137
    .line 138
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->ooO:Landroid/view/View;

    .line 139
    .line 140
    invoke-virtual {p0, v1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setToolbarMenu(Landroid/view/View;)V

    .line 141
    .line 142
    .line 143
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 144
    .line 145
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0O〇OOo()Z

    .line 146
    .line 147
    .line 148
    move-result v1

    .line 149
    if-nez v1, :cond_1

    .line 150
    .line 151
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08〇o0O:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 152
    .line 153
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;->setVisibility(I)V

    .line 154
    .line 155
    .line 156
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o00o0O〇〇o()Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;

    .line 157
    .line 158
    .line 159
    move-result-object v1

    .line 160
    iget-object v4, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 161
    .line 162
    invoke-virtual {v1, v3, v4}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇〇808〇(ZLandroidx/fragment/app/FragmentActivity;)V

    .line 163
    .line 164
    .line 165
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oo〇O〇o〇8(Z)V

    .line 166
    .line 167
    .line 168
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 169
    .line 170
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oooO888()V

    .line 171
    .line 172
    .line 173
    new-instance v1, Ljava/lang/StringBuilder;

    .line 174
    .line 175
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 176
    .line 177
    .line 178
    const-string v2, "before edit--- doctitle:"

    .line 179
    .line 180
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    .line 182
    .line 183
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 184
    .line 185
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0〇oo()Ljava/lang/String;

    .line 186
    .line 187
    .line 188
    move-result-object v2

    .line 189
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    .line 191
    .line 192
    const-string v2, ",    pdf path:"

    .line 193
    .line 194
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    .line 196
    .line 197
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 198
    .line 199
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇0〇0o8()Ljava/lang/String;

    .line 200
    .line 201
    .line 202
    move-result-object v2

    .line 203
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    .line 205
    .line 206
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 207
    .line 208
    .line 209
    move-result-object v1

    .line 210
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    .line 212
    .line 213
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO8ooO8〇:Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;

    .line 214
    .line 215
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;->o〇0()V

    .line 216
    .line 217
    .line 218
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 219
    .line 220
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇()Z

    .line 221
    .line 222
    .line 223
    move-result v0

    .line 224
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->Oo0(Z)V

    .line 225
    .line 226
    .line 227
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 228
    .line 229
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O880O〇()V

    .line 230
    .line 231
    .line 232
    return-void
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public O0oO008(I)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇〇8〇〇(I)Z

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-nez p1, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 9
    .line 10
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇0008O8()I

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    invoke-static {p1}, Lcom/intsig/camscanner/certificate_package/util/CertificateDBUtil;->OO0o〇〇〇〇0(I)Z

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    if-eqz p1, :cond_1

    .line 19
    .line 20
    return-void

    .line 21
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 22
    .line 23
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO80OOO〇()Z

    .line 24
    .line 25
    .line 26
    move-result p1

    .line 27
    if-eqz p1, :cond_2

    .line 28
    .line 29
    return-void

    .line 30
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇〇O()Lcom/intsig/camscanner/pagelist/bubble/PageListBubbleControl;

    .line 31
    .line 32
    .line 33
    move-result-object p1

    .line 34
    const/4 v0, 0x0

    .line 35
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/pagelist/bubble/PageListBubbleControl;->〇80〇808〇O(Ljava/lang/Runnable;)V

    .line 36
    .line 37
    .line 38
    const-string p1, "CSCollageEntrance"

    .line 39
    .line 40
    invoke-static {p1}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public O8O〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->Oo0〇Ooo:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public O8o()Landroid/graphics/Rect;
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-lez v0, :cond_0

    .line 9
    .line 10
    const/4 v0, 0x2

    .line 11
    new-array v0, v0, [I

    .line 12
    .line 13
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->Oo〇〇〇〇([I)Landroid/view/View;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    if-eqz v2, :cond_0

    .line 18
    .line 19
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 20
    .line 21
    invoke-virtual {v3}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    const v4, 0x7f070191

    .line 26
    .line 27
    .line 28
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 29
    .line 30
    .line 31
    move-result v3

    .line 32
    new-instance v4, Landroid/graphics/Rect;

    .line 33
    .line 34
    const/4 v5, 0x0

    .line 35
    aget v5, v0, v5

    .line 36
    .line 37
    sub-int v6, v5, v3

    .line 38
    .line 39
    const/4 v7, 0x1

    .line 40
    aget v8, v0, v7

    .line 41
    .line 42
    sub-int/2addr v8, v3

    .line 43
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    .line 44
    .line 45
    .line 46
    move-result v9

    .line 47
    add-int/2addr v5, v9

    .line 48
    add-int/2addr v5, v3

    .line 49
    aget v0, v0, v7

    .line 50
    .line 51
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    .line 52
    .line 53
    .line 54
    move-result v2

    .line 55
    add-int/2addr v0, v2

    .line 56
    add-int/2addr v0, v3

    .line 57
    invoke-direct {v4, v6, v8, v5, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 58
    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_0
    move-object v4, v1

    .line 62
    :goto_0
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 63
    .line 64
    new-instance v2, Ljava/lang/StringBuilder;

    .line 65
    .line 66
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 67
    .line 68
    .line 69
    const-string v3, "getFistMovePageRect area = "

    .line 70
    .line 71
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    if-eqz v4, :cond_1

    .line 75
    .line 76
    invoke-virtual {v4}, Landroid/graphics/Rect;->toString()Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object v1

    .line 80
    :cond_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object v1

    .line 87
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    return-object v4
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public O8o〇O0(I)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "scrollToSearchedImagePosition >>> searchedPos = "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    if-gez p1, :cond_0

    .line 24
    .line 25
    return-void

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 27
    .line 28
    invoke-virtual {v0}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->〇O888o0o()Ljava/util/List;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-static {v0}, Lcom/intsig/utils/ListUtils;->〇o〇(Ljava/util/List;)Z

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    if-eqz v0, :cond_1

    .line 37
    .line 38
    return-void

    .line 39
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 40
    .line 41
    invoke-virtual {v0}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->〇O888o0o()Ljava/util/List;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    if-lt p1, v0, :cond_2

    .line 50
    .line 51
    return-void

    .line 52
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 53
    .line 54
    new-instance v1, Lo0Oo/OOo8o〇O;

    .line 55
    .line 56
    invoke-direct {v1, p0, p1}, Lo0Oo/OOo8o〇O;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;I)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 60
    .line 61
    .line 62
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public bridge synthetic OOo(Landroid/view/View;ILjava/lang/Object;I)Z
    .locals 0
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    check-cast p3, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8080o8〇〇(Landroid/view/View;ILcom/intsig/camscanner/pagelist/model/PageTypeItem;I)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    return p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public OO〇()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/pagelist/model/PageTypeItem;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->〇O888o0o()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public OO〇0008O8()Landroidx/recyclerview/widget/RecyclerView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic Oo0oOo〇0(Landroid/view/View;ILjava/lang/Object;I)V
    .locals 0
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    check-cast p3, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oO80O0(Landroid/view/View;ILcom/intsig/camscanner/pagelist/model/PageTypeItem;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public Ooo8(Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->isComputingLayout()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 10
    .line 11
    invoke-virtual {v0, p1}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->〇0〇O0088o(Ljava/lang/Object;)Z

    .line 12
    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 16
    .line 17
    new-instance v1, Lo0Oo/〇8;

    .line 18
    .line 19
    invoke-direct {v1, p0, p1}, Lo0Oo/〇8;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;)V

    .line 20
    .line 21
    .line 22
    const-wide/16 v2, 0x64

    .line 23
    .line 24
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 25
    .line 26
    .line 27
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 28
    .line 29
    const/16 v0, 0x12

    .line 30
    .line 31
    invoke-static {p1, v0}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 32
    .line 33
    .line 34
    move-result p1

    .line 35
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8o8o(I)V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public O〇0()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "updateActionBarCoState = "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 14
    .line 15
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oOo()I

    .line 16
    .line 17
    .line 18
    move-result v2

    .line 19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇00OO()I

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    const/4 v1, -0x1

    .line 36
    if-ne v0, v1, :cond_0

    .line 37
    .line 38
    sget-boolean v0, Lcom/intsig/camscanner/app/AppSwitch;->〇〇888:Z

    .line 39
    .line 40
    if-eqz v0, :cond_0

    .line 41
    .line 42
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO8ooO8〇:Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;

    .line 43
    .line 44
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;->Oo08()V

    .line 45
    .line 46
    .line 47
    :cond_0
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public O〇Oo(JF)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public beforeInitialize()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO0oO()V

    .line 2
    .line 3
    .line 4
    invoke-static {p0}, Lcom/intsig/camscanner/eventbus/CsEventBus;->O8(Ljava/lang/Object;)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public copyOrMoveDoc(Lcom/intsig/camscanner/pagelist/newpagelist/data/CopyOrMoveDocEvent;)V
    .locals 2
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        sticky = true
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->MAIN:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "copyOrMoveDoc"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 9
    .line 10
    iget-object v0, v0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇80:Landroidx/lifecycle/MutableLiveData;

    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/data/CopyOrMoveDocEvent;->〇080()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    invoke-virtual {v0, p1}, Landroidx/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    .line 17
    .line 18
    .line 19
    const/4 p1, 0x0

    .line 20
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO〇OOo:Z

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public fromGuideScanKit(Lcom/intsig/camscanner/eventbus/ScanKitEvent;)V
    .locals 1
    .annotation runtime Lorg/greenrobot/eventbus/Subscribe;
        sticky = true
        threadMode = .enum Lorg/greenrobot/eventbus/ThreadMode;->MAIN:Lorg/greenrobot/eventbus/ThreadMode;
    .end annotation

    .line 1
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 2
    .line 3
    const-string v0, "fromGuideScanKit"

    .line 4
    .line 5
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 p1, 0x1

    .line 9
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8oOo0:Z

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public getCurrentActivity()Landroidx/fragment/app/FragmentActivity;
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getFragment()Landroidx/fragment/app/Fragment;
    .locals 0

    .line 1
    return-object p0
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getHandler()Landroid/os/Handler;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o880:Landroid/os/Handler;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 10

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "onActivityCreated"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    sget-object v1, Lcom/intsig/base/ToolbarThemeGet;->〇080:Lcom/intsig/base/ToolbarThemeGet;

    .line 13
    .line 14
    invoke-virtual {v1}, Lcom/intsig/base/ToolbarThemeGet;->〇o〇()I

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    iput v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8〇OO:I

    .line 23
    .line 24
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    const v1, 0x7f06007e

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    iput v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->Ooo08:I

    .line 36
    .line 37
    new-instance v0, Lcom/intsig/camscanner/pagelist/PageListFragment$PhoneDevice;

    .line 38
    .line 39
    const/4 v1, 0x0

    .line 40
    invoke-direct {v0, p0, v1}, Lcom/intsig/camscanner/pagelist/PageListFragment$PhoneDevice;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;Lo0Oo/〇000〇〇08;)V

    .line 41
    .line 42
    .line 43
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO8ooO8〇:Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;

    .line 44
    .line 45
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 46
    .line 47
    const/4 v1, 0x1

    .line 48
    new-array v1, v1, [Ljava/lang/Object;

    .line 49
    .line 50
    const-string v2, "0"

    .line 51
    .line 52
    const/4 v3, 0x0

    .line 53
    aput-object v2, v1, v3

    .line 54
    .line 55
    const v2, 0x7f130164

    .line 56
    .line 57
    .line 58
    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->Oo80:Ljava/lang/String;

    .line 63
    .line 64
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o00oooo()V

    .line 65
    .line 66
    .line 67
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇80o〇o0()V

    .line 68
    .line 69
    .line 70
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O8oO0()V

    .line 71
    .line 72
    .line 73
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 74
    .line 75
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇〇0880()V

    .line 76
    .line 77
    .line 78
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 79
    .line 80
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    .line 81
    .line 82
    .line 83
    move-result-object v1

    .line 84
    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 85
    .line 86
    .line 87
    move-result-object v1

    .line 88
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇800OO〇0O:Ljava/lang/String;

    .line 89
    .line 90
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO00〇o(Landroid/content/Intent;Ljava/lang/String;)V

    .line 91
    .line 92
    .line 93
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇oO:Lcom/intsig/camscanner/view/AbstractPullToSyncView$PullToSyncAssistant;

    .line 94
    .line 95
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/AbstractPullToSyncView$PullToSyncAssistant;->〇〇8O0〇8()V

    .line 96
    .line 97
    .line 98
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 99
    .line 100
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o8oO()V

    .line 101
    .line 102
    .line 103
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 104
    .line 105
    sget-object v1, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 106
    .line 107
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 108
    .line 109
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 110
    .line 111
    .line 112
    move-result-wide v4

    .line 113
    invoke-static {v1, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 114
    .line 115
    .line 116
    move-result-object v1

    .line 117
    invoke-static {v0, v1}, Lcom/intsig/camscanner/db/dao/DocumentDao;->oo88o8O(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    .line 118
    .line 119
    .line 120
    move-result-object v6

    .line 121
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 122
    .line 123
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 124
    .line 125
    .line 126
    move-result-object v0

    .line 127
    if-eqz v0, :cond_0

    .line 128
    .line 129
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 130
    .line 131
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 132
    .line 133
    .line 134
    move-result-object v0

    .line 135
    sget-object v1, Lcom/intsig/camscanner/util/CONSTANT;->〇8o8o〇:Ljava/lang/String;

    .line 136
    .line 137
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 138
    .line 139
    .line 140
    move-result v0

    .line 141
    if-nez v0, :cond_0

    .line 142
    .line 143
    sget-object v4, Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;->〇080:Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;

    .line 144
    .line 145
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 146
    .line 147
    .line 148
    move-result-object v5

    .line 149
    const/4 v7, 0x1

    .line 150
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 151
    .line 152
    .line 153
    move-result-wide v8

    .line 154
    invoke-virtual/range {v4 .. v9}, Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;->OoO8(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;IJ)V

    .line 155
    .line 156
    .line 157
    invoke-static {}, Lcom/intsig/camscanner/transfer/CsTransferDocUtil;->OO0o〇〇〇〇0()Ljava/lang/Long;

    .line 158
    .line 159
    .line 160
    move-result-object v0

    .line 161
    if-eqz v0, :cond_1

    .line 162
    .line 163
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 164
    .line 165
    new-instance v2, Lo0Oo/O8〇o;

    .line 166
    .line 167
    invoke-direct {v2, p0, v0}, Lo0Oo/O8〇o;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;Ljava/lang/Long;)V

    .line 168
    .line 169
    .line 170
    invoke-virtual {v1, v2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 171
    .line 172
    .line 173
    goto :goto_0

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 175
    .line 176
    new-instance v1, Lo0Oo/〇00〇8;

    .line 177
    .line 178
    invoke-direct {v1, p0}, Lo0Oo/〇00〇8;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 179
    .line 180
    .line 181
    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 182
    .line 183
    .line 184
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O0o0〇8o()V

    .line 185
    .line 186
    .line 187
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oo〇O〇o〇8(Z)V

    .line 188
    .line 189
    .line 190
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 191
    .line 192
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8OO08o()Z

    .line 193
    .line 194
    .line 195
    move-result v0

    .line 196
    if-eqz v0, :cond_2

    .line 197
    .line 198
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oo8ooo8O:Lcom/intsig/camscanner/view/PullToSyncRecyclerView;

    .line 199
    .line 200
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 201
    .line 202
    .line 203
    goto :goto_1

    .line 204
    :cond_2
    invoke-static {}, Lcom/intsig/base/ToolbarThemeGet;->Oo08()Z

    .line 205
    .line 206
    .line 207
    move-result v0

    .line 208
    if-eqz v0, :cond_3

    .line 209
    .line 210
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oo8ooo8O:Lcom/intsig/camscanner/view/PullToSyncRecyclerView;

    .line 211
    .line 212
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 213
    .line 214
    .line 215
    goto :goto_1

    .line 216
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 217
    .line 218
    new-instance v1, Lo0Oo/〇o;

    .line 219
    .line 220
    invoke-direct {v1, p0}, Lo0Oo/〇o;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 221
    .line 222
    .line 223
    const-wide/16 v2, 0x64

    .line 224
    .line 225
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 226
    .line 227
    .line 228
    :goto_1
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 229
    .line 230
    .line 231
    move-result-object v0

    .line 232
    new-instance v1, Lo0Oo/o0ooO;

    .line 233
    .line 234
    invoke-direct {v1, p0}, Lo0Oo/o0ooO;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 235
    .line 236
    .line 237
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 238
    .line 239
    .line 240
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 241
    .line 242
    .line 243
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O0〇o8o〇〇()V

    .line 244
    .line 245
    .line 246
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O80〇()V

    .line 247
    .line 248
    .line 249
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇8088()V

    .line 250
    .line 251
    .line 252
    new-instance p1, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/TipsManager;

    .line 253
    .line 254
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 255
    .line 256
    .line 257
    move-result-object v0

    .line 258
    invoke-direct {p1, p0, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/TipsManager;-><init>(Lcom/intsig/mvp/fragment/BaseChangeFragment;Landroid/app/Activity;)V

    .line 259
    .line 260
    .line 261
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇08O:Lcom/intsig/camscanner/pagelist/newpagelist/dialog/TipsManager;

    .line 262
    .line 263
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->ooO:Landroid/view/View;

    .line 264
    .line 265
    const v1, 0x7f0a0216

    .line 266
    .line 267
    .line 268
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 269
    .line 270
    .line 271
    move-result-object v0

    .line 272
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/pagelist/newpagelist/dialog/TipsManager;->〇00(Landroid/view/View;)V

    .line 273
    .line 274
    .line 275
    invoke-static {}, Lcom/intsig/camscanner/pagedetail/PageDetailReeditUtil;->〇80〇808〇O()V

    .line 276
    .line 277
    .line 278
    return-void
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public interceptBackPressed()Z
    .locals 5

    .line 1
    const-string v0, "CSList"

    .line 2
    .line 3
    const-string v1, "back"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0o8〇O()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    const/4 v1, 0x1

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8o80O()V

    .line 18
    .line 19
    .line 20
    return v1

    .line 21
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 22
    .line 23
    new-instance v2, Ljava/lang/StringBuilder;

    .line 24
    .line 25
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 26
    .line 27
    .line 28
    const-string v3, "pageListPresenter.isEditMode()="

    .line 29
    .line 30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 34
    .line 35
    invoke-virtual {v3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇()Z

    .line 36
    .line 37
    .line 38
    move-result v3

    .line 39
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v2

    .line 46
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 50
    .line 51
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇()Z

    .line 52
    .line 53
    .line 54
    move-result v2

    .line 55
    if-eqz v2, :cond_1

    .line 56
    .line 57
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O00O()V

    .line 58
    .line 59
    .line 60
    return v1

    .line 61
    :cond_1
    const-string v2, "onBackPressed"

    .line 62
    .line 63
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 67
    .line 68
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8〇o〇8()Z

    .line 69
    .line 70
    .line 71
    move-result v2

    .line 72
    if-eqz v2, :cond_2

    .line 73
    .line 74
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 75
    .line 76
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o8O〇O()I

    .line 77
    .line 78
    .line 79
    move-result v2

    .line 80
    if-nez v2, :cond_2

    .line 81
    .line 82
    const-string v2, "onBackPressed mPageNum = 0"

    .line 83
    .line 84
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 88
    .line 89
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 90
    .line 91
    invoke-virtual {v3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 92
    .line 93
    .line 94
    move-result-wide v3

    .line 95
    invoke-static {v2, v3, v4}, Lcom/intsig/camscanner/db/dao/ImageDao;->〇o〇(Landroid/content/Context;J)Z

    .line 96
    .line 97
    .line 98
    move-result v2

    .line 99
    if-nez v2, :cond_2

    .line 100
    .line 101
    const/16 v0, 0x68

    .line 102
    .line 103
    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->showDialog(I)V

    .line 104
    .line 105
    .line 106
    return v1

    .line 107
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    .line 108
    .line 109
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 110
    .line 111
    .line 112
    const-string v3, "PreferenceHelper.isShowLongPressGuid(mActivity):"

    .line 113
    .line 114
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 118
    .line 119
    invoke-static {v3}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO800o(Landroid/content/Context;)Z

    .line 120
    .line 121
    .line 122
    move-result v3

    .line 123
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 124
    .line 125
    .line 126
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 127
    .line 128
    .line 129
    move-result-object v2

    .line 130
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    .line 132
    .line 133
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 134
    .line 135
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO800o(Landroid/content/Context;)Z

    .line 136
    .line 137
    .line 138
    move-result v0

    .line 139
    if-eqz v0, :cond_3

    .line 140
    .line 141
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 142
    .line 143
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO8〇(Landroid/content/Context;)V

    .line 144
    .line 145
    .line 146
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 147
    .line 148
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇8O0O80〇(Landroid/content/Context;)Z

    .line 149
    .line 150
    .line 151
    move-result v0

    .line 152
    if-nez v0, :cond_4

    .line 153
    .line 154
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 155
    .line 156
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->Oo8O(Landroid/content/Context;Z)V

    .line 157
    .line 158
    .line 159
    :cond_4
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8oOo0:Z

    .line 160
    .line 161
    if-eqz v0, :cond_5

    .line 162
    .line 163
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 164
    .line 165
    invoke-static {v0}, Lcom/intsig/camscanner/mainmenu/MainPageRoute;->〇O888o0o(Landroid/content/Context;)Landroid/content/Intent;

    .line 166
    .line 167
    .line 168
    move-result-object v1

    .line 169
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 170
    .line 171
    .line 172
    :cond_5
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇O0ooo()V

    .line 173
    .line 174
    .line 175
    const/4 v0, 0x0

    .line 176
    return v0
    .line 177
.end method

.method public o08o〇0(ILjava/lang/String;)V
    .locals 4

    .line 1
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 9
    .line 10
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    const/4 v1, 0x1

    .line 15
    invoke-virtual {v0, p2, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 16
    .line 17
    .line 18
    move-result-object p2

    .line 19
    iget-object p2, p2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    .line 20
    .line 21
    invoke-virtual {p2, v0}, Landroid/content/pm/PackageItemInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    .line 22
    .line 23
    .line 24
    move-result-object p2

    .line 25
    const/4 v0, 0x4

    .line 26
    new-array v0, v0, [Ljava/lang/Object;

    .line 27
    .line 28
    new-instance v2, Ljava/lang/StringBuilder;

    .line 29
    .line 30
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 31
    .line 32
    .line 33
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    const-string v3, ", "

    .line 37
    .line 38
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-static {p1}, Lcom/intsig/camscanner/openapi/ReturnCode;->〇080(I)Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    const/4 v2, 0x0

    .line 53
    aput-object p1, v0, v2

    .line 54
    .line 55
    aput-object p2, v0, v1

    .line 56
    .line 57
    const p1, 0x7f1304ed

    .line 58
    .line 59
    .line 60
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object p1

    .line 64
    const/4 v3, 0x2

    .line 65
    aput-object p1, v0, v3

    .line 66
    .line 67
    const/4 p1, 0x3

    .line 68
    aput-object p2, v0, p1

    .line 69
    .line 70
    const p1, 0x7f13008f

    .line 71
    .line 72
    .line 73
    invoke-virtual {p0, p1, v0}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 74
    .line 75
    .line 76
    move-result-object p1

    .line 77
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 78
    .line 79
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 80
    .line 81
    invoke-direct {v0, v3}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 82
    .line 83
    .line 84
    new-array v1, v1, [Ljava/lang/Object;

    .line 85
    .line 86
    aput-object p2, v1, v2

    .line 87
    .line 88
    const p2, 0x7f1300aa

    .line 89
    .line 90
    .line 91
    invoke-virtual {p0, p2, v1}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 92
    .line 93
    .line 94
    move-result-object p2

    .line 95
    invoke-virtual {v0, p2}, Lcom/intsig/app/AlertDialog$Builder;->〇〇〇0〇〇0(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 96
    .line 97
    .line 98
    move-result-object p2

    .line 99
    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    .line 100
    .line 101
    .line 102
    move-result-object p1

    .line 103
    invoke-virtual {p2, p1}, Lcom/intsig/app/AlertDialog$Builder;->〇O00(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 104
    .line 105
    .line 106
    move-result-object p1

    .line 107
    const p2, 0x7f131e36

    .line 108
    .line 109
    .line 110
    const/4 v0, 0x0

    .line 111
    invoke-virtual {p1, p2, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 112
    .line 113
    .line 114
    move-result-object p1

    .line 115
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 116
    .line 117
    .line 118
    move-result-object p1

    .line 119
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    .line 121
    .line 122
    goto :goto_0

    .line 123
    :catch_0
    move-exception p1

    .line 124
    sget-object p2, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 125
    .line 126
    const-string v0, "NameNotFoundException"

    .line 127
    .line 128
    invoke-static {p2, v0, p1}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 129
    .line 130
    .line 131
    :goto_0
    return-void
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public o8080o8〇〇(Landroid/view/View;ILcom/intsig/camscanner/pagelist/model/PageTypeItem;I)Z
    .locals 0
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/pagelist/model/PageTypeItem;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 2
    .line 3
    new-instance p2, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string p3, "User Operation: onItemLongClick isEdit="

    .line 9
    .line 10
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    iget-object p3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 14
    .line 15
    invoke-virtual {p3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇()Z

    .line 16
    .line 17
    .line 18
    move-result p3

    .line 19
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object p2

    .line 26
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 30
    .line 31
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0o8〇O()Z

    .line 32
    .line 33
    .line 34
    move-result p1

    .line 35
    const/4 p2, 0x1

    .line 36
    if-eqz p1, :cond_0

    .line 37
    .line 38
    return p2

    .line 39
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 40
    .line 41
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇()Z

    .line 42
    .line 43
    .line 44
    move-result p1

    .line 45
    if-nez p1, :cond_1

    .line 46
    .line 47
    invoke-direct {p0, p4}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oO0o(I)Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 48
    .line 49
    .line 50
    move-result-object p1

    .line 51
    if-eqz p1, :cond_1

    .line 52
    .line 53
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageImageItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/pagelist/model/PageItem;

    .line 54
    .line 55
    .line 56
    move-result-object p3

    .line 57
    iget-wide p3, p3, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇080:J

    .line 58
    .line 59
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageImageItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/pagelist/model/PageItem;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    iget p1, p1, Lcom/intsig/camscanner/pagelist/model/PageItem;->o〇0:I

    .line 64
    .line 65
    invoke-direct {p0, p3, p4, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇O(JI)V

    .line 66
    .line 67
    .line 68
    :cond_1
    return p2
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public o88O8()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "finishWhenDocNotExist mNeedAutoFinish="

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    iget-boolean v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O88O:Z

    .line 14
    .line 15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 26
    .line 27
    if-eqz v0, :cond_2

    .line 28
    .line 29
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    if-eqz v0, :cond_0

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O88O:Z

    .line 37
    .line 38
    if-nez v0, :cond_2

    .line 39
    .line 40
    const/4 v0, 0x1

    .line 41
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O88O:Z

    .line 42
    .line 43
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 44
    .line 45
    iget-boolean v0, v0, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO:Z

    .line 46
    .line 47
    if-nez v0, :cond_1

    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 50
    .line 51
    const v1, 0x7f131d12

    .line 52
    .line 53
    .line 54
    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->oO80(Landroid/content/Context;I)V

    .line 55
    .line 56
    .line 57
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 58
    .line 59
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 60
    .line 61
    .line 62
    :cond_2
    :goto_0
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o8O(J)V
    .locals 8

    .line 1
    const-wide/16 v0, 0x1a

    .line 2
    .line 3
    const-string v2, "CSList"

    .line 4
    .line 5
    cmp-long v3, p1, v0

    .line 6
    .line 7
    if-nez v3, :cond_1

    .line 8
    .line 9
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 10
    .line 11
    const-string p2, "User Operation:  go to pdf app"

    .line 12
    .line 13
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string p1, "cs_pdf_icon"

    .line 17
    .line 18
    invoke-static {v2, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    invoke-static {}, Lcom/intsig/camscanner/pdf/PreferenceCsPdfHelper;->O8()Z

    .line 22
    .line 23
    .line 24
    move-result p1

    .line 25
    if-nez p1, :cond_0

    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 28
    .line 29
    const-string p2, "com.intsig.cspdf"

    .line 30
    .line 31
    const-string v0, "gp_cs_pdf_app_edit"

    .line 32
    .line 33
    invoke-static {p1, p2, v0}, Lcom/intsig/camscanner/app/IntentUtil;->〇O00(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    .line 34
    .line 35
    .line 36
    return-void

    .line 37
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 38
    .line 39
    const/4 p2, 0x6

    .line 40
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇008〇o0〇〇(I)V

    .line 41
    .line 42
    .line 43
    goto/16 :goto_0

    .line 44
    .line 45
    :cond_1
    const-wide/16 v0, 0x0

    .line 46
    .line 47
    const/4 v3, 0x0

    .line 48
    const/4 v4, 0x1

    .line 49
    cmp-long v5, p1, v0

    .line 50
    .line 51
    if-nez v5, :cond_2

    .line 52
    .line 53
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 54
    .line 55
    const-string p2, "User Operation:  view pdf"

    .line 56
    .line 57
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    const-string p1, "pdf_preview"

    .line 61
    .line 62
    invoke-static {v2, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    iput-boolean v4, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇〇0o〇〇0:Z

    .line 66
    .line 67
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 68
    .line 69
    invoke-virtual {p1, v3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇008〇o0〇〇(I)V

    .line 70
    .line 71
    .line 72
    goto/16 :goto_0

    .line 73
    .line 74
    :cond_2
    const-wide/16 v0, 0x1

    .line 75
    .line 76
    cmp-long v5, p1, v0

    .line 77
    .line 78
    if-nez v5, :cond_3

    .line 79
    .line 80
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 81
    .line 82
    const-string p2, "User Operation: share"

    .line 83
    .line 84
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    const-string p1, "scheme"

    .line 88
    .line 89
    const-string p2, "mod02"

    .line 90
    .line 91
    const-string v0, "share"

    .line 92
    .line 93
    invoke-static {v2, v0, p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    .line 95
    .line 96
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 97
    .line 98
    invoke-virtual {p1, v3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OoOOo8(Z)V

    .line 99
    .line 100
    .line 101
    goto/16 :goto_0

    .line 102
    .line 103
    :cond_3
    const-wide/16 v0, 0x2

    .line 104
    .line 105
    cmp-long v5, p1, v0

    .line 106
    .line 107
    if-nez v5, :cond_4

    .line 108
    .line 109
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 110
    .line 111
    invoke-virtual {p1, v4}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OOo0O(Z)V

    .line 112
    .line 113
    .line 114
    goto/16 :goto_0

    .line 115
    .line 116
    :cond_4
    const-wide/16 v0, 0x3

    .line 117
    .line 118
    cmp-long v5, p1, v0

    .line 119
    .line 120
    if-nez v5, :cond_5

    .line 121
    .line 122
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 123
    .line 124
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇oO〇oo8o()V

    .line 125
    .line 126
    .line 127
    goto/16 :goto_0

    .line 128
    .line 129
    :cond_5
    const-wide/16 v0, 0x6

    .line 130
    .line 131
    cmp-long v5, p1, v0

    .line 132
    .line 133
    if-nez v5, :cond_6

    .line 134
    .line 135
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 136
    .line 137
    const-string p2, "User Operation: mail to myself"

    .line 138
    .line 139
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    .line 141
    .line 142
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 143
    .line 144
    new-instance p2, Lo0Oo/〇〇〇0〇〇0;

    .line 145
    .line 146
    invoke-direct {p2, p0}, Lo0Oo/〇〇〇0〇〇0;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 147
    .line 148
    .line 149
    invoke-static {p1, p2}, Lcom/intsig/camscanner/ipo/IPOCheck;->〇80〇808〇O(Landroidx/fragment/app/FragmentActivity;Ljava/lang/Runnable;)V

    .line 150
    .line 151
    .line 152
    goto/16 :goto_0

    .line 153
    .line 154
    :cond_6
    const-wide/16 v0, 0x7

    .line 155
    .line 156
    cmp-long v5, p1, v0

    .line 157
    .line 158
    if-nez v5, :cond_7

    .line 159
    .line 160
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 161
    .line 162
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O8〇8O0oO()V

    .line 163
    .line 164
    .line 165
    goto/16 :goto_0

    .line 166
    .line 167
    :cond_7
    const-wide/16 v0, 0x8

    .line 168
    .line 169
    cmp-long v5, p1, v0

    .line 170
    .line 171
    if-nez v5, :cond_8

    .line 172
    .line 173
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 174
    .line 175
    const-string p2, "User Operation:  rename"

    .line 176
    .line 177
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    .line 179
    .line 180
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOOo()V

    .line 181
    .line 182
    .line 183
    goto/16 :goto_0

    .line 184
    .line 185
    :cond_8
    const-wide/16 v0, 0x9

    .line 186
    .line 187
    const-string v5, "CSMorePop"

    .line 188
    .line 189
    cmp-long v6, p1, v0

    .line 190
    .line 191
    if-nez v6, :cond_9

    .line 192
    .line 193
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 194
    .line 195
    const-string p2, "User Operation:  manual sort"

    .line 196
    .line 197
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    .line 199
    .line 200
    const-string p1, "manual_sort"

    .line 201
    .line 202
    invoke-static {v5, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    .line 204
    .line 205
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇80O80O〇0()V

    .line 206
    .line 207
    .line 208
    goto/16 :goto_0

    .line 209
    .line 210
    :cond_9
    const-wide/16 v0, 0xd

    .line 211
    .line 212
    cmp-long v6, p1, v0

    .line 213
    .line 214
    if-nez v6, :cond_a

    .line 215
    .line 216
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 217
    .line 218
    const-string p2, "User Operation: to select mode"

    .line 219
    .line 220
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    .line 222
    .line 223
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O00O()V

    .line 224
    .line 225
    .line 226
    goto/16 :goto_0

    .line 227
    .line 228
    :cond_a
    const-wide/16 v0, 0xa

    .line 229
    .line 230
    cmp-long v6, p1, v0

    .line 231
    .line 232
    if-nez v6, :cond_b

    .line 233
    .line 234
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 235
    .line 236
    const-string p2, "User Operation: TOP_MENU_MANU_SORT_PAGE"

    .line 237
    .line 238
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    .line 240
    .line 241
    const-string p1, "see_view"

    .line 242
    .line 243
    invoke-static {v5, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    .line 245
    .line 246
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o0o〇〇〇8o()V

    .line 247
    .line 248
    .line 249
    goto/16 :goto_0

    .line 250
    .line 251
    :cond_b
    const-wide/16 v0, 0xb

    .line 252
    .line 253
    cmp-long v6, p1, v0

    .line 254
    .line 255
    if-nez v6, :cond_c

    .line 256
    .line 257
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 258
    .line 259
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o08oOO()V

    .line 260
    .line 261
    .line 262
    goto/16 :goto_0

    .line 263
    .line 264
    :cond_c
    const-wide/16 v0, 0x1c

    .line 265
    .line 266
    cmp-long v6, p1, v0

    .line 267
    .line 268
    if-nez v6, :cond_d

    .line 269
    .line 270
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 271
    .line 272
    const-string p2, "User Operation: TOP_MENU_SHARE_DIR"

    .line 273
    .line 274
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    .line 276
    .line 277
    invoke-static {}, Lcom/intsig/camscanner/sharedir/recommed/ShareDirLogAgentHelper;->〇o〇()V

    .line 278
    .line 279
    .line 280
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 281
    .line 282
    const/4 p2, 0x0

    .line 283
    sget-object v0, Lcom/intsig/camscanner/sharedir/recommed/RecommendShareDirFromType$RecommendFromMore;->〇o00〇〇Oo:Lcom/intsig/camscanner/sharedir/recommed/RecommendShareDirFromType$RecommendFromMore;

    .line 284
    .line 285
    invoke-virtual {p1, p2, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO8〇(Ljava/lang/String;Lcom/intsig/camscanner/sharedir/recommed/RecommendShareDirFromType;)V

    .line 286
    .line 287
    .line 288
    goto/16 :goto_0

    .line 289
    .line 290
    :cond_d
    const-wide/16 v0, 0xc

    .line 291
    .line 292
    cmp-long v6, p1, v0

    .line 293
    .line 294
    if-nez v6, :cond_e

    .line 295
    .line 296
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 297
    .line 298
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8O0O808〇()V

    .line 299
    .line 300
    .line 301
    goto/16 :goto_0

    .line 302
    .line 303
    :cond_e
    const-wide/16 v0, 0xe

    .line 304
    .line 305
    cmp-long v6, p1, v0

    .line 306
    .line 307
    if-nez v6, :cond_f

    .line 308
    .line 309
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 310
    .line 311
    const-string p2, "upload all e evidence"

    .line 312
    .line 313
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    .line 315
    .line 316
    const-string p1, "digital_evidence"

    .line 317
    .line 318
    invoke-static {v5, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    .line 320
    .line 321
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 322
    .line 323
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇OOo()V

    .line 324
    .line 325
    .line 326
    goto/16 :goto_0

    .line 327
    .line 328
    :cond_f
    const-wide/16 v0, 0x10

    .line 329
    .line 330
    cmp-long v6, p1, v0

    .line 331
    .line 332
    if-nez v6, :cond_10

    .line 333
    .line 334
    sget-object p1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MORE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 335
    .line 336
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->OoOO〇(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 337
    .line 338
    .line 339
    goto/16 :goto_0

    .line 340
    .line 341
    :cond_10
    const-wide/16 v0, 0x11

    .line 342
    .line 343
    cmp-long v6, p1, v0

    .line 344
    .line 345
    if-nez v6, :cond_11

    .line 346
    .line 347
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 348
    .line 349
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o08〇〇0O()V

    .line 350
    .line 351
    .line 352
    goto/16 :goto_0

    .line 353
    .line 354
    :cond_11
    const-wide/16 v0, 0x12

    .line 355
    .line 356
    const-string v6, "cs_list"

    .line 357
    .line 358
    cmp-long v7, p1, v0

    .line 359
    .line 360
    if-nez v7, :cond_12

    .line 361
    .line 362
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 363
    .line 364
    const-string p2, "To Word"

    .line 365
    .line 366
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    .line 368
    .line 369
    sget-object p1, Lcom/intsig/camscanner/ipo/IPOCheck;->〇080:Lcom/intsig/camscanner/ipo/IPOCheck;

    .line 370
    .line 371
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 372
    .line 373
    new-instance p2, Lcom/intsig/camscanner/pagelist/PageListFragment$9;

    .line 374
    .line 375
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/pagelist/PageListFragment$9;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 376
    .line 377
    .line 378
    const-string v0, "to_word"

    .line 379
    .line 380
    invoke-static {p1, p2, v4, v0, v6}, Lcom/intsig/camscanner/ipo/IPOCheck;->〇〇888(Landroid/content/Context;Lcom/intsig/camscanner/ipo/IPOCheckCallback;ZLjava/lang/String;Ljava/lang/String;)V

    .line 381
    .line 382
    .line 383
    goto/16 :goto_0

    .line 384
    .line 385
    :cond_12
    const-wide/16 v0, 0x13

    .line 386
    .line 387
    cmp-long v7, p1, v0

    .line 388
    .line 389
    if-nez v7, :cond_13

    .line 390
    .line 391
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 392
    .line 393
    invoke-virtual {p1, v4}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0ooOOo(Z)V

    .line 394
    .line 395
    .line 396
    const-string p1, "from_part"

    .line 397
    .line 398
    const-string p2, "cs_list_multiple_choice"

    .line 399
    .line 400
    const-string v0, "batch_process_image"

    .line 401
    .line 402
    invoke-static {v5, v0, p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    .line 404
    .line 405
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 406
    .line 407
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O00〇o00()Ljava/util/ArrayList;

    .line 408
    .line 409
    .line 410
    move-result-object p2

    .line 411
    invoke-virtual {p1, p2, v4}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o88O8(Ljava/util/ArrayList;Z)V

    .line 412
    .line 413
    .line 414
    goto/16 :goto_0

    .line 415
    .line 416
    :cond_13
    const-wide/16 v0, 0x1d

    .line 417
    .line 418
    cmp-long v4, p1, v0

    .line 419
    .line 420
    if-nez v4, :cond_14

    .line 421
    .line 422
    sget-object p1, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->CsMoreOpenPdf:Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;

    .line 423
    .line 424
    invoke-virtual {p1}, Lcom/intsig/camscanner/pdf/pdfriver/PdfEntryRiver;->getCsPdfTrack()Ljava/lang/String;

    .line 425
    .line 426
    .line 427
    move-result-object p1

    .line 428
    const-string p2, "CSMore"

    .line 429
    .line 430
    const-string v0, "open_pdf_app"

    .line 431
    .line 432
    const-string v1, "refer_source"

    .line 433
    .line 434
    invoke-static {p2, v0, v1, p1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    .line 436
    .line 437
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇8O〇()V

    .line 438
    .line 439
    .line 440
    goto/16 :goto_0

    .line 441
    .line 442
    :cond_14
    const-wide/16 v0, 0x14

    .line 443
    .line 444
    cmp-long v4, p1, v0

    .line 445
    .line 446
    if-nez v4, :cond_15

    .line 447
    .line 448
    sget-object v0, Lcom/intsig/camscanner/ipo/IPOCheck;->〇080:Lcom/intsig/camscanner/ipo/IPOCheck;

    .line 449
    .line 450
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 451
    .line 452
    new-instance v1, Lo0Oo/o〇0OOo〇0;

    .line 453
    .line 454
    invoke-direct {v1, p0, p1, p2}, Lo0Oo/o〇0OOo〇0;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;J)V

    .line 455
    .line 456
    .line 457
    invoke-static {v0, v1}, Lcom/intsig/camscanner/ipo/IPOCheck;->〇80〇808〇O(Landroidx/fragment/app/FragmentActivity;Ljava/lang/Runnable;)V

    .line 458
    .line 459
    .line 460
    goto :goto_0

    .line 461
    :cond_15
    const-wide/16 v0, 0x15

    .line 462
    .line 463
    cmp-long v4, p1, v0

    .line 464
    .line 465
    if-nez v4, :cond_16

    .line 466
    .line 467
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o880:Landroid/os/Handler;

    .line 468
    .line 469
    new-instance p2, Lo0Oo/〇08O8o〇0;

    .line 470
    .line 471
    invoke-direct {p2, p0}, Lo0Oo/〇08O8o〇0;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 472
    .line 473
    .line 474
    const-wide/16 v0, 0xc8

    .line 475
    .line 476
    invoke-virtual {p1, p2, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 477
    .line 478
    .line 479
    goto :goto_0

    .line 480
    :cond_16
    const-wide/16 v0, 0x16

    .line 481
    .line 482
    cmp-long v4, p1, v0

    .line 483
    .line 484
    if-nez v4, :cond_17

    .line 485
    .line 486
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 487
    .line 488
    const-string p2, "User Operation: TOP_MENU_CHANGE_PAPER_PROPERTY"

    .line 489
    .line 490
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    .line 492
    .line 493
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o088〇〇()V

    .line 494
    .line 495
    .line 496
    goto :goto_0

    .line 497
    :cond_17
    const-wide/16 v0, 0x17

    .line 498
    .line 499
    cmp-long v4, p1, v0

    .line 500
    .line 501
    if-nez v4, :cond_18

    .line 502
    .line 503
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 504
    .line 505
    const-string p2, "User Operation: TOP_MENU_TOPIC_SET"

    .line 506
    .line 507
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    .line 509
    .line 510
    const-string p1, "wrong_question_set"

    .line 511
    .line 512
    invoke-static {v5, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 513
    .line 514
    .line 515
    sget-object p1, Lcom/intsig/camscanner/paper/PaperUtil;->〇080:Lcom/intsig/camscanner/paper/PaperUtil;

    .line 516
    .line 517
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 518
    .line 519
    invoke-virtual {p1, p2, v6}, Lcom/intsig/camscanner/paper/PaperUtil;->〇O8o08O(Landroid/app/Activity;Ljava/lang/String;)V

    .line 520
    .line 521
    .line 522
    goto :goto_0

    .line 523
    :cond_18
    const-wide/16 v0, 0x18

    .line 524
    .line 525
    cmp-long v4, p1, v0

    .line 526
    .line 527
    if-nez v4, :cond_19

    .line 528
    .line 529
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 530
    .line 531
    const-string p2, "TOP_MENU_PRINT"

    .line 532
    .line 533
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    .line 535
    .line 536
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 537
    .line 538
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O88〇〇o0O()V

    .line 539
    .line 540
    .line 541
    invoke-static {v3}, Lcom/intsig/camscanner/util/PreferenceHelper;->OOOO00(Z)V

    .line 542
    .line 543
    .line 544
    const-string p1, "print"

    .line 545
    .line 546
    invoke-static {v2, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    .line 548
    .line 549
    goto :goto_0

    .line 550
    :cond_19
    const-wide/16 v0, 0x19

    .line 551
    .line 552
    cmp-long v2, p1, v0

    .line 553
    .line 554
    if-nez v2, :cond_1a

    .line 555
    .line 556
    const-string p1, "ocr"

    .line 557
    .line 558
    invoke-static {v5, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 559
    .line 560
    .line 561
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 562
    .line 563
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇080〇o0()V

    .line 564
    .line 565
    .line 566
    :cond_1a
    :goto_0
    return-void
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
.end method

.method public oO00〇o(J)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public oO0〇〇o8〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇080OO8〇0:Lcom/intsig/adapter/BaseRecyclerViewTouchHelper;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return v0

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/adapter/BaseRecyclerViewTouchHelper;->〇o00〇〇Oo()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    return v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oO80O0(Landroid/view/View;ILcom/intsig/camscanner/pagelist/model/PageTypeItem;I)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/pagelist/model/PageTypeItem;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    instance-of p2, p3, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 2
    .line 3
    if-nez p2, :cond_0

    .line 4
    .line 5
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 6
    .line 7
    new-instance p2, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string p3, "absRecyclerViewItem is not PageListViewItem pos="

    .line 13
    .line 14
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object p2

    .line 24
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    return-void

    .line 28
    :cond_0
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oO〇8O8oOo:Lcom/intsig/utils/ClickLimit;

    .line 29
    .line 30
    sget-wide v0, Lcom/intsig/utils/ClickLimit;->〇o〇:J

    .line 31
    .line 32
    invoke-virtual {p2, p1, v0, v1}, Lcom/intsig/utils/ClickLimit;->〇o00〇〇Oo(Landroid/view/View;J)Z

    .line 33
    .line 34
    .line 35
    move-result p2

    .line 36
    if-nez p2, :cond_1

    .line 37
    .line 38
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 39
    .line 40
    const-string p2, "click too fast"

    .line 41
    .line 42
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    return-void

    .line 46
    :cond_1
    sget-object p2, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 47
    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    .line 49
    .line 50
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 51
    .line 52
    .line 53
    const-string v1, "pos="

    .line 54
    .line 55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    invoke-static {p2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 69
    .line 70
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0o8〇O()Z

    .line 71
    .line 72
    .line 73
    move-result v0

    .line 74
    if-eqz v0, :cond_2

    .line 75
    .line 76
    const-string p1, "onItemClick >>> ManualSort. nothing to do."

    .line 77
    .line 78
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    goto :goto_0

    .line 82
    :cond_2
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 83
    .line 84
    invoke-virtual {p2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇()Z

    .line 85
    .line 86
    .line 87
    move-result p2

    .line 88
    if-eqz p2, :cond_3

    .line 89
    .line 90
    invoke-direct {p0, p4}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇Ooo0o(I)V

    .line 91
    .line 92
    .line 93
    goto :goto_0

    .line 94
    :cond_3
    check-cast p3, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 95
    .line 96
    invoke-virtual {p3}, Lcom/intsig/camscanner/pagelist/model/PageImageItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/pagelist/model/PageItem;

    .line 97
    .line 98
    .line 99
    move-result-object p2

    .line 100
    iget-wide p2, p2, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇080:J

    .line 101
    .line 102
    invoke-direct {p0, p4, p2, p3, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇Oo〇0O〇8(IJLandroid/view/View;)V

    .line 103
    .line 104
    .line 105
    :goto_0
    return-void
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public oO80OOO〇(Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;->〇〇0o(Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public oO8o()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇〇O()Lcom/intsig/camscanner/pagelist/bubble/PageListBubbleControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/bubble/PageListBubbleControl;->OO0o〇〇〇〇0()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oOOO0(JLjava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oo(JLjava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public oO〇8O8oOo()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/purchase/dialog/CloudOverrunDialog;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/purchase/dialog/CloudOverrunDialog;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    const-string v2, "CloudOverrunDialog"

    .line 11
    .line 12
    invoke-virtual {v0, v1, v2}, Lcom/intsig/app/BaseDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oO〇oo()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 23

    move-object/from16 v1, p0

    move/from16 v2, p1

    move/from16 v3, p2

    move-object/from16 v12, p3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onActivityResult requestCode="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, " resultCode="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, " data="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v4, 0x3f8

    const/4 v5, 0x0

    if-ne v2, v4, :cond_0

    .line 2
    iget-object v0, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8oOOo:Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;

    invoke-virtual {v0, v5}, Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;->〇o00〇〇Oo(Ljava/lang/String;)V

    return-void

    :cond_0
    const/16 v4, 0x3f2

    if-ne v2, v4, :cond_1

    .line 3
    iget-object v0, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇o〇Oo88()V

    return-void

    :cond_1
    const/16 v4, 0x63

    if-ne v2, v4, :cond_2

    .line 4
    iget-object v0, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v0}, Lcom/intsig/camscanner/app/DialogUtils;->〇oOO8O8(Landroid/content/Context;)V

    return-void

    :cond_2
    const/16 v4, 0x3f4

    if-ne v2, v4, :cond_4

    .line 5
    iget-object v0, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->oooO888:Landroid/widget/EditText;

    if-eqz v0, :cond_3

    .line 6
    iget-object v2, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v2, v0}, Lcom/intsig/utils/SoftKeyboardUtils;->O8(Landroid/content/Context;Landroid/widget/EditText;)V

    :cond_3
    return-void

    :cond_4
    const/16 v4, 0x3f7

    const/4 v6, -0x1

    const/16 v7, 0x400

    const/16 v8, 0x3ec

    const/16 v9, 0x3ea

    const/16 v10, 0x3e9

    const/4 v11, 0x0

    if-ne v2, v4, :cond_8

    if-eq v3, v10, :cond_6

    if-eq v3, v9, :cond_6

    if-eq v3, v8, :cond_6

    if-eq v3, v7, :cond_6

    const/16 v0, 0x7e1

    if-eq v3, v0, :cond_5

    goto :goto_0

    .line 7
    :cond_5
    iput-boolean v11, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO〇OOo:Z

    goto :goto_0

    :cond_6
    if-eqz v12, :cond_7

    const-string v0, "with_data"

    .line 8
    invoke-virtual {v12, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 9
    iget-object v2, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v12, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O08〇(Ljava/lang/String;)V

    .line 10
    :cond_7
    invoke-virtual {v1, v3, v6, v12}, Lcom/intsig/camscanner/pagelist/PageListFragment;->onActivityResult(IILandroid/content/Intent;)V

    :goto_0
    return-void

    :cond_8
    const/16 v4, 0x3fe

    if-ne v2, v4, :cond_9

    .line 11
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O00O()V

    return-void

    :cond_9
    const/16 v4, 0x3fd

    if-ne v2, v4, :cond_a

    .line 12
    invoke-static {}, Lcom/intsig/camscanner/pagedetail/PageDetailReeditUtil;->oO80()V

    :cond_a
    const/4 v13, 0x1

    if-eq v3, v6, :cond_c

    if-eq v3, v13, :cond_b

    goto :goto_1

    .line 13
    :cond_b
    iget-object v0, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0, v13}, Landroid/app/Activity;->setResult(I)V

    .line 14
    iget-object v0, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :goto_1
    return-void

    :cond_c
    const-string v6, ",try to record recent history, add new page Modified"

    if-ne v2, v10, :cond_1b

    if-eqz v12, :cond_19

    .line 15
    iget-object v4, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    iget-object v7, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v7}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    move-result-wide v7

    invoke-static {v4, v7, v8}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇o00〇〇Oo(Landroid/content/Context;J)Z

    move-result v4

    if-eqz v4, :cond_19

    .line 16
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const-string v7, "constant_add_spec_action"

    .line 17
    invoke-virtual {v12, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v7}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0o(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "intent_extra_check_show_ad"

    .line 18
    invoke-virtual {v12, v8, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    iput-boolean v8, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->ooo0〇〇O:Z

    const-string v8, "com.intsig.camscanner.NEW_PAGE_MULTIPLE"

    .line 19
    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_11

    const-string v4, "doc_title"

    .line 20
    invoke-virtual {v12, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 21
    iget-boolean v8, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO〇OOo:Z

    if-eqz v8, :cond_e

    iget-boolean v8, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOO〇〇:Z

    if-eqz v8, :cond_d

    goto :goto_2

    :cond_d
    move-object v5, v7

    .line 22
    :cond_e
    :goto_2
    iput-object v5, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇800OO〇0O:Ljava/lang/String;

    .line 23
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_10

    iget-object v5, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v5}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0〇oo()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_f

    goto :goto_3

    .line 24
    :cond_f
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "newTitle="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v7, " mTitle="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v7}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0〇oo()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    iget-object v5, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v5}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    move-result-wide v7

    iget-object v9, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v9}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇0〇0o8()Ljava/lang/String;

    move-result-object v9

    iget-object v10, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v7, v8, v4, v9, v10}, Lcom/intsig/camscanner/util/Util;->o8O0(JLjava/lang/String;Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o88(Ljava/lang/String;)V

    .line 26
    iget-object v5, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v5, v4}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇(Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_10
    :goto_3
    const-string v4, "NOT NEED TO RENAME."

    .line 27
    invoke-static {v0, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_11
    const-string v8, "com.intsig.camscanner.NEW_PAGE_CERTIFICATE"

    .line 28
    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_12

    .line 29
    iget-object v4, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v4, v13}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇O80〇0o(Z)V

    const-string v4, "extra_id_card_flow"

    .line 30
    invoke-virtual {v12, v4, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    iput-boolean v4, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOO〇〇:Z

    const-string v4, "spec_action_show_scan_done"

    .line 31
    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_18

    .line 32
    iput-object v5, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇800OO〇0O:Ljava/lang/String;

    .line 33
    iput-boolean v11, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO〇OOo:Z

    goto/16 :goto_6

    :cond_12
    const-string v8, "com.intsig.camscanner.NEW_PAGE_BOOK_SPLITTER"

    .line 34
    invoke-virtual {v8, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_17

    const-string v8, "com.intsig.camscanner.NEW_PAGE_CERTIFICATE_PHOTO"

    .line 35
    invoke-static {v8, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_17

    const-string v8, "com.intsig.camscanner.NEW_PAGE_EXCEL"

    .line 36
    invoke-static {v8, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_17

    const-string v8, "com.intsig.camscanner.NEW_BATOCR_PAGE"

    .line 37
    invoke-static {v8, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_17

    const-string v8, "com.intsig.camscanner.NEW_PAGE_IMAGE_RESTORE"

    .line 38
    invoke-static {v8, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_17

    const-string v8, "com.intsig.camscanner.NEW_PAGE_IMAGE_TRANSLATE"

    .line 39
    invoke-static {v8, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_13

    goto/16 :goto_5

    .line 40
    :cond_13
    iget-object v4, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v4}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o()Landroid/net/Uri;

    move-result-object v4

    .line 41
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "data "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v9, " docUri="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v8}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v4, :cond_18

    const-string v8, "image_sync_id"

    .line 42
    invoke-virtual {v12, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const-string v8, "issaveready"

    .line 43
    invoke-virtual {v12, v8, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    .line 44
    iget-object v9, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇800OO〇0O:Ljava/lang/String;

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_14

    const-string v9, "spec_action_show_image_page_view"

    iget-object v10, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇800OO〇0O:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_14

    iget-boolean v9, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO〇OOo:Z

    if-eqz v9, :cond_16

    :cond_14
    iget-boolean v9, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOO〇〇:Z

    if-eqz v9, :cond_15

    goto :goto_4

    :cond_15
    move-object v5, v7

    .line 45
    :cond_16
    :goto_4
    iput-object v5, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇800OO〇0O:Ljava/lang/String;

    .line 46
    iget-object v5, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v5}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v7, "Constant_doc_finish_page_type"

    invoke-virtual {v5, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->O8o〇O0:Ljava/lang/String;

    .line 47
    iget-object v5, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v5, v12, v4, v15, v8}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇o(Landroid/content/Intent;Landroid/net/Uri;Ljava/lang/String;Z)V

    const-string v4, "extra_ocr_result"

    .line 48
    invoke-virtual {v12, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    const-string v4, "extra_ocr_user_result"

    .line 49
    invoke-virtual {v12, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    const-string v4, "extra_ocr_file"

    .line 50
    invoke-virtual {v12, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    const-string v4, "extra_ocr_paragraph"

    .line 51
    invoke-virtual {v12, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    const-string v4, "extra_ocr_time"

    const-wide/16 v7, 0x0

    .line 52
    invoke-virtual {v12, v4, v7, v8}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v20

    const-string v4, "extra_ocr_mode"

    .line 53
    invoke-virtual {v12, v4, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v22

    .line 54
    iget-object v14, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static/range {v14 .. v22}, Lcom/intsig/camscanner/app/DBUtil;->o88O8(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JI)V

    .line 55
    iget-object v4, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v4}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o8O〇O()I

    move-result v5

    invoke-virtual {v4, v12, v5}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0〇0(Landroid/content/Intent;I)V

    goto :goto_6

    :cond_17
    :goto_5
    const-string v4, "NEW_PAGE_BOOK_SPLITTER | NEW_PAGE_CERTIFICATE_PHOTO | NEW_PAGE_EXCEL | NEW_BATOCR_PAGE, nothing to do."

    .line 56
    invoke-static {v0, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    :cond_18
    :goto_6
    invoke-static {}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OoO8()Lcom/intsig/camscanner/background_batch/client/BackScanClient;

    move-result-object v4

    iget-object v5, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v5}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    move-result-wide v7

    invoke-virtual {v4, v7, v8}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->O〇O〇oO(J)Lcom/intsig/camscanner/background_batch/client/BackScanClient;

    .line 58
    :cond_19
    iget-object v4, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    sget-object v5, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    iget-object v7, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v7}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    move-result-wide v7

    invoke-static {v5, v7, v8}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/intsig/camscanner/db/dao/DocumentDao;->oo88o8O(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v15

    .line 59
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1a

    .line 60
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "fragmentActivity, syncDocId="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    sget-object v13, Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;->〇080:Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v14

    const/16 v16, 0x3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v17

    invoke-virtual/range {v13 .. v18}, Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;->OoO8(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;IJ)V

    .line 62
    :cond_1a
    invoke-static {}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->〇o〇()Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;

    move-result-object v0

    sget-object v4, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;->NOVICE_SCAN:Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;

    invoke-virtual {v0, v4}, Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper;->o〇0(Lcom/intsig/camscanner/operategrowth/NoviceTaskHelper$NoviceTaskType;)V

    goto/16 :goto_a

    :cond_1b
    if-ne v2, v8, :cond_1f

    .line 63
    iget-object v4, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    iget-object v5, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v5}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇o00〇〇Oo(Landroid/content/Context;J)Z

    move-result v4

    if-eqz v4, :cond_31

    .line 64
    iget-object v4, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v4}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇OO8Oo0〇()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    const v5, 0x7f130085

    if-nez v4, :cond_1e

    .line 65
    new-instance v4, Ljava/io/File;

    iget-object v6, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v6}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇OO8Oo0〇()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 66
    invoke-virtual {v4}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_1d

    .line 67
    new-instance v6, Ljava/io/File;

    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->〇〇808〇()Ljava/lang/String;

    move-result-object v7

    const-string v8, ".jpg"

    invoke-static {v7, v8}, Lcom/intsig/camscanner/util/SDStorageManager;->〇O8o08O(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 68
    :try_start_0
    invoke-static {v4, v6}, Lcom/intsig/utils/FileUtil;->Oo08(Ljava/io/File;Ljava/io/File;)V

    .line 69
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1c

    .line 70
    invoke-static {v6}, Lcom/intsig/utils/FileUtil;->〇〇8O0〇8(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇O〇88O8O(Landroid/net/Uri;)V

    goto/16 :goto_a

    :cond_1c
    const-string v4, "copyFile fail"

    .line 71
    invoke-static {v0, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_a

    :catch_0
    move-exception v0

    .line 72
    iget-object v4, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v4, v5}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 73
    sget-object v4, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_a

    .line 74
    :cond_1d
    iget-object v4, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v4, v5}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    const-string v4, "tempFile is not exists"

    .line 75
    invoke-static {v0, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_a

    :cond_1e
    const-string v4, "mTmpPhotoFile == null"

    .line 76
    invoke-static {v0, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    iget-object v0, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v0, v5}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    goto/16 :goto_a

    :cond_1f
    const/16 v5, 0x3ed

    if-eq v2, v5, :cond_30

    const/16 v5, 0x3ee

    if-ne v2, v5, :cond_20

    goto/16 :goto_9

    :cond_20
    const/16 v5, 0x67

    if-ne v2, v5, :cond_21

    .line 78
    invoke-direct {v1, v12}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇Oo(Landroid/content/Intent;)V

    goto/16 :goto_a

    :cond_21
    if-ne v2, v9, :cond_24

    if-eqz v12, :cond_31

    .line 79
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_22

    .line 80
    iget-object v0, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    iget-object v5, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v5}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    move-result-wide v5

    invoke-static {v0, v5, v6}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇o00〇〇Oo(Landroid/content/Context;J)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 81
    invoke-direct {v1, v4}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇O〇88O8O(Landroid/net/Uri;)V

    goto/16 :goto_a

    .line 82
    :cond_22
    invoke-static/range {p3 .. p3}, Lcom/intsig/camscanner/app/IntentUtil;->〇O8o08O(Landroid/content/Intent;)Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_23

    .line 83
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_23

    .line 84
    invoke-direct {v1, v4}, Lcom/intsig/camscanner/pagelist/PageListFragment;->OOOo〇(Ljava/util/ArrayList;)V

    goto/16 :goto_a

    :cond_23
    const-string v4, "uris are null"

    .line 85
    invoke-static {v0, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_a

    :cond_24
    const/16 v5, 0x3f0

    if-eq v2, v5, :cond_2f

    const/16 v5, 0x3ff

    if-ne v2, v5, :cond_25

    goto/16 :goto_8

    :cond_25
    const/16 v5, 0x38d

    if-ne v2, v5, :cond_28

    if-eqz v12, :cond_27

    .line 86
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    .line 87
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "REQUEST_COMPOSITE docUri:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v4, :cond_26

    .line 88
    :try_start_1
    iget-object v0, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v0, v4}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oOoO8OO〇(Landroid/net/Uri;)V

    .line 89
    iget-object v0, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-static {v4}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇O8OO(J)V

    .line 90
    iget-object v0, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v0, v11}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0O0〇(I)V

    .line 91
    iget-object v0, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oO8O0〇〇O()V

    .line 92
    invoke-static {v13}, Lcom/intsig/camscanner/util/PreferenceHelper;->OOO8Oo(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_a

    :catch_1
    move-exception v0

    .line 93
    sget-object v4, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_a

    .line 94
    :cond_26
    iget-object v0, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o8O〇O()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O0oO008(I)V

    .line 95
    iget-object v0, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->ooO:Landroid/view/View;

    const v4, 0x7f0a0213

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇o〇0O(Landroid/view/View;)V

    goto/16 :goto_a

    .line 96
    :cond_27
    iget-object v0, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_a

    :cond_28
    if-ne v2, v4, :cond_29

    if-eqz v12, :cond_31

    .line 97
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    .line 98
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "REQUEST_CODE_SECURITY docUri:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v4, :cond_31

    .line 99
    iget-object v0, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v0, v4}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oOoO8OO〇(Landroid/net/Uri;)V

    .line 100
    :try_start_2
    iget-object v0, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-static {v4}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇O8OO(J)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_7

    .line 101
    :catch_2
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    const-string v4, "docId uri parse exception"

    invoke-static {v0, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    :goto_7
    iget-object v0, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v0, v11}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0O0〇(I)V

    .line 103
    iget-object v0, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oO8O0〇〇O()V

    goto/16 :goto_a

    :cond_29
    if-ne v2, v7, :cond_2a

    .line 104
    iget-object v0, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto/16 :goto_a

    :cond_2a
    const/16 v5, 0x401

    if-ne v5, v2, :cond_2c

    const-string v4, "onActivityResult GO_TO_PAPER_PROPERTY_SELECT"

    .line 105
    invoke-static {v0, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v12, :cond_31

    const-string v4, "extra_key_paper_property_result_from_full"

    .line 106
    invoke-virtual {v12, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 107
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_31

    .line 108
    iget-object v5, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    sget-object v7, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    iget-object v8, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v8}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    move-result-wide v8

    invoke-static {v7, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v7

    invoke-static {v5, v7}, Lcom/intsig/camscanner/db/dao/DocumentDao;->oo88o8O(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v15

    .line 109
    invoke-static {v15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2b

    .line 110
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "PaperPropertySelectFullScreenActivity syncDocId="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    sget-object v13, Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;->〇080:Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;

    invoke-virtual/range {p0 .. p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v14

    const/16 v16, 0x3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v17

    invoke-virtual/range {v13 .. v18}, Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;->OoO8(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;IJ)V

    .line 112
    :cond_2b
    invoke-direct {v1, v4}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇8〇Oo0(Ljava/lang/String;)V

    goto :goto_a

    :cond_2c
    const/16 v0, 0x402

    if-eq v2, v0, :cond_2d

    if-ne v2, v4, :cond_31

    :cond_2d
    if-eqz v12, :cond_31

    .line 113
    invoke-direct {v1, v11}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oO0o(I)Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    move-result-object v0

    .line 114
    instance-of v4, v0, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    if-eqz v4, :cond_2e

    .line 115
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageImageItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/pagelist/model/PageItem;

    move-result-object v0

    if-eqz v0, :cond_2e

    .line 116
    new-instance v7, Lcom/intsig/camscanner/loadimage/PageImage;

    iget-wide v4, v0, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇080:J

    iget-object v0, v0, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇o00〇〇Oo:Ljava/lang/String;

    invoke-direct {v7, v4, v5, v0}, Lcom/intsig/camscanner/loadimage/PageImage;-><init>(JLjava/lang/String;)V

    .line 117
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇〇08〇0oo0()Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    move-result-wide v5

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v4, p3

    .line 118
    invoke-static/range {v4 .. v11}, Lcom/intsig/camscanner/pagedetail/PageDetailReeditUtil;->〇o〇(Landroid/content/Intent;JLcom/intsig/camscanner/loadimage/PageImage;ZLcom/intsig/camscanner/tsapp/SyncCallbackListener;Lcom/intsig/callback/Callback0;Lcom/intsig/camscanner/pagedetail/PageDetailReeditUtil$OnUpdateImageCallback;)V

    :cond_2e
    const/4 v11, 0x1

    goto :goto_a

    .line 119
    :cond_2f
    :goto_8
    iget-object v4, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    iget-object v5, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v5}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇o00〇〇Oo(Landroid/content/Context;J)Z

    move-result v4

    if-eqz v4, :cond_31

    const-string v4, "onActivityResult REQUEST_ADD_PAGES_FROM_GALLERY"

    .line 120
    invoke-static {v0, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    iget-object v5, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    iget-object v0, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    move-result-wide v6

    const/4 v8, 0x3

    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-static/range {v5 .. v10}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o08o〇0(Landroid/content/Context;JIZZ)V

    goto :goto_a

    :cond_30
    :goto_9
    if-eqz v12, :cond_31

    .line 122
    iget-object v0, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    const-string v4, "doc_id"

    const-wide/16 v5, -0x1

    invoke-virtual {v12, v4, v5, v6}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇O(J)V

    .line 123
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O00O()V

    :cond_31
    :goto_a
    if-eqz v11, :cond_32

    .line 124
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    const-string v4, "onActivityResult needRecordForRecent=true, add recent doc;"

    invoke-static {v0, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    iget-object v0, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    sget-object v4, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    iget-object v5, v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 126
    invoke-virtual {v5}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    .line 127
    invoke-static {v0, v4}, Lcom/intsig/camscanner/db/dao/DocumentDao;->oo88o8O(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v7

    .line 128
    sget-object v5, Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;->〇080:Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;

    .line 129
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->getCurrentActivity()Landroidx/fragment/app/FragmentActivity;

    move-result-object v6

    const/4 v8, 0x3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    .line 130
    invoke-virtual/range {v5 .. v10}, Lcom/intsig/camscanner/mainmenu/mainpage/MainRecentDocAdapter;->OoO8(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;IJ)V

    .line 131
    :cond_32
    invoke-super/range {p0 .. p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->onAttach(Landroid/app/Activity;)V

    .line 2
    .line 3
    .line 4
    check-cast p1, Landroidx/appcompat/app/AppCompatActivity;

    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 4
    .line 5
    const-string v0, "onClick v == null"

    .line 6
    .line 7
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    const v1, 0x7f0a05db

    .line 16
    .line 17
    .line 18
    const/16 v2, 0x8

    .line 19
    .line 20
    const-string v3, "CSList"

    .line 21
    .line 22
    if-ne v0, v1, :cond_3

    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oO〇8O8oOo:Lcom/intsig/utils/ClickLimit;

    .line 25
    .line 26
    invoke-virtual {v0, p1}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 27
    .line 28
    .line 29
    move-result p1

    .line 30
    if-nez p1, :cond_1

    .line 31
    .line 32
    return-void

    .line 33
    :cond_1
    const-string p1, "txt_identify"

    .line 34
    .line 35
    invoke-static {v3, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOo0:Lcom/intsig/comm/widget/CustomTextView;

    .line 39
    .line 40
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    .line 41
    .line 42
    .line 43
    move-result p1

    .line 44
    if-nez p1, :cond_2

    .line 45
    .line 46
    invoke-static {}, Lcom/intsig/camscanner/mode_ocr/PreferenceOcrHelper;->O8()V

    .line 47
    .line 48
    .line 49
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOo0:Lcom/intsig/comm/widget/CustomTextView;

    .line 50
    .line 51
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 52
    .line 53
    .line 54
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 55
    .line 56
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OOo88OOo()V

    .line 57
    .line 58
    .line 59
    return-void

    .line 60
    :cond_3
    const v1, 0x7f0a05d5

    .line 61
    .line 62
    .line 63
    const-string v4, "click too fast"

    .line 64
    .line 65
    if-ne v0, v1, :cond_5

    .line 66
    .line 67
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 68
    .line 69
    const-string v1, "User Operation: camera"

    .line 70
    .line 71
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oO〇8O8oOo:Lcom/intsig/utils/ClickLimit;

    .line 75
    .line 76
    invoke-virtual {v1, p1}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 77
    .line 78
    .line 79
    move-result p1

    .line 80
    if-nez p1, :cond_4

    .line 81
    .line 82
    invoke-static {v0, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    return-void

    .line 86
    :cond_4
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 87
    .line 88
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OOo8o〇O()V

    .line 89
    .line 90
    .line 91
    return-void

    .line 92
    :cond_5
    const v1, 0x7f0a09f0

    .line 93
    .line 94
    .line 95
    if-ne v0, v1, :cond_7

    .line 96
    .line 97
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 98
    .line 99
    const-string v1, "User Operation: print doc"

    .line 100
    .line 101
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    .line 103
    .line 104
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oO〇8O8oOo:Lcom/intsig/utils/ClickLimit;

    .line 105
    .line 106
    invoke-virtual {v1, p1}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 107
    .line 108
    .line 109
    move-result p1

    .line 110
    if-nez p1, :cond_6

    .line 111
    .line 112
    const-string p1, "click print too fast"

    .line 113
    .line 114
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    .line 116
    .line 117
    return-void

    .line 118
    :cond_6
    const-string p1, "print"

    .line 119
    .line 120
    invoke-static {v3, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    .line 122
    .line 123
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 124
    .line 125
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O88〇〇o0O()V

    .line 126
    .line 127
    .line 128
    return-void

    .line 129
    :cond_7
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oO〇8O8oOo:Lcom/intsig/utils/ClickLimit;

    .line 130
    .line 131
    const/16 v5, 0x1f4

    .line 132
    .line 133
    int-to-long v5, v5

    .line 134
    invoke-virtual {v1, p1, v5, v6}, Lcom/intsig/utils/ClickLimit;->〇o00〇〇Oo(Landroid/view/View;J)Z

    .line 135
    .line 136
    .line 137
    move-result v1

    .line 138
    if-nez v1, :cond_8

    .line 139
    .line 140
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 141
    .line 142
    invoke-static {p1, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    .line 144
    .line 145
    return-void

    .line 146
    :cond_8
    const v1, 0x7f0a07bd

    .line 147
    .line 148
    .line 149
    const/4 v4, 0x1

    .line 150
    if-ne v0, v1, :cond_9

    .line 151
    .line 152
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 153
    .line 154
    const-string v0, "User Operation: itb_bottom_pdf_kit_add"

    .line 155
    .line 156
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    .line 158
    .line 159
    iput-boolean v4, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0O〇O00O:Z

    .line 160
    .line 161
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 162
    .line 163
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OOo8o〇O()V

    .line 164
    .line 165
    .line 166
    goto/16 :goto_4

    .line 167
    .line 168
    :cond_9
    const v1, 0x7f0a0218

    .line 169
    .line 170
    .line 171
    if-ne v0, v1, :cond_a

    .line 172
    .line 173
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 174
    .line 175
    new-instance v0, Lo0Oo/o〇〇0〇;

    .line 176
    .line 177
    invoke-direct {v0, p0}, Lo0Oo/o〇〇0〇;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 178
    .line 179
    .line 180
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇(Lcom/intsig/callback/Callback0;)V

    .line 181
    .line 182
    .line 183
    goto/16 :goto_4

    .line 184
    .line 185
    :cond_a
    const v1, 0x7f0a0216

    .line 186
    .line 187
    .line 188
    if-ne v0, v1, :cond_b

    .line 189
    .line 190
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇88()V

    .line 191
    .line 192
    .line 193
    goto/16 :goto_4

    .line 194
    .line 195
    :cond_b
    const v1, 0x7f0a0213

    .line 196
    .line 197
    .line 198
    if-ne v0, v1, :cond_c

    .line 199
    .line 200
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 201
    .line 202
    const-string v1, "User Operation: menu btn"

    .line 203
    .line 204
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    .line 206
    .line 207
    const-string v0, "more"

    .line 208
    .line 209
    invoke-static {v3, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    .line 211
    .line 212
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0oO()V

    .line 213
    .line 214
    .line 215
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO8ooO8〇:Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;

    .line 216
    .line 217
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;->oO80(Landroid/view/View;)V

    .line 218
    .line 219
    .line 220
    goto/16 :goto_4

    .line 221
    .line 222
    :cond_c
    const v1, 0x7f0a0b93

    .line 223
    .line 224
    .line 225
    if-ne v0, v1, :cond_d

    .line 226
    .line 227
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 228
    .line 229
    const-string v0, "User Operation:  camcard banner"

    .line 230
    .line 231
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    .line 233
    .line 234
    const-string p1, "list_cc_click"

    .line 235
    .line 236
    invoke-static {v3, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    .line 238
    .line 239
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 240
    .line 241
    const v0, 0x7f13040d

    .line 242
    .line 243
    .line 244
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 245
    .line 246
    .line 247
    move-result-object v0

    .line 248
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 249
    .line 250
    invoke-static {v1}, Lcom/intsig/camscanner/web/UrlUtil;->〇o〇(Landroid/content/Context;)Ljava/lang/String;

    .line 251
    .line 252
    .line 253
    move-result-object v1

    .line 254
    invoke-static {p1, v0, v1}, Lcom/intsig/webview/util/WebUtil;->OO0o〇〇(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    .line 256
    .line 257
    goto/16 :goto_4

    .line 258
    .line 259
    :cond_d
    const v1, 0x7f0a0b99

    .line 260
    .line 261
    .line 262
    if-ne v0, v1, :cond_e

    .line 263
    .line 264
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 265
    .line 266
    const-string v0, "User Operation:  click caputure guide page"

    .line 267
    .line 268
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    .line 270
    .line 271
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 272
    .line 273
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OOo8o〇O()V

    .line 274
    .line 275
    .line 276
    goto/16 :goto_4

    .line 277
    .line 278
    :cond_e
    const v1, 0x7f0a07f9

    .line 279
    .line 280
    .line 281
    if-eq v0, v1, :cond_1e

    .line 282
    .line 283
    const v1, 0x7f0a177e

    .line 284
    .line 285
    .line 286
    if-ne v0, v1, :cond_f

    .line 287
    .line 288
    goto/16 :goto_3

    .line 289
    .line 290
    :cond_f
    const v1, 0x7f0a07c1

    .line 291
    .line 292
    .line 293
    if-ne v0, v1, :cond_10

    .line 294
    .line 295
    const-string p1, "CSPdfPackage"

    .line 296
    .line 297
    const-string v0, "adjust_page_success"

    .line 298
    .line 299
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    .line 301
    .line 302
    invoke-virtual {p0}, Lcom/intsig/fragmentBackHandler/BackHandledFragment;->onBackPressed()Z

    .line 303
    .line 304
    .line 305
    goto/16 :goto_4

    .line 306
    .line 307
    :cond_10
    const v1, 0x7f0a1694

    .line 308
    .line 309
    .line 310
    if-eq v0, v1, :cond_1d

    .line 311
    .line 312
    const v1, 0x7f0a07bf

    .line 313
    .line 314
    .line 315
    if-ne v0, v1, :cond_11

    .line 316
    .line 317
    goto/16 :goto_2

    .line 318
    .line 319
    :cond_11
    const v1, 0x7f0a07c3

    .line 320
    .line 321
    .line 322
    if-eq v0, v1, :cond_1c

    .line 323
    .line 324
    const v1, 0x7f0a07c0

    .line 325
    .line 326
    .line 327
    if-ne v0, v1, :cond_12

    .line 328
    .line 329
    goto/16 :goto_1

    .line 330
    .line 331
    :cond_12
    const v1, 0x7f0a07c2

    .line 332
    .line 333
    .line 334
    if-ne v0, v1, :cond_13

    .line 335
    .line 336
    invoke-direct {p0, v4}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oo〇88(I)V

    .line 337
    .line 338
    .line 339
    goto/16 :goto_4

    .line 340
    .line 341
    :cond_13
    const v1, 0x7f0a07bc

    .line 342
    .line 343
    .line 344
    if-ne v0, v1, :cond_14

    .line 345
    .line 346
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 347
    .line 348
    new-instance v0, Lo0Oo/〇〇0o;

    .line 349
    .line 350
    invoke-direct {v0, p0}, Lo0Oo/〇〇0o;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 351
    .line 352
    .line 353
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇(Lcom/intsig/callback/Callback0;)V

    .line 354
    .line 355
    .line 356
    goto/16 :goto_4

    .line 357
    .line 358
    :cond_14
    const v1, 0x7f0a07b9

    .line 359
    .line 360
    .line 361
    if-eq v0, v1, :cond_1b

    .line 362
    .line 363
    const v1, 0x7f0a07be

    .line 364
    .line 365
    .line 366
    if-ne v0, v1, :cond_15

    .line 367
    .line 368
    goto/16 :goto_0

    .line 369
    .line 370
    :cond_15
    const v1, 0x7f0a07bb

    .line 371
    .line 372
    .line 373
    if-ne v0, v1, :cond_16

    .line 374
    .line 375
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 376
    .line 377
    const-string v1, "click bottom more"

    .line 378
    .line 379
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    .line 381
    .line 382
    const-string v0, "document_more"

    .line 383
    .line 384
    invoke-static {v3, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    .line 386
    .line 387
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO8ooO8〇:Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;

    .line 388
    .line 389
    invoke-interface {v0, p1}, Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;->〇80〇808〇O(Landroid/view/View;)V

    .line 390
    .line 391
    .line 392
    goto/16 :goto_4

    .line 393
    .line 394
    :cond_16
    const p1, 0x7f0a0215

    .line 395
    .line 396
    .line 397
    if-ne v0, p1, :cond_17

    .line 398
    .line 399
    const-wide/16 v0, 0x8

    .line 400
    .line 401
    invoke-virtual {p0, v0, v1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8O(J)V

    .line 402
    .line 403
    .line 404
    goto/16 :goto_4

    .line 405
    .line 406
    :cond_17
    const p1, 0x7f0a0212

    .line 407
    .line 408
    .line 409
    if-ne v0, p1, :cond_18

    .line 410
    .line 411
    const-wide/16 v0, 0x2

    .line 412
    .line 413
    invoke-virtual {p0, v0, v1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8O(J)V

    .line 414
    .line 415
    .line 416
    goto :goto_4

    .line 417
    :cond_18
    const p1, 0x7f0a0211

    .line 418
    .line 419
    .line 420
    if-ne v0, p1, :cond_19

    .line 421
    .line 422
    const-wide/16 v0, 0x6

    .line 423
    .line 424
    invoke-virtual {p0, v0, v1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8O(J)V

    .line 425
    .line 426
    .line 427
    goto :goto_4

    .line 428
    :cond_19
    const p1, 0x7f0a0f36

    .line 429
    .line 430
    .line 431
    if-ne v0, p1, :cond_1a

    .line 432
    .line 433
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 434
    .line 435
    const-string v0, "onclick start get reward"

    .line 436
    .line 437
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    .line 439
    .line 440
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 441
    .line 442
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 443
    .line 444
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oOoo80oO(Landroid/app/Activity;)V

    .line 445
    .line 446
    .line 447
    goto :goto_4

    .line 448
    :cond_1a
    const p1, 0x7f0a0f37

    .line 449
    .line 450
    .line 451
    if-ne v0, p1, :cond_1f

    .line 452
    .line 453
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 454
    .line 455
    const-string v0, "onclick close  reward view"

    .line 456
    .line 457
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 458
    .line 459
    .line 460
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 461
    .line 462
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oOO0880O()V

    .line 463
    .line 464
    .line 465
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇OOoooo:Landroid/view/ViewStub;

    .line 466
    .line 467
    invoke-virtual {p1, v2}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 468
    .line 469
    .line 470
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 471
    .line 472
    const/16 v0, 0x12

    .line 473
    .line 474
    invoke-static {p1, v0}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 475
    .line 476
    .line 477
    move-result p1

    .line 478
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8o8o(I)V

    .line 479
    .line 480
    .line 481
    goto :goto_4

    .line 482
    :cond_1b
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 483
    .line 484
    new-instance v0, Lo0Oo/O000;

    .line 485
    .line 486
    invoke-direct {v0, p0}, Lo0Oo/O000;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 487
    .line 488
    .line 489
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O80〇O〇080(Lcom/intsig/callback/Callback0;)V

    .line 490
    .line 491
    .line 492
    goto :goto_4

    .line 493
    :cond_1c
    :goto_1
    const/4 p1, 0x0

    .line 494
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oo〇88(I)V

    .line 495
    .line 496
    .line 497
    goto :goto_4

    .line 498
    :cond_1d
    :goto_2
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 499
    .line 500
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇oo()Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 501
    .line 502
    .line 503
    move-result-object v0

    .line 504
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇8o8o〇()Ljava/util/ArrayList;

    .line 505
    .line 506
    .line 507
    move-result-object v0

    .line 508
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0〇OO8(Ljava/util/ArrayList;)V

    .line 509
    .line 510
    .line 511
    goto :goto_4

    .line 512
    :cond_1e
    :goto_3
    sget-object p1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 513
    .line 514
    const-string v0, "click select"

    .line 515
    .line 516
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    .line 518
    .line 519
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OoOo()V

    .line 520
    .line 521
    .line 522
    :cond_1f
    :goto_4
    return-void
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
    .line 1539
    .line 1540
    .line 1541
    .line 1542
    .line 1543
    .line 1544
    .line 1545
    .line 1546
    .line 1547
    .line 1548
    .line 1549
    .line 1550
    .line 1551
    .line 1552
    .line 1553
    .line 1554
    .line 1555
    .line 1556
    .line 1557
    .line 1558
    .line 1559
    .line 1560
    .line 1561
    .line 1562
    .line 1563
    .line 1564
    .line 1565
    .line 1566
    .line 1567
    .line 1568
    .line 1569
    .line 1570
    .line 1571
    .line 1572
    .line 1573
    .line 1574
    .line 1575
    .line 1576
    .line 1577
    .line 1578
    .line 1579
    .line 1580
    .line 1581
    .line 1582
    .line 1583
    .line 1584
    .line 1585
    .line 1586
    .line 1587
    .line 1588
    .line 1589
    .line 1590
    .line 1591
    .line 1592
    .line 1593
    .line 1594
    .line 1595
    .line 1596
    .line 1597
    .line 1598
    .line 1599
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1    # Landroid/content/res/Configuration;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 5
    .line 6
    const-string v1, "onConfigurationChanged"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o880:Landroid/os/Handler;

    .line 12
    .line 13
    new-instance v1, Lo0Oo/O0OO8〇0;

    .line 14
    .line 15
    invoke-direct {v1, p0, p1}, Lo0Oo/O0OO8〇0;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;Landroid/content/res/Configuration;)V

    .line 16
    .line 17
    .line 18
    const-wide/16 v2, 0x64

    .line 19
    .line 20
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 5
    .line 6
    const-string v1, "onCreate()"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o00〇88〇08(Landroid/os/Bundle;)V

    .line 14
    .line 15
    .line 16
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oO00〇o:Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$OnMultipleFunctionResponse;

    .line 19
    .line 20
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇O0o〇〇o(Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$OnMultipleFunctionResponse;)V

    .line 21
    .line 22
    .line 23
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 24
    .line 25
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    const-string v0, "constant_add_spec_action"

    .line 30
    .line 31
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0o(Ljava/lang/String;)Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    const-string v1, "constant_show_batch_process_tips"

    .line 40
    .line 41
    const/4 v2, 0x0

    .line 42
    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 43
    .line 44
    .line 45
    move-result v1

    .line 46
    iput-boolean v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->Oo0O0o8:Z

    .line 47
    .line 48
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 49
    .line 50
    const-string v3, "constant_doc_enc_green_channel"

    .line 51
    .line 52
    invoke-virtual {p1, v3, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 53
    .line 54
    .line 55
    move-result v3

    .line 56
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o〇88〇8(Z)V

    .line 57
    .line 58
    .line 59
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 60
    .line 61
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o〇8()Z

    .line 62
    .line 63
    .line 64
    move-result v1

    .line 65
    const/4 v3, 0x1

    .line 66
    if-eqz v1, :cond_0

    .line 67
    .line 68
    iput-boolean v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇oo〇O〇80:Z

    .line 69
    .line 70
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8oOOo:Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;

    .line 71
    .line 72
    const-string v4, "nps_dialog"

    .line 73
    .line 74
    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/pagelist/PageListFragment$DialogConflictHandling;->〇o00〇〇Oo(Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    :cond_0
    const-string v1, "extra_is_from_ocr_result_save"

    .line 78
    .line 79
    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 80
    .line 81
    .line 82
    move-result v1

    .line 83
    iput-boolean v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O8〇o〇88:Z

    .line 84
    .line 85
    const-string v1, "constant_doc_edit_type"

    .line 86
    .line 87
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v1

    .line 91
    const-string v4, "intent_extra_check_show_ad"

    .line 92
    .line 93
    invoke-virtual {p1, v4, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 94
    .line 95
    .line 96
    move-result v4

    .line 97
    iput-boolean v4, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->ooo0〇〇O:Z

    .line 98
    .line 99
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 100
    .line 101
    .line 102
    move-result v4

    .line 103
    if-eqz v4, :cond_1

    .line 104
    .line 105
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 106
    .line 107
    sget-object v4, Lcom/intsig/camscanner/pagelist/model/EditType;->DEFAULT:Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 108
    .line 109
    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Ooo8o(Lcom/intsig/camscanner/pagelist/model/EditType;)V

    .line 110
    .line 111
    .line 112
    goto :goto_0

    .line 113
    :cond_1
    sget-object v4, Lcom/intsig/camscanner/pagelist/model/EditType;->DEFAULT:Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 114
    .line 115
    invoke-virtual {v4}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 116
    .line 117
    .line 118
    move-result-object v5

    .line 119
    invoke-virtual {v1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 120
    .line 121
    .line 122
    move-result v5

    .line 123
    if-eqz v5, :cond_2

    .line 124
    .line 125
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 126
    .line 127
    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Ooo8o(Lcom/intsig/camscanner/pagelist/model/EditType;)V

    .line 128
    .line 129
    .line 130
    goto :goto_0

    .line 131
    :cond_2
    sget-object v4, Lcom/intsig/camscanner/pagelist/model/EditType;->EXTRACT:Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 132
    .line 133
    invoke-virtual {v4}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 134
    .line 135
    .line 136
    move-result-object v5

    .line 137
    invoke-virtual {v1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 138
    .line 139
    .line 140
    move-result v5

    .line 141
    if-eqz v5, :cond_3

    .line 142
    .line 143
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 144
    .line 145
    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Ooo8o(Lcom/intsig/camscanner/pagelist/model/EditType;)V

    .line 146
    .line 147
    .line 148
    goto :goto_0

    .line 149
    :cond_3
    sget-object v4, Lcom/intsig/camscanner/pagelist/model/EditType;->EXTRACT_CS_DOC:Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 150
    .line 151
    invoke-virtual {v4}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 152
    .line 153
    .line 154
    move-result-object v5

    .line 155
    invoke-virtual {v1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 156
    .line 157
    .line 158
    move-result v5

    .line 159
    if-eqz v5, :cond_4

    .line 160
    .line 161
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 162
    .line 163
    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Ooo8o(Lcom/intsig/camscanner/pagelist/model/EditType;)V

    .line 164
    .line 165
    .line 166
    goto :goto_0

    .line 167
    :cond_4
    sget-object v4, Lcom/intsig/camscanner/pagelist/model/EditType;->MOVE:Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 168
    .line 169
    invoke-virtual {v4}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 170
    .line 171
    .line 172
    move-result-object v5

    .line 173
    invoke-virtual {v1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 174
    .line 175
    .line 176
    move-result v1

    .line 177
    if-eqz v1, :cond_5

    .line 178
    .line 179
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 180
    .line 181
    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Ooo8o(Lcom/intsig/camscanner/pagelist/model/EditType;)V

    .line 182
    .line 183
    .line 184
    :cond_5
    :goto_0
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 185
    .line 186
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o〇8()Z

    .line 187
    .line 188
    .line 189
    move-result v1

    .line 190
    if-eqz v1, :cond_6

    .line 191
    .line 192
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 193
    .line 194
    const-string v4, "constant_doc_enc_green_channel_ori_path"

    .line 195
    .line 196
    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 197
    .line 198
    .line 199
    move-result-object v4

    .line 200
    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8〇80o(Ljava/lang/String;)V

    .line 201
    .line 202
    .line 203
    :cond_6
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 204
    .line 205
    const-string v4, "extra_doc_type"

    .line 206
    .line 207
    invoke-virtual {p1, v4, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 208
    .line 209
    .line 210
    move-result v4

    .line 211
    invoke-virtual {v1, v4}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇0〇0(I)V

    .line 212
    .line 213
    .line 214
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 215
    .line 216
    const-string v4, "extra_from_detect_idcard_2_a4_paper"

    .line 217
    .line 218
    invoke-virtual {p1, v4, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 219
    .line 220
    .line 221
    move-result v2

    .line 222
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇088O(Z)V

    .line 223
    .line 224
    .line 225
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇800OO〇0O:Ljava/lang/String;

    .line 226
    .line 227
    const-string v1, "Constant_doc_finish_page_type"

    .line 228
    .line 229
    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 230
    .line 231
    .line 232
    move-result-object v1

    .line 233
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O8o〇O0:Ljava/lang/String;

    .line 234
    .line 235
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 236
    .line 237
    .line 238
    move-result v0

    .line 239
    xor-int/2addr v0, v3

    .line 240
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO〇OOo:Z

    .line 241
    .line 242
    const-string v0, "extra_entrance"

    .line 243
    .line 244
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    .line 245
    .line 246
    .line 247
    move-result-object p1

    .line 248
    instance-of v0, p1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 249
    .line 250
    if-eqz v0, :cond_7

    .line 251
    .line 252
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 253
    .line 254
    check-cast p1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 255
    .line 256
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇〇0(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V

    .line 257
    .line 258
    .line 259
    :cond_7
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇〇O()Lcom/intsig/camscanner/pagelist/bubble/PageListBubbleControl;

    .line 260
    .line 261
    .line 262
    move-result-object p1

    .line 263
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOO8:Lcom/intsig/camscanner/pagelist/bubble/PageListBubbleControl;

    .line 264
    .line 265
    iget-boolean p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO〇OOo:Z

    .line 266
    .line 267
    if-eqz p1, :cond_8

    .line 268
    .line 269
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇800OO〇0O:Ljava/lang/String;

    .line 270
    .line 271
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 272
    .line 273
    .line 274
    move-result p1

    .line 275
    if-nez p1, :cond_8

    .line 276
    .line 277
    const-string p1, "spec_action_show_scan_done"

    .line 278
    .line 279
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇800OO〇0O:Ljava/lang/String;

    .line 280
    .line 281
    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 282
    .line 283
    .line 284
    move-result p1

    .line 285
    if-nez p1, :cond_9

    .line 286
    .line 287
    :cond_8
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 288
    .line 289
    .line 290
    move-result-object p1

    .line 291
    if-eqz p1, :cond_9

    .line 292
    .line 293
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 294
    .line 295
    .line 296
    move-result-object p1

    .line 297
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 298
    .line 299
    .line 300
    move-result-object p1

    .line 301
    new-instance v0, Lo0Oo/O〇08;

    .line 302
    .line 303
    invoke-direct {v0, p0}, Lo0Oo/O〇08;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 304
    .line 305
    .line 306
    invoke-static {p1, v0}, Lcom/intsig/camscanner/marketing/trialrenew/OneTrialRenewConfiguration;->〇080(Landroidx/fragment/app/FragmentManager;Lkotlin/jvm/functions/Function0;)Z

    .line 307
    .line 308
    .line 309
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 310
    .line 311
    .line 312
    move-result-object p1

    .line 313
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 314
    .line 315
    .line 316
    move-result-object p1

    .line 317
    const-string v0, "cs_list"

    .line 318
    .line 319
    invoke-static {v0, p1}, Lcom/intsig/camscanner/dialog/ExternalMemberRetainedDialog;->oOoO8OO〇(Ljava/lang/String;Landroidx/fragment/app/FragmentManager;)V

    .line 320
    .line 321
    .line 322
    :cond_9
    return-void
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public onDestroy()V
    .locals 4

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/eventbus/CsEventBus;->o〇0(Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oo0O0o8()V

    .line 7
    .line 8
    .line 9
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o880:Landroid/os/Handler;

    .line 12
    .line 13
    sget-object v2, Lcom/intsig/camscanner/pagelist/model/PageListModel;->OO:[I

    .line 14
    .line 15
    const/4 v3, 0x0

    .line 16
    invoke-static {v0, v1, v2, v3}, Lcom/intsig/comm/recycle/HandlerMsglerRecycle;->〇o〇(Ljava/lang/String;Landroid/os/Handler;[I[Ljava/lang/Runnable;)V

    .line 17
    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 20
    .line 21
    invoke-static {v1}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->〇00(Landroidx/recyclerview/widget/RecyclerView;)V

    .line 22
    .line 23
    .line 24
    const/4 v1, 0x0

    .line 25
    invoke-static {v1}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->o08808〇(Z)V

    .line 26
    .line 27
    .line 28
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    .line 29
    .line 30
    .line 31
    const-string v1, "onDestroy()"

    .line 32
    .line 33
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public onDestroyView()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->onDestroyView()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇o88:Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇o88:Lcom/intsig/camscanner/question/nps/NPSDetectionLifecycleObserver;

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Landroidx/lifecycle/Lifecycle;->removeObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 21
    .line 22
    .line 23
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O〇〇O8:Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;

    .line 24
    .line 25
    if-eqz v0, :cond_1

    .line 26
    .line 27
    invoke-interface {v0}, Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;->onDestroy()V

    .line 28
    .line 29
    .line 30
    const/4 v0, 0x0

    .line 31
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O〇〇O8:Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;

    .line 32
    .line 33
    :cond_1
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public onDetach()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDetach()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 5
    .line 6
    const-string v1, "onDetach()"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    .line 1
    sget-object p2, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v0, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v1, "keyCode="

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-static {p2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO8ooO8〇:Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;

    .line 24
    .line 25
    invoke-interface {p2, p1}, Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;->〇O8o08O(I)Z

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    return p1
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public onPause()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇oO:Lcom/intsig/camscanner/view/AbstractPullToSyncView$PullToSyncAssistant;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/AbstractPullToSyncView$PullToSyncAssistant;->〇O00()V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇0888o()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇80O8o8O〇:Landroid/widget/PopupWindow;

    .line 13
    .line 14
    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 15
    .line 16
    .line 17
    const/4 v0, 0x0

    .line 18
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇80O8o8O〇:Landroid/widget/PopupWindow;

    .line 19
    .line 20
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 21
    .line 22
    const-string v1, "onPause() clear cache"

    .line 23
    .line 24
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 28
    .line 29
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    if-eqz v0, :cond_1

    .line 34
    .line 35
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncClient;->O8ooOoo〇()Lcom/intsig/camscanner/tsapp/sync/SyncClient;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oO〇oo:Lcom/intsig/camscanner/tsapp/SyncCallbackListener;

    .line 40
    .line 41
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncClient;->〇8(Lcom/intsig/camscanner/tsapp/SyncCallbackListener;)V

    .line 42
    .line 43
    .line 44
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 45
    .line 46
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇〇00OO()I

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    if-eqz v0, :cond_2

    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 53
    .line 54
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 57
    .line 58
    .line 59
    move-result-wide v1

    .line 60
    const/4 v3, 0x6

    .line 61
    const/4 v4, 0x0

    .line 62
    invoke-static {v0, v1, v2, v3, v4}, Lcom/intsig/camscanner/tsapp/collaborate/CollaborateUtil;->OO0o〇〇〇〇0(Landroid/content/Context;JIZ)V

    .line 63
    .line 64
    .line 65
    :cond_2
    :try_start_0
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onPause()V
    :try_end_0
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    .line 67
    .line 68
    goto :goto_0

    .line 69
    :catch_0
    move-exception v0

    .line 70
    sget-object v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 71
    .line 72
    const-string v2, "onPause"

    .line 73
    .line 74
    invoke-static {v1, v2, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 75
    .line 76
    .line 77
    :goto_0
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public onResume()V
    .locals 6

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO8ooO8〇:Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;

    .line 5
    .line 6
    invoke-interface {v0}, Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;->〇o00〇〇Oo()V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇oO:Lcom/intsig/camscanner/view/AbstractPullToSyncView$PullToSyncAssistant;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/AbstractPullToSyncView$PullToSyncAssistant;->〇8o8o〇()V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 15
    .line 16
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 17
    .line 18
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 19
    .line 20
    .line 21
    move-result-wide v1

    .line 22
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->o〇8oOO88(Landroid/content/Context;J)Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-nez v0, :cond_0

    .line 27
    .line 28
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 29
    .line 30
    new-instance v1, Ljava/lang/StringBuilder;

    .line 31
    .line 32
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 33
    .line 34
    .line 35
    const-string v2, "not current account doc "

    .line 36
    .line 37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 41
    .line 42
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o()Landroid/net/Uri;

    .line 43
    .line 44
    .line 45
    move-result-object v2

    .line 46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 57
    .line 58
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 59
    .line 60
    .line 61
    return-void

    .line 62
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 63
    .line 64
    new-instance v1, Ljava/lang/StringBuilder;

    .line 65
    .line 66
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 67
    .line 68
    .line 69
    const-string v2, "onResume mSpecAction="

    .line 70
    .line 71
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇800OO〇0O:Ljava/lang/String;

    .line 75
    .line 76
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v1

    .line 83
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    invoke-static {}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OoO8()Lcom/intsig/camscanner/background_batch/client/BackScanClient;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    invoke-virtual {v0}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->o〇0OOo〇0()V

    .line 91
    .line 92
    .line 93
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇88()V

    .line 94
    .line 95
    .line 96
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 97
    .line 98
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 99
    .line 100
    .line 101
    move-result v0

    .line 102
    if-eqz v0, :cond_1

    .line 103
    .line 104
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncClient;->O8ooOoo〇()Lcom/intsig/camscanner/tsapp/sync/SyncClient;

    .line 105
    .line 106
    .line 107
    move-result-object v0

    .line 108
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oO〇oo:Lcom/intsig/camscanner/tsapp/SyncCallbackListener;

    .line 109
    .line 110
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncClient;->o〇O(Lcom/intsig/camscanner/tsapp/SyncCallbackListener;)V

    .line 111
    .line 112
    .line 113
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oo8ooo8O:Lcom/intsig/camscanner/view/PullToSyncRecyclerView;

    .line 114
    .line 115
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O0Oo〇8()Z

    .line 116
    .line 117
    .line 118
    move-result v1

    .line 119
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->setLock(Z)V

    .line 120
    .line 121
    .line 122
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->Oo0〇Ooo:Z

    .line 123
    .line 124
    const/4 v1, 0x0

    .line 125
    if-eqz v0, :cond_2

    .line 126
    .line 127
    iput-boolean v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->Oo0〇Ooo:Z

    .line 128
    .line 129
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 130
    .line 131
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O880O〇()V

    .line 132
    .line 133
    .line 134
    :cond_2
    const-string v0, "spec_action_show_image_page_view"

    .line 135
    .line 136
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇800OO〇0O:Ljava/lang/String;

    .line 137
    .line 138
    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 139
    .line 140
    .line 141
    move-result v0

    .line 142
    const/4 v2, 0x1

    .line 143
    const/4 v3, 0x0

    .line 144
    if-eqz v0, :cond_3

    .line 145
    .line 146
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇O80o8OO()V

    .line 147
    .line 148
    .line 149
    iput-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇800OO〇0O:Ljava/lang/String;

    .line 150
    .line 151
    goto/16 :goto_2

    .line 152
    .line 153
    :cond_3
    const-string v0, "space_action_show_smart_erase_share"

    .line 154
    .line 155
    iget-object v4, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇800OO〇0O:Ljava/lang/String;

    .line 156
    .line 157
    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 158
    .line 159
    .line 160
    move-result v0

    .line 161
    if-eqz v0, :cond_4

    .line 162
    .line 163
    new-instance v0, Lcom/intsig/camscanner/smarterase/share/SmartEraseShareBundle;

    .line 164
    .line 165
    iget-object v4, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 166
    .line 167
    invoke-virtual {v4}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 168
    .line 169
    .line 170
    move-result-wide v4

    .line 171
    invoke-direct {v0, v4, v5}, Lcom/intsig/camscanner/smarterase/share/SmartEraseShareBundle;-><init>(J)V

    .line 172
    .line 173
    .line 174
    iget-object v4, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 175
    .line 176
    invoke-static {v4, v0}, Lcom/intsig/camscanner/smarterase/SmartEraseUtils;->o〇0(Landroid/content/Context;Lcom/intsig/camscanner/smarterase/share/SmartEraseShareBundle;)Landroid/content/Intent;

    .line 177
    .line 178
    .line 179
    move-result-object v0

    .line 180
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 181
    .line 182
    .line 183
    iput-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇800OO〇0O:Ljava/lang/String;

    .line 184
    .line 185
    goto/16 :goto_2

    .line 186
    .line 187
    :cond_4
    const-string v0, "spec_action_transfer_doc_share"

    .line 188
    .line 189
    iget-object v4, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇800OO〇0O:Ljava/lang/String;

    .line 190
    .line 191
    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 192
    .line 193
    .line 194
    move-result v0

    .line 195
    if-eqz v0, :cond_5

    .line 196
    .line 197
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇88()V

    .line 198
    .line 199
    .line 200
    iput-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇800OO〇0O:Ljava/lang/String;

    .line 201
    .line 202
    goto/16 :goto_2

    .line 203
    .line 204
    :cond_5
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO〇OOo:Z

    .line 205
    .line 206
    if-eqz v0, :cond_f

    .line 207
    .line 208
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇800OO〇0O:Ljava/lang/String;

    .line 209
    .line 210
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 211
    .line 212
    .line 213
    move-result v0

    .line 214
    if-nez v0, :cond_f

    .line 215
    .line 216
    const-string v0, "spec_action_show_scan_done"

    .line 217
    .line 218
    iget-object v4, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇800OO〇0O:Ljava/lang/String;

    .line 219
    .line 220
    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 221
    .line 222
    .line 223
    move-result v0

    .line 224
    const/16 v4, 0x3f7

    .line 225
    .line 226
    if-eqz v0, :cond_b

    .line 227
    .line 228
    invoke-static {}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseManager;->〇o〇()V

    .line 229
    .line 230
    .line 231
    sget-object v0, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocPremiumActivity;->O0O:Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocPremiumActivity$Companion;

    .line 232
    .line 233
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocPremiumActivity$Companion;->〇080()I

    .line 234
    .line 235
    .line 236
    move-result v0

    .line 237
    if-lez v0, :cond_6

    .line 238
    .line 239
    iget-object v5, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 240
    .line 241
    invoke-static {v5, v4, v0}, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocPremiumActivity;->startActivityForResult(Landroid/app/Activity;II)V

    .line 242
    .line 243
    .line 244
    goto :goto_0

    .line 245
    :cond_6
    sget-object v0, Lcom/intsig/camscanner/guide/gppostpay/GPGuidePostPayConfiguration;->〇080:Lcom/intsig/camscanner/guide/gppostpay/GPGuidePostPayConfiguration;

    .line 246
    .line 247
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/guide/gppostpay/GPGuidePostPayConfiguration;->〇o〇(I)Z

    .line 248
    .line 249
    .line 250
    move-result v0

    .line 251
    if-eqz v0, :cond_7

    .line 252
    .line 253
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OOo08()V

    .line 254
    .line 255
    .line 256
    goto :goto_0

    .line 257
    :cond_7
    invoke-static {}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseManager;->〇080()Z

    .line 258
    .line 259
    .line 260
    move-result v0

    .line 261
    if-eqz v0, :cond_8

    .line 262
    .line 263
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OOo0Oo8O()V

    .line 264
    .line 265
    .line 266
    goto :goto_0

    .line 267
    :cond_8
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->OOO〇()Ljava/lang/Boolean;

    .line 268
    .line 269
    .line 270
    move-result-object v0

    .line 271
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 272
    .line 273
    .line 274
    move-result v0

    .line 275
    if-eqz v0, :cond_9

    .line 276
    .line 277
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o0o〇〇〇8o()Z

    .line 278
    .line 279
    .line 280
    move-result v0

    .line 281
    if-nez v0, :cond_a

    .line 282
    .line 283
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o8〇〇O8O()V

    .line 284
    .line 285
    .line 286
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 287
    .line 288
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OoOOo8(Z)V

    .line 289
    .line 290
    .line 291
    goto :goto_0

    .line 292
    :cond_9
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0o88O()V

    .line 293
    .line 294
    .line 295
    :cond_a
    :goto_0
    iput-boolean v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->ooo0〇〇O:Z

    .line 296
    .line 297
    goto :goto_1

    .line 298
    :cond_b
    const-string v0, "spec_action_loading_to_pdf_editing"

    .line 299
    .line 300
    iget-object v5, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇800OO〇0O:Ljava/lang/String;

    .line 301
    .line 302
    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 303
    .line 304
    .line 305
    move-result v0

    .line 306
    if-eqz v0, :cond_c

    .line 307
    .line 308
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oo〇O0o〇()V

    .line 309
    .line 310
    .line 311
    goto :goto_1

    .line 312
    :cond_c
    const-string v0, "SPEC_ACTION_SHOW_OPERATION_PAGE"

    .line 313
    .line 314
    iget-object v5, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇800OO〇0O:Ljava/lang/String;

    .line 315
    .line 316
    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    .line 317
    .line 318
    .line 319
    move-result v0

    .line 320
    if-eqz v0, :cond_e

    .line 321
    .line 322
    invoke-static {}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseManager;->〇o〇()V

    .line 323
    .line 324
    .line 325
    sget-object v0, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocPremiumActivity;->O0O:Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocPremiumActivity$Companion;

    .line 326
    .line 327
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocPremiumActivity$Companion;->〇080()I

    .line 328
    .line 329
    .line 330
    move-result v0

    .line 331
    if-lez v0, :cond_d

    .line 332
    .line 333
    iget-object v5, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 334
    .line 335
    invoke-static {v5, v4, v0}, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocPremiumActivity;->startActivityForResult(Landroid/app/Activity;II)V

    .line 336
    .line 337
    .line 338
    iput-boolean v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->ooo0〇〇O:Z

    .line 339
    .line 340
    goto :goto_1

    .line 341
    :cond_d
    invoke-static {}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseManager;->〇080()Z

    .line 342
    .line 343
    .line 344
    move-result v0

    .line 345
    if-eqz v0, :cond_e

    .line 346
    .line 347
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OOo0Oo8O()V

    .line 348
    .line 349
    .line 350
    iput-boolean v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->ooo0〇〇O:Z

    .line 351
    .line 352
    :cond_e
    :goto_1
    iput-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇800OO〇0O:Ljava/lang/String;

    .line 353
    .line 354
    goto :goto_2

    .line 355
    :cond_f
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇Oooo088〇()V

    .line 356
    .line 357
    .line 358
    :goto_2
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇〇O()Lcom/intsig/camscanner/pagelist/bubble/PageListBubbleControl;

    .line 359
    .line 360
    .line 361
    move-result-object v0

    .line 362
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 363
    .line 364
    invoke-virtual {v3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 365
    .line 366
    .line 367
    move-result-wide v3

    .line 368
    iget-object v5, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 369
    .line 370
    invoke-virtual {v5}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OoO〇()Lcom/intsig/camscanner/util/CSInternalResolver$CSInternalActionCallback;

    .line 371
    .line 372
    .line 373
    move-result-object v5

    .line 374
    invoke-virtual {v0, v3, v4, v5}, Lcom/intsig/camscanner/pagelist/bubble/PageListBubbleControl;->OO0o〇〇(JLcom/intsig/camscanner/util/CSInternalResolver$CSInternalActionCallback;)V

    .line 375
    .line 376
    .line 377
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 378
    .line 379
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o〇8()Z

    .line 380
    .line 381
    .line 382
    move-result v0

    .line 383
    if-eqz v0, :cond_10

    .line 384
    .line 385
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 386
    .line 387
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o08〇〇0O()V

    .line 388
    .line 389
    .line 390
    :cond_10
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0O〇O00O:Z

    .line 391
    .line 392
    if-eqz v0, :cond_12

    .line 393
    .line 394
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 395
    .line 396
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇0〇o808〇()Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 397
    .line 398
    .line 399
    move-result-object v0

    .line 400
    sget-object v3, Lcom/intsig/camscanner/pagelist/model/EditType;->EXTRACT:Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 401
    .line 402
    if-eq v0, v3, :cond_11

    .line 403
    .line 404
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 405
    .line 406
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇0〇o808〇()Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 407
    .line 408
    .line 409
    move-result-object v0

    .line 410
    sget-object v3, Lcom/intsig/camscanner/pagelist/model/EditType;->EXTRACT_CS_DOC:Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 411
    .line 412
    if-eq v0, v3, :cond_11

    .line 413
    .line 414
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 415
    .line 416
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇0〇o808〇()Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 417
    .line 418
    .line 419
    move-result-object v0

    .line 420
    sget-object v3, Lcom/intsig/camscanner/pagelist/model/EditType;->MOVE:Lcom/intsig/camscanner/pagelist/model/EditType;

    .line 421
    .line 422
    if-ne v0, v3, :cond_12

    .line 423
    .line 424
    :cond_11
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O00O()V

    .line 425
    .line 426
    .line 427
    iput-boolean v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0O〇O00O:Z

    .line 428
    .line 429
    :cond_12
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 430
    .line 431
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    .line 432
    .line 433
    .line 434
    move-result v0

    .line 435
    if-eqz v0, :cond_13

    .line 436
    .line 437
    iget v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇8〇oO〇〇8o:I

    .line 438
    .line 439
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 440
    .line 441
    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    .line 442
    .line 443
    .line 444
    move-result v3

    .line 445
    if-eq v0, v3, :cond_14

    .line 446
    .line 447
    :cond_13
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 448
    .line 449
    new-instance v3, Lo0Oo/ooo0〇O88O;

    .line 450
    .line 451
    invoke-direct {v3, p0}, Lo0Oo/ooo0〇O88O;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 452
    .line 453
    .line 454
    invoke-virtual {v0, v3}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 455
    .line 456
    .line 457
    :cond_14
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->ooo0〇〇O:Z

    .line 458
    .line 459
    if-eqz v0, :cond_15

    .line 460
    .line 461
    iput-boolean v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->ooo0〇〇O:Z

    .line 462
    .line 463
    sget-object v0, Lcom/intsig/advertisement/adapters/positions/ShotDoneManager;->OO0o〇〇:Lcom/intsig/advertisement/adapters/positions/ShotDoneManager$Companion;

    .line 464
    .line 465
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/positions/ShotDoneManager$Companion;->〇080()Lcom/intsig/advertisement/adapters/positions/ShotDoneManager;

    .line 466
    .line 467
    .line 468
    move-result-object v0

    .line 469
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 470
    .line 471
    invoke-virtual {v0, v3}, Lcom/intsig/advertisement/adapters/positions/ShotDoneManager;->o8oO〇(Landroid/app/Activity;)V

    .line 472
    .line 473
    .line 474
    :cond_15
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o00o0O〇〇o()Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;

    .line 475
    .line 476
    .line 477
    move-result-object v0

    .line 478
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 479
    .line 480
    iget-object v4, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 481
    .line 482
    invoke-virtual {v4}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO88o()Z

    .line 483
    .line 484
    .line 485
    move-result v4

    .line 486
    xor-int/2addr v4, v2

    .line 487
    invoke-virtual {v0, v2, v3, v4}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇O〇(ZLandroidx/fragment/app/FragmentActivity;Z)V

    .line 488
    .line 489
    .line 490
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->ooO888O0〇()Z

    .line 491
    .line 492
    .line 493
    move-result v0

    .line 494
    if-eqz v0, :cond_16

    .line 495
    .line 496
    const-wide/16 v2, 0x1

    .line 497
    .line 498
    invoke-virtual {p0, v2, v3}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8O(J)V

    .line 499
    .line 500
    .line 501
    invoke-static {v1}, Lcom/intsig/camscanner/mainmenu/docpage/MainDocFragment;->o08808〇(Z)V

    .line 502
    .line 503
    .line 504
    :cond_16
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇o008o08O()V

    .line 505
    .line 506
    .line 507
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O〇〇〇()V

    .line 508
    .line 509
    .line 510
    sget-object v0, Lkotlinx/coroutines/GlobalScope;->o0:Lkotlinx/coroutines/GlobalScope;

    .line 511
    .line 512
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 513
    .line 514
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->ooo〇〇O〇()J

    .line 515
    .line 516
    .line 517
    move-result-wide v1

    .line 518
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 519
    .line 520
    .line 521
    move-result-object v1

    .line 522
    invoke-static {v0, v1}, Lcom/intsig/camscanner/demoire/DeMoireManager;->〇8o8o〇(Lkotlinx/coroutines/CoroutineScope;Ljava/lang/Long;)V

    .line 523
    .line 524
    .line 525
    return-void
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .line 1
    const-string v0, "killed by system"

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o()Landroid/net/Uri;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const-string v1, "doc_uri"

    .line 14
    .line 15
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o8O〇O()I

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    const-string v1, "doc_pagenum"

    .line 25
    .line 26
    invoke-virtual {p1, v1, v0}, Landroid/os/BaseBundle;->putInt(Ljava/lang/String;I)V

    .line 27
    .line 28
    .line 29
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 30
    .line 31
    new-instance v1, Ljava/lang/StringBuilder;

    .line 32
    .line 33
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    .line 35
    .line 36
    const-string v2, "onSaveInstanceState() mPageNum = "

    .line 37
    .line 38
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 42
    .line 43
    invoke-virtual {v2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o8O〇O()I

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v1

    .line 54
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 58
    .line 59
    .line 60
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onStart()V
    .locals 6

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onStart()V

    .line 2
    .line 3
    .line 4
    const-string v0, "spec_action_show_scan_done"

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇800OO〇0O:Ljava/lang/String;

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-nez v0, :cond_4

    .line 13
    .line 14
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8O:Z

    .line 15
    .line 16
    const-string v1, ""

    .line 17
    .line 18
    if-eqz v0, :cond_0

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 21
    .line 22
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO88o()Z

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    if-eqz v0, :cond_0

    .line 27
    .line 28
    const/4 v0, 0x0

    .line 29
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8O:Z

    .line 30
    .line 31
    const-string v0, "0"

    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_0
    move-object v0, v1

    .line 35
    :goto_0
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇800OO〇0O:Ljava/lang/String;

    .line 36
    .line 37
    const/4 v3, 0x0

    .line 38
    if-nez v2, :cond_1

    .line 39
    .line 40
    goto :goto_1

    .line 41
    :cond_1
    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    .line 42
    .line 43
    .line 44
    const-string v4, "spec_action_transfer_doc_share"

    .line 45
    .line 46
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 47
    .line 48
    .line 49
    move-result v4

    .line 50
    if-nez v4, :cond_2

    .line 51
    .line 52
    const-string v4, "spec_action_transfer_doc_edit"

    .line 53
    .line 54
    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 55
    .line 56
    .line 57
    move-result v2

    .line 58
    if-nez v2, :cond_2

    .line 59
    .line 60
    goto :goto_1

    .line 61
    :cond_2
    const-string v3, "share_link_h5"

    .line 62
    .line 63
    :goto_1
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 64
    .line 65
    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 66
    .line 67
    .line 68
    move-result-object v2

    .line 69
    const-string v4, "EXTRA_FROM_PART"

    .line 70
    .line 71
    invoke-virtual {v2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v2

    .line 75
    invoke-static {}, Lcom/intsig/logagent/LogAgent;->json()Lcom/intsig/logagent/JsonBuilder;

    .line 76
    .line 77
    .line 78
    move-result-object v4

    .line 79
    const-string v5, "from"

    .line 80
    .line 81
    invoke-virtual {v4, v5, v3}, Lcom/intsig/logagent/JsonBuilder;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/logagent/JsonBuilder;

    .line 82
    .line 83
    .line 84
    const-string v3, "newdoc"

    .line 85
    .line 86
    invoke-virtual {v4, v3, v0}, Lcom/intsig/logagent/JsonBuilder;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/logagent/JsonBuilder;

    .line 87
    .line 88
    .line 89
    const-string v0, "view_type"

    .line 90
    .line 91
    const-string v3, "image"

    .line 92
    .line 93
    invoke-virtual {v4, v0, v3}, Lcom/intsig/logagent/JsonBuilder;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/logagent/JsonBuilder;

    .line 94
    .line 95
    .line 96
    new-instance v0, Ljava/lang/StringBuilder;

    .line 97
    .line 98
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 99
    .line 100
    .line 101
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 102
    .line 103
    invoke-virtual {v3}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇0008O8()I

    .line 104
    .line 105
    .line 106
    move-result v3

    .line 107
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 108
    .line 109
    .line 110
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    .line 112
    .line 113
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 114
    .line 115
    .line 116
    move-result-object v0

    .line 117
    const-string v1, "type"

    .line 118
    .line 119
    invoke-virtual {v4, v1, v0}, Lcom/intsig/logagent/JsonBuilder;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/logagent/JsonBuilder;

    .line 120
    .line 121
    .line 122
    if-eqz v2, :cond_3

    .line 123
    .line 124
    const-string v0, "from_part"

    .line 125
    .line 126
    invoke-virtual {v4, v0, v2}, Lcom/intsig/logagent/JsonBuilder;->add(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/logagent/JsonBuilder;

    .line 127
    .line 128
    .line 129
    :cond_3
    const-string v0, "CSList"

    .line 130
    .line 131
    invoke-virtual {v4}, Lcom/intsig/logagent/JsonBuilder;->get()Lorg/json/JSONObject;

    .line 132
    .line 133
    .line 134
    move-result-object v1

    .line 135
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇O00(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 136
    .line 137
    .line 138
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇800OO〇0O:Ljava/lang/String;

    .line 139
    .line 140
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 141
    .line 142
    .line 143
    move-result v0

    .line 144
    if-eqz v0, :cond_5

    .line 145
    .line 146
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 147
    .line 148
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oOO8()V

    .line 149
    .line 150
    .line 151
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->ooo0〇080()Z

    .line 152
    .line 153
    .line 154
    move-result v0

    .line 155
    if-nez v0, :cond_6

    .line 156
    .line 157
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 158
    .line 159
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO〇oo()V

    .line 160
    .line 161
    .line 162
    :cond_6
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 163
    .line 164
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o()Landroid/net/Uri;

    .line 165
    .line 166
    .line 167
    move-result-object v0

    .line 168
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 169
    .line 170
    new-instance v2, Lo0Oo/O〇OO;

    .line 171
    .line 172
    invoke-direct {v2, p0}, Lo0Oo/O〇OO;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 173
    .line 174
    .line 175
    const-wide/16 v3, 0x320

    .line 176
    .line 177
    invoke-virtual {v1, v2, v3, v4}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 178
    .line 179
    .line 180
    if-eqz v0, :cond_7

    .line 181
    .line 182
    :try_start_0
    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 183
    .line 184
    .line 185
    move-result-wide v1

    .line 186
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 187
    .line 188
    invoke-static {v3, v1, v2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇o00〇〇Oo(Landroid/content/Context;J)Z

    .line 189
    .line 190
    .line 191
    move-result v1

    .line 192
    if-nez v1, :cond_7

    .line 193
    .line 194
    sget-object v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 195
    .line 196
    new-instance v2, Ljava/lang/StringBuilder;

    .line 197
    .line 198
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 199
    .line 200
    .line 201
    const-string v3, "onStart doc may be deleted "

    .line 202
    .line 203
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 204
    .line 205
    .line 206
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 207
    .line 208
    .line 209
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 210
    .line 211
    .line 212
    move-result-object v0

    .line 213
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    .line 215
    .line 216
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o88O8()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 217
    .line 218
    .line 219
    goto :goto_2

    .line 220
    :catch_0
    move-exception v0

    .line 221
    sget-object v1, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 222
    .line 223
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 224
    .line 225
    .line 226
    :cond_7
    :goto_2
    return-void
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method public onStop()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onStop()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 5
    .line 6
    const/4 v1, 0x1

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇o08(Z)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o0OoOOo0:Z

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o8O0()V

    .line 16
    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0oO()V

    .line 19
    .line 20
    .line 21
    return-void
.end method

.method public onToolbarTitleClick(Landroid/view/View;)V
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p1}, Lcom/intsig/tsapp/sync/AppConfigJson;->isShowGptEntrance()Z

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    if-eqz p1, :cond_0

    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOO〇0o8〇()V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 16
    .line 17
    new-instance v0, Lo0Oo/O0O8OO088;

    .line 18
    .line 19
    invoke-direct {v0, p0}, Lo0Oo/O0O8OO088;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇(Lcom/intsig/callback/Callback0;)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public oooO888(IILandroid/content/Intent;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "onFragmentResult requestCode"

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string v2, " result "

    .line 17
    .line 18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    const-string p2, " data="

    .line 25
    .line 26
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 30
    .line 31
    .line 32
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object p2

    .line 36
    invoke-static {v0, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    const/16 p2, 0x67

    .line 40
    .line 41
    if-ne p1, p2, :cond_0

    .line 42
    .line 43
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇Oo(Landroid/content/Intent;)V

    .line 44
    .line 45
    .line 46
    :cond_0
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public o〇O0ooo()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "onFinish"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8O0880(Z)V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->o〇()Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-nez v0, :cond_0

    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 23
    .line 24
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->oO0〇〇O8o()Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-nez v0, :cond_0

    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 31
    .line 32
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O8O〇8oo08()Z

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    if-eqz v0, :cond_1

    .line 37
    .line 38
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 39
    .line 40
    invoke-static {v0}, Lcom/intsig/camscanner/mainmenu/MainPageRoute;->OoO8(Landroid/content/Context;)Landroid/content/Intent;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 45
    .line 46
    .line 47
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 48
    .line 49
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 50
    .line 51
    .line 52
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o〇OOo000(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o880:Landroid/os/Handler;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d031a

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public showDialog(I)V
    .locals 1

    .line 1
    const/16 v0, 0x64

    .line 2
    .line 3
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oO8O(II)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public updateTitle(Z)V
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const p1, 0x7f1301e9

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    invoke-virtual {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setToolbarTitle(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0〇oo()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    invoke-virtual {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setToolbarTitle(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    :goto_0
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇0()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O〇〇O()Lcom/intsig/camscanner/pagelist/bubble/PageListBubbleControl;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/bubble/PageListBubbleControl;->Oooo8o0〇()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇0OO8(I)V
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x1

    .line 3
    if-nez p1, :cond_0

    .line 4
    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO〇00〇0O:Z

    .line 6
    .line 7
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO8ooO8〇:Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;

    .line 8
    .line 9
    invoke-interface {v2, v1}, Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;->〇8o8o〇(Z)V

    .line 10
    .line 11
    .line 12
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0〇8o〇(Z)V

    .line 13
    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    if-ne p1, v1, :cond_1

    .line 17
    .line 18
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0〇8o〇(Z)V

    .line 19
    .line 20
    .line 21
    :cond_1
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 22
    .line 23
    invoke-virtual {v2}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->getItemCount()I

    .line 24
    .line 25
    .line 26
    move-result v2

    .line 27
    if-ne p1, v2, :cond_2

    .line 28
    .line 29
    const/4 v0, 0x1

    .line 30
    :cond_2
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO〇00〇0O:Z

    .line 31
    .line 32
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇OO8ooO8〇:Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;

    .line 33
    .line 34
    xor-int/2addr v0, v1

    .line 35
    invoke-interface {v2, v0}, Lcom/intsig/camscanner/pagelist/contract/DeviceInteface;->〇8o8o〇(Z)V

    .line 36
    .line 37
    .line 38
    :goto_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇o88〇O(I)V

    .line 39
    .line 40
    .line 41
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O0OO8O()V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇8o()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    new-instance v1, Lo0Oo/O880oOO08;

    .line 4
    .line 5
    invoke-direct {v1, p0}, Lo0Oo/O880oOO08;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 6
    .line 7
    .line 8
    const-wide/16 v2, 0x64

    .line 9
    .line 10
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O(J)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇O8oOo0()[Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇〇()[Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇O8〇OO〇(Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_1

    .line 10
    .line 11
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isDetached()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->getItemCount()I

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    if-lez v0, :cond_1

    .line 25
    .line 26
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 27
    .line 28
    add-int/lit8 v0, v0, -0x1

    .line 29
    .line 30
    invoke-virtual {v1, v0}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->getItem(I)Ljava/lang/Object;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    instance-of v0, v0, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;

    .line 35
    .line 36
    if-nez v0, :cond_1

    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 39
    .line 40
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->Oo〇o()Z

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    if-eqz v0, :cond_1

    .line 45
    .line 46
    new-instance v0, Ljava/util/ArrayList;

    .line 47
    .line 48
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 49
    .line 50
    .line 51
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    .line 53
    .line 54
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 55
    .line 56
    invoke-virtual {p1, v0}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->〇O00(Ljava/util/List;)V

    .line 57
    .line 58
    .line 59
    :try_start_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 60
    .line 61
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    .line 63
    .line 64
    goto :goto_0

    .line 65
    :catch_0
    move-exception p1

    .line 66
    sget-object v0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O8〇8000:Ljava/lang/String;

    .line 67
    .line 68
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 69
    .line 70
    .line 71
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o0〇OO008O()V

    .line 72
    .line 73
    .line 74
    :cond_1
    :goto_1
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public 〇Oo〇o8()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O880O〇()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇oO〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8O0880(Z)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇00OO()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->O00O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇08〇0oo0()Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇0o〇o8(Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;Z)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o00o0O〇〇o()Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    const/4 p2, 0x0

    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    invoke-virtual {p1, p2, v0}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇〇808〇(ZLandroidx/fragment/app/FragmentActivity;)V

    .line 9
    .line 10
    .line 11
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->ooO:Landroid/view/View;

    .line 12
    .line 13
    if-eqz p1, :cond_0

    .line 14
    .line 15
    const p2, 0x7f0a0218

    .line 16
    .line 17
    .line 18
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    check-cast p1, Lcom/intsig/view/ImageTextButton;

    .line 23
    .line 24
    if-eqz p1, :cond_0

    .line 25
    .line 26
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 27
    .line 28
    invoke-virtual {p2}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0O〇OOo()Z

    .line 29
    .line 30
    .line 31
    move-result p2

    .line 32
    if-eqz p2, :cond_0

    .line 33
    .line 34
    invoke-virtual {p1}, Lcom/intsig/view/ImageTextButton;->〇8o8o〇()V

    .line 35
    .line 36
    .line 37
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 38
    .line 39
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O0O〇OOo()Z

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    if-eqz p1, :cond_1

    .line 44
    .line 45
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08〇o0O:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 46
    .line 47
    const/16 p2, 0x8

    .line 48
    .line 49
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;->setVisibility(I)V

    .line 50
    .line 51
    .line 52
    :cond_1
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public 〇〇8O0〇8(J)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇〇O00〇8()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o00o0O〇〇o()Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x1

    .line 6
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/pagelist/dialog/PageListTagTipsWindow;->〇〇808〇(ZLandroidx/fragment/app/FragmentActivity;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇o008o08O()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇〇(Ljava/util/List;Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NotifyDataSetChanged"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/pagelist/model/PageTypeItem;",
            ">;",
            "Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;",
            ")V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    if-eqz v0, :cond_5

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_5

    .line 10
    .line 11
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isDetached()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    goto/16 :goto_2

    .line 18
    .line 19
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 20
    .line 21
    invoke-virtual {v0, p1}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->O〇8O8〇008(Ljava/util/List;)V

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 25
    .line 26
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;->〇〇〇0〇〇0(Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;)V

    .line 27
    .line 28
    .line 29
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->o0〇OO008O()V

    .line 30
    .line 31
    .line 32
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 33
    .line 34
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;->〇o(Lcom/intsig/adapter/BaseRecyclerViewAdapter$OnRecyclerViewItemClickListener;)V

    .line 35
    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 38
    .line 39
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;->o0ooO(Lcom/intsig/adapter/BaseRecyclerViewAdapter$OnRecyclerViewItemLongClickListener;)V

    .line 40
    .line 41
    .line 42
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 43
    .line 44
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;->o〇0OOo〇0(Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;)V

    .line 45
    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 48
    .line 49
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;->o〇8(Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OnOcrClickListener;)V

    .line 50
    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 53
    .line 54
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇8oOo8O()Lcom/intsig/camscanner/business/operation/OperateContent;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;->o8(Lcom/intsig/camscanner/business/operation/OperateContent;)V

    .line 61
    .line 62
    .line 63
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 64
    .line 65
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 66
    .line 67
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->〇8o〇〇8080()Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;

    .line 68
    .line 69
    .line 70
    move-result-object v1

    .line 71
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;->Oo8Oo00oo(Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;)V

    .line 72
    .line 73
    .line 74
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 75
    .line 76
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 77
    .line 78
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->OO〇0008O8()I

    .line 79
    .line 80
    .line 81
    move-result v1

    .line 82
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;->〇00〇8(I)V

    .line 83
    .line 84
    .line 85
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 86
    .line 87
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 88
    .line 89
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    .line 90
    .line 91
    .line 92
    move-result v1

    .line 93
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇0(I)I

    .line 94
    .line 95
    .line 96
    move-result v0

    .line 97
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0O:Landroidx/recyclerview/widget/GridLayoutManager;

    .line 98
    .line 99
    if-eqz v1, :cond_2

    .line 100
    .line 101
    invoke-virtual {v1}, Landroidx/recyclerview/widget/GridLayoutManager;->getSpanCount()I

    .line 102
    .line 103
    .line 104
    move-result v1

    .line 105
    if-eq v0, v1, :cond_1

    .line 106
    .line 107
    iput v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO〇00〇8oO:I

    .line 108
    .line 109
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0O:Landroidx/recyclerview/widget/GridLayoutManager;

    .line 110
    .line 111
    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/GridLayoutManager;->setSpanCount(I)V

    .line 112
    .line 113
    .line 114
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇O0OO8O()V

    .line 115
    .line 116
    .line 117
    goto :goto_0

    .line 118
    :cond_2
    iput v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO〇00〇8oO:I

    .line 119
    .line 120
    new-instance v1, Lcom/intsig/recycleviewLayoutmanager/TrycatchGridLayoutManager;

    .line 121
    .line 122
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 123
    .line 124
    invoke-direct {v1, v2, v0}, Lcom/intsig/recycleviewLayoutmanager/TrycatchGridLayoutManager;-><init>(Landroid/content/Context;I)V

    .line 125
    .line 126
    .line 127
    iput-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0O:Landroidx/recyclerview/widget/GridLayoutManager;

    .line 128
    .line 129
    const/4 v0, 0x1

    .line 130
    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->setOrientation(I)V

    .line 131
    .line 132
    .line 133
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0O:Landroidx/recyclerview/widget/GridLayoutManager;

    .line 134
    .line 135
    new-instance v2, Lcom/intsig/camscanner/pagelist/PageListFragment$1;

    .line 136
    .line 137
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/pagelist/PageListFragment$1;-><init>(Lcom/intsig/camscanner/pagelist/PageListFragment;)V

    .line 138
    .line 139
    .line 140
    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/GridLayoutManager;->setSpanSizeLookup(Landroidx/recyclerview/widget/GridLayoutManager$SpanSizeLookup;)V

    .line 141
    .line 142
    .line 143
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 144
    .line 145
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇0O:Landroidx/recyclerview/widget/GridLayoutManager;

    .line 146
    .line 147
    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 148
    .line 149
    .line 150
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 151
    .line 152
    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 153
    .line 154
    .line 155
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 156
    .line 157
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->OO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;

    .line 158
    .line 159
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 160
    .line 161
    .line 162
    :goto_0
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇〇8O0〇8(Ljava/util/List;)Z

    .line 163
    .line 164
    .line 165
    move-result p1

    .line 166
    if-eqz p1, :cond_3

    .line 167
    .line 168
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOo〇8o008:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 169
    .line 170
    const/4 p2, 0x0

    .line 171
    invoke-virtual {p1, p2}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setVisibility(I)V

    .line 172
    .line 173
    .line 174
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOo〇8o008:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 175
    .line 176
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    .line 177
    .line 178
    .line 179
    move-result-object p1

    .line 180
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇o0O:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 181
    .line 182
    invoke-virtual {p1, v0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 183
    .line 184
    .line 185
    iget-boolean p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->O8〇o〇88:Z

    .line 186
    .line 187
    if-eqz p1, :cond_4

    .line 188
    .line 189
    invoke-static {}, Lcom/intsig/camscanner/mode_ocr/PreferenceOcrHelper;->〇o00〇〇Oo()Z

    .line 190
    .line 191
    .line 192
    move-result p1

    .line 193
    if-eqz p1, :cond_4

    .line 194
    .line 195
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOo0:Lcom/intsig/comm/widget/CustomTextView;

    .line 196
    .line 197
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 198
    .line 199
    .line 200
    goto :goto_1

    .line 201
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->oOo〇8o008:Lcom/google/android/material/floatingactionbutton/FloatingActionButton;

    .line 202
    .line 203
    const/16 p2, 0x8

    .line 204
    .line 205
    invoke-virtual {p1, p2}, Lcom/google/android/material/floatingactionbutton/FloatingActionButton;->setVisibility(I)V

    .line 206
    .line 207
    .line 208
    :cond_4
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/pagelist/PageListFragment;->oO800o()V

    .line 209
    .line 210
    .line 211
    const-string p1, "spec_action_show_scan_done"

    .line 212
    .line 213
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇800OO〇0O:Ljava/lang/String;

    .line 214
    .line 215
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 216
    .line 217
    .line 218
    move-result p1

    .line 219
    if-nez p1, :cond_5

    .line 220
    .line 221
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->〇08O〇00〇o:Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;

    .line 222
    .line 223
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/presenter/PageListPresenter;->O〇0O〇Oo〇o()V

    .line 224
    .line 225
    .line 226
    :cond_5
    :goto_2
    return-void
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method public 〇〇〇0880(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/PageListFragment;->o8o:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-static {v0, p1}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

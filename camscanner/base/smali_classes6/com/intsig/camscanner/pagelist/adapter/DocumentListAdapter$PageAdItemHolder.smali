.class public Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageAdItemHolder;
.super Lcom/intsig/adapter/BaseViewHolder;
.source "DocumentListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PageAdItemHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/intsig/adapter/BaseViewHolder<",
        "Lcom/intsig/camscanner/pagelist/model/PageTypeItem;",
        ">;"
    }
.end annotation


# instance fields
.field private OO:Landroid/widget/RelativeLayout;


# direct methods
.method private constructor <init>(Landroid/view/View;)V
    .locals 1

    .line 2
    invoke-direct {p0, p1}, Lcom/intsig/adapter/BaseViewHolder;-><init>(Landroid/view/View;)V

    const v0, 0x7f0a1018

    .line 3
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RelativeLayout;

    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageAdItemHolder;->OO:Landroid/widget/RelativeLayout;

    return-void
.end method

.method synthetic constructor <init>(Landroid/view/View;L〇oO88o/〇o00〇〇Oo;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageAdItemHolder;-><init>(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic O〇8O8〇008(Ljava/lang/Object;I)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    check-cast p1, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageAdItemHolder;->〇0000OOO(Lcom/intsig/camscanner/pagelist/model/PageTypeItem;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇0000OOO(Lcom/intsig/camscanner/pagelist/model/PageTypeItem;I)V
    .locals 10
    .param p1    # Lcom/intsig/camscanner/pagelist/model/PageTypeItem;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    instance-of p2, p1, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;

    .line 2
    .line 3
    if-eqz p2, :cond_5

    .line 4
    .line 5
    check-cast p1, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;->o〇0()Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 8
    .line 9
    .line 10
    move-result-object p2

    .line 11
    if-nez p2, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 15
    .line 16
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p2}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-virtual {v1}, Lcom/intsig/advertisement/params/RequestParam;->〇80〇808〇O()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    const-string v1, "_"

    .line 31
    .line 32
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    .line 34
    .line 35
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;->Oo08()J

    .line 36
    .line 37
    .line 38
    move-result-wide v1

    .line 39
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageAdItemHolder;->OO:Landroid/widget/RelativeLayout;

    .line 47
    .line 48
    const v2, 0x7f0a0e48

    .line 49
    .line 50
    .line 51
    invoke-virtual {v1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageAdItemHolder;->OO:Landroid/widget/RelativeLayout;

    .line 56
    .line 57
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 58
    .line 59
    .line 60
    move-result-object v3

    .line 61
    check-cast v3, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 62
    .line 63
    if-eqz v3, :cond_1

    .line 64
    .line 65
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;->〇〇888()I

    .line 66
    .line 67
    .line 68
    move-result v4

    .line 69
    iput v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 70
    .line 71
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;->O8()I

    .line 72
    .line 73
    .line 74
    move-result v4

    .line 75
    iput v4, v3, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 76
    .line 77
    iget-object v4, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageAdItemHolder;->OO:Landroid/widget/RelativeLayout;

    .line 78
    .line 79
    invoke-virtual {v4, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 80
    .line 81
    .line 82
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageAdTypeItem;->〇080()V

    .line 83
    .line 84
    .line 85
    instance-of p1, v1, Ljava/lang/String;

    .line 86
    .line 87
    if-eqz p1, :cond_2

    .line 88
    .line 89
    check-cast v1, Ljava/lang/CharSequence;

    .line 90
    .line 91
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 92
    .line 93
    .line 94
    move-result p1

    .line 95
    if-eqz p1, :cond_2

    .line 96
    .line 97
    return-void

    .line 98
    :cond_2
    instance-of p1, p2, Lcom/intsig/advertisement/interfaces/BannerRequest;

    .line 99
    .line 100
    if-eqz p1, :cond_3

    .line 101
    .line 102
    check-cast p2, Lcom/intsig/advertisement/interfaces/BannerRequest;

    .line 103
    .line 104
    iget-object p1, p0, Lcom/intsig/adapter/BaseViewHolder;->o0:Landroid/content/Context;

    .line 105
    .line 106
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageAdItemHolder;->OO:Landroid/widget/RelativeLayout;

    .line 107
    .line 108
    invoke-virtual {p2, p1, v1}, Lcom/intsig/advertisement/interfaces/BannerRequest;->bindBannerView(Landroid/content/Context;Landroid/widget/RelativeLayout;)V

    .line 109
    .line 110
    .line 111
    goto :goto_0

    .line 112
    :cond_3
    instance-of p1, p2, Lcom/intsig/advertisement/interfaces/NativeRequest;

    .line 113
    .line 114
    if-eqz p1, :cond_4

    .line 115
    .line 116
    invoke-static {}, Lcom/intsig/advertisement/adapters/positions/ListBannerManager;->o〇8oOO88()Lcom/intsig/advertisement/adapters/positions/ListBannerManager;

    .line 117
    .line 118
    .line 119
    move-result-object v3

    .line 120
    iget-object v4, p0, Lcom/intsig/adapter/BaseViewHolder;->o0:Landroid/content/Context;

    .line 121
    .line 122
    iget-object v5, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageAdItemHolder;->OO:Landroid/widget/RelativeLayout;

    .line 123
    .line 124
    const/4 v6, -0x1

    .line 125
    const/4 v7, -0x1

    .line 126
    const/4 v8, 0x0

    .line 127
    const/4 v9, 0x0

    .line 128
    invoke-virtual/range {v3 .. v9}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->o800o8O(Landroid/content/Context;Landroid/view/ViewGroup;IIILcom/intsig/advertisement/listener/OnFeedBackListener;)Z

    .line 129
    .line 130
    .line 131
    :cond_4
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageAdItemHolder;->OO:Landroid/widget/RelativeLayout;

    .line 132
    .line 133
    invoke-virtual {p1, v2, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 134
    .line 135
    .line 136
    :cond_5
    return-void
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.class public Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;
.super Lcom/intsig/adapter/BaseViewHolder;
.source "DocumentListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OperationHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/intsig/adapter/BaseViewHolder<",
        "Lcom/intsig/camscanner/pagelist/model/PageTypeItem;",
        ">;"
    }
.end annotation


# instance fields
.field public O0O:Landroid/widget/TextView;

.field private O88O:Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;

.field public O8o08O8O:Landroid/widget/ImageView;

.field public OO:Landroid/widget/RelativeLayout;

.field public OO〇00〇8oO:Landroidx/constraintlayout/widget/ConstraintLayout;

.field public o8oOOo:Landroid/widget/TextView;

.field public o8〇OO0〇0o:Landroid/widget/LinearLayout;

.field public oOo0:Landroidx/constraintlayout/widget/ConstraintLayout;

.field public oOo〇8o008:Landroid/widget/TextView;

.field public ooo0〇〇O:Landroid/widget/ImageView;

.field public o〇00O:Landroid/widget/ImageView;

.field public 〇080OO8〇0:Landroid/widget/TextView;

.field public 〇08O〇00〇o:Landroid/widget/TextView;

.field public 〇0O:Landroid/widget/TextView;

.field public 〇8〇oO〇〇8o:Landroid/widget/ImageView;

.field public 〇O〇〇O8:Landroidx/constraintlayout/widget/ConstraintLayout;

.field private 〇o0O:Lcom/intsig/camscanner/business/operation/OperateContent;

.field public 〇〇08O:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/adapter/BaseViewHolder;-><init>(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    const v0, 0x7f0a0419

    .line 5
    .line 6
    .line 7
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->oOo0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 14
    .line 15
    const v0, 0x7f0a0fcc

    .line 16
    .line 17
    .line 18
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    check-cast v0, Landroid/widget/RelativeLayout;

    .line 23
    .line 24
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->OO:Landroid/widget/RelativeLayout;

    .line 25
    .line 26
    const v0, 0x7f0a11bb

    .line 27
    .line 28
    .line 29
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    check-cast v0, Landroid/widget/TextView;

    .line 34
    .line 35
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 36
    .line 37
    const v0, 0x7f0a11f5

    .line 38
    .line 39
    .line 40
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    check-cast v0, Landroid/widget/ImageView;

    .line 45
    .line 46
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->o〇00O:Landroid/widget/ImageView;

    .line 47
    .line 48
    const v0, 0x7f0a01d4

    .line 49
    .line 50
    .line 51
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    check-cast v0, Landroid/widget/ImageView;

    .line 56
    .line 57
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->O8o08O8O:Landroid/widget/ImageView;

    .line 58
    .line 59
    const v0, 0x7f0a11b9

    .line 60
    .line 61
    .line 62
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    check-cast v0, Landroid/widget/TextView;

    .line 67
    .line 68
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->〇080OO8〇0:Landroid/widget/TextView;

    .line 69
    .line 70
    const v0, 0x7f0a05d3

    .line 71
    .line 72
    .line 73
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    check-cast v0, Landroid/widget/TextView;

    .line 78
    .line 79
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->〇0O:Landroid/widget/TextView;

    .line 80
    .line 81
    const v0, 0x7f0a1414

    .line 82
    .line 83
    .line 84
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    check-cast v0, Landroid/widget/TextView;

    .line 89
    .line 90
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->oOo〇8o008:Landroid/widget/TextView;

    .line 91
    .line 92
    const v0, 0x7f0a100c

    .line 93
    .line 94
    .line 95
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 96
    .line 97
    .line 98
    move-result-object v0

    .line 99
    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 100
    .line 101
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->OO〇00〇8oO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 102
    .line 103
    const v0, 0x7f0a100d

    .line 104
    .line 105
    .line 106
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 107
    .line 108
    .line 109
    move-result-object v0

    .line 110
    check-cast v0, Landroid/widget/LinearLayout;

    .line 111
    .line 112
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->o8〇OO0〇0o:Landroid/widget/LinearLayout;

    .line 113
    .line 114
    const v0, 0x7f0a09a2

    .line 115
    .line 116
    .line 117
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 118
    .line 119
    .line 120
    move-result-object v0

    .line 121
    check-cast v0, Landroid/widget/ImageView;

    .line 122
    .line 123
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->〇8〇oO〇〇8o:Landroid/widget/ImageView;

    .line 124
    .line 125
    const v0, 0x7f0a0a7e

    .line 126
    .line 127
    .line 128
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 129
    .line 130
    .line 131
    move-result-object v0

    .line 132
    check-cast v0, Landroid/widget/ImageView;

    .line 133
    .line 134
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->ooo0〇〇O:Landroid/widget/ImageView;

    .line 135
    .line 136
    const v0, 0x7f0a1372

    .line 137
    .line 138
    .line 139
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 140
    .line 141
    .line 142
    move-result-object v0

    .line 143
    check-cast v0, Landroid/widget/TextView;

    .line 144
    .line 145
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->〇〇08O:Landroid/widget/TextView;

    .line 146
    .line 147
    const v0, 0x7f0a1378

    .line 148
    .line 149
    .line 150
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 151
    .line 152
    .line 153
    move-result-object v0

    .line 154
    check-cast v0, Landroid/widget/TextView;

    .line 155
    .line 156
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->O0O:Landroid/widget/TextView;

    .line 157
    .line 158
    const v0, 0x7f0a12ac

    .line 159
    .line 160
    .line 161
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 162
    .line 163
    .line 164
    move-result-object v0

    .line 165
    check-cast v0, Landroid/widget/TextView;

    .line 166
    .line 167
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->o8oOOo:Landroid/widget/TextView;

    .line 168
    .line 169
    const v0, 0x7f0a03f6

    .line 170
    .line 171
    .line 172
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 173
    .line 174
    .line 175
    move-result-object p1

    .line 176
    check-cast p1, Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 177
    .line 178
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->〇O〇〇O8:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 179
    .line 180
    return-void
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method private static synthetic o〇〇0〇(Lcom/intsig/camscanner/business/operation/document_page/ODOperateContent;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-interface {p0}, Lcom/intsig/camscanner/business/operation/document_page/ODOperateContent;->〇o00〇〇Oo()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇0000OOO(Lcom/intsig/camscanner/business/operation/document_page/ODOperateContent;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->o〇〇0〇(Lcom/intsig/camscanner/business/operation/document_page/ODOperateContent;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public O8〇o(Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->O88O:Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public OOO〇O0(Lcom/intsig/camscanner/pagelist/model/PageTypeItem;I)V
    .locals 2
    .param p1    # Lcom/intsig/camscanner/pagelist/model/PageTypeItem;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->〇o0O:Lcom/intsig/camscanner/business/operation/OperateContent;

    .line 2
    .line 3
    instance-of p2, p2, Lcom/intsig/camscanner/business/operation/document_page/ODOperateContent;

    .line 4
    .line 5
    const-string v0, "OperationHolder"

    .line 6
    .line 7
    if-nez p2, :cond_0

    .line 8
    .line 9
    const-string p1, "here should be ODOperateContent Class type"

    .line 10
    .line 11
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    instance-of p2, p1, Lcom/intsig/camscanner/pagelist/model/PageOperationItem;

    .line 16
    .line 17
    if-nez p2, :cond_1

    .line 18
    .line 19
    const-string p1, "data class not PageOperationItem"

    .line 20
    .line 21
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    return-void

    .line 25
    :cond_1
    check-cast p1, Lcom/intsig/camscanner/pagelist/model/PageOperationItem;

    .line 26
    .line 27
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageOperationItem;->〇080()Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->〇o0O:Lcom/intsig/camscanner/business/operation/OperateContent;

    .line 32
    .line 33
    check-cast p2, Lcom/intsig/camscanner/business/operation/document_page/ODOperateContent;

    .line 34
    .line 35
    if-eqz p1, :cond_2

    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->O88O:Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;

    .line 38
    .line 39
    invoke-interface {v0}, Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;->〇080()Z

    .line 40
    .line 41
    .line 42
    move-result v0

    .line 43
    if-eqz v0, :cond_2

    .line 44
    .line 45
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->O88O:Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;

    .line 46
    .line 47
    const/4 v1, 0x0

    .line 48
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/business/operation/OperationShowTraceCallback;->〇o00〇〇Oo(Z)V

    .line 49
    .line 50
    .line 51
    iget v0, p1, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;->〇080:I

    .line 52
    .line 53
    iget p1, p1, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine$Data;->〇o00〇〇Oo:I

    .line 54
    .line 55
    invoke-static {v0, p1}, Lcom/intsig/camscanner/business/operation/document_page/OperateDocumentEngine;->o〇0(II)V

    .line 56
    .line 57
    .line 58
    :cond_2
    instance-of p1, p2, Lcom/intsig/camscanner/business/operation/document_page/ODCamExamDiversion;

    .line 59
    .line 60
    if-nez p1, :cond_3

    .line 61
    .line 62
    invoke-interface {p2, p0}, Lcom/intsig/camscanner/business/operation/document_page/ODOperateContent;->O8(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;)V

    .line 63
    .line 64
    .line 65
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->oOo0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 66
    .line 67
    new-instance v0, L〇oO88o/〇080;

    .line 68
    .line 69
    invoke-direct {v0, p2}, L〇oO88o/〇080;-><init>(Lcom/intsig/camscanner/business/operation/document_page/ODOperateContent;)V

    .line 70
    .line 71
    .line 72
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    .line 74
    .line 75
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public bridge synthetic O〇8O8〇008(Ljava/lang/Object;I)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    check-cast p1, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->OOO〇O0(Lcom/intsig/camscanner/pagelist/model/PageTypeItem;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public oo〇(Lcom/intsig/camscanner/business/operation/OperateContent;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OperationHolder;->〇o0O:Lcom/intsig/camscanner/business/operation/OperateContent;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.class public Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;
.super Lcom/intsig/adapter/BaseViewHolder;
.source "DocumentListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PageImageHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/intsig/adapter/BaseViewHolder<",
        "Lcom/intsig/camscanner/pagelist/model/PageTypeItem;",
        ">;"
    }
.end annotation


# instance fields
.field public O0O:Landroid/view/View;

.field public O88O:Landroid/widget/ImageView;

.field public O8o08O8O:Landroid/widget/ImageView;

.field public OO:Landroid/view/View;

.field public OO〇00〇8oO:Landroid/widget/TextView;

.field private Oo80:Lcom/intsig/adapter/BaseRecyclerViewAdapter$OnRecyclerViewItemLongClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/adapter/BaseRecyclerViewAdapter$OnRecyclerViewItemLongClickListener<",
            "Lcom/intsig/camscanner/pagelist/model/PageTypeItem;",
            ">;"
        }
    .end annotation
.end field

.field private O〇08oOOO0:Landroid/view/View$OnClickListener;

.field private O〇o88o08〇:I

.field private o8o:Z

.field public o8oOOo:Landroid/widget/CheckBox;

.field public o8〇OO0〇0o:Landroid/widget/ImageView;

.field private oOO〇〇:Z

.field public oOo0:Landroid/widget/ImageView;

.field public oOo〇8o008:Landroid/widget/ImageView;

.field private oo8ooo8O:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

.field public ooo0〇〇O:Landroid/widget/TextView;

.field public o〇00O:Landroid/widget/TextView;

.field private o〇oO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OnOcrClickListener;

.field private 〇00O0:Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;

.field public 〇080OO8〇0:Landroid/view/View;

.field public 〇08O〇00〇o:Landroid/widget/TextView;

.field private 〇08〇o0O:Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;

.field public 〇0O:Landroid/view/View;

.field public 〇8〇oO〇〇8o:Landroid/view/View;

.field public 〇O〇〇O8:Landroid/widget/TextView;

.field public 〇o0O:Landroid/view/View;

.field public 〇〇08O:Landroid/widget/TextView;

.field private 〇〇o〇:Lcom/intsig/adapter/BaseRecyclerViewAdapter$OnRecyclerViewItemClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/adapter/BaseRecyclerViewAdapter$OnRecyclerViewItemClickListener<",
            "Lcom/intsig/camscanner/pagelist/model/PageTypeItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/adapter/BaseViewHolder;-><init>(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder$1;

    .line 5
    .line 6
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder$1;-><init>(Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;)V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->O〇08oOOO0:Landroid/view/View$OnClickListener;

    .line 10
    .line 11
    const v0, 0x7f0a0fcd

    .line 12
    .line 13
    .line 14
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->OO:Landroid/view/View;

    .line 19
    .line 20
    const v0, 0x7f0a11ab

    .line 21
    .line 22
    .line 23
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    check-cast v0, Landroid/widget/TextView;

    .line 28
    .line 29
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 30
    .line 31
    const v0, 0x7f0a1966

    .line 32
    .line 33
    .line 34
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    check-cast v0, Landroid/widget/TextView;

    .line 39
    .line 40
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o〇00O:Landroid/widget/TextView;

    .line 41
    .line 42
    const v0, 0x7f0a0e43

    .line 43
    .line 44
    .line 45
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    check-cast v0, Landroid/widget/ImageView;

    .line 50
    .line 51
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oOo〇8o008:Landroid/widget/ImageView;

    .line 52
    .line 53
    const v0, 0x7f0a11aa

    .line 54
    .line 55
    .line 56
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    check-cast v0, Landroid/widget/TextView;

    .line 61
    .line 62
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 63
    .line 64
    const v0, 0x7f0a1129

    .line 65
    .line 66
    .line 67
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇080OO8〇0:Landroid/view/View;

    .line 72
    .line 73
    const v0, 0x7f0a1127

    .line 74
    .line 75
    .line 76
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇0O:Landroid/view/View;

    .line 81
    .line 82
    const v0, 0x7f0a1128

    .line 83
    .line 84
    .line 85
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 86
    .line 87
    .line 88
    move-result-object v0

    .line 89
    check-cast v0, Landroid/widget/ImageView;

    .line 90
    .line 91
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->O8o08O8O:Landroid/widget/ImageView;

    .line 92
    .line 93
    const v0, 0x7f0a1172

    .line 94
    .line 95
    .line 96
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 97
    .line 98
    .line 99
    move-result-object v0

    .line 100
    check-cast v0, Landroid/widget/ImageView;

    .line 101
    .line 102
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oOo0:Landroid/widget/ImageView;

    .line 103
    .line 104
    const v0, 0x7f0a0733

    .line 105
    .line 106
    .line 107
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 108
    .line 109
    .line 110
    move-result-object v0

    .line 111
    check-cast v0, Landroid/widget/ImageView;

    .line 112
    .line 113
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o8〇OO0〇0o:Landroid/widget/ImageView;

    .line 114
    .line 115
    const v0, 0x7f0a0c7b

    .line 116
    .line 117
    .line 118
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 119
    .line 120
    .line 121
    move-result-object v0

    .line 122
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇8〇oO〇〇8o:Landroid/view/View;

    .line 123
    .line 124
    const v0, 0x7f0a1964

    .line 125
    .line 126
    .line 127
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 128
    .line 129
    .line 130
    move-result-object v0

    .line 131
    check-cast v0, Landroid/widget/TextView;

    .line 132
    .line 133
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->ooo0〇〇O:Landroid/widget/TextView;

    .line 134
    .line 135
    const v0, 0x7f0a1965

    .line 136
    .line 137
    .line 138
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 139
    .line 140
    .line 141
    move-result-object v0

    .line 142
    check-cast v0, Landroid/widget/TextView;

    .line 143
    .line 144
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇〇08O:Landroid/widget/TextView;

    .line 145
    .line 146
    const v0, 0x7f0a0336

    .line 147
    .line 148
    .line 149
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 150
    .line 151
    .line 152
    move-result-object v0

    .line 153
    check-cast v0, Landroid/widget/CheckBox;

    .line 154
    .line 155
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o8oOOo:Landroid/widget/CheckBox;

    .line 156
    .line 157
    const v0, 0x7f0a1780

    .line 158
    .line 159
    .line 160
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 161
    .line 162
    .line 163
    move-result-object v0

    .line 164
    check-cast v0, Landroid/widget/TextView;

    .line 165
    .line 166
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇O〇〇O8:Landroid/widget/TextView;

    .line 167
    .line 168
    const v0, 0x7f0a0cd3

    .line 169
    .line 170
    .line 171
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 172
    .line 173
    .line 174
    move-result-object v0

    .line 175
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->O0O:Landroid/view/View;

    .line 176
    .line 177
    const v0, 0x7f0a1995

    .line 178
    .line 179
    .line 180
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 181
    .line 182
    .line 183
    move-result-object v0

    .line 184
    iput-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇o0O:Landroid/view/View;

    .line 185
    .line 186
    const v0, 0x7f0a0f0e

    .line 187
    .line 188
    .line 189
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 190
    .line 191
    .line 192
    move-result-object p1

    .line 193
    check-cast p1, Landroid/widget/ImageView;

    .line 194
    .line 195
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->O88O:Landroid/widget/ImageView;

    .line 196
    .line 197
    iput-object p2, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇00O0:Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;

    .line 198
    .line 199
    return-void
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method private O000(Lcom/intsig/camscanner/pagelist/model/PageItem;)V
    .locals 5

    .line 1
    iget v0, p1, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇〇808〇:I

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/business/folders/OfflineFolder;->OO0o〇〇(I)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/16 v1, 0x8

    .line 8
    .line 9
    if-nez v0, :cond_4

    .line 10
    .line 11
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 12
    .line 13
    invoke-static {v0}, Lcom/intsig/camscanner/app/AppUtil;->oO(Landroid/content/Context;)Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    iget v0, p1, Lcom/intsig/camscanner/pagelist/model/PageItem;->OO0o〇〇:I

    .line 21
    .line 22
    const/4 v2, 0x1

    .line 23
    const v3, 0x7f081202

    .line 24
    .line 25
    .line 26
    const/4 v4, 0x0

    .line 27
    if-ne v0, v2, :cond_1

    .line 28
    .line 29
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oOo0:Landroid/widget/ImageView;

    .line 30
    .line 31
    invoke-virtual {p1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 32
    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oOo0:Landroid/widget/ImageView;

    .line 35
    .line 36
    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 37
    .line 38
    .line 39
    goto :goto_1

    .line 40
    :cond_1
    const/4 v2, 0x2

    .line 41
    if-ne v0, v2, :cond_2

    .line 42
    .line 43
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oOo0:Landroid/widget/ImageView;

    .line 44
    .line 45
    invoke-virtual {p1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 46
    .line 47
    .line 48
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oOo0:Landroid/widget/ImageView;

    .line 49
    .line 50
    const v0, 0x7f081206

    .line 51
    .line 52
    .line 53
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 54
    .line 55
    .line 56
    goto :goto_1

    .line 57
    :cond_2
    iget p1, p1, Lcom/intsig/camscanner/pagelist/model/PageItem;->Oooo8o0〇:I

    .line 58
    .line 59
    const/4 v0, -0x1

    .line 60
    if-ne p1, v0, :cond_3

    .line 61
    .line 62
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oOo0:Landroid/widget/ImageView;

    .line 63
    .line 64
    invoke-virtual {p1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 65
    .line 66
    .line 67
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oOo0:Landroid/widget/ImageView;

    .line 68
    .line 69
    invoke-virtual {p1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 70
    .line 71
    .line 72
    goto :goto_1

    .line 73
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oOo0:Landroid/widget/ImageView;

    .line 74
    .line 75
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 76
    .line 77
    .line 78
    goto :goto_1

    .line 79
    :cond_4
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oOo0:Landroid/widget/ImageView;

    .line 80
    .line 81
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 82
    .line 83
    .line 84
    :goto_1
    return-void
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method static bridge synthetic O8〇o(Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oOO〇〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private OOO(Landroid/view/View;I)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eq v0, p2, :cond_0

    .line 8
    .line 9
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic OOO〇O0(Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;Lcom/intsig/camscanner/pagelist/model/PageTypeItem;ILandroid/view/View;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o〇0OOo〇0(Lcom/intsig/camscanner/pagelist/model/PageTypeItem;ILandroid/view/View;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private synthetic Oo8Oo00oo(Lcom/intsig/camscanner/pagelist/model/PageItem;)V
    .locals 10

    .line 1
    sget-object v0, Lcom/intsig/camscanner/sharedir/InviteShareDirSyncClient;->〇O8o08O:Lcom/intsig/camscanner/sharedir/InviteShareDirSyncClient$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/sharedir/InviteShareDirSyncClient$Companion;->O8()Lcom/intsig/camscanner/sharedir/InviteShareDirSyncClient;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/sharedir/InviteShareDirSyncClient;->O8〇o()Lcom/intsig/camscanner/tsapp/request/RequestTask;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    new-instance v9, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;

    .line 12
    .line 13
    iget-wide v2, p1, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇080:J

    .line 14
    .line 15
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 16
    .line 17
    .line 18
    move-result-wide v4

    .line 19
    const/4 v6, 0x1

    .line 20
    const/4 v7, 0x0

    .line 21
    iget-object v8, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇08〇o0O:Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;

    .line 22
    .line 23
    move-object v1, v9

    .line 24
    invoke-direct/range {v1 .. v8}, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;-><init>(JJZILcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;)V

    .line 25
    .line 26
    .line 27
    const/4 p1, 0x0

    .line 28
    invoke-virtual {v0, v9, p1, p1}, Lcom/intsig/camscanner/tsapp/request/RequestTask;->OoO8(Lcom/intsig/camscanner/tsapp/request/RequestTaskData;ZZ)V

    .line 29
    .line 30
    .line 31
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 32
    .line 33
    .line 34
    move-result-wide v1

    .line 35
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/tsapp/request/RequestTask;->〇oOO8O8(J)V

    .line 36
    .line 37
    .line 38
    sget-object p1, Lcom/intsig/camscanner/tsapp/imagedownload/ImageDownloadClient;->〇8o8o〇:Lcom/intsig/camscanner/tsapp/imagedownload/ImageDownloadClient$Companion;

    .line 39
    .line 40
    invoke-virtual {p1}, Lcom/intsig/camscanner/tsapp/imagedownload/ImageDownloadClient$Companion;->〇080()Lcom/intsig/camscanner/tsapp/imagedownload/ImageDownloadClient;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/tsapp/request/RequestTaskClient;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/tsapp/request/RequestTask;)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private Ooo(Lcom/intsig/camscanner/pagelist/model/PageItem;)V
    .locals 3

    .line 1
    iget-object p1, p1, Lcom/intsig/camscanner/pagelist/model/PageItem;->oO80:Ljava/lang/String;

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oo8ooo8O:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->oO80()[Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oo8ooo8O:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇〇888()[Ljava/util/regex/Pattern;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    array-length v2, v0

    .line 18
    if-lez v2, :cond_1

    .line 19
    .line 20
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    if-nez v2, :cond_1

    .line 25
    .line 26
    invoke-static {p1, v0}, Lcom/intsig/camscanner/util/StringUtil;->〇o00〇〇Oo(Ljava/lang/String;[Ljava/lang/String;)Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-eqz v0, :cond_0

    .line 31
    .line 32
    if-eqz v1, :cond_0

    .line 33
    .line 34
    array-length v0, v1

    .line 35
    if-lez v0, :cond_0

    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o〇00O:Landroid/widget/TextView;

    .line 38
    .line 39
    iget-object v2, p0, Lcom/intsig/adapter/BaseViewHolder;->o0:Landroid/content/Context;

    .line 40
    .line 41
    invoke-static {p1, v1, v2}, Lcom/intsig/camscanner/util/StringUtil;->〇80〇808〇O(Ljava/lang/String;[Ljava/util/regex/Pattern;Landroid/content/Context;)Landroid/text/SpannableStringBuilder;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 46
    .line 47
    .line 48
    const/4 p1, 0x1

    .line 49
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o8o:Z

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o〇00O:Landroid/widget/TextView;

    .line 53
    .line 54
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o〇00O:Landroid/widget/TextView;

    .line 59
    .line 60
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    .line 62
    .line 63
    :goto_0
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private o0ooO(Lcom/intsig/camscanner/pagelist/model/PageItem;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->O88O:Landroid/widget/ImageView;

    .line 2
    .line 3
    const/16 v0, 0x8

    .line 4
    .line 5
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private oO(Lcom/intsig/camscanner/pagelist/model/PageItem;Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;)V
    .locals 4

    .line 1
    invoke-virtual {p2}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->oO80()[Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p2}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇〇808〇()Z

    .line 6
    .line 7
    .line 8
    move-result p2

    .line 9
    const v1, 0x7f080fac

    .line 10
    .line 11
    .line 12
    if-nez p2, :cond_6

    .line 13
    .line 14
    if-eqz v0, :cond_6

    .line 15
    .line 16
    array-length p2, v0

    .line 17
    if-lez p2, :cond_6

    .line 18
    .line 19
    iget-object p2, p1, Lcom/intsig/camscanner/pagelist/model/PageItem;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 20
    .line 21
    iget-object v2, p1, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇8o8o〇:Ljava/lang/String;

    .line 22
    .line 23
    iget-object p1, p1, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇O00:Ljava/lang/String;

    .line 24
    .line 25
    iget-boolean v3, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o8o:Z

    .line 26
    .line 27
    if-nez v3, :cond_4

    .line 28
    .line 29
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 30
    .line 31
    .line 32
    move-result v3

    .line 33
    if-nez v3, :cond_0

    .line 34
    .line 35
    invoke-static {p2, v0}, Lcom/intsig/camscanner/util/StringUtil;->〇o00〇〇Oo(Ljava/lang/String;[Ljava/lang/String;)Z

    .line 36
    .line 37
    .line 38
    move-result p2

    .line 39
    if-nez p2, :cond_2

    .line 40
    .line 41
    :cond_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 42
    .line 43
    .line 44
    move-result p2

    .line 45
    if-nez p2, :cond_1

    .line 46
    .line 47
    invoke-static {v2, v0}, Lcom/intsig/camscanner/util/StringUtil;->〇o00〇〇Oo(Ljava/lang/String;[Ljava/lang/String;)Z

    .line 48
    .line 49
    .line 50
    move-result p2

    .line 51
    if-nez p2, :cond_2

    .line 52
    .line 53
    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 54
    .line 55
    .line 56
    move-result p2

    .line 57
    if-nez p2, :cond_3

    .line 58
    .line 59
    invoke-static {p1, v0}, Lcom/intsig/camscanner/util/StringUtil;->〇o00〇〇Oo(Ljava/lang/String;[Ljava/lang/String;)Z

    .line 60
    .line 61
    .line 62
    move-result p1

    .line 63
    if-eqz p1, :cond_3

    .line 64
    .line 65
    :cond_2
    const/4 p1, 0x1

    .line 66
    goto :goto_0

    .line 67
    :cond_3
    const/4 p1, 0x0

    .line 68
    :goto_0
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o8o:Z

    .line 69
    .line 70
    :cond_4
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->OO:Landroid/view/View;

    .line 71
    .line 72
    iget-boolean p2, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o8o:Z

    .line 73
    .line 74
    if-eqz p2, :cond_5

    .line 75
    .line 76
    const v1, 0x7f080fae

    .line 77
    .line 78
    .line 79
    :cond_5
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 80
    .line 81
    .line 82
    goto :goto_1

    .line 83
    :cond_6
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->OO:Landroid/view/View;

    .line 84
    .line 85
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 86
    .line 87
    .line 88
    :goto_1
    return-void
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private oO00OOO(Lcom/intsig/camscanner/pagelist/model/PageItem;)V
    .locals 2

    .line 1
    iget p1, p1, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇O8o08O:I

    .line 2
    .line 3
    if-nez p1, :cond_0

    .line 4
    .line 5
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->O8o08O8O:Landroid/widget/ImageView;

    .line 6
    .line 7
    const/16 v0, 0x8

    .line 8
    .line 9
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 10
    .line 11
    .line 12
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->O8o08O8O:Landroid/widget/ImageView;

    .line 13
    .line 14
    const/4 v1, 0x0

    .line 15
    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 16
    .line 17
    .line 18
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇080OO8〇0:Landroid/view/View;

    .line 19
    .line 20
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 21
    .line 22
    .line 23
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇0O:Landroid/view/View;

    .line 24
    .line 25
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    const/4 v0, 0x2

    .line 30
    if-eq p1, v0, :cond_1

    .line 31
    .line 32
    const/4 v0, 0x3

    .line 33
    if-eq p1, v0, :cond_1

    .line 34
    .line 35
    const/4 v0, 0x1

    .line 36
    if-ne p1, v0, :cond_2

    .line 37
    .line 38
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->O8o08O8O:Landroid/widget/ImageView;

    .line 39
    .line 40
    const v0, 0x7f08103e

    .line 41
    .line 42
    .line 43
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 44
    .line 45
    .line 46
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->O8o08O8O:Landroid/widget/ImageView;

    .line 47
    .line 48
    const/4 v0, 0x0

    .line 49
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 50
    .line 51
    .line 52
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇080OO8〇0:Landroid/view/View;

    .line 53
    .line 54
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 55
    .line 56
    .line 57
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇0O:Landroid/view/View;

    .line 58
    .line 59
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 60
    .line 61
    .line 62
    :cond_2
    :goto_0
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic oo〇(Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;)Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oo8ooo8O:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic o〇0OOo〇0(Lcom/intsig/camscanner/pagelist/model/PageTypeItem;ILandroid/view/View;)Z
    .locals 2

    .line 1
    iget-object p3, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->Oo80:Lcom/intsig/adapter/BaseRecyclerViewAdapter$OnRecyclerViewItemLongClickListener;

    .line 2
    .line 3
    if-eqz p3, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;->getType()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    invoke-interface {p3, v0, v1, p1, p2}, Lcom/intsig/adapter/BaseRecyclerViewAdapter$OnRecyclerViewItemLongClickListener;->OOo(Landroid/view/View;ILjava/lang/Object;I)Z

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    return p1

    .line 16
    :cond_0
    const/4 p1, 0x0

    .line 17
    return p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private o〇8(Lcom/intsig/camscanner/pagelist/model/PageItem;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oo8ooo8O:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇O〇()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_2

    .line 8
    .line 9
    iget v0, p1, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇O8o08O:I

    .line 10
    .line 11
    if-nez v0, :cond_2

    .line 12
    .line 13
    iget-object v0, p1, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇o〇:Ljava/lang/String;

    .line 14
    .line 15
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-nez v1, :cond_0

    .line 20
    .line 21
    iget-object v0, p1, Lcom/intsig/camscanner/pagelist/model/PageItem;->O8:Ljava/lang/String;

    .line 22
    .line 23
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->OOO〇O0()I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    if-nez v1, :cond_1

    .line 28
    .line 29
    new-instance v1, Ljava/lang/StringBuilder;

    .line 30
    .line 31
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    .line 33
    .line 34
    invoke-static {v0}, Lcom/intsig/camscanner/util/StringUtil;->oO80(Ljava/lang/String;)Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    const-string v0, ","

    .line 42
    .line 43
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    iget-object v0, p1, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇o00〇〇Oo:Ljava/lang/String;

    .line 47
    .line 48
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->ooo0〇〇O:Landroid/widget/TextView;

    .line 56
    .line 57
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    .line 59
    .line 60
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->ooo0〇〇O:Landroid/widget/TextView;

    .line 61
    .line 62
    const/high16 v1, 0x41200000    # 10.0f

    .line 63
    .line 64
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 65
    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->ooo0〇〇O:Landroid/widget/TextView;

    .line 69
    .line 70
    invoke-static {v0}, Lcom/intsig/camscanner/util/StringUtil;->oO80(Ljava/lang/String;)Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    .line 76
    .line 77
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇〇08O:Landroid/widget/TextView;

    .line 78
    .line 79
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oo8ooo8O:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 80
    .line 81
    iget-object v2, p0, Lcom/intsig/adapter/BaseViewHolder;->o0:Landroid/content/Context;

    .line 82
    .line 83
    iget-wide v3, p1, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇80〇808〇O:J

    .line 84
    .line 85
    invoke-virtual {v1, v2, v3, v4}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->O8(Landroid/content/Context;J)Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object p1

    .line 89
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    .line 91
    .line 92
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇8〇oO〇〇8o:Landroid/view/View;

    .line 93
    .line 94
    const/4 v0, 0x0

    .line 95
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 96
    .line 97
    .line 98
    goto :goto_1

    .line 99
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇8〇oO〇〇8o:Landroid/view/View;

    .line 100
    .line 101
    const/16 v0, 0x8

    .line 102
    .line 103
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 104
    .line 105
    .line 106
    :goto_1
    return-void
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private o〇O(Lcom/intsig/camscanner/pagelist/model/PageImageItem;)V
    .locals 6

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageImageItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/pagelist/model/PageItem;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageImageItem;->o〇0()Z

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oo8ooo8O:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->Oooo8o0〇()Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    const/16 v2, 0x8

    .line 16
    .line 17
    if-nez v1, :cond_3

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oo8ooo8O:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 20
    .line 21
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇〇808〇()Z

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    if-eqz v1, :cond_1

    .line 26
    .line 27
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->O0O:Landroid/view/View;

    .line 28
    .line 29
    const/4 v3, 0x0

    .line 30
    invoke-direct {p0, v1, v3}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->OOO(Landroid/view/View;I)V

    .line 31
    .line 32
    .line 33
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇O〇〇O8:Landroid/widget/TextView;

    .line 34
    .line 35
    invoke-direct {p0, v1, v2}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->OOO(Landroid/view/View;I)V

    .line 36
    .line 37
    .line 38
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o8oOOo:Landroid/widget/CheckBox;

    .line 39
    .line 40
    invoke-direct {p0, v1, v3}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->OOO(Landroid/view/View;I)V

    .line 41
    .line 42
    .line 43
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oo8ooo8O:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 44
    .line 45
    iget-wide v4, v0, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇080:J

    .line 46
    .line 47
    invoke-virtual {v1, v4, v5}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇0〇O0088o(J)Z

    .line 48
    .line 49
    .line 50
    move-result v1

    .line 51
    iget-object v2, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o8oOOo:Landroid/widget/CheckBox;

    .line 52
    .line 53
    invoke-virtual {v2, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 54
    .line 55
    .line 56
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇o0O:Landroid/view/View;

    .line 57
    .line 58
    invoke-direct {p0, v1, v3}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->OOO(Landroid/view/View;I)V

    .line 59
    .line 60
    .line 61
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇o0O:Landroid/view/View;

    .line 62
    .line 63
    if-eqz v1, :cond_2

    .line 64
    .line 65
    if-eqz p1, :cond_0

    .line 66
    .line 67
    const-string p1, "#8affffff"

    .line 68
    .line 69
    goto :goto_0

    .line 70
    :cond_0
    const-string p1, "#4c000000"

    .line 71
    .line 72
    :goto_0
    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 73
    .line 74
    .line 75
    move-result p1

    .line 76
    invoke-virtual {v1, p1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 77
    .line 78
    .line 79
    goto :goto_1

    .line 80
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇o0O:Landroid/view/View;

    .line 81
    .line 82
    invoke-direct {p0, p1, v2}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->OOO(Landroid/view/View;I)V

    .line 83
    .line 84
    .line 85
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->O0O:Landroid/view/View;

    .line 86
    .line 87
    invoke-direct {p0, p1, v2}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->OOO(Landroid/view/View;I)V

    .line 88
    .line 89
    .line 90
    :cond_2
    :goto_1
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇80(Lcom/intsig/camscanner/pagelist/model/PageItem;)V

    .line 91
    .line 92
    .line 93
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oo8ooo8O:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 94
    .line 95
    invoke-direct {p0, v0, p1}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oO(Lcom/intsig/camscanner/pagelist/model/PageItem;Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;)V

    .line 96
    .line 97
    .line 98
    goto :goto_2

    .line 99
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o8〇OO0〇0o:Landroid/widget/ImageView;

    .line 100
    .line 101
    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 102
    .line 103
    .line 104
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 105
    .line 106
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 107
    .line 108
    .line 109
    :goto_2
    return-void
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic o〇〇0〇(Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;Lcom/intsig/camscanner/pagelist/model/PageTypeItem;ILandroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇〇〇0〇〇0(Lcom/intsig/camscanner/pagelist/model/PageTypeItem;ILandroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic 〇0000OOO(Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;Lcom/intsig/camscanner/pagelist/model/PageItem;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->Oo8Oo00oo(Lcom/intsig/camscanner/pagelist/model/PageItem;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static synthetic 〇00〇8(Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/adapter/BaseViewHolder;->o0:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇80(Lcom/intsig/camscanner/pagelist/model/PageItem;)V
    .locals 8

    .line 1
    iget-object v0, p1, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇〇888:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    const/16 v2, 0x8

    .line 8
    .line 9
    if-nez v1, :cond_3

    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oo8ooo8O:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 12
    .line 13
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇〇808〇()Z

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-nez v1, :cond_3

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oo8ooo8O:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 20
    .line 21
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->oO80()[Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oo8ooo8O:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 26
    .line 27
    invoke-virtual {v3}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇〇888()[Ljava/util/regex/Pattern;

    .line 28
    .line 29
    .line 30
    move-result-object v3

    .line 31
    iget-object v4, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o8〇OO0〇0o:Landroid/widget/ImageView;

    .line 32
    .line 33
    const/4 v5, 0x0

    .line 34
    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 35
    .line 36
    .line 37
    iget-object v4, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 38
    .line 39
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 40
    .line 41
    .line 42
    iget-object v4, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o8〇OO0〇0o:Landroid/widget/ImageView;

    .line 43
    .line 44
    iget-object v6, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 45
    .line 46
    invoke-virtual {v4, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 47
    .line 48
    .line 49
    iget-object v4, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 50
    .line 51
    iget-wide v6, p1, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇080:J

    .line 52
    .line 53
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    invoke-virtual {v4, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 58
    .line 59
    .line 60
    if-eqz v1, :cond_1

    .line 61
    .line 62
    array-length p1, v1

    .line 63
    if-lez p1, :cond_1

    .line 64
    .line 65
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/StringUtil;->〇o00〇〇Oo(Ljava/lang/String;[Ljava/lang/String;)Z

    .line 66
    .line 67
    .line 68
    move-result p1

    .line 69
    if-eqz p1, :cond_0

    .line 70
    .line 71
    if-eqz v3, :cond_0

    .line 72
    .line 73
    array-length v1, v3

    .line 74
    if-lez v1, :cond_0

    .line 75
    .line 76
    iget-object v1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 77
    .line 78
    iget-object v4, p0, Lcom/intsig/adapter/BaseViewHolder;->o0:Landroid/content/Context;

    .line 79
    .line 80
    invoke-static {v0, v3, v4}, Lcom/intsig/camscanner/util/StringUtil;->〇80〇808〇O(Ljava/lang/String;[Ljava/util/regex/Pattern;Landroid/content/Context;)Landroid/text/SpannableStringBuilder;

    .line 81
    .line 82
    .line 83
    move-result-object v0

    .line 84
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    .line 86
    .line 87
    const/4 v0, 0x1

    .line 88
    iput-boolean v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o8o:Z

    .line 89
    .line 90
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oOO〇〇:Z

    .line 91
    .line 92
    if-nez v0, :cond_1

    .line 93
    .line 94
    iput-boolean p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oOO〇〇:Z

    .line 95
    .line 96
    :cond_1
    iget-boolean p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oOO〇〇:Z

    .line 97
    .line 98
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 99
    .line 100
    if-eqz p1, :cond_2

    .line 101
    .line 102
    const/4 v2, 0x0

    .line 103
    :cond_2
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 104
    .line 105
    .line 106
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o8〇OO0〇0o:Landroid/widget/ImageView;

    .line 107
    .line 108
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 109
    .line 110
    .line 111
    new-instance p1, Ljava/lang/StringBuilder;

    .line 112
    .line 113
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 114
    .line 115
    .line 116
    const-string v0, "vh.noteBtn.setSelected = "

    .line 117
    .line 118
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    .line 120
    .line 121
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oOO〇〇:Z

    .line 122
    .line 123
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 124
    .line 125
    .line 126
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 127
    .line 128
    .line 129
    move-result-object p1

    .line 130
    const-string v0, "DocListHolder"

    .line 131
    .line 132
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    .line 134
    .line 135
    goto :goto_0

    .line 136
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o8〇OO0〇0o:Landroid/widget/ImageView;

    .line 137
    .line 138
    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 139
    .line 140
    .line 141
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->OO〇00〇8oO:Landroid/widget/TextView;

    .line 142
    .line 143
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 144
    .line 145
    .line 146
    :goto_0
    return-void
    .line 147
.end method

.method private 〇8〇0〇o〇O(Lcom/intsig/camscanner/pagelist/model/PageTypeItem;I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->OO:Landroid/view/View;

    .line 2
    .line 3
    new-instance v1, L〇oO88o/O8;

    .line 4
    .line 5
    invoke-direct {v1, p0, p1, p2}, L〇oO88o/O8;-><init>(Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;Lcom/intsig/camscanner/pagelist/model/PageTypeItem;I)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->OO:Landroid/view/View;

    .line 12
    .line 13
    new-instance v1, L〇oO88o/Oo08;

    .line 14
    .line 15
    invoke-direct {v1, p0, p1, p2}, L〇oO88o/Oo08;-><init>(Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;Lcom/intsig/camscanner/pagelist/model/PageTypeItem;I)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static synthetic 〇o(Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/adapter/BaseViewHolder;->o0:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇〇〇0〇〇0(Lcom/intsig/camscanner/pagelist/model/PageTypeItem;ILandroid/view/View;)V
    .locals 2

    .line 1
    iget-object p3, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇〇o〇:Lcom/intsig/adapter/BaseRecyclerViewAdapter$OnRecyclerViewItemClickListener;

    .line 2
    .line 3
    if-eqz p3, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;->getType()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    invoke-interface {p3, v0, v1, p1, p2}, Lcom/intsig/adapter/BaseRecyclerViewAdapter$OnRecyclerViewItemClickListener;->Oo0oOo〇0(Landroid/view/View;ILjava/lang/Object;I)V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public O08000(Lcom/intsig/adapter/BaseRecyclerViewAdapter$OnRecyclerViewItemLongClickListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/adapter/BaseRecyclerViewAdapter$OnRecyclerViewItemLongClickListener<",
            "Lcom/intsig/camscanner/pagelist/model/PageTypeItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->Oo80:Lcom/intsig/adapter/BaseRecyclerViewAdapter$OnRecyclerViewItemLongClickListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public bridge synthetic O〇8O8〇008(Ljava/lang/Object;I)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    check-cast p1, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇〇0o(Lcom/intsig/camscanner/pagelist/model/PageTypeItem;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public O〇O〇oO(Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OnOcrClickListener;)V
    .locals 0
    .param p1    # Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OnOcrClickListener;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o〇oO:Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$OnOcrClickListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o8()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oOO〇〇:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o8oO〇(Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;)V
    .locals 0
    .param p1    # Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oo8ooo8O:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇8oOO88(Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇08〇o0O:Lcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected 〇00()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/adapter/BaseViewHolder;->〇00()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oOo〇8o008:Landroid/widget/ImageView;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    const/4 v1, 0x0

    .line 9
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇08O8o〇0(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->O〇o88o08〇:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇8(Lcom/intsig/adapter/BaseRecyclerViewAdapter$OnRecyclerViewItemClickListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/adapter/BaseRecyclerViewAdapter$OnRecyclerViewItemClickListener<",
            "Lcom/intsig/camscanner/pagelist/model/PageTypeItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇〇o〇:Lcom/intsig/adapter/BaseRecyclerViewAdapter$OnRecyclerViewItemClickListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇O〇80o08O(I)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇O〇〇O8:Landroid/widget/TextView;

    .line 2
    .line 3
    const/16 v1, 0x63

    .line 4
    .line 5
    if-gt p1, v1, :cond_0

    .line 6
    .line 7
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const-string v1, "99+"

    .line 13
    .line 14
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇O〇〇O8:Landroid/widget/TextView;

    .line 18
    .line 19
    const/4 v1, 0x0

    .line 20
    const/16 v2, 0x8

    .line 21
    .line 22
    if-lez p1, :cond_1

    .line 23
    .line 24
    const/4 v3, 0x0

    .line 25
    goto :goto_1

    .line 26
    :cond_1
    const/16 v3, 0x8

    .line 27
    .line 28
    :goto_1
    invoke-direct {p0, v0, v3}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->OOO(Landroid/view/View;I)V

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o8oOOo:Landroid/widget/CheckBox;

    .line 32
    .line 33
    if-gtz p1, :cond_2

    .line 34
    .line 35
    goto :goto_2

    .line 36
    :cond_2
    const/16 v1, 0x8

    .line 37
    .line 38
    :goto_2
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->OOO(Landroid/view/View;I)V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇〇0o(Lcom/intsig/camscanner/pagelist/model/PageTypeItem;I)V
    .locals 9
    .param p1    # Lcom/intsig/camscanner/pagelist/model/PageTypeItem;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    instance-of v0, p1, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 2
    .line 3
    const-string v1, "DocListHolder"

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const-string p1, "data type not correct."

    .line 8
    .line 9
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    move-object v0, p1

    .line 14
    check-cast v0, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 15
    .line 16
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageImageItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/pagelist/model/PageItem;

    .line 17
    .line 18
    .line 19
    move-result-object v2

    .line 20
    if-nez v2, :cond_1

    .line 21
    .line 22
    const-string p1, "pageItem can not be null."

    .line 23
    .line 24
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    return-void

    .line 28
    :cond_1
    const/4 v1, 0x0

    .line 29
    iput-boolean v1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o8o:Z

    .line 30
    .line 31
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇8〇0〇o〇O(Lcom/intsig/camscanner/pagelist/model/PageTypeItem;I)V

    .line 32
    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o8〇OO0〇0o:Landroid/widget/ImageView;

    .line 35
    .line 36
    iget-object p2, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->O〇08oOOO0:Landroid/view/View$OnClickListener;

    .line 37
    .line 38
    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    .line 40
    .line 41
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 42
    .line 43
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    .line 44
    .line 45
    .line 46
    move-result-object p2

    .line 47
    const/4 v3, 0x1

    .line 48
    new-array v4, v3, [Ljava/lang/Object;

    .line 49
    .line 50
    iget v5, v2, Lcom/intsig/camscanner/pagelist/model/PageItem;->o〇0:I

    .line 51
    .line 52
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 53
    .line 54
    .line 55
    move-result-object v5

    .line 56
    aput-object v5, v4, v1

    .line 57
    .line 58
    const-string v1, "%1$02d"

    .line 59
    .line 60
    invoke-static {p2, v1, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object p2

    .line 64
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    .line 66
    .line 67
    iget-object p1, v2, Lcom/intsig/camscanner/pagelist/model/PageItem;->O8:Ljava/lang/String;

    .line 68
    .line 69
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 70
    .line 71
    .line 72
    move-result p1

    .line 73
    if-nez p1, :cond_3

    .line 74
    .line 75
    iget-object p1, v2, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇o〇:Ljava/lang/String;

    .line 76
    .line 77
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 78
    .line 79
    .line 80
    move-result p1

    .line 81
    if-nez p1, :cond_3

    .line 82
    .line 83
    iget-object p1, v2, Lcom/intsig/camscanner/pagelist/model/PageItem;->Oo08:Ljava/lang/String;

    .line 84
    .line 85
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 86
    .line 87
    .line 88
    move-result p1

    .line 89
    if-eqz p1, :cond_2

    .line 90
    .line 91
    goto :goto_0

    .line 92
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇00O0:Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;

    .line 93
    .line 94
    invoke-virtual {p1}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇080()Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object p1

    .line 98
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 99
    .line 100
    .line 101
    move-result p1

    .line 102
    if-nez p1, :cond_3

    .line 103
    .line 104
    iget-object p1, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->〇00O0:Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;

    .line 105
    .line 106
    invoke-virtual {p1}, Lcom/intsig/camscanner/sharedir/data/ShareDirDBData;->〇o00〇〇Oo()I

    .line 107
    .line 108
    .line 109
    move-result p1

    .line 110
    if-ne p1, v3, :cond_3

    .line 111
    .line 112
    new-instance p1, L〇oO88o/〇o〇;

    .line 113
    .line 114
    invoke-direct {p1, p0, v2}, L〇oO88o/〇o〇;-><init>(Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;Lcom/intsig/camscanner/pagelist/model/PageItem;)V

    .line 115
    .line 116
    .line 117
    invoke-static {p1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇080(Ljava/lang/Runnable;)V

    .line 118
    .line 119
    .line 120
    :cond_3
    :goto_0
    iget-object v3, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oo8ooo8O:Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;

    .line 121
    .line 122
    iget-object v4, p0, Lcom/intsig/adapter/BaseViewHolder;->o0:Landroid/content/Context;

    .line 123
    .line 124
    iget-object v5, p0, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oOo〇8o008:Landroid/widget/ImageView;

    .line 125
    .line 126
    iget v6, v2, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇O8o08O:I

    .line 127
    .line 128
    new-instance v7, Lcom/intsig/camscanner/loadimage/BitmapPara;

    .line 129
    .line 130
    iget-object p1, v2, Lcom/intsig/camscanner/pagelist/model/PageItem;->O8:Ljava/lang/String;

    .line 131
    .line 132
    iget-object p2, v2, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇o〇:Ljava/lang/String;

    .line 133
    .line 134
    iget-object v1, v2, Lcom/intsig/camscanner/pagelist/model/PageItem;->Oo08:Ljava/lang/String;

    .line 135
    .line 136
    invoke-direct {v7, p1, p2, v1}, Lcom/intsig/camscanner/loadimage/BitmapPara;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    .line 138
    .line 139
    iget v8, v2, Lcom/intsig/camscanner/pagelist/model/PageItem;->Oooo8o0〇:I

    .line 140
    .line 141
    invoke-virtual/range {v3 .. v8}, Lcom/intsig/camscanner/pagelist/model/PageListBaseItem;->〇080(Landroid/content/Context;Landroid/widget/ImageView;ILcom/intsig/camscanner/loadimage/BitmapPara;I)V

    .line 142
    .line 143
    .line 144
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->Ooo(Lcom/intsig/camscanner/pagelist/model/PageItem;)V

    .line 145
    .line 146
    .line 147
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o〇8(Lcom/intsig/camscanner/pagelist/model/PageItem;)V

    .line 148
    .line 149
    .line 150
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o0ooO(Lcom/intsig/camscanner/pagelist/model/PageItem;)V

    .line 151
    .line 152
    .line 153
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->oO00OOO(Lcom/intsig/camscanner/pagelist/model/PageItem;)V

    .line 154
    .line 155
    .line 156
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->O000(Lcom/intsig/camscanner/pagelist/model/PageItem;)V

    .line 157
    .line 158
    .line 159
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/pagelist/adapter/DocumentListAdapter$PageImageHolder;->o〇O(Lcom/intsig/camscanner/pagelist/model/PageImageItem;)V

    .line 160
    .line 161
    .line 162
    return-void
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

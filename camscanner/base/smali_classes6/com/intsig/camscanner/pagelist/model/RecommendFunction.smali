.class public final enum Lcom/intsig/camscanner/pagelist/model/RecommendFunction;
.super Ljava/lang/Enum;
.source "FunctionItem.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/camscanner/pagelist/model/RecommendFunction;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

.field public static final enum COMPOSITE:Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

.field public static final enum COMPOSITE_LONG_IMG:Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

.field public static final enum CREATE_QUESTION_BOOK:Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

.field public static final enum MARK:Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

.field public static final enum OCR:Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

.field public static final enum SIGNATURE:Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

.field public static final enum TO_WORD:Lcom/intsig/camscanner/pagelist/model/RecommendFunction;


# instance fields
.field private final desId:I

.field private final iconId:I


# direct methods
.method private static final synthetic $values()[Lcom/intsig/camscanner/pagelist/model/RecommendFunction;
    .locals 3

    .line 1
    const/4 v0, 0x7

    .line 2
    new-array v0, v0, [Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    sget-object v2, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;->OCR:Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 6
    .line 7
    aput-object v2, v0, v1

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    sget-object v2, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;->COMPOSITE:Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 11
    .line 12
    aput-object v2, v0, v1

    .line 13
    .line 14
    const/4 v1, 0x2

    .line 15
    sget-object v2, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;->TO_WORD:Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 16
    .line 17
    aput-object v2, v0, v1

    .line 18
    .line 19
    const/4 v1, 0x3

    .line 20
    sget-object v2, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;->COMPOSITE_LONG_IMG:Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 21
    .line 22
    aput-object v2, v0, v1

    .line 23
    .line 24
    const/4 v1, 0x4

    .line 25
    sget-object v2, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;->SIGNATURE:Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 26
    .line 27
    aput-object v2, v0, v1

    .line 28
    .line 29
    const/4 v1, 0x5

    .line 30
    sget-object v2, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;->CREATE_QUESTION_BOOK:Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 31
    .line 32
    aput-object v2, v0, v1

    .line 33
    .line 34
    const/4 v1, 0x6

    .line 35
    sget-object v2, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;->MARK:Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 36
    .line 37
    aput-object v2, v0, v1

    .line 38
    .line 39
    return-object v0
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static constructor <clinit>()V
    .locals 5

    .line 1
    new-instance v0, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 2
    .line 3
    const v1, 0x7f130c64

    .line 4
    .line 5
    .line 6
    const v2, 0x7f080a4b

    .line 7
    .line 8
    .line 9
    const-string v3, "OCR"

    .line 10
    .line 11
    const/4 v4, 0x0

    .line 12
    invoke-direct {v0, v3, v4, v1, v2}, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;-><init>(Ljava/lang/String;III)V

    .line 13
    .line 14
    .line 15
    sput-object v0, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;->OCR:Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 16
    .line 17
    new-instance v0, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 18
    .line 19
    const v1, 0x7f130113

    .line 20
    .line 21
    .line 22
    const v2, 0x7f080cf1

    .line 23
    .line 24
    .line 25
    const-string v3, "COMPOSITE"

    .line 26
    .line 27
    const/4 v4, 0x1

    .line 28
    invoke-direct {v0, v3, v4, v1, v2}, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;-><init>(Ljava/lang/String;III)V

    .line 29
    .line 30
    .line 31
    sput-object v0, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;->COMPOSITE:Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 32
    .line 33
    new-instance v0, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 34
    .line 35
    const v1, 0x7f1313b0

    .line 36
    .line 37
    .line 38
    const v2, 0x7f080ce7

    .line 39
    .line 40
    .line 41
    const-string v3, "TO_WORD"

    .line 42
    .line 43
    const/4 v4, 0x2

    .line 44
    invoke-direct {v0, v3, v4, v1, v2}, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;-><init>(Ljava/lang/String;III)V

    .line 45
    .line 46
    .line 47
    sput-object v0, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;->TO_WORD:Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 48
    .line 49
    new-instance v0, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 50
    .line 51
    const v1, 0x7f131a3b

    .line 52
    .line 53
    .line 54
    const v2, 0x7f080e5a

    .line 55
    .line 56
    .line 57
    const-string v3, "COMPOSITE_LONG_IMG"

    .line 58
    .line 59
    const/4 v4, 0x3

    .line 60
    invoke-direct {v0, v3, v4, v1, v2}, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;-><init>(Ljava/lang/String;III)V

    .line 61
    .line 62
    .line 63
    sput-object v0, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;->COMPOSITE_LONG_IMG:Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 64
    .line 65
    new-instance v0, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 66
    .line 67
    const v1, 0x7f1313e0

    .line 68
    .line 69
    .line 70
    const v2, 0x7f080c81

    .line 71
    .line 72
    .line 73
    const-string v3, "SIGNATURE"

    .line 74
    .line 75
    const/4 v4, 0x4

    .line 76
    invoke-direct {v0, v3, v4, v1, v2}, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;-><init>(Ljava/lang/String;III)V

    .line 77
    .line 78
    .line 79
    sput-object v0, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;->SIGNATURE:Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 80
    .line 81
    new-instance v0, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 82
    .line 83
    const v1, 0x7f130c96

    .line 84
    .line 85
    .line 86
    const v2, 0x7f080e28

    .line 87
    .line 88
    .line 89
    const-string v3, "CREATE_QUESTION_BOOK"

    .line 90
    .line 91
    const/4 v4, 0x5

    .line 92
    invoke-direct {v0, v3, v4, v1, v2}, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;-><init>(Ljava/lang/String;III)V

    .line 93
    .line 94
    .line 95
    sput-object v0, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;->CREATE_QUESTION_BOOK:Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 96
    .line 97
    new-instance v0, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 98
    .line 99
    const v1, 0x7f1300c5

    .line 100
    .line 101
    .line 102
    const v2, 0x7f08099b

    .line 103
    .line 104
    .line 105
    const-string v3, "MARK"

    .line 106
    .line 107
    const/4 v4, 0x6

    .line 108
    invoke-direct {v0, v3, v4, v1, v2}, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;-><init>(Ljava/lang/String;III)V

    .line 109
    .line 110
    .line 111
    sput-object v0, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;->MARK:Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 112
    .line 113
    invoke-static {}, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;->$values()[Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 114
    .line 115
    .line 116
    move-result-object v0

    .line 117
    sput-object v0, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;->$VALUES:[Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 118
    .line 119
    return-void
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput p3, p0, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;->desId:I

    .line 5
    .line 6
    iput p4, p0, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;->iconId:I

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/camscanner/pagelist/model/RecommendFunction;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static values()[Lcom/intsig/camscanner/pagelist/model/RecommendFunction;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;->$VALUES:[Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 2
    .line 3
    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/camscanner/pagelist/model/RecommendFunction;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final getDesId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;->desId:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getIconId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/pagelist/model/RecommendFunction;->iconId:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

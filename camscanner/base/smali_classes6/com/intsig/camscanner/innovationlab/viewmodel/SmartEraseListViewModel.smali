.class public final Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;
.super Lcom/intsig/camscanner/innovationlab/BaseInnovationViewModel;
.source "SmartEraseListViewModel.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final O0O:Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final o8〇OO0〇0o:Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final ooo0〇〇O:Lcom/intsig/camscanner/innovationlab/data/InnoLabTwoTabItem;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇8〇oO〇〇8o:Lcom/intsig/camscanner/innovationlab/data/InnoLabTwoTabItem;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇〇08O:Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Landroidx/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final OO〇00〇8oO:Lcom/intsig/camscanner/pagelist/newpagelist/data/PageListRepository;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oOo0:Lcom/intsig/camscanner/innovationlab/repo/InnoLabRepo;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oOo〇8o008:Lkotlinx/coroutines/flow/Flow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/flow/Flow<",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:Z

.field private 〇080OO8〇0:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08O〇00〇o:Landroid/app/Application;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇0O:Lkotlinx/coroutines/channels/Channel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlinx/coroutines/channels/Channel<",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .line 1
    new-instance v0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->o8〇OO0〇0o:Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel$Companion;

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/camscanner/innovationlab/data/InnoLabTwoTabItem;

    .line 10
    .line 11
    sget-object v2, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->O8:Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem$Companion;

    .line 12
    .line 13
    invoke-virtual {v2}, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem$Companion;->〇o00〇〇Oo()Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;

    .line 14
    .line 15
    .line 16
    move-result-object v3

    .line 17
    invoke-virtual {v2}, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem$Companion;->〇080()Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;

    .line 18
    .line 19
    .line 20
    move-result-object v4

    .line 21
    invoke-direct {v0, v3, v4}, Lcom/intsig/camscanner/innovationlab/data/InnoLabTwoTabItem;-><init>(Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;)V

    .line 22
    .line 23
    .line 24
    sput-object v0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/innovationlab/data/InnoLabTwoTabItem;

    .line 25
    .line 26
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->〇80〇808〇O()Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    const/4 v3, 0x1

    .line 31
    if-ne v0, v3, :cond_0

    .line 32
    .line 33
    new-instance v0, Lcom/intsig/camscanner/innovationlab/data/InnoLabTwoTabItem;

    .line 34
    .line 35
    invoke-virtual {v2}, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem$Companion;->〇o〇()Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;

    .line 36
    .line 37
    .line 38
    move-result-object v2

    .line 39
    invoke-direct {v0, v2, v1}, Lcom/intsig/camscanner/innovationlab/data/InnoLabTwoTabItem;-><init>(Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;)V

    .line 40
    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_0
    if-nez v0, :cond_1

    .line 44
    .line 45
    new-instance v0, Lcom/intsig/camscanner/innovationlab/data/InnoLabTwoTabItem;

    .line 46
    .line 47
    invoke-virtual {v2}, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem$Companion;->O8()Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    invoke-virtual {v2}, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem$Companion;->〇o〇()Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;

    .line 52
    .line 53
    .line 54
    move-result-object v2

    .line 55
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/innovationlab/data/InnoLabTwoTabItem;-><init>(Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;)V

    .line 56
    .line 57
    .line 58
    :goto_0
    sput-object v0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->ooo0〇〇O:Lcom/intsig/camscanner/innovationlab/data/InnoLabTwoTabItem;

    .line 59
    .line 60
    new-instance v0, Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;

    .line 61
    .line 62
    invoke-direct {v0, v3}, Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;-><init>(I)V

    .line 63
    .line 64
    .line 65
    sput-object v0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇〇08O:Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;

    .line 66
    .line 67
    new-instance v0, Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;

    .line 68
    .line 69
    const/4 v1, 0x3

    .line 70
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;-><init>(I)V

    .line 71
    .line 72
    .line 73
    sput-object v0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->O0O:Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;

    .line 74
    .line 75
    return-void

    .line 76
    :cond_1
    new-instance v0, Lkotlin/NoWhenBranchMatchedException;

    .line 77
    .line 78
    invoke-direct {v0}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 79
    .line 80
    .line 81
    throw v0
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public constructor <init>(Landroid/app/Application;)V
    .locals 3
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "app"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "SmartEraseListViewModel"

    .line 7
    .line 8
    invoke-direct {p0, v0, p1}, Lcom/intsig/camscanner/innovationlab/BaseInnovationViewModel;-><init>(Ljava/lang/String;Landroid/app/Application;)V

    .line 9
    .line 10
    .line 11
    iput-object p1, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇08O〇00〇o:Landroid/app/Application;

    .line 12
    .line 13
    new-instance v0, Landroidx/lifecycle/MutableLiveData;

    .line 14
    .line 15
    invoke-direct {v0}, Landroidx/lifecycle/MutableLiveData;-><init>()V

    .line 16
    .line 17
    .line 18
    iput-object v0, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->O8o08O8O:Landroidx/lifecycle/MutableLiveData;

    .line 19
    .line 20
    new-instance v0, Ljava/util/ArrayList;

    .line 21
    .line 22
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 23
    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 26
    .line 27
    const/4 v0, 0x0

    .line 28
    const/4 v1, 0x6

    .line 29
    const/4 v2, -0x1

    .line 30
    invoke-static {v2, v0, v0, v1, v0}, Lkotlinx/coroutines/channels/ChannelKt;->〇o00〇〇Oo(ILkotlinx/coroutines/channels/BufferOverflow;Lkotlin/jvm/functions/Function1;ILjava/lang/Object;)Lkotlinx/coroutines/channels/Channel;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    iput-object v0, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇0O:Lkotlinx/coroutines/channels/Channel;

    .line 35
    .line 36
    invoke-static {v0}, Lkotlinx/coroutines/flow/FlowKt;->〇oo〇(Lkotlinx/coroutines/channels/ReceiveChannel;)Lkotlinx/coroutines/flow/Flow;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    iput-object v0, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->oOo〇8o008:Lkotlinx/coroutines/flow/Flow;

    .line 41
    .line 42
    new-instance v0, Lcom/intsig/camscanner/innovationlab/repo/InnoLabRepo;

    .line 43
    .line 44
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/innovationlab/repo/InnoLabRepo;-><init>(Landroid/app/Application;)V

    .line 45
    .line 46
    .line 47
    iput-object v0, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->oOo0:Lcom/intsig/camscanner/innovationlab/repo/InnoLabRepo;

    .line 48
    .line 49
    new-instance p1, Lcom/intsig/camscanner/pagelist/newpagelist/data/PageListRepository;

    .line 50
    .line 51
    invoke-direct {p1}, Lcom/intsig/camscanner/pagelist/newpagelist/data/PageListRepository;-><init>()V

    .line 52
    .line 53
    .line 54
    iput-object p1, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->OO〇00〇8oO:Lcom/intsig/camscanner/pagelist/newpagelist/data/PageListRepository;

    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic O8oOo80(Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;ZLjava/lang/Integer;ILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p3, p3, 0x2

    .line 2
    .line 3
    if-eqz p3, :cond_0

    .line 4
    .line 5
    const/4 p2, 0x0

    .line 6
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->Oo0oOo〇0(ZLjava/lang/Integer;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method public static final synthetic O8ooOoo〇()Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->O0O:Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic O8〇o(Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;)Lkotlinx/coroutines/channels/Channel;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇0O:Lkotlinx/coroutines/channels/Channel;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic oo〇()Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇〇08O:Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇00()Lcom/intsig/camscanner/innovationlab/data/InnoLabTwoTabItem;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->ooo0〇〇O:Lcom/intsig/camscanner/innovationlab/data/InnoLabTwoTabItem;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇oo〇()Lcom/intsig/camscanner/innovationlab/data/InnoLabTwoTabItem;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/innovationlab/data/InnoLabTwoTabItem;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final Oo0oOo〇0(ZLjava/lang/Integer;)V
    .locals 6

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "isChangeEdit = "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    const-string v1, "\tposition = "

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const-string v1, "SmartEraseListViewModel"

    .line 27
    .line 28
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    iget-boolean v0, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->o〇00O:Z

    .line 32
    .line 33
    if-ne v0, p1, :cond_0

    .line 34
    .line 35
    const-string p1, "current mode is already what you want"

    .line 36
    .line 37
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    return-void

    .line 41
    :cond_0
    iput-boolean p1, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->o〇00O:Z

    .line 42
    .line 43
    iget-object p1, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 44
    .line 45
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    const/4 v0, 0x0

    .line 50
    const/4 v1, 0x0

    .line 51
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 52
    .line 53
    .line 54
    move-result v2

    .line 55
    if-eqz v2, :cond_5

    .line 56
    .line 57
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 58
    .line 59
    .line 60
    move-result-object v2

    .line 61
    add-int/lit8 v3, v1, 0x1

    .line 62
    .line 63
    if-gez v1, :cond_1

    .line 64
    .line 65
    invoke-static {}, Lkotlin/collections/CollectionsKt;->〇〇8O0〇8()V

    .line 66
    .line 67
    .line 68
    :cond_1
    check-cast v2, Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;

    .line 69
    .line 70
    invoke-virtual {v2}, Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;->〇080()I

    .line 71
    .line 72
    .line 73
    move-result v4

    .line 74
    const/4 v5, 0x2

    .line 75
    if-ne v4, v5, :cond_4

    .line 76
    .line 77
    const-string v4, "null cannot be cast to non-null type com.intsig.camscanner.innovationlab.data.InnoLabLinearItem"

    .line 78
    .line 79
    invoke-static {v2, v4}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    .line 81
    .line 82
    check-cast v2, Lcom/intsig/camscanner/innovationlab/data/InnoLabLinearItem;

    .line 83
    .line 84
    if-eqz p2, :cond_3

    .line 85
    .line 86
    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    .line 87
    .line 88
    .line 89
    move-result v4

    .line 90
    if-ne v4, v1, :cond_2

    .line 91
    .line 92
    const/4 v1, 0x1

    .line 93
    goto :goto_1

    .line 94
    :cond_2
    const/4 v1, 0x0

    .line 95
    :goto_1
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/innovationlab/data/InnoLabLinearItem;->Oo08(Z)V

    .line 96
    .line 97
    .line 98
    sget-object v1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 99
    .line 100
    goto :goto_2

    .line 101
    :cond_3
    const/4 v1, 0x0

    .line 102
    :goto_2
    if-nez v1, :cond_4

    .line 103
    .line 104
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/innovationlab/data/InnoLabLinearItem;->Oo08(Z)V

    .line 105
    .line 106
    .line 107
    :cond_4
    move v1, v3

    .line 108
    goto :goto_0

    .line 109
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->O8o08O8O:Landroidx/lifecycle/MutableLiveData;

    .line 110
    .line 111
    iget-boolean p2, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->o〇00O:Z

    .line 112
    .line 113
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 114
    .line 115
    .line 116
    move-result-object p2

    .line 117
    invoke-virtual {p1, p2}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 118
    .line 119
    .line 120
    iget-object p1, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇0O:Lkotlinx/coroutines/channels/Channel;

    .line 121
    .line 122
    iget-object p2, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 123
    .line 124
    invoke-interface {p1, p2}, Lkotlinx/coroutines/channels/SendChannel;->〇o〇(Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    .line 126
    .line 127
    return-void
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public final O〇8oOo8O()I
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    if-eqz v2, :cond_1

    .line 13
    .line 14
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object v2

    .line 18
    check-cast v2, Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;

    .line 19
    .line 20
    invoke-virtual {v2}, Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;->〇080()I

    .line 21
    .line 22
    .line 23
    move-result v3

    .line 24
    const/4 v4, 0x2

    .line 25
    if-ne v3, v4, :cond_0

    .line 26
    .line 27
    const-string v3, "null cannot be cast to non-null type com.intsig.camscanner.innovationlab.data.InnoLabLinearItem"

    .line 28
    .line 29
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    check-cast v2, Lcom/intsig/camscanner/innovationlab/data/InnoLabLinearItem;

    .line 33
    .line 34
    invoke-virtual {v2}, Lcom/intsig/camscanner/innovationlab/data/InnoLabLinearItem;->O8()Z

    .line 35
    .line 36
    .line 37
    move-result v2

    .line 38
    if-eqz v2, :cond_0

    .line 39
    .line 40
    add-int/lit8 v1, v1, 0x1

    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_1
    return v1
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final O〇O〇oO()Landroidx/lifecycle/MutableLiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroidx/lifecycle/MutableLiveData<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->O8o08O8O:Landroidx/lifecycle/MutableLiveData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o0ooO()V
    .locals 9

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 7
    .line 8
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    if-eqz v2, :cond_1

    .line 17
    .line 18
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    check-cast v2, Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;

    .line 23
    .line 24
    invoke-virtual {v2}, Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;->〇080()I

    .line 25
    .line 26
    .line 27
    move-result v3

    .line 28
    const/4 v4, 0x2

    .line 29
    if-ne v3, v4, :cond_0

    .line 30
    .line 31
    const-string v3, "null cannot be cast to non-null type com.intsig.camscanner.innovationlab.data.InnoLabLinearItem"

    .line 32
    .line 33
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    check-cast v2, Lcom/intsig/camscanner/innovationlab/data/InnoLabLinearItem;

    .line 37
    .line 38
    invoke-virtual {v2}, Lcom/intsig/camscanner/innovationlab/data/InnoLabLinearItem;->O8()Z

    .line 39
    .line 40
    .line 41
    move-result v3

    .line 42
    if-eqz v3, :cond_0

    .line 43
    .line 44
    invoke-virtual {v2}, Lcom/intsig/camscanner/innovationlab/data/InnoLabLinearItem;->〇o〇()Lcom/intsig/camscanner/innovationlab/data/InnoLabDataItem;

    .line 45
    .line 46
    .line 47
    move-result-object v2

    .line 48
    invoke-virtual {v2}, Lcom/intsig/camscanner/innovationlab/data/InnoLabDataItem;->getDocId()J

    .line 49
    .line 50
    .line 51
    move-result-wide v2

    .line 52
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 53
    .line 54
    .line 55
    move-result-object v2

    .line 56
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_1
    invoke-static {p0}, Landroidx/lifecycle/ViewModelKt;->getViewModelScope(Landroidx/lifecycle/ViewModel;)Lkotlinx/coroutines/CoroutineScope;

    .line 61
    .line 62
    .line 63
    move-result-object v3

    .line 64
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 65
    .line 66
    .line 67
    move-result-object v4

    .line 68
    const/4 v5, 0x0

    .line 69
    new-instance v6, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel$deleteSelectedItems$2;

    .line 70
    .line 71
    const/4 v1, 0x0

    .line 72
    invoke-direct {v6, p0, v0, v1}, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel$deleteSelectedItems$2;-><init>(Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;Ljava/util/ArrayList;Lkotlin/coroutines/Continuation;)V

    .line 73
    .line 74
    .line 75
    const/4 v7, 0x2

    .line 76
    const/4 v8, 0x0

    .line 77
    invoke-static/range {v3 .. v8}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 78
    .line 79
    .line 80
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public final o8o〇〇0O(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 3
    .param p3    # Lkotlin/coroutines/Continuation;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel$queryImageListByDocId$2;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-direct {v1, p0, p1, p2, v2}, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel$queryImageListByDocId$2;-><init>(Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;JLkotlin/coroutines/Continuation;)V

    .line 9
    .line 10
    .line 11
    invoke-static {v0, v1, p3}, Lkotlinx/coroutines/BuildersKt;->Oo08(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    return-object p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final oO()Lkotlinx/coroutines/flow/Flow;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/flow/Flow<",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;",
            ">;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->oOo〇8o008:Lkotlinx/coroutines/flow/Flow;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final ooo8o〇o〇(JLjava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 8
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lkotlin/coroutines/Continuation;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v7, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel$updateDoc$2;

    .line 6
    .line 7
    const/4 v6, 0x0

    .line 8
    move-object v1, v7

    .line 9
    move-object v2, p0

    .line 10
    move-wide v3, p1

    .line 11
    move-object v5, p3

    .line 12
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel$updateDoc$2;-><init>(Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;JLjava/lang/String;Lkotlin/coroutines/Continuation;)V

    .line 13
    .line 14
    .line 15
    invoke-static {v0, v7, p4}, Lkotlinx/coroutines/BuildersKt;->Oo08(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    return-object p1
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public final o〇0OOo〇0()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇8oOO88(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 3
    .param p1    # Lkotlin/coroutines/Continuation;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel$getRenamedDocTitle$2;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-direct {v1, p0, v2}, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel$getRenamedDocTitle$2;-><init>(Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;Lkotlin/coroutines/Continuation;)V

    .line 9
    .line 10
    .line 11
    invoke-static {v0, v1, p1}, Lkotlinx/coroutines/BuildersKt;->Oo08(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    return-object p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final o〇O()Lcom/intsig/camscanner/innovationlab/repo/InnoLabRepo;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->oOo0:Lcom/intsig/camscanner/innovationlab/repo/InnoLabRepo;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇〇0〇88(I)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/4 v2, 0x1

    .line 5
    if-ltz p1, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 8
    .line 9
    .line 10
    move-result v3

    .line 11
    if-ge p1, v3, :cond_0

    .line 12
    .line 13
    const/4 v3, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v3, 0x0

    .line 16
    :goto_0
    if-nez v3, :cond_1

    .line 17
    .line 18
    return-void

    .line 19
    :cond_1
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    const-string v3, "this[position]"

    .line 24
    .line 25
    invoke-static {p1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    check-cast p1, Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;

    .line 29
    .line 30
    invoke-virtual {p1}, Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;->〇080()I

    .line 31
    .line 32
    .line 33
    move-result v3

    .line 34
    const-string v4, "null cannot be cast to non-null type com.intsig.camscanner.innovationlab.data.InnoLabLinearItem"

    .line 35
    .line 36
    const/4 v5, 0x2

    .line 37
    if-ne v3, v5, :cond_2

    .line 38
    .line 39
    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    check-cast p1, Lcom/intsig/camscanner/innovationlab/data/InnoLabLinearItem;

    .line 43
    .line 44
    invoke-virtual {p1}, Lcom/intsig/camscanner/innovationlab/data/InnoLabLinearItem;->O8()Z

    .line 45
    .line 46
    .line 47
    move-result v3

    .line 48
    xor-int/2addr v3, v2

    .line 49
    invoke-virtual {p1, v3}, Lcom/intsig/camscanner/innovationlab/data/InnoLabLinearItem;->Oo08(Z)V

    .line 50
    .line 51
    .line 52
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇0O:Lkotlinx/coroutines/channels/Channel;

    .line 53
    .line 54
    invoke-interface {p1, v0}, Lkotlinx/coroutines/channels/SendChannel;->〇o〇(Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    .line 56
    .line 57
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    const/4 v0, 0x1

    .line 62
    :cond_3
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 63
    .line 64
    .line 65
    move-result v3

    .line 66
    if-eqz v3, :cond_4

    .line 67
    .line 68
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 69
    .line 70
    .line 71
    move-result-object v3

    .line 72
    check-cast v3, Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;

    .line 73
    .line 74
    invoke-virtual {v3}, Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;->〇080()I

    .line 75
    .line 76
    .line 77
    move-result v6

    .line 78
    if-ne v6, v5, :cond_3

    .line 79
    .line 80
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    check-cast v3, Lcom/intsig/camscanner/innovationlab/data/InnoLabLinearItem;

    .line 84
    .line 85
    invoke-virtual {v3}, Lcom/intsig/camscanner/innovationlab/data/InnoLabLinearItem;->O8()Z

    .line 86
    .line 87
    .line 88
    move-result v3

    .line 89
    if-eqz v3, :cond_3

    .line 90
    .line 91
    const/4 v0, 0x0

    .line 92
    goto :goto_1

    .line 93
    :cond_4
    xor-int/lit8 p1, v0, 0x1

    .line 94
    .line 95
    iput-boolean p1, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->o〇00O:Z

    .line 96
    .line 97
    iget-object v0, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->O8o08O8O:Landroidx/lifecycle/MutableLiveData;

    .line 98
    .line 99
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 100
    .line 101
    .line 102
    move-result-object p1

    .line 103
    invoke-virtual {v0, p1}, Landroidx/lifecycle/MutableLiveData;->postValue(Ljava/lang/Object;)V

    .line 104
    .line 105
    .line 106
    return-void
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public final 〇8o8O〇O()V
    .locals 6

    .line 1
    invoke-static {p0}, Landroidx/lifecycle/ViewModelKt;->getViewModelScope(Landroidx/lifecycle/ViewModel;)Lkotlinx/coroutines/CoroutineScope;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    const/4 v2, 0x0

    .line 10
    new-instance v3, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel$queryDocs$1;

    .line 11
    .line 12
    const/4 v4, 0x0

    .line 13
    invoke-direct {v3, p0, v4}, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel$queryDocs$1;-><init>(Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;Lkotlin/coroutines/Continuation;)V

    .line 14
    .line 15
    .line 16
    const/4 v4, 0x2

    .line 17
    const/4 v5, 0x0

    .line 18
    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 19
    .line 20
    .line 21
    return-void
.end method

.method public final 〇8o〇〇8080()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->o〇00O:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇oo()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 7
    .line 8
    sget-object v1, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/innovationlab/data/InnoLabTwoTabItem;

    .line 9
    .line 10
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 14
    .line 15
    sget-object v1, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->ooo0〇〇O:Lcom/intsig/camscanner/innovationlab/data/InnoLabTwoTabItem;

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 18
    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 21
    .line 22
    sget-object v1, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇〇08O:Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;

    .line 23
    .line 24
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 28
    .line 29
    sget-object v1, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->O0O:Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;

    .line 30
    .line 31
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 32
    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇0O:Lkotlinx/coroutines/channels/Channel;

    .line 35
    .line 36
    iget-object v1, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇080OO8〇0:Ljava/util/ArrayList;

    .line 37
    .line 38
    invoke-interface {v0, v1}, Lkotlinx/coroutines/channels/SendChannel;->〇o〇(Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇〇〇0〇〇0()Landroid/app/Application;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/innovationlab/viewmodel/SmartEraseListViewModel;->〇08O〇00〇o:Landroid/app/Application;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

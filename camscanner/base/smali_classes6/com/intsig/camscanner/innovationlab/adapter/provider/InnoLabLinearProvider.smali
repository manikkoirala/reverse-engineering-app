.class public final Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider;
.super Lcom/chad/library/adapter/base/provider/BaseItemProvider;
.source "InnoLabLinearProvider.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider$InnoLabLinearHolder;,
        Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/provider/BaseItemProvider<",
        "Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080OO8〇0:Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇0O:Landroid/graphics/drawable/GradientDrawable;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8o08O8O:I

.field private final o〇00O:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider;->〇080OO8〇0:Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider$Companion;

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 10
    .line 11
    invoke-direct {v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;-><init>()V

    .line 12
    .line 13
    .line 14
    const/4 v1, 0x4

    .line 15
    invoke-static {v1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O888o0o(F)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    const v1, 0x7f0601ee

    .line 24
    .line 25
    .line 26
    const v2, 0x3dcccccd    # 0.1f

    .line 27
    .line 28
    .line 29
    invoke-static {v1, v2}, Lcom/intsig/utils/ColorUtil;->〇o〇(IF)I

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O00(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    invoke-virtual {v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->OoO8()Landroid/graphics/drawable/GradientDrawable;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    const-string v1, "Builder()\n            .c\u20261F))\n            .build()"

    .line 42
    .line 43
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    sput-object v0, Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider;->〇0O:Landroid/graphics/drawable/GradientDrawable;

    .line 47
    .line 48
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x2

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider;->o〇00O:I

    .line 6
    .line 7
    const v0, 0x7f0d041c

    .line 8
    .line 9
    .line 10
    iput v0, p0, Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider;->O8o08O8O:I

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
    .locals 1
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string p2, "parent"

    .line 2
    .line 3
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance p2, Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider$InnoLabLinearHolder;

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider;->oO80()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-static {p1, v0}, Lcom/chad/library/adapter/base/util/AdapterUtilsKt;->〇080(Landroid/view/ViewGroup;I)Landroid/view/View;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    invoke-direct {p2, p0, p1}, Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider$InnoLabLinearHolder;-><init>(Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider;Landroid/view/View;)V

    .line 17
    .line 18
    .line 19
    return-object p2
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public o800o8O(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;)V
    .locals 2
    .param p1    # Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "helper"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "item"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    check-cast p1, Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider$InnoLabLinearHolder;

    .line 12
    .line 13
    check-cast p2, Lcom/intsig/camscanner/innovationlab/data/InnoLabLinearItem;

    .line 14
    .line 15
    invoke-virtual {p1}, Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider$InnoLabLinearHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemInnoLabLinearBinding;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemInnoLabLinearBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 20
    .line 21
    invoke-virtual {p2}, Lcom/intsig/camscanner/innovationlab/data/InnoLabLinearItem;->〇o00〇〇Oo()I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p1}, Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider$InnoLabLinearHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemInnoLabLinearBinding;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemInnoLabLinearBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 33
    .line 34
    invoke-virtual {p2}, Lcom/intsig/camscanner/innovationlab/data/InnoLabLinearItem;->〇o〇()Lcom/intsig/camscanner/innovationlab/data/InnoLabDataItem;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-virtual {v1}, Lcom/intsig/camscanner/innovationlab/data/InnoLabDataItem;->getDocTitle()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    if-eqz v1, :cond_0

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_0
    const-string v1, ""

    .line 46
    .line 47
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    .line 49
    .line 50
    invoke-virtual {p2}, Lcom/intsig/camscanner/innovationlab/data/InnoLabLinearItem;->O8()Z

    .line 51
    .line 52
    .line 53
    move-result p2

    .line 54
    const-string v0, "helper.mBinding.vBgBlueCover"

    .line 55
    .line 56
    const/4 v1, 0x1

    .line 57
    if-ne p2, v1, :cond_1

    .line 58
    .line 59
    invoke-virtual {p1}, Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider$InnoLabLinearHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemInnoLabLinearBinding;

    .line 60
    .line 61
    .line 62
    move-result-object p2

    .line 63
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ItemInnoLabLinearBinding;->〇OOo8〇0:Landroid/widget/CheckBox;

    .line 64
    .line 65
    invoke-virtual {p2, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 66
    .line 67
    .line 68
    invoke-virtual {p1}, Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider$InnoLabLinearHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemInnoLabLinearBinding;

    .line 69
    .line 70
    .line 71
    move-result-object p2

    .line 72
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ItemInnoLabLinearBinding;->〇080OO8〇0:Landroid/view/View;

    .line 73
    .line 74
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    invoke-static {p2, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 78
    .line 79
    .line 80
    invoke-virtual {p1}, Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider$InnoLabLinearHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemInnoLabLinearBinding;

    .line 81
    .line 82
    .line 83
    move-result-object p1

    .line 84
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemInnoLabLinearBinding;->〇080OO8〇0:Landroid/view/View;

    .line 85
    .line 86
    sget-object p2, Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider;->〇0O:Landroid/graphics/drawable/GradientDrawable;

    .line 87
    .line 88
    invoke-virtual {p1, p2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 89
    .line 90
    .line 91
    goto :goto_1

    .line 92
    :cond_1
    if-nez p2, :cond_2

    .line 93
    .line 94
    invoke-virtual {p1}, Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider$InnoLabLinearHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemInnoLabLinearBinding;

    .line 95
    .line 96
    .line 97
    move-result-object p2

    .line 98
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ItemInnoLabLinearBinding;->〇OOo8〇0:Landroid/widget/CheckBox;

    .line 99
    .line 100
    const/4 v1, 0x0

    .line 101
    invoke-virtual {p2, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 102
    .line 103
    .line 104
    invoke-virtual {p1}, Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider$InnoLabLinearHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemInnoLabLinearBinding;

    .line 105
    .line 106
    .line 107
    move-result-object p1

    .line 108
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemInnoLabLinearBinding;->〇080OO8〇0:Landroid/view/View;

    .line 109
    .line 110
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 111
    .line 112
    .line 113
    invoke-static {p1, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 114
    .line 115
    .line 116
    :cond_2
    :goto_1
    return-void
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public oO80()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider;->O8o08O8O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic 〇080(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider;->o800o8O(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/innovationlab/data/BaseInnoLabItem;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/innovationlab/adapter/provider/InnoLabLinearProvider;->o〇00O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

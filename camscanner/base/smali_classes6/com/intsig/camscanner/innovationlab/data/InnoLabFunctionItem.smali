.class public final Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;
.super Ljava/lang/Object;
.source "InnoLabFunctionItem.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final O8:Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final Oo08:Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final oO80:Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final o〇0:Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇〇888:Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final 〇080:I

.field private final 〇o00〇〇Oo:I

.field private final 〇o〇:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->O8:Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem$Companion;

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;

    .line 10
    .line 11
    const v1, 0x7f080cc6

    .line 12
    .line 13
    .line 14
    const v2, 0x7f1300e6

    .line 15
    .line 16
    .line 17
    const/4 v3, 0x0

    .line 18
    invoke-direct {v0, v3, v1, v2}, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;-><init>(III)V

    .line 19
    .line 20
    .line 21
    sput-object v0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->Oo08:Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;

    .line 22
    .line 23
    new-instance v0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;

    .line 24
    .line 25
    const v1, 0x7f080819

    .line 26
    .line 27
    .line 28
    const v2, 0x7f1310fe

    .line 29
    .line 30
    .line 31
    const/4 v3, 0x1

    .line 32
    invoke-direct {v0, v3, v1, v2}, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;-><init>(III)V

    .line 33
    .line 34
    .line 35
    sput-object v0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->o〇0:Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;

    .line 36
    .line 37
    new-instance v0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;

    .line 38
    .line 39
    const v1, 0x7f080df1

    .line 40
    .line 41
    .line 42
    const v2, 0x7f1310ff

    .line 43
    .line 44
    .line 45
    const/4 v3, 0x2

    .line 46
    invoke-direct {v0, v3, v1, v2}, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;-><init>(III)V

    .line 47
    .line 48
    .line 49
    sput-object v0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->〇〇888:Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;

    .line 50
    .line 51
    new-instance v0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;

    .line 52
    .line 53
    const v1, 0x7f0804f7

    .line 54
    .line 55
    .line 56
    const v2, 0x7f130bf8

    .line 57
    .line 58
    .line 59
    const/4 v3, 0x3

    .line 60
    invoke-direct {v0, v3, v1, v2}, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;-><init>(III)V

    .line 61
    .line 62
    .line 63
    sput-object v0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->oO80:Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;

    .line 64
    .line 65
    return-void
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>(III)V
    .locals 0
    .param p2    # I
        .annotation build Landroidx/annotation/DrawableRes;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroidx/annotation/StringRes;
        .end annotation
    .end param

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    iput p1, p0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->〇080:I

    .line 5
    .line 6
    iput p2, p0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->〇o00〇〇Oo:I

    .line 7
    .line 8
    iput p3, p0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->〇o〇:I

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic O8()Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->〇〇888:Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇080()Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->o〇0:Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇o00〇〇Oo()Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->Oo08:Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇o〇()Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->oO80:Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final Oo08()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->〇080:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p0, p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    instance-of v1, p1, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    if-nez v1, :cond_1

    .line 9
    .line 10
    return v2

    .line 11
    :cond_1
    check-cast p1, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;

    .line 12
    .line 13
    iget v1, p0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->〇080:I

    .line 14
    .line 15
    iget v3, p1, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->〇080:I

    .line 16
    .line 17
    if-eq v1, v3, :cond_2

    .line 18
    .line 19
    return v2

    .line 20
    :cond_2
    iget v1, p0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->〇o00〇〇Oo:I

    .line 21
    .line 22
    iget v3, p1, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->〇o00〇〇Oo:I

    .line 23
    .line 24
    if-eq v1, v3, :cond_3

    .line 25
    .line 26
    return v2

    .line 27
    :cond_3
    iget v1, p0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->〇o〇:I

    .line 28
    .line 29
    iget p1, p1, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->〇o〇:I

    .line 30
    .line 31
    if-eq v1, p1, :cond_4

    .line 32
    .line 33
    return v2

    .line 34
    :cond_4
    return v0
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public hashCode()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->〇080:I

    .line 2
    .line 3
    mul-int/lit8 v0, v0, 0x1f

    .line 4
    .line 5
    iget v1, p0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->〇o00〇〇Oo:I

    .line 6
    .line 7
    add-int/2addr v0, v1

    .line 8
    mul-int/lit8 v0, v0, 0x1f

    .line 9
    .line 10
    iget v1, p0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->〇o〇:I

    .line 11
    .line 12
    add-int/2addr v0, v1

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o〇0()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->〇o00〇〇Oo:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public toString()Ljava/lang/String;
    .locals 5
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->〇080:I

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->〇o00〇〇Oo:I

    .line 4
    .line 5
    iget v2, p0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->〇o〇:I

    .line 6
    .line 7
    new-instance v3, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v4, "InnoLabFunctionItem(funcType="

    .line 13
    .line 14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    const-string v0, ", imageResId="

    .line 21
    .line 22
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    const-string v0, ", titleResId="

    .line 29
    .line 30
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    const-string v0, ")"

    .line 37
    .line 38
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    return-object v0
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇〇888()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/innovationlab/data/InnoLabFunctionItem;->〇o〇:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

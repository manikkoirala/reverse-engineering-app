.class Lcom/intsig/camscanner/fragment/TeamImagePageFragment$CustomGestureListener;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "TeamImagePageFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/fragment/TeamImagePageFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CustomGestureListener"
.end annotation


# instance fields
.field final synthetic o0:Lcom/intsig/camscanner/fragment/TeamImagePageFragment;


# direct methods
.method private constructor <init>(Lcom/intsig/camscanner/fragment/TeamImagePageFragment;)V
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/TeamImagePageFragment$CustomGestureListener;->o0:Lcom/intsig/camscanner/fragment/TeamImagePageFragment;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/intsig/camscanner/fragment/TeamImagePageFragment;L〇8〇o〇8/〇8O0O808〇;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/TeamImagePageFragment$CustomGestureListener;-><init>(Lcom/intsig/camscanner/fragment/TeamImagePageFragment;)V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamImagePageFragment$CustomGestureListener;->o0:Lcom/intsig/camscanner/fragment/TeamImagePageFragment;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/fragment/TeamImagePageFragment;->o88oo〇O(Lcom/intsig/camscanner/fragment/TeamImagePageFragment;)Lcom/intsig/camscanner/view/ImageViewTouch;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const-string v1, "TeamImagePageFragment"

    .line 8
    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getScale()F

    .line 12
    .line 13
    .line 14
    move-result v2

    .line 15
    const/high16 v3, 0x40000000    # 2.0f

    .line 16
    .line 17
    cmpl-float v2, v2, v3

    .line 18
    .line 19
    if-lez v2, :cond_0

    .line 20
    .line 21
    const/high16 p1, 0x3f800000    # 1.0f

    .line 22
    .line 23
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oo88o8O(F)[F

    .line 24
    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    .line 32
    .line 33
    .line 34
    move-result p1

    .line 35
    const/high16 v3, 0x40400000    # 3.0f

    .line 36
    .line 37
    invoke-virtual {v0, v3, v2, p1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->o〇O8〇〇o(FFF)V

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_1
    const-string p1, "imageView=null"

    .line 42
    .line 43
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    :goto_0
    const-string p1, "onDoubleTap"

    .line 47
    .line 48
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    const/4 p1, 0x1

    .line 52
    return p1
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/fragment/TeamImagePageFragment$CustomGestureListener;->o0:Lcom/intsig/camscanner/fragment/TeamImagePageFragment;

    .line 2
    .line 3
    invoke-static {p1}, Lcom/intsig/camscanner/fragment/TeamImagePageFragment;->〇〇〇O〇(Lcom/intsig/camscanner/fragment/TeamImagePageFragment;)Lcom/intsig/camscanner/fragment/TeamImagePageFragment$DeviceInteface;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    const/4 v0, 0x0

    .line 8
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/fragment/TeamImagePageFragment$DeviceInteface;->oO80(I)V

    .line 9
    .line 10
    .line 11
    const/4 p1, 0x1

    .line 12
    return p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamImagePageFragment$CustomGestureListener;->o0:Lcom/intsig/camscanner/fragment/TeamImagePageFragment;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/fragment/TeamImagePageFragment;->o88oo〇O(Lcom/intsig/camscanner/fragment/TeamImagePageFragment;)Lcom/intsig/camscanner/view/ImageViewTouch;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    new-instance v1, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v2, "onFling mIsScrolling = "

    .line 13
    .line 14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamImagePageFragment$CustomGestureListener;->o0:Lcom/intsig/camscanner/fragment/TeamImagePageFragment;

    .line 18
    .line 19
    invoke-static {v2}, Lcom/intsig/camscanner/fragment/TeamImagePageFragment;->O〇080〇o0(Lcom/intsig/camscanner/fragment/TeamImagePageFragment;)Z

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    const-string v2, ", donePostTranslate = "

    .line 27
    .line 28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamImagePageFragment$CustomGestureListener;->o0:Lcom/intsig/camscanner/fragment/TeamImagePageFragment;

    .line 32
    .line 33
    invoke-static {v2}, Lcom/intsig/camscanner/fragment/TeamImagePageFragment;->〇O8〇8000(Lcom/intsig/camscanner/fragment/TeamImagePageFragment;)Z

    .line 34
    .line 35
    .line 36
    move-result v2

    .line 37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    const-string v2, "TeamImagePageFragment"

    .line 45
    .line 46
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamImagePageFragment$CustomGestureListener;->o0:Lcom/intsig/camscanner/fragment/TeamImagePageFragment;

    .line 50
    .line 51
    invoke-static {v1}, Lcom/intsig/camscanner/fragment/TeamImagePageFragment;->O〇080〇o0(Lcom/intsig/camscanner/fragment/TeamImagePageFragment;)Z

    .line 52
    .line 53
    .line 54
    move-result v1

    .line 55
    if-nez v1, :cond_0

    .line 56
    .line 57
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamImagePageFragment$CustomGestureListener;->o0:Lcom/intsig/camscanner/fragment/TeamImagePageFragment;

    .line 58
    .line 59
    invoke-static {v1}, Lcom/intsig/camscanner/fragment/TeamImagePageFragment;->〇O8〇8000(Lcom/intsig/camscanner/fragment/TeamImagePageFragment;)Z

    .line 60
    .line 61
    .line 62
    move-result v1

    .line 63
    if-nez v1, :cond_0

    .line 64
    .line 65
    if-eqz v0, :cond_0

    .line 66
    .line 67
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getScale()F

    .line 68
    .line 69
    .line 70
    move-result v0

    .line 71
    const/high16 v1, 0x3f800000    # 1.0f

    .line 72
    .line 73
    cmpg-float v0, v0, v1

    .line 74
    .line 75
    if-gtz v0, :cond_0

    .line 76
    .line 77
    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    .line 78
    .line 79
    .line 80
    move-result v0

    .line 81
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    .line 82
    .line 83
    .line 84
    move-result v1

    .line 85
    cmpl-float v0, v0, v1

    .line 86
    .line 87
    if-lez v0, :cond_0

    .line 88
    .line 89
    new-instance p1, Ljava/lang/StringBuilder;

    .line 90
    .line 91
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 92
    .line 93
    .line 94
    const-string p2, "scroll up res="

    .line 95
    .line 96
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    const/4 p2, 0x0

    .line 100
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 101
    .line 102
    .line 103
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 104
    .line 105
    .line 106
    move-result-object p1

    .line 107
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    .line 109
    .line 110
    goto :goto_0

    .line 111
    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    .line 112
    .line 113
    .line 114
    move-result p2

    .line 115
    :goto_0
    return p2
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/fragment/TeamImagePageFragment$CustomGestureListener;->o0:Lcom/intsig/camscanner/fragment/TeamImagePageFragment;

    .line 2
    .line 3
    const/4 p2, 0x0

    .line 4
    invoke-static {p1, p2}, Lcom/intsig/camscanner/fragment/TeamImagePageFragment;->OoO〇OOo8o(Lcom/intsig/camscanner/fragment/TeamImagePageFragment;Z)V

    .line 5
    .line 6
    .line 7
    iget-object p1, p0, Lcom/intsig/camscanner/fragment/TeamImagePageFragment$CustomGestureListener;->o0:Lcom/intsig/camscanner/fragment/TeamImagePageFragment;

    .line 8
    .line 9
    invoke-static {p1}, Lcom/intsig/camscanner/fragment/TeamImagePageFragment;->o88oo〇O(Lcom/intsig/camscanner/fragment/TeamImagePageFragment;)Lcom/intsig/camscanner/view/ImageViewTouch;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamImagePageFragment$CustomGestureListener;->o0:Lcom/intsig/camscanner/fragment/TeamImagePageFragment;

    .line 14
    .line 15
    invoke-static {v0}, Lcom/intsig/camscanner/fragment/TeamImagePageFragment;->O〇080〇o0(Lcom/intsig/camscanner/fragment/TeamImagePageFragment;)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-nez v0, :cond_3

    .line 20
    .line 21
    if-eqz p1, :cond_3

    .line 22
    .line 23
    invoke-virtual {p1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getScale()F

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    const/high16 v1, 0x3f800000    # 1.0f

    .line 28
    .line 29
    cmpl-float v0, v0, v1

    .line 30
    .line 31
    if-lez v0, :cond_3

    .line 32
    .line 33
    neg-float p3, p3

    .line 34
    neg-float p4, p4

    .line 35
    invoke-virtual {p1, p3, p4}, Lcom/intsig/camscanner/view/ImageViewTouch;->O8〇o(FF)Landroid/graphics/PointF;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    const/4 p3, 0x1

    .line 40
    if-eqz p1, :cond_2

    .line 41
    .line 42
    iget p4, p1, Landroid/graphics/PointF;->x:F

    .line 43
    .line 44
    const/4 v0, 0x0

    .line 45
    cmpl-float p4, p4, v0

    .line 46
    .line 47
    if-lez p4, :cond_0

    .line 48
    .line 49
    iget-object p4, p0, Lcom/intsig/camscanner/fragment/TeamImagePageFragment$CustomGestureListener;->o0:Lcom/intsig/camscanner/fragment/TeamImagePageFragment;

    .line 50
    .line 51
    invoke-static {p4}, Lcom/intsig/camscanner/fragment/TeamImagePageFragment;->〇ooO8Ooo〇(Lcom/intsig/camscanner/fragment/TeamImagePageFragment;)Lcom/intsig/camscanner/view/MyViewPager;

    .line 52
    .line 53
    .line 54
    move-result-object p4

    .line 55
    invoke-virtual {p4}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 56
    .line 57
    .line 58
    move-result p4

    .line 59
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamImagePageFragment$CustomGestureListener;->o0:Lcom/intsig/camscanner/fragment/TeamImagePageFragment;

    .line 60
    .line 61
    invoke-static {v1}, Lcom/intsig/camscanner/fragment/TeamImagePageFragment;->〇ooO8Ooo〇(Lcom/intsig/camscanner/fragment/TeamImagePageFragment;)Lcom/intsig/camscanner/view/MyViewPager;

    .line 62
    .line 63
    .line 64
    move-result-object v1

    .line 65
    invoke-virtual {v1}, Landroidx/viewpager/widget/ViewPager;->getAdapter()Landroidx/viewpager/widget/PagerAdapter;

    .line 66
    .line 67
    .line 68
    move-result-object v1

    .line 69
    invoke-virtual {v1}, Landroidx/viewpager/widget/PagerAdapter;->getCount()I

    .line 70
    .line 71
    .line 72
    move-result v1

    .line 73
    sub-int/2addr v1, p3

    .line 74
    if-lt p4, v1, :cond_1

    .line 75
    .line 76
    :cond_0
    iget p1, p1, Landroid/graphics/PointF;->x:F

    .line 77
    .line 78
    cmpg-float p1, p1, v0

    .line 79
    .line 80
    if-gez p1, :cond_2

    .line 81
    .line 82
    iget-object p1, p0, Lcom/intsig/camscanner/fragment/TeamImagePageFragment$CustomGestureListener;->o0:Lcom/intsig/camscanner/fragment/TeamImagePageFragment;

    .line 83
    .line 84
    invoke-static {p1}, Lcom/intsig/camscanner/fragment/TeamImagePageFragment;->〇ooO8Ooo〇(Lcom/intsig/camscanner/fragment/TeamImagePageFragment;)Lcom/intsig/camscanner/view/MyViewPager;

    .line 85
    .line 86
    .line 87
    move-result-object p1

    .line 88
    invoke-virtual {p1}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 89
    .line 90
    .line 91
    move-result p1

    .line 92
    if-lez p1, :cond_2

    .line 93
    .line 94
    :cond_1
    const/4 p1, 0x1

    .line 95
    goto :goto_0

    .line 96
    :cond_2
    const/4 p1, 0x0

    .line 97
    :goto_0
    if-nez p1, :cond_3

    .line 98
    .line 99
    iget-object p1, p0, Lcom/intsig/camscanner/fragment/TeamImagePageFragment$CustomGestureListener;->o0:Lcom/intsig/camscanner/fragment/TeamImagePageFragment;

    .line 100
    .line 101
    invoke-static {p1, p3}, Lcom/intsig/camscanner/fragment/TeamImagePageFragment;->OoO〇OOo8o(Lcom/intsig/camscanner/fragment/TeamImagePageFragment;Z)V

    .line 102
    .line 103
    .line 104
    const/4 p2, 0x1

    .line 105
    :cond_3
    return p2
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/fragment/TeamImagePageFragment$CustomGestureListener;->o0:Lcom/intsig/camscanner/fragment/TeamImagePageFragment;

    .line 2
    .line 3
    const/4 v0, 0x0

    .line 4
    const/4 v1, 0x1

    .line 5
    invoke-static {p1, v0, v1}, Lcom/intsig/camscanner/fragment/TeamImagePageFragment;->oo8(Lcom/intsig/camscanner/fragment/TeamImagePageFragment;ZZ)V

    .line 6
    .line 7
    .line 8
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

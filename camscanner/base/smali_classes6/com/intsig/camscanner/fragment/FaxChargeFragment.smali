.class public Lcom/intsig/camscanner/fragment/FaxChargeFragment;
.super Lcom/intsig/mvp/fragment/BaseChangeFragment;
.source "FaxChargeFragment.java"


# instance fields
.field private O0O:Lcom/intsig/app/BaseProgressDialog;

.field private final O88O:Landroidx/lifecycle/Observer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/lifecycle/Observer<",
            "Lcom/intsig/camscanner/purchase/pay/task/entity/LivePayResponse;",
            ">;"
        }
    .end annotation
.end field

.field O8o08O8O:Landroid/widget/TextView;

.field OO:Landroid/widget/ImageView;

.field private OO〇00〇8oO:I

.field private o0:[I

.field private o8oOOo:I

.field private o8〇OO0〇0o:Ljava/lang/String;

.field private oOo0:I

.field oOo〇8o008:Landroid/widget/Button;

.field private ooo0〇〇O:Z

.field o〇00O:Landroid/widget/Button;

.field 〇080OO8〇0:Landroid/widget/Button;

.field 〇08O〇00〇o:Landroid/widget/TextView;

.field 〇0O:Landroid/widget/TextView;

.field private 〇8〇oO〇〇8o:Z

.field 〇OOo8〇0:Landroid/widget/TextView;

.field private 〇O〇〇O8:Ljava/lang/String;

.field private 〇o0O:Lcom/intsig/camscanner/purchase/dialog/BottomPurchaseDialog;

.field private 〇〇08O:Landroid/view/animation/Animation;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x3

    .line 5
    new-array v0, v0, [I

    .line 6
    .line 7
    fill-array-data v0, :array_0

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->o0:[I

    .line 11
    .line 12
    const/4 v0, 0x0

    .line 13
    iput v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->oOo0:I

    .line 14
    .line 15
    iput v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->OO〇00〇8oO:I

    .line 16
    .line 17
    const/4 v0, 0x4

    .line 18
    iput v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->o8oOOo:I

    .line 19
    .line 20
    new-instance v0, Lcom/intsig/camscanner/fragment/FaxChargeFragment$1;

    .line 21
    .line 22
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/fragment/FaxChargeFragment$1;-><init>(Lcom/intsig/camscanner/fragment/FaxChargeFragment;)V

    .line 23
    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->O88O:Landroidx/lifecycle/Observer;

    .line 26
    .line 27
    return-void

    .line 28
    nop

    .line 29
    :array_0
    .array-data 4
        0x1
        0x2
        0x3
    .end array-data
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private O0O0〇(I)V
    .locals 2

    .line 1
    const-string v0, "FaxChargeFragment"

    .line 2
    .line 3
    const-string v1, "verify developer payload success"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/camscanner/https/account/UserPropertyAPI;->〇〇888()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-ltz v0, :cond_0

    .line 13
    .line 14
    iput v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->oOo0:I

    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mHandler:Landroid/os/Handler;

    .line 17
    .line 18
    const/4 v1, 0x3

    .line 19
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 20
    .line 21
    .line 22
    :cond_0
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mHandler:Landroid/os/Handler;

    .line 23
    .line 24
    new-instance v1, L〇8〇o〇8/oO80;

    .line 25
    .line 26
    invoke-direct {v1, p0, p1}, L〇8〇o〇8/oO80;-><init>(Lcom/intsig/camscanner/fragment/FaxChargeFragment;I)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic O0〇0(Lcom/intsig/camscanner/fragment/FaxChargeFragment;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->o8〇OO0〇0o:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O8〇8〇O80()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇OOo8〇0:Landroid/widget/TextView;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v1, ""

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    iget v1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->oOo0:I

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 25
    .line 26
    const/4 v2, 0x1

    .line 27
    new-array v2, v2, [Ljava/lang/Object;

    .line 28
    .line 29
    new-instance v3, Ljava/lang/StringBuilder;

    .line 30
    .line 31
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    .line 33
    .line 34
    const-string v4, " "

    .line 35
    .line 36
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v3

    .line 49
    const/4 v4, 0x0

    .line 50
    aput-object v3, v2, v4

    .line 51
    .line 52
    const v3, 0x7f130485

    .line 53
    .line 54
    .line 55
    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    .line 60
    .line 61
    .line 62
    move-result v2

    .line 63
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    .line 64
    .line 65
    .line 66
    move-result v0

    .line 67
    add-int/2addr v0, v2

    .line 68
    new-instance v3, Landroid/text/SpannableStringBuilder;

    .line 69
    .line 70
    invoke-direct {v3, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 71
    .line 72
    .line 73
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    .line 74
    .line 75
    iget-object v4, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 76
    .line 77
    invoke-virtual {v4}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 78
    .line 79
    .line 80
    move-result-object v4

    .line 81
    const v5, 0x7f060527

    .line 82
    .line 83
    .line 84
    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    .line 85
    .line 86
    .line 87
    move-result v4

    .line 88
    invoke-direct {v1, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 89
    .line 90
    .line 91
    const/16 v4, 0x21

    .line 92
    .line 93
    invoke-virtual {v3, v1, v2, v0, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 94
    .line 95
    .line 96
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇OOo8〇0:Landroid/widget/TextView;

    .line 97
    .line 98
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    .line 100
    .line 101
    :cond_0
    return-void
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private synthetic Ooo8o()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/https/account/UserPropertyAPI;->〇〇888()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-ltz v0, :cond_0

    .line 6
    .line 7
    iput v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->oOo0:I

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mHandler:Landroid/os/Handler;

    .line 10
    .line 11
    const/4 v1, 0x3

    .line 12
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 13
    .line 14
    .line 15
    :cond_0
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic o00〇88〇08(Lcom/intsig/camscanner/fragment/FaxChargeFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇〇〇0(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o880(Lcom/intsig/camscanner/fragment/FaxChargeFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->Ooo8o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private oOoO8OO〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->O0O:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->O0O:Lcom/intsig/app/BaseProgressDialog;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 14
    .line 15
    .line 16
    goto :goto_0

    .line 17
    :catch_0
    const-string v0, "FaxChargeFragment"

    .line 18
    .line 19
    const-string v1, "dismiss dialog exception"

    .line 20
    .line 21
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    :cond_0
    :goto_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic oO〇oo(Lcom/intsig/camscanner/fragment/FaxChargeFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇0ooOOo(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oooO888(Lcom/intsig/camscanner/fragment/FaxChargeFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇o08()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o〇0〇o(Lcom/intsig/camscanner/fragment/FaxChargeFragment;)Landroid/view/animation/Animation;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇〇08O:Landroid/view/animation/Animation;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic o〇O8OO(Lcom/intsig/camscanner/fragment/FaxChargeFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇088O(Lcom/intsig/camscanner/fragment/FaxChargeFragment;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇8〇oO〇〇8o:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇08O()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->O0O:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    invoke-static {v0, v1}, Lcom/intsig/utils/DialogUtils;->〇o〇(Landroid/content/Context;I)Lcom/intsig/app/BaseProgressDialog;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->O0O:Lcom/intsig/app/BaseProgressDialog;

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 15
    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->O0O:Lcom/intsig/app/BaseProgressDialog;

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 20
    .line 21
    const v2, 0x7f13024d

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-virtual {v0, v1}, Lcom/intsig/app/BaseProgressDialog;->o〇O8〇〇o(Ljava/lang/CharSequence;)V

    .line 29
    .line 30
    .line 31
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->O0O:Lcom/intsig/app/BaseProgressDialog;

    .line 32
    .line 33
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    if-nez v0, :cond_1

    .line 38
    .line 39
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->O0O:Lcom/intsig/app/BaseProgressDialog;

    .line 40
    .line 41
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    .line 43
    .line 44
    goto :goto_0

    .line 45
    :catch_0
    move-exception v0

    .line 46
    const-string v1, "FaxChargeFragment"

    .line 47
    .line 48
    const-string v2, "show progress dialog "

    .line 49
    .line 50
    invoke-static {v1, v2, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 51
    .line 52
    .line 53
    :cond_1
    :goto_0
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇0oO〇oo00()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇〇08O:Landroid/view/animation/Animation;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/fragment/FaxChargeFragment$2;

    .line 4
    .line 5
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/fragment/FaxChargeFragment$2;-><init>(Lcom/intsig/camscanner/fragment/FaxChargeFragment;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->OO:Landroid/widget/ImageView;

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇〇08O:Landroid/view/animation/Animation;

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 16
    .line 17
    .line 18
    const/4 v0, 0x1

    .line 19
    iput-boolean v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->ooo0〇〇O:Z

    .line 20
    .line 21
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    new-instance v1, L〇8〇o〇8/〇O8o08O;

    .line 26
    .line 27
    invoke-direct {v1, p0}, L〇8〇o〇8/〇O8o08O;-><init>(Lcom/intsig/camscanner/fragment/FaxChargeFragment;)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic 〇0ooOOo(Landroid/view/View;)V
    .locals 0

    .line 1
    iget-boolean p1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇8〇oO〇〇8o:Z

    .line 2
    .line 3
    if-nez p1, :cond_0

    .line 4
    .line 5
    iget-boolean p1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->ooo0〇〇O:Z

    .line 6
    .line 7
    if-nez p1, :cond_0

    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇0oO〇oo00()V

    .line 10
    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static synthetic 〇0〇0(Lcom/intsig/camscanner/fragment/FaxChargeFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇8O0880(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductId;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->o8oOOo:I

    .line 2
    .line 3
    const/16 v1, 0x63

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/purchase/dialog/BottomPurchaseDialog;->〇080OO8〇0:Lcom/intsig/camscanner/purchase/dialog/BottomPurchaseDialog$Companion;

    .line 8
    .line 9
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇O〇〇O8:Ljava/lang/String;

    .line 10
    .line 11
    invoke-virtual {v0, v1, p1}, Lcom/intsig/camscanner/purchase/dialog/BottomPurchaseDialog$Companion;->〇080(Ljava/lang/String;Ljava/util/ArrayList;)Lcom/intsig/camscanner/purchase/dialog/BottomPurchaseDialog;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    new-instance v0, L〇8〇o〇8/〇80〇808〇O;

    .line 16
    .line 17
    invoke-direct {v0, p0}, L〇8〇o〇8/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/fragment/FaxChargeFragment;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/purchase/dialog/BottomPurchaseDialog;->〇8〇80o(Lcom/intsig/camscanner/purchase/dialog/BottomPurchaseDialog$IBottomPurchaseCallback;)Lcom/intsig/camscanner/purchase/dialog/BottomPurchaseDialog;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇o0O:Lcom/intsig/camscanner/purchase/dialog/BottomPurchaseDialog;

    .line 25
    .line 26
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/purchase/dialog/BottomPurchaseDialog;->o〇O8OO(Landroidx/fragment/app/FragmentManager;)V

    .line 31
    .line 32
    .line 33
    return-void

    .line 34
    :cond_0
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇O0o〇〇o(I)V

    .line 35
    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic 〇8〇80o(Lcom/intsig/camscanner/fragment/FaxChargeFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->O0O0〇(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/fragment/FaxChargeFragment;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->OO〇00〇8oO:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇O0o〇〇o(I)V
    .locals 8

    .line 1
    const/4 v0, 0x4

    .line 2
    if-ne p1, v0, :cond_0

    .line 3
    .line 4
    const-string v0, "CamScanner_Fax_Balance"

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const-string v0, "cs_fax"

    .line 8
    .line 9
    :goto_0
    move-object v6, v0

    .line 10
    sget-object v1, Lcom/intsig/camscanner/purchase/pay/task/PayOrderRequest;->〇080:Lcom/intsig/camscanner/purchase/pay/task/PayOrderRequest;

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->O88O:Landroidx/lifecycle/Observer;

    .line 13
    .line 14
    invoke-virtual {v1, p0, v0}, Lcom/intsig/camscanner/purchase/pay/task/PayOrderRequest;->〇0000OOO(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 18
    .line 19
    .line 20
    move-result-object v2

    .line 21
    iget-object v3, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->o8〇OO0〇0o:Ljava/lang/String;

    .line 22
    .line 23
    const/4 v5, 0x0

    .line 24
    const/4 v7, 0x0

    .line 25
    move v4, p1

    .line 26
    invoke-virtual/range {v1 .. v7}, Lcom/intsig/camscanner/purchase/pay/task/PayOrderRequest;->Ooo(Landroidx/fragment/app/FragmentActivity;Ljava/lang/String;IILjava/lang/String;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/camscanner/fragment/FaxChargeFragment;ILjava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇〇〇00(ILjava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private 〇O8〇8000(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductId;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget v1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->o8oOOo:I

    .line 7
    .line 8
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    iget v2, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->o8oOOo:I

    .line 13
    .line 14
    const/4 v3, 0x4

    .line 15
    if-eq v2, v3, :cond_2

    .line 16
    .line 17
    const/16 v4, 0xd

    .line 18
    .line 19
    if-eq v2, v4, :cond_1

    .line 20
    .line 21
    const/16 v1, 0x63

    .line 22
    .line 23
    if-eq v2, v1, :cond_0

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    new-instance v1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductId;

    .line 27
    .line 28
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    invoke-direct {v1, p1, v2}, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    new-instance p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductId;

    .line 39
    .line 40
    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    invoke-direct {p1, p2, v1}, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_1
    new-instance p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductId;

    .line 52
    .line 53
    invoke-direct {p1, p2, v1}, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    .line 58
    .line 59
    goto :goto_0

    .line 60
    :cond_2
    new-instance p2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductId;

    .line 61
    .line 62
    invoke-direct {p2, p1, v1}, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 66
    .line 67
    .line 68
    :goto_0
    return-object v0
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private synthetic 〇o08()V
    .locals 3

    .line 1
    const-string v0, "start refresh fax balance"

    .line 2
    .line 3
    const-string v1, "FaxChargeFragment"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x1

    .line 9
    iput-boolean v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇8〇oO〇〇8o:Z

    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/camscanner/https/account/UserPropertyAPI;->〇〇888()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-ltz v0, :cond_0

    .line 16
    .line 17
    iput v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->oOo0:I

    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mHandler:Landroid/os/Handler;

    .line 20
    .line 21
    const/4 v2, 0x3

    .line 22
    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 23
    .line 24
    .line 25
    :cond_0
    const/4 v0, 0x0

    .line 26
    iput-boolean v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇8〇oO〇〇8o:Z

    .line 27
    .line 28
    const-string v0, "refresh fax balance end"

    .line 29
    .line 30
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic 〇o〇88〇8(Lcom/intsig/camscanner/fragment/FaxChargeFragment;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->ooo0〇〇O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇〇O80〇0o()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, L〇8〇o〇8/〇8o8o〇;

    .line 6
    .line 7
    invoke-direct {v1, p0}, L〇8〇o〇8/〇8o8o〇;-><init>(Lcom/intsig/camscanner/fragment/FaxChargeFragment;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇〇o0〇8(Lcom/intsig/camscanner/fragment/FaxChargeFragment;)Lcom/intsig/camscanner/purchase/dialog/BottomPurchaseDialog;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇o0O:Lcom/intsig/camscanner/purchase/dialog/BottomPurchaseDialog;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇〇〇0(I)V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 6
    .line 7
    .line 8
    const v1, 0x7f131d10

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇〇〇0〇〇0(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 16
    .line 17
    .line 18
    const/4 v1, 0x1

    .line 19
    new-array v1, v1, [Ljava/lang/Object;

    .line 20
    .line 21
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    const/4 v2, 0x0

    .line 26
    aput-object p1, v1, v2

    .line 27
    .line 28
    const p1, 0x7f130242

    .line 29
    .line 30
    .line 31
    invoke-virtual {p0, p1, v1}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    invoke-virtual {v0, p1}, Lcom/intsig/app/AlertDialog$Builder;->〇O00(Ljava/lang/CharSequence;)Lcom/intsig/app/AlertDialog$Builder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v0, v2}, Lcom/intsig/app/AlertDialog$Builder;->o〇0(Z)Lcom/intsig/app/AlertDialog$Builder;

    .line 39
    .line 40
    .line 41
    const p1, 0x7f131d01

    .line 42
    .line 43
    .line 44
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    const/4 v1, 0x0

    .line 49
    invoke-virtual {v0, p1, v1}, Lcom/intsig/app/AlertDialog$Builder;->oo〇(Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 50
    .line 51
    .line 52
    :try_start_0
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    .line 58
    .line 59
    goto :goto_0

    .line 60
    :catch_0
    move-exception p1

    .line 61
    const-string v0, "FaxChargeFragment"

    .line 62
    .line 63
    const-string v1, "show buy fax success "

    .line 64
    .line 65
    invoke-static {v0, v1, p1}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 66
    .line 67
    .line 68
    :goto_0
    return-void
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private synthetic 〇〇〇00(ILjava/lang/String;)V
    .locals 0

    .line 1
    iput-object p2, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->o8〇OO0〇0o:Ljava/lang/String;

    .line 2
    .line 3
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇O0o〇〇o(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public dealClickAction(Landroid/view/View;)V
    .locals 5

    .line 1
    const-string v0, "com.intsig.camscanner.faxtwopage"

    .line 2
    .line 3
    const-string v1, "com.intsig.camscanner.faxtenpage"

    .line 4
    .line 5
    const-string v2, "com.intsig.camscanner.faxonepage"

    .line 6
    .line 7
    const-string v3, "FaxChargeFragment"

    .line 8
    .line 9
    :try_start_0
    new-instance v4, Ljava/util/ArrayList;

    .line 10
    .line 11
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 12
    .line 13
    .line 14
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    packed-switch p1, :pswitch_data_0

    .line 19
    .line 20
    .line 21
    goto :goto_0

    .line 22
    :pswitch_0
    const-string p1, "onBuyBtnClicked: btn_buy_2"

    .line 23
    .line 24
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    const/4 p1, 0x2

    .line 28
    iput p1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->OO〇00〇8oO:I

    .line 29
    .line 30
    const p1, 0x7f130484

    .line 31
    .line 32
    .line 33
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇O〇〇O8:Ljava/lang/String;

    .line 38
    .line 39
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->o8〇OO0〇0o:Ljava/lang/String;

    .line 40
    .line 41
    const-string p1, "com.intsig.camscanner.huaweifree.faxtwopage"

    .line 42
    .line 43
    invoke-direct {p0, v0, p1}, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇O8〇8000(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 48
    .line 49
    const-string v1, "Go to buy fax 2"

    .line 50
    .line 51
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogTrackerUserData;->〇80〇808〇O(Landroid/content/Context;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    goto :goto_1

    .line 55
    :pswitch_1
    const-string p1, "onBuyBtnClicked: btn_buy_10"

    .line 56
    .line 57
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    .line 59
    .line 60
    const/16 p1, 0xa

    .line 61
    .line 62
    iput p1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->OO〇00〇8oO:I

    .line 63
    .line 64
    const p1, 0x7f1303ec

    .line 65
    .line 66
    .line 67
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object p1

    .line 71
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇O〇〇O8:Ljava/lang/String;

    .line 72
    .line 73
    iput-object v1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->o8〇OO0〇0o:Ljava/lang/String;

    .line 74
    .line 75
    const-string p1, "com.intsig.camscanner.huaweifree.faxtenpage"

    .line 76
    .line 77
    invoke-direct {p0, v1, p1}, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇O8〇8000(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    .line 78
    .line 79
    .line 80
    move-result-object p1

    .line 81
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 82
    .line 83
    const-string v1, "Go to buy fax 10"

    .line 84
    .line 85
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogTrackerUserData;->〇80〇808〇O(Landroid/content/Context;Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    goto :goto_1

    .line 89
    :goto_0
    const-string p1, "onBuyBtnClicked: btn_buy_1"

    .line 90
    .line 91
    invoke-static {v3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    const/4 p1, 0x1

    .line 95
    iput p1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->OO〇00〇8oO:I

    .line 96
    .line 97
    const p1, 0x7f130386

    .line 98
    .line 99
    .line 100
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 101
    .line 102
    .line 103
    move-result-object p1

    .line 104
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇O〇〇O8:Ljava/lang/String;

    .line 105
    .line 106
    iput-object v2, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->o8〇OO0〇0o:Ljava/lang/String;

    .line 107
    .line 108
    const-string p1, "com.intsig.camscanner.huaweifree.faxonepage"

    .line 109
    .line 110
    invoke-direct {p0, v2, p1}, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇O8〇8000(Ljava/lang/String;Ljava/lang/String;)Ljava/util/ArrayList;

    .line 111
    .line 112
    .line 113
    move-result-object p1

    .line 114
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 115
    .line 116
    const-string v1, "Go to buy fax 1"

    .line 117
    .line 118
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogTrackerUserData;->〇80〇808〇O(Landroid/content/Context;Ljava/lang/String;)V

    .line 119
    .line 120
    .line 121
    :goto_1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇8O0880(Ljava/util/ArrayList;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    .line 123
    .line 124
    goto :goto_2

    .line 125
    :catch_0
    move-exception p1

    .line 126
    const-string v0, "onBuyBtnClicked"

    .line 127
    .line 128
    invoke-static {v3, v0, p1}, Lcom/intsig/log/LogUtils;->〇80〇808〇O(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 129
    .line 130
    .line 131
    :goto_2
    return-void

    .line 132
    nop

    .line 133
    :pswitch_data_0
    .packed-switch 0x7f0a022b
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->handleMessage(Landroid/os/Message;)V

    .line 2
    .line 3
    .line 4
    iget p1, p1, Landroid/os/Message;->what:I

    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    if-eq p1, v0, :cond_2

    .line 8
    .line 9
    const/4 v0, 0x2

    .line 10
    if-eq p1, v0, :cond_1

    .line 11
    .line 12
    const/4 v0, 0x3

    .line 13
    if-eq p1, v0, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->O8〇8〇O80()V

    .line 17
    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->oOoO8OO〇()V

    .line 21
    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇08O()V

    .line 25
    .line 26
    .line 27
    :goto_0
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 6

    .line 1
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 2
    .line 3
    const v0, 0x7f0a1423

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    check-cast p1, Landroid/widget/TextView;

    .line 11
    .line 12
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇OOo8〇0:Landroid/widget/TextView;

    .line 13
    .line 14
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 15
    .line 16
    const v0, 0x7f0a05e0

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    check-cast p1, Landroid/widget/ImageView;

    .line 24
    .line 25
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->OO:Landroid/widget/ImageView;

    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 28
    .line 29
    const v0, 0x7f0a12b5

    .line 30
    .line 31
    .line 32
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    check-cast p1, Landroid/widget/TextView;

    .line 37
    .line 38
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 39
    .line 40
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 41
    .line 42
    const v0, 0x7f0a022a

    .line 43
    .line 44
    .line 45
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    check-cast p1, Landroid/widget/Button;

    .line 50
    .line 51
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->o〇00O:Landroid/widget/Button;

    .line 52
    .line 53
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 54
    .line 55
    const v0, 0x7f0a12b7

    .line 56
    .line 57
    .line 58
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    check-cast p1, Landroid/widget/TextView;

    .line 63
    .line 64
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->O8o08O8O:Landroid/widget/TextView;

    .line 65
    .line 66
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 67
    .line 68
    const v0, 0x7f0a022c

    .line 69
    .line 70
    .line 71
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 72
    .line 73
    .line 74
    move-result-object p1

    .line 75
    check-cast p1, Landroid/widget/Button;

    .line 76
    .line 77
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇080OO8〇0:Landroid/widget/Button;

    .line 78
    .line 79
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 80
    .line 81
    const v0, 0x7f0a12b6

    .line 82
    .line 83
    .line 84
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 85
    .line 86
    .line 87
    move-result-object p1

    .line 88
    check-cast p1, Landroid/widget/TextView;

    .line 89
    .line 90
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇0O:Landroid/widget/TextView;

    .line 91
    .line 92
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 93
    .line 94
    const v0, 0x7f0a022b

    .line 95
    .line 96
    .line 97
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 98
    .line 99
    .line 100
    move-result-object p1

    .line 101
    check-cast p1, Landroid/widget/Button;

    .line 102
    .line 103
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->oOo〇8o008:Landroid/widget/Button;

    .line 104
    .line 105
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 106
    .line 107
    .line 108
    move-result-object p1

    .line 109
    invoke-static {p1}, Lcom/intsig/camscanner/app/AppSwitch;->〇o〇(Landroid/content/Context;)I

    .line 110
    .line 111
    .line 112
    move-result p1

    .line 113
    iput p1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->o8oOOo:I

    .line 114
    .line 115
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 116
    .line 117
    const v0, 0x7f130427

    .line 118
    .line 119
    .line 120
    invoke-virtual {p1, v0}, Landroid/app/Activity;->setTitle(I)V

    .line 121
    .line 122
    .line 123
    const/4 p1, 0x1

    .line 124
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->setHasOptionsMenu(Z)V

    .line 125
    .line 126
    .line 127
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->OO:Landroid/widget/ImageView;

    .line 128
    .line 129
    new-instance v1, L〇8〇o〇8/OO0o〇〇〇〇0;

    .line 130
    .line 131
    invoke-direct {v1, p0}, L〇8〇o〇8/OO0o〇〇〇〇0;-><init>(Lcom/intsig/camscanner/fragment/FaxChargeFragment;)V

    .line 132
    .line 133
    .line 134
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    .line 136
    .line 137
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 138
    .line 139
    const v1, 0x7f010040

    .line 140
    .line 141
    .line 142
    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    .line 143
    .line 144
    .line 145
    move-result-object v0

    .line 146
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇〇08O:Landroid/view/animation/Animation;

    .line 147
    .line 148
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 149
    .line 150
    .line 151
    move-result-object v0

    .line 152
    if-eqz v0, :cond_0

    .line 153
    .line 154
    const-string v1, "data"

    .line 155
    .line 156
    invoke-virtual {v0, v1}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 157
    .line 158
    .line 159
    move-result v0

    .line 160
    iput v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->oOo0:I

    .line 161
    .line 162
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->O8〇8〇O80()V

    .line 163
    .line 164
    .line 165
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇〇O80〇0o()V

    .line 166
    .line 167
    .line 168
    const/4 v0, 0x3

    .line 169
    new-array v0, v0, [Landroid/view/View;

    .line 170
    .line 171
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->o〇00O:Landroid/widget/Button;

    .line 172
    .line 173
    const/4 v2, 0x0

    .line 174
    aput-object v1, v0, v2

    .line 175
    .line 176
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇080OO8〇0:Landroid/widget/Button;

    .line 177
    .line 178
    aput-object v1, v0, p1

    .line 179
    .line 180
    const/4 v1, 0x2

    .line 181
    iget-object v3, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->oOo〇8o008:Landroid/widget/Button;

    .line 182
    .line 183
    aput-object v3, v0, v1

    .line 184
    .line 185
    invoke-virtual {p0, v0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 186
    .line 187
    .line 188
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 189
    .line 190
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 191
    .line 192
    new-array v3, p1, [Ljava/lang/Object;

    .line 193
    .line 194
    const-string v4, "<font color=\"#000000\"><b>1</b></font>"

    .line 195
    .line 196
    aput-object v4, v3, v2

    .line 197
    .line 198
    const v4, 0x7f13013f

    .line 199
    .line 200
    .line 201
    invoke-virtual {v1, v4, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 202
    .line 203
    .line 204
    move-result-object v1

    .line 205
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    .line 206
    .line 207
    .line 208
    move-result-object v1

    .line 209
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    .line 211
    .line 212
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->O8o08O8O:Landroid/widget/TextView;

    .line 213
    .line 214
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 215
    .line 216
    new-array v3, p1, [Ljava/lang/Object;

    .line 217
    .line 218
    const-string v5, "<font color=\"#000000\"><b>2</b></font>"

    .line 219
    .line 220
    aput-object v5, v3, v2

    .line 221
    .line 222
    invoke-virtual {v1, v4, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 223
    .line 224
    .line 225
    move-result-object v1

    .line 226
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    .line 227
    .line 228
    .line 229
    move-result-object v1

    .line 230
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    .line 232
    .line 233
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->〇0O:Landroid/widget/TextView;

    .line 234
    .line 235
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 236
    .line 237
    new-array p1, p1, [Ljava/lang/Object;

    .line 238
    .line 239
    const-string v3, "<font color=\"#000000\"><b>10</b></font>"

    .line 240
    .line 241
    aput-object v3, p1, v2

    .line 242
    .line 243
    invoke-virtual {v1, v4, p1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 244
    .line 245
    .line 246
    move-result-object p1

    .line 247
    invoke-static {p1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    .line 248
    .line 249
    .line 250
    move-result-object p1

    .line 251
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 252
    .line 253
    .line 254
    return-void
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public onDestroy()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mHandler:Landroid/os/Handler;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/FaxChargeFragment;->o0:[I

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    const-string v3, "FaxChargeFragment"

    .line 7
    .line 8
    invoke-static {v3, v0, v1, v2}, Lcom/intsig/comm/recycle/HandlerMsglerRecycle;->〇o〇(Ljava/lang/String;Landroid/os/Handler;[I[Ljava/lang/Runnable;)V

    .line 9
    .line 10
    .line 11
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d027e

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

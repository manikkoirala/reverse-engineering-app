.class Lcom/intsig/camscanner/fragment/TeamDocFragment$SyncCallbackListenerImpl;
.super Ljava/lang/Object;
.source "TeamDocFragment.java"

# interfaces
.implements Lcom/intsig/camscanner/tsapp/SyncCallbackListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/fragment/TeamDocFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SyncCallbackListenerImpl"
.end annotation


# instance fields
.field private final 〇080:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Lcom/intsig/camscanner/fragment/TeamDocFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/intsig/camscanner/fragment/TeamDocFragment;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamDocFragment$SyncCallbackListenerImpl;->〇080:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method synthetic constructor <init>(Lcom/intsig/camscanner/fragment/TeamDocFragment;L〇8〇o〇8/〇〇00OO;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/TeamDocFragment$SyncCallbackListenerImpl;-><init>(Lcom/intsig/camscanner/fragment/TeamDocFragment;)V

    return-void
.end method


# virtual methods
.method public O8()Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamDocFragment$SyncCallbackListenerImpl;->〇080:Ljava/lang/ref/WeakReference;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Oo08(JJJIZ)V
    .locals 14

    .line 1
    move-wide v1, p1

    .line 2
    move-wide/from16 v3, p3

    .line 3
    .line 4
    move-wide/from16 v5, p5

    .line 5
    .line 6
    move/from16 v7, p7

    .line 7
    .line 8
    move-object v8, p0

    .line 9
    iget-object v0, v8, Lcom/intsig/camscanner/fragment/TeamDocFragment$SyncCallbackListenerImpl;->〇080:Ljava/lang/ref/WeakReference;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    move-object v9, v0

    .line 16
    check-cast v9, Lcom/intsig/camscanner/fragment/TeamDocFragment;

    .line 17
    .line 18
    if-nez v9, :cond_0

    .line 19
    .line 20
    invoke-static {}, Lcom/intsig/camscanner/fragment/TeamDocFragment;->O008oO0()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    const-string v1, "teamDocFragment == null"

    .line 25
    .line 26
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    return-void

    .line 30
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/fragment/TeamDocFragment;->O008oO0()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    new-instance v10, Ljava/lang/StringBuilder;

    .line 35
    .line 36
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 37
    .line 38
    .line 39
    const-string v11, "contentChange docId="

    .line 40
    .line 41
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    invoke-virtual {v10, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    const-string v11, " mDocId "

    .line 48
    .line 49
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-static {v9}, Lcom/intsig/camscanner/fragment/TeamDocFragment;->O8〇8〇O80(Lcom/intsig/camscanner/fragment/TeamDocFragment;)J

    .line 53
    .line 54
    .line 55
    move-result-wide v11

    .line 56
    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    const-string v11, " tagId = "

    .line 60
    .line 61
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    invoke-virtual {v10, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object v10

    .line 71
    invoke-static {v0, v10}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    invoke-static {}, Lcom/intsig/camscanner/fragment/TeamDocFragment;->O008oO0()Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    new-instance v10, Ljava/lang/StringBuilder;

    .line 79
    .line 80
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 81
    .line 82
    .line 83
    const-string v11, "contentChange jpgChange = "

    .line 84
    .line 85
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    move/from16 v11, p8

    .line 89
    .line 90
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    const-string v12, " syncAction = "

    .line 94
    .line 95
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 99
    .line 100
    .line 101
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object v10

    .line 105
    invoke-static {v0, v10}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    invoke-static {}, Lcom/intsig/camscanner/fragment/TeamDocFragment;->O008oO0()Ljava/lang/String;

    .line 109
    .line 110
    .line 111
    move-result-object v0

    .line 112
    new-instance v10, Ljava/lang/StringBuilder;

    .line 113
    .line 114
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 115
    .line 116
    .line 117
    const-string v12, "contentChange imageId="

    .line 118
    .line 119
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    .line 121
    .line 122
    invoke-virtual {v10, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 123
    .line 124
    .line 125
    const-string v12, " mCurrentPosition = "

    .line 126
    .line 127
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    .line 129
    .line 130
    invoke-static {v9}, Lcom/intsig/camscanner/fragment/TeamDocFragment;->〇O0o〇〇o(Lcom/intsig/camscanner/fragment/TeamDocFragment;)I

    .line 131
    .line 132
    .line 133
    move-result v12

    .line 134
    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 135
    .line 136
    .line 137
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 138
    .line 139
    .line 140
    move-result-object v10

    .line 141
    invoke-static {v0, v10}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 142
    .line 143
    .line 144
    const-wide/16 v12, 0x0

    .line 145
    .line 146
    cmp-long v0, v3, v12

    .line 147
    .line 148
    if-gtz v0, :cond_1

    .line 149
    .line 150
    cmp-long v0, v1, v12

    .line 151
    .line 152
    if-lez v0, :cond_3

    .line 153
    .line 154
    invoke-static {v9}, Lcom/intsig/camscanner/fragment/TeamDocFragment;->O8〇8〇O80(Lcom/intsig/camscanner/fragment/TeamDocFragment;)J

    .line 155
    .line 156
    .line 157
    move-result-wide v12

    .line 158
    cmp-long v0, v1, v12

    .line 159
    .line 160
    if-nez v0, :cond_3

    .line 161
    .line 162
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/fragment/TeamDocFragment;->O008oO0()Ljava/lang/String;

    .line 163
    .line 164
    .line 165
    move-result-object v0

    .line 166
    const-string v10, "contentChange do Change"

    .line 167
    .line 168
    invoke-static {v0, v10}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    .line 170
    .line 171
    const/4 v0, 0x2

    .line 172
    if-ne v7, v0, :cond_2

    .line 173
    .line 174
    const-wide/16 v12, -0x1

    .line 175
    .line 176
    cmp-long v0, v3, v12

    .line 177
    .line 178
    if-nez v0, :cond_2

    .line 179
    .line 180
    cmp-long v0, v5, v12

    .line 181
    .line 182
    if-nez v0, :cond_2

    .line 183
    .line 184
    invoke-static {}, Lcom/intsig/camscanner/fragment/TeamDocFragment;->O008oO0()Ljava/lang/String;

    .line 185
    .line 186
    .line 187
    move-result-object v0

    .line 188
    const-string v1, "contentChange do Change Delete doc back"

    .line 189
    .line 190
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    .line 192
    .line 193
    invoke-static {v9}, Lcom/intsig/camscanner/fragment/TeamDocFragment;->o〇o0oOO8(Lcom/intsig/camscanner/fragment/TeamDocFragment;)V

    .line 194
    .line 195
    .line 196
    return-void

    .line 197
    :cond_2
    new-instance v10, Lcom/intsig/camscanner/fragment/SyncContentChangedInfo;

    .line 198
    .line 199
    move-object v0, v10

    .line 200
    move-wide v1, p1

    .line 201
    move-wide/from16 v3, p3

    .line 202
    .line 203
    move/from16 v5, p8

    .line 204
    .line 205
    move/from16 v6, p7

    .line 206
    .line 207
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/fragment/SyncContentChangedInfo;-><init>(JJZI)V

    .line 208
    .line 209
    .line 210
    iget-object v0, v9, Lcom/intsig/camscanner/fragment/TeamDocFragment;->〇O0o〇〇o:Landroid/os/Handler;

    .line 211
    .line 212
    const/16 v1, 0x6a

    .line 213
    .line 214
    invoke-static {v0, v1, v10}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    .line 215
    .line 216
    .line 217
    move-result-object v1

    .line 218
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 219
    .line 220
    .line 221
    :cond_3
    return-void
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

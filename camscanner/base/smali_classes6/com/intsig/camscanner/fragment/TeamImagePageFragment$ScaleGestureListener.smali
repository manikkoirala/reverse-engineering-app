.class Lcom/intsig/camscanner/fragment/TeamImagePageFragment$ScaleGestureListener;
.super Lcom/intsig/camscanner/multitouch/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source "TeamImagePageFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/fragment/TeamImagePageFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ScaleGestureListener"
.end annotation


# instance fields
.field final synthetic 〇080:Lcom/intsig/camscanner/fragment/TeamImagePageFragment;


# direct methods
.method private constructor <init>(Lcom/intsig/camscanner/fragment/TeamImagePageFragment;)V
    .locals 0

    .line 2
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/TeamImagePageFragment$ScaleGestureListener;->〇080:Lcom/intsig/camscanner/fragment/TeamImagePageFragment;

    invoke-direct {p0}, Lcom/intsig/camscanner/multitouch/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/intsig/camscanner/fragment/TeamImagePageFragment;L〇8〇o〇8/O88o〇;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/TeamImagePageFragment$ScaleGestureListener;-><init>(Lcom/intsig/camscanner/fragment/TeamImagePageFragment;)V

    return-void
.end method


# virtual methods
.method public 〇080(Lcom/intsig/camscanner/multitouch/ScaleGestureDetector;)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamImagePageFragment$ScaleGestureListener;->〇080:Lcom/intsig/camscanner/fragment/TeamImagePageFragment;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/fragment/TeamImagePageFragment;->O〇080〇o0(Lcom/intsig/camscanner/fragment/TeamImagePageFragment;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-nez v0, :cond_4

    .line 9
    .line 10
    if-nez p1, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamImagePageFragment$ScaleGestureListener;->〇080:Lcom/intsig/camscanner/fragment/TeamImagePageFragment;

    .line 14
    .line 15
    invoke-static {v0}, Lcom/intsig/camscanner/fragment/TeamImagePageFragment;->o88oo〇O(Lcom/intsig/camscanner/fragment/TeamImagePageFragment;)Lcom/intsig/camscanner/view/ImageViewTouch;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    if-nez v0, :cond_1

    .line 20
    .line 21
    return v1

    .line 22
    :cond_1
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getScale()F

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    invoke-virtual {p1}, Lcom/intsig/camscanner/multitouch/ScaleGestureDetector;->〇〇888()F

    .line 27
    .line 28
    .line 29
    move-result p1

    .line 30
    mul-float v2, v2, p1

    .line 31
    .line 32
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    invoke-virtual {p1}, Ljava/lang/Float;->isNaN()Z

    .line 37
    .line 38
    .line 39
    move-result v2

    .line 40
    if-eqz v2, :cond_2

    .line 41
    .line 42
    return v1

    .line 43
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    .line 44
    .line 45
    .line 46
    move-result v1

    .line 47
    const v2, 0x3f828f5c    # 1.02f

    .line 48
    .line 49
    .line 50
    cmpg-float v1, v1, v2

    .line 51
    .line 52
    if-gez v1, :cond_3

    .line 53
    .line 54
    const/high16 p1, 0x3f800000    # 1.0f

    .line 55
    .line 56
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 57
    .line 58
    .line 59
    move-result-object p1

    .line 60
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    .line 61
    .line 62
    .line 63
    move-result p1

    .line 64
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->oo88o8O(F)[F

    .line 65
    .line 66
    .line 67
    iget-object p1, p0, Lcom/intsig/camscanner/fragment/TeamImagePageFragment$ScaleGestureListener;->〇080:Lcom/intsig/camscanner/fragment/TeamImagePageFragment;

    .line 68
    .line 69
    const/4 v0, 0x1

    .line 70
    invoke-static {p1, v0}, Lcom/intsig/camscanner/fragment/TeamImagePageFragment;->〇0o0oO〇〇0(Lcom/intsig/camscanner/fragment/TeamImagePageFragment;Z)V

    .line 71
    .line 72
    .line 73
    return v0

    .line 74
    :cond_4
    :goto_0
    return v1
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

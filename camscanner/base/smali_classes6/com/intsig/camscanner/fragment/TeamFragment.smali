.class public Lcom/intsig/camscanner/fragment/TeamFragment;
.super Lcom/intsig/camscanner/fragment/BaseFragment;
.source "TeamFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/fragment/TeamFragment$NaviIconClickListener;,
        Lcom/intsig/camscanner/fragment/TeamFragment$TabletDevice;,
        Lcom/intsig/camscanner/fragment/TeamFragment$PhoneDevice;,
        Lcom/intsig/camscanner/fragment/TeamFragment$PermiRoleInterface;,
        Lcom/intsig/camscanner/fragment/TeamFragment$DeviceInterface;,
        Lcom/intsig/camscanner/fragment/TeamFragment$SyncListenerImpl;,
        Lcom/intsig/camscanner/fragment/TeamFragment$MyDialogFragment;,
        Lcom/intsig/camscanner/fragment/TeamFragment$NoDocViewControl;
    }
.end annotation


# static fields
.field private static final O0〇:Ljava/lang/String; = "TeamFragment"

.field public static O〇080〇o0:Z = false

.field private static O〇0O〇Oo〇o:I = 0x0

.field public static O〇〇O80o8:Z = false

.field private static o0Oo:Z = false

.field public static o808o8o08:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/datastruct/FolderItem;",
            ">;"
        }
    .end annotation
.end field

.field public static 〇oO88o:Ljava/lang/String;


# instance fields
.field private O08〇:Lcom/intsig/menu/PopupMenuItems;

.field private O0O:Landroid/widget/ProgressBar;

.field private final O0O0〇:I

.field private O0〇0:I

.field private O88O:I

.field private final O8o08O8O:I

.field private O8o〇O0:Landroidx/drawerlayout/widget/DrawerLayout;

.field private O8〇8〇O80:Landroidx/loader/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/loader/app/LoaderManager$LoaderCallbacks<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private O8〇o〇88:Landroid/widget/ImageView;

.field private final OO:I

.field private OO〇00〇8oO:Z

.field private OO〇OOo:Landroid/widget/TextView;

.field private Oo0O0o8:Landroid/widget/TextView;

.field private Oo0〇Ooo:[Ljava/lang/String;

.field private Oo80:Lcom/intsig/app/AlertDialog;

.field private Ooo08:Landroid/view/View;

.field private final Ooo8o:I

.field private O〇08oOOO0:Lcom/intsig/view/ImageTextButton;

.field private O〇8〇008:Ljava/lang/String;

.field private O〇O:Landroid/widget/EditText;

.field private O〇o88o08〇:I

.field private final o0:I

.field private o00〇88〇08:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/view/ImageTextButton;",
            ">;"
        }
    .end annotation
.end field

.field private o0OoOOo0:J

.field private o0〇〇00:I

.field private o88:Ljava/lang/String;

.field private o880:Landroid/view/View;

.field private o8O:Landroid/widget/TextView;

.field private o8o:J

.field private o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

.field private o8〇OO:Landroid/app/Dialog;

.field private o8〇OO0〇0o:Lcom/intsig/camscanner/fragment/TeamFragment$NoDocViewControl;

.field private oO00〇o:[I

.field private oOO0880O:Landroid/os/Handler;

.field private oOO8:Landroid/widget/LinearLayout;

.field private oOO〇〇:J

.field private oOo0:I

.field private oOoO8OO〇:Landroidx/fragment/app/DialogFragment;

.field private oOoo80oO:Lcom/intsig/view/ImageTextButton;

.field private oOo〇08〇:Landroid/view/View;

.field private final oOo〇8o008:I

.field private oO〇8O8oOo:Lcom/intsig/camscanner/datastruct/DocumentListItem;

.field private oO〇oo:Lcom/intsig/view/ImageTextButton;

.field private oo8ooo8O:Ljava/lang/String;

.field private ooO:Ljava/lang/String;

.field private ooo0〇〇O:Landroid/widget/GridView;

.field private oooO888:Landroidx/appcompat/app/AppCompatActivity;

.field private final o〇00O:I

.field private o〇0〇o:I

.field private o〇O8OO:Lcom/intsig/camscanner/view/PullToSyncView;

.field private o〇oO:J

.field private o〇oo:I

.field private o〇o〇Oo88:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private 〇00O0:Ljava/lang/String;

.field private final 〇080OO8〇0:I

.field private 〇088O:Z

.field private 〇08O:Lcom/intsig/view/ImageTextButton;

.field private final 〇08O〇00〇o:I

.field private 〇08〇o0O:[Ljava/lang/String;

.field private final 〇0O:I

.field private 〇0O〇O00O:Landroid/widget/AdapterView$OnItemClickListener;

.field private 〇0oO〇oo00:Landroid/widget/FrameLayout;

.field private 〇0ooOOo:Landroidx/loader/app/LoaderManager$LoaderCallbacks;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/loader/app/LoaderManager$LoaderCallbacks<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private 〇0〇0:Landroid/view/View;

.field private 〇800OO〇0O:Landroidx/appcompat/widget/Toolbar;

.field private 〇80O8o8O〇:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

.field private 〇8O0880:Landroidx/appcompat/app/ActionBar;

.field 〇8〇80o:Landroid/view/View$OnTouchListener;

.field private 〇8〇OOoooo:Z

.field private 〇8〇o88:Landroid/widget/RelativeLayout;

.field private 〇8〇oO〇〇8o:Landroid/widget/AbsListView;

.field private 〇O0o〇〇o:Z

.field private 〇O8oOo0:Lcom/intsig/view/ImageTextButton;

.field private 〇O8〇8000:Lcom/intsig/camscanner/fragment/TeamFragment$DeviceInterface;

.field private 〇O8〇8O0oO:Lcom/intsig/camscanner/tsapp/sync/TeamDownloadStateMonitor;

.field private 〇OO8ooO8〇:Lcom/intsig/camscanner/control/ShareControl$OcrLanguageSetting;

.field private final 〇OOo8〇0:I

.field private 〇OO〇00〇0O:Ljava/lang/String;

.field private 〇Oo〇O:Ljava/lang/String;

.field private 〇O〇〇O8:I

.field private 〇o08:Z

.field private 〇o0O:I

.field private 〇oO〇08o:I

.field private 〇oo〇O〇80:Landroid/widget/ImageView;

.field 〇o〇88〇8:Lcom/intsig/camscanner/adapter/QueryInterface;

.field private 〇〇:Lcom/intsig/menu/PopupListMenu;

.field private 〇〇08O:Landroid/widget/AbsListView;

.field private 〇〇O80〇0o:Landroid/widget/EditText;

.field private 〇〇o0〇8:Ljava/lang/String;

.field private 〇〇o〇:Ljava/lang/String;

.field private 〇〇〇0:Lcom/intsig/camscanner/tsapp/SyncListener;

.field private 〇〇〇00:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/datastruct/DocumentListItem;",
            ">;"
        }
    .end annotation
.end field

.field private 〇〇〇0o〇〇0:Ljava/lang/CharSequence;

.field private 〇〇〇O〇:Lcom/intsig/camscanner/fragment/TeamFragment$NaviIconClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->o808o8o08:Ljava/util/ArrayList;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/BaseFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o0:I

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    iput v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇OOo8〇0:I

    .line 9
    .line 10
    const/16 v2, 0x66

    .line 11
    .line 12
    iput v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->OO:I

    .line 13
    .line 14
    const/16 v2, 0x7c

    .line 15
    .line 16
    iput v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇08O〇00〇o:I

    .line 17
    .line 18
    const/16 v2, 0x80

    .line 19
    .line 20
    iput v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇00O:I

    .line 21
    .line 22
    const/16 v2, 0x81

    .line 23
    .line 24
    iput v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O8o08O8O:I

    .line 25
    .line 26
    const/16 v2, 0x82

    .line 27
    .line 28
    iput v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇080OO8〇0:I

    .line 29
    .line 30
    const/16 v2, 0x83

    .line 31
    .line 32
    iput v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0O:I

    .line 33
    .line 34
    const/16 v2, 0x85

    .line 35
    .line 36
    iput v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOo〇8o008:I

    .line 37
    .line 38
    iput v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOo0:I

    .line 39
    .line 40
    iput-boolean v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->OO〇00〇8oO:Z

    .line 41
    .line 42
    const/16 v2, -0x3e7

    .line 43
    .line 44
    iput v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O88O:I

    .line 45
    .line 46
    const-wide/16 v2, -0x2

    .line 47
    .line 48
    iput-wide v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇oO:J

    .line 49
    .line 50
    const/4 v2, 0x0

    .line 51
    iput-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇08〇o0O:[Ljava/lang/String;

    .line 52
    .line 53
    const-string v3, ""

    .line 54
    .line 55
    iput-object v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇o〇:Ljava/lang/String;

    .line 56
    .line 57
    const-string v3, "origin_parent_sync_id"

    .line 58
    .line 59
    iput-object v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇OO〇00〇0O:Ljava/lang/String;

    .line 60
    .line 61
    iput-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->Oo0〇Ooo:[Ljava/lang/String;

    .line 62
    .line 63
    new-instance v3, Lcom/intsig/camscanner/fragment/TeamFragment$2;

    .line 64
    .line 65
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$2;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 66
    .line 67
    .line 68
    iput-object v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0O〇O00O:Landroid/widget/AdapterView$OnItemClickListener;

    .line 69
    .line 70
    new-instance v3, Lcom/intsig/camscanner/fragment/TeamFragment$3;

    .line 71
    .line 72
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$3;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 73
    .line 74
    .line 75
    iput-object v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇o〇Oo88:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 76
    .line 77
    const/4 v3, 0x6

    .line 78
    new-array v3, v3, [I

    .line 79
    .line 80
    fill-array-data v3, :array_0

    .line 81
    .line 82
    .line 83
    iput-object v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oO00〇o:[I

    .line 84
    .line 85
    new-instance v3, Landroid/os/Handler;

    .line 86
    .line 87
    new-instance v4, Lcom/intsig/camscanner/fragment/TeamFragment$4;

    .line 88
    .line 89
    invoke-direct {v4, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$4;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 90
    .line 91
    .line 92
    invoke-direct {v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    .line 93
    .line 94
    .line 95
    iput-object v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO0880O:Landroid/os/Handler;

    .line 96
    .line 97
    iput-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇o0〇8:Ljava/lang/String;

    .line 98
    .line 99
    iput v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇0:I

    .line 100
    .line 101
    iput-boolean v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇OOoooo:Z

    .line 102
    .line 103
    iput v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇0〇o:I

    .line 104
    .line 105
    iput-boolean v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇088O:Z

    .line 106
    .line 107
    new-instance v3, Lcom/intsig/camscanner/fragment/TeamFragment$8;

    .line 108
    .line 109
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$8;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 110
    .line 111
    .line 112
    iput-object v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇o〇88〇8:Lcom/intsig/camscanner/adapter/QueryInterface;

    .line 113
    .line 114
    new-instance v3, Lcom/intsig/camscanner/fragment/TeamFragment$9;

    .line 115
    .line 116
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$9;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 117
    .line 118
    .line 119
    iput-object v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇80o:Landroid/view/View$OnTouchListener;

    .line 120
    .line 121
    iput-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOoO8OO〇:Landroidx/fragment/app/DialogFragment;

    .line 122
    .line 123
    sget v3, Lcom/intsig/camscanner/util/CursorLoaderId;->o〇0:I

    .line 124
    .line 125
    iput v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->Ooo8o:I

    .line 126
    .line 127
    iput-boolean v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇o08:Z

    .line 128
    .line 129
    new-instance v3, Lcom/intsig/camscanner/fragment/TeamFragment$SyncListenerImpl;

    .line 130
    .line 131
    invoke-direct {v3, p0, v2}, Lcom/intsig/camscanner/fragment/TeamFragment$SyncListenerImpl;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;L〇8〇o〇8/〇Oo〇o8;)V

    .line 132
    .line 133
    .line 134
    iput-object v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇〇0:Lcom/intsig/camscanner/tsapp/SyncListener;

    .line 135
    .line 136
    iput-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇O80〇0o:Landroid/widget/EditText;

    .line 137
    .line 138
    iput-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0oO〇oo00:Landroid/widget/FrameLayout;

    .line 139
    .line 140
    iput-boolean v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O0o〇〇o:Z

    .line 141
    .line 142
    sget v3, Lcom/intsig/camscanner/util/CursorLoaderId;->〇〇888:I

    .line 143
    .line 144
    iput v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0O0〇:I

    .line 145
    .line 146
    iput-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 147
    .line 148
    iput v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇oo:I

    .line 149
    .line 150
    iput-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o88:Ljava/lang/String;

    .line 151
    .line 152
    iput-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O8〇8O0oO:Lcom/intsig/camscanner/tsapp/sync/TeamDownloadStateMonitor;

    .line 153
    .line 154
    iput v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o0〇〇00:I

    .line 155
    .line 156
    const/4 v0, 0x2

    .line 157
    iput v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO〇08o:I

    .line 158
    .line 159
    return-void

    .line 160
    nop

    .line 161
    :array_0
    .array-data 4
        0x5
        0x6
        0x9
        0xc
        0xe
        0xf
    .end array-data
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic O008o8oo()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic O008oO0(Lcom/intsig/camscanner/fragment/TeamFragment;JZ)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/fragment/TeamFragment;->o00(JZ)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic O00OoO〇(Lcom/intsig/camscanner/fragment/TeamFragment;Lcom/intsig/view/ImageTextButton;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇08oOOO0:Lcom/intsig/view/ImageTextButton;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O00o(Ljava/util/ArrayList;Z)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/datastruct/DocumentListItem;",
            ">;Z)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    const/4 v2, 0x1

    .line 10
    if-eqz v1, :cond_1

    .line 11
    .line 12
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    check-cast v1, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 17
    .line 18
    iget-object v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 19
    .line 20
    invoke-virtual {v1}, Lcom/intsig/webstorage/UploadFile;->〇080()J

    .line 21
    .line 22
    .line 23
    move-result-wide v4

    .line 24
    invoke-static {v3, v4, v5}, Lcom/intsig/camscanner/util/Util;->OOo8o〇O(Landroid/content/Context;J)Z

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    if-eqz v1, :cond_0

    .line 29
    .line 30
    const/4 v0, 0x1

    .line 31
    goto :goto_0

    .line 32
    :cond_1
    const/4 v0, 0x0

    .line 33
    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    .line 34
    .line 35
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 39
    .line 40
    .line 41
    move-result-object v3

    .line 42
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 43
    .line 44
    .line 45
    move-result v4

    .line 46
    if-eqz v4, :cond_2

    .line 47
    .line 48
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 49
    .line 50
    .line 51
    move-result-object v4

    .line 52
    check-cast v4, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 53
    .line 54
    iget-wide v4, v4, Lcom/intsig/webstorage/UploadFile;->OO:J

    .line 55
    .line 56
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 57
    .line 58
    .line 59
    move-result-object v4

    .line 60
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 61
    .line 62
    .line 63
    goto :goto_1

    .line 64
    :cond_2
    xor-int/lit8 v8, v0, 0x1

    .line 65
    .line 66
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 67
    .line 68
    new-instance v2, L〇8〇o〇8/〇o〇Oo0;

    .line 69
    .line 70
    move-object v3, v2

    .line 71
    move-object v4, p0

    .line 72
    move-object v5, p1

    .line 73
    move-object v6, v1

    .line 74
    move v7, p2

    .line 75
    invoke-direct/range {v3 .. v8}, L〇8〇o〇8/〇o〇Oo0;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;Ljava/util/ArrayList;Ljava/util/ArrayList;ZZ)V

    .line 76
    .line 77
    .line 78
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/control/DataChecker;->〇O8o08O(Landroid/app/Activity;Ljava/util/ArrayList;Lcom/intsig/camscanner/control/DataChecker$ActionListener;)Z

    .line 79
    .line 80
    .line 81
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private O00〇8(Z)V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->〇O00()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, -0x1

    .line 8
    const/4 v2, 0x1

    .line 9
    const/4 v3, 0x0

    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    if-lez v0, :cond_2

    .line 13
    .line 14
    :goto_0
    const/4 p1, 0x1

    .line 15
    goto :goto_1

    .line 16
    :cond_0
    if-nez v0, :cond_1

    .line 17
    .line 18
    const/4 p1, -0x1

    .line 19
    goto :goto_1

    .line 20
    :cond_1
    if-ne v0, v2, :cond_2

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_2
    const/4 p1, 0x0

    .line 24
    :goto_1
    if-eq p1, v2, :cond_3

    .line 25
    .line 26
    if-ne p1, v1, :cond_6

    .line 27
    .line 28
    :cond_3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    const v1, 0x7f06007b

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    .line 36
    .line 37
    .line 38
    move-result v0

    .line 39
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    const v4, 0x7f06007e

    .line 44
    .line 45
    .line 46
    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getColor(I)I

    .line 47
    .line 48
    .line 49
    move-result v1

    .line 50
    iget-object v4, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o00〇88〇08:Ljava/util/ArrayList;

    .line 51
    .line 52
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 53
    .line 54
    .line 55
    move-result-object v4

    .line 56
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    .line 57
    .line 58
    .line 59
    move-result v5

    .line 60
    if-eqz v5, :cond_6

    .line 61
    .line 62
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 63
    .line 64
    .line 65
    move-result-object v5

    .line 66
    check-cast v5, Lcom/intsig/view/ImageTextButton;

    .line 67
    .line 68
    if-ne p1, v2, :cond_4

    .line 69
    .line 70
    const/4 v6, 0x1

    .line 71
    goto :goto_3

    .line 72
    :cond_4
    const/4 v6, 0x0

    .line 73
    :goto_3
    invoke-virtual {v5, v6}, Landroid/view/View;->setEnabled(Z)V

    .line 74
    .line 75
    .line 76
    if-ne p1, v2, :cond_5

    .line 77
    .line 78
    move v6, v0

    .line 79
    goto :goto_4

    .line 80
    :cond_5
    move v6, v1

    .line 81
    :goto_4
    invoke-virtual {v5, v6}, Lcom/intsig/view/ImageTextButton;->setIconAndTextColor(I)V

    .line 82
    .line 83
    .line 84
    goto :goto_2

    .line 85
    :cond_6
    return-void
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private O00〇o00()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO0880O:Landroid/os/Handler;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/fragment/TeamFragment$21;

    .line 4
    .line 5
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$21;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic O088O(Lcom/intsig/camscanner/fragment/TeamFragment;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO〇0o8〇()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O08o(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8o8o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O08〇(Lcom/intsig/camscanner/fragment/TeamFragment;)[Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇08〇o0O:[Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O08〇oO8〇(Lcom/intsig/camscanner/fragment/TeamFragment;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O8o0〇()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O0O0〇(Lcom/intsig/camscanner/fragment/TeamFragment;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇o0O:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O0Oo〇8()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "User Operation: save to gallery"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇88()Ljava/util/ArrayList;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    if-lez v1, :cond_0

    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 21
    .line 22
    new-instance v2, L〇8〇o〇8/OO〇0008O8;

    .line 23
    .line 24
    invoke-direct {v2, p0, v0}, L〇8〇o〇8/OO〇0008O8;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;Ljava/util/ArrayList;)V

    .line 25
    .line 26
    .line 27
    invoke-static {v1, v0, v2}, Lcom/intsig/camscanner/control/DataChecker;->〇O8o08O(Landroid/app/Activity;Ljava/util/ArrayList;Lcom/intsig/camscanner/control/DataChecker$ActionListener;)Z

    .line 28
    .line 29
    .line 30
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇O()V

    .line 31
    .line 32
    .line 33
    const/4 v0, 0x0

    .line 34
    iput-boolean v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->OO〇00〇8oO:Z

    .line 35
    .line 36
    :cond_0
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic O0o0(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O0o0〇8o(JILandroid/net/Uri;)V
    .locals 4

    .line 1
    new-instance v0, Landroid/content/Intent;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    const-class v2, Lcom/intsig/camscanner/ImageScannerActivity;

    .line 6
    .line 7
    const-string v3, "com.intsig.camscanner.NEW_DOC"

    .line 8
    .line 9
    invoke-direct {v0, v3, p4, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 10
    .line 11
    .line 12
    const-string p4, "scanner_image_src"

    .line 13
    .line 14
    invoke-virtual {v0, p4, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 15
    .line 16
    .line 17
    const-string p3, "tag_id"

    .line 18
    .line 19
    invoke-virtual {v0, p3, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 20
    .line 21
    .line 22
    const-string p1, "extra_folder_id"

    .line 23
    .line 24
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO〇0o8〇()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object p2

    .line 28
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 29
    .line 30
    .line 31
    const-string p1, "team_token"

    .line 32
    .line 33
    iget-object p2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 34
    .line 35
    invoke-virtual {v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 36
    .line 37
    .line 38
    const/16 p1, 0x6c

    .line 39
    .line 40
    invoke-virtual {p0, v0, p1}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic O0oO(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0〇〇o0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O0oOo(J)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o00(JZ)V

    .line 3
    .line 4
    .line 5
    iget-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->notifyDataSetChanged()V

    .line 8
    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇00O00o()V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O00〇8(Z)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O0〇(Lcom/intsig/camscanner/fragment/TeamFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o880:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O0〇0(Lcom/intsig/camscanner/fragment/TeamFragment;)Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O0〇0o8O()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "onLevelBack sParentSyncId:"

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    sget-object v2, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO88o:Ljava/lang/String;

    .line 14
    .line 15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->o808o8o08:Ljava/util/ArrayList;

    .line 26
    .line 27
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    const/4 v1, 0x1

    .line 32
    if-ne v0, v1, :cond_0

    .line 33
    .line 34
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->o808o8o08:Ljava/util/ArrayList;

    .line 35
    .line 36
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 37
    .line 38
    .line 39
    move-result v2

    .line 40
    sub-int/2addr v2, v1

    .line 41
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 42
    .line 43
    .line 44
    const/4 v0, 0x0

    .line 45
    sput-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO88o:Ljava/lang/String;

    .line 46
    .line 47
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇8Oo()V

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->o808o8o08:Ljava/util/ArrayList;

    .line 52
    .line 53
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 54
    .line 55
    .line 56
    move-result v2

    .line 57
    sub-int/2addr v2, v1

    .line 58
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 59
    .line 60
    .line 61
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->o808o8o08:Ljava/util/ArrayList;

    .line 62
    .line 63
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 64
    .line 65
    .line 66
    move-result v2

    .line 67
    sub-int/2addr v2, v1

    .line 68
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 69
    .line 70
    .line 71
    move-result-object v0

    .line 72
    check-cast v0, Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 73
    .line 74
    if-eqz v0, :cond_1

    .line 75
    .line 76
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/FolderItem;->o〇O8〇〇o()Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    sput-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO88o:Ljava/lang/String;

    .line 81
    .line 82
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇OO〇00〇0O:Ljava/lang/String;

    .line 83
    .line 84
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO〇0o8〇()Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v1

    .line 88
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 89
    .line 90
    .line 91
    move-result v0

    .line 92
    if-eqz v0, :cond_2

    .line 93
    .line 94
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇O0ooo()V

    .line 95
    .line 96
    .line 97
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇88()V

    .line 98
    .line 99
    .line 100
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇00O()V

    .line 101
    .line 102
    .line 103
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->Oo088O〇8〇()V

    .line 104
    .line 105
    .line 106
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0OOoO8O0()V

    .line 107
    .line 108
    .line 109
    return-void
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic O0〇8〇(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->OoO888()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O0〇O80ooo()V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "User Operation: menu sort"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSBusiness"

    .line 9
    .line 10
    const-string v1, "sort"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    new-instance v0, Lcom/intsig/camscanner/view/DocSortDialog;

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 18
    .line 19
    new-instance v2, Lcom/intsig/camscanner/fragment/TeamFragment$15;

    .line 20
    .line 21
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$15;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 22
    .line 23
    .line 24
    const/4 v3, 0x0

    .line 25
    invoke-direct {v0, v1, v3, v2}, Lcom/intsig/camscanner/view/DocSortDialog;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/intsig/camscanner/view/DocSortDialog$OnNewSortOrderListener;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/DocSortDialog;->Oo08()V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private O0〇o8o〇〇()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇0〇〇8O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const/16 v0, 0x6a

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    invoke-static {p0, v0, v1}, Lcom/intsig/camscanner/app/IntentUtil;->O〇8O8〇008(Landroidx/fragment/app/Fragment;IZ)V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private O80(Ljava/util/ArrayList;Z)J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/datastruct/DocumentListItem;",
            ">;Z)J"
        }
    .end annotation

    .line 1
    const-wide/16 v0, 0x0

    .line 2
    .line 3
    if-eqz p1, :cond_1

    .line 4
    .line 5
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 10
    .line 11
    .line 12
    move-result v2

    .line 13
    if-eqz v2, :cond_1

    .line 14
    .line 15
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    check-cast v2, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 20
    .line 21
    if-eqz p2, :cond_0

    .line 22
    .line 23
    iget-object v3, v2, Lcom/intsig/webstorage/UploadFile;->〇OOo8〇0:Ljava/lang/String;

    .line 24
    .line 25
    invoke-static {v3}, Lcom/intsig/utils/FileUtil;->〇O888o0o(Ljava/lang/String;)J

    .line 26
    .line 27
    .line 28
    move-result-wide v3

    .line 29
    goto :goto_1

    .line 30
    :cond_0
    iget-object v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 31
    .line 32
    invoke-virtual {v2}, Lcom/intsig/webstorage/UploadFile;->〇080()J

    .line 33
    .line 34
    .line 35
    move-result-wide v4

    .line 36
    invoke-static {v3, v4, v5}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->estimateDocsPDFSize(Landroid/content/Context;J)J

    .line 37
    .line 38
    .line 39
    move-result-wide v3

    .line 40
    :goto_1
    add-long/2addr v0, v3

    .line 41
    sget-object v3, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 42
    .line 43
    new-instance v4, Ljava/lang/StringBuilder;

    .line 44
    .line 45
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 46
    .line 47
    .line 48
    const-string v5, "getPDFSize size="

    .line 49
    .line 50
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    const-string v5, " path="

    .line 57
    .line 58
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    iget-object v2, v2, Lcom/intsig/webstorage/UploadFile;->〇OOo8〇0:Ljava/lang/String;

    .line 62
    .line 63
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object v2

    .line 70
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    goto :goto_0

    .line 74
    :cond_1
    return-wide v0
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private O8080〇O8o(Landroid/content/res/Configuration;)V
    .locals 3

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o880:Landroid/view/View;

    .line 4
    .line 5
    const v1, 0x7f0a0d7e

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/view/PullToSyncView;

    .line 13
    .line 14
    if-eqz v0, :cond_2

    .line 15
    .line 16
    const v1, 0x7f0a1171

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    check-cast v0, Landroid/widget/RelativeLayout;

    .line 24
    .line 25
    if-eqz v0, :cond_2

    .line 26
    .line 27
    iget p1, p1, Landroid/content/res/Configuration;->orientation:I

    .line 28
    .line 29
    const/4 v1, 0x2

    .line 30
    if-ne p1, v1, :cond_0

    .line 31
    .line 32
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    const v1, 0x7f07054d

    .line 37
    .line 38
    .line 39
    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 40
    .line 41
    .line 42
    move-result p1

    .line 43
    goto :goto_0

    .line 44
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    const v1, 0x7f07054f

    .line 49
    .line 50
    .line 51
    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 52
    .line 53
    .line 54
    move-result p1

    .line 55
    :goto_0
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 56
    .line 57
    .line 58
    move-result-object v1

    .line 59
    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 60
    .line 61
    const/4 v2, 0x0

    .line 62
    invoke-virtual {v1, p1, v2, p1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 66
    .line 67
    .line 68
    goto :goto_1

    .line 69
    :cond_1
    sget-object p1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 70
    .line 71
    const-string v0, "updatePullToSyncViewMargin newConfig is null"

    .line 72
    .line 73
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    :cond_2
    :goto_1
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method static bridge synthetic O80OO(Lcom/intsig/camscanner/fragment/TeamFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇00O0:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O80〇()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "User Operation: delete"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 9
    .line 10
    new-instance v1, Lcom/intsig/camscanner/fragment/TeamFragment$36;

    .line 11
    .line 12
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$36;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 13
    .line 14
    .line 15
    invoke-static {v0, v1}, Lcom/intsig/camscanner/control/ConnectChecker;->〇080(Landroid/content/Context;Lcom/intsig/camscanner/control/ConnectChecker$ActionListener;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method private O80〇〇o()V
    .locals 2

    .line 1
    sget-boolean v0, Lcom/intsig/camscanner/app/AppConfig;->〇o00〇〇Oo:Z

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    sget-boolean v0, Lcom/intsig/camscanner/app/AppConfig;->O8:Z

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/fragment/TeamFragment$TabletDevice;

    .line 12
    .line 13
    invoke-direct {v0, p0, v1}, Lcom/intsig/camscanner/fragment/TeamFragment$TabletDevice;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;L〇8〇o〇8/OoO〇;)V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O8〇8000:Lcom/intsig/camscanner/fragment/TeamFragment$DeviceInterface;

    .line 17
    .line 18
    goto :goto_1

    .line 19
    :cond_1
    :goto_0
    new-instance v0, Lcom/intsig/camscanner/fragment/TeamFragment$PhoneDevice;

    .line 20
    .line 21
    invoke-direct {v0, p0, v1}, Lcom/intsig/camscanner/fragment/TeamFragment$PhoneDevice;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;L〇8〇o〇8/Oo08OO8oO;)V

    .line 22
    .line 23
    .line 24
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O8〇8000:Lcom/intsig/camscanner/fragment/TeamFragment$DeviceInterface;

    .line 25
    .line 26
    :goto_1
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic O88(Lcom/intsig/camscanner/fragment/TeamFragment;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O0o〇〇o:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic O880O〇(Lcom/intsig/camscanner/fragment/TeamFragment;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->OO〇00〇8oO:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic O888Oo(Lcom/intsig/camscanner/fragment/TeamFragment;)Landroid/view/View;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8o088〇0()Landroid/view/View;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O88Oo8()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO88o:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_1

    .line 8
    .line 9
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO88o:Ljava/lang/String;

    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇o0〇8:Ljava/lang/String;

    .line 12
    .line 13
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const/4 v0, 0x0

    .line 21
    goto :goto_1

    .line 22
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 23
    :goto_1
    return v0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic O8O(Lcom/intsig/camscanner/fragment/TeamFragment;Lcom/intsig/view/ImageTextButton;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOoo80oO:Lcom/intsig/view/ImageTextButton;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O8O〇8〇〇8〇(Landroid/view/View;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0oO〇oo00:Landroid/widget/FrameLayout;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 4
    .line 5
    .line 6
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    .line 7
    .line 8
    const/4 v1, -0x2

    .line 9
    const/4 v2, -0x1

    .line 10
    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 11
    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0oO〇oo00:Landroid/widget/FrameLayout;

    .line 14
    .line 15
    invoke-virtual {v1, p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
.end method

.method private O8o()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "SearchView onClose "

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇08〇o0O:[Ljava/lang/String;

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇O:Landroid/widget/EditText;

    .line 12
    .line 13
    const-string v1, ""

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 19
    .line 20
    invoke-static {v0}, Lcom/intsig/utils/SoftKeyboardUtils;->〇080(Landroid/app/Activity;)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇800OO〇0O:Landroidx/appcompat/widget/Toolbar;

    .line 24
    .line 25
    const/4 v1, 0x0

    .line 26
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 27
    .line 28
    .line 29
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO8:Landroid/widget/LinearLayout;

    .line 30
    .line 31
    const/16 v2, 0x8

    .line 32
    .line 33
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 34
    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇o88:Landroid/widget/RelativeLayout;

    .line 37
    .line 38
    if-eqz v0, :cond_0

    .line 39
    .line 40
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 41
    .line 42
    .line 43
    :cond_0
    iput v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOo0:I

    .line 44
    .line 45
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇00O()V

    .line 46
    .line 47
    .line 48
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private O8o0〇()Ljava/lang/String;
    .locals 6

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇08〇o0O:[Ljava/lang/String;

    .line 7
    .line 8
    array-length v1, v1

    .line 9
    const/4 v2, 0x0

    .line 10
    :goto_0
    if-ge v2, v1, :cond_1

    .line 11
    .line 12
    const-string v3, "(_id in (select _id from documents where title like ?) or _id in (select document_id from images where (note like ? or image_titile like ? or ocr_result like ? or ocr_result_user like ?)))"

    .line 13
    .line 14
    if-nez v2, :cond_0

    .line 15
    .line 16
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    goto :goto_1

    .line 20
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    .line 21
    .line 22
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 23
    .line 24
    .line 25
    const-string v5, " and "

    .line 26
    .line 27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v3

    .line 37
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    :goto_1
    add-int/lit8 v2, v2, 0x1

    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    return-object v0
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic O8〇(Lcom/intsig/camscanner/fragment/TeamFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇8O〇(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic O8〇8〇O80(Lcom/intsig/camscanner/fragment/TeamFragment;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8o:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O8〇o0〇〇()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->OOO8(Landroid/content/Context;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 10
    .line 11
    const-string v1, "showTeamIntroduce"

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 17
    .line 18
    const/4 v1, 0x0

    .line 19
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇o08(Landroid/content/Context;Z)V

    .line 20
    .line 21
    .line 22
    new-instance v0, Landroid/app/Dialog;

    .line 23
    .line 24
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 25
    .line 26
    const v3, 0x7f1401d6

    .line 27
    .line 28
    .line 29
    invoke-direct {v0, v2, v3}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 33
    .line 34
    .line 35
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 36
    .line 37
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    const v2, 0x7f0d074f

    .line 42
    .line 43
    .line 44
    const/4 v3, 0x0

    .line 45
    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 50
    .line 51
    .line 52
    const v2, 0x7f0a1547

    .line 53
    .line 54
    .line 55
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    const v3, 0x7f0a087d

    .line 60
    .line 61
    .line 62
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 63
    .line 64
    .line 65
    move-result-object v1

    .line 66
    new-instance v3, Lcom/intsig/camscanner/fragment/TeamFragment$39;

    .line 67
    .line 68
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$39;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 69
    .line 70
    .line 71
    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    .line 73
    .line 74
    new-instance v2, Lcom/intsig/camscanner/fragment/TeamFragment$40;

    .line 75
    .line 76
    invoke-direct {v2, p0, v0}, Lcom/intsig/camscanner/fragment/TeamFragment$40;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;Landroid/app/Dialog;)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    .line 81
    .line 82
    :try_start_0
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    .line 84
    .line 85
    goto :goto_0

    .line 86
    :catch_0
    move-exception v0

    .line 87
    sget-object v1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 88
    .line 89
    const-string v2, "show introduceDlg"

    .line 90
    .line 91
    invoke-static {v1, v2, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 92
    .line 93
    .line 94
    :cond_0
    :goto_0
    return-void
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic O8〇o0〇〇8(Lcom/intsig/camscanner/fragment/TeamFragment;[Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇08〇o0O:[Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private OO00〇0o〇〇(Landroid/view/View;Landroid/content/DialogInterface;)V
    .locals 6

    .line 1
    const v0, 0x7f0a0f2c

    .line 2
    .line 3
    .line 4
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    check-cast p1, Landroid/widget/EditText;

    .line 9
    .line 10
    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇〇00:Ljava/util/ArrayList;

    .line 23
    .line 24
    if-eqz v1, :cond_8

    .line 25
    .line 26
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    const/4 v3, 0x2

    .line 31
    if-ge v2, v3, :cond_0

    .line 32
    .line 33
    goto/16 :goto_2

    .line 34
    .line 35
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 36
    .line 37
    .line 38
    move-result v2

    .line 39
    const/4 v3, 0x0

    .line 40
    if-eqz v2, :cond_1

    .line 41
    .line 42
    invoke-static {p2, v3}, Lcom/intsig/camscanner/app/AppUtil;->Oooo8o0〇(Landroid/content/DialogInterface;Z)V

    .line 43
    .line 44
    .line 45
    iget-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 46
    .line 47
    const p2, 0x7f13026e

    .line 48
    .line 49
    .line 50
    invoke-static {p1, p2}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 51
    .line 52
    .line 53
    return-void

    .line 54
    :cond_1
    iget-boolean v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇o08:Z

    .line 55
    .line 56
    const/4 v4, 0x1

    .line 57
    if-eqz v2, :cond_3

    .line 58
    .line 59
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO〇0o8〇()Ljava/lang/String;

    .line 60
    .line 61
    .line 62
    move-result-object v2

    .line 63
    iget-object v5, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 64
    .line 65
    invoke-static {v2, v0, v5, p2}, Lcom/intsig/camscanner/util/Util;->〇o〇(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Landroid/content/DialogInterface;)Z

    .line 66
    .line 67
    .line 68
    move-result v2

    .line 69
    if-eqz v2, :cond_2

    .line 70
    .line 71
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 72
    .line 73
    invoke-static {v2, p1}, Lcom/intsig/utils/SoftKeyboardUtils;->〇o00〇〇Oo(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 74
    .line 75
    .line 76
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇o8〇〇(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    invoke-static {p2, v4}, Lcom/intsig/camscanner/app/AppUtil;->Oooo8o0〇(Landroid/content/DialogInterface;Z)V

    .line 80
    .line 81
    .line 82
    goto :goto_1

    .line 83
    :cond_2
    invoke-static {p2, v3}, Lcom/intsig/camscanner/app/AppUtil;->Oooo8o0〇(Landroid/content/DialogInterface;Z)V

    .line 84
    .line 85
    .line 86
    goto :goto_1

    .line 87
    :cond_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 88
    .line 89
    .line 90
    move-result-object v2

    .line 91
    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    .line 92
    .line 93
    .line 94
    move-result v5

    .line 95
    if-eqz v5, :cond_5

    .line 96
    .line 97
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 98
    .line 99
    .line 100
    move-result-object v5

    .line 101
    check-cast v5, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 102
    .line 103
    iget-object v5, v5, Lcom/intsig/webstorage/UploadFile;->o0:Ljava/lang/String;

    .line 104
    .line 105
    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 106
    .line 107
    .line 108
    move-result v5

    .line 109
    if-eqz v5, :cond_4

    .line 110
    .line 111
    const/4 v2, 0x1

    .line 112
    goto :goto_0

    .line 113
    :cond_5
    const/4 v2, 0x0

    .line 114
    :goto_0
    if-eqz v2, :cond_6

    .line 115
    .line 116
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 117
    .line 118
    invoke-static {v2, p1}, Lcom/intsig/utils/SoftKeyboardUtils;->〇o00〇〇Oo(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 119
    .line 120
    .line 121
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇o8〇〇(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 122
    .line 123
    .line 124
    invoke-static {p2, v4}, Lcom/intsig/camscanner/app/AppUtil;->Oooo8o0〇(Landroid/content/DialogInterface;Z)V

    .line 125
    .line 126
    .line 127
    goto :goto_1

    .line 128
    :cond_6
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO〇0o8〇()Ljava/lang/String;

    .line 129
    .line 130
    .line 131
    move-result-object v2

    .line 132
    iget-object v5, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 133
    .line 134
    invoke-static {v2, v0, v5, p2}, Lcom/intsig/camscanner/util/Util;->〇o〇(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Landroid/content/DialogInterface;)Z

    .line 135
    .line 136
    .line 137
    move-result v2

    .line 138
    if-eqz v2, :cond_7

    .line 139
    .line 140
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 141
    .line 142
    invoke-static {v2, p1}, Lcom/intsig/utils/SoftKeyboardUtils;->〇o00〇〇Oo(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 143
    .line 144
    .line 145
    invoke-direct {p0, v1, v0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇o8〇〇(Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 146
    .line 147
    .line 148
    invoke-static {p2, v4}, Lcom/intsig/camscanner/app/AppUtil;->Oooo8o0〇(Landroid/content/DialogInterface;Z)V

    .line 149
    .line 150
    .line 151
    goto :goto_1

    .line 152
    :cond_7
    invoke-static {p2, v3}, Lcom/intsig/camscanner/app/AppUtil;->Oooo8o0〇(Landroid/content/DialogInterface;Z)V

    .line 153
    .line 154
    .line 155
    :goto_1
    return-void

    .line 156
    :cond_8
    :goto_2
    sget-object p1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 157
    .line 158
    const-string p2, "doMerge docs number invalid"

    .line 159
    .line 160
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    .line 162
    .line 163
    iget-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 164
    .line 165
    const p2, 0x7f131d83

    .line 166
    .line 167
    .line 168
    invoke-static {p1, p2}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 169
    .line 170
    .line 171
    return-void
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method static bridge synthetic OO0O(Lcom/intsig/camscanner/fragment/TeamFragment;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OO0o(Lcom/intsig/camscanner/fragment/TeamFragment;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇o0O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private OO0o88(Lcom/intsig/camscanner/datastruct/FolderItem;)V
    .locals 2

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    sget-object p1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 4
    .line 5
    const-string v0, "User Operation: open folderItem , item == null"

    .line 6
    .line 7
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOo0:I

    .line 12
    .line 13
    const/4 v1, 0x1

    .line 14
    if-ne v0, v1, :cond_1

    .line 15
    .line 16
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oo〇88()V

    .line 17
    .line 18
    .line 19
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/FolderItem;->o〇O8〇〇o()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    sput-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO88o:Ljava/lang/String;

    .line 24
    .line 25
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->o808o8o08:Ljava/util/ArrayList;

    .line 26
    .line 27
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 28
    .line 29
    .line 30
    sget-object p1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 31
    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    const-string v1, "User Operation: open folderItem , syncId:"

    .line 38
    .line 39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    sget-object v1, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO88o:Ljava/lang/String;

    .line 43
    .line 44
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    invoke-virtual {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇88()V

    .line 55
    .line 56
    .line 57
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇00O()V

    .line 58
    .line 59
    .line 60
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->Oo088O〇8〇()V

    .line 61
    .line 62
    .line 63
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0OOoO8O0()V

    .line 64
    .line 65
    .line 66
    :goto_0
    return-void
    .line 67
.end method

.method static bridge synthetic OO0〇O(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇8Oo()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OO8〇O8(Lcom/intsig/camscanner/fragment/TeamFragment;Landroid/view/View;Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/fragment/TeamFragment;->OO00〇0o〇〇(Landroid/view/View;Landroid/content/DialogInterface;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private OOO0o〇()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/fragment/TeamFragment$13;

    .line 6
    .line 7
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$13;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 8
    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/util/PermissionUtil;->O8(Landroid/content/Context;Lcom/intsig/permission/PermissionCallback;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private synthetic OOO80〇〇88(I)V
    .locals 4

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO〇08o:I

    .line 2
    .line 3
    if-ne v0, p1, :cond_0

    .line 4
    .line 5
    new-instance p1, Lcom/intsig/app/AlertDialog$Builder;

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 8
    .line 9
    invoke-direct {p1, v0}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 13
    .line 14
    invoke-virtual {v0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const v1, 0x7f06033e

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    const v1, 0x7f1307fc

    .line 26
    .line 27
    .line 28
    invoke-virtual {p1, v1, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇08O8o〇0(II)Lcom/intsig/app/AlertDialog$Builder;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    const v0, 0x7f13033d

    .line 33
    .line 34
    .line 35
    invoke-virtual {p1, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    new-instance v0, L〇8〇o〇8/O〇0〇o808〇;

    .line 40
    .line 41
    invoke-direct {v0}, L〇8〇o〇8/O〇0〇o808〇;-><init>()V

    .line 42
    .line 43
    .line 44
    const v1, 0x7f131e36

    .line 45
    .line 46
    .line 47
    invoke-virtual {p1, v1, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 48
    .line 49
    .line 50
    move-result-object p1

    .line 51
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V

    .line 56
    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o0〇〇00:I

    .line 60
    .line 61
    if-ne v0, p1, :cond_1

    .line 62
    .line 63
    const-string p1, "CSBusiness"

    .line 64
    .line 65
    const-string v0, "user_guide"

    .line 66
    .line 67
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    iget-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 71
    .line 72
    const v0, 0x7f131db4

    .line 73
    .line 74
    .line 75
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v0

    .line 79
    invoke-static {}, Lcom/intsig/camscanner/web/UrlUtil;->oO()Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v1

    .line 83
    const/4 v2, 0x1

    .line 84
    const/4 v3, 0x0

    .line 85
    invoke-static {p1, v0, v1, v2, v3}, Lcom/intsig/webview/util/WebUtil;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 86
    .line 87
    .line 88
    :cond_1
    :goto_0
    return-void
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private OOOo〇()V
    .locals 13

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇0〇〇8O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 8
    .line 9
    const-string v1, "not isWellPrepared"

    .line 10
    .line 11
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 16
    .line 17
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    const-string v1, "KEY_USE_SYS_CAMERA"

    .line 22
    .line 23
    const/4 v2, 0x0

    .line 24
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-eqz v0, :cond_1

    .line 29
    .line 30
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->ooo〇8oO()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oo8ooo8O:Ljava/lang/String;

    .line 35
    .line 36
    const/16 v1, 0x66

    .line 37
    .line 38
    invoke-static {p0, v1, v0}, Lcom/intsig/camscanner/app/IntentUtil;->oO(Landroidx/fragment/app/Fragment;ILjava/lang/String;)V

    .line 39
    .line 40
    .line 41
    goto :goto_1

    .line 42
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/app/AppPerformanceInfo;->〇080()Lcom/intsig/camscanner/app/AppPerformanceInfo;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    iget-boolean v1, v0, Lcom/intsig/camscanner/app/AppPerformanceInfo;->〇o00〇〇Oo:Z

    .line 47
    .line 48
    if-eqz v1, :cond_2

    .line 49
    .line 50
    iput-boolean v2, v0, Lcom/intsig/camscanner/app/AppPerformanceInfo;->〇o00〇〇Oo:Z

    .line 51
    .line 52
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 53
    .line 54
    .line 55
    move-result-wide v1

    .line 56
    iput-wide v1, v0, Lcom/intsig/camscanner/app/AppPerformanceInfo;->O8:J

    .line 57
    .line 58
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 59
    .line 60
    .line 61
    move-result-wide v1

    .line 62
    iput-wide v1, v0, Lcom/intsig/camscanner/app/AppPerformanceInfo;->Oo08:J

    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 66
    .line 67
    .line 68
    move-result-wide v1

    .line 69
    iput-wide v1, v0, Lcom/intsig/camscanner/app/AppPerformanceInfo;->Oo08:J

    .line 70
    .line 71
    :goto_0
    iget-object v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 72
    .line 73
    iget-wide v4, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇oO:J

    .line 74
    .line 75
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO〇0o8〇()Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v6

    .line 79
    iget-object v7, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 80
    .line 81
    sget-object v8, Lcom/intsig/camscanner/capture/CaptureMode;->NONE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 82
    .line 83
    const/4 v9, 0x0

    .line 84
    const/4 v10, 0x0

    .line 85
    const/4 v11, 0x0

    .line 86
    const/4 v12, 0x0

    .line 87
    invoke-static/range {v3 .. v12}, Lcom/intsig/camscanner/capture/util/CaptureActivityRouterUtil;->〇080(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/capture/CaptureMode;ZLjava/lang/String;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;Z)Landroid/content/Intent;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    const/16 v1, 0x84

    .line 92
    .line 93
    invoke-virtual {p0, v0, v1}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 94
    .line 95
    .line 96
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 97
    .line 98
    const v1, 0x7f01004a

    .line 99
    .line 100
    .line 101
    const v2, 0x7f010049

    .line 102
    .line 103
    .line 104
    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->overridePendingTransition(II)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    .line 106
    .line 107
    goto :goto_1

    .line 108
    :catch_0
    move-exception v0

    .line 109
    sget-object v1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 110
    .line 111
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 112
    .line 113
    .line 114
    :goto_1
    return-void
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private OOO〇()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "User Operation:  switch view mode"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSBusiness"

    .line 9
    .line 10
    const-string v1, "switch_display"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    sget v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇0O〇Oo〇o:I

    .line 16
    .line 17
    const/4 v1, 0x1

    .line 18
    if-ne v0, v1, :cond_0

    .line 19
    .line 20
    const/4 v1, 0x0

    .line 21
    :cond_0
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/fragment/TeamFragment;->o8〇O〇0O0〇(I)V

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O8〇8000:Lcom/intsig/camscanner/fragment/TeamFragment$DeviceInterface;

    .line 25
    .line 26
    invoke-interface {v0}, Lcom/intsig/camscanner/fragment/TeamFragment$DeviceInterface;->o〇0()V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic OOo00(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O8o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OO〇000(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->OOO0o〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OO〇80oO〇(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o0O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OO〇〇o0oO(Lcom/intsig/camscanner/fragment/TeamFragment;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o00〇88〇08:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private Oo088O〇8〇()V
    .locals 4

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0ooOOo:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o0〇()V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getLoaderManager()Landroidx/loader/app/LoaderManager;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    iget v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->Ooo8o:I

    .line 14
    .line 15
    iget-object v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0ooOOo:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 16
    .line 17
    invoke-virtual {v0, v2, v1, v3}, Landroidx/loader/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroidx/loader/app/LoaderManager$LoaderCallbacks;)Landroidx/loader/content/Loader;

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getLoaderManager()Landroidx/loader/app/LoaderManager;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    iget v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->Ooo8o:I

    .line 26
    .line 27
    iget-object v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0ooOOo:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 28
    .line 29
    invoke-virtual {v0, v2, v1, v3}, Landroidx/loader/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroidx/loader/app/LoaderManager$LoaderCallbacks;)Landroidx/loader/content/Loader;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :catch_0
    move-exception v0

    .line 34
    sget-object v1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 35
    .line 36
    const-string v2, "updateLoader"

    .line 37
    .line 38
    invoke-static {v1, v2, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 39
    .line 40
    .line 41
    :goto_0
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic Oo0O〇8800(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->ooO888O0〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private Oo0o0o8()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇80O8o8O〇:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 12
    .line 13
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->OOOo〇(Landroid/content/Context;)Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 20
    .line 21
    const-string v1, "stop the camera animation"

    .line 22
    .line 23
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 27
    .line 28
    const/4 v1, 0x0

    .line 29
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->Oo00O0O〇(Landroid/content/Context;Z)V

    .line 30
    .line 31
    .line 32
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇80O8o8O〇:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 33
    .line 34
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 35
    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOo〇08〇:Landroid/view/View;

    .line 38
    .line 39
    if-eqz v0, :cond_0

    .line 40
    .line 41
    const/16 v1, 0x8

    .line 42
    .line 43
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 44
    .line 45
    .line 46
    :cond_0
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private Oo8〇〇ooo()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroidx/appcompat/app/AppCompatActivity;->getSupportActionBar()Landroidx/appcompat/app/ActionBar;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8O0880:Landroidx/appcompat/app/ActionBar;

    .line 8
    .line 9
    const/16 v1, 0x16

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Landroidx/appcompat/app/ActionBar;->setDisplayOptions(I)V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 15
    .line 16
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const v1, 0x7f0d02fb

    .line 21
    .line 22
    .line 23
    const/4 v2, 0x0

    .line 24
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    const v1, 0x7f0a1839

    .line 29
    .line 30
    .line 31
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    check-cast v1, Landroid/widget/TextView;

    .line 36
    .line 37
    iput-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->OO〇OOo:Landroid/widget/TextView;

    .line 38
    .line 39
    const/4 v3, -0x1

    .line 40
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 41
    .line 42
    .line 43
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->OO〇OOo:Landroid/widget/TextView;

    .line 44
    .line 45
    const/4 v4, 0x0

    .line 46
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 47
    .line 48
    .line 49
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->OO〇OOo:Landroid/widget/TextView;

    .line 50
    .line 51
    invoke-virtual {v1, v4}, Landroid/view/View;->setClickable(Z)V

    .line 52
    .line 53
    .line 54
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->OO〇OOo:Landroid/widget/TextView;

    .line 55
    .line 56
    invoke-virtual {v1, v2, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 57
    .line 58
    .line 59
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇8OO()V

    .line 60
    .line 61
    .line 62
    const v1, 0x7f0a060a

    .line 63
    .line 64
    .line 65
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 66
    .line 67
    .line 68
    move-result-object v1

    .line 69
    check-cast v1, Landroid/widget/FrameLayout;

    .line 70
    .line 71
    iput-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0oO〇oo00:Landroid/widget/FrameLayout;

    .line 72
    .line 73
    new-instance v1, Landroidx/appcompat/app/ActionBar$LayoutParams;

    .line 74
    .line 75
    const/4 v2, 0x5

    .line 76
    invoke-direct {v1, v3, v3, v2}, Landroidx/appcompat/app/ActionBar$LayoutParams;-><init>(III)V

    .line 77
    .line 78
    .line 79
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8O0880:Landroidx/appcompat/app/ActionBar;

    .line 80
    .line 81
    invoke-virtual {v2, v0, v1}, Landroidx/appcompat/app/ActionBar;->setCustomView(Landroid/view/View;Landroidx/appcompat/app/ActionBar$LayoutParams;)V

    .line 82
    .line 83
    .line 84
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 85
    .line 86
    const v1, 0x7f0a055f

    .line 87
    .line 88
    .line 89
    invoke-virtual {v0, v1}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 90
    .line 91
    .line 92
    move-result-object v0

    .line 93
    check-cast v0, Landroidx/drawerlayout/widget/DrawerLayout;

    .line 94
    .line 95
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O8o〇O0:Landroidx/drawerlayout/widget/DrawerLayout;

    .line 96
    .line 97
    const/4 v1, 0x1

    .line 98
    invoke-virtual {v0, v1}, Landroidx/drawerlayout/widget/DrawerLayout;->setDrawerLockMode(I)V

    .line 99
    .line 100
    .line 101
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 102
    .line 103
    const v1, 0x7f0a119c

    .line 104
    .line 105
    .line 106
    invoke-virtual {v0, v1}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 107
    .line 108
    .line 109
    move-result-object v0

    .line 110
    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    .line 111
    .line 112
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇800OO〇0O:Landroidx/appcompat/widget/Toolbar;

    .line 113
    .line 114
    new-instance v0, Lcom/intsig/camscanner/fragment/TeamFragment$NaviIconClickListener;

    .line 115
    .line 116
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$NaviIconClickListener;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 117
    .line 118
    .line 119
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇〇O〇:Lcom/intsig/camscanner/fragment/TeamFragment$NaviIconClickListener;

    .line 120
    .line 121
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇800OO〇0O:Landroidx/appcompat/widget/Toolbar;

    .line 122
    .line 123
    invoke-virtual {v1, v0}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 124
    .line 125
    .line 126
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇800OO〇0O:Landroidx/appcompat/widget/Toolbar;

    .line 127
    .line 128
    const v1, 0x7f080065

    .line 129
    .line 130
    .line 131
    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationIcon(I)V

    .line 132
    .line 133
    .line 134
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O8〇8000:Lcom/intsig/camscanner/fragment/TeamFragment$DeviceInterface;

    .line 135
    .line 136
    invoke-interface {v0}, Lcom/intsig/camscanner/fragment/TeamFragment$DeviceInterface;->〇080()V

    .line 137
    .line 138
    .line 139
    return-void
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private OoO888()V
    .locals 5

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O0o〇〇o:Z

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_2

    .line 5
    .line 6
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 7
    .line 8
    const-string v2, "User Operation: select all doc"

    .line 9
    .line 10
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 14
    .line 15
    invoke-virtual {v0}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    if-eqz v0, :cond_1

    .line 20
    .line 21
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    if-eqz v2, :cond_1

    .line 26
    .line 27
    :cond_0
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 28
    .line 29
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    .line 30
    .line 31
    .line 32
    move-result-wide v3

    .line 33
    invoke-virtual {v2, v3, v4}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->o〇8(J)V

    .line 34
    .line 35
    .line 36
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    .line 37
    .line 38
    .line 39
    move-result v2

    .line 40
    if-nez v2, :cond_0

    .line 41
    .line 42
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 43
    .line 44
    invoke-virtual {v0}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->notifyDataSetChanged()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    .line 46
    .line 47
    goto :goto_0

    .line 48
    :catch_0
    move-exception v0

    .line 49
    sget-object v1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 50
    .line 51
    const-string v2, "selectAllOrCancell "

    .line 52
    .line 53
    invoke-static {v1, v2, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 54
    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 57
    .line 58
    invoke-virtual {v0}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->〇〇8O0〇8()V

    .line 59
    .line 60
    .line 61
    :goto_0
    const/4 v0, 0x1

    .line 62
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O00〇8(Z)V

    .line 63
    .line 64
    .line 65
    goto :goto_1

    .line 66
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 67
    .line 68
    const-string v2, "User Operation: cancel all selected"

    .line 69
    .line 70
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 74
    .line 75
    invoke-virtual {v0}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->〇〇8O0〇8()V

    .line 76
    .line 77
    .line 78
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/fragment/TeamFragment;->O00〇8(Z)V

    .line 79
    .line 80
    .line 81
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 82
    .line 83
    invoke-virtual {v0}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->notifyDataSetChanged()V

    .line 84
    .line 85
    .line 86
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇00O00o()V

    .line 87
    .line 88
    .line 89
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private OoOO〇()Ljava/lang/String;
    .locals 14

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o80()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const-string v1, " and "

    .line 6
    .line 7
    const-string v2, "sync_state != ? and sync_state != ? and team_token =?"

    .line 8
    .line 9
    if-nez v0, :cond_6

    .line 10
    .line 11
    new-instance v0, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    iget-object v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇08〇o0O:[Ljava/lang/String;

    .line 17
    .line 18
    array-length v3, v3

    .line 19
    const/4 v4, 0x0

    .line 20
    const/4 v5, 0x0

    .line 21
    :goto_0
    if-ge v5, v3, :cond_1

    .line 22
    .line 23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    .line 24
    .line 25
    .line 26
    move-result v6

    .line 27
    if-lez v6, :cond_0

    .line 28
    .line 29
    const-string v6, " and title like ? "

    .line 30
    .line 31
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    goto :goto_1

    .line 35
    :cond_0
    const-string v6, " title like ? "

    .line 36
    .line 37
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    :goto_1
    add-int/lit8 v5, v5, 0x1

    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_1
    sget-object v3, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO88o:Ljava/lang/String;

    .line 44
    .line 45
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 46
    .line 47
    .line 48
    move-result v3

    .line 49
    const-string v5, ")"

    .line 50
    .line 51
    const-string v6, "("

    .line 52
    .line 53
    if-nez v3, :cond_5

    .line 54
    .line 55
    iget-object v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 56
    .line 57
    sget-object v7, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO88o:Ljava/lang/String;

    .line 58
    .line 59
    iget-object v8, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 60
    .line 61
    invoke-static {v3, v7, v8}, Lcom/intsig/camscanner/db/dao/DirDao;->〇080(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v3

    .line 65
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 66
    .line 67
    .line 68
    move-result v7

    .line 69
    if-nez v7, :cond_5

    .line 70
    .line 71
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    .line 72
    .line 73
    .line 74
    move-result v7

    .line 75
    const/4 v8, 0x1

    .line 76
    sub-int/2addr v7, v8

    .line 77
    invoke-virtual {v3, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    .line 78
    .line 79
    .line 80
    move-result-object v3

    .line 81
    const-string v7, ","

    .line 82
    .line 83
    invoke-virtual {v3, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object v3

    .line 87
    new-instance v8, Ljava/lang/StringBuilder;

    .line 88
    .line 89
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 90
    .line 91
    .line 92
    array-length v9, v3

    .line 93
    :goto_2
    if-ge v4, v9, :cond_4

    .line 94
    .line 95
    aget-object v10, v3, v4

    .line 96
    .line 97
    new-instance v11, Ljava/lang/StringBuilder;

    .line 98
    .line 99
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 100
    .line 101
    .line 102
    const-string v12, "\'"

    .line 103
    .line 104
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    sget-object v13, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO88o:Ljava/lang/String;

    .line 108
    .line 109
    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    .line 111
    .line 112
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    .line 114
    .line 115
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 116
    .line 117
    .line 118
    move-result-object v11

    .line 119
    invoke-static {v10, v11}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 120
    .line 121
    .line 122
    move-result v11

    .line 123
    if-nez v11, :cond_3

    .line 124
    .line 125
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    .line 126
    .line 127
    .line 128
    move-result v11

    .line 129
    if-lez v11, :cond_2

    .line 130
    .line 131
    new-instance v11, Ljava/lang/StringBuilder;

    .line 132
    .line 133
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 134
    .line 135
    .line 136
    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    .line 138
    .line 139
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    .line 141
    .line 142
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 143
    .line 144
    .line 145
    move-result-object v10

    .line 146
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    .line 148
    .line 149
    goto :goto_3

    .line 150
    :cond_2
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    .line 152
    .line 153
    :cond_3
    :goto_3
    add-int/lit8 v4, v4, 0x1

    .line 154
    .line 155
    goto :goto_2

    .line 156
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    .line 157
    .line 158
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 159
    .line 160
    .line 161
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    .line 163
    .line 164
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 165
    .line 166
    .line 167
    move-result-object v4

    .line 168
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    .line 170
    .line 171
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    .line 173
    .line 174
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 175
    .line 176
    .line 177
    move-result-object v3

    .line 178
    sget-object v4, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 179
    .line 180
    new-instance v7, Ljava/lang/StringBuilder;

    .line 181
    .line 182
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 183
    .line 184
    .line 185
    const-string v8, "getFolderSearchSelection dirIds:"

    .line 186
    .line 187
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    .line 189
    .line 190
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    .line 192
    .line 193
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 194
    .line 195
    .line 196
    move-result-object v7

    .line 197
    invoke-static {v4, v7}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    .line 199
    .line 200
    new-instance v4, Ljava/lang/StringBuilder;

    .line 201
    .line 202
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 203
    .line 204
    .line 205
    const-string v7, " and sync_dir_id in "

    .line 206
    .line 207
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    .line 209
    .line 210
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    .line 212
    .line 213
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 214
    .line 215
    .line 216
    move-result-object v3

    .line 217
    goto :goto_4

    .line 218
    :cond_5
    const-string v3, ""

    .line 219
    .line 220
    :goto_4
    new-instance v4, Ljava/lang/StringBuilder;

    .line 221
    .line 222
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 223
    .line 224
    .line 225
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    .line 227
    .line 228
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 229
    .line 230
    .line 231
    move-result-object v0

    .line 232
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 233
    .line 234
    .line 235
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    .line 237
    .line 238
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 239
    .line 240
    .line 241
    move-result-object v0

    .line 242
    new-instance v4, Ljava/lang/StringBuilder;

    .line 243
    .line 244
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 245
    .line 246
    .line 247
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    .line 249
    .line 250
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    .line 252
    .line 253
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    .line 255
    .line 256
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    .line 258
    .line 259
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 260
    .line 261
    .line 262
    move-result-object v0

    .line 263
    goto :goto_5

    .line 264
    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    .line 265
    .line 266
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 267
    .line 268
    .line 269
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    .line 271
    .line 272
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 273
    .line 274
    .line 275
    const-string v1, "parent_sync_id"

    .line 276
    .line 277
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    .line 279
    .line 280
    const-string v1, "=?"

    .line 281
    .line 282
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    .line 284
    .line 285
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 286
    .line 287
    .line 288
    move-result-object v0

    .line 289
    :goto_5
    return-object v0
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method static bridge synthetic OoO〇OOo8o(Lcom/intsig/camscanner/fragment/TeamFragment;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇oo:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic Ooo8o(Lcom/intsig/camscanner/fragment/TeamFragment;)Landroid/widget/EditText;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇O:Landroid/widget/EditText;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic OooO〇(Lcom/intsig/camscanner/fragment/TeamFragment;)Landroid/widget/TextView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->Oo0O0o8:Landroid/widget/TextView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private OooO〇080()[Ljava/lang/String;
    .locals 9

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o80()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const-string v1, "5"

    .line 6
    .line 7
    const-string v2, "2"

    .line 8
    .line 9
    const/4 v3, 0x0

    .line 10
    if-nez v0, :cond_4

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇08〇o0O:[Ljava/lang/String;

    .line 13
    .line 14
    array-length v0, v0

    .line 15
    add-int/lit8 v4, v0, 0x3

    .line 16
    .line 17
    new-array v5, v4, [Ljava/lang/String;

    .line 18
    .line 19
    :goto_0
    if-ge v3, v4, :cond_5

    .line 20
    .line 21
    if-ge v3, v0, :cond_0

    .line 22
    .line 23
    new-instance v6, Ljava/lang/StringBuilder;

    .line 24
    .line 25
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 26
    .line 27
    .line 28
    const-string v7, "%"

    .line 29
    .line 30
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    iget-object v8, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇08〇o0O:[Ljava/lang/String;

    .line 34
    .line 35
    aget-object v8, v8, v3

    .line 36
    .line 37
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v6

    .line 47
    aput-object v6, v5, v3

    .line 48
    .line 49
    goto :goto_1

    .line 50
    :cond_0
    if-ne v3, v0, :cond_1

    .line 51
    .line 52
    aput-object v2, v5, v3

    .line 53
    .line 54
    goto :goto_1

    .line 55
    :cond_1
    add-int/lit8 v6, v0, 0x1

    .line 56
    .line 57
    if-ne v3, v6, :cond_2

    .line 58
    .line 59
    aput-object v1, v5, v3

    .line 60
    .line 61
    goto :goto_1

    .line 62
    :cond_2
    add-int/lit8 v6, v0, 0x2

    .line 63
    .line 64
    if-ne v3, v6, :cond_3

    .line 65
    .line 66
    iget-object v6, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 67
    .line 68
    aput-object v6, v5, v3

    .line 69
    .line 70
    :cond_3
    :goto_1
    add-int/lit8 v3, v3, 0x1

    .line 71
    .line 72
    goto :goto_0

    .line 73
    :cond_4
    const/4 v0, 0x4

    .line 74
    new-array v5, v0, [Ljava/lang/String;

    .line 75
    .line 76
    aput-object v2, v5, v3

    .line 77
    .line 78
    const/4 v0, 0x1

    .line 79
    aput-object v1, v5, v0

    .line 80
    .line 81
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 82
    .line 83
    const/4 v1, 0x2

    .line 84
    aput-object v0, v5, v1

    .line 85
    .line 86
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO〇0o8〇()Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    const/4 v1, 0x3

    .line 91
    aput-object v0, v5, v1

    .line 92
    .line 93
    :cond_5
    return-object v5
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private Oo〇0o()V
    .locals 4

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOo0:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    const/4 v2, 0x0

    .line 5
    const/16 v3, 0x8

    .line 6
    .line 7
    if-ne v0, v1, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO8:Landroid/widget/LinearLayout;

    .line 10
    .line 11
    if-eqz v0, :cond_3

    .line 12
    .line 13
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇800OO〇0O:Landroidx/appcompat/widget/Toolbar;

    .line 17
    .line 18
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 19
    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO8:Landroid/widget/LinearLayout;

    .line 23
    .line 24
    if-eqz v0, :cond_1

    .line 25
    .line 26
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 27
    .line 28
    .line 29
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇o88:Landroid/widget/RelativeLayout;

    .line 30
    .line 31
    if-eqz v0, :cond_2

    .line 32
    .line 33
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 34
    .line 35
    .line 36
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇800OO〇0O:Landroidx/appcompat/widget/Toolbar;

    .line 37
    .line 38
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 39
    .line 40
    .line 41
    :cond_3
    :goto_0
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private Oo〇〇〇〇(ZLjava/util/ArrayList;Landroid/content/Intent;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/datastruct/DocumentListItem;",
            ">;",
            "Landroid/content/Intent;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-virtual {p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x1

    .line 6
    const/4 v2, 0x0

    .line 7
    if-eqz v0, :cond_2

    .line 8
    .line 9
    invoke-virtual {p3}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    const-class v3, Lcom/intsig/camscanner/uploadfaxprint/UploadFaxPrintActivity;

    .line 18
    .line 19
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v4

    .line 23
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-eqz v0, :cond_2

    .line 28
    .line 29
    new-instance p1, Landroid/content/Intent;

    .line 30
    .line 31
    const/4 p3, 0x0

    .line 32
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 33
    .line 34
    const-string v4, "android.intent.action.SEND"

    .line 35
    .line 36
    invoke-direct {p1, v4, p3, v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    .line 40
    .line 41
    .line 42
    move-result p3

    .line 43
    const-string v0, "SEND_TYPE"

    .line 44
    .line 45
    if-le p3, v1, :cond_0

    .line 46
    .line 47
    const/16 p3, 0xb

    .line 48
    .line 49
    invoke-virtual {p1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 50
    .line 51
    .line 52
    goto :goto_0

    .line 53
    :cond_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    .line 54
    .line 55
    .line 56
    move-result p3

    .line 57
    if-ne p3, v1, :cond_1

    .line 58
    .line 59
    const/16 p3, 0xa

    .line 60
    .line 61
    invoke-virtual {p1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 62
    .line 63
    .line 64
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 65
    .line 66
    .line 67
    move-result-object p3

    .line 68
    check-cast p3, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 69
    .line 70
    iget-wide v0, p3, Lcom/intsig/webstorage/UploadFile;->OO:J

    .line 71
    .line 72
    const-string p3, "doc_id"

    .line 73
    .line 74
    invoke-virtual {p1, p3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 75
    .line 76
    .line 77
    const-string p3, "is_need_suffix"

    .line 78
    .line 79
    invoke-virtual {p1, p3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 80
    .line 81
    .line 82
    :cond_1
    :goto_0
    const-string p3, "ids"

    .line 83
    .line 84
    invoke-virtual {p1, p3, p2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 85
    .line 86
    .line 87
    invoke-virtual {p0, p1}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 88
    .line 89
    .line 90
    goto/16 :goto_2

    .line 91
    .line 92
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 93
    .line 94
    new-instance v3, Ljava/lang/StringBuilder;

    .line 95
    .line 96
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 97
    .line 98
    .line 99
    const-string v4, "doShare size="

    .line 100
    .line 101
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    .line 103
    .line 104
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    .line 105
    .line 106
    .line 107
    move-result v4

    .line 108
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    const-string v4, " single="

    .line 112
    .line 113
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 117
    .line 118
    .line 119
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 120
    .line 121
    .line 122
    move-result-object p1

    .line 123
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    .line 125
    .line 126
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    .line 127
    .line 128
    .line 129
    move-result p1

    .line 130
    const-string v3, "android.intent.extra.SUBJECT"

    .line 131
    .line 132
    if-ne p1, v1, :cond_4

    .line 133
    .line 134
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 135
    .line 136
    .line 137
    move-result-object p1

    .line 138
    check-cast p1, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 139
    .line 140
    iget-object p2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 141
    .line 142
    iget-object p1, p1, Lcom/intsig/webstorage/UploadFile;->o0:Ljava/lang/String;

    .line 143
    .line 144
    invoke-static {p2, p1, v2, v2}, Lcom/intsig/camscanner/util/StringUtil;->OO0o〇〇(Landroid/content/Context;Ljava/lang/String;II)Ljava/lang/String;

    .line 145
    .line 146
    .line 147
    move-result-object p1

    .line 148
    new-instance p2, Ljava/lang/StringBuilder;

    .line 149
    .line 150
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 151
    .line 152
    .line 153
    const-string v1, "subject="

    .line 154
    .line 155
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    .line 157
    .line 158
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    .line 160
    .line 161
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 162
    .line 163
    .line 164
    move-result-object p2

    .line 165
    invoke-static {v0, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    .line 167
    .line 168
    invoke-static {p3, p1}, Lcom/intsig/camscanner/control/ShareControl;->OOO〇O0(Landroid/content/Intent;Ljava/lang/String;)Z

    .line 169
    .line 170
    .line 171
    move-result p2

    .line 172
    if-eqz p2, :cond_3

    .line 173
    .line 174
    goto :goto_1

    .line 175
    :cond_3
    invoke-virtual {p3, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 176
    .line 177
    .line 178
    goto :goto_1

    .line 179
    :cond_4
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    .line 180
    .line 181
    .line 182
    move-result p1

    .line 183
    if-le p1, v1, :cond_5

    .line 184
    .line 185
    iget-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 186
    .line 187
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 188
    .line 189
    .line 190
    move-result-object v0

    .line 191
    check-cast v0, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 192
    .line 193
    iget-object v0, v0, Lcom/intsig/webstorage/UploadFile;->o0:Ljava/lang/String;

    .line 194
    .line 195
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    .line 196
    .line 197
    .line 198
    move-result p2

    .line 199
    invoke-static {p1, v0, p2, v1}, Lcom/intsig/camscanner/util/StringUtil;->OO0o〇〇(Landroid/content/Context;Ljava/lang/String;II)Ljava/lang/String;

    .line 200
    .line 201
    .line 202
    move-result-object p1

    .line 203
    invoke-virtual {p3, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 204
    .line 205
    .line 206
    :cond_5
    :goto_1
    iget-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 207
    .line 208
    invoke-static {p1, p3}, Lcom/intsig/camscanner/control/ShareControl;->oo〇(Landroid/content/Context;Landroid/content/Intent;)V

    .line 209
    .line 210
    .line 211
    invoke-static {p3}, Lcom/intsig/camscanner/control/ShareControl;->O8ooOoo〇(Landroid/content/Intent;)V

    .line 212
    .line 213
    .line 214
    const/16 p1, 0x63

    .line 215
    .line 216
    :try_start_0
    invoke-virtual {p0, p3, p1}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 217
    .line 218
    .line 219
    goto :goto_2

    .line 220
    :catch_0
    move-exception p1

    .line 221
    sget-object p2, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 222
    .line 223
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 224
    .line 225
    .line 226
    :goto_2
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇O()V

    .line 227
    .line 228
    .line 229
    return-void
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method static bridge synthetic O〇00O(Lcom/intsig/camscanner/fragment/TeamFragment;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇o88o08〇:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O〇00o08()V
    .locals 3

    .line 1
    const/16 v0, -0x3e7

    .line 2
    .line 3
    iput v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O88O:I

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇oO〇〇8o:Landroid/widget/AbsListView;

    .line 6
    .line 7
    const/16 v1, 0x8

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 10
    .line 11
    .line 12
    sget v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇0O〇Oo〇o:I

    .line 13
    .line 14
    const/4 v1, 0x0

    .line 15
    const/4 v2, 0x1

    .line 16
    if-ne v0, v2, :cond_0

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->ooo0〇〇O:Landroid/widget/GridView;

    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇oO〇〇8o:Landroid/widget/AbsListView;

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇08O:Landroid/widget/AbsListView;

    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇oO〇〇8o:Landroid/widget/AbsListView;

    .line 26
    .line 27
    const/4 v2, 0x0

    .line 28
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇oO〇〇8o:Landroid/widget/AbsListView;

    .line 29
    .line 30
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 31
    .line 32
    .line 33
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 34
    .line 35
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇oO〇〇8o:Landroid/widget/AbsListView;

    .line 36
    .line 37
    invoke-virtual {v0, v2, v1}, Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;->〇000O0(ILandroid/widget/AbsListView;)V

    .line 38
    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇oO〇〇8o:Landroid/widget/AbsListView;

    .line 41
    .line 42
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 43
    .line 44
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 45
    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇oO〇〇8o:Landroid/widget/AbsListView;

    .line 48
    .line 49
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0O〇O00O:Landroid/widget/AdapterView$OnItemClickListener;

    .line 50
    .line 51
    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 52
    .line 53
    .line 54
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇oO〇〇8o:Landroid/widget/AbsListView;

    .line 55
    .line 56
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇o〇Oo88:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 57
    .line 58
    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 59
    .line 60
    .line 61
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇o80Oo()V

    .line 62
    .line 63
    .line 64
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic O〇080〇o0(Lcom/intsig/camscanner/fragment/TeamFragment;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇o88o08〇:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O〇0888o(Ljava/util/ArrayList;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    const-wide/16 v3, -0x1

    .line 15
    .line 16
    iget-wide v5, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇oO:J

    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO〇0o8〇()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v7

    .line 22
    iget-object v8, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 23
    .line 24
    const/4 v9, 0x0

    .line 25
    const/4 v10, 0x0

    .line 26
    move-object v2, p1

    .line 27
    invoke-static/range {v1 .. v10}, Lcom/intsig/camscanner/BatchModeActivity;->o88o88(Landroid/content/Context;Ljava/util/ArrayList;JJLjava/lang/String;Ljava/lang/String;ZZ)Landroid/content/Intent;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    const/16 v0, 0x6b

    .line 32
    .line 33
    invoke-virtual {p0, p1, v0}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 34
    .line 35
    .line 36
    return-void

    .line 37
    :cond_1
    :goto_0
    sget-object p1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 38
    .line 39
    const-string v0, "importPictures uris == null || uris.size() == 0"

    .line 40
    .line 41
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic O〇0O〇Oo〇o(Lcom/intsig/camscanner/fragment/TeamFragment;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOo0:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O〇0o8o8〇(Lcom/intsig/camscanner/fragment/TeamFragment;Landroid/widget/TextView;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->Oo0O0o8:Landroid/widget/TextView;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic O〇0o8〇(Lcom/intsig/camscanner/fragment/TeamFragment;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O8〇〇o8〇()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O〇8(Lcom/intsig/camscanner/fragment/TeamFragment;Lcom/intsig/camscanner/datastruct/FolderItem;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/TeamFragment;->OO0o88(Lcom/intsig/camscanner/datastruct/FolderItem;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic O〇8O0O80〇(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇8088()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O〇8Oo()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 2
    .line 3
    const/16 v1, 0x8

    .line 4
    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->〇00〇8()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_1

    .line 12
    .line 13
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O8〇8000:Lcom/intsig/camscanner/fragment/TeamFragment$DeviceInterface;

    .line 14
    .line 15
    invoke-interface {v0}, Lcom/intsig/camscanner/fragment/TeamFragment$DeviceInterface;->〇o00〇〇Oo()Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-nez v0, :cond_2

    .line 20
    .line 21
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇80O8o8O〇:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;->setVisibility(I)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOo〇08〇:Landroid/view/View;

    .line 27
    .line 28
    if-eqz v0, :cond_4

    .line 29
    .line 30
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 31
    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O88Oo8()Z

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    if-eqz v0, :cond_3

    .line 39
    .line 40
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O8〇〇o8〇()Z

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    if-nez v0, :cond_3

    .line 45
    .line 46
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇80O8o8O〇:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 47
    .line 48
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;->setVisibility(I)V

    .line 49
    .line 50
    .line 51
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOo〇08〇:Landroid/view/View;

    .line 52
    .line 53
    if-eqz v0, :cond_4

    .line 54
    .line 55
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 56
    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇80O8o8O〇:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 60
    .line 61
    const/4 v1, 0x0

    .line 62
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;->setVisibility(I)V

    .line 63
    .line 64
    .line 65
    :cond_4
    :goto_0
    return-void
    .line 66
    .line 67
    .line 68
.end method

.method private O〇8O〇(I)V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "showCustomDialog id:"

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    :try_start_0
    invoke-static {p1}, Lcom/intsig/camscanner/fragment/TeamFragment$MyDialogFragment;->〇O8oOo0(I)Lcom/intsig/camscanner/fragment/TeamFragment$MyDialogFragment;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    iput-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOoO8OO〇:Landroidx/fragment/app/DialogFragment;

    .line 28
    .line 29
    const/4 v3, 0x0

    .line 30
    invoke-virtual {v1, p0, v3}, Landroidx/fragment/app/Fragment;->setTargetFragment(Landroidx/fragment/app/Fragment;I)V

    .line 31
    .line 32
    .line 33
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOoO8OO〇:Landroidx/fragment/app/DialogFragment;

    .line 34
    .line 35
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 36
    .line 37
    .line 38
    move-result-object v3

    .line 39
    invoke-virtual {v1, v3, v0}, Landroidx/fragment/app/DialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    .line 41
    .line 42
    goto :goto_0

    .line 43
    :catch_0
    move-exception v0

    .line 44
    sget-object v1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 45
    .line 46
    new-instance v3, Ljava/lang/StringBuilder;

    .line 47
    .line 48
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 49
    .line 50
    .line 51
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    invoke-static {v1, p1, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 62
    .line 63
    .line 64
    :goto_0
    return-void
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic O〇8〇008(Lcom/intsig/camscanner/fragment/TeamFragment;)Lcom/intsig/camscanner/datastruct/DocumentListItem;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oO〇8O8oOo:Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O〇O800oo(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇80o〇o0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O〇O88(Landroid/net/Uri;)V
    .locals 9

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    sget-object p1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 4
    .line 5
    const-string v0, "openMergedDocument u == null"

    .line 6
    .line 7
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    .line 12
    .line 13
    const-string v1, "android.intent.action.VIEW"

    .line 14
    .line 15
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 16
    .line 17
    const-class v3, Lcom/intsig/camscanner/DocumentActivity;

    .line 18
    .line 19
    invoke-direct {v0, v1, p1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 20
    .line 21
    .line 22
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o80()Z

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    if-nez p1, :cond_1

    .line 27
    .line 28
    const-string p1, "EXTRA_QUERY_STRING"

    .line 29
    .line 30
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇08〇o0O:[Ljava/lang/String;

    .line 31
    .line 32
    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 33
    .line 34
    .line 35
    :cond_1
    new-instance p1, Lcom/intsig/camscanner/datastruct/TeamDocInfo;

    .line 36
    .line 37
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 38
    .line 39
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO〇0o8〇()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v3

    .line 43
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0o()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v4

    .line 47
    iget v5, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇0〇o:I

    .line 48
    .line 49
    const/4 v6, 0x2

    .line 50
    iget v7, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇oo:I

    .line 51
    .line 52
    iget-boolean v8, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇OOoooo:Z

    .line 53
    .line 54
    move-object v1, p1

    .line 55
    invoke-direct/range {v1 .. v8}, Lcom/intsig/camscanner/datastruct/TeamDocInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIZ)V

    .line 56
    .line 57
    .line 58
    const-string v1, "team_doc_info"

    .line 59
    .line 60
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 61
    .line 62
    .line 63
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 64
    .line 65
    .line 66
    iget-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 67
    .line 68
    const v0, 0x7f010028

    .line 69
    .line 70
    .line 71
    const/4 v1, 0x0

    .line 72
    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 73
    .line 74
    .line 75
    goto :goto_0

    .line 76
    :catch_0
    move-exception p1

    .line 77
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 78
    .line 79
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 80
    .line 81
    .line 82
    :goto_0
    return-void
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method static bridge synthetic O〇o8(Lcom/intsig/camscanner/fragment/TeamFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/TeamFragment;->o00o0O〇〇o(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic O〇oo8O80(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o8〇8oooO〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O〇〇O(J)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0o(Z)V

    .line 3
    .line 4
    .line 5
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o00(JZ)V

    .line 6
    .line 7
    .line 8
    iget-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 9
    .line 10
    invoke-virtual {p1}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->notifyDataSetChanged()V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇00O00o()V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O00〇8(Z)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
.end method

.method static bridge synthetic O〇〇O80o8(Lcom/intsig/camscanner/fragment/TeamFragment;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇0〇o:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O〇〇o8O(Lcom/intsig/camscanner/fragment/TeamFragment;J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO〇〇:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private o00(JZ)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-static {v0, p1, p2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇o8OO0(Landroid/content/Context;J)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_1

    .line 8
    .line 9
    if-eqz p3, :cond_0

    .line 10
    .line 11
    iget-object p3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 12
    .line 13
    invoke-virtual {p3, p1, p2}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->〇0〇O0088o(J)V

    .line 14
    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    iget-object p3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 18
    .line 19
    invoke-virtual {p3, p1, p2}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->o8(J)V

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_1
    if-nez p3, :cond_2

    .line 24
    .line 25
    const/16 p3, 0xfb

    .line 26
    .line 27
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇8O〇(I)V

    .line 28
    .line 29
    .line 30
    sget-object p3, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 31
    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    const-string v1, "onItemSelected isDocImageJpgComplete false id = "

    .line 38
    .line 39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    invoke-static {p3, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    :cond_2
    :goto_0
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private o008()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/tsapp/sync/TeamDownloadStateMonitor;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 6
    .line 7
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/tsapp/sync/TeamDownloadStateMonitor;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O8〇8O0oO:Lcom/intsig/camscanner/tsapp/sync/TeamDownloadStateMonitor;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/tsapp/sync/TeamDownloadStateMonitor;->O8()V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private o00o0O〇〇o(I)V
    .locals 0

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    const/4 p1, 0x1

    .line 4
    iput-boolean p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇o08:Z

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 p1, 0x0

    .line 8
    iput-boolean p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇o08:Z

    .line 9
    .line 10
    :goto_0
    const/16 p1, 0xd8

    .line 11
    .line 12
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇8O〇(I)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o00oooo()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/util/PermissionUtil;->〇O〇()[Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    new-instance v2, Lcom/intsig/camscanner/fragment/TeamFragment$24;

    .line 8
    .line 9
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$24;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 10
    .line 11
    .line 12
    invoke-static {v0, v1, v2}, Lcom/intsig/util/PermissionUtil;->Oo08(Landroid/content/Context;[Ljava/lang/String;Lcom/intsig/permission/PermissionCallback;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic o00〇88〇08(Lcom/intsig/camscanner/fragment/TeamFragment;Ljava/util/ArrayList;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇o0o8Oo(Ljava/util/ArrayList;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private o08(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    :goto_0
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O8〇8000:Lcom/intsig/camscanner/fragment/TeamFragment$DeviceInterface;

    .line 16
    .line 17
    invoke-interface {v1, p1, v0}, Lcom/intsig/camscanner/fragment/TeamFragment$DeviceInterface;->〇〇888(II)V

    .line 18
    .line 19
    .line 20
    return-void
.end method

.method static bridge synthetic o088O8800(Lcom/intsig/camscanner/fragment/TeamFragment;)[Ljava/lang/String;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇08〇0oo0()[Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o088〇〇(Landroid/net/Uri;)Ljava/lang/String;
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const-string v0, "title"

    .line 8
    .line 9
    filled-new-array {v0}, [Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v3

    .line 13
    const/4 v4, 0x0

    .line 14
    const/4 v5, 0x0

    .line 15
    const/4 v6, 0x0

    .line 16
    move-object v2, p1

    .line 17
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    const/4 v0, 0x0

    .line 22
    if-eqz p1, :cond_1

    .line 23
    .line 24
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    if-eqz v1, :cond_0

    .line 29
    .line 30
    const/4 v0, 0x0

    .line 31
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 36
    .line 37
    .line 38
    :cond_1
    return-object v0
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private o08〇808()V
    .locals 2

    .line 1
    const-string v0, "CSBusiness"

    .line 2
    .line 3
    const-string v1, "back"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    sput-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO88o:Ljava/lang/String;

    .line 10
    .line 11
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->o808o8o08:Ljava/util/ArrayList;

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-lez v0, :cond_0

    .line 20
    .line 21
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->o808o8o08:Ljava/util/ArrayList;

    .line 22
    .line 23
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 24
    .line 25
    .line 26
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 27
    .line 28
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private o0O()V
    .locals 15

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇88()Ljava/util/ArrayList;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-lez v1, :cond_0

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    check-cast v0, Ljava/lang/Long;

    .line 19
    .line 20
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    .line 21
    .line 22
    .line 23
    move-result-wide v8

    .line 24
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 25
    .line 26
    invoke-static {v0, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 27
    .line 28
    .line 29
    move-result-object v3

    .line 30
    invoke-direct {p0, v3}, Lcom/intsig/camscanner/fragment/TeamFragment;->o088〇〇(Landroid/net/Uri;)Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    iget-object v7, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 35
    .line 36
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO〇0o8〇()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v10

    .line 40
    const v11, 0x7f131e5a

    .line 41
    .line 42
    .line 43
    const/4 v12, 0x0

    .line 44
    new-instance v13, Lcom/intsig/camscanner/fragment/TeamFragment$22;

    .line 45
    .line 46
    move-object v1, v13

    .line 47
    move-object v2, p0

    .line 48
    move-wide v4, v8

    .line 49
    move-object v6, v0

    .line 50
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/fragment/TeamFragment$22;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;Landroid/net/Uri;JLjava/lang/String;)V

    .line 51
    .line 52
    .line 53
    new-instance v14, Lcom/intsig/camscanner/fragment/TeamFragment$23;

    .line 54
    .line 55
    invoke-direct {v14, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$23;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 56
    .line 57
    .line 58
    move-object v1, v7

    .line 59
    move-object v2, v10

    .line 60
    move v3, v11

    .line 61
    move v4, v12

    .line 62
    move-object v5, v0

    .line 63
    move-object v6, v13

    .line 64
    move-object v7, v14

    .line 65
    invoke-static/range {v1 .. v9}, Lcom/intsig/camscanner/app/DialogUtils;->〇0(Landroid/app/Activity;Ljava/lang/String;IZLjava/lang/String;Lcom/intsig/camscanner/app/DialogUtils$OnDocTitleEditListener;Lcom/intsig/camscanner/app/DialogUtils$OnTemplateSettingsListener;J)V

    .line 66
    .line 67
    .line 68
    :cond_0
    return-void
.end method

.method static bridge synthetic o0O0O〇〇〇0(Lcom/intsig/camscanner/fragment/TeamFragment;J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o0OoOOo0:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic o0OO(Lcom/intsig/camscanner/fragment/TeamFragment;J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8o:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic o0Oo(Lcom/intsig/camscanner/fragment/TeamFragment;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇o0〇8:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o0o〇〇〇8o()[I
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oO0o()Landroid/view/View;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_2

    .line 7
    .line 8
    const/4 v2, 0x2

    .line 9
    new-array v3, v2, [I

    .line 10
    .line 11
    invoke-virtual {v0, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 12
    .line 13
    .line 14
    new-array v2, v2, [I

    .line 15
    .line 16
    iget-object v4, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇oO〇〇8o:Landroid/widget/AbsListView;

    .line 17
    .line 18
    invoke-virtual {v4, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    iget-object v4, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇oO〇〇8o:Landroid/widget/AbsListView;

    .line 26
    .line 27
    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    .line 28
    .line 29
    .line 30
    move-result v4

    .line 31
    const/4 v5, 0x1

    .line 32
    aget v2, v2, v5

    .line 33
    .line 34
    add-int/2addr v4, v2

    .line 35
    aget v6, v3, v5

    .line 36
    .line 37
    add-int/2addr v0, v6

    .line 38
    if-le v4, v0, :cond_3

    .line 39
    .line 40
    add-int/lit8 v6, v6, -0x16

    .line 41
    .line 42
    if-le v2, v6, :cond_1

    .line 43
    .line 44
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇oO〇〇8o:Landroid/widget/AbsListView;

    .line 45
    .line 46
    instance-of v2, v0, Landroid/widget/GridView;

    .line 47
    .line 48
    if-eqz v2, :cond_0

    .line 49
    .line 50
    check-cast v0, Landroid/widget/GridView;

    .line 51
    .line 52
    invoke-virtual {v0}, Landroid/widget/GridView;->getNumColumns()I

    .line 53
    .line 54
    .line 55
    move-result v5

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇oO〇〇8o:Landroid/widget/AbsListView;

    .line 57
    .line 58
    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    if-eqz v0, :cond_3

    .line 63
    .line 64
    const v1, 0x7f0a0529

    .line 65
    .line 66
    .line 67
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    if-eqz v0, :cond_1

    .line 72
    .line 73
    invoke-virtual {v0, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 74
    .line 75
    .line 76
    :cond_1
    move-object v1, v3

    .line 77
    goto :goto_0

    .line 78
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 79
    .line 80
    const-string v2, "docView == null"

    .line 81
    .line 82
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    :cond_3
    :goto_0
    return-object v1
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private o0〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0ooOOo:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/fragment/TeamFragment$20;

    .line 6
    .line 7
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$20;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0ooOOo:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private o0〇OO008O()Z
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 10
    .line 11
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->〇00(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-lez v1, :cond_1

    .line 22
    .line 23
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    if-eqz v1, :cond_1

    .line 32
    .line 33
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    check-cast v1, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 38
    .line 39
    const/4 v2, 0x0

    .line 40
    if-eqz v1, :cond_2

    .line 41
    .line 42
    sget-object v3, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 43
    .line 44
    new-instance v4, Ljava/lang/StringBuilder;

    .line 45
    .line 46
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 47
    .line 48
    .line 49
    const-string v5, "title:"

    .line 50
    .line 51
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    iget-object v5, v1, Lcom/intsig/webstorage/UploadFile;->o0:Ljava/lang/String;

    .line 55
    .line 56
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    const-string v5, ", docPermission:"

    .line 60
    .line 61
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    iget v5, v1, Lcom/intsig/camscanner/datastruct/DocumentListItem;->o〇00O:I

    .line 65
    .line 66
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    const-string v5, ", uid:"

    .line 70
    .line 71
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    iget-object v5, v1, Lcom/intsig/camscanner/datastruct/DocumentListItem;->O8o08O8O:Ljava/lang/String;

    .line 75
    .line 76
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v4

    .line 83
    invoke-static {v3, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0o()Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v3

    .line 90
    iget-object v4, v1, Lcom/intsig/camscanner/datastruct/DocumentListItem;->O8o08O8O:Ljava/lang/String;

    .line 91
    .line 92
    invoke-static {v3, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 93
    .line 94
    .line 95
    move-result v3

    .line 96
    if-eqz v3, :cond_2

    .line 97
    .line 98
    iget-boolean v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇OOoooo:Z

    .line 99
    .line 100
    if-eqz v3, :cond_0

    .line 101
    .line 102
    iget v1, v1, Lcom/intsig/camscanner/datastruct/DocumentListItem;->o〇00O:I

    .line 103
    .line 104
    const/16 v3, 0x100

    .line 105
    .line 106
    if-ne v1, v3, :cond_0

    .line 107
    .line 108
    goto :goto_0

    .line 109
    :cond_1
    const/4 v2, 0x1

    .line 110
    :cond_2
    :goto_0
    return v2
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic o0〇〇00(Lcom/intsig/camscanner/fragment/TeamFragment;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇OOoooo:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o0〇〇00〇o(Lcom/intsig/camscanner/fragment/TeamFragment;)[Ljava/lang/String;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->OooO〇080()[Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o80()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇08〇o0O:[Ljava/lang/String;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    array-length v0, v0

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 v0, 0x0

    .line 10
    goto :goto_1

    .line 11
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 12
    :goto_1
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private o808Oo()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇0Oo0880()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0〇〇o0()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic o808o8o08(Lcom/intsig/camscanner/fragment/TeamFragment;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇8〇008:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o80oO()V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇80O8o8O〇:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 2
    .line 3
    if-eqz v0, :cond_2

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_2

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 12
    .line 13
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->OOOo〇(Landroid/content/Context;)Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_2

    .line 18
    .line 19
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 20
    .line 21
    const-string v1, "startCameraAnimation"

    .line 22
    .line 23
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇80O8o8O〇:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 27
    .line 28
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 29
    .line 30
    .line 31
    new-instance v0, Landroid/view/animation/ScaleAnimation;

    .line 32
    .line 33
    const/high16 v2, 0x3f800000    # 1.0f

    .line 34
    .line 35
    const v3, 0x3fa66666    # 1.3f

    .line 36
    .line 37
    .line 38
    const/high16 v4, 0x3f800000    # 1.0f

    .line 39
    .line 40
    const v5, 0x3fa66666    # 1.3f

    .line 41
    .line 42
    .line 43
    const/4 v6, 0x1

    .line 44
    const/high16 v7, 0x3f000000    # 0.5f

    .line 45
    .line 46
    const/4 v8, 0x1

    .line 47
    const/high16 v9, 0x3f000000    # 0.5f

    .line 48
    .line 49
    move-object v1, v0

    .line 50
    invoke-direct/range {v1 .. v9}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    .line 51
    .line 52
    .line 53
    const-wide/16 v1, 0x1f4

    .line 54
    .line 55
    invoke-virtual {v0, v1, v2}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 56
    .line 57
    .line 58
    const/4 v1, -0x1

    .line 59
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    .line 60
    .line 61
    .line 62
    const/4 v1, 0x2

    .line 63
    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setRepeatMode(I)V

    .line 64
    .line 65
    .line 66
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇80O8o8O〇:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 67
    .line 68
    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 69
    .line 70
    .line 71
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOo〇08〇:Landroid/view/View;

    .line 72
    .line 73
    if-nez v0, :cond_1

    .line 74
    .line 75
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o880:Landroid/view/View;

    .line 76
    .line 77
    const v1, 0x7f0a113d

    .line 78
    .line 79
    .line 80
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 81
    .line 82
    .line 83
    move-result-object v0

    .line 84
    check-cast v0, Landroid/view/ViewStub;

    .line 85
    .line 86
    if-eqz v0, :cond_0

    .line 87
    .line 88
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    .line 90
    .line 91
    goto :goto_0

    .line 92
    :catch_0
    move-exception v0

    .line 93
    sget-object v1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 94
    .line 95
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 96
    .line 97
    .line 98
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o880:Landroid/view/View;

    .line 99
    .line 100
    const v1, 0x7f0a05ff

    .line 101
    .line 102
    .line 103
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 104
    .line 105
    .line 106
    move-result-object v0

    .line 107
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOo〇08〇:Landroid/view/View;

    .line 108
    .line 109
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOo〇08〇:Landroid/view/View;

    .line 110
    .line 111
    if-eqz v0, :cond_2

    .line 112
    .line 113
    const/4 v1, 0x0

    .line 114
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 115
    .line 116
    .line 117
    :cond_2
    return-void
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic o88(Lcom/intsig/camscanner/fragment/TeamFragment;)Lcom/intsig/camscanner/fragment/TeamFragment$NoDocViewControl;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/fragment/TeamFragment$NoDocViewControl;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o880(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0880O0〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o88o88(Lcom/intsig/camscanner/fragment/TeamFragment;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o0〇OO008O()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o88oo〇O(Lcom/intsig/camscanner/fragment/TeamFragment;ZLjava/util/ArrayList;Landroid/content/Intent;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/fragment/TeamFragment;->Oo〇〇〇〇(ZLjava/util/ArrayList;Landroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method static bridge synthetic o8O〇008(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOOO0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o8o0(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o08〇808()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o8o0o8(Lcom/intsig/camscanner/fragment/TeamFragment;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0o(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private o8o8〇o()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o880:Landroid/view/View;

    .line 2
    .line 3
    const v1, 0x7f0a05d5

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    check-cast v0, Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇80O8o8O〇:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;->setContext(Landroid/content/Context;)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇80O8o8O〇:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 20
    .line 21
    new-instance v1, Lcom/intsig/camscanner/fragment/TeamFragment$6;

    .line 22
    .line 23
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$6;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 24
    .line 25
    .line 26
    const-wide/16 v2, 0x12c

    .line 27
    .line 28
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇80O8o8O〇:Lcom/intsig/camscanner/view/SlideUpFloatingActionButton;

    .line 32
    .line 33
    new-instance v1, Lcom/intsig/camscanner/fragment/TeamFragment$7;

    .line 34
    .line 35
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$7;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    .line 40
    .line 41
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇8Oo()V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic o8oo0OOO(Lcom/intsig/camscanner/fragment/TeamFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/TeamFragment;->O8O〇8〇〇8〇(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private o8o〇8()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oO〇oo:Lcom/intsig/view/ImageTextButton;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    invoke-static {v2}, Lcom/intsig/camscanner/util/PreferenceHelper;->OOOo〇(Landroid/content/Context;)Z

    .line 9
    .line 10
    .line 11
    move-result v2

    .line 12
    if-nez v2, :cond_0

    .line 13
    .line 14
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 15
    .line 16
    invoke-static {v2}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇OO0oO(Landroid/content/Context;)Z

    .line 17
    .line 18
    .line 19
    move-result v2

    .line 20
    if-eqz v2, :cond_0

    .line 21
    .line 22
    const/4 v2, 0x1

    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const/4 v2, 0x0

    .line 25
    :goto_0
    invoke-virtual {v0, v2}, Lcom/intsig/view/ImageTextButton;->Oooo8o0〇(Z)V

    .line 26
    .line 27
    .line 28
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇08oOOO0:Lcom/intsig/view/ImageTextButton;

    .line 29
    .line 30
    if-eqz v0, :cond_2

    .line 31
    .line 32
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 33
    .line 34
    invoke-static {v2}, Lcom/intsig/camscanner/util/PreferenceHelper;->OOOo〇(Landroid/content/Context;)Z

    .line 35
    .line 36
    .line 37
    move-result v2

    .line 38
    xor-int/2addr v1, v2

    .line 39
    invoke-virtual {v0, v1}, Lcom/intsig/view/ImageTextButton;->Oooo8o0〇(Z)V

    .line 40
    .line 41
    .line 42
    :cond_2
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static o8〇0o〇()Z
    .locals 1

    .line 1
    sget v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇0O〇Oo〇o:I

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v0, 0x0

    .line 8
    :goto_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private o8〇8oooO〇()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "User Operation: rename"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 9
    .line 10
    new-instance v1, Lcom/intsig/camscanner/fragment/TeamFragment$37;

    .line 11
    .line 12
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$37;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 13
    .line 14
    .line 15
    invoke-static {v0, v1}, Lcom/intsig/camscanner/control/ConnectChecker;->〇080(Landroid/content/Context;Lcom/intsig/camscanner/control/ConnectChecker$ActionListener;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method private o8〇O〇0O0〇(I)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "User Operation: menu switch view mode == "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string v2, " mViewMode = "

    .line 17
    .line 18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    sget v2, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇0O〇Oo〇o:I

    .line 22
    .line 23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    sget v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇0O〇Oo〇o:I

    .line 34
    .line 35
    if-ne v0, p1, :cond_0

    .line 36
    .line 37
    return-void

    .line 38
    :cond_0
    const/4 v0, 0x1

    .line 39
    if-ne p1, v0, :cond_1

    .line 40
    .line 41
    sput v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇0O〇Oo〇o:I

    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_1
    const/4 p1, 0x0

    .line 45
    sput p1, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇0O〇Oo〇o:I

    .line 46
    .line 47
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 48
    .line 49
    sget v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇0O〇Oo〇o:I

    .line 50
    .line 51
    invoke-static {p1, v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇8〇oo(Landroid/content/Context;I)V

    .line 52
    .line 53
    .line 54
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇00o08()V

    .line 55
    .line 56
    .line 57
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private oO0o()Landroid/view/View;
    .locals 3

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    :goto_0
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 4
    .line 5
    invoke-virtual {v2}, Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;->getCount()I

    .line 6
    .line 7
    .line 8
    move-result v2

    .line 9
    if-ge v1, v2, :cond_0

    .line 10
    .line 11
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇oO〇〇8o:Landroid/widget/AbsListView;

    .line 12
    .line 13
    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    if-eqz v2, :cond_0

    .line 18
    .line 19
    const v0, 0x7f0a0529

    .line 20
    .line 21
    .line 22
    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    if-nez v0, :cond_0

    .line 27
    .line 28
    add-int/lit8 v1, v1, 0x1

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    return-object v0
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic oO8(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O00〇o00()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private oO800o()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "User Operation: multi merge"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 9
    .line 10
    new-instance v1, Lcom/intsig/camscanner/fragment/TeamFragment$11;

    .line 11
    .line 12
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$11;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 13
    .line 14
    .line 15
    invoke-static {v0, v1}, Lcom/intsig/camscanner/control/ConnectChecker;->〇080(Landroid/content/Context;Lcom/intsig/camscanner/control/ConnectChecker$ActionListener;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method private oO80O0(Ljava/util/ArrayList;Ljava/util/ArrayList;ZZ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/datastruct/DocumentListItem;",
            ">;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;ZZ)V"
        }
    .end annotation

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 11
    .line 12
    .line 13
    new-instance v0, Landroid/content/Intent;

    .line 14
    .line 15
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 16
    .line 17
    .line 18
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇Oo(Ljava/util/ArrayList;Landroid/content/Intent;)Landroid/content/Intent;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 23
    .line 24
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    iget-object v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 29
    .line 30
    const/4 v4, 0x0

    .line 31
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v5

    .line 35
    check-cast v5, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 36
    .line 37
    iget-wide v5, v5, Lcom/intsig/webstorage/UploadFile;->OO:J

    .line 38
    .line 39
    invoke-static {v3, v5, v6}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇〇8O0〇8(Landroid/content/Context;J)I

    .line 40
    .line 41
    .line 42
    move-result v3

    .line 43
    invoke-static {v1, v2, v3}, Lcom/intsig/camscanner/app/IntentUtil;->O8(Landroid/content/Context;II)Landroid/content/Intent;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    new-instance v2, Lcom/intsig/camscanner/fragment/TeamFragment$17;

    .line 48
    .line 49
    invoke-direct {v2, p0, p4, p3, p1}, Lcom/intsig/camscanner/fragment/TeamFragment$17;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;ZZLjava/util/ArrayList;)V

    .line 50
    .line 51
    .line 52
    new-instance v3, Lcom/intsig/camscanner/fragment/TeamFragment$18;

    .line 53
    .line 54
    invoke-direct {v3, p0, p1, p3, p2}, Lcom/intsig/camscanner/fragment/TeamFragment$18;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;Ljava/util/ArrayList;ZLjava/util/ArrayList;)V

    .line 55
    .line 56
    .line 57
    new-instance p3, Lcom/intsig/camscanner/fragment/TeamFragment$19;

    .line 58
    .line 59
    invoke-direct {p3, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$19;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 60
    .line 61
    .line 62
    new-instance v5, Lcom/intsig/camscanner/control/ShareControl$OcrLanguageSetting;

    .line 63
    .line 64
    const/16 v6, 0x82

    .line 65
    .line 66
    invoke-direct {v5, p0, v6}, Lcom/intsig/camscanner/control/ShareControl$OcrLanguageSetting;-><init>(Landroidx/fragment/app/Fragment;I)V

    .line 67
    .line 68
    .line 69
    iput-object v5, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇OO8ooO8〇:Lcom/intsig/camscanner/control/ShareControl$OcrLanguageSetting;

    .line 70
    .line 71
    new-instance v5, Lcom/intsig/camscanner/datastruct/ShareInfo;

    .line 72
    .line 73
    invoke-direct {v5}, Lcom/intsig/camscanner/datastruct/ShareInfo;-><init>()V

    .line 74
    .line 75
    .line 76
    iget-object v6, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 77
    .line 78
    iput-object v6, v5, Lcom/intsig/camscanner/datastruct/ShareInfo;->〇080:Landroid/content/Context;

    .line 79
    .line 80
    iput-boolean v4, v5, Lcom/intsig/camscanner/datastruct/ShareInfo;->〇o00〇〇Oo:Z

    .line 81
    .line 82
    iput-object p2, v5, Lcom/intsig/camscanner/datastruct/ShareInfo;->〇o〇:Ljava/util/ArrayList;

    .line 83
    .line 84
    iput-object v0, v5, Lcom/intsig/camscanner/datastruct/ShareInfo;->O8:Landroid/content/Intent;

    .line 85
    .line 86
    iput-object v1, v5, Lcom/intsig/camscanner/datastruct/ShareInfo;->Oo08:Landroid/content/Intent;

    .line 87
    .line 88
    iput-object v2, v5, Lcom/intsig/camscanner/datastruct/ShareInfo;->o〇0:Lcom/intsig/utils/SquareShareDialogControl$ShareListener;

    .line 89
    .line 90
    iput-object v3, v5, Lcom/intsig/camscanner/datastruct/ShareInfo;->〇〇888:Lcom/intsig/utils/SquareShareDialogControl$ShareListener;

    .line 91
    .line 92
    iput-object p3, v5, Lcom/intsig/camscanner/datastruct/ShareInfo;->oO80:Lcom/intsig/utils/SquareShareDialogControl$ShareListener;

    .line 93
    .line 94
    invoke-direct {p0, p1, p4}, Lcom/intsig/camscanner/fragment/TeamFragment;->O80(Ljava/util/ArrayList;Z)J

    .line 95
    .line 96
    .line 97
    move-result-wide p3

    .line 98
    iput-wide p3, v5, Lcom/intsig/camscanner/datastruct/ShareInfo;->〇80〇808〇O:J

    .line 99
    .line 100
    iget-object p3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 101
    .line 102
    invoke-static {p3, p2}, Lcom/intsig/camscanner/control/ShareControl;->o0ooO(Landroid/content/Context;Ljava/util/ArrayList;)J

    .line 103
    .line 104
    .line 105
    move-result-wide p2

    .line 106
    iput-wide p2, v5, Lcom/intsig/camscanner/datastruct/ShareInfo;->OO0o〇〇〇〇0:J

    .line 107
    .line 108
    iput-boolean v4, v5, Lcom/intsig/camscanner/datastruct/ShareInfo;->〇8o8o〇:Z

    .line 109
    .line 110
    iget-object p2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇OO8ooO8〇:Lcom/intsig/camscanner/control/ShareControl$OcrLanguageSetting;

    .line 111
    .line 112
    iput-object p2, v5, Lcom/intsig/camscanner/datastruct/ShareInfo;->〇O8o08O:Lcom/intsig/camscanner/control/ShareControl$OcrLanguageSetting;

    .line 113
    .line 114
    iput-boolean v4, v5, Lcom/intsig/camscanner/datastruct/ShareInfo;->OO0o〇〇:Z

    .line 115
    .line 116
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 117
    .line 118
    .line 119
    move-result-object p1

    .line 120
    check-cast p1, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 121
    .line 122
    iget-object p1, p1, Lcom/intsig/webstorage/UploadFile;->o0:Ljava/lang/String;

    .line 123
    .line 124
    iput-object p1, v5, Lcom/intsig/camscanner/datastruct/ShareInfo;->Oooo8o0〇:Ljava/lang/String;

    .line 125
    .line 126
    invoke-static {}, Lcom/intsig/camscanner/control/ShareControl;->o〇8()Lcom/intsig/camscanner/control/ShareControl;

    .line 127
    .line 128
    .line 129
    move-result-object p1

    .line 130
    invoke-virtual {p1, v5}, Lcom/intsig/camscanner/control/ShareControl;->O〇0(Lcom/intsig/camscanner/datastruct/ShareInfo;)V

    .line 131
    .line 132
    .line 133
    return-void

    .line 134
    :cond_1
    :goto_0
    sget-object p1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 135
    .line 136
    const-string p2, "shareLinkOrPDF empyt list"

    .line 137
    .line 138
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    .line 140
    .line 141
    return-void
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method static bridge synthetic oO88〇0O8O(Lcom/intsig/camscanner/fragment/TeamFragment;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇o〇〇88〇8()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic oO8o〇08〇(Lcom/intsig/camscanner/fragment/TeamFragment;Landroid/net/Uri;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇O88(Landroid/net/Uri;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private oO8o〇o〇8()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 10
    .line 11
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->〇00(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    const/4 v2, 0x2

    .line 22
    if-ge v1, v2, :cond_0

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    new-instance v1, Landroid/content/Intent;

    .line 26
    .line 27
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 28
    .line 29
    const-class v3, Lcom/intsig/camscanner/merge/ResortMergedDocsActivity;

    .line 30
    .line 31
    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 32
    .line 33
    .line 34
    const-string v2, "extra_list_data"

    .line 35
    .line 36
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 37
    .line 38
    .line 39
    const/16 v0, 0x85

    .line 40
    .line 41
    invoke-virtual {p0, v1, v0}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 42
    .line 43
    .line 44
    return-void

    .line 45
    :cond_1
    :goto_0
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 46
    .line 47
    const-string v1, "doMerge docs number invalid"

    .line 48
    .line 49
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 53
    .line 54
    const v1, 0x7f131d83

    .line 55
    .line 56
    .line 57
    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 58
    .line 59
    .line 60
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic oOO8oo0(Lcom/intsig/camscanner/fragment/TeamFragment;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O〇〇O8:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private oOOO0()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->O8〇o()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x1

    .line 8
    const/4 v2, 0x0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->o〇0OOo〇0(I)V

    .line 14
    .line 15
    .line 16
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 17
    .line 18
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;->O0(Z)V

    .line 19
    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 23
    .line 24
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->o〇0OOo〇0(I)V

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 28
    .line 29
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;->O0(Z)V

    .line 30
    .line 31
    .line 32
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o808Oo()V

    .line 33
    .line 34
    .line 35
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇8Oo()V

    .line 36
    .line 37
    .line 38
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8880()V

    .line 39
    .line 40
    .line 41
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o80oO()V

    .line 42
    .line 43
    .line 44
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 45
    .line 46
    invoke-virtual {v0}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->notifyDataSetChanged()V

    .line 47
    .line 48
    .line 49
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O80O()V

    .line 50
    .line 51
    .line 52
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private oOOo()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->Oo80:Lcom/intsig/app/AlertDialog;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 8
    .line 9
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 10
    .line 11
    .line 12
    const v1, 0x7f1300a9

    .line 13
    .line 14
    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 16
    .line 17
    .line 18
    const v1, 0x7f1302a7

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 22
    .line 23
    .line 24
    const/4 v1, 0x0

    .line 25
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->o〇0(Z)Lcom/intsig/app/AlertDialog$Builder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    iput-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->Oo80:Lcom/intsig/app/AlertDialog;

    .line 33
    .line 34
    new-instance v1, Lcom/intsig/camscanner/fragment/TeamFragment$35;

    .line 35
    .line 36
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$35;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 37
    .line 38
    .line 39
    const v2, 0x7f130019

    .line 40
    .line 41
    .line 42
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 43
    .line 44
    .line 45
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->Oo80:Lcom/intsig/app/AlertDialog;

    .line 46
    .line 47
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    if-eqz v0, :cond_1

    .line 52
    .line 53
    return-void

    .line 54
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 55
    .line 56
    const-string v1, "folder has been delete on other device"

    .line 57
    .line 58
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->Oo80:Lcom/intsig/app/AlertDialog;

    .line 62
    .line 63
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    .line 65
    .line 66
    goto :goto_0

    .line 67
    :catch_0
    move-exception v0

    .line 68
    sget-object v1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 69
    .line 70
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 71
    .line 72
    .line 73
    :goto_0
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private oOOo8〇o()V
    .locals 9

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 10
    .line 11
    new-instance v1, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v2, "mTeamToken="

    .line 17
    .line 18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 22
    .line 23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    return-void

    .line 34
    :cond_0
    const-string v5, "team_token =? "

    .line 35
    .line 36
    const/4 v0, 0x1

    .line 37
    new-array v6, v0, [Ljava/lang/String;

    .line 38
    .line 39
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 40
    .line 41
    const/4 v8, 0x0

    .line 42
    aput-object v1, v6, v8

    .line 43
    .line 44
    const-string v1, "title"

    .line 45
    .line 46
    const-string v2, "top_doc"

    .line 47
    .line 48
    const-string v3, "lock"

    .line 49
    .line 50
    const-string v4, "root_dir_sync_id"

    .line 51
    .line 52
    filled-new-array {v3, v4, v1, v2}, [Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v4

    .line 56
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 57
    .line 58
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 59
    .line 60
    .line 61
    move-result-object v2

    .line 62
    sget-object v3, Lcom/intsig/camscanner/provider/Documents$TeamInfo;->〇080:Landroid/net/Uri;

    .line 63
    .line 64
    const/4 v7, 0x0

    .line 65
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 66
    .line 67
    .line 68
    move-result-object v1

    .line 69
    if-eqz v1, :cond_3

    .line 70
    .line 71
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 72
    .line 73
    .line 74
    move-result v2

    .line 75
    if-eqz v2, :cond_2

    .line 76
    .line 77
    invoke-interface {v1, v8}, Landroid/database/Cursor;->getInt(I)I

    .line 78
    .line 79
    .line 80
    move-result v2

    .line 81
    if-ne v2, v0, :cond_1

    .line 82
    .line 83
    const/4 v8, 0x1

    .line 84
    :cond_1
    iput-boolean v8, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇OOoooo:Z

    .line 85
    .line 86
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇o0〇8:Ljava/lang/String;

    .line 91
    .line 92
    const/4 v0, 0x2

    .line 93
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇8〇008:Ljava/lang/String;

    .line 98
    .line 99
    const/4 v0, 0x3

    .line 100
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    .line 101
    .line 102
    .line 103
    move-result v0

    .line 104
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/teampermission/TeamPermissionUtil;->〇080(I)Z

    .line 105
    .line 106
    .line 107
    move-result v0

    .line 108
    iput-boolean v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇088O:Z

    .line 109
    .line 110
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 111
    .line 112
    new-instance v2, Ljava/lang/StringBuilder;

    .line 113
    .line 114
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 115
    .line 116
    .line 117
    const-string v3, "mEnableAddRootDoc = "

    .line 118
    .line 119
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    .line 121
    .line 122
    iget-boolean v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇088O:Z

    .line 123
    .line 124
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 125
    .line 126
    .line 127
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object v2

    .line 131
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    .line 133
    .line 134
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 135
    .line 136
    .line 137
    :cond_3
    return-void
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private oOO〇0o8〇()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO88o:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇o0〇8:Ljava/lang/String;

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO88o:Ljava/lang/String;

    .line 13
    .line 14
    :goto_0
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private oOO〇OO8(I)V
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Ljava/lang/Object;

    .line 3
    .line 4
    new-instance v1, Ljava/lang/StringBuilder;

    .line 5
    .line 6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 7
    .line 8
    .line 9
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 10
    .line 11
    .line 12
    const-string p1, ""

    .line 13
    .line 14
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    const/4 v1, 0x0

    .line 22
    aput-object p1, v0, v1

    .line 23
    .line 24
    const p1, 0x7f130164

    .line 25
    .line 26
    .line 27
    invoke-virtual {p0, p1, v0}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->OO〇OOo:Landroid/widget/TextView;

    .line 32
    .line 33
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 34
    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic oOoO8OO〇(Lcom/intsig/camscanner/fragment/TeamFragment;)Landroid/app/Dialog;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8〇OO:Landroid/app/Dialog;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic oOo〇08〇(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0O0Oo〇(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private oOo〇0o8〇8()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO0880O:Landroid/os/Handler;

    .line 2
    .line 3
    const/16 v1, 0x9

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO0880O:Landroid/os/Handler;

    .line 9
    .line 10
    const-wide/16 v2, 0xc8

    .line 11
    .line 12
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic oO〇O0O(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇O80ooo()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic oO〇oo(Lcom/intsig/camscanner/fragment/TeamFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/TeamFragment;->OOO80〇〇88(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic oo0O(Lcom/intsig/camscanner/fragment/TeamFragment;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/TeamFragment;->O00〇8(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic oo8(Lcom/intsig/camscanner/fragment/TeamFragment;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇OO0oO(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic oo88(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oO800o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private oo8O8o80(Landroid/content/Intent;)V
    .locals 5

    .line 1
    if-eqz p1, :cond_2

    .line 2
    .line 3
    const-string v0, "team_info"

    .line 4
    .line 5
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/camscanner/datastruct/TeamInfo;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    iget-object v1, v0, Lcom/intsig/camscanner/datastruct/TeamInfo;->o0:Ljava/lang/String;

    .line 14
    .line 15
    iput-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 16
    .line 17
    iget-object v1, v0, Lcom/intsig/camscanner/datastruct/TeamInfo;->〇OOo8〇0:Ljava/lang/String;

    .line 18
    .line 19
    iput-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o88:Ljava/lang/String;

    .line 20
    .line 21
    iget v1, v0, Lcom/intsig/camscanner/datastruct/TeamInfo;->o〇00O:I

    .line 22
    .line 23
    iput v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇0〇o:I

    .line 24
    .line 25
    iget v0, v0, Lcom/intsig/camscanner/datastruct/TeamInfo;->O8o08O8O:I

    .line 26
    .line 27
    iput v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇0:I

    .line 28
    .line 29
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOOo8〇o()V

    .line 30
    .line 31
    .line 32
    const-string v0, "extra_new_doc_id"

    .line 33
    .line 34
    const-wide/16 v1, -0x1

    .line 35
    .line 36
    invoke-virtual {p1, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 37
    .line 38
    .line 39
    move-result-wide v0

    .line 40
    const-wide/16 v2, 0x0

    .line 41
    .line 42
    cmp-long v4, v0, v2

    .line 43
    .line 44
    if-lez v4, :cond_1

    .line 45
    .line 46
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O〇〇〇(J)V

    .line 47
    .line 48
    .line 49
    :cond_1
    const-string v0, "extra_team_folder_syncid"

    .line 50
    .line 51
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->ooO:Ljava/lang/String;

    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOOo8〇o()V

    .line 59
    .line 60
    .line 61
    :goto_0
    sget-object p1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 62
    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    .line 64
    .line 65
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 66
    .line 67
    .line 68
    const-string v1, "mTeamName:"

    .line 69
    .line 70
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o88:Ljava/lang/String;

    .line 74
    .line 75
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    const-string v1, ", mMaxLayerNum:"

    .line 79
    .line 80
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    iget v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇0:I

    .line 84
    .line 85
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    const-string v1, ", mReviewOpen:"

    .line 89
    .line 90
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    iget-boolean v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇OOoooo:Z

    .line 94
    .line 95
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 96
    .line 97
    .line 98
    const-string v1, ", mTeamArea:"

    .line 99
    .line 100
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    .line 102
    .line 103
    iget v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇0〇o:I

    .line 104
    .line 105
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 106
    .line 107
    .line 108
    const-string v1, ", mTeamFolderSyncId:"

    .line 109
    .line 110
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    .line 112
    .line 113
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->ooO:Ljava/lang/String;

    .line 114
    .line 115
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    .line 117
    .line 118
    const-string v1, ", sParentSyncId"

    .line 119
    .line 120
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    .line 122
    .line 123
    sget-object v1, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO88o:Ljava/lang/String;

    .line 124
    .line 125
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    .line 127
    .line 128
    const-string v1, ", mTeamEntrySyncId="

    .line 129
    .line 130
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    .line 132
    .line 133
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇o0〇8:Ljava/lang/String;

    .line 134
    .line 135
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    .line 137
    .line 138
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 139
    .line 140
    .line 141
    move-result-object v0

    .line 142
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    .line 144
    .line 145
    return-void
    .line 146
    .line 147
.end method

.method static bridge synthetic oo8〇〇(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->OOOo〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private ooO888O0〇()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "User Operation: move doc"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 9
    .line 10
    new-instance v1, Lcom/intsig/camscanner/fragment/TeamFragment$34;

    .line 11
    .line 12
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$34;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 13
    .line 14
    .line 15
    invoke-static {v0, v1}, Lcom/intsig/camscanner/control/ConnectChecker;->〇080(Landroid/content/Context;Lcom/intsig/camscanner/control/ConnectChecker$ActionListener;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic ooo008(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->Oo〇0o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private ooo0〇080()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "User Operation: go2Verify"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSTeamfolder"

    .line 9
    .line 10
    const-string v1, "review"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 16
    .line 17
    new-instance v1, Lcom/intsig/camscanner/fragment/TeamFragment$38;

    .line 18
    .line 19
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$38;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 20
    .line 21
    .line 22
    invoke-static {v0, v1}, Lcom/intsig/camscanner/control/ConnectChecker;->〇080(Landroid/content/Context;Lcom/intsig/camscanner/control/ConnectChecker$ActionListener;)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic oooO888(Lcom/intsig/camscanner/fragment/TeamFragment;Ljava/util/ArrayList;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oo8(Ljava/util/ArrayList;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic oooO8〇00(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇o8o〇〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic oooo800〇〇(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o80oO()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic ooooo0O(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oO8o〇o〇8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private oo〇88()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o80()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇08〇o0O:[Ljava/lang/String;

    .line 9
    .line 10
    array-length v0, v0

    .line 11
    new-array v0, v0, [Ljava/lang/String;

    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->Oo0〇Ooo:[Ljava/lang/String;

    .line 14
    .line 15
    const/4 v0, 0x0

    .line 16
    :goto_0
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇08〇o0O:[Ljava/lang/String;

    .line 17
    .line 18
    array-length v3, v2

    .line 19
    if-ge v0, v3, :cond_0

    .line 20
    .line 21
    iget-object v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->Oo0〇Ooo:[Ljava/lang/String;

    .line 22
    .line 23
    aget-object v2, v2, v0

    .line 24
    .line 25
    aput-object v2, v3, v0

    .line 26
    .line 27
    add-int/lit8 v0, v0, 0x1

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_0
    const/4 v0, 0x0

    .line 31
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇08〇o0O:[Ljava/lang/String;

    .line 32
    .line 33
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO〇0o8〇()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇OO〇00〇0O:Ljava/lang/String;

    .line 38
    .line 39
    iput v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOo0:I

    .line 40
    .line 41
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 42
    .line 43
    invoke-static {v0}, Lcom/intsig/utils/SoftKeyboardUtils;->〇080(Landroid/app/Activity;)V

    .line 44
    .line 45
    .line 46
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇800OO〇0O:Landroidx/appcompat/widget/Toolbar;

    .line 47
    .line 48
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 49
    .line 50
    .line 51
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO8:Landroid/widget/LinearLayout;

    .line 52
    .line 53
    const/16 v1, 0x8

    .line 54
    .line 55
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 56
    .line 57
    .line 58
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇o88:Landroid/widget/RelativeLayout;

    .line 59
    .line 60
    if-eqz v0, :cond_1

    .line 61
    .line 62
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 63
    .line 64
    .line 65
    :cond_1
    return-void
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic oo〇O0o〇(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇OoOo()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o〇08oO80o(Lcom/intsig/camscanner/fragment/TeamFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->Ooo08:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o〇0〇o(Lcom/intsig/camscanner/fragment/TeamFragment;)Lcom/intsig/view/ImageTextButton;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOoo80oO:Lcom/intsig/view/ImageTextButton;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o〇O0ooo()V
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->Oo0〇Ooo:[Ljava/lang/String;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    array-length v2, v0

    .line 7
    if-lez v2, :cond_0

    .line 8
    .line 9
    array-length v0, v0

    .line 10
    new-array v0, v0, [Ljava/lang/String;

    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇08〇o0O:[Ljava/lang/String;

    .line 13
    .line 14
    const/4 v0, 0x0

    .line 15
    :goto_0
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->Oo0〇Ooo:[Ljava/lang/String;

    .line 16
    .line 17
    array-length v3, v2

    .line 18
    if-ge v0, v3, :cond_0

    .line 19
    .line 20
    iget-object v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇08〇o0O:[Ljava/lang/String;

    .line 21
    .line 22
    aget-object v2, v2, v0

    .line 23
    .line 24
    aput-object v2, v3, v0

    .line 25
    .line 26
    add-int/lit8 v0, v0, 0x1

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    const/4 v0, 0x0

    .line 30
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->Oo0〇Ooo:[Ljava/lang/String;

    .line 31
    .line 32
    const-string v0, "origin_parent_sync_id"

    .line 33
    .line 34
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇OO〇00〇0O:Ljava/lang/String;

    .line 35
    .line 36
    const/4 v0, 0x1

    .line 37
    iput v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOo0:I

    .line 38
    .line 39
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇800OO〇0O:Landroidx/appcompat/widget/Toolbar;

    .line 40
    .line 41
    const/16 v3, 0x8

    .line 42
    .line 43
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 44
    .line 45
    .line 46
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO8:Landroid/widget/LinearLayout;

    .line 47
    .line 48
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 49
    .line 50
    .line 51
    sget-object v2, Lcom/intsig/camscanner/fragment/TeamFragment;->o808o8o08:Ljava/util/ArrayList;

    .line 52
    .line 53
    if-eqz v2, :cond_1

    .line 54
    .line 55
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    .line 56
    .line 57
    .line 58
    move-result v2

    .line 59
    if-lez v2, :cond_1

    .line 60
    .line 61
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇o88:Landroid/widget/RelativeLayout;

    .line 62
    .line 63
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 64
    .line 65
    .line 66
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8O:Landroid/widget/TextView;

    .line 67
    .line 68
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 69
    .line 70
    .line 71
    move-result-object v3

    .line 72
    new-array v4, v0, [Ljava/lang/Object;

    .line 73
    .line 74
    sget-object v5, Lcom/intsig/camscanner/fragment/TeamFragment;->o808o8o08:Ljava/util/ArrayList;

    .line 75
    .line 76
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    .line 77
    .line 78
    .line 79
    move-result v6

    .line 80
    sub-int/2addr v6, v0

    .line 81
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 82
    .line 83
    .line 84
    move-result-object v0

    .line 85
    check-cast v0, Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 86
    .line 87
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/FolderItem;->〇O00()Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    aput-object v0, v4, v1

    .line 92
    .line 93
    const v0, 0x7f1301b2

    .line 94
    .line 95
    .line 96
    invoke-virtual {v3, v0, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object v0

    .line 100
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    .line 102
    .line 103
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇O:Landroid/widget/EditText;

    .line 104
    .line 105
    if-eqz v0, :cond_2

    .line 106
    .line 107
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 108
    .line 109
    .line 110
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇〇0o〇〇0:Ljava/lang/CharSequence;

    .line 111
    .line 112
    if-eqz v0, :cond_2

    .line 113
    .line 114
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇O:Landroid/widget/EditText;

    .line 115
    .line 116
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    .line 118
    .line 119
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇O:Landroid/widget/EditText;

    .line 120
    .line 121
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇〇0o〇〇0:Ljava/lang/CharSequence;

    .line 122
    .line 123
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    .line 124
    .line 125
    .line 126
    move-result v1

    .line 127
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 128
    .line 129
    .line 130
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 131
    .line 132
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇O:Landroid/widget/EditText;

    .line 133
    .line 134
    invoke-static {v0, v1}, Lcom/intsig/utils/SoftKeyboardUtils;->O8(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 135
    .line 136
    .line 137
    return-void
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic o〇O80o8OO(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->Oo0o0o8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o〇O8OO(Lcom/intsig/camscanner/fragment/TeamFragment;)Lcom/intsig/camscanner/fragment/TeamFragment$DeviceInterface;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O8〇8000:Lcom/intsig/camscanner/fragment/TeamFragment$DeviceInterface;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o〇Oo(Ljava/util/ArrayList;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/datastruct/DocumentListItem;",
            ">;",
            "Landroid/content/Intent;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "getShareDocumentsIntent"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "application/pdf"

    .line 9
    .line 10
    invoke-virtual {p2, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 11
    .line 12
    .line 13
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    const/4 v1, 0x1

    .line 18
    const-string v2, "android.intent.extra.STREAM"

    .line 19
    .line 20
    if-ne v0, v1, :cond_0

    .line 21
    .line 22
    const-string v0, "android.intent.action.SEND"

    .line 23
    .line 24
    invoke-virtual {p2, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 25
    .line 26
    .line 27
    const/4 v0, 0x0

    .line 28
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    check-cast p1, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 33
    .line 34
    invoke-virtual {p1}, Lcom/intsig/webstorage/UploadFile;->〇o00〇〇Oo()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0〇O0088o(Ljava/lang/String;)Landroid/net/Uri;

    .line 39
    .line 40
    .line 41
    move-result-object p1

    .line 42
    invoke-virtual {p2, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 43
    .line 44
    .line 45
    goto :goto_1

    .line 46
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    .line 47
    .line 48
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 49
    .line 50
    .line 51
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    if-eqz v1, :cond_1

    .line 60
    .line 61
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 62
    .line 63
    .line 64
    move-result-object v1

    .line 65
    check-cast v1, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 66
    .line 67
    invoke-virtual {v1}, Lcom/intsig/webstorage/UploadFile;->〇o00〇〇Oo()Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object v1

    .line 71
    invoke-static {v1}, Lcom/intsig/utils/FileUtil;->〇0〇O0088o(Ljava/lang/String;)Landroid/net/Uri;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_1
    const-string p1, "android.intent.action.SEND_MULTIPLE"

    .line 80
    .line 81
    invoke-virtual {p2, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 82
    .line 83
    .line 84
    invoke-virtual {p2, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 85
    .line 86
    .line 87
    :goto_1
    return-object p2
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method static bridge synthetic o〇OoO0(Lcom/intsig/camscanner/fragment/TeamFragment;Ljava/lang/CharSequence;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇〇0o〇〇0:Ljava/lang/CharSequence;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic o〇o08〇(Lcom/intsig/camscanner/fragment/TeamFragment;)Landroidx/appcompat/widget/Toolbar;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇800OO〇0O:Landroidx/appcompat/widget/Toolbar;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic o〇o0o8Oo(Ljava/util/ArrayList;I)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 2
    .line 3
    .line 4
    move-result p2

    .line 5
    new-array v0, p2, [J

    .line 6
    .line 7
    const/4 v1, 0x0

    .line 8
    :goto_0
    if-ge v1, p2, :cond_0

    .line 9
    .line 10
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 11
    .line 12
    .line 13
    move-result-object v2

    .line 14
    check-cast v2, Ljava/lang/Long;

    .line 15
    .line 16
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    .line 17
    .line 18
    .line 19
    move-result-wide v2

    .line 20
    aput-wide v2, v0, v1

    .line 21
    .line 22
    add-int/lit8 v1, v1, 0x1

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    new-instance p1, Landroid/content/Intent;

    .line 26
    .line 27
    iget-object p2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 28
    .line 29
    const-class v1, Lcom/intsig/camscanner/morc/MoveOrCopyDocActivity;

    .line 30
    .line 31
    invoke-direct {p1, p2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 32
    .line 33
    .line 34
    const-string p2, "extra_multi_doc_id"

    .line 35
    .line 36
    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 37
    .line 38
    .line 39
    const-string p2, "extra_folder_id"

    .line 40
    .line 41
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO〇0o8〇()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 46
    .line 47
    .line 48
    const-string p2, "action"

    .line 49
    .line 50
    const/4 v0, 0x1

    .line 51
    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 52
    .line 53
    .line 54
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    .line 55
    .line 56
    .line 57
    move-result p2

    .line 58
    if-eqz p2, :cond_1

    .line 59
    .line 60
    const/16 p2, 0x81

    .line 61
    .line 62
    invoke-virtual {p0, p1, p2}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 63
    .line 64
    .line 65
    goto :goto_1

    .line 66
    :cond_1
    sget-object p1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 67
    .line 68
    const-string p2, "activity not attach when move doc"

    .line 69
    .line 70
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    :goto_1
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method static bridge synthetic o〇o0oOO8(Lcom/intsig/camscanner/fragment/TeamFragment;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇88()Ljava/util/ArrayList;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o〇o0〇0O〇o()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O8〇8000:Lcom/intsig/camscanner/fragment/TeamFragment$DeviceInterface;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/fragment/TeamFragment$DeviceInterface;->〇o〇()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->〇O00()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o08(I)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic o〇o8〇〇O(Lcom/intsig/camscanner/fragment/TeamFragment;J)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/fragment/TeamFragment;->O0oOo(J)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic o〇oO08〇o0(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOo〇0o8〇8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o〇oo(Lcom/intsig/camscanner/fragment/TeamFragment;)Landroid/widget/LinearLayout;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO8:Landroid/widget/LinearLayout;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o〇〇8〇〇()V
    .locals 9

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "User Operation: create new team folder"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSBusiness"

    .line 9
    .line 10
    const-string v1, "create_folder"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO〇0o8〇()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v3

    .line 21
    const v4, 0x7f13021e

    .line 22
    .line 23
    .line 24
    const/4 v5, 0x0

    .line 25
    const/4 v6, 0x1

    .line 26
    const/4 v7, 0x0

    .line 27
    new-instance v8, Lcom/intsig/camscanner/fragment/TeamFragment$31;

    .line 28
    .line 29
    invoke-direct {v8, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$31;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 30
    .line 31
    .line 32
    invoke-static/range {v2 .. v8}, Lcom/intsig/camscanner/app/DialogUtils;->o8(Landroid/app/Activity;Ljava/lang/String;IIZLjava/lang/String;Lcom/intsig/camscanner/app/DialogUtils$OnDocTitleEditListener;)V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇00O()V
    .locals 6

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇oO:J

    .line 2
    .line 3
    const-wide/16 v2, -0x2

    .line 4
    .line 5
    const/4 v4, 0x0

    .line 6
    cmp-long v5, v0, v2

    .line 7
    .line 8
    if-eqz v5, :cond_0

    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 11
    .line 12
    if-eqz v0, :cond_2

    .line 13
    .line 14
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 15
    .line 16
    const-string v1, "hideFolder changeFolderData == null"

    .line 17
    .line 18
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 22
    .line 23
    invoke-virtual {v0, v4}, Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;->〇〇808〇(Landroid/database/Cursor;)V

    .line 24
    .line 25
    .line 26
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇8OO()V

    .line 27
    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O8〇8〇O80:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 31
    .line 32
    if-nez v0, :cond_1

    .line 33
    .line 34
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8Oo8〇8()V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getLoaderManager()Landroidx/loader/app/LoaderManager;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    iget v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0O0〇:I

    .line 42
    .line 43
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O8〇8〇O80:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 44
    .line 45
    invoke-virtual {v0, v1, v4, v2}, Landroidx/loader/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroidx/loader/app/LoaderManager$LoaderCallbacks;)Landroidx/loader/content/Loader;

    .line 46
    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getLoaderManager()Landroidx/loader/app/LoaderManager;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    iget v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0O0〇:I

    .line 54
    .line 55
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O8〇8〇O80:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 56
    .line 57
    invoke-virtual {v0, v1, v4, v2}, Landroidx/loader/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroidx/loader/app/LoaderManager$LoaderCallbacks;)Landroidx/loader/content/Loader;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    .line 59
    .line 60
    goto :goto_0

    .line 61
    :catch_0
    move-exception v0

    .line 62
    sget-object v1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 63
    .line 64
    const-string v2, "updateFolderInfo"

    .line 65
    .line 66
    invoke-static {v1, v2, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 67
    .line 68
    .line 69
    :cond_2
    :goto_0
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private 〇00O00o()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->〇O00()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO〇OO8(I)V

    .line 8
    .line 9
    .line 10
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o08(I)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇00o80oo(I)V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 10
    .line 11
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->〇00(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    new-instance v1, Ljava/util/ArrayList;

    .line 16
    .line 17
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    if-lez v2, :cond_1

    .line 25
    .line 26
    const/4 v3, 0x0

    .line 27
    :goto_0
    if-ge v3, v2, :cond_0

    .line 28
    .line 29
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v4

    .line 33
    check-cast v4, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 34
    .line 35
    iget-wide v4, v4, Lcom/intsig/webstorage/UploadFile;->OO:J

    .line 36
    .line 37
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 38
    .line 39
    .line 40
    move-result-object v4

    .line 41
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    .line 43
    .line 44
    add-int/lit8 v3, v3, 0x1

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_0
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 48
    .line 49
    new-instance v3, L〇8〇o〇8/〇o〇8;

    .line 50
    .line 51
    invoke-direct {v3, p0, v1, v0, p1}, L〇8〇o〇8/〇o〇8;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    .line 52
    .line 53
    .line 54
    invoke-static {v2, v1, v3}, Lcom/intsig/camscanner/control/DataChecker;->〇O8o08O(Landroid/app/Activity;Ljava/util/ArrayList;Lcom/intsig/camscanner/control/DataChecker$ActionListener;)Z

    .line 55
    .line 56
    .line 57
    const/4 p1, 0x1

    .line 58
    iput-boolean p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->OO〇00〇8oO:Z

    .line 59
    .line 60
    :cond_1
    return-void
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic 〇00o〇O8(Lcom/intsig/camscanner/fragment/TeamFragment;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->OoOO〇()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇00〇〇〇o〇8(Lcom/intsig/camscanner/fragment/TeamFragment;Lcom/intsig/camscanner/datastruct/DocumentListItem;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O〇o0(Lcom/intsig/camscanner/datastruct/DocumentListItem;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic 〇0880O0〇()V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "doDelete() delete multi documents"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 11
    .line 12
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 17
    .line 18
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->O〇8O8〇008(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 23
    .line 24
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    const/4 v2, 0x2

    .line 29
    invoke-static {v1, v0, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Oo8(Landroid/content/Context;Ljava/util/ArrayList;I)V

    .line 30
    .line 31
    .line 32
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 33
    .line 34
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 35
    .line 36
    invoke-static {v1, v2}, Lcom/intsig/camscanner/util/PreferenceHelper;->O〇080〇o0(Landroid/content/Context;Ljava/lang/String;)J

    .line 37
    .line 38
    .line 39
    move-result-wide v1

    .line 40
    iget-object v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 41
    .line 42
    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 43
    .line 44
    .line 45
    move-result-object v3

    .line 46
    invoke-static {v3, v0, v1, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o〇0〇(Landroid/content/Context;Ljava/util/ArrayList;J)V

    .line 47
    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO0880O:Landroid/os/Handler;

    .line 50
    .line 51
    const/4 v1, 0x5

    .line 52
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 53
    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic 〇0888(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->OOO〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇088O(Lcom/intsig/camscanner/fragment/TeamFragment;)Lcom/intsig/view/ImageTextButton;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇08O:Lcom/intsig/view/ImageTextButton;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇08O(Lcom/intsig/camscanner/fragment/TeamFragment;)Landroid/widget/ImageView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O8〇o〇88:Landroid/widget/ImageView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static synthetic 〇0O0Oo〇(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇0O8Oo(Lcom/intsig/camscanner/fragment/TeamFragment;J)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇〇O(J)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇0OOoO8O0()V
    .locals 2

    .line 1
    const/16 v0, 0xd7

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇8O〇(I)V

    .line 4
    .line 5
    .line 6
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    new-instance v1, Lcom/intsig/camscanner/fragment/TeamFragment$1;

    .line 11
    .line 12
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$1;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 13
    .line 14
    .line 15
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method private 〇0o(Z)V
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇O8OO:Lcom/intsig/camscanner/view/PullToSyncView;

    .line 4
    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->〇O8o08O()V

    .line 8
    .line 9
    .line 10
    :cond_0
    sget-object p1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 11
    .line 12
    const-string v0, "User Operation: to edit mode"

    .line 13
    .line 14
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOOO0()V

    .line 18
    .line 19
    .line 20
    const/4 p1, 0x0

    .line 21
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/TeamFragment;->O00〇8(Z)V

    .line 22
    .line 23
    .line 24
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇00O00o()V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic 〇0o0oO〇〇0(Lcom/intsig/camscanner/fragment/TeamFragment;Lcom/intsig/camscanner/datastruct/DocumentListItem;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oO〇8O8oOo:Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇0o88O(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->Oo088O〇8〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇0o88Oo〇(Lcom/intsig/camscanner/fragment/TeamFragment;Lcom/intsig/view/ImageTextButton;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O8oOo0:Lcom/intsig/view/ImageTextButton;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇0oO〇oo00(Lcom/intsig/camscanner/fragment/TeamFragment;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇o08:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇0ooOOo(Lcom/intsig/camscanner/fragment/TeamFragment;)Landroid/widget/AbsListView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇oO〇〇8o:Landroid/widget/AbsListView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇0o〇o(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o8o〇8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇0〇(Ljava/lang/String;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "doCheckFolderSameName: "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    if-nez v1, :cond_0

    .line 28
    .line 29
    new-instance v1, Lcom/intsig/camscanner/fragment/TeamFragment$32;

    .line 30
    .line 31
    invoke-direct {v1, p0, p1}, Lcom/intsig/camscanner/fragment/TeamFragment$32;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->Oooo8o0〇(Ljava/lang/String;)Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    invoke-virtual {p1}, Lcom/intsig/camscanner/capture/certificatephoto/util/CustomAsyncTask;->o〇0()V

    .line 39
    .line 40
    .line 41
    :cond_0
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic 〇0〇0(Lcom/intsig/camscanner/fragment/TeamFragment;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇oO:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇0〇8o〇()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isDetached()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const v0, 0x7f131cfa

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇00O0:Ljava/lang/String;

    .line 22
    .line 23
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    if-nez v0, :cond_1

    .line 28
    .line 29
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 30
    .line 31
    const-string v1, "doDelete getActivity()==null"

    .line 32
    .line 33
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    :cond_1
    const/16 v0, 0xd7

    .line 37
    .line 38
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇8O〇(I)V

    .line 39
    .line 40
    .line 41
    new-instance v0, L〇8〇o〇8/ooo〇〇O〇;

    .line 42
    .line 43
    invoke-direct {v0, p0}, L〇8〇o〇8/ooo〇〇O〇;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 44
    .line 45
    .line 46
    invoke-static {v0}, Lcom/intsig/thread/ThreadPoolSingleton;->〇080(Ljava/lang/Runnable;)V

    .line 47
    .line 48
    .line 49
    return-void

    .line 50
    :cond_2
    :goto_0
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 51
    .line 52
    const-string v1, "doDelete isDetached()"

    .line 53
    .line 54
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic 〇0〇o8〇(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇0o8O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇0〇〇o0()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->O8〇o()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O8〇8000:Lcom/intsig/camscanner/fragment/TeamFragment$DeviceInterface;

    .line 10
    .line 11
    invoke-interface {v0}, Lcom/intsig/camscanner/fragment/TeamFragment$DeviceInterface;->OO0o〇〇〇〇0()V

    .line 12
    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇o0〇0O〇o()V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇80O(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O80〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇80O80O〇0()I
    .locals 1

    .line 1
    sget v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇0O〇Oo〇o:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇80O8o8O〇(Lcom/intsig/camscanner/fragment/TeamFragment;Ljava/util/ArrayList;Ljava/util/ArrayList;ZZI)V
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p5}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8OooO0(Ljava/util/ArrayList;Ljava/util/ArrayList;ZZI)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method static bridge synthetic 〇80〇(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0〇8o〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇88()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 10
    .line 11
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->〇00(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    new-instance v1, Ljava/util/ArrayList;

    .line 16
    .line 17
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 21
    .line 22
    .line 23
    move-result v2

    .line 24
    if-lez v2, :cond_0

    .line 25
    .line 26
    const/4 v3, 0x0

    .line 27
    :goto_0
    if-ge v3, v2, :cond_0

    .line 28
    .line 29
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v4

    .line 33
    check-cast v4, Lcom/intsig/camscanner/datastruct/DocumentListItem;

    .line 34
    .line 35
    iget-wide v4, v4, Lcom/intsig/webstorage/UploadFile;->OO:J

    .line 36
    .line 37
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 38
    .line 39
    .line 40
    move-result-object v4

    .line 41
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    .line 43
    .line 44
    add-int/lit8 v3, v3, 0x1

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_0
    return-object v1
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇8880()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO8:Landroid/widget/LinearLayout;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇O:Landroid/widget/EditText;

    .line 14
    .line 15
    invoke-static {v0, v1}, Lcom/intsig/utils/SoftKeyboardUtils;->O8(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇O:Landroid/widget/EditText;

    .line 19
    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 27
    .line 28
    invoke-static {v0}, Lcom/intsig/utils/SoftKeyboardUtils;->〇080(Landroid/app/Activity;)V

    .line 29
    .line 30
    .line 31
    :cond_1
    :goto_0
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic 〇8O(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOOo8〇o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇8O0880(Lcom/intsig/camscanner/fragment/TeamFragment;)Lcom/intsig/view/ImageTextButton;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O8oOo0:Lcom/intsig/view/ImageTextButton;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇8Oo8〇8()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O8〇8〇O80:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/fragment/TeamFragment$33;

    .line 6
    .line 7
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$33;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O8〇8〇O80:Landroidx/loader/app/LoaderManager$LoaderCallbacks;

    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private synthetic 〇8OooO0(Ljava/util/ArrayList;Ljava/util/ArrayList;ZZI)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/fragment/TeamFragment;->oO80O0(Ljava/util/ArrayList;Ljava/util/ArrayList;ZZ)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private 〇8o088〇0()Landroid/view/View;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const v1, 0x7f0d0716

    .line 8
    .line 9
    .line 10
    const/4 v2, 0x0

    .line 11
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const v1, 0x7f0a0f2c

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    check-cast v1, Landroid/widget/EditText;

    .line 23
    .line 24
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 25
    .line 26
    invoke-static {v2, v1}, Lcom/intsig/utils/SoftKeyboardUtils;->O8(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 27
    .line 28
    .line 29
    return-object v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method static bridge synthetic 〇8o0o0(Lcom/intsig/camscanner/fragment/TeamFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0〇(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇8o8o()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8〇OO:Landroid/app/Dialog;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Landroid/app/Dialog;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 8
    .line 9
    const v2, 0x7f1401d6

    .line 10
    .line 11
    .line 12
    invoke-direct {v0, v1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 13
    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8〇OO:Landroid/app/Dialog;

    .line 16
    .line 17
    const/4 v1, 0x1

    .line 18
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 19
    .line 20
    .line 21
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8〇OO:Landroid/app/Dialog;

    .line 22
    .line 23
    new-instance v1, Lcom/intsig/camscanner/fragment/TeamFragment$28;

    .line 24
    .line 25
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$28;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 29
    .line 30
    .line 31
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->Ooo08:Landroid/view/View;

    .line 32
    .line 33
    if-nez v0, :cond_1

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 36
    .line 37
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    const v1, 0x7f0d05b5

    .line 42
    .line 43
    .line 44
    const/4 v2, 0x0

    .line 45
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->Ooo08:Landroid/view/View;

    .line 50
    .line 51
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 52
    .line 53
    .line 54
    move-result-wide v0

    .line 55
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->Ooo08:Landroid/view/View;

    .line 56
    .line 57
    new-instance v3, Lcom/intsig/camscanner/fragment/TeamFragment$29;

    .line 58
    .line 59
    invoke-direct {v3, p0, v0, v1}, Lcom/intsig/camscanner/fragment/TeamFragment$29;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;J)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v2, v3}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 63
    .line 64
    .line 65
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8〇OO:Landroid/app/Dialog;

    .line 66
    .line 67
    invoke-virtual {v2}, Landroid/app/Dialog;->isShowing()Z

    .line 68
    .line 69
    .line 70
    move-result v2

    .line 71
    if-nez v2, :cond_2

    .line 72
    .line 73
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8〇OO:Landroid/app/Dialog;

    .line 74
    .line 75
    iget-object v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->Ooo08:Landroid/view/View;

    .line 76
    .line 77
    invoke-virtual {v2, v3}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 78
    .line 79
    .line 80
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->Ooo08:Landroid/view/View;

    .line 81
    .line 82
    new-instance v3, Lcom/intsig/camscanner/fragment/TeamFragment$30;

    .line 83
    .line 84
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$30;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 85
    .line 86
    .line 87
    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    .line 89
    .line 90
    sget-object v2, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 91
    .line 92
    new-instance v3, Ljava/lang/StringBuilder;

    .line 93
    .line 94
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 95
    .line 96
    .line 97
    const-string v4, "mDialogTip show consume time: "

    .line 98
    .line 99
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 103
    .line 104
    .line 105
    move-result-wide v4

    .line 106
    sub-long/2addr v4, v0

    .line 107
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 108
    .line 109
    .line 110
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 111
    .line 112
    .line 113
    move-result-object v0

    .line 114
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    .line 116
    .line 117
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8〇OO:Landroid/app/Dialog;

    .line 118
    .line 119
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 120
    .line 121
    .line 122
    :cond_2
    return-void
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic 〇8oo0oO0(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o00oooo()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇8oo8888(Lcom/intsig/camscanner/fragment/TeamFragment;Landroid/net/Uri;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/TeamFragment;->o088〇〇(Landroid/net/Uri;)Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇8ooOO(Lcom/intsig/camscanner/fragment/TeamFragment;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O88Oo8()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇8ooo(Lcom/intsig/camscanner/fragment/TeamFragment;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o80()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇8oo〇〇oO(Lcom/intsig/camscanner/fragment/TeamFragment;Ljava/util/ArrayList;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇Oo(Ljava/util/ArrayList;Landroid/content/Intent;)Landroid/content/Intent;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic 〇8〇(Ljava/util/ArrayList;Ljava/util/ArrayList;II)V
    .locals 4

    .line 1
    new-instance p4, Landroid/content/Intent;

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    const-class v1, Lcom/intsig/camscanner/uploadfaxprint/UploadFaxPrintActivity;

    .line 6
    .line 7
    const-string v2, "android.intent.action.SEND"

    .line 8
    .line 9
    const/4 v3, 0x0

    .line 10
    invoke-direct {p4, v2, v3, v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    const-string v1, "SEND_TYPE"

    .line 18
    .line 19
    const-string v2, "User Operation: go upload"

    .line 20
    .line 21
    const/4 v3, 0x1

    .line 22
    if-le v0, v3, :cond_0

    .line 23
    .line 24
    sget-object p1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 25
    .line 26
    invoke-static {p1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    const/16 p1, 0xb

    .line 30
    .line 31
    invoke-virtual {p4, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 32
    .line 33
    .line 34
    const-string p1, "ids"

    .line 35
    .line 36
    invoke-virtual {p4, p1, p2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 37
    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 41
    .line 42
    .line 43
    move-result p2

    .line 44
    if-ne p2, v3, :cond_2

    .line 45
    .line 46
    const/16 p2, 0xa

    .line 47
    .line 48
    invoke-virtual {p4, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 49
    .line 50
    .line 51
    const/4 p2, 0x0

    .line 52
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    check-cast p1, Ljava/io/Serializable;

    .line 57
    .line 58
    const-string p2, "doc_id"

    .line 59
    .line 60
    invoke-virtual {p4, p2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 61
    .line 62
    .line 63
    if-ne p3, v3, :cond_1

    .line 64
    .line 65
    sget-object p1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 66
    .line 67
    const-string p2, "User Operation: go fax"

    .line 68
    .line 69
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    .line 71
    .line 72
    const-string p1, "actiontype"

    .line 73
    .line 74
    invoke-virtual {p4, p1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 75
    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_1
    sget-object p1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 79
    .line 80
    invoke-static {p1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    :cond_2
    :goto_0
    invoke-virtual {p0, p4}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 84
    .line 85
    .line 86
    return-void
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method static bridge synthetic 〇8〇0O〇(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->ooo0〇080()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇8〇80o(Lcom/intsig/camscanner/fragment/TeamFragment;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇oo:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇8〇8o00(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇00O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/fragment/TeamFragment;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o00〇88〇08:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇8〇o〇OoO8(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇0Oo0880()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇8〇〇8o(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇〇8〇〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇8〇〇8〇8(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOOo()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇O0OO8O(Lcom/intsig/camscanner/view/PullToSyncView;)V
    .locals 1

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇O8OO:Lcom/intsig/camscanner/view/PullToSyncView;

    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->setNormalMode(Z)V

    .line 8
    .line 9
    .line 10
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0〇0:Landroid/view/View;

    .line 11
    .line 12
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->setOverlayView(Landroid/view/View;)V

    .line 13
    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O80O()V

    .line 16
    .line 17
    .line 18
    new-instance v0, Lcom/intsig/camscanner/fragment/TeamFragment$12;

    .line 19
    .line 20
    invoke-direct {v0, p0, p1}, Lcom/intsig/camscanner/fragment/TeamFragment$12;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;Lcom/intsig/camscanner/view/PullToSyncView;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->setOnHeaderRefreshListener(Lcom/intsig/camscanner/view/OnHeaderRefreshListener;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic 〇O0o〇〇o(Lcom/intsig/camscanner/fragment/TeamFragment;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO〇〇:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇O80O()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇O8OO:Lcom/intsig/camscanner/view/PullToSyncView;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 4
    .line 5
    if-eqz v1, :cond_0

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->〇00〇8()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    const/4 v1, 0x1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v1, 0x0

    .line 16
    :goto_0
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->setLock(Z)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/camscanner/fragment/TeamFragment;Ljava/util/ArrayList;Ljava/util/ArrayList;II)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇(Ljava/util/ArrayList;Ljava/util/ArrayList;II)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method static bridge synthetic 〇O8〇8000(Lcom/intsig/camscanner/fragment/TeamFragment;)Lcom/intsig/view/ImageTextButton;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oO〇oo:Lcom/intsig/view/ImageTextButton;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇O8〇8O0oO(Lcom/intsig/camscanner/fragment/TeamFragment;)Landroid/widget/ProgressBar;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0O:Landroid/widget/ProgressBar;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇O8〇〇o8〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O8〇8000:Lcom/intsig/camscanner/fragment/TeamFragment$DeviceInterface;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0}, Lcom/intsig/camscanner/fragment/TeamFragment$DeviceInterface;->Oo08()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    return v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇OO0oO(Z)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "User Operation: share"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSBusiness"

    .line 9
    .line 10
    const-string v1, "share"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 16
    .line 17
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 18
    .line 19
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 24
    .line 25
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->〇00(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-direct {p0, v0, p1}, Lcom/intsig/camscanner/fragment/TeamFragment;->O00o(Ljava/util/ArrayList;Z)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic 〇OoO0o0(Lcom/intsig/camscanner/fragment/TeamFragment;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O88O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇OoOO〇()V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o880:Landroid/view/View;

    .line 2
    .line 3
    const v1, 0x7f0a0e63

    .line 4
    .line 5
    .line 6
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    check-cast v0, Landroid/widget/ProgressBar;

    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0O:Landroid/widget/ProgressBar;

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 15
    .line 16
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->oOo(Landroid/content/Context;)I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    sput v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇0O〇Oo〇o:I

    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 23
    .line 24
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇0OO8(Landroid/content/Context;)I

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    iput v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇o88o08〇:I

    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o880:Landroid/view/View;

    .line 31
    .line 32
    const v1, 0x7f0a0542

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    check-cast v0, Landroid/widget/AbsListView;

    .line 40
    .line 41
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇08O:Landroid/widget/AbsListView;

    .line 42
    .line 43
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o880:Landroid/view/View;

    .line 44
    .line 45
    const v1, 0x7f0a0541

    .line 46
    .line 47
    .line 48
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    check-cast v0, Landroid/widget/GridView;

    .line 53
    .line 54
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->ooo0〇〇O:Landroid/widget/GridView;

    .line 55
    .line 56
    invoke-static {}, Lcom/intsig/camscanner/fragment/TeamFragment;->o8〇0o〇()Z

    .line 57
    .line 58
    .line 59
    move-result v1

    .line 60
    const/16 v2, 0x8

    .line 61
    .line 62
    const/4 v3, 0x0

    .line 63
    if-eqz v1, :cond_0

    .line 64
    .line 65
    const/16 v1, 0x8

    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_0
    const/4 v1, 0x0

    .line 69
    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 70
    .line 71
    .line 72
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇08O:Landroid/widget/AbsListView;

    .line 73
    .line 74
    invoke-static {}, Lcom/intsig/camscanner/fragment/TeamFragment;->o8〇0o〇()Z

    .line 75
    .line 76
    .line 77
    move-result v1

    .line 78
    if-eqz v1, :cond_1

    .line 79
    .line 80
    const/4 v2, 0x0

    .line 81
    :cond_1
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 82
    .line 83
    .line 84
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o880:Landroid/view/View;

    .line 85
    .line 86
    const v1, 0x7f0a0d7e

    .line 87
    .line 88
    .line 89
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 90
    .line 91
    .line 92
    move-result-object v0

    .line 93
    check-cast v0, Lcom/intsig/camscanner/view/PullToSyncView;

    .line 94
    .line 95
    new-instance v1, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;

    .line 96
    .line 97
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 98
    .line 99
    invoke-direct {v1, p0, v2, v0}, Lcom/intsig/camscanner/view/header/MainDocHeaderViewStrategy;-><init>(Ljava/lang/Object;Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 100
    .line 101
    .line 102
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->setIHeaderViewStrategy(Lcom/intsig/camscanner/view/header/IHeaderViewStrategy;)V

    .line 103
    .line 104
    .line 105
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O0OO8O(Lcom/intsig/camscanner/view/PullToSyncView;)V

    .line 106
    .line 107
    .line 108
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 109
    .line 110
    .line 111
    move-result-object v0

    .line 112
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    .line 113
    .line 114
    .line 115
    move-result-object v0

    .line 116
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O8080〇O8o(Landroid/content/res/Configuration;)V

    .line 117
    .line 118
    .line 119
    invoke-static {}, Lcom/intsig/camscanner/fragment/TeamFragment;->o8〇0o〇()Z

    .line 120
    .line 121
    .line 122
    move-result v0

    .line 123
    if-eqz v0, :cond_2

    .line 124
    .line 125
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇08O:Landroid/widget/AbsListView;

    .line 126
    .line 127
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇oO〇〇8o:Landroid/widget/AbsListView;

    .line 128
    .line 129
    const/4 v8, 0x0

    .line 130
    goto :goto_1

    .line 131
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->ooo0〇〇O:Landroid/widget/GridView;

    .line 132
    .line 133
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇oO〇〇8o:Landroid/widget/AbsListView;

    .line 134
    .line 135
    const/4 v3, 0x1

    .line 136
    const/4 v8, 0x1

    .line 137
    :goto_1
    new-instance v0, Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 138
    .line 139
    iget-object v5, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 140
    .line 141
    const/4 v6, 0x0

    .line 142
    iget-object v7, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇o〇88〇8:Lcom/intsig/camscanner/adapter/QueryInterface;

    .line 143
    .line 144
    iget-object v9, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇oO〇〇8o:Landroid/widget/AbsListView;

    .line 145
    .line 146
    move-object v4, v0

    .line 147
    invoke-direct/range {v4 .. v9}, Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/intsig/camscanner/adapter/QueryInterface;ILandroid/widget/AbsListView;)V

    .line 148
    .line 149
    .line 150
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 151
    .line 152
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 153
    .line 154
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇o0〇8:Ljava/lang/String;

    .line 155
    .line 156
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;->〇0O〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    .line 158
    .line 159
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 160
    .line 161
    iget-boolean v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇OOoooo:Z

    .line 162
    .line 163
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;->〇〇0o(Z)V

    .line 164
    .line 165
    .line 166
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇oO〇〇8o:Landroid/widget/AbsListView;

    .line 167
    .line 168
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 169
    .line 170
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 171
    .line 172
    .line 173
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇oO〇〇8o:Landroid/widget/AbsListView;

    .line 174
    .line 175
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0O〇O00O:Landroid/widget/AdapterView$OnItemClickListener;

    .line 176
    .line 177
    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 178
    .line 179
    .line 180
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇oO〇〇8o:Landroid/widget/AbsListView;

    .line 181
    .line 182
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇o〇Oo88:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 183
    .line 184
    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 185
    .line 186
    .line 187
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇oO〇〇8o:Landroid/widget/AbsListView;

    .line 188
    .line 189
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇80o:Landroid/view/View$OnTouchListener;

    .line 190
    .line 191
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 192
    .line 193
    .line 194
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇o80Oo()V

    .line 195
    .line 196
    .line 197
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->ooO:Ljava/lang/String;

    .line 198
    .line 199
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 200
    .line 201
    .line 202
    move-result v0

    .line 203
    if-eqz v0, :cond_3

    .line 204
    .line 205
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇o0〇8:Ljava/lang/String;

    .line 206
    .line 207
    sput-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO88o:Ljava/lang/String;

    .line 208
    .line 209
    goto :goto_2

    .line 210
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->ooO:Ljava/lang/String;

    .line 211
    .line 212
    sput-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO88o:Ljava/lang/String;

    .line 213
    .line 214
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 215
    .line 216
    sget-object v2, Lcom/intsig/camscanner/fragment/TeamFragment;->o808o8o08:Ljava/util/ArrayList;

    .line 217
    .line 218
    invoke-static {v1, v0, v2}, Lcom/intsig/camscanner/app/DBUtil;->O0(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 219
    .line 220
    .line 221
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 222
    .line 223
    new-instance v1, Ljava/lang/StringBuilder;

    .line 224
    .line 225
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 226
    .line 227
    .line 228
    const-string v2, "from MainMenuFragment after click searched team folder, syncId:"

    .line 229
    .line 230
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 231
    .line 232
    .line 233
    sget-object v2, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO88o:Ljava/lang/String;

    .line 234
    .line 235
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    .line 237
    .line 238
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 239
    .line 240
    .line 241
    move-result-object v1

    .line 242
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    .line 244
    .line 245
    invoke-virtual {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇88()V

    .line 246
    .line 247
    .line 248
    :goto_2
    return-void
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private 〇OoOo()V
    .locals 7

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "User Operation: do search"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x1

    .line 9
    iput v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOo0:I

    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO8:Landroid/widget/LinearLayout;

    .line 12
    .line 13
    if-nez v1, :cond_0

    .line 14
    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 16
    .line 17
    const v2, 0x7f0a0cc8

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1, v2}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    check-cast v1, Landroid/widget/LinearLayout;

    .line 25
    .line 26
    iput-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO8:Landroid/widget/LinearLayout;

    .line 27
    .line 28
    const v2, 0x7f0a0884

    .line 29
    .line 30
    .line 31
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    check-cast v1, Landroid/widget/ImageView;

    .line 36
    .line 37
    iput-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oo〇O〇80:Landroid/widget/ImageView;

    .line 38
    .line 39
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO8:Landroid/widget/LinearLayout;

    .line 40
    .line 41
    const v2, 0x7f0a0878

    .line 42
    .line 43
    .line 44
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    check-cast v1, Landroid/widget/ImageView;

    .line 49
    .line 50
    iput-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O8〇o〇88:Landroid/widget/ImageView;

    .line 51
    .line 52
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO8:Landroid/widget/LinearLayout;

    .line 53
    .line 54
    const v2, 0x7f0a05b0

    .line 55
    .line 56
    .line 57
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    check-cast v1, Landroid/widget/EditText;

    .line 62
    .line 63
    iput-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇O:Landroid/widget/EditText;

    .line 64
    .line 65
    :cond_0
    sget-object v1, Lcom/intsig/camscanner/fragment/TeamFragment;->o808o8o08:Ljava/util/ArrayList;

    .line 66
    .line 67
    const/4 v2, 0x0

    .line 68
    if-eqz v1, :cond_1

    .line 69
    .line 70
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    .line 71
    .line 72
    .line 73
    move-result v1

    .line 74
    if-lez v1, :cond_1

    .line 75
    .line 76
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 77
    .line 78
    const v3, 0x7f0a0f9f

    .line 79
    .line 80
    .line 81
    invoke-virtual {v1, v3}, Landroidx/appcompat/app/AppCompatActivity;->findViewById(I)Landroid/view/View;

    .line 82
    .line 83
    .line 84
    move-result-object v1

    .line 85
    check-cast v1, Landroid/widget/RelativeLayout;

    .line 86
    .line 87
    iput-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇o88:Landroid/widget/RelativeLayout;

    .line 88
    .line 89
    const v3, 0x7f0a144f

    .line 90
    .line 91
    .line 92
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 93
    .line 94
    .line 95
    move-result-object v1

    .line 96
    check-cast v1, Landroid/widget/TextView;

    .line 97
    .line 98
    iput-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8O:Landroid/widget/TextView;

    .line 99
    .line 100
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇o88:Landroid/widget/RelativeLayout;

    .line 101
    .line 102
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 103
    .line 104
    .line 105
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8O:Landroid/widget/TextView;

    .line 106
    .line 107
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 108
    .line 109
    .line 110
    move-result-object v3

    .line 111
    new-array v4, v0, [Ljava/lang/Object;

    .line 112
    .line 113
    sget-object v5, Lcom/intsig/camscanner/fragment/TeamFragment;->o808o8o08:Ljava/util/ArrayList;

    .line 114
    .line 115
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    .line 116
    .line 117
    .line 118
    move-result v6

    .line 119
    sub-int/2addr v6, v0

    .line 120
    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 121
    .line 122
    .line 123
    move-result-object v0

    .line 124
    check-cast v0, Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 125
    .line 126
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/FolderItem;->〇O00()Ljava/lang/String;

    .line 127
    .line 128
    .line 129
    move-result-object v0

    .line 130
    aput-object v0, v4, v2

    .line 131
    .line 132
    const v0, 0x7f1301b2

    .line 133
    .line 134
    .line 135
    invoke-virtual {v3, v0, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 136
    .line 137
    .line 138
    move-result-object v0

    .line 139
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 140
    .line 141
    .line 142
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇800OO〇0O:Landroidx/appcompat/widget/Toolbar;

    .line 143
    .line 144
    const/16 v1, 0x8

    .line 145
    .line 146
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 147
    .line 148
    .line 149
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO8:Landroid/widget/LinearLayout;

    .line 150
    .line 151
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 152
    .line 153
    .line 154
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O8〇o〇88:Landroid/widget/ImageView;

    .line 155
    .line 156
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 157
    .line 158
    .line 159
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 160
    .line 161
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇O:Landroid/widget/EditText;

    .line 162
    .line 163
    invoke-static {v0, v1}, Lcom/intsig/utils/SoftKeyboardUtils;->O8(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 164
    .line 165
    .line 166
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇O:Landroid/widget/EditText;

    .line 167
    .line 168
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 169
    .line 170
    .line 171
    const/4 v0, 0x0

    .line 172
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇08〇o0O:[Ljava/lang/String;

    .line 173
    .line 174
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇O:Landroid/widget/EditText;

    .line 175
    .line 176
    const-string v1, ""

    .line 177
    .line 178
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    .line 180
    .line 181
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇O:Landroid/widget/EditText;

    .line 182
    .line 183
    new-instance v1, Lcom/intsig/camscanner/fragment/TeamFragment$25;

    .line 184
    .line 185
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$25;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 186
    .line 187
    .line 188
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 189
    .line 190
    .line 191
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oo〇O〇80:Landroid/widget/ImageView;

    .line 192
    .line 193
    new-instance v1, Lcom/intsig/camscanner/fragment/TeamFragment$26;

    .line 194
    .line 195
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$26;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 196
    .line 197
    .line 198
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 199
    .line 200
    .line 201
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O8〇o〇88:Landroid/widget/ImageView;

    .line 202
    .line 203
    new-instance v1, Lcom/intsig/camscanner/fragment/TeamFragment$27;

    .line 204
    .line 205
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$27;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 206
    .line 207
    .line 208
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 209
    .line 210
    .line 211
    return-void
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method static bridge synthetic 〇Oo〇O(Lcom/intsig/camscanner/fragment/TeamFragment;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O〇〇O8:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇O〇o0(Lcom/intsig/camscanner/datastruct/DocumentListItem;)V
    .locals 10

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    sget-object p1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 4
    .line 5
    const-string v0, "openDocument docItem == null"

    .line 6
    .line 7
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 12
    .line 13
    iget-wide v1, p1, Lcom/intsig/webstorage/UploadFile;->OO:J

    .line 14
    .line 15
    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    .line 20
    .line 21
    const-string v2, "android.intent.action.VIEW"

    .line 22
    .line 23
    iget-object v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 24
    .line 25
    const-class v4, Lcom/intsig/camscanner/DocumentActivity;

    .line 26
    .line 27
    invoke-direct {v1, v2, v0, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 28
    .line 29
    .line 30
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o80()Z

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    if-nez v0, :cond_1

    .line 35
    .line 36
    const-string v0, "EXTRA_QUERY_STRING"

    .line 37
    .line 38
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇08〇o0O:[Ljava/lang/String;

    .line 39
    .line 40
    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 41
    .line 42
    .line 43
    :cond_1
    new-instance v0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;

    .line 44
    .line 45
    iget-object v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 46
    .line 47
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO〇0o8〇()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v4

    .line 51
    iget-object v5, p1, Lcom/intsig/camscanner/datastruct/DocumentListItem;->O8o08O8O:Ljava/lang/String;

    .line 52
    .line 53
    iget v6, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇0〇o:I

    .line 54
    .line 55
    iget v7, p1, Lcom/intsig/camscanner/datastruct/DocumentListItem;->o〇00O:I

    .line 56
    .line 57
    iget v8, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇oo:I

    .line 58
    .line 59
    iget-boolean v9, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇OOoooo:Z

    .line 60
    .line 61
    move-object v2, v0

    .line 62
    invoke-direct/range {v2 .. v9}, Lcom/intsig/camscanner/datastruct/TeamDocInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIZ)V

    .line 63
    .line 64
    .line 65
    const-string p1, "team_doc_info"

    .line 66
    .line 67
    invoke-virtual {v1, p1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 68
    .line 69
    .line 70
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 71
    .line 72
    .line 73
    iget-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 74
    .line 75
    const v0, 0x7f010028

    .line 76
    .line 77
    .line 78
    const/4 v1, 0x0

    .line 79
    invoke-virtual {p1, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    .line 81
    .line 82
    goto :goto_0

    .line 83
    :catch_0
    move-exception p1

    .line 84
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 85
    .line 86
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 87
    .line 88
    .line 89
    :goto_0
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private 〇O〇〇〇(J)V
    .locals 11

    .line 1
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 2
    .line 3
    invoke-static {v0, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    :try_start_0
    new-instance p2, Landroid/content/Intent;

    .line 8
    .line 9
    const-string v0, "android.intent.action.VIEW"

    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 12
    .line 13
    const-class v2, Lcom/intsig/camscanner/DocumentActivity;

    .line 14
    .line 15
    invoke-direct {p2, v0, p1, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 16
    .line 17
    .line 18
    iget-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 21
    .line 22
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO〇0o8〇()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0o()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    invoke-static {p1, v0, v1, v2}, Lcom/intsig/camscanner/app/DBUtil;->O〇Oooo〇〇(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    .line 32
    .line 33
    move-result v9

    .line 34
    const/4 v8, 0x2

    .line 35
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0o()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v6

    .line 39
    new-instance p1, Lcom/intsig/camscanner/datastruct/TeamDocInfo;

    .line 40
    .line 41
    iget-object v4, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 42
    .line 43
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO〇0o8〇()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v5

    .line 47
    iget v7, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇0〇o:I

    .line 48
    .line 49
    iget-boolean v10, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇OOoooo:Z

    .line 50
    .line 51
    move-object v3, p1

    .line 52
    invoke-direct/range {v3 .. v10}, Lcom/intsig/camscanner/datastruct/TeamDocInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIZ)V

    .line 53
    .line 54
    .line 55
    const-string v0, "team_doc_info"

    .line 56
    .line 57
    invoke-virtual {p2, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 58
    .line 59
    .line 60
    invoke-virtual {p0, p2}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 61
    .line 62
    .line 63
    iget-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 64
    .line 65
    const p2, 0x7f010028

    .line 66
    .line 67
    .line 68
    const/4 v0, 0x0

    .line 69
    invoke-virtual {p1, p2, v0}, Landroid/app/Activity;->overridePendingTransition(II)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    .line 71
    .line 72
    goto :goto_0

    .line 73
    :catch_0
    move-exception p1

    .line 74
    sget-object p2, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 75
    .line 76
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 77
    .line 78
    .line 79
    :goto_0
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method static bridge synthetic 〇o08(Lcom/intsig/camscanner/fragment/TeamFragment;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇088O:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇o88〇O()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-static {v0}, Lcom/intsig/camscanner/util/Util;->O0(Landroid/content/Context;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 14
    .line 15
    const v1, 0x7f13008d

    .line 16
    .line 17
    .line 18
    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 19
    .line 20
    .line 21
    return-void

    .line 22
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 23
    .line 24
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    if-nez v0, :cond_1

    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 31
    .line 32
    const/4 v1, 0x0

    .line 33
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o〇8oOO88()Z

    .line 34
    .line 35
    .line 36
    move-result v2

    .line 37
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/app/DialogUtils;->oO(Landroid/content/Context;ZZ)V

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 42
    .line 43
    invoke-static {v0}, Lcom/intsig/camscanner/app/AppUtil;->〇o(Landroid/content/Context;)Z

    .line 44
    .line 45
    .line 46
    move-result v0

    .line 47
    if-nez v0, :cond_2

    .line 48
    .line 49
    const/16 v0, 0xef

    .line 50
    .line 51
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇8O〇(I)V

    .line 52
    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 56
    .line 57
    const-string v1, "Sync manually"

    .line 58
    .line 59
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncClient;->O8ooOoo〇()Lcom/intsig/camscanner/tsapp/sync/SyncClient;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    const/4 v1, 0x0

    .line 67
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncClient;->O0o〇〇Oo(Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 71
    .line 72
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇O888o0o(Landroid/content/Context;)V

    .line 73
    .line 74
    .line 75
    :goto_0
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic 〇o8〇8(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O0Oo〇8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇o8〇〇(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/datastruct/DocumentListItem;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    new-instance v10, Lcom/intsig/camscanner/merge/MergeDocumentsTask;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    sget-object v3, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO88o:Ljava/lang/String;

    .line 6
    .line 7
    iget-object v4, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 8
    .line 9
    iget-boolean v6, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇o08:Z

    .line 10
    .line 11
    iget-wide v7, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇oO:J

    .line 12
    .line 13
    new-instance v9, Lcom/intsig/camscanner/fragment/TeamFragment$16;

    .line 14
    .line 15
    invoke-direct {v9, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$16;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 16
    .line 17
    .line 18
    move-object v0, v10

    .line 19
    move-object v2, p1

    .line 20
    move-object v5, p2

    .line 21
    invoke-direct/range {v0 .. v9}, Lcom/intsig/camscanner/merge/MergeDocumentsTask;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZJLcom/intsig/camscanner/merge/MergeDocumentsTask$PostListener;)V

    .line 22
    .line 23
    .line 24
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->oo88o8O()Ljava/util/concurrent/ExecutorService;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    const/4 p2, 0x0

    .line 29
    new-array p2, p2, [Ljava/lang/Integer;

    .line 30
    .line 31
    invoke-virtual {v10, p1, p2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method static bridge synthetic 〇oO88o(Lcom/intsig/camscanner/fragment/TeamFragment;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->ooO:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇oOO80o(Lcom/intsig/camscanner/fragment/TeamFragment;Lcom/intsig/view/ImageTextButton;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oO〇oo:Lcom/intsig/view/ImageTextButton;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇oO〇(I)V
    .locals 4

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOoO8OO〇:Landroidx/fragment/app/DialogFragment;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroidx/fragment/app/DialogFragment;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 4
    .line 5
    .line 6
    goto :goto_0

    .line 7
    :catch_0
    move-exception v0

    .line 8
    sget-object v1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 9
    .line 10
    new-instance v2, Ljava/lang/StringBuilder;

    .line 11
    .line 12
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    .line 14
    .line 15
    const-string v3, "dismissCustomDialog id "

    .line 16
    .line 17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    invoke-static {v1, p1, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 28
    .line 29
    .line 30
    :goto_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic 〇oO〇08o(Lcom/intsig/camscanner/fragment/TeamFragment;)Landroid/widget/RelativeLayout;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇o88:Landroid/widget/RelativeLayout;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇oo8(Ljava/util/ArrayList;I)V
    .locals 7

    .line 1
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 6
    .line 7
    .line 8
    move-result p2

    .line 9
    if-eqz p2, :cond_1

    .line 10
    .line 11
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p2

    .line 15
    check-cast p2, Ljava/lang/Long;

    .line 16
    .line 17
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    .line 18
    .line 19
    .line 20
    move-result-wide v1

    .line 21
    new-instance p2, Ljava/util/ArrayList;

    .line 22
    .line 23
    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    .line 24
    .line 25
    .line 26
    new-instance v6, Ljava/util/ArrayList;

    .line 27
    .line 28
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 29
    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 32
    .line 33
    const-string v3, "page_num ASC"

    .line 34
    .line 35
    move-object v4, p2

    .line 36
    move-object v5, v6

    .line 37
    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/db/dao/ImageDao;->ooo0〇O88O(Landroid/content/Context;JLjava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    .line 41
    .line 42
    .line 43
    move-result v0

    .line 44
    if-lez v0, :cond_0

    .line 45
    .line 46
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    if-lez v0, :cond_0

    .line 51
    .line 52
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    .line 53
    .line 54
    .line 55
    move-result v0

    .line 56
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    .line 57
    .line 58
    .line 59
    move-result v1

    .line 60
    if-ne v0, v1, :cond_0

    .line 61
    .line 62
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 63
    .line 64
    invoke-static {v0, p2, v6}, Lcom/intsig/camscanner/control/ShareControl;->〇O(Landroid/app/Activity;Ljava/util/List;Ljava/util/ArrayList;)V

    .line 65
    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_1
    return-void
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method static bridge synthetic 〇ooO8Ooo〇(Lcom/intsig/camscanner/fragment/TeamFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇o〇:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇ooO〇000(Lcom/intsig/camscanner/fragment/TeamFragment;Lcom/intsig/view/ImageTextButton;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇08O:Lcom/intsig/view/ImageTextButton;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇o〇88(Lcom/intsig/camscanner/fragment/TeamFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO〇(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇o〇88〇8(Lcom/intsig/camscanner/fragment/TeamFragment;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇o〇:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇o〇OO80oO(Lcom/intsig/camscanner/fragment/TeamFragment;Landroid/widget/EditText;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇O80〇0o:Landroid/widget/EditText;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇o〇〇88〇8()Z
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->o808o8o08:Ljava/util/ArrayList;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇0:I

    .line 6
    .line 7
    if-lez v1, :cond_0

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    iget v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇0:I

    .line 14
    .line 15
    const/4 v2, 0x1

    .line 16
    sub-int/2addr v1, v2

    .line 17
    if-le v0, v1, :cond_0

    .line 18
    .line 19
    return v2

    .line 20
    :cond_0
    const/4 v0, 0x0

    .line 21
    return v0
.end method

.method static bridge synthetic 〇〇(Lcom/intsig/camscanner/fragment/TeamFragment;)Lcom/intsig/camscanner/view/PullToSyncView;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇O8OO:Lcom/intsig/camscanner/view/PullToSyncView;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇〇0(Lcom/intsig/camscanner/fragment/TeamFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇00o80oo(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇〇08〇0oo0()[Ljava/lang/String;
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇08〇o0O:[Ljava/lang/String;

    .line 2
    .line 3
    array-length v0, v0

    .line 4
    new-array v1, v0, [Ljava/lang/String;

    .line 5
    .line 6
    const/4 v2, 0x0

    .line 7
    const/4 v3, 0x0

    .line 8
    :goto_0
    if-ge v3, v0, :cond_0

    .line 9
    .line 10
    new-instance v4, Ljava/lang/StringBuilder;

    .line 11
    .line 12
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    .line 14
    .line 15
    const-string v5, "%"

    .line 16
    .line 17
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    iget-object v6, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇08〇o0O:[Ljava/lang/String;

    .line 21
    .line 22
    aget-object v6, v6, v3

    .line 23
    .line 24
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v4

    .line 34
    aput-object v4, v1, v3

    .line 35
    .line 36
    add-int/lit8 v3, v3, 0x1

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    mul-int/lit8 v0, v0, 0x5

    .line 40
    .line 41
    new-array v3, v0, [Ljava/lang/String;

    .line 42
    .line 43
    :goto_1
    if-ge v2, v0, :cond_1

    .line 44
    .line 45
    div-int/lit8 v4, v2, 0x5

    .line 46
    .line 47
    aget-object v4, v1, v4

    .line 48
    .line 49
    aput-object v4, v3, v2

    .line 50
    .line 51
    add-int/lit8 v2, v2, 0x1

    .line 52
    .line 53
    goto :goto_1

    .line 54
    :cond_1
    return-object v3
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇〇0Oo0880()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->O8〇o()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇8OO()V

    .line 10
    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->〇O00()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO〇OO8(I)V

    .line 20
    .line 21
    .line 22
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇O8OO:Lcom/intsig/camscanner/view/PullToSyncView;

    .line 23
    .line 24
    if-eqz v0, :cond_1

    .line 25
    .line 26
    const/4 v1, 0x1

    .line 27
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/view/AbstractPullToSyncView;->setNormalMode(Z)V

    .line 28
    .line 29
    .line 30
    goto :goto_1

    .line 31
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 32
    .line 33
    const-string v1, "mPullToRefreshView = null"

    .line 34
    .line 35
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    :goto_1
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private 〇〇0〇〇8O()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/util/SDStorageManager;->〇〇888(Landroid/app/Activity;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇〇8088()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "User Operation: import from gallery"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSBusiness"

    .line 9
    .line 10
    const-string v1, "import"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o8o〇8()V

    .line 16
    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇80o〇o0()V

    .line 19
    .line 20
    .line 21
    return-void
.end method

.method private 〇〇80o〇o0()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-static {}, Lcom/intsig/util/PermissionUtil;->〇O〇()[Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    new-instance v2, Lcom/intsig/camscanner/fragment/TeamFragment$14;

    .line 8
    .line 9
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$14;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 10
    .line 11
    .line 12
    invoke-static {v0, v1, v2}, Lcom/intsig/util/PermissionUtil;->Oo08(Landroid/content/Context;[Ljava/lang/String;Lcom/intsig/permission/PermissionCallback;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private 〇〇8OO()V
    .locals 5

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->o808o8o08:Ljava/util/ArrayList;

    .line 2
    .line 3
    if-eqz v0, :cond_3

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-lez v0, :cond_3

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->OO〇OOo:Landroid/widget/TextView;

    .line 12
    .line 13
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->START:Landroid/text/TextUtils$TruncateAt;

    .line 14
    .line 15
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇o〇:Ljava/lang/String;

    .line 19
    .line 20
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    const-string v1, ""

    .line 25
    .line 26
    if-nez v0, :cond_4

    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->ooO:Ljava/lang/String;

    .line 29
    .line 30
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    const-string v2, "/"

    .line 35
    .line 36
    if-eqz v0, :cond_0

    .line 37
    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    .line 39
    .line 40
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 41
    .line 42
    .line 43
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o88:Ljava/lang/String;

    .line 44
    .line 45
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    :cond_0
    const/4 v0, 0x0

    .line 56
    :goto_0
    sget-object v3, Lcom/intsig/camscanner/fragment/TeamFragment;->o808o8o08:Ljava/util/ArrayList;

    .line 57
    .line 58
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    .line 59
    .line 60
    .line 61
    move-result v3

    .line 62
    add-int/lit8 v3, v3, -0x1

    .line 63
    .line 64
    if-ge v0, v3, :cond_2

    .line 65
    .line 66
    sget-object v3, Lcom/intsig/camscanner/fragment/TeamFragment;->o808o8o08:Ljava/util/ArrayList;

    .line 67
    .line 68
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 69
    .line 70
    .line 71
    move-result-object v3

    .line 72
    check-cast v3, Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 73
    .line 74
    if-eqz v3, :cond_1

    .line 75
    .line 76
    new-instance v4, Ljava/lang/StringBuilder;

    .line 77
    .line 78
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 79
    .line 80
    .line 81
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    .line 83
    .line 84
    invoke-virtual {v3}, Lcom/intsig/camscanner/datastruct/FolderItem;->〇O00()Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v1

    .line 88
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 95
    .line 96
    .line 97
    move-result-object v1

    .line 98
    :cond_1
    add-int/lit8 v0, v0, 0x1

    .line 99
    .line 100
    goto :goto_0

    .line 101
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    .line 102
    .line 103
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 104
    .line 105
    .line 106
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    .line 108
    .line 109
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇o〇:Ljava/lang/String;

    .line 110
    .line 111
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object v1

    .line 118
    goto :goto_1

    .line 119
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->OO〇OOo:Landroid/widget/TextView;

    .line 120
    .line 121
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MIDDLE:Landroid/text/TextUtils$TruncateAt;

    .line 122
    .line 123
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 124
    .line 125
    .line 126
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o88:Ljava/lang/String;

    .line 127
    .line 128
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->OO〇OOo:Landroid/widget/TextView;

    .line 129
    .line 130
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    .line 132
    .line 133
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 134
    .line 135
    if-eqz v0, :cond_5

    .line 136
    .line 137
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;->〇00O0O0(Ljava/lang/String;)V

    .line 138
    .line 139
    .line 140
    :cond_5
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 141
    .line 142
    new-instance v2, Ljava/lang/StringBuilder;

    .line 143
    .line 144
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 145
    .line 146
    .line 147
    const-string v3, "refresh normal title name :"

    .line 148
    .line 149
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    .line 151
    .line 152
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    .line 154
    .line 155
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 156
    .line 157
    .line 158
    move-result-object v1

    .line 159
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    .line 161
    .line 162
    return-void
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method static bridge synthetic 〇〇8o0OOOo(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇00O00o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇〇O()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->〇00〇8()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOOO0()V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇〇O80〇0o(Lcom/intsig/camscanner/fragment/TeamFragment;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o0OoOOo0:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇〇o0〇8(Lcom/intsig/camscanner/fragment/TeamFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇〇o80Oo()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇oO〇〇8o:Landroid/widget/AbsListView;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/fragment/TeamFragment$10;

    .line 4
    .line 5
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$10;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method static bridge synthetic 〇〇〇0(Lcom/intsig/camscanner/fragment/TeamFragment;)Landroid/os/Handler;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO0880O:Landroid/os/Handler;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇〇〇00(Lcom/intsig/camscanner/fragment/TeamFragment;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O88O:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇〇〇OOO〇〇(Lcom/intsig/camscanner/fragment/TeamFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇o88〇O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇〇〇O〇(Lcom/intsig/camscanner/fragment/TeamFragment;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇00O0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public O888o8()Z
    .locals 6

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/camscanner/app/DBUtil;->oO8o(Landroid/content/Context;)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 14
    .line 15
    invoke-static {v1, v2}, Lcom/intsig/camscanner/app/DBUtil;->ooo8o〇o〇(Landroid/content/Context;Ljava/lang/String;)[I

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    sget-object v2, Lcom/intsig/camscanner/fragment/TeamFragment;->o808o8o08:Ljava/util/ArrayList;

    .line 20
    .line 21
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    sget-object v3, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 26
    .line 27
    new-instance v4, Ljava/lang/StringBuilder;

    .line 28
    .line 29
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 30
    .line 31
    .line 32
    const-string v5, "localMaxTeamNumber: "

    .line 33
    .line 34
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    const-string v5, " localLayer: "

    .line 41
    .line 42
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    const-string v5, "  limitInfo:"

    .line 49
    .line 50
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-static {v1}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v5

    .line 57
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v4

    .line 64
    invoke-static {v3, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    const/4 v3, 0x0

    .line 68
    aget v4, v1, v3

    .line 69
    .line 70
    const/4 v5, 0x1

    .line 71
    if-ge v0, v4, :cond_0

    .line 72
    .line 73
    add-int/2addr v2, v5

    .line 74
    aget v0, v1, v5

    .line 75
    .line 76
    if-lt v2, v0, :cond_1

    .line 77
    .line 78
    :cond_0
    const/4 v3, 0x1

    .line 79
    :cond_1
    return v3
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public O8oO0()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "User Operation: copy doc"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇88()Ljava/util/ArrayList;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    if-lez v1, :cond_0

    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 21
    .line 22
    new-instance v2, L〇8〇o〇8/O0oo0o0〇;

    .line 23
    .line 24
    invoke-direct {v2, p0, v0}, L〇8〇o〇8/O0oo0o0〇;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;Ljava/util/ArrayList;)V

    .line 25
    .line 26
    .line 27
    invoke-static {v1, v0, v2}, Lcom/intsig/camscanner/control/DataChecker;->〇O8o08O(Landroid/app/Activity;Ljava/util/ArrayList;Lcom/intsig/camscanner/control/DataChecker$ActionListener;)Z

    .line 28
    .line 29
    .line 30
    :cond_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public O〇88()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;->ooOO(Landroid/database/Cursor;)V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Landroid/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public O〇O〇88O8O()[I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇oO〇〇8o:Landroid/widget/AbsListView;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-lez v0, :cond_0

    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o0o〇〇〇8o()[I

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    :goto_0
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public o8080o8〇〇(Landroid/view/View;)V
    .locals 5

    .line 1
    const-string v0, "CSBusiness"

    .line 2
    .line 3
    const-string v1, "help"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇:Lcom/intsig/menu/PopupListMenu;

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    new-instance v0, Lcom/intsig/menu/PopupListMenu;

    .line 14
    .line 15
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 16
    .line 17
    const/4 v3, 0x0

    .line 18
    invoke-direct {v0, v2, v1, v3}, Lcom/intsig/menu/PopupListMenu;-><init>(Landroid/content/Context;ZZ)V

    .line 19
    .line 20
    .line 21
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇:Lcom/intsig/menu/PopupListMenu;

    .line 22
    .line 23
    const/4 v2, 0x7

    .line 24
    invoke-virtual {v0, v2}, Lcom/intsig/menu/PopupListMenu;->OoO8(I)V

    .line 25
    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇:Lcom/intsig/menu/PopupListMenu;

    .line 28
    .line 29
    new-instance v2, L〇8〇o〇8/〇8o;

    .line 30
    .line 31
    invoke-direct {v2, p0}, L〇8〇o〇8/〇8o;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, v2}, Lcom/intsig/menu/PopupListMenu;->o800o8O(Lcom/intsig/menu/PopupListMenu$MenuItemClickListener;)V

    .line 35
    .line 36
    .line 37
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O08〇:Lcom/intsig/menu/PopupMenuItems;

    .line 38
    .line 39
    if-nez v0, :cond_1

    .line 40
    .line 41
    new-instance v0, Lcom/intsig/menu/PopupMenuItems;

    .line 42
    .line 43
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 44
    .line 45
    invoke-direct {v0, v2}, Lcom/intsig/menu/PopupMenuItems;-><init>(Landroid/content/Context;)V

    .line 46
    .line 47
    .line 48
    iput-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O08〇:Lcom/intsig/menu/PopupMenuItems;

    .line 49
    .line 50
    invoke-virtual {v0, v1}, Lcom/intsig/menu/PopupMenuItems;->Oo08(Z)V

    .line 51
    .line 52
    .line 53
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O08〇:Lcom/intsig/menu/PopupMenuItems;

    .line 54
    .line 55
    new-instance v1, Lcom/intsig/menu/MenuItem;

    .line 56
    .line 57
    iget v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o0〇〇00:I

    .line 58
    .line 59
    const v3, 0x7f13080c

    .line 60
    .line 61
    .line 62
    invoke-virtual {p0, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v3

    .line 66
    const v4, 0x7f080a06

    .line 67
    .line 68
    .line 69
    invoke-direct {v1, v2, v3, v4}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 70
    .line 71
    .line 72
    invoke-virtual {v0, v1}, Lcom/intsig/menu/PopupMenuItems;->〇o00〇〇Oo(Lcom/intsig/menu/MenuItem;)V

    .line 73
    .line 74
    .line 75
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O08〇:Lcom/intsig/menu/PopupMenuItems;

    .line 76
    .line 77
    new-instance v1, Lcom/intsig/menu/MenuItem;

    .line 78
    .line 79
    iget v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO〇08o:I

    .line 80
    .line 81
    const v3, 0x7f1307fc

    .line 82
    .line 83
    .line 84
    invoke-virtual {p0, v3}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v3

    .line 88
    const v4, 0x7f080a0d

    .line 89
    .line 90
    .line 91
    invoke-direct {v1, v2, v3, v4}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 92
    .line 93
    .line 94
    invoke-virtual {v0, v1}, Lcom/intsig/menu/PopupMenuItems;->〇o00〇〇Oo(Lcom/intsig/menu/MenuItem;)V

    .line 95
    .line 96
    .line 97
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇:Lcom/intsig/menu/PopupListMenu;

    .line 98
    .line 99
    invoke-virtual {v0}, Lcom/intsig/menu/PopupListMenu;->〇〇808〇()Z

    .line 100
    .line 101
    .line 102
    move-result v0

    .line 103
    if-nez v0, :cond_2

    .line 104
    .line 105
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 106
    .line 107
    if-eqz v0, :cond_2

    .line 108
    .line 109
    invoke-virtual {v0}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->O8〇o()Z

    .line 110
    .line 111
    .line 112
    move-result v0

    .line 113
    if-eqz v0, :cond_2

    .line 114
    .line 115
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇:Lcom/intsig/menu/PopupListMenu;

    .line 116
    .line 117
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O08〇:Lcom/intsig/menu/PopupMenuItems;

    .line 118
    .line 119
    invoke-virtual {v0, v1}, Lcom/intsig/menu/PopupListMenu;->〇oo〇(Lcom/intsig/menu/PopupMenuItems;)V

    .line 120
    .line 121
    .line 122
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇:Lcom/intsig/menu/PopupListMenu;

    .line 123
    .line 124
    invoke-virtual {v0}, Lcom/intsig/menu/PopupListMenu;->〇O00()V

    .line 125
    .line 126
    .line 127
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇:Lcom/intsig/menu/PopupListMenu;

    .line 128
    .line 129
    invoke-virtual {v0, p1}, Lcom/intsig/menu/PopupListMenu;->〇00(Landroid/view/View;)V

    .line 130
    .line 131
    .line 132
    :cond_2
    return-void
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/camscanner/fragment/BaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    sget-object p1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 5
    .line 6
    const-string v0, "onActivityCreated"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O08O0〇O()J

    .line 12
    .line 13
    .line 14
    move-result-wide v0

    .line 15
    iput-wide v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇oO:J

    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O80〇〇o()V

    .line 18
    .line 19
    .line 20
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->Oo8〇〇ooo()V

    .line 21
    .line 22
    .line 23
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o8o8〇o()V

    .line 24
    .line 25
    .line 26
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇OoOO〇()V

    .line 27
    .line 28
    .line 29
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0OOoO8O0()V

    .line 30
    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O8〇o0〇〇()V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 18

    move-object/from16 v1, p0

    move/from16 v0, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    .line 1
    sget-object v4, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onActivityResult requestCode:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, ",resultCode:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, ",data"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v5, 0x66

    const/4 v6, 0x1

    const/4 v7, -0x1

    if-ne v0, v5, :cond_2

    if-ne v2, v7, :cond_16

    .line 2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityResult() mTmpPhotoFile "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->oo8ooo8O:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 3
    iget-object v0, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->oo8ooo8O:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    const v3, 0x7f130085

    if-nez v0, :cond_1

    .line 4
    new-instance v0, Ljava/io/File;

    iget-object v5, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->oo8ooo8O:Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 5
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 6
    new-instance v4, Ljava/io/File;

    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->〇〇808〇()Ljava/lang/String;

    move-result-object v5

    const-string v7, ".jpg"

    invoke-static {v5, v7}, Lcom/intsig/camscanner/util/SDStorageManager;->〇O8o08O(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 7
    :try_start_0
    invoke-static {v0, v4}, Lcom/intsig/utils/FileUtil;->Oo08(Ljava/io/File;Ljava/io/File;)V

    .line 8
    iget-wide v7, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇oO:J

    invoke-static {v4}, Lcom/intsig/utils/FileUtil;->〇〇8O0〇8(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    const/4 v4, 0x2

    invoke-direct {v1, v7, v8, v4, v0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O0o0〇8o(JILandroid/net/Uri;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    :catch_0
    move-exception v0

    .line 9
    iget-object v4, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v4, v3}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 10
    sget-object v3, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    :cond_0
    const-string v0, "tempFile is not exists"

    .line 11
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 12
    :cond_1
    iget-object v0, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v0, v3}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    goto/16 :goto_2

    :cond_2
    const/16 v5, 0x6a

    const-string v8, "current folder has been delete"

    if-ne v0, v5, :cond_5

    if-ne v2, v7, :cond_16

    .line 13
    iget-object v0, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO〇0o8〇()Ljava/lang/String;

    move-result-object v5

    iget-object v7, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    invoke-static {v0, v5, v7}, Lcom/intsig/camscanner/app/DBUtil;->O0o〇〇Oo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇o〇:Ljava/lang/String;

    .line 14
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 15
    invoke-static {v4, v8}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_3
    if-eqz v3, :cond_16

    .line 16
    invoke-virtual/range {p3 .. p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 17
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "pick image form gallery  Uri:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, " Path:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    iget-wide v3, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇oO:J

    invoke-direct {v1, v3, v4, v6, v0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O0o0〇8o(JILandroid/net/Uri;)V

    goto/16 :goto_2

    :cond_4
    const-string v0, "pick multi image form gallery"

    .line 19
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    invoke-static/range {p3 .. p3}, Lcom/intsig/camscanner/app/IntentUtil;->〇O8o08O(Landroid/content/Intent;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇0888o(Ljava/util/ArrayList;)V

    goto/16 :goto_2

    :cond_5
    const/16 v5, 0x6b

    const-string v9, "team_doc_info"

    if-ne v0, v5, :cond_8

    if-ne v2, v7, :cond_8

    .line 21
    iget-object v0, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    sget-object v5, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO88o:Ljava/lang/String;

    iget-object v7, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    invoke-static {v0, v5, v7}, Lcom/intsig/camscanner/app/DBUtil;->O0o〇〇Oo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇o〇:Ljava/lang/String;

    .line 22
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 23
    invoke-static {v4, v8}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_6
    if-eqz v3, :cond_7

    :try_start_1
    const-string v0, "batch handle images finish, go to view doc"

    .line 24
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "view_doc_uri"

    .line 25
    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 26
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    iget-object v5, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    const-class v7, Lcom/intsig/camscanner/DocumentActivity;

    invoke-direct {v3, v4, v0, v5, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 27
    new-instance v0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;

    iget-object v11, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO〇0o8〇()Ljava/lang/String;

    move-result-object v12

    .line 28
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0o()Ljava/lang/String;

    move-result-object v13

    iget v14, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇0〇o:I

    const/4 v15, 0x2

    iget v4, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇oo:I

    iget-boolean v5, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇OOoooo:Z

    move-object v10, v0

    move/from16 v16, v4

    move/from16 v17, v5

    invoke-direct/range {v10 .. v17}, Lcom/intsig/camscanner/datastruct/TeamDocInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIZ)V

    .line 29
    invoke-virtual {v3, v9, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 30
    invoke-virtual {v1, v3}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_2

    :cond_7
    const-string v0, "data == null"

    .line 31
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_2

    :catch_1
    move-exception v0

    .line 32
    sget-object v3, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    :cond_8
    const/16 v5, 0x6f

    if-ne v0, v5, :cond_9

    if-ne v2, v7, :cond_16

    .line 33
    iget-object v0, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    sget-object v3, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO88o:Ljava/lang/String;

    iget-object v4, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    invoke-static {v0, v3, v4}, Lcom/intsig/camscanner/app/DBUtil;->O0o〇〇Oo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇o〇:Ljava/lang/String;

    goto/16 :goto_2

    :cond_9
    const/16 v5, 0x6c

    if-eq v0, v5, :cond_a

    const/16 v5, 0x84

    if-ne v0, v5, :cond_d

    :cond_a
    if-ne v2, v7, :cond_d

    .line 34
    iget-object v0, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO〇0o8〇()Ljava/lang/String;

    move-result-object v5

    iget-object v7, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    invoke-static {v0, v5, v7}, Lcom/intsig/camscanner/app/DBUtil;->O0o〇〇Oo(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇o〇:Ljava/lang/String;

    .line 35
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 36
    invoke-static {v4, v8}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_b
    if-eqz v3, :cond_c

    .line 37
    :try_start_2
    new-instance v0, Lcom/intsig/camscanner/datastruct/TeamDocInfo;

    iget-object v11, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO〇0o8〇()Ljava/lang/String;

    move-result-object v12

    .line 38
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0o()Ljava/lang/String;

    move-result-object v13

    iget v14, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇0〇o:I

    const/4 v15, 0x2

    iget v4, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇oo:I

    iget-boolean v5, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇OOoooo:Z

    move-object v10, v0

    move/from16 v16, v4

    move/from16 v17, v5

    invoke-direct/range {v10 .. v17}, Lcom/intsig/camscanner/datastruct/TeamDocInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIIZ)V

    .line 39
    invoke-virtual {v3, v9, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 40
    :cond_c
    invoke-virtual {v1, v3}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_2

    :catch_2
    move-exception v0

    .line 41
    sget-object v3, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    :cond_d
    const/16 v5, 0x80

    if-eq v0, v5, :cond_12

    const/16 v5, 0x81

    if-ne v0, v5, :cond_e

    goto :goto_0

    :cond_e
    const/16 v4, 0x82

    if-ne v0, v4, :cond_f

    .line 42
    iget-object v0, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->〇OO8ooO8〇:Lcom/intsig/camscanner/control/ShareControl$OcrLanguageSetting;

    if-eqz v0, :cond_16

    .line 43
    invoke-virtual {v0}, Lcom/intsig/camscanner/control/ShareControl$OcrLanguageSetting;->〇080()V

    goto/16 :goto_2

    :cond_f
    const/16 v4, 0x83

    if-ne v0, v4, :cond_11

    .line 44
    iget-object v0, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇O80〇0o:Landroid/widget/EditText;

    if-eqz v0, :cond_10

    .line 45
    iget-object v2, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    invoke-static {v2, v0}, Lcom/intsig/utils/SoftKeyboardUtils;->O8(Landroid/content/Context;Landroid/widget/EditText;)V

    :cond_10
    return-void

    :cond_11
    const/16 v4, 0x85

    if-ne v4, v0, :cond_16

    if-ne v7, v2, :cond_16

    const-string v0, "extra_list_data"

    .line 46
    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇〇00:Ljava/util/ArrayList;

    const/16 v0, 0xe9

    .line 47
    invoke-direct {v1, v0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O〇8O〇(I)V

    return-void

    .line 48
    :cond_12
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "sParentSyncId:"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v5, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO88o:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v3, :cond_13

    const-string v0, "extra_target_teamfolder"

    const/4 v4, 0x0

    .line 49
    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_13

    const/4 v0, 0x0

    .line 50
    sput-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO88o:Ljava/lang/String;

    .line 51
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->o808o8o08:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 52
    iget-object v0, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0, v7, v3}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 53
    iget-object v0, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_2

    :cond_13
    if-eqz v3, :cond_15

    const-string v0, "team_info"

    .line 54
    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/intsig/camscanner/datastruct/TeamInfo;

    if-eqz v0, :cond_14

    .line 55
    iget-object v4, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    iget-object v0, v0, Lcom/intsig/camscanner/datastruct/TeamInfo;->o0:Ljava/lang/String;

    invoke-static {v4, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_14

    goto :goto_1

    .line 56
    :cond_14
    invoke-direct {v1, v3}, Lcom/intsig/camscanner/fragment/TeamFragment;->oo8O8o80(Landroid/content/Intent;)V

    .line 57
    :cond_15
    :goto_1
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇O()V

    .line 58
    iget-object v0, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    invoke-virtual {v0, v6}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->〇〇〇0〇〇0(Z)V

    .line 59
    iget-object v0, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    invoke-virtual {v0, v6}, Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;->OOO8o〇〇(Z)V

    :cond_16
    :goto_2
    if-ne v2, v6, :cond_17

    .line 60
    iget-object v0, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0, v6}, Landroid/app/Activity;->setResult(I)V

    .line 61
    iget-object v0, v1, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_17
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 2
    .line 3
    .line 4
    check-cast p1, Landroidx/appcompat/app/AppCompatActivity;

    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 7
    .line 8
    sget-object p1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 9
    .line 10
    const-string v0, "onAttach"

    .line 11
    .line 12
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    iget-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 16
    .line 17
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/TeamFragment;->oo8O8o80(Landroid/content/Intent;)V

    .line 22
    .line 23
    .line 24
    sget-boolean p1, Lcom/intsig/camscanner/app/AppConfig;->〇080:Z

    .line 25
    .line 26
    sput-boolean p1, Lcom/intsig/camscanner/fragment/TeamFragment;->o0Oo:Z

    .line 27
    .line 28
    new-instance p1, Lcom/intsig/camscanner/fragment/TeamFragment$NoDocViewControl;

    .line 29
    .line 30
    const/4 v0, 0x0

    .line 31
    invoke-direct {p1, p0, v0}, Lcom/intsig/camscanner/fragment/TeamFragment$NoDocViewControl;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;L〇8〇o〇8/OO0〇〇8;)V

    .line 32
    .line 33
    .line 34
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/fragment/TeamFragment$NoDocViewControl;

    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/fragment/TeamFragment;->O8080〇O8o(Landroid/content/res/Configuration;)V

    .line 5
    .line 6
    .line 7
    iget-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇8〇oO〇〇8o:Landroid/widget/AbsListView;

    .line 8
    .line 9
    if-eqz p1, :cond_0

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/fragment/TeamFragment$5;

    .line 12
    .line 13
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/fragment/TeamFragment$5;-><init>(Lcom/intsig/camscanner/fragment/TeamFragment;)V

    .line 14
    .line 15
    .line 16
    const-wide/16 v1, 0x64

    .line 17
    .line 18
    invoke-virtual {p1, v0, v1, v2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 19
    .line 20
    .line 21
    :cond_0
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/camscanner/fragment/BaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 5
    .line 6
    const-string v1, "onCreate"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇80oo〇0〇o(Landroid/os/Bundle;)V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o008()V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .line 1
    sget-object p2, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {p2}, Lcom/intsig/camscanner/CustomExceptionHandler;->O8(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 p2, 0x0

    .line 7
    const/4 p3, 0x0

    .line 8
    const v0, 0x7f0d05b6

    .line 9
    .line 10
    .line 11
    invoke-virtual {p1, v0, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    iput-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o880:Landroid/view/View;

    .line 16
    .line 17
    return-object p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public onDestroy()V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "onDestroy"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/camscanner/loadimage/BitmapLoaderUtil;->〇080()V

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOO0880O:Landroid/os/Handler;

    .line 12
    .line 13
    iget-object v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oO00〇o:[I

    .line 14
    .line 15
    const/4 v3, 0x0

    .line 16
    invoke-static {v0, v1, v2, v3}, Lcom/intsig/comm/recycle/HandlerMsglerRecycle;->〇o〇(Ljava/lang/String;Landroid/os/Handler;[I[Ljava/lang/Runnable;)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O8〇8O0oO:Lcom/intsig/camscanner/tsapp/sync/TeamDownloadStateMonitor;

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/camscanner/tsapp/sync/TeamDownloadStateMonitor;->〇o〇()V

    .line 22
    .line 23
    .line 24
    invoke-super {p0}, Lcom/intsig/camscanner/fragment/BaseFragment;->onDestroy()V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "onKeyDown = "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    iget v2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOo0:I

    .line 14
    .line 15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    const/4 v1, 0x4

    .line 26
    const/4 v2, 0x1

    .line 27
    if-ne p1, v1, :cond_3

    .line 28
    .line 29
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    .line 30
    .line 31
    .line 32
    move-result p2

    .line 33
    if-nez p2, :cond_3

    .line 34
    .line 35
    new-instance p1, Ljava/lang/StringBuilder;

    .line 36
    .line 37
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 38
    .line 39
    .line 40
    const-string p2, "User Operation: KEY_BACK, search mode="

    .line 41
    .line 42
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    iget p2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOo0:I

    .line 46
    .line 47
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object p1

    .line 54
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    iget-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o8oOOo:Lcom/intsig/camscanner/adapter/TeamFolderAndDocAdapter;

    .line 58
    .line 59
    if-eqz p1, :cond_0

    .line 60
    .line 61
    invoke-virtual {p1}, Lcom/intsig/camscanner/adapter/TeamDocCursorAdapter;->〇00〇8()Z

    .line 62
    .line 63
    .line 64
    move-result p1

    .line 65
    if-eqz p1, :cond_0

    .line 66
    .line 67
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->oOOO0()V

    .line 68
    .line 69
    .line 70
    return v2

    .line 71
    :cond_0
    iget p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oOo0:I

    .line 72
    .line 73
    if-ne p1, v2, :cond_1

    .line 74
    .line 75
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O8o()V

    .line 76
    .line 77
    .line 78
    return v2

    .line 79
    :cond_1
    sget-object p1, Lcom/intsig/camscanner/fragment/TeamFragment;->o808o8o08:Ljava/util/ArrayList;

    .line 80
    .line 81
    if-eqz p1, :cond_2

    .line 82
    .line 83
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 84
    .line 85
    .line 86
    move-result p1

    .line 87
    if-lez p1, :cond_2

    .line 88
    .line 89
    sget-object p1, Lcom/intsig/camscanner/fragment/TeamFragment;->〇oO88o:Ljava/lang/String;

    .line 90
    .line 91
    iget-object p2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->ooO:Ljava/lang/String;

    .line 92
    .line 93
    invoke-static {p1, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 94
    .line 95
    .line 96
    move-result p1

    .line 97
    if-nez p1, :cond_2

    .line 98
    .line 99
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇0o8O()V

    .line 100
    .line 101
    .line 102
    return v2

    .line 103
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o08〇808()V

    .line 104
    .line 105
    .line 106
    return v2

    .line 107
    :cond_3
    const/16 p2, 0x52

    .line 108
    .line 109
    if-ne p1, p2, :cond_4

    .line 110
    .line 111
    iget-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O8〇8000:Lcom/intsig/camscanner/fragment/TeamFragment$DeviceInterface;

    .line 112
    .line 113
    instance-of p1, p1, Lcom/intsig/camscanner/fragment/TeamFragment$PhoneDevice;

    .line 114
    .line 115
    if-eqz p1, :cond_4

    .line 116
    .line 117
    const-string p1, "Menu Key is pressed"

    .line 118
    .line 119
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    .line 121
    .line 122
    iget-object p1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O8〇8000:Lcom/intsig/camscanner/fragment/TeamFragment$DeviceInterface;

    .line 123
    .line 124
    check-cast p1, Lcom/intsig/camscanner/fragment/TeamFragment$PhoneDevice;

    .line 125
    .line 126
    iget-object p2, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oO〇oo:Lcom/intsig/view/ImageTextButton;

    .line 127
    .line 128
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/fragment/TeamFragment$PhoneDevice;->〇00(Landroid/view/View;)V

    .line 129
    .line 130
    .line 131
    return v2

    .line 132
    :cond_4
    const/4 p1, 0x0

    .line 133
    return p1
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public onPause()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "onPause()"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-super {p0}, Lcom/intsig/camscanner/fragment/BaseFragment;->onPause()V

    .line 9
    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncClient;->O8ooOoo〇()Lcom/intsig/camscanner/tsapp/sync/SyncClient;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇〇0:Lcom/intsig/camscanner/tsapp/SyncListener;

    .line 16
    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncClient;->O08000(Lcom/intsig/camscanner/tsapp/SyncListener;)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
.end method

.method public onResume()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/camscanner/fragment/BaseFragment;->onResume()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 5
    .line 6
    const-string v1, "onResume"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇00O()V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->Oo088O〇8〇()V

    .line 15
    .line 16
    .line 17
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncClient;->O8ooOoo〇()Lcom/intsig/camscanner/tsapp/sync/SyncClient;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇〇0:Lcom/intsig/camscanner/tsapp/SyncListener;

    .line 22
    .line 23
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tsapp/sync/SyncClient;->oO00OOO(Lcom/intsig/camscanner/tsapp/SyncListener;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇0oO()Lcom/intsig/camscanner/tsapp/sync/SyncThread;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    if-eqz v0, :cond_0

    .line 31
    .line 32
    invoke-virtual {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->OO8oO0o〇()Z

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    if-nez v0, :cond_0

    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0O:Landroid/widget/ProgressBar;

    .line 39
    .line 40
    const/16 v1, 0x8

    .line 41
    .line 42
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 43
    .line 44
    .line 45
    :cond_0
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "onSaveInstanceState()"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "tag_id"

    .line 9
    .line 10
    iget-wide v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇oO:J

    .line 11
    .line 12
    invoke-virtual {p1, v0, v1, v2}, Landroid/os/BaseBundle;->putLong(Ljava/lang/String;J)V

    .line 13
    .line 14
    .line 15
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
.end method

.method public onStart()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "onStart "

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-super {p0}, Lcom/intsig/camscanner/fragment/BaseFragment;->onStart()V

    .line 9
    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->OO0o88()Z

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇O8〇8000:Lcom/intsig/camscanner/fragment/TeamFragment$DeviceInterface;

    .line 15
    .line 16
    invoke-interface {v0}, Lcom/intsig/camscanner/fragment/TeamFragment$DeviceInterface;->oO80()V

    .line 17
    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->o8o〇8()V

    .line 20
    .line 21
    .line 22
    const-string v0, "CSBusiness"

    .line 23
    .line 24
    invoke-static {v0}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public onStop()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/camscanner/fragment/BaseFragment;->onStop()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 5
    .line 6
    const-string v1, "onStop()"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-boolean v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->OO〇00〇8oO:Z

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/camscanner/fragment/TeamFragment;->〇〇O()V

    .line 16
    .line 17
    .line 18
    const/4 v0, 0x0

    .line 19
    iput-boolean v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->OO〇00〇8oO:Z

    .line 20
    .line 21
    :cond_0
    return-void
.end method

.method public 〇0oO()Lcom/intsig/camscanner/tsapp/sync/SyncThread;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return-object v0

    .line 7
    :cond_0
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncThread;->oo〇(Landroid/content/Context;)Lcom/intsig/camscanner/tsapp/sync/SyncThread;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected 〇80oo〇0〇o(Landroid/os/Bundle;)V
    .locals 3

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 5
    .line 6
    const-string v1, "onRestoreInstanceState()"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "tag_id"

    .line 12
    .line 13
    const-wide/16 v1, -0x2

    .line 14
    .line 15
    invoke-virtual {p1, v0, v1, v2}, Landroid/os/BaseBundle;->getLong(Ljava/lang/String;J)J

    .line 16
    .line 17
    .line 18
    move-result-wide v0

    .line 19
    iput-wide v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->o〇oO:J

    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇8o80O()V
    .locals 5

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/camscanner/db/dao/TeamInfoDao;->〇080(Landroid/content/Context;Ljava/lang/String;)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x2

    .line 10
    if-ne v1, v0, :cond_0

    .line 11
    .line 12
    const/16 v0, 0x19

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/16 v0, 0x18

    .line 16
    .line 17
    :goto_0
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->O0〇OO8()Lcom/intsig/tianshu/UserInfo;

    .line 18
    .line 19
    .line 20
    move-result-object v2

    .line 21
    invoke-virtual {v2, v0}, Lcom/intsig/tianshu/UserInfo;->getAPI(I)Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    const-string v2, "%1s/%2s/mconsole/member?"

    .line 26
    .line 27
    new-array v1, v1, [Ljava/lang/Object;

    .line 28
    .line 29
    const/4 v3, 0x0

    .line 30
    aput-object v0, v1, v3

    .line 31
    .line 32
    iget-object v3, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->〇Oo〇O:Ljava/lang/String;

    .line 33
    .line 34
    const/4 v4, 0x1

    .line 35
    aput-object v3, v1, v4

    .line 36
    .line 37
    invoke-static {v2, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    new-instance v2, Ljava/lang/StringBuilder;

    .line 42
    .line 43
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 44
    .line 45
    .line 46
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 50
    .line 51
    invoke-static {v1, v0}, Lcom/intsig/utils/WebUrlUtils;->o〇0(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    iget-object v1, p0, Lcom/intsig/camscanner/fragment/TeamFragment;->oooO888:Landroidx/appcompat/app/AppCompatActivity;

    .line 63
    .line 64
    invoke-virtual {v1}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 65
    .line 66
    .line 67
    move-result-object v2

    .line 68
    const v3, 0x7f1307f4

    .line 69
    .line 70
    .line 71
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v2

    .line 75
    invoke-static {v1, v2, v0}, Lcom/intsig/webview/util/WebUtil;->OO0o〇〇(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    sget-object v1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 79
    .line 80
    new-instance v2, Ljava/lang/StringBuilder;

    .line 81
    .line 82
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 83
    .line 84
    .line 85
    const-string v3, "doInvite: "

    .line 86
    .line 87
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    .line 89
    .line 90
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    .line 99
    .line 100
    goto :goto_1

    .line 101
    :catch_0
    move-exception v0

    .line 102
    sget-object v1, Lcom/intsig/camscanner/fragment/TeamFragment;->O0〇:Ljava/lang/String;

    .line 103
    .line 104
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 105
    .line 106
    .line 107
    :goto_1
    return-void
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

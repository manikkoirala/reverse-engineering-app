.class public final Lcom/intsig/camscanner/edu/EduGroupDialog$Companion;
.super Ljava/lang/Object;
.source "EduGroupDialog.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/edu/EduGroupDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/edu/EduGroupDialog$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final 〇080()I
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/edu/EduGroupHelper;->〇080:Lcom/intsig/camscanner/edu/EduGroupHelper;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/edu/EduGroupHelper;->o〇0()Z

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    const/4 v2, 0x1

    .line 8
    const/4 v3, -0x1

    .line 9
    if-eqz v1, :cond_3

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/edu/EduGroupHelper;->O8()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    if-eq v0, v2, :cond_7

    .line 18
    .line 19
    :cond_0
    :goto_0
    const/4 v2, -0x1

    .line 20
    goto :goto_1

    .line 21
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 22
    .line 23
    .line 24
    move-result v0

    .line 25
    if-eqz v0, :cond_2

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_2
    const/4 v2, 0x2

    .line 29
    goto :goto_1

    .line 30
    :cond_3
    invoke-virtual {v0}, Lcom/intsig/camscanner/edu/EduGroupHelper;->O8()I

    .line 31
    .line 32
    .line 33
    move-result v0

    .line 34
    if-ne v0, v2, :cond_4

    .line 35
    .line 36
    const/4 v2, 0x6

    .line 37
    goto :goto_1

    .line 38
    :cond_4
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    if-eqz v0, :cond_5

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_5
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o08o〇0()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    sget-object v1, Lcom/intsig/camscanner/occupation_label/model/OccupationLabel;->STUDENT_PRIMARY:Lcom/intsig/camscanner/occupation_label/model/OccupationLabel;

    .line 50
    .line 51
    invoke-virtual {v1}, Lcom/intsig/camscanner/occupation_label/model/OccupationLabel;->getTagCode()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 56
    .line 57
    .line 58
    move-result v1

    .line 59
    if-nez v1, :cond_6

    .line 60
    .line 61
    sget-object v1, Lcom/intsig/camscanner/occupation_label/model/OccupationLabel;->TEACHER:Lcom/intsig/camscanner/occupation_label/model/OccupationLabel;

    .line 62
    .line 63
    invoke-virtual {v1}, Lcom/intsig/camscanner/occupation_label/model/OccupationLabel;->getTagCode()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object v1

    .line 67
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 68
    .line 69
    .line 70
    move-result v1

    .line 71
    if-nez v1, :cond_6

    .line 72
    .line 73
    sget-object v1, Lcom/intsig/camscanner/occupation_label/model/OccupationLabel;->STUDENT_COLLEGE:Lcom/intsig/camscanner/occupation_label/model/OccupationLabel;

    .line 74
    .line 75
    invoke-virtual {v1}, Lcom/intsig/camscanner/occupation_label/model/OccupationLabel;->getTagCode()Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v1

    .line 79
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 80
    .line 81
    .line 82
    move-result v0

    .line 83
    if-eqz v0, :cond_0

    .line 84
    .line 85
    :cond_6
    const/4 v2, 0x5

    .line 86
    :cond_7
    :goto_1
    return v2
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public final 〇o00〇〇Oo()Lcom/intsig/camscanner/edu/EduGroupDialog;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/edu/EduGroupDialog;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/edu/EduGroupDialog;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

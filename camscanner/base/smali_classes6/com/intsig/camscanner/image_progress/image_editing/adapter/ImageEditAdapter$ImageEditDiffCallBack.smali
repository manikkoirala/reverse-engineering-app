.class public final Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$ImageEditDiffCallBack;
.super Landroidx/recyclerview/widget/DiffUtil$ItemCallback;
.source "ImageEditAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "ImageEditDiffCallBack"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/recyclerview/widget/DiffUtil$ItemCallback<",
        "Lcom/intsig/camscanner/pagelist/model/PageTypeItem;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field final synthetic 〇080:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$ImageEditDiffCallBack;->〇080:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 2
    .line 3
    invoke-direct {p0}, Landroidx/recyclerview/widget/DiffUtil$ItemCallback;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public bridge synthetic areContentsTheSame(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 0

    .line 1
    check-cast p1, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;

    .line 2
    .line 3
    check-cast p2, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;

    .line 4
    .line 5
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$ImageEditDiffCallBack;->〇080(Lcom/intsig/camscanner/pagelist/model/PageTypeItem;Lcom/intsig/camscanner/pagelist/model/PageTypeItem;)Z

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    return p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public bridge synthetic areItemsTheSame(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 0

    .line 1
    check-cast p1, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;

    .line 2
    .line 3
    check-cast p2, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;

    .line 4
    .line 5
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$ImageEditDiffCallBack;->〇o00〇〇Oo(Lcom/intsig/camscanner/pagelist/model/PageTypeItem;Lcom/intsig/camscanner/pagelist/model/PageTypeItem;)Z

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    return p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇080(Lcom/intsig/camscanner/pagelist/model/PageTypeItem;Lcom/intsig/camscanner/pagelist/model/PageTypeItem;)Z
    .locals 1
    .param p1    # Lcom/intsig/camscanner/pagelist/model/PageTypeItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/pagelist/model/PageTypeItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "oldItem"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "newItem"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    instance-of v0, p1, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListImageItem;

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    instance-of v0, p2, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListImageItem;

    .line 16
    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    check-cast p1, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListImageItem;

    .line 20
    .line 21
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListImageItem;->getPageImage()Lcom/intsig/camscanner/loadimage/PageImage;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    check-cast p2, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListImageItem;

    .line 26
    .line 27
    invoke-virtual {p2}, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListImageItem;->getPageImage()Lcom/intsig/camscanner/loadimage/PageImage;

    .line 28
    .line 29
    .line 30
    move-result-object p2

    .line 31
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 32
    .line 33
    .line 34
    move-result p1

    .line 35
    goto :goto_0

    .line 36
    :cond_0
    instance-of p1, p1, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListBottomCaptureItem;

    .line 37
    .line 38
    if-eqz p1, :cond_1

    .line 39
    .line 40
    instance-of p1, p2, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListBottomCaptureItem;

    .line 41
    .line 42
    if-eqz p1, :cond_1

    .line 43
    .line 44
    const/4 p1, 0x1

    .line 45
    return p1

    .line 46
    :cond_1
    const/4 p1, 0x0

    .line 47
    :goto_0
    return p1
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public 〇o00〇〇Oo(Lcom/intsig/camscanner/pagelist/model/PageTypeItem;Lcom/intsig/camscanner/pagelist/model/PageTypeItem;)Z
    .locals 5
    .param p1    # Lcom/intsig/camscanner/pagelist/model/PageTypeItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/pagelist/model/PageTypeItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "oldItem"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "newItem"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    instance-of v0, p1, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListImageItem;

    .line 12
    .line 13
    const/4 v1, 0x1

    .line 14
    const/4 v2, 0x0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    instance-of v0, p2, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListImageItem;

    .line 18
    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    check-cast p1, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListImageItem;

    .line 22
    .line 23
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListImageItem;->getPageImage()Lcom/intsig/camscanner/loadimage/PageImage;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/PageImage;->o800o8O()J

    .line 28
    .line 29
    .line 30
    move-result-wide v3

    .line 31
    check-cast p2, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListImageItem;

    .line 32
    .line 33
    invoke-virtual {p2}, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListImageItem;->getPageImage()Lcom/intsig/camscanner/loadimage/PageImage;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/PageImage;->o800o8O()J

    .line 38
    .line 39
    .line 40
    move-result-wide p1

    .line 41
    cmp-long v0, v3, p1

    .line 42
    .line 43
    if-nez v0, :cond_1

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_0
    instance-of p1, p1, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListBottomCaptureItem;

    .line 47
    .line 48
    if-eqz p1, :cond_1

    .line 49
    .line 50
    instance-of p1, p2, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListBottomCaptureItem;

    .line 51
    .line 52
    if-eqz p1, :cond_1

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_1
    const/4 v1, 0x0

    .line 56
    :goto_0
    return v1
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

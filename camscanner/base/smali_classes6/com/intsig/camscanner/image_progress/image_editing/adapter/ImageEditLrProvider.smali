.class public final Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;
.super Lcom/chad/library/adapter/base/provider/BaseItemProvider;
.source "ImageEditLrProvider.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;,
        Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/provider/BaseItemProvider<",
        "Lcom/intsig/camscanner/pagelist/model/PageTypeItem;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final OO〇00〇8oO:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8o08O8O:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oOo0:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$mDrawableRequestListener$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOo〇8o008:Z

.field private final o〇00O:Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇080OO8〇0:I

.field private final 〇0O:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->OO〇00〇8oO:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter;Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "mPresenter"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "mAdapter"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->o〇00O:Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->O8o08O8O:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 17
    .line 18
    sget-object p1, Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;->IMAGE:Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;

    .line 19
    .line 20
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;->getType()I

    .line 21
    .line 22
    .line 23
    move-result p1

    .line 24
    iput p1, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->〇080OO8〇0:I

    .line 25
    .line 26
    const p1, 0x7f0d042a

    .line 27
    .line 28
    .line 29
    iput p1, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->〇0O:I

    .line 30
    .line 31
    new-instance p1, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$mDrawableRequestListener$1;

    .line 32
    .line 33
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$mDrawableRequestListener$1;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;)V

    .line 34
    .line 35
    .line 36
    iput-object p1, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->oOo0:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$mDrawableRequestListener$1;

    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private static final O8〇o(Lkotlin/jvm/functions/Function0;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final OOO〇O0(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;Lcom/intsig/camscanner/loadimage/PageImage;J)V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$loadJsonWithAnim$r$1;

    .line 2
    .line 3
    invoke-direct {v0, p1, p2, p0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$loadJsonWithAnim$r$1;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;Lcom/intsig/camscanner/loadimage/PageImage;Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;)V

    .line 4
    .line 5
    .line 6
    const-wide/16 p1, 0x0

    .line 7
    .line 8
    cmp-long v1, p3, p1

    .line 9
    .line 10
    if-lez v1, :cond_0

    .line 11
    .line 12
    sget-object p1, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->OO〇OOo:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$Companion;

    .line 13
    .line 14
    invoke-virtual {p1}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$Companion;->〇080()Landroid/os/Handler;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    new-instance p2, L〇08O8o8/Oo08;

    .line 19
    .line 20
    invoke-direct {p2, v0}, L〇08O8o8/Oo08;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {p1, p2, p3, p4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 24
    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    invoke-interface {v0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    :goto_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static synthetic Oo8Oo00oo(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;Lcom/intsig/camscanner/loadimage/PageImage;Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData$OnImageCompleteListener;ILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p3, p3, 0x2

    .line 2
    .line 3
    if-eqz p3, :cond_0

    .line 4
    .line 5
    const/4 p2, 0x0

    .line 6
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->o8(Lcom/intsig/camscanner/loadimage/PageImage;Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData$OnImageCompleteListener;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private final o0ooO(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;ILcom/intsig/camscanner/loadimage/PageImage;)V
    .locals 6

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "onBindWord, position = "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "ImageEditLrProvider"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {p1}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemLrWordBinding;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->o〇00O:Landroid/widget/ImageView;

    .line 28
    .line 29
    const/16 v1, 0x8

    .line 30
    .line 31
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 32
    .line 33
    .line 34
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->O8o08O8O:Landroid/widget/LinearLayout;

    .line 35
    .line 36
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 37
    .line 38
    .line 39
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇OOo8〇0:Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;

    .line 40
    .line 41
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 42
    .line 43
    .line 44
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇0O:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 45
    .line 46
    const/4 v1, 0x0

    .line 47
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 48
    .line 49
    .line 50
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇0O:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 51
    .line 52
    iget-object v2, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->O8o08O8O:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 53
    .line 54
    invoke-virtual {v2}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->O00O()Lkotlin/jvm/functions/Function1;

    .line 55
    .line 56
    .line 57
    move-result-object v2

    .line 58
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/pic2word/lr/LrView;->o〇0(Lkotlin/jvm/functions/Function1;)V

    .line 59
    .line 60
    .line 61
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇0O:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 62
    .line 63
    iget-object v2, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->O8o08O8O:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 64
    .line 65
    invoke-virtual {v2}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->〇o〇Oo0()Lkotlin/jvm/functions/Function1;

    .line 66
    .line 67
    .line 68
    move-result-object v2

    .line 69
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/pic2word/lr/LrView;->Oo08(Lkotlin/jvm/functions/Function1;)V

    .line 70
    .line 71
    .line 72
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇0O:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 73
    .line 74
    iget-object v2, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->O8o08O8O:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 75
    .line 76
    invoke-virtual {v2}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->〇8o()Lkotlin/jvm/functions/Function1;

    .line 77
    .line 78
    .line 79
    move-result-object v2

    .line 80
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/pic2word/lr/LrView;->setOnTableCellClickListener(Lkotlin/jvm/functions/Function1;)V

    .line 81
    .line 82
    .line 83
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇0O:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 84
    .line 85
    iget-object v2, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->O8o08O8O:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 86
    .line 87
    invoke-virtual {v2}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->ooo〇〇O〇()Lkotlin/jvm/functions/Function1;

    .line 88
    .line 89
    .line 90
    move-result-object v2

    .line 91
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/pic2word/lr/LrView;->setOnChildFocusChangeListener(Lkotlin/jvm/functions/Function1;)V

    .line 92
    .line 93
    .line 94
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇0O:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 95
    .line 96
    iget-object v2, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->O8o08O8O:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 97
    .line 98
    invoke-virtual {v2}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->OO〇0008O8()Lkotlin/jvm/functions/Function0;

    .line 99
    .line 100
    .line 101
    move-result-object v2

    .line 102
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/pic2word/lr/LrView;->setOnKeyBoardChangeListener(Lkotlin/jvm/functions/Function0;)V

    .line 103
    .line 104
    .line 105
    invoke-virtual {p3}, Lcom/intsig/camscanner/loadimage/PageImage;->Oooo8o0〇()Lcom/intsig/camscanner/pic2word/lr/LrImageJson;

    .line 106
    .line 107
    .line 108
    move-result-object v0

    .line 109
    const/4 v2, 0x0

    .line 110
    if-eqz v0, :cond_0

    .line 111
    .line 112
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrImageJson;->getPages()Ljava/util/List;

    .line 113
    .line 114
    .line 115
    move-result-object v0

    .line 116
    goto :goto_0

    .line 117
    :cond_0
    move-object v0, v2

    .line 118
    :goto_0
    check-cast v0, Ljava/util/Collection;

    .line 119
    .line 120
    const/4 v3, 0x1

    .line 121
    if-eqz v0, :cond_2

    .line 122
    .line 123
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    .line 124
    .line 125
    .line 126
    move-result v0

    .line 127
    if-eqz v0, :cond_1

    .line 128
    .line 129
    goto :goto_1

    .line 130
    :cond_1
    const/4 v0, 0x0

    .line 131
    goto :goto_2

    .line 132
    :cond_2
    :goto_1
    const/4 v0, 0x1

    .line 133
    :goto_2
    if-nez v0, :cond_3

    .line 134
    .line 135
    invoke-virtual {p3}, Lcom/intsig/camscanner/loadimage/PageImage;->Oooo8o0〇()Lcom/intsig/camscanner/pic2word/lr/LrImageJson;

    .line 136
    .line 137
    .line 138
    move-result-object v0

    .line 139
    if-eqz v0, :cond_3

    .line 140
    .line 141
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrImageJson;->getPages()Ljava/util/List;

    .line 142
    .line 143
    .line 144
    move-result-object v0

    .line 145
    if-eqz v0, :cond_3

    .line 146
    .line 147
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 148
    .line 149
    .line 150
    move-result-object v0

    .line 151
    check-cast v0, Lcom/intsig/camscanner/pic2word/lr/LrPageBean;

    .line 152
    .line 153
    move-object v2, v0

    .line 154
    :cond_3
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇0O:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 155
    .line 156
    iget-object v4, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->O8o08O8O:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 157
    .line 158
    invoke-virtual {v4}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->〇o〇8()Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;

    .line 159
    .line 160
    .line 161
    move-result-object v4

    .line 162
    invoke-virtual {v0, v4}, Lcom/intsig/camscanner/pic2word/lr/LrView;->setMImageJsonParam(Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;)V

    .line 163
    .line 164
    .line 165
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇0O:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 166
    .line 167
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/pic2word/lr/LrView;->setLimitDrawDecoration(Z)V

    .line 168
    .line 169
    .line 170
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇0O:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 171
    .line 172
    iget-object v4, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->o〇00O:Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter;

    .line 173
    .line 174
    const-string v5, "null cannot be cast to non-null type com.intsig.camscanner.image_progress.image_editing.presenter.ImageEditPresenter"

    .line 175
    .line 176
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 177
    .line 178
    .line 179
    check-cast v4, Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;

    .line 180
    .line 181
    invoke-virtual {v4}, Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;->o〇O8〇〇o()Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew;

    .line 182
    .line 183
    .line 184
    move-result-object v4

    .line 185
    invoke-virtual {v0, v4}, Lcom/intsig/camscanner/pic2word/lr/LrView;->setMLrUndoManagerNew(Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew;)V

    .line 186
    .line 187
    .line 188
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇0O:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 189
    .line 190
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/pic2word/lr/LrView;->setMIsImageEdit(Z)V

    .line 191
    .line 192
    .line 193
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇0O:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 194
    .line 195
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/pic2word/lr/LrView;->setPageIndex(I)V

    .line 196
    .line 197
    .line 198
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇0O:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 199
    .line 200
    invoke-virtual {p3}, Lcom/intsig/camscanner/loadimage/PageImage;->〇O〇()Ljava/lang/String;

    .line 201
    .line 202
    .line 203
    move-result-object v4

    .line 204
    invoke-virtual {v0, v4}, Lcom/intsig/camscanner/pic2word/lr/LrView;->setPageSyncId(Ljava/lang/String;)V

    .line 205
    .line 206
    .line 207
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇0O:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 208
    .line 209
    invoke-virtual {p3}, Lcom/intsig/camscanner/loadimage/PageImage;->O8ooOoo〇()Ljava/lang/String;

    .line 210
    .line 211
    .line 212
    move-result-object p3

    .line 213
    invoke-virtual {v0, p3}, Lcom/intsig/camscanner/pic2word/lr/LrView;->setPagePath(Ljava/lang/String;)V

    .line 214
    .line 215
    .line 216
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇0O:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 217
    .line 218
    invoke-virtual {p3, v2}, Lcom/intsig/camscanner/pic2word/lr/LrView;->setPageData(Lcom/intsig/camscanner/pic2word/lr/LrPageBean;)V

    .line 219
    .line 220
    .line 221
    iget-object p3, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->O8o08O8O:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 222
    .line 223
    invoke-virtual {p3}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->O〇0〇o808〇()Z

    .line 224
    .line 225
    .line 226
    move-result p3

    .line 227
    if-eqz p3, :cond_4

    .line 228
    .line 229
    if-nez p2, :cond_4

    .line 230
    .line 231
    iget-object p2, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->O8o08O8O:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 232
    .line 233
    invoke-virtual {p2, v1}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->O〇Oo(Z)V

    .line 234
    .line 235
    .line 236
    invoke-static {}, Lcom/intsig/camscanner/pagelist/WordContentController;->〇o00〇〇Oo()Z

    .line 237
    .line 238
    .line 239
    move-result p2

    .line 240
    if-nez p2, :cond_4

    .line 241
    .line 242
    iget-object p2, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->O8o08O8O:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 243
    .line 244
    invoke-virtual {p2}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->O0oo0o0〇()Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 245
    .line 246
    .line 247
    move-result-object p2

    .line 248
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇0O:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 249
    .line 250
    invoke-virtual {p3, v1}, Lcom/intsig/camscanner/pic2word/lr/LrView;->〇〇〇0〇〇0(Z)Z

    .line 251
    .line 252
    .line 253
    move-result p3

    .line 254
    invoke-virtual {p2, p3}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->o〇O(Z)V

    .line 255
    .line 256
    .line 257
    :cond_4
    sget-object p2, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingHelper;->〇080:Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingHelper;

    .line 258
    .line 259
    invoke-virtual {p2}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingHelper;->〇O00()Z

    .line 260
    .line 261
    .line 262
    move-result p3

    .line 263
    if-eqz p3, :cond_5

    .line 264
    .line 265
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->oOo〇8o008:Landroid/widget/RelativeLayout;

    .line 266
    .line 267
    const-string v0, "rlGuide"

    .line 268
    .line 269
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 270
    .line 271
    .line 272
    invoke-static {p3, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 273
    .line 274
    .line 275
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇080OO8〇0:Lcom/airbnb/lottie/LottieAnimationView;

    .line 276
    .line 277
    const v0, 0x7f120033

    .line 278
    .line 279
    .line 280
    invoke-virtual {p3, v0}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(I)V

    .line 281
    .line 282
    .line 283
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇080OO8〇0:Lcom/airbnb/lottie/LottieAnimationView;

    .line 284
    .line 285
    const/4 v0, 0x2

    .line 286
    invoke-virtual {p3, v0}, Lcom/airbnb/lottie/LottieAnimationView;->setRepeatCount(I)V

    .line 287
    .line 288
    .line 289
    iget-object p3, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇080OO8〇0:Lcom/airbnb/lottie/LottieAnimationView;

    .line 290
    .line 291
    new-instance v0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$onBindWord$1$1;

    .line 292
    .line 293
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$onBindWord$1$1;-><init>(Lcom/intsig/camscanner/databinding/ItemLrWordBinding;)V

    .line 294
    .line 295
    .line 296
    invoke-virtual {p3, v0}, Lcom/airbnb/lottie/LottieAnimationView;->O8(Landroid/animation/Animator$AnimatorListener;)V

    .line 297
    .line 298
    .line 299
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇080OO8〇0:Lcom/airbnb/lottie/LottieAnimationView;

    .line 300
    .line 301
    invoke-virtual {p1}, Lcom/airbnb/lottie/LottieAnimationView;->〇O〇()V

    .line 302
    .line 303
    .line 304
    invoke-virtual {p2, v1}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingHelper;->OOO〇O0(Z)V

    .line 305
    .line 306
    .line 307
    :cond_5
    return-void
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private final o8(Lcom/intsig/camscanner/loadimage/PageImage;Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData$OnImageCompleteListener;)V
    .locals 11

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/PageImage;->O8ooOoo〇()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/PageImage;->〇0〇O0088o()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-eqz v0, :cond_0

    .line 28
    .line 29
    new-instance v0, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;

    .line 30
    .line 31
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/PageImage;->o800o8O()J

    .line 32
    .line 33
    .line 34
    move-result-wide v2

    .line 35
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 36
    .line 37
    .line 38
    move-result-wide v4

    .line 39
    const/4 v6, 0x0

    .line 40
    const/4 v7, 0x0

    .line 41
    const/4 v8, 0x0

    .line 42
    const/16 v9, 0x1c

    .line 43
    .line 44
    const/4 v10, 0x0

    .line 45
    move-object v1, v0

    .line 46
    invoke-direct/range {v1 .. v10}, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;-><init>(JJZILcom/intsig/camscanner/tsapp/request/RequestTaskData$RequestTaskDataListener;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData;->〇〇888(Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData$OnImageCompleteListener;)V

    .line 50
    .line 51
    .line 52
    iget-object p2, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->O8o08O8O:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 53
    .line 54
    invoke-virtual {p2}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->OOoo()Lcom/intsig/camscanner/tsapp/request/RequestTask;

    .line 55
    .line 56
    .line 57
    move-result-object v1

    .line 58
    const/4 v3, 0x0

    .line 59
    const/4 v4, 0x0

    .line 60
    const/4 v5, 0x4

    .line 61
    const/4 v6, 0x0

    .line 62
    move-object v2, v0

    .line 63
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/tsapp/request/RequestTask;->o800o8O(Lcom/intsig/camscanner/tsapp/request/RequestTask;Lcom/intsig/camscanner/tsapp/request/RequestTaskData;ZZILjava/lang/Object;)V

    .line 64
    .line 65
    .line 66
    iget-object p2, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->O8o08O8O:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 67
    .line 68
    invoke-virtual {p2}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->OOoo()Lcom/intsig/camscanner/tsapp/request/RequestTask;

    .line 69
    .line 70
    .line 71
    move-result-object p2

    .line 72
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 73
    .line 74
    .line 75
    move-result-wide v0

    .line 76
    invoke-virtual {p2, v0, v1}, Lcom/intsig/camscanner/tsapp/request/RequestTask;->〇oOO8O8(J)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/PageImage;->o800o8O()J

    .line 80
    .line 81
    .line 82
    move-result-wide p1

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    .line 84
    .line 85
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 86
    .line 87
    .line 88
    const-string v1, "pushOneRequest pageId:"

    .line 89
    .line 90
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object p1

    .line 100
    const-string p2, "ImageEditLrProvider"

    .line 101
    .line 102
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    sget-object p1, Lcom/intsig/camscanner/tsapp/imagedownload/ImageDownloadClient;->〇8o8o〇:Lcom/intsig/camscanner/tsapp/imagedownload/ImageDownloadClient$Companion;

    .line 106
    .line 107
    invoke-virtual {p1}, Lcom/intsig/camscanner/tsapp/imagedownload/ImageDownloadClient$Companion;->〇080()Lcom/intsig/camscanner/tsapp/imagedownload/ImageDownloadClient;

    .line 108
    .line 109
    .line 110
    move-result-object p1

    .line 111
    iget-object p2, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->O8o08O8O:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 112
    .line 113
    invoke-virtual {p2}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->OOoo()Lcom/intsig/camscanner/tsapp/request/RequestTask;

    .line 114
    .line 115
    .line 116
    move-result-object p2

    .line 117
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/tsapp/request/RequestTaskClient;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/tsapp/request/RequestTask;)V

    .line 118
    .line 119
    .line 120
    :cond_0
    return-void
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic o800o8O(Lkotlin/jvm/functions/Function0;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->O8〇o(Lkotlin/jvm/functions/Function0;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic oo88o8O(Landroid/view/ViewGroup;Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->o〇8(Landroid/view/ViewGroup;Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method static synthetic oo〇(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;Lcom/intsig/camscanner/loadimage/PageImage;JILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p5, p5, 0x4

    .line 2
    .line 3
    if-eqz p5, :cond_0

    .line 4
    .line 5
    const-wide/16 p3, 0x0

    .line 6
    .line 7
    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->OOO〇O0(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;Lcom/intsig/camscanner/loadimage/PageImage;J)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
.end method

.method private static final o〇8(Landroid/view/ViewGroup;Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string p3, "$parent"

    .line 2
    .line 3
    invoke-static {p0, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p3, "$holder"

    .line 7
    .line 8
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string p3, "this$0"

    .line 12
    .line 13
    invoke-static {p2, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 17
    .line 18
    .line 19
    move-result-object p3

    .line 20
    invoke-static {p3}, Lcom/intsig/tsapp/account/util/AccountUtils;->〇8(Landroid/content/Context;)Z

    .line 21
    .line 22
    .line 23
    move-result p3

    .line 24
    if-nez p3, :cond_0

    .line 25
    .line 26
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 27
    .line 28
    .line 29
    move-result-object p0

    .line 30
    const p1, 0x7f13055d

    .line 31
    .line 32
    .line 33
    invoke-static {p0, p1}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 34
    .line 35
    .line 36
    return-void

    .line 37
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;->O〇8O8〇008()Lcom/intsig/camscanner/loadimage/PageImage;

    .line 38
    .line 39
    .line 40
    move-result-object p0

    .line 41
    if-nez p0, :cond_1

    .line 42
    .line 43
    return-void

    .line 44
    :cond_1
    new-instance p3, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$onCreateViewHolder$1$1;

    .line 45
    .line 46
    invoke-direct {p3, p2, p1, p0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$onCreateViewHolder$1$1;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;Lcom/intsig/camscanner/loadimage/PageImage;)V

    .line 47
    .line 48
    .line 49
    invoke-direct {p2, p0, p3}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->〇0000OOO(Lcom/intsig/camscanner/loadimage/PageImage;Lkotlin/jvm/functions/Function0;)V

    .line 50
    .line 51
    .line 52
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic o〇O8〇〇o(Lkotlin/jvm/functions/Function0;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->o〇〇0〇(Lkotlin/jvm/functions/Function0;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final o〇〇0〇(Lkotlin/jvm/functions/Function0;)V
    .locals 1

    .line 1
    const-string v0, "$loader"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇00(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;Lcom/intsig/camscanner/loadimage/PageImage;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->o〇00O:Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter;->〇080()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->〇oOO8O8(Lcom/intsig/camscanner/loadimage/PageImage;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    const-string v0, "ImageEditLrProvider"

    .line 16
    .line 17
    const-string v1, "checkAutoScanLastImage"

    .line 18
    .line 19
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->o〇00O:Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter;

    .line 23
    .line 24
    const/4 v1, 0x0

    .line 25
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter;->〇o00〇〇Oo(Z)V

    .line 26
    .line 27
    .line 28
    new-instance v0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$checkAutoScanLastImage$1;

    .line 29
    .line 30
    invoke-direct {v0, p0, p1, p2}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$checkAutoScanLastImage$1;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;Lcom/intsig/camscanner/loadimage/PageImage;)V

    .line 31
    .line 32
    .line 33
    invoke-direct {p0, p2, v0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->〇0000OOO(Lcom/intsig/camscanner/loadimage/PageImage;Lkotlin/jvm/functions/Function0;)V

    .line 34
    .line 35
    .line 36
    :cond_0
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final 〇0000OOO(Lcom/intsig/camscanner/loadimage/PageImage;Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/loadimage/PageImage;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/PageImage;->O8ooOoo〇()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    sget-object p1, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->OO〇OOo:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$Companion;

    .line 12
    .line 13
    invoke-virtual {p1}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$Companion;->〇080()Landroid/os/Handler;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    new-instance v0, L〇08O8o8/O8;

    .line 18
    .line 19
    invoke-direct {v0, p2}, L〇08O8o8/O8;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$loadJsonAfterCheckFile$2;

    .line 27
    .line 28
    invoke-direct {v0, p2}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$loadJsonAfterCheckFile$2;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 29
    .line 30
    .line 31
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->o8(Lcom/intsig/camscanner/loadimage/PageImage;Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData$OnImageCompleteListener;)V

    .line 32
    .line 33
    .line 34
    :goto_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final 〇00〇8(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;ILcom/intsig/camscanner/loadimage/PageImage;)V
    .locals 9

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "onBindImage, position = "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "ImageEditLrProvider"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {p1}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemLrWordBinding;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->o〇00O:Landroid/widget/ImageView;

    .line 28
    .line 29
    const/4 v3, 0x0

    .line 30
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 31
    .line 32
    .line 33
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇0O:Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 34
    .line 35
    const/16 v4, 0x8

    .line 36
    .line 37
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 38
    .line 39
    .line 40
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇08O〇00〇o:Landroid/widget/ImageView;

    .line 41
    .line 42
    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 43
    .line 44
    .line 45
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇OOo8〇0:Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;

    .line 46
    .line 47
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 48
    .line 49
    .line 50
    iget-object v2, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->o〇00O:Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter;

    .line 51
    .line 52
    invoke-interface {v2}, Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter;->〇o〇()Z

    .line 53
    .line 54
    .line 55
    move-result v2

    .line 56
    if-nez v2, :cond_0

    .line 57
    .line 58
    const-string p1, "not show scan anim."

    .line 59
    .line 60
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    iget-object p1, v0, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->O8o08O8O:Landroid/widget/LinearLayout;

    .line 64
    .line 65
    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 66
    .line 67
    .line 68
    if-nez p2, :cond_4

    .line 69
    .line 70
    iget-object p1, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->oOo0:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$mDrawableRequestListener$1;

    .line 71
    .line 72
    goto/16 :goto_1

    .line 73
    .line 74
    :cond_0
    invoke-virtual {p3}, Lcom/intsig/camscanner/loadimage/PageImage;->OO0o〇〇()I

    .line 75
    .line 76
    .line 77
    move-result p2

    .line 78
    const/4 v2, 0x1

    .line 79
    if-ne p2, v2, :cond_2

    .line 80
    .line 81
    iget-object p2, v0, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->O8o08O8O:Landroid/widget/LinearLayout;

    .line 82
    .line 83
    invoke-virtual {p2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 84
    .line 85
    .line 86
    const-string p2, "is loading."

    .line 87
    .line 88
    invoke-static {v1, p2}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    invoke-virtual {p3}, Lcom/intsig/camscanner/loadimage/PageImage;->o800o8O()J

    .line 92
    .line 93
    .line 94
    move-result-wide v1

    .line 95
    iget-object p2, v0, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->o〇00O:Landroid/widget/ImageView;

    .line 96
    .line 97
    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    .line 98
    .line 99
    .line 100
    move-result p2

    .line 101
    const/16 v3, 0xa

    .line 102
    .line 103
    if-le p2, v3, :cond_1

    .line 104
    .line 105
    sget-object p2, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->OO〇OOo:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$Companion;

    .line 106
    .line 107
    invoke-virtual {p2}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$Companion;->〇080()Landroid/os/Handler;

    .line 108
    .line 109
    .line 110
    move-result-object p2

    .line 111
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 112
    .line 113
    .line 114
    move-result-object v1

    .line 115
    invoke-virtual {p2, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 116
    .line 117
    .line 118
    iget-object v2, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->o〇00O:Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter;

    .line 119
    .line 120
    iget-object v4, v0, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇OOo8〇0:Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;

    .line 121
    .line 122
    const-string p2, "galaxy"

    .line 123
    .line 124
    invoke-static {v4, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 125
    .line 126
    .line 127
    invoke-virtual {p1}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemLrWordBinding;

    .line 128
    .line 129
    .line 130
    move-result-object p1

    .line 131
    iget-object v5, p1, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->o〇00O:Landroid/widget/ImageView;

    .line 132
    .line 133
    const-string p1, "holder.binding.ivImage"

    .line 134
    .line 135
    invoke-static {v5, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 136
    .line 137
    .line 138
    const/4 v6, 0x0

    .line 139
    const/16 v7, 0x8

    .line 140
    .line 141
    const/4 v8, 0x0

    .line 142
    move-object v3, p3

    .line 143
    invoke-static/range {v2 .. v8}, Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter$DefaultImpls;->〇080(Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter;Lcom/intsig/camscanner/loadimage/PageImage;Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;Landroid/view/View;ZILjava/lang/Object;)V

    .line 144
    .line 145
    .line 146
    goto :goto_0

    .line 147
    :cond_1
    sget-object p2, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->OO〇OOo:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$Companion;

    .line 148
    .line 149
    invoke-virtual {p2}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$Companion;->〇080()Landroid/os/Handler;

    .line 150
    .line 151
    .line 152
    move-result-object p2

    .line 153
    new-instance v3, L〇08O8o8/〇〇888;

    .line 154
    .line 155
    invoke-direct {v3, p0, p3, v0, p1}, L〇08O8o8/〇〇888;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;Lcom/intsig/camscanner/loadimage/PageImage;Lcom/intsig/camscanner/databinding/ItemLrWordBinding;Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;)V

    .line 156
    .line 157
    .line 158
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 159
    .line 160
    .line 161
    move-result-object p1

    .line 162
    const-wide/16 v1, 0x64

    .line 163
    .line 164
    invoke-static {p2, v3, p1, v1, v2}, Landroidx/core/os/HandlerCompat;->postDelayed(Landroid/os/Handler;Ljava/lang/Runnable;Ljava/lang/Object;J)Z

    .line 165
    .line 166
    .line 167
    goto :goto_0

    .line 168
    :cond_2
    iget-object p2, v0, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->O8o08O8O:Landroid/widget/LinearLayout;

    .line 169
    .line 170
    invoke-virtual {p2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 171
    .line 172
    .line 173
    const-string p2, "load over."

    .line 174
    .line 175
    invoke-static {v1, p2}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    .line 177
    .line 178
    invoke-virtual {p3}, Lcom/intsig/camscanner/loadimage/PageImage;->OO0o〇〇()I

    .line 179
    .line 180
    .line 181
    move-result p2

    .line 182
    const/4 v1, 0x2

    .line 183
    if-ne p2, v1, :cond_3

    .line 184
    .line 185
    const/4 v3, 0x1

    .line 186
    :cond_3
    invoke-virtual {p1, v3}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;->o〇〇0〇(Z)V

    .line 187
    .line 188
    .line 189
    :cond_4
    :goto_0
    const/4 p1, 0x0

    .line 190
    :goto_1
    sget-object p2, Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;->O88O:Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter$Companion;

    .line 191
    .line 192
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->o〇00O:Landroid/widget/ImageView;

    .line 193
    .line 194
    const-string v1, "ivImage"

    .line 195
    .line 196
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 197
    .line 198
    .line 199
    invoke-virtual {p3}, Lcom/intsig/camscanner/loadimage/PageImage;->oO80()Ljava/lang/String;

    .line 200
    .line 201
    .line 202
    move-result-object p3

    .line 203
    invoke-virtual {p2, v0, p3, p1}, Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter$Companion;->O8(Landroid/widget/ImageView;Ljava/lang/String;Lcom/bumptech/glide/request/RequestListener;)V

    .line 204
    .line 205
    .line 206
    return-void
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method private static final 〇o(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;Lcom/intsig/camscanner/loadimage/PageImage;Lcom/intsig/camscanner/databinding/ItemLrWordBinding;Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;)V
    .locals 8

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "$d"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "$this_apply"

    .line 12
    .line 13
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string v0, "$holder"

    .line 17
    .line 18
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->o〇00O:Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter;

    .line 22
    .line 23
    iget-object v3, p2, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->〇OOo8〇0:Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;

    .line 24
    .line 25
    const-string p0, "galaxy"

    .line 26
    .line 27
    invoke-static {v3, p0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {p3}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemLrWordBinding;

    .line 31
    .line 32
    .line 33
    move-result-object p0

    .line 34
    iget-object v4, p0, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->o〇00O:Landroid/widget/ImageView;

    .line 35
    .line 36
    const-string p0, "holder.binding.ivImage"

    .line 37
    .line 38
    invoke-static {v4, p0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    const/4 v5, 0x0

    .line 42
    const/16 v6, 0x8

    .line 43
    .line 44
    const/4 v7, 0x0

    .line 45
    move-object v2, p1

    .line 46
    invoke-static/range {v1 .. v7}, Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter$DefaultImpls;->〇080(Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter;Lcom/intsig/camscanner/loadimage/PageImage;Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;Landroid/view/View;ZILjava/lang/Object;)V

    .line 47
    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final 〇oOO8O8(Lcom/intsig/camscanner/loadimage/PageImage;)Z
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->O8o08O8O:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O8〇o()Ljava/util/List;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    const/4 v2, 0x0

    .line 12
    if-nez v1, :cond_0

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    check-cast v0, Ljava/lang/Iterable;

    .line 16
    .line 17
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->〇0(Ljava/lang/Iterable;)Ljava/util/List;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    check-cast v0, Ljava/lang/Iterable;

    .line 22
    .line 23
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 28
    .line 29
    .line 30
    move-result v1

    .line 31
    if-eqz v1, :cond_2

    .line 32
    .line 33
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    check-cast v1, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;

    .line 38
    .line 39
    instance-of v3, v1, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListImageItem;

    .line 40
    .line 41
    if-eqz v3, :cond_1

    .line 42
    .line 43
    check-cast v1, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListImageItem;

    .line 44
    .line 45
    invoke-virtual {v1}, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListImageItem;->getPageImage()Lcom/intsig/camscanner/loadimage/PageImage;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/PageImage;->o800o8O()J

    .line 50
    .line 51
    .line 52
    move-result-wide v0

    .line 53
    invoke-virtual {p1}, Lcom/intsig/camscanner/loadimage/PageImage;->o800o8O()J

    .line 54
    .line 55
    .line 56
    move-result-wide v3

    .line 57
    cmp-long p1, v0, v3

    .line 58
    .line 59
    if-nez p1, :cond_2

    .line 60
    .line 61
    const/4 p1, 0x1

    .line 62
    const/4 v2, 0x1

    .line 63
    :cond_2
    :goto_0
    return v2
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static synthetic 〇oo〇(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;Lcom/intsig/camscanner/loadimage/PageImage;Lcom/intsig/camscanner/databinding/ItemLrWordBinding;Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->〇o(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;Lcom/intsig/camscanner/loadimage/PageImage;Lcom/intsig/camscanner/databinding/ItemLrWordBinding;Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method


# virtual methods
.method public final O8ooOoo〇()Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->o〇00O:Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "parent"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1, p2}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->Oooo8o0〇(Landroid/view/ViewGroup;I)Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;

    .line 7
    .line 8
    .line 9
    move-result-object p2

    .line 10
    new-instance v0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;

    .line 11
    .line 12
    iget-object p2, p2, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    .line 13
    .line 14
    const-string v1, "baseViewHolder.itemView"

    .line 15
    .line 16
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    invoke-direct {v0, p2}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;-><init>(Landroid/view/View;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;->〇00()Lcom/intsig/camscanner/databinding/ItemLrWordBinding;

    .line 23
    .line 24
    .line 25
    move-result-object p2

    .line 26
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ItemLrWordBinding;->O8o08O8O:Landroid/widget/LinearLayout;

    .line 27
    .line 28
    new-instance v1, L〇08O8o8/o〇0;

    .line 29
    .line 30
    invoke-direct {v1, p1, v0, p0}, L〇08O8o8/o〇0;-><init>(Landroid/view/ViewGroup;Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 34
    .line 35
    .line 36
    return-object v0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public O〇8O8〇008(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/pagelist/model/PageTypeItem;)V
    .locals 3
    .param p1    # Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/pagelist/model/PageTypeItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "helper"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "item"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "ImageEditLrProvider"

    .line 12
    .line 13
    const-string v1, "convert"

    .line 14
    .line 15
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    check-cast p1, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;

    .line 19
    .line 20
    instance-of v0, p2, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListImageItem;

    .line 21
    .line 22
    if-nez v0, :cond_0

    .line 23
    .line 24
    return-void

    .line 25
    :cond_0
    move-object v0, p2

    .line 26
    check-cast v0, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListImageItem;

    .line 27
    .line 28
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListImageItem;->getPageImage()Lcom/intsig/camscanner/loadimage/PageImage;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;->〇0000OOO(Lcom/intsig/camscanner/loadimage/PageImage;)V

    .line 33
    .line 34
    .line 35
    iget-object v1, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->O8o08O8O:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 36
    .line 37
    invoke-virtual {v1, p2}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->oO(Ljava/lang/Object;)I

    .line 38
    .line 39
    .line 40
    move-result p2

    .line 41
    if-nez p2, :cond_2

    .line 42
    .line 43
    iget-boolean v1, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->oOo〇8o008:Z

    .line 44
    .line 45
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/PageImage;->Oooo8o0〇()Lcom/intsig/camscanner/pic2word/lr/LrImageJson;

    .line 46
    .line 47
    .line 48
    move-result-object v2

    .line 49
    if-nez v2, :cond_1

    .line 50
    .line 51
    const/4 v2, 0x1

    .line 52
    goto :goto_0

    .line 53
    :cond_1
    const/4 v2, 0x0

    .line 54
    :goto_0
    or-int/2addr v1, v2

    .line 55
    iput-boolean v1, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->oOo〇8o008:Z

    .line 56
    .line 57
    :cond_2
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/PageImage;->Oooo8o0〇()Lcom/intsig/camscanner/pic2word/lr/LrImageJson;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    if-nez v1, :cond_3

    .line 62
    .line 63
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->〇00〇8(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;ILcom/intsig/camscanner/loadimage/PageImage;)V

    .line 64
    .line 65
    .line 66
    const/4 v1, 0x2

    .line 67
    const/4 v2, 0x0

    .line 68
    invoke-static {p0, v0, v2, v1, v2}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->Oo8Oo00oo(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;Lcom/intsig/camscanner/loadimage/PageImage;Lcom/intsig/camscanner/tsapp/imagedownload/DownloadPageRequestTaskData$OnImageCompleteListener;ILjava/lang/Object;)V

    .line 69
    .line 70
    .line 71
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->〇00(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;Lcom/intsig/camscanner/loadimage/PageImage;)V

    .line 72
    .line 73
    .line 74
    goto :goto_1

    .line 75
    :cond_3
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->o0ooO(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;ILcom/intsig/camscanner/loadimage/PageImage;)V

    .line 76
    .line 77
    .line 78
    :goto_1
    iget-object v1, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->O8o08O8O:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 79
    .line 80
    invoke-virtual {v1}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->o0()I

    .line 81
    .line 82
    .line 83
    move-result v1

    .line 84
    if-eqz v1, :cond_4

    .line 85
    .line 86
    iget-object v1, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->O8o08O8O:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 87
    .line 88
    invoke-virtual {v1}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->o0()I

    .line 89
    .line 90
    .line 91
    move-result v1

    .line 92
    if-ne p2, v1, :cond_4

    .line 93
    .line 94
    new-instance p2, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$convert$1;

    .line 95
    .line 96
    invoke-direct {p2, p0, p1, v0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$convert$1;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;Lcom/intsig/camscanner/loadimage/PageImage;)V

    .line 97
    .line 98
    .line 99
    invoke-direct {p0, v0, p2}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->〇0000OOO(Lcom/intsig/camscanner/loadimage/PageImage;Lkotlin/jvm/functions/Function0;)V

    .line 100
    .line 101
    .line 102
    :cond_4
    return-void
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public oO80()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->〇0O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic 〇080(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->O〇8O8〇008(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;Lcom/intsig/camscanner/pagelist/model/PageTypeItem;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public 〇O00(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;)V
    .locals 1
    .param p1    # Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "holder"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-super {p0, p1}, Lcom/chad/library/adapter/base/provider/BaseItemProvider;->〇O00(Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;)V

    .line 7
    .line 8
    .line 9
    instance-of v0, p1, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;

    .line 10
    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    check-cast p1, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;

    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider$ItemHolder;->O8ooOoo〇()V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
.end method

.method public 〇〇888()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;->〇080OO8〇0:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

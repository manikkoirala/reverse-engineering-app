.class public final Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;
.super Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;
.source "ImageEditAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$ImageEditDiffCallBack;,
        Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/chad/library/adapter/base/BaseProviderMultiAdapter<",
        "Lcom/intsig/camscanner/pagelist/model/PageTypeItem;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final O8o〇O0:Landroid/os/Handler;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final OO〇OOo:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇800OO〇0O:I


# instance fields
.field private Oo0O0o8:Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;

.field private final Oo0〇Ooo:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/intsig/camscanner/pic2word/lr/LrView;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final Ooo08:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final O〇08oOOO0:Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o0OoOOo0:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o8〇OO:I

.field private oO00〇o:Landroidx/recyclerview/widget/RecyclerView;

.field private final oOO0880O:Ljava/lang/Runnable;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOoo80oO:Ljava/lang/Runnable;

.field private final oO〇8O8oOo:Lcom/intsig/camscanner/tsapp/request/RequestTask;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final ooO:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/text/Editable;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇o〇Oo88:Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;

.field private final 〇00O0:Landroidx/fragment/app/FragmentActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇0O〇O00O:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/intsig/camscanner/pic2word/lr/LrElement;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇OO8ooO8〇:Z

.field private final 〇OO〇00〇0O:Lkotlin/jvm/functions/Function1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/intsig/camscanner/pic2word/lr/LrView;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇〇〇0o〇〇0:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->OO〇OOo:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$Companion;

    .line 8
    .line 9
    const/16 v0, 0x64

    .line 10
    .line 11
    invoke-static {v0}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    float-to-int v0, v0

    .line 16
    sput v0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->〇800OO〇0O:I

    .line 17
    .line 18
    new-instance v0, Landroid/os/Handler;

    .line 19
    .line 20
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 25
    .line 26
    .line 27
    sput-object v0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->O8o〇O0:Landroid/os/Handler;

    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter;I)V
    .locals 5
    .param p1    # Landroidx/fragment/app/FragmentActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "mPresenter"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    const/4 v1, 0x1

    .line 13
    invoke-direct {p0, v0, v1, v0}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;-><init>(Ljava/util/List;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object p1, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->〇00O0:Landroidx/fragment/app/FragmentActivity;

    .line 17
    .line 18
    iput-object p2, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->O〇08oOOO0:Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter;

    .line 19
    .line 20
    iput p3, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->o8〇OO:I

    .line 21
    .line 22
    new-instance p3, Landroidx/lifecycle/ViewModelProvider;

    .line 23
    .line 24
    invoke-direct {p3, p1}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;)V

    .line 25
    .line 26
    .line 27
    const-class v2, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 28
    .line 29
    invoke-virtual {p3, v2}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 30
    .line 31
    .line 32
    move-result-object p3

    .line 33
    check-cast p3, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 34
    .line 35
    iput-object p3, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->Ooo08:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 36
    .line 37
    new-instance v2, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$mOnTableCellClickListener$1;

    .line 38
    .line 39
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$mOnTableCellClickListener$1;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;)V

    .line 40
    .line 41
    .line 42
    iput-object v2, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->ooO:Lkotlin/jvm/functions/Function1;

    .line 43
    .line 44
    new-instance v2, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$mUndoRedoUpdateListener$1;

    .line 45
    .line 46
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$mUndoRedoUpdateListener$1;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;)V

    .line 47
    .line 48
    .line 49
    iput-object v2, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->〇OO〇00〇0O:Lkotlin/jvm/functions/Function1;

    .line 50
    .line 51
    new-instance v2, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$mTextSelectionChangeListener$1;

    .line 52
    .line 53
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$mTextSelectionChangeListener$1;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;)V

    .line 54
    .line 55
    .line 56
    iput-object v2, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->Oo0〇Ooo:Lkotlin/jvm/functions/Function1;

    .line 57
    .line 58
    new-instance v2, Lcom/intsig/camscanner/tsapp/request/RequestTask;

    .line 59
    .line 60
    const-wide/16 v3, 0x0

    .line 61
    .line 62
    invoke-direct {v2, v3, v4, v1, v0}, Lcom/intsig/camscanner/tsapp/request/RequestTask;-><init>(JILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 63
    .line 64
    .line 65
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->o〇0()Ljava/util/concurrent/ExecutorService;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/tsapp/request/RequestTask;->〇00(Ljava/util/concurrent/ExecutorService;)V

    .line 70
    .line 71
    .line 72
    const/4 v0, 0x4

    .line 73
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/tsapp/request/RequestTask;->o〇O8〇〇o(I)V

    .line 74
    .line 75
    .line 76
    iput-object v2, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->oO〇8O8oOo:Lcom/intsig/camscanner/tsapp/request/RequestTask;

    .line 77
    .line 78
    new-instance v0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$mOnChildFocusChangeListener$1;

    .line 79
    .line 80
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$mOnChildFocusChangeListener$1;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;)V

    .line 81
    .line 82
    .line 83
    iput-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->〇0O〇O00O:Lkotlin/jvm/functions/Function1;

    .line 84
    .line 85
    new-instance v0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$mOnKeyboardChangeListener$1;

    .line 86
    .line 87
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$mOnKeyboardChangeListener$1;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;)V

    .line 88
    .line 89
    .line 90
    iput-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->o0OoOOo0:Lkotlin/jvm/functions/Function0;

    .line 91
    .line 92
    invoke-virtual {p3}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->o0ooO()Landroidx/lifecycle/MutableLiveData;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    new-instance v1, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$1;

    .line 97
    .line 98
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$1;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;)V

    .line 99
    .line 100
    .line 101
    new-instance v2, L〇08O8o8/〇080;

    .line 102
    .line 103
    invoke-direct {v2, v1}, L〇08O8o8/〇080;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 104
    .line 105
    .line 106
    invoke-virtual {v0, p1, v2}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 107
    .line 108
    .line 109
    invoke-virtual {p3}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->〇00()Landroidx/lifecycle/MutableLiveData;

    .line 110
    .line 111
    .line 112
    move-result-object p3

    .line 113
    new-instance v0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$2;

    .line 114
    .line 115
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$2;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;)V

    .line 116
    .line 117
    .line 118
    new-instance v1, L〇08O8o8/〇o00〇〇Oo;

    .line 119
    .line 120
    invoke-direct {v1, v0}, L〇08O8o8/〇o00〇〇Oo;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 121
    .line 122
    .line 123
    invoke-virtual {p3, p1, v1}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 124
    .line 125
    .line 126
    new-instance p1, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;

    .line 127
    .line 128
    invoke-direct {p1, p2, p0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditLrProvider;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter;Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;)V

    .line 129
    .line 130
    .line 131
    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;->ooO〇00O(Lcom/chad/library/adapter/base/provider/BaseItemProvider;)V

    .line 132
    .line 133
    .line 134
    new-instance p1, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$ImageEditDiffCallBack;

    .line 135
    .line 136
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$ImageEditDiffCallBack;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;)V

    .line 137
    .line 138
    .line 139
    invoke-virtual {p0, p1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o8O〇(Landroidx/recyclerview/widget/DiffUtil$ItemCallback;)V

    .line 140
    .line 141
    .line 142
    new-instance p1, L〇08O8o8/〇o〇;

    .line 143
    .line 144
    invoke-direct {p1, p0}, L〇08O8o8/〇o〇;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;)V

    .line 145
    .line 146
    .line 147
    iput-object p1, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->oOO0880O:Ljava/lang/Runnable;

    .line 148
    .line 149
    return-void
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public static synthetic O00(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->o0O〇8o0O(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic O0o(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->OO0〇〇8(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic O0oO008()Landroid/os/Handler;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->O8o〇O0:Landroid/os/Handler;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static final O0o〇O0〇(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final OO0〇〇8(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;)V
    .locals 12

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    iput-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->oOoo80oO:Ljava/lang/Runnable;

    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->oOo()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    return-void

    .line 16
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->〇00O0:Landroidx/fragment/app/FragmentActivity;

    .line 17
    .line 18
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    if-eqz v1, :cond_6

    .line 23
    .line 24
    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    if-nez v1, :cond_1

    .line 29
    .line 30
    goto/16 :goto_0

    .line 31
    .line 32
    :cond_1
    iget-object p0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->oO00〇o:Landroidx/recyclerview/widget/RecyclerView;

    .line 33
    .line 34
    if-nez p0, :cond_2

    .line 35
    .line 36
    return-void

    .line 37
    :cond_2
    new-instance v2, Landroid/graphics/RectF;

    .line 38
    .line 39
    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    .line 40
    .line 41
    .line 42
    invoke-virtual {v0, v2}, Lcom/intsig/camscanner/pic2word/lr/LrView;->〇O888o0o(Landroid/graphics/RectF;)V

    .line 43
    .line 44
    .line 45
    invoke-virtual {v2}, Landroid/graphics/RectF;->isEmpty()Z

    .line 46
    .line 47
    .line 48
    move-result v3

    .line 49
    if-eqz v3, :cond_3

    .line 50
    .line 51
    return-void

    .line 52
    :cond_3
    const/4 v3, 0x2

    .line 53
    new-array v4, v3, [I

    .line 54
    .line 55
    invoke-virtual {p0, v4}, Landroid/view/View;->getLocationInWindow([I)V

    .line 56
    .line 57
    .line 58
    new-array v3, v3, [I

    .line 59
    .line 60
    invoke-virtual {v0, v3}, Landroid/view/View;->getLocationInWindow([I)V

    .line 61
    .line 62
    .line 63
    new-instance v5, Landroid/graphics/Rect;

    .line 64
    .line 65
    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    .line 66
    .line 67
    .line 68
    invoke-virtual {v0, v5}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 69
    .line 70
    .line 71
    const/4 v0, 0x1

    .line 72
    aget v3, v3, v0

    .line 73
    .line 74
    iput v3, v5, Landroid/graphics/Rect;->top:I

    .line 75
    .line 76
    new-instance v3, Landroid/graphics/RectF;

    .line 77
    .line 78
    invoke-direct {v3, v5}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 79
    .line 80
    .line 81
    new-instance v6, Ljava/lang/StringBuilder;

    .line 82
    .line 83
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 84
    .line 85
    .line 86
    const-string v7, "focus rect: "

    .line 87
    .line 88
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    .line 90
    .line 91
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 92
    .line 93
    .line 94
    const-string v7, " lrRect: "

    .line 95
    .line 96
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    .line 98
    .line 99
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 103
    .line 104
    .line 105
    move-result-object v5

    .line 106
    const-string v6, "ImageEditAdapter"

    .line 107
    .line 108
    invoke-static {v6, v5}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    .line 110
    .line 111
    instance-of v5, p0, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;

    .line 112
    .line 113
    const/high16 v8, 0x3f800000    # 1.0f

    .line 114
    .line 115
    const/4 v9, 0x0

    .line 116
    if-eqz v5, :cond_4

    .line 117
    .line 118
    move-object v5, p0

    .line 119
    check-cast v5, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;

    .line 120
    .line 121
    invoke-virtual {v5}, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->getMatrixScale()F

    .line 122
    .line 123
    .line 124
    move-result v10

    .line 125
    cmpl-float v10, v10, v8

    .line 126
    .line 127
    if-lez v10, :cond_4

    .line 128
    .line 129
    invoke-virtual {v5}, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->getMatrixScale()F

    .line 130
    .line 131
    .line 132
    move-result v8

    .line 133
    aget v10, v4, v0

    .line 134
    .line 135
    int-to-float v10, v10

    .line 136
    neg-float v11, v10

    .line 137
    invoke-virtual {v3, v9, v11}, Landroid/graphics/RectF;->offset(FF)V

    .line 138
    .line 139
    .line 140
    invoke-virtual {v5}, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->getDrawMatrix()Landroid/graphics/Matrix;

    .line 141
    .line 142
    .line 143
    move-result-object v11

    .line 144
    invoke-virtual {v11, v3}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 145
    .line 146
    .line 147
    invoke-virtual {v5}, Lcom/intsig/camscanner/pic2word/lr/ZoomRv;->getDrawMatrix()Landroid/graphics/Matrix;

    .line 148
    .line 149
    .line 150
    move-result-object v5

    .line 151
    invoke-virtual {v5, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 152
    .line 153
    .line 154
    invoke-virtual {v3, v9, v10}, Landroid/graphics/RectF;->offset(FF)V

    .line 155
    .line 156
    .line 157
    :cond_4
    iget v5, v3, Landroid/graphics/RectF;->left:F

    .line 158
    .line 159
    iget v10, v3, Landroid/graphics/RectF;->top:F

    .line 160
    .line 161
    invoke-virtual {v2, v5, v10}, Landroid/graphics/RectF;->offset(FF)V

    .line 162
    .line 163
    .line 164
    new-instance v5, Ljava/lang/StringBuilder;

    .line 165
    .line 166
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 167
    .line 168
    .line 169
    const-string v10, "matrix map rect: "

    .line 170
    .line 171
    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    .line 173
    .line 174
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 175
    .line 176
    .line 177
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 178
    .line 179
    .line 180
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 181
    .line 182
    .line 183
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 184
    .line 185
    .line 186
    move-result-object v3

    .line 187
    invoke-static {v6, v3}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    .line 189
    .line 190
    new-instance v3, Landroid/graphics/Rect;

    .line 191
    .line 192
    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 193
    .line 194
    .line 195
    invoke-virtual {v1, v3}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 196
    .line 197
    .line 198
    aget v0, v4, v0

    .line 199
    .line 200
    int-to-float v0, v0

    .line 201
    iget v1, v2, Landroid/graphics/RectF;->top:F

    .line 202
    .line 203
    sub-float/2addr v0, v1

    .line 204
    const/4 v1, 0x0

    .line 205
    cmpl-float v4, v0, v9

    .line 206
    .line 207
    if-lez v4, :cond_5

    .line 208
    .line 209
    sget v2, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->〇800OO〇0O:I

    .line 210
    .line 211
    int-to-float v2, v2

    .line 212
    add-float/2addr v0, v2

    .line 213
    div-float/2addr v0, v8

    .line 214
    float-to-int v0, v0

    .line 215
    neg-int v0, v0

    .line 216
    new-instance v2, Ljava/lang/StringBuilder;

    .line 217
    .line 218
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 219
    .line 220
    .line 221
    const-string v3, "\u4e0a\u9762\u88ab\u906e\u6321 scrollY: "

    .line 222
    .line 223
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    .line 225
    .line 226
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 227
    .line 228
    .line 229
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 230
    .line 231
    .line 232
    move-result-object v2

    .line 233
    invoke-static {v6, v2}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    .line 235
    .line 236
    invoke-virtual {p0, v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->smoothScrollBy(II)V

    .line 237
    .line 238
    .line 239
    return-void

    .line 240
    :cond_5
    iget v0, v2, Landroid/graphics/RectF;->bottom:F

    .line 241
    .line 242
    iget v2, v3, Landroid/graphics/Rect;->bottom:I

    .line 243
    .line 244
    int-to-float v2, v2

    .line 245
    sub-float/2addr v0, v2

    .line 246
    sget v2, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->〇800OO〇0O:I

    .line 247
    .line 248
    int-to-float v2, v2

    .line 249
    add-float/2addr v0, v2

    .line 250
    cmpl-float v2, v0, v9

    .line 251
    .line 252
    if-lez v2, :cond_6

    .line 253
    .line 254
    div-float/2addr v0, v8

    .line 255
    float-to-int v0, v0

    .line 256
    new-instance v2, Ljava/lang/StringBuilder;

    .line 257
    .line 258
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 259
    .line 260
    .line 261
    const-string v3, "scrollY: "

    .line 262
    .line 263
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    .line 265
    .line 266
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 267
    .line 268
    .line 269
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 270
    .line 271
    .line 272
    move-result-object v2

    .line 273
    invoke-static {v6, v2}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    .line 275
    .line 276
    invoke-virtual {p0, v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->smoothScrollBy(II)V

    .line 277
    .line 278
    .line 279
    :cond_6
    :goto_0
    return-void
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public static final synthetic Oo0oO〇O〇O(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;)Lcom/intsig/camscanner/pic2word/lr/LrView;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->oOo()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final o0O〇8o0O(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o8O0(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->O0o〇O0〇(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final oOo()Lcom/intsig/camscanner/pic2word/lr/LrView;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->Ooo08:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->〇8o8o〇()Lcom/intsig/camscanner/pic2word/lr/LrView;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic o〇8〇(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;)Ljava/lang/Runnable;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->oOoo80oO:Ljava/lang/Runnable;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇0OO8(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->〇OO8ooO8〇:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇Oo〇o8()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->oO00〇o:Landroidx/recyclerview/widget/RecyclerView;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getScrollState()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->oOO0880O:Ljava/lang/Runnable;

    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->oOoo80oO:Ljava/lang/Runnable;

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->oOO0880O:Ljava/lang/Runnable;

    .line 18
    .line 19
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 20
    .line 21
    .line 22
    :goto_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic 〇〇00OO(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->〇Oo〇o8()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public final O00O()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/intsig/camscanner/pic2word/lr/LrView;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->〇OO〇00〇0O:Lkotlin/jvm/functions/Function1;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O0oo0o0〇()Lcom/intsig/camscanner/pic2word/lr/LrViewModel;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->Ooo08:Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final OOoo()Lcom/intsig/camscanner/tsapp/request/RequestTask;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->oO〇8O8oOo:Lcom/intsig/camscanner/tsapp/request/RequestTask;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final OO〇0008O8()Lkotlin/jvm/functions/Function0;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->o0OoOOo0:Lkotlin/jvm/functions/Function0;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final Oo08OO8oO(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/loadimage/PageImage;",
            ">;)V"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    check-cast p1, Ljava/lang/Iterable;

    .line 9
    .line 10
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    if-eqz v1, :cond_0

    .line 19
    .line 20
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    check-cast v1, Lcom/intsig/camscanner/loadimage/PageImage;

    .line 25
    .line 26
    new-instance v2, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListImageItem;

    .line 27
    .line 28
    invoke-direct {v2, v1}, Lcom/intsig/camscanner/pagelist/adapter/word/data/WordListImageItem;-><init>(Lcom/intsig/camscanner/loadimage/PageImage;)V

    .line 29
    .line 30
    .line 31
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 32
    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_0
    const/4 p1, 0x1

    .line 36
    iput-boolean p1, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->〇〇〇0o〇〇0:Z

    .line 37
    .line 38
    iget-object p1, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->O〇08oOOO0:Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter;

    .line 39
    .line 40
    instance-of v1, p1, Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;

    .line 41
    .line 42
    if-eqz v1, :cond_1

    .line 43
    .line 44
    check-cast p1, Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;

    .line 45
    .line 46
    invoke-virtual {p1}, Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;->o〇O8〇〇o()Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    invoke-virtual {p1, p0}, Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew;->OoO8(Lcom/chad/library/adapter/base/BaseProviderMultiAdapter;)V

    .line 51
    .line 52
    .line 53
    :cond_1
    const/4 p1, 0x2

    .line 54
    const/4 v1, 0x0

    .line 55
    invoke-static {p0, v0, v1, p1, v1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->〇O(Lcom/chad/library/adapter/base/BaseQuickAdapter;Ljava/util/List;Ljava/lang/Runnable;ILjava/lang/Object;)V

    .line 56
    .line 57
    .line 58
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final O〇0〇o808〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->〇〇〇0o〇〇0:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final O〇Oo(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->〇〇〇0o〇〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final getActivity()Landroidx/fragment/app/FragmentActivity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->〇00O0:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final o0()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->o8〇OO:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method protected oO〇(Ljava/util/List;I)I
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/camscanner/pagelist/model/PageTypeItem;",
            ">;I)I"
        }
    .end annotation

    .line 1
    const-string v0, "data"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    check-cast p1, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;

    .line 11
    .line 12
    instance-of p2, p1, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 13
    .line 14
    if-eqz p2, :cond_0

    .line 15
    .line 16
    sget-object p1, Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;->IMAGE:Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;

    .line 17
    .line 18
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;->getType()I

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    instance-of p1, p1, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/PageBtmEmptyTypeItem;

    .line 24
    .line 25
    if-eqz p1, :cond_1

    .line 26
    .line 27
    sget-object p1, Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;->BTM_EMPTY:Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;

    .line 28
    .line 29
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;->getType()I

    .line 30
    .line 31
    .line 32
    move-result p1

    .line 33
    goto :goto_0

    .line 34
    :cond_1
    sget-object p1, Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;->IMAGE:Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;

    .line 35
    .line 36
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageTypeEnum;->getType()I

    .line 37
    .line 38
    .line 39
    move-result p1

    .line 40
    :goto_0
    return p1
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public onAttachedToRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 1
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "recyclerView"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->oO00〇o:Landroidx/recyclerview/widget/RecyclerView;

    .line 7
    .line 8
    new-instance v0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$onAttachedToRecyclerView$l$1;

    .line 9
    .line 10
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter$onAttachedToRecyclerView$l$1;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;)V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->o〇o〇Oo88:Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;

    .line 14
    .line 15
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
.end method

.method public onDetachedFromRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 1
    .param p1    # Landroidx/recyclerview/widget/RecyclerView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "recyclerView"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    iput-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->oO00〇o:Landroidx/recyclerview/widget/RecyclerView;

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->o〇o〇Oo88:Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;

    .line 10
    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    return-void

    .line 14
    :cond_0
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->removeOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
.end method

.method public final ooo〇〇O〇()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/intsig/camscanner/pic2word/lr/LrElement;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->〇0O〇O00O:Lkotlin/jvm/functions/Function1;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇8o()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Landroid/text/Editable;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->ooO:Lkotlin/jvm/functions/Function1;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o〇8()Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->Oo0O0o8:Lcom/intsig/camscanner/pagelist/model/ImageJsonParam;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇o〇Oo0()Lkotlin/jvm/functions/Function1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlin/jvm/functions/Function1<",
            "Lcom/intsig/camscanner/pic2word/lr/LrView;",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->Oo0〇Ooo:Lkotlin/jvm/functions/Function1;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;
.super Lcom/intsig/mvp/fragment/BaseChangeFragment;
.source "ImageEditingFragment.kt"

# interfaces
.implements Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$View;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field static final synthetic oOo〇8o008:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final 〇0O:Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:I

.field private final OO:Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:Z

.field private 〇080OO8〇0:Z

.field private 〇08O〇00〇o:I

.field private 〇OOo8〇0:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "vb"

    .line 7
    .line 8
    const-string v3, "getVb()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->oOo〇8o008:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇0O:Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment$Companion;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v6, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x4

    .line 10
    const/4 v5, 0x0

    .line 11
    move-object v0, v6

    .line 12
    move-object v2, p0

    .line 13
    invoke-direct/range {v0 .. v5}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object v6, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 17
    .line 18
    new-instance v0, Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;

    .line 19
    .line 20
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$View;)V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->OO:Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final O0O0〇()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->OO:Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;

    .line 2
    .line 3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;->Oo8Oo00oo(Landroid/os/Bundle;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic O0〇0(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O8〇8〇O80(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V
    .locals 8

    .line 1
    instance-of v0, p1, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    check-cast p1, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 7
    .line 8
    invoke-virtual {p1}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstCompletelyVisibleItemPosition()I

    .line 9
    .line 10
    .line 11
    invoke-virtual {p1}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    invoke-virtual {p1}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    iget-object v2, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->OO:Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;

    .line 20
    .line 21
    invoke-virtual {v2}, Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;->O〇8O8〇008()Ljava/util/ArrayList;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    const/4 v3, 0x0

    .line 26
    const-string v4, "lrAdapter"

    .line 27
    .line 28
    if-eqz v2, :cond_1

    .line 29
    .line 30
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    .line 31
    .line 32
    .line 33
    move-result v2

    .line 34
    goto :goto_0

    .line 35
    :cond_1
    iget-object v2, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇OOo8〇0:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 36
    .line 37
    if-nez v2, :cond_2

    .line 38
    .line 39
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    move-object v2, v3

    .line 43
    :cond_2
    invoke-virtual {v2}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->getItemCount()I

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    :goto_0
    invoke-static {v1, v2}, Lkotlin/ranges/RangesKt;->〇o〇(II)I

    .line 48
    .line 49
    .line 50
    move-result v1

    .line 51
    const/4 v2, 0x1

    .line 52
    if-gt v0, v1, :cond_4

    .line 53
    .line 54
    move v5, v1

    .line 55
    :goto_1
    const/4 v6, 0x2

    .line 56
    new-array v6, v6, [I

    .line 57
    .line 58
    invoke-virtual {p1, v5}, Landroidx/recyclerview/widget/LinearLayoutManager;->findViewByPosition(I)Landroid/view/View;

    .line 59
    .line 60
    .line 61
    move-result-object v7

    .line 62
    if-eqz v7, :cond_3

    .line 63
    .line 64
    invoke-virtual {v7, v6}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 65
    .line 66
    .line 67
    aget v6, v6, v2

    .line 68
    .line 69
    iget v7, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇08O〇00〇o:I

    .line 70
    .line 71
    if-gt v6, v7, :cond_3

    .line 72
    .line 73
    move v1, v5

    .line 74
    goto :goto_2

    .line 75
    :cond_3
    if-eq v5, v0, :cond_4

    .line 76
    .line 77
    add-int/lit8 v5, v5, -0x1

    .line 78
    .line 79
    goto :goto_1

    .line 80
    :cond_4
    :goto_2
    iget-object p1, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇OOo8〇0:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 81
    .line 82
    if-nez p1, :cond_5

    .line 83
    .line 84
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    goto :goto_3

    .line 88
    :cond_5
    move-object v3, p1

    .line 89
    :goto_3
    invoke-virtual {v3}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->getItemCount()I

    .line 90
    .line 91
    .line 92
    move-result p1

    .line 93
    if-lez p1, :cond_6

    .line 94
    .line 95
    if-ge v1, p1, :cond_6

    .line 96
    .line 97
    add-int/2addr v1, v2

    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    .line 99
    .line 100
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 101
    .line 102
    .line 103
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 104
    .line 105
    .line 106
    const-string v1, "/"

    .line 107
    .line 108
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object p1

    .line 118
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 119
    .line 120
    .line 121
    move-result-object v0

    .line 122
    if-eqz v0, :cond_6

    .line 123
    .line 124
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->o8〇OO0〇0o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 125
    .line 126
    if-eqz v0, :cond_6

    .line 127
    .line 128
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    .line 130
    .line 131
    const/high16 p1, 0x3f800000    # 1.0f

    .line 132
    .line 133
    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    .line 134
    .line 135
    .line 136
    const/4 p1, 0x0

    .line 137
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 138
    .line 139
    .line 140
    :cond_6
    return-void
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final Ooo8o()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->o8〇OO0〇0o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 19
    .line 20
    .line 21
    const-wide/16 v1, 0xfa

    .line 22
    .line 23
    invoke-virtual {v0, v1, v2}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 27
    .line 28
    .line 29
    :cond_0
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic o00〇88〇08(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->Ooo8o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o880(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇Oo〇O(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final oOoO8OO〇()V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment$animTitleRoot$listener$1;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment$animTitleRoot$listener$1;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->〇080OO8〇0:Landroid/widget/RelativeLayout;

    .line 13
    .line 14
    if-eqz v1, :cond_0

    .line 15
    .line 16
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    if-eqz v1, :cond_0

    .line 21
    .line 22
    sget-object v2, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 23
    .line 24
    invoke-virtual {v2}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 25
    .line 26
    .line 27
    move-result-object v2

    .line 28
    const/16 v3, 0x58

    .line 29
    .line 30
    invoke-static {v2, v3}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 31
    .line 32
    .line 33
    move-result v2

    .line 34
    int-to-float v2, v2

    .line 35
    neg-float v2, v2

    .line 36
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->translationYBy(F)Landroid/view/ViewPropertyAnimator;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    if-eqz v1, :cond_0

    .line 41
    .line 42
    const-wide/16 v2, 0xa

    .line 43
    .line 44
    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    if-eqz v1, :cond_0

    .line 49
    .line 50
    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    if-eqz v0, :cond_0

    .line 55
    .line 56
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 57
    .line 58
    .line 59
    :cond_0
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic oO〇oo(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇0〇0(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oooO888(ILcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇8O0880(ILcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic o〇0〇o(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;)Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o〇O8OO(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;ZZ)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->o〇oo(ZZ)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private final o〇oo(ZZ)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->ooo0〇〇O:Lcom/intsig/camscanner/image_progress/image_editing/views/ImageEditOpeView;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/image_progress/image_editing/views/ImageEditOpeView;->〇o〇(Z)V

    .line 12
    .line 13
    .line 14
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 15
    .line 16
    .line 17
    move-result-object p2

    .line 18
    if-eqz p2, :cond_1

    .line 19
    .line 20
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->ooo0〇〇O:Lcom/intsig/camscanner/image_progress/image_editing/views/ImageEditOpeView;

    .line 21
    .line 22
    if-eqz p2, :cond_1

    .line 23
    .line 24
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/image_progress/image_editing/views/ImageEditOpeView;->O8(Z)V

    .line 25
    .line 26
    .line 27
    :cond_1
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇088O(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇08O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇08O()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    const-string v1, "mActivity"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v1, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment$saveEdit$1;

    .line 9
    .line 10
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment$saveEdit$1;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;)V

    .line 11
    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingHelper;->o8(Landroid/content/Context;Lkotlin/jvm/functions/Function0;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇0oO〇oo00(Ljava/util/List;Z)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lcom/intsig/camscanner/loadimage/PageImage;",
            ">;Z)V"
        }
    .end annotation

    .line 1
    move-object v0, p1

    .line 2
    check-cast v0, Ljava/util/Collection;

    .line 3
    .line 4
    const/4 v1, 0x1

    .line 5
    const/4 v2, 0x0

    .line 6
    if-eqz v0, :cond_1

    .line 7
    .line 8
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    goto :goto_1

    .line 17
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 18
    :goto_1
    const/4 v3, -0x1

    .line 19
    const-string v4, "ImageEditingFragment"

    .line 20
    .line 21
    const/4 v5, 0x2

    .line 22
    const/4 v6, 0x0

    .line 23
    if-eqz v0, :cond_3

    .line 24
    .line 25
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    if-eqz p1, :cond_2

    .line 30
    .line 31
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->ooo0〇〇O:Lcom/intsig/camscanner/image_progress/image_editing/views/ImageEditOpeView;

    .line 32
    .line 33
    if-eqz p1, :cond_2

    .line 34
    .line 35
    invoke-static {p1, v3, v2, v5, v6}, Lcom/intsig/camscanner/image_progress/image_editing/views/ImageEditOpeView;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/image_progress/image_editing/views/ImageEditOpeView;IZILjava/lang/Object;)V

    .line 36
    .line 37
    .line 38
    :cond_2
    const-string p1, "refreshHeaderView page list is empty"

    .line 39
    .line 40
    invoke-static {v4, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇〇O80〇0o(Z)V

    .line 44
    .line 45
    .line 46
    return-void

    .line 47
    :cond_3
    check-cast p1, Ljava/lang/Iterable;

    .line 48
    .line 49
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    :cond_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 54
    .line 55
    .line 56
    move-result v7

    .line 57
    if-eqz v7, :cond_6

    .line 58
    .line 59
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 60
    .line 61
    .line 62
    move-result-object v7

    .line 63
    move-object v8, v7

    .line 64
    check-cast v8, Lcom/intsig/camscanner/loadimage/PageImage;

    .line 65
    .line 66
    invoke-virtual {v8}, Lcom/intsig/camscanner/loadimage/PageImage;->Oooo8o0〇()Lcom/intsig/camscanner/pic2word/lr/LrImageJson;

    .line 67
    .line 68
    .line 69
    move-result-object v8

    .line 70
    if-eqz v8, :cond_5

    .line 71
    .line 72
    const/4 v8, 0x1

    .line 73
    goto :goto_2

    .line 74
    :cond_5
    const/4 v8, 0x0

    .line 75
    :goto_2
    if-eqz v8, :cond_4

    .line 76
    .line 77
    goto :goto_3

    .line 78
    :cond_6
    move-object v7, v6

    .line 79
    :goto_3
    check-cast v7, Lcom/intsig/camscanner/loadimage/PageImage;

    .line 80
    .line 81
    if-nez v7, :cond_8

    .line 82
    .line 83
    if-nez p2, :cond_7

    .line 84
    .line 85
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 86
    .line 87
    .line 88
    move-result-object p1

    .line 89
    if-eqz p1, :cond_7

    .line 90
    .line 91
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->ooo0〇〇O:Lcom/intsig/camscanner/image_progress/image_editing/views/ImageEditOpeView;

    .line 92
    .line 93
    if-eqz p1, :cond_7

    .line 94
    .line 95
    const-string p2, "viewOpe"

    .line 96
    .line 97
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    .line 99
    .line 100
    invoke-static {p1, v3, v2, v5, v6}, Lcom/intsig/camscanner/image_progress/image_editing/views/ImageEditOpeView;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/image_progress/image_editing/views/ImageEditOpeView;IZILjava/lang/Object;)V

    .line 101
    .line 102
    .line 103
    :cond_7
    const-string p1, "refreshHeaderView no word data"

    .line 104
    .line 105
    invoke-static {v4, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇〇O80〇0o(Z)V

    .line 109
    .line 110
    .line 111
    return-void

    .line 112
    :cond_8
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 113
    .line 114
    .line 115
    move-result-object p1

    .line 116
    const/4 p2, 0x0

    .line 117
    :goto_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 118
    .line 119
    .line 120
    move-result v0

    .line 121
    if-eqz v0, :cond_9

    .line 122
    .line 123
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 124
    .line 125
    .line 126
    move-result-object v0

    .line 127
    check-cast v0, Lcom/intsig/camscanner/loadimage/PageImage;

    .line 128
    .line 129
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/PageImage;->Oooo8o0〇()Lcom/intsig/camscanner/pic2word/lr/LrImageJson;

    .line 130
    .line 131
    .line 132
    move-result-object v0

    .line 133
    invoke-static {v0}, Lcom/intsig/camscanner/pic2word/util/LrTextUtil;->〇o00〇〇Oo(Lcom/intsig/camscanner/pic2word/lr/LrImageJson;)I

    .line 134
    .line 135
    .line 136
    move-result v0

    .line 137
    add-int/2addr p2, v0

    .line 138
    goto :goto_4

    .line 139
    :cond_9
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇〇O80〇0o(Z)V

    .line 140
    .line 141
    .line 142
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 143
    .line 144
    .line 145
    move-result-object p1

    .line 146
    if-eqz p1, :cond_a

    .line 147
    .line 148
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->ooo0〇〇O:Lcom/intsig/camscanner/image_progress/image_editing/views/ImageEditOpeView;

    .line 149
    .line 150
    if-eqz p1, :cond_a

    .line 151
    .line 152
    invoke-static {p1, p2, v2, v5, v6}, Lcom/intsig/camscanner/image_progress/image_editing/views/ImageEditOpeView;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/image_progress/image_editing/views/ImageEditOpeView;IZILjava/lang/Object;)V

    .line 153
    .line 154
    .line 155
    :cond_a
    return-void
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method private final 〇0ooOOo()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/advertisement/util/NetworkUtil;->〇080(Landroid/content/Context;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 10
    .line 11
    const v1, 0x7f13008d

    .line 12
    .line 13
    .line 14
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    return-void

    .line 22
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const-string v1, "viewLifecycleOwner"

    .line 27
    .line 28
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-static {v0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 36
    .line 37
    .line 38
    move-result-object v3

    .line 39
    const/4 v4, 0x0

    .line 40
    new-instance v5, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment$checkOverEditTimes$1;

    .line 41
    .line 42
    const/4 v0, 0x0

    .line 43
    invoke-direct {v5, p0, v0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment$checkOverEditTimes$1;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;Lkotlin/coroutines/Continuation;)V

    .line 44
    .line 45
    .line 46
    const/4 v6, 0x2

    .line 47
    const/4 v7, 0x0

    .line 48
    invoke-static/range {v2 .. v7}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 49
    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final 〇0〇0(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇8O0880(ILcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;)V
    .locals 4

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    if-ltz p0, :cond_3

    .line 7
    .line 8
    iget-object v0, p1, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇OOo8〇0:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 9
    .line 10
    const/4 v1, 0x0

    .line 11
    const-string v2, "lrAdapter"

    .line 12
    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    move-object v0, v1

    .line 19
    :cond_0
    invoke-virtual {v0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->getItemCount()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    if-ge p0, v0, :cond_3

    .line 24
    .line 25
    iget-object v0, p1, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->OO:Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;

    .line 26
    .line 27
    invoke-virtual {v0}, Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;->O〇8O8〇008()Ljava/util/ArrayList;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    const/4 v3, 0x0

    .line 32
    invoke-direct {p1, v0, v3}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇0oO〇oo00(Ljava/util/List;Z)V

    .line 33
    .line 34
    .line 35
    if-nez p0, :cond_1

    .line 36
    .line 37
    iget-boolean v0, p1, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇080OO8〇0:Z

    .line 38
    .line 39
    if-nez v0, :cond_1

    .line 40
    .line 41
    const/4 v0, 0x1

    .line 42
    iput-boolean v0, p1, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇080OO8〇0:Z

    .line 43
    .line 44
    invoke-direct {p1}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->oOoO8OO〇()V

    .line 45
    .line 46
    .line 47
    :cond_1
    iget-object p1, p1, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇OOo8〇0:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 48
    .line 49
    if-nez p1, :cond_2

    .line 50
    .line 51
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_2
    move-object v1, p1

    .line 56
    :goto_0
    invoke-virtual {v1, p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(I)V

    .line 57
    .line 58
    .line 59
    :cond_3
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static final synthetic 〇8〇80o(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->O8〇8〇O80(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;)Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->OO:Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇O0o〇〇o(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;Ljava/lang/Boolean;)V
    .locals 3

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    const/4 v1, 0x0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->〇OOo8〇0:Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    move-object v0, v1

    .line 17
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 18
    .line 19
    .line 20
    move-result-object v2

    .line 21
    if-eqz v2, :cond_1

    .line 22
    .line 23
    iget-object v1, v2, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->o〇00O:Landroid/widget/RelativeLayout;

    .line 24
    .line 25
    :cond_1
    if-eqz v0, :cond_3

    .line 26
    .line 27
    if-eqz v1, :cond_3

    .line 28
    .line 29
    iget-object v2, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->OO:Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;

    .line 30
    .line 31
    invoke-virtual {v2, v0, v1, p1}, Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;->o〇8oOO88(Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;Landroid/view/View;Ljava/lang/Boolean;)V

    .line 32
    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 35
    .line 36
    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    .line 37
    .line 38
    .line 39
    move-result p1

    .line 40
    if-eqz p1, :cond_2

    .line 41
    .line 42
    return-void

    .line 43
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 44
    .line 45
    .line 46
    move-result-object p0

    .line 47
    if-eqz p0, :cond_3

    .line 48
    .line 49
    iget-object p1, p0, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->〇0O:Landroid/widget/RelativeLayout;

    .line 50
    .line 51
    const-string v0, "rlTransfer"

    .line 52
    .line 53
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    const/4 v0, 0x1

    .line 57
    invoke-static {p1, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 58
    .line 59
    .line 60
    iget-object p1, p0, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->O8o08O8O:Landroid/widget/RelativeLayout;

    .line 61
    .line 62
    const/4 v0, 0x4

    .line 63
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 64
    .line 65
    .line 66
    iget-object p0, p0, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->ooo0〇〇O:Lcom/intsig/camscanner/image_progress/image_editing/views/ImageEditOpeView;

    .line 67
    .line 68
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 69
    .line 70
    .line 71
    :cond_3
    return-void
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;Ljava/lang/Boolean;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇O0o〇〇o(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;Ljava/lang/Boolean;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static synthetic 〇O8〇8000(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;Ljava/util/List;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p3, p3, 0x2

    .line 2
    .line 3
    if-eqz p3, :cond_0

    .line 4
    .line 5
    const/4 p2, 0x1

    .line 6
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇0oO〇oo00(Ljava/util/List;Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private static final 〇Oo〇O(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->oOo〇8o008:Lcom/intsig/camscanner/pic2word/lr/ZoomRv;

    .line 13
    .line 14
    if-eqz v0, :cond_0

    .line 15
    .line 16
    iget p0, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->O8o08O8O:I

    .line 17
    .line 18
    invoke-virtual {v0, p0}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    .line 19
    .line 20
    .line 21
    :cond_0
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final 〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->oOo〇8o008:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇o〇88〇8(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->o〇00O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇〇O80〇0o(Z)V
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const-string p1, "no_text"

    .line 4
    .line 5
    goto :goto_0

    .line 6
    :cond_0
    const-string p1, "text"

    .line 7
    .line 8
    :goto_0
    const-string v0, "CSEditText"

    .line 9
    .line 10
    const-string v1, "type"

    .line 11
    .line 12
    invoke-static {v0, v1, p1}, Lcom/intsig/log/LogAgentHelper;->o〇〇0〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇〇o0〇8(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;)Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇OOo8〇0:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇〇0()V
    .locals 8

    .line 1
    const/4 v0, 0x4

    .line 2
    new-array v1, v0, [Landroid/view/View;

    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 5
    .line 6
    .line 7
    move-result-object v2

    .line 8
    const/4 v3, 0x0

    .line 9
    if-eqz v2, :cond_0

    .line 10
    .line 11
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    move-object v2, v3

    .line 15
    :goto_0
    const/4 v4, 0x0

    .line 16
    aput-object v2, v1, v4

    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    if-eqz v2, :cond_1

    .line 23
    .line 24
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->〇8〇oO〇〇8o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 25
    .line 26
    goto :goto_1

    .line 27
    :cond_1
    move-object v2, v3

    .line 28
    :goto_1
    const/4 v5, 0x1

    .line 29
    aput-object v2, v1, v5

    .line 30
    .line 31
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    if-eqz v2, :cond_2

    .line 36
    .line 37
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->oOo0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 38
    .line 39
    goto :goto_2

    .line 40
    :cond_2
    move-object v2, v3

    .line 41
    :goto_2
    const/4 v5, 0x2

    .line 42
    aput-object v2, v1, v5

    .line 43
    .line 44
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 45
    .line 46
    .line 47
    move-result-object v2

    .line 48
    if-eqz v2, :cond_3

    .line 49
    .line 50
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->OO〇00〇8oO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 51
    .line 52
    goto :goto_3

    .line 53
    :cond_3
    move-object v2, v3

    .line 54
    :goto_3
    const/4 v5, 0x3

    .line 55
    aput-object v2, v1, v5

    .line 56
    .line 57
    invoke-virtual {p0, v1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 58
    .line 59
    .line 60
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    if-eqz v1, :cond_5

    .line 65
    .line 66
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->oOo〇8o008:Lcom/intsig/camscanner/pic2word/lr/ZoomRv;

    .line 67
    .line 68
    if-eqz v1, :cond_5

    .line 69
    .line 70
    iget-object v2, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇OOo8〇0:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 71
    .line 72
    if-nez v2, :cond_4

    .line 73
    .line 74
    const-string v2, "lrAdapter"

    .line 75
    .line 76
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    move-object v2, v3

    .line 80
    :cond_4
    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 81
    .line 82
    .line 83
    invoke-virtual {v1, v4}, Landroidx/recyclerview/widget/RecyclerView;->setNestedScrollingEnabled(Z)V

    .line 84
    .line 85
    .line 86
    new-instance v2, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment$initViews$1$1;

    .line 87
    .line 88
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment$initViews$1$1;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;)V

    .line 89
    .line 90
    .line 91
    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    .line 92
    .line 93
    .line 94
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 95
    .line 96
    .line 97
    move-result-object v1

    .line 98
    if-eqz v1, :cond_6

    .line 99
    .line 100
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 101
    .line 102
    if-eqz v1, :cond_6

    .line 103
    .line 104
    invoke-static {}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingHelper;->o800o8O()Z

    .line 105
    .line 106
    .line 107
    move-result v2

    .line 108
    invoke-static {v1, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 109
    .line 110
    .line 111
    :cond_6
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 112
    .line 113
    .line 114
    move-result-object v1

    .line 115
    if-eqz v1, :cond_7

    .line 116
    .line 117
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->ooo0〇〇O:Lcom/intsig/camscanner/image_progress/image_editing/views/ImageEditOpeView;

    .line 118
    .line 119
    if-eqz v1, :cond_7

    .line 120
    .line 121
    new-instance v2, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment$initViews$2;

    .line 122
    .line 123
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment$initViews$2;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;)V

    .line 124
    .line 125
    .line 126
    new-instance v5, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment$initViews$3;

    .line 127
    .line 128
    invoke-direct {v5, p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment$initViews$3;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;)V

    .line 129
    .line 130
    .line 131
    invoke-virtual {v1, v2, v5}, Lcom/intsig/camscanner/image_progress/image_editing/views/ImageEditOpeView;->o〇0(Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    .line 132
    .line 133
    .line 134
    :cond_7
    invoke-static {}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingHelper;->o800o8O()Z

    .line 135
    .line 136
    .line 137
    move-result v1

    .line 138
    if-eqz v1, :cond_8

    .line 139
    .line 140
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 141
    .line 142
    .line 143
    move-result-object v0

    .line 144
    if-eqz v0, :cond_c

    .line 145
    .line 146
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->〇8〇oO〇〇8o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 147
    .line 148
    if-eqz v0, :cond_c

    .line 149
    .line 150
    invoke-virtual {v0, v3, v3, v3, v3}, Landroidx/appcompat/widget/AppCompatTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 151
    .line 152
    .line 153
    goto :goto_4

    .line 154
    :cond_8
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    .line 155
    .line 156
    .line 157
    move-result-object v1

    .line 158
    const v2, 0x7f080d8a

    .line 159
    .line 160
    .line 161
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    .line 162
    .line 163
    .line 164
    move-result-object v1

    .line 165
    sget-object v2, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 166
    .line 167
    invoke-virtual {v2}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 168
    .line 169
    .line 170
    move-result-object v5

    .line 171
    const/16 v6, 0x10

    .line 172
    .line 173
    invoke-static {v5, v6}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 174
    .line 175
    .line 176
    move-result v5

    .line 177
    invoke-virtual {v2}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 178
    .line 179
    .line 180
    move-result-object v7

    .line 181
    invoke-static {v7, v6}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 182
    .line 183
    .line 184
    move-result v6

    .line 185
    invoke-virtual {v1, v4, v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 186
    .line 187
    .line 188
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 189
    .line 190
    .line 191
    move-result-object v4

    .line 192
    if-eqz v4, :cond_9

    .line 193
    .line 194
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->〇8〇oO〇〇8o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 195
    .line 196
    if-eqz v4, :cond_9

    .line 197
    .line 198
    invoke-virtual {v4, v3, v3, v1, v3}, Landroidx/appcompat/widget/AppCompatTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 199
    .line 200
    .line 201
    :cond_9
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 202
    .line 203
    .line 204
    move-result-object v1

    .line 205
    if-eqz v1, :cond_a

    .line 206
    .line 207
    iget-object v3, v1, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->〇8〇oO〇〇8o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 208
    .line 209
    :cond_a
    if-nez v3, :cond_b

    .line 210
    .line 211
    goto :goto_4

    .line 212
    :cond_b
    invoke-virtual {v2}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 213
    .line 214
    .line 215
    move-result-object v1

    .line 216
    invoke-static {v1, v0}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 217
    .line 218
    .line 219
    move-result v0

    .line 220
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 221
    .line 222
    .line 223
    :cond_c
    :goto_4
    return-void
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private final 〇〇〇00()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const-string v1, "page_pos"

    .line 8
    .line 9
    invoke-virtual {v0, v1}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    :goto_0
    iput v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->O8o08O8O:I

    .line 16
    .line 17
    new-instance v0, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 20
    .line 21
    const-string v2, "mActivity"

    .line 22
    .line 23
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    iget-object v2, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->OO:Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;

    .line 27
    .line 28
    iget v3, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->O8o08O8O:I

    .line 29
    .line 30
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/image_progress/image_editing/contract/ImageEditContract$Presenter;I)V

    .line 31
    .line 32
    .line 33
    iput-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇OOo8〇0:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 36
    .line 37
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    shr-int/lit8 v0, v0, 0x1

    .line 42
    .line 43
    iput v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇08O〇00〇o:I

    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public O000()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->OO:Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;->O〇8O8〇008()Ljava/util/ArrayList;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x0

    .line 8
    const/4 v2, 0x2

    .line 9
    const/4 v3, 0x0

    .line 10
    invoke-static {p0, v0, v1, v2, v3}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇O8〇8000(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;Ljava/util/List;ZILjava/lang/Object;)V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇OOo8〇0:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 14
    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    const-string v0, "lrAdapter"

    .line 18
    .line 19
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    move-object v3, v0

    .line 24
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->OO:Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;

    .line 25
    .line 26
    invoke-virtual {v0}, Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;->O〇8O8〇008()Ljava/util/ArrayList;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-virtual {v3, v0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->Oo08OO8oO(Ljava/util/List;)V

    .line 31
    .line 32
    .line 33
    iget v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->O8o08O8O:I

    .line 34
    .line 35
    if-eqz v0, :cond_1

    .line 36
    .line 37
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    if-eqz v0, :cond_1

    .line 42
    .line 43
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->oOo〇8o008:Lcom/intsig/camscanner/pic2word/lr/ZoomRv;

    .line 44
    .line 45
    if-eqz v0, :cond_1

    .line 46
    .line 47
    new-instance v1, LO08O0〇O/〇080;

    .line 48
    .line 49
    invoke-direct {v1, p0}, LO08O0〇O/〇080;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 53
    .line 54
    .line 55
    :cond_1
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public O8ooOoo〇(Ljava/lang/Boolean;)V
    .locals 3

    .line 1
    new-instance v0, LO08O0〇O/〇o〇;

    .line 2
    .line 3
    invoke-direct {v0, p0, p1}, LO08O0〇O/〇o〇;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;Ljava/lang/Boolean;)V

    .line 4
    .line 5
    .line 6
    const-wide/16 v1, 0x64

    .line 7
    .line 8
    invoke-static {v0, v1, v2}, Lcom/intsig/thread/ThreadUtil;->〇o〇(Ljava/lang/Runnable;J)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public addEvents()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇OOo8〇0:Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "lrAdapter"

    .line 6
    .line 7
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/image_progress/image_editing/adapter/ImageEditAdapter;->O0oo0o0〇()Lcom/intsig/camscanner/pic2word/lr/LrViewModel;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {v0}, Lcom/intsig/camscanner/pic2word/lr/LrViewModel;->o〇0OOo〇0()Landroidx/lifecycle/MutableLiveData;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    new-instance v1, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment$addEvents$1;

    .line 20
    .line 21
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment$addEvents$1;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;)V

    .line 22
    .line 23
    .line 24
    new-instance v2, LO08O0〇O/〇o00〇〇Oo;

    .line 25
    .line 26
    invoke-direct {v2, v1}, LO08O0〇O/〇o00〇〇Oo;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {v0, p0, v2}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 30
    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public dealClickAction(Landroid/view/View;)V
    .locals 8

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->dealClickAction(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    const/4 v1, 0x0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    move-object v0, v1

    .line 15
    :goto_0
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    const-string v2, "ImageEditingFragment"

    .line 20
    .line 21
    if-eqz v0, :cond_3

    .line 22
    .line 23
    const-string p1, "click close"

    .line 24
    .line 25
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    iget-boolean p1, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->o〇00O:Z

    .line 29
    .line 30
    if-eqz p1, :cond_2

    .line 31
    .line 32
    iget-object v2, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 33
    .line 34
    const-string p1, "mActivity"

    .line 35
    .line 36
    invoke-static {v2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    const/4 v3, 0x1

    .line 40
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    instance-of v0, p1, Landroid/view/ViewGroup;

    .line 45
    .line 46
    if-eqz v0, :cond_1

    .line 47
    .line 48
    move-object v1, p1

    .line 49
    check-cast v1, Landroid/view/ViewGroup;

    .line 50
    .line 51
    :cond_1
    move-object v4, v1

    .line 52
    iget-object p1, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->OO:Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;

    .line 53
    .line 54
    invoke-virtual {p1}, Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;->o800o8O()Ljava/util/List;

    .line 55
    .line 56
    .line 57
    move-result-object v5

    .line 58
    iget-object p1, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->OO:Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;

    .line 59
    .line 60
    invoke-virtual {p1}, Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;->o〇O8〇〇o()Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew;

    .line 61
    .line 62
    .line 63
    move-result-object v6

    .line 64
    new-instance v7, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment$dealClickAction$1;

    .line 65
    .line 66
    invoke-direct {v7, p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment$dealClickAction$1;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;)V

    .line 67
    .line 68
    .line 69
    invoke-static/range {v2 .. v7}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingHelper;->〇o(Landroid/content/Context;ZLandroid/view/ViewGroup;Ljava/util/List;Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew;Lkotlin/jvm/functions/Function0;)V

    .line 70
    .line 71
    .line 72
    goto/16 :goto_3

    .line 73
    .line 74
    :cond_2
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 75
    .line 76
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 77
    .line 78
    .line 79
    goto :goto_3

    .line 80
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 81
    .line 82
    .line 83
    move-result-object v0

    .line 84
    if-eqz v0, :cond_4

    .line 85
    .line 86
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->〇8〇oO〇〇8o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 87
    .line 88
    goto :goto_1

    .line 89
    :cond_4
    move-object v0, v1

    .line 90
    :goto_1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 91
    .line 92
    .line 93
    move-result v0

    .line 94
    const-string v3, "CSEditText"

    .line 95
    .line 96
    if-eqz v0, :cond_5

    .line 97
    .line 98
    const-string p1, "click save"

    .line 99
    .line 100
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    .line 102
    .line 103
    const-string p1, "save"

    .line 104
    .line 105
    invoke-static {v3, p1}, Lcom/intsig/log/LogAgentHelper;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇0ooOOo()V

    .line 109
    .line 110
    .line 111
    goto :goto_3

    .line 112
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 113
    .line 114
    .line 115
    move-result-object v0

    .line 116
    if-eqz v0, :cond_6

    .line 117
    .line 118
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->oOo0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 119
    .line 120
    goto :goto_2

    .line 121
    :cond_6
    move-object v0, v1

    .line 122
    :goto_2
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 123
    .line 124
    .line 125
    move-result v0

    .line 126
    if-eqz v0, :cond_7

    .line 127
    .line 128
    const-string p1, "click cancel"

    .line 129
    .line 130
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    .line 132
    .line 133
    const-string p1, "CSEditTextWaiting"

    .line 134
    .line 135
    const-string v0, "cancel"

    .line 136
    .line 137
    invoke-static {p1, v0}, Lcom/intsig/log/LogAgentHelper;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    .line 139
    .line 140
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 141
    .line 142
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 143
    .line 144
    .line 145
    goto :goto_3

    .line 146
    :cond_7
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 147
    .line 148
    .line 149
    move-result-object v0

    .line 150
    if-eqz v0, :cond_8

    .line 151
    .line 152
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->OO〇00〇8oO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 153
    .line 154
    :cond_8
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 155
    .line 156
    .line 157
    move-result p1

    .line 158
    if-eqz p1, :cond_9

    .line 159
    .line 160
    const-string p1, "click feedback"

    .line 161
    .line 162
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    .line 164
    .line 165
    const-string p1, "feedback"

    .line 166
    .line 167
    invoke-static {v3, p1}, Lcom/intsig/log/LogAgentHelper;->oO80(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    .line 169
    .line 170
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 171
    .line 172
    invoke-static {}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingHelper;->Oooo8o0〇()Ljava/lang/String;

    .line 173
    .line 174
    .line 175
    move-result-object v0

    .line 176
    invoke-static {p1, v0}, Lcom/intsig/webview/util/WebUtil;->〇8o8o〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 177
    .line 178
    .line 179
    :cond_9
    :goto_3
    return-void
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇〇〇00()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->addEvents()V

    .line 5
    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇〇〇0()V

    .line 8
    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->O0O0〇()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public interceptBackPressed()Z
    .locals 7

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->o〇00O:Z

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 6
    .line 7
    const-string v0, "mActivity"

    .line 8
    .line 9
    invoke-static {v1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    const/4 v2, 0x1

    .line 13
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getView()Landroid/view/View;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    instance-of v3, v0, Landroid/view/ViewGroup;

    .line 18
    .line 19
    if-eqz v3, :cond_0

    .line 20
    .line 21
    check-cast v0, Landroid/view/ViewGroup;

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    const/4 v0, 0x0

    .line 25
    :goto_0
    move-object v3, v0

    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->OO:Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;

    .line 27
    .line 28
    invoke-virtual {v0}, Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;->o800o8O()Ljava/util/List;

    .line 29
    .line 30
    .line 31
    move-result-object v4

    .line 32
    iget-object v0, p0, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->OO:Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;

    .line 33
    .line 34
    invoke-virtual {v0}, Lcom/intsig/camscanner/image_progress/image_editing/presenter/ImageEditPresenter;->o〇O8〇〇o()Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew;

    .line 35
    .line 36
    .line 37
    move-result-object v5

    .line 38
    new-instance v6, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment$interceptBackPressed$1;

    .line 39
    .line 40
    invoke-direct {v6, p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment$interceptBackPressed$1;-><init>(Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;)V

    .line 41
    .line 42
    .line 43
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingHelper;->〇o(Landroid/content/Context;ZLandroid/view/ViewGroup;Ljava/util/List;Lcom/intsig/camscanner/pic2word/lr/LrUndoManagerNew;Lkotlin/jvm/functions/Function0;)V

    .line 44
    .line 45
    .line 46
    const/4 v0, 0x1

    .line 47
    goto :goto_1

    .line 48
    :cond_1
    const/4 v0, 0x0

    .line 49
    :goto_1
    return v0
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public o〇8oOO88(I)V
    .locals 1

    .line 1
    new-instance v0, LO08O0〇O/O8;

    .line 2
    .line 3
    invoke-direct {v0, p1, p0}, LO08O0〇O/O8;-><init>(ILcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;)V

    .line 4
    .line 5
    .line 6
    invoke-static {v0}, Lcom/intsig/thread/ThreadUtil;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o〇O()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/image_progress/image_editing/ImageEditingFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->〇0O:Landroid/widget/RelativeLayout;

    .line 8
    .line 9
    const-string v2, "rlTransfer"

    .line 10
    .line 11
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    const/4 v2, 0x0

    .line 15
    invoke-static {v1, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 16
    .line 17
    .line 18
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->O8o08O8O:Landroid/widget/RelativeLayout;

    .line 19
    .line 20
    const-string v2, "rlTitleBar"

    .line 21
    .line 22
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    const/4 v2, 0x1

    .line 26
    invoke-static {v1, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 27
    .line 28
    .line 29
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->ooo0〇〇O:Lcom/intsig/camscanner/image_progress/image_editing/views/ImageEditOpeView;

    .line 30
    .line 31
    const-string v3, "viewOpe"

    .line 32
    .line 33
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    invoke-static {v1, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 37
    .line 38
    .line 39
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentImageEditingBinding;->〇OOo8〇0:Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;

    .line 40
    .line 41
    const/16 v1, 0x8

    .line 42
    .line 43
    const/4 v3, 0x0

    .line 44
    invoke-virtual {v0, v1, v3, v3, v2}, Lcom/intsig/camscanner/pic2word/view/GalaxyFlushView;->setVisibility(ILandroid/graphics/Bitmap;Landroid/graphics/Bitmap;Z)V

    .line 45
    .line 46
    .line 47
    :cond_0
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d02e8

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇〇〇0〇〇0()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "image_json_editing"

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

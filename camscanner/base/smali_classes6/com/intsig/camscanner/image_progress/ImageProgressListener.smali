.class public interface abstract Lcom/intsig/camscanner/image_progress/ImageProgressListener;
.super Ljava/lang/Object;
.source "ImageProgressListener.kt"


# annotations
.annotation runtime Lcom/intsig/okbinder/AIDL;
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# virtual methods
.method public abstract setTrimmedImageBorder([I[I)V
.end method

.method public abstract startAdjustImage(III)V
.end method

.method public abstract startDecodeImage()V
.end method

.method public abstract startEncodeImage()V
.end method

.method public abstract startEnhanceImage(I)V
.end method

.method public abstract startRotateAndScaleImage()V
.end method

.method public abstract startTrimImage()V
.end method

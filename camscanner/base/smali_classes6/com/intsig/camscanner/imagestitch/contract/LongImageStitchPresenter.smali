.class public Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;
.super Ljava/lang/Object;
.source "LongImageStitchPresenter.java"

# interfaces
.implements Lcom/intsig/camscanner/imagestitch/contract/ImageStitchContract$Presenter;


# static fields
.field private static Oo08:Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/camscanner/loadimage/BitmapCacheLoader<",
            "Ljava/lang/String;",
            "Lcom/intsig/camscanner/loadimage/BitmapPara;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private O8:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Landroid/graphics/BitmapRegionDecoder;",
            ">;"
        }
    .end annotation
.end field

.field private final 〇080:Lcom/intsig/camscanner/imagestitch/contract/ImageStitchContract$View;

.field private 〇o00〇〇Oo:Lcom/intsig/camscanner/imagestitch/ImageStitchData;

.field private 〇o〇:Lcom/intsig/camscanner/recycler_adapter/item/LongImageWaterMarkItem$UpdateWaterMarkListener;


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/imagestitch/contract/ImageStitchContract$View;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter$1;

    .line 5
    .line 6
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter$1;-><init>(Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;)V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->〇o〇:Lcom/intsig/camscanner/recycler_adapter/item/LongImageWaterMarkItem$UpdateWaterMarkListener;

    .line 10
    .line 11
    new-instance v0, Ljava/util/ArrayList;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->O8:Ljava/util/List;

    .line 17
    .line 18
    iput-object p1, p0, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->〇080:Lcom/intsig/camscanner/imagestitch/contract/ImageStitchContract$View;

    .line 19
    .line 20
    return-void
.end method

.method static bridge synthetic Oo08(Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;)Lcom/intsig/camscanner/imagestitch/contract/ImageStitchContract$View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->〇080:Lcom/intsig/camscanner/imagestitch/contract/ImageStitchContract$View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private oO80()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->Oo08:Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->〇080:Lcom/intsig/camscanner/imagestitch/contract/ImageStitchContract$View;

    .line 8
    .line 9
    invoke-interface {v1}, Lcom/intsig/camscanner/imagestitch/contract/ImageStitchContract$View;->getCurrentActivity()Landroid/app/Activity;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-static {v1}, Lcom/intsig/camscanner/loadimage/BitmapLoaderUtil;->〇o〇(Landroid/content/Context;)I

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;-><init>(I)V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->Oo08:Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;

    .line 21
    .line 22
    :cond_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private o〇0(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Lcom/intsig/adapter/AbsRecyclerViewItem;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-interface {p2}, Ljava/util/List;->size()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 8
    .line 9
    .line 10
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 11
    .line 12
    .line 13
    move-result-object p2

    .line 14
    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 15
    .line 16
    .line 17
    move-result v1

    .line 18
    if-eqz v1, :cond_4

    .line 19
    .line 20
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    check-cast v1, Ljava/lang/String;

    .line 25
    .line 26
    invoke-static {v1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    if-nez v2, :cond_1

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_1
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->〇〇888(Ljava/lang/String;)Ljava/util/List;

    .line 34
    .line 35
    .line 36
    move-result-object v2

    .line 37
    if-eqz v2, :cond_3

    .line 38
    .line 39
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 40
    .line 41
    .line 42
    move-result v3

    .line 43
    if-nez v3, :cond_2

    .line 44
    .line 45
    goto :goto_2

    .line 46
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->oO80()V

    .line 47
    .line 48
    .line 49
    const/4 v3, 0x0

    .line 50
    :try_start_0
    invoke-static {v1, v3}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/lang/String;Z)Landroid/graphics/BitmapRegionDecoder;

    .line 51
    .line 52
    .line 53
    move-result-object v4
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    iget-object v5, p0, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->O8:Ljava/util/List;

    .line 55
    .line 56
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    .line 58
    .line 59
    new-instance v5, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 60
    .line 61
    invoke-direct {v5, v1}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;-><init>(Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 65
    .line 66
    .line 67
    move-result-object v1

    .line 68
    const/4 v2, 0x1

    .line 69
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 70
    .line 71
    .line 72
    move-result v6

    .line 73
    if-eqz v6, :cond_0

    .line 74
    .line 75
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 76
    .line 77
    .line 78
    move-result-object v6

    .line 79
    check-cast v6, Landroid/graphics/Rect;

    .line 80
    .line 81
    new-instance v7, Lcom/intsig/camscanner/recycler_adapter/item/LongRegionImageStitchItem;

    .line 82
    .line 83
    sget-object v8, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->Oo08:Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;

    .line 84
    .line 85
    invoke-direct {v7, p1, v5, v6, v8}, Lcom/intsig/camscanner/recycler_adapter/item/LongRegionImageStitchItem;-><init>(Landroid/content/Context;Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;Landroid/graphics/Rect;Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;)V

    .line 86
    .line 87
    .line 88
    invoke-virtual {v7, v4}, Lcom/intsig/camscanner/recycler_adapter/item/LongRegionImageStitchItem;->〇〇808〇(Landroid/graphics/BitmapRegionDecoder;)V

    .line 89
    .line 90
    .line 91
    iget-object v6, p0, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->〇080:Lcom/intsig/camscanner/imagestitch/contract/ImageStitchContract$View;

    .line 92
    .line 93
    invoke-interface {v6}, Lcom/intsig/camscanner/imagestitch/contract/ImageStitchContract$View;->ooo〇8oO()I

    .line 94
    .line 95
    .line 96
    move-result v6

    .line 97
    invoke-virtual {v7, v6}, Lcom/intsig/camscanner/recycler_adapter/item/LongImageStitchItem;->〇80〇808〇O(I)V

    .line 98
    .line 99
    .line 100
    invoke-virtual {v7, v2}, Lcom/intsig/camscanner/recycler_adapter/item/LongRegionImageStitchItem;->〇O〇(Z)V

    .line 101
    .line 102
    .line 103
    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    .line 105
    .line 106
    const/4 v2, 0x0

    .line 107
    goto :goto_1

    .line 108
    :catch_0
    move-exception v1

    .line 109
    const-string v2, "LongImageStitchPresenter"

    .line 110
    .line 111
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 112
    .line 113
    .line 114
    goto :goto_0

    .line 115
    :cond_3
    :goto_2
    new-instance v2, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;

    .line 116
    .line 117
    invoke-direct {v2, v1}, Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;-><init>(Ljava/lang/String;)V

    .line 118
    .line 119
    .line 120
    new-instance v1, Lcom/intsig/camscanner/recycler_adapter/item/LongImageStitchItem;

    .line 121
    .line 122
    invoke-direct {v1, p1, v2}, Lcom/intsig/camscanner/recycler_adapter/item/LongImageStitchItem;-><init>(Landroid/content/Context;Lcom/intsig/camscanner/recycler_adapter/item/ImageFileData;)V

    .line 123
    .line 124
    .line 125
    iget-object v2, p0, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->〇080:Lcom/intsig/camscanner/imagestitch/contract/ImageStitchContract$View;

    .line 126
    .line 127
    invoke-interface {v2}, Lcom/intsig/camscanner/imagestitch/contract/ImageStitchContract$View;->ooo〇8oO()I

    .line 128
    .line 129
    .line 130
    move-result v2

    .line 131
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/recycler_adapter/item/LongImageStitchItem;->〇80〇808〇O(I)V

    .line 132
    .line 133
    .line 134
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    .line 136
    .line 137
    goto :goto_0

    .line 138
    :cond_4
    return-object v0
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private 〇〇888(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Landroid/graphics/Rect;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->〇080:Lcom/intsig/camscanner/imagestitch/contract/ImageStitchContract$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/imagestitch/contract/ImageStitchContract$View;->ooo〇8oO()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->〇080:Lcom/intsig/camscanner/imagestitch/contract/ImageStitchContract$View;

    .line 8
    .line 9
    invoke-interface {v1}, Lcom/intsig/camscanner/imagestitch/contract/ImageStitchContract$View;->OO〇00〇8oO()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    const/4 v2, 0x0

    .line 14
    if-lez v0, :cond_4

    .line 15
    .line 16
    if-gtz v1, :cond_0

    .line 17
    .line 18
    goto :goto_1

    .line 19
    :cond_0
    const/4 v3, 0x0

    .line 20
    invoke-static {p1, v3}, Lcom/intsig/utils/ImageUtil;->Oooo8o0〇(Ljava/lang/String;Z)[I

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    if-nez p1, :cond_1

    .line 25
    .line 26
    return-object v2

    .line 27
    :cond_1
    aget v4, p1, v3

    .line 28
    .line 29
    const/4 v5, 0x1

    .line 30
    aget p1, p1, v5

    .line 31
    .line 32
    int-to-float v5, p1

    .line 33
    int-to-float v6, v4

    .line 34
    div-float/2addr v5, v6

    .line 35
    int-to-float v6, v1

    .line 36
    int-to-float v0, v0

    .line 37
    div-float/2addr v6, v0

    .line 38
    sub-float/2addr v5, v6

    .line 39
    div-float/2addr v5, v6

    .line 40
    const v6, 0x3e4ccccd    # 0.2f

    .line 41
    .line 42
    .line 43
    cmpg-float v5, v5, v6

    .line 44
    .line 45
    if-gez v5, :cond_2

    .line 46
    .line 47
    return-object v2

    .line 48
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    .line 49
    .line 50
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 51
    .line 52
    .line 53
    mul-int v1, v1, v4

    .line 54
    .line 55
    int-to-float v1, v1

    .line 56
    div-float/2addr v1, v0

    .line 57
    float-to-int v0, v1

    .line 58
    const/4 v1, 0x0

    .line 59
    :goto_0
    if-ge v1, p1, :cond_4

    .line 60
    .line 61
    add-int v5, v1, v0

    .line 62
    .line 63
    if-le v5, p1, :cond_3

    .line 64
    .line 65
    move v5, p1

    .line 66
    :cond_3
    new-instance v6, Landroid/graphics/Rect;

    .line 67
    .line 68
    invoke-direct {v6, v3, v1, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 69
    .line 70
    .line 71
    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    .line 73
    .line 74
    move v1, v5

    .line 75
    goto :goto_0

    .line 76
    :cond_4
    :goto_1
    return-object v2
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method


# virtual methods
.method public O8()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/adapter/AbsRecyclerViewItem;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->〇080:Lcom/intsig/camscanner/imagestitch/contract/ImageStitchContract$View;

    .line 2
    .line 3
    invoke-interface {v0}, Lcom/intsig/camscanner/imagestitch/contract/ImageStitchContract$View;->getCurrentActivity()Landroid/app/Activity;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-eqz v0, :cond_3

    .line 8
    .line 9
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/imagestitch/ImageStitchData;

    .line 17
    .line 18
    if-nez v1, :cond_1

    .line 19
    .line 20
    new-instance v0, Ljava/util/ArrayList;

    .line 21
    .line 22
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 23
    .line 24
    .line 25
    return-object v0

    .line 26
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->〇80〇808〇O()V

    .line 27
    .line 28
    .line 29
    iget-object v1, p0, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->〇080:Lcom/intsig/camscanner/imagestitch/contract/ImageStitchContract$View;

    .line 30
    .line 31
    invoke-interface {v1}, Lcom/intsig/camscanner/imagestitch/contract/ImageStitchContract$View;->getCurrentActivity()Landroid/app/Activity;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    iget-object v2, p0, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/imagestitch/ImageStitchData;

    .line 36
    .line 37
    invoke-virtual {v2}, Lcom/intsig/camscanner/imagestitch/ImageStitchData;->〇o00〇〇Oo()Ljava/util/List;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    invoke-direct {p0, v1, v2}, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->o〇0(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇O8O00()Z

    .line 46
    .line 47
    .line 48
    move-result v2

    .line 49
    if-eqz v2, :cond_2

    .line 50
    .line 51
    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 52
    .line 53
    .line 54
    move-result-object v2

    .line 55
    invoke-static {v2}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇〇8(Landroid/content/Context;)Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v2

    .line 59
    new-instance v3, Lcom/intsig/camscanner/recycler_adapter/item/LongImageWaterMarkItem;

    .line 60
    .line 61
    invoke-direct {v3, v0, v2}, Lcom/intsig/camscanner/recycler_adapter/item/LongImageWaterMarkItem;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    iget-object v0, p0, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->〇o〇:Lcom/intsig/camscanner/recycler_adapter/item/LongImageWaterMarkItem$UpdateWaterMarkListener;

    .line 65
    .line 66
    invoke-virtual {v3, v0}, Lcom/intsig/camscanner/recycler_adapter/item/LongImageWaterMarkItem;->〇O8o08O(Lcom/intsig/camscanner/recycler_adapter/item/LongImageWaterMarkItem$UpdateWaterMarkListener;)V

    .line 67
    .line 68
    .line 69
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    .line 71
    .line 72
    :cond_2
    return-object v1

    .line 73
    :cond_3
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    .line 74
    .line 75
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 76
    .line 77
    .line 78
    return-object v0
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public 〇080(Lcom/intsig/camscanner/imagestitch/ImageStitchData;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/imagestitch/ImageStitchData;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇80〇808〇O()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->O8:Ljava/util/List;

    .line 2
    .line 3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Landroid/graphics/BitmapRegionDecoder;

    .line 18
    .line 19
    :try_start_0
    invoke-virtual {v1}, Landroid/graphics/BitmapRegionDecoder;->recycle()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :catch_0
    move-exception v1

    .line 24
    const-string v2, "LongImageStitchPresenter"

    .line 25
    .line 26
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 27
    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->O8:Ljava/util/List;

    .line 31
    .line 32
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇o00〇〇Oo()V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->Oo08:Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;->O8()V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    sput-object v0, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->Oo08:Lcom/intsig/camscanner/loadimage/BitmapCacheLoader;

    .line 10
    .line 11
    :cond_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇()Lcom/intsig/camscanner/imagestitch/ImageStitchData;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/imagestitch/contract/LongImageStitchPresenter;->〇o00〇〇Oo:Lcom/intsig/camscanner/imagestitch/ImageStitchData;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

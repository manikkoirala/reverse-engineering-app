.class public interface abstract Lcom/intsig/camscanner/imagescanner/IImageProcessDelegate;
.super Ljava/lang/Object;
.source "ImageProcessDelegate.kt"


# annotations
.annotation runtime Lcom/intsig/okbinder/AIDL;
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# virtual methods
.method public abstract executeProgress(Ljava/lang/String;Lcom/intsig/camscanner/util/ImageProgressClient;Lcom/intsig/camscanner/scanner/pagescene/PageSceneResultCallback;Lcom/intsig/camscanner/image_progress/ImageProcessCallback;Lcom/intsig/camscanner/image_progress/ImageProgressListener;Lcom/intsig/camscanner/imagescanner/EraseMaskAllServerCallback;Lcom/intsig/camscanner/imagescanner/DeMoireWithTrimmedBackBack;Lcom/intsig/camscanner/imagescanner/SuperFilterImageCallback;Lcom/intsig/camscanner/imagescanner/CloudFilterImageCallback;)Lcom/intsig/camscanner/util/ImageProgressClient;
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/util/ImageProgressClient;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract scaleImage(Ljava/lang/String;IFILjava/lang/String;)I
.end method

.method public abstract setLogAgentDelegate(Lcom/intsig/log/LogAgentDelegate;)V
.end method

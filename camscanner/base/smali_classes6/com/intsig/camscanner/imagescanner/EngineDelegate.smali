.class public interface abstract Lcom/intsig/camscanner/imagescanner/EngineDelegate;
.super Ljava/lang/Object;
.source "EngineDelegate.kt"


# annotations
.annotation runtime Lcom/intsig/okbinder/AIDL;
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# virtual methods
.method public abstract adjustImageBrightness(Landroid/graphics/Bitmap;III)Landroid/graphics/Bitmap;
.end method

.method public abstract checkCropBounds([I[I)Z
    .param p1    # [I
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # [I
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract deBlurImage(Ljava/lang/String;Ljava/lang/String;)Z
.end method

.method public abstract decodeImage(Ljava/lang/String;)I
.end method

.method public abstract detectImageBorder(Ljava/lang/String;[I)[F
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract detectQualityLocal(Landroid/graphics/Bitmap;)I
.end method

.method public abstract detectorFinger(Ljava/lang/String;)Z
.end method

.method public abstract encodeImageWithFlexibleQuality(ILjava/lang/String;Z)I
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract enhanceBitmapByOnceTimeout(Landroid/graphics/Bitmap;IIZZ)Landroid/graphics/Bitmap;
.end method

.method public abstract enhanceBitmapTimeout(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;
.end method

.method public abstract findCandidateLines(Ljava/lang/String;ZLcom/intsig/camscanner/imagescanner/CandidateResultCallback;)V
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/imagescanner/CandidateResultCallback;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract getClassifyShadowType(Landroid/graphics/Bitmap;)I
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract handleRawImage(Ljava/lang/String;Lcom/intsig/camscanner/imagescanner/ImageStoreRequest;Lcom/intsig/camscanner/imagescanner/PageSceneCallback;Ljava/lang/String;IZZLjava/lang/String;Lcom/intsig/camscanner/imagescanner/EngineProcessListener;)Z
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/imagescanner/ImageStoreRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p9    # Lcom/intsig/camscanner/imagescanner/EngineProcessListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract recycleBitmap()V
.end method

.method public abstract release()V
.end method

.method public abstract releaseImage(I)V
.end method

.method public abstract reloadThumbStruct(Ljava/lang/String;)V
.end method

.method public abstract saveSuperFilterImageLocal(Ljava/lang/String;Ljava/lang/String;ZILcom/intsig/camscanner/imagescanner/EngineProcessListener;)Z
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # Lcom/intsig/camscanner/imagescanner/EngineProcessListener;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract setEnhanceTimeout(JJ)V
.end method

.method public abstract storeThumbToFile(Lcom/intsig/camscanner/imagescanner/ImageStoreRequest;Ljava/lang/String;Ljava/lang/String;I)Z
    .param p1    # Lcom/intsig/camscanner/imagescanner/ImageStoreRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract trimBitmap(Landroid/graphics/Bitmap;[IIIZ)Landroid/graphics/Bitmap;
    .param p2    # [I
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract trimImageFile(Ljava/lang/String;Ljava/lang/String;[I)I
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

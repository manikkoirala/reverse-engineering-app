.class public Lcom/intsig/camscanner/ocrapi/OcrLogical;
.super Ljava/lang/Object;
.source "OcrLogical.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/ocrapi/OcrLogical$OnOCRExceptionListener;,
        Lcom/intsig/camscanner/ocrapi/OcrLogical$OnOcrDataRefreshingListener;,
        Lcom/intsig/camscanner/ocrapi/OcrLogical$IEverSwitchedInterceptor;,
        Lcom/intsig/camscanner/ocrapi/OcrLogical$EverSwitchedInterceptor;
    }
.end annotation


# instance fields
.field private O8:Ljava/lang/String;

.field private Oo08:Z

.field private o〇0:Lcom/intsig/camscanner/ocrapi/OcrLogical$IEverSwitchedInterceptor;

.field private 〇080:Landroid/app/Activity;

.field private 〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

.field private 〇o〇:J


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroidx/fragment/app/FragmentManager;)V
    .locals 1

    const/4 v0, 0x1

    .line 1
    invoke-direct {p0, p1, p2, v0}, Lcom/intsig/camscanner/ocrapi/OcrLogical;-><init>(Landroid/app/Activity;Landroidx/fragment/app/FragmentManager;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Landroidx/fragment/app/FragmentManager;Z)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/intsig/camscanner/ocrapi/OcrLogical;->〇080:Landroid/app/Activity;

    .line 4
    iput-object p2, p0, Lcom/intsig/camscanner/ocrapi/OcrLogical;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 5
    iput-boolean p3, p0, Lcom/intsig/camscanner/ocrapi/OcrLogical;->Oo08:Z

    return-void
.end method

.method static bridge synthetic O8(Lcom/intsig/camscanner/ocrapi/OcrLogical;Lcom/intsig/camscanner/ocrapi/OcrModeChoosing$OnModeChoosingListener;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/ocrapi/OcrLogical;->Oooo8o0〇(Lcom/intsig/camscanner/ocrapi/OcrModeChoosing$OnModeChoosingListener;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private Oooo8o0〇(Lcom/intsig/camscanner/ocrapi/OcrModeChoosing$OnModeChoosingListener;)V
    .locals 4

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "mCloudOcrLeftNum = "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget-wide v1, p0, Lcom/intsig/camscanner/ocrapi/OcrLogical;->〇o〇:J

    .line 12
    .line 13
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "OcrLogical"

    .line 21
    .line 22
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    new-instance v0, Lcom/intsig/camscanner/ocrapi/OcrModeChoosing;

    .line 26
    .line 27
    iget-object v1, p0, Lcom/intsig/camscanner/ocrapi/OcrLogical;->〇080:Landroid/app/Activity;

    .line 28
    .line 29
    new-instance v2, Lcom/intsig/camscanner/ocrapi/OcrLogical$3;

    .line 30
    .line 31
    invoke-direct {v2, p0, p1}, Lcom/intsig/camscanner/ocrapi/OcrLogical$3;-><init>(Lcom/intsig/camscanner/ocrapi/OcrLogical;Lcom/intsig/camscanner/ocrapi/OcrModeChoosing$OnModeChoosingListener;)V

    .line 32
    .line 33
    .line 34
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/ocrapi/OcrModeChoosing;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/ocrapi/OcrModeChoosing$OnModeChoosingListener;)V

    .line 35
    .line 36
    .line 37
    iget-object p1, p0, Lcom/intsig/camscanner/ocrapi/OcrLogical;->O8:Ljava/lang/String;

    .line 38
    .line 39
    iget-wide v1, p0, Lcom/intsig/camscanner/ocrapi/OcrLogical;->〇o〇:J

    .line 40
    .line 41
    iget-boolean v3, p0, Lcom/intsig/camscanner/ocrapi/OcrLogical;->Oo08:Z

    .line 42
    .line 43
    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/intsig/camscanner/ocrapi/OcrModeChoosing;->〇o00〇〇Oo(Ljava/lang/String;JZ)V

    .line 44
    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private oO80(Lcom/intsig/camscanner/ocrapi/OcrLogical$OnOcrDataRefreshingListener;Lcom/intsig/camscanner/ocrapi/OcrModeChoosing$OnModeChoosingListener;Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/ocrapi/OcrLogical;->〇080:Landroid/app/Activity;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/util/Util;->O0(Landroid/content/Context;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x1

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    if-eqz p1, :cond_0

    .line 11
    .line 12
    new-instance v0, L〇o08/OO0o〇〇;

    .line 13
    .line 14
    invoke-direct {v0, p1, p2}, L〇o08/OO0o〇〇;-><init>(Lcom/intsig/camscanner/ocrapi/OcrLogical$OnOcrDataRefreshingListener;Lcom/intsig/camscanner/ocrapi/OcrModeChoosing$OnModeChoosingListener;)V

    .line 15
    .line 16
    .line 17
    const/4 p1, 0x0

    .line 18
    invoke-virtual {p0, v0, p1, v1, p3}, Lcom/intsig/camscanner/ocrapi/OcrLogical;->〇80〇808〇O(Lcom/intsig/camscanner/ocrapi/OcrLogical$OnOcrDataRefreshingListener;Lcom/intsig/camscanner/ocrapi/OcrLogical$OnOCRExceptionListener;ZLjava/lang/String;)V

    .line 19
    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    invoke-interface {p2, v1}, Lcom/intsig/camscanner/ocrapi/OcrModeChoosing$OnModeChoosingListener;->〇080(I)V

    .line 23
    .line 24
    .line 25
    :goto_0
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/ocrapi/OcrLogical$OnOcrDataRefreshingListener;Lcom/intsig/camscanner/ocrapi/OcrModeChoosing$OnModeChoosingListener;J)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/ocrapi/OcrLogical;->〇〇888(Lcom/intsig/camscanner/ocrapi/OcrLogical$OnOcrDataRefreshingListener;Lcom/intsig/camscanner/ocrapi/OcrModeChoosing$OnModeChoosingListener;J)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private 〇8o8o〇(Lcom/intsig/camscanner/ocrapi/OcrModeChoosing$OnModeChoosingListener;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/ocrapi/OcrLogical;->o〇0:Lcom/intsig/camscanner/ocrapi/OcrLogical$IEverSwitchedInterceptor;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/ocrapi/OcrLogical$EverSwitchedInterceptor;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/ocrapi/OcrLogical;->〇080:Landroid/app/Activity;

    .line 8
    .line 9
    invoke-direct {v0, p0, v1, p1}, Lcom/intsig/camscanner/ocrapi/OcrLogical$EverSwitchedInterceptor;-><init>(Lcom/intsig/camscanner/ocrapi/OcrLogical;Landroid/app/Activity;Lcom/intsig/camscanner/ocrapi/OcrModeChoosing$OnModeChoosingListener;)V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/ocrapi/OcrLogical;->o〇0:Lcom/intsig/camscanner/ocrapi/OcrLogical$IEverSwitchedInterceptor;

    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/ocrapi/OcrLogical;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/ocrapi/OcrLogical;->〇o〇:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/camscanner/ocrapi/OcrLogical;J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/ocrapi/OcrLogical;->〇o〇:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static synthetic 〇〇888(Lcom/intsig/camscanner/ocrapi/OcrLogical$OnOcrDataRefreshingListener;Lcom/intsig/camscanner/ocrapi/OcrModeChoosing$OnModeChoosingListener;J)V
    .locals 0

    .line 1
    invoke-interface {p0, p2, p3}, Lcom/intsig/camscanner/ocrapi/OcrLogical$OnOcrDataRefreshingListener;->〇080(J)V

    .line 2
    .line 3
    .line 4
    const/4 p0, 0x1

    .line 5
    invoke-interface {p1, p0}, Lcom/intsig/camscanner/ocrapi/OcrModeChoosing$OnModeChoosingListener;->〇080(I)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public OO0o〇〇(Landroid/app/Activity;Lcom/intsig/camscanner/ocrapi/ocrdialog/BaseOcrResultDialogFragment$OcrDialogCallback;)V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/ocrapi/ocrdialog/LoginForMoreTryDialogFragment;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/ocrapi/ocrdialog/LoginForMoreTryDialogFragment;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-static {}, Lcom/intsig/utils/LanguageUtil;->〇〇808〇()Z

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    if-eqz v1, :cond_0

    .line 11
    .line 12
    const/16 v1, 0xa

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v1, 0x4

    .line 16
    :goto_0
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/ocrapi/ocrdialog/LoginForMoreTryDialogFragment;->oO〇oo(I)V

    .line 17
    .line 18
    .line 19
    iget-object v1, p0, Lcom/intsig/camscanner/ocrapi/OcrLogical;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentManager;

    .line 20
    .line 21
    new-instance v2, Lcom/intsig/camscanner/ocrapi/OcrLogical$4;

    .line 22
    .line 23
    invoke-direct {v2, p0, p1, p2}, Lcom/intsig/camscanner/ocrapi/OcrLogical$4;-><init>(Lcom/intsig/camscanner/ocrapi/OcrLogical;Landroid/app/Activity;Lcom/intsig/camscanner/ocrapi/ocrdialog/BaseOcrResultDialogFragment$OcrDialogCallback;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/ocrapi/ocrdialog/BaseOcrResultDialogFragment;->〇O8oOo0(Landroidx/fragment/app/FragmentManager;Lcom/intsig/camscanner/ocrapi/ocrdialog/BaseOcrResultDialogFragment$OcrDialogCallback;)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public OO0o〇〇〇〇0(Lcom/intsig/camscanner/ocrapi/OcrLogical$OnOcrDataRefreshingListener;Z)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, p1, v0, p2, v0}, Lcom/intsig/camscanner/ocrapi/OcrLogical;->〇80〇808〇O(Lcom/intsig/camscanner/ocrapi/OcrLogical$OnOcrDataRefreshingListener;Lcom/intsig/camscanner/ocrapi/OcrLogical$OnOCRExceptionListener;ZLjava/lang/String;)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public Oo08(Ljava/lang/String;Lcom/intsig/camscanner/ocrapi/OcrLogical$OnOcrDataRefreshingListener;Lcom/intsig/camscanner/ocrapi/OcrModeChoosing$OnModeChoosingListener;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/ocrapi/OcrLogical;->O8:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    if-nez p1, :cond_0

    .line 8
    .line 9
    invoke-static {}, Lcom/intsig/camscanner/ocrapi/OcrStateSwitcher;->〇〇888()Z

    .line 10
    .line 11
    .line 12
    move-result p1

    .line 13
    if-eqz p1, :cond_0

    .line 14
    .line 15
    invoke-interface {p3}, Lcom/intsig/camscanner/ocrapi/OcrModeChoosing$OnModeChoosingListener;->〇o00〇〇Oo()V

    .line 16
    .line 17
    .line 18
    return-void

    .line 19
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/ocrapi/OcrStateSwitcher;->〇o00〇〇Oo()Z

    .line 20
    .line 21
    .line 22
    move-result p1

    .line 23
    if-eqz p1, :cond_1

    .line 24
    .line 25
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/ocrapi/OcrLogical;->〇8o8o〇(Lcom/intsig/camscanner/ocrapi/OcrModeChoosing$OnModeChoosingListener;)V

    .line 26
    .line 27
    .line 28
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/ocrapi/OcrStateSwitcher;->〇〇888()Z

    .line 29
    .line 30
    .line 31
    move-result p1

    .line 32
    if-eqz p1, :cond_2

    .line 33
    .line 34
    if-eqz p3, :cond_2

    .line 35
    .line 36
    invoke-direct {p0, p2, p3, p4}, Lcom/intsig/camscanner/ocrapi/OcrLogical;->oO80(Lcom/intsig/camscanner/ocrapi/OcrLogical$OnOcrDataRefreshingListener;Lcom/intsig/camscanner/ocrapi/OcrModeChoosing$OnModeChoosingListener;Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    return-void

    .line 40
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/ocrapi/OcrLogical;->〇080:Landroid/app/Activity;

    .line 41
    .line 42
    invoke-static {p1}, Lcom/intsig/camscanner/util/Util;->O0(Landroid/content/Context;)Z

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    if-eqz p1, :cond_3

    .line 47
    .line 48
    if-eqz p2, :cond_3

    .line 49
    .line 50
    new-instance p1, Lcom/intsig/camscanner/ocrapi/OcrLogical$1;

    .line 51
    .line 52
    invoke-direct {p1, p0, p2, p3}, Lcom/intsig/camscanner/ocrapi/OcrLogical$1;-><init>(Lcom/intsig/camscanner/ocrapi/OcrLogical;Lcom/intsig/camscanner/ocrapi/OcrLogical$OnOcrDataRefreshingListener;Lcom/intsig/camscanner/ocrapi/OcrModeChoosing$OnModeChoosingListener;)V

    .line 53
    .line 54
    .line 55
    const/4 p2, 0x0

    .line 56
    const/4 p3, 0x1

    .line 57
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/ocrapi/OcrLogical;->〇80〇808〇O(Lcom/intsig/camscanner/ocrapi/OcrLogical$OnOcrDataRefreshingListener;Lcom/intsig/camscanner/ocrapi/OcrLogical$OnOCRExceptionListener;ZLjava/lang/String;)V

    .line 58
    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_3
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/ocrapi/OcrLogical;->Oooo8o0〇(Lcom/intsig/camscanner/ocrapi/OcrModeChoosing$OnModeChoosingListener;)V

    .line 62
    .line 63
    .line 64
    :goto_0
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public o〇0()Lcom/intsig/camscanner/ocrapi/OcrLogical$IEverSwitchedInterceptor;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/ocrapi/OcrLogical;->o〇0:Lcom/intsig/camscanner/ocrapi/OcrLogical$IEverSwitchedInterceptor;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇80〇808〇O(Lcom/intsig/camscanner/ocrapi/OcrLogical$OnOcrDataRefreshingListener;Lcom/intsig/camscanner/ocrapi/OcrLogical$OnOCRExceptionListener;ZLjava/lang/String;)V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/ocrapi/OcrLogical;->〇080:Landroid/app/Activity;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const-string v0, "OcrLogical"

    .line 7
    .line 8
    const-string v1, "query userInfo"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    const-string v0, "points"

    .line 14
    .line 15
    const-string v1, "ocr_count"

    .line 16
    .line 17
    filled-new-array {v0, v1}, [Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v4

    .line 21
    new-instance v0, Lcom/intsig/camscanner/fundamental/net_tasks/QueryUserInfoTask;

    .line 22
    .line 23
    iget-object v3, p0, Lcom/intsig/camscanner/ocrapi/OcrLogical;->〇080:Landroid/app/Activity;

    .line 24
    .line 25
    new-instance v6, Lcom/intsig/camscanner/ocrapi/OcrLogical$2;

    .line 26
    .line 27
    invoke-direct {v6, p0, p1, p2}, Lcom/intsig/camscanner/ocrapi/OcrLogical$2;-><init>(Lcom/intsig/camscanner/ocrapi/OcrLogical;Lcom/intsig/camscanner/ocrapi/OcrLogical$OnOcrDataRefreshingListener;Lcom/intsig/camscanner/ocrapi/OcrLogical$OnOCRExceptionListener;)V

    .line 28
    .line 29
    .line 30
    move-object v2, v0

    .line 31
    move v5, p3

    .line 32
    move-object v7, p4

    .line 33
    invoke-direct/range {v2 .. v7}, Lcom/intsig/camscanner/fundamental/net_tasks/QueryUserInfoTask;-><init>(Landroid/app/Activity;[Ljava/lang/String;ZLcom/intsig/camscanner/fundamental/net_tasks/QueryUserInfoTask$OnQueryInfoListener;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    invoke-static {}, Lcom/intsig/utils/CustomExecutor;->oo88o8O()Ljava/util/concurrent/ExecutorService;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    const/4 p2, 0x0

    .line 41
    new-array p2, p2, [Ljava/lang/Void;

    .line 42
    .line 43
    invoke-virtual {v0, p1, p2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 44
    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public 〇O8o08O(Landroid/app/Activity;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    invoke-virtual {p0, p1, v0}, Lcom/intsig/camscanner/ocrapi/OcrLogical;->OO0o〇〇(Landroid/app/Activity;Lcom/intsig/camscanner/ocrapi/ocrdialog/BaseOcrResultDialogFragment$OcrDialogCallback;)V

    .line 3
    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

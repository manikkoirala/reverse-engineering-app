.class public Lcom/intsig/camscanner/ocrapi/CommonInfo;
.super Ljava/lang/Object;
.source "CommonInfo.java"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field private listener:Landroid/view/View$OnClickListener;

.field private nearTextImageId:I

.field private rightImageId:I

.field private text:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;IILandroid/view/View$OnClickListener;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/intsig/camscanner/ocrapi/CommonInfo;->text:Ljava/lang/String;

    .line 4
    iput p2, p0, Lcom/intsig/camscanner/ocrapi/CommonInfo;->nearTextImageId:I

    .line 5
    iput p3, p0, Lcom/intsig/camscanner/ocrapi/CommonInfo;->rightImageId:I

    .line 6
    iput-object p4, p0, Lcom/intsig/camscanner/ocrapi/CommonInfo;->listener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 1

    const/4 v0, -0x1

    .line 1
    invoke-direct {p0, p1, v0, v0, p2}, Lcom/intsig/camscanner/ocrapi/CommonInfo;-><init>(Ljava/lang/String;IILandroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public getListener()Landroid/view/View$OnClickListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/ocrapi/CommonInfo;->listener:Landroid/view/View$OnClickListener;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getNearTextImageId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/ocrapi/CommonInfo;->nearTextImageId:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getRightImageId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/ocrapi/CommonInfo;->rightImageId:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/ocrapi/CommonInfo;->text:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public setListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/ocrapi/CommonInfo;->listener:Landroid/view/View$OnClickListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setNearTextImageId(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/ocrapi/CommonInfo;->nearTextImageId:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setRightImageId(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/ocrapi/CommonInfo;->rightImageId:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public setText(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/ocrapi/CommonInfo;->text:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

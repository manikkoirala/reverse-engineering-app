.class public Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil;
.super Ljava/lang/Object;
.source "MultipleFunctionDisplayForTextUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;
    }
.end annotation


# static fields
.field private static O8:Ljava/util/regex/Pattern;

.field private static 〇080:Ljava/util/regex/Pattern;

.field private static 〇o00〇〇Oo:Ljava/util/regex/Pattern;

.field private static 〇o〇:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    const-string v0, "\\d{8,14}"

    .line 2
    .line 3
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sput-object v0, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil;->〇080:Ljava/util/regex/Pattern;

    .line 8
    .line 9
    const-string v0, "\\d{3,4}-\\d{3,8}"

    .line 10
    .line 11
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    sput-object v0, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil;->〇o00〇〇Oo:Ljava/util/regex/Pattern;

    .line 16
    .line 17
    const-string v0, "\\d{3,4}-\\d{3,8}-\\d{3,4}"

    .line 18
    .line 19
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    sput-object v0, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil;->〇o〇:Ljava/util/regex/Pattern;

    .line 24
    .line 25
    const-string v0, "\\d{5,}"

    .line 26
    .line 27
    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    sput-object v0, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil;->O8:Ljava/util/regex/Pattern;

    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static O8(Landroid/widget/TextView;)V
    .locals 2

    .line 1
    const-string v0, "tel"

    .line 2
    .line 3
    const/16 v1, 0xf

    .line 4
    .line 5
    :try_start_0
    invoke-static {p0, v1}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    .line 6
    .line 7
    .line 8
    sget-object v1, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil;->〇080:Ljava/util/regex/Pattern;

    .line 9
    .line 10
    invoke-static {p0, v1, v0}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    sget-object v1, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil;->〇o00〇〇Oo:Ljava/util/regex/Pattern;

    .line 14
    .line 15
    invoke-static {p0, v1, v0}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    sget-object v1, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil;->〇o〇:Ljava/util/regex/Pattern;

    .line 19
    .line 20
    invoke-static {p0, v1, v0}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    sget-object v0, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil;->O8:Ljava/util/regex/Pattern;

    .line 24
    .line 25
    const-string v1, "digital"

    .line 26
    .line 27
    invoke-static {p0, v0, v1}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    invoke-static {v0, p0}, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil;->OO0o〇〇〇〇0(Landroid/content/Context;Landroid/widget/TextView;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 35
    .line 36
    .line 37
    goto :goto_0

    .line 38
    :catch_0
    move-exception p0

    .line 39
    const-string v0, "MultipleFunctionDisplayForTextUtil"

    .line 40
    .line 41
    invoke-static {v0, p0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 42
    .line 43
    .line 44
    :goto_0
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private static OO0o〇〇〇〇0(Landroid/content/Context;Landroid/widget/TextView;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-lez v1, :cond_2

    .line 12
    .line 13
    instance-of v1, v0, Landroid/text/Spannable;

    .line 14
    .line 15
    if-eqz v1, :cond_2

    .line 16
    .line 17
    check-cast v0, Landroid/text/Spannable;

    .line 18
    .line 19
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    const-class v2, Landroid/text/style/URLSpan;

    .line 24
    .line 25
    const/4 v3, 0x0

    .line 26
    invoke-interface {v0, v3, v1, v2}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    check-cast v1, [Landroid/text/style/URLSpan;

    .line 31
    .line 32
    if-eqz v1, :cond_1

    .line 33
    .line 34
    array-length v2, v1

    .line 35
    if-nez v2, :cond_0

    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_0
    invoke-static {v1, v0}, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil;->〇o〇([Landroid/text/style/URLSpan;Landroid/text/Spannable;)Ljava/util/List;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    invoke-static {v1}, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil;->〇80〇808〇O(Ljava/util/List;)V

    .line 43
    .line 44
    .line 45
    invoke-static {p0, v0, v1}, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil;->〇〇888(Landroid/content/Context;Landroid/text/Spannable;Ljava/util/List;)Landroid/text/SpannableString;

    .line 46
    .line 47
    .line 48
    move-result-object p0

    .line 49
    invoke-virtual {p1, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 50
    .line 51
    .line 52
    goto :goto_1

    .line 53
    :cond_1
    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    .line 55
    .line 56
    :cond_2
    :goto_1
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private static synthetic Oo08(Landroid/content/Context;Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p2, "CSOcr"

    .line 2
    .line 3
    const-string v0, "blue_click"

    .line 4
    .line 5
    invoke-static {p2, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance p2, Lcom/intsig/camscanner/ocrapi/TextJumpToControl;

    .line 9
    .line 10
    invoke-direct {p2}, Lcom/intsig/camscanner/ocrapi/TextJumpToControl;-><init>()V

    .line 11
    .line 12
    .line 13
    iget-object v0, p1, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;->〇080:Ljava/lang/String;

    .line 14
    .line 15
    iget-object p1, p1, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;->〇o00〇〇Oo:Ljava/lang/CharSequence;

    .line 16
    .line 17
    invoke-virtual {p2, p0, v0, p1}, Lcom/intsig/camscanner/ocrapi/TextJumpToControl;->〇〇888(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static oO80(Landroid/widget/TextView;)V
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-lez v1, :cond_2

    .line 12
    .line 13
    instance-of v1, v0, Landroid/text/Spannable;

    .line 14
    .line 15
    if-eqz v1, :cond_2

    .line 16
    .line 17
    check-cast v0, Landroid/text/Spannable;

    .line 18
    .line 19
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 20
    .line 21
    .line 22
    move-result v1

    .line 23
    const-class v2, Landroid/text/style/CharacterStyle;

    .line 24
    .line 25
    const/4 v3, 0x0

    .line 26
    invoke-interface {v0, v3, v1, v2}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    check-cast v1, [Landroid/text/style/CharacterStyle;

    .line 31
    .line 32
    if-eqz v1, :cond_2

    .line 33
    .line 34
    array-length v2, v1

    .line 35
    if-nez v2, :cond_0

    .line 36
    .line 37
    goto :goto_1

    .line 38
    :cond_0
    array-length v2, v1

    .line 39
    :goto_0
    if-ge v3, v2, :cond_1

    .line 40
    .line 41
    aget-object v4, v1, v3

    .line 42
    .line 43
    invoke-interface {v0, v4}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 44
    .line 45
    .line 46
    add-int/lit8 v3, v3, 0x1

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_1
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setTextKeepState(Ljava/lang/CharSequence;)V

    .line 50
    .line 51
    .line 52
    nop

    .line 53
    :cond_2
    :goto_1
    return-void
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private static synthetic o〇0(Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;)I
    .locals 2

    .line 1
    iget v0, p1, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;->〇o〇:I

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;->〇o〇:I

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    iget p1, p1, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;->O8:I

    .line 8
    .line 9
    iget p0, p0, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;->O8:I

    .line 10
    .line 11
    sub-int/2addr p1, p0

    .line 12
    return p1

    .line 13
    :cond_0
    iget p1, p1, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;->O8:I

    .line 14
    .line 15
    iget p0, p0, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;->O8:I

    .line 16
    .line 17
    if-ne p1, p0, :cond_1

    .line 18
    .line 19
    sub-int/2addr v0, v1

    .line 20
    return v0

    .line 21
    :cond_1
    add-int/2addr v0, p1

    .line 22
    add-int/2addr v1, p0

    .line 23
    sub-int/2addr v0, v1

    .line 24
    return v0
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇080(Landroid/content/Context;Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil;->Oo08(Landroid/content/Context;Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private static 〇80〇808〇O(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;",
            ">;)V"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/ocrapi/〇o00〇〇Oo;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/ocrapi/〇o00〇〇Oo;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-static {p0, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;)I
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil;->o〇0(Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;)I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static 〇o〇([Landroid/text/style/URLSpan;Landroid/text/Spannable;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Landroid/text/style/URLSpan;",
            "Landroid/text/Spannable;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    array-length v1, p0

    .line 7
    const/4 v2, 0x0

    .line 8
    :goto_0
    if-ge v2, v1, :cond_1

    .line 9
    .line 10
    aget-object v3, p0, v2

    .line 11
    .line 12
    invoke-interface {p1, v3}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    .line 13
    .line 14
    .line 15
    move-result v4

    .line 16
    invoke-interface {p1, v3}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    .line 17
    .line 18
    .line 19
    move-result v5

    .line 20
    if-ltz v4, :cond_0

    .line 21
    .line 22
    if-ltz v5, :cond_0

    .line 23
    .line 24
    new-instance v6, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;

    .line 25
    .line 26
    invoke-virtual {v3}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v7

    .line 30
    invoke-interface {p1, v4, v5}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    .line 31
    .line 32
    .line 33
    move-result-object v8

    .line 34
    invoke-direct {v6, v7, v8, v4, v5}, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;II)V

    .line 35
    .line 36
    .line 37
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 38
    .line 39
    .line 40
    goto :goto_1

    .line 41
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    .line 42
    .line 43
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 44
    .line 45
    .line 46
    const-string v7, "getSpanDataList start:"

    .line 47
    .line 48
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    .line 50
    .line 51
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    const-string v4, " end:"

    .line 55
    .line 56
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    .line 58
    .line 59
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    const-string v4, " span\uff1a"

    .line 63
    .line 64
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v4

    .line 74
    const-string v5, "MultipleFunctionDisplayForTextUtil"

    .line 75
    .line 76
    invoke-static {v5, v4}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    :goto_1
    invoke-interface {p1, v3}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 80
    .line 81
    .line 82
    add-int/lit8 v2, v2, 0x1

    .line 83
    .line 84
    goto :goto_0

    .line 85
    :cond_1
    return-object v0
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private static 〇〇888(Landroid/content/Context;Landroid/text/Spannable;Ljava/util/List;)Landroid/text/SpannableString;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/text/Spannable;",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;",
            ">;)",
            "Landroid/text/SpannableString;"
        }
    .end annotation

    .line 1
    new-instance v0, Landroid/text/SpannableString;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    const/4 p2, -0x1

    .line 11
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    if-eqz v1, :cond_3

    .line 16
    .line 17
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    check-cast v1, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;

    .line 22
    .line 23
    iget v2, v1, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;->〇o〇:I

    .line 24
    .line 25
    if-ne p2, v2, :cond_1

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_1
    if-ltz v2, :cond_0

    .line 29
    .line 30
    iget v3, v1, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;->O8:I

    .line 31
    .line 32
    if-gez v3, :cond_2

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_2
    new-instance p2, Lcom/intsig/span/CustomClickableURLSpan;

    .line 36
    .line 37
    iget-object v3, v1, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;->〇080:Ljava/lang/String;

    .line 38
    .line 39
    new-instance v4, Lcom/intsig/camscanner/ocrapi/〇080;

    .line 40
    .line 41
    invoke-direct {v4, p0, v1}, Lcom/intsig/camscanner/ocrapi/〇080;-><init>(Landroid/content/Context;Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;)V

    .line 42
    .line 43
    .line 44
    invoke-direct {p2, v3, v4}, Lcom/intsig/span/CustomClickableURLSpan;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 45
    .line 46
    .line 47
    iget v3, v1, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;->〇o〇:I

    .line 48
    .line 49
    iget v1, v1, Lcom/intsig/camscanner/ocrapi/MultipleFunctionDisplayForTextUtil$SpanData;->O8:I

    .line 50
    .line 51
    const/16 v4, 0x21

    .line 52
    .line 53
    invoke-virtual {v0, p2, v3, v1, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 54
    .line 55
    .line 56
    move p2, v2

    .line 57
    goto :goto_0

    .line 58
    :cond_3
    return-object v0
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

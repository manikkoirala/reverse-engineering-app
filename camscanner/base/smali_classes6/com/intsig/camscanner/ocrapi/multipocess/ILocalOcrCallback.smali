.class public interface abstract Lcom/intsig/camscanner/ocrapi/multipocess/ILocalOcrCallback;
.super Ljava/lang/Object;
.source "ILocalOcrCallback.kt"


# annotations
.annotation runtime Lcom/intsig/okbinder/AIDL;
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# virtual methods
.method public abstract recognizePage(JLjava/lang/String;[I[IIILcom/intsig/camscanner/ocrapi/multipocess/OcrClassifyCallback;)Ljava/lang/String;
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p5    # [I
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract releaseOcrModel()V
.end method

.method public abstract setLogAgentDelegate(Lcom/intsig/log/LogAgentDelegate;)V
.end method

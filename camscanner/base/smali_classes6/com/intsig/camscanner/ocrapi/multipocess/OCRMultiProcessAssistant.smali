.class public final Lcom/intsig/camscanner/ocrapi/multipocess/OCRMultiProcessAssistant;
.super Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;
.source "OCRMultiProcessAssistant.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/ocrapi/multipocess/OCRMultiProcessAssistant$Companion;,
        Lcom/intsig/camscanner/ocrapi/multipocess/OCRMultiProcessAssistant$OCRMultiProcessAssistantImpl;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant<",
        "Lcom/intsig/camscanner/ocrapi/multipocess/ILocalOcrCallback;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇8o8o〇:Lcom/intsig/camscanner/ocrapi/multipocess/OCRMultiProcessAssistant$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇O8o08O:Lcom/intsig/camscanner/ocrapi/multipocess/OCRMultiProcessAssistant;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/ocrapi/multipocess/OCRMultiProcessAssistant$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/ocrapi/multipocess/OCRMultiProcessAssistant$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/ocrapi/multipocess/OCRMultiProcessAssistant;->〇8o8o〇:Lcom/intsig/camscanner/ocrapi/multipocess/OCRMultiProcessAssistant$Companion;

    .line 8
    .line 9
    sget-object v0, Lcom/intsig/camscanner/ocrapi/multipocess/OCRMultiProcessAssistant$OCRMultiProcessAssistantImpl;->〇080:Lcom/intsig/camscanner/ocrapi/multipocess/OCRMultiProcessAssistant$OCRMultiProcessAssistantImpl$Companion;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/ocrapi/multipocess/OCRMultiProcessAssistant$OCRMultiProcessAssistantImpl$Companion;->〇080()Lcom/intsig/camscanner/ocrapi/multipocess/OCRMultiProcessAssistant;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    sput-object v0, Lcom/intsig/camscanner/ocrapi/multipocess/OCRMultiProcessAssistant;->〇O8o08O:Lcom/intsig/camscanner/ocrapi/multipocess/OCRMultiProcessAssistant;

    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private constructor <init>()V
    .locals 3

    .line 2
    const-class v0, Lcom/intsig/camscanner/ocrapi/multipocess/LocalOcrService;

    .line 3
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "LocalOcrService::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v1, Lcom/intsig/camscanner/ocrapi/multipocess/ILocalOcrCallback;

    const-string v2, "OCRMultiProcessAssistant"

    .line 4
    invoke-direct {p0, v0, v1, v2}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;-><init>(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    .line 5
    new-instance v0, Lcom/intsig/camscanner/ocrapi/multipocess/OCRMultiProcessAssistant$1;

    invoke-direct {v0, p0}, Lcom/intsig/camscanner/ocrapi/multipocess/OCRMultiProcessAssistant$1;-><init>(Lcom/intsig/camscanner/ocrapi/multipocess/OCRMultiProcessAssistant;)V

    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->〇8o8o〇(Lcom/intsig/camscanner/multiprocess/MultiProcessConnectListener;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/ocrapi/multipocess/OCRMultiProcessAssistant;-><init>()V

    return-void
.end method

.method public static final synthetic 〇〇808〇()Lcom/intsig/camscanner/ocrapi/multipocess/OCRMultiProcessAssistant;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/ocrapi/multipocess/OCRMultiProcessAssistant;->〇O8o08O:Lcom/intsig/camscanner/ocrapi/multipocess/OCRMultiProcessAssistant;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final 〇O〇(Ljava/lang/String;)Z
    .locals 8

    .line 1
    sget-object v0, Lcom/intsig/utils/MultiProcessProvider;->OO:Lcom/intsig/utils/MultiProcessProvider$Companion;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    const-string v3, "key_crash_ocr_engine_process"

    .line 10
    .line 11
    const/4 v4, 0x0

    .line 12
    invoke-virtual {v0, v2, v3, v4}, Lcom/intsig/utils/MultiProcessProvider$Companion;->〇o00〇〇Oo(Landroid/content/Context;Ljava/lang/String;Z)Z

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    if-eqz v2, :cond_3

    .line 17
    .line 18
    new-instance v5, Lorg/json/JSONObject;

    .line 19
    .line 20
    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 21
    .line 22
    .line 23
    const-string v6, "crash_reason"

    .line 24
    .line 25
    const-string v7, "ocr_engine_native_unusual"

    .line 26
    .line 27
    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 28
    .line 29
    .line 30
    if-eqz p1, :cond_1

    .line 31
    .line 32
    invoke-static {p1}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 33
    .line 34
    .line 35
    move-result v6

    .line 36
    if-eqz v6, :cond_0

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    const/4 v6, 0x0

    .line 40
    goto :goto_1

    .line 41
    :cond_1
    :goto_0
    const/4 v6, 0x1

    .line 42
    :goto_1
    if-nez v6, :cond_2

    .line 43
    .line 44
    const-string v6, "crash_msg"

    .line 45
    .line 46
    invoke-virtual {v5, v6, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 47
    .line 48
    .line 49
    :cond_2
    const-string p1, "CSDevelopmentTool"

    .line 50
    .line 51
    const-string v6, "crash"

    .line 52
    .line 53
    invoke-static {p1, v6, v5}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 54
    .line 55
    .line 56
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 57
    .line 58
    .line 59
    move-result-object p1

    .line 60
    invoke-virtual {v0, p1, v3, v4}, Lcom/intsig/utils/MultiProcessProvider$Companion;->Oo08(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 61
    .line 62
    .line 63
    :cond_3
    return v2
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.class public interface abstract Lcom/intsig/camscanner/ocrapi/multipocess/OcrClassifyCallback;
.super Ljava/lang/Object;
.source "OcrClassifyCallback.kt"


# annotations
.annotation runtime Lcom/intsig/okbinder/AIDL;
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# virtual methods
.method public abstract checkModelFile()Z
.end method

.method public abstract checkTitle(Ljava/lang/String;Lcom/intsig/camscanner/mainmenu/SensitiveWordsChecker$CheckSensitiveCallback;)V
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract getCloudOcrTitle(ILjava/lang/String;Lcom/intsig/camscanner/ocrapi/rename/OCRTitleCloudClient$Callback;)V
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/ocrapi/rename/OCRTitleCloudClient$Callback;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract onCallback(Ljava/lang/String;)V
.end method

.method public abstract updateTitle(JLjava/lang/String;I)V
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

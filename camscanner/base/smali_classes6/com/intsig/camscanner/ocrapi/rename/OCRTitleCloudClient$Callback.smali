.class public interface abstract Lcom/intsig/camscanner/ocrapi/rename/OCRTitleCloudClient$Callback;
.super Ljava/lang/Object;
.source "OCRTitleCloudClient.kt"


# annotations
.annotation runtime Lcom/intsig/okbinder/AIDL;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/ocrapi/rename/OCRTitleCloudClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Callback"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# virtual methods
.method public abstract onFail(Ljava/lang/String;)V
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract onSuccess(Ljava/lang/String;)V
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

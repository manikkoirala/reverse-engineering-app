.class public final Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;
.super Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;
.source "ImageMultiProcessAssistant.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant$Companion;,
        Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant$ImageMultiProcessAssistantImpl;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant<",
        "Lcom/intsig/camscanner/imagescanner/IImageProcessDelegate;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇8o8o〇:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇O8o08O:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;->〇8o8o〇:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant$Companion;

    .line 8
    .line 9
    sget-object v0, Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant$ImageMultiProcessAssistantImpl;->〇080:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant$ImageMultiProcessAssistantImpl$Companion;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant$ImageMultiProcessAssistantImpl$Companion;->〇080()Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    sput-object v0, Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;->〇O8o08O:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private constructor <init>()V
    .locals 3

    .line 2
    const-class v0, Lcom/intsig/camscanner/imagescanner/ImageProcessService;

    .line 3
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ImageProcessService::class.java.name"

    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v1, Lcom/intsig/camscanner/imagescanner/IImageProcessDelegate;

    const-string v2, "ImageProcessAssistant"

    .line 4
    invoke-direct {p0, v0, v1, v2}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;-><init>(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V

    .line 5
    new-instance v0, Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant$1;

    invoke-direct {v0, p0}, Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant$1;-><init>(Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;)V

    invoke-virtual {p0, v0}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->〇8o8o〇(Lcom/intsig/camscanner/multiprocess/MultiProcessConnectListener;)V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;-><init>()V

    return-void
.end method

.method public static final synthetic 〇〇808〇()Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;->〇O8o08O:Lcom/intsig/camscanner/multiprocess/ImageMultiProcessAssistant;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final 〇O〇()Z
    .locals 9

    .line 1
    sget-object v0, Lcom/intsig/utils/MultiProcessProvider;->OO:Lcom/intsig/utils/MultiProcessProvider$Companion;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 4
    .line 5
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    const-string v3, "key_crash_batch_engine_process"

    .line 10
    .line 11
    const/4 v4, 0x0

    .line 12
    invoke-virtual {v0, v2, v3, v4}, Lcom/intsig/utils/MultiProcessProvider$Companion;->〇o00〇〇Oo(Landroid/content/Context;Ljava/lang/String;Z)Z

    .line 13
    .line 14
    .line 15
    move-result v2

    .line 16
    if-eqz v2, :cond_0

    .line 17
    .line 18
    const/4 v5, 0x1

    .line 19
    new-array v5, v5, [Landroid/util/Pair;

    .line 20
    .line 21
    new-instance v6, Landroid/util/Pair;

    .line 22
    .line 23
    const-string v7, "crash_reason"

    .line 24
    .line 25
    const-string v8, "batch_engine_native_unusual"

    .line 26
    .line 27
    invoke-direct {v6, v7, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 28
    .line 29
    .line 30
    aput-object v6, v5, v4

    .line 31
    .line 32
    const-string v6, "CSDevelopmentTool"

    .line 33
    .line 34
    const-string v7, "crash"

    .line 35
    .line 36
    invoke-static {v6, v7, v5}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    invoke-virtual {v0, v1, v3, v4}, Lcom/intsig/utils/MultiProcessProvider$Companion;->Oo08(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 44
    .line 45
    .line 46
    :cond_0
    return v2
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

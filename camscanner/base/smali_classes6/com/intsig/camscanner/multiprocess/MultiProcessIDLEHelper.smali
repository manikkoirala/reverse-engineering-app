.class public final Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;
.super Ljava/lang/Object;
.source "MultiProcessIDLEHelper.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇〇888:Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8:Landroid/os/Handler$Callback;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private Oo08:Landroid/os/HandlerThread;

.field private o〇0:Landroid/os/Handler;

.field private final 〇080:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o00〇〇Oo:Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEListener;

.field private volatile 〇o〇:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->〇〇888:Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "handlerThreadName"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->〇080:Ljava/lang/String;

    .line 10
    .line 11
    new-instance p1, L〇80O8o8O〇/〇080;

    .line 12
    .line 13
    invoke-direct {p1, p0}, L〇80O8o8O〇/〇080;-><init>(Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;)V

    .line 14
    .line 15
    .line 16
    iput-object p1, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->O8:Landroid/os/Handler$Callback;

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
.end method

.method private static final o〇0(Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;Landroid/os/Message;)Z
    .locals 5

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "msg"

    .line 7
    .line 8
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget p1, p1, Landroid/os/Message;->what:I

    .line 12
    .line 13
    const/4 v0, 0x1

    .line 14
    const-string v1, "MultiProcessIDEHelper"

    .line 15
    .line 16
    if-eqz p1, :cond_2

    .line 17
    .line 18
    const/4 v2, 0x2

    .line 19
    const/4 v3, 0x0

    .line 20
    if-eq p1, v2, :cond_1

    .line 21
    .line 22
    const/4 v2, 0x3

    .line 23
    if-eq p1, v2, :cond_0

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    iget p1, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->〇o〇:I

    .line 27
    .line 28
    new-instance v2, Ljava/lang/StringBuilder;

    .line 29
    .line 30
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 31
    .line 32
    .line 33
    const-string v4, "MSG_RELEASE_PROCESS connectTimes:"

    .line 34
    .line 35
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    iget p1, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->〇o〇:I

    .line 49
    .line 50
    add-int/lit8 p1, p1, -0x1

    .line 51
    .line 52
    iput p1, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->〇o〇:I

    .line 53
    .line 54
    iget p1, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->〇o〇:I

    .line 55
    .line 56
    if-gtz p1, :cond_3

    .line 57
    .line 58
    iput v3, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->〇o〇:I

    .line 59
    .line 60
    iget-object p0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->o〇0:Landroid/os/Handler;

    .line 61
    .line 62
    if-eqz p0, :cond_3

    .line 63
    .line 64
    const-wide/32 v1, 0x2bf20

    .line 65
    .line 66
    .line 67
    invoke-virtual {p0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 68
    .line 69
    .line 70
    goto :goto_0

    .line 71
    :cond_1
    iget p1, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->〇o〇:I

    .line 72
    .line 73
    new-instance v2, Ljava/lang/StringBuilder;

    .line 74
    .line 75
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 76
    .line 77
    .line 78
    const-string v4, "MSG_USE_PROCESS connectTimes:"

    .line 79
    .line 80
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    .line 82
    .line 83
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object p1

    .line 90
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    .line 92
    .line 93
    iget p1, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->〇o〇:I

    .line 94
    .line 95
    add-int/2addr p1, v0

    .line 96
    iput p1, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->〇o〇:I

    .line 97
    .line 98
    iget-object p0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->o〇0:Landroid/os/Handler;

    .line 99
    .line 100
    if-eqz p0, :cond_3

    .line 101
    .line 102
    invoke-virtual {p0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 103
    .line 104
    .line 105
    goto :goto_0

    .line 106
    :cond_2
    iget p1, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->〇o〇:I

    .line 107
    .line 108
    new-instance v2, Ljava/lang/StringBuilder;

    .line 109
    .line 110
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 111
    .line 112
    .line 113
    const-string v3, "MSG_IDLE connectTimes:"

    .line 114
    .line 115
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    .line 117
    .line 118
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 119
    .line 120
    .line 121
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 122
    .line 123
    .line 124
    move-result-object p1

    .line 125
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    .line 127
    .line 128
    iget p1, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->〇o〇:I

    .line 129
    .line 130
    if-gtz p1, :cond_3

    .line 131
    .line 132
    iget-object p0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->〇o00〇〇Oo:Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEListener;

    .line 133
    .line 134
    if-eqz p0, :cond_3

    .line 135
    .line 136
    invoke-interface {p0}, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEListener;->unbindService()V

    .line 137
    .line 138
    .line 139
    :cond_3
    :goto_0
    return v0
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;Landroid/os/Message;)Z
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->o〇0(Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;Landroid/os/Message;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public final O8(Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->〇o00〇〇Oo:Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final Oo08(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->o〇0:Landroid/os/Handler;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    const/4 p1, 0x2

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    const/4 p1, 0x3

    .line 10
    :goto_0
    invoke-virtual {v0, p1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 11
    .line 12
    .line 13
    :cond_1
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇o00〇〇Oo()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->Oo08:Landroid/os/HandlerThread;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->o〇0:Landroid/os/Handler;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    new-instance v0, Landroid/os/HandlerThread;

    .line 11
    .line 12
    iget-object v1, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->〇080:Ljava/lang/String;

    .line 13
    .line 14
    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 18
    .line 19
    .line 20
    new-instance v1, Landroid/os/Handler;

    .line 21
    .line 22
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    .line 23
    .line 24
    .line 25
    move-result-object v2

    .line 26
    iget-object v3, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->O8:Landroid/os/Handler$Callback;

    .line 27
    .line 28
    invoke-direct {v1, v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    .line 29
    .line 30
    .line 31
    iput-object v1, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->o〇0:Landroid/os/Handler;

    .line 32
    .line 33
    iput-object v0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->Oo08:Landroid/os/HandlerThread;

    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final 〇o〇()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    iput v0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->〇o〇:I

    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;
.super Ljava/lang/Object;
.source "MultiProcessAssistant.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant$Companion;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final OO0o〇〇〇〇0:Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O8:Lkotlinx/coroutines/CoroutineScope;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private volatile Oo08:Lcom/intsig/okbinder/ServerInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/okbinder/ServerInfo<",
            "TT;>;"
        }
    .end annotation
.end field

.field private oO80:Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;

.field private o〇0:Z

.field private final 〇080:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇80〇808〇O:Lcom/intsig/camscanner/multiprocess/MultiProcessConnectListener;

.field private final 〇o00〇〇Oo:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "*>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o〇:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇〇888:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Class;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    const-string v0, "serviceName"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "binderClass"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "handlerThreadName"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    .line 18
    .line 19
    iput-object p1, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->〇080:Ljava/lang/String;

    .line 20
    .line 21
    iput-object p2, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->〇o00〇〇Oo:Ljava/lang/Class;

    .line 22
    .line 23
    iput-object p3, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->〇o〇:Ljava/lang/String;

    .line 24
    .line 25
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    invoke-static {p1}, Lkotlinx/coroutines/CoroutineScopeKt;->〇080(Lkotlin/coroutines/CoroutineContext;)Lkotlinx/coroutines/CoroutineScope;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    iput-object p1, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->O8:Lkotlinx/coroutines/CoroutineScope;

    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static final synthetic O8(Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->OO0o〇〇〇〇0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final OO0o〇〇〇〇0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->oO80:Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;

    .line 6
    .line 7
    iget-object v1, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->〇o〇:Ljava/lang/String;

    .line 8
    .line 9
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;-><init>(Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->oO80:Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;

    .line 13
    .line 14
    new-instance v1, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant$initMultiProcessIDLEHelper$1;

    .line 15
    .line 16
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant$initMultiProcessIDLEHelper$1;-><init>(Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->O8(Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEListener;)V

    .line 20
    .line 21
    .line 22
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->oO80:Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;

    .line 23
    .line 24
    if-eqz v0, :cond_1

    .line 25
    .line 26
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->〇o00〇〇Oo()V

    .line 27
    .line 28
    .line 29
    :cond_1
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic Oo08(Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->o〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic o〇0(Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->〇〇888:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇080(Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;)Ljava/lang/Class;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->〇o00〇〇Oo:Ljava/lang/Class;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;)Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->oO80:Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->〇080:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public final OO0o〇〇()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->Oo08:Lcom/intsig/okbinder/ServerInfo;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/okbinder/ServerInfo;->〇080()Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    move-object v0, v1

    .line 12
    :goto_0
    if-nez v0, :cond_1

    .line 13
    .line 14
    iget-boolean v0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->〇〇888:Z

    .line 15
    .line 16
    if-nez v0, :cond_1

    .line 17
    .line 18
    const/4 v0, 0x1

    .line 19
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->〇〇888:Z

    .line 20
    .line 21
    const/4 v0, 0x0

    .line 22
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->o〇0:Z

    .line 23
    .line 24
    iget-object v2, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->O8:Lkotlinx/coroutines/CoroutineScope;

    .line 25
    .line 26
    const/4 v3, 0x0

    .line 27
    const/4 v4, 0x0

    .line 28
    new-instance v5, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant$tryConnectChildProcess$1;

    .line 29
    .line 30
    invoke-direct {v5, p0, v1}, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant$tryConnectChildProcess$1;-><init>(Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;Lkotlin/coroutines/Continuation;)V

    .line 31
    .line 32
    .line 33
    const/4 v6, 0x3

    .line 34
    const/4 v7, 0x0

    .line 35
    invoke-static/range {v2 .. v7}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 36
    .line 37
    .line 38
    :cond_1
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final Oooo8o0〇(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->oO80:Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->Oo08(Z)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final oO80()Lcom/intsig/camscanner/multiprocess/MultiProcessConnectListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->〇80〇808〇O:Lcom/intsig/camscanner/multiprocess/MultiProcessConnectListener;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇80〇808〇O()Lcom/intsig/okbinder/ServerInfo;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/intsig/okbinder/ServerInfo<",
            "TT;>;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->Oo08:Lcom/intsig/okbinder/ServerInfo;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇8o8o〇(Lcom/intsig/camscanner/multiprocess/MultiProcessConnectListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->〇80〇808〇O:Lcom/intsig/camscanner/multiprocess/MultiProcessConnectListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇O8o08O(Lcom/intsig/okbinder/ServerInfo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/okbinder/ServerInfo<",
            "TT;>;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->Oo08:Lcom/intsig/okbinder/ServerInfo;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final 〇〇888()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->Oo08:Lcom/intsig/okbinder/ServerInfo;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    monitor-enter p0

    .line 6
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->Oo08:Lcom/intsig/okbinder/ServerInfo;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    const-string v0, "MultiProcessAssistant"

    .line 11
    .line 12
    const-string v1, "disconnectService"

    .line 13
    .line 14
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    sget-object v0, Lcom/intsig/okbinder/OkBinder;->〇080:Lcom/intsig/okbinder/OkBinder;

    .line 18
    .line 19
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 20
    .line 21
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    iget-object v2, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->Oo08:Lcom/intsig/okbinder/ServerInfo;

    .line 26
    .line 27
    invoke-virtual {v0, v1, v2}, Lcom/intsig/okbinder/OkBinder;->Oo08(Landroid/content/Context;Lcom/intsig/okbinder/ServerInfo;)V

    .line 28
    .line 29
    .line 30
    const/4 v0, 0x0

    .line 31
    iput-boolean v0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->〇〇888:Z

    .line 32
    .line 33
    const/4 v0, 0x0

    .line 34
    iput-object v0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->Oo08:Lcom/intsig/okbinder/ServerInfo;

    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/camscanner/multiprocess/MultiProcessAssistant;->oO80:Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;

    .line 37
    .line 38
    if-eqz v0, :cond_0

    .line 39
    .line 40
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiprocess/MultiProcessIDLEHelper;->〇o〇()V

    .line 41
    .line 42
    .line 43
    :cond_0
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    .line 45
    monitor-exit p0

    .line 46
    goto :goto_0

    .line 47
    :catchall_0
    move-exception v0

    .line 48
    monitor-exit p0

    .line 49
    throw v0

    .line 50
    :cond_1
    :goto_0
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

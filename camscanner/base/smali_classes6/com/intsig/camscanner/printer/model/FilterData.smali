.class public final Lcom/intsig/camscanner/printer/model/FilterData;
.super Ljava/lang/Object;
.source "PrintPaperFilter.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private filter_list:Lcom/intsig/camscanner/printer/model/FilterType;

.field private upload_time:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const-wide/16 v0, -0x1

    .line 5
    .line 6
    iput-wide v0, p0, Lcom/intsig/camscanner/printer/model/FilterData;->upload_time:J

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final getFilter_list()Lcom/intsig/camscanner/printer/model/FilterType;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/printer/model/FilterData;->filter_list:Lcom/intsig/camscanner/printer/model/FilterType;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getUpload_time()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/printer/model/FilterData;->upload_time:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final setFilter_list(Lcom/intsig/camscanner/printer/model/FilterType;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/printer/model/FilterData;->filter_list:Lcom/intsig/camscanner/printer/model/FilterType;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setUpload_time(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/printer/model/FilterData;->upload_time:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

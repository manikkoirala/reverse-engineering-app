.class public final Lcom/intsig/camscanner/printer/fragment/PrinterHistoryFragment$connectListener$1;
.super Ljava/lang/Object;
.source "PrinterHistoryFragment.kt"

# interfaces
.implements Lcom/intsig/camscanner/printer/contract/PrinterSearchClickItem;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/intsig/camscanner/printer/fragment/PrinterHistoryFragment;-><init>()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field final synthetic 〇080:Lcom/intsig/camscanner/printer/fragment/PrinterHistoryFragment;


# direct methods
.method constructor <init>(Lcom/intsig/camscanner/printer/fragment/PrinterHistoryFragment;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/printer/fragment/PrinterHistoryFragment$connectListener$1;->〇080:Lcom/intsig/camscanner/printer/fragment/PrinterHistoryFragment;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public 〇080(Landroid/view/View;Lcom/intsig/camscanner/printer/model/PrinterPropertyData;)V
    .locals 6
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/printer/model/PrinterPropertyData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "view"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "data"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p2}, Lcom/intsig/camscanner/printer/model/PrinterPropertyData;->getPrinterNumberName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    new-instance v1, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v2, "on click printer item "

    .line 21
    .line 22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    const-string v1, "PrinterHistoryFragment"

    .line 33
    .line 34
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/printer/fragment/PrinterHistoryFragment$connectListener$1;->〇080:Lcom/intsig/camscanner/printer/fragment/PrinterHistoryFragment;

    .line 38
    .line 39
    invoke-static {v0}, Lcom/intsig/camscanner/printer/fragment/PrinterHistoryFragment;->O0〇0(Lcom/intsig/camscanner/printer/fragment/PrinterHistoryFragment;)Lcom/intsig/utils/ClickLimit;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    const-wide/16 v2, 0x12c

    .line 44
    .line 45
    invoke-virtual {v0, p1, v2, v3}, Lcom/intsig/utils/ClickLimit;->〇o00〇〇Oo(Landroid/view/View;J)Z

    .line 46
    .line 47
    .line 48
    move-result p1

    .line 49
    if-nez p1, :cond_0

    .line 50
    .line 51
    const-string p1, "click item too fast"

    .line 52
    .line 53
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    return-void

    .line 57
    :cond_0
    invoke-virtual {p2}, Lcom/intsig/camscanner/printer/model/PrinterPropertyData;->isConnected()Z

    .line 58
    .line 59
    .line 60
    move-result p1

    .line 61
    if-eqz p1, :cond_1

    .line 62
    .line 63
    return-void

    .line 64
    :cond_1
    invoke-virtual {p2}, Lcom/intsig/camscanner/printer/model/PrinterPropertyData;->isConnecting()Z

    .line 65
    .line 66
    .line 67
    move-result p1

    .line 68
    if-eqz p1, :cond_2

    .line 69
    .line 70
    return-void

    .line 71
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/printer/viewmodel/PrinterPermissionManager;->〇080:Lcom/intsig/camscanner/printer/viewmodel/PrinterPermissionManager;

    .line 72
    .line 73
    iget-object p1, p0, Lcom/intsig/camscanner/printer/fragment/PrinterHistoryFragment$connectListener$1;->〇080:Lcom/intsig/camscanner/printer/fragment/PrinterHistoryFragment;

    .line 74
    .line 75
    invoke-static {p1}, Lcom/intsig/camscanner/printer/fragment/PrinterHistoryFragment;->〇〇o0〇8(Lcom/intsig/camscanner/printer/fragment/PrinterHistoryFragment;)Landroidx/appcompat/app/AppCompatActivity;

    .line 76
    .line 77
    .line 78
    move-result-object v1

    .line 79
    const-string p1, "mActivity"

    .line 80
    .line 81
    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 82
    .line 83
    .line 84
    new-instance v2, Lcom/intsig/camscanner/printer/fragment/PrinterHistoryFragment$connectListener$1$onClickItem$1;

    .line 85
    .line 86
    iget-object p1, p0, Lcom/intsig/camscanner/printer/fragment/PrinterHistoryFragment$connectListener$1;->〇080:Lcom/intsig/camscanner/printer/fragment/PrinterHistoryFragment;

    .line 87
    .line 88
    invoke-direct {v2, p2, p1}, Lcom/intsig/camscanner/printer/fragment/PrinterHistoryFragment$connectListener$1$onClickItem$1;-><init>(Lcom/intsig/camscanner/printer/model/PrinterPropertyData;Lcom/intsig/camscanner/printer/fragment/PrinterHistoryFragment;)V

    .line 89
    .line 90
    .line 91
    const/4 v3, 0x0

    .line 92
    const/4 v4, 0x4

    .line 93
    const/4 v5, 0x0

    .line 94
    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/printer/viewmodel/PrinterPermissionManager;->〇〇888(Lcom/intsig/camscanner/printer/viewmodel/PrinterPermissionManager;Landroid/app/Activity;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    .line 95
    .line 96
    .line 97
    return-void
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public 〇o00〇〇Oo(Landroid/view/View;Lcom/intsig/camscanner/printer/model/PrinterPropertyData;)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/printer/model/PrinterPropertyData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "view"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "data"

    .line 7
    .line 8
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p2}, Lcom/intsig/camscanner/printer/model/PrinterPropertyData;->getPrinterNumberName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    new-instance v0, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v1, "on click info "

    .line 21
    .line 22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    const-string v0, "PrinterHistoryFragment"

    .line 33
    .line 34
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    invoke-static {}, Lcom/intsig/router/CSRouter;->〇o〇()Lcom/intsig/router/CSRouter;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    const-string v0, "/printer/home"

    .line 42
    .line 43
    invoke-virtual {p1, v0}, Lcom/intsig/router/CSRouter;->〇080(Ljava/lang/String;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    const-string v0, "which_page_type"

    .line 48
    .line 49
    const/4 v1, 0x2

    .line 50
    invoke-virtual {p1, v0, v1}, Lcom/alibaba/android/arouter/facade/Postcard;->withInt(Ljava/lang/String;I)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 51
    .line 52
    .line 53
    move-result-object p1

    .line 54
    const-string v0, "extra_device_data"

    .line 55
    .line 56
    invoke-virtual {p1, v0, p2}, Lcom/alibaba/android/arouter/facade/Postcard;->withSerializable(Ljava/lang/String;Ljava/io/Serializable;)Lcom/alibaba/android/arouter/facade/Postcard;

    .line 57
    .line 58
    .line 59
    move-result-object p1

    .line 60
    invoke-virtual {p1}, Lcom/alibaba/android/arouter/facade/Postcard;->navigation()Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public 〇o〇()V
    .locals 2

    .line 1
    const-string v0, "PrinterHistoryFragment"

    .line 2
    .line 3
    const-string v1, "onCloseAdBanner"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.class public final Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;
.super Lcom/intsig/camscanner/printer/fragment/BasePrintFragment;
.source "PrinterSearchFragment.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final OO〇00〇8oO:Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field static final synthetic o8〇OO0〇0o:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Landroid/view/View;

.field private OO:Z

.field private final o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oOo0:Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$itemDecoration$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOo〇8o008:Ljava/lang/String;

.field private final o〇00O:Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$itemClickListener$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇080OO8〇0:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇08O〇00〇o:Z

.field private final 〇0O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇OOo8〇0:Lcom/intsig/camscanner/printer/adapter/PrinterSearchAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->o8〇OO0〇0o:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->OO〇00〇8oO:Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$Companion;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/BasePrintFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v6, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x4

    .line 10
    const/4 v5, 0x0

    .line 11
    move-object v0, v6

    .line 12
    move-object v2, p0

    .line 13
    invoke-direct/range {v0 .. v5}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object v6, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 17
    .line 18
    new-instance v0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$itemClickListener$1;

    .line 19
    .line 20
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$itemClickListener$1;-><init>(Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;)V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->o〇00O:Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$itemClickListener$1;

    .line 24
    .line 25
    sget-object v0, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 26
    .line 27
    new-instance v1, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$printerSearchViewModel$2;

    .line 28
    .line 29
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$printerSearchViewModel$2;-><init>(Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;)V

    .line 30
    .line 31
    .line 32
    invoke-static {v0, v1}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    iput-object v1, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇080OO8〇0:Lkotlin/Lazy;

    .line 37
    .line 38
    new-instance v1, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$printerConnectViewModel$2;

    .line 39
    .line 40
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$printerConnectViewModel$2;-><init>(Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;)V

    .line 41
    .line 42
    .line 43
    invoke-static {v0, v1}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    iput-object v0, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇0O:Lkotlin/Lazy;

    .line 48
    .line 49
    new-instance v0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$itemDecoration$1;

    .line 50
    .line 51
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$itemDecoration$1;-><init>(Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;)V

    .line 52
    .line 53
    .line 54
    iput-object v0, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->oOo0:Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$itemDecoration$1;

    .line 55
    .line 56
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final O0O0〇()V
    .locals 6

    .line 1
    sget-object v0, Lcom/intsig/camscanner/printer/viewmodel/PrinterPermissionManager;->〇080:Lcom/intsig/camscanner/printer/viewmodel/PrinterPermissionManager;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 4
    .line 5
    const-string v2, "mActivity"

    .line 6
    .line 7
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    new-instance v2, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$startSearch$1;

    .line 11
    .line 12
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$startSearch$1;-><init>(Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;)V

    .line 13
    .line 14
    .line 15
    const/4 v3, 0x0

    .line 16
    const/4 v4, 0x4

    .line 17
    const/4 v5, 0x0

    .line 18
    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/printer/viewmodel/PrinterPermissionManager;->〇〇888(Lcom/intsig/camscanner/printer/viewmodel/PrinterPermissionManager;Landroid/app/Activity;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    .line 19
    .line 20
    .line 21
    return-void
.end method

.method public static synthetic O0〇0(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇0oO〇oo00(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final O8〇8〇O80(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final Ooo8o()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_1

    .line 7
    .line 8
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;->OO:Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;

    .line 9
    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 13
    .line 14
    if-eqz v0, :cond_1

    .line 15
    .line 16
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    const/4 v2, 0x1

    .line 21
    if-nez v0, :cond_0

    .line 22
    .line 23
    const/4 v0, 0x1

    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 v0, 0x0

    .line 26
    :goto_0
    if-ne v0, v2, :cond_1

    .line 27
    .line 28
    const/4 v1, 0x1

    .line 29
    :cond_1
    if-eqz v1, :cond_2

    .line 30
    .line 31
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    if-eqz v0, :cond_2

    .line 36
    .line 37
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;->OO:Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;

    .line 38
    .line 39
    if-eqz v0, :cond_2

    .line 40
    .line 41
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;->〇08O〇00〇o:Lcom/airbnb/lottie/LottieAnimationView;

    .line 42
    .line 43
    if-eqz v0, :cond_2

    .line 44
    .line 45
    const v1, 0x7f12004f

    .line 46
    .line 47
    .line 48
    invoke-virtual {v0, v1}, Lcom/airbnb/lottie/LottieAnimationView;->setAnimation(I)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0}, Lcom/airbnb/lottie/LottieAnimationView;->〇O〇()V

    .line 52
    .line 53
    .line 54
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇〇〇0()Lcom/intsig/camscanner/printer/viewmodel/PrinterSearchViewModel;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    invoke-virtual {v0}, Lcom/intsig/camscanner/printer/viewmodel/PrinterSearchViewModel;->oo88o8O()V

    .line 59
    .line 60
    .line 61
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇Oo〇O()V

    .line 62
    .line 63
    .line 64
    return-void
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic o00〇88〇08(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇O8〇8000(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o880(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇08O(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic oOoO8OO〇(Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇Oo〇O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic oooO888(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->O8〇8〇O80(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic o〇0〇o(Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->Ooo8o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o〇O8OO(Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->oOo〇8o008:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇088O(Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;)Landroidx/appcompat/app/AppCompatActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private static final 〇08O(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇0oO〇oo00(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇0ooOOo(Lcom/intsig/camscanner/printer/model/PrinterPropertyData;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NotifyDataSetChanged"
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Lcom/intsig/camscanner/printer/model/PrinterPropertyData;->isConnected()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->oOo〇8o008:Ljava/lang/String;

    .line 9
    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    new-instance p1, Ljava/lang/StringBuilder;

    .line 13
    .line 14
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 15
    .line 16
    .line 17
    const-string v1, "connectBluetoothAddress:"

    .line 18
    .line 19
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    const-string v0, "PrinterConnectFragment"

    .line 30
    .line 31
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    return-void

    .line 35
    :cond_1
    sget-object v0, Lcom/intsig/developer/printer/PrinterAdapterImpl;->〇080:Lcom/intsig/developer/printer/PrinterAdapterImpl;

    .line 36
    .line 37
    invoke-virtual {v0}, Lcom/intsig/developer/printer/PrinterAdapterImpl;->〇O00()V

    .line 38
    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇OOo8〇0:Lcom/intsig/camscanner/printer/adapter/PrinterSearchAdapter;

    .line 41
    .line 42
    if-eqz v0, :cond_2

    .line 43
    .line 44
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 45
    .line 46
    .line 47
    :cond_2
    const/4 v0, 0x1

    .line 48
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/printer/model/PrinterPropertyData;->setConnectStatus(I)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {p1}, Lcom/intsig/camscanner/printer/model/PrinterPropertyData;->getMacAddress()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    iput-object v0, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->oOo〇8o008:Ljava/lang/String;

    .line 56
    .line 57
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇〇〇00()Lcom/intsig/camscanner/printer/viewmodel/PrinterConnectViewModel;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    const/4 v1, 0x2

    .line 62
    const/4 v2, 0x0

    .line 63
    const/4 v3, 0x0

    .line 64
    invoke-static {v0, p1, v3, v1, v2}, Lcom/intsig/camscanner/printer/viewmodel/PrinterConnectViewModel;->oo88o8O(Lcom/intsig/camscanner/printer/viewmodel/PrinterConnectViewModel;Lcom/intsig/camscanner/printer/model/PrinterPropertyData;ZILjava/lang/Object;)V

    .line 65
    .line 66
    .line 67
    const-string p1, "CSPrintConnectPage"

    .line 68
    .line 69
    const-string v0, "connect_printer"

    .line 70
    .line 71
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static final synthetic 〇0〇0(Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇08O〇00〇o:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇8O0880()V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NotifyDataSetChanged"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇〇〇0()Lcom/intsig/camscanner/printer/viewmodel/PrinterSearchViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/printer/viewmodel/PrinterSearchViewModel;->Oooo8o0〇()Landroidx/lifecycle/MutableLiveData;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    new-instance v2, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$initSearchViewModel$1;

    .line 14
    .line 15
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$initSearchViewModel$1;-><init>(Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;)V

    .line 16
    .line 17
    .line 18
    new-instance v3, Lcom/intsig/camscanner/printer/fragment/o8oO〇;

    .line 19
    .line 20
    invoke-direct {v3, v2}, Lcom/intsig/camscanner/printer/fragment/o8oO〇;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1, v3}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 24
    .line 25
    .line 26
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇〇〇0()Lcom/intsig/camscanner/printer/viewmodel/PrinterSearchViewModel;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lcom/intsig/camscanner/printer/viewmodel/PrinterSearchViewModel;->〇8o8o〇()Landroidx/lifecycle/MutableLiveData;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    new-instance v2, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$initSearchViewModel$2;

    .line 39
    .line 40
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$initSearchViewModel$2;-><init>(Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;)V

    .line 41
    .line 42
    .line 43
    new-instance v3, Lcom/intsig/camscanner/printer/fragment/o〇8oOO88;

    .line 44
    .line 45
    invoke-direct {v3, v2}, Lcom/intsig/camscanner/printer/fragment/o〇8oOO88;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {v0, v1, v3}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 49
    .line 50
    .line 51
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇〇〇0()Lcom/intsig/camscanner/printer/viewmodel/PrinterSearchViewModel;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    invoke-virtual {v0}, Lcom/intsig/camscanner/printer/viewmodel/PrinterSearchViewModel;->〇80〇808〇O()Landroidx/lifecycle/MutableLiveData;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    new-instance v2, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$initSearchViewModel$3;

    .line 64
    .line 65
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$initSearchViewModel$3;-><init>(Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;)V

    .line 66
    .line 67
    .line 68
    new-instance v3, Lcom/intsig/camscanner/printer/fragment/o〇O;

    .line 69
    .line 70
    invoke-direct {v3, v2}, Lcom/intsig/camscanner/printer/fragment/o〇O;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 71
    .line 72
    .line 73
    invoke-virtual {v0, v1, v3}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 74
    .line 75
    .line 76
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static final synthetic 〇8〇80o(Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;)Lcom/intsig/camscanner/printer/adapter/PrinterSearchAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇OOo8〇0:Lcom/intsig/camscanner/printer/adapter/PrinterSearchAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;Lcom/intsig/camscanner/printer/model/PrinterPropertyData;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇0ooOOo(Lcom/intsig/camscanner/printer/model/PrinterPropertyData;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇O0o〇〇o(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇O8〇8000(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇Oo〇O()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇OOo8〇0:Lcom/intsig/camscanner/printer/adapter/PrinterSearchAdapter;

    .line 2
    .line 3
    if-eqz v0, :cond_1a

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O8〇o()Ljava/util/List;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_1a

    .line 10
    .line 11
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    const v1, 0x3e99999a    # 0.3f

    .line 16
    .line 17
    .line 18
    const/high16 v2, 0x3f800000    # 1.0f

    .line 19
    .line 20
    const/16 v3, 0x8

    .line 21
    .line 22
    const/4 v4, 0x0

    .line 23
    const/4 v5, 0x0

    .line 24
    if-lez v0, :cond_10

    .line 25
    .line 26
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    if-eqz v0, :cond_0

    .line 31
    .line 32
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;->OO:Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;

    .line 33
    .line 34
    if-eqz v0, :cond_0

    .line 35
    .line 36
    invoke-virtual {v0}, Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    goto :goto_0

    .line 41
    :cond_0
    move-object v0, v5

    .line 42
    :goto_0
    if-nez v0, :cond_1

    .line 43
    .line 44
    goto :goto_1

    .line 45
    :cond_1
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 46
    .line 47
    .line 48
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    if-eqz v0, :cond_2

    .line 53
    .line 54
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 55
    .line 56
    goto :goto_2

    .line 57
    :cond_2
    move-object v0, v5

    .line 58
    :goto_2
    if-nez v0, :cond_3

    .line 59
    .line 60
    goto :goto_3

    .line 61
    :cond_3
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 62
    .line 63
    .line 64
    :goto_3
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇〇〇0()Lcom/intsig/camscanner/printer/viewmodel/PrinterSearchViewModel;

    .line 65
    .line 66
    .line 67
    move-result-object v0

    .line 68
    invoke-virtual {v0}, Lcom/intsig/camscanner/printer/viewmodel/PrinterSearchViewModel;->〇O00()Z

    .line 69
    .line 70
    .line 71
    move-result v0

    .line 72
    if-eqz v0, :cond_a

    .line 73
    .line 74
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    if-eqz v0, :cond_4

    .line 79
    .line 80
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;->O8o08O8O:Landroid/widget/TextView;

    .line 81
    .line 82
    goto :goto_4

    .line 83
    :cond_4
    move-object v0, v5

    .line 84
    :goto_4
    if-nez v0, :cond_5

    .line 85
    .line 86
    goto :goto_5

    .line 87
    :cond_5
    const-string v2, ""

    .line 88
    .line 89
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    .line 91
    .line 92
    :goto_5
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    if-eqz v0, :cond_6

    .line 97
    .line 98
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;->〇08O〇00〇o:Landroid/widget/ProgressBar;

    .line 99
    .line 100
    goto :goto_6

    .line 101
    :cond_6
    move-object v0, v5

    .line 102
    :goto_6
    if-nez v0, :cond_7

    .line 103
    .line 104
    goto :goto_7

    .line 105
    :cond_7
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 106
    .line 107
    .line 108
    :goto_7
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 109
    .line 110
    .line 111
    move-result-object v0

    .line 112
    if-eqz v0, :cond_8

    .line 113
    .line 114
    iget-object v5, v0, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;->O8o08O8O:Landroid/widget/TextView;

    .line 115
    .line 116
    :cond_8
    if-nez v5, :cond_9

    .line 117
    .line 118
    goto/16 :goto_f

    .line 119
    .line 120
    :cond_9
    invoke-virtual {v5, v1}, Landroid/view/View;->setAlpha(F)V

    .line 121
    .line 122
    .line 123
    goto/16 :goto_f

    .line 124
    .line 125
    :cond_a
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 126
    .line 127
    .line 128
    move-result-object v0

    .line 129
    if-eqz v0, :cond_b

    .line 130
    .line 131
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;->O8o08O8O:Landroid/widget/TextView;

    .line 132
    .line 133
    if-eqz v0, :cond_b

    .line 134
    .line 135
    const v1, 0x7f131937

    .line 136
    .line 137
    .line 138
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 139
    .line 140
    .line 141
    :cond_b
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 142
    .line 143
    .line 144
    move-result-object v0

    .line 145
    if-eqz v0, :cond_c

    .line 146
    .line 147
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;->〇08O〇00〇o:Landroid/widget/ProgressBar;

    .line 148
    .line 149
    goto :goto_8

    .line 150
    :cond_c
    move-object v0, v5

    .line 151
    :goto_8
    if-nez v0, :cond_d

    .line 152
    .line 153
    goto :goto_9

    .line 154
    :cond_d
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 155
    .line 156
    .line 157
    :goto_9
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 158
    .line 159
    .line 160
    move-result-object v0

    .line 161
    if-eqz v0, :cond_e

    .line 162
    .line 163
    iget-object v5, v0, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;->O8o08O8O:Landroid/widget/TextView;

    .line 164
    .line 165
    :cond_e
    if-nez v5, :cond_f

    .line 166
    .line 167
    goto/16 :goto_f

    .line 168
    .line 169
    :cond_f
    invoke-virtual {v5, v2}, Landroid/view/View;->setAlpha(F)V

    .line 170
    .line 171
    .line 172
    goto/16 :goto_f

    .line 173
    .line 174
    :cond_10
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 175
    .line 176
    .line 177
    move-result-object v0

    .line 178
    if-eqz v0, :cond_11

    .line 179
    .line 180
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;->OO:Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;

    .line 181
    .line 182
    if-eqz v0, :cond_11

    .line 183
    .line 184
    invoke-virtual {v0}, Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;->〇080()Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 185
    .line 186
    .line 187
    move-result-object v0

    .line 188
    goto :goto_a

    .line 189
    :cond_11
    move-object v0, v5

    .line 190
    :goto_a
    if-nez v0, :cond_12

    .line 191
    .line 192
    goto :goto_b

    .line 193
    :cond_12
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 194
    .line 195
    .line 196
    :goto_b
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 197
    .line 198
    .line 199
    move-result-object v0

    .line 200
    if-eqz v0, :cond_13

    .line 201
    .line 202
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 203
    .line 204
    goto :goto_c

    .line 205
    :cond_13
    move-object v0, v5

    .line 206
    :goto_c
    if-nez v0, :cond_14

    .line 207
    .line 208
    goto :goto_d

    .line 209
    :cond_14
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 210
    .line 211
    .line 212
    :goto_d
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 213
    .line 214
    .line 215
    move-result-object v0

    .line 216
    if-eqz v0, :cond_1a

    .line 217
    .line 218
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;->OO:Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;

    .line 219
    .line 220
    if-eqz v0, :cond_1a

    .line 221
    .line 222
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;->〇080OO8〇0:Landroid/widget/TextView;

    .line 223
    .line 224
    if-eqz v0, :cond_1a

    .line 225
    .line 226
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇〇〇0()Lcom/intsig/camscanner/printer/viewmodel/PrinterSearchViewModel;

    .line 227
    .line 228
    .line 229
    move-result-object v3

    .line 230
    invoke-virtual {v3}, Lcom/intsig/camscanner/printer/viewmodel/PrinterSearchViewModel;->〇O00()Z

    .line 231
    .line 232
    .line 233
    move-result v3

    .line 234
    if-eqz v3, :cond_16

    .line 235
    .line 236
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 237
    .line 238
    .line 239
    move-result-object v2

    .line 240
    if-eqz v2, :cond_15

    .line 241
    .line 242
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;->OO:Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;

    .line 243
    .line 244
    if-eqz v2, :cond_15

    .line 245
    .line 246
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;->〇0O:Landroid/widget/TextView;

    .line 247
    .line 248
    if-eqz v2, :cond_15

    .line 249
    .line 250
    const v3, 0x7f130d5c

    .line 251
    .line 252
    .line 253
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 254
    .line 255
    .line 256
    :cond_15
    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 257
    .line 258
    .line 259
    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    .line 260
    .line 261
    .line 262
    goto :goto_f

    .line 263
    :cond_16
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 264
    .line 265
    .line 266
    move-result-object v1

    .line 267
    if-eqz v1, :cond_19

    .line 268
    .line 269
    const v3, 0x7f130d87

    .line 270
    .line 271
    .line 272
    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 273
    .line 274
    .line 275
    move-result-object v3

    .line 276
    const v4, 0x7f130d88

    .line 277
    .line 278
    .line 279
    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 280
    .line 281
    .line 282
    move-result-object v1

    .line 283
    new-instance v4, Ljava/lang/StringBuilder;

    .line 284
    .line 285
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 286
    .line 287
    .line 288
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    .line 290
    .line 291
    const-string v3, "\n\r"

    .line 292
    .line 293
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 294
    .line 295
    .line 296
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 297
    .line 298
    .line 299
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 300
    .line 301
    .line 302
    move-result-object v1

    .line 303
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 304
    .line 305
    .line 306
    move-result-object v3

    .line 307
    if-eqz v3, :cond_17

    .line 308
    .line 309
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;->OO:Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;

    .line 310
    .line 311
    if-eqz v3, :cond_17

    .line 312
    .line 313
    iget-object v5, v3, Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;->〇0O:Landroid/widget/TextView;

    .line 314
    .line 315
    :cond_17
    if-nez v5, :cond_18

    .line 316
    .line 317
    goto :goto_e

    .line 318
    :cond_18
    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 319
    .line 320
    .line 321
    :goto_e
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 322
    .line 323
    .line 324
    move-result-object v1

    .line 325
    if-eqz v1, :cond_19

    .line 326
    .line 327
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;->OO:Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;

    .line 328
    .line 329
    if-eqz v1, :cond_19

    .line 330
    .line 331
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;->〇08O〇00〇o:Lcom/airbnb/lottie/LottieAnimationView;

    .line 332
    .line 333
    if-eqz v1, :cond_19

    .line 334
    .line 335
    const v3, 0x7f0806d8

    .line 336
    .line 337
    .line 338
    invoke-virtual {v1, v3}, Lcom/airbnb/lottie/LottieAnimationView;->setImageResource(I)V

    .line 339
    .line 340
    .line 341
    :cond_19
    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 342
    .line 343
    .line 344
    const/4 v1, 0x1

    .line 345
    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 346
    .line 347
    .line 348
    :cond_1a
    :goto_f
    return-void
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

.method private final 〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->o8〇OO0〇0o:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic 〇o〇88〇8(Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;)Lcom/intsig/utils/ClickLimit;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mClickLimit:Lcom/intsig/utils/ClickLimit;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇〇O80〇0o()V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NotifyDataSetChanged"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇〇〇00()Lcom/intsig/camscanner/printer/viewmodel/PrinterConnectViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/printer/viewmodel/PrinterConnectViewModel;->〇oo〇()Landroidx/lifecycle/MutableLiveData;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    new-instance v2, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$initConnectViewModel$1;

    .line 14
    .line 15
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$initConnectViewModel$1;-><init>(Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;)V

    .line 16
    .line 17
    .line 18
    new-instance v3, Lcom/intsig/camscanner/printer/fragment/oO00OOO;

    .line 19
    .line 20
    invoke-direct {v3, v2}, Lcom/intsig/camscanner/printer/fragment/oO00OOO;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1, v3}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 24
    .line 25
    .line 26
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇〇〇00()Lcom/intsig/camscanner/printer/viewmodel/PrinterConnectViewModel;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lcom/intsig/camscanner/printer/viewmodel/PrinterConnectViewModel;->〇00()Landroidx/lifecycle/MutableLiveData;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getViewLifecycleOwner()Landroidx/lifecycle/LifecycleOwner;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    new-instance v2, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$initConnectViewModel$2;

    .line 39
    .line 40
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$initConnectViewModel$2;-><init>(Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;)V

    .line 41
    .line 42
    .line 43
    new-instance v3, Lcom/intsig/camscanner/printer/fragment/O000;

    .line 44
    .line 45
    invoke-direct {v3, v2}, Lcom/intsig/camscanner/printer/fragment/O000;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {v0, v1, v3}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 49
    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic 〇〇o0〇8(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇O0o〇〇o(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇〇〇0()Lcom/intsig/camscanner/printer/viewmodel/PrinterSearchViewModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇080OO8〇0:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/printer/viewmodel/PrinterSearchViewModel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇〇〇00()Lcom/intsig/camscanner/printer/viewmodel/PrinterConnectViewModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇0O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/printer/viewmodel/PrinterConnectViewModel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public dealClickAction(Landroid/view/View;)V
    .locals 6

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    move-object p1, v0

    .line 14
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    if-eqz v1, :cond_1

    .line 19
    .line 20
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;->O8o08O8O:Landroid/widget/TextView;

    .line 21
    .line 22
    if-eqz v1, :cond_1

    .line 23
    .line 24
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    goto :goto_1

    .line 33
    :cond_1
    move-object v1, v0

    .line 34
    :goto_1
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    const-string v2, "PrinterConnectFragment"

    .line 39
    .line 40
    if-eqz v1, :cond_2

    .line 41
    .line 42
    const-string p1, "click tvRefresh"

    .line 43
    .line 44
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->O0O0〇()V

    .line 48
    .line 49
    .line 50
    goto/16 :goto_7

    .line 51
    .line 52
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    if-eqz v1, :cond_3

    .line 57
    .line 58
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;->OO:Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;

    .line 59
    .line 60
    if-eqz v1, :cond_3

    .line 61
    .line 62
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;->〇080OO8〇0:Landroid/widget/TextView;

    .line 63
    .line 64
    if-eqz v1, :cond_3

    .line 65
    .line 66
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    .line 67
    .line 68
    .line 69
    move-result v1

    .line 70
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 71
    .line 72
    .line 73
    move-result-object v1

    .line 74
    goto :goto_2

    .line 75
    :cond_3
    move-object v1, v0

    .line 76
    :goto_2
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 77
    .line 78
    .line 79
    move-result v1

    .line 80
    if-eqz v1, :cond_4

    .line 81
    .line 82
    const-string p1, "click tvReSearch"

    .line 83
    .line 84
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->O0O0〇()V

    .line 88
    .line 89
    .line 90
    goto/16 :goto_7

    .line 91
    .line 92
    :cond_4
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 93
    .line 94
    .line 95
    move-result-object v1

    .line 96
    if-eqz v1, :cond_5

    .line 97
    .line 98
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;->OO:Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;

    .line 99
    .line 100
    if-eqz v1, :cond_5

    .line 101
    .line 102
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;->o〇00O:Landroid/widget/TextView;

    .line 103
    .line 104
    if-eqz v1, :cond_5

    .line 105
    .line 106
    invoke-virtual {v1}, Landroid/view/View;->getId()I

    .line 107
    .line 108
    .line 109
    move-result v1

    .line 110
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 111
    .line 112
    .line 113
    move-result-object v1

    .line 114
    goto :goto_3

    .line 115
    :cond_5
    move-object v1, v0

    .line 116
    :goto_3
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 117
    .line 118
    .line 119
    move-result v1

    .line 120
    const-string v3, "CSPrintConnectPage"

    .line 121
    .line 122
    const/4 v4, 0x0

    .line 123
    const/4 v5, 0x1

    .line 124
    if-eqz v1, :cond_d

    .line 125
    .line 126
    const-string p1, "not find printer"

    .line 127
    .line 128
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    .line 130
    .line 131
    const-string p1, "click_to_buy"

    .line 132
    .line 133
    invoke-static {v3, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    .line 135
    .line 136
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 137
    .line 138
    .line 139
    move-result-object p1

    .line 140
    iget-object p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson;->printer_buy_entry:Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;

    .line 141
    .line 142
    if-eqz p1, :cond_c

    .line 143
    .line 144
    iget-object v0, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;->link_link:Ljava/lang/String;

    .line 145
    .line 146
    if-eqz v0, :cond_7

    .line 147
    .line 148
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 149
    .line 150
    .line 151
    move-result v0

    .line 152
    if-nez v0, :cond_6

    .line 153
    .line 154
    goto :goto_4

    .line 155
    :cond_6
    const/4 v0, 0x0

    .line 156
    goto :goto_5

    .line 157
    :cond_7
    :goto_4
    const/4 v0, 0x1

    .line 158
    :goto_5
    if-eqz v0, :cond_8

    .line 159
    .line 160
    const-string p1, "link_link is empty"

    .line 161
    .line 162
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    .line 164
    .line 165
    return-void

    .line 166
    :cond_8
    iget-object v0, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;->link_mini_app:Ljava/lang/String;

    .line 167
    .line 168
    if-eqz v0, :cond_9

    .line 169
    .line 170
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 171
    .line 172
    .line 173
    move-result v0

    .line 174
    if-nez v0, :cond_a

    .line 175
    .line 176
    :cond_9
    const/4 v4, 0x1

    .line 177
    :cond_a
    if-nez v4, :cond_b

    .line 178
    .line 179
    iget-object v0, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;->link_mini_app:Ljava/lang/String;

    .line 180
    .line 181
    new-instance v1, Ljava/lang/StringBuilder;

    .line 182
    .line 183
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 184
    .line 185
    .line 186
    const-string v3, "link_mini_app = "

    .line 187
    .line 188
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    .line 190
    .line 191
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 192
    .line 193
    .line 194
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 195
    .line 196
    .line 197
    move-result-object v0

    .line 198
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    .line 200
    .line 201
    invoke-static {}, Lcom/intsig/camscanner/attention/smallroutine/SmallRoutine;->〇o00〇〇Oo()Lcom/intsig/camscanner/attention/smallroutine/SmallRoutine;

    .line 202
    .line 203
    .line 204
    move-result-object v0

    .line 205
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 206
    .line 207
    .line 208
    move-result-object v1

    .line 209
    iget-object v3, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;->link_mini_app:Ljava/lang/String;

    .line 210
    .line 211
    iget-object p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;->link_link:Ljava/lang/String;

    .line 212
    .line 213
    invoke-virtual {v0, v1, v3, p1}, Lcom/intsig/camscanner/attention/smallroutine/SmallRoutine;->o〇0(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    .line 215
    .line 216
    goto :goto_6

    .line 217
    :cond_b
    iget-object v0, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;->link_link:Ljava/lang/String;

    .line 218
    .line 219
    new-instance v1, Ljava/lang/StringBuilder;

    .line 220
    .line 221
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 222
    .line 223
    .line 224
    const-string v3, "link_link="

    .line 225
    .line 226
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 227
    .line 228
    .line 229
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    .line 231
    .line 232
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 233
    .line 234
    .line 235
    move-result-object v0

    .line 236
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    .line 238
    .line 239
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 240
    .line 241
    .line 242
    move-result-object v0

    .line 243
    iget-object p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;->link_link:Ljava/lang/String;

    .line 244
    .line 245
    invoke-static {v0, p1}, Lcom/intsig/webview/util/WebUtil;->〇8o8o〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 246
    .line 247
    .line 248
    :goto_6
    sget-object v0, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 249
    .line 250
    :cond_c
    if-nez v0, :cond_f

    .line 251
    .line 252
    const-string p1, "printer_buy_entry is null"

    .line 253
    .line 254
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    .line 256
    .line 257
    goto :goto_7

    .line 258
    :cond_d
    if-nez p1, :cond_e

    .line 259
    .line 260
    goto :goto_7

    .line 261
    :cond_e
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 262
    .line 263
    .line 264
    move-result p1

    .line 265
    const v0, 0x7f0a1501

    .line 266
    .line 267
    .line 268
    if-ne p1, v0, :cond_f

    .line 269
    .line 270
    const-string p1, "introduce"

    .line 271
    .line 272
    invoke-static {v3, p1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    .line 274
    .line 275
    const-string p1, "tv_introduce"

    .line 276
    .line 277
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    .line 279
    .line 280
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 281
    .line 282
    .line 283
    move-result-object p1

    .line 284
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 285
    .line 286
    .line 287
    move-result-object v0

    .line 288
    invoke-static {v0}, Lcom/intsig/camscanner/web/UrlUtil;->oo〇(Landroid/content/Context;)Ljava/lang/String;

    .line 289
    .line 290
    .line 291
    move-result-object v0

    .line 292
    const-string v1, ""

    .line 293
    .line 294
    invoke-static {p1, v1, v0, v5, v4}, Lcom/intsig/webview/util/WebUtil;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 295
    .line 296
    .line 297
    :cond_f
    :goto_7
    return-void
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    const/4 v0, 0x0

    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    const-string v1, "extra_is_from_my_device"

    .line 9
    .line 10
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    iput-boolean p1, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->OO:Z

    .line 15
    .line 16
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    iget-object p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson;->printer_buy_entry:Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;

    .line 21
    .line 22
    const/4 v1, 0x0

    .line 23
    if-eqz p1, :cond_1

    .line 24
    .line 25
    iget-object p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;->link_text_des:Ljava/lang/String;

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_1
    move-object p1, v1

    .line 29
    :goto_0
    const-string v2, ""

    .line 30
    .line 31
    if-nez p1, :cond_2

    .line 32
    .line 33
    move-object p1, v2

    .line 34
    :cond_2
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 35
    .line 36
    .line 37
    move-result v3

    .line 38
    if-nez v3, :cond_5

    .line 39
    .line 40
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 41
    .line 42
    .line 43
    move-result-object v3

    .line 44
    if-eqz v3, :cond_3

    .line 45
    .line 46
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;->OO:Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;

    .line 47
    .line 48
    if-eqz v3, :cond_3

    .line 49
    .line 50
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;->O8o08O8O:Landroid/widget/TextView;

    .line 51
    .line 52
    goto :goto_1

    .line 53
    :cond_3
    move-object v3, v1

    .line 54
    :goto_1
    if-nez v3, :cond_4

    .line 55
    .line 56
    goto :goto_2

    .line 57
    :cond_4
    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    .line 59
    .line 60
    :cond_5
    :goto_2
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 61
    .line 62
    .line 63
    move-result-object p1

    .line 64
    iget-object p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson;->printer_buy_entry:Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;

    .line 65
    .line 66
    if-eqz p1, :cond_6

    .line 67
    .line 68
    iget-object p1, p1, Lcom/intsig/tsapp/sync/AppConfigJson$PrinterBuyEntry;->link_text:Ljava/lang/String;

    .line 69
    .line 70
    goto :goto_3

    .line 71
    :cond_6
    move-object p1, v1

    .line 72
    :goto_3
    if-nez p1, :cond_7

    .line 73
    .line 74
    goto :goto_4

    .line 75
    :cond_7
    move-object v2, p1

    .line 76
    :goto_4
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 77
    .line 78
    .line 79
    move-result-object p1

    .line 80
    if-eqz p1, :cond_8

    .line 81
    .line 82
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;->OO:Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;

    .line 83
    .line 84
    if-eqz p1, :cond_8

    .line 85
    .line 86
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;->o〇00O:Landroid/widget/TextView;

    .line 87
    .line 88
    goto :goto_5

    .line 89
    :cond_8
    move-object p1, v1

    .line 90
    :goto_5
    if-nez p1, :cond_9

    .line 91
    .line 92
    goto :goto_6

    .line 93
    :cond_9
    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    .line 95
    .line 96
    :goto_6
    iget-boolean p1, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->OO:Z

    .line 97
    .line 98
    new-instance v2, Ljava/lang/StringBuilder;

    .line 99
    .line 100
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 101
    .line 102
    .line 103
    const-string v3, "isFromMyDevice:"

    .line 104
    .line 105
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    .line 107
    .line 108
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 112
    .line 113
    .line 114
    move-result-object p1

    .line 115
    const-string v2, "PrinterConnectFragment"

    .line 116
    .line 117
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    .line 119
    .line 120
    const/4 p1, 0x3

    .line 121
    new-array p1, p1, [Landroid/view/View;

    .line 122
    .line 123
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 124
    .line 125
    .line 126
    move-result-object v3

    .line 127
    if-eqz v3, :cond_a

    .line 128
    .line 129
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;->O8o08O8O:Landroid/widget/TextView;

    .line 130
    .line 131
    goto :goto_7

    .line 132
    :cond_a
    move-object v3, v1

    .line 133
    :goto_7
    aput-object v3, p1, v0

    .line 134
    .line 135
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 136
    .line 137
    .line 138
    move-result-object v0

    .line 139
    if-eqz v0, :cond_b

    .line 140
    .line 141
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;->OO:Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;

    .line 142
    .line 143
    if-eqz v0, :cond_b

    .line 144
    .line 145
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;->〇080OO8〇0:Landroid/widget/TextView;

    .line 146
    .line 147
    goto :goto_8

    .line 148
    :cond_b
    move-object v0, v1

    .line 149
    :goto_8
    const/4 v3, 0x1

    .line 150
    aput-object v0, p1, v3

    .line 151
    .line 152
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 153
    .line 154
    .line 155
    move-result-object v0

    .line 156
    if-eqz v0, :cond_c

    .line 157
    .line 158
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;->OO:Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;

    .line 159
    .line 160
    if-eqz v0, :cond_c

    .line 161
    .line 162
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutEmptySearchBinding;->o〇00O:Landroid/widget/TextView;

    .line 163
    .line 164
    goto :goto_9

    .line 165
    :cond_c
    move-object v0, v1

    .line 166
    :goto_9
    const/4 v3, 0x2

    .line 167
    aput-object v0, p1, v3

    .line 168
    .line 169
    invoke-virtual {p0, p1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 170
    .line 171
    .line 172
    new-instance p1, Lcom/intsig/camscanner/printer/adapter/PrinterSearchAdapter;

    .line 173
    .line 174
    iget-object v0, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->o〇00O:Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$itemClickListener$1;

    .line 175
    .line 176
    invoke-direct {p1, v1, v0}, Lcom/intsig/camscanner/printer/adapter/PrinterSearchAdapter;-><init>(Ljava/util/List;Lcom/intsig/camscanner/printer/contract/PrinterSearchClickItem;)V

    .line 177
    .line 178
    .line 179
    iput-object p1, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇OOo8〇0:Lcom/intsig/camscanner/printer/adapter/PrinterSearchAdapter;

    .line 180
    .line 181
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇o08()Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;

    .line 182
    .line 183
    .line 184
    move-result-object p1

    .line 185
    if-eqz p1, :cond_d

    .line 186
    .line 187
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/FragmentPrinterSearchBinding;->o〇00O:Landroidx/recyclerview/widget/RecyclerView;

    .line 188
    .line 189
    if-eqz p1, :cond_d

    .line 190
    .line 191
    iget-object v0, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->oOo0:Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment$itemDecoration$1;

    .line 192
    .line 193
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 194
    .line 195
    .line 196
    new-instance v0, Lcom/intsig/recycleviewLayoutmanager/TrycatchLinearLayoutManager;

    .line 197
    .line 198
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 199
    .line 200
    invoke-direct {v0, v1}, Lcom/intsig/recycleviewLayoutmanager/TrycatchLinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 201
    .line 202
    .line 203
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 204
    .line 205
    .line 206
    iget-object v0, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇OOo8〇0:Lcom/intsig/camscanner/printer/adapter/PrinterSearchAdapter;

    .line 207
    .line 208
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 209
    .line 210
    .line 211
    :cond_d
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇8O0880()V

    .line 212
    .line 213
    .line 214
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇〇O80〇0o()V

    .line 215
    .line 216
    .line 217
    const-string p1, "initialize"

    .line 218
    .line 219
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    .line 221
    .line 222
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->O0O0〇()V

    .line 223
    .line 224
    .line 225
    return-void
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public oO〇oo()V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NotifyDataSetChanged"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    instance-of v0, v0, Lcom/intsig/camscanner/printer/PrintHomeActivity;

    .line 6
    .line 7
    if-eqz v0, :cond_2

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->O8o08O8O:Landroid/view/View;

    .line 10
    .line 11
    if-nez v0, :cond_1

    .line 12
    .line 13
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    const v1, 0x7f0d06b8

    .line 22
    .line 23
    .line 24
    const/4 v2, 0x0

    .line 25
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    iput-object v0, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->O8o08O8O:Landroid/view/View;

    .line 30
    .line 31
    const/4 v1, 0x1

    .line 32
    new-array v1, v1, [Landroid/view/View;

    .line 33
    .line 34
    if-eqz v0, :cond_0

    .line 35
    .line 36
    const v2, 0x7f0a1501

    .line 37
    .line 38
    .line 39
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 40
    .line 41
    .line 42
    move-result-object v2

    .line 43
    :cond_0
    const/4 v0, 0x0

    .line 44
    aput-object v2, v1, v0

    .line 45
    .line 46
    invoke-virtual {p0, v1}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 47
    .line 48
    .line 49
    :cond_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    const-string v1, "null cannot be cast to non-null type com.intsig.camscanner.printer.PrintHomeActivity"

    .line 54
    .line 55
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    check-cast v0, Lcom/intsig/camscanner/printer/PrintHomeActivity;

    .line 59
    .line 60
    iget-object v1, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->O8o08O8O:Landroid/view/View;

    .line 61
    .line 62
    invoke-virtual {v0, v1}, Lcom/intsig/mvp/activity/BaseChangeActivity;->setToolbarMenu(Landroid/view/View;)V

    .line 63
    .line 64
    .line 65
    :cond_2
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    if-nez v0, :cond_3

    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_3
    const v1, 0x7f131940

    .line 73
    .line 74
    .line 75
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 76
    .line 77
    .line 78
    move-result-object v1

    .line 79
    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 80
    .line 81
    .line 82
    :goto_0
    return-void
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public onDestroyView()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->onDestroyView()V

    .line 2
    .line 3
    .line 4
    const-string v0, "PrinterConnectFragment"

    .line 5
    .line 6
    const-string v1, "onDestroyView"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onHiddenChanged(Z)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/camscanner/printer/fragment/BasePrintFragment;->onHiddenChanged(Z)V

    .line 2
    .line 3
    .line 4
    iget-boolean v0, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇08O〇00〇o:Z

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    if-nez p1, :cond_0

    .line 9
    .line 10
    const/4 p1, 0x0

    .line 11
    iput-boolean p1, p0, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->〇08O〇00〇o:Z

    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/camscanner/printer/fragment/BasePrintFragment;->〇O8oOo0()Z

    .line 14
    .line 15
    .line 16
    :cond_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onResume()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 2
    .line 3
    .line 4
    const-string v0, "PrinterConnectFragment"

    .line 5
    .line 6
    const-string v1, "onResume"

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/printer/fragment/PrinterSearchFragment;->oO〇oo()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onStart()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onStart()V

    .line 2
    .line 3
    .line 4
    const-string v0, "CSPrintConnectPage"

    .line 5
    .line 6
    invoke-static {v0}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d032f

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

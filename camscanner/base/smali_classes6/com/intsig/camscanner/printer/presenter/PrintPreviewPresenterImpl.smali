.class public final Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;
.super Ljava/lang/Object;
.source "PrintPreviewPresenterImpl.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇8o8o〇:Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8:F

.field private final OO0o〇〇〇〇0:Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl$printSettingListener$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private Oo08:Z

.field private oO80:I

.field private o〇0:Z

.field private final 〇080:Landroid/app/Activity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇80〇808〇O:Lcom/intsig/camscanner/printer/PrintClient;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:Lcom/intsig/camscanner/printer/contract/IPrintPreviewView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o〇:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/printer/model/PrintImageData;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇〇888:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇8o8o〇:Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/intsig/camscanner/printer/contract/IPrintPreviewView;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/intsig/camscanner/printer/contract/IPrintPreviewView;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "iPrintPreviewView"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇080:Landroid/app/Activity;

    .line 15
    .line 16
    iput-object p2, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇o00〇〇Oo:Lcom/intsig/camscanner/printer/contract/IPrintPreviewView;

    .line 17
    .line 18
    new-instance p1, Ljava/util/ArrayList;

    .line 19
    .line 20
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 21
    .line 22
    .line 23
    iput-object p1, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇o〇:Ljava/util/List;

    .line 24
    .line 25
    const/high16 p1, 0x3f800000    # 1.0f

    .line 26
    .line 27
    iput p1, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->O8:F

    .line 28
    .line 29
    const/4 p1, 0x1

    .line 30
    iput-boolean p1, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->Oo08:Z

    .line 31
    .line 32
    iput-boolean p1, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇〇888:Z

    .line 33
    .line 34
    iput p1, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->oO80:I

    .line 35
    .line 36
    new-instance p1, Lcom/intsig/camscanner/printer/PrintClient;

    .line 37
    .line 38
    invoke-direct {p1}, Lcom/intsig/camscanner/printer/PrintClient;-><init>()V

    .line 39
    .line 40
    .line 41
    iput-object p1, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇80〇808〇O:Lcom/intsig/camscanner/printer/PrintClient;

    .line 42
    .line 43
    new-instance p1, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl$printSettingListener$1;

    .line 44
    .line 45
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl$printSettingListener$1;-><init>(Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;)V

    .line 46
    .line 47
    .line 48
    iput-object p1, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl$printSettingListener$1;

    .line 49
    .line 50
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static final synthetic 〇080(Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;)Ljava/util/List;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇o〇:Ljava/util/List;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->Oo08:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇o〇()V
    .locals 15

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇〇888()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    sget-object v1, Lcom/intsig/developer/printer/PrinterAdapterImpl;->〇080:Lcom/intsig/developer/printer/PrinterAdapterImpl;

    .line 6
    .line 7
    invoke-virtual {v1}, Lcom/intsig/developer/printer/PrinterAdapterImpl;->o〇0()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    const/4 v9, 0x2

    .line 12
    new-array v10, v9, [I

    .line 13
    .line 14
    fill-array-data v10, :array_0

    .line 15
    .line 16
    .line 17
    new-array v11, v9, [I

    .line 18
    .line 19
    fill-array-data v11, :array_1

    .line 20
    .line 21
    .line 22
    move-object v2, v0

    .line 23
    check-cast v2, Ljava/lang/Iterable;

    .line 24
    .line 25
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 26
    .line 27
    .line 28
    move-result-object v12

    .line 29
    const/4 v2, 0x0

    .line 30
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    .line 31
    .line 32
    .line 33
    move-result v3

    .line 34
    const/4 v13, 0x1

    .line 35
    if-eqz v3, :cond_1

    .line 36
    .line 37
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 38
    .line 39
    .line 40
    move-result-object v3

    .line 41
    check-cast v3, Lcom/intsig/camscanner/printer/model/PrintImageData;

    .line 42
    .line 43
    int-to-float v4, v9

    .line 44
    const v5, 0x40733333    # 3.8f

    .line 45
    .line 46
    .line 47
    mul-float v5, v5, v4

    .line 48
    .line 49
    add-float v14, v2, v5

    .line 50
    .line 51
    invoke-virtual {v3}, Lcom/intsig/camscanner/printer/model/PrintImageData;->getImagePath()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v2

    .line 55
    const/4 v4, 0x0

    .line 56
    invoke-static {v2, v4}, Lcom/intsig/utils/ImageUtil;->Oooo8o0〇(Ljava/lang/String;Z)[I

    .line 57
    .line 58
    .line 59
    move-result-object v2

    .line 60
    invoke-static {v10, v4}, Ljava/util/Arrays;->fill([II)V

    .line 61
    .line 62
    .line 63
    invoke-static {v11, v4}, Ljava/util/Arrays;->fill([II)V

    .line 64
    .line 65
    .line 66
    if-eqz v2, :cond_0

    .line 67
    .line 68
    invoke-virtual {v3}, Lcom/intsig/camscanner/printer/model/PrintImageData;->getRotation()I

    .line 69
    .line 70
    .line 71
    move-result v3

    .line 72
    const/4 v5, -0x1

    .line 73
    iget-boolean v8, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇〇888:Z

    .line 74
    .line 75
    move v4, v1

    .line 76
    move-object v6, v10

    .line 77
    move-object v7, v11

    .line 78
    invoke-static/range {v2 .. v8}, Lcom/intsig/camscanner/printer/PrintUtil;->o800o8O([IIII[I[IZ)V

    .line 79
    .line 80
    .line 81
    sget-object v2, Lcom/intsig/developer/printer/PrinterAdapterImpl;->〇080:Lcom/intsig/developer/printer/PrinterAdapterImpl;

    .line 82
    .line 83
    aget v3, v10, v13

    .line 84
    .line 85
    invoke-virtual {v2, v3}, Lcom/intsig/developer/printer/PrinterAdapterImpl;->〇0〇O0088o(I)I

    .line 86
    .line 87
    .line 88
    move-result v2

    .line 89
    int-to-float v2, v2

    .line 90
    add-float/2addr v14, v2

    .line 91
    :cond_0
    move v2, v14

    .line 92
    goto :goto_0

    .line 93
    :cond_1
    iget v1, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->oO80:I

    .line 94
    .line 95
    int-to-float v1, v1

    .line 96
    mul-float v2, v2, v1

    .line 97
    .line 98
    iget-boolean v1, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->o〇0:Z

    .line 99
    .line 100
    if-eqz v1, :cond_2

    .line 101
    .line 102
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 103
    .line 104
    .line 105
    move-result v0

    .line 106
    iget v1, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->oO80:I

    .line 107
    .line 108
    mul-int v0, v0, v1

    .line 109
    .line 110
    sub-int/2addr v0, v13

    .line 111
    int-to-float v0, v0

    .line 112
    const v1, 0x3e4ccccd    # 0.2f

    .line 113
    .line 114
    .line 115
    mul-float v0, v0, v1

    .line 116
    .line 117
    add-float/2addr v2, v0

    .line 118
    :cond_2
    iput v2, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->O8:F

    .line 119
    .line 120
    return-void

    .line 121
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    :array_1
    .array-data 4
        0x0
        0x0
    .end array-data
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method


# virtual methods
.method public final O8()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->o〇0:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final OO0o〇〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇〇888:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public OO0o〇〇〇〇0(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/printer/model/PrintImageData;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "data"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇o〇:Ljava/util/List;

    .line 7
    .line 8
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇o〇:Ljava/util/List;

    .line 15
    .line 16
    check-cast p1, Ljava/util/Collection;

    .line 17
    .line 18
    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 19
    .line 20
    .line 21
    :cond_0
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final Oo08()Lcom/intsig/camscanner/printer/contract/IPrintPreviewView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇o00〇〇Oo:Lcom/intsig/camscanner/printer/contract/IPrintPreviewView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final Oooo8o0〇(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->oO80:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final getActivity()Landroid/app/Activity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇080:Landroid/app/Activity;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public oO80()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇080:Landroid/app/Activity;

    .line 2
    .line 3
    instance-of v1, v0, Lcom/intsig/camscanner/printer/PrintHomeActivity;

    .line 4
    .line 5
    if-eqz v1, :cond_1

    .line 6
    .line 7
    check-cast v0, Lcom/intsig/camscanner/printer/PrintHomeActivity;

    .line 8
    .line 9
    new-instance v1, Landroid/os/Bundle;

    .line 10
    .line 11
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 12
    .line 13
    .line 14
    sget-object v2, Lcom/intsig/camscanner/printer/viewmodel/PrinterConnectViewModel;->〇080OO8〇0:Lcom/intsig/camscanner/printer/viewmodel/PrinterConnectViewModel$Companion;

    .line 15
    .line 16
    invoke-virtual {v2}, Lcom/intsig/camscanner/printer/viewmodel/PrinterConnectViewModel$Companion;->〇080()Lcom/intsig/camscanner/printer/model/PrinterPropertyData;

    .line 17
    .line 18
    .line 19
    move-result-object v2

    .line 20
    if-nez v2, :cond_0

    .line 21
    .line 22
    const-string v2, "extra_finish_on_connect"

    .line 23
    .line 24
    const/4 v3, 0x1

    .line 25
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 26
    .line 27
    .line 28
    :cond_0
    sget-object v2, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 29
    .line 30
    const/4 v2, 0x4

    .line 31
    invoke-virtual {v0, v2, v1}, Lcom/intsig/camscanner/printer/PrintHomeActivity;->O0〇(ILandroid/os/Bundle;)V

    .line 32
    .line 33
    .line 34
    :cond_1
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final o〇0()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->oO80:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇80〇808〇O(Landroidx/fragment/app/FragmentManager;)V
    .locals 9
    .param p1    # Landroidx/fragment/app/FragmentManager;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "supportFragmentManager"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/intsig/camscanner/printer/fragment/PrintSettingFragment;->O0O:Lcom/intsig/camscanner/printer/fragment/PrintSettingFragment$Companion;

    .line 7
    .line 8
    iget-object v3, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇o〇:Ljava/util/List;

    .line 9
    .line 10
    iget-object v4, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->OO0o〇〇〇〇0:Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl$printSettingListener$1;

    .line 11
    .line 12
    iget v5, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->oO80:I

    .line 13
    .line 14
    iget-boolean v6, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->o〇0:Z

    .line 15
    .line 16
    iget-boolean v7, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇〇888:Z

    .line 17
    .line 18
    iget-boolean v8, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->Oo08:Z

    .line 19
    .line 20
    move-object v2, p1

    .line 21
    invoke-virtual/range {v1 .. v8}, Lcom/intsig/camscanner/printer/fragment/PrintSettingFragment$Companion;->〇080(Landroidx/fragment/app/FragmentManager;Ljava/util/List;Lcom/intsig/camscanner/printer/contract/PrintSettingListener;IZZZ)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public 〇8o8o〇()V
    .locals 5

    .line 1
    sget-object v0, Lcom/intsig/developer/printer/PrinterAdapterImpl;->〇080:Lcom/intsig/developer/printer/PrinterAdapterImpl;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/developer/printer/PrinterAdapterImpl;->oO80()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    const-string v0, "PrintClient"

    .line 10
    .line 11
    const-string v1, "showPrintProgressDialog do not connect printer"

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    sget-object v0, Lcom/intsig/camscanner/printer/PrintClient;->Oo08:Lcom/intsig/camscanner/printer/PrintClient$Companion;

    .line 17
    .line 18
    iget-object v1, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇080:Landroid/app/Activity;

    .line 19
    .line 20
    const/4 v2, 0x2

    .line 21
    const/4 v3, 0x0

    .line 22
    const/4 v4, 0x0

    .line 23
    invoke-static {v0, v1, v4, v2, v3}, Lcom/intsig/camscanner/printer/PrintClient$Companion;->Oo08(Lcom/intsig/camscanner/printer/PrintClient$Companion;Landroid/app/Activity;ZILjava/lang/Object;)V

    .line 24
    .line 25
    .line 26
    return-void

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇80〇808〇O:Lcom/intsig/camscanner/printer/PrintClient;

    .line 28
    .line 29
    iget-object v1, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇080:Landroid/app/Activity;

    .line 30
    .line 31
    new-instance v2, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl$requestPrint$1;

    .line 32
    .line 33
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl$requestPrint$1;-><init>(Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;)V

    .line 34
    .line 35
    .line 36
    iget-boolean v3, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->o〇0:Z

    .line 37
    .line 38
    iget-boolean v4, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇〇888:Z

    .line 39
    .line 40
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/intsig/camscanner/printer/PrintClient;->〇8o8o〇(Landroid/app/Activity;Lcom/intsig/camscanner/printer/contract/IPreparePrintListener;ZZ)V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇O00(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇o〇:Ljava/util/List;

    .line 2
    .line 3
    check-cast v0, Ljava/lang/Iterable;

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    check-cast v1, Lcom/intsig/camscanner/printer/model/PrintImageData;

    .line 20
    .line 21
    invoke-virtual {v1, p1}, Lcom/intsig/camscanner/printer/model/PrintImageData;->setModifyEnhanceMode(I)V

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇o00〇〇Oo:Lcom/intsig/camscanner/printer/contract/IPrintPreviewView;

    .line 26
    .line 27
    invoke-interface {p1}, Lcom/intsig/camscanner/printer/contract/IPrintPreviewView;->o8oOOo()V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final 〇O8o08O(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->o〇0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇O〇()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇o〇:Ljava/util/List;

    .line 2
    .line 3
    check-cast v0, Ljava/lang/Iterable;

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    check-cast v1, Lcom/intsig/camscanner/printer/model/PrintImageData;

    .line 20
    .line 21
    invoke-virtual {v1}, Lcom/intsig/camscanner/printer/model/PrintImageData;->getRotation()I

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    add-int/lit16 v2, v2, 0x10e

    .line 26
    .line 27
    rem-int/lit16 v2, v2, 0x168

    .line 28
    .line 29
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/printer/model/PrintImageData;->setRotation(I)V

    .line 30
    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇o00〇〇Oo:Lcom/intsig/camscanner/printer/contract/IPrintPreviewView;

    .line 34
    .line 35
    invoke-interface {v0}, Lcom/intsig/camscanner/printer/contract/IPrintPreviewView;->o8oOOo()V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇〇808〇()Z
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->o〇0:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇〇888()Ljava/util/List;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    iget v1, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->oO80:I

    .line 14
    .line 15
    mul-int v0, v0, v1

    .line 16
    .line 17
    const/4 v1, 0x1

    .line 18
    if-le v0, v1, :cond_0

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v1, 0x0

    .line 22
    :goto_0
    return v1
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇〇888()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/printer/model/PrintImageData;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇o〇:Ljava/util/List;

    .line 2
    .line 3
    check-cast v0, Ljava/lang/Iterable;

    .line 4
    .line 5
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    check-cast v1, Lcom/intsig/camscanner/printer/model/PrintImageData;

    .line 20
    .line 21
    iget-boolean v2, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇〇888:Z

    .line 22
    .line 23
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/printer/model/PrintImageData;->setEnableDottedPaper(Z)V

    .line 24
    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇o〇:Ljava/util/List;

    .line 28
    .line 29
    check-cast v0, Ljava/lang/Iterable;

    .line 30
    .line 31
    new-instance v1, Ljava/util/ArrayList;

    .line 32
    .line 33
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 34
    .line 35
    .line 36
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    :cond_1
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    if-eqz v2, :cond_2

    .line 45
    .line 46
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    move-object v3, v2

    .line 51
    check-cast v3, Lcom/intsig/camscanner/printer/model/PrintImageData;

    .line 52
    .line 53
    invoke-virtual {v3}, Lcom/intsig/camscanner/printer/model/PrintImageData;->getChecked()Z

    .line 54
    .line 55
    .line 56
    move-result v3

    .line 57
    if-eqz v3, :cond_1

    .line 58
    .line 59
    invoke-interface {v1, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 60
    .line 61
    .line 62
    goto :goto_1

    .line 63
    :cond_2
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->〇00O0O0(Ljava/util/Collection;)Ljava/util/List;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    return-object v0
    .line 68
.end method

.method public 〇〇8O0〇8()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇o00〇〇Oo:Lcom/intsig/camscanner/printer/contract/IPrintPreviewView;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->oO80:I

    .line 4
    .line 5
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/printer/contract/IPrintPreviewView;->〇08〇o0O(I)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇o00〇〇Oo:Lcom/intsig/camscanner/printer/contract/IPrintPreviewView;

    .line 9
    .line 10
    iget-boolean v1, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇〇888:Z

    .line 11
    .line 12
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/printer/contract/IPrintPreviewView;->O8OO08o(Z)V

    .line 13
    .line 14
    .line 15
    iget-boolean v0, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->Oo08:Z

    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    if-eqz v0, :cond_0

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇o〇:Ljava/util/List;

    .line 21
    .line 22
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    add-int/lit8 v0, v0, -0x1

    .line 27
    .line 28
    goto :goto_2

    .line 29
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇o〇:Ljava/util/List;

    .line 30
    .line 31
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    const/4 v2, -0x1

    .line 36
    const/4 v3, -0x1

    .line 37
    :goto_0
    if-ge v1, v0, :cond_4

    .line 38
    .line 39
    iget-object v4, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇o〇:Ljava/util/List;

    .line 40
    .line 41
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 42
    .line 43
    .line 44
    move-result-object v4

    .line 45
    check-cast v4, Lcom/intsig/camscanner/printer/model/PrintImageData;

    .line 46
    .line 47
    invoke-virtual {v4}, Lcom/intsig/camscanner/printer/model/PrintImageData;->getChecked()Z

    .line 48
    .line 49
    .line 50
    move-result v4

    .line 51
    if-eqz v4, :cond_3

    .line 52
    .line 53
    if-gez v3, :cond_1

    .line 54
    .line 55
    move v3, v1

    .line 56
    :cond_1
    if-gez v2, :cond_2

    .line 57
    .line 58
    move v2, v1

    .line 59
    goto :goto_1

    .line 60
    :cond_2
    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    .line 61
    .line 62
    .line 63
    move-result v2

    .line 64
    :cond_3
    :goto_1
    add-int/lit8 v1, v1, 0x1

    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_4
    move v0, v2

    .line 68
    move v1, v3

    .line 69
    :goto_2
    iget-object v2, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇o00〇〇Oo:Lcom/intsig/camscanner/printer/contract/IPrintPreviewView;

    .line 70
    .line 71
    iget-boolean v3, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->Oo08:Z

    .line 72
    .line 73
    invoke-interface {v2, v3, v1, v0}, Lcom/intsig/camscanner/printer/contract/IPrintPreviewView;->Oo0O080(ZII)V

    .line 74
    .line 75
    .line 76
    invoke-direct {p0}, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇o〇()V

    .line 77
    .line 78
    .line 79
    iget-object v0, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->〇o00〇〇Oo:Lcom/intsig/camscanner/printer/contract/IPrintPreviewView;

    .line 80
    .line 81
    iget v1, p0, Lcom/intsig/camscanner/printer/presenter/PrintPreviewPresenterImpl;->O8:F

    .line 82
    .line 83
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/printer/contract/IPrintPreviewView;->O0o〇〇Oo(F)V

    .line 84
    .line 85
    .line 86
    return-void
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

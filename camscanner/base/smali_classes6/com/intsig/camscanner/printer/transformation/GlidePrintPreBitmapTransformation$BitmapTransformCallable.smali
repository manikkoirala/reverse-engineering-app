.class final Lcom/intsig/camscanner/printer/transformation/GlidePrintPreBitmapTransformation$BitmapTransformCallable;
.super Ljava/lang/Object;
.source "GlidePrintPreBitmapTransformation.kt"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/printer/transformation/GlidePrintPreBitmapTransformation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "BitmapTransformCallable"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final OO:Lcom/intsig/camscanner/printer/model/PrintImageData;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o0:Lcom/bumptech/glide/load/engine/bitmap_recycle/BitmapPool;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Landroid/graphics/Bitmap;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/bumptech/glide/load/engine/bitmap_recycle/BitmapPool;Landroid/graphics/Bitmap;Lcom/intsig/camscanner/printer/model/PrintImageData;)V
    .locals 1
    .param p1    # Lcom/bumptech/glide/load/engine/bitmap_recycle/BitmapPool;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/Bitmap;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/camscanner/printer/model/PrintImageData;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "pool"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "toTransform"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const-string v0, "printImageData"

    .line 12
    .line 13
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    .line 18
    .line 19
    iput-object p1, p0, Lcom/intsig/camscanner/printer/transformation/GlidePrintPreBitmapTransformation$BitmapTransformCallable;->o0:Lcom/bumptech/glide/load/engine/bitmap_recycle/BitmapPool;

    .line 20
    .line 21
    iput-object p2, p0, Lcom/intsig/camscanner/printer/transformation/GlidePrintPreBitmapTransformation$BitmapTransformCallable;->〇OOo8〇0:Landroid/graphics/Bitmap;

    .line 22
    .line 23
    iput-object p3, p0, Lcom/intsig/camscanner/printer/transformation/GlidePrintPreBitmapTransformation$BitmapTransformCallable;->OO:Lcom/intsig/camscanner/printer/model/PrintImageData;

    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method


# virtual methods
.method public call()Landroid/graphics/Bitmap;
    .locals 11
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 3
    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    .line 4
    iget-object v3, p0, Lcom/intsig/camscanner/printer/transformation/GlidePrintPreBitmapTransformation$BitmapTransformCallable;->〇OOo8〇0:Landroid/graphics/Bitmap;

    invoke-static {v3}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->OO0o〇〇〇〇0(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v3

    if-nez v3, :cond_0

    iget-object v0, p0, Lcom/intsig/camscanner/printer/transformation/GlidePrintPreBitmapTransformation$BitmapTransformCallable;->〇OOo8〇0:Landroid/graphics/Bitmap;

    return-object v0

    .line 5
    :cond_0
    iget-object v4, p0, Lcom/intsig/camscanner/printer/transformation/GlidePrintPreBitmapTransformation$BitmapTransformCallable;->OO:Lcom/intsig/camscanner/printer/model/PrintImageData;

    invoke-virtual {v4}, Lcom/intsig/camscanner/printer/model/PrintImageData;->getModifyEnhanceMode()I

    move-result v4

    const-string v5, "GlidePrintPreBitmapTransformation"

    const/4 v6, -0x1

    if-eq v4, v6, :cond_1

    .line 6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v7

    .line 7
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerUtils;->initThreadContext()I

    move-result v4

    .line 8
    iget-object v9, p0, Lcom/intsig/camscanner/printer/transformation/GlidePrintPreBitmapTransformation$BitmapTransformCallable;->OO:Lcom/intsig/camscanner/printer/model/PrintImageData;

    invoke-virtual {v9}, Lcom/intsig/camscanner/printer/model/PrintImageData;->getModifyEnhanceMode()I

    move-result v9

    .line 9
    invoke-static {v4, v3, v9}, Lcom/intsig/camscanner/scanner/ScannerUtils;->enhanceImage(ILandroid/graphics/Bitmap;I)Z

    .line 10
    invoke-static {v4}, Lcom/intsig/camscanner/scanner/ScannerUtils;->destroyThreadContext(I)V

    .line 11
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v9

    sub-long/2addr v9, v7

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "transform enhanceImage costTime:"

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 12
    invoke-static {v5, v4}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    :cond_1
    iget-object v4, p0, Lcom/intsig/camscanner/printer/transformation/GlidePrintPreBitmapTransformation$BitmapTransformCallable;->OO:Lcom/intsig/camscanner/printer/model/PrintImageData;

    invoke-virtual {v4}, Lcom/intsig/camscanner/printer/model/PrintImageData;->getRotation()I

    move-result v4

    rem-int/lit16 v4, v4, 0xb4

    if-nez v4, :cond_2

    .line 14
    iget-object v4, p0, Lcom/intsig/camscanner/printer/transformation/GlidePrintPreBitmapTransformation$BitmapTransformCallable;->o0:Lcom/bumptech/glide/load/engine/bitmap_recycle/BitmapPool;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-interface {v4, v7, v8, v9}, Lcom/bumptech/glide/load/engine/bitmap_recycle/BitmapPool;->O8(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    goto :goto_0

    .line 15
    :cond_2
    iget-object v4, p0, Lcom/intsig/camscanner/printer/transformation/GlidePrintPreBitmapTransformation$BitmapTransformCallable;->o0:Lcom/bumptech/glide/load/engine/bitmap_recycle/BitmapPool;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    sget-object v9, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-interface {v4, v7, v8, v9}, Lcom/bumptech/glide/load/engine/bitmap_recycle/BitmapPool;->O8(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    :goto_0
    const-string v7, "if (printImageData.rotat\u2026.ARGB_8888)\n            }"

    .line 16
    invoke-static {v4, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 17
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    neg-int v7, v7

    int-to-float v7, v7

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    neg-int v9, v9

    int-to-float v9, v9

    div-float/2addr v9, v8

    invoke-virtual {v2, v7, v9}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 18
    iget-object v7, p0, Lcom/intsig/camscanner/printer/transformation/GlidePrintPreBitmapTransformation$BitmapTransformCallable;->OO:Lcom/intsig/camscanner/printer/model/PrintImageData;

    invoke-virtual {v7}, Lcom/intsig/camscanner/printer/model/PrintImageData;->getRotation()I

    move-result v7

    int-to-float v7, v7

    invoke-virtual {v2, v7}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 19
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v7, v8

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v9, v8

    invoke-virtual {v2, v7, v9}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 20
    new-instance v7, Landroid/graphics/Canvas;

    invoke-direct {v7, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 21
    invoke-virtual {v7, v6}, Landroid/graphics/Canvas;->drawColor(I)V

    const/4 v6, 0x0

    .line 22
    invoke-virtual {v7, v3, v2, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    const/4 v2, 0x1

    new-array v6, v2, [Landroid/graphics/Bitmap;

    const/4 v8, 0x0

    aput-object v3, v6, v8

    .line 23
    invoke-static {v6}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->o0ooO([Landroid/graphics/Bitmap;)V

    .line 24
    invoke-virtual {v7}, Landroid/graphics/Canvas;->save()I

    .line 25
    iget-object v3, p0, Lcom/intsig/camscanner/printer/transformation/GlidePrintPreBitmapTransformation$BitmapTransformCallable;->OO:Lcom/intsig/camscanner/printer/model/PrintImageData;

    invoke-virtual {v3}, Lcom/intsig/camscanner/printer/model/PrintImageData;->getModifyEnhanceMode()I

    move-result v3

    const/16 v6, 0x13

    if-eq v3, v6, :cond_4

    .line 26
    iget-object v3, p0, Lcom/intsig/camscanner/printer/transformation/GlidePrintPreBitmapTransformation$BitmapTransformCallable;->OO:Lcom/intsig/camscanner/printer/model/PrintImageData;

    invoke-virtual {v3}, Lcom/intsig/camscanner/printer/model/PrintImageData;->getModifyEnhanceMode()I

    move-result v3

    const/16 v6, 0x10

    if-eq v3, v6, :cond_4

    .line 27
    sget-object v3, Lcom/intsig/developer/printer/PrinterAdapterImpl;->〇080:Lcom/intsig/developer/printer/PrinterAdapterImpl;

    invoke-virtual {v3, v4}, Lcom/intsig/developer/printer/PrinterAdapterImpl;->O8(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v3

    if-nez v3, :cond_3

    new-array v0, v2, [Landroid/graphics/Bitmap;

    aput-object v4, v0, v8

    .line 28
    invoke-static {v0}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->o0ooO([Landroid/graphics/Bitmap;)V

    .line 29
    iget-object v0, p0, Lcom/intsig/camscanner/printer/transformation/GlidePrintPreBitmapTransformation$BitmapTransformCallable;->〇OOo8〇0:Landroid/graphics/Bitmap;

    return-object v0

    .line 30
    :cond_3
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    new-array v2, v2, [Landroid/graphics/Bitmap;

    aput-object v4, v2, v8

    .line 31
    invoke-static {v2}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->o0ooO([Landroid/graphics/Bitmap;)V

    move-object v4, v3

    .line 32
    :cond_4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "transform costTime:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    return-object v4
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/printer/transformation/GlidePrintPreBitmapTransformation$BitmapTransformCallable;->call()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.class public final Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$RangeSelfSelectHolder;
.super Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
.source "PrintSettingDetailAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "RangeSelfSelectHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final OO:Lcom/intsig/camscanner/view/WheelView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o0:Lcom/intsig/camscanner/view/DispatchConstraintLayout;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08O〇00〇o:Lcom/intsig/camscanner/view/WheelView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Lcom/intsig/camscanner/view/DispatchConstraintLayout;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;I)V
    .locals 8
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "itemView"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1}, Landroidx/recyclerview/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 7
    .line 8
    .line 9
    const v0, 0x7f0a0533

    .line 10
    .line 11
    .line 12
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    const-string v1, "itemView.findViewById(R.id.dispatch_layout_start)"

    .line 17
    .line 18
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    check-cast v0, Lcom/intsig/camscanner/view/DispatchConstraintLayout;

    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$RangeSelfSelectHolder;->o0:Lcom/intsig/camscanner/view/DispatchConstraintLayout;

    .line 24
    .line 25
    const v1, 0x7f0a0532

    .line 26
    .line 27
    .line 28
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    const-string v2, "itemView.findViewById(R.id.dispatch_layout_end)"

    .line 33
    .line 34
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    check-cast v1, Lcom/intsig/camscanner/view/DispatchConstraintLayout;

    .line 38
    .line 39
    iput-object v1, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$RangeSelfSelectHolder;->〇OOo8〇0:Lcom/intsig/camscanner/view/DispatchConstraintLayout;

    .line 40
    .line 41
    const v2, 0x7f0a1aba

    .line 42
    .line 43
    .line 44
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 45
    .line 46
    .line 47
    move-result-object v2

    .line 48
    move-object v3, v2

    .line 49
    check-cast v3, Lcom/intsig/camscanner/view/WheelView;

    .line 50
    .line 51
    sget-object v4, Lcom/intsig/camscanner/view/listener/DispatchTouchEventListener;->〇080:Lcom/intsig/camscanner/view/listener/DispatchTouchEventListener$Companion;

    .line 52
    .line 53
    invoke-virtual {v4, v0}, Lcom/intsig/camscanner/view/listener/DispatchTouchEventListener$Companion;->〇080(Landroid/view/View;)Lcom/intsig/camscanner/view/listener/DispatchTouchEventListener;

    .line 54
    .line 55
    .line 56
    move-result-object v5

    .line 57
    invoke-virtual {v0, v5}, Lcom/intsig/camscanner/view/DispatchConstraintLayout;->setDispatchTouchEventListener(Lcom/intsig/camscanner/view/listener/DispatchTouchEventListener;)V

    .line 58
    .line 59
    .line 60
    new-instance v0, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$PageIndexWheelAdapter;

    .line 61
    .line 62
    invoke-direct {v0, p2}, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$PageIndexWheelAdapter;-><init>(I)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {v3, v0}, Lcom/intsig/camscanner/view/WheelView;->setAdapter(Lcom/contrarywind/adapter/WheelAdapter;)V

    .line 66
    .line 67
    .line 68
    const/4 v0, 0x0

    .line 69
    invoke-virtual {v3, v0}, Lcom/intsig/camscanner/view/WheelView;->setCyclic(Z)V

    .line 70
    .line 71
    .line 72
    const v5, 0x3f4ccccd    # 0.8f

    .line 73
    .line 74
    .line 75
    invoke-virtual {v3, v5}, Lcom/intsig/camscanner/view/WheelView;->setScaleContent(F)V

    .line 76
    .line 77
    .line 78
    const/4 v6, 0x5

    .line 79
    invoke-virtual {v3, v6}, Lcom/intsig/camscanner/view/WheelView;->setItemsVisibleCount(I)V

    .line 80
    .line 81
    .line 82
    invoke-virtual {v3, v0}, Lcom/intsig/camscanner/view/WheelView;->〇80〇808〇O(Z)V

    .line 83
    .line 84
    .line 85
    const-string v7, "itemView.findViewById<Wh\u2026terLabel(false)\n        }"

    .line 86
    .line 87
    invoke-static {v2, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 88
    .line 89
    .line 90
    iput-object v3, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$RangeSelfSelectHolder;->OO:Lcom/intsig/camscanner/view/WheelView;

    .line 91
    .line 92
    const v2, 0x7f0a1ab9

    .line 93
    .line 94
    .line 95
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 96
    .line 97
    .line 98
    move-result-object p1

    .line 99
    move-object v2, p1

    .line 100
    check-cast v2, Lcom/intsig/camscanner/view/WheelView;

    .line 101
    .line 102
    invoke-virtual {v4, v1}, Lcom/intsig/camscanner/view/listener/DispatchTouchEventListener$Companion;->〇080(Landroid/view/View;)Lcom/intsig/camscanner/view/listener/DispatchTouchEventListener;

    .line 103
    .line 104
    .line 105
    move-result-object v3

    .line 106
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/view/DispatchConstraintLayout;->setDispatchTouchEventListener(Lcom/intsig/camscanner/view/listener/DispatchTouchEventListener;)V

    .line 107
    .line 108
    .line 109
    new-instance v1, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$PageIndexWheelAdapter;

    .line 110
    .line 111
    invoke-direct {v1, p2}, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$PageIndexWheelAdapter;-><init>(I)V

    .line 112
    .line 113
    .line 114
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/view/WheelView;->setAdapter(Lcom/contrarywind/adapter/WheelAdapter;)V

    .line 115
    .line 116
    .line 117
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/view/WheelView;->setCyclic(Z)V

    .line 118
    .line 119
    .line 120
    invoke-virtual {v2, v5}, Lcom/intsig/camscanner/view/WheelView;->setScaleContent(F)V

    .line 121
    .line 122
    .line 123
    invoke-virtual {v2, v6}, Lcom/intsig/camscanner/view/WheelView;->setItemsVisibleCount(I)V

    .line 124
    .line 125
    .line 126
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/view/WheelView;->〇80〇808〇O(Z)V

    .line 127
    .line 128
    .line 129
    invoke-static {p1, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 130
    .line 131
    .line 132
    iput-object v2, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$RangeSelfSelectHolder;->〇08O〇00〇o:Lcom/intsig/camscanner/view/WheelView;

    .line 133
    .line 134
    return-void
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method


# virtual methods
.method public final O〇8O8〇008()Lcom/intsig/camscanner/view/WheelView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$RangeSelfSelectHolder;->OO:Lcom/intsig/camscanner/view/WheelView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇00()Lcom/intsig/camscanner/view/WheelView;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$RangeSelfSelectHolder;->〇08O〇00〇o:Lcom/intsig/camscanner/view/WheelView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

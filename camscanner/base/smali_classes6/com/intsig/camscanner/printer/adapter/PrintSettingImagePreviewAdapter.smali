.class public final Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter;
.super Lcom/alibaba/android/vlayout/DelegateAdapter$Adapter;
.source "PrintSettingPreviewAdatper.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$PreThumbDataAdapter;,
        Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$SettingViewHolder;,
        Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$Companion;,
        Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$PreviewImageViewHolder;,
        Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$PreImageData;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/alibaba/android/vlayout/DelegateAdapter$Adapter<",
        "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final oOo〇8o008:Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Landroidx/recyclerview/widget/RecyclerView;

.field private OO:I

.field private final o0:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/printer/model/PrintImageData;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇00O:Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$PreThumbDataAdapter;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇080OO8〇0:Z

.field private final 〇08O〇00〇o:Landroidx/recyclerview/widget/RecyclerView$RecycledViewPool;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇0O:Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$itemDecoration$1;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇OOo8〇0:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter;->oOo〇8o008:Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Ljava/util/List;IILandroidx/recyclerview/widget/RecyclerView$RecycledViewPool;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Landroidx/recyclerview/widget/RecyclerView$RecycledViewPool;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/printer/model/PrintImageData;",
            ">;II",
            "Landroidx/recyclerview/widget/RecyclerView$RecycledViewPool;",
            ")V"
        }
    .end annotation

    .line 1
    const-string v0, "printImageDataList"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "recycledViewPool"

    .line 7
    .line 8
    invoke-static {p4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/alibaba/android/vlayout/DelegateAdapter$Adapter;-><init>()V

    .line 12
    .line 13
    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter;->o0:Ljava/util/List;

    .line 15
    .line 16
    iput p2, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter;->〇OOo8〇0:I

    .line 17
    .line 18
    iput p3, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter;->OO:I

    .line 19
    .line 20
    iput-object p4, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter;->〇08O〇00〇o:Landroidx/recyclerview/widget/RecyclerView$RecycledViewPool;

    .line 21
    .line 22
    new-instance p2, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$PreThumbDataAdapter;

    .line 23
    .line 24
    invoke-direct {p2, p1}, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$PreThumbDataAdapter;-><init>(Ljava/util/List;)V

    .line 25
    .line 26
    .line 27
    iput-object p2, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter;->o〇00O:Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$PreThumbDataAdapter;

    .line 28
    .line 29
    const/4 p1, 0x1

    .line 30
    iput-boolean p1, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter;->〇080OO8〇0:Z

    .line 31
    .line 32
    new-instance p1, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$itemDecoration$1;

    .line 33
    .line 34
    invoke-direct {p1}, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$itemDecoration$1;-><init>()V

    .line 35
    .line 36
    .line 37
    iput-object p1, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter;->〇0O:Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$itemDecoration$1;

    .line 38
    .line 39
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private final o800o8O(Landroidx/recyclerview/widget/RecyclerView;)V
    .locals 5

    .line 1
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    instance-of v0, v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 6
    .line 7
    if-eqz v0, :cond_2

    .line 8
    .line 9
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const-string v1, "null cannot be cast to non-null type androidx.recyclerview.widget.LinearLayoutManager"

    .line 14
    .line 15
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    check-cast v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 19
    .line 20
    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstCompletelyVisibleItemPosition()I

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastCompletelyVisibleItemPosition()I

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    iget-object v3, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter;->o〇00O:Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$PreThumbDataAdapter;

    .line 29
    .line 30
    iget v4, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter;->〇OOo8〇0:I

    .line 31
    .line 32
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$PreThumbDataAdapter;->〇O00(I)I

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    const/4 v4, 0x0

    .line 37
    if-gt v1, v3, :cond_0

    .line 38
    .line 39
    if-gt v3, v2, :cond_0

    .line 40
    .line 41
    const/4 v4, 0x1

    .line 42
    :cond_0
    if-nez v4, :cond_1

    .line 43
    .line 44
    invoke-virtual {p1, v3}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    .line 45
    .line 46
    .line 47
    new-instance v1, Landroidx/recyclerview/widget/RecyclerView$State;

    .line 48
    .line 49
    invoke-direct {v1}, Landroidx/recyclerview/widget/RecyclerView$State;-><init>()V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v0, p1, v1, v3}, Landroidx/recyclerview/widget/LinearLayoutManager;->smoothScrollToPosition(Landroidx/recyclerview/widget/RecyclerView;Landroidx/recyclerview/widget/RecyclerView$State;I)V

    .line 53
    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    .line 57
    .line 58
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 59
    .line 60
    .line 61
    const-string v0, "onBindViewHolder startPagePosition:"

    .line 62
    .line 63
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    const-string v0, ", firstCompletelyVisibleItemPosition:"

    .line 70
    .line 71
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    const-string v0, ", lastCompletelyVisibleItemPosition:"

    .line 78
    .line 79
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object p1

    .line 89
    const-string v0, "PrintSettingImagePreviewAdapter"

    .line 90
    .line 91
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    :cond_2
    :goto_0
    return-void
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method


# virtual methods
.method public final OoO8(II)V
    .locals 1

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter;->〇OOo8〇0:I

    .line 2
    .line 3
    iput p2, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter;->OO:I

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter;->O8o08O8O:Landroidx/recyclerview/widget/RecyclerView;

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter;->o800o8O(Landroidx/recyclerview/widget/RecyclerView;)V

    .line 10
    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter;->o〇00O:Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$PreThumbDataAdapter;

    .line 13
    .line 14
    invoke-virtual {v0, p1, p2}, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$PreThumbDataAdapter;->o〇O8〇〇o(II)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public getItemCount()I
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getItemViewType(I)I
    .locals 0

    .line 1
    const/4 p1, 0x2

    .line 2
    return p1
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0
    .param p1    # Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string p2, "holder"

    .line 2
    .line 3
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    instance-of p2, p1, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$SettingViewHolder;

    .line 7
    .line 8
    if-nez p2, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    iget-boolean p2, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter;->〇080OO8〇0:Z

    .line 12
    .line 13
    if-eqz p2, :cond_1

    .line 14
    .line 15
    const/4 p2, 0x0

    .line 16
    iput-boolean p2, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter;->〇080OO8〇0:Z

    .line 17
    .line 18
    check-cast p1, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$SettingViewHolder;

    .line 19
    .line 20
    invoke-virtual {p1}, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$SettingViewHolder;->〇00()Landroidx/recyclerview/widget/RecyclerView;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter;->o800o8O(Landroidx/recyclerview/widget/RecyclerView;)V

    .line 25
    .line 26
    .line 27
    :cond_1
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string p2, "parent"

    .line 2
    .line 3
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 7
    .line 8
    .line 9
    move-result-object p2

    .line 10
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 11
    .line 12
    .line 13
    move-result-object p2

    .line 14
    const v0, 0x7f0d056b

    .line 15
    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    const-string p2, "from(parent.context)\n   \u2026g_preview, parent, false)"

    .line 23
    .line 24
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    new-instance p2, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$SettingViewHolder;

    .line 28
    .line 29
    invoke-direct {p2, p1}, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$SettingViewHolder;-><init>(Landroid/view/View;)V

    .line 30
    .line 31
    .line 32
    new-instance p1, Landroidx/recyclerview/widget/LinearSnapHelper;

    .line 33
    .line 34
    invoke-direct {p1}, Landroidx/recyclerview/widget/LinearSnapHelper;-><init>()V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p2}, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$SettingViewHolder;->〇00()Landroidx/recyclerview/widget/RecyclerView;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/SnapHelper;->attachToRecyclerView(Landroidx/recyclerview/widget/RecyclerView;)V

    .line 42
    .line 43
    .line 44
    invoke-virtual {p2}, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$SettingViewHolder;->〇00()Landroidx/recyclerview/widget/RecyclerView;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    invoke-virtual {p1, v1}, Landroidx/recyclerview/widget/RecyclerView;->setNestedScrollingEnabled(Z)V

    .line 49
    .line 50
    .line 51
    invoke-virtual {p2}, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$SettingViewHolder;->〇00()Landroidx/recyclerview/widget/RecyclerView;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    iget-object v0, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter;->〇08O〇00〇o:Landroidx/recyclerview/widget/RecyclerView$RecycledViewPool;

    .line 56
    .line 57
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setRecycledViewPool(Landroidx/recyclerview/widget/RecyclerView$RecycledViewPool;)V

    .line 58
    .line 59
    .line 60
    invoke-virtual {p2}, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$SettingViewHolder;->〇00()Landroidx/recyclerview/widget/RecyclerView;

    .line 61
    .line 62
    .line 63
    move-result-object p1

    .line 64
    const/4 v0, 0x1

    .line 65
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 66
    .line 67
    .line 68
    new-instance p1, Lcom/intsig/recycler/layoutmanager/CenterLayoutManager;

    .line 69
    .line 70
    invoke-virtual {p2}, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$SettingViewHolder;->〇00()Landroidx/recyclerview/widget/RecyclerView;

    .line 71
    .line 72
    .line 73
    move-result-object v0

    .line 74
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    invoke-direct {p1, v0}, Lcom/intsig/recycler/layoutmanager/CenterLayoutManager;-><init>(Landroid/content/Context;)V

    .line 79
    .line 80
    .line 81
    invoke-virtual {p1, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->setOrientation(I)V

    .line 82
    .line 83
    .line 84
    invoke-virtual {p2}, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$SettingViewHolder;->〇00()Landroidx/recyclerview/widget/RecyclerView;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    invoke-virtual {v0, p1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 89
    .line 90
    .line 91
    invoke-virtual {p2}, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$SettingViewHolder;->〇00()Landroidx/recyclerview/widget/RecyclerView;

    .line 92
    .line 93
    .line 94
    move-result-object p1

    .line 95
    iget-object v0, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter;->o〇00O:Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$PreThumbDataAdapter;

    .line 96
    .line 97
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 98
    .line 99
    .line 100
    invoke-virtual {p2}, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$SettingViewHolder;->〇00()Landroidx/recyclerview/widget/RecyclerView;

    .line 101
    .line 102
    .line 103
    move-result-object p1

    .line 104
    iget-object v0, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter;->〇0O:Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$itemDecoration$1;

    .line 105
    .line 106
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 107
    .line 108
    .line 109
    invoke-virtual {p2}, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter$SettingViewHolder;->〇00()Landroidx/recyclerview/widget/RecyclerView;

    .line 110
    .line 111
    .line 112
    move-result-object p1

    .line 113
    iput-object p1, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter;->O8o08O8O:Landroidx/recyclerview/widget/RecyclerView;

    .line 114
    .line 115
    return-object p2
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public 〇0〇O0088o()Lcom/alibaba/android/vlayout/layout/LinearLayoutHelper;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/alibaba/android/vlayout/layout/LinearLayoutHelper;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/alibaba/android/vlayout/layout/LinearLayoutHelper;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public bridge synthetic 〇〇8O0〇8()Lcom/alibaba/android/vlayout/LayoutHelper;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/printer/adapter/PrintSettingImagePreviewAdapter;->〇0〇O0088o()Lcom/alibaba/android/vlayout/layout/LinearLayoutHelper;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

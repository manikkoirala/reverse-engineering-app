.class public final Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter;
.super Lcom/alibaba/android/vlayout/DelegateAdapter$Adapter;
.source "PrintSettingDetailAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$SettingViewHolder;,
        Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$ActionListener;,
        Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$PrintSettingDetailAdapter;,
        Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$Companion;,
        Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$SettingItemHolder;,
        Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$RangeSelfSelectHolder;,
        Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$PageIndexWheelAdapter;,
        Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$PageIndexWheelEntity;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/alibaba/android/vlayout/DelegateAdapter$Adapter<",
        "Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$SettingViewHolder;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final OO:Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final o0:Lcom/intsig/camscanner/printer/PrintSettingSp;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$PrintSettingDetailAdapter;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter;->OO:Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Lcom/intsig/camscanner/printer/PrintSettingSp;Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$ActionListener;)V
    .locals 2
    .param p1    # Lcom/intsig/camscanner/printer/PrintSettingSp;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "sp"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/alibaba/android/vlayout/DelegateAdapter$Adapter;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter;->o0:Lcom/intsig/camscanner/printer/PrintSettingSp;

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$PrintSettingDetailAdapter;

    .line 12
    .line 13
    const/4 v1, 0x2

    .line 14
    invoke-direct {v0, p1, v1, p2}, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$PrintSettingDetailAdapter;-><init>(Lcom/intsig/camscanner/printer/PrintSettingSp;ILcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$ActionListener;)V

    .line 15
    .line 16
    .line 17
    iput-object v0, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter;->〇OOo8〇0:Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$PrintSettingDetailAdapter;

    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public OoO8(Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$SettingViewHolder;I)V
    .locals 0
    .param p1    # Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$SettingViewHolder;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string p2, "holder"

    .line 2
    .line 3
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter;->〇OOo8〇0:Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$PrintSettingDetailAdapter;

    .line 7
    .line 8
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public getItemCount()I
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public getItemViewType(I)I
    .locals 0

    .line 1
    const/4 p1, 0x7

    .line 2
    return p1
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public o800o8O(Landroid/view/ViewGroup;I)Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$SettingViewHolder;
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string p2, "parent"

    .line 2
    .line 3
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 7
    .line 8
    .line 9
    move-result-object p2

    .line 10
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 11
    .line 12
    .line 13
    move-result-object p2

    .line 14
    const v0, 0x7f0d056b

    .line 15
    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    const-string p2, "from(parent.context)\n   \u2026g_preview, parent, false)"

    .line 23
    .line 24
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    new-instance p2, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$SettingViewHolder;

    .line 28
    .line 29
    invoke-direct {p2, p1}, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$SettingViewHolder;-><init>(Landroid/view/View;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {p2}, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$SettingViewHolder;->〇00()Landroidx/recyclerview/widget/RecyclerView;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    new-instance v0, Lcom/intsig/recycler/layoutmanager/CenterLayoutManager;

    .line 41
    .line 42
    invoke-direct {v0, p1}, Lcom/intsig/recycler/layoutmanager/CenterLayoutManager;-><init>(Landroid/content/Context;)V

    .line 43
    .line 44
    .line 45
    const/4 v1, 0x1

    .line 46
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->setOrientation(I)V

    .line 47
    .line 48
    .line 49
    invoke-virtual {p2}, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$SettingViewHolder;->〇00()Landroidx/recyclerview/widget/RecyclerView;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 54
    .line 55
    .line 56
    invoke-virtual {p2}, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$SettingViewHolder;->〇00()Landroidx/recyclerview/widget/RecyclerView;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    iget-object v1, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter;->〇OOo8〇0:Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$PrintSettingDetailAdapter;

    .line 61
    .line 62
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 63
    .line 64
    .line 65
    invoke-virtual {p2}, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$SettingViewHolder;->〇00()Landroidx/recyclerview/widget/RecyclerView;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    const v1, 0x7f080275

    .line 70
    .line 71
    .line 72
    invoke-virtual {p1, v1}, Landroid/content/Context;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    .line 73
    .line 74
    .line 75
    move-result-object v1

    .line 76
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {p2}, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$SettingViewHolder;->〇00()Landroidx/recyclerview/widget/RecyclerView;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 84
    .line 85
    .line 86
    move-result-object v0

    .line 87
    const/16 v1, 0x10

    .line 88
    .line 89
    invoke-static {p1, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 90
    .line 91
    .line 92
    move-result p1

    .line 93
    instance-of v1, v0, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 94
    .line 95
    if-eqz v1, :cond_0

    .line 96
    .line 97
    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 98
    .line 99
    invoke-virtual {v0, p1, p1, p1, p1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 100
    .line 101
    .line 102
    :cond_0
    return-object p2
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public bridge synthetic onBindViewHolder(Landroidx/recyclerview/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .line 1
    check-cast p1, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$SettingViewHolder;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter;->OoO8(Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$SettingViewHolder;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroidx/recyclerview/widget/RecyclerView$ViewHolder;
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter;->o800o8O(Landroid/view/ViewGroup;I)Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$SettingViewHolder;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    return-object p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final 〇0〇O0088o(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter;->〇OOo8〇0:Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$PrintSettingDetailAdapter;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/printer/adapter/PrintSettingDetailAdapter$PrintSettingDetailAdapter;->O8ooOoo〇(I)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇〇8O0〇8()Lcom/alibaba/android/vlayout/LayoutHelper;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/alibaba/android/vlayout/layout/LinearLayoutHelper;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/alibaba/android/vlayout/layout/LinearLayoutHelper;-><init>()V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

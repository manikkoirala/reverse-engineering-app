.class public final Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftResponse$CellProperty;
.super Ljava/lang/Object;
.source "OneTrialRenewGiftResponse.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftResponse;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "CellProperty"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private expiry:J

.field private num:I

.field final synthetic this$0:Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftResponse;


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftResponse;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftResponse$CellProperty;->this$0:Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftResponse;

    .line 2
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public final getExpiry()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftResponse$CellProperty;->expiry:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getNum()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftResponse$CellProperty;->num:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final setExpiry(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftResponse$CellProperty;->expiry:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setNum(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftResponse$CellProperty;->num:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

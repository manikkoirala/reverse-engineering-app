.class public final Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;
.super Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;
.source "OneTrialRenewAdapter.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "FirstPremiumHolder"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final o0:Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field final synthetic 〇OOo8〇0:Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter;


# direct methods
.method public constructor <init>(Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter;Landroid/view/View;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .line 1
    const-string v0, "itemView"

    .line 2
    .line 3
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->〇OOo8〇0:Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter;

    .line 7
    .line 8
    invoke-direct {p0, p2}, Lcom/chad/library/adapter/base/viewholder/BaseViewHolder;-><init>(Landroid/view/View;)V

    .line 9
    .line 10
    .line 11
    invoke-static {p2}, Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    const-string p2, "bind(itemView)"

    .line 16
    .line 17
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    iput-object p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->o0:Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O8ooOoo〇()Landroid/graphics/drawable/GradientDrawable;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;-><init>()V

    .line 4
    .line 5
    .line 6
    const/high16 v1, 0x41000000    # 8.0f

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇〇8O0〇8(F)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇0〇O0088o(F)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    iget-object v1, p0, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->〇OOo8〇0:Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter;

    .line 17
    .line 18
    invoke-virtual {v1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->getContext()Landroid/content/Context;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    const v2, 0x7f060194

    .line 23
    .line 24
    .line 25
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O00(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->OoO8()Landroid/graphics/drawable/GradientDrawable;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    const-string v1, "Builder()\n              \u2026\n                .build()"

    .line 38
    .line 39
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    return-object v0
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final O〇8O8〇008()Landroid/graphics/drawable/GradientDrawable;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;-><init>()V

    .line 4
    .line 5
    .line 6
    const/16 v1, 0x8

    .line 7
    .line 8
    invoke-static {v1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O888o0o(F)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    iget-object v1, p0, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->〇OOo8〇0:Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter;

    .line 17
    .line 18
    invoke-virtual {v1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->getContext()Landroid/content/Context;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    const v2, 0x7f06019e

    .line 23
    .line 24
    .line 25
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O00(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->OoO8()Landroid/graphics/drawable/GradientDrawable;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    const-string v1, "Builder()\n              \u2026\n                .build()"

    .line 38
    .line 39
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    return-object v0
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final o〇〇0〇()Landroid/graphics/drawable/GradientDrawable;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;-><init>()V

    .line 4
    .line 5
    .line 6
    const/16 v1, 0x8

    .line 7
    .line 8
    invoke-static {v1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O888o0o(F)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    const/4 v1, 0x2

    .line 17
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->O8ooOoo〇(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    iget-object v1, p0, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->〇OOo8〇0:Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter;

    .line 22
    .line 23
    invoke-virtual {v1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->getContext()Landroid/content/Context;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    const v2, 0x7f0601b3

    .line 28
    .line 29
    .line 30
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->O〇8O8〇008(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    iget-object v1, p0, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->〇OOo8〇0:Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter;

    .line 39
    .line 40
    invoke-virtual {v1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->getContext()Landroid/content/Context;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    const v2, 0x7f06019e

    .line 45
    .line 46
    .line 47
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 48
    .line 49
    .line 50
    move-result v1

    .line 51
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O00(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    invoke-virtual {v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->OoO8()Landroid/graphics/drawable/GradientDrawable;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    const-string v1, "Builder()\n              \u2026\n                .build()"

    .line 60
    .line 61
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    return-object v0
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇oOO8O8()Landroid/graphics/drawable/GradientDrawable;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;-><init>()V

    .line 4
    .line 5
    .line 6
    const/high16 v1, 0x41000000    # 8.0f

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇〇8O0〇8(F)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇0〇O0088o(F)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    iget-object v1, p0, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->〇OOo8〇0:Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter;

    .line 17
    .line 18
    invoke-virtual {v1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->getContext()Landroid/content/Context;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    const v2, 0x7f06014c

    .line 23
    .line 24
    .line 25
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇00(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    iget-object v1, p0, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->〇OOo8〇0:Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter;

    .line 34
    .line 35
    invoke-virtual {v1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->getContext()Landroid/content/Context;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    const v2, 0x7f060146

    .line 40
    .line 41
    .line 42
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 43
    .line 44
    .line 45
    move-result v1

    .line 46
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇oo〇(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->LEFT_RIGHT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    .line 51
    .line 52
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->o〇O8〇〇o(Landroid/graphics/drawable/GradientDrawable$Orientation;)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->OoO8()Landroid/graphics/drawable/GradientDrawable;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    const-string v1, "Builder()\n              \u2026\n                .build()"

    .line 61
    .line 62
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    return-object v0
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public final 〇00(I)V
    .locals 5

    .line 1
    const/4 v0, -0x1

    .line 2
    const v1, 0x3f19999a    # 0.6f

    .line 3
    .line 4
    .line 5
    const v2, 0x7f060156

    .line 6
    .line 7
    .line 8
    const/4 v3, 0x1

    .line 9
    if-eq p1, v0, :cond_3

    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    const-string v4, "mBinding.ivRewardStatus"

    .line 13
    .line 14
    if-eqz p1, :cond_2

    .line 15
    .line 16
    if-eq p1, v3, :cond_1

    .line 17
    .line 18
    const/4 v1, 0x2

    .line 19
    if-eq p1, v1, :cond_0

    .line 20
    .line 21
    sget-object p1, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter;->O〇o88o08〇:Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$Companion;

    .line 22
    .line 23
    invoke-virtual {p1}, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$Companion;->〇080()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    const-string v0, "sth wrong"

    .line 28
    .line 29
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    goto/16 :goto_0

    .line 33
    .line 34
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->o0:Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;

    .line 35
    .line 36
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 37
    .line 38
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->O〇8O8〇008()Landroid/graphics/drawable/GradientDrawable;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 43
    .line 44
    .line 45
    iget-object p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->o0:Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;

    .line 46
    .line 47
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 48
    .line 49
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->O8ooOoo〇()Landroid/graphics/drawable/GradientDrawable;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 54
    .line 55
    .line 56
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 57
    .line 58
    .line 59
    move-result-object v1

    .line 60
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 61
    .line 62
    .line 63
    move-result v1

    .line 64
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 65
    .line 66
    .line 67
    iget-object p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->o0:Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;

    .line 68
    .line 69
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 70
    .line 71
    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    invoke-static {p1, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 75
    .line 76
    .line 77
    goto/16 :goto_0

    .line 78
    .line 79
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->o0:Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;

    .line 80
    .line 81
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 82
    .line 83
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->O〇8O8〇008()Landroid/graphics/drawable/GradientDrawable;

    .line 84
    .line 85
    .line 86
    move-result-object v0

    .line 87
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 88
    .line 89
    .line 90
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 91
    .line 92
    .line 93
    iget-object p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->o0:Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;

    .line 94
    .line 95
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 96
    .line 97
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->O8ooOoo〇()Landroid/graphics/drawable/GradientDrawable;

    .line 98
    .line 99
    .line 100
    move-result-object v0

    .line 101
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 102
    .line 103
    .line 104
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 105
    .line 106
    .line 107
    move-result-object v0

    .line 108
    invoke-static {v0, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 109
    .line 110
    .line 111
    move-result v0

    .line 112
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 113
    .line 114
    .line 115
    iget-object p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->o0:Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;

    .line 116
    .line 117
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 118
    .line 119
    const-string v0, "checkRewardStatus$lambda$5"

    .line 120
    .line 121
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    .line 123
    .line 124
    invoke-static {p1, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 125
    .line 126
    .line 127
    const v0, 0x7f080d54

    .line 128
    .line 129
    .line 130
    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 131
    .line 132
    .line 133
    goto :goto_0

    .line 134
    :cond_2
    iget-object p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->o0:Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;

    .line 135
    .line 136
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 137
    .line 138
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->o〇〇0〇()Landroid/graphics/drawable/GradientDrawable;

    .line 139
    .line 140
    .line 141
    move-result-object v1

    .line 142
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 143
    .line 144
    .line 145
    iget-object p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->o0:Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;

    .line 146
    .line 147
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 148
    .line 149
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->〇oOO8O8()Landroid/graphics/drawable/GradientDrawable;

    .line 150
    .line 151
    .line 152
    move-result-object v1

    .line 153
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 154
    .line 155
    .line 156
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 157
    .line 158
    .line 159
    move-result-object v1

    .line 160
    const v2, 0x7f06019e

    .line 161
    .line 162
    .line 163
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 164
    .line 165
    .line 166
    move-result v1

    .line 167
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 168
    .line 169
    .line 170
    iget-object p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->o0:Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;

    .line 171
    .line 172
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 173
    .line 174
    invoke-static {p1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 175
    .line 176
    .line 177
    invoke-static {p1, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 178
    .line 179
    .line 180
    goto :goto_0

    .line 181
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->o0:Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;

    .line 182
    .line 183
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;->〇OOo8〇0:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 184
    .line 185
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->O〇8O8〇008()Landroid/graphics/drawable/GradientDrawable;

    .line 186
    .line 187
    .line 188
    move-result-object v0

    .line 189
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 190
    .line 191
    .line 192
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 193
    .line 194
    .line 195
    iget-object p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->o0:Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;

    .line 196
    .line 197
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 198
    .line 199
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->O8ooOoo〇()Landroid/graphics/drawable/GradientDrawable;

    .line 200
    .line 201
    .line 202
    move-result-object v0

    .line 203
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 204
    .line 205
    .line 206
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 207
    .line 208
    .line 209
    move-result-object v0

    .line 210
    invoke-static {v0, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 211
    .line 212
    .line 213
    move-result v0

    .line 214
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 215
    .line 216
    .line 217
    iget-object p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->o0:Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;

    .line 218
    .line 219
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 220
    .line 221
    const-string v0, "checkRewardStatus$lambda$8"

    .line 222
    .line 223
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 224
    .line 225
    .line 226
    invoke-static {p1, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 227
    .line 228
    .line 229
    const v0, 0x7f0807c8

    .line 230
    .line 231
    .line 232
    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 233
    .line 234
    .line 235
    :goto_0
    return-void
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public final 〇0000OOO()Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter$FirstPremiumHolder;->o0:Lcom/intsig/camscanner/databinding/ItemOneTrialRenewBinding;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

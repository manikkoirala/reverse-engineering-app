.class public final Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;
.super Lcom/intsig/app/BaseDialogFragment;
.source "OneTrialRenewDialog.kt"

# interfaces
.implements Lcom/chad/library/adapter/base/listener/OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final o8〇OO0〇0o:Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field static final synthetic 〇8〇oO〇〇8o:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Ljava/util/concurrent/atomic/AtomicBoolean;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private OO〇00〇8oO:Lkotlin/jvm/functions/Function0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;"
        }
    .end annotation
.end field

.field private oOo0:Z

.field private oOo〇8o008:Lcom/intsig/app/ProgressDialogClient;

.field private final o〇00O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇080OO8〇0:Landroid/os/CountDownTimer;

.field private final 〇08O〇00〇o:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0O:Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftItem;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇8〇oO〇〇8o:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->o8〇OO0〇0o:Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog$Companion;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/app/BaseDialogFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v6, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x4

    .line 10
    const/4 v5, 0x0

    .line 11
    move-object v0, v6

    .line 12
    move-object v2, p0

    .line 13
    invoke-direct/range {v0 .. v5}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object v6, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇08O〇00〇o:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 17
    .line 18
    new-instance v0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog$special$$inlined$viewModels$default$1;

    .line 19
    .line 20
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog$special$$inlined$viewModels$default$1;-><init>(Landroidx/fragment/app/Fragment;)V

    .line 21
    .line 22
    .line 23
    sget-object v1, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 24
    .line 25
    new-instance v2, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog$special$$inlined$viewModels$default$2;

    .line 26
    .line 27
    invoke-direct {v2, v0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog$special$$inlined$viewModels$default$2;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 28
    .line 29
    .line 30
    invoke-static {v1, v2}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    const-class v1, Lcom/intsig/camscanner/marketing/trialrenew/viewmodel/OneTrialRenewViewModel;

    .line 35
    .line 36
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->〇o00〇〇Oo(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    new-instance v2, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog$special$$inlined$viewModels$default$3;

    .line 41
    .line 42
    invoke-direct {v2, v0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog$special$$inlined$viewModels$default$3;-><init>(Lkotlin/Lazy;)V

    .line 43
    .line 44
    .line 45
    new-instance v3, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog$special$$inlined$viewModels$default$4;

    .line 46
    .line 47
    const/4 v4, 0x0

    .line 48
    invoke-direct {v3, v4, v0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog$special$$inlined$viewModels$default$4;-><init>(Lkotlin/jvm/functions/Function0;Lkotlin/Lazy;)V

    .line 49
    .line 50
    .line 51
    new-instance v4, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog$special$$inlined$viewModels$default$5;

    .line 52
    .line 53
    invoke-direct {v4, p0, v0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog$special$$inlined$viewModels$default$5;-><init>(Landroidx/fragment/app/Fragment;Lkotlin/Lazy;)V

    .line 54
    .line 55
    .line 56
    invoke-static {p0, v1, v2, v3, v4}, Landroidx/fragment/app/FragmentViewModelLazyKt;->createViewModelLazy(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    iput-object v0, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->o〇00O:Lkotlin/Lazy;

    .line 61
    .line 62
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 63
    .line 64
    const/4 v1, 0x0

    .line 65
    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    .line 66
    .line 67
    .line 68
    iput-object v0, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O8o08O8O:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 69
    .line 70
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private final O08〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇080OO8〇0:Landroid/os/CountDownTimer;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog$registerCountDownTimer$1;

    .line 6
    .line 7
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog$registerCountDownTimer$1;-><init>(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;)V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇080OO8〇0:Landroid/os/CountDownTimer;

    .line 11
    .line 12
    :cond_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final O0O0〇()Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇08O〇00〇o:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇8〇oO〇〇8o:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final O0〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇080OO8〇0:Landroid/os/CountDownTimer;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 6
    .line 7
    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇080OO8〇0:Landroid/os/CountDownTimer;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic O0〇0(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇0oO〇oo00(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final O80〇O〇080()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->oOo〇8o008:Lcom/intsig/app/ProgressDialogClient;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/app/ProgressDialogClient;->〇080()V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final O8〇8〇O80(J)Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "yyyy-MM-dd"

    .line 2
    .line 3
    invoke-static {p1, p2, v0}, Lcom/intsig/utils/DateTimeUtil;->Oo08(JLjava/lang/String;)Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    const-string p2, "getFormattedDateBySpecSt\u2026TimeUtil.FORMAT_YYYYMMDD)"

    .line 8
    .line 9
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-object p1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic Ooo8o(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇〇888()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final O〇8〇008()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O0O0〇()Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;->o8〇OO0〇0o:Landroidx/recyclerview/widget/RecyclerView;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    new-instance v1, Landroidx/recyclerview/widget/GridLayoutManager;

    .line 12
    .line 13
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    const/4 v3, 0x4

    .line 18
    invoke-direct {v1, v2, v3}, Landroidx/recyclerview/widget/GridLayoutManager;-><init>(Landroid/content/Context;I)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 22
    .line 23
    .line 24
    new-instance v1, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;

    .line 25
    .line 26
    invoke-virtual {v0}, Landroidx/recyclerview/widget/RecyclerView;->getLayoutManager()Landroidx/recyclerview/widget/RecyclerView$LayoutManager;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    const-string v3, "null cannot be cast to non-null type androidx.recyclerview.widget.GridLayoutManager"

    .line 31
    .line 32
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    check-cast v2, Landroidx/recyclerview/widget/GridLayoutManager;

    .line 36
    .line 37
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    .line 38
    .line 39
    .line 40
    move-result-object v3

    .line 41
    invoke-direct {v1, v2, v3}, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;-><init>(Landroidx/recyclerview/widget/LinearLayoutManager;Landroid/content/Context;)V

    .line 42
    .line 43
    .line 44
    const/high16 v2, 0x41000000    # 8.0f

    .line 45
    .line 46
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->oO80(F)Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->OO0o〇〇〇〇0(F)Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;

    .line 51
    .line 52
    .line 53
    move-result-object v1

    .line 54
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->〇80〇808〇O(F)Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;

    .line 55
    .line 56
    .line 57
    move-result-object v1

    .line 58
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->〇8o8o〇(F)Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;

    .line 59
    .line 60
    .line 61
    move-result-object v1

    .line 62
    const/4 v2, 0x1

    .line 63
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->〇O8o08O(Z)Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;

    .line 64
    .line 65
    .line 66
    move-result-object v1

    .line 67
    invoke-virtual {v1}, Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration$Builder;->〇〇888()Lcom/intsig/camscanner/util/superdecoration/SuperOffsetDecoration;

    .line 68
    .line 69
    .line 70
    move-result-object v1

    .line 71
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 72
    .line 73
    .line 74
    :cond_0
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public static synthetic o00〇88〇08(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇〇〇0(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o0〇〇00(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftItem;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O0O0〇()Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;->o8〇OO0〇0o:Landroidx/recyclerview/widget/RecyclerView;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    new-instance v1, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter;

    .line 12
    .line 13
    invoke-direct {v1, p1}, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter;-><init>(Ljava/util/List;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1, p0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O880oOO08(Lcom/chad/library/adapter/base/listener/OnItemClickListener;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 20
    .line 21
    .line 22
    :cond_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final o88()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O0O0〇()Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    if-nez v0, :cond_1

    .line 12
    .line 13
    goto :goto_1

    .line 14
    :cond_1
    new-instance v1, Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 15
    .line 16
    invoke-direct {v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;-><init>()V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    const v3, 0x7f060194

    .line 24
    .line 25
    .line 26
    invoke-static {v2, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    invoke-virtual {v1, v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O00(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    const/16 v2, 0x8

    .line 35
    .line 36
    invoke-static {v2}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 37
    .line 38
    .line 39
    move-result v2

    .line 40
    invoke-virtual {v1, v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O888o0o(F)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    invoke-virtual {v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->OoO8()Landroid/graphics/drawable/GradientDrawable;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 49
    .line 50
    .line 51
    :goto_1
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic o880(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇〇〇00(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic oOoO8OO〇(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->oOo0:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic o〇0〇o(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;)Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O0O0〇()Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic o〇O8OO(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftItem;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->o〇oo(Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftItem;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final o〇oo(Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftItem;)V
    .locals 8

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇0O:Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftItem;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O0O0〇()Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/4 v1, 0x1

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 11
    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 15
    .line 16
    .line 17
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftItem;->getStatus()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    const/4 v2, -0x2

    .line 22
    const/4 v3, 0x0

    .line 23
    const-string v4, ""

    .line 24
    .line 25
    if-eq v0, v2, :cond_15

    .line 26
    .line 27
    const/4 v2, -0x1

    .line 28
    const v5, 0x3f19999a    # 0.6f

    .line 29
    .line 30
    .line 31
    const/4 v6, 0x0

    .line 32
    const v7, 0x7f06019e

    .line 33
    .line 34
    .line 35
    if-eq v0, v2, :cond_d

    .line 36
    .line 37
    if-eqz v0, :cond_9

    .line 38
    .line 39
    if-eq v0, v1, :cond_1

    .line 40
    .line 41
    const/4 v2, 0x2

    .line 42
    if-eq v0, v2, :cond_15

    .line 43
    .line 44
    const-string p1, "OneTrialRenewDialog"

    .line 45
    .line 46
    const-string v0, "sth wrong"

    .line 47
    .line 48
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O0O0〇()Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    if-eqz p1, :cond_19

    .line 56
    .line 57
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 58
    .line 59
    if-eqz p1, :cond_19

    .line 60
    .line 61
    invoke-static {p1, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 62
    .line 63
    .line 64
    goto/16 :goto_6

    .line 65
    .line 66
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O0O0〇()Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    if-eqz v0, :cond_5

    .line 71
    .line 72
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;->ooo0〇〇O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 73
    .line 74
    if-eqz v0, :cond_5

    .line 75
    .line 76
    invoke-virtual {p1}, Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftItem;->getTime_text()Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object p1

    .line 80
    if-eqz p1, :cond_4

    .line 81
    .line 82
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 83
    .line 84
    .line 85
    move-result v2

    .line 86
    if-nez v2, :cond_2

    .line 87
    .line 88
    const/4 v3, 0x1

    .line 89
    :cond_2
    if-eqz v3, :cond_3

    .line 90
    .line 91
    goto :goto_0

    .line 92
    :cond_3
    move-object v4, p1

    .line 93
    :cond_4
    :goto_0
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    .line 95
    .line 96
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇08O()Landroid/graphics/drawable/GradientDrawable;

    .line 97
    .line 98
    .line 99
    move-result-object p1

    .line 100
    invoke-virtual {v0, p1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 101
    .line 102
    .line 103
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    .line 104
    .line 105
    .line 106
    move-result-object p1

    .line 107
    invoke-static {p1, v7}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 108
    .line 109
    .line 110
    move-result p1

    .line 111
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 112
    .line 113
    .line 114
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O0O0〇()Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;

    .line 115
    .line 116
    .line 117
    move-result-object p1

    .line 118
    if-eqz p1, :cond_6

    .line 119
    .line 120
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;->〇0O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 121
    .line 122
    if-eqz p1, :cond_6

    .line 123
    .line 124
    invoke-static {p1, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 125
    .line 126
    .line 127
    const v0, 0x7f080d54

    .line 128
    .line 129
    .line 130
    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 131
    .line 132
    .line 133
    :cond_6
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O0O0〇()Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;

    .line 134
    .line 135
    .line 136
    move-result-object p1

    .line 137
    if-eqz p1, :cond_7

    .line 138
    .line 139
    iget-object v6, p1, Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 140
    .line 141
    :cond_7
    if-nez v6, :cond_8

    .line 142
    .line 143
    goto/16 :goto_6

    .line 144
    .line 145
    :cond_8
    invoke-virtual {v6, v5}, Landroid/view/View;->setAlpha(F)V

    .line 146
    .line 147
    .line 148
    goto/16 :goto_6

    .line 149
    .line 150
    :cond_9
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O0O0〇()Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;

    .line 151
    .line 152
    .line 153
    move-result-object v0

    .line 154
    if-eqz v0, :cond_19

    .line 155
    .line 156
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;->ooo0〇〇O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 157
    .line 158
    if-eqz v0, :cond_19

    .line 159
    .line 160
    invoke-virtual {p1}, Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftItem;->getTime_text()Ljava/lang/String;

    .line 161
    .line 162
    .line 163
    move-result-object p1

    .line 164
    if-eqz p1, :cond_c

    .line 165
    .line 166
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 167
    .line 168
    .line 169
    move-result v2

    .line 170
    if-nez v2, :cond_a

    .line 171
    .line 172
    goto :goto_1

    .line 173
    :cond_a
    const/4 v1, 0x0

    .line 174
    :goto_1
    if-eqz v1, :cond_b

    .line 175
    .line 176
    goto :goto_2

    .line 177
    :cond_b
    move-object v4, p1

    .line 178
    :cond_c
    :goto_2
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    .line 180
    .line 181
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇08O()Landroid/graphics/drawable/GradientDrawable;

    .line 182
    .line 183
    .line 184
    move-result-object p1

    .line 185
    invoke-virtual {v0, p1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 186
    .line 187
    .line 188
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    .line 189
    .line 190
    .line 191
    move-result-object p1

    .line 192
    invoke-static {p1, v7}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 193
    .line 194
    .line 195
    move-result p1

    .line 196
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 197
    .line 198
    .line 199
    goto/16 :goto_6

    .line 200
    .line 201
    :cond_d
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O0O0〇()Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;

    .line 202
    .line 203
    .line 204
    move-result-object v0

    .line 205
    if-eqz v0, :cond_11

    .line 206
    .line 207
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;->ooo0〇〇O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 208
    .line 209
    if-eqz v0, :cond_11

    .line 210
    .line 211
    invoke-virtual {p1}, Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftItem;->getTime_text()Ljava/lang/String;

    .line 212
    .line 213
    .line 214
    move-result-object p1

    .line 215
    if-eqz p1, :cond_10

    .line 216
    .line 217
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 218
    .line 219
    .line 220
    move-result v2

    .line 221
    if-nez v2, :cond_e

    .line 222
    .line 223
    const/4 v3, 0x1

    .line 224
    :cond_e
    if-eqz v3, :cond_f

    .line 225
    .line 226
    goto :goto_3

    .line 227
    :cond_f
    move-object v4, p1

    .line 228
    :cond_10
    :goto_3
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 229
    .line 230
    .line 231
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇08O()Landroid/graphics/drawable/GradientDrawable;

    .line 232
    .line 233
    .line 234
    move-result-object p1

    .line 235
    invoke-virtual {v0, p1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 236
    .line 237
    .line 238
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    .line 239
    .line 240
    .line 241
    move-result-object p1

    .line 242
    invoke-static {p1, v7}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 243
    .line 244
    .line 245
    move-result p1

    .line 246
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 247
    .line 248
    .line 249
    :cond_11
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O0O0〇()Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;

    .line 250
    .line 251
    .line 252
    move-result-object p1

    .line 253
    if-eqz p1, :cond_12

    .line 254
    .line 255
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;->〇0O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 256
    .line 257
    if-eqz p1, :cond_12

    .line 258
    .line 259
    invoke-static {p1, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 260
    .line 261
    .line 262
    const v0, 0x7f0807c8

    .line 263
    .line 264
    .line 265
    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 266
    .line 267
    .line 268
    :cond_12
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O0O0〇()Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;

    .line 269
    .line 270
    .line 271
    move-result-object p1

    .line 272
    if-eqz p1, :cond_13

    .line 273
    .line 274
    iget-object v6, p1, Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 275
    .line 276
    :cond_13
    if-nez v6, :cond_14

    .line 277
    .line 278
    goto :goto_6

    .line 279
    :cond_14
    invoke-virtual {v6, v5}, Landroid/view/View;->setAlpha(F)V

    .line 280
    .line 281
    .line 282
    goto :goto_6

    .line 283
    :cond_15
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O0O0〇()Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;

    .line 284
    .line 285
    .line 286
    move-result-object v0

    .line 287
    if-eqz v0, :cond_19

    .line 288
    .line 289
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;->ooo0〇〇O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 290
    .line 291
    if-eqz v0, :cond_19

    .line 292
    .line 293
    invoke-virtual {p1}, Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftItem;->getTime_text()Ljava/lang/String;

    .line 294
    .line 295
    .line 296
    move-result-object p1

    .line 297
    if-eqz p1, :cond_18

    .line 298
    .line 299
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 300
    .line 301
    .line 302
    move-result v2

    .line 303
    if-nez v2, :cond_16

    .line 304
    .line 305
    goto :goto_4

    .line 306
    :cond_16
    const/4 v1, 0x0

    .line 307
    :goto_4
    if-eqz v1, :cond_17

    .line 308
    .line 309
    goto :goto_5

    .line 310
    :cond_17
    move-object v4, p1

    .line 311
    :cond_18
    :goto_5
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 312
    .line 313
    .line 314
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇O0o〇〇o()Landroid/graphics/drawable/GradientDrawable;

    .line 315
    .line 316
    .line 317
    move-result-object p1

    .line 318
    invoke-virtual {v0, p1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 319
    .line 320
    .line 321
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    .line 322
    .line 323
    .line 324
    move-result-object p1

    .line 325
    const v1, 0x7f060156

    .line 326
    .line 327
    .line 328
    invoke-static {p1, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 329
    .line 330
    .line 331
    move-result p1

    .line 332
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 333
    .line 334
    .line 335
    :cond_19
    :goto_6
    return-void
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public static final synthetic 〇088O(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;)Lkotlin/jvm/functions/Function0;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->OO〇00〇8oO:Lkotlin/jvm/functions/Function0;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇08O()Landroid/graphics/drawable/GradientDrawable;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;-><init>()V

    .line 4
    .line 5
    .line 6
    const/16 v1, 0xb

    .line 7
    .line 8
    invoke-static {v1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O888o0o(F)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    const v2, 0x7f06014c

    .line 21
    .line 22
    .line 23
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇00(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    const v2, 0x7f060146

    .line 36
    .line 37
    .line 38
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇oo〇(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->LEFT_RIGHT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    .line 47
    .line 48
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->o〇O8〇〇o(Landroid/graphics/drawable/GradientDrawable$Orientation;)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->OoO8()Landroid/graphics/drawable/GradientDrawable;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    const-string v1, "Builder()\n            .c\u2026GHT)\n            .build()"

    .line 57
    .line 58
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    return-object v0
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇0oO〇oo00(Ljava/lang/String;)V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇Oo〇O()Lcom/intsig/camscanner/marketing/trialrenew/viewmodel/OneTrialRenewViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x2

    .line 6
    const/4 v2, 0x0

    .line 7
    const/4 v3, 0x0

    .line 8
    invoke-static {v0, p1, v3, v1, v2}, Lcom/intsig/camscanner/marketing/trialrenew/viewmodel/OneTrialRenewViewModel;->oo88o8O(Lcom/intsig/camscanner/marketing/trialrenew/viewmodel/OneTrialRenewViewModel;Ljava/lang/String;ZILjava/lang/Object;)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇0ooOOo(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->o0〇〇00(Ljava/util/ArrayList;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static final synthetic 〇0〇0(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O8o08O8O:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private final 〇8O0880(Ljava/lang/String;)V
    .locals 5

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "addGift\tactionId="

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "OneTrialRenewDialog"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-boolean v0, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->oOo0:Z

    .line 24
    .line 25
    const/4 v1, 0x1

    .line 26
    const/4 v2, 0x0

    .line 27
    if-nez v0, :cond_0

    .line 28
    .line 29
    invoke-static {}, Lcom/intsig/camscanner/marketing/trialrenew/OneTrialRenewConfiguration;->O8()Z

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    if-eqz v0, :cond_0

    .line 34
    .line 35
    const/4 v0, 0x1

    .line 36
    goto :goto_0

    .line 37
    :cond_0
    const/4 v0, 0x0

    .line 38
    :goto_0
    if-ne v0, v1, :cond_3

    .line 39
    .line 40
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O0O0〇()Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;

    .line 41
    .line 42
    .line 43
    move-result-object v0

    .line 44
    if-eqz v0, :cond_2

    .line 45
    .line 46
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;->OO:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 47
    .line 48
    if-eqz v0, :cond_2

    .line 49
    .line 50
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    if-nez v0, :cond_1

    .line 55
    .line 56
    const/4 v0, 0x1

    .line 57
    goto :goto_1

    .line 58
    :cond_1
    const/4 v0, 0x0

    .line 59
    :goto_1
    xor-int/2addr v0, v1

    .line 60
    goto :goto_2

    .line 61
    :cond_2
    const/4 v0, 0x0

    .line 62
    :goto_2
    sget-object v1, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewPurchaseDialog;->ooo0〇〇O:Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewPurchaseDialog$Companion;

    .line 63
    .line 64
    const/4 v3, 0x2

    .line 65
    const/4 v4, 0x0

    .line 66
    invoke-static {v1, v0, v2, v3, v4}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewPurchaseDialog$Companion;->〇o〇(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewPurchaseDialog$Companion;ZIILjava/lang/Object;)Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewPurchaseDialog;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    new-instance v1, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog$checkAddGift$purchaseDialog$1$1;

    .line 71
    .line 72
    invoke-direct {v1, p0, p1}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog$checkAddGift$purchaseDialog$1$1;-><init>(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewPurchaseDialog;->〇oO〇08o(Lkotlin/jvm/functions/Function0;)V

    .line 76
    .line 77
    .line 78
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    .line 79
    .line 80
    .line 81
    move-result-object p1

    .line 82
    invoke-virtual {p1}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 83
    .line 84
    .line 85
    move-result-object p1

    .line 86
    const-string v1, "OneTrialRenewPurchaseDialog"

    .line 87
    .line 88
    invoke-virtual {v0, p1, v1}, Lcom/intsig/app/BaseDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    goto :goto_3

    .line 92
    :cond_3
    if-nez v0, :cond_4

    .line 93
    .line 94
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇0oO〇oo00(Ljava/lang/String;)V

    .line 95
    .line 96
    .line 97
    :cond_4
    :goto_3
    return-void
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static final synthetic 〇8〇80o(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O80〇O〇080()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;J)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O8〇8〇O80(J)Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private final 〇O0o〇〇o()Landroid/graphics/drawable/GradientDrawable;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;-><init>()V

    .line 4
    .line 5
    .line 6
    const/16 v1, 0xb

    .line 7
    .line 8
    invoke-static {v1}, Lcom/intsig/camscanner/pic2word/lr/SizeKtKt;->〇o00〇〇Oo(I)F

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O888o0o(F)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    const v2, 0x7f06019e

    .line 21
    .line 22
    .line 23
    invoke-static {v1, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    invoke-virtual {v0, v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O00(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->OoO8()Landroid/graphics/drawable/GradientDrawable;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    const-string v1, "Builder()\n            .c\u2026DD))\n            .build()"

    .line 36
    .line 37
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    return-object v0
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇O8〇8000()V
    .locals 3

    .line 1
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog$addSubscribe$1;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-direct {v1, p0, v2}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog$addSubscribe$1;-><init>(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;Lkotlin/coroutines/Continuation;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, v1}, Landroidx/lifecycle/LifecycleCoroutineScope;->launchWhenStarted(Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/Job;

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇O8〇8O0oO()V
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O0O0〇()Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;->o8oOOo:Landroidx/appcompat/widget/AppCompatTextView;

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    if-nez v0, :cond_1

    .line 12
    .line 13
    goto :goto_2

    .line 14
    :cond_1
    sget-object v1, Lkotlin/jvm/internal/StringCompanionObject;->〇080:Lkotlin/jvm/internal/StringCompanionObject;

    .line 15
    .line 16
    const v1, 0x7f1311b1

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    const-string v2, "getString(R.string.cs_625_new_vip_subtitle1)"

    .line 24
    .line 25
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    const/4 v2, 0x1

    .line 29
    new-array v3, v2, [Ljava/lang/Object;

    .line 30
    .line 31
    invoke-static {}, Lcom/intsig/camscanner/marketing/trialrenew/OneTrialRenewConfiguration;->〇o〇()Z

    .line 32
    .line 33
    .line 34
    move-result v4

    .line 35
    if-eqz v4, :cond_2

    .line 36
    .line 37
    const/16 v4, 0x11

    .line 38
    .line 39
    goto :goto_1

    .line 40
    :cond_2
    const/16 v4, 0xa

    .line 41
    .line 42
    :goto_1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 43
    .line 44
    .line 45
    move-result-object v4

    .line 46
    const/4 v5, 0x0

    .line 47
    aput-object v4, v3, v5

    .line 48
    .line 49
    invoke-static {v3, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    .line 50
    .line 51
    .line 52
    move-result-object v2

    .line 53
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v1

    .line 57
    const-string v2, "format(format, *args)"

    .line 58
    .line 59
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    .line 64
    .line 65
    :goto_2
    return-void
    .line 66
    .line 67
    .line 68
.end method

.method private final 〇Oo〇O()Lcom/intsig/camscanner/marketing/trialrenew/viewmodel/OneTrialRenewViewModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->o〇00O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/marketing/trialrenew/viewmodel/OneTrialRenewViewModel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇o08()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O0O0〇()Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    new-instance v1, L〇08〇o0O/〇080;

    .line 12
    .line 13
    invoke-direct {v1, p0}, L〇08〇o0O/〇080;-><init>(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 17
    .line 18
    .line 19
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O0O0〇()Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;->ooo0〇〇O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 26
    .line 27
    if-eqz v0, :cond_1

    .line 28
    .line 29
    new-instance v1, L〇08〇o0O/〇o00〇〇Oo;

    .line 30
    .line 31
    invoke-direct {v1, p0}, L〇08〇o0O/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;)V

    .line 32
    .line 33
    .line 34
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 35
    .line 36
    .line 37
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O0O0〇()Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;

    .line 38
    .line 39
    .line 40
    move-result-object v0

    .line 41
    if-eqz v0, :cond_2

    .line 42
    .line 43
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 44
    .line 45
    if-eqz v0, :cond_2

    .line 46
    .line 47
    new-instance v1, L〇08〇o0O/〇o〇;

    .line 48
    .line 49
    invoke-direct {v1, p0}, L〇08〇o0O/〇o〇;-><init>(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;)V

    .line 50
    .line 51
    .line 52
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    .line 54
    .line 55
    :cond_2
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final synthetic 〇o〇88〇8(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;)Lcom/intsig/camscanner/marketing/trialrenew/viewmodel/OneTrialRenewViewModel;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇Oo〇O()Lcom/intsig/camscanner/marketing/trialrenew/viewmodel/OneTrialRenewViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final 〇〇()Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->o8〇OO0〇0o:Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog$Companion;->〇080()Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final 〇〇888()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->oOo〇8o008:Lcom/intsig/app/ProgressDialogClient;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    const v1, 0x7f130e22

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    invoke-static {v0, v1}, Lcom/intsig/app/ProgressDialogClient;->〇o00〇〇Oo(Landroid/app/Activity;Ljava/lang/String;)Lcom/intsig/app/ProgressDialogClient;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    iput-object v0, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->oOo〇8o008:Lcom/intsig/app/ProgressDialogClient;

    .line 23
    .line 24
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->oOo〇8o008:Lcom/intsig/app/ProgressDialogClient;

    .line 25
    .line 26
    if-eqz v0, :cond_1

    .line 27
    .line 28
    invoke-virtual {v0}, Lcom/intsig/app/ProgressDialogClient;->O8()V

    .line 29
    .line 30
    .line 31
    :cond_1
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private static final 〇〇O80〇0o(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;Landroid/view/View;)V
    .locals 2

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "OneTrialRenewDialog"

    .line 7
    .line 8
    const-string v0, "on close"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/camscanner/marketing/trialrenew/OneTrialRenewConfiguration;->O8()Z

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    if-nez p1, :cond_2

    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O0O0〇()Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    if-eqz p1, :cond_2

    .line 24
    .line 25
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;->o8〇OO0〇0o:Landroidx/recyclerview/widget/RecyclerView;

    .line 26
    .line 27
    if-eqz p1, :cond_2

    .line 28
    .line 29
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView;->getAdapter()Landroidx/recyclerview/widget/RecyclerView$Adapter;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    if-eqz p1, :cond_2

    .line 34
    .line 35
    check-cast p1, Lcom/intsig/camscanner/marketing/trialrenew/adapter/OneTrialRenewAdapter;

    .line 36
    .line 37
    invoke-virtual {p1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O8〇o()Ljava/util/List;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    if-eqz v0, :cond_2

    .line 50
    .line 51
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    check-cast v0, Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftItem;

    .line 56
    .line 57
    invoke-virtual {v0}, Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftItem;->getStatus()I

    .line 58
    .line 59
    .line 60
    move-result v1

    .line 61
    if-nez v1, :cond_0

    .line 62
    .line 63
    invoke-virtual {v0}, Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftItem;->getAct_id()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    if-nez p1, :cond_1

    .line 68
    .line 69
    const-string p1, ""

    .line 70
    .line 71
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇Oo〇O()Lcom/intsig/camscanner/marketing/trialrenew/viewmodel/OneTrialRenewViewModel;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    const/4 v1, 0x1

    .line 76
    invoke-virtual {v0, p1, v1}, Lcom/intsig/camscanner/marketing/trialrenew/viewmodel/OneTrialRenewViewModel;->〇O00(Ljava/lang/String;Z)V

    .line 77
    .line 78
    .line 79
    :cond_2
    const-string p1, "CSNewGiftPop"

    .line 80
    .line 81
    sget-object v0, Lcom/intsig/comm/tracker/TrackAction$CSOneTrialRenew;->〇080:Ljava/lang/String;

    .line 82
    .line 83
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->dismissAllowingStateLoss()V

    .line 87
    .line 88
    .line 89
    return-void
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic 〇〇o0〇8(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇〇O80〇0o(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static final 〇〇〇0(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;Landroid/view/View;)V
    .locals 2

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇0O:Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftItem;

    .line 7
    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    invoke-virtual {p1}, Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftItem;->getStatus()I

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    goto :goto_0

    .line 19
    :cond_0
    const/4 p1, 0x0

    .line 20
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 21
    .line 22
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 23
    .line 24
    .line 25
    const-string v1, "on attendance status\tstatus = "

    .line 26
    .line 27
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    const-string v0, "OneTrialRenewDialog"

    .line 38
    .line 39
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    iget-object p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇0O:Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftItem;

    .line 43
    .line 44
    const/4 v0, 0x0

    .line 45
    if-eqz p1, :cond_1

    .line 46
    .line 47
    invoke-virtual {p1}, Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftItem;->getStatus()I

    .line 48
    .line 49
    .line 50
    move-result p1

    .line 51
    if-nez p1, :cond_1

    .line 52
    .line 53
    const/4 v0, 0x1

    .line 54
    :cond_1
    if-eqz v0, :cond_4

    .line 55
    .line 56
    const-string p1, "CSNewGiftPop"

    .line 57
    .line 58
    sget-object v0, Lcom/intsig/comm/tracker/TrackAction$CSOneTrialRenew;->〇o〇:Ljava/lang/String;

    .line 59
    .line 60
    invoke-static {p1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    iget-object p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇0O:Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftItem;

    .line 64
    .line 65
    if-eqz p1, :cond_2

    .line 66
    .line 67
    invoke-virtual {p1}, Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftItem;->getAct_id()Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object p1

    .line 71
    if-nez p1, :cond_3

    .line 72
    .line 73
    :cond_2
    const-string p1, ""

    .line 74
    .line 75
    :cond_3
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇8O0880(Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    :cond_4
    return-void
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private static final 〇〇〇00(Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;Landroid/view/View;)V
    .locals 1

    .line 1
    const-string p1, "this$0"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string p1, "OneTrialRenewDialog"

    .line 7
    .line 8
    const-string v0, "on claim tip"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    iget-object p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇080OO8〇0:Landroid/os/CountDownTimer;

    .line 14
    .line 15
    if-eqz p1, :cond_0

    .line 16
    .line 17
    invoke-virtual {p1}, Landroid/os/CountDownTimer;->cancel()V

    .line 18
    .line 19
    .line 20
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O8o08O8O:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 21
    .line 22
    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    if-eqz p1, :cond_1

    .line 27
    .line 28
    iget-object p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O8o08O8O:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 29
    .line 30
    const/4 v0, 0x0

    .line 31
    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 32
    .line 33
    .line 34
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O0O0〇()Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;

    .line 35
    .line 36
    .line 37
    move-result-object p0

    .line 38
    if-eqz p0, :cond_3

    .line 39
    .line 40
    iget-object p0, p0, Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;->〇OOo8〇0:Lcom/github/happlebubble/BubbleLayout;

    .line 41
    .line 42
    if-eqz p0, :cond_3

    .line 43
    .line 44
    invoke-static {p0, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 45
    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O8o08O8O:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 49
    .line 50
    const/4 v0, 0x1

    .line 51
    invoke-virtual {p1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 52
    .line 53
    .line 54
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O0O0〇()Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;

    .line 55
    .line 56
    .line 57
    move-result-object p1

    .line 58
    if-eqz p1, :cond_2

    .line 59
    .line 60
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;->〇OOo8〇0:Lcom/github/happlebubble/BubbleLayout;

    .line 61
    .line 62
    if-eqz p1, :cond_2

    .line 63
    .line 64
    invoke-static {p1, v0}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 65
    .line 66
    .line 67
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O08〇()V

    .line 68
    .line 69
    .line 70
    iget-object p0, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇080OO8〇0:Landroid/os/CountDownTimer;

    .line 71
    .line 72
    if-eqz p0, :cond_3

    .line 73
    .line 74
    invoke-virtual {p0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 75
    .line 76
    .line 77
    :cond_3
    :goto_0
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private final 〇〇〇O〇()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O0O0〇()Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogOneTrialRenewBinding;->〇O〇〇O8:Landroid/view/View;

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    if-nez v0, :cond_1

    .line 12
    .line 13
    goto :goto_1

    .line 14
    :cond_1
    new-instance v1, Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 15
    .line 16
    invoke-direct {v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;-><init>()V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    const v3, 0x7f0602fa

    .line 24
    .line 25
    .line 26
    invoke-static {v2, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    invoke-virtual {v1, v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O00(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    const/high16 v3, 0x41c00000    # 24.0f

    .line 39
    .line 40
    invoke-static {v2, v3}, Lcom/intsig/utils/DisplayUtil;->〇o00〇〇Oo(Landroid/content/Context;F)F

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    invoke-virtual {v1, v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇O888o0o(F)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    invoke-virtual {v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->OoO8()Landroid/graphics/drawable/GradientDrawable;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 53
    .line 54
    .line 55
    :goto_1
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method protected init(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    const-string p1, "OneTrialRenewDialog"

    .line 2
    .line 3
    const-string v0, "init"

    .line 4
    .line 5
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 p1, 0x0

    .line 9
    invoke-virtual {p0, p1}, Landroidx/fragment/app/DialogFragment;->setCancelable(Z)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p0}, Lcom/intsig/app/BaseDialogFragment;->〇O8oOo0()V

    .line 13
    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇〇〇O〇()V

    .line 16
    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇O8〇8O0oO()V

    .line 19
    .line 20
    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->o88()V

    .line 22
    .line 23
    .line 24
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O〇8〇008()V

    .line 25
    .line 26
    .line 27
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇O8〇8000()V

    .line 28
    .line 29
    .line 30
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇o08()V

    .line 31
    .line 32
    .line 33
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇Oo〇O()Lcom/intsig/camscanner/marketing/trialrenew/viewmodel/OneTrialRenewViewModel;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    invoke-virtual {p1}, Lcom/intsig/camscanner/marketing/trialrenew/viewmodel/OneTrialRenewViewModel;->oo〇()V

    .line 38
    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public onDestroy()V
    .locals 0

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onDestroy()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->O0〇()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onStart()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/DialogFragment;->onStart()V

    .line 2
    .line 3
    .line 4
    const-string v0, "CSNewGiftPop"

    .line 5
    .line 6
    invoke-static {v0}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public ooO(Lcom/chad/library/adapter/base/BaseQuickAdapter;Landroid/view/View;I)V
    .locals 2
    .param p1    # Lcom/chad/library/adapter/base/BaseQuickAdapter;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/chad/library/adapter/base/BaseQuickAdapter<",
            "**>;",
            "Landroid/view/View;",
            "I)V"
        }
    .end annotation

    .line 1
    const-string v0, "adapter"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "view"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/utils/ClickLimit;->〇o〇()Lcom/intsig/utils/ClickLimit;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {v0, p2}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 16
    .line 17
    .line 18
    move-result p2

    .line 19
    const-string v0, "OneTrialRenewDialog"

    .line 20
    .line 21
    if-nez p2, :cond_0

    .line 22
    .line 23
    const-string p1, "click too fast."

    .line 24
    .line 25
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    return-void

    .line 29
    :cond_0
    invoke-virtual {p1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O8〇o()Ljava/util/List;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    invoke-interface {p1, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    const-string p2, "null cannot be cast to non-null type com.intsig.camscanner.marketing.trialrenew.entity.OneTrialRenewGiftItem"

    .line 38
    .line 39
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    check-cast p1, Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftItem;

    .line 43
    .line 44
    new-instance p2, Ljava/lang/StringBuilder;

    .line 45
    .line 46
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 47
    .line 48
    .line 49
    const-string v1, "onItemClick >>> position = "

    .line 50
    .line 51
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    .line 53
    .line 54
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object p2

    .line 61
    invoke-static {v0, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {p1}, Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftItem;->getStatus()I

    .line 65
    .line 66
    .line 67
    move-result p2

    .line 68
    if-nez p2, :cond_2

    .line 69
    .line 70
    sget-object p2, Lcom/intsig/comm/tracker/TrackAction$CSOneTrialRenew;->O8:Ljava/lang/String;

    .line 71
    .line 72
    const-string v0, "day"

    .line 73
    .line 74
    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object p3

    .line 78
    const-string v1, "CSNewGiftPop"

    .line 79
    .line 80
    invoke-static {v1, p2, v0, p3}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    invoke-virtual {p1}, Lcom/intsig/camscanner/marketing/trialrenew/entity/OneTrialRenewGiftItem;->getAct_id()Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object p1

    .line 87
    if-nez p1, :cond_1

    .line 88
    .line 89
    const-string p1, ""

    .line 90
    .line 91
    :cond_1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->〇8O0880(Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    :cond_2
    return-void
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d01f7

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final 〇oO〇08o(Lkotlin/jvm/functions/Function0;)V
    .locals 1
    .param p1    # Lkotlin/jvm/functions/Function0;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "callback"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/marketing/trialrenew/ui/OneTrialRenewDialog;->OO〇00〇8oO:Lkotlin/jvm/functions/Function0;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

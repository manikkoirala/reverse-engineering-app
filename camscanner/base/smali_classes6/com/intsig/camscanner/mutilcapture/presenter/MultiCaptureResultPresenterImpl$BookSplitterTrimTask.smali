.class Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl$BookSplitterTrimTask;
.super Lcom/intsig/camscanner/tools/ProgressAsyncTask;
.source "MultiCaptureResultPresenterImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BookSplitterTrimTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/intsig/camscanner/tools/ProgressAsyncTask<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final O8:Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

.field private final Oo08:Landroid/app/Activity;

.field final synthetic o〇0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl;


# direct methods
.method private constructor <init>(Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl;Landroid/app/Activity;Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;)V
    .locals 3

    .line 2
    iput-object p1, p0, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl$BookSplitterTrimTask;->o〇0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl;

    .line 3
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/tools/ProgressAsyncTask;-><init>(Landroid/content/Context;)V

    .line 4
    iput-object p2, p0, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl$BookSplitterTrimTask;->Oo08:Landroid/app/Activity;

    .line 5
    iput-object p3, p0, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl$BookSplitterTrimTask;->O8:Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    .line 6
    invoke-virtual {p3}, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;->〇080()[[I

    move-result-object p1

    if-eqz p1, :cond_0

    .line 7
    array-length p2, p1

    const/4 p3, 0x0

    :goto_0
    if-ge p3, p2, :cond_0

    aget-object v0, p1, p3

    .line 8
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BookSplitterTrimTask mBorders="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " roation="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl$BookSplitterTrimTask;->O8:Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    invoke-virtual {v0}, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;->〇80〇808〇O()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MultiCaptureResultPresenterImpl"

    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 p3, p3, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl;Landroid/app/Activity;Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;L〇O8oOo0/oO80;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl$BookSplitterTrimTask;-><init>(Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl;Landroid/app/Activity;Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Void;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl$BookSplitterTrimTask;->oO80([Ljava/lang/Void;)Ljava/lang/Void;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected varargs oO80([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 8

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl$BookSplitterTrimTask;->O8:Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl$BookSplitterTrimTask;->O8:Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;->〇080()[[I

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    invoke-static {p1, v0}, Lcom/intsig/camscanner/booksplitter/Util/BooksplitterUtils;->oO80(Ljava/lang/String;[[I)[I

    .line 14
    .line 15
    .line 16
    move-result-object v7

    .line 17
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl$BookSplitterTrimTask;->O8:Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    .line 18
    .line 19
    invoke-virtual {p1}, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v3

    .line 23
    const/4 p1, 0x0

    .line 24
    invoke-static {v3, p1}, Lcom/intsig/camscanner/scanner/ScannerUtils;->decodeImageS(Ljava/lang/String;Z)I

    .line 25
    .line 26
    .line 27
    move-result p1

    .line 28
    invoke-static {p1}, Lcom/intsig/scanner/ScannerEngine;->getImageStructPointer(I)J

    .line 29
    .line 30
    .line 31
    move-result-wide v1

    .line 32
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl$BookSplitterTrimTask;->O8:Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    .line 33
    .line 34
    invoke-virtual {v0}, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;->〇080()[[I

    .line 35
    .line 36
    .line 37
    move-result-object v4

    .line 38
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl$BookSplitterTrimTask;->O8:Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    .line 39
    .line 40
    invoke-virtual {v0}, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;->〇o〇()I

    .line 41
    .line 42
    .line 43
    move-result v5

    .line 44
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl$BookSplitterTrimTask;->O8:Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    .line 45
    .line 46
    invoke-virtual {v0}, Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;->〇80〇808〇O()I

    .line 47
    .line 48
    .line 49
    move-result v6

    .line 50
    invoke-static/range {v1 .. v7}, Lcom/intsig/camscanner/booksplitter/Util/BooksplitterUtils;->〇o00〇〇Oo(JLjava/lang/String;[[III[I)Ljava/util/List;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    invoke-static {p1}, Lcom/intsig/scanner/ScannerEngine;->releaseImageS(I)I

    .line 55
    .line 56
    .line 57
    invoke-static {}, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterManager;->Oooo8o0〇()Lcom/intsig/camscanner/booksplitter/Util/BookSplitterManager;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    iget-object v1, p0, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl$BookSplitterTrimTask;->O8:Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    .line 62
    .line 63
    invoke-virtual {p1, v1, v0}, Lcom/intsig/camscanner/booksplitter/Util/BookSplitterManager;->〇80〇808〇O(Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;Ljava/util/List;)V

    .line 64
    .line 65
    .line 66
    const/4 p1, 0x0

    .line 67
    return-object p1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/lang/Void;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl$BookSplitterTrimTask;->〇80〇808〇O(Ljava/lang/Void;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method protected 〇80〇808〇O(Ljava/lang/Void;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/camscanner/tools/ProgressAsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    new-instance p1, Ljava/lang/StringBuilder;

    .line 5
    .line 6
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 7
    .line 8
    .line 9
    const-string v0, "onPostExecute, mActivity is null="

    .line 10
    .line 11
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl$BookSplitterTrimTask;->Oo08:Landroid/app/Activity;

    .line 15
    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    const/4 v0, 0x1

    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const/4 v0, 0x0

    .line 21
    :goto_0
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    const-string v0, "MultiCaptureResultPresenterImpl"

    .line 29
    .line 30
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl$BookSplitterTrimTask;->Oo08:Landroid/app/Activity;

    .line 34
    .line 35
    if-eqz p1, :cond_1

    .line 36
    .line 37
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl$BookSplitterTrimTask;->o〇0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl;

    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl$BookSplitterTrimTask;->O8:Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;

    .line 40
    .line 41
    invoke-static {p1, v0}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl;->O〇8O8〇008(Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl;Lcom/intsig/camscanner/booksplitter/mode/BookSplitterModel;)V

    .line 42
    .line 43
    .line 44
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl$BookSplitterTrimTask;->o〇0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl;

    .line 45
    .line 46
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl$BookSplitterTrimTask;->Oo08:Landroid/app/Activity;

    .line 47
    .line 48
    invoke-static {p1, v0}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl;->O8ooOoo〇(Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl;Landroid/app/Activity;)V

    .line 49
    .line 50
    .line 51
    :cond_1
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

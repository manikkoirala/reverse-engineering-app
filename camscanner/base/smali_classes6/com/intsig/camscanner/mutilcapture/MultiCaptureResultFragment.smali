.class public Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;
.super Lcom/intsig/mvp/fragment/BaseChangeFragment;
.source "MultiCaptureResultFragment.java"

# interfaces
.implements Lcom/intsig/camscanner/mutilcapture/view/MultiCaptureResultView;
.implements Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter$BorderChangeCallBack;


# static fields
.field private static 〇00O0:Ljava/lang/String; = "MultiCaptureResultFragment"


# instance fields
.field private final O0O:I

.field private O88O:Landroid/os/Handler;

.field O8o08O8O:Lcom/intsig/camscanner/view/MagnifierView;

.field private final OO:I

.field OO〇00〇8oO:Landroid/view/View;

.field public Oo80:Landroid/view/View$OnClickListener;

.field private O〇o88o08〇:Lcom/intsig/camscanner/Client/ProgressDialogClient;

.field private final o0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;

.field private o8o:Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter$LoadImageCallBack;

.field private o8oOOo:I

.field private o8〇OO0〇0o:Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;

.field private oOO〇〇:Z

.field oOo0:Lcom/intsig/view/ImageTextButton;

.field oOo〇8o008:Lcom/intsig/view/ImageTextButton;

.field private oo8ooo8O:Z

.field private ooo0〇〇O:I

.field o〇00O:Lcom/intsig/camscanner/view/PreviewViewPager;

.field private o〇oO:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

.field 〇080OO8〇0:Lcom/intsig/view/ImageTextButton;

.field 〇08O〇00〇o:Landroid/widget/TextView;

.field private 〇08〇o0O:I

.field 〇0O:Lcom/intsig/view/ImageTextButton;

.field private final 〇8〇oO〇〇8o:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Lcom/intsig/camscanner/loadimage/CacheKey;",
            ">;"
        }
    .end annotation
.end field

.field private final 〇OOo8〇0:I

.field private 〇O〇〇O8:Ljava/lang/String;

.field private 〇o0O:Ljava/lang/String;

.field private final 〇〇08O:I

.field private 〇〇o〇:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 4

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl;

    .line 5
    .line 6
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenterImpl;-><init>(Lcom/intsig/camscanner/mutilcapture/view/MultiCaptureResultView;)V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;

    .line 10
    .line 11
    const/16 v0, 0x65

    .line 12
    .line 13
    iput v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇OOo8〇0:I

    .line 14
    .line 15
    const/16 v1, 0x66

    .line 16
    .line 17
    iput v1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->OO:I

    .line 18
    .line 19
    const/4 v2, 0x0

    .line 20
    iput-object v2, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;

    .line 21
    .line 22
    new-instance v3, Ljava/util/HashSet;

    .line 23
    .line 24
    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 25
    .line 26
    .line 27
    iput-object v3, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇8〇oO〇〇8o:Ljava/util/HashSet;

    .line 28
    .line 29
    iput v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇〇08O:I

    .line 30
    .line 31
    iput v1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O0O:I

    .line 32
    .line 33
    const/4 v0, 0x1

    .line 34
    iput v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8oOOo:I

    .line 35
    .line 36
    const-string v0, "batch"

    .line 37
    .line 38
    iput-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇O〇〇O8:Ljava/lang/String;

    .line 39
    .line 40
    iput-object v2, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇o0O:Ljava/lang/String;

    .line 41
    .line 42
    new-instance v0, Landroid/os/Handler;

    .line 43
    .line 44
    new-instance v1, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment$1;

    .line 45
    .line 46
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment$1;-><init>(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;)V

    .line 47
    .line 48
    .line 49
    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    .line 50
    .line 51
    .line 52
    iput-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O88O:Landroid/os/Handler;

    .line 53
    .line 54
    const/4 v0, 0x0

    .line 55
    iput-boolean v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->oOO〇〇:Z

    .line 56
    .line 57
    new-instance v1, LoOo〇08〇/〇〇808〇;

    .line 58
    .line 59
    invoke-direct {v1, p0}, LoOo〇08〇/〇〇808〇;-><init>(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;)V

    .line 60
    .line 61
    .line 62
    iput-object v1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8o:Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter$LoadImageCallBack;

    .line 63
    .line 64
    iput-boolean v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->oo8ooo8O:Z

    .line 65
    .line 66
    new-instance v0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment$2;

    .line 67
    .line 68
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment$2;-><init>(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;)V

    .line 69
    .line 70
    .line 71
    iput-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o〇oO:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    .line 72
    .line 73
    iput-object v2, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇〇o〇:Landroid/widget/EditText;

    .line 74
    .line 75
    new-instance v0, LoOo〇08〇/〇O00;

    .line 76
    .line 77
    invoke-direct {v0, p0}, LoOo〇08〇/〇O00;-><init>(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;)V

    .line 78
    .line 79
    .line 80
    iput-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->Oo80:Landroid/view/View$OnClickListener;

    .line 81
    .line 82
    return-void
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method private synthetic O00OoO〇(Lcom/intsig/camscanner/view/ImageEditView;Lcom/intsig/camscanner/mutilcapture/mode/PagePara;)V
    .locals 4

    .line 1
    iget-object v0, p2, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o〇00O:[I

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/camscanner/util/Util;->〇o0O0O8([I)[F

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    iget v1, p2, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇0O:F

    .line 8
    .line 9
    const/4 v2, 0x1

    .line 10
    invoke-virtual {p1, v0, v1, v2}, Lcom/intsig/camscanner/view/ImageEditView;->o8oO〇([FFZ)V

    .line 11
    .line 12
    .line 13
    iput-boolean v2, p2, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇8〇oO〇〇8o:Z

    .line 14
    .line 15
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇8〇〇8o(Z)V

    .line 16
    .line 17
    .line 18
    iget v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->ooo0〇〇O:I

    .line 19
    .line 20
    iget-object v1, p2, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->ooo0〇〇O:[I

    .line 21
    .line 22
    iget-object v3, p2, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o〇00O:[I

    .line 23
    .line 24
    invoke-static {v0, v1, v3}, Lcom/intsig/camscanner/scanner/ScannerUtils;->checkCropBounds(I[I[I)Z

    .line 25
    .line 26
    .line 27
    move-result v0

    .line 28
    iput-boolean v0, p2, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o8〇OO0〇0o:Z

    .line 29
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8O〇008(Lcom/intsig/camscanner/view/ImageEditView;Lcom/intsig/camscanner/mutilcapture/mode/PagePara;)V

    .line 31
    .line 32
    .line 33
    const/4 v0, 0x0

    .line 34
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/view/ImageEditView;->〇oOO8O8(Z)[I

    .line 35
    .line 36
    .line 37
    move-result-object p1

    .line 38
    iput-object p1, p2, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇OOo8〇0:[I

    .line 39
    .line 40
    iget-object v0, p2, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇08O〇00〇o:[I

    .line 41
    .line 42
    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([I[I)Z

    .line 43
    .line 44
    .line 45
    move-result p1

    .line 46
    xor-int/2addr p1, v2

    .line 47
    iput-boolean p1, p2, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->OO〇00〇8oO:Z

    .line 48
    .line 49
    sget-object p1, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 50
    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    .line 52
    .line 53
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 54
    .line 55
    .line 56
    const-string v1, "mPagePara.mNeedTrim "

    .line 57
    .line 58
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    iget-boolean v1, p2, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇8〇oO〇〇8o:Z

    .line 62
    .line 63
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    const-string v1, " pagePara.boundChanged="

    .line 67
    .line 68
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    iget-boolean p2, p2, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->OO〇00〇8oO:Z

    .line 72
    .line 73
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object p2

    .line 80
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method static bridge synthetic O0O0〇(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇08〇o0O:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O0〇()V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    instance-of v1, v0, Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 6
    .line 7
    if-eqz v1, :cond_2

    .line 8
    .line 9
    check-cast v0, Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;

    .line 12
    .line 13
    invoke-interface {v1}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;->〇080()Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    const-string v2, ""

    .line 18
    .line 19
    if-nez v1, :cond_0

    .line 20
    .line 21
    sget-object v1, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 22
    .line 23
    const-string v3, "parcelDocInfo == null"

    .line 24
    .line 25
    invoke-static {v1, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, v2}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 29
    .line 30
    .line 31
    return-void

    .line 32
    :cond_0
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O〇0O〇Oo〇o(Lcom/intsig/camscanner/datastruct/ParcelDocInfo;)Z

    .line 33
    .line 34
    .line 35
    move-result v3

    .line 36
    if-eqz v3, :cond_1

    .line 37
    .line 38
    const/4 v2, 0x3

    .line 39
    invoke-virtual {v0, v2}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇〇(I)V

    .line 40
    .line 41
    .line 42
    iget-object v2, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->O8o08O8O:Ljava/lang/String;

    .line 43
    .line 44
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 45
    .line 46
    .line 47
    move-result v2

    .line 48
    if-nez v2, :cond_2

    .line 49
    .line 50
    iget-object v1, v1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->O8o08O8O:Ljava/lang/String;

    .line 51
    .line 52
    invoke-virtual {v0, v1}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 53
    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_1
    invoke-virtual {v0, v2}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 57
    .line 58
    .line 59
    :cond_2
    :goto_0
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static synthetic O0〇0(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇ooO〇000(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private synthetic O88(Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Ljava/lang/String;Ljava/lang/String;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O8〇o0〇〇8(Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    const/4 p1, 0x0

    .line 5
    return-object p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic O880O〇(I)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o〇00O:Lcom/intsig/camscanner/view/PreviewViewPager;

    .line 2
    .line 3
    if-eqz v0, :cond_3

    .line 4
    .line 5
    invoke-virtual {v0}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-ne p1, v0, :cond_3

    .line 10
    .line 11
    iget-boolean v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->oOO〇〇:Z

    .line 12
    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 v0, 0x1

    .line 17
    iput-boolean v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->oOO〇〇:Z

    .line 18
    .line 19
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇OO0()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    const v1, 0x7fffffff

    .line 24
    .line 25
    .line 26
    if-ge v0, v1, :cond_1

    .line 27
    .line 28
    add-int/lit8 v0, v0, 0x1

    .line 29
    .line 30
    :cond_1
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇o0880o(I)V

    .line 31
    .line 32
    .line 33
    const/4 v1, 0x3

    .line 34
    if-le v0, v1, :cond_2

    .line 35
    .line 36
    return-void

    .line 37
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o〇00O:Lcom/intsig/camscanner/view/PreviewViewPager;

    .line 38
    .line 39
    new-instance v1, LoOo〇08〇/〇8o8o〇;

    .line 40
    .line 41
    invoke-direct {v1, p0, p1}, LoOo〇08〇/〇8o8o〇;-><init>(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;I)V

    .line 42
    .line 43
    .line 44
    const-wide/16 v2, 0x1f4

    .line 45
    .line 46
    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 47
    .line 48
    .line 49
    :cond_3
    :goto_0
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private synthetic O8O(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o〇00O:Lcom/intsig/camscanner/view/PreviewViewPager;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->getCurrentActivity()Landroid/app/Activity;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    if-eqz v0, :cond_4

    .line 11
    .line 12
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    if-eqz v1, :cond_1

    .line 17
    .line 18
    goto :goto_2

    .line 19
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o〇00O:Lcom/intsig/camscanner/view/PreviewViewPager;

    .line 20
    .line 21
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    if-lez v1, :cond_2

    .line 26
    .line 27
    div-int/lit8 v1, v1, 0x3

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_2
    const/16 v1, 0x2d

    .line 31
    .line 32
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/Util;->〇0〇O0088o(Landroid/content/Context;I)I

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o〇00O:Lcom/intsig/camscanner/view/PreviewViewPager;

    .line 37
    .line 38
    if-lez p1, :cond_3

    .line 39
    .line 40
    const/4 p1, 0x1

    .line 41
    goto :goto_1

    .line 42
    :cond_3
    const/4 p1, 0x0

    .line 43
    :goto_1
    invoke-virtual {v0, v1, p1}, Lcom/intsig/camscanner/view/PreviewViewPager;->〇80〇808〇O(IZ)V

    .line 44
    .line 45
    .line 46
    :cond_4
    :goto_2
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method static bridge synthetic O8〇8〇O80(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->oo8ooo8O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O8〇o0〇〇8(Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v1, p1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO:Ljava/lang/String;

    .line 6
    .line 7
    const v2, 0x7f130420

    .line 8
    .line 9
    .line 10
    const/4 v3, 0x0

    .line 11
    new-instance v6, LoOo〇08〇/〇o〇;

    .line 12
    .line 13
    invoke-direct {v6, p0, p1, p2}, LoOo〇08〇/〇o〇;-><init>(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    new-instance v7, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment$3;

    .line 17
    .line 18
    invoke-direct {v7, p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment$3;-><init>(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;)V

    .line 19
    .line 20
    .line 21
    iget-wide v8, p1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 22
    .line 23
    move-object v4, p2

    .line 24
    move-object v5, p3

    .line 25
    invoke-static/range {v0 .. v9}, Lcom/intsig/camscanner/app/DialogUtils;->o88〇OO08〇(Landroid/app/Activity;Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/app/DialogUtils$OnDocTitleEditListener;Lcom/intsig/camscanner/app/DialogUtils$OnTemplateSettingsListener;J)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic OO0O(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic OO〇〇o0oO(Landroid/view/View;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;

    .line 2
    .line 3
    invoke-interface {p1}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;->〇080()Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    if-nez p1, :cond_0

    .line 8
    .line 9
    sget-object p1, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 10
    .line 11
    const-string v0, "parcelDocInfo == null"

    .line 12
    .line 13
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void

    .line 17
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O〇0O〇Oo〇o(Lcom/intsig/camscanner/datastruct/ParcelDocInfo;)Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-nez v0, :cond_1

    .line 22
    .line 23
    sget-object p1, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 24
    .line 25
    const-string v0, "not new doc"

    .line 26
    .line 27
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    return-void

    .line 31
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 32
    .line 33
    const-string v1, "rename"

    .line 34
    .line 35
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    iget-object v0, p1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->O8o08O8O:Ljava/lang/String;

    .line 39
    .line 40
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0, p1, v0, v1}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O8〇o0〇〇8(Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private synthetic OoO〇OOo8o(Landroid/content/DialogInterface;I)V
    .locals 2

    .line 1
    sget-object p1, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 2
    .line 3
    const-string p2, "showAutoAdjustBorderDialog auto trime"

    .line 4
    .line 5
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string p1, "CSAutoCropNotice"

    .line 9
    .line 10
    const-string p2, "auto_crop"

    .line 11
    .line 12
    invoke-static {p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;

    .line 16
    .line 17
    iget-object p2, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;

    .line 18
    .line 19
    invoke-virtual {p2}, Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;->〇o〇()Ljava/util/List;

    .line 20
    .line 21
    .line 22
    move-result-object p2

    .line 23
    invoke-direct {p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇o〇88()Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    new-instance v1, LoOo〇08〇/OO0o〇〇;

    .line 28
    .line 29
    invoke-direct {v1, p0}, LoOo〇08〇/OO0o〇〇;-><init>(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;)V

    .line 30
    .line 31
    .line 32
    invoke-interface {p1, p2, v0, v1}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;->〇O00(Ljava/util/List;ZLjava/lang/Runnable;)V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic Ooo8o(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;Lcom/intsig/camscanner/view/ImageEditView;Lcom/intsig/camscanner/mutilcapture/mode/PagePara;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O00OoO〇(Lcom/intsig/camscanner/view/ImageEditView;Lcom/intsig/camscanner/mutilcapture/mode/PagePara;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private synthetic OooO〇(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;

    .line 2
    .line 3
    invoke-interface {p1}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;->〇80〇808〇O()V

    .line 4
    .line 5
    .line 6
    sget-object p1, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 7
    .line 8
    const-string p2, "save"

    .line 9
    .line 10
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private O〇00O(Lcom/intsig/camscanner/mutilcapture/mode/PagePara;Lcom/intsig/camscanner/view/ImageEditView;)V
    .locals 2

    .line 1
    iget-boolean v0, p1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇8〇oO〇〇8o:Z

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇8〇〇8o(Z)V

    .line 4
    .line 5
    .line 6
    const v0, -0xe64364

    .line 7
    .line 8
    .line 9
    invoke-virtual {p2, v0}, Lcom/intsig/camscanner/view/ImageEditView;->setLinePaintColor(I)V

    .line 10
    .line 11
    .line 12
    iget v0, p1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇0O:F

    .line 13
    .line 14
    iget-object v1, p1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo〇8o008:Ljava/lang/String;

    .line 15
    .line 16
    invoke-virtual {p2, v0, v1}, Lcom/intsig/camscanner/view/ImageEditView;->〇8(FLjava/lang/String;)V

    .line 17
    .line 18
    .line 19
    const/4 v0, 0x0

    .line 20
    invoke-virtual {p2, v0}, Lcom/intsig/camscanner/view/ImageEditView;->〇oOO8O8(Z)[I

    .line 21
    .line 22
    .line 23
    move-result-object p2

    .line 24
    iput-object p2, p1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇OOo8〇0:[I

    .line 25
    .line 26
    iget-object v0, p1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇08O〇00〇o:[I

    .line 27
    .line 28
    invoke-static {v0, p2}, Ljava/util/Arrays;->equals([I[I)Z

    .line 29
    .line 30
    .line 31
    move-result p2

    .line 32
    xor-int/lit8 p2, p2, 0x1

    .line 33
    .line 34
    iput-boolean p2, p1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->OO〇00〇8oO:Z

    .line 35
    .line 36
    sget-object p2, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 37
    .line 38
    new-instance v0, Ljava/lang/StringBuilder;

    .line 39
    .line 40
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 41
    .line 42
    .line 43
    const-string v1, "mPagePara.mNeedTrim "

    .line 44
    .line 45
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    iget-boolean v1, p1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇8〇oO〇〇8o:Z

    .line 49
    .line 50
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    const-string v1, " pagePara.boundChanged="

    .line 54
    .line 55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    iget-boolean p1, p1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->OO〇00〇8oO:Z

    .line 59
    .line 60
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private synthetic O〇080〇o0(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O08〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private O〇0O〇Oo〇o(Lcom/intsig/camscanner/datastruct/ParcelDocInfo;)Z
    .locals 2
    .param p1    # Lcom/intsig/camscanner/datastruct/ParcelDocInfo;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8oOOo:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    iget-boolean p1, p1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO〇00〇8oO:Z

    .line 7
    .line 8
    if-eqz p1, :cond_0

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v1, 0x0

    .line 12
    :goto_0
    return v1
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic O〇8〇008(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇0o0oO〇〇0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic O〇〇O80o8(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O80OO()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic o00〇88〇08(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇0o88Oo〇(Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Ljava/lang/String;Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private synthetic o0O0O〇〇〇0(Landroid/content/DialogInterface;I)V
    .locals 1

    .line 1
    sget-object p1, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 2
    .line 3
    const-string p2, "showConfirmDeleteDialog ok"

    .line 4
    .line 5
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string p1, "delete"

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇oO〇08o()Lorg/json/JSONObject;

    .line 11
    .line 12
    .line 13
    move-result-object p2

    .line 14
    const-string v0, "CSCrop"

    .line 15
    .line 16
    invoke-static {v0, p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 17
    .line 18
    .line 19
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;

    .line 20
    .line 21
    iget p2, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇08〇o0O:I

    .line 22
    .line 23
    invoke-interface {p1, p2}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;->〇o〇(I)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private o0OO()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget v1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇08〇o0O:I

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;->〇o00〇〇Oo(I)Lcom/intsig/camscanner/mutilcapture/mode/PagePara;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    if-nez v0, :cond_1

    .line 13
    .line 14
    sget-object v0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 15
    .line 16
    const-string v1, "updatePageSelected mPagePara == null"

    .line 17
    .line 18
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    return-void

    .line 22
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;

    .line 23
    .line 24
    iget-wide v2, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o0:J

    .line 25
    .line 26
    invoke-interface {v1, v2, v3}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;->〇〇888(J)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic o0Oo(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O〇〇o8O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private o0〇〇00(I)Lcom/intsig/camscanner/view/ImageEditView;
    .locals 4

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇08〇o0O:I

    .line 2
    .line 3
    add-int/lit8 v1, v0, -0x1

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-lt p1, v1, :cond_0

    .line 7
    .line 8
    add-int/lit8 v0, v0, 0x1

    .line 9
    .line 10
    if-gt p1, v0, :cond_0

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o〇00O:Lcom/intsig/camscanner/view/PreviewViewPager;

    .line 13
    .line 14
    new-instance v1, Ljava/lang/StringBuilder;

    .line 15
    .line 16
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 17
    .line 18
    .line 19
    const-string v3, "MultiCaptureImagePagerAdapter"

    .line 20
    .line 21
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    goto :goto_0

    .line 36
    :cond_0
    move-object p1, v2

    .line 37
    :goto_0
    if-nez p1, :cond_1

    .line 38
    .line 39
    return-object v2

    .line 40
    :cond_1
    check-cast p1, Lcom/intsig/camscanner/view/ImageEditView;

    .line 41
    .line 42
    return-object p1
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private synthetic o808o8o08(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O0o0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic o88(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->oOO8oo0(Ljava/util/List;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o880(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O〇080〇o0(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private o88o88(Lcom/intsig/camscanner/view/ImageEditView;Lcom/intsig/camscanner/mutilcapture/mode/PagePara;)V
    .locals 2

    .line 1
    iget-boolean v0, p2, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇8〇oO〇〇8o:Z

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    iget-object v0, p2, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇OOo8〇0:[I

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    invoke-static {v0}, Lcom/intsig/camscanner/util/Util;->〇o0O0O8([I)[F

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    iget p2, p2, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇0O:F

    .line 14
    .line 15
    const/4 v1, 0x1

    .line 16
    invoke-virtual {p1, v0, p2, v1}, Lcom/intsig/camscanner/view/ImageEditView;->o8oO〇([FFZ)V

    .line 17
    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    sget-object p1, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 21
    .line 22
    const-string p2, "bindBitmap pageId  pagePara.currentBounds == null"

    .line 23
    .line 24
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_1
    iget v0, p2, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇0O:F

    .line 29
    .line 30
    iget-object p2, p2, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo〇8o008:Ljava/lang/String;

    .line 31
    .line 32
    invoke-virtual {p1, v0, p2}, Lcom/intsig/camscanner/view/ImageEditView;->〇8(FLjava/lang/String;)V

    .line 33
    .line 34
    .line 35
    :goto_0
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method private o8O〇008(Lcom/intsig/camscanner/view/ImageEditView;Lcom/intsig/camscanner/mutilcapture/mode/PagePara;)V
    .locals 1

    .line 1
    iget-boolean v0, p2, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇8〇oO〇〇8o:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-boolean p2, p2, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o8〇OO0〇0o:Z

    .line 6
    .line 7
    if-nez p2, :cond_0

    .line 8
    .line 9
    const/16 p2, -0x6b00

    .line 10
    .line 11
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/view/ImageEditView;->setLinePaintColor(I)V

    .line 12
    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const p2, -0xe64364

    .line 16
    .line 17
    .line 18
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/view/ImageEditView;->setLinePaintColor(I)V

    .line 19
    .line 20
    .line 21
    :goto_0
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private oOO8oo0(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mutilcapture/mode/PagePara;",
            ">;)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "setupImageViewPager"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8oOOo:I

    .line 9
    .line 10
    const/4 v1, 0x5

    .line 11
    if-ne v0, v1, :cond_0

    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    const/4 v7, 0x0

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO0〇Oo()Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    move v7, v0

    .line 21
    :goto_0
    new-instance v0, Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;

    .line 22
    .line 23
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    iget v3, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->ooo0〇〇O:I

    .line 28
    .line 29
    iget-object v4, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MagnifierView;

    .line 30
    .line 31
    iget-object v5, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇080OO8〇0:Lcom/intsig/view/ImageTextButton;

    .line 32
    .line 33
    iget-object v6, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇8〇oO〇〇8o:Ljava/util/HashSet;

    .line 34
    .line 35
    move-object v1, v0

    .line 36
    move-object v8, p0

    .line 37
    invoke-direct/range {v1 .. v8}, Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;-><init>(Landroid/app/Activity;ILcom/intsig/camscanner/view/MagnifierView;Lcom/intsig/view/ImageTextButton;Ljava/util/HashSet;ZLcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter$BorderChangeCallBack;)V

    .line 38
    .line 39
    .line 40
    iput-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;

    .line 41
    .line 42
    iget-object v1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o〇00O:Lcom/intsig/camscanner/view/PreviewViewPager;

    .line 43
    .line 44
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;->〇O8o08O(Landroid/view/ViewGroup;)V

    .line 45
    .line 46
    .line 47
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;

    .line 48
    .line 49
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;->〇8o8o〇(Ljava/util/List;)V

    .line 50
    .line 51
    .line 52
    if-eqz p1, :cond_1

    .line 53
    .line 54
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 55
    .line 56
    .line 57
    move-result p1

    .line 58
    const/4 v0, 0x1

    .line 59
    if-le p1, v0, :cond_1

    .line 60
    .line 61
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;

    .line 62
    .line 63
    invoke-interface {p1}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;->〇8o8o〇()Z

    .line 64
    .line 65
    .line 66
    move-result p1

    .line 67
    if-nez p1, :cond_1

    .line 68
    .line 69
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;

    .line 70
    .line 71
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8o:Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter$LoadImageCallBack;

    .line 72
    .line 73
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter$LoadImageCallBack;)V

    .line 74
    .line 75
    .line 76
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o〇00O:Lcom/intsig/camscanner/view/PreviewViewPager;

    .line 77
    .line 78
    const/4 v0, 0x2

    .line 79
    invoke-virtual {p1, v0}, Landroidx/viewpager/widget/ViewPager;->setOffscreenPageLimit(I)V

    .line 80
    .line 81
    .line 82
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o〇00O:Lcom/intsig/camscanner/view/PreviewViewPager;

    .line 83
    .line 84
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;

    .line 85
    .line 86
    invoke-virtual {p1, v0}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 87
    .line 88
    .line 89
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;

    .line 90
    .line 91
    invoke-interface {p1}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;->〇O〇()I

    .line 92
    .line 93
    .line 94
    move-result p1

    .line 95
    iput p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇08〇o0O:I

    .line 96
    .line 97
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o〇00O:Lcom/intsig/camscanner/view/PreviewViewPager;

    .line 98
    .line 99
    invoke-virtual {v0, p1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(I)V

    .line 100
    .line 101
    .line 102
    iget p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇08〇o0O:I

    .line 103
    .line 104
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇〇〇OOO〇〇(I)V

    .line 105
    .line 106
    .line 107
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o〇00O:Lcom/intsig/camscanner/view/PreviewViewPager;

    .line 108
    .line 109
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o〇oO:Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;

    .line 110
    .line 111
    invoke-virtual {p1, v0}, Landroidx/viewpager/widget/ViewPager;->addOnPageChangeListener(Landroidx/viewpager/widget/ViewPager$OnPageChangeListener;)V

    .line 112
    .line 113
    .line 114
    return-void
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static synthetic oOoO8OO〇(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->OO〇〇o0oO(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oO〇oo(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o〇08oO80o(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic oooO888(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->OO0O(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static synthetic o〇08oO80o(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    sget-object p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 2
    .line 3
    const-string p1, "cancel"

    .line 4
    .line 5
    invoke-static {p0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o〇0〇o(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o808o8o08(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic o〇O8OO(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇OoO0o0(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private o〇OoO0(I)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;

    .line 2
    .line 3
    iget v1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇08〇o0O:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;->〇o00〇〇Oo(I)Lcom/intsig/camscanner/mutilcapture/mode/PagePara;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    sget-object p1, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 12
    .line 13
    const-string v0, "adjustCurrentPage mPagePara == null"

    .line 14
    .line 15
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    return-void

    .line 19
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;

    .line 20
    .line 21
    iget-wide v2, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o0:J

    .line 22
    .line 23
    invoke-virtual {v1, v2, v3}, Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;->O8(J)I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0〇〇00(I)Lcom/intsig/camscanner/view/ImageEditView;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    if-nez v1, :cond_1

    .line 32
    .line 33
    sget-object p1, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 34
    .line 35
    const-string v0, "adjustCurrentPage mImageView == null"

    .line 36
    .line 37
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    .line 39
    .line 40
    return-void

    .line 41
    :cond_1
    const/4 v2, 0x1

    .line 42
    iput-boolean v2, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->OO〇00〇8oO:Z

    .line 43
    .line 44
    iget v3, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->O8o08O8O:I

    .line 45
    .line 46
    add-int/2addr v3, p1

    .line 47
    rem-int/lit16 v3, v3, 0x168

    .line 48
    .line 49
    iput v3, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->O8o08O8O:I

    .line 50
    .line 51
    invoke-virtual {v1}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getRotateBitmap()Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 52
    .line 53
    .line 54
    move-result-object p1

    .line 55
    if-eqz p1, :cond_2

    .line 56
    .line 57
    iget v0, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->O8o08O8O:I

    .line 58
    .line 59
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->oO80(I)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v1, p1, v2}, Lcom/intsig/camscanner/view/ImageEditView;->oO(Lcom/intsig/camscanner/loadimage/RotateBitmap;Z)V

    .line 63
    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_2
    sget-object p1, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 67
    .line 68
    const-string v0, "imageEditView.getRotateBitmap() is null"

    .line 69
    .line 70
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    :goto_0
    return-void
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private synthetic o〇o08〇(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 2
    .line 3
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 4
    .line 5
    .line 6
    sget-object p1, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 7
    .line 8
    const-string p2, "not save"

    .line 9
    .line 10
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic o〇oo(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;)Lorg/json/JSONObject;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇oO〇08o()Lorg/json/JSONObject;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇088O(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o〇o08〇(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic 〇08O(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;)Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private 〇0o0oO〇〇0()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "showAutoAdjustBorderDialog"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSAutoCropNotice"

    .line 9
    .line 10
    invoke-static {v0}, Lcom/intsig/camscanner/log/LogAgentData;->OO0o〇〇(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 14
    .line 15
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 20
    .line 21
    .line 22
    const v1, 0x7f1308fc

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    const v1, 0x7f1308f8

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    new-instance v1, LoOo〇08〇/o〇0;

    .line 37
    .line 38
    invoke-direct {v1}, LoOo〇08〇/o〇0;-><init>()V

    .line 39
    .line 40
    .line 41
    const v2, 0x7f13057e

    .line 42
    .line 43
    .line 44
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 45
    .line 46
    .line 47
    move-result-object v0

    .line 48
    new-instance v1, LoOo〇08〇/〇〇888;

    .line 49
    .line 50
    invoke-direct {v1, p0}, LoOo〇08〇/〇〇888;-><init>(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;)V

    .line 51
    .line 52
    .line 53
    const v2, 0x7f1308f7

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 65
    .line 66
    .line 67
    return-void
    .line 68
.end method

.method private synthetic 〇0o88Oo〇(Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .line 1
    const/4 v0, 0x0

    .line 2
    iget-object v1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 3
    .line 4
    iget-object v2, p1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO:Ljava/lang/String;

    .line 5
    .line 6
    const/4 v4, 0x0

    .line 7
    new-instance v5, LoOo〇08〇/oO80;

    .line 8
    .line 9
    invoke-direct {v5, p0, p1, p3}, LoOo〇08〇/oO80;-><init>(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    new-instance v6, LoOo〇08〇/〇80〇808〇O;

    .line 13
    .line 14
    invoke-direct {v6, p0, p3, p2, p1}, LoOo〇08〇/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;)V

    .line 15
    .line 16
    .line 17
    move-object v3, p3

    .line 18
    invoke-static/range {v0 .. v6}, Lcom/intsig/camscanner/mainmenu/SensitiveWordsChecker;->〇080(Ljava/lang/Boolean;Landroidx/appcompat/app/AppCompatActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method static bridge synthetic 〇0oO〇oo00(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->oo8ooo8O:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇0ooOOo(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->OoO〇OOo8o(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇0〇0(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O880O〇(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇8O0880(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇08〇o0O:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇8〇80o(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O8O(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0Oo(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇8〇〇8o(Z)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "updateResetRegionView needTrim="

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇080OO8〇0:Lcom/intsig/view/ImageTextButton;

    .line 24
    .line 25
    if-nez v0, :cond_0

    .line 26
    .line 27
    sget-object p1, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 28
    .line 29
    const-string v0, "mResetRegionView IS NULL. SO CAN NOT RENDER VIEW."

    .line 30
    .line 31
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    return-void

    .line 35
    :cond_0
    if-eqz p1, :cond_1

    .line 36
    .line 37
    const p1, 0x7f0806c7

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, p1}, Lcom/intsig/view/ImageTextButton;->setImageResource(I)V

    .line 41
    .line 42
    .line 43
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇080OO8〇0:Lcom/intsig/view/ImageTextButton;

    .line 44
    .line 45
    const v0, 0x7f130c02

    .line 46
    .line 47
    .line 48
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    invoke-virtual {p1, v0}, Lcom/intsig/view/ImageTextButton;->setTipText(Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇080OO8〇0:Lcom/intsig/view/ImageTextButton;

    .line 56
    .line 57
    invoke-virtual {p1}, Landroid/view/View;->invalidate()V

    .line 58
    .line 59
    .line 60
    goto :goto_0

    .line 61
    :cond_1
    const p1, 0x7f0805c7

    .line 62
    .line 63
    .line 64
    invoke-virtual {v0, p1}, Lcom/intsig/view/ImageTextButton;->setImageResource(I)V

    .line 65
    .line 66
    .line 67
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇080OO8〇0:Lcom/intsig/view/ImageTextButton;

    .line 68
    .line 69
    const v0, 0x7f1300ab

    .line 70
    .line 71
    .line 72
    invoke-virtual {p0, v0}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    invoke-virtual {p1, v0}, Lcom/intsig/view/ImageTextButton;->setTipText(Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    :goto_0
    return-void
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method static bridge synthetic 〇O0o〇〇o(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8oOOo:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O〇〇O80o8(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇O8〇8000(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;)Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method static bridge synthetic 〇O8〇8O0oO()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static synthetic 〇OoO0o0(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    sget-object p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 2
    .line 3
    const-string p1, "showConfirmDeleteDialog cancel"

    .line 4
    .line 5
    invoke-static {p0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method static bridge synthetic 〇Oo〇O(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;Landroid/widget/EditText;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇〇o〇:Landroid/widget/EditText;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇o08(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇ooO8Ooo〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇oO88o(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O〇0o8o8〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method private synthetic 〇oOO80o(Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;)Lkotlin/Unit;
    .locals 4

    .line 1
    invoke-static {p1}, Lcom/intsig/util/WordFilter;->O8(Ljava/lang/String;)Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-nez v1, :cond_1

    .line 10
    .line 11
    sget-object v1, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->〇080:Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;

    .line 12
    .line 13
    iget-wide v2, p3, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 14
    .line 15
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    const-string v3, "MultiCaptureResultFragment.showRenameDlg"

    .line 20
    .line 21
    invoke-virtual {v1, v3, p1, p2, v2}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager;->OoO8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 22
    .line 23
    .line 24
    invoke-static {p2, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 25
    .line 26
    .line 27
    move-result p1

    .line 28
    if-eqz p1, :cond_0

    .line 29
    .line 30
    sget-object p1, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->UNDEFINED:Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_0
    sget-object p1, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->CUSTOM:Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;

    .line 34
    .line 35
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/ocrapi/rename/OcrRenameManager$TitleSource;->getType()I

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    iput-object v0, p3, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->O8o08O8O:Ljava/lang/String;

    .line 40
    .line 41
    iput p1, p3, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇8〇oO〇〇8o:I

    .line 42
    .line 43
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 44
    .line 45
    invoke-virtual {p1, v0}, Landroid/app/Activity;->setTitle(Ljava/lang/CharSequence;)V

    .line 46
    .line 47
    .line 48
    :cond_1
    const/4 p1, 0x0

    .line 49
    return-object p1
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private 〇oO〇08o()Lorg/json/JSONObject;
    .locals 3

    .line 1
    new-instance v0, Lorg/json/JSONObject;

    .line 2
    .line 3
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 4
    .line 5
    .line 6
    :try_start_0
    iget-object v1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇O〇〇O8:Ljava/lang/String;

    .line 7
    .line 8
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    if-nez v1, :cond_0

    .line 13
    .line 14
    const-string v1, "from"

    .line 15
    .line 16
    iget-object v2, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇O〇〇O8:Ljava/lang/String;

    .line 17
    .line 18
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 19
    .line 20
    .line 21
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇o0O:Ljava/lang/String;

    .line 22
    .line 23
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    if-nez v1, :cond_1

    .line 28
    .line 29
    const-string v1, "from_part"

    .line 30
    .line 31
    iget-object v2, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇o0O:Ljava/lang/String;

    .line 32
    .line 33
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :catch_0
    move-exception v1

    .line 38
    sget-object v2, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 39
    .line 40
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 41
    .line 42
    .line 43
    :cond_1
    :goto_0
    return-object v0
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private synthetic 〇ooO8Ooo〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroidx/viewpager/widget/PagerAdapter;->notifyDataSetChanged()V

    .line 4
    .line 5
    .line 6
    iget v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇08〇o0O:I

    .line 7
    .line 8
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇〇〇OOO〇〇(I)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private static synthetic 〇ooO〇000(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    sget-object p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 2
    .line 3
    const-string p1, "showAutoAdjustBorderDialog cancel"

    .line 4
    .line 5
    invoke-static {p0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private 〇o〇88()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8oOOo:I

    .line 2
    .line 3
    const/4 v1, 0x5

    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    goto :goto_0

    .line 8
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO0〇Oo()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    :goto_0
    return v0
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic 〇o〇88〇8(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->OooO〇(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method private 〇o〇OO80oO(Lcom/intsig/camscanner/mutilcapture/mode/PagePara;Lcom/intsig/camscanner/view/ImageEditView;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇o〇88()Z

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    new-instance v2, LoOo〇08〇/OO0o〇〇〇〇0;

    .line 8
    .line 9
    invoke-direct {v2, p0, p2, p1}, LoOo〇08〇/OO0o〇〇〇〇0;-><init>(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;Lcom/intsig/camscanner/view/ImageEditView;Lcom/intsig/camscanner/mutilcapture/mode/PagePara;)V

    .line 10
    .line 11
    .line 12
    invoke-interface {v0, p1, v1, v2}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;->Oo08(Lcom/intsig/camscanner/mutilcapture/mode/PagePara;ZLjava/lang/Runnable;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇〇O80〇0o(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇oO88o(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public static synthetic 〇〇o0〇8(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0O0O〇〇〇0(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇〇〇0(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇oOO80o(Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;)Lkotlin/Unit;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method public static synthetic 〇〇〇00(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Ljava/lang/String;Ljava/lang/String;)Lkotlin/Unit;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O88(Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Ljava/lang/String;Ljava/lang/String;)Lkotlin/Unit;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private 〇〇〇OOO〇〇(I)V
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0OO()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;

    .line 5
    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 10
    .line 11
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    const/4 v2, 0x2

    .line 16
    new-array v2, v2, [Ljava/lang/Object;

    .line 17
    .line 18
    add-int/lit8 v3, p1, 0x1

    .line 19
    .line 20
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 21
    .line 22
    .line 23
    move-result-object v3

    .line 24
    const/4 v4, 0x0

    .line 25
    aput-object v3, v2, v4

    .line 26
    .line 27
    iget-object v3, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;

    .line 28
    .line 29
    invoke-virtual {v3}, Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;->getCount()I

    .line 30
    .line 31
    .line 32
    move-result v3

    .line 33
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 34
    .line 35
    .line 36
    move-result-object v3

    .line 37
    const/4 v5, 0x1

    .line 38
    aput-object v3, v2, v5

    .line 39
    .line 40
    const-string v3, "%d/%d"

    .line 41
    .line 42
    invoke-static {v1, v3, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    .line 48
    .line 49
    sget-object v0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 50
    .line 51
    new-instance v1, Ljava/lang/StringBuilder;

    .line 52
    .line 53
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 54
    .line 55
    .line 56
    const-string v2, "updatePageSelected pos="

    .line 57
    .line 58
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 65
    .line 66
    .line 67
    move-result-object v1

    .line 68
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    iput p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇08〇o0O:I

    .line 72
    .line 73
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;

    .line 74
    .line 75
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;->〇o00〇〇Oo(I)Lcom/intsig/camscanner/mutilcapture/mode/PagePara;

    .line 76
    .line 77
    .line 78
    move-result-object p1

    .line 79
    if-nez p1, :cond_1

    .line 80
    .line 81
    sget-object p1, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 82
    .line 83
    const-string v0, "updatePageSelected mPagePara == null"

    .line 84
    .line 85
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    return-void

    .line 89
    :cond_1
    iget-boolean v0, p1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇8〇oO〇〇8o:Z

    .line 90
    .line 91
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇8〇〇8o(Z)V

    .line 92
    .line 93
    .line 94
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;

    .line 95
    .line 96
    iget-wide v1, p1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o0:J

    .line 97
    .line 98
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;->O8(J)I

    .line 99
    .line 100
    .line 101
    move-result v0

    .line 102
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0〇〇00(I)Lcom/intsig/camscanner/view/ImageEditView;

    .line 103
    .line 104
    .line 105
    move-result-object v0

    .line 106
    if-nez v0, :cond_2

    .line 107
    .line 108
    sget-object p1, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 109
    .line 110
    const-string v0, "updatePageSelected mImageView == null"

    .line 111
    .line 112
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    .line 114
    .line 115
    return-void

    .line 116
    :cond_2
    invoke-virtual {v0}, Lcom/intsig/camscanner/view/ImageViewTouchBase;->getRotateBitmap()Lcom/intsig/camscanner/loadimage/RotateBitmap;

    .line 117
    .line 118
    .line 119
    move-result-object v1

    .line 120
    if-nez v1, :cond_3

    .line 121
    .line 122
    sget-object p1, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 123
    .line 124
    const-string v0, "rotateBitmap == null"

    .line 125
    .line 126
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    .line 128
    .line 129
    return-void

    .line 130
    :cond_3
    iget v2, p1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->O8o08O8O:I

    .line 131
    .line 132
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->oO80(I)V

    .line 133
    .line 134
    .line 135
    invoke-virtual {v0, v1, v5}, Lcom/intsig/camscanner/view/ImageEditView;->oO(Lcom/intsig/camscanner/loadimage/RotateBitmap;Z)V

    .line 136
    .line 137
    .line 138
    invoke-virtual {v1}, Lcom/intsig/camscanner/loadimage/RotateBitmap;->〇080()Landroid/graphics/Bitmap;

    .line 139
    .line 140
    .line 141
    move-result-object v1

    .line 142
    if-eqz v1, :cond_4

    .line 143
    .line 144
    iget-object v2, p1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->ooo0〇〇O:[I

    .line 145
    .line 146
    if-eqz v2, :cond_4

    .line 147
    .line 148
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    .line 149
    .line 150
    .line 151
    move-result v1

    .line 152
    int-to-float v1, v1

    .line 153
    const/high16 v2, 0x3f800000    # 1.0f

    .line 154
    .line 155
    mul-float v1, v1, v2

    .line 156
    .line 157
    iget-object v2, p1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->ooo0〇〇O:[I

    .line 158
    .line 159
    aget v2, v2, v4

    .line 160
    .line 161
    int-to-float v2, v2

    .line 162
    div-float/2addr v1, v2

    .line 163
    iput v1, p1, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇0O:F

    .line 164
    .line 165
    :cond_4
    invoke-direct {p0, v0, p1}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o88o88(Lcom/intsig/camscanner/view/ImageEditView;Lcom/intsig/camscanner/mutilcapture/mode/PagePara;)V

    .line 166
    .line 167
    .line 168
    invoke-direct {p0, v0, p1}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8O〇008(Lcom/intsig/camscanner/view/ImageEditView;Lcom/intsig/camscanner/mutilcapture/mode/PagePara;)V

    .line 169
    .line 170
    .line 171
    return-void
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method static bridge synthetic 〇〇〇O〇(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇〇〇OOO〇〇(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method


# virtual methods
.method public O08〇()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "saveDocument"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "CSPageProcess"

    .line 9
    .line 10
    const-string v1, "save"

    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    const-string v0, "next"

    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇oO〇08o()Lorg/json/JSONObject;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    const-string v2, "CSCrop"

    .line 22
    .line 23
    invoke-static {v2, v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 24
    .line 25
    .line 26
    iget v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8oOOo:I

    .line 27
    .line 28
    const/4 v1, 0x2

    .line 29
    if-eq v0, v1, :cond_1

    .line 30
    .line 31
    const/4 v1, 0x3

    .line 32
    if-eq v0, v1, :cond_1

    .line 33
    .line 34
    const/4 v1, 0x4

    .line 35
    if-eq v0, v1, :cond_1

    .line 36
    .line 37
    const/4 v1, 0x5

    .line 38
    if-eq v0, v1, :cond_1

    .line 39
    .line 40
    const/4 v1, 0x7

    .line 41
    if-eq v0, v1, :cond_1

    .line 42
    .line 43
    const/4 v1, 0x6

    .line 44
    if-ne v0, v1, :cond_0

    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;

    .line 48
    .line 49
    invoke-interface {v0}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;->oO80()V

    .line 50
    .line 51
    .line 52
    goto :goto_1

    .line 53
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;

    .line 54
    .line 55
    invoke-interface {v0}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;->〇80〇808〇O()V

    .line 56
    .line 57
    .line 58
    :goto_1
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method O0o0()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "turnRight"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "turn_right"

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇oO〇08o()Lorg/json/JSONObject;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    const-string v2, "CSCrop"

    .line 15
    .line 16
    invoke-static {v2, v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 17
    .line 18
    .line 19
    const/16 v0, 0x5a

    .line 20
    .line 21
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o〇OoO0(I)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method O80OO()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "showConfirmDeleteDialog"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 9
    .line 10
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 15
    .line 16
    .line 17
    const v1, 0x7f131d10

    .line 18
    .line 19
    .line 20
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    const v1, 0x7f130115

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    new-instance v1, LoOo〇08〇/O8;

    .line 32
    .line 33
    invoke-direct {v1}, LoOo〇08〇/O8;-><init>()V

    .line 34
    .line 35
    .line 36
    const v2, 0x7f13057e

    .line 37
    .line 38
    .line 39
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    new-instance v1, LoOo〇08〇/Oo08;

    .line 44
    .line 45
    invoke-direct {v1, p0}, LoOo〇08〇/Oo08;-><init>(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;)V

    .line 46
    .line 47
    .line 48
    const v2, 0x7f131e36

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 52
    .line 53
    .line 54
    move-result-object v0

    .line 55
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 56
    .line 57
    .line 58
    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 60
    .line 61
    .line 62
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method OO0o(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8oOOo:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method O〇0o8o8〇()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "turnLeft"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "turn_left"

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇oO〇08o()Lorg/json/JSONObject;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    const-string v2, "CSCrop"

    .line 15
    .line 16
    invoke-static {v2, v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 17
    .line 18
    .line 19
    const/16 v0, 0x10e

    .line 20
    .line 21
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o〇OoO0(I)V

    .line 22
    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method O〇〇o8O()V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "reTakeCurrentPage"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇oO〇08o()Lorg/json/JSONObject;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const-string v1, "CSCrop"

    .line 13
    .line 14
    const-string v2, "retake"

    .line 15
    .line 16
    invoke-static {v1, v2, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 17
    .line 18
    .line 19
    iget v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8oOOo:I

    .line 20
    .line 21
    const/4 v3, 0x3

    .line 22
    if-eq v0, v3, :cond_1

    .line 23
    .line 24
    const/4 v3, 0x5

    .line 25
    if-ne v0, v3, :cond_0

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;

    .line 29
    .line 30
    iget v3, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇08〇o0O:I

    .line 31
    .line 32
    invoke-interface {v0, v3}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;->〇〇8O0〇8(I)V

    .line 33
    .line 34
    .line 35
    goto :goto_1

    .line 36
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;

    .line 37
    .line 38
    iget v3, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇08〇o0O:I

    .line 39
    .line 40
    invoke-interface {v0, v3}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;->OO0o〇〇(I)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-static {v0}, Lcom/intsig/camscanner/capture/util/CaptureActivityRouterUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)Landroid/content/Intent;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    const/16 v3, 0x66

    .line 52
    .line 53
    invoke-virtual {p0, v0, v3}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 54
    .line 55
    .line 56
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇oO〇08o()Lorg/json/JSONObject;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    invoke-static {v1, v2, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 61
    .line 62
    .line 63
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public getCurrentActivity()Landroid/app/Activity;
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    sget-object p1, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v0, "onCreateView"

    .line 4
    .line 5
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerUtils;->initThreadContext()I

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    iput p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->ooo0〇〇O:I

    .line 13
    .line 14
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 15
    .line 16
    const v0, 0x7f0a0e44

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    check-cast p1, Landroid/widget/TextView;

    .line 24
    .line 25
    iput-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 28
    .line 29
    const v0, 0x7f0a1a3f

    .line 30
    .line 31
    .line 32
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    check-cast p1, Lcom/intsig/camscanner/view/PreviewViewPager;

    .line 37
    .line 38
    iput-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o〇00O:Lcom/intsig/camscanner/view/PreviewViewPager;

    .line 39
    .line 40
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 41
    .line 42
    const v0, 0x7f0a0d65

    .line 43
    .line 44
    .line 45
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    check-cast p1, Lcom/intsig/camscanner/view/MagnifierView;

    .line 50
    .line 51
    iput-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O8o08O8O:Lcom/intsig/camscanner/view/MagnifierView;

    .line 52
    .line 53
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 54
    .line 55
    const v0, 0x7f0a07b5

    .line 56
    .line 57
    .line 58
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    check-cast p1, Lcom/intsig/view/ImageTextButton;

    .line 63
    .line 64
    iput-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇080OO8〇0:Lcom/intsig/view/ImageTextButton;

    .line 65
    .line 66
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 67
    .line 68
    const v0, 0x7f0a07cb

    .line 69
    .line 70
    .line 71
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 72
    .line 73
    .line 74
    move-result-object p1

    .line 75
    check-cast p1, Lcom/intsig/view/ImageTextButton;

    .line 76
    .line 77
    iput-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇0O:Lcom/intsig/view/ImageTextButton;

    .line 78
    .line 79
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 80
    .line 81
    const v0, 0x7f0a07f5

    .line 82
    .line 83
    .line 84
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 85
    .line 86
    .line 87
    move-result-object p1

    .line 88
    check-cast p1, Lcom/intsig/view/ImageTextButton;

    .line 89
    .line 90
    iput-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->oOo〇8o008:Lcom/intsig/view/ImageTextButton;

    .line 91
    .line 92
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 93
    .line 94
    const v0, 0x7f0a0815

    .line 95
    .line 96
    .line 97
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 98
    .line 99
    .line 100
    move-result-object p1

    .line 101
    check-cast p1, Lcom/intsig/view/ImageTextButton;

    .line 102
    .line 103
    iput-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->oOo0:Lcom/intsig/view/ImageTextButton;

    .line 104
    .line 105
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 106
    .line 107
    const v0, 0x7f0a018c

    .line 108
    .line 109
    .line 110
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 111
    .line 112
    .line 113
    move-result-object p1

    .line 114
    iput-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->OO〇00〇8oO:Landroid/view/View;

    .line 115
    .line 116
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 117
    .line 118
    const v0, 0x7f0a0be1

    .line 119
    .line 120
    .line 121
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 122
    .line 123
    .line 124
    move-result-object p1

    .line 125
    new-instance v0, LoOo〇08〇/〇〇8O0〇8;

    .line 126
    .line 127
    invoke-direct {v0, p0}, LoOo〇08〇/〇〇8O0〇8;-><init>(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;)V

    .line 128
    .line 129
    .line 130
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    .line 132
    .line 133
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇0O:Lcom/intsig/view/ImageTextButton;

    .line 134
    .line 135
    new-instance v0, LoOo〇08〇/〇0〇O0088o;

    .line 136
    .line 137
    invoke-direct {v0, p0}, LoOo〇08〇/〇0〇O0088o;-><init>(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;)V

    .line 138
    .line 139
    .line 140
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    .line 142
    .line 143
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->oOo〇8o008:Lcom/intsig/view/ImageTextButton;

    .line 144
    .line 145
    new-instance v0, LoOo〇08〇/OoO8;

    .line 146
    .line 147
    invoke-direct {v0, p0}, LoOo〇08〇/OoO8;-><init>(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;)V

    .line 148
    .line 149
    .line 150
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 151
    .line 152
    .line 153
    iget-object p1, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->rootView:Landroid/view/View;

    .line 154
    .line 155
    const v0, 0x7f0a0814

    .line 156
    .line 157
    .line 158
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 159
    .line 160
    .line 161
    move-result-object p1

    .line 162
    new-instance v0, LoOo〇08〇/o800o8O;

    .line 163
    .line 164
    invoke-direct {v0, p0}, LoOo〇08〇/o800o8O;-><init>(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;)V

    .line 165
    .line 166
    .line 167
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 168
    .line 169
    .line 170
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->oOo0:Lcom/intsig/view/ImageTextButton;

    .line 171
    .line 172
    new-instance v0, LoOo〇08〇/〇O888o0o;

    .line 173
    .line 174
    invoke-direct {v0, p0}, LoOo〇08〇/〇O888o0o;-><init>(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;)V

    .line 175
    .line 176
    .line 177
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    .line 179
    .line 180
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇080OO8〇0:Lcom/intsig/view/ImageTextButton;

    .line 181
    .line 182
    new-instance v0, LoOo〇08〇/〇o00〇〇Oo;

    .line 183
    .line 184
    invoke-direct {v0, p0}, LoOo〇08〇/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;)V

    .line 185
    .line 186
    .line 187
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 188
    .line 189
    .line 190
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;

    .line 191
    .line 192
    invoke-interface {p1}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;->〇0〇O0088o()V

    .line 193
    .line 194
    .line 195
    return-void
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public interceptBackPressed()Z
    .locals 3

    .line 1
    const-string v0, "back"

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇oO〇08o()Lorg/json/JSONObject;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const-string v2, "CSCrop"

    .line 8
    .line 9
    invoke-static {v2, v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 10
    .line 11
    .line 12
    iget v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8oOOo:I

    .line 13
    .line 14
    const/4 v1, 0x2

    .line 15
    if-eq v0, v1, :cond_1

    .line 16
    .line 17
    const/4 v1, 0x3

    .line 18
    if-eq v0, v1, :cond_1

    .line 19
    .line 20
    const/4 v1, 0x4

    .line 21
    if-eq v0, v1, :cond_1

    .line 22
    .line 23
    const/4 v1, 0x5

    .line 24
    if-eq v0, v1, :cond_1

    .line 25
    .line 26
    const/4 v1, 0x7

    .line 27
    if-eq v0, v1, :cond_1

    .line 28
    .line 29
    const/4 v1, 0x6

    .line 30
    if-ne v0, v1, :cond_0

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;

    .line 34
    .line 35
    invoke-interface {v0}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;->onBackPressed()V

    .line 36
    .line 37
    .line 38
    goto :goto_1

    .line 39
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;

    .line 40
    .line 41
    invoke-interface {v0}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;->O8()Z

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    if-eqz v0, :cond_2

    .line 46
    .line 47
    new-instance v0, Lcom/intsig/app/AlertDialog$Builder;

    .line 48
    .line 49
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    invoke-direct {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 54
    .line 55
    .line 56
    const v1, 0x7f13081c

    .line 57
    .line 58
    .line 59
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    new-instance v1, LoOo〇08〇/〇080;

    .line 64
    .line 65
    invoke-direct {v1}, LoOo〇08〇/〇080;-><init>()V

    .line 66
    .line 67
    .line 68
    const v2, 0x7f13057e

    .line 69
    .line 70
    .line 71
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    new-instance v1, LoOo〇08〇/〇O8o08O;

    .line 76
    .line 77
    invoke-direct {v1, p0}, LoOo〇08〇/〇O8o08O;-><init>(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;)V

    .line 78
    .line 79
    .line 80
    const v2, 0x7f1301df

    .line 81
    .line 82
    .line 83
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇oo〇(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 84
    .line 85
    .line 86
    move-result-object v0

    .line 87
    new-instance v1, LoOo〇08〇/Oooo8o0〇;

    .line 88
    .line 89
    invoke-direct {v1, p0}, LoOo〇08〇/Oooo8o0〇;-><init>(Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;)V

    .line 90
    .line 91
    .line 92
    const v2, 0x7f1302e9

    .line 93
    .line 94
    .line 95
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 96
    .line 97
    .line 98
    move-result-object v0

    .line 99
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 100
    .line 101
    .line 102
    move-result-object v0

    .line 103
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 104
    .line 105
    .line 106
    goto :goto_1

    .line 107
    :cond_2
    iget-object v0, p0, Lcom/intsig/mvp/fragment/BaseChangeFragment;->mActivity:Landroidx/appcompat/app/AppCompatActivity;

    .line 108
    .line 109
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 110
    .line 111
    .line 112
    :goto_1
    const/4 v0, 0x1

    .line 113
    return v0
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.method public oO0(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mutilcapture/mode/PagePara;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O88O:Landroid/os/Handler;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/16 v1, 0x66

    .line 8
    .line 9
    iput v1, v0, Landroid/os/Message;->what:I

    .line 10
    .line 11
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 12
    .line 13
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O88O:Landroid/os/Handler;

    .line 14
    .line 15
    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
.end method

.method public oO8008O(Lcom/intsig/camscanner/mutilcapture/mode/PagePara;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O88O:Landroid/os/Handler;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const/16 v1, 0x65

    .line 8
    .line 9
    iput v1, v0, Landroid/os/Message;->what:I

    .line 10
    .line 11
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 12
    .line 13
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O88O:Landroid/os/Handler;

    .line 14
    .line 15
    invoke-virtual {p1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "onActivityCreated="

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O0〇()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .line 1
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 5
    .line 6
    new-instance v1, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v2, "onActivityResult requestCode="

    .line 12
    .line 13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    const-string v2, " resultCode="

    .line 20
    .line 21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    const/16 v0, 0x65

    .line 35
    .line 36
    const/4 v1, -0x1

    .line 37
    if-ne p1, v0, :cond_0

    .line 38
    .line 39
    if-ne p2, v1, :cond_0

    .line 40
    .line 41
    const-string p1, "extra_parcel_doc_info"

    .line 42
    .line 43
    invoke-virtual {p3, p1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    check-cast p1, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 48
    .line 49
    const-string p2, "key_chose_file_path_info"

    .line 50
    .line 51
    invoke-virtual {p3, p2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 52
    .line 53
    .line 54
    move-result-object p2

    .line 55
    check-cast p2, Lcom/intsig/camscanner/datastruct/FolderDocInfo;

    .line 56
    .line 57
    iget-object p3, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;

    .line 58
    .line 59
    invoke-interface {p3, p1, p2}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;->〇O8o08O(Lcom/intsig/camscanner/datastruct/ParcelDocInfo;Lcom/intsig/camscanner/datastruct/FolderDocInfo;)V

    .line 60
    .line 61
    .line 62
    goto :goto_0

    .line 63
    :cond_0
    const/16 v0, 0x67

    .line 64
    .line 65
    if-ne p1, v0, :cond_1

    .line 66
    .line 67
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇〇o〇:Landroid/widget/EditText;

    .line 68
    .line 69
    if-eqz p1, :cond_2

    .line 70
    .line 71
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 72
    .line 73
    .line 74
    move-result-object p1

    .line 75
    iget-object p2, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇〇o〇:Landroid/widget/EditText;

    .line 76
    .line 77
    invoke-static {p1, p2}, Lcom/intsig/utils/SoftKeyboardUtils;->O8(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 78
    .line 79
    .line 80
    goto :goto_0

    .line 81
    :cond_1
    const/16 v0, 0x66

    .line 82
    .line 83
    if-ne p1, v0, :cond_2

    .line 84
    .line 85
    if-ne p2, v1, :cond_2

    .line 86
    .line 87
    if-eqz p3, :cond_2

    .line 88
    .line 89
    invoke-static {}, Lcom/intsig/utils/DocumentUtil;->Oo08()Lcom/intsig/utils/DocumentUtil;

    .line 90
    .line 91
    .line 92
    move-result-object p1

    .line 93
    invoke-virtual {p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->getCurrentActivity()Landroid/app/Activity;

    .line 94
    .line 95
    .line 96
    move-result-object p2

    .line 97
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    .line 98
    .line 99
    .line 100
    move-result-object v0

    .line 101
    invoke-virtual {p1, p2, v0}, Lcom/intsig/utils/DocumentUtil;->〇〇888(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object p1

    .line 105
    const-string p2, "extra_border"

    .line 106
    .line 107
    invoke-virtual {p3, p2}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    .line 108
    .line 109
    .line 110
    move-result-object p2

    .line 111
    invoke-static {p1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 112
    .line 113
    .line 114
    move-result p3

    .line 115
    if-eqz p3, :cond_2

    .line 116
    .line 117
    iget-object p3, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;

    .line 118
    .line 119
    iget v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8oOOo:I

    .line 120
    .line 121
    invoke-interface {p3, p1, v0, p2}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;->Oooo8o0〇(Ljava/lang/String;I[I)V

    .line 122
    .line 123
    .line 124
    :cond_2
    :goto_0
    return-void
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public onDestroyView()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->onDestroyView()V

    .line 2
    .line 3
    .line 4
    iget v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->ooo0〇〇O:I

    .line 5
    .line 6
    invoke-static {v0}, Lcom/intsig/camscanner/scanner/ScannerUtils;->destroyThreadContext(I)V

    .line 7
    .line 8
    .line 9
    sget-object v0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 10
    .line 11
    const-string v1, "onDestroyView"

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onPause()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onPause()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇8〇oO〇〇8o:Ljava/util/HashSet;

    .line 5
    .line 6
    invoke-static {v0}, Lcom/intsig/camscanner/loadimage/BitmapLoaderUtil;->〇o00〇〇Oo(Ljava/util/HashSet;)V

    .line 7
    .line 8
    .line 9
    sget-object v0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 10
    .line 11
    const-string v1, "onPause"

    .line 12
    .line 13
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onStart()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onStart()V

    .line 2
    .line 3
    .line 4
    const-string v0, "CSCrop"

    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇oO〇08o()Lorg/json/JSONObject;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇O00(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/view/View;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "onViewCreated"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-super {p0, p1, p2}, Lcom/intsig/mvp/fragment/BaseChangeFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 9
    .line 10
    .line 11
    iget p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8oOOo:I

    .line 12
    .line 13
    const/4 p2, 0x2

    .line 14
    const-string v0, "cs_batch_process"

    .line 15
    .line 16
    const/4 v1, 0x0

    .line 17
    const/16 v2, 0x8

    .line 18
    .line 19
    if-ne p1, p2, :cond_0

    .line 20
    .line 21
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->OO〇00〇8oO:Landroid/view/View;

    .line 22
    .line 23
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 24
    .line 25
    .line 26
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇0O:Lcom/intsig/view/ImageTextButton;

    .line 27
    .line 28
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 29
    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->oOo〇8o008:Lcom/intsig/view/ImageTextButton;

    .line 32
    .line 33
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 34
    .line 35
    .line 36
    iput-object v1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇O〇〇O8:Ljava/lang/String;

    .line 37
    .line 38
    iput-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇o0O:Ljava/lang/String;

    .line 39
    .line 40
    goto/16 :goto_0

    .line 41
    .line 42
    :cond_0
    const/4 p2, 0x1

    .line 43
    const-string v3, "batch"

    .line 44
    .line 45
    if-ne p1, p2, :cond_1

    .line 46
    .line 47
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->OO〇00〇8oO:Landroid/view/View;

    .line 48
    .line 49
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 50
    .line 51
    .line 52
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->oOo0:Lcom/intsig/view/ImageTextButton;

    .line 53
    .line 54
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 55
    .line 56
    .line 57
    iput-object v3, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇O〇〇O8:Ljava/lang/String;

    .line 58
    .line 59
    iput-object v1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇o0O:Ljava/lang/String;

    .line 60
    .line 61
    goto/16 :goto_0

    .line 62
    .line 63
    :cond_1
    const/4 p2, 0x3

    .line 64
    const/4 v4, 0x0

    .line 65
    if-ne p1, p2, :cond_2

    .line 66
    .line 67
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->OO〇00〇8oO:Landroid/view/View;

    .line 68
    .line 69
    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 70
    .line 71
    .line 72
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇0O:Lcom/intsig/view/ImageTextButton;

    .line 73
    .line 74
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 75
    .line 76
    .line 77
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->oOo〇8o008:Lcom/intsig/view/ImageTextButton;

    .line 78
    .line 79
    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 80
    .line 81
    .line 82
    iput-object v1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇O〇〇O8:Ljava/lang/String;

    .line 83
    .line 84
    iput-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇o0O:Ljava/lang/String;

    .line 85
    .line 86
    goto :goto_0

    .line 87
    :cond_2
    const/4 p2, 0x4

    .line 88
    if-ne p1, p2, :cond_3

    .line 89
    .line 90
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->OO〇00〇8oO:Landroid/view/View;

    .line 91
    .line 92
    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 93
    .line 94
    .line 95
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇0O:Lcom/intsig/view/ImageTextButton;

    .line 96
    .line 97
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 98
    .line 99
    .line 100
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->oOo〇8o008:Lcom/intsig/view/ImageTextButton;

    .line 101
    .line 102
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 103
    .line 104
    .line 105
    iput-object v1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇O〇〇O8:Ljava/lang/String;

    .line 106
    .line 107
    iput-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇o0O:Ljava/lang/String;

    .line 108
    .line 109
    goto :goto_0

    .line 110
    :cond_3
    const/4 p2, 0x5

    .line 111
    if-ne p1, p2, :cond_4

    .line 112
    .line 113
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->OO〇00〇8oO:Landroid/view/View;

    .line 114
    .line 115
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 116
    .line 117
    .line 118
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇0O:Lcom/intsig/view/ImageTextButton;

    .line 119
    .line 120
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 121
    .line 122
    .line 123
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->oOo〇8o008:Lcom/intsig/view/ImageTextButton;

    .line 124
    .line 125
    invoke-virtual {p1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 126
    .line 127
    .line 128
    const-string p1, "id_mode"

    .line 129
    .line 130
    iput-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇O〇〇O8:Ljava/lang/String;

    .line 131
    .line 132
    const-string p1, "cs_id_collage_preview"

    .line 133
    .line 134
    iput-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇o0O:Ljava/lang/String;

    .line 135
    .line 136
    goto :goto_0

    .line 137
    :cond_4
    const/4 p2, 0x6

    .line 138
    if-ne p1, p2, :cond_5

    .line 139
    .line 140
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->OO〇00〇8oO:Landroid/view/View;

    .line 141
    .line 142
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 143
    .line 144
    .line 145
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇0O:Lcom/intsig/view/ImageTextButton;

    .line 146
    .line 147
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 148
    .line 149
    .line 150
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->oOo〇8o008:Lcom/intsig/view/ImageTextButton;

    .line 151
    .line 152
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 153
    .line 154
    .line 155
    iput-object v3, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇O〇〇O8:Ljava/lang/String;

    .line 156
    .line 157
    const-string p1, "CSBatchResult"

    .line 158
    .line 159
    iput-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇o0O:Ljava/lang/String;

    .line 160
    .line 161
    goto :goto_0

    .line 162
    :cond_5
    const/4 p2, 0x7

    .line 163
    if-ne p1, p2, :cond_6

    .line 164
    .line 165
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->OO〇00〇8oO:Landroid/view/View;

    .line 166
    .line 167
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 168
    .line 169
    .line 170
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇0O:Lcom/intsig/view/ImageTextButton;

    .line 171
    .line 172
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 173
    .line 174
    .line 175
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->oOo〇8o008:Lcom/intsig/view/ImageTextButton;

    .line 176
    .line 177
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 178
    .line 179
    .line 180
    iput-object v3, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇O〇〇O8:Ljava/lang/String;

    .line 181
    .line 182
    const-string p1, "CSTransPreview"

    .line 183
    .line 184
    iput-object p1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇o0O:Ljava/lang/String;

    .line 185
    .line 186
    :cond_6
    :goto_0
    return-void
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d030d

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public showProgressDialog()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_2

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-nez v1, :cond_2

    .line 12
    .line 13
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->isDetached()Z

    .line 14
    .line 15
    .line 16
    move-result v1

    .line 17
    if-eqz v1, :cond_0

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O〇o88o08〇:Lcom/intsig/camscanner/Client/ProgressDialogClient;

    .line 21
    .line 22
    if-nez v1, :cond_1

    .line 23
    .line 24
    const v1, 0x7f130e22

    .line 25
    .line 26
    .line 27
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-static {v0, v1}, Lcom/intsig/camscanner/Client/ProgressDialogClient;->〇o00〇〇Oo(Landroid/app/Activity;Ljava/lang/String;)Lcom/intsig/camscanner/Client/ProgressDialogClient;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    iput-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O〇o88o08〇:Lcom/intsig/camscanner/Client/ProgressDialogClient;

    .line 36
    .line 37
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O〇o88o08〇:Lcom/intsig/camscanner/Client/ProgressDialogClient;

    .line 38
    .line 39
    invoke-virtual {v0}, Lcom/intsig/camscanner/Client/ProgressDialogClient;->Oo08()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    .line 41
    .line 42
    goto :goto_0

    .line 43
    :catch_0
    move-exception v0

    .line 44
    sget-object v1, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 45
    .line 46
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 47
    .line 48
    .line 49
    :cond_2
    :goto_0
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public 〇8〇0〇o〇O(J)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;

    .line 2
    .line 3
    invoke-interface {v0, p1, p2}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;->〇〇808〇(J)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public 〇O80〇oOo()Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇OO0()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->ooo0〇〇O:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public 〇o〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O〇o88o08〇:Lcom/intsig/camscanner/Client/ProgressDialogClient;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/Client/ProgressDialogClient;->〇080()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method 〇〇()V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "adjustCurrentPage"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;

    .line 9
    .line 10
    iget v1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇08〇o0O:I

    .line 11
    .line 12
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;->〇o00〇〇Oo(I)Lcom/intsig/camscanner/mutilcapture/mode/PagePara;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    if-nez v0, :cond_0

    .line 17
    .line 18
    sget-object v0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 19
    .line 20
    const-string v1, "adjustCurrentPage pagePara == null || pagePara.defaultBounds == null"

    .line 21
    .line 22
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    return-void

    .line 26
    :cond_0
    iget-object v1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o8〇OO0〇0o:Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;

    .line 27
    .line 28
    iget-wide v2, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o0:J

    .line 29
    .line 30
    invoke-virtual {v1, v2, v3}, Lcom/intsig/camscanner/mutilcapture/adapter/MultiCaptureImagePagerAdapter;->O8(J)I

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0〇〇00(I)Lcom/intsig/camscanner/view/ImageEditView;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    if-nez v1, :cond_1

    .line 39
    .line 40
    sget-object v0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 41
    .line 42
    const-string v1, "adjustCurrentPage mImageView == null"

    .line 43
    .line 44
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    return-void

    .line 48
    :cond_1
    iget-object v2, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇OOo8〇0:[I

    .line 49
    .line 50
    iput-object v2, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->OO:[I

    .line 51
    .line 52
    const/4 v2, 0x1

    .line 53
    iput-boolean v2, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o8〇OO0〇0o:Z

    .line 54
    .line 55
    iget-boolean v3, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇8〇oO〇〇8o:Z

    .line 56
    .line 57
    xor-int/2addr v3, v2

    .line 58
    iput-boolean v3, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇8〇oO〇〇8o:Z

    .line 59
    .line 60
    iput-boolean v2, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->O0O:Z

    .line 61
    .line 62
    if-eqz v3, :cond_2

    .line 63
    .line 64
    sget-object v2, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 65
    .line 66
    const-string v3, "User Operation:  change bound trim"

    .line 67
    .line 68
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇o〇OO80oO(Lcom/intsig/camscanner/mutilcapture/mode/PagePara;Lcom/intsig/camscanner/view/ImageEditView;)V

    .line 72
    .line 73
    .line 74
    goto :goto_0

    .line 75
    :cond_2
    sget-object v2, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 76
    .line 77
    const-string v3, "User Operation:  change bound no trim"

    .line 78
    .line 79
    invoke-static {v2, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    .line 81
    .line 82
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->O〇00O(Lcom/intsig/camscanner/mutilcapture/mode/PagePara;Lcom/intsig/camscanner/view/ImageEditView;)V

    .line 83
    .line 84
    .line 85
    :goto_0
    invoke-direct {p0}, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇oO〇08o()Lorg/json/JSONObject;

    .line 86
    .line 87
    .line 88
    move-result-object v1

    .line 89
    :try_start_0
    const-string v2, "type"

    .line 90
    .line 91
    iget-boolean v3, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇8〇oO〇〇8o:Z

    .line 92
    .line 93
    if-eqz v3, :cond_3

    .line 94
    .line 95
    const-string v3, "auto"

    .line 96
    .line 97
    goto :goto_1

    .line 98
    :cond_3
    const-string v3, "all"

    .line 99
    .line 100
    :goto_1
    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 101
    .line 102
    .line 103
    const-string v2, "CSCrop"

    .line 104
    .line 105
    const-string v3, "auto_select"

    .line 106
    .line 107
    invoke-static {v2, v3, v1}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    .line 109
    .line 110
    goto :goto_2

    .line 111
    :catch_0
    move-exception v1

    .line 112
    sget-object v2, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->〇00O0:Ljava/lang/String;

    .line 113
    .line 114
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 115
    .line 116
    .line 117
    :goto_2
    iget-object v1, p0, Lcom/intsig/camscanner/mutilcapture/MultiCaptureResultFragment;->o0:Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;

    .line 118
    .line 119
    iget-wide v2, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o0:J

    .line 120
    .line 121
    invoke-interface {v1, v2, v3}, Lcom/intsig/camscanner/mutilcapture/presenter/MultiCaptureResultPresenter;->〇〇808〇(J)V

    .line 122
    .line 123
    .line 124
    return-void
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method

.class public Lcom/intsig/camscanner/mutilcapture/PageParaUtil;
.super Ljava/lang/Object;
.source "PageParaUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/mutilcapture/PageParaUtil$ImageHandleTaskCallback;
    }
.end annotation


# direct methods
.method public static O8(JLjava/lang/String;I[I)Lcom/intsig/camscanner/mutilcapture/mode/PagePara;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;-><init>()V

    .line 4
    .line 5
    .line 6
    iput-wide p0, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o0:J

    .line 7
    .line 8
    iput-object p2, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo〇8o008:Ljava/lang/String;

    .line 9
    .line 10
    iput-object p2, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo0:Ljava/lang/String;

    .line 11
    .line 12
    iput p3, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->O8o08O8O:I

    .line 13
    .line 14
    iput p3, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇080OO8〇0:I

    .line 15
    .line 16
    invoke-static {p2}, Lcom/intsig/camscanner/util/Util;->〇08O8o〇0(Ljava/lang/String;)[I

    .line 17
    .line 18
    .line 19
    move-result-object p0

    .line 20
    iput-object p0, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->ooo0〇〇O:[I

    .line 21
    .line 22
    iput-object p4, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇OOo8〇0:[I

    .line 23
    .line 24
    const/4 p1, 0x1

    .line 25
    if-eqz p0, :cond_0

    .line 26
    .line 27
    const/16 p2, 0x8

    .line 28
    .line 29
    new-array p2, p2, [I

    .line 30
    .line 31
    const/4 p3, 0x0

    .line 32
    aput p3, p2, p3

    .line 33
    .line 34
    aput p3, p2, p1

    .line 35
    .line 36
    aget v1, p0, p3

    .line 37
    .line 38
    const/4 v2, 0x2

    .line 39
    aput v1, p2, v2

    .line 40
    .line 41
    const/4 v2, 0x3

    .line 42
    aput p3, p2, v2

    .line 43
    .line 44
    const/4 v2, 0x4

    .line 45
    aput v1, p2, v2

    .line 46
    .line 47
    aget p0, p0, p1

    .line 48
    .line 49
    const/4 v1, 0x5

    .line 50
    aput p0, p2, v1

    .line 51
    .line 52
    const/4 v1, 0x6

    .line 53
    aput p3, p2, v1

    .line 54
    .line 55
    const/4 p3, 0x7

    .line 56
    aput p0, p2, p3

    .line 57
    .line 58
    if-nez p4, :cond_1

    .line 59
    .line 60
    iput-object p2, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇OOo8〇0:[I

    .line 61
    .line 62
    const-string p0, "PageParaUtil"

    .line 63
    .line 64
    const-string p3, "para.currentBounds == null"

    .line 65
    .line 66
    invoke-static {p0, p3}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    goto :goto_0

    .line 70
    :cond_0
    const/4 p2, 0x0

    .line 71
    :cond_1
    :goto_0
    iget-object p0, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇OOo8〇0:[I

    .line 72
    .line 73
    invoke-static {p2, p0}, Ljava/util/Arrays;->equals([I[I)Z

    .line 74
    .line 75
    .line 76
    move-result p0

    .line 77
    xor-int/2addr p0, p1

    .line 78
    iput-boolean p0, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇8〇oO〇〇8o:Z

    .line 79
    .line 80
    iget-object p0, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇OOo8〇0:[I

    .line 81
    .line 82
    iput-object p0, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->OO:[I

    .line 83
    .line 84
    iput-object p0, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇08O〇00〇o:[I

    .line 85
    .line 86
    iput-object p0, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o〇00O:[I

    .line 87
    .line 88
    iput-boolean p1, v0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o8〇OO0〇0o:Z

    .line 89
    .line 90
    return-object v0
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
.end method

.method private static synthetic OO0o〇〇〇〇0(Lcom/intsig/camscanner/mutilcapture/PageParaUtil$ImageHandleTaskCallback;[FI)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    aget p1, p1, v0

    .line 3
    .line 4
    invoke-interface {p0, p1, p2}, Lcom/intsig/camscanner/mutilcapture/PageParaUtil$ImageHandleTaskCallback;->〇o〇(FI)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static Oo08(Landroid/content/Context;JLjava/lang/String;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "J",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mutilcapture/mode/PagePara;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    const/4 v2, 0x0

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    move-object v6, v2

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v3, "_id in "

    .line 21
    .line 22
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    move-object/from16 v3, p3

    .line 26
    .line 27
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    move-object v6, v1

    .line 35
    :goto_0
    const-string v7, "_id"

    .line 36
    .line 37
    const-string v8, "_data"

    .line 38
    .line 39
    const-string v9, "raw_data"

    .line 40
    .line 41
    const-string v10, "image_rotation"

    .line 42
    .line 43
    const-string v11, "ori_rotation"

    .line 44
    .line 45
    const-string v12, "image_border"

    .line 46
    .line 47
    filled-new-array/range {v7 .. v12}, [Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v5

    .line 51
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 52
    .line 53
    .line 54
    move-result-object v3

    .line 55
    invoke-static/range {p1 .. p2}, Lcom/intsig/camscanner/provider/Documents$Image;->〇080(J)Landroid/net/Uri;

    .line 56
    .line 57
    .line 58
    move-result-object v4

    .line 59
    const/4 v7, 0x0

    .line 60
    const-string v8, "page_num ASC"

    .line 61
    .line 62
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 63
    .line 64
    .line 65
    move-result-object v1

    .line 66
    if-eqz v1, :cond_4

    .line 67
    .line 68
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    .line 69
    .line 70
    .line 71
    move-result v3

    .line 72
    if-eqz v3, :cond_3

    .line 73
    .line 74
    new-instance v3, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;

    .line 75
    .line 76
    invoke-direct {v3}, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;-><init>()V

    .line 77
    .line 78
    .line 79
    const/4 v4, 0x0

    .line 80
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    .line 81
    .line 82
    .line 83
    move-result v5

    .line 84
    int-to-long v5, v5

    .line 85
    iput-wide v5, v3, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o0:J

    .line 86
    .line 87
    const/4 v5, 0x1

    .line 88
    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object v6

    .line 92
    iput-object v6, v3, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇o0O:Ljava/lang/String;

    .line 93
    .line 94
    const/4 v6, 0x2

    .line 95
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 96
    .line 97
    .line 98
    move-result-object v7

    .line 99
    iput-object v7, v3, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo〇8o008:Ljava/lang/String;

    .line 100
    .line 101
    iput-object v7, v3, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo0:Ljava/lang/String;

    .line 102
    .line 103
    const/4 v7, 0x3

    .line 104
    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    .line 105
    .line 106
    .line 107
    move-result v8

    .line 108
    add-int/lit16 v8, v8, 0x168

    .line 109
    .line 110
    const/4 v9, 0x4

    .line 111
    invoke-interface {v1, v9}, Landroid/database/Cursor;->getInt(I)I

    .line 112
    .line 113
    .line 114
    move-result v10

    .line 115
    add-int/2addr v8, v10

    .line 116
    iget-object v10, v3, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo〇8o008:Ljava/lang/String;

    .line 117
    .line 118
    invoke-static {v10}, Lcom/intsig/utils/ImageUtil;->〇〇808〇(Ljava/lang/String;)I

    .line 119
    .line 120
    .line 121
    move-result v10

    .line 122
    add-int/2addr v8, v10

    .line 123
    rem-int/lit16 v8, v8, 0x168

    .line 124
    .line 125
    iput v8, v3, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->O8o08O8O:I

    .line 126
    .line 127
    iput v8, v3, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇080OO8〇0:I

    .line 128
    .line 129
    iget-object v8, v3, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo〇8o008:Ljava/lang/String;

    .line 130
    .line 131
    invoke-static {v8}, Lcom/intsig/camscanner/util/Util;->〇08O8o〇0(Ljava/lang/String;)[I

    .line 132
    .line 133
    .line 134
    move-result-object v8

    .line 135
    iput-object v8, v3, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->ooo0〇〇O:[I

    .line 136
    .line 137
    const/4 v10, 0x5

    .line 138
    invoke-interface {v1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 139
    .line 140
    .line 141
    move-result-object v11

    .line 142
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 143
    .line 144
    .line 145
    move-result v12

    .line 146
    if-nez v12, :cond_1

    .line 147
    .line 148
    invoke-static {v11}, Lcom/intsig/camscanner/db/dao/ImageDaoUtil;->〇080(Ljava/lang/String;)[I

    .line 149
    .line 150
    .line 151
    move-result-object v11

    .line 152
    goto :goto_2

    .line 153
    :cond_1
    move-object v11, v2

    .line 154
    :goto_2
    iput-object v11, v3, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇OOo8〇0:[I

    .line 155
    .line 156
    const/16 v12, 0x8

    .line 157
    .line 158
    new-array v12, v12, [I

    .line 159
    .line 160
    aput v4, v12, v4

    .line 161
    .line 162
    aput v4, v12, v5

    .line 163
    .line 164
    aget v13, v8, v4

    .line 165
    .line 166
    aput v13, v12, v6

    .line 167
    .line 168
    aput v4, v12, v7

    .line 169
    .line 170
    aput v13, v12, v9

    .line 171
    .line 172
    aget v6, v8, v5

    .line 173
    .line 174
    aput v6, v12, v10

    .line 175
    .line 176
    const/4 v7, 0x6

    .line 177
    aput v4, v12, v7

    .line 178
    .line 179
    const/4 v4, 0x7

    .line 180
    aput v6, v12, v4

    .line 181
    .line 182
    if-nez v11, :cond_2

    .line 183
    .line 184
    iput-object v12, v3, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇OOo8〇0:[I

    .line 185
    .line 186
    const-string v4, "PageParaUtil"

    .line 187
    .line 188
    const-string v6, "para.currentBounds == null"

    .line 189
    .line 190
    invoke-static {v4, v6}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    .line 192
    .line 193
    :cond_2
    iget-object v4, v3, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇OOo8〇0:[I

    .line 194
    .line 195
    invoke-static {v12, v4}, Ljava/util/Arrays;->equals([I[I)Z

    .line 196
    .line 197
    .line 198
    move-result v4

    .line 199
    xor-int/2addr v4, v5

    .line 200
    iput-boolean v4, v3, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇8〇oO〇〇8o:Z

    .line 201
    .line 202
    iget-object v4, v3, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇OOo8〇0:[I

    .line 203
    .line 204
    iput-object v4, v3, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->OO:[I

    .line 205
    .line 206
    iput-object v4, v3, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇08O〇00〇o:[I

    .line 207
    .line 208
    iput-object v4, v3, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o〇00O:[I

    .line 209
    .line 210
    iput-boolean v5, v3, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o8〇OO0〇0o:Z

    .line 211
    .line 212
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    .line 214
    .line 215
    goto/16 :goto_1

    .line 216
    .line 217
    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 218
    .line 219
    .line 220
    :cond_4
    return-object v0
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public static oO80(Ljava/util/List;Lcom/intsig/camscanner/mutilcapture/PageParaUtil$ImageHandleTaskCallback;)V
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/mutilcapture/mode/PagePara;",
            ">;",
            "Lcom/intsig/camscanner/mutilcapture/PageParaUtil$ImageHandleTaskCallback;",
            ")V"
        }
    .end annotation

    .line 1
    move-object/from16 v0, p1

    .line 2
    .line 3
    new-instance v1, Landroid/os/Handler;

    .line 4
    .line 5
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 10
    .line 11
    .line 12
    new-instance v2, Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 13
    .line 14
    invoke-direct {v2}, Lcom/intsig/camscanner/util/ImageProgressClient;-><init>()V

    .line 15
    .line 16
    .line 17
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerUtils;->initThreadContext()I

    .line 18
    .line 19
    .line 20
    move-result v3

    .line 21
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    .line 22
    .line 23
    .line 24
    move-result v4

    .line 25
    if-eqz v0, :cond_0

    .line 26
    .line 27
    new-instance v5, LoOo〇08〇/oo88o8O;

    .line 28
    .line 29
    invoke-direct {v5, v0, v4}, LoOo〇08〇/oo88o8O;-><init>(Lcom/intsig/camscanner/mutilcapture/PageParaUtil$ImageHandleTaskCallback;I)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v1, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 33
    .line 34
    .line 35
    :cond_0
    const/4 v5, 0x1

    .line 36
    new-array v6, v5, [F

    .line 37
    .line 38
    const/4 v7, 0x0

    .line 39
    const/4 v8, 0x0

    .line 40
    aput v7, v6, v8

    .line 41
    .line 42
    if-eqz v3, :cond_7

    .line 43
    .line 44
    invoke-static {}, Lcom/intsig/camscanner/booksplitter/Util/BooksplitterUtils;->OO0o〇〇()I

    .line 45
    .line 46
    .line 47
    move-result v7

    .line 48
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 49
    .line 50
    .line 51
    move-result-object v9

    .line 52
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    .line 53
    .line 54
    .line 55
    move-result v10

    .line 56
    if-eqz v10, :cond_6

    .line 57
    .line 58
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 59
    .line 60
    .line 61
    move-result-object v10

    .line 62
    check-cast v10, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;

    .line 63
    .line 64
    iget-object v11, v10, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo0:Ljava/lang/String;

    .line 65
    .line 66
    invoke-static {v11}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 67
    .line 68
    .line 69
    move-result v11

    .line 70
    if-eqz v11, :cond_1

    .line 71
    .line 72
    iget-object v11, v10, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo〇8o008:Ljava/lang/String;

    .line 73
    .line 74
    invoke-static {v11}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 75
    .line 76
    .line 77
    move-result v11

    .line 78
    if-eqz v11, :cond_1

    .line 79
    .line 80
    iget-object v11, v10, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo0:Ljava/lang/String;

    .line 81
    .line 82
    iget-object v12, v10, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo〇8o008:Ljava/lang/String;

    .line 83
    .line 84
    invoke-static {v11, v12}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 85
    .line 86
    .line 87
    move-result v11

    .line 88
    if-nez v11, :cond_1

    .line 89
    .line 90
    iget-object v11, v10, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo0:Ljava/lang/String;

    .line 91
    .line 92
    invoke-static {v11}, Lcom/intsig/utils/FileUtil;->〇O8o08O(Ljava/lang/String;)Z

    .line 93
    .line 94
    .line 95
    iget-object v11, v10, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo〇8o008:Ljava/lang/String;

    .line 96
    .line 97
    iget-object v12, v10, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo0:Ljava/lang/String;

    .line 98
    .line 99
    invoke-static {v11, v12}, Lcom/intsig/utils/FileUtil;->o〇0OOo〇0(Ljava/lang/String;Ljava/lang/String;)Z

    .line 100
    .line 101
    .line 102
    iget-object v11, v10, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo0:Ljava/lang/String;

    .line 103
    .line 104
    iput-object v11, v10, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo〇8o008:Ljava/lang/String;

    .line 105
    .line 106
    :cond_1
    iget-object v11, v10, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo〇8o008:Ljava/lang/String;

    .line 107
    .line 108
    invoke-static {v11}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 109
    .line 110
    .line 111
    move-result v11

    .line 112
    const-string v12, "PageParaUtil"

    .line 113
    .line 114
    if-nez v11, :cond_2

    .line 115
    .line 116
    new-instance v11, Ljava/lang/StringBuilder;

    .line 117
    .line 118
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 119
    .line 120
    .line 121
    const-string v13, "pagePara.rawPath="

    .line 122
    .line 123
    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 124
    .line 125
    .line 126
    iget-object v10, v10, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo〇8o008:Ljava/lang/String;

    .line 127
    .line 128
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    .line 130
    .line 131
    const-string v10, " is not exist"

    .line 132
    .line 133
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    .line 135
    .line 136
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 137
    .line 138
    .line 139
    move-result-object v10

    .line 140
    invoke-static {v12, v10}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    .line 142
    .line 143
    goto :goto_0

    .line 144
    :cond_2
    aget v11, v6, v8

    .line 145
    .line 146
    const v13, 0x3f4ccccd    # 0.8f

    .line 147
    .line 148
    .line 149
    add-float/2addr v11, v13

    .line 150
    aput v11, v6, v8

    .line 151
    .line 152
    if-eqz v0, :cond_3

    .line 153
    .line 154
    new-instance v11, LoOo〇08〇/〇oo〇;

    .line 155
    .line 156
    invoke-direct {v11, v0, v6, v4}, LoOo〇08〇/〇oo〇;-><init>(Lcom/intsig/camscanner/mutilcapture/PageParaUtil$ImageHandleTaskCallback;[FI)V

    .line 157
    .line 158
    .line 159
    invoke-virtual {v1, v11}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 160
    .line 161
    .line 162
    :cond_3
    new-instance v11, Ljava/lang/StringBuilder;

    .line 163
    .line 164
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 165
    .line 166
    .line 167
    const-string v13, "PAGE_PARA_OCR_"

    .line 168
    .line 169
    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    .line 171
    .line 172
    invoke-static {}, Lcom/intsig/tianshu/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 173
    .line 174
    .line 175
    move-result-object v13

    .line 176
    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    .line 178
    .line 179
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 180
    .line 181
    .line 182
    move-result-object v11

    .line 183
    new-instance v13, Ljava/lang/StringBuilder;

    .line 184
    .line 185
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 186
    .line 187
    .line 188
    const-string v14, "handleImage: uuid="

    .line 189
    .line 190
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    .line 192
    .line 193
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    .line 195
    .line 196
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 197
    .line 198
    .line 199
    move-result-object v13

    .line 200
    invoke-static {v12, v13}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    .line 202
    .line 203
    new-instance v13, Ljava/io/File;

    .line 204
    .line 205
    iget-object v14, v10, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo〇8o008:Ljava/lang/String;

    .line 206
    .line 207
    invoke-direct {v13, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 208
    .line 209
    .line 210
    new-instance v14, Ljava/io/File;

    .line 211
    .line 212
    invoke-virtual {v13}, Ljava/io/File;->getParent()Ljava/lang/String;

    .line 213
    .line 214
    .line 215
    move-result-object v15

    .line 216
    new-instance v8, Ljava/lang/StringBuilder;

    .line 217
    .line 218
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 219
    .line 220
    .line 221
    const-string v5, "save_"

    .line 222
    .line 223
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    .line 225
    .line 226
    invoke-virtual {v13}, Ljava/io/File;->getName()Ljava/lang/String;

    .line 227
    .line 228
    .line 229
    move-result-object v5

    .line 230
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 231
    .line 232
    .line 233
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 234
    .line 235
    .line 236
    move-result-object v5

    .line 237
    invoke-direct {v14, v15, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    .line 239
    .line 240
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/util/ImageProgressClient;->setThreadContext(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 241
    .line 242
    .line 243
    move-result-object v5

    .line 244
    iget-object v8, v10, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo〇8o008:Ljava/lang/String;

    .line 245
    .line 246
    invoke-virtual {v5, v8}, Lcom/intsig/camscanner/util/ImageProgressClient;->setSrcImagePath(Ljava/lang/String;)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 247
    .line 248
    .line 249
    move-result-object v5

    .line 250
    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 251
    .line 252
    .line 253
    move-result-object v8

    .line 254
    invoke-virtual {v5, v8}, Lcom/intsig/camscanner/util/ImageProgressClient;->setSaveImagePath(Ljava/lang/String;)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 255
    .line 256
    .line 257
    move-result-object v5

    .line 258
    iget-object v8, v10, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo〇8o008:Ljava/lang/String;

    .line 259
    .line 260
    invoke-static {v8}, Lcom/intsig/camscanner/util/Util;->〇08O8o〇0(Ljava/lang/String;)[I

    .line 261
    .line 262
    .line 263
    move-result-object v8

    .line 264
    invoke-virtual {v5, v8}, Lcom/intsig/camscanner/util/ImageProgressClient;->setRawImageSize([I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 265
    .line 266
    .line 267
    move-result-object v5

    .line 268
    iget-object v8, v10, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇OOo8〇0:[I

    .line 269
    .line 270
    invoke-virtual {v5, v8}, Lcom/intsig/camscanner/util/ImageProgressClient;->setImageBorder([I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 271
    .line 272
    .line 273
    move-result-object v5

    .line 274
    iget v8, v10, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->O8o08O8O:I

    .line 275
    .line 276
    invoke-virtual {v5, v8}, Lcom/intsig/camscanner/util/ImageProgressClient;->setRation(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 277
    .line 278
    .line 279
    move-result-object v5

    .line 280
    iget v8, v10, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇O〇〇O8:I

    .line 281
    .line 282
    invoke-virtual {v5, v8}, Lcom/intsig/camscanner/util/ImageProgressClient;->setImageEnhanceMode(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 283
    .line 284
    .line 285
    move-result-object v5

    .line 286
    const/4 v8, 0x1

    .line 287
    invoke-virtual {v5, v8}, Lcom/intsig/camscanner/util/ImageProgressClient;->enableTrim(Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 288
    .line 289
    .line 290
    move-result-object v5

    .line 291
    sget-object v13, Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;->INSTANCE:Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;

    .line 292
    .line 293
    invoke-virtual {v13}, Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;->isCropDewrapOn()Z

    .line 294
    .line 295
    .line 296
    move-result v13

    .line 297
    invoke-virtual {v5, v13}, Lcom/intsig/camscanner/util/ImageProgressClient;->setNeedCropDewrap(Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 298
    .line 299
    .line 300
    move-result-object v5

    .line 301
    invoke-static {}, Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;->getNeedTrimWhenNoBorder()Z

    .line 302
    .line 303
    .line 304
    move-result v13

    .line 305
    invoke-virtual {v5, v13}, Lcom/intsig/camscanner/util/ImageProgressClient;->setNeedCropWhenNoBorder(Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 306
    .line 307
    .line 308
    move-result-object v5

    .line 309
    invoke-virtual {v5, v11}, Lcom/intsig/camscanner/util/ImageProgressClient;->executeProgress(Ljava/lang/String;)V

    .line 310
    .line 311
    .line 312
    if-eqz v0, :cond_4

    .line 313
    .line 314
    invoke-virtual {v14}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    .line 315
    .line 316
    .line 317
    move-result-object v5

    .line 318
    invoke-interface {v0, v10, v5}, Lcom/intsig/camscanner/mutilcapture/PageParaUtil$ImageHandleTaskCallback;->〇o00〇〇Oo(Lcom/intsig/camscanner/mutilcapture/mode/PagePara;Ljava/lang/String;)V

    .line 319
    .line 320
    .line 321
    :cond_4
    const/4 v5, 0x0

    .line 322
    aget v11, v6, v5

    .line 323
    .line 324
    const v13, 0x3e4ccccd    # 0.2f

    .line 325
    .line 326
    .line 327
    add-float/2addr v11, v13

    .line 328
    aput v11, v6, v5

    .line 329
    .line 330
    if-eqz v0, :cond_5

    .line 331
    .line 332
    new-instance v11, LoOo〇08〇/o〇O8〇〇o;

    .line 333
    .line 334
    invoke-direct {v11, v0, v6, v4}, LoOo〇08〇/o〇O8〇〇o;-><init>(Lcom/intsig/camscanner/mutilcapture/PageParaUtil$ImageHandleTaskCallback;[FI)V

    .line 335
    .line 336
    .line 337
    invoke-virtual {v1, v11}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 338
    .line 339
    .line 340
    :cond_5
    new-instance v11, Ljava/lang/StringBuilder;

    .line 341
    .line 342
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 343
    .line 344
    .line 345
    const-string v13, "imageChange.imageId="

    .line 346
    .line 347
    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 348
    .line 349
    .line 350
    iget-wide v13, v10, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o0:J

    .line 351
    .line 352
    invoke-virtual {v11, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 353
    .line 354
    .line 355
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 356
    .line 357
    .line 358
    move-result-object v10

    .line 359
    invoke-static {v12, v10}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    .line 361
    .line 362
    const/4 v5, 0x1

    .line 363
    const/4 v8, 0x0

    .line 364
    goto/16 :goto_0

    .line 365
    .line 366
    :cond_6
    invoke-static {v7}, Lcom/intsig/camscanner/booksplitter/Util/BooksplitterUtils;->〇〇808〇(I)V

    .line 367
    .line 368
    .line 369
    invoke-static {v3}, Lcom/intsig/camscanner/scanner/ScannerUtils;->destroyThreadContext(I)V

    .line 370
    .line 371
    .line 372
    :cond_7
    if-eqz v0, :cond_8

    .line 373
    .line 374
    invoke-interface/range {p1 .. p1}, Lcom/intsig/camscanner/mutilcapture/PageParaUtil$ImageHandleTaskCallback;->〇080()V

    .line 375
    .line 376
    .line 377
    new-instance v2, LoOo〇08〇/〇00;

    .line 378
    .line 379
    invoke-direct {v2, v0}, LoOo〇08〇/〇00;-><init>(Lcom/intsig/camscanner/mutilcapture/PageParaUtil$ImageHandleTaskCallback;)V

    .line 380
    .line 381
    .line 382
    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 383
    .line 384
    .line 385
    :cond_8
    return-void
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

.method public static o〇0(Landroid/content/Context;JJ)V
    .locals 16

    .line 1
    move-object/from16 v7, p0

    .line 2
    .line 3
    move-wide/from16 v8, p1

    .line 4
    .line 5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 6
    .line 7
    .line 8
    move-result-wide v10

    .line 9
    invoke-static/range {p3 .. p4}, Lcom/intsig/camscanner/loadimage/BitmapLoaderUtil;->〇〇888(J)V

    .line 10
    .line 11
    .line 12
    const/4 v4, 0x2

    .line 13
    const/4 v5, 0x1

    .line 14
    const/4 v6, 0x0

    .line 15
    move-object/from16 v1, p0

    .line 16
    .line 17
    move-wide/from16 v2, p3

    .line 18
    .line 19
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->oOo0(Landroid/content/Context;JIZZ)V

    .line 20
    .line 21
    .line 22
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇08O〇00〇o(Landroid/content/Context;JIZZ)V

    .line 23
    .line 24
    .line 25
    new-instance v0, Ljava/util/ArrayList;

    .line 26
    .line 27
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 28
    .line 29
    .line 30
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-static/range {p1 .. p2}, Lcom/intsig/camscanner/provider/Documents$Image;->〇080(J)Landroid/net/Uri;

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    const-string v3, "_id"

    .line 39
    .line 40
    const-string v12, "page_num"

    .line 41
    .line 42
    filled-new-array {v3, v12}, [Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v3

    .line 46
    const-string v4, "page_num > 0"

    .line 47
    .line 48
    const/4 v5, 0x0

    .line 49
    const-string v6, "page_num ASC"

    .line 50
    .line 51
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    const/4 v2, 0x1

    .line 56
    const/4 v3, 0x0

    .line 57
    if-eqz v1, :cond_2

    .line 58
    .line 59
    const/4 v4, 0x0

    .line 60
    :cond_0
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    .line 61
    .line 62
    .line 63
    move-result v5

    .line 64
    if-eqz v5, :cond_1

    .line 65
    .line 66
    add-int/lit8 v4, v4, 0x1

    .line 67
    .line 68
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    .line 69
    .line 70
    .line 71
    move-result v5

    .line 72
    if-eq v4, v5, :cond_0

    .line 73
    .line 74
    invoke-interface {v1, v3}, Landroid/database/Cursor;->getInt(I)I

    .line 75
    .line 76
    .line 77
    move-result v5

    .line 78
    new-instance v6, Landroid/content/ContentValues;

    .line 79
    .line 80
    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 81
    .line 82
    .line 83
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 84
    .line 85
    .line 86
    move-result-object v13

    .line 87
    invoke-virtual {v6, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 88
    .line 89
    .line 90
    sget-object v13, Lcom/intsig/camscanner/provider/Documents$Image;->〇080:Landroid/net/Uri;

    .line 91
    .line 92
    int-to-long v14, v5

    .line 93
    invoke-static {v13, v14, v15}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 94
    .line 95
    .line 96
    move-result-object v5

    .line 97
    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    .line 98
    .line 99
    .line 100
    move-result-object v5

    .line 101
    invoke-virtual {v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    .line 102
    .line 103
    .line 104
    move-result-object v5

    .line 105
    invoke-virtual {v5}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    .line 106
    .line 107
    .line 108
    move-result-object v5

    .line 109
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    .line 111
    .line 112
    goto :goto_0

    .line 113
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 114
    .line 115
    .line 116
    move v3, v4

    .line 117
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 118
    .line 119
    .line 120
    move-result v1

    .line 121
    const-string v4, "PageParaUtil"

    .line 122
    .line 123
    if-lez v1, :cond_3

    .line 124
    .line 125
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 126
    .line 127
    .line 128
    move-result-object v1

    .line 129
    sget-object v5, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 130
    .line 131
    invoke-virtual {v1, v5, v0}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/database/CursorIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    .line 133
    .line 134
    goto :goto_2

    .line 135
    :catch_0
    move-exception v0

    .line 136
    goto :goto_1

    .line 137
    :catch_1
    move-exception v0

    .line 138
    goto :goto_1

    .line 139
    :catch_2
    move-exception v0

    .line 140
    goto :goto_1

    .line 141
    :catch_3
    move-exception v0

    .line 142
    :goto_1
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 143
    .line 144
    .line 145
    :cond_3
    :goto_2
    new-instance v0, Landroid/content/ContentValues;

    .line 146
    .line 147
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 148
    .line 149
    .line 150
    const-string v1, "pages"

    .line 151
    .line 152
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 153
    .line 154
    .line 155
    move-result-object v5

    .line 156
    invoke-virtual {v0, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 157
    .line 158
    .line 159
    sget-object v1, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 160
    .line 161
    invoke-static {v1, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 162
    .line 163
    .line 164
    move-result-object v1

    .line 165
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 166
    .line 167
    .line 168
    move-result-object v5

    .line 169
    const/4 v6, 0x0

    .line 170
    invoke-virtual {v5, v1, v0, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 171
    .line 172
    .line 173
    if-nez v3, :cond_4

    .line 174
    .line 175
    const/4 v0, 0x2

    .line 176
    invoke-static {v7, v8, v9, v0, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0oO0(Landroid/content/Context;JIZ)V

    .line 177
    .line 178
    .line 179
    new-instance v0, Ljava/util/ArrayList;

    .line 180
    .line 181
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 182
    .line 183
    .line 184
    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 185
    .line 186
    .line 187
    move-result-object v2

    .line 188
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    .line 190
    .line 191
    invoke-static {v7, v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->oO0(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 192
    .line 193
    .line 194
    goto :goto_3

    .line 195
    :cond_4
    const/4 v0, 0x3

    .line 196
    invoke-static {v7, v8, v9, v0, v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0oO0(Landroid/content/Context;JIZ)V

    .line 197
    .line 198
    .line 199
    :goto_3
    if-lez v3, :cond_5

    .line 200
    .line 201
    invoke-static {v1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 202
    .line 203
    .line 204
    move-result-wide v0

    .line 205
    invoke-static {v7, v0, v1}, Lcom/intsig/camscanner/tsapp/AutoUploadThread;->〇〇8O0〇8(Landroid/content/Context;J)I

    .line 206
    .line 207
    .line 208
    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    .line 209
    .line 210
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 211
    .line 212
    .line 213
    const-string v1, "after delete, docPageNum="

    .line 214
    .line 215
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    .line 217
    .line 218
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 219
    .line 220
    .line 221
    const-string v1, " remove cost = "

    .line 222
    .line 223
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    .line 225
    .line 226
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 227
    .line 228
    .line 229
    move-result-wide v1

    .line 230
    sub-long/2addr v1, v10

    .line 231
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 232
    .line 233
    .line 234
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 235
    .line 236
    .line 237
    move-result-object v0

    .line 238
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    .line 240
    .line 241
    return-void
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/mutilcapture/PageParaUtil$ImageHandleTaskCallback;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/mutilcapture/PageParaUtil;->〇80〇808〇O(Lcom/intsig/camscanner/mutilcapture/PageParaUtil$ImageHandleTaskCallback;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static synthetic 〇80〇808〇O(Lcom/intsig/camscanner/mutilcapture/PageParaUtil$ImageHandleTaskCallback;I)V
    .locals 0

    .line 1
    invoke-interface {p0, p1}, Lcom/intsig/camscanner/mutilcapture/PageParaUtil$ImageHandleTaskCallback;->start(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method private static synthetic 〇8o8o〇(Lcom/intsig/camscanner/mutilcapture/PageParaUtil$ImageHandleTaskCallback;[FI)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    aget p1, p1, v0

    .line 3
    .line 4
    invoke-interface {p0, p1, p2}, Lcom/intsig/camscanner/mutilcapture/PageParaUtil$ImageHandleTaskCallback;->O8(FI)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static 〇O8o08O(Lcom/intsig/camscanner/mutilcapture/mode/PagePara;Ljava/lang/String;)V
    .locals 5

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo〇8o008:Ljava/lang/String;

    .line 2
    .line 3
    invoke-static {p1}, Lcom/intsig/utils/ImageUtil;->〇〇808〇(Ljava/lang/String;)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    iput p1, p0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->O8o08O8O:I

    .line 8
    .line 9
    iput p1, p0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇080OO8〇0:I

    .line 10
    .line 11
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->oOo〇8o008:Ljava/lang/String;

    .line 12
    .line 13
    invoke-static {p1}, Lcom/intsig/camscanner/util/Util;->〇08O8o〇0(Ljava/lang/String;)[I

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    const/4 v0, 0x0

    .line 18
    iput-boolean v0, p0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇8〇oO〇〇8o:Z

    .line 19
    .line 20
    const/4 v1, 0x1

    .line 21
    if-nez p1, :cond_0

    .line 22
    .line 23
    const-string p1, "PageParaUtil"

    .line 24
    .line 25
    const-string v0, "size == null"

    .line 26
    .line 27
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    iput-object p1, p0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->ooo0〇〇O:[I

    .line 32
    .line 33
    const/16 v2, 0x8

    .line 34
    .line 35
    new-array v2, v2, [I

    .line 36
    .line 37
    aput v0, v2, v0

    .line 38
    .line 39
    aput v0, v2, v1

    .line 40
    .line 41
    aget v3, p1, v0

    .line 42
    .line 43
    const/4 v4, 0x2

    .line 44
    aput v3, v2, v4

    .line 45
    .line 46
    const/4 v4, 0x3

    .line 47
    aput v0, v2, v4

    .line 48
    .line 49
    const/4 v4, 0x4

    .line 50
    aput v3, v2, v4

    .line 51
    .line 52
    aget p1, p1, v1

    .line 53
    .line 54
    const/4 v3, 0x5

    .line 55
    aput p1, v2, v3

    .line 56
    .line 57
    const/4 v3, 0x6

    .line 58
    aput v0, v2, v3

    .line 59
    .line 60
    const/4 v0, 0x7

    .line 61
    aput p1, v2, v0

    .line 62
    .line 63
    iput-object v2, p0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇OOo8〇0:[I

    .line 64
    .line 65
    :goto_0
    iget-object p1, p0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇OOo8〇0:[I

    .line 66
    .line 67
    iput-object p1, p0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->OO:[I

    .line 68
    .line 69
    iput-object p1, p0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->〇08O〇00〇o:[I

    .line 70
    .line 71
    iput-object p1, p0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o〇00O:[I

    .line 72
    .line 73
    iput-boolean v1, p0, Lcom/intsig/camscanner/mutilcapture/mode/PagePara;->o8〇OO0〇0o:Z

    .line 74
    .line 75
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/mutilcapture/PageParaUtil$ImageHandleTaskCallback;[FI)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mutilcapture/PageParaUtil;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/mutilcapture/PageParaUtil$ImageHandleTaskCallback;[FI)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static synthetic 〇o〇(Lcom/intsig/camscanner/mutilcapture/PageParaUtil$ImageHandleTaskCallback;[FI)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/mutilcapture/PageParaUtil;->〇8o8o〇(Lcom/intsig/camscanner/mutilcapture/PageParaUtil$ImageHandleTaskCallback;[FI)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
.end method

.method public static 〇〇888(JZ)Ljava/lang/String;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->oo〇()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string p0, "_only_trim_img.jpg"

    .line 17
    .line 18
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object p0

    .line 25
    invoke-static {p0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    if-eqz p1, :cond_0

    .line 30
    .line 31
    if-eqz p2, :cond_0

    .line 32
    .line 33
    invoke-static {p0}, Lcom/intsig/utils/FileUtil;->〇O8o08O(Ljava/lang/String;)Z

    .line 34
    .line 35
    .line 36
    :cond_0
    return-object p0
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.class public final Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;
.super Lcom/intsig/camscanner/util/AbstractPreferenceHelper;
.source "ScannerPreferenceHelper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final INSTANCE:Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final KEY_BOOLEAN_CLICK_PAGE_LIST_EDIT:Ljava/lang/String; = "key_boolean_click_page_list_edit"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final KEY_BOOLEAN_USING_NEW_MAGIC:Ljava/lang/String; = "key_boolean_using_new_magic"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final KEY_INT_CURRENT_UPLOAD_TIMES:Ljava/lang/String; = "key_int_current_upload_times"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final KEY_INT_SUPER_FILTER_DEFAULT:Ljava/lang/String; = "key_int_current_upload_times"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final KEY_LONG_CURRENT_RECORD_SUPER_FILTER_UPLOAD_DAY:Ljava/lang/String; = "key_long_current_record_super_filter_upload_day"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final KEY_LONG_PAGE_LIST_EDIT_SHOW_TIME:Ljava/lang/String; = "key_long_page_list_edit_show_time"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final KEY_SCANNER_PREFIX:Ljava/lang/String; = "scanner_helper_"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "ScannerPreferenceHelper"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final isUsingNewMagicDeShadow$delegate:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static superFilterUploadTimeThreshold:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;

    .line 7
    .line 8
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper$isUsingNewMagicDeShadow$2;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper$isUsingNewMagicDeShadow$2;

    .line 9
    .line 10
    invoke-static {v0}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    sput-object v0, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->isUsingNewMagicDeShadow$delegate:Lkotlin/Lazy;

    .line 15
    .line 16
    const/16 v0, 0x64

    .line 17
    .line 18
    sput v0, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->superFilterUploadTimeThreshold:I

    .line 19
    .line 20
    return-void
    .line 21
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/util/AbstractPreferenceHelper;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final getCurrentDayUploadTime()I
    .locals 5

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-static {v0, v1}, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->getDayWithTimeMillis(J)J

    .line 6
    .line 7
    .line 8
    move-result-wide v0

    .line 9
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->getCurrentUploadSuperFilterDay()J

    .line 10
    .line 11
    .line 12
    move-result-wide v2

    .line 13
    cmp-long v4, v0, v2

    .line 14
    .line 15
    if-gtz v4, :cond_0

    .line 16
    .line 17
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->getCurrentUploadSuperFilterTimes()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    return v0

    .line 22
    :cond_0
    invoke-static {v0, v1}, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->setCurrentUploadSuperFilterDay(J)V

    .line 23
    .line 24
    .line 25
    const/4 v0, 0x0

    .line 26
    invoke-static {v0}, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->setCurrentUploadSuperFilterTimes(I)V

    .line 27
    .line 28
    .line 29
    return v0
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public static final getCurrentUploadSuperFilterDay()J
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;

    .line 2
    .line 3
    const-string v1, "key_long_current_record_super_filter_upload_day"

    .line 4
    .line 5
    const-wide/16 v2, 0x0

    .line 6
    .line 7
    invoke-virtual {v0, v1, v2, v3}, Lcom/intsig/camscanner/util/AbstractPreferenceHelper;->getLong(Ljava/lang/String;J)J

    .line 8
    .line 9
    .line 10
    move-result-wide v0

    .line 11
    return-wide v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final getCurrentUploadSuperFilterTimes()I
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;

    .line 2
    .line 3
    const-string v1, "key_int_current_upload_times"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/util/AbstractPreferenceHelper;->getInt(Ljava/lang/String;I)I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final getDayWithTimeMillis(J)J
    .locals 2

    .line 1
    const-wide/16 v0, 0x3e8

    .line 2
    .line 3
    div-long/2addr p0, v0

    .line 4
    const-wide/16 v0, 0xe10

    .line 5
    .line 6
    div-long/2addr p0, v0

    .line 7
    const-wide/16 v0, 0x18

    .line 8
    .line 9
    div-long/2addr p0, v0

    .line 10
    return-wide p0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final getPageListEditShowShowTime()J
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;

    .line 2
    .line 3
    const-string v1, "key_long_page_list_edit_show_time"

    .line 4
    .line 5
    const-wide/16 v2, 0x0

    .line 6
    .line 7
    invoke-virtual {v0, v1, v2, v3}, Lcom/intsig/camscanner/util/AbstractPreferenceHelper;->getLong(Ljava/lang/String;J)J

    .line 8
    .line 9
    .line 10
    move-result-wide v0

    .line 11
    return-wide v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final getSuperFilterAsDefaultLocal()I
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;

    .line 2
    .line 3
    const-string v1, "key_int_current_upload_times"

    .line 4
    .line 5
    const/4 v2, -0x1

    .line 6
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/util/AbstractPreferenceHelper;->getInt(Ljava/lang/String;I)I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final getSuperFilterUploadTimeThreshold()I
    .locals 1

    .line 1
    sget v0, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->superFilterUploadTimeThreshold:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic getSuperFilterUploadTimeThreshold$annotations()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final isPageListEditClicked()Z
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;

    .line 2
    .line 3
    const-string v1, "key_boolean_click_page_list_edit"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/util/AbstractPreferenceHelper;->getBoolean(Ljava/lang/String;Z)Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final isPageListEditShowShowTimeOverOneDay()Z
    .locals 4

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->getPageListEditShowShowTime()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 6
    .line 7
    .line 8
    move-result-wide v2

    .line 9
    invoke-static {v0, v1, v2, v3}, Lcom/intsig/utils/DateTimeUtil;->〇〇808〇(JJ)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final isSpUsingNewMagic()Z
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;

    .line 2
    .line 3
    const-string v1, "key_boolean_using_new_magic"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/util/AbstractPreferenceHelper;->getBoolean(Ljava/lang/String;Z)Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    return v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final isUsingNewMagicDeShadow()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->isUsingNewMagicDeShadow$delegate:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Boolean;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic isUsingNewMagicDeShadow$annotations()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final setCurrentUploadSuperFilterDay(J)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;

    .line 2
    .line 3
    const-string v1, "key_long_current_record_super_filter_upload_day"

    .line 4
    .line 5
    invoke-virtual {v0, v1, p0, p1}, Lcom/intsig/camscanner/util/AbstractPreferenceHelper;->putLong(Ljava/lang/String;J)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final setCurrentUploadSuperFilterTimes(I)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;

    .line 2
    .line 3
    const-string v1, "key_int_current_upload_times"

    .line 4
    .line 5
    invoke-virtual {v0, v1, p0}, Lcom/intsig/camscanner/util/AbstractPreferenceHelper;->putInt(Ljava/lang/String;I)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final setPageListEditClicked(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;

    .line 2
    .line 3
    const-string v1, "key_boolean_click_page_list_edit"

    .line 4
    .line 5
    invoke-virtual {v0, v1, p0}, Lcom/intsig/camscanner/util/AbstractPreferenceHelper;->putBoolean(Ljava/lang/String;Z)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final setPageListEditShowTime(J)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;

    .line 2
    .line 3
    const-string v1, "key_long_page_list_edit_show_time"

    .line 4
    .line 5
    invoke-virtual {v0, v1, p0, p1}, Lcom/intsig/camscanner/util/AbstractPreferenceHelper;->putLong(Ljava/lang/String;J)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final setSuperFilterAsDefaultLocal(I)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;

    .line 2
    .line 3
    const-string v1, "key_int_current_upload_times"

    .line 4
    .line 5
    invoke-virtual {v0, v1, p0}, Lcom/intsig/camscanner/util/AbstractPreferenceHelper;->putInt(Ljava/lang/String;I)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final setSuperFilterUploadTimeThreshold(I)V
    .locals 0

    .line 1
    sput p0, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->superFilterUploadTimeThreshold:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final setUsingNewMagicDeShadow(Z)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;

    .line 2
    .line 3
    const-string v1, "key_boolean_using_new_magic"

    .line 4
    .line 5
    invoke-virtual {v0, v1, p0}, Lcom/intsig/camscanner/util/AbstractPreferenceHelper;->putBoolean(Ljava/lang/String;Z)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final showOriginImgCompareFun()Z
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇8o8o〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    return v1

    .line 9
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    iget v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson;->alg_orgin_image_compare:I

    .line 14
    .line 15
    if-ne v0, v1, :cond_1

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_1
    const/4 v1, 0x0

    .line 19
    :goto_0
    return v1
    .line 20
    .line 21
.end method

.method public static final useSuperFilterAsDefault()Z
    .locals 4

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->getSuperFilterAsDefaultLocal()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, -0x1

    .line 6
    const/4 v2, 0x0

    .line 7
    const/4 v3, 0x1

    .line 8
    if-ne v0, v1, :cond_0

    .line 9
    .line 10
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    iget v0, v0, Lcom/intsig/tsapp/sync/AppConfigJson;->ai_filter_default:I

    .line 15
    .line 16
    if-ne v0, v3, :cond_1

    .line 17
    .line 18
    goto :goto_0

    .line 19
    :cond_0
    if-ne v0, v3, :cond_1

    .line 20
    .line 21
    :goto_0
    const/4 v2, 0x1

    .line 22
    :cond_1
    return v2
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public getPrefixString()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "scanner_helper_"

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

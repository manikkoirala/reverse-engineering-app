.class public final Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;
.super Ljava/lang/Object;
.source "CropDewrapUtils.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final INSTANCE:Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "CropDewrapUtils"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final TOTAL_MEM_THRESHOLD_DEWRAP:J = 0x719999L

.field private static recordStatus:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;->INSTANCE:Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;

    .line 7
    .line 8
    const/4 v0, -0x1

    .line 9
    sput v0, Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;->recordStatus:I

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final getNeedTrimWhenNoBorder()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static synthetic getNeedTrimWhenNoBorder$annotations()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final isCropDewrapOn()Z
    .locals 4

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/AppConfigJsonUtils;->Oo08()Lcom/intsig/tsapp/sync/AppConfigJson;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/tsapp/sync/AppConfigJson;->isCropDeWrapOn()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const-string v1, "CropDewrapUtils"

    .line 10
    .line 11
    const/4 v2, 0x0

    .line 12
    const/4 v3, 0x1

    .line 13
    if-nez v0, :cond_1

    .line 14
    .line 15
    sget v0, Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;->recordStatus:I

    .line 16
    .line 17
    if-eq v0, v3, :cond_0

    .line 18
    .line 19
    sput v3, Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;->recordStatus:I

    .line 20
    .line 21
    const-string v0, "isCropDewrapOn - AppConfig is off"

    .line 22
    .line 23
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    :cond_0
    return v2

    .line 27
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerEngineUtil;->is64BitEngine()Z

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-nez v0, :cond_3

    .line 32
    .line 33
    sget v0, Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;->recordStatus:I

    .line 34
    .line 35
    const/4 v3, 0x3

    .line 36
    if-eq v0, v3, :cond_2

    .line 37
    .line 38
    sput v3, Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;->recordStatus:I

    .line 39
    .line 40
    const-string v0, "isCropDewrapOn - is64Bit=false"

    .line 41
    .line 42
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    :cond_2
    return v2

    .line 46
    :cond_3
    sget v0, Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;->recordStatus:I

    .line 47
    .line 48
    if-eqz v0, :cond_4

    .line 49
    .line 50
    sput v2, Lcom/intsig/camscanner/scanner/cropdewrap/CropDewrapUtils;->recordStatus:I

    .line 51
    .line 52
    const-string v0, "isCropDewrapOn - finally ON"

    .line 53
    .line 54
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    :cond_4
    return v3
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

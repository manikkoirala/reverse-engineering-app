.class public final Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;
.super Ljava/lang/Object;
.source "CollectWorkbenchEntity.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private volatile bigImageModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

.field private volatile smallImgModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-direct {p0, v0, v0, v1, v0}, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;-><init>(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput-object p1, p0, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->smallImgModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 4
    iput-object p2, p0, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->bigImageModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    return-void
.end method

.method public synthetic constructor <init>(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 1

    and-int/lit8 p4, p3, 0x1

    const/4 v0, 0x0

    if-eqz p4, :cond_0

    move-object p1, v0

    :cond_0
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_1

    move-object p2, v0

    .line 5
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;-><init>(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    return-void
.end method

.method public static synthetic copy$default(Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;ILjava/lang/Object;)Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;
    .locals 0

    .line 1
    and-int/lit8 p4, p3, 0x1

    .line 2
    .line 3
    if-eqz p4, :cond_0

    .line 4
    .line 5
    iget-object p1, p0, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->smallImgModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 6
    .line 7
    :cond_0
    and-int/lit8 p3, p3, 0x2

    .line 8
    .line 9
    if-eqz p3, :cond_1

    .line 10
    .line 11
    iget-object p2, p0, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->bigImageModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 12
    .line 13
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->copy(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;

    .line 14
    .line 15
    .line 16
    move-result-object p0

    .line 17
    return-object p0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
.end method

.method private static final reset$lambda$0(Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p0, p0, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->smallImgModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 7
    .line 8
    if-eqz p0, :cond_0

    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇080()V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->reset$lambda$0(Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public final component1()Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->smallImgModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final component2()Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->bigImageModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final copy(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;

    .line 2
    .line 3
    invoke-direct {v0, p1, p2}, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;-><init>(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 1
    const/4 v0, 0x1

    .line 2
    if-ne p0, p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    instance-of v1, p1, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    if-nez v1, :cond_1

    .line 9
    .line 10
    return v2

    .line 11
    :cond_1
    check-cast p1, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->smallImgModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 14
    .line 15
    iget-object v3, p1, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->smallImgModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 16
    .line 17
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-nez v1, :cond_2

    .line 22
    .line 23
    return v2

    .line 24
    :cond_2
    iget-object v1, p0, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->bigImageModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 25
    .line 26
    iget-object p1, p1, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->bigImageModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 27
    .line 28
    invoke-static {v1, p1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 29
    .line 30
    .line 31
    move-result p1

    .line 32
    if-nez p1, :cond_3

    .line 33
    .line 34
    return v2

    .line 35
    :cond_3
    return v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final getBigImageModel()Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->bigImageModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getSmallImgModel()Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->smallImgModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public hashCode()I
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->smallImgModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    const/4 v0, 0x0

    .line 7
    goto :goto_0

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->smallImgModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->hashCode()I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    .line 15
    .line 16
    iget-object v2, p0, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->bigImageModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 17
    .line 18
    if-nez v2, :cond_1

    .line 19
    .line 20
    goto :goto_1

    .line 21
    :cond_1
    iget-object v1, p0, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->bigImageModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 22
    .line 23
    invoke-virtual {v1}, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->hashCode()I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    :goto_1
    add-int/2addr v0, v1

    .line 28
    return v0
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final reset()V
    .locals 2
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .line 1
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, L〇80O/〇080;

    .line 6
    .line 7
    invoke-direct {v1, p0}, L〇80O/〇080;-><init>(Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 11
    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    iput-object v0, p0, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->smallImgModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->bigImageModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
.end method

.method public final setBigImageModel(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->bigImageModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final setSmallImgModel(Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->smallImgModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public toString()Ljava/lang/String;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->smallImgModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->bigImageModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 4
    .line 5
    new-instance v2, Ljava/lang/StringBuilder;

    .line 6
    .line 7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 8
    .line 9
    .line 10
    const-string v3, "CollectWorkbenchEntity(smallImgModel="

    .line 11
    .line 12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 13
    .line 14
    .line 15
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 16
    .line 17
    .line 18
    const-string v0, ", bigImageModel="

    .line 19
    .line 20
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    const-string v0, ")"

    .line 27
    .line 28
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    return-object v0
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method public final tryUpload()V
    .locals 48
    .annotation build Landroidx/annotation/UiThread;
    .end annotation

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    iget-object v1, v0, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->smallImgModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-eqz v1, :cond_2

    .line 7
    .line 8
    iget-object v3, v0, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->bigImageModel:Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;

    .line 9
    .line 10
    if-eqz v3, :cond_0

    .line 11
    .line 12
    new-instance v2, Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 13
    .line 14
    move-object v4, v2

    .line 15
    const/4 v5, 0x0

    .line 16
    const/4 v6, 0x0

    .line 17
    const/4 v7, 0x0

    .line 18
    const/4 v8, 0x0

    .line 19
    const/4 v9, 0x0

    .line 20
    const/4 v10, 0x0

    .line 21
    const/4 v11, 0x0

    .line 22
    const/4 v12, 0x0

    .line 23
    const/4 v13, 0x0

    .line 24
    const/4 v14, 0x0

    .line 25
    const/4 v15, 0x0

    .line 26
    const/16 v16, 0x0

    .line 27
    .line 28
    const/16 v17, 0x0

    .line 29
    .line 30
    const/16 v18, 0x0

    .line 31
    .line 32
    const/16 v19, 0x0

    .line 33
    .line 34
    const/16 v20, 0x0

    .line 35
    .line 36
    const/16 v21, 0x0

    .line 37
    .line 38
    const/16 v22, 0x0

    .line 39
    .line 40
    const/16 v23, 0x0

    .line 41
    .line 42
    const/16 v24, 0x0

    .line 43
    .line 44
    const/16 v25, 0x0

    .line 45
    .line 46
    const/16 v26, 0x0

    .line 47
    .line 48
    const/16 v27, 0x0

    .line 49
    .line 50
    const/16 v28, 0x0

    .line 51
    .line 52
    const/16 v29, 0x0

    .line 53
    .line 54
    const/16 v30, 0x0

    .line 55
    .line 56
    const/16 v31, 0x0

    .line 57
    .line 58
    const/16 v32, 0x0

    .line 59
    .line 60
    const/16 v33, 0x0

    .line 61
    .line 62
    const/16 v34, 0x0

    .line 63
    .line 64
    const/16 v35, 0x0

    .line 65
    .line 66
    const/16 v36, 0x0

    .line 67
    .line 68
    const/16 v37, 0x0

    .line 69
    .line 70
    const/16 v38, 0x0

    .line 71
    .line 72
    const/16 v39, 0x0

    .line 73
    .line 74
    const/16 v40, 0x0

    .line 75
    .line 76
    const/16 v41, 0x0

    .line 77
    .line 78
    const/16 v42, 0x0

    .line 79
    .line 80
    const/16 v43, 0x0

    .line 81
    .line 82
    const/16 v44, 0x0

    .line 83
    .line 84
    const/16 v45, -0x1

    .line 85
    .line 86
    const/16 v46, 0xff

    .line 87
    .line 88
    const/16 v47, 0x0

    .line 89
    .line 90
    invoke-direct/range {v4 .. v47}, Lcom/intsig/camscanner/util/ImageProgressClient;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[IZIIIII[IZZLjava/lang/String;Ljava/lang/String;ZIZZIIIZZZZZZZZIZIZZIZIZLjava/lang/String;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 91
    .line 92
    .line 93
    iget-object v4, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 94
    .line 95
    invoke-virtual {v2, v4}, Lcom/intsig/camscanner/util/ImageProgressClient;->setSrcImagePath(Ljava/lang/String;)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 96
    .line 97
    .line 98
    move-result-object v2

    .line 99
    iget-object v4, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 100
    .line 101
    const/4 v5, 0x1

    .line 102
    invoke-static {v4, v5}, Lcom/intsig/utils/ImageUtil;->Oooo8o0〇(Ljava/lang/String;Z)[I

    .line 103
    .line 104
    .line 105
    move-result-object v4

    .line 106
    invoke-virtual {v2, v4}, Lcom/intsig/camscanner/util/ImageProgressClient;->setRawImageSize([I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 107
    .line 108
    .line 109
    move-result-object v2

    .line 110
    const/4 v4, 0x0

    .line 111
    invoke-virtual {v2, v4}, Lcom/intsig/camscanner/util/ImageProgressClient;->enableTrim(Z)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 112
    .line 113
    .line 114
    move-result-object v2

    .line 115
    iget v4, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 116
    .line 117
    invoke-virtual {v2, v4}, Lcom/intsig/camscanner/util/ImageProgressClient;->setRation(I)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 118
    .line 119
    .line 120
    move-result-object v2

    .line 121
    iget-object v4, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 122
    .line 123
    invoke-virtual {v2, v4}, Lcom/intsig/camscanner/util/ImageProgressClient;->setSaveImagePath(Ljava/lang/String;)Lcom/intsig/camscanner/util/ImageProgressClient;

    .line 124
    .line 125
    .line 126
    move-result-object v2

    .line 127
    invoke-static {}, Lcom/intsig/utils/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object v4

    .line 131
    new-instance v5, Ljava/lang/StringBuilder;

    .line 132
    .line 133
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 134
    .line 135
    .line 136
    const-string v6, "tryUpload_COLLECT_Workbench_"

    .line 137
    .line 138
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    .line 140
    .line 141
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    .line 143
    .line 144
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 145
    .line 146
    .line 147
    move-result-object v4

    .line 148
    invoke-virtual {v2, v4}, Lcom/intsig/camscanner/util/ImageProgressClient;->executeProgress(Ljava/lang/String;)V

    .line 149
    .line 150
    .line 151
    new-instance v2, Lcom/intsig/camscanner/scanner/SpecialImageCollectNewEntity;

    .line 152
    .line 153
    invoke-direct {v2}, Lcom/intsig/camscanner/scanner/SpecialImageCollectNewEntity;-><init>()V

    .line 154
    .line 155
    .line 156
    const-string v4, "single_multi_enhance_android_upload"

    .line 157
    .line 158
    invoke-virtual {v2, v4}, Lcom/intsig/camscanner/scanner/SpecialImageCollectNewEntity;->setExperimentKey(Ljava/lang/String;)V

    .line 159
    .line 160
    .line 161
    iget-object v4, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oo8ooo8O:[I

    .line 162
    .line 163
    invoke-virtual {v2, v4}, Lcom/intsig/camscanner/scanner/SpecialImageCollectNewEntity;->setEngineSmallBounds([I)V

    .line 164
    .line 165
    .line 166
    iget-object v4, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->oo8ooo8O:[I

    .line 167
    .line 168
    invoke-virtual {v2, v4}, Lcom/intsig/camscanner/scanner/SpecialImageCollectNewEntity;->setEngineBounds([I)V

    .line 169
    .line 170
    .line 171
    iget v4, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->〇8〇oO〇〇8o:I

    .line 172
    .line 173
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 174
    .line 175
    .line 176
    move-result-object v4

    .line 177
    invoke-virtual {v2, v4}, Lcom/intsig/camscanner/scanner/SpecialImageCollectNewEntity;->setRotation(Ljava/lang/Integer;)V

    .line 178
    .line 179
    .line 180
    iget-object v1, v1, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 181
    .line 182
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/scanner/SpecialImageCollectNewEntity;->setBeforePath(Ljava/lang/String;)V

    .line 183
    .line 184
    .line 185
    iget-object v1, v3, Lcom/intsig/camscanner/multiimageedit/model/MultiImageEditModel;->OO:Ljava/lang/String;

    .line 186
    .line 187
    invoke-virtual {v2, v1}, Lcom/intsig/camscanner/scanner/SpecialImageCollectNewEntity;->setAfterPath(Ljava/lang/String;)V

    .line 188
    .line 189
    .line 190
    new-instance v1, Lcom/intsig/camscanner/scanner/SpecialImageCollectNewRunnable;

    .line 191
    .line 192
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/scanner/SpecialImageCollectNewRunnable;-><init>(Lcom/intsig/camscanner/scanner/SpecialImageCollectNewEntity;)V

    .line 193
    .line 194
    .line 195
    invoke-static {}, Lcom/intsig/thread/ThreadPoolSingleton;->O8()Lcom/intsig/thread/ThreadPoolSingleton;

    .line 196
    .line 197
    .line 198
    move-result-object v2

    .line 199
    invoke-virtual {v2, v1}, Lcom/intsig/thread/ThreadPoolSingleton;->〇o00〇〇Oo(Ljava/lang/Runnable;)V

    .line 200
    .line 201
    .line 202
    sget-object v2, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 203
    .line 204
    :cond_0
    if-nez v2, :cond_1

    .line 205
    .line 206
    return-void

    .line 207
    :cond_1
    sget-object v2, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 208
    .line 209
    :cond_2
    if-nez v2, :cond_3

    .line 210
    .line 211
    return-void

    .line 212
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/intsig/camscanner/scanner/collect/CollectWorkbenchEntity;->reset()V

    .line 213
    .line 214
    .line 215
    return-void
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
.end method

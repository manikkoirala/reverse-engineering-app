.class public final Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;
.super Ljava/lang/Object;
.source "SuperFilterParticleLayer.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer$Particle;,
        Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final Companion:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final MAX_LIVE:J = 0xfa0L

.field private static final MAX_SIZE:I = 0xb4

.field private static final MAX_SPEED_X:D = 0.06

.field private static final MAX_SPEED_Y:D = 0.08

.field private static final MIN_LIVE:J = 0x7d0L

.field private static final MIN_SPEED_X:D = 0.02

.field private static final MIN_SPEED_Y:D = 0.02

.field private static final MIN_WIDTH:F = 1.2f


# instance fields
.field private final context:Landroid/content/Context;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private h:I

.field private mIsInit:Z

.field private final mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer$Particle;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final mPaint:Landroid/graphics/Paint;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final mPointSize:F

.field private final mRandom:Lkotlin/random/Random;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private mShowCount:I

.field private mStartTime:J

.field private w:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->Companion:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->context:Landroid/content/Context;

    .line 10
    .line 11
    new-instance v0, Ljava/util/ArrayList;

    .line 12
    .line 13
    const/16 v1, 0xb4

    .line 14
    .line 15
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 16
    .line 17
    .line 18
    iput-object v0, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mList:Ljava/util/ArrayList;

    .line 19
    .line 20
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    iget p1, p1, Landroid/util/DisplayMetrics;->density:F

    .line 29
    .line 30
    const v0, 0x3f99999a    # 1.2f

    .line 31
    .line 32
    .line 33
    mul-float p1, p1, v0

    .line 34
    .line 35
    iput p1, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mPointSize:F

    .line 36
    .line 37
    new-instance v0, Landroid/graphics/Paint;

    .line 38
    .line 39
    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 40
    .line 41
    .line 42
    const/4 v2, 0x1

    .line 43
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 44
    .line 45
    .line 46
    const/high16 v2, -0x10000

    .line 47
    .line 48
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 49
    .line 50
    .line 51
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    .line 52
    .line 53
    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 57
    .line 58
    .line 59
    iput-object v0, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mPaint:Landroid/graphics/Paint;

    .line 60
    .line 61
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    .line 62
    .line 63
    .line 64
    move-result-wide v2

    .line 65
    invoke-static {v2, v3}, Lkotlin/random/RandomKt;->〇080(J)Lkotlin/random/Random;

    .line 66
    .line 67
    .line 68
    move-result-object p1

    .line 69
    iput-object p1, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mRandom:Lkotlin/random/Random;

    .line 70
    .line 71
    iput v1, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mShowCount:I

    .line 72
    .line 73
    const/4 p1, 0x0

    .line 74
    :goto_0
    if-ge p1, v1, :cond_0

    .line 75
    .line 76
    new-instance v0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer$Particle;

    .line 77
    .line 78
    iget-object v2, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mPaint:Landroid/graphics/Paint;

    .line 79
    .line 80
    invoke-direct {v0, v2}, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer$Particle;-><init>(Landroid/graphics/Paint;)V

    .line 81
    .line 82
    .line 83
    iget-object v2, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mList:Ljava/util/ArrayList;

    .line 84
    .line 85
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 86
    .line 87
    .line 88
    add-int/lit8 p1, p1, 0x1

    .line 89
    .line 90
    goto :goto_0

    .line 91
    :cond_0
    return-void
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final initParticles()V
    .locals 15

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mIsInit:Z

    .line 3
    .line 4
    iget v1, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->h:I

    .line 5
    .line 6
    const/16 v2, -0x64

    .line 7
    .line 8
    if-le v1, v2, :cond_1

    .line 9
    .line 10
    iget v3, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->w:I

    .line 11
    .line 12
    if-gtz v3, :cond_0

    .line 13
    .line 14
    goto/16 :goto_1

    .line 15
    .line 16
    :cond_0
    const/4 v3, 0x1

    .line 17
    iput-boolean v3, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mIsInit:Z

    .line 18
    .line 19
    iget-object v3, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mList:Ljava/util/ArrayList;

    .line 20
    .line 21
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    .line 26
    .line 27
    .line 28
    move-result v4

    .line 29
    if-eqz v4, :cond_1

    .line 30
    .line 31
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v4

    .line 35
    check-cast v4, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer$Particle;

    .line 36
    .line 37
    iget-object v5, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mRandom:Lkotlin/random/Random;

    .line 38
    .line 39
    iget v6, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->w:I

    .line 40
    .line 41
    invoke-virtual {v5, v0, v6}, Lkotlin/random/Random;->nextInt(II)I

    .line 42
    .line 43
    .line 44
    move-result v5

    .line 45
    int-to-float v6, v5

    .line 46
    iget-object v5, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mRandom:Lkotlin/random/Random;

    .line 47
    .line 48
    invoke-virtual {v5, v2, v1}, Lkotlin/random/Random;->nextInt(II)I

    .line 49
    .line 50
    .line 51
    move-result v5

    .line 52
    int-to-float v7, v5

    .line 53
    iget-object v5, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mRandom:Lkotlin/random/Random;

    .line 54
    .line 55
    const-wide/16 v8, 0x7d0

    .line 56
    .line 57
    const-wide/16 v10, 0xfa0

    .line 58
    .line 59
    invoke-virtual {v5, v8, v9, v10, v11}, Lkotlin/random/Random;->nextLong(JJ)J

    .line 60
    .line 61
    .line 62
    move-result-wide v10

    .line 63
    iget-object v5, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mRandom:Lkotlin/random/Random;

    .line 64
    .line 65
    const/4 v8, 0x3

    .line 66
    invoke-virtual {v5, v8}, Lkotlin/random/Random;->nextInt(I)I

    .line 67
    .line 68
    .line 69
    move-result v5

    .line 70
    int-to-float v5, v5

    .line 71
    iget-object v8, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->context:Landroid/content/Context;

    .line 72
    .line 73
    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 74
    .line 75
    .line 76
    move-result-object v8

    .line 77
    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 78
    .line 79
    .line 80
    move-result-object v8

    .line 81
    iget v8, v8, Landroid/util/DisplayMetrics;->density:F

    .line 82
    .line 83
    mul-float v12, v5, v8

    .line 84
    .line 85
    iget-wide v8, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mStartTime:J

    .line 86
    .line 87
    iget-object v5, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mRandom:Lkotlin/random/Random;

    .line 88
    .line 89
    invoke-virtual {v5}, Lkotlin/random/Random;->nextBoolean()Z

    .line 90
    .line 91
    .line 92
    move-result v13

    .line 93
    iget-object v5, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mRandom:Lkotlin/random/Random;

    .line 94
    .line 95
    invoke-virtual {v5}, Lkotlin/random/Random;->nextBoolean()Z

    .line 96
    .line 97
    .line 98
    move-result v14

    .line 99
    move-object v5, v4

    .line 100
    invoke-virtual/range {v5 .. v14}, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer$Particle;->reset(FFJJFZZ)V

    .line 101
    .line 102
    .line 103
    iget-object v5, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mRandom:Lkotlin/random/Random;

    .line 104
    .line 105
    const-wide v6, 0x3fb47ae147ae147bL    # 0.08

    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    const-wide v8, 0x3f947ae147ae147bL    # 0.02

    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    invoke-virtual {v5, v8, v9, v6, v7}, Lkotlin/random/Random;->nextDouble(DD)D

    .line 116
    .line 117
    .line 118
    move-result-wide v5

    .line 119
    double-to-float v5, v5

    .line 120
    invoke-virtual {v4, v5}, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer$Particle;->setSpeedY(F)V

    .line 121
    .line 122
    .line 123
    iget-object v5, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mRandom:Lkotlin/random/Random;

    .line 124
    .line 125
    const-wide v6, 0x3faeb851eb851eb8L    # 0.06

    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    invoke-virtual {v5, v8, v9, v6, v7}, Lkotlin/random/Random;->nextDouble(DD)D

    .line 131
    .line 132
    .line 133
    move-result-wide v5

    .line 134
    double-to-float v5, v5

    .line 135
    invoke-virtual {v4, v5}, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer$Particle;->setSpeedX(F)V

    .line 136
    .line 137
    .line 138
    goto :goto_0

    .line 139
    :cond_1
    :goto_1
    return-void
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
.end method


# virtual methods
.method public final cancel()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final draw(Landroid/graphics/RectF;Landroid/graphics/Canvas;)V
    .locals 4
    .param p1    # Landroid/graphics/RectF;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Landroid/graphics/Canvas;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "displayRectF"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "c"

    .line 7
    .line 8
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    iget-boolean v0, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mIsInit:Z

    .line 12
    .line 13
    if-nez v0, :cond_0

    .line 14
    .line 15
    return-void

    .line 16
    :cond_0
    iget v0, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mShowCount:I

    .line 17
    .line 18
    const/4 v1, 0x0

    .line 19
    :goto_0
    if-ge v1, v0, :cond_2

    .line 20
    .line 21
    iget-object v2, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mList:Ljava/util/ArrayList;

    .line 22
    .line 23
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    const-string v3, "mList[i]"

    .line 28
    .line 29
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    check-cast v2, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer$Particle;

    .line 33
    .line 34
    invoke-virtual {v2, p1}, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer$Particle;->isInBounds(Landroid/graphics/RectF;)Z

    .line 35
    .line 36
    .line 37
    move-result v3

    .line 38
    if-eqz v3, :cond_1

    .line 39
    .line 40
    invoke-virtual {v2, p2}, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer$Particle;->draw(Landroid/graphics/Canvas;)V

    .line 41
    .line 42
    .line 43
    :cond_1
    add-int/lit8 v1, v1, 0x1

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_2
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
.end method

.method public final getMPaint()Landroid/graphics/Paint;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mPaint:Landroid/graphics/Paint;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final onSizeChange(II)V
    .locals 0

    .line 1
    add-int/lit8 p1, p1, -0x14

    .line 2
    .line 3
    iput p1, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->w:I

    .line 4
    .line 5
    iput p2, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->h:I

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
.end method

.method public final prepare()V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->reset()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->initParticles()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final reset()V
    .locals 2

    .line 1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    iput-wide v0, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mStartTime:J

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final setColor(I)V
    .locals 1
    .param p1    # I
        .annotation build Landroidx/annotation/ColorInt;
        .end annotation
    .end param

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mPaint:Landroid/graphics/Paint;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public final update(I)V
    .locals 5

    .line 1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    iget v2, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mShowCount:I

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    :goto_0
    if-ge v3, v2, :cond_0

    .line 9
    .line 10
    iget-object v4, p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer;->mList:Ljava/util/ArrayList;

    .line 11
    .line 12
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 13
    .line 14
    .line 15
    move-result-object v4

    .line 16
    check-cast v4, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer$Particle;

    .line 17
    .line 18
    invoke-virtual {v4, v0, v1, p1}, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterParticleLayer$Particle;->update(JI)V

    .line 19
    .line 20
    .line 21
    add-int/lit8 v3, v3, 0x1

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

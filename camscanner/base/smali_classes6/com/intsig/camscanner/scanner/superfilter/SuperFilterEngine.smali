.class public final Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine;
.super Ljava/lang/Object;
.source "SuperFilterEngine.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final CPU_MAX_FREQUENCY_THRESHOLD:J = 0x1a17b0L

.field public static final Companion:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final KEY_SUPER_FILTER_MODE:Ljava/lang/String; = "key_super_filter_mode"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final MODE_FORCE_LOCAL:I = 0x1

.field public static final MODE_FORCE_SERVER:I = 0x2

.field public static final MODE_WITH_IMAGE:I = 0x0

.field public static final SUPER_FILTER_RESULT_LOCAL_ERROR:I = 0x2

.field public static final SUPER_FILTER_RESULT_NETWORK_ERROR:I = 0x3

.field public static final SUPER_FILTER_RESULT_NO_NETWORK:I = 0x1

.field public static final SUPER_FILTER_RESULT_OK:I = 0x0

.field public static final SUPER_FILTER_RESULT_OVER_DAILY_LIMIT:I = 0x4

.field private static final TAG:Ljava/lang/String; = "SuperFilterEngine"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static localStartTime:J

.field private static needToastForMultiImage:Z

.field private static serverStartTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine;->Companion:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic access$getLocalStartTime$cp()J
    .locals 2

    .line 1
    sget-wide v0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine;->localStartTime:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic access$getNeedToastForMultiImage$cp()Z
    .locals 1

    .line 1
    sget-boolean v0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine;->needToastForMultiImage:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic access$getServerStartTime$cp()J
    .locals 2

    .line 1
    sget-wide v0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine;->serverStartTime:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic access$setLocalStartTime$cp(J)V
    .locals 0

    .line 1
    sput-wide p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine;->localStartTime:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic access$setNeedToastForMultiImage$cp(Z)V
    .locals 0

    .line 1
    sput-boolean p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine;->needToastForMultiImage:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final synthetic access$setServerStartTime$cp(J)V
    .locals 0

    .line 1
    sput-wide p0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine;->serverStartTime:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final isAllowLocalSuperFilter()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine;->Companion:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine$Companion;->isAllowLocalSuperFilter()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final isDeviceSupportLocalSuperFilter()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine;->Companion:Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/scanner/superfilter/SuperFilterEngine$Companion;->isDeviceSupportLocalSuperFilter()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

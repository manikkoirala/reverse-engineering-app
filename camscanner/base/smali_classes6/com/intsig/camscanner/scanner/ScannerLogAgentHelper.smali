.class public final Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;
.super Ljava/lang/Object;
.source "ScannerLogAgentHelper.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final INSTANCE:Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "ScannerLogAgentHelper"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static volatile localMultiStartTime:J

.field private static volatile localSingleStartTime:J

.field private static volatile serverMultiStartTime:J

.field private static volatile serverSingleStartTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getCurrentCacheTime(Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog;)J
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterLocalSingle;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterLocalSingle;

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    sget-wide v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;->localSingleStartTime:J

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterServerSingle;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterServerSingle;

    .line 13
    .line 14
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    sget-wide v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;->serverSingleStartTime:J

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterLocalMulti;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterLocalMulti;

    .line 24
    .line 25
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_2

    .line 30
    .line 31
    sget-wide v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;->localMultiStartTime:J

    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterServerMulti;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterServerMulti;

    .line 35
    .line 36
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 37
    .line 38
    .line 39
    move-result p1

    .line 40
    if-eqz p1, :cond_3

    .line 41
    .line 42
    sget-wide v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;->serverMultiStartTime:J

    .line 43
    .line 44
    :goto_0
    return-wide v0

    .line 45
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    .line 46
    .line 47
    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 48
    .line 49
    .line 50
    throw p1
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public static final logForClickThumb()V
    .locals 4

    .line 1
    const-string v0, "type"

    .line 2
    .line 3
    const-string v1, "scan_doc"

    .line 4
    .line 5
    const-string v2, "CSScan"

    .line 6
    .line 7
    const-string v3, "thumbnail"

    .line 8
    .line 9
    invoke-static {v2, v3, v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final logForCostLog(Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog;)V
    .locals 9
    .param p0    # Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "type"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->useSuperFilterAsDefault()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 14
    .line 15
    .line 16
    move-result-wide v0

    .line 17
    sget-object v2, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;

    .line 18
    .line 19
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;->getCurrentCacheTime(Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog;)J

    .line 20
    .line 21
    .line 22
    move-result-wide v3

    .line 23
    new-instance v5, Ljava/lang/StringBuilder;

    .line 24
    .line 25
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 26
    .line 27
    .line 28
    const-string v6, "logForCostLog: type="

    .line 29
    .line 30
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    const-string v6, ", time="

    .line 37
    .line 38
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v5, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    const-string v3, ", current="

    .line 45
    .line 46
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 50
    .line 51
    .line 52
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v3

    .line 56
    const-string v4, "ScannerLogAgentHelper"

    .line 57
    .line 58
    invoke-static {v4, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;->getCurrentCacheTime(Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog;)J

    .line 62
    .line 63
    .line 64
    move-result-wide v5

    .line 65
    sub-long/2addr v0, v5

    .line 66
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 67
    .line 68
    .line 69
    move-result-object v0

    .line 70
    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    .line 71
    .line 72
    .line 73
    move-result-wide v5

    .line 74
    const-wide/16 v7, 0x1

    .line 75
    .line 76
    const/4 v1, 0x0

    .line 77
    cmp-long v3, v7, v5

    .line 78
    .line 79
    if-gtz v3, :cond_1

    .line 80
    .line 81
    const-wide/32 v7, 0xea61

    .line 82
    .line 83
    .line 84
    cmp-long v3, v5, v7

    .line 85
    .line 86
    if-gez v3, :cond_1

    .line 87
    .line 88
    const/4 v1, 0x1

    .line 89
    :cond_1
    if-eqz v1, :cond_2

    .line 90
    .line 91
    goto :goto_0

    .line 92
    :cond_2
    const/4 v0, 0x0

    .line 93
    :goto_0
    if-eqz v0, :cond_3

    .line 94
    .line 95
    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    .line 96
    .line 97
    .line 98
    move-result-wide v0

    .line 99
    new-instance v3, Lorg/json/JSONObject;

    .line 100
    .line 101
    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 102
    .line 103
    .line 104
    :try_start_0
    const-string v5, "time"

    .line 105
    .line 106
    invoke-virtual {v3, v5, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 107
    .line 108
    .line 109
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;->provideCostLogPageId(Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog;)Ljava/lang/String;

    .line 110
    .line 111
    .line 112
    move-result-object v0

    .line 113
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;->provideCostLogActionId(Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog;)Ljava/lang/String;

    .line 114
    .line 115
    .line 116
    move-result-object v1

    .line 117
    invoke-static {v0, v1, v3}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 118
    .line 119
    .line 120
    goto :goto_1

    .line 121
    :catchall_0
    move-exception v0

    .line 122
    new-instance v1, Ljava/lang/StringBuilder;

    .line 123
    .line 124
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 125
    .line 126
    .line 127
    const-string v2, "logForCostLog but t="

    .line 128
    .line 129
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    .line 131
    .line 132
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 133
    .line 134
    .line 135
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 136
    .line 137
    .line 138
    move-result-object v0

    .line 139
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    .line 141
    .line 142
    :cond_3
    :goto_1
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterLocalSingle;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterLocalSingle;

    .line 143
    .line 144
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 145
    .line 146
    .line 147
    move-result v0

    .line 148
    const-wide/16 v1, 0x0

    .line 149
    .line 150
    if-eqz v0, :cond_4

    .line 151
    .line 152
    sput-wide v1, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;->localSingleStartTime:J

    .line 153
    .line 154
    goto :goto_2

    .line 155
    :cond_4
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterServerSingle;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterServerSingle;

    .line 156
    .line 157
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 158
    .line 159
    .line 160
    move-result v0

    .line 161
    if-eqz v0, :cond_5

    .line 162
    .line 163
    sput-wide v1, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;->serverSingleStartTime:J

    .line 164
    .line 165
    goto :goto_2

    .line 166
    :cond_5
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterLocalMulti;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterLocalMulti;

    .line 167
    .line 168
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 169
    .line 170
    .line 171
    move-result v0

    .line 172
    if-eqz v0, :cond_6

    .line 173
    .line 174
    sput-wide v1, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;->localMultiStartTime:J

    .line 175
    .line 176
    goto :goto_2

    .line 177
    :cond_6
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterServerMulti;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterServerMulti;

    .line 178
    .line 179
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 180
    .line 181
    .line 182
    move-result p0

    .line 183
    if-eqz p0, :cond_7

    .line 184
    .line 185
    sput-wide v1, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;->serverMultiStartTime:J

    .line 186
    .line 187
    :cond_7
    :goto_2
    return-void
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
.end method

.method public static final logForNewDocDone()V
    .locals 2

    .line 1
    const-string v0, "CSNewDoc"

    .line 2
    .line 3
    const-string v1, "newdoc_done"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final logForWorkbenchScanComplete(I)V
    .locals 4

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "logForWorkbenchScanComplete: pageNum="

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const-string v1, "ScannerLogAgentHelper"

    .line 19
    .line 20
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    new-instance v0, Lorg/json/JSONObject;

    .line 24
    .line 25
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 26
    .line 27
    .line 28
    :try_start_0
    const-string v2, "scheme"

    .line 29
    .line 30
    const/4 v3, 0x1

    .line 31
    if-le p0, v3, :cond_0

    .line 32
    .line 33
    const-string v3, "batch"

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_0
    const-string v3, "single"

    .line 37
    .line 38
    :goto_0
    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 39
    .line 40
    .line 41
    const-string v2, "type"

    .line 42
    .line 43
    const-string v3, "scan_doc"

    .line 44
    .line 45
    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 46
    .line 47
    .line 48
    const-string v2, "num"

    .line 49
    .line 50
    invoke-virtual {v0, v2, p0}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 51
    .line 52
    .line 53
    goto :goto_1

    .line 54
    :catchall_0
    move-exception p0

    .line 55
    new-instance v2, Ljava/lang/StringBuilder;

    .line 56
    .line 57
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 58
    .line 59
    .line 60
    const-string v3, "logForWorkbenchScanComplete: error = "

    .line 61
    .line 62
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object p0

    .line 72
    invoke-static {v1, p0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    :goto_1
    const-string p0, "CSScan"

    .line 76
    .line 77
    const-string v1, "complete"

    .line 78
    .line 79
    invoke-static {p0, v1, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 80
    .line 81
    .line 82
    return-void
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method public static final prepareForCostLog(Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog;)V
    .locals 4
    .param p0    # Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "type"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {}, Lcom/intsig/camscanner/scanner/ScannerPreferenceHelper;->useSuperFilterAsDefault()Z

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 14
    .line 15
    .line 16
    move-result-wide v0

    .line 17
    sget-object v2, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterLocalSingle;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterLocalSingle;

    .line 18
    .line 19
    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    if-eqz v2, :cond_1

    .line 24
    .line 25
    sput-wide v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;->localSingleStartTime:J

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_1
    sget-object v2, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterServerSingle;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterServerSingle;

    .line 29
    .line 30
    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 31
    .line 32
    .line 33
    move-result v2

    .line 34
    if-eqz v2, :cond_2

    .line 35
    .line 36
    sput-wide v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;->serverSingleStartTime:J

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_2
    sget-object v2, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterLocalMulti;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterLocalMulti;

    .line 40
    .line 41
    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 42
    .line 43
    .line 44
    move-result v2

    .line 45
    if-eqz v2, :cond_3

    .line 46
    .line 47
    sput-wide v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;->localMultiStartTime:J

    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_3
    sget-object v2, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterServerMulti;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterServerMulti;

    .line 51
    .line 52
    invoke-static {p0, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 53
    .line 54
    .line 55
    move-result v2

    .line 56
    if-eqz v2, :cond_4

    .line 57
    .line 58
    sput-wide v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;->serverMultiStartTime:J

    .line 59
    .line 60
    :cond_4
    :goto_0
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;

    .line 61
    .line 62
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper;->getCurrentCacheTime(Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog;)J

    .line 63
    .line 64
    .line 65
    move-result-wide v0

    .line 66
    new-instance v2, Ljava/lang/StringBuilder;

    .line 67
    .line 68
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 69
    .line 70
    .line 71
    const-string v3, "prepareForCostLog: type="

    .line 72
    .line 73
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    .line 75
    .line 76
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    const-string p0, ", time="

    .line 80
    .line 81
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    .line 83
    .line 84
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object p0

    .line 91
    const-string v0, "ScannerLogAgentHelper"

    .line 92
    .line 93
    invoke-static {v0, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    .line 95
    .line 96
    return-void
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
.end method

.method private final provideCostLogActionId(Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog;)Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterLocalSingle;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterLocalSingle;

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string p1, "single_super_filter_time_local"

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterServerSingle;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterServerSingle;

    .line 13
    .line 14
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    const-string p1, "single_super_filter_time_sever"

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterLocalMulti;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterLocalMulti;

    .line 24
    .line 25
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_2

    .line 30
    .line 31
    const-string p1, "batch_super_filter_time_local"

    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterServerMulti;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterServerMulti;

    .line 35
    .line 36
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 37
    .line 38
    .line 39
    move-result p1

    .line 40
    if-eqz p1, :cond_3

    .line 41
    .line 42
    const-string p1, "batch_super_filter_time_sever"

    .line 43
    .line 44
    :goto_0
    return-object p1

    .line 45
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    .line 46
    .line 47
    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 48
    .line 49
    .line 50
    throw p1
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method private final provideCostLogPageId(Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog;)Ljava/lang/String;
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterLocalSingle;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterLocalSingle;

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x1

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    const/4 v0, 0x1

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterServerSingle;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterServerSingle;

    .line 13
    .line 14
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    :goto_0
    if-eqz v0, :cond_1

    .line 19
    .line 20
    const-string p1, "CSEnhance"

    .line 21
    .line 22
    goto :goto_2

    .line 23
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterLocalMulti;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterLocalMulti;

    .line 24
    .line 25
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_2

    .line 30
    .line 31
    goto :goto_1

    .line 32
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterServerMulti;->INSTANCE:Lcom/intsig/camscanner/scanner/ScannerLogAgentHelper$TypeSuperFilterLog$TypeSuperFilterServerMulti;

    .line 33
    .line 34
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 35
    .line 36
    .line 37
    move-result v1

    .line 38
    :goto_1
    if-eqz v1, :cond_3

    .line 39
    .line 40
    const-string p1, "CSBatchResult"

    .line 41
    .line 42
    :goto_2
    return-object p1

    .line 43
    :cond_3
    new-instance p1, Lkotlin/NoWhenBranchMatchedException;

    .line 44
    .line 45
    invoke-direct {p1}, Lkotlin/NoWhenBranchMatchedException;-><init>()V

    .line 46
    .line 47
    .line 48
    throw p1
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

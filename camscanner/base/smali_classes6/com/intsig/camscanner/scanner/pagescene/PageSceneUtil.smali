.class public final Lcom/intsig/camscanner/scanner/pagescene/PageSceneUtil;
.super Ljava/lang/Object;
.source "PageSceneUtil.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/scanner/pagescene/PageSceneUtil$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final Companion:Lcom/intsig/camscanner/scanner/pagescene/PageSceneUtil$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final HIDE_TAG_LIST:[I
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final PAGE_SCENE_TAG_NAME_RES_ID_LIST_NEW:[I
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "PageSceneUtil"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/scanner/pagescene/PageSceneUtil$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/scanner/pagescene/PageSceneUtil$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/scanner/pagescene/PageSceneUtil;->Companion:Lcom/intsig/camscanner/scanner/pagescene/PageSceneUtil$Companion;

    .line 8
    .line 9
    const/16 v0, 0x1d

    .line 10
    .line 11
    new-array v0, v0, [I

    .line 12
    .line 13
    fill-array-data v0, :array_0

    .line 14
    .line 15
    .line 16
    sput-object v0, Lcom/intsig/camscanner/scanner/pagescene/PageSceneUtil;->PAGE_SCENE_TAG_NAME_RES_ID_LIST_NEW:[I

    .line 17
    .line 18
    const/4 v0, 0x2

    .line 19
    new-array v0, v0, [I

    .line 20
    .line 21
    fill-array-data v0, :array_1

    .line 22
    .line 23
    .line 24
    sput-object v0, Lcom/intsig/camscanner/scanner/pagescene/PageSceneUtil;->HIDE_TAG_LIST:[I

    .line 25
    .line 26
    return-void

    .line 27
    :array_0
    .array-data 4
        0x7f131a00
        0x7f131a03
        0x7f131a06
        0x7f131a04
        0x7f130f86
        0x7f130f89
        0x7f130f85
        0x7f13126f
        0x7f13126e
        0x7f130f8a
        0x7f131a02
        0x7f131a07
        0x7f131a05
        0x7f130f87
        0x7f131270
        0x7f130f88
        0x7f131a01
        0x7f13126d
        0x7f1319ff
        0x7f13126c
        0x7f131779
        0x7f131781
        0x7f13177a
        0x7f13177e
        0x7f131780
        0x7f131782
        0x7f13177c
        0x7f13177f
        0x7f13177b
    .end array-data

    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    :array_1
    .array-data 4
        0x7f13126f
        0x7f131270
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic access$getHIDE_TAG_LIST$cp()[I
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/pagescene/PageSceneUtil;->HIDE_TAG_LIST:[I

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final synthetic access$getPAGE_SCENE_TAG_NAME_RES_ID_LIST_NEW$cp()[I
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/pagescene/PageSceneUtil;->PAGE_SCENE_TAG_NAME_RES_ID_LIST_NEW:[I

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final getDocTypeByRes(I)Ljava/lang/Integer;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/pagescene/PageSceneUtil;->Companion:Lcom/intsig/camscanner/scanner/pagescene/PageSceneUtil$Companion;

    .line 2
    .line 3
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/scanner/pagescene/PageSceneUtil$Companion;->getDocTypeByRes(I)Ljava/lang/Integer;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    return-object p0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method

.method public static final needDetect()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/pagescene/PageSceneUtil;->Companion:Lcom/intsig/camscanner/scanner/pagescene/PageSceneUtil$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/scanner/pagescene/PageSceneUtil$Companion;->needDetect()Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

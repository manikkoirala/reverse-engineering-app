.class public final Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult$Companion;
.super Ljava/lang/Object;
.source "PageSceneResult.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult$Companion;-><init>()V

    return-void
.end method

.method private final getErrorInstance()Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;->access$getErrorInstance$delegate$cp()Lkotlin/Lazy;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;

    .line 10
    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getNoNeedInstance()Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;->access$getNoNeedInstance$delegate$cp()Lkotlin/Lazy;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;

    .line 10
    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getWaitingInstance()Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;->access$getWaitingInstance$delegate$cp()Lkotlin/Lazy;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    check-cast v0, Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;

    .line 10
    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method


# virtual methods
.method public final requireErrorInstance()Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult$Companion;->getErrorInstance()Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final requireNoNeedInstance()Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult$Companion;->getNoNeedInstance()Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final requireWaitingInstance()Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult$Companion;->getWaitingInstance()Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

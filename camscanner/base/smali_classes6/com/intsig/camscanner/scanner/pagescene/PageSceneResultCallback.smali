.class public interface abstract Lcom/intsig/camscanner/scanner/pagescene/PageSceneResultCallback;
.super Ljava/lang/Object;
.source "PageSceneResultCallback.kt"

# interfaces
.implements Lcom/intsig/callback/Callback;


# annotations
.annotation runtime Lcom/intsig/okbinder/AIDL;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/intsig/callback/Callback<",
        "Lcom/intsig/camscanner/scanner/pagescene/PageSceneResult;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# virtual methods
.method public abstract synthetic call(Ljava/lang/Object;)V
    .param p1    # Ljava/lang/Object;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
.end method

.method public abstract getDocSyncId()Ljava/lang/String;
.end method

.method public abstract isCreatingDoc()Z
.end method

.class public final Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar;
.super Ljava/lang/Object;
.source "GalleryScanRadar.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;,
        Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final Companion:Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "GalleryScanRadar"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final localScannedImageMap$delegate:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final radarRecorder:Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar;->Companion:Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public constructor <init>()V
    .locals 13

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v12, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;

    .line 5
    .line 6
    const-wide/16 v1, 0x0

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const-wide/16 v4, 0x0

    .line 10
    .line 11
    const/4 v6, 0x0

    .line 12
    const-wide/16 v7, 0x0

    .line 13
    .line 14
    const/4 v9, 0x0

    .line 15
    const/16 v10, 0x3f

    .line 16
    .line 17
    const/4 v11, 0x0

    .line 18
    move-object v0, v12

    .line 19
    invoke-direct/range {v0 .. v11}, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;-><init>(JIJIJIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 20
    .line 21
    .line 22
    iput-object v12, p0, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar;->radarRecorder:Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;

    .line 23
    .line 24
    sget-object v0, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$localScannedImageMap$2;->INSTANCE:Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$localScannedImageMap$2;

    .line 25
    .line 26
    invoke-static {v0}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    iput-object v0, p0, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar;->localScannedImageMap$delegate:Lkotlin/Lazy;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method


# virtual methods
.method public final getLocalScannedImageMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lcom/intsig/camscanner/gallery/GallerySelectedItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar;->localScannedImageMap$delegate:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/util/Map;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final getTotalCount()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar;->radarRecorder:Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;->getTotalCount()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public final releaseRadar(I)V
    .locals 2

    .line 1
    if-ltz p1, :cond_0

    .line 2
    .line 3
    sget-object v0, Lcom/intsig/camscanner/booksplitter/Util/SessionIdPoolManager;->〇O〇:Lcom/intsig/camscanner/booksplitter/Util/SessionIdPoolManager$Companion;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/booksplitter/Util/SessionIdPoolManager$Companion;->〇080()Lcom/intsig/camscanner/booksplitter/Util/SessionIdPoolManager;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/booksplitter/Util/SessionIdPoolManager;->〇oOO8O8(I)V

    .line 10
    .line 11
    .line 12
    new-instance v0, Ljava/lang/StringBuilder;

    .line 13
    .line 14
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 15
    .line 16
    .line 17
    const-string v1, "released radar sessionId == "

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    const-string v0, "GalleryScanRadar"

    .line 30
    .line 31
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    :cond_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
.end method

.method public final scanRadarForSpecificImage(Lcom/intsig/camscanner/gallery/GallerySelectedItem;I)Z
    .locals 13
    .param p1    # Lcom/intsig/camscanner/gallery/GallerySelectedItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/WorkerThread;
    .end annotation

    .line 1
    const-string v0, "image"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;->getPath()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;->getId()J

    .line 11
    .line 12
    .line 13
    move-result-wide v1

    .line 14
    const/4 v3, 0x0

    .line 15
    const/4 v4, 0x1

    .line 16
    if-eqz v0, :cond_1

    .line 17
    .line 18
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 19
    .line 20
    .line 21
    move-result v5

    .line 22
    if-nez v5, :cond_0

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 v5, 0x0

    .line 26
    goto :goto_1

    .line 27
    :cond_1
    :goto_0
    const/4 v5, 0x1

    .line 28
    :goto_1
    const-string v6, "GalleryScanRadar"

    .line 29
    .line 30
    if-eqz v5, :cond_2

    .line 31
    .line 32
    new-instance p1, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    const-string p2, "scanGalleryRadar but path="

    .line 38
    .line 39
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    const-string p2, "; continue"

    .line 46
    .line 47
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object p1

    .line 54
    invoke-static {v6, p1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    return v3

    .line 58
    :cond_2
    invoke-static {v0}, Lcom/intsig/camscanner/gallery/CustomGalleryUtil;->Oo08(Ljava/lang/String;)Z

    .line 59
    .line 60
    .line 61
    move-result v5

    .line 62
    iget-object v7, p0, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar;->radarRecorder:Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;

    .line 63
    .line 64
    invoke-virtual {v7}, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;->getPtrCount()I

    .line 65
    .line 66
    .line 67
    move-result v7

    .line 68
    iget-object v8, p0, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar;->radarRecorder:Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;

    .line 69
    .line 70
    invoke-virtual {v8}, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;->getPathCount()I

    .line 71
    .line 72
    .line 73
    move-result v8

    .line 74
    add-int/2addr v7, v8

    .line 75
    new-instance v8, Ljava/lang/StringBuilder;

    .line 76
    .line 77
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 78
    .line 79
    .line 80
    const-string v9, "scanRadarForSpecific path = "

    .line 81
    .line 82
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    const-string v9, "; needDecode="

    .line 89
    .line 90
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    .line 92
    .line 93
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 94
    .line 95
    .line 96
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object v8

    .line 100
    invoke-static {v6, v8}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    .line 102
    .line 103
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 104
    .line 105
    .line 106
    move-result-wide v8

    .line 107
    const-string v10, ", index="

    .line 108
    .line 109
    if-eqz v5, :cond_3

    .line 110
    .line 111
    new-instance v11, Ljava/lang/StringBuilder;

    .line 112
    .line 113
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 114
    .line 115
    .line 116
    const-string v12, "scanRadarForSpecificImage - A - 1\u51c6\u5907\u8bb0\u5f55ptr\u6570\u91cf path="

    .line 117
    .line 118
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    .line 120
    .line 121
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    .line 123
    .line 124
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    .line 126
    .line 127
    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 128
    .line 129
    .line 130
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 131
    .line 132
    .line 133
    move-result-object v7

    .line 134
    invoke-static {v6, v7}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    .line 136
    .line 137
    iget-object v7, p0, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar;->radarRecorder:Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;

    .line 138
    .line 139
    invoke-virtual {v7}, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;->getPtrCount()I

    .line 140
    .line 141
    .line 142
    move-result v10

    .line 143
    add-int/2addr v10, v4

    .line 144
    invoke-virtual {v7, v10}, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;->setPtrCount(I)V

    .line 145
    .line 146
    .line 147
    const-string v7, "scanRadarForSpecificImage - A - 2\u51c6\u5907\u89e3\u7801"

    .line 148
    .line 149
    invoke-static {v6, v7}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    .line 151
    .line 152
    invoke-static {v0}, Lcom/intsig/camscanner/scanner/ScannerUtils;->decodeImageS(Ljava/lang/String;)I

    .line 153
    .line 154
    .line 155
    move-result v7

    .line 156
    new-instance v10, Ljava/lang/StringBuilder;

    .line 157
    .line 158
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 159
    .line 160
    .line 161
    const-string v11, "scanRadarForSpecificImage - A - 3\u51c6\u5907\u8bc6\u522bstruct-imageStruct="

    .line 162
    .line 163
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    .line 165
    .line 166
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 167
    .line 168
    .line 169
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 170
    .line 171
    .line 172
    move-result-object v10

    .line 173
    invoke-static {v6, v10}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    .line 175
    .line 176
    invoke-static {p2, v7}, Lcom/intsig/camscanner/scanner/ScannerUtils;->radarClassifyImageStruct(II)F

    .line 177
    .line 178
    .line 179
    move-result p2

    .line 180
    new-instance v10, Ljava/lang/StringBuilder;

    .line 181
    .line 182
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 183
    .line 184
    .line 185
    const-string v11, "scanRadarForSpecificImage - A - 4\u5b8c\u6210\u8bc6\u522b r="

    .line 186
    .line 187
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    .line 189
    .line 190
    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 191
    .line 192
    .line 193
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 194
    .line 195
    .line 196
    move-result-object v10

    .line 197
    invoke-static {v6, v10}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    .line 199
    .line 200
    invoke-static {v7}, Lcom/intsig/camscanner/scanner/ScannerUtils;->releaseStruct(I)V

    .line 201
    .line 202
    .line 203
    goto/16 :goto_2

    .line 204
    .line 205
    :cond_3
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇o〇Oo0()I

    .line 206
    .line 207
    .line 208
    move-result v11

    .line 209
    const/4 v12, 0x2

    .line 210
    if-ne v11, v12, :cond_4

    .line 211
    .line 212
    new-instance v11, Ljava/lang/StringBuilder;

    .line 213
    .line 214
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 215
    .line 216
    .line 217
    const-string v12, "scanRadarForSpecificImage - B - 1\u51c6\u5907\u8bb0\u5f55path\u6570\u91cf path="

    .line 218
    .line 219
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    .line 221
    .line 222
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    .line 224
    .line 225
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    .line 227
    .line 228
    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 229
    .line 230
    .line 231
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 232
    .line 233
    .line 234
    move-result-object v7

    .line 235
    invoke-static {v6, v7}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    .line 237
    .line 238
    iget-object v7, p0, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar;->radarRecorder:Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;

    .line 239
    .line 240
    invoke-virtual {v7}, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;->getPathCount()I

    .line 241
    .line 242
    .line 243
    move-result v10

    .line 244
    add-int/2addr v10, v4

    .line 245
    invoke-virtual {v7, v10}, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;->setPathCount(I)V

    .line 246
    .line 247
    .line 248
    new-instance v7, Ljava/lang/StringBuilder;

    .line 249
    .line 250
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 251
    .line 252
    .line 253
    const-string v10, "scanRadarForSpecificImage - B - 2\u51c6\u5907\u8bc6\u522bpath-path="

    .line 254
    .line 255
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    .line 257
    .line 258
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    .line 260
    .line 261
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 262
    .line 263
    .line 264
    move-result-object v7

    .line 265
    invoke-static {v6, v7}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    .line 267
    .line 268
    invoke-static {p2, v0}, Lcom/intsig/camscanner/scanner/ScannerUtils;->radarClassifyImageFile(ILjava/lang/String;)F

    .line 269
    .line 270
    .line 271
    move-result p2

    .line 272
    new-instance v7, Ljava/lang/StringBuilder;

    .line 273
    .line 274
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 275
    .line 276
    .line 277
    const-string v10, "scanRadarForSpecificImage - B - 4\u5b8c\u6210\u8bc6\u522b r="

    .line 278
    .line 279
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 280
    .line 281
    .line 282
    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 283
    .line 284
    .line 285
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 286
    .line 287
    .line 288
    move-result-object v7

    .line 289
    invoke-static {v6, v7}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    .line 291
    .line 292
    goto :goto_2

    .line 293
    :cond_4
    new-instance v11, Ljava/lang/StringBuilder;

    .line 294
    .line 295
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 296
    .line 297
    .line 298
    const-string v12, "scanRadarForSpecificImage - C - 1\u51c6\u5907\u8bb0\u5f55ptr\u6570\u91cf path="

    .line 299
    .line 300
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301
    .line 302
    .line 303
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 304
    .line 305
    .line 306
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 307
    .line 308
    .line 309
    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 310
    .line 311
    .line 312
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 313
    .line 314
    .line 315
    move-result-object v7

    .line 316
    invoke-static {v6, v7}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    .line 318
    .line 319
    iget-object v7, p0, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar;->radarRecorder:Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;

    .line 320
    .line 321
    invoke-virtual {v7}, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;->getPtrCount()I

    .line 322
    .line 323
    .line 324
    move-result v10

    .line 325
    add-int/2addr v10, v4

    .line 326
    invoke-virtual {v7, v10}, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;->setPtrCount(I)V

    .line 327
    .line 328
    .line 329
    const-string v7, "scanRadarForSpecificImage - C - 2\u51c6\u5907\u89e3\u7801"

    .line 330
    .line 331
    invoke-static {v6, v7}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    .line 333
    .line 334
    invoke-static {v0}, Lcom/intsig/camscanner/scanner/ScannerUtils;->decodeImageS(Ljava/lang/String;)I

    .line 335
    .line 336
    .line 337
    move-result v7

    .line 338
    new-instance v10, Ljava/lang/StringBuilder;

    .line 339
    .line 340
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 341
    .line 342
    .line 343
    const-string v11, "scanRadarForSpecificImage - C - 3\u51c6\u5907\u8bc6\u522bstruct-imageStruct="

    .line 344
    .line 345
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    .line 347
    .line 348
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 349
    .line 350
    .line 351
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 352
    .line 353
    .line 354
    move-result-object v10

    .line 355
    invoke-static {v6, v10}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    .line 357
    .line 358
    invoke-static {p2, v7}, Lcom/intsig/camscanner/scanner/ScannerUtils;->radarClassifyImageStruct(II)F

    .line 359
    .line 360
    .line 361
    move-result p2

    .line 362
    new-instance v10, Ljava/lang/StringBuilder;

    .line 363
    .line 364
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 365
    .line 366
    .line 367
    const-string v11, "scanRadarForSpecificImage - C - 4\u5b8c\u6210\u8bc6\u522b r="

    .line 368
    .line 369
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 370
    .line 371
    .line 372
    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 373
    .line 374
    .line 375
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 376
    .line 377
    .line 378
    move-result-object v10

    .line 379
    invoke-static {v6, v10}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    .line 381
    .line 382
    invoke-static {v7}, Lcom/intsig/camscanner/scanner/ScannerUtils;->releaseStruct(I)V

    .line 383
    .line 384
    .line 385
    :goto_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 386
    .line 387
    .line 388
    move-result-wide v10

    .line 389
    sub-long/2addr v10, v8

    .line 390
    if-eqz v5, :cond_5

    .line 391
    .line 392
    iget-object v5, p0, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar;->radarRecorder:Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;

    .line 393
    .line 394
    invoke-virtual {v5}, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;->getPtrCost()J

    .line 395
    .line 396
    .line 397
    move-result-wide v7

    .line 398
    add-long/2addr v7, v10

    .line 399
    invoke-virtual {v5, v7, v8}, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;->setPtrCost(J)V

    .line 400
    .line 401
    .line 402
    goto :goto_3

    .line 403
    :cond_5
    iget-object v5, p0, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar;->radarRecorder:Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;

    .line 404
    .line 405
    invoke-virtual {v5}, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;->getPathCost()J

    .line 406
    .line 407
    .line 408
    move-result-wide v7

    .line 409
    add-long/2addr v7, v10

    .line 410
    invoke-virtual {v5, v7, v8}, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar$GalleryRadarRecorder;->setPathCost(J)V

    .line 411
    .line 412
    .line 413
    :goto_3
    new-instance v5, Ljava/lang/StringBuilder;

    .line 414
    .line 415
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 416
    .line 417
    .line 418
    const-string v7, "scanGalleryRadar - path="

    .line 419
    .line 420
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 421
    .line 422
    .line 423
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 424
    .line 425
    .line 426
    const-string v7, "; res="

    .line 427
    .line 428
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 429
    .line 430
    .line 431
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 432
    .line 433
    .line 434
    const-string v7, "; imageId="

    .line 435
    .line 436
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 437
    .line 438
    .line 439
    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 440
    .line 441
    .line 442
    const-string v1, "; singleImageTime="

    .line 443
    .line 444
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 445
    .line 446
    .line 447
    invoke-virtual {v5, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 448
    .line 449
    .line 450
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 451
    .line 452
    .line 453
    move-result-object v1

    .line 454
    invoke-static {v6, v1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    .line 456
    .line 457
    const v1, 0x3f4ccccd    # 0.8f

    .line 458
    .line 459
    .line 460
    cmpl-float p2, p2, v1

    .line 461
    .line 462
    if-lez p2, :cond_6

    .line 463
    .line 464
    const/4 v3, 0x1

    .line 465
    :cond_6
    new-instance p2, Landroid/content/ContentValues;

    .line 466
    .line 467
    invoke-direct {p2}, Landroid/content/ContentValues;-><init>()V

    .line 468
    .line 469
    .line 470
    const-string v1, "local_path"

    .line 471
    .line 472
    invoke-virtual {p2, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    .line 474
    .line 475
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;->getModifiedDate()J

    .line 476
    .line 477
    .line 478
    move-result-wide v1

    .line 479
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 480
    .line 481
    .line 482
    move-result-object v1

    .line 483
    const-string v2, "modified_date"

    .line 484
    .line 485
    invoke-virtual {p2, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 486
    .line 487
    .line 488
    const-string v1, "image_type"

    .line 489
    .line 490
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 491
    .line 492
    .line 493
    move-result-object v2

    .line 494
    invoke-virtual {p2, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 495
    .line 496
    .line 497
    invoke-virtual {p1, v3}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;->setImageType(I)V

    .line 498
    .line 499
    .line 500
    invoke-virtual {p0}, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar;->getLocalScannedImageMap()Ljava/util/Map;

    .line 501
    .line 502
    .line 503
    move-result-object v1

    .line 504
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 505
    .line 506
    .line 507
    move-result-object v1

    .line 508
    check-cast v1, Lcom/intsig/camscanner/gallery/GallerySelectedItem;

    .line 509
    .line 510
    if-nez v1, :cond_7

    .line 511
    .line 512
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 513
    .line 514
    .line 515
    move-result-object v1

    .line 516
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 517
    .line 518
    .line 519
    move-result-object v1

    .line 520
    sget-object v2, Lcom/intsig/camscanner/provider/Documents$GalleryRadar;->〇080:Landroid/net/Uri;

    .line 521
    .line 522
    invoke-virtual {v1, v2, p2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 523
    .line 524
    .line 525
    invoke-virtual {p0}, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar;->getLocalScannedImageMap()Ljava/util/Map;

    .line 526
    .line 527
    .line 528
    move-result-object p2

    .line 529
    invoke-interface {p2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 530
    .line 531
    .line 532
    goto :goto_4

    .line 533
    :cond_7
    invoke-virtual {v1}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;->getModifiedDate()J

    .line 534
    .line 535
    .line 536
    move-result-wide v1

    .line 537
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;->getModifiedDate()J

    .line 538
    .line 539
    .line 540
    move-result-wide v4

    .line 541
    cmp-long v6, v1, v4

    .line 542
    .line 543
    if-eqz v6, :cond_8

    .line 544
    .line 545
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 546
    .line 547
    .line 548
    move-result-object v1

    .line 549
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 550
    .line 551
    .line 552
    move-result-object v1

    .line 553
    sget-object v2, Lcom/intsig/camscanner/provider/Documents$GalleryRadar;->〇080:Landroid/net/Uri;

    .line 554
    .line 555
    invoke-virtual {p1}, Lcom/intsig/camscanner/gallery/GallerySelectedItem;->getModifiedDate()J

    .line 556
    .line 557
    .line 558
    move-result-wide v4

    .line 559
    new-instance v6, Ljava/lang/StringBuilder;

    .line 560
    .line 561
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 562
    .line 563
    .line 564
    const-string v7, "modified_date = "

    .line 565
    .line 566
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 567
    .line 568
    .line 569
    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 570
    .line 571
    .line 572
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 573
    .line 574
    .line 575
    move-result-object v4

    .line 576
    const/4 v5, 0x0

    .line 577
    invoke-virtual {v1, v2, p2, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 578
    .line 579
    .line 580
    invoke-virtual {p0}, Lcom/intsig/camscanner/scanner/galleryradar/GalleryScanRadar;->getLocalScannedImageMap()Ljava/util/Map;

    .line 581
    .line 582
    .line 583
    move-result-object p2

    .line 584
    invoke-interface {p2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 585
    .line 586
    .line 587
    :cond_8
    :goto_4
    return v3
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
.end method

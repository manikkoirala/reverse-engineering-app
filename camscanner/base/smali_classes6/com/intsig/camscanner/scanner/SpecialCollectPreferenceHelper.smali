.class public final Lcom/intsig/camscanner/scanner/SpecialCollectPreferenceHelper;
.super Lcom/intsig/camscanner/util/AbstractPreferenceHelper;
.source "SpecialCollectPreferenceHelper.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final INSTANCE:Lcom/intsig/camscanner/scanner/SpecialCollectPreferenceHelper;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final KEY_CURRENT_SUPER_FILTER_UPLOAD_DATE_TIME:Ljava/lang/String; = "key_current_super_filter_upload_date_time"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final KEY_SPECIAL_COLLECT:Ljava/lang/String; = "special_collect_helper_"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final KEY_TODAY_SUPER_FILTER_UPLOAD_PAGE:Ljava/lang/String; = "key_today_super_filter_upload_page"
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/scanner/SpecialCollectPreferenceHelper;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/scanner/SpecialCollectPreferenceHelper;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/scanner/SpecialCollectPreferenceHelper;->INSTANCE:Lcom/intsig/camscanner/scanner/SpecialCollectPreferenceHelper;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/util/AbstractPreferenceHelper;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method private final getCurrentSuperFilterUploadDate()J
    .locals 3

    .line 1
    const-string v0, "key_current_super_filter_upload_date_time"

    .line 2
    .line 3
    const-wide/16 v1, 0x0

    .line 4
    .line 5
    invoke-virtual {p0, v0, v1, v2}, Lcom/intsig/camscanner/util/AbstractPreferenceHelper;->getLong(Ljava/lang/String;J)J

    .line 6
    .line 7
    .line 8
    move-result-wide v0

    .line 9
    return-wide v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final getTodaySuperFilterUploadPage()I
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/SpecialCollectPreferenceHelper;->INSTANCE:Lcom/intsig/camscanner/scanner/SpecialCollectPreferenceHelper;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/scanner/SpecialCollectPreferenceHelper;->getCurrentSuperFilterUploadDate()J

    .line 4
    .line 5
    .line 6
    move-result-wide v1

    .line 7
    invoke-static {v1, v2}, Lcom/intsig/utils/DateTimeUtil;->OoO8(J)Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    const/4 v2, 0x0

    .line 12
    if-eqz v1, :cond_0

    .line 13
    .line 14
    const-string v1, "key_today_super_filter_upload_page"

    .line 15
    .line 16
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/util/AbstractPreferenceHelper;->getInt(Ljava/lang/String;I)I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    return v0

    .line 21
    :cond_0
    invoke-static {v2}, Lcom/intsig/camscanner/scanner/SpecialCollectPreferenceHelper;->setTodaySuperFilterUploadPage(I)V

    .line 22
    .line 23
    .line 24
    invoke-direct {v0}, Lcom/intsig/camscanner/scanner/SpecialCollectPreferenceHelper;->setCurrentSuperFilterUploadDate()V

    .line 25
    .line 26
    .line 27
    return v2
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
.end method

.method private final setCurrentSuperFilterUploadDate()V
    .locals 3

    .line 1
    const-string v0, "key_current_super_filter_upload_date_time"

    .line 2
    .line 3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 4
    .line 5
    .line 6
    move-result-wide v1

    .line 7
    invoke-virtual {p0, v0, v1, v2}, Lcom/intsig/camscanner/util/AbstractPreferenceHelper;->putLong(Ljava/lang/String;J)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

.method public static final setTodaySuperFilterUploadPage(I)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scanner/SpecialCollectPreferenceHelper;->INSTANCE:Lcom/intsig/camscanner/scanner/SpecialCollectPreferenceHelper;

    .line 2
    .line 3
    const-string v1, "key_today_super_filter_upload_page"

    .line 4
    .line 5
    invoke-virtual {v0, v1, p0}, Lcom/intsig/camscanner/util/AbstractPreferenceHelper;->putInt(Ljava/lang/String;I)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
.end method


# virtual methods
.method public getPrefixString()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    const-string v0, "special_collect_helper_"

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
.end method

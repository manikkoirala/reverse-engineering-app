.class public final enum Lcom/intsig/advertisement/enums/FunctionModel;
.super Ljava/lang/Enum;
.source "FunctionModel.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/advertisement/enums/FunctionModel;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/advertisement/enums/FunctionModel;

.field public static final enum card_model:Lcom/intsig/advertisement/enums/FunctionModel;

.field public static final enum jigsaw:Lcom/intsig/advertisement/enums/FunctionModel;

.field public static final enum watermark_pdf:Lcom/intsig/advertisement/enums/FunctionModel;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 1
    new-instance v0, Lcom/intsig/advertisement/enums/FunctionModel;

    .line 2
    .line 3
    const-string/jumbo v1, "watermark_pdf"

    .line 4
    .line 5
    .line 6
    const/4 v2, 0x0

    .line 7
    invoke-direct {v0, v1, v2}, Lcom/intsig/advertisement/enums/FunctionModel;-><init>(Ljava/lang/String;I)V

    .line 8
    .line 9
    .line 10
    sput-object v0, Lcom/intsig/advertisement/enums/FunctionModel;->watermark_pdf:Lcom/intsig/advertisement/enums/FunctionModel;

    .line 11
    .line 12
    new-instance v1, Lcom/intsig/advertisement/enums/FunctionModel;

    .line 13
    .line 14
    const-string v3, "jigsaw"

    .line 15
    .line 16
    const/4 v4, 0x1

    .line 17
    invoke-direct {v1, v3, v4}, Lcom/intsig/advertisement/enums/FunctionModel;-><init>(Ljava/lang/String;I)V

    .line 18
    .line 19
    .line 20
    sput-object v1, Lcom/intsig/advertisement/enums/FunctionModel;->jigsaw:Lcom/intsig/advertisement/enums/FunctionModel;

    .line 21
    .line 22
    new-instance v3, Lcom/intsig/advertisement/enums/FunctionModel;

    .line 23
    .line 24
    const-string v5, "card_model"

    .line 25
    .line 26
    const/4 v6, 0x2

    .line 27
    invoke-direct {v3, v5, v6}, Lcom/intsig/advertisement/enums/FunctionModel;-><init>(Ljava/lang/String;I)V

    .line 28
    .line 29
    .line 30
    sput-object v3, Lcom/intsig/advertisement/enums/FunctionModel;->card_model:Lcom/intsig/advertisement/enums/FunctionModel;

    .line 31
    .line 32
    const/4 v5, 0x3

    .line 33
    new-array v5, v5, [Lcom/intsig/advertisement/enums/FunctionModel;

    .line 34
    .line 35
    aput-object v0, v5, v2

    .line 36
    .line 37
    aput-object v1, v5, v4

    .line 38
    .line 39
    aput-object v3, v5, v6

    .line 40
    .line 41
    sput-object v5, Lcom/intsig/advertisement/enums/FunctionModel;->$VALUES:[Lcom/intsig/advertisement/enums/FunctionModel;

    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/advertisement/enums/FunctionModel;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/advertisement/enums/FunctionModel;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/advertisement/enums/FunctionModel;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static values()[Lcom/intsig/advertisement/enums/FunctionModel;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/advertisement/enums/FunctionModel;->$VALUES:[Lcom/intsig/advertisement/enums/FunctionModel;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/intsig/advertisement/enums/FunctionModel;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/advertisement/enums/FunctionModel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

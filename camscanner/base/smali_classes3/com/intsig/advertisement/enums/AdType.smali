.class public final enum Lcom/intsig/advertisement/enums/AdType;
.super Ljava/lang/Enum;
.source "AdType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/advertisement/enums/AdType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/advertisement/enums/AdType;

.field public static final enum Banner:Lcom/intsig/advertisement/enums/AdType;

.field public static final enum Interstitial:Lcom/intsig/advertisement/enums/AdType;

.field public static final enum Native:Lcom/intsig/advertisement/enums/AdType;

.field public static final enum RewardInters:Lcom/intsig/advertisement/enums/AdType;

.field public static final enum RewardVideo:Lcom/intsig/advertisement/enums/AdType;

.field public static final enum Splash:Lcom/intsig/advertisement/enums/AdType;

.field public static final enum UnknownType:Lcom/intsig/advertisement/enums/AdType;


# instance fields
.field public description:Ljava/lang/String;

.field public value:I


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1
    new-instance v0, Lcom/intsig/advertisement/enums/AdType;

    .line 2
    .line 3
    const-string v1, "native"

    .line 4
    .line 5
    const-string v2, "Native"

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    const/4 v4, 0x1

    .line 9
    invoke-direct {v0, v2, v3, v4, v1}, Lcom/intsig/advertisement/enums/AdType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 10
    .line 11
    .line 12
    sput-object v0, Lcom/intsig/advertisement/enums/AdType;->Native:Lcom/intsig/advertisement/enums/AdType;

    .line 13
    .line 14
    new-instance v1, Lcom/intsig/advertisement/enums/AdType;

    .line 15
    .line 16
    const-string v2, "interstitial"

    .line 17
    .line 18
    const-string v5, "Interstitial"

    .line 19
    .line 20
    const/4 v6, 0x2

    .line 21
    invoke-direct {v1, v5, v4, v6, v2}, Lcom/intsig/advertisement/enums/AdType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 22
    .line 23
    .line 24
    sput-object v1, Lcom/intsig/advertisement/enums/AdType;->Interstitial:Lcom/intsig/advertisement/enums/AdType;

    .line 25
    .line 26
    new-instance v2, Lcom/intsig/advertisement/enums/AdType;

    .line 27
    .line 28
    const-string/jumbo v5, "splash"

    .line 29
    .line 30
    .line 31
    const-string v7, "Splash"

    .line 32
    .line 33
    const/4 v8, 0x3

    .line 34
    invoke-direct {v2, v7, v6, v8, v5}, Lcom/intsig/advertisement/enums/AdType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 35
    .line 36
    .line 37
    sput-object v2, Lcom/intsig/advertisement/enums/AdType;->Splash:Lcom/intsig/advertisement/enums/AdType;

    .line 38
    .line 39
    new-instance v5, Lcom/intsig/advertisement/enums/AdType;

    .line 40
    .line 41
    const-string v7, "rewardvideo"

    .line 42
    .line 43
    const-string v9, "RewardVideo"

    .line 44
    .line 45
    const/4 v10, 0x4

    .line 46
    invoke-direct {v5, v9, v8, v10, v7}, Lcom/intsig/advertisement/enums/AdType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 47
    .line 48
    .line 49
    sput-object v5, Lcom/intsig/advertisement/enums/AdType;->RewardVideo:Lcom/intsig/advertisement/enums/AdType;

    .line 50
    .line 51
    new-instance v7, Lcom/intsig/advertisement/enums/AdType;

    .line 52
    .line 53
    const-string v9, "rewardinters"

    .line 54
    .line 55
    const-string v11, "RewardInters"

    .line 56
    .line 57
    const/4 v12, 0x7

    .line 58
    invoke-direct {v7, v11, v10, v12, v9}, Lcom/intsig/advertisement/enums/AdType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 59
    .line 60
    .line 61
    sput-object v7, Lcom/intsig/advertisement/enums/AdType;->RewardInters:Lcom/intsig/advertisement/enums/AdType;

    .line 62
    .line 63
    new-instance v9, Lcom/intsig/advertisement/enums/AdType;

    .line 64
    .line 65
    const-string v11, "banner"

    .line 66
    .line 67
    const-string v13, "Banner"

    .line 68
    .line 69
    const/4 v14, 0x5

    .line 70
    invoke-direct {v9, v13, v14, v14, v11}, Lcom/intsig/advertisement/enums/AdType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 71
    .line 72
    .line 73
    sput-object v9, Lcom/intsig/advertisement/enums/AdType;->Banner:Lcom/intsig/advertisement/enums/AdType;

    .line 74
    .line 75
    new-instance v11, Lcom/intsig/advertisement/enums/AdType;

    .line 76
    .line 77
    const-string/jumbo v13, "unknownSourceType"

    .line 78
    .line 79
    .line 80
    const-string v15, "UnknownType"

    .line 81
    .line 82
    const/4 v14, 0x6

    .line 83
    invoke-direct {v11, v15, v14, v14, v13}, Lcom/intsig/advertisement/enums/AdType;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    .line 84
    .line 85
    .line 86
    sput-object v11, Lcom/intsig/advertisement/enums/AdType;->UnknownType:Lcom/intsig/advertisement/enums/AdType;

    .line 87
    .line 88
    new-array v12, v12, [Lcom/intsig/advertisement/enums/AdType;

    .line 89
    .line 90
    aput-object v0, v12, v3

    .line 91
    .line 92
    aput-object v1, v12, v4

    .line 93
    .line 94
    aput-object v2, v12, v6

    .line 95
    .line 96
    aput-object v5, v12, v8

    .line 97
    .line 98
    aput-object v7, v12, v10

    .line 99
    .line 100
    const/4 v0, 0x5

    .line 101
    aput-object v9, v12, v0

    .line 102
    .line 103
    aput-object v11, v12, v14

    .line 104
    .line 105
    sput-object v12, Lcom/intsig/advertisement/enums/AdType;->$VALUES:[Lcom/intsig/advertisement/enums/AdType;

    .line 106
    .line 107
    return-void
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput p3, p0, Lcom/intsig/advertisement/enums/AdType;->value:I

    .line 5
    .line 6
    iput-object p4, p0, Lcom/intsig/advertisement/enums/AdType;->description:Ljava/lang/String;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/advertisement/enums/AdType;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/advertisement/enums/AdType;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/advertisement/enums/AdType;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static values()[Lcom/intsig/advertisement/enums/AdType;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/advertisement/enums/AdType;->$VALUES:[Lcom/intsig/advertisement/enums/AdType;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/intsig/advertisement/enums/AdType;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/advertisement/enums/AdType;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

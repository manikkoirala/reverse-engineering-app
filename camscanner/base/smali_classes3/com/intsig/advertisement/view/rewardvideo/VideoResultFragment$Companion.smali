.class public final Lcom/intsig/advertisement/view/rewardvideo/VideoResultFragment$Companion;
.super Ljava/lang/Object;
.source "VideoResultFragment.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/advertisement/view/rewardvideo/VideoResultFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Companion"
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/advertisement/view/rewardvideo/VideoResultFragment$Companion;-><init>()V

    return-void
.end method


# virtual methods
.method public final 〇080(Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;)Lcom/intsig/advertisement/view/rewardvideo/VideoResultFragment;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/advertisement/view/rewardvideo/VideoResultFragment;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/advertisement/view/rewardvideo/VideoResultFragment;-><init>()V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    new-instance v1, Landroid/os/Bundle;

    .line 9
    .line 10
    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 11
    .line 12
    .line 13
    const-string/jumbo v2, "url_main_image"

    .line 14
    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getPic()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v3

    .line 20
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 21
    .line 22
    .line 23
    const-string/jumbo v2, "url_icon"

    .line 24
    .line 25
    .line 26
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getIcon_pic()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v3

    .line 30
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 31
    .line 32
    .line 33
    const-string/jumbo v2, "title"

    .line 34
    .line 35
    .line 36
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getTitle()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v3

    .line 40
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 41
    .line 42
    .line 43
    const-string/jumbo v2, "sub_title"

    .line 44
    .line 45
    .line 46
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getDescription()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v3

    .line 50
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 51
    .line 52
    .line 53
    const-string v2, "action_text"

    .line 54
    .line 55
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getBtn_text()Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object p1

    .line 59
    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v0, v1}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 63
    .line 64
    .line 65
    :cond_0
    return-object v0
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.class public final Lcom/intsig/advertisement/view/FullScreenITActivity;
.super Lcom/intsig/mvp/activity/BaseChangeActivity;
.source "FullScreenITActivity.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/advertisement/view/FullScreenITActivity$WhenMappings;,
        Lcom/intsig/advertisement/view/FullScreenITActivity$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final o8oOOo:Lcom/intsig/advertisement/view/FullScreenITActivity$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field static final synthetic 〇O〇〇O8:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private O0O:Lcom/intsig/advertisement/enums/PositionType;

.field private ooo0〇〇O:Lcom/intsig/advertisement/interfaces/RealRequestAbs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/advertisement/interfaces/RealRequestAbs<",
            "***>;"
        }
    .end annotation
.end field

.field private final 〇〇08O:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mViewBinding"

    .line 7
    .line 8
    const-string v3, "getMViewBinding()Lcom/intsig/advertisement/databinding/ActivityFullScreenAdLayoutBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/advertisement/view/FullScreenITActivity;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/advertisement/view/FullScreenITActivity;->〇O〇〇O8:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/advertisement/view/FullScreenITActivity$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/advertisement/view/FullScreenITActivity$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/advertisement/view/FullScreenITActivity;->o8oOOo:Lcom/intsig/advertisement/view/FullScreenITActivity$Companion;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/advertisement/databinding/ActivityFullScreenAdLayoutBinding;

    .line 7
    .line 8
    invoke-direct {v0, v1, p0}, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;-><init>(Ljava/lang/Class;Landroid/app/Activity;)V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/advertisement/view/FullScreenITActivity;->〇〇08O:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 12
    .line 13
    return-void
    .line 14
    .line 15
.end method

.method public static final synthetic O0〇(Lcom/intsig/advertisement/view/FullScreenITActivity;)Lcom/intsig/advertisement/interfaces/RealRequestAbs;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/advertisement/view/FullScreenITActivity;->ooo0〇〇O:Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static final O〇080〇o0(Landroid/app/Activity;Lcom/intsig/advertisement/enums/PositionType;)V
    .locals 1
    .param p0    # Landroid/app/Activity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p1    # Lcom/intsig/advertisement/enums/PositionType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    sget-object v0, Lcom/intsig/advertisement/view/FullScreenITActivity;->o8oOOo:Lcom/intsig/advertisement/view/FullScreenITActivity$Companion;

    .line 2
    .line 3
    invoke-virtual {v0, p0, p1}, Lcom/intsig/advertisement/view/FullScreenITActivity$Companion;->〇080(Landroid/app/Activity;Lcom/intsig/advertisement/enums/PositionType;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final O〇0O〇Oo〇o()Lcom/intsig/advertisement/databinding/ActivityFullScreenAdLayoutBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/view/FullScreenITActivity;->〇〇08O:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/advertisement/view/FullScreenITActivity;->〇O〇〇O8:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;->〇〇888(Landroid/app/Activity;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/advertisement/databinding/ActivityFullScreenAdLayoutBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
.end method

.method private final O〇〇O80o8()V
    .locals 6

    .line 1
    new-instance v0, Lcom/intsig/advertisement/view/FullScreenITActivity$renderInterstitial$listener$1;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/advertisement/view/FullScreenITActivity$renderInterstitial$listener$1;-><init>(Lcom/intsig/advertisement/view/FullScreenITActivity;)V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/advertisement/view/FullScreenITActivity;->ooo0〇〇O:Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    if-nez v1, :cond_0

    .line 10
    .line 11
    const-string v1, "requestAbs"

    .line 12
    .line 13
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    move-object v1, v2

    .line 17
    :cond_0
    iget-object v1, v1, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 18
    .line 19
    const-string v3, "null cannot be cast to non-null type com.intsig.advertisement.adapters.sources.api.sdk.bean.ApiAdBean"

    .line 20
    .line 21
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    check-cast v1, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 25
    .line 26
    new-instance v3, Lcom/intsig/advertisement/adapters/sources/api/sdk/ViewRender;

    .line 27
    .line 28
    iget-object v4, p0, Lcom/intsig/advertisement/view/FullScreenITActivity;->O0O:Lcom/intsig/advertisement/enums/PositionType;

    .line 29
    .line 30
    if-nez v4, :cond_1

    .line 31
    .line 32
    const-string v4, "positionType"

    .line 33
    .line 34
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    move-object v4, v2

    .line 38
    :cond_1
    invoke-direct {v3, v1, v4, v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/ViewRender;-><init>(Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;Lcom/intsig/advertisement/enums/PositionType;Lcom/intsig/advertisement/listener/OnAdShowListener;)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {v3}, Lcom/intsig/advertisement/adapters/sources/api/sdk/ViewRender;->〇〇8O0〇8()Z

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    if-eqz v0, :cond_2

    .line 46
    .line 47
    invoke-direct {p0}, Lcom/intsig/advertisement/view/FullScreenITActivity;->O〇0O〇Oo〇o()Lcom/intsig/advertisement/databinding/ActivityFullScreenAdLayoutBinding;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    if-eqz v0, :cond_3

    .line 52
    .line 53
    iget-object v0, v0, Lcom/intsig/advertisement/databinding/ActivityFullScreenAdLayoutBinding;->〇OOo8〇0:Landroid/widget/FrameLayout;

    .line 54
    .line 55
    if-eqz v0, :cond_3

    .line 56
    .line 57
    invoke-static {p0}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 58
    .line 59
    .line 60
    move-result v4

    .line 61
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getHeight()I

    .line 62
    .line 63
    .line 64
    move-result v5

    .line 65
    mul-int v5, v5, v4

    .line 66
    .line 67
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getWidth()I

    .line 68
    .line 69
    .line 70
    move-result v1

    .line 71
    div-int/2addr v5, v1

    .line 72
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 73
    .line 74
    .line 75
    move-result-object v1

    .line 76
    iput v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 77
    .line 78
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 79
    .line 80
    .line 81
    move-result-object v1

    .line 82
    iput v5, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 83
    .line 84
    invoke-virtual {v3, p0, v0, v2}, Lcom/intsig/advertisement/adapters/sources/api/sdk/ViewRender;->o800o8O(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/intsig/advertisement/view/NativeViewHolder;)Z

    .line 85
    .line 86
    .line 87
    goto :goto_0

    .line 88
    :cond_2
    new-instance v0, Lcom/intsig/advertisement/view/NativeViewHolder;

    .line 89
    .line 90
    sget v1, Lcom/intsig/advertisement/R$layout;->single_rv_container:I

    .line 91
    .line 92
    invoke-direct {v0, p0, v1}, Lcom/intsig/advertisement/view/NativeViewHolder;-><init>(Landroid/content/Context;I)V

    .line 93
    .line 94
    .line 95
    sget v1, Lcom/intsig/advertisement/R$id;->rv_main_view_container:I

    .line 96
    .line 97
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/view/NativeViewHolder;->Oo08(I)V

    .line 98
    .line 99
    .line 100
    invoke-direct {p0}, Lcom/intsig/advertisement/view/FullScreenITActivity;->O〇0O〇Oo〇o()Lcom/intsig/advertisement/databinding/ActivityFullScreenAdLayoutBinding;

    .line 101
    .line 102
    .line 103
    move-result-object v1

    .line 104
    if-eqz v1, :cond_3

    .line 105
    .line 106
    iget-object v1, v1, Lcom/intsig/advertisement/databinding/ActivityFullScreenAdLayoutBinding;->〇OOo8〇0:Landroid/widget/FrameLayout;

    .line 107
    .line 108
    if-eqz v1, :cond_3

    .line 109
    .line 110
    invoke-virtual {v3, p0, v1, v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/ViewRender;->o800o8O(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/intsig/advertisement/view/NativeViewHolder;)Z

    .line 111
    .line 112
    .line 113
    :cond_3
    :goto_0
    return-void
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method


# virtual methods
.method public bridge synthetic dealClickAction(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->〇080(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getResources()Landroid/content/res/Resources;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    sget v0, Lcom/intsig/advertisement/R$color;->transparent:I

    .line 6
    .line 7
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    const/4 v0, 0x1

    .line 12
    const/4 v1, 0x0

    .line 13
    invoke-static {p0, v0, v1, p1}, Lcom/intsig/utils/StatusBarUtil;->〇o00〇〇Oo(Landroid/app/Activity;ZZI)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    if-eqz p1, :cond_0

    .line 21
    .line 22
    const-string v2, "ad_data"

    .line 23
    .line 24
    invoke-virtual {p1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    const-string v2, "null cannot be cast to non-null type com.intsig.advertisement.enums.PositionType"

    .line 29
    .line 30
    invoke-static {p1, v2}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    check-cast p1, Lcom/intsig/advertisement/enums/PositionType;

    .line 34
    .line 35
    iput-object p1, p0, Lcom/intsig/advertisement/view/FullScreenITActivity;->O0O:Lcom/intsig/advertisement/enums/PositionType;

    .line 36
    .line 37
    :cond_0
    iget-object p1, p0, Lcom/intsig/advertisement/view/FullScreenITActivity;->O0O:Lcom/intsig/advertisement/enums/PositionType;

    .line 38
    .line 39
    const/4 v2, 0x0

    .line 40
    if-nez p1, :cond_1

    .line 41
    .line 42
    const-string p1, "positionType"

    .line 43
    .line 44
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    move-object p1, v2

    .line 48
    :cond_1
    sget-object v3, Lcom/intsig/advertisement/view/FullScreenITActivity$WhenMappings;->〇080:[I

    .line 49
    .line 50
    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    .line 51
    .line 52
    .line 53
    move-result p1

    .line 54
    aget p1, v3, p1

    .line 55
    .line 56
    if-eq p1, v0, :cond_3

    .line 57
    .line 58
    const/4 v3, 0x2

    .line 59
    if-eq p1, v3, :cond_2

    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_2
    sget-object p1, Lcom/intsig/advertisement/adapters/positions/ShotDoneManager;->OO0o〇〇:Lcom/intsig/advertisement/adapters/positions/ShotDoneManager$Companion;

    .line 63
    .line 64
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/positions/ShotDoneManager$Companion;->〇080()Lcom/intsig/advertisement/adapters/positions/ShotDoneManager;

    .line 65
    .line 66
    .line 67
    move-result-object p1

    .line 68
    invoke-virtual {p1, v1}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->OOO〇O0(I)Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 69
    .line 70
    .line 71
    move-result-object v2

    .line 72
    goto :goto_0

    .line 73
    :cond_3
    invoke-static {}, Lcom/intsig/advertisement/adapters/positions/ShareDoneManager;->o〇O()Lcom/intsig/advertisement/adapters/positions/ShareDoneManager;

    .line 74
    .line 75
    .line 76
    move-result-object p1

    .line 77
    invoke-virtual {p1, v1}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->OOO〇O0(I)Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 78
    .line 79
    .line 80
    move-result-object v2

    .line 81
    :goto_0
    if-nez v2, :cond_4

    .line 82
    .line 83
    invoke-virtual {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇00()V

    .line 84
    .line 85
    .line 86
    return-void

    .line 87
    :cond_4
    iput-object v2, p0, Lcom/intsig/advertisement/view/FullScreenITActivity;->ooo0〇〇O:Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 88
    .line 89
    invoke-direct {p0}, Lcom/intsig/advertisement/view/FullScreenITActivity;->O〇0O〇Oo〇o()Lcom/intsig/advertisement/databinding/ActivityFullScreenAdLayoutBinding;

    .line 90
    .line 91
    .line 92
    move-result-object p1

    .line 93
    if-eqz p1, :cond_5

    .line 94
    .line 95
    iget-object p1, p1, Lcom/intsig/advertisement/databinding/ActivityFullScreenAdLayoutBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 96
    .line 97
    if-eqz p1, :cond_5

    .line 98
    .line 99
    new-array v0, v0, [Landroid/view/View;

    .line 100
    .line 101
    aput-object p1, v0, v1

    .line 102
    .line 103
    invoke-virtual {p0, v0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇O8〇8O0oO([Landroid/view/View;)V

    .line 104
    .line 105
    .line 106
    :cond_5
    invoke-direct {p0}, Lcom/intsig/advertisement/view/FullScreenITActivity;->O〇〇O80o8()V

    .line 107
    .line 108
    .line 109
    return-void
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/mvp/activity/BaseChangeActivity;->onClick(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    move-object p1, v0

    .line 17
    :goto_0
    sget v1, Lcom/intsig/advertisement/R$id;->iv_close:I

    .line 18
    .line 19
    if-nez p1, :cond_1

    .line 20
    .line 21
    goto :goto_2

    .line 22
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    if-ne p1, v1, :cond_3

    .line 27
    .line 28
    iget-object p1, p0, Lcom/intsig/advertisement/view/FullScreenITActivity;->ooo0〇〇O:Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 29
    .line 30
    if-nez p1, :cond_2

    .line 31
    .line 32
    const-string p1, "requestAbs"

    .line 33
    .line 34
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    goto :goto_1

    .line 38
    :cond_2
    move-object v0, p1

    .line 39
    :goto_1
    invoke-virtual {v0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyOnClose()V

    .line 40
    .line 41
    .line 42
    invoke-virtual {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇00()V

    .line 43
    .line 44
    .line 45
    :cond_3
    :goto_2
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public bridge synthetic onToolbarTitleClick(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->Oo08(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public o〇oo()I
    .locals 1

    .line 1
    sget v0, Lcom/intsig/advertisement/R$layout;->activity_full_screen_ad_layout:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇0〇0()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

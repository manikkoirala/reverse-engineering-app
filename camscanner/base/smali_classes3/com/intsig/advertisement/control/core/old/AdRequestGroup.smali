.class public Lcom/intsig/advertisement/control/core/old/AdRequestGroup;
.super Ljava/lang/Object;
.source "AdRequestGroup.java"

# interfaces
.implements Lcom/intsig/advertisement/listener/OnAdRequestListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/intsig/advertisement/listener/OnAdRequestListener<",
        "Lcom/intsig/advertisement/interfaces/RealRequestAbs;",
        "Lcom/intsig/advertisement/interfaces/RealRequestAbs;",
        ">;"
    }
.end annotation


# instance fields
.field private O8:Z

.field private Oo08:I

.field private oO80:Lcom/intsig/advertisement/control/GroupType;

.field private o〇0:Z

.field private 〇080:Ljava/lang/String;

.field private 〇80〇808〇O:Lcom/intsig/advertisement/enums/RequestState;

.field private 〇o00〇〇Oo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/advertisement/interfaces/RealRequestAbs;",
            ">;"
        }
    .end annotation
.end field

.field private 〇o〇:Lcom/intsig/advertisement/listener/OnAdRequestListener;

.field private 〇〇888:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/advertisement/control/CarouseInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "GroupRequestCore"

    .line 2
    iput-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇080:Ljava/lang/String;

    const/4 v0, 0x0

    .line 3
    iput-boolean v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->O8:Z

    .line 4
    iput-boolean v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->o〇0:Z

    .line 5
    sget-object v0, Lcom/intsig/advertisement/control/GroupType;->OtherPriorityGp:Lcom/intsig/advertisement/control/GroupType;

    iput-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->oO80:Lcom/intsig/advertisement/control/GroupType;

    .line 6
    sget-object v0, Lcom/intsig/advertisement/enums/RequestState;->normal:Lcom/intsig/advertisement/enums/RequestState;

    iput-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇80〇808〇O:Lcom/intsig/advertisement/enums/RequestState;

    .line 7
    iput-object p1, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇080:Ljava/lang/String;

    .line 8
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇o00〇〇Oo:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/intsig/advertisement/control/GroupType;)V
    .locals 1

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "GroupRequestCore"

    .line 10
    iput-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇080:Ljava/lang/String;

    const/4 v0, 0x0

    .line 11
    iput-boolean v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->O8:Z

    .line 12
    iput-boolean v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->o〇0:Z

    .line 13
    sget-object v0, Lcom/intsig/advertisement/control/GroupType;->OtherPriorityGp:Lcom/intsig/advertisement/control/GroupType;

    iput-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->oO80:Lcom/intsig/advertisement/control/GroupType;

    .line 14
    sget-object v0, Lcom/intsig/advertisement/enums/RequestState;->normal:Lcom/intsig/advertisement/enums/RequestState;

    iput-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇80〇808〇O:Lcom/intsig/advertisement/enums/RequestState;

    .line 15
    iput-object p1, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇080:Ljava/lang/String;

    .line 16
    iput-object p2, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->oO80:Lcom/intsig/advertisement/control/GroupType;

    .line 17
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇o00〇〇Oo:Ljava/util/ArrayList;

    return-void
.end method

.method private OO0o〇〇(ILjava/lang/String;Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->O8:Z

    .line 3
    .line 4
    sget-object v0, Lcom/intsig/advertisement/enums/RequestState;->failed:Lcom/intsig/advertisement/enums/RequestState;

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇80〇808〇O:Lcom/intsig/advertisement/enums/RequestState;

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇080:Ljava/lang/String;

    .line 9
    .line 10
    new-instance v1, Ljava/lang/StringBuilder;

    .line 11
    .line 12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    .line 14
    .line 15
    const-string v2, "on fail,errorCode="

    .line 16
    .line 17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 21
    .line 22
    .line 23
    const-string v2, ",errorMsg="

    .line 24
    .line 25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    invoke-static {v0, v1}, Lcom/intsig/advertisement/logagent/LogPrinter;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇o〇:Lcom/intsig/advertisement/listener/OnAdRequestListener;

    .line 39
    .line 40
    if-eqz v0, :cond_0

    .line 41
    .line 42
    invoke-interface {v0, p1, p2, p3}, Lcom/intsig/advertisement/listener/OnAdRequestListener;->〇80〇808〇O(ILjava/lang/String;Ljava/lang/Object;)V

    .line 43
    .line 44
    .line 45
    :cond_0
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private OO0o〇〇〇〇0(Ljava/util/ArrayList;Lcom/intsig/advertisement/control/CarouseInfo;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/advertisement/control/CarouseInfo;",
            ">;",
            "Lcom/intsig/advertisement/control/CarouseInfo;",
            ")Z"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    .line 2
    if-nez p1, :cond_0

    .line 3
    .line 4
    return v0

    .line 5
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    :cond_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    if-eqz v1, :cond_2

    .line 14
    .line 15
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    check-cast v1, Lcom/intsig/advertisement/control/CarouseInfo;

    .line 20
    .line 21
    invoke-virtual {p2}, Lcom/intsig/advertisement/control/CarouseInfo;->〇080()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v2

    .line 25
    invoke-virtual {v1}, Lcom/intsig/advertisement/control/CarouseInfo;->〇080()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    invoke-static {v2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    if-eqz v1, :cond_1

    .line 34
    .line 35
    const/4 p1, 0x1

    .line 36
    return p1

    .line 37
    :cond_2
    return v0
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private Oooo8o0〇(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V
    .locals 4

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->O8:Z

    .line 3
    .line 4
    sget-object v0, Lcom/intsig/advertisement/enums/RequestState;->succeed:Lcom/intsig/advertisement/enums/RequestState;

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇80〇808〇O:Lcom/intsig/advertisement/enums/RequestState;

    .line 7
    .line 8
    iget-boolean v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->o〇0:Z

    .line 9
    .line 10
    const-string v1, " on succeed"

    .line 11
    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇080:Ljava/lang/String;

    .line 15
    .line 16
    new-instance v2, Ljava/lang/StringBuilder;

    .line 17
    .line 18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 19
    .line 20
    .line 21
    const-string/jumbo v3, "timeout check low impression find: "

    .line 22
    .line 23
    .line 24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 28
    .line 29
    .line 30
    move-result-object v3

    .line 31
    invoke-virtual {v3}, Lcom/intsig/advertisement/params/RequestParam;->〇80〇808〇O()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v3

    .line 35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    .line 37
    .line 38
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    invoke-static {v0, v1}, Lcom/intsig/advertisement/logagent/LogPrinter;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇080:Ljava/lang/String;

    .line 50
    .line 51
    new-instance v2, Ljava/lang/StringBuilder;

    .line 52
    .line 53
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 54
    .line 55
    .line 56
    invoke-virtual {p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 57
    .line 58
    .line 59
    move-result-object v3

    .line 60
    invoke-virtual {v3}, Lcom/intsig/advertisement/params/RequestParam;->〇80〇808〇O()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v3

    .line 64
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v1

    .line 74
    invoke-static {v0, v1}, Lcom/intsig/advertisement/logagent/LogPrinter;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    :goto_0
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇o〇:Lcom/intsig/advertisement/listener/OnAdRequestListener;

    .line 78
    .line 79
    if-eqz v0, :cond_1

    .line 80
    .line 81
    invoke-interface {v0, p1}, Lcom/intsig/advertisement/listener/OnAdRequestListener;->O8(Ljava/lang/Object;)V

    .line 82
    .line 83
    .line 84
    :cond_1
    return-void
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method private o〇0()Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/advertisement/control/CarouseInfo;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->oO80:Lcom/intsig/advertisement/control/GroupType;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/advertisement/control/GroupType;->FirstPriorityGp:Lcom/intsig/advertisement/control/GroupType;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-eq v0, v1, :cond_0

    .line 7
    .line 8
    return-object v2

    .line 9
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    .line 10
    .line 11
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 12
    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 21
    .line 22
    .line 23
    move-result v3

    .line 24
    if-eqz v3, :cond_4

    .line 25
    .line 26
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object v3

    .line 30
    check-cast v3, Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 31
    .line 32
    invoke-virtual {v3}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 33
    .line 34
    .line 35
    move-result-object v4

    .line 36
    invoke-virtual {v4}, Lcom/intsig/advertisement/params/RequestParam;->O8()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v4

    .line 40
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 41
    .line 42
    .line 43
    move-result v5

    .line 44
    if-eqz v5, :cond_2

    .line 45
    .line 46
    return-object v2

    .line 47
    :cond_2
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 48
    .line 49
    .line 50
    move-result v5

    .line 51
    if-nez v5, :cond_1

    .line 52
    .line 53
    const-string v5, ","

    .line 54
    .line 55
    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    .line 56
    .line 57
    .line 58
    move-result-object v4

    .line 59
    array-length v5, v4

    .line 60
    if-lez v5, :cond_1

    .line 61
    .line 62
    array-length v5, v4

    .line 63
    const/4 v6, 0x0

    .line 64
    :goto_0
    if-ge v6, v5, :cond_1

    .line 65
    .line 66
    aget-object v7, v4, v6

    .line 67
    .line 68
    invoke-static {v7}, Lcom/intsig/advertisement/util/CommonUtil;->〇〇808〇(Ljava/lang/String;)Z

    .line 69
    .line 70
    .line 71
    move-result v8

    .line 72
    if-nez v8, :cond_3

    .line 73
    .line 74
    return-object v2

    .line 75
    :cond_3
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    .line 76
    .line 77
    .line 78
    move-result v7

    .line 79
    new-instance v8, Lcom/intsig/advertisement/control/CarouseInfo;

    .line 80
    .line 81
    invoke-virtual {v3}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 82
    .line 83
    .line 84
    move-result-object v9

    .line 85
    invoke-virtual {v9}, Lcom/intsig/advertisement/params/RequestParam;->〇O〇()Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v9

    .line 89
    invoke-direct {v8, v9, v7}, Lcom/intsig/advertisement/control/CarouseInfo;-><init>(Ljava/lang/String;I)V

    .line 90
    .line 91
    .line 92
    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    .line 94
    .line 95
    add-int/lit8 v6, v6, 0x1

    .line 96
    .line 97
    goto :goto_0

    .line 98
    :cond_4
    return-object v0
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private 〇0〇O0088o(Lcom/intsig/advertisement/params/RequestParam;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇〇888:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_1

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Lcom/intsig/advertisement/control/CarouseInfo;

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/advertisement/control/CarouseInfo;->〇080()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    invoke-virtual {p1}, Lcom/intsig/advertisement/params/RequestParam;->〇O〇()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v3

    .line 27
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    if-eqz v2, :cond_0

    .line 32
    .line 33
    iget-object p1, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇〇888:Ljava/util/ArrayList;

    .line 34
    .line 35
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 36
    .line 37
    .line 38
    :cond_1
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private 〇8o8o〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇〇888:Ljava/util/ArrayList;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v0, 0x0

    .line 8
    :goto_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private static synthetic 〇O8o08O(Lcom/intsig/advertisement/control/CarouseInfo;Lcom/intsig/advertisement/control/CarouseInfo;)I
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/advertisement/control/CarouseInfo;->〇o00〇〇Oo()I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    invoke-virtual {p1}, Lcom/intsig/advertisement/control/CarouseInfo;->〇o00〇〇Oo()I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    sub-int/2addr p0, p1

    .line 10
    return p0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private 〇O〇(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/advertisement/interfaces/RealRequestAbs;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇080:Ljava/lang/String;

    .line 2
    .line 3
    const-string/jumbo v1, "start request--"

    .line 4
    .line 5
    .line 6
    invoke-static {v0, v1}, Lcom/intsig/advertisement/logagent/LogPrinter;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 10
    .line 11
    .line 12
    move-result-object p2

    .line 13
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    check-cast v0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 24
    .line 25
    invoke-static {}, Lcom/intsig/advertisement/logagent/LogAgentManager;->〇8o8o〇()Lcom/intsig/advertisement/logagent/LogAgentManager;

    .line 26
    .line 27
    .line 28
    move-result-object v1

    .line 29
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->addAdRequestListener(Lcom/intsig/advertisement/listener/OnAdRequestListener;)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0, p0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->addAdRequestListener(Lcom/intsig/advertisement/listener/OnAdRequestListener;)V

    .line 33
    .line 34
    .line 35
    sget-object v1, Lcom/intsig/advertisement/control/AdCachePool;->Oo08:Lcom/intsig/advertisement/control/AdCachePool$Companion;

    .line 36
    .line 37
    invoke-virtual {v1}, Lcom/intsig/advertisement/control/AdCachePool$Companion;->〇080()Lcom/intsig/advertisement/control/AdCachePool;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    invoke-virtual {v0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 42
    .line 43
    .line 44
    move-result-object v2

    .line 45
    invoke-virtual {v2}, Lcom/intsig/advertisement/params/RequestParam;->〇O〇()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v2

    .line 49
    invoke-virtual {v1, v2}, Lcom/intsig/advertisement/control/AdCachePool;->O8(Ljava/lang/String;)Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    if-eqz v1, :cond_0

    .line 54
    .line 55
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇080:Ljava/lang/String;

    .line 56
    .line 57
    new-instance v2, Ljava/lang/StringBuilder;

    .line 58
    .line 59
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 60
    .line 61
    .line 62
    invoke-virtual {v1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 63
    .line 64
    .line 65
    move-result-object v3

    .line 66
    invoke-virtual {v3}, Lcom/intsig/advertisement/params/RequestParam;->〇80〇808〇O()Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object v3

    .line 70
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    const-string v3, " fetch from cache pool"

    .line 74
    .line 75
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object v2

    .line 82
    invoke-static {v0, v2}, Lcom/intsig/advertisement/logagent/LogPrinter;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    invoke-virtual {p0, v1}, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇〇8O0〇8(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V

    .line 86
    .line 87
    .line 88
    goto :goto_0

    .line 89
    :cond_0
    invoke-virtual {v0, p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->startRequest(Landroid/content/Context;)V

    .line 90
    .line 91
    .line 92
    goto :goto_0

    .line 93
    :cond_1
    return-void
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/advertisement/control/CarouseInfo;Lcom/intsig/advertisement/control/CarouseInfo;)I
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇O8o08O(Lcom/intsig/advertisement/control/CarouseInfo;Lcom/intsig/advertisement/control/CarouseInfo;)I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private 〇〇888()Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/advertisement/control/CarouseInfo;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->o〇0()Ljava/util/ArrayList;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_5

    .line 6
    .line 7
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-lez v1, :cond_5

    .line 12
    .line 13
    iget-object v1, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 14
    .line 15
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 20
    .line 21
    .line 22
    move-result v2

    .line 23
    if-eqz v2, :cond_0

    .line 24
    .line 25
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    check-cast v2, Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 30
    .line 31
    invoke-virtual {v2}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    const/4 v3, 0x1

    .line 36
    invoke-virtual {v2, v3}, Lcom/intsig/advertisement/params/RequestParam;->O8ooOoo〇(Z)V

    .line 37
    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_0
    new-instance v1, LOO8oO0o〇/〇o00〇〇Oo;

    .line 41
    .line 42
    invoke-direct {v1}, LOO8oO0o〇/〇o00〇〇Oo;-><init>()V

    .line 43
    .line 44
    .line 45
    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 46
    .line 47
    .line 48
    iget-object v1, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 49
    .line 50
    const/4 v2, 0x0

    .line 51
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    check-cast v1, Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 56
    .line 57
    invoke-virtual {v1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 62
    .line 63
    .line 64
    move-result v3

    .line 65
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 66
    .line 67
    .line 68
    move-result-object v4

    .line 69
    invoke-virtual {v1}, Lcom/intsig/advertisement/params/RequestParam;->〇O8o08O()Lcom/intsig/advertisement/enums/PositionType;

    .line 70
    .line 71
    .line 72
    move-result-object v5

    .line 73
    invoke-virtual {v1}, Lcom/intsig/advertisement/params/RequestParam;->oO80()I

    .line 74
    .line 75
    .line 76
    move-result v1

    .line 77
    invoke-virtual {v4, v5, v1}, Lcom/intsig/advertisement/record/AdRecordHelper;->OO0o〇〇(Lcom/intsig/advertisement/enums/PositionType;I)I

    .line 78
    .line 79
    .line 80
    move-result v1

    .line 81
    rem-int/2addr v1, v3

    .line 82
    new-instance v4, Ljava/util/ArrayList;

    .line 83
    .line 84
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 85
    .line 86
    .line 87
    new-instance v5, Ljava/lang/StringBuffer;

    .line 88
    .line 89
    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    .line 90
    .line 91
    .line 92
    move v6, v1

    .line 93
    :goto_1
    const-string v7, ","

    .line 94
    .line 95
    if-ge v6, v3, :cond_2

    .line 96
    .line 97
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 98
    .line 99
    .line 100
    move-result-object v8

    .line 101
    check-cast v8, Lcom/intsig/advertisement/control/CarouseInfo;

    .line 102
    .line 103
    invoke-direct {p0, v4, v8}, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->OO0o〇〇〇〇0(Ljava/util/ArrayList;Lcom/intsig/advertisement/control/CarouseInfo;)Z

    .line 104
    .line 105
    .line 106
    move-result v9

    .line 107
    if-nez v9, :cond_1

    .line 108
    .line 109
    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    .line 111
    .line 112
    new-instance v9, Ljava/lang/StringBuilder;

    .line 113
    .line 114
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 115
    .line 116
    .line 117
    invoke-virtual {v8}, Lcom/intsig/advertisement/control/CarouseInfo;->〇080()Ljava/lang/String;

    .line 118
    .line 119
    .line 120
    move-result-object v8

    .line 121
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 122
    .line 123
    .line 124
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    .line 126
    .line 127
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object v7

    .line 131
    invoke-virtual {v5, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 132
    .line 133
    .line 134
    :cond_1
    add-int/lit8 v6, v6, 0x1

    .line 135
    .line 136
    goto :goto_1

    .line 137
    :cond_2
    :goto_2
    if-ge v2, v1, :cond_4

    .line 138
    .line 139
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 140
    .line 141
    .line 142
    move-result-object v3

    .line 143
    check-cast v3, Lcom/intsig/advertisement/control/CarouseInfo;

    .line 144
    .line 145
    invoke-direct {p0, v4, v3}, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->OO0o〇〇〇〇0(Ljava/util/ArrayList;Lcom/intsig/advertisement/control/CarouseInfo;)Z

    .line 146
    .line 147
    .line 148
    move-result v6

    .line 149
    if-nez v6, :cond_3

    .line 150
    .line 151
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 152
    .line 153
    .line 154
    new-instance v6, Ljava/lang/StringBuilder;

    .line 155
    .line 156
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 157
    .line 158
    .line 159
    invoke-virtual {v3}, Lcom/intsig/advertisement/control/CarouseInfo;->〇080()Ljava/lang/String;

    .line 160
    .line 161
    .line 162
    move-result-object v3

    .line 163
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    .line 165
    .line 166
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    .line 168
    .line 169
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 170
    .line 171
    .line 172
    move-result-object v3

    .line 173
    invoke-virtual {v5, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 174
    .line 175
    .line 176
    :cond_3
    add-int/lit8 v2, v2, 0x1

    .line 177
    .line 178
    goto :goto_2

    .line 179
    :cond_4
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇080:Ljava/lang/String;

    .line 180
    .line 181
    new-instance v1, Ljava/lang/StringBuilder;

    .line 182
    .line 183
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 184
    .line 185
    .line 186
    const-string v2, "carouse order="

    .line 187
    .line 188
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 189
    .line 190
    .line 191
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    .line 192
    .line 193
    .line 194
    move-result-object v2

    .line 195
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    .line 197
    .line 198
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 199
    .line 200
    .line 201
    move-result-object v1

    .line 202
    invoke-static {v0, v1}, Lcom/intsig/advertisement/logagent/LogPrinter;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    .line 204
    .line 205
    return-object v4

    .line 206
    :cond_5
    const/4 v0, 0x0

    .line 207
    return-object v0
    .line 208
    .line 209
    .line 210
    .line 211
.end method


# virtual methods
.method public bridge synthetic O8(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇〇8O0〇8(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public Oo08()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-lez v0, :cond_1

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 12
    .line 13
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 18
    .line 19
    .line 20
    move-result v1

    .line 21
    if-eqz v1, :cond_1

    .line 22
    .line 23
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    check-cast v1, Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 28
    .line 29
    invoke-virtual {v1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestState()Lcom/intsig/advertisement/enums/RequestState;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    sget-object v3, Lcom/intsig/advertisement/enums/RequestState;->requesting:Lcom/intsig/advertisement/enums/RequestState;

    .line 34
    .line 35
    if-ne v2, v3, :cond_0

    .line 36
    .line 37
    invoke-virtual {v1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->onCancelRequest()V

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_1
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method protected OoO8(Lcom/intsig/advertisement/control/GroupType;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->oO80:Lcom/intsig/advertisement/control/GroupType;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public o800o8O(Landroid/content/Context;Lcom/intsig/advertisement/listener/OnAdRequestListener;)V
    .locals 1

    .line 1
    iput-object p2, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇o〇:Lcom/intsig/advertisement/listener/OnAdRequestListener;

    .line 2
    .line 3
    sget-object p2, Lcom/intsig/advertisement/enums/RequestState;->requesting:Lcom/intsig/advertisement/enums/RequestState;

    .line 4
    .line 5
    iput-object p2, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇80〇808〇O:Lcom/intsig/advertisement/enums/RequestState;

    .line 6
    .line 7
    const/4 p2, 0x0

    .line 8
    if-nez p1, :cond_0

    .line 9
    .line 10
    iget-object p1, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 11
    .line 12
    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 13
    .line 14
    .line 15
    move-result-object p1

    .line 16
    check-cast p1, Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 17
    .line 18
    const/4 p2, -0x1

    .line 19
    const-string v0, "context is null"

    .line 20
    .line 21
    invoke-direct {p0, p2, v0, p1}, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->OO0o〇〇(ILjava/lang/String;Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V

    .line 22
    .line 23
    .line 24
    return-void

    .line 25
    :cond_0
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 26
    .line 27
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object p2

    .line 31
    check-cast p2, Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 32
    .line 33
    invoke-virtual {p2}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 34
    .line 35
    .line 36
    move-result-object p2

    .line 37
    invoke-virtual {p2}, Lcom/intsig/advertisement/params/RequestParam;->Oooo8o0〇()I

    .line 38
    .line 39
    .line 40
    move-result p2

    .line 41
    iput p2, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->Oo08:I

    .line 42
    .line 43
    new-instance p2, Ljava/lang/StringBuilder;

    .line 44
    .line 45
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 46
    .line 47
    .line 48
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇080:Ljava/lang/String;

    .line 49
    .line 50
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    const-string v0, " group="

    .line 54
    .line 55
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    iget v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->Oo08:I

    .line 59
    .line 60
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object p2

    .line 67
    iput-object p2, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇080:Ljava/lang/String;

    .line 68
    .line 69
    invoke-direct {p0}, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇〇888()Ljava/util/ArrayList;

    .line 70
    .line 71
    .line 72
    move-result-object p2

    .line 73
    iput-object p2, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇〇888:Ljava/util/ArrayList;

    .line 74
    .line 75
    new-instance p2, Ljava/util/ArrayList;

    .line 76
    .line 77
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 78
    .line 79
    invoke-direct {p2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 80
    .line 81
    .line 82
    invoke-direct {p0, p1, p2}, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇O〇(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 83
    .line 84
    .line 85
    return-void
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

.method protected oO80()Lcom/intsig/advertisement/enums/RequestState;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇80〇808〇O:Lcom/intsig/advertisement/enums/RequestState;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public bridge synthetic 〇080(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇O00(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public bridge synthetic 〇80〇808〇O(ILjava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p3, Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2, p3}, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇〇808〇(ILjava/lang/String;Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public 〇O00(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇o〇:Lcom/intsig/advertisement/listener/OnAdRequestListener;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-interface {v0, p1}, Lcom/intsig/advertisement/listener/OnAdRequestListener;->〇080(Ljava/lang/Object;)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public 〇O888o0o()V
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->o〇0:Z

    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇80〇808〇O:Lcom/intsig/advertisement/enums/RequestState;

    .line 5
    .line 6
    sget-object v1, Lcom/intsig/advertisement/enums/RequestState;->requesting:Lcom/intsig/advertisement/enums/RequestState;

    .line 7
    .line 8
    if-eq v0, v1, :cond_0

    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->Oo08()V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇8o8o〇()Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-eqz v0, :cond_2

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇〇888:Ljava/util/ArrayList;

    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    if-eqz v1, :cond_2

    .line 31
    .line 32
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    check-cast v1, Lcom/intsig/advertisement/control/CarouseInfo;

    .line 37
    .line 38
    sget-object v2, Lcom/intsig/advertisement/control/AdCachePool;->Oo08:Lcom/intsig/advertisement/control/AdCachePool$Companion;

    .line 39
    .line 40
    invoke-virtual {v2}, Lcom/intsig/advertisement/control/AdCachePool$Companion;->〇080()Lcom/intsig/advertisement/control/AdCachePool;

    .line 41
    .line 42
    .line 43
    move-result-object v2

    .line 44
    invoke-virtual {v1}, Lcom/intsig/advertisement/control/CarouseInfo;->〇080()Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    invoke-virtual {v2, v1}, Lcom/intsig/advertisement/control/AdCachePool;->O8(Ljava/lang/String;)Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 49
    .line 50
    .line 51
    move-result-object v1

    .line 52
    if-eqz v1, :cond_1

    .line 53
    .line 54
    invoke-direct {p0, v1}, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->Oooo8o0〇(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V

    .line 55
    .line 56
    .line 57
    return-void

    .line 58
    :cond_2
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 59
    .line 60
    const/4 v1, 0x0

    .line 61
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    check-cast v0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 66
    .line 67
    const/4 v1, -0x1

    .line 68
    const-string/jumbo v2, "timeout check cache pool,but no ad is ready"

    .line 69
    .line 70
    .line 71
    invoke-direct {p0, v1, v2, v0}, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->OO0o〇〇(ILjava/lang/String;Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V

    .line 72
    .line 73
    .line 74
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method protected 〇o〇(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇80〇808〇O:Lcom/intsig/advertisement/enums/RequestState;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/advertisement/enums/RequestState;->normal:Lcom/intsig/advertisement/enums/RequestState;

    .line 4
    .line 5
    if-eq v0, v1, :cond_0

    .line 6
    .line 7
    iget-object p1, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇080:Ljava/lang/String;

    .line 8
    .line 9
    const-string v0, "group is start request and not allow add"

    .line 10
    .line 11
    invoke-static {p1, v0}, Lcom/intsig/advertisement/logagent/LogPrinter;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    if-eqz p1, :cond_1

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 18
    .line 19
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 20
    .line 21
    .line 22
    :cond_1
    return-void
.end method

.method public 〇〇808〇(ILjava/lang/String;Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 4
    .line 5
    .line 6
    iget-boolean p1, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->o〇0:Z

    .line 7
    .line 8
    if-nez p1, :cond_3

    .line 9
    .line 10
    iget-boolean p1, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->O8:Z

    .line 11
    .line 12
    if-eqz p1, :cond_0

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    iget-object p1, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 16
    .line 17
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 18
    .line 19
    .line 20
    move-result p1

    .line 21
    if-nez p1, :cond_1

    .line 22
    .line 23
    const/4 p1, -0x1

    .line 24
    const-string p2, "concurrentGroup request fail"

    .line 25
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->OO0o〇〇(ILjava/lang/String;Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V

    .line 27
    .line 28
    .line 29
    return-void

    .line 30
    :cond_1
    invoke-direct {p0}, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇8o8o〇()Z

    .line 31
    .line 32
    .line 33
    move-result p1

    .line 34
    if-eqz p1, :cond_3

    .line 35
    .line 36
    invoke-virtual {p3}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    invoke-virtual {p1}, Lcom/intsig/advertisement/params/RequestParam;->〇O〇()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    iget-object p2, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇〇888:Ljava/util/ArrayList;

    .line 45
    .line 46
    const/4 v0, 0x0

    .line 47
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 48
    .line 49
    .line 50
    move-result-object p2

    .line 51
    check-cast p2, Lcom/intsig/advertisement/control/CarouseInfo;

    .line 52
    .line 53
    invoke-virtual {p2}, Lcom/intsig/advertisement/control/CarouseInfo;->〇080()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object p2

    .line 57
    invoke-static {p1, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 58
    .line 59
    .line 60
    move-result p1

    .line 61
    if-eqz p1, :cond_2

    .line 62
    .line 63
    invoke-virtual {p3}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇0〇O0088o(Lcom/intsig/advertisement/params/RequestParam;)V

    .line 68
    .line 69
    .line 70
    iget-object p1, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇〇888:Ljava/util/ArrayList;

    .line 71
    .line 72
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 73
    .line 74
    .line 75
    move-result p1

    .line 76
    if-lez p1, :cond_3

    .line 77
    .line 78
    sget-object p1, Lcom/intsig/advertisement/control/AdCachePool;->Oo08:Lcom/intsig/advertisement/control/AdCachePool$Companion;

    .line 79
    .line 80
    invoke-virtual {p1}, Lcom/intsig/advertisement/control/AdCachePool$Companion;->〇080()Lcom/intsig/advertisement/control/AdCachePool;

    .line 81
    .line 82
    .line 83
    move-result-object p1

    .line 84
    iget-object p2, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇〇888:Ljava/util/ArrayList;

    .line 85
    .line 86
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 87
    .line 88
    .line 89
    move-result-object p2

    .line 90
    check-cast p2, Lcom/intsig/advertisement/control/CarouseInfo;

    .line 91
    .line 92
    invoke-virtual {p2}, Lcom/intsig/advertisement/control/CarouseInfo;->〇080()Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object p2

    .line 96
    invoke-virtual {p1, p2}, Lcom/intsig/advertisement/control/AdCachePool;->O8(Ljava/lang/String;)Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 97
    .line 98
    .line 99
    move-result-object p1

    .line 100
    if-eqz p1, :cond_3

    .line 101
    .line 102
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->Oooo8o0〇(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V

    .line 103
    .line 104
    .line 105
    goto :goto_0

    .line 106
    :cond_2
    invoke-virtual {p3}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 107
    .line 108
    .line 109
    move-result-object p1

    .line 110
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇0〇O0088o(Lcom/intsig/advertisement/params/RequestParam;)V

    .line 111
    .line 112
    .line 113
    :cond_3
    :goto_0
    return-void
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public 〇〇8O0〇8(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->O8:Z

    .line 2
    .line 3
    if-nez v0, :cond_3

    .line 4
    .line 5
    iget-boolean v0, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->o〇0:Z

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    goto :goto_1

    .line 10
    :cond_0
    invoke-direct {p0}, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇8o8o〇()Z

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    if-eqz v0, :cond_2

    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-virtual {v0}, Lcom/intsig/advertisement/params/RequestParam;->〇O〇()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    iget-object v1, p0, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->〇〇888:Ljava/util/ArrayList;

    .line 25
    .line 26
    const/4 v2, 0x0

    .line 27
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    check-cast v1, Lcom/intsig/advertisement/control/CarouseInfo;

    .line 32
    .line 33
    invoke-virtual {v1}, Lcom/intsig/advertisement/control/CarouseInfo;->〇080()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    if-eqz v0, :cond_1

    .line 42
    .line 43
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->Oooo8o0〇(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V

    .line 44
    .line 45
    .line 46
    goto :goto_0

    .line 47
    :cond_1
    sget-object v0, Lcom/intsig/advertisement/control/AdCachePool;->Oo08:Lcom/intsig/advertisement/control/AdCachePool$Companion;

    .line 48
    .line 49
    invoke-virtual {v0}, Lcom/intsig/advertisement/control/AdCachePool$Companion;->〇080()Lcom/intsig/advertisement/control/AdCachePool;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    invoke-virtual {v0, p1}, Lcom/intsig/advertisement/control/AdCachePool;->o〇0(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)Z

    .line 54
    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_2
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/control/core/old/AdRequestGroup;->Oooo8o0〇(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V

    .line 58
    .line 59
    .line 60
    :goto_0
    return-void

    .line 61
    :cond_3
    :goto_1
    sget-object v0, Lcom/intsig/advertisement/control/AdCachePool;->Oo08:Lcom/intsig/advertisement/control/AdCachePool$Companion;

    .line 62
    .line 63
    invoke-virtual {v0}, Lcom/intsig/advertisement/control/AdCachePool$Companion;->〇080()Lcom/intsig/advertisement/control/AdCachePool;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    invoke-virtual {v0, p1}, Lcom/intsig/advertisement/control/AdCachePool;->o〇0(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)Z

    .line 68
    .line 69
    .line 70
    return-void
    .line 71
.end method

.class public abstract Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;
.super Ljava/lang/Object;
.source "AbsGroupRequest.kt"

# interfaces
.implements Lcom/intsig/advertisement/listener/OnAdRequestListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/intsig/advertisement/listener/OnAdRequestListener<",
        "Lcom/intsig/advertisement/interfaces/RealRequestAbs<",
        "***>;",
        "Lcom/intsig/advertisement/interfaces/RealRequestAbs<",
        "***>;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O8:Lcom/intsig/advertisement/listener/OnAdRequestListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/intsig/advertisement/listener/OnAdRequestListener<",
            "Lcom/intsig/advertisement/interfaces/RealRequestAbs<",
            "***>;",
            "Lcom/intsig/advertisement/interfaces/RealRequestAbs<",
            "***>;>;"
        }
    .end annotation
.end field

.field private Oo08:Lcom/intsig/advertisement/enums/RequestState;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final oO80:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇0:Z

.field private final 〇080:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o00〇〇Oo:I

.field private final 〇o〇:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/advertisement/interfaces/RealRequestAbs<",
            "***>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇〇888:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/util/ArrayList;Lcom/intsig/advertisement/listener/OnAdRequestListener;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Ljava/util/ArrayList;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/advertisement/interfaces/RealRequestAbs<",
            "***>;>;",
            "Lcom/intsig/advertisement/listener/OnAdRequestListener<",
            "Lcom/intsig/advertisement/interfaces/RealRequestAbs<",
            "***>;",
            "Lcom/intsig/advertisement/interfaces/RealRequestAbs<",
            "***>;>;)V"
        }
    .end annotation

    .line 1
    const-string/jumbo v0, "tag"

    .line 2
    .line 3
    .line 4
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    const-string v0, "requestList"

    .line 8
    .line 9
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    .line 14
    .line 15
    iput-object p1, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->〇080:Ljava/lang/String;

    .line 16
    .line 17
    iput p2, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->〇o00〇〇Oo:I

    .line 18
    .line 19
    iput-object p3, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->〇o〇:Ljava/util/ArrayList;

    .line 20
    .line 21
    iput-object p4, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->O8:Lcom/intsig/advertisement/listener/OnAdRequestListener;

    .line 22
    .line 23
    sget-object p1, Lcom/intsig/advertisement/enums/RequestState;->normal:Lcom/intsig/advertisement/enums/RequestState;

    .line 24
    .line 25
    iput-object p1, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->Oo08:Lcom/intsig/advertisement/enums/RequestState;

    .line 26
    .line 27
    const-string p1, "AbsGroupRequest"

    .line 28
    .line 29
    iput-object p1, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->oO80:Ljava/lang/String;

    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
.end method


# virtual methods
.method public OO0o〇〇(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V
    .locals 4
    .param p1    # Lcom/intsig/advertisement/interfaces/RealRequestAbs;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/advertisement/interfaces/RealRequestAbs<",
            "***>;)V"
        }
    .end annotation

    .line 1
    const-string v0, "requestAbs"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->〇〇888()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-virtual {v1}, Lcom/intsig/advertisement/params/RequestParam;->〇80〇808〇O()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    new-instance v2, Ljava/lang/StringBuilder;

    .line 19
    .line 20
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 21
    .line 22
    .line 23
    const-string v3, "group succeed "

    .line 24
    .line 25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    invoke-static {v0, v1}, Lcom/intsig/advertisement/logagent/LogPrinter;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    const/4 v0, 0x1

    .line 39
    iput-boolean v0, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->o〇0:Z

    .line 40
    .line 41
    sget-object v0, Lcom/intsig/advertisement/enums/RequestState;->succeed:Lcom/intsig/advertisement/enums/RequestState;

    .line 42
    .line 43
    iput-object v0, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->Oo08:Lcom/intsig/advertisement/enums/RequestState;

    .line 44
    .line 45
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->O8:Lcom/intsig/advertisement/listener/OnAdRequestListener;

    .line 46
    .line 47
    if-eqz v0, :cond_0

    .line 48
    .line 49
    invoke-interface {v0, p1}, Lcom/intsig/advertisement/listener/OnAdRequestListener;->O8(Ljava/lang/Object;)V

    .line 50
    .line 51
    .line 52
    :cond_0
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public final OO0o〇〇〇〇0()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->〇080:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method protected final Oo08()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->o〇0:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public Oooo8o0〇(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/advertisement/interfaces/RealRequestAbs<",
            "***>;)V"
        }
    .end annotation

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final oO80()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/advertisement/interfaces/RealRequestAbs<",
            "***>;>;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->〇o〇:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final o〇0()Lcom/intsig/advertisement/enums/RequestState;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->Oo08:Lcom/intsig/advertisement/enums/RequestState;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public bridge synthetic 〇080(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->Oooo8o0〇(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method protected final 〇8o8o〇()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->〇〇888:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇O00(Landroid/content/Context;)V
    .locals 6

    .line 1
    sget-object v0, Lcom/intsig/advertisement/enums/RequestState;->requesting:Lcom/intsig/advertisement/enums/RequestState;

    .line 2
    .line 3
    iput-object v0, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->Oo08:Lcom/intsig/advertisement/enums/RequestState;

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->〇o〇:Ljava/util/ArrayList;

    .line 6
    .line 7
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    sget-object p1, Lcom/intsig/advertisement/enums/RequestState;->failed:Lcom/intsig/advertisement/enums/RequestState;

    .line 14
    .line 15
    iput-object p1, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->Oo08:Lcom/intsig/advertisement/enums/RequestState;

    .line 16
    .line 17
    const/4 p1, 0x1

    .line 18
    iput-boolean p1, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->o〇0:Z

    .line 19
    .line 20
    invoke-virtual {p0}, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->〇〇888()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    const-string v0, "requestList is empty"

    .line 25
    .line 26
    invoke-static {p1, v0}, Lcom/intsig/advertisement/logagent/LogPrinter;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    return-void

    .line 30
    :cond_0
    const/4 v0, 0x0

    .line 31
    if-nez p1, :cond_1

    .line 32
    .line 33
    iget-object p1, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->〇o〇:Ljava/util/ArrayList;

    .line 34
    .line 35
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    const-string v0, "requestList[0]"

    .line 40
    .line 41
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    check-cast p1, Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 45
    .line 46
    const/4 v0, -0x1

    .line 47
    const-string v1, "context is null"

    .line 48
    .line 49
    invoke-virtual {p0, v0, v1, p1}, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->〇O8o08O(ILjava/lang/String;Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V

    .line 50
    .line 51
    .line 52
    return-void

    .line 53
    :cond_1
    iget-object v1, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->O8:Lcom/intsig/advertisement/listener/OnAdRequestListener;

    .line 54
    .line 55
    if-eqz v1, :cond_2

    .line 56
    .line 57
    iget-object v2, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->〇o〇:Ljava/util/ArrayList;

    .line 58
    .line 59
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    invoke-interface {v1, v0}, Lcom/intsig/advertisement/listener/OnAdRequestListener;->〇080(Ljava/lang/Object;)V

    .line 64
    .line 65
    .line 66
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    .line 67
    .line 68
    iget-object v1, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->〇o〇:Ljava/util/ArrayList;

    .line 69
    .line 70
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 71
    .line 72
    .line 73
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 74
    .line 75
    .line 76
    move-result-object v0

    .line 77
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 78
    .line 79
    .line 80
    move-result v1

    .line 81
    if-eqz v1, :cond_4

    .line 82
    .line 83
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 84
    .line 85
    .line 86
    move-result-object v1

    .line 87
    check-cast v1, Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 88
    .line 89
    invoke-static {}, Lcom/intsig/advertisement/logagent/LogAgentManager;->〇8o8o〇()Lcom/intsig/advertisement/logagent/LogAgentManager;

    .line 90
    .line 91
    .line 92
    move-result-object v2

    .line 93
    invoke-virtual {v1, v2}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->addAdRequestListener(Lcom/intsig/advertisement/listener/OnAdRequestListener;)V

    .line 94
    .line 95
    .line 96
    invoke-virtual {v1, p0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->addAdRequestListener(Lcom/intsig/advertisement/listener/OnAdRequestListener;)V

    .line 97
    .line 98
    .line 99
    sget-object v2, Lcom/intsig/advertisement/control/AdCachePool;->Oo08:Lcom/intsig/advertisement/control/AdCachePool$Companion;

    .line 100
    .line 101
    invoke-virtual {v2}, Lcom/intsig/advertisement/control/AdCachePool$Companion;->〇080()Lcom/intsig/advertisement/control/AdCachePool;

    .line 102
    .line 103
    .line 104
    move-result-object v2

    .line 105
    invoke-virtual {v1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 106
    .line 107
    .line 108
    move-result-object v3

    .line 109
    invoke-virtual {v3}, Lcom/intsig/advertisement/params/RequestParam;->〇O〇()Ljava/lang/String;

    .line 110
    .line 111
    .line 112
    move-result-object v3

    .line 113
    const-string v4, "requestAbs.requestParam.sourceKey"

    .line 114
    .line 115
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 116
    .line 117
    .line 118
    invoke-virtual {v2, v3}, Lcom/intsig/advertisement/control/AdCachePool;->O8(Ljava/lang/String;)Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 119
    .line 120
    .line 121
    move-result-object v2

    .line 122
    if-eqz v2, :cond_3

    .line 123
    .line 124
    invoke-virtual {p0}, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->〇〇888()Ljava/lang/String;

    .line 125
    .line 126
    .line 127
    move-result-object v3

    .line 128
    invoke-virtual {v1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 129
    .line 130
    .line 131
    move-result-object v1

    .line 132
    invoke-virtual {v1}, Lcom/intsig/advertisement/params/RequestParam;->〇80〇808〇O()Ljava/lang/String;

    .line 133
    .line 134
    .line 135
    move-result-object v1

    .line 136
    invoke-virtual {v2}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestParam()Lcom/intsig/advertisement/params/RequestParam;

    .line 137
    .line 138
    .line 139
    move-result-object v4

    .line 140
    invoke-virtual {v4}, Lcom/intsig/advertisement/params/RequestParam;->〇80〇808〇O()Ljava/lang/String;

    .line 141
    .line 142
    .line 143
    move-result-object v4

    .line 144
    new-instance v5, Ljava/lang/StringBuilder;

    .line 145
    .line 146
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 147
    .line 148
    .line 149
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    .line 151
    .line 152
    const-string v1, " has cached  with "

    .line 153
    .line 154
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    .line 156
    .line 157
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    .line 159
    .line 160
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 161
    .line 162
    .line 163
    move-result-object v1

    .line 164
    invoke-static {v3, v1}, Lcom/intsig/advertisement/logagent/LogPrinter;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    .line 166
    .line 167
    invoke-interface {p0, v2}, Lcom/intsig/advertisement/listener/OnAdRequestListener;->O8(Ljava/lang/Object;)V

    .line 168
    .line 169
    .line 170
    goto :goto_0

    .line 171
    :cond_3
    invoke-virtual {v1, p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->startRequest(Landroid/content/Context;)V

    .line 172
    .line 173
    .line 174
    goto :goto_0

    .line 175
    :cond_4
    return-void
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method public 〇O8o08O(ILjava/lang/String;Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/advertisement/interfaces/RealRequestAbs;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Lcom/intsig/advertisement/interfaces/RealRequestAbs<",
            "***>;)V"
        }
    .end annotation

    .line 1
    const-string v0, "errorMsg"

    .line 2
    .line 3
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string v0, "realRequestAbs"

    .line 7
    .line 8
    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->〇〇888()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "group fail"

    .line 16
    .line 17
    invoke-static {v0, v1}, Lcom/intsig/advertisement/logagent/LogPrinter;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    const/4 v0, 0x1

    .line 21
    iput-boolean v0, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->o〇0:Z

    .line 22
    .line 23
    sget-object v0, Lcom/intsig/advertisement/enums/RequestState;->failed:Lcom/intsig/advertisement/enums/RequestState;

    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->Oo08:Lcom/intsig/advertisement/enums/RequestState;

    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->O8:Lcom/intsig/advertisement/listener/OnAdRequestListener;

    .line 28
    .line 29
    if-eqz v0, :cond_0

    .line 30
    .line 31
    invoke-interface {v0, p1, p2, p3}, Lcom/intsig/advertisement/listener/OnAdRequestListener;->〇80〇808〇O(ILjava/lang/String;Ljava/lang/Object;)V

    .line 32
    .line 33
    .line 34
    :cond_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method protected final 〇O〇(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->〇〇888:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public 〇o00〇〇Oo()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->〇o〇:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-lez v0, :cond_1

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->〇o〇:Ljava/util/ArrayList;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-eqz v1, :cond_1

    .line 20
    .line 21
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    check-cast v1, Lcom/intsig/advertisement/interfaces/RealRequestAbs;

    .line 26
    .line 27
    invoke-virtual {v1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRequestState()Lcom/intsig/advertisement/enums/RequestState;

    .line 28
    .line 29
    .line 30
    move-result-object v2

    .line 31
    sget-object v3, Lcom/intsig/advertisement/enums/RequestState;->requesting:Lcom/intsig/advertisement/enums/RequestState;

    .line 32
    .line 33
    if-ne v2, v3, :cond_0

    .line 34
    .line 35
    invoke-virtual {v1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->onCancelRequest()V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_1
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public final 〇o〇()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->〇o00〇〇Oo:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final 〇〇808〇(Lcom/intsig/advertisement/enums/RequestState;)V
    .locals 1
    .param p1    # Lcom/intsig/advertisement/enums/RequestState;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->Oo08:Lcom/intsig/advertisement/enums/RequestState;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method protected 〇〇888()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/control/core/bid/AbsGroupRequest;->oO80:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public abstract 〇〇8O0〇8()V
.end method

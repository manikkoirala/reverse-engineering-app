.class public abstract Lcom/intsig/advertisement/interfaces/RealRequestAbs;
.super Ljava/lang/Object;
.source "RealRequestAbs.java"

# interfaces
.implements Lcom/intsig/DocMultiEntity;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Param:",
        "Lcom/intsig/advertisement/params/RequestParam;",
        "Listener::Lcom/intsig/advertisement/listener/OnAdShowListener;",
        "AdData:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/intsig/DocMultiEntity;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field public static BID_FAIL_LOW_PRICE:I = 0x2

.field public static BID_FAIL_OTHER:I = 0x3

.field public static BID_SUCCEED:I = 0x0

.field public static INVALIDATE_PRICE:F = -999.0f


# instance fields
.field public hasLoadNotify:Z

.field protected hasNotifyShow:Z

.field protected hasShowClose:Z

.field protected hasUsed:Z

.field public isBid:Z

.field private isDropByPrice:Z

.field protected mAdShowListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "T",
            "Listener;",
            ">;"
        }
    .end annotation
.end field

.field public mData:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TAdData;"
        }
    .end annotation
.end field

.field protected mExtraMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field protected mFeedBackInfo:Lcom/intsig/advertisement/feedback/FeedBackInfo;

.field protected mFillTime:J

.field protected mRequestListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/advertisement/listener/OnAdRequestListener;",
            ">;"
        }
    .end annotation
.end field

.field protected mRequestParam:Lcom/intsig/advertisement/params/RequestParam;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TParam;"
        }
    .end annotation
.end field

.field protected mRequestTime:J

.field protected mShowTime:J

.field private mState:Lcom/intsig/advertisement/enums/RequestState;

.field protected mTag:Ljava/lang/String;

.field private preShowPosition:Lcom/intsig/advertisement/enums/PositionType;

.field protected realPrice:F

.field private tag:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public constructor <init>(Lcom/intsig/advertisement/params/RequestParam;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Ljava/util/ArrayList;

    .line 5
    .line 6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mAdShowListeners:Ljava/util/ArrayList;

    .line 10
    .line 11
    new-instance v0, Ljava/util/ArrayList;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestListeners:Ljava/util/ArrayList;

    .line 17
    .line 18
    sget-object v0, Lcom/intsig/advertisement/enums/RequestState;->normal:Lcom/intsig/advertisement/enums/RequestState;

    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mState:Lcom/intsig/advertisement/enums/RequestState;

    .line 21
    .line 22
    const/4 v0, 0x0

    .line 23
    iput-boolean v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->hasUsed:Z

    .line 24
    .line 25
    iput-boolean v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->hasNotifyShow:Z

    .line 26
    .line 27
    iput-boolean v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->isBid:Z

    .line 28
    .line 29
    iput-boolean v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->hasLoadNotify:Z

    .line 30
    .line 31
    iput-boolean v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->isDropByPrice:Z

    .line 32
    .line 33
    iput-boolean v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->hasShowClose:Z

    .line 34
    .line 35
    iput-object p1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 36
    .line 37
    invoke-virtual {p1}, Lcom/intsig/advertisement/params/RequestParam;->〇80〇808〇O()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    iput-object p1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mTag:Ljava/lang/String;

    .line 42
    .line 43
    new-instance p1, Lcom/intsig/advertisement/feedback/FeedBackInfo;

    .line 44
    .line 45
    invoke-direct {p1}, Lcom/intsig/advertisement/feedback/FeedBackInfo;-><init>()V

    .line 46
    .line 47
    .line 48
    iput-object p1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mFeedBackInfo:Lcom/intsig/advertisement/feedback/FeedBackInfo;

    .line 49
    .line 50
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 51
    .line 52
    invoke-virtual {v0}, Lcom/intsig/advertisement/params/RequestParam;->〇O8o08O()Lcom/intsig/advertisement/enums/PositionType;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lcom/intsig/advertisement/enums/PositionType;->getPositionId()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/feedback/FeedBackInfo;->setPosition(Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    iget-object p1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mFeedBackInfo:Lcom/intsig/advertisement/feedback/FeedBackInfo;

    .line 64
    .line 65
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 66
    .line 67
    invoke-virtual {v0}, Lcom/intsig/advertisement/params/RequestParam;->〇〇8O0〇8()Lcom/intsig/advertisement/enums/SourceType;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    invoke-virtual {v0}, Lcom/intsig/advertisement/enums/SourceType;->getSourceName()Ljava/lang/String;

    .line 72
    .line 73
    .line 74
    move-result-object v0

    .line 75
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/feedback/FeedBackInfo;->setSource(Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    iget-object p1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mFeedBackInfo:Lcom/intsig/advertisement/feedback/FeedBackInfo;

    .line 79
    .line 80
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 81
    .line 82
    invoke-virtual {v0}, Lcom/intsig/advertisement/params/RequestParam;->〇o00〇〇Oo()Lcom/intsig/advertisement/enums/AdType;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/feedback/FeedBackInfo;->setType(Ljava/lang/String;)V

    .line 91
    .line 92
    .line 93
    iget-object p1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mFeedBackInfo:Lcom/intsig/advertisement/feedback/FeedBackInfo;

    .line 94
    .line 95
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 96
    .line 97
    invoke-virtual {v0}, Lcom/intsig/advertisement/params/RequestParam;->〇O8o08O()Lcom/intsig/advertisement/enums/PositionType;

    .line 98
    .line 99
    .line 100
    move-result-object v0

    .line 101
    invoke-static {v0}, Lcom/intsig/advertisement/logagent/AdTrackUtils;->o〇0(Lcom/intsig/advertisement/enums/PositionType;)Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object v0

    .line 105
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/feedback/FeedBackInfo;->setPageId(Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    iget-object p1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 109
    .line 110
    invoke-virtual {p1}, Lcom/intsig/advertisement/params/RequestParam;->〇O8o08O()Lcom/intsig/advertisement/enums/PositionType;

    .line 111
    .line 112
    .line 113
    move-result-object p1

    .line 114
    sget-object v0, Lcom/intsig/advertisement/enums/PositionType;->DocList:Lcom/intsig/advertisement/enums/PositionType;

    .line 115
    .line 116
    if-ne p1, v0, :cond_0

    .line 117
    .line 118
    iget-object p1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mFeedBackInfo:Lcom/intsig/advertisement/feedback/FeedBackInfo;

    .line 119
    .line 120
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 121
    .line 122
    invoke-virtual {v0}, Lcom/intsig/advertisement/params/RequestParam;->oO80()I

    .line 123
    .line 124
    .line 125
    move-result v0

    .line 126
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/feedback/FeedBackInfo;->setIndex(I)V

    .line 127
    .line 128
    .line 129
    :cond_0
    return-void
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method public static synthetic Oo08(Lcom/intsig/advertisement/interfaces/RealRequestAbs;ILjava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->lambda$notifyOnFailed$1(ILjava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private getCfgPrice()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/advertisement/params/RequestParam;->OO0o〇〇()F

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private initPriceInfo()V
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getCfgPrice()F

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getRealPrice()F

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    iput v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->realPrice:F

    .line 10
    .line 11
    new-instance v1, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v2, "price info: cfgPrice = "

    .line 17
    .line 18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    const-string v2, ",realPrice ="

    .line 25
    .line 26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    iget v2, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->realPrice:F

    .line 30
    .line 31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    const/4 v2, 0x1

    .line 39
    invoke-virtual {p0, v2, v1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->printLog(ZLjava/lang/String;)V

    .line 40
    .line 41
    .line 42
    iget v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->realPrice:F

    .line 43
    .line 44
    const/4 v3, 0x0

    .line 45
    const/4 v4, 0x0

    .line 46
    cmpl-float v5, v1, v4

    .line 47
    .line 48
    if-lez v5, :cond_1

    .line 49
    .line 50
    cmpl-float v4, v0, v4

    .line 51
    .line 52
    if-lez v4, :cond_1

    .line 53
    .line 54
    cmpg-float v0, v1, v0

    .line 55
    .line 56
    if-gez v0, :cond_0

    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_0
    const/4 v2, 0x0

    .line 60
    :goto_0
    iput-boolean v2, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->isDropByPrice:Z

    .line 61
    .line 62
    goto :goto_1

    .line 63
    :cond_1
    iput-boolean v3, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->isDropByPrice:Z

    .line 64
    .line 65
    :goto_1
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private isValidateContext(Landroid/content/Context;)Z
    .locals 2

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    const/4 p1, 0x0

    .line 4
    return p1

    .line 5
    :cond_0
    instance-of v0, p1, Landroid/app/Activity;

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    if-eqz v0, :cond_1

    .line 9
    .line 10
    invoke-virtual {p0, p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->isActivityFinish(Landroid/content/Context;)Z

    .line 11
    .line 12
    .line 13
    move-result p1

    .line 14
    xor-int/2addr p1, v1

    .line 15
    return p1

    .line 16
    :cond_1
    return v1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private synthetic lambda$notifyOnFailed$1(ILjava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyFail(ILjava/lang/String;)V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->selectClearRequestListener()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private synthetic lambda$notifyOnShowSucceed$2(Lcom/intsig/advertisement/listener/OnAdShowListener;)V
    .locals 0

    .line 1
    invoke-interface {p1, p0}, Lcom/intsig/advertisement/listener/OnAdShowListener;->o〇0(Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private synthetic lambda$notifyOnSucceed$0()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyAllSucceed()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private notifyAllSucceed()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/advertisement/logagent/LogAgentManager;->〇8o8o〇()Lcom/intsig/advertisement/logagent/LogAgentManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {p0, v0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->addOnAdShowListener(Lcom/intsig/advertisement/listener/OnAdShowListener;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/advertisement/params/RequestParam;->〇O8o08O()Lcom/intsig/advertisement/enums/PositionType;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    sget-object v1, Lcom/intsig/advertisement/enums/PositionType;->CachePool:Lcom/intsig/advertisement/enums/PositionType;

    .line 15
    .line 16
    if-ne v0, v1, :cond_0

    .line 17
    .line 18
    sget-object v0, Lcom/intsig/advertisement/adapters/positions/CachePoolManager;->〇O00:Lcom/intsig/advertisement/adapters/positions/CachePoolManager$Companion;

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/positions/CachePoolManager$Companion;->〇080()Lcom/intsig/advertisement/adapters/positions/CachePoolManager;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/positions/CachePoolManager;->o〇O()Lcom/intsig/advertisement/listener/OnAdShowListener;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    invoke-virtual {p0, v0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->addOnAdShowListener(Lcom/intsig/advertisement/listener/OnAdShowListener;)V

    .line 29
    .line 30
    .line 31
    :cond_0
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mFeedBackInfo:Lcom/intsig/advertisement/feedback/FeedBackInfo;

    .line 32
    .line 33
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 34
    .line 35
    invoke-virtual {v1}, Lcom/intsig/advertisement/params/RequestParam;->〇8o8o〇()Ljava/lang/String;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/feedback/FeedBackInfo;->setPlacementId(Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    const/4 v0, 0x1

    .line 43
    const-string v1, " request succeed"

    .line 44
    .line 45
    invoke-virtual {p0, v0, v1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->printLog(ZLjava/lang/String;)V

    .line 46
    .line 47
    .line 48
    sget-object v0, Lcom/intsig/advertisement/enums/RequestState;->succeed:Lcom/intsig/advertisement/enums/RequestState;

    .line 49
    .line 50
    iput-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mState:Lcom/intsig/advertisement/enums/RequestState;

    .line 51
    .line 52
    new-instance v0, Ljava/util/ArrayList;

    .line 53
    .line 54
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestListeners:Ljava/util/ArrayList;

    .line 55
    .line 56
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 57
    .line 58
    .line 59
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 64
    .line 65
    .line 66
    move-result v1

    .line 67
    if-eqz v1, :cond_1

    .line 68
    .line 69
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 70
    .line 71
    .line 72
    move-result-object v1

    .line 73
    check-cast v1, Lcom/intsig/advertisement/listener/OnAdRequestListener;

    .line 74
    .line 75
    invoke-interface {v1, p0}, Lcom/intsig/advertisement/listener/OnAdRequestListener;->O8(Ljava/lang/Object;)V

    .line 76
    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_1
    invoke-direct {p0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->selectClearRequestListener()V

    .line 80
    .line 81
    .line 82
    return-void
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private notifyFail(ILjava/lang/String;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestListeners:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_1

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Lcom/intsig/advertisement/listener/OnAdRequestListener;

    .line 18
    .line 19
    instance-of v2, v1, Lcom/intsig/advertisement/logagent/LogAgentManager;

    .line 20
    .line 21
    if-eqz v2, :cond_0

    .line 22
    .line 23
    iget-boolean v2, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->isDropByPrice:Z

    .line 24
    .line 25
    if-eqz v2, :cond_0

    .line 26
    .line 27
    goto :goto_1

    .line 28
    :cond_0
    invoke-interface {v1, p1, p2, p0}, Lcom/intsig/advertisement/listener/OnAdRequestListener;->〇80〇808〇O(ILjava/lang/String;Ljava/lang/Object;)V

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->selectClearRequestListener()V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static synthetic o〇0(Lcom/intsig/advertisement/interfaces/RealRequestAbs;Lcom/intsig/advertisement/listener/OnAdShowListener;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->lambda$notifyOnShowSucceed$2(Lcom/intsig/advertisement/listener/OnAdShowListener;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private selectClearRequestListener()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestListeners:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_1

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    instance-of v1, v1, Lcom/intsig/advertisement/logagent/LogAgentManager;

    .line 18
    .line 19
    if-eqz v1, :cond_0

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_1
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static synthetic 〇〇888(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->lambda$notifyOnSucceed$0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public addAdRequestListener(Lcom/intsig/advertisement/listener/OnAdRequestListener;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestListeners:Ljava/util/ArrayList;

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public addOnAdShowListener(Lcom/intsig/advertisement/listener/OnAdShowListener;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mAdShowListeners:Ljava/util/ArrayList;

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public destroy()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getCacheType()Lcom/intsig/advertisement/enums/CacheType;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/advertisement/enums/CacheType;->StrongReference:Lcom/intsig/advertisement/enums/CacheType;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getData()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TAdData;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getExtraValue(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mExtraMap:Ljava/util/HashMap;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mExtraMap:Ljava/util/HashMap;

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    return-object p1

    .line 18
    :cond_0
    const/4 p1, 0x0

    .line 19
    return-object p1
    .line 20
    .line 21
    .line 22
.end method

.method public getFeedBackInfo()Lcom/intsig/advertisement/feedback/FeedBackInfo;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mFeedBackInfo:Lcom/intsig/advertisement/feedback/FeedBackInfo;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getFillDur()J
    .locals 4

    .line 1
    iget-wide v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mFillTime:J

    .line 2
    .line 3
    iget-wide v2, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestTime:J

    .line 4
    .line 5
    sub-long/2addr v0, v2

    .line 6
    return-wide v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getFillTime()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mFillTime:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getInteractType()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getPreShowPosition()Lcom/intsig/advertisement/enums/PositionType;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->preShowPosition:Lcom/intsig/advertisement/enums/PositionType;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getPrice()F
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->realPrice:F

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    cmpl-float v1, v0, v1

    .line 5
    .line 6
    if-lez v1, :cond_0

    .line 7
    .line 8
    return v0

    .line 9
    :cond_0
    invoke-direct {p0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getCfgPrice()F

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
.end method

.method protected getRealPrice()F
    .locals 1

    .line 1
    sget v0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->INVALIDATE_PRICE:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getRequestParam()Lcom/intsig/advertisement/params/RequestParam;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TParam;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getRequestState()Lcom/intsig/advertisement/enums/RequestState;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mState:Lcom/intsig/advertisement/enums/RequestState;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getShowDur()J
    .locals 4

    .line 1
    iget-wide v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mShowTime:J

    .line 2
    .line 3
    iget-wide v2, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestTime:J

    .line 4
    .line 5
    sub-long/2addr v0, v2

    .line 6
    invoke-static {v0, v1}, Lcom/intsig/advertisement/util/CommonUtil;->o〇0(J)J

    .line 7
    .line 8
    .line 9
    move-result-wide v0

    .line 10
    return-wide v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getTag()Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->tag:Ljava/lang/Object;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getTrackRealPrice()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->realPrice:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public init()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method protected isActivityFinish(Landroid/content/Context;)Z
    .locals 2

    .line 1
    instance-of v0, p1, Landroid/app/Activity;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-eqz v0, :cond_1

    .line 5
    .line 6
    check-cast p1, Landroid/app/Activity;

    .line 7
    .line 8
    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-nez v0, :cond_1

    .line 13
    .line 14
    invoke-virtual {p1}, Landroid/app/Activity;->isDestroyed()Z

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    if-eqz p1, :cond_0

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 v1, 0x0

    .line 22
    :cond_1
    :goto_0
    return v1
.end method

.method public isCacheExpire()Z
    .locals 5

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-virtual {p0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->getFillTime()J

    .line 6
    .line 7
    .line 8
    move-result-wide v2

    .line 9
    sub-long/2addr v0, v2

    .line 10
    const-wide/16 v2, 0x3e8

    .line 11
    .line 12
    div-long/2addr v0, v2

    .line 13
    const-wide/16 v2, 0x3c

    .line 14
    .line 15
    div-long/2addr v0, v2

    .line 16
    iget-object v2, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 17
    .line 18
    invoke-virtual {v2}, Lcom/intsig/advertisement/params/RequestParam;->〇o〇()I

    .line 19
    .line 20
    .line 21
    move-result v2

    .line 22
    int-to-long v2, v2

    .line 23
    cmp-long v4, v0, v2

    .line 24
    .line 25
    if-lez v4, :cond_0

    .line 26
    .line 27
    const/4 v0, 0x1

    .line 28
    goto :goto_0

    .line 29
    :cond_0
    const/4 v0, 0x0

    .line 30
    :goto_0
    return v0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public isHasNotifyShow()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->hasNotifyShow:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public isHasUsed()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->hasUsed:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public isPriceFromResponse()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public isShowClose()Ljava/lang/Boolean;
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->hasShowClose:Z

    .line 2
    .line 3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public notifyBidResult(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/advertisement/params/RequestParam;->〇〇8O0〇8()Lcom/intsig/advertisement/enums/SourceType;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sget-object v1, Lcom/intsig/advertisement/enums/SourceType;->Tencent:Lcom/intsig/advertisement/enums/SourceType;

    .line 8
    .line 9
    if-ne v0, v1, :cond_0

    .line 10
    .line 11
    new-instance v0, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string/jumbo v1, "tencent bid result state = "

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 26
    .line 27
    .line 28
    move-result-object p1

    .line 29
    const/4 v0, 0x1

    .line 30
    invoke-virtual {p0, v0, p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->printLog(ZLjava/lang/String;)V

    .line 31
    .line 32
    .line 33
    :cond_0
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public notifyForVirImpression()V
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    const-string v1, "notifyForVirImpression"

    .line 3
    .line 4
    invoke-virtual {p0, v0, v1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->printLog(ZLjava/lang/String;)V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public notifyOnClick()V
    .locals 2

    .line 1
    const-string v0, " on click"

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-virtual {p0, v1, v0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->printLog(ZLjava/lang/String;)V

    .line 5
    .line 6
    .line 7
    sput-boolean v1, Lcom/intsig/advertisement/control/AdConfigManager;->o〇0:Z

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mAdShowListeners:Ljava/util/ArrayList;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-eqz v1, :cond_0

    .line 20
    .line 21
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    check-cast v1, Lcom/intsig/advertisement/listener/OnAdShowListener;

    .line 26
    .line 27
    invoke-interface {v1, p0}, Lcom/intsig/advertisement/listener/OnAdShowListener;->〇o〇(Ljava/lang/Object;)V

    .line 28
    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public notifyOnClose()V
    .locals 2

    .line 1
    const-string v0, " on close"

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    invoke-virtual {p0, v1, v0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->printLog(ZLjava/lang/String;)V

    .line 5
    .line 6
    .line 7
    iput-boolean v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->hasShowClose:Z

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mAdShowListeners:Ljava/util/ArrayList;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    if-eqz v1, :cond_0

    .line 20
    .line 21
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    check-cast v1, Lcom/intsig/advertisement/listener/OnAdShowListener;

    .line 26
    .line 27
    invoke-interface {v1, p0}, Lcom/intsig/advertisement/listener/OnAdShowListener;->〇〇888(Ljava/lang/Object;)V

    .line 28
    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public notifyOnFailed(ILjava/lang/String;)V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->hasLoadNotify:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    const-string p2, "has notify"

    .line 7
    .line 8
    invoke-virtual {p0, p1, p2}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->printLog(ZLjava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    const/4 v0, 0x1

    .line 13
    iput-boolean v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->hasLoadNotify:Z

    .line 14
    .line 15
    new-instance v1, Ljava/lang/StringBuilder;

    .line 16
    .line 17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .line 19
    .line 20
    const-string v2, "request fail errorCode="

    .line 21
    .line 22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 26
    .line 27
    .line 28
    const-string v2, ",errorMsg="

    .line 29
    .line 30
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    invoke-virtual {p0, v0, v1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->printLog(ZLjava/lang/String;)V

    .line 41
    .line 42
    .line 43
    sget-object v0, Lcom/intsig/advertisement/enums/RequestState;->failed:Lcom/intsig/advertisement/enums/RequestState;

    .line 44
    .line 45
    iput-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mState:Lcom/intsig/advertisement/enums/RequestState;

    .line 46
    .line 47
    new-instance v0, Lcom/intsig/advertisement/interfaces/〇o00〇〇Oo;

    .line 48
    .line 49
    invoke-direct {v0, p0, p1, p2}, Lcom/intsig/advertisement/interfaces/〇o00〇〇Oo;-><init>(Lcom/intsig/advertisement/interfaces/RealRequestAbs;ILjava/lang/String;)V

    .line 50
    .line 51
    .line 52
    invoke-static {v0}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdUtil;->o800o8O(Ljava/lang/Runnable;)V

    .line 53
    .line 54
    .line 55
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

.method public notifyOnShowFailed(ILjava/lang/String;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, " on show fail  errorCode="

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    const-string v1, ",errorMsg="

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const/4 v1, 0x1

    .line 27
    invoke-virtual {p0, v1, v0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->printLog(ZLjava/lang/String;)V

    .line 28
    .line 29
    .line 30
    iput-boolean v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->hasShowClose:Z

    .line 31
    .line 32
    iput-boolean v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->hasNotifyShow:Z

    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mAdShowListeners:Ljava/util/ArrayList;

    .line 35
    .line 36
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    if-eqz v1, :cond_0

    .line 45
    .line 46
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 47
    .line 48
    .line 49
    move-result-object v1

    .line 50
    check-cast v1, Lcom/intsig/advertisement/listener/OnAdShowListener;

    .line 51
    .line 52
    invoke-interface {v1, p1, p2, p0}, Lcom/intsig/advertisement/listener/OnAdShowListener;->OO0o〇〇〇〇0(ILjava/lang/String;Ljava/lang/Object;)V

    .line 53
    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_0
    return-void
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

.method public notifyOnShowSucceed()V
    .locals 5

    .line 1
    iget-boolean v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->hasNotifyShow:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const-string v0, " on show"

    .line 7
    .line 8
    const/4 v1, 0x1

    .line 9
    invoke-virtual {p0, v1, v0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->printLog(ZLjava/lang/String;)V

    .line 10
    .line 11
    .line 12
    iput-boolean v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->hasNotifyShow:Z

    .line 13
    .line 14
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 15
    .line 16
    .line 17
    move-result-wide v0

    .line 18
    iput-wide v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mShowTime:J

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mAdShowListeners:Ljava/util/ArrayList;

    .line 21
    .line 22
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    if-eqz v1, :cond_2

    .line 31
    .line 32
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    check-cast v1, Lcom/intsig/advertisement/listener/OnAdShowListener;

    .line 37
    .line 38
    instance-of v2, v1, Lcom/intsig/advertisement/logagent/LogAgentManager;

    .line 39
    .line 40
    if-eqz v2, :cond_1

    .line 41
    .line 42
    iget-object v2, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 43
    .line 44
    invoke-virtual {v2}, Lcom/intsig/advertisement/params/RequestParam;->〇〇8O0〇8()Lcom/intsig/advertisement/enums/SourceType;

    .line 45
    .line 46
    .line 47
    move-result-object v2

    .line 48
    sget-object v3, Lcom/intsig/advertisement/enums/SourceType;->Admob:Lcom/intsig/advertisement/enums/SourceType;

    .line 49
    .line 50
    if-ne v2, v3, :cond_1

    .line 51
    .line 52
    new-instance v2, Lcom/intsig/advertisement/interfaces/〇o〇;

    .line 53
    .line 54
    invoke-direct {v2, p0, v1}, Lcom/intsig/advertisement/interfaces/〇o〇;-><init>(Lcom/intsig/advertisement/interfaces/RealRequestAbs;Lcom/intsig/advertisement/listener/OnAdShowListener;)V

    .line 55
    .line 56
    .line 57
    const-wide/16 v3, 0x1f4

    .line 58
    .line 59
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    invoke-static {v2, v1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdUtil;->OoO8(Ljava/lang/Runnable;Ljava/lang/Long;)V

    .line 64
    .line 65
    .line 66
    goto :goto_0

    .line 67
    :cond_1
    invoke-interface {v1, p0}, Lcom/intsig/advertisement/listener/OnAdShowListener;->o〇0(Ljava/lang/Object;)V

    .line 68
    .line 69
    .line 70
    goto :goto_0

    .line 71
    :cond_2
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 72
    .line 73
    invoke-virtual {v0}, Lcom/intsig/advertisement/params/RequestParam;->OoO8()Z

    .line 74
    .line 75
    .line 76
    move-result v0

    .line 77
    if-eqz v0, :cond_3

    .line 78
    .line 79
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 84
    .line 85
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/record/AdRecordHelper;->o8(Lcom/intsig/advertisement/params/RequestParam;)V

    .line 86
    .line 87
    .line 88
    :cond_3
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 89
    .line 90
    invoke-virtual {v0}, Lcom/intsig/advertisement/params/RequestParam;->〇O8o08O()Lcom/intsig/advertisement/enums/PositionType;

    .line 91
    .line 92
    .line 93
    move-result-object v0

    .line 94
    sget-object v1, Lcom/intsig/advertisement/enums/PositionType;->LotteryVideo:Lcom/intsig/advertisement/enums/PositionType;

    .line 95
    .line 96
    if-eq v0, v1, :cond_4

    .line 97
    .line 98
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 99
    .line 100
    invoke-virtual {v0}, Lcom/intsig/advertisement/params/RequestParam;->〇O8o08O()Lcom/intsig/advertisement/enums/PositionType;

    .line 101
    .line 102
    .line 103
    move-result-object v0

    .line 104
    sget-object v1, Lcom/intsig/advertisement/enums/PositionType;->RewardedVideo:Lcom/intsig/advertisement/enums/PositionType;

    .line 105
    .line 106
    if-eq v0, v1, :cond_4

    .line 107
    .line 108
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 109
    .line 110
    .line 111
    move-result-object v0

    .line 112
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 113
    .line 114
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇〇〇0〇〇0(Lcom/intsig/advertisement/params/RequestParam;)V

    .line 115
    .line 116
    .line 117
    invoke-static {}, Lcom/intsig/advertisement/record/SessionRecorder;->getInstance()Lcom/intsig/advertisement/record/SessionRecorder;

    .line 118
    .line 119
    .line 120
    move-result-object v0

    .line 121
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 122
    .line 123
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/record/SessionRecorder;->recordShow(Lcom/intsig/advertisement/params/RequestParam;)V

    .line 124
    .line 125
    .line 126
    :cond_4
    return-void
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method protected notifyOnStart()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestListeners:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_0

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Lcom/intsig/advertisement/listener/OnAdRequestListener;

    .line 18
    .line 19
    invoke-interface {v1, p0}, Lcom/intsig/advertisement/listener/OnAdRequestListener;->〇080(Ljava/lang/Object;)V

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method protected notifyOnSucceed()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->initPriceInfo()V

    .line 2
    .line 3
    .line 4
    iget-boolean v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->hasLoadNotify:Z

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    const/4 v0, 0x0

    .line 9
    const-string v1, "has notify"

    .line 10
    .line 11
    invoke-virtual {p0, v0, v1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->printLog(ZLjava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    const/4 v0, 0x1

    .line 16
    iput-boolean v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->hasLoadNotify:Z

    .line 17
    .line 18
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 19
    .line 20
    .line 21
    move-result-wide v0

    .line 22
    iput-wide v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mFillTime:J

    .line 23
    .line 24
    iget-boolean v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->isDropByPrice:Z

    .line 25
    .line 26
    if-eqz v0, :cond_1

    .line 27
    .line 28
    invoke-static {}, Lcom/intsig/advertisement/logagent/LogAgentManager;->〇8o8o〇()Lcom/intsig/advertisement/logagent/LogAgentManager;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    invoke-virtual {v0, p0}, Lcom/intsig/advertisement/logagent/LogAgentManager;->O8ooOoo〇(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V

    .line 33
    .line 34
    .line 35
    invoke-static {}, Lcom/intsig/advertisement/logagent/LogAgentManager;->〇8o8o〇()Lcom/intsig/advertisement/logagent/LogAgentManager;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    invoke-virtual {v0, p0}, Lcom/intsig/advertisement/logagent/LogAgentManager;->Oooo8o0〇(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V

    .line 40
    .line 41
    .line 42
    sget v0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->BID_FAIL_LOW_PRICE:I

    .line 43
    .line 44
    invoke-virtual {p0, v0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyBidResult(I)V

    .line 45
    .line 46
    .line 47
    const/16 v0, 0x18a

    .line 48
    .line 49
    const-string v1, "drop by price"

    .line 50
    .line 51
    invoke-virtual {p0, v0, v1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyOnFailed(ILjava/lang/String;)V

    .line 52
    .line 53
    .line 54
    return-void

    .line 55
    :cond_1
    new-instance v0, Lcom/intsig/advertisement/interfaces/〇080;

    .line 56
    .line 57
    invoke-direct {v0, p0}, Lcom/intsig/advertisement/interfaces/〇080;-><init>(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V

    .line 58
    .line 59
    .line 60
    invoke-static {v0}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdUtil;->o800o8O(Ljava/lang/Runnable;)V

    .line 61
    .line 62
    .line 63
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public onCancelRequest()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method protected abstract onRequest(Landroid/content/Context;)V
.end method

.method protected printLog(ZLjava/lang/String;)V
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    iget-object p1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mTag:Ljava/lang/String;

    .line 4
    .line 5
    invoke-static {p1, p2}, Lcom/intsig/advertisement/logagent/LogPrinter;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    iget-object p1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mTag:Ljava/lang/String;

    .line 10
    .line 11
    invoke-static {p1, p2}, Lcom/intsig/advertisement/logagent/LogPrinter;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    :goto_0
    return-void
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public setHasUsed(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->hasUsed:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setPreShowPosition(Lcom/intsig/advertisement/enums/PositionType;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->preShowPosition:Lcom/intsig/advertisement/enums/PositionType;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setTag(Ljava/lang/Object;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->tag:Ljava/lang/Object;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public startRequest(Landroid/content/Context;)V
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    const-string/jumbo v1, "start requesting"

    .line 3
    .line 4
    .line 5
    invoke-virtual {p0, v0, v1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->printLog(ZLjava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {p0}, Lcom/intsig/advertisement/control/AdRequestingManager;->〇o00〇〇Oo(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-nez v0, :cond_0

    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyOnStart()V

    .line 16
    .line 17
    .line 18
    sget-object v0, Lcom/intsig/advertisement/enums/RequestState;->requesting:Lcom/intsig/advertisement/enums/RequestState;

    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mState:Lcom/intsig/advertisement/enums/RequestState;

    .line 21
    .line 22
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 23
    .line 24
    .line 25
    move-result-wide v0

    .line 26
    iput-wide v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestTime:J

    .line 27
    .line 28
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->isValidateContext(Landroid/content/Context;)Z

    .line 29
    .line 30
    .line 31
    move-result v0

    .line 32
    if-eqz v0, :cond_1

    .line 33
    .line 34
    invoke-virtual {p0, p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->onRequest(Landroid/content/Context;)V

    .line 35
    .line 36
    .line 37
    goto :goto_0

    .line 38
    :cond_1
    const/4 p1, -0x1

    .line 39
    const-string v0, "context is invalidate"

    .line 40
    .line 41
    invoke-virtual {p0, p1, v0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyOnFailed(ILjava/lang/String;)V

    .line 42
    .line 43
    .line 44
    :goto_0
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

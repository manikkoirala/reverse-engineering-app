.class public abstract Lcom/intsig/advertisement/interfaces/RewardIntersRequest;
.super Lcom/intsig/advertisement/interfaces/RealRequestAbs;
.source "RewardIntersRequest.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<AdData:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/intsig/advertisement/interfaces/RealRequestAbs<",
        "Lcom/intsig/advertisement/params/RewardIntersParam;",
        "Ljava/lang/Object;",
        "TAdData;>;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private param:Lcom/intsig/advertisement/params/RewardIntersParam;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/intsig/advertisement/params/RewardIntersParam;)V
    .locals 1
    .param p1    # Lcom/intsig/advertisement/params/RewardIntersParam;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "param"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;-><init>(Lcom/intsig/advertisement/params/RequestParam;)V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/advertisement/interfaces/RewardIntersRequest;->param:Lcom/intsig/advertisement/params/RewardIntersParam;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public final getParam()Lcom/intsig/advertisement/params/RewardIntersParam;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RewardIntersRequest;->param:Lcom/intsig/advertisement/params/RewardIntersParam;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final setParam(Lcom/intsig/advertisement/params/RewardIntersParam;)V
    .locals 1
    .param p1    # Lcom/intsig/advertisement/params/RewardIntersParam;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/advertisement/interfaces/RewardIntersRequest;->param:Lcom/intsig/advertisement/params/RewardIntersParam;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public showInterstitialAd(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-static {}, Lcom/intsig/advertisement/logagent/LogAgentManager;->〇8o8o〇()Lcom/intsig/advertisement/logagent/LogAgentManager;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    invoke-virtual {v0, p0}, Lcom/intsig/advertisement/logagent/LogAgentManager;->〇oo〇(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V

    .line 11
    .line 12
    .line 13
    invoke-static {p1, p0}, Lcom/intsig/advertisement/util/AdViewShotUtil;->O8(Landroid/content/Context;Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.class public abstract Lcom/intsig/advertisement/interfaces/RewardVideoRequest;
.super Lcom/intsig/advertisement/interfaces/RealRequestAbs;
.source "RewardVideoRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<AdData:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/intsig/advertisement/interfaces/RealRequestAbs<",
        "Lcom/intsig/advertisement/params/RewardVideoParam;",
        "Lcom/intsig/advertisement/listener/OnRewardVideoAdListener;",
        "TAdData;>;"
    }
.end annotation


# instance fields
.field private hasNotifyReward:Z


# direct methods
.method public constructor <init>(Lcom/intsig/advertisement/params/RewardVideoParam;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;-><init>(Lcom/intsig/advertisement/params/RequestParam;)V

    .line 2
    .line 3
    .line 4
    const/4 p1, 0x0

    .line 5
    iput-boolean p1, p0, Lcom/intsig/advertisement/interfaces/RewardVideoRequest;->hasNotifyReward:Z

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method protected notifyOnReward()V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lcom/intsig/advertisement/interfaces/RewardVideoRequest;->hasNotifyReward:Z

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    const/4 v0, 0x1

    .line 7
    iput-boolean v0, p0, Lcom/intsig/advertisement/interfaces/RewardVideoRequest;->hasNotifyReward:Z

    .line 8
    .line 9
    const-string v1, "on reward"

    .line 10
    .line 11
    invoke-virtual {p0, v0, v1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->printLog(ZLjava/lang/String;)V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mAdShowListeners:Ljava/util/ArrayList;

    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    if-eqz v1, :cond_2

    .line 25
    .line 26
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    check-cast v1, Lcom/intsig/advertisement/listener/OnAdShowListener;

    .line 31
    .line 32
    instance-of v2, v1, Lcom/intsig/advertisement/listener/OnRewardVideoAdListener;

    .line 33
    .line 34
    if-eqz v2, :cond_1

    .line 35
    .line 36
    check-cast v1, Lcom/intsig/advertisement/listener/OnRewardVideoAdListener;

    .line 37
    .line 38
    invoke-interface {v1, p0}, Lcom/intsig/advertisement/listener/OnRewardVideoAdListener;->oO80(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_2
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method protected onPlayVideo(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-static {}, Lcom/intsig/advertisement/logagent/LogAgentManager;->〇8o8o〇()Lcom/intsig/advertisement/logagent/LogAgentManager;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    invoke-virtual {p1, p0}, Lcom/intsig/advertisement/logagent/LogAgentManager;->〇oo〇(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public startPlayVideo(Landroid/content/Context;)V
    .locals 2

    .line 1
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/intsig/advertisement/interfaces/RewardVideoRequest;->onPlayVideo(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2
    .line 3
    .line 4
    goto :goto_0

    .line 5
    :catch_0
    move-exception p1

    .line 6
    sget-object v0, Lcom/intsig/advertisement/crash/AdCrashManager;->〇080:Lcom/intsig/advertisement/crash/AdCrashManager;

    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 9
    .line 10
    check-cast v1, Lcom/intsig/advertisement/params/RewardVideoParam;

    .line 11
    .line 12
    invoke-virtual {v1}, Lcom/intsig/advertisement/params/RequestParam;->〇〇8O0〇8()Lcom/intsig/advertisement/enums/SourceType;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-virtual {v0, v1, p1}, Lcom/intsig/advertisement/crash/AdCrashManager;->〇080(Lcom/intsig/advertisement/enums/SourceType;Ljava/lang/Exception;)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0}, Lcom/intsig/advertisement/interfaces/RewardVideoRequest;->notifyOnReward()V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyOnClose()V

    .line 23
    .line 24
    .line 25
    :goto_0
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

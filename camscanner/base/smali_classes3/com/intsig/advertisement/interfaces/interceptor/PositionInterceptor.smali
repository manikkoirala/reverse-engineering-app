.class public Lcom/intsig/advertisement/interfaces/interceptor/PositionInterceptor;
.super Ljava/lang/Object;
.source "PositionInterceptor.java"

# interfaces
.implements Lcom/intsig/advertisement/interfaces/interceptor/InterceptorInterface;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/intsig/advertisement/interfaces/interceptor/InterceptorInterface<",
        "Lcom/intsig/advertisement/adapters/AbsPositionAdapter;",
        "Lcom/intsig/advertisement/listener/OnAdItemRequestListener;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private O8(ILcom/intsig/advertisement/bean/IntervalModel;)J
    .locals 4

    .line 1
    if-gtz p1, :cond_0

    .line 2
    .line 3
    const-wide/16 p1, 0x0

    .line 4
    .line 5
    return-wide p1

    .line 6
    :cond_0
    int-to-double v0, p1

    .line 7
    iget-wide v2, p2, Lcom/intsig/advertisement/bean/IntervalModel;->b:D

    .line 8
    .line 9
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    .line 10
    .line 11
    .line 12
    move-result-wide v0

    .line 13
    iget-wide v2, p2, Lcom/intsig/advertisement/bean/IntervalModel;->a:D

    .line 14
    .line 15
    mul-double v2, v2, v0

    .line 16
    .line 17
    iget-wide p1, p2, Lcom/intsig/advertisement/bean/IntervalModel;->c:D

    .line 18
    .line 19
    add-double/2addr v2, p1

    .line 20
    new-instance p1, Ljava/lang/StringBuilder;

    .line 21
    .line 22
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 23
    .line 24
    .line 25
    const-string p2, "model 2 interval = "

    .line 26
    .line 27
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 28
    .line 29
    .line 30
    invoke-virtual {p1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 31
    .line 32
    .line 33
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    const-string p2, "PositionInterceptor"

    .line 38
    .line 39
    invoke-static {p2, p1}, Lcom/intsig/advertisement/logagent/LogPrinter;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    double-to-long p1, v2

    .line 43
    return-wide p1
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private Oo08(ILcom/intsig/advertisement/bean/IntervalModel;)J
    .locals 4

    .line 1
    if-gtz p1, :cond_0

    .line 2
    .line 3
    const-wide/16 p1, 0x0

    .line 4
    .line 5
    return-wide p1

    .line 6
    :cond_0
    iget-wide v0, p2, Lcom/intsig/advertisement/bean/IntervalModel;->a:D

    .line 7
    .line 8
    int-to-double v2, p1

    .line 9
    mul-double v0, v0, v2

    .line 10
    .line 11
    iget-wide p1, p2, Lcom/intsig/advertisement/bean/IntervalModel;->b:D

    .line 12
    .line 13
    add-double/2addr v0, p1

    .line 14
    new-instance p1, Ljava/lang/StringBuilder;

    .line 15
    .line 16
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 17
    .line 18
    .line 19
    const-string p2, "model 3 interval = "

    .line 20
    .line 21
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    const-string p2, "PositionInterceptor"

    .line 32
    .line 33
    invoke-static {p2, p1}, Lcom/intsig/advertisement/logagent/LogPrinter;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    double-to-long p1, v0

    .line 37
    return-wide p1
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private oO80(ILcom/intsig/advertisement/bean/IntervalModel;)I
    .locals 5

    .line 1
    iget-wide v0, p2, Lcom/intsig/advertisement/bean/IntervalModel;->z:D

    .line 2
    .line 3
    const-wide/16 v2, 0x0

    .line 4
    .line 5
    cmpl-double v4, v0, v2

    .line 6
    .line 7
    if-lez v4, :cond_0

    .line 8
    .line 9
    iget-wide v2, p2, Lcom/intsig/advertisement/bean/IntervalModel;->c:D

    .line 10
    .line 11
    sub-double/2addr v0, v2

    .line 12
    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    .line 13
    .line 14
    .line 15
    move-result-wide v0

    .line 16
    iget-wide v2, p2, Lcom/intsig/advertisement/bean/IntervalModel;->a:D

    .line 17
    .line 18
    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    .line 19
    .line 20
    .line 21
    move-result-wide v2

    .line 22
    div-double/2addr v0, v2

    .line 23
    iget-wide p1, p2, Lcom/intsig/advertisement/bean/IntervalModel;->b:D

    .line 24
    .line 25
    div-double/2addr v0, p1

    .line 26
    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    .line 27
    .line 28
    .line 29
    move-result-wide p1

    .line 30
    double-to-int p1, p1

    .line 31
    add-int/lit8 p1, p1, 0x1

    .line 32
    .line 33
    :cond_0
    return p1
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private o〇0(ILcom/intsig/advertisement/bean/IntervalModel;)J
    .locals 2

    .line 1
    if-gtz p1, :cond_0

    .line 2
    .line 3
    const-wide/16 p1, 0x0

    .line 4
    .line 5
    return-wide p1

    .line 6
    :cond_0
    iget-object p2, p2, Lcom/intsig/advertisement/bean/IntervalModel;->disperse_array:[I

    .line 7
    .line 8
    if-eqz p2, :cond_1

    .line 9
    .line 10
    array-length v0, p2

    .line 11
    if-lez v0, :cond_1

    .line 12
    .line 13
    add-int/lit8 p1, p1, -0x1

    .line 14
    .line 15
    array-length v0, p2

    .line 16
    rem-int/2addr p1, v0

    .line 17
    aget p1, p2, p1

    .line 18
    .line 19
    int-to-double p1, p1

    .line 20
    goto :goto_0

    .line 21
    :cond_1
    const-wide p1, 0x404e0ccccccccccdL    # 60.1

    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    .line 27
    .line 28
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 29
    .line 30
    .line 31
    const-string v1, "model 4 interval = "

    .line 32
    .line 33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 34
    .line 35
    .line 36
    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v0

    .line 43
    const-string v1, "PositionInterceptor"

    .line 44
    .line 45
    invoke-static {v1, v0}, Lcom/intsig/advertisement/logagent/LogPrinter;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    double-to-long p1, p1

    .line 49
    return-wide p1
.end method

.method private 〇80〇808〇O(Lcom/intsig/advertisement/enums/PositionType;Lcom/intsig/advertisement/bean/PosFlowCfg;)J
    .locals 1

    .line 1
    invoke-virtual {p2}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getIntervalModel()Lcom/intsig/advertisement/bean/IntervalModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-direct {p0, p1, p2}, Lcom/intsig/advertisement/interfaces/interceptor/PositionInterceptor;->〇o00〇〇Oo(Lcom/intsig/advertisement/enums/PositionType;Lcom/intsig/advertisement/bean/PosFlowCfg;)J

    .line 8
    .line 9
    .line 10
    move-result-wide p1

    .line 11
    return-wide p1

    .line 12
    :cond_0
    invoke-virtual {p2}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getMin_interval()I

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    int-to-long p1, p1

    .line 17
    return-wide p1
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private 〇O8o08O([Ljava/lang/String;)Z
    .locals 5

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_2

    .line 3
    .line 4
    array-length v1, p1

    .line 5
    if-nez v1, :cond_0

    .line 6
    .line 7
    goto :goto_1

    .line 8
    :cond_0
    sget-object v1, Landroid/os/Build;->BRAND:Ljava/lang/String;

    .line 9
    .line 10
    array-length v2, p1

    .line 11
    const/4 v3, 0x0

    .line 12
    :goto_0
    if-ge v3, v2, :cond_2

    .line 13
    .line 14
    aget-object v4, p1, v3

    .line 15
    .line 16
    invoke-static {v4, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 17
    .line 18
    .line 19
    move-result v4

    .line 20
    if-eqz v4, :cond_1

    .line 21
    .line 22
    const/4 p1, 0x1

    .line 23
    return p1

    .line 24
    :cond_1
    add-int/lit8 v3, v3, 0x1

    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_2
    :goto_1
    return v0
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private 〇o00〇〇Oo(Lcom/intsig/advertisement/enums/PositionType;Lcom/intsig/advertisement/bean/PosFlowCfg;)J
    .locals 3

    .line 1
    invoke-virtual {p2}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getIntervalModel()Lcom/intsig/advertisement/bean/IntervalModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_3

    .line 6
    .line 7
    iget v1, v0, Lcom/intsig/advertisement/bean/IntervalModel;->model:I

    .line 8
    .line 9
    if-lez v1, :cond_3

    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    invoke-virtual {v2, p1}, Lcom/intsig/advertisement/record/AdRecordHelper;->O8ooOoo〇(Lcom/intsig/advertisement/enums/PositionType;)I

    .line 16
    .line 17
    .line 18
    move-result p1

    .line 19
    const/4 v2, 0x1

    .line 20
    if-ne v1, v2, :cond_0

    .line 21
    .line 22
    invoke-direct {p0, p1, v0}, Lcom/intsig/advertisement/interfaces/interceptor/PositionInterceptor;->〇o〇(ILcom/intsig/advertisement/bean/IntervalModel;)J

    .line 23
    .line 24
    .line 25
    move-result-wide p1

    .line 26
    return-wide p1

    .line 27
    :cond_0
    const/4 v2, 0x2

    .line 28
    if-ne v1, v2, :cond_1

    .line 29
    .line 30
    invoke-direct {p0, p1, v0}, Lcom/intsig/advertisement/interfaces/interceptor/PositionInterceptor;->O8(ILcom/intsig/advertisement/bean/IntervalModel;)J

    .line 31
    .line 32
    .line 33
    move-result-wide p1

    .line 34
    return-wide p1

    .line 35
    :cond_1
    const/4 v2, 0x3

    .line 36
    if-ne v1, v2, :cond_2

    .line 37
    .line 38
    invoke-direct {p0, p1, v0}, Lcom/intsig/advertisement/interfaces/interceptor/PositionInterceptor;->Oo08(ILcom/intsig/advertisement/bean/IntervalModel;)J

    .line 39
    .line 40
    .line 41
    move-result-wide p1

    .line 42
    return-wide p1

    .line 43
    :cond_2
    const/4 v2, 0x4

    .line 44
    if-ne v1, v2, :cond_3

    .line 45
    .line 46
    invoke-direct {p0, p1, v0}, Lcom/intsig/advertisement/interfaces/interceptor/PositionInterceptor;->o〇0(ILcom/intsig/advertisement/bean/IntervalModel;)J

    .line 47
    .line 48
    .line 49
    move-result-wide p1

    .line 50
    return-wide p1

    .line 51
    :cond_3
    invoke-virtual {p2}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getMin_interval()I

    .line 52
    .line 53
    .line 54
    move-result p1

    .line 55
    int-to-long p1, p1

    .line 56
    return-wide p1
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

.method private 〇o〇(ILcom/intsig/advertisement/bean/IntervalModel;)J
    .locals 6

    .line 1
    if-gtz p1, :cond_0

    .line 2
    .line 3
    const-wide/16 p1, 0x0

    .line 4
    .line 5
    return-wide p1

    .line 6
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/intsig/advertisement/interfaces/interceptor/PositionInterceptor;->oO80(ILcom/intsig/advertisement/bean/IntervalModel;)I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    rem-int v1, p1, v0

    .line 11
    .line 12
    int-to-double v1, v1

    .line 13
    iget-wide v3, p2, Lcom/intsig/advertisement/bean/IntervalModel;->b:D

    .line 14
    .line 15
    mul-double v3, v3, v1

    .line 16
    .line 17
    iget-wide v1, p2, Lcom/intsig/advertisement/bean/IntervalModel;->a:D

    .line 18
    .line 19
    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->pow(DD)D

    .line 20
    .line 21
    .line 22
    move-result-wide v1

    .line 23
    iget-wide v3, p2, Lcom/intsig/advertisement/bean/IntervalModel;->c:D

    .line 24
    .line 25
    add-double/2addr v1, v3

    .line 26
    iget-wide v3, p2, Lcom/intsig/advertisement/bean/IntervalModel;->z:D

    .line 27
    .line 28
    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->min(DD)D

    .line 29
    .line 30
    .line 31
    move-result-wide v3

    .line 32
    new-instance p2, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    const-string v5, "model 1 interval = "

    .line 38
    .line 39
    invoke-virtual {p2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {p2, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    const-string v5, ",showCount="

    .line 46
    .line 47
    invoke-virtual {p2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    const-string p1, ",reverseCount="

    .line 54
    .line 55
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    const-string p1, ",y="

    .line 62
    .line 63
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    invoke-virtual {p2, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object p1

    .line 73
    const-string p2, "PositionInterceptor"

    .line 74
    .line 75
    invoke-static {p2, p1}, Lcom/intsig/advertisement/logagent/LogPrinter;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    .line 77
    .line 78
    double-to-long p1, v3

    .line 79
    return-wide p1
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

.method private 〇〇888(Lcom/intsig/advertisement/enums/PositionType;I)I
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/advertisement/enums/PositionType;->MainMiddleBanner:Lcom/intsig/advertisement/enums/PositionType;

    .line 2
    .line 3
    if-ne p1, v0, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x1

    .line 6
    invoke-static {p2, p1}, Ljava/lang/Math;->max(II)I

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    return p1

    .line 11
    :cond_0
    return p2
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method


# virtual methods
.method public OO0o〇〇〇〇0(Landroid/content/Context;Lcom/intsig/advertisement/adapters/AbsPositionAdapter;Lcom/intsig/advertisement/listener/OnAdItemRequestListener;)Z
    .locals 16

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    move-object/from16 v1, p3

    .line 4
    .line 5
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->o〇O8〇〇o()Lcom/intsig/advertisement/bean/PosFlowCfg;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->o〇〇0〇()Lcom/intsig/advertisement/enums/PositionType;

    .line 10
    .line 11
    .line 12
    move-result-object v3

    .line 13
    invoke-virtual {v3}, Lcom/intsig/advertisement/enums/PositionType;->getPositionId()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v3

    .line 17
    const/4 v4, 0x1

    .line 18
    const/4 v5, 0x0

    .line 19
    if-eqz v2, :cond_17

    .line 20
    .line 21
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getBanners()[Lcom/intsig/advertisement/bean/ItemConfig;

    .line 22
    .line 23
    .line 24
    move-result-object v6

    .line 25
    if-eqz v6, :cond_17

    .line 26
    .line 27
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getBanners()[Lcom/intsig/advertisement/bean/ItemConfig;

    .line 28
    .line 29
    .line 30
    move-result-object v6

    .line 31
    array-length v6, v6

    .line 32
    if-nez v6, :cond_0

    .line 33
    .line 34
    goto/16 :goto_2

    .line 35
    .line 36
    :cond_0
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getBrandBlacklist()[Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v6

    .line 40
    invoke-direct {v0, v6}, Lcom/intsig/advertisement/interfaces/interceptor/PositionInterceptor;->〇O8o08O([Ljava/lang/String;)Z

    .line 41
    .line 42
    .line 43
    move-result v6

    .line 44
    if-eqz v6, :cond_2

    .line 45
    .line 46
    if-eqz v1, :cond_1

    .line 47
    .line 48
    new-instance v2, Ljava/lang/StringBuilder;

    .line 49
    .line 50
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 51
    .line 52
    .line 53
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    const-string v3, " PosInterceptor current device is in blacklist"

    .line 57
    .line 58
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 59
    .line 60
    .line 61
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 62
    .line 63
    .line 64
    move-result-object v2

    .line 65
    const/16 v3, 0xc1

    .line 66
    .line 67
    invoke-interface {v1, v5, v3, v2}, Lcom/intsig/advertisement/listener/OnAdItemRequestListener;->〇080(IILjava/lang/String;)V

    .line 68
    .line 69
    .line 70
    :cond_1
    return v4

    .line 71
    :cond_2
    invoke-static/range {p1 .. p1}, Lcom/intsig/advertisement/util/CommonUtil;->Oo08(Landroid/content/Context;)I

    .line 72
    .line 73
    .line 74
    move-result v6

    .line 75
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getGray()I

    .line 76
    .line 77
    .line 78
    move-result v7

    .line 79
    if-eqz v7, :cond_15

    .line 80
    .line 81
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getGray()I

    .line 82
    .line 83
    .line 84
    move-result v7

    .line 85
    if-le v6, v7, :cond_3

    .line 86
    .line 87
    goto/16 :goto_1

    .line 88
    .line 89
    :cond_3
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 90
    .line 91
    .line 92
    move-result-object v6

    .line 93
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->o〇〇0〇()Lcom/intsig/advertisement/enums/PositionType;

    .line 94
    .line 95
    .line 96
    move-result-object v7

    .line 97
    invoke-virtual {v6, v7}, Lcom/intsig/advertisement/record/AdRecordHelper;->O8ooOoo〇(Lcom/intsig/advertisement/enums/PositionType;)I

    .line 98
    .line 99
    .line 100
    move-result v6

    .line 101
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getMax_impression()I

    .line 102
    .line 103
    .line 104
    move-result v7

    .line 105
    if-lt v6, v7, :cond_5

    .line 106
    .line 107
    if-eqz v1, :cond_4

    .line 108
    .line 109
    new-instance v2, Ljava/lang/StringBuilder;

    .line 110
    .line 111
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 112
    .line 113
    .line 114
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    const-string v3, " PosInterceptor max count is reach max="

    .line 118
    .line 119
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    .line 121
    .line 122
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 123
    .line 124
    .line 125
    const-string v3, ",hasNum="

    .line 126
    .line 127
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    .line 129
    .line 130
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 131
    .line 132
    .line 133
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 134
    .line 135
    .line 136
    move-result-object v2

    .line 137
    const/16 v3, 0xc5

    .line 138
    .line 139
    invoke-interface {v1, v5, v3, v2}, Lcom/intsig/advertisement/listener/OnAdItemRequestListener;->〇080(IILjava/lang/String;)V

    .line 140
    .line 141
    .line 142
    :cond_4
    return v4

    .line 143
    :cond_5
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 144
    .line 145
    .line 146
    move-result-object v6

    .line 147
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->o〇〇0〇()Lcom/intsig/advertisement/enums/PositionType;

    .line 148
    .line 149
    .line 150
    move-result-object v7

    .line 151
    invoke-virtual {v6, v7}, Lcom/intsig/advertisement/record/AdRecordHelper;->O〇8O8〇008(Lcom/intsig/advertisement/enums/PositionType;)J

    .line 152
    .line 153
    .line 154
    move-result-wide v6

    .line 155
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 156
    .line 157
    .line 158
    move-result-wide v8

    .line 159
    sub-long/2addr v8, v6

    .line 160
    const-wide/16 v10, 0x3e8

    .line 161
    .line 162
    div-long/2addr v8, v10

    .line 163
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->o〇〇0〇()Lcom/intsig/advertisement/enums/PositionType;

    .line 164
    .line 165
    .line 166
    move-result-object v12

    .line 167
    invoke-direct {v0, v12, v2}, Lcom/intsig/advertisement/interfaces/interceptor/PositionInterceptor;->〇80〇808〇O(Lcom/intsig/advertisement/enums/PositionType;Lcom/intsig/advertisement/bean/PosFlowCfg;)J

    .line 168
    .line 169
    .line 170
    move-result-wide v12

    .line 171
    cmp-long v14, v12, v8

    .line 172
    .line 173
    if-lez v14, :cond_7

    .line 174
    .line 175
    if-eqz v1, :cond_6

    .line 176
    .line 177
    new-instance v2, Ljava/lang/StringBuilder;

    .line 178
    .line 179
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 180
    .line 181
    .line 182
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 183
    .line 184
    .line 185
    const-string v3, " PosInterceptor not meet interval , min_show="

    .line 186
    .line 187
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    .line 189
    .line 190
    invoke-virtual {v2, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 191
    .line 192
    .line 193
    const-string v3, ",lastShow="

    .line 194
    .line 195
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    .line 197
    .line 198
    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 199
    .line 200
    .line 201
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 202
    .line 203
    .line 204
    move-result-object v2

    .line 205
    const/16 v3, 0xc4

    .line 206
    .line 207
    invoke-interface {v1, v5, v3, v2}, Lcom/intsig/advertisement/listener/OnAdItemRequestListener;->〇080(IILjava/lang/String;)V

    .line 208
    .line 209
    .line 210
    :cond_6
    return v4

    .line 211
    :cond_7
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->o〇〇0〇()Lcom/intsig/advertisement/enums/PositionType;

    .line 212
    .line 213
    .line 214
    move-result-object v8

    .line 215
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getRelateInterval()Ljava/util/HashMap;

    .line 216
    .line 217
    .line 218
    move-result-object v9

    .line 219
    invoke-virtual {v0, v8, v9}, Lcom/intsig/advertisement/interfaces/interceptor/PositionInterceptor;->〇8o8o〇(Lcom/intsig/advertisement/enums/PositionType;Ljava/util/HashMap;)Z

    .line 220
    .line 221
    .line 222
    move-result v8

    .line 223
    if-nez v8, :cond_9

    .line 224
    .line 225
    if-eqz v1, :cond_8

    .line 226
    .line 227
    new-instance v2, Ljava/lang/StringBuilder;

    .line 228
    .line 229
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 230
    .line 231
    .line 232
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 233
    .line 234
    .line 235
    const-string v3, " PosInterceptor not meet relate interval"

    .line 236
    .line 237
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 238
    .line 239
    .line 240
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 241
    .line 242
    .line 243
    move-result-object v2

    .line 244
    const/16 v3, 0xc0

    .line 245
    .line 246
    invoke-interface {v1, v5, v3, v2}, Lcom/intsig/advertisement/listener/OnAdItemRequestListener;->〇080(IILjava/lang/String;)V

    .line 247
    .line 248
    .line 249
    :cond_8
    return v4

    .line 250
    :cond_9
    sget-object v8, Lcom/intsig/advertisement/control/AdConfigManager;->〇o〇:Lcom/intsig/advertisement/control/AdInfoCallback;

    .line 251
    .line 252
    const-wide/16 v12, 0x0

    .line 253
    .line 254
    if-eqz v8, :cond_a

    .line 255
    .line 256
    move-object/from16 v9, p1

    .line 257
    .line 258
    invoke-interface {v8, v9}, Lcom/intsig/advertisement/control/AdInfoCallback;->o〇0(Landroid/content/Context;)J

    .line 259
    .line 260
    .line 261
    move-result-wide v8

    .line 262
    goto :goto_0

    .line 263
    :cond_a
    move-wide v8, v12

    .line 264
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 265
    .line 266
    .line 267
    move-result-wide v14

    .line 268
    sub-long/2addr v14, v8

    .line 269
    div-long/2addr v14, v10

    .line 270
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getInit_show_after()I

    .line 271
    .line 272
    .line 273
    move-result v8

    .line 274
    int-to-long v8, v8

    .line 275
    cmp-long v10, v14, v8

    .line 276
    .line 277
    if-gez v10, :cond_c

    .line 278
    .line 279
    if-eqz v1, :cond_b

    .line 280
    .line 281
    new-instance v6, Ljava/lang/StringBuilder;

    .line 282
    .line 283
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 284
    .line 285
    .line 286
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 287
    .line 288
    .line 289
    const-string v3, " PosInterceptor not meet new user , hasInstall="

    .line 290
    .line 291
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 292
    .line 293
    .line 294
    invoke-virtual {v6, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 295
    .line 296
    .line 297
    const-string v3, ",Init_show_after="

    .line 298
    .line 299
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300
    .line 301
    .line 302
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getInit_show_after()I

    .line 303
    .line 304
    .line 305
    move-result v2

    .line 306
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 307
    .line 308
    .line 309
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 310
    .line 311
    .line 312
    move-result-object v2

    .line 313
    const/16 v3, 0xc3

    .line 314
    .line 315
    invoke-interface {v1, v5, v3, v2}, Lcom/intsig/advertisement/listener/OnAdItemRequestListener;->〇080(IILjava/lang/String;)V

    .line 316
    .line 317
    .line 318
    :cond_b
    return v4

    .line 319
    :cond_c
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 320
    .line 321
    .line 322
    move-result-object v8

    .line 323
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->o〇〇0〇()Lcom/intsig/advertisement/enums/PositionType;

    .line 324
    .line 325
    .line 326
    move-result-object v9

    .line 327
    invoke-virtual {v8, v9}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇oOO8O8(Lcom/intsig/advertisement/enums/PositionType;)I

    .line 328
    .line 329
    .line 330
    move-result v8

    .line 331
    const-wide/16 v9, 0x1

    .line 332
    .line 333
    cmp-long v11, v6, v9

    .line 334
    .line 335
    if-gez v11, :cond_e

    .line 336
    .line 337
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getInit_skip_after()I

    .line 338
    .line 339
    .line 340
    move-result v9

    .line 341
    if-le v9, v8, :cond_e

    .line 342
    .line 343
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 344
    .line 345
    .line 346
    move-result-object v6

    .line 347
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->o〇〇0〇()Lcom/intsig/advertisement/enums/PositionType;

    .line 348
    .line 349
    .line 350
    move-result-object v7

    .line 351
    invoke-virtual {v6, v7}, Lcom/intsig/advertisement/record/AdRecordHelper;->o〇0(Lcom/intsig/advertisement/enums/PositionType;)V

    .line 352
    .line 353
    .line 354
    if-eqz v1, :cond_d

    .line 355
    .line 356
    new-instance v6, Ljava/lang/StringBuilder;

    .line 357
    .line 358
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 359
    .line 360
    .line 361
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 362
    .line 363
    .line 364
    const-string v3, " PosInterceptor not meet skipTime , total="

    .line 365
    .line 366
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 367
    .line 368
    .line 369
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getInit_skip_after()I

    .line 370
    .line 371
    .line 372
    move-result v2

    .line 373
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 374
    .line 375
    .line 376
    const-string v2, ",hasSkip="

    .line 377
    .line 378
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 379
    .line 380
    .line 381
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 382
    .line 383
    .line 384
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 385
    .line 386
    .line 387
    move-result-object v2

    .line 388
    const/16 v3, 0xc2

    .line 389
    .line 390
    invoke-interface {v1, v5, v3, v2}, Lcom/intsig/advertisement/listener/OnAdItemRequestListener;->〇080(IILjava/lang/String;)V

    .line 391
    .line 392
    .line 393
    :cond_d
    return v4

    .line 394
    :cond_e
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 395
    .line 396
    .line 397
    move-result-object v8

    .line 398
    invoke-virtual {v8}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇〇8O0〇8()I

    .line 399
    .line 400
    .line 401
    move-result v8

    .line 402
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->o〇〇0〇()Lcom/intsig/advertisement/enums/PositionType;

    .line 403
    .line 404
    .line 405
    move-result-object v9

    .line 406
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getMin_doc_num()I

    .line 407
    .line 408
    .line 409
    move-result v10

    .line 410
    invoke-direct {v0, v9, v10}, Lcom/intsig/advertisement/interfaces/interceptor/PositionInterceptor;->〇〇888(Lcom/intsig/advertisement/enums/PositionType;I)I

    .line 411
    .line 412
    .line 413
    move-result v9

    .line 414
    if-lez v9, :cond_10

    .line 415
    .line 416
    if-le v9, v8, :cond_10

    .line 417
    .line 418
    if-eqz v1, :cond_f

    .line 419
    .line 420
    new-instance v2, Ljava/lang/StringBuilder;

    .line 421
    .line 422
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 423
    .line 424
    .line 425
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 426
    .line 427
    .line 428
    const-string v3, " PosInterceptor not meet min doc count , docCount="

    .line 429
    .line 430
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 431
    .line 432
    .line 433
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 434
    .line 435
    .line 436
    const-string v3, ",cfgCount="

    .line 437
    .line 438
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 439
    .line 440
    .line 441
    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 442
    .line 443
    .line 444
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 445
    .line 446
    .line 447
    move-result-object v2

    .line 448
    const/16 v3, 0xbd

    .line 449
    .line 450
    invoke-interface {v1, v5, v3, v2}, Lcom/intsig/advertisement/listener/OnAdItemRequestListener;->〇080(IILjava/lang/String;)V

    .line 451
    .line 452
    .line 453
    :cond_f
    return v4

    .line 454
    :cond_10
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->o〇〇0〇()Lcom/intsig/advertisement/enums/PositionType;

    .line 455
    .line 456
    .line 457
    move-result-object v8

    .line 458
    sget-object v9, Lcom/intsig/advertisement/enums/PositionType;->ShotDone:Lcom/intsig/advertisement/enums/PositionType;

    .line 459
    .line 460
    if-ne v8, v9, :cond_14

    .line 461
    .line 462
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 463
    .line 464
    .line 465
    move-result-object v8

    .line 466
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->o〇〇0〇()Lcom/intsig/advertisement/enums/PositionType;

    .line 467
    .line 468
    .line 469
    move-result-object v9

    .line 470
    invoke-virtual {v8, v9}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O00(Lcom/intsig/advertisement/enums/PositionType;)I

    .line 471
    .line 472
    .line 473
    move-result v8

    .line 474
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getSkipTimeDaily()I

    .line 475
    .line 476
    .line 477
    move-result v9

    .line 478
    if-ge v8, v9, :cond_12

    .line 479
    .line 480
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 481
    .line 482
    .line 483
    move-result-object v2

    .line 484
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->o〇〇0〇()Lcom/intsig/advertisement/enums/PositionType;

    .line 485
    .line 486
    .line 487
    move-result-object v6

    .line 488
    invoke-virtual {v2, v6}, Lcom/intsig/advertisement/record/AdRecordHelper;->O8(Lcom/intsig/advertisement/enums/PositionType;)V

    .line 489
    .line 490
    .line 491
    if-eqz v1, :cond_11

    .line 492
    .line 493
    new-instance v2, Ljava/lang/StringBuilder;

    .line 494
    .line 495
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 496
    .line 497
    .line 498
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 499
    .line 500
    .line 501
    const-string v3, " PosInterceptor not meet skipTimedDaily , todaySkip="

    .line 502
    .line 503
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 504
    .line 505
    .line 506
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 507
    .line 508
    .line 509
    const-string v3, ",todaySkipCfg="

    .line 510
    .line 511
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 512
    .line 513
    .line 514
    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 515
    .line 516
    .line 517
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 518
    .line 519
    .line 520
    move-result-object v2

    .line 521
    const/16 v3, 0xbf

    .line 522
    .line 523
    invoke-interface {v1, v5, v3, v2}, Lcom/intsig/advertisement/listener/OnAdItemRequestListener;->〇080(IILjava/lang/String;)V

    .line 524
    .line 525
    .line 526
    :cond_11
    return v4

    .line 527
    :cond_12
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 528
    .line 529
    .line 530
    move-result-object v8

    .line 531
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->o〇〇0〇()Lcom/intsig/advertisement/enums/PositionType;

    .line 532
    .line 533
    .line 534
    move-result-object v9

    .line 535
    invoke-virtual {v8, v9}, Lcom/intsig/advertisement/record/AdRecordHelper;->oo88o8O(Lcom/intsig/advertisement/enums/PositionType;)I

    .line 536
    .line 537
    .line 538
    move-result v8

    .line 539
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getShowSkipNum()I

    .line 540
    .line 541
    .line 542
    move-result v2

    .line 543
    cmp-long v9, v6, v12

    .line 544
    .line 545
    if-lez v9, :cond_14

    .line 546
    .line 547
    if-ge v8, v2, :cond_14

    .line 548
    .line 549
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 550
    .line 551
    .line 552
    move-result-object v6

    .line 553
    invoke-virtual/range {p2 .. p2}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->o〇〇0〇()Lcom/intsig/advertisement/enums/PositionType;

    .line 554
    .line 555
    .line 556
    move-result-object v7

    .line 557
    invoke-virtual {v6, v7}, Lcom/intsig/advertisement/record/AdRecordHelper;->Oo08(Lcom/intsig/advertisement/enums/PositionType;)V

    .line 558
    .line 559
    .line 560
    if-eqz v1, :cond_13

    .line 561
    .line 562
    new-instance v6, Ljava/lang/StringBuilder;

    .line 563
    .line 564
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 565
    .line 566
    .line 567
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 568
    .line 569
    .line 570
    const-string v3, " PosInterceptor not meet skipTimedInterval , skipInterval="

    .line 571
    .line 572
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 573
    .line 574
    .line 575
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 576
    .line 577
    .line 578
    const-string v3, ",skipIntervalCfg="

    .line 579
    .line 580
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 581
    .line 582
    .line 583
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 584
    .line 585
    .line 586
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 587
    .line 588
    .line 589
    move-result-object v2

    .line 590
    const/16 v3, 0xbe

    .line 591
    .line 592
    invoke-interface {v1, v5, v3, v2}, Lcom/intsig/advertisement/listener/OnAdItemRequestListener;->〇080(IILjava/lang/String;)V

    .line 593
    .line 594
    .line 595
    :cond_13
    return v4

    .line 596
    :cond_14
    return v5

    .line 597
    :cond_15
    :goto_1
    if-eqz v1, :cond_16

    .line 598
    .line 599
    new-instance v7, Ljava/lang/StringBuilder;

    .line 600
    .line 601
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 602
    .line 603
    .line 604
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 605
    .line 606
    .line 607
    const-string v3, " PosInterceptor intercept gray="

    .line 608
    .line 609
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 610
    .line 611
    .line 612
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 613
    .line 614
    .line 615
    const-string v3, ",cfg="

    .line 616
    .line 617
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 618
    .line 619
    .line 620
    invoke-virtual {v2}, Lcom/intsig/advertisement/bean/PosFlowCfg;->getGray()I

    .line 621
    .line 622
    .line 623
    move-result v2

    .line 624
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 625
    .line 626
    .line 627
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 628
    .line 629
    .line 630
    move-result-object v2

    .line 631
    const/16 v3, 0xc6

    .line 632
    .line 633
    invoke-interface {v1, v5, v3, v2}, Lcom/intsig/advertisement/listener/OnAdItemRequestListener;->〇080(IILjava/lang/String;)V

    .line 634
    .line 635
    .line 636
    :cond_16
    return v4

    .line 637
    :cond_17
    :goto_2
    if-eqz v1, :cond_18

    .line 638
    .line 639
    new-instance v2, Ljava/lang/StringBuilder;

    .line 640
    .line 641
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 642
    .line 643
    .line 644
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 645
    .line 646
    .line 647
    const-string v3, " PosInterceptor posConfig is null or empty"

    .line 648
    .line 649
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 650
    .line 651
    .line 652
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 653
    .line 654
    .line 655
    move-result-object v2

    .line 656
    const/16 v3, 0xc7

    .line 657
    .line 658
    invoke-interface {v1, v5, v3, v2}, Lcom/intsig/advertisement/listener/OnAdItemRequestListener;->〇080(IILjava/lang/String;)V

    .line 659
    .line 660
    .line 661
    :cond_18
    return v4
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
.end method

.method public bridge synthetic 〇080(Landroid/content/Context;Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 0

    .line 1
    check-cast p2, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;

    .line 2
    .line 3
    check-cast p3, Lcom/intsig/advertisement/listener/OnAdItemRequestListener;

    .line 4
    .line 5
    invoke-virtual {p0, p1, p2, p3}, Lcom/intsig/advertisement/interfaces/interceptor/PositionInterceptor;->OO0o〇〇〇〇0(Landroid/content/Context;Lcom/intsig/advertisement/adapters/AbsPositionAdapter;Lcom/intsig/advertisement/listener/OnAdItemRequestListener;)Z

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    return p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public 〇8o8o〇(Lcom/intsig/advertisement/enums/PositionType;Ljava/util/HashMap;)Z
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/advertisement/enums/PositionType;",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)Z"
        }
    .end annotation

    .line 1
    const/4 v0, 0x1

    .line 2
    if-eqz p2, :cond_6

    .line 3
    .line 4
    invoke-virtual {p2}, Ljava/util/HashMap;->size()I

    .line 5
    .line 6
    .line 7
    move-result v1

    .line 8
    if-nez v1, :cond_0

    .line 9
    .line 10
    goto/16 :goto_2

    .line 11
    .line 12
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 13
    .line 14
    .line 15
    move-result-wide v1

    .line 16
    invoke-virtual {p2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    .line 17
    .line 18
    .line 19
    move-result-object p2

    .line 20
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    .line 21
    .line 22
    .line 23
    move-result-object p2

    .line 24
    const-wide/16 v3, 0x0

    .line 25
    .line 26
    const-string v5, ""

    .line 27
    .line 28
    move-object v7, v5

    .line 29
    move-wide v5, v3

    .line 30
    :cond_1
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    .line 31
    .line 32
    .line 33
    move-result v8

    .line 34
    if-eqz v8, :cond_5

    .line 35
    .line 36
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    move-result-object v8

    .line 40
    check-cast v8, Ljava/util/Map$Entry;

    .line 41
    .line 42
    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 43
    .line 44
    .line 45
    move-result-object v9

    .line 46
    check-cast v9, Ljava/lang/String;

    .line 47
    .line 48
    const-string v10, "app_cold_launch"

    .line 49
    .line 50
    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 51
    .line 52
    .line 53
    move-result v10

    .line 54
    if-eqz v10, :cond_2

    .line 55
    .line 56
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 57
    .line 58
    .line 59
    move-result-object v9

    .line 60
    sget-object v10, Lcom/intsig/advertisement/enums/AppLaunchType;->ColdBoot:Lcom/intsig/advertisement/enums/AppLaunchType;

    .line 61
    .line 62
    invoke-virtual {v9, v10}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇80〇808〇O(Lcom/intsig/advertisement/enums/AppLaunchType;)J

    .line 63
    .line 64
    .line 65
    move-result-wide v9

    .line 66
    goto :goto_1

    .line 67
    :cond_2
    const-string v10, "app_launch"

    .line 68
    .line 69
    invoke-static {v9, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 70
    .line 71
    .line 72
    move-result v10

    .line 73
    if-eqz v10, :cond_3

    .line 74
    .line 75
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 76
    .line 77
    .line 78
    move-result-object v9

    .line 79
    sget-object v10, Lcom/intsig/advertisement/enums/AppLaunchType;->WarmBoot:Lcom/intsig/advertisement/enums/AppLaunchType;

    .line 80
    .line 81
    invoke-virtual {v9, v10}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇80〇808〇O(Lcom/intsig/advertisement/enums/AppLaunchType;)J

    .line 82
    .line 83
    .line 84
    move-result-wide v9

    .line 85
    goto :goto_1

    .line 86
    :cond_3
    invoke-static {v9}, Lcom/intsig/advertisement/enums/PositionType;->getPosByCfgMapId(Ljava/lang/String;)Lcom/intsig/advertisement/enums/PositionType;

    .line 87
    .line 88
    .line 89
    move-result-object v9

    .line 90
    sget-object v10, Lcom/intsig/advertisement/enums/PositionType;->Unknown:Lcom/intsig/advertisement/enums/PositionType;

    .line 91
    .line 92
    if-ne v9, v10, :cond_4

    .line 93
    .line 94
    goto :goto_0

    .line 95
    :cond_4
    invoke-static {}, Lcom/intsig/advertisement/record/AdRecordHelper;->〇O888o0o()Lcom/intsig/advertisement/record/AdRecordHelper;

    .line 96
    .line 97
    .line 98
    move-result-object v10

    .line 99
    invoke-virtual {v10, v9}, Lcom/intsig/advertisement/record/AdRecordHelper;->O〇8O8〇008(Lcom/intsig/advertisement/enums/PositionType;)J

    .line 100
    .line 101
    .line 102
    move-result-wide v9

    .line 103
    :goto_1
    cmp-long v11, v9, v3

    .line 104
    .line 105
    if-lez v11, :cond_1

    .line 106
    .line 107
    invoke-interface {v8}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    .line 108
    .line 109
    .line 110
    move-result-object v3

    .line 111
    check-cast v3, Ljava/lang/Integer;

    .line 112
    .line 113
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    .line 114
    .line 115
    .line 116
    move-result v3

    .line 117
    int-to-long v3, v3

    .line 118
    invoke-interface {v8}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 119
    .line 120
    .line 121
    move-result-object v5

    .line 122
    check-cast v5, Ljava/lang/String;

    .line 123
    .line 124
    move-object v7, v5

    .line 125
    move-wide v5, v3

    .line 126
    move-wide v3, v9

    .line 127
    goto :goto_0

    .line 128
    :cond_5
    sub-long/2addr v1, v3

    .line 129
    const-wide/16 v3, 0x3e8

    .line 130
    .line 131
    div-long/2addr v1, v3

    .line 132
    cmp-long p2, v1, v5

    .line 133
    .line 134
    if-gez p2, :cond_6

    .line 135
    .line 136
    new-instance p2, Ljava/lang/StringBuilder;

    .line 137
    .line 138
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 139
    .line 140
    .line 141
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 142
    .line 143
    .line 144
    const-string p1, " not meet relate interval,dis="

    .line 145
    .line 146
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    .line 148
    .line 149
    invoke-virtual {p2, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 150
    .line 151
    .line 152
    const-string p1, ",relate pos = "

    .line 153
    .line 154
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    .line 156
    .line 157
    invoke-virtual {p2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    .line 159
    .line 160
    const-string p1, ",value = "

    .line 161
    .line 162
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    .line 164
    .line 165
    invoke-virtual {p2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 166
    .line 167
    .line 168
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 169
    .line 170
    .line 171
    move-result-object p1

    .line 172
    const-string p2, "PositionInterceptor"

    .line 173
    .line 174
    invoke-static {p2, p1}, Lcom/intsig/advertisement/logagent/LogPrinter;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    .line 176
    .line 177
    const/4 p1, 0x0

    .line 178
    return p1

    .line 179
    :cond_6
    :goto_2
    return v0
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

.class public final Lcom/intsig/advertisement/generate/AdSourceAdapterFactory;
.super Ljava/lang/Object;
.source "AdSourceAdapterFactory.java"


# static fields
.field private static final 〇080:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/advertisement/generate/AdSourceAdapterFactory;->〇080:Ljava/util/HashMap;

    .line 7
    .line 8
    new-instance v1, Lcom/intsig/advertisement/adapters/sources/vungle/VungleAdapter;

    .line 9
    .line 10
    invoke-direct {v1}, Lcom/intsig/advertisement/adapters/sources/vungle/VungleAdapter;-><init>()V

    .line 11
    .line 12
    .line 13
    const-string/jumbo v2, "vungle"

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    new-instance v1, Lcom/intsig/advertisement/adapters/sources/pangle/PangleAdapter;

    .line 20
    .line 21
    invoke-direct {v1}, Lcom/intsig/advertisement/adapters/sources/pangle/PangleAdapter;-><init>()V

    .line 22
    .line 23
    .line 24
    const-string v2, "pangle_sdk"

    .line 25
    .line 26
    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    new-instance v1, Lcom/intsig/advertisement/adapters/sources/cs/CsAdapter;

    .line 30
    .line 31
    invoke-direct {v1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdapter;-><init>()V

    .line 32
    .line 33
    .line 34
    const-string v2, "cs"

    .line 35
    .line 36
    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    new-instance v1, Lcom/intsig/advertisement/adapters/sources/admob/AdmobAdapter;

    .line 40
    .line 41
    invoke-direct {v1}, Lcom/intsig/advertisement/adapters/sources/admob/AdmobAdapter;-><init>()V

    .line 42
    .line 43
    .line 44
    const-string v2, "admob"

    .line 45
    .line 46
    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    .line 48
    .line 49
    new-instance v1, Lcom/intsig/advertisement/adapters/sources/unknown/UnKnownAdapter;

    .line 50
    .line 51
    invoke-direct {v1}, Lcom/intsig/advertisement/adapters/sources/unknown/UnKnownAdapter;-><init>()V

    .line 52
    .line 53
    .line 54
    const-string/jumbo v2, "unknown"

    .line 55
    .line 56
    .line 57
    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    .line 59
    .line 60
    new-instance v1, Lcom/intsig/advertisement/adapters/sources/xiaomi/XiaoMiAdapter;

    .line 61
    .line 62
    invoke-direct {v1}, Lcom/intsig/advertisement/adapters/sources/xiaomi/XiaoMiAdapter;-><init>()V

    .line 63
    .line 64
    .line 65
    const-string/jumbo v2, "xiaomi"

    .line 66
    .line 67
    .line 68
    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    .line 70
    .line 71
    new-instance v1, Lcom/intsig/advertisement/adapters/sources/facebook/FacebookAdapter;

    .line 72
    .line 73
    invoke-direct {v1}, Lcom/intsig/advertisement/adapters/sources/facebook/FacebookAdapter;-><init>()V

    .line 74
    .line 75
    .line 76
    const-string v2, "facebook"

    .line 77
    .line 78
    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    .line 80
    .line 81
    new-instance v1, Lcom/intsig/advertisement/adapters/sources/api/ApiAdapter;

    .line 82
    .line 83
    invoke-direct {v1}, Lcom/intsig/advertisement/adapters/sources/api/ApiAdapter;-><init>()V

    .line 84
    .line 85
    .line 86
    const-string v2, "api"

    .line 87
    .line 88
    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    .line 90
    .line 91
    new-instance v1, Lcom/intsig/advertisement/adapters/sources/applovin/AppLovinAdapter;

    .line 92
    .line 93
    invoke-direct {v1}, Lcom/intsig/advertisement/adapters/sources/applovin/AppLovinAdapter;-><init>()V

    .line 94
    .line 95
    .line 96
    const-string v2, "applovin"

    .line 97
    .line 98
    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    .line 100
    .line 101
    return-void
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public static 〇080(Ljava/lang/String;)Lcom/intsig/advertisement/adapters/AdAbsAdapter;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/advertisement/generate/AdSourceAdapterFactory;->〇080:Ljava/util/HashMap;

    .line 2
    .line 3
    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/advertisement/adapters/AdAbsAdapter;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

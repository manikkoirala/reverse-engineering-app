.class public Lcom/intsig/advertisement/params/AdRequestOptions;
.super Ljava/lang/Object;
.source "AdRequestOptions.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/advertisement/params/AdRequestOptions$Builder;
    }
.end annotation


# instance fields
.field private final O8:I

.field private final Oo08:Lcom/intsig/advertisement/listener/OnAdRequestListener;

.field private oO80:Lcom/intsig/advertisement/interfaces/interceptor/InterceptorInterface;

.field private o〇0:Ljava/lang/Object;

.field private final 〇080:Landroid/content/Context;

.field private 〇80〇808〇O:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final 〇o00〇〇Oo:I

.field private final 〇o〇:I

.field private final 〇〇888:Z


# direct methods
.method private constructor <init>(Lcom/intsig/advertisement/params/AdRequestOptions$Builder;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    invoke-static {p1}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->〇o00〇〇Oo(Lcom/intsig/advertisement/params/AdRequestOptions$Builder;)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/advertisement/params/AdRequestOptions;->〇080:Landroid/content/Context;

    .line 4
    invoke-static {p1}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->o〇0(Lcom/intsig/advertisement/params/AdRequestOptions$Builder;)I

    move-result v0

    iput v0, p0, Lcom/intsig/advertisement/params/AdRequestOptions;->〇o00〇〇Oo:I

    .line 5
    invoke-static {p1}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->〇〇888(Lcom/intsig/advertisement/params/AdRequestOptions$Builder;)I

    move-result v0

    iput v0, p0, Lcom/intsig/advertisement/params/AdRequestOptions;->O8:I

    .line 6
    invoke-static {p1}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->oO80(Lcom/intsig/advertisement/params/AdRequestOptions$Builder;)I

    move-result v0

    iput v0, p0, Lcom/intsig/advertisement/params/AdRequestOptions;->〇o〇:I

    .line 7
    invoke-static {p1}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->〇080(Lcom/intsig/advertisement/params/AdRequestOptions$Builder;)Lcom/intsig/advertisement/listener/OnAdRequestListener;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/advertisement/params/AdRequestOptions;->Oo08:Lcom/intsig/advertisement/listener/OnAdRequestListener;

    .line 8
    invoke-static {p1}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->Oo08(Lcom/intsig/advertisement/params/AdRequestOptions$Builder;)Lcom/intsig/advertisement/interfaces/interceptor/InterceptorInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/advertisement/params/AdRequestOptions;->oO80:Lcom/intsig/advertisement/interfaces/interceptor/InterceptorInterface;

    .line 9
    invoke-static {p1}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->〇o〇(Lcom/intsig/advertisement/params/AdRequestOptions$Builder;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/advertisement/params/AdRequestOptions;->〇80〇808〇O:Ljava/util/HashMap;

    .line 10
    invoke-static {p1}, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->O8(Lcom/intsig/advertisement/params/AdRequestOptions$Builder;)Z

    move-result p1

    iput-boolean p1, p0, Lcom/intsig/advertisement/params/AdRequestOptions;->〇〇888:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/intsig/advertisement/params/AdRequestOptions$Builder;Lo0O0/〇O8o08O;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/params/AdRequestOptions;-><init>(Lcom/intsig/advertisement/params/AdRequestOptions$Builder;)V

    return-void
.end method


# virtual methods
.method public O8()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/advertisement/params/AdRequestOptions;->〇〇888:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public Oo08(Ljava/lang/Object;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/params/AdRequestOptions;->o〇0:Ljava/lang/Object;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/params/AdRequestOptions;->〇080:Landroid/content/Context;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇080()Lcom/intsig/advertisement/listener/OnAdRequestListener;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/params/AdRequestOptions;->Oo08:Lcom/intsig/advertisement/listener/OnAdRequestListener;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇o00〇〇Oo()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/params/AdRequestOptions;->〇80〇808〇O:Ljava/util/HashMap;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇o〇()Lcom/intsig/advertisement/interfaces/interceptor/InterceptorInterface;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/params/AdRequestOptions;->oO80:Lcom/intsig/advertisement/interfaces/interceptor/InterceptorInterface;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.class public Lcom/intsig/advertisement/params/AdRequestOptions$Builder;
.super Ljava/lang/Object;
.source "AdRequestOptions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/advertisement/params/AdRequestOptions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private O8:I

.field private Oo08:Z

.field private oO80:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private o〇0:Lcom/intsig/advertisement/listener/OnAdRequestListener;

.field private final 〇080:Landroid/content/Context;

.field private 〇o00〇〇Oo:I

.field private 〇o〇:I

.field private 〇〇888:Lcom/intsig/advertisement/interfaces/interceptor/InterceptorInterface;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->Oo08:Z

    .line 6
    .line 7
    iput-object p1, p0, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->〇080:Landroid/content/Context;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic O8(Lcom/intsig/advertisement/params/AdRequestOptions$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->Oo08:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic Oo08(Lcom/intsig/advertisement/params/AdRequestOptions$Builder;)Lcom/intsig/advertisement/interfaces/interceptor/InterceptorInterface;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->〇〇888:Lcom/intsig/advertisement/interfaces/interceptor/InterceptorInterface;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic oO80(Lcom/intsig/advertisement/params/AdRequestOptions$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->〇o〇:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic o〇0(Lcom/intsig/advertisement/params/AdRequestOptions$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->〇o00〇〇Oo:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic 〇080(Lcom/intsig/advertisement/params/AdRequestOptions$Builder;)Lcom/intsig/advertisement/listener/OnAdRequestListener;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->o〇0:Lcom/intsig/advertisement/listener/OnAdRequestListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/advertisement/params/AdRequestOptions$Builder;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->〇080:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/advertisement/params/AdRequestOptions$Builder;)Ljava/util/HashMap;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->oO80:Ljava/util/HashMap;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic 〇〇888(Lcom/intsig/advertisement/params/AdRequestOptions$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->O8:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public OO0o〇〇(I)Lcom/intsig/advertisement/params/AdRequestOptions$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->〇o00〇〇Oo:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public OO0o〇〇〇〇0()Lcom/intsig/advertisement/params/AdRequestOptions$Builder;
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->Oo08:Z

    .line 3
    .line 4
    return-object p0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇80〇808〇O()Lcom/intsig/advertisement/params/AdRequestOptions;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/advertisement/params/AdRequestOptions;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, p0, v1}, Lcom/intsig/advertisement/params/AdRequestOptions;-><init>(Lcom/intsig/advertisement/params/AdRequestOptions$Builder;Lo0O0/〇O8o08O;)V

    .line 5
    .line 6
    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇8o8o〇(Ljava/util/HashMap;)Lcom/intsig/advertisement/params/AdRequestOptions$Builder;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/intsig/advertisement/params/AdRequestOptions$Builder;"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->oO80:Ljava/util/HashMap;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public 〇O8o08O(Lcom/intsig/advertisement/listener/OnAdRequestListener;)Lcom/intsig/advertisement/params/AdRequestOptions$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/params/AdRequestOptions$Builder;->o〇0:Lcom/intsig/advertisement/listener/OnAdRequestListener;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

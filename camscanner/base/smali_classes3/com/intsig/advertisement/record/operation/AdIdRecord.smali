.class public Lcom/intsig/advertisement/record/operation/AdIdRecord;
.super Ljava/lang/Object;
.source "AdIdRecord.java"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation


# instance fields
.field private clickCount:I

.field private closeCount:I

.field private id:Ljava/lang/String;

.field private lastUpdateTime:J

.field private oneDayRecord:Lcom/intsig/advertisement/record/operation/OneDayRecord;

.field private showCount:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/advertisement/record/operation/OneDayRecord;

    .line 5
    .line 6
    invoke-direct {v0}, Lcom/intsig/advertisement/record/operation/OneDayRecord;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/advertisement/record/operation/AdIdRecord;->oneDayRecord:Lcom/intsig/advertisement/record/operation/OneDayRecord;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
.end method


# virtual methods
.method public getClickCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/record/operation/AdIdRecord;->clickCount:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getCloseCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/record/operation/AdIdRecord;->closeCount:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/record/operation/AdIdRecord;->id:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getLastUpdateTime()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/advertisement/record/operation/AdIdRecord;->lastUpdateTime:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getOneDayRecord()Lcom/intsig/advertisement/record/operation/OneDayRecord;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/record/operation/AdIdRecord;->oneDayRecord:Lcom/intsig/advertisement/record/operation/OneDayRecord;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getRelationTimes()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/record/operation/AdIdRecord;->oneDayRecord:Lcom/intsig/advertisement/record/operation/OneDayRecord;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/advertisement/record/operation/OneDayRecord;->getRelationTimes()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getShowCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/record/operation/AdIdRecord;->showCount:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public setClickCount(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/record/operation/AdIdRecord;->clickCount:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setCloseCount(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/record/operation/AdIdRecord;->closeCount:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/record/operation/AdIdRecord;->id:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setLastUpdateTime(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/advertisement/record/operation/AdIdRecord;->lastUpdateTime:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setOneDayRecord(Lcom/intsig/advertisement/record/operation/OneDayRecord;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/record/operation/AdIdRecord;->oneDayRecord:Lcom/intsig/advertisement/record/operation/OneDayRecord;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setRelationTimes(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/record/operation/AdIdRecord;->oneDayRecord:Lcom/intsig/advertisement/record/operation/OneDayRecord;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/advertisement/record/operation/OneDayRecord;->setRelationTimes(I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setShowCount(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/record/operation/AdIdRecord;->showCount:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

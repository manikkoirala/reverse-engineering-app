.class public final Lcom/intsig/advertisement/record/AppLaunchPatch;
.super Ljava/lang/Object;
.source "AppLaunchPatch.kt"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private coolLastShow:J

.field private coolShowCount:I

.field private hotLastShow:J

.field private hotShowCount:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method


# virtual methods
.method public final clear()V
    .locals 2

    .line 1
    const-wide/16 v0, 0x0

    .line 2
    .line 3
    iput-wide v0, p0, Lcom/intsig/advertisement/record/AppLaunchPatch;->coolLastShow:J

    .line 4
    .line 5
    iput-wide v0, p0, Lcom/intsig/advertisement/record/AppLaunchPatch;->hotLastShow:J

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    iput v0, p0, Lcom/intsig/advertisement/record/AppLaunchPatch;->coolShowCount:I

    .line 9
    .line 10
    iput v0, p0, Lcom/intsig/advertisement/record/AppLaunchPatch;->hotShowCount:I

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
.end method

.method public final getLastShowTime(Lcom/intsig/advertisement/enums/AppLaunchType;)J
    .locals 2
    .param p1    # Lcom/intsig/advertisement/enums/AppLaunchType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "launchType"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object v0, Lcom/intsig/advertisement/enums/AppLaunchType;->ColdBoot:Lcom/intsig/advertisement/enums/AppLaunchType;

    .line 7
    .line 8
    if-ne p1, v0, :cond_0

    .line 9
    .line 10
    iget-wide v0, p0, Lcom/intsig/advertisement/record/AppLaunchPatch;->coolLastShow:J

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    iget-wide v0, p0, Lcom/intsig/advertisement/record/AppLaunchPatch;->hotLastShow:J

    .line 14
    .line 15
    :goto_0
    return-wide v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final getShowCount()I
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;->〇80()Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;->Ooo()Lcom/intsig/advertisement/enums/AppLaunchType;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    sget-object v1, Lcom/intsig/advertisement/enums/AppLaunchType;->ColdBoot:Lcom/intsig/advertisement/enums/AppLaunchType;

    .line 10
    .line 11
    if-ne v0, v1, :cond_0

    .line 12
    .line 13
    iget v0, p0, Lcom/intsig/advertisement/record/AppLaunchPatch;->coolShowCount:I

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    iget v0, p0, Lcom/intsig/advertisement/record/AppLaunchPatch;->hotShowCount:I

    .line 17
    .line 18
    :goto_0
    return v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public final recordShow(Lcom/intsig/advertisement/params/RequestParam;)V
    .locals 2

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    invoke-virtual {p1}, Lcom/intsig/advertisement/params/RequestParam;->〇O8o08O()Lcom/intsig/advertisement/enums/PositionType;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    sget-object v0, Lcom/intsig/advertisement/enums/PositionType;->AppLaunch:Lcom/intsig/advertisement/enums/PositionType;

    .line 8
    .line 9
    if-ne p1, v0, :cond_1

    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;->〇80()Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/positions/AppLaunchManager;->Ooo()Lcom/intsig/advertisement/enums/AppLaunchType;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    sget-object v0, Lcom/intsig/advertisement/enums/AppLaunchType;->ColdBoot:Lcom/intsig/advertisement/enums/AppLaunchType;

    .line 20
    .line 21
    if-ne p1, v0, :cond_0

    .line 22
    .line 23
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 24
    .line 25
    .line 26
    move-result-wide v0

    .line 27
    iput-wide v0, p0, Lcom/intsig/advertisement/record/AppLaunchPatch;->coolLastShow:J

    .line 28
    .line 29
    iget p1, p0, Lcom/intsig/advertisement/record/AppLaunchPatch;->coolShowCount:I

    .line 30
    .line 31
    add-int/lit8 p1, p1, 0x1

    .line 32
    .line 33
    iput p1, p0, Lcom/intsig/advertisement/record/AppLaunchPatch;->coolShowCount:I

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 37
    .line 38
    .line 39
    move-result-wide v0

    .line 40
    iput-wide v0, p0, Lcom/intsig/advertisement/record/AppLaunchPatch;->hotLastShow:J

    .line 41
    .line 42
    iget p1, p0, Lcom/intsig/advertisement/record/AppLaunchPatch;->hotShowCount:I

    .line 43
    .line 44
    add-int/lit8 p1, p1, 0x1

    .line 45
    .line 46
    iput p1, p0, Lcom/intsig/advertisement/record/AppLaunchPatch;->hotShowCount:I

    .line 47
    .line 48
    :cond_1
    :goto_0
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.class public final Lcom/intsig/advertisement/util/sensor/AdAccelerometerSensor;
.super Lcom/intsig/advertisement/util/sensor/AdSensorBase;
.source "AdAccelerometerSensor.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private o〇0:Z

.field private 〇〇888:I


# direct methods
.method public constructor <init>(Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;)V
    .locals 1
    .param p1    # Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string/jumbo v0, "style"

    .line 2
    .line 3
    .line 4
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/util/sensor/AdSensorBase;-><init>(Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public O8()I
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇o〇()I
    .locals 1

    .line 1
    sget v0, Lcom/intsig/advertisement/R$raw;->shake_accelerometer:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇〇888(Landroid/hardware/SensorEvent;)V
    .locals 11

    .line 1
    if-eqz p1, :cond_5

    .line 2
    .line 3
    iget-object p1, p1, Landroid/hardware/SensorEvent;->values:[F

    .line 4
    .line 5
    if-eqz p1, :cond_5

    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    aget v1, p1, v0

    .line 9
    .line 10
    const/4 v2, 0x1

    .line 11
    aget v3, p1, v2

    .line 12
    .line 13
    invoke-virtual {p0}, Lcom/intsig/advertisement/util/sensor/AdSensorBase;->Oo08()Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;

    .line 14
    .line 15
    .line 16
    move-result-object v4

    .line 17
    invoke-virtual {v4}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->is_lon_accele()I

    .line 18
    .line 19
    .line 20
    move-result v4

    .line 21
    const-string/jumbo v5, "x="

    .line 22
    .line 23
    .line 24
    const/4 v6, 0x2

    .line 25
    if-ne v4, v2, :cond_2

    .line 26
    .line 27
    mul-float v4, v1, v1

    .line 28
    .line 29
    mul-float v7, v3, v3

    .line 30
    .line 31
    add-float/2addr v4, v7

    .line 32
    float-to-double v7, v4

    .line 33
    invoke-static {v7, v8}, Ljava/lang/Math;->sqrt(D)D

    .line 34
    .line 35
    .line 36
    move-result-wide v7

    .line 37
    invoke-virtual {p0}, Lcom/intsig/advertisement/util/sensor/AdSensorBase;->Oo08()Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;

    .line 38
    .line 39
    .line 40
    move-result-object v4

    .line 41
    invoke-virtual {v4}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->getAcceleration()F

    .line 42
    .line 43
    .line 44
    move-result v4

    .line 45
    float-to-double v9, v4

    .line 46
    cmpl-double v4, v7, v9

    .line 47
    .line 48
    if-lez v4, :cond_1

    .line 49
    .line 50
    invoke-virtual {p0}, Lcom/intsig/advertisement/util/sensor/AdSensorBase;->o〇0()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v4

    .line 54
    new-instance v9, Ljava/lang/StringBuilder;

    .line 55
    .line 56
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 57
    .line 58
    .line 59
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    .line 61
    .line 62
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    const-string v5, " y="

    .line 66
    .line 67
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    const-string v5, "  a="

    .line 74
    .line 75
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    invoke-virtual {v9, v7, v8}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 82
    .line 83
    .line 84
    move-result-object v5

    .line 85
    invoke-static {v4, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    .line 87
    .line 88
    iget-boolean v4, p0, Lcom/intsig/advertisement/util/sensor/AdAccelerometerSensor;->o〇0:Z

    .line 89
    .line 90
    if-nez v4, :cond_0

    .line 91
    .line 92
    iget v4, p0, Lcom/intsig/advertisement/util/sensor/AdAccelerometerSensor;->〇〇888:I

    .line 93
    .line 94
    add-int/2addr v4, v2

    .line 95
    iput v4, p0, Lcom/intsig/advertisement/util/sensor/AdAccelerometerSensor;->〇〇888:I

    .line 96
    .line 97
    if-lt v4, v6, :cond_0

    .line 98
    .line 99
    sget-object v4, Lcom/intsig/advertisement/control/AdConfigManager;->OoO8:[I

    .line 100
    .line 101
    float-to-int v1, v1

    .line 102
    aput v1, v4, v0

    .line 103
    .line 104
    float-to-int v0, v3

    .line 105
    aput v0, v4, v2

    .line 106
    .line 107
    aget p1, p1, v6

    .line 108
    .line 109
    float-to-int p1, p1

    .line 110
    aput p1, v4, v6

    .line 111
    .line 112
    invoke-virtual {p0}, Lcom/intsig/advertisement/util/sensor/AdSensorBase;->〇080()V

    .line 113
    .line 114
    .line 115
    :cond_0
    const/4 v0, 0x1

    .line 116
    :cond_1
    iput-boolean v0, p0, Lcom/intsig/advertisement/util/sensor/AdAccelerometerSensor;->o〇0:Z

    .line 117
    .line 118
    goto :goto_0

    .line 119
    :cond_2
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    .line 120
    .line 121
    .line 122
    move-result v4

    .line 123
    invoke-virtual {p0}, Lcom/intsig/advertisement/util/sensor/AdSensorBase;->Oo08()Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;

    .line 124
    .line 125
    .line 126
    move-result-object v7

    .line 127
    invoke-virtual {v7}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->getAcceleration()F

    .line 128
    .line 129
    .line 130
    move-result v7

    .line 131
    cmpl-float v4, v4, v7

    .line 132
    .line 133
    if-lez v4, :cond_4

    .line 134
    .line 135
    invoke-virtual {p0}, Lcom/intsig/advertisement/util/sensor/AdSensorBase;->o〇0()Ljava/lang/String;

    .line 136
    .line 137
    .line 138
    move-result-object v4

    .line 139
    new-instance v7, Ljava/lang/StringBuilder;

    .line 140
    .line 141
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 142
    .line 143
    .line 144
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    .line 146
    .line 147
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 148
    .line 149
    .line 150
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 151
    .line 152
    .line 153
    move-result-object v5

    .line 154
    invoke-static {v4, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    .line 156
    .line 157
    iget-boolean v4, p0, Lcom/intsig/advertisement/util/sensor/AdAccelerometerSensor;->o〇0:Z

    .line 158
    .line 159
    if-nez v4, :cond_3

    .line 160
    .line 161
    iget v4, p0, Lcom/intsig/advertisement/util/sensor/AdAccelerometerSensor;->〇〇888:I

    .line 162
    .line 163
    add-int/2addr v4, v2

    .line 164
    iput v4, p0, Lcom/intsig/advertisement/util/sensor/AdAccelerometerSensor;->〇〇888:I

    .line 165
    .line 166
    if-lt v4, v6, :cond_3

    .line 167
    .line 168
    sget-object v4, Lcom/intsig/advertisement/control/AdConfigManager;->OoO8:[I

    .line 169
    .line 170
    float-to-int v1, v1

    .line 171
    aput v1, v4, v0

    .line 172
    .line 173
    float-to-int v0, v3

    .line 174
    aput v0, v4, v2

    .line 175
    .line 176
    aget p1, p1, v6

    .line 177
    .line 178
    float-to-int p1, p1

    .line 179
    aput p1, v4, v6

    .line 180
    .line 181
    invoke-virtual {p0}, Lcom/intsig/advertisement/util/sensor/AdSensorBase;->〇080()V

    .line 182
    .line 183
    .line 184
    :cond_3
    const/4 v0, 0x1

    .line 185
    :cond_4
    iput-boolean v0, p0, Lcom/intsig/advertisement/util/sensor/AdAccelerometerSensor;->o〇0:Z

    .line 186
    .line 187
    :cond_5
    :goto_0
    return-void
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

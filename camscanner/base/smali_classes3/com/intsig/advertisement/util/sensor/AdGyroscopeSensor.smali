.class public final Lcom/intsig/advertisement/util/sensor/AdGyroscopeSensor;
.super Lcom/intsig/advertisement/util/sensor/AdSensorBase;
.source "AdGyroscopeSensor.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final oO80:D

.field private o〇0:J

.field private 〇〇888:I


# direct methods
.method public constructor <init>(Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;)V
    .locals 2
    .param p1    # Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string/jumbo v0, "style"

    .line 2
    .line 3
    .line 4
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/util/sensor/AdSensorBase;-><init>(Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;)V

    .line 8
    .line 9
    .line 10
    const-wide v0, 0x3e112e0be0000000L    # 9.999999717180685E-10

    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    iput-wide v0, p0, Lcom/intsig/advertisement/util/sensor/AdGyroscopeSensor;->oO80:D

    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public O8()I
    .locals 1

    .line 1
    const/4 v0, 0x4

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇o〇()I
    .locals 1

    .line 1
    sget v0, Lcom/intsig/advertisement/R$raw;->shake_turn:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇〇888(Landroid/hardware/SensorEvent;)V
    .locals 12

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    iget-object v0, p1, Landroid/hardware/SensorEvent;->values:[F

    .line 4
    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    const/4 v1, 0x1

    .line 8
    aget v2, v0, v1

    .line 9
    .line 10
    const/4 v3, 0x2

    .line 11
    aget v4, v0, v3

    .line 12
    .line 13
    mul-float v5, v2, v2

    .line 14
    .line 15
    mul-float v6, v4, v4

    .line 16
    .line 17
    add-float/2addr v5, v6

    .line 18
    float-to-double v5, v5

    .line 19
    invoke-static {v5, v6}, Ljava/lang/Math;->sqrt(D)D

    .line 20
    .line 21
    .line 22
    move-result-wide v5

    .line 23
    invoke-static {v5, v6}, Ljava/lang/Math;->toDegrees(D)D

    .line 24
    .line 25
    .line 26
    move-result-wide v5

    .line 27
    iget-wide v7, p0, Lcom/intsig/advertisement/util/sensor/AdGyroscopeSensor;->o〇0:J

    .line 28
    .line 29
    const-wide/16 v9, 0x0

    .line 30
    .line 31
    cmp-long v11, v7, v9

    .line 32
    .line 33
    if-eqz v11, :cond_0

    .line 34
    .line 35
    iget-wide v9, p1, Landroid/hardware/SensorEvent;->timestamp:J

    .line 36
    .line 37
    sub-long/2addr v9, v7

    .line 38
    long-to-double v7, v9

    .line 39
    iget-wide v9, p0, Lcom/intsig/advertisement/util/sensor/AdGyroscopeSensor;->oO80:D

    .line 40
    .line 41
    mul-double v7, v7, v9

    .line 42
    .line 43
    iget v9, p0, Lcom/intsig/advertisement/util/sensor/AdGyroscopeSensor;->〇〇888:I

    .line 44
    .line 45
    mul-double v7, v7, v5

    .line 46
    .line 47
    double-to-int v7, v7

    .line 48
    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    .line 49
    .line 50
    .line 51
    move-result v7

    .line 52
    add-int/2addr v9, v7

    .line 53
    iput v9, p0, Lcom/intsig/advertisement/util/sensor/AdGyroscopeSensor;->〇〇888:I

    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/advertisement/util/sensor/AdSensorBase;->o〇0()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v7

    .line 60
    new-instance v8, Ljava/lang/StringBuilder;

    .line 61
    .line 62
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 63
    .line 64
    .line 65
    const-string v9, "first data degreeSpeed="

    .line 66
    .line 67
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    .line 69
    .line 70
    invoke-virtual {v8, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 71
    .line 72
    .line 73
    const-string v9, "  y="

    .line 74
    .line 75
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    .line 77
    .line 78
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 79
    .line 80
    .line 81
    const-string v9, " z="

    .line 82
    .line 83
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    .line 85
    .line 86
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 87
    .line 88
    .line 89
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 90
    .line 91
    .line 92
    move-result-object v8

    .line 93
    invoke-static {v7, v8}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    .line 95
    .line 96
    :goto_0
    iget-wide v7, p1, Landroid/hardware/SensorEvent;->timestamp:J

    .line 97
    .line 98
    iput-wide v7, p0, Lcom/intsig/advertisement/util/sensor/AdGyroscopeSensor;->o〇0:J

    .line 99
    .line 100
    iget p1, p0, Lcom/intsig/advertisement/util/sensor/AdGyroscopeSensor;->〇〇888:I

    .line 101
    .line 102
    int-to-float p1, p1

    .line 103
    invoke-virtual {p0}, Lcom/intsig/advertisement/util/sensor/AdSensorBase;->Oo08()Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;

    .line 104
    .line 105
    .line 106
    move-result-object v7

    .line 107
    invoke-virtual {v7}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->getRotate_angle()F

    .line 108
    .line 109
    .line 110
    move-result v7

    .line 111
    cmpl-float p1, p1, v7

    .line 112
    .line 113
    if-lez p1, :cond_1

    .line 114
    .line 115
    invoke-virtual {p0}, Lcom/intsig/advertisement/util/sensor/AdSensorBase;->Oo08()Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;

    .line 116
    .line 117
    .line 118
    move-result-object p1

    .line 119
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->getAngular_speed()I

    .line 120
    .line 121
    .line 122
    move-result p1

    .line 123
    int-to-double v7, p1

    .line 124
    cmpl-double p1, v5, v7

    .line 125
    .line 126
    if-lez p1, :cond_1

    .line 127
    .line 128
    invoke-virtual {p0}, Lcom/intsig/advertisement/util/sensor/AdSensorBase;->o〇0()Ljava/lang/String;

    .line 129
    .line 130
    .line 131
    move-result-object p1

    .line 132
    iget v7, p0, Lcom/intsig/advertisement/util/sensor/AdGyroscopeSensor;->〇〇888:I

    .line 133
    .line 134
    new-instance v8, Ljava/lang/StringBuilder;

    .line 135
    .line 136
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 137
    .line 138
    .line 139
    const-string v9, "degreeSpeed="

    .line 140
    .line 141
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    .line 143
    .line 144
    invoke-virtual {v8, v5, v6}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 145
    .line 146
    .line 147
    const-string v5, "  rotateDegree="

    .line 148
    .line 149
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    .line 151
    .line 152
    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 153
    .line 154
    .line 155
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 156
    .line 157
    .line 158
    move-result-object v5

    .line 159
    invoke-static {p1, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    .line 161
    .line 162
    sget-object p1, Lcom/intsig/advertisement/control/AdConfigManager;->OoO8:[I

    .line 163
    .line 164
    const/4 v5, 0x0

    .line 165
    aget v0, v0, v5

    .line 166
    .line 167
    float-to-int v0, v0

    .line 168
    aput v0, p1, v5

    .line 169
    .line 170
    float-to-int v0, v2

    .line 171
    aput v0, p1, v1

    .line 172
    .line 173
    float-to-int v0, v4

    .line 174
    aput v0, p1, v3

    .line 175
    .line 176
    invoke-virtual {p0}, Lcom/intsig/advertisement/util/sensor/AdSensorBase;->〇080()V

    .line 177
    .line 178
    .line 179
    :cond_1
    return-void
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.class public Lcom/intsig/advertisement/bean/ConfigResponse;
.super Ljava/lang/Object;
.source "ConfigResponse.java"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation


# instance fields
.field private ad_request_time:I

.field private app_cold_launch:Lcom/intsig/advertisement/bean/ItemConfig;

.field private app_exit_trace_threshold:I

.field private app_launch:Lcom/intsig/advertisement/bean/ItemConfig;

.field private bid_switch:I

.field private cache_pool:Lcom/intsig/advertisement/bean/ItemConfig;

.field private cfg_request_time:I

.field private doc_list:Lcom/intsig/advertisement/bean/PosFlowCfg;

.field private doclist_popup:Lcom/intsig/advertisement/bean/ItemConfig;

.field private function_reward:Lcom/intsig/advertisement/bean/ItemConfig;

.field private i_ci:Ljava/lang/String;

.field private i_cou:Ljava/lang/String;

.field private i_pro:Ljava/lang/String;

.field private ip:Ljava/lang/String;

.field private is_show_personalized_ad:I

.field private key_free_ad:I

.field private main_middle_banner:Lcom/intsig/advertisement/bean/ItemConfig;

.field private max_impression:I

.field private ocr_reward:Lcom/intsig/advertisement/bean/ItemConfig;

.field private page_list_banner:Lcom/intsig/advertisement/bean/ItemConfig;

.field private pdf_watermark_video:Lcom/intsig/advertisement/bean/ItemConfig;

.field private preload_origins:Lcom/google/gson/JsonObject;

.field private purchase_exit:Lcom/intsig/advertisement/bean/ItemConfig;

.field private reward_video:Lcom/intsig/advertisement/bean/ItemConfig;

.field private scan_done:Lcom/intsig/advertisement/bean/PosFlowCfg;

.field private scandone_reward:Lcom/intsig/advertisement/bean/ItemConfig;

.field private scandone_top:Lcom/intsig/advertisement/bean/ItemConfig;

.field private service_time:J

.field private session:Lcom/intsig/advertisement/bean/Session;

.field private share_done:Lcom/intsig/advertisement/bean/ItemConfig;

.field private shot_done:Lcom/intsig/advertisement/bean/ItemConfig;

.field private space_lottery:Lcom/intsig/advertisement/bean/ItemConfig;

.field private touch_forbid_launch_ad:I

.field private upload_time:J

.field private vir_applaunch:Lcom/intsig/advertisement/bean/ItemConfig;

.field private vir_cold_applaunch:Lcom/intsig/advertisement/bean/ItemConfig;

.field private vir_mainbanner:Lcom/intsig/advertisement/bean/ItemConfig;

.field private vir_pagelist_banner:Lcom/intsig/advertisement/bean/ItemConfig;

.field private vir_scandone:Lcom/intsig/advertisement/bean/PosFlowCfg;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const v0, 0x7fffffff

    .line 5
    .line 6
    .line 7
    iput v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->max_impression:I

    .line 8
    .line 9
    const/4 v0, 0x1

    .line 10
    iput v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->is_show_personalized_ad:I

    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
.end method


# virtual methods
.method public getAdRequestTime()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->ad_request_time:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getAppColdLaunch()Lcom/intsig/advertisement/bean/ItemConfig;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->app_cold_launch:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getAppExitTraceThreshold()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->app_exit_trace_threshold:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getApp_launch()Lcom/intsig/advertisement/bean/ItemConfig;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->app_launch:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getBid_switch()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->bid_switch:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getCachePool()Lcom/intsig/advertisement/bean/ItemConfig;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->cache_pool:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getCfgRequestTime()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->cfg_request_time:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getDocListPopUp()Lcom/intsig/advertisement/bean/ItemConfig;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->doclist_popup:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getDoc_list()Lcom/intsig/advertisement/bean/PosFlowCfg;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->doc_list:Lcom/intsig/advertisement/bean/PosFlowCfg;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getFunctionReward()Lcom/intsig/advertisement/bean/ItemConfig;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->function_reward:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getICi()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->i_ci:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getICou()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->i_cou:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getIPro()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->i_pro:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getIp()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->ip:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getIs_show_personalized_ad()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->is_show_personalized_ad:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getKeyFreeAd()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->key_free_ad:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getMainMiddleBanner()Lcom/intsig/advertisement/bean/ItemConfig;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->main_middle_banner:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getMax_impression()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->max_impression:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getOcrReward()Lcom/intsig/advertisement/bean/ItemConfig;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->ocr_reward:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getPage_list_banner()Lcom/intsig/advertisement/bean/ItemConfig;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->page_list_banner:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getPdfWatermarkVideo()Lcom/intsig/advertisement/bean/ItemConfig;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->pdf_watermark_video:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getPreload_origins()Lcom/google/gson/JsonObject;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->preload_origins:Lcom/google/gson/JsonObject;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getPurchase_exit()Lcom/intsig/advertisement/bean/ItemConfig;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->purchase_exit:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getReward_video()Lcom/intsig/advertisement/bean/ItemConfig;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->reward_video:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getScan_done()Lcom/intsig/advertisement/bean/PosFlowCfg;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->scan_done:Lcom/intsig/advertisement/bean/PosFlowCfg;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getScandoneReward()Lcom/intsig/advertisement/bean/ItemConfig;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->scandone_reward:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getScandoneTop()Lcom/intsig/advertisement/bean/ItemConfig;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->scandone_top:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getService_time()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->service_time:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getSession()Lcom/intsig/advertisement/bean/Session;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->session:Lcom/intsig/advertisement/bean/Session;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getShare_done()Lcom/intsig/advertisement/bean/ItemConfig;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->share_done:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getShotDone()Lcom/intsig/advertisement/bean/ItemConfig;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->shot_done:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getSpace_lottery()Lcom/intsig/advertisement/bean/ItemConfig;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->space_lottery:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getTouchForbidLaunchAd()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->touch_forbid_launch_ad:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getUpload_time()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->upload_time:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getVirApplaunch()Lcom/intsig/advertisement/bean/ItemConfig;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->vir_applaunch:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getVirColdApplaunch()Lcom/intsig/advertisement/bean/ItemConfig;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->vir_cold_applaunch:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getVirMainbanner()Lcom/intsig/advertisement/bean/ItemConfig;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->vir_mainbanner:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getVirPagelistBanner()Lcom/intsig/advertisement/bean/ItemConfig;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->vir_pagelist_banner:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getVirScandone()Lcom/intsig/advertisement/bean/PosFlowCfg;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->vir_scandone:Lcom/intsig/advertisement/bean/PosFlowCfg;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public setAdRequestTime(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->ad_request_time:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setAppColdLaunch(Lcom/intsig/advertisement/bean/ItemConfig;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->app_cold_launch:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setAppExitTraceThreshold(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->app_exit_trace_threshold:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setApp_launch(Lcom/intsig/advertisement/bean/ItemConfig;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->app_launch:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setBid_switch(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->bid_switch:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setCachePool(Lcom/intsig/advertisement/bean/ItemConfig;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->cache_pool:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setCfgRequestTime(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->cfg_request_time:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setDocListPopUp(Lcom/intsig/advertisement/bean/ItemConfig;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->doclist_popup:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setDoc_list(Lcom/intsig/advertisement/bean/PosFlowCfg;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->doc_list:Lcom/intsig/advertisement/bean/PosFlowCfg;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setFunctionReward(Lcom/intsig/advertisement/bean/ItemConfig;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->function_reward:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setICi(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->i_ci:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setICou(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->i_cou:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setIPro(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->i_pro:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setIp(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->ip:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setIs_show_personalized_ad(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->is_show_personalized_ad:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setKeyFreeAd(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->key_free_ad:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setMainMiddleBanner(Lcom/intsig/advertisement/bean/ItemConfig;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->main_middle_banner:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setMax_impression(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->max_impression:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setOcrReward(Lcom/intsig/advertisement/bean/ItemConfig;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->ocr_reward:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setPage_list_banner(Lcom/intsig/advertisement/bean/ItemConfig;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->page_list_banner:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setPdfWatermarkVideo(Lcom/intsig/advertisement/bean/ItemConfig;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->pdf_watermark_video:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setPreload_origins(Lcom/google/gson/JsonObject;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->preload_origins:Lcom/google/gson/JsonObject;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setPurchase_exit(Lcom/intsig/advertisement/bean/ItemConfig;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->purchase_exit:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setReward_video(Lcom/intsig/advertisement/bean/ItemConfig;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->reward_video:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setScan_done(Lcom/intsig/advertisement/bean/PosFlowCfg;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->scan_done:Lcom/intsig/advertisement/bean/PosFlowCfg;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setScandoneReward(Lcom/intsig/advertisement/bean/ItemConfig;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->scandone_reward:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setScandoneTop(Lcom/intsig/advertisement/bean/ItemConfig;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->scandone_top:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setService_time(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->service_time:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setSession(Lcom/intsig/advertisement/bean/Session;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->session:Lcom/intsig/advertisement/bean/Session;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setShare_done(Lcom/intsig/advertisement/bean/ItemConfig;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->share_done:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setShotDone(Lcom/intsig/advertisement/bean/ItemConfig;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->shot_done:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->shot_done:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setSpace_lottery(Lcom/intsig/advertisement/bean/ItemConfig;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->space_lottery:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setTouchForbidLaunchAd(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->touch_forbid_launch_ad:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setUpload_time(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->upload_time:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setVirApplaunch(Lcom/intsig/advertisement/bean/ItemConfig;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->vir_applaunch:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setVirColdApplaunch(Lcom/intsig/advertisement/bean/ItemConfig;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->vir_cold_applaunch:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setVirMainbanner(Lcom/intsig/advertisement/bean/ItemConfig;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->vir_mainbanner:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setVirPagelistBanner(Lcom/intsig/advertisement/bean/ItemConfig;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->vir_pagelist_banner:Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setVirScandone(Lcom/intsig/advertisement/bean/PosFlowCfg;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ConfigResponse;->vir_scandone:Lcom/intsig/advertisement/bean/PosFlowCfg;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

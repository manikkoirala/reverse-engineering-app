.class public Lcom/intsig/advertisement/bean/ItemConfig;
.super Ljava/lang/Object;
.source "ItemConfig.java"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation


# instance fields
.field private brand_blacklist:[Ljava/lang/String;

.field private cache_retry_interval:I

.field private free_ad_text:Ljava/lang/String;

.field private free_close_interval_count:I

.field private free_max_show_count:I

.field private free_tip_interval:I

.field private gray:I

.field private index:I

.field private init_show_after:I

.field private init_skip_after:I

.field private interceptCode:I

.field private interval_model:Lcom/intsig/advertisement/bean/IntervalModel;

.field private is_launch_with_inters:I

.field private max_impression:I

.field private min_doc_num:I

.field private min_interval:I

.field private pop_priority:F

.field private relate_interval:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private sdk_upload:I

.field private show_close:I

.field private show_skip_num:I

.field private skip_time_daily:I

.field private source_info:[Lcom/intsig/advertisement/bean/SourceCfg;

.field private spot_wait_time:I

.field private timeout:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->index:I

    .line 6
    .line 7
    iput v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->init_show_after:I

    .line 8
    .line 9
    iput v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->init_skip_after:I

    .line 10
    .line 11
    iput v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->min_interval:I

    .line 12
    .line 13
    const/4 v1, 0x0

    .line 14
    iput v1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->timeout:F

    .line 15
    .line 16
    const v1, 0x7fffffff

    .line 17
    .line 18
    .line 19
    iput v1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->max_impression:I

    .line 20
    .line 21
    iput v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->sdk_upload:I

    .line 22
    .line 23
    iput v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->spot_wait_time:I

    .line 24
    .line 25
    const/16 v1, 0x64

    .line 26
    .line 27
    iput v1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->gray:I

    .line 28
    .line 29
    const/4 v1, 0x1

    .line 30
    iput v1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->show_close:I

    .line 31
    .line 32
    const/4 v1, -0x1

    .line 33
    iput v1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->interceptCode:I

    .line 34
    .line 35
    const/high16 v1, 0x40400000    # 3.0f

    .line 36
    .line 37
    iput v1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->pop_priority:F

    .line 38
    .line 39
    iput v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->min_doc_num:I

    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method


# virtual methods
.method public getBrandBlacklist()[Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->brand_blacklist:[Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getCacheRetryInterval()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->cache_retry_interval:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getFreeAdText()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->free_ad_text:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getFreeCloseIntervalCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->free_close_interval_count:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getFreeMaxShowCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->free_max_show_count:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getFreeTipInterval()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->free_tip_interval:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getGray()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->gray:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getIndex()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->index:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getInit_show_after()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->init_show_after:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getInit_skip_after()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->init_skip_after:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getInterceptCode()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->interceptCode:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getIntervalModel()Lcom/intsig/advertisement/bean/IntervalModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->interval_model:Lcom/intsig/advertisement/bean/IntervalModel;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getMax_impression()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->max_impression:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getMinDocNum()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->min_doc_num:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getMin_interval()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->min_interval:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getPopPriority()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->pop_priority:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getRelateInterval()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->relate_interval:Ljava/util/HashMap;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getSdk_upload()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->sdk_upload:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getShowSkipNum()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->show_skip_num:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getShow_close()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->show_close:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getSkipTimeDaily()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->skip_time_daily:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getSource_info()[Lcom/intsig/advertisement/bean/SourceCfg;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->source_info:[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getSpotWaitTime()I
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->spot_wait_time:I

    .line 2
    .line 3
    const/16 v1, 0xc8

    .line 4
    .line 5
    if-ge v0, v1, :cond_0

    .line 6
    .line 7
    const/16 v0, 0x1f4

    .line 8
    .line 9
    return v0

    .line 10
    :cond_0
    const/16 v1, 0x1388

    .line 11
    .line 12
    if-le v0, v1, :cond_1

    .line 13
    .line 14
    return v1

    .line 15
    :cond_1
    return v0
.end method

.method public getTimeout()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->timeout:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public isLaunchWithInters()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/ItemConfig;->is_launch_with_inters:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public setBrandBlacklist([Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->brand_blacklist:[Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setCacheRetryInterval(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->cache_retry_interval:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setFreeAdText(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->free_ad_text:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setFreeCloseIntervalCount(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->free_close_interval_count:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setFreeMaxShowCount(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->free_max_show_count:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setFreeTipInterval(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->free_tip_interval:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setGray(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->gray:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setIndex(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->index:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setInit_show_after(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->init_show_after:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setInit_skip_after(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->init_skip_after:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setInterceptCode(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->interceptCode:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setIntervalModel(Lcom/intsig/advertisement/bean/IntervalModel;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->interval_model:Lcom/intsig/advertisement/bean/IntervalModel;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setLaunchWithInters(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->is_launch_with_inters:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setMax_impression(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->max_impression:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setMinDocNum(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->min_doc_num:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setMin_interval(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->min_interval:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setPopPriority(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->pop_priority:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setRelateInterval(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->relate_interval:Ljava/util/HashMap;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setSdk_upload(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->sdk_upload:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setShowSkipNum(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->show_skip_num:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setShow_close(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->show_close:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setSkipTimeDaily(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->skip_time_daily:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setSource_info([Lcom/intsig/advertisement/bean/SourceCfg;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->source_info:[Lcom/intsig/advertisement/bean/SourceCfg;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setSpotWaitTime(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->spot_wait_time:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setTimeout(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/ItemConfig;->timeout:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

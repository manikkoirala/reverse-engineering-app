.class public Lcom/intsig/advertisement/bean/PosFlowCfg;
.super Ljava/lang/Object;
.source "PosFlowCfg.java"


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation


# instance fields
.field private banners:[Lcom/intsig/advertisement/bean/ItemConfig;

.field private brand_blacklist:[Ljava/lang/String;

.field private cache_retry_interval:I

.field private free_ad_text:Ljava/lang/String;

.field private free_close_interval_count:I

.field private free_max_show_count:I

.field private free_tip_interval:I

.field private gray:I

.field private init_show_after:I

.field private init_skip_after:I

.field private interval_model:Lcom/intsig/advertisement/bean/IntervalModel;

.field private is_launch_with_inters:I

.field private max_impression:I

.field private min_doc_num:I

.field private min_interval:I

.field private pop_priority:F

.field private relate_interval:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private sdk_upload:I

.field private show_close:I

.field private show_skip_num:I

.field private skip_time_daily:I

.field private spot_wait_time:I

.field private timeout:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->init_show_after:I

    .line 6
    .line 7
    iput v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->init_skip_after:I

    .line 8
    .line 9
    iput v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->min_interval:I

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    iput v1, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->timeout:F

    .line 13
    .line 14
    const v1, 0x7fffffff

    .line 15
    .line 16
    .line 17
    iput v1, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->max_impression:I

    .line 18
    .line 19
    iput v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->sdk_upload:I

    .line 20
    .line 21
    iput v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->spot_wait_time:I

    .line 22
    .line 23
    iput v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->min_doc_num:I

    .line 24
    .line 25
    const/16 v0, 0x64

    .line 26
    .line 27
    iput v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->gray:I

    .line 28
    .line 29
    const/4 v0, 0x1

    .line 30
    iput v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->show_close:I

    .line 31
    .line 32
    const/high16 v0, 0x40400000    # 3.0f

    .line 33
    .line 34
    iput v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->pop_priority:F

    .line 35
    .line 36
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method


# virtual methods
.method public getBanners()[Lcom/intsig/advertisement/bean/ItemConfig;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->banners:[Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getBrandBlacklist()[Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->brand_blacklist:[Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getCacheRetryInterval()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->cache_retry_interval:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getFreeAdText()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->free_ad_text:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getFreeCloseIntervalCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->free_close_interval_count:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getFreeMaxShowCount()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->free_max_show_count:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getFreeTipInterval()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->free_tip_interval:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getGray()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->gray:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getInit_show_after()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->init_show_after:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getInit_skip_after()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->init_skip_after:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getIntervalModel()Lcom/intsig/advertisement/bean/IntervalModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->interval_model:Lcom/intsig/advertisement/bean/IntervalModel;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getMax_impression()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->max_impression:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getMin_doc_num()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->min_doc_num:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getMin_interval()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->min_interval:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getPopPriority()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->pop_priority:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getRelateInterval()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->relate_interval:Ljava/util/HashMap;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getSdk_upload()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->sdk_upload:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getShowSkipNum()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->show_skip_num:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getShow_close()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->show_close:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getSkipTimeDaily()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->skip_time_daily:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getSpotWaitTime()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->spot_wait_time:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getTimeout()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->timeout:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public isLaunchWithInters()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->is_launch_with_inters:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public setBanners([Lcom/intsig/advertisement/bean/ItemConfig;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->banners:[Lcom/intsig/advertisement/bean/ItemConfig;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setBrandBlacklist([Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->brand_blacklist:[Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setCacheRetryInterval(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->cache_retry_interval:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setFreeAdText(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->free_ad_text:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setFreeCloseIntervalCount(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->free_close_interval_count:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setFreeMaxShowCount(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->free_max_show_count:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setFreeTipInterval(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->free_tip_interval:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setGray(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->gray:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setInit_show_after(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->init_show_after:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setInit_skip_after(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->init_skip_after:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setIntervalModel(Lcom/intsig/advertisement/bean/IntervalModel;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->interval_model:Lcom/intsig/advertisement/bean/IntervalModel;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setLaunchWithInters(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->is_launch_with_inters:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setMax_impression(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->max_impression:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setMin_doc_num(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->min_doc_num:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setMin_interval(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->min_interval:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setPopPriority(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->pop_priority:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setRelateInterval(Ljava/util/HashMap;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->relate_interval:Ljava/util/HashMap;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setSdk_upload(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->sdk_upload:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setShowSkipNum(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->show_skip_num:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setShow_close(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->show_close:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setSkipTimeDaily(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->skip_time_daily:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setSpotWaitTime(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->spot_wait_time:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setTimeout(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/bean/PosFlowCfg;->timeout:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

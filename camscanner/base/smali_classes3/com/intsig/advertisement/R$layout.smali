.class public final Lcom/intsig/advertisement/R$layout;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/advertisement/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final abc_action_bar_title_item:I = 0x7f0d0000

.field public static final abc_action_bar_up_container:I = 0x7f0d0001

.field public static final abc_action_menu_item_layout:I = 0x7f0d0002

.field public static final abc_action_menu_layout:I = 0x7f0d0003

.field public static final abc_action_mode_bar:I = 0x7f0d0004

.field public static final abc_action_mode_close_item_material:I = 0x7f0d0005

.field public static final abc_activity_chooser_view:I = 0x7f0d0006

.field public static final abc_activity_chooser_view_list_item:I = 0x7f0d0007

.field public static final abc_alert_dialog_button_bar_material:I = 0x7f0d0008

.field public static final abc_alert_dialog_material:I = 0x7f0d0009

.field public static final abc_alert_dialog_title_material:I = 0x7f0d000a

.field public static final abc_cascading_menu_item_layout:I = 0x7f0d000b

.field public static final abc_dialog_title_material:I = 0x7f0d000c

.field public static final abc_expanded_menu_layout:I = 0x7f0d000d

.field public static final abc_list_menu_item_checkbox:I = 0x7f0d000e

.field public static final abc_list_menu_item_icon:I = 0x7f0d000f

.field public static final abc_list_menu_item_layout:I = 0x7f0d0010

.field public static final abc_list_menu_item_radio:I = 0x7f0d0011

.field public static final abc_popup_menu_header_item_layout:I = 0x7f0d0012

.field public static final abc_popup_menu_item_layout:I = 0x7f0d0013

.field public static final abc_screen_content_include:I = 0x7f0d0014

.field public static final abc_screen_simple:I = 0x7f0d0015

.field public static final abc_screen_simple_overlay_action_mode:I = 0x7f0d0016

.field public static final abc_screen_toolbar:I = 0x7f0d0017

.field public static final abc_search_dropdown_item_icons_2line:I = 0x7f0d0018

.field public static final abc_search_view:I = 0x7f0d0019

.field public static final abc_select_dialog_material:I = 0x7f0d001a

.field public static final abc_tooltip:I = 0x7f0d001b

.field public static final activity_account_single_fragment:I = 0x7f0d005a

.field public static final activity_bind_phone:I = 0x7f0d005f

.field public static final activity_fragment_container:I = 0x7f0d0079

.field public static final activity_full_screen_ad_layout:I = 0x7f0d007a

.field public static final activity_id_verify:I = 0x7f0d0080

.field public static final activity_login_main:I = 0x7f0d0082

.field public static final activity_reward_video_layout:I = 0x7f0d00ae

.field public static final activity_super_verify_code:I = 0x7f0d00bf

.field public static final ad_click_tip_layout_15:I = 0x7f0d00d9

.field public static final ad_launch_layout:I = 0x7f0d00e4

.field public static final ad_monitor:I = 0x7f0d00e7

.field public static final admob_empty_layout:I = 0x7f0d00ee

.field public static final alert_bottom_dialog_layout:I = 0x7f0d00ef

.field public static final app_launch_native:I = 0x7f0d00f2

.field public static final app_launch_native_only_image:I = 0x7f0d00f3

.field public static final applovin_consent_flow_gdpr_are_you_sure_screen:I = 0x7f0d00f4

.field public static final applovin_consent_flow_gdpr_phase_learn_more_screen:I = 0x7f0d00f5

.field public static final applovin_consent_flow_gdpr_phase_main_screen:I = 0x7f0d00f6

.field public static final applovin_consent_flow_gdpr_phase_partners_screen:I = 0x7f0d00f7

.field public static final applovin_debugger_list_item_detail:I = 0x7f0d00f8

.field public static final applovin_exo_list_divider:I = 0x7f0d00f9

.field public static final applovin_exo_player_control_view:I = 0x7f0d00fa

.field public static final applovin_exo_player_view:I = 0x7f0d00fb

.field public static final applovin_exo_styled_player_control_ffwd_button:I = 0x7f0d00fc

.field public static final applovin_exo_styled_player_control_rewind_button:I = 0x7f0d00fd

.field public static final applovin_exo_styled_player_control_view:I = 0x7f0d00fe

.field public static final applovin_exo_styled_player_view:I = 0x7f0d00ff

.field public static final applovin_exo_styled_settings_list:I = 0x7f0d0100

.field public static final applovin_exo_styled_settings_list_item:I = 0x7f0d0101

.field public static final applovin_exo_styled_sub_settings_list_item:I = 0x7f0d0102

.field public static final applovin_exo_track_selection_dialog:I = 0x7f0d0103

.field public static final applovin_native_ad_media_view:I = 0x7f0d0104

.field public static final base_actionbar_btn:I = 0x7f0d0107

.field public static final bottom_dialog_login:I = 0x7f0d010e

.field public static final browser_actions_context_menu_page:I = 0x7f0d0111

.field public static final browser_actions_context_menu_row:I = 0x7f0d0112

.field public static final choose_country_code:I = 0x7f0d013d

.field public static final choose_country_code_item:I = 0x7f0d013e

.field public static final collaborate_message_layout:I = 0x7f0d0140

.field public static final comm_dialog_cs_loading:I = 0x7f0d0146

.field public static final complete_button:I = 0x7f0d0148

.field public static final creative_debugger_displayed_ad_detail_activity:I = 0x7f0d014b

.field public static final cs_alert_dialog:I = 0x7f0d014c

.field public static final cs_alert_dialog_progress:I = 0x7f0d014d

.field public static final cs_progress_dialog:I = 0x7f0d014f

.field public static final cs_simple_list_item:I = 0x7f0d0150

.field public static final cs_simple_list_item_1:I = 0x7f0d0151

.field public static final cs_simple_list_item_2:I = 0x7f0d0152

.field public static final cs_simple_list_item_multiple_choice:I = 0x7f0d0153

.field public static final cs_simple_list_item_single_choice:I = 0x7f0d0154

.field public static final custom_dialog:I = 0x7f0d0158

.field public static final design_bottom_navigation_item:I = 0x7f0d0160

.field public static final design_bottom_sheet_dialog:I = 0x7f0d0161

.field public static final design_layout_snackbar:I = 0x7f0d0162

.field public static final design_layout_snackbar_include:I = 0x7f0d0163

.field public static final design_layout_tab_icon:I = 0x7f0d0164

.field public static final design_layout_tab_text:I = 0x7f0d0165

.field public static final design_menu_item_action_area:I = 0x7f0d0166

.field public static final design_navigation_item:I = 0x7f0d0167

.field public static final design_navigation_item_header:I = 0x7f0d0168

.field public static final design_navigation_item_separator:I = 0x7f0d0169

.field public static final design_navigation_item_subheader:I = 0x7f0d016a

.field public static final design_navigation_menu:I = 0x7f0d016b

.field public static final design_navigation_menu_item:I = 0x7f0d016c

.field public static final design_text_input_end_icon:I = 0x7f0d016d

.field public static final design_text_input_start_icon:I = 0x7f0d016e

.field public static final dialog_bind_account_result:I = 0x7f0d0180

.field public static final dialog_change_existed_account:I = 0x7f0d0190

.field public static final dialog_common_with_image:I = 0x7f0d019d

.field public static final dialog_exemption:I = 0x7f0d01bc

.field public static final dialog_login_for_compliance:I = 0x7f0d01e4

.field public static final dialog_login_protocol:I = 0x7f0d01e5

.field public static final dialog_pwd_login_over_five:I = 0x7f0d0212

.field public static final dialog_pwd_login_over_three:I = 0x7f0d0213

.field public static final dialog_super_vcode_over_five:I = 0x7f0d023a

.field public static final dialog_super_vcode_over_three:I = 0x7f0d023b

.field public static final doc_ad_item_grid:I = 0x7f0d025f

.field public static final doc_pop_close_menu:I = 0x7f0d026b

.field public static final doc_pop_multi_layout:I = 0x7f0d026c

.field public static final doc_single_image_container:I = 0x7f0d026e

.field public static final doclist_ad_item_list:I = 0x7f0d026f

.field public static final doclist_ad_item_list_new:I = 0x7f0d0270

.field public static final dot_layout:I = 0x7f0d0277

.field public static final edit_user_name_dialog:I = 0x7f0d0279

.field public static final expandable_layout_child:I = 0x7f0d027b

.field public static final expandable_layout_frame:I = 0x7f0d027c

.field public static final fg_reward_play_layout:I = 0x7f0d0289

.field public static final fg_reward_result_layout:I = 0x7f0d028a

.field public static final fragment_a_key_login:I = 0x7f0d0293

.field public static final fragment_area_code_confirm:I = 0x7f0d0296

.field public static final fragment_bind_phone_email:I = 0x7f0d02a0

.field public static final fragment_cancel_account_home:I = 0x7f0d02a4

.field public static final fragment_change_account:I = 0x7f0d02ae

.field public static final fragment_cloud_service_auth:I = 0x7f0d02b3

.field public static final fragment_default_email_login:I = 0x7f0d02bd

.field public static final fragment_default_email_register:I = 0x7f0d02be

.field public static final fragment_default_phone_pwd_login:I = 0x7f0d02bf

.field public static final fragment_default_verify_code_login:I = 0x7f0d02c0

.field public static final fragment_email_login:I = 0x7f0d02d6

.field public static final fragment_fail_cancel_account:I = 0x7f0d02d7

.field public static final fragment_forget_pwd:I = 0x7f0d02d9

.field public static final fragment_login_main:I = 0x7f0d02f7

.field public static final fragment_login_ways:I = 0x7f0d02f8

.field public static final fragment_phone_account_verify:I = 0x7f0d0323

.field public static final fragment_phone_pwd_login:I = 0x7f0d0324

.field public static final fragment_phone_verify_code_login:I = 0x7f0d0325

.field public static final fragment_setting_pwd:I = 0x7f0d033f

.field public static final fragment_super_vcode_validate:I = 0x7f0d034c

.field public static final fragment_verify_code:I = 0x7f0d0356

.field public static final fragment_verify_code_none_receive:I = 0x7f0d0357

.field public static final fragment_verify_new_email:I = 0x7f0d0358

.field public static final fragment_verify_new_phone:I = 0x7f0d0359

.field public static final fragment_verify_old_account:I = 0x7f0d035a

.field public static final have_toolbar_dark_layout:I = 0x7f0d036d

.field public static final have_toolbar_layout:I = 0x7f0d036e

.field public static final item_nine_phone_view:I = 0x7f0d045c

.field public static final item_single_choice:I = 0x7f0d04bf

.field public static final item_view_bind_account:I = 0x7f0d04ea

.field public static final layout_a_key_other_email_or_phone:I = 0x7f0d04f6

.field public static final layout_a_key_wechat:I = 0x7f0d04f7

.field public static final layout_actionbar_default_verifycode_login:I = 0x7f0d04f9

.field public static final layout_api_interstitial_dialog:I = 0x7f0d0504

.field public static final layout_cn_email_login_register_edit:I = 0x7f0d050b

.field public static final layout_common_other_login:I = 0x7f0d050c

.field public static final layout_custom_image_toast:I = 0x7f0d050e

.field public static final layout_custom_image_toast_new:I = 0x7f0d050f

.field public static final layout_error_msg_and_privacy_agreement:I = 0x7f0d051d

.field public static final layout_float_view:I = 0x7f0d0521

.field public static final layout_login_force_guide_rights:I = 0x7f0d053d

.field public static final layout_login_main_google_login:I = 0x7f0d053e

.field public static final layout_login_main_other_login:I = 0x7f0d053f

.field public static final layout_one_login_auth_custom:I = 0x7f0d0557

.field public static final layout_one_login_auth_half_screen_for_compliance:I = 0x7f0d0558

.field public static final layout_one_login_auth_half_screen_for_login:I = 0x7f0d0559

.field public static final layout_one_login_privacy_title:I = 0x7f0d055a

.field public static final layout_purchase_viewpager:I = 0x7f0d0579

.field public static final layout_rotate_image_text:I = 0x7f0d0581

.field public static final layout_third_login:I = 0x7f0d0599

.field public static final layout_verify_phone_code:I = 0x7f0d059c

.field public static final m3_alert_dialog:I = 0x7f0d05a5

.field public static final m3_alert_dialog_actions:I = 0x7f0d05a6

.field public static final m3_alert_dialog_title:I = 0x7f0d05a7

.field public static final m3_auto_complete_simple_item:I = 0x7f0d05a8

.field public static final m3_side_sheet_dialog:I = 0x7f0d05a9

.field public static final main_home_top_message_layout:I = 0x7f0d05b4

.field public static final main_middle_native_layout:I = 0x7f0d05b9

.field public static final main_page_top_message_layout:I = 0x7f0d05ba

.field public static final material_chip_input_combo:I = 0x7f0d05bc

.field public static final material_clock_display:I = 0x7f0d05bd

.field public static final material_clock_display_divider:I = 0x7f0d05be

.field public static final material_clock_period_toggle:I = 0x7f0d05bf

.field public static final material_clock_period_toggle_land:I = 0x7f0d05c0

.field public static final material_clockface_textview:I = 0x7f0d05c1

.field public static final material_clockface_view:I = 0x7f0d05c2

.field public static final material_radial_view_group:I = 0x7f0d05c3

.field public static final material_textinput_timepicker:I = 0x7f0d05c4

.field public static final material_time_chip:I = 0x7f0d05c5

.field public static final material_time_input:I = 0x7f0d05c6

.field public static final material_timepicker:I = 0x7f0d05c7

.field public static final material_timepicker_dialog:I = 0x7f0d05c8

.field public static final material_timepicker_textinput_display:I = 0x7f0d05c9

.field public static final max_hybrid_native_ad_view:I = 0x7f0d05ca

.field public static final max_native_ad_banner_icon_and_text_layout:I = 0x7f0d05cb

.field public static final max_native_ad_banner_view:I = 0x7f0d05cc

.field public static final max_native_ad_leader_view:I = 0x7f0d05cd

.field public static final max_native_ad_media_banner_view:I = 0x7f0d05ce

.field public static final max_native_ad_medium_template_1:I = 0x7f0d05cf

.field public static final max_native_ad_mrec_view:I = 0x7f0d05d0

.field public static final max_native_ad_recycler_view_item:I = 0x7f0d05d1

.field public static final max_native_ad_small_template_1:I = 0x7f0d05d2

.field public static final max_native_ad_vertical_banner_view:I = 0x7f0d05d3

.field public static final max_native_ad_vertical_leader_view:I = 0x7f0d05d4

.field public static final max_native_ad_vertical_media_banner_view:I = 0x7f0d05d5

.field public static final mediation_debugger_ad_unit_detail_activity:I = 0x7f0d05d7

.field public static final mediation_debugger_list_item_right_detail:I = 0x7f0d05d8

.field public static final mediation_debugger_list_section:I = 0x7f0d05d9

.field public static final mediation_debugger_list_section_centered:I = 0x7f0d05da

.field public static final mediation_debugger_list_view:I = 0x7f0d05db

.field public static final mediation_debugger_multi_ad_activity:I = 0x7f0d05dc

.field public static final mediation_debugger_text_view_activity:I = 0x7f0d05dd

.field public static final mtrl_alert_dialog:I = 0x7f0d05e6

.field public static final mtrl_alert_dialog_actions:I = 0x7f0d05e7

.field public static final mtrl_alert_dialog_title:I = 0x7f0d05e8

.field public static final mtrl_alert_select_dialog_item:I = 0x7f0d05e9

.field public static final mtrl_alert_select_dialog_multichoice:I = 0x7f0d05ea

.field public static final mtrl_alert_select_dialog_singlechoice:I = 0x7f0d05eb

.field public static final mtrl_auto_complete_simple_item:I = 0x7f0d05ec

.field public static final mtrl_calendar_day:I = 0x7f0d05ed

.field public static final mtrl_calendar_day_of_week:I = 0x7f0d05ee

.field public static final mtrl_calendar_days_of_week:I = 0x7f0d05ef

.field public static final mtrl_calendar_horizontal:I = 0x7f0d05f0

.field public static final mtrl_calendar_month:I = 0x7f0d05f1

.field public static final mtrl_calendar_month_labeled:I = 0x7f0d05f2

.field public static final mtrl_calendar_month_navigation:I = 0x7f0d05f3

.field public static final mtrl_calendar_months:I = 0x7f0d05f4

.field public static final mtrl_calendar_vertical:I = 0x7f0d05f5

.field public static final mtrl_calendar_year:I = 0x7f0d05f6

.field public static final mtrl_layout_snackbar:I = 0x7f0d05f7

.field public static final mtrl_layout_snackbar_include:I = 0x7f0d05f8

.field public static final mtrl_navigation_rail_item:I = 0x7f0d05f9

.field public static final mtrl_picker_actions:I = 0x7f0d05fa

.field public static final mtrl_picker_dialog:I = 0x7f0d05fb

.field public static final mtrl_picker_fullscreen:I = 0x7f0d05fc

.field public static final mtrl_picker_header_dialog:I = 0x7f0d05fd

.field public static final mtrl_picker_header_fullscreen:I = 0x7f0d05fe

.field public static final mtrl_picker_header_selection_text:I = 0x7f0d05ff

.field public static final mtrl_picker_header_title_text:I = 0x7f0d0600

.field public static final mtrl_picker_header_toggle:I = 0x7f0d0601

.field public static final mtrl_picker_text_input_date:I = 0x7f0d0602

.field public static final mtrl_picker_text_input_date_range:I = 0x7f0d0603

.field public static final mtrl_search_bar:I = 0x7f0d0604

.field public static final mtrl_search_view:I = 0x7f0d0605

.field public static final naire_question_layout:I = 0x7f0d0606

.field public static final naire_result_layout:I = 0x7f0d0607

.field public static final notification_action:I = 0x7f0d060e

.field public static final notification_action_tombstone:I = 0x7f0d060f

.field public static final notification_media_action:I = 0x7f0d0610

.field public static final notification_media_cancel_action:I = 0x7f0d0611

.field public static final notification_template_big_media:I = 0x7f0d0612

.field public static final notification_template_big_media_custom:I = 0x7f0d0613

.field public static final notification_template_big_media_narrow:I = 0x7f0d0614

.field public static final notification_template_big_media_narrow_custom:I = 0x7f0d0615

.field public static final notification_template_custom_big:I = 0x7f0d0616

.field public static final notification_template_icon_group:I = 0x7f0d0617

.field public static final notification_template_lines_media:I = 0x7f0d0618

.field public static final notification_template_media:I = 0x7f0d0619

.field public static final notification_template_media_custom:I = 0x7f0d061a

.field public static final notification_template_part_chronometer:I = 0x7f0d061b

.field public static final notification_template_part_time:I = 0x7f0d061c

.field public static final page_list_image_container:I = 0x7f0d0631

.field public static final pagelist_image_text_container:I = 0x7f0d0636

.field public static final pager_navigator_layout:I = 0x7f0d063a

.field public static final pager_navigator_layout_no_scroll:I = 0x7f0d063b

.field public static final pnl_cancel_account_des_item:I = 0x7f0d064c

.field public static final pnl_cancel_warning_item:I = 0x7f0d064d

.field public static final pnl_dlg_btn:I = 0x7f0d0675

.field public static final pnl_fail_cancel_reason:I = 0x7f0d0681

.field public static final pnl_ko_registration_agreement:I = 0x7f0d0690

.field public static final pnl_login_tips:I = 0x7f0d0693

.field public static final pnl_premium_image_layout:I = 0x7f0d06b4

.field public static final pnl_reappear:I = 0x7f0d06c5

.field public static final pnl_shortcut:I = 0x7f0d06cd

.field public static final pop_feedback_layout:I = 0x7f0d06e4

.field public static final popup_list_menu_item:I = 0x7f0d06eb

.field public static final popup_list_menu_listview:I = 0x7f0d06ec

.field public static final popup_window_login_guide:I = 0x7f0d06ef

.field public static final protocol_korea:I = 0x7f0d0700

.field public static final router_welcome_main:I = 0x7f0d0718

.field public static final scandone_mult_elements:I = 0x7f0d0719

.field public static final scene_email_pwd_login:I = 0x7f0d071a

.field public static final scene_forget_pwd:I = 0x7f0d071b

.field public static final scene_gp_login:I = 0x7f0d071c

.field public static final scene_mobile_number_input:I = 0x7f0d071d

.field public static final scene_mobile_pwd_login:I = 0x7f0d071e

.field public static final scene_register:I = 0x7f0d071f

.field public static final scene_reset_pwd:I = 0x7f0d0720

.field public static final scene_verify_code_input:I = 0x7f0d0721

.field public static final select_dialog_item_material:I = 0x7f0d0723

.field public static final select_dialog_multichoice_material:I = 0x7f0d0724

.field public static final select_dialog_singlechoice_material:I = 0x7f0d0726

.field public static final share_exit_layout:I = 0x7f0d072e

.field public static final share_exit_only_image_layout:I = 0x7f0d072f

.field public static final simple_dropdown_item_1line:I = 0x7f0d0737

.field public static final simple_list_vip_item:I = 0x7f0d0738

.field public static final single_rv_container:I = 0x7f0d0739

.field public static final support_simple_spinner_dropdown_item:I = 0x7f0d0741

.field public static final sync_progress:I = 0x7f0d0743

.field public static final toolbar_overlay_layout:I = 0x7f0d0759

.field public static final util_dlg_share:I = 0x7f0d0761

.field public static final util_item_squared_share:I = 0x7f0d0762

.field public static final util_view_circle_image_text_button:I = 0x7f0d0763

.field public static final util_view_image_text_button:I = 0x7f0d0764

.field public static final view_nine_photo:I = 0x7f0d076d

.field public static final view_pager_item_normal:I = 0x7f0d0772

.field public static final view_pager_item_one_title:I = 0x7f0d0773

.field public static final view_pager_item_one_title_new:I = 0x7f0d0774

.field public static final view_pager_item_one_title_new_beidong:I = 0x7f0d0775

.field public static final view_pager_item_program:I = 0x7f0d0776

.field public static final view_toolbar_dark_layout:I = 0x7f0d0785

.field public static final view_toolbar_layout:I = 0x7f0d0786

.field public static final view_verify_code:I = 0x7f0d078a

.field public static final view_verify_code_rect_style:I = 0x7f0d078b

.field public static final x_edittext:I = 0x7f0d07b0


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

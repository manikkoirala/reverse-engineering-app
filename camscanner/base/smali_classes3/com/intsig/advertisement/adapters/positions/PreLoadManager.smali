.class public Lcom/intsig/advertisement/adapters/positions/PreLoadManager;
.super Ljava/lang/Object;
.source "PreLoadManager.java"

# interfaces
.implements Lcom/intsig/okgo/OkGoUtils$DownloadListener;


# instance fields
.field private O8:I

.field private Oo08:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/advertisement/cache/CacheFile;",
            ">;"
        }
    .end annotation
.end field

.field private o〇0:Z

.field private 〇080:Lcom/google/gson/JsonObject;

.field private 〇o00〇〇Oo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/advertisement/cache/CacheFile;",
            ">;"
        }
    .end annotation
.end field

.field private 〇o〇:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/gson/JsonObject;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->o〇0:Z

    .line 6
    .line 7
    iput-object p1, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->〇o〇:Landroid/content/Context;

    .line 8
    .line 9
    iput-object p2, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->〇080:Lcom/google/gson/JsonObject;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method static bridge synthetic O8(Lcom/intsig/advertisement/adapters/positions/PreLoadManager;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->Oo08:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private OO0o〇〇〇〇0(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/advertisement/adapters/sources/api/sdk/preload/PreloadInfo;",
            ">;)",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/advertisement/cache/CacheFile;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    if-eqz p1, :cond_2

    .line 7
    .line 8
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    if-lez v1, :cond_2

    .line 13
    .line 14
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    if-eqz v1, :cond_2

    .line 23
    .line 24
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    check-cast v1, Lcom/intsig/advertisement/adapters/sources/api/sdk/preload/PreloadInfo;

    .line 29
    .line 30
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/preload/PreloadInfo;->getVideo()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v2

    .line 34
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 35
    .line 36
    .line 37
    move-result v3

    .line 38
    if-nez v3, :cond_1

    .line 39
    .line 40
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/preload/PreloadInfo;->getVideo()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v3

    .line 44
    invoke-static {v3}, Lcom/intsig/advertisement/util/FileDownLoadUtil;->Oo08(Ljava/lang/String;)Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v3

    .line 48
    new-instance v4, Lcom/intsig/advertisement/cache/CacheFile;

    .line 49
    .line 50
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/preload/PreloadInfo;->getExpire_time()J

    .line 51
    .line 52
    .line 53
    move-result-wide v5

    .line 54
    invoke-direct {v4, v2, v3, v5, v6}, Lcom/intsig/advertisement/cache/CacheFile;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 58
    .line 59
    .line 60
    :cond_1
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/preload/PreloadInfo;->getPic()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v2

    .line 64
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 65
    .line 66
    .line 67
    move-result v3

    .line 68
    if-nez v3, :cond_0

    .line 69
    .line 70
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/preload/PreloadInfo;->getPic()Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v3

    .line 74
    invoke-static {v3}, Lcom/intsig/advertisement/util/FileDownLoadUtil;->Oo08(Ljava/lang/String;)Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object v3

    .line 78
    new-instance v4, Lcom/intsig/advertisement/cache/CacheFile;

    .line 79
    .line 80
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/preload/PreloadInfo;->getExpire_time()J

    .line 81
    .line 82
    .line 83
    move-result-wide v5

    .line 84
    invoke-direct {v4, v2, v3, v5, v6}, Lcom/intsig/advertisement/cache/CacheFile;-><init>(Ljava/lang/String;Ljava/lang/String;J)V

    .line 85
    .line 86
    .line 87
    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    .line 89
    .line 90
    goto :goto_0

    .line 91
    :cond_2
    return-object v0
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method static bridge synthetic Oo08(Lcom/intsig/advertisement/adapters/positions/PreLoadManager;I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->O8:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method static bridge synthetic oO80(Lcom/intsig/advertisement/adapters/positions/PreLoadManager;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->〇8o8o〇(I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method static bridge synthetic o〇0(Lcom/intsig/advertisement/adapters/positions/PreLoadManager;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method static bridge synthetic 〇080(Lcom/intsig/advertisement/adapters/positions/PreLoadManager;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->O8:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private 〇80〇808〇O(Lcom/intsig/advertisement/cache/CacheFile;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->Oo08:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 8
    .line 9
    .line 10
    move-result v1

    .line 11
    if-eqz v1, :cond_1

    .line 12
    .line 13
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    check-cast v1, Lcom/intsig/advertisement/cache/CacheFile;

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/advertisement/cache/CacheFile;->getFileUrl()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    invoke-virtual {p1}, Lcom/intsig/advertisement/cache/CacheFile;->getFileUrl()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v3

    .line 27
    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 28
    .line 29
    .line 30
    move-result v2

    .line 31
    if-eqz v2, :cond_0

    .line 32
    .line 33
    invoke-virtual {p1}, Lcom/intsig/advertisement/cache/CacheFile;->getExpireTime()J

    .line 34
    .line 35
    .line 36
    move-result-wide v2

    .line 37
    invoke-virtual {v1, v2, v3}, Lcom/intsig/advertisement/cache/CacheFile;->setExpireTime(J)V

    .line 38
    .line 39
    .line 40
    return-void

    .line 41
    :cond_1
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->Oo08:Ljava/util/ArrayList;

    .line 42
    .line 43
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private 〇8o8o〇(I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-ge p1, v0, :cond_1

    .line 8
    .line 9
    if-ltz p1, :cond_1

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 12
    .line 13
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    check-cast p1, Lcom/intsig/advertisement/cache/CacheFile;

    .line 18
    .line 19
    invoke-virtual {p1}, Lcom/intsig/advertisement/cache/CacheFile;->getCachePath()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    const-string v1, "PreLoadManager"

    .line 28
    .line 29
    if-eqz v0, :cond_0

    .line 30
    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    .line 32
    .line 33
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 34
    .line 35
    .line 36
    const-string v2, "it has cache url="

    .line 37
    .line 38
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 39
    .line 40
    .line 41
    invoke-virtual {p1}, Lcom/intsig/advertisement/cache/CacheFile;->getFileUrl()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 46
    .line 47
    .line 48
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    invoke-static {v1, p1}, Lcom/intsig/advertisement/logagent/LogPrinter;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    iget p1, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->O8:I

    .line 56
    .line 57
    add-int/lit8 p1, p1, 0x1

    .line 58
    .line 59
    iput p1, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->O8:I

    .line 60
    .line 61
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->〇8o8o〇(I)V

    .line 62
    .line 63
    .line 64
    goto :goto_0

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->〇o〇:Landroid/content/Context;

    .line 66
    .line 67
    invoke-virtual {p1}, Lcom/intsig/advertisement/cache/CacheFile;->getFileUrl()Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object p1

    .line 71
    invoke-static {v1, v0, p1, p0}, Lcom/intsig/advertisement/util/FileDownLoadUtil;->〇o00〇〇Oo(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Lcom/intsig/okgo/OkGoUtils$DownloadListener;)V

    .line 72
    .line 73
    .line 74
    goto :goto_0

    .line 75
    :cond_1
    iget-boolean p1, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->o〇0:Z

    .line 76
    .line 77
    if-eqz p1, :cond_2

    .line 78
    .line 79
    iget-object p1, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->〇o〇:Landroid/content/Context;

    .line 80
    .line 81
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->Oo08:Ljava/util/ArrayList;

    .line 82
    .line 83
    invoke-static {p1, v0}, Lcom/intsig/advertisement/cache/AdCacheManager;->Oo08(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 84
    .line 85
    .line 86
    :cond_2
    :goto_0
    return-void
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/advertisement/adapters/positions/PreLoadManager;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->〇o〇:Landroid/content/Context;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/advertisement/adapters/positions/PreLoadManager;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic 〇〇888(Lcom/intsig/advertisement/adapters/positions/PreLoadManager;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->OO0o〇〇〇〇0(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method


# virtual methods
.method public onFail(Ljava/lang/String;)V
    .locals 0

    .line 1
    iget p1, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->O8:I

    .line 2
    .line 3
    add-int/lit8 p1, p1, 0x1

    .line 4
    .line 5
    iput p1, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->O8:I

    .line 6
    .line 7
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->〇8o8o〇(I)V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public onProgress(JJ)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public onSuccess()V
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->o〇0:Z

    .line 3
    .line 4
    iget-object v1, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 5
    .line 6
    iget v2, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->O8:I

    .line 7
    .line 8
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    check-cast v1, Lcom/intsig/advertisement/cache/CacheFile;

    .line 13
    .line 14
    invoke-direct {p0, v1}, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->〇80〇808〇O(Lcom/intsig/advertisement/cache/CacheFile;)V

    .line 15
    .line 16
    .line 17
    iget v1, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->O8:I

    .line 18
    .line 19
    add-int/2addr v1, v0

    .line 20
    iput v1, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->O8:I

    .line 21
    .line 22
    invoke-direct {p0, v1}, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->〇8o8o〇(I)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public 〇O8o08O()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/ApiAdRequest;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->〇o〇:Landroid/content/Context;

    .line 4
    .line 5
    new-instance v2, Lcom/intsig/advertisement/adapters/positions/PreLoadManager$1;

    .line 6
    .line 7
    invoke-direct {v2, p0}, Lcom/intsig/advertisement/adapters/positions/PreLoadManager$1;-><init>(Lcom/intsig/advertisement/adapters/positions/PreLoadManager;)V

    .line 8
    .line 9
    .line 10
    invoke-direct {v0, v1, v2}, Lcom/intsig/advertisement/adapters/sources/api/sdk/ApiAdRequest;-><init>(Landroid/content/Context;Lcom/intsig/advertisement/listener/OnAdRequestListener;)V

    .line 11
    .line 12
    .line 13
    sget-object v1, Lcom/intsig/advertisement/adapters/sources/api/sdk/ApiAdRequest$ApiPosition;->AppLaunch:Lcom/intsig/advertisement/adapters/sources/api/sdk/ApiAdRequest$ApiPosition;

    .line 14
    .line 15
    iget-object v2, p0, Lcom/intsig/advertisement/adapters/positions/PreLoadManager;->〇080:Lcom/google/gson/JsonObject;

    .line 16
    .line 17
    invoke-virtual {v0, v1, v2}, Lcom/intsig/advertisement/adapters/sources/api/sdk/ApiAdRequest;->oO80(Lcom/intsig/advertisement/adapters/sources/api/sdk/ApiAdRequest$ApiPosition;Lcom/google/gson/JsonObject;)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

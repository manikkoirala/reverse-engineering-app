.class public final Lcom/intsig/advertisement/adapters/positions/vir/VirAppLaunchManager;
.super Lcom/intsig/advertisement/adapters/AbsPositionAdapter;
.source "VirAppLaunchManager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/advertisement/adapters/positions/vir/VirAppLaunchManager$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final OO0o〇〇:Lcom/intsig/advertisement/adapters/positions/vir/VirAppLaunchManager$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static Oooo8o0〇:Lcom/intsig/advertisement/adapters/positions/vir/VirAppLaunchManager;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private 〇O8o08O:Lcom/intsig/advertisement/enums/AppLaunchType;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/advertisement/adapters/positions/vir/VirAppLaunchManager$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/advertisement/adapters/positions/vir/VirAppLaunchManager$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/advertisement/adapters/positions/vir/VirAppLaunchManager;->OO0o〇〇:Lcom/intsig/advertisement/adapters/positions/vir/VirAppLaunchManager$Companion;

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/advertisement/adapters/positions/vir/VirAppLaunchManager;

    .line 10
    .line 11
    invoke-direct {v0}, Lcom/intsig/advertisement/adapters/positions/vir/VirAppLaunchManager;-><init>()V

    .line 12
    .line 13
    .line 14
    sput-object v0, Lcom/intsig/advertisement/adapters/positions/vir/VirAppLaunchManager;->Oooo8o0〇:Lcom/intsig/advertisement/adapters/positions/vir/VirAppLaunchManager;

    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;-><init>()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/advertisement/enums/AppLaunchType;->ColdBoot:Lcom/intsig/advertisement/enums/AppLaunchType;

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/advertisement/adapters/positions/vir/VirAppLaunchManager;->〇O8o08O:Lcom/intsig/advertisement/enums/AppLaunchType;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public static final synthetic O〇O〇oO()Lcom/intsig/advertisement/adapters/positions/vir/VirAppLaunchManager;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/advertisement/adapters/positions/vir/VirAppLaunchManager;->Oooo8o0〇:Lcom/intsig/advertisement/adapters/positions/vir/VirAppLaunchManager;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method


# virtual methods
.method protected OO0o〇〇()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/advertisement/enums/SourceType;->CS:Lcom/intsig/advertisement/enums/SourceType;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/advertisement/enums/AdType;->Splash:Lcom/intsig/advertisement/enums/AdType;

    .line 4
    .line 5
    invoke-virtual {p0, v0, v1}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->〇8(Lcom/intsig/advertisement/enums/SourceType;Lcom/intsig/advertisement/enums/AdType;)V

    .line 6
    .line 7
    .line 8
    sget-object v2, Lcom/intsig/advertisement/enums/SourceType;->API:Lcom/intsig/advertisement/enums/SourceType;

    .line 9
    .line 10
    invoke-virtual {p0, v2, v1}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->〇8(Lcom/intsig/advertisement/enums/SourceType;Lcom/intsig/advertisement/enums/AdType;)V

    .line 11
    .line 12
    .line 13
    sget-object v1, Lcom/intsig/advertisement/enums/AdType;->Native:Lcom/intsig/advertisement/enums/AdType;

    .line 14
    .line 15
    invoke-virtual {p0, v0, v1}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->〇8(Lcom/intsig/advertisement/enums/SourceType;Lcom/intsig/advertisement/enums/AdType;)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {p0, v2, v1}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->〇8(Lcom/intsig/advertisement/enums/SourceType;Lcom/intsig/advertisement/enums/AdType;)V

    .line 19
    .line 20
    .line 21
    sget-object v0, Lcom/intsig/advertisement/enums/AdType;->Interstitial:Lcom/intsig/advertisement/enums/AdType;

    .line 22
    .line 23
    invoke-virtual {p0, v2, v0}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->〇8(Lcom/intsig/advertisement/enums/SourceType;Lcom/intsig/advertisement/enums/AdType;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public final o8oO〇(Lcom/intsig/advertisement/enums/AppLaunchType;)V
    .locals 1
    .param p1    # Lcom/intsig/advertisement/enums/AppLaunchType;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/advertisement/adapters/positions/vir/VirAppLaunchManager;->〇O8o08O:Lcom/intsig/advertisement/enums/AppLaunchType;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public oO80(Lcom/intsig/advertisement/params/NativeParam;)Lcom/intsig/advertisement/params/NativeParam;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const-string v0, "key_launch_type"

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/advertisement/adapters/positions/vir/VirAppLaunchManager;->〇O8o08O:Lcom/intsig/advertisement/enums/AppLaunchType;

    .line 6
    .line 7
    invoke-virtual {p1, v0, v1}, Lcom/intsig/advertisement/params/RequestParam;->〇〇0o(Ljava/lang/String;Ljava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    :cond_0
    invoke-super {p0, p1}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->oO80(Lcom/intsig/advertisement/params/NativeParam;)Lcom/intsig/advertisement/params/NativeParam;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    const-string/jumbo v0, "super.appendNativeParam(nativeParam)"

    .line 15
    .line 16
    .line 17
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    return-object p1
    .line 21
    .line 22
.end method

.method public o〇0OOo〇0(ILcom/intsig/advertisement/interfaces/RealRequestAbs;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/intsig/advertisement/interfaces/RealRequestAbs<",
            "*+",
            "Lcom/intsig/advertisement/listener/OnAdShowListener<",
            "*>;*>;)V"
        }
    .end annotation

    .line 1
    invoke-super {p0, p1, p2}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->o〇0OOo〇0(ILcom/intsig/advertisement/interfaces/RealRequestAbs;)V

    .line 2
    .line 3
    .line 4
    invoke-static {p2}, Lcom/intsig/advertisement/util/CommonUtil;->〇〇8O0〇8(Lcom/intsig/advertisement/interfaces/RealRequestAbs;)Ljava/lang/Boolean;

    .line 5
    .line 6
    .line 7
    move-result-object p1

    .line 8
    const-string v0, "isVirReportImpression(realRequestAbs)"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    .line 14
    .line 15
    .line 16
    move-result p1

    .line 17
    if-eqz p1, :cond_0

    .line 18
    .line 19
    if-eqz p2, :cond_1

    .line 20
    .line 21
    invoke-virtual {p2}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyForVirImpression()V

    .line 22
    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const-string p1, "not meet impression range"

    .line 26
    .line 27
    invoke-virtual {p0, p1}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->〇08O8o〇0(Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    :cond_1
    :goto_0
    if-nez p2, :cond_2

    .line 31
    .line 32
    goto :goto_1

    .line 33
    :cond_2
    const/4 p1, 0x1

    .line 34
    invoke-virtual {p2, p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->setHasUsed(Z)V

    .line 35
    .line 36
    .line 37
    :goto_1
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public o〇〇0〇()Lcom/intsig/advertisement/enums/PositionType;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/advertisement/enums/PositionType;->VirAppLaunch:Lcom/intsig/advertisement/enums/PositionType;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇8o8o〇(Lcom/intsig/advertisement/params/SplashParam;)Lcom/intsig/advertisement/params/SplashParam;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const-string v0, "key_launch_type"

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/advertisement/adapters/positions/vir/VirAppLaunchManager;->〇O8o08O:Lcom/intsig/advertisement/enums/AppLaunchType;

    .line 6
    .line 7
    invoke-virtual {p1, v0, v1}, Lcom/intsig/advertisement/params/RequestParam;->〇〇0o(Ljava/lang/String;Ljava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    :cond_0
    invoke-super {p0, p1}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->〇8o8o〇(Lcom/intsig/advertisement/params/SplashParam;)Lcom/intsig/advertisement/params/SplashParam;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    const-string/jumbo v0, "super.appendSplashParam(splashParam)"

    .line 15
    .line 16
    .line 17
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    return-object p1
    .line 21
    .line 22
.end method

.method protected 〇oo〇(Lcom/intsig/advertisement/bean/ConfigResponse;)Lcom/intsig/advertisement/bean/PosFlowCfg;
    .locals 2

    .line 1
    if-eqz p1, :cond_1

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/positions/vir/VirAppLaunchManager;->〇O8o08O:Lcom/intsig/advertisement/enums/AppLaunchType;

    .line 4
    .line 5
    sget-object v1, Lcom/intsig/advertisement/enums/AppLaunchType;->ColdBoot:Lcom/intsig/advertisement/enums/AppLaunchType;

    .line 6
    .line 7
    if-ne v0, v1, :cond_0

    .line 8
    .line 9
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getAppColdLaunch()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getVirColdApplaunch()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    invoke-virtual {p0, p1}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->〇O00(Lcom/intsig/advertisement/bean/ItemConfig;)Lcom/intsig/advertisement/bean/PosFlowCfg;

    .line 20
    .line 21
    .line 22
    move-result-object p1

    .line 23
    goto :goto_0

    .line 24
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/advertisement/bean/ConfigResponse;->getVirApplaunch()Lcom/intsig/advertisement/bean/ItemConfig;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    invoke-virtual {p0, p1}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->〇O00(Lcom/intsig/advertisement/bean/ItemConfig;)Lcom/intsig/advertisement/bean/PosFlowCfg;

    .line 29
    .line 30
    .line 31
    move-result-object p1

    .line 32
    :goto_0
    return-object p1

    .line 33
    :cond_1
    const/4 p1, 0x0

    .line 34
    return-object p1
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public 〇〇888(Lcom/intsig/advertisement/params/InterstitialParam;)Lcom/intsig/advertisement/params/InterstitialParam;
    .locals 2
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    const-string v0, "key_launch_type"

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/advertisement/adapters/positions/vir/VirAppLaunchManager;->〇O8o08O:Lcom/intsig/advertisement/enums/AppLaunchType;

    .line 6
    .line 7
    invoke-virtual {p1, v0, v1}, Lcom/intsig/advertisement/params/RequestParam;->〇〇0o(Ljava/lang/String;Ljava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    :cond_0
    invoke-super {p0, p1}, Lcom/intsig/advertisement/adapters/AbsPositionAdapter;->〇〇888(Lcom/intsig/advertisement/params/InterstitialParam;)Lcom/intsig/advertisement/params/InterstitialParam;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    const-string/jumbo v0, "super.appendInterstitialParam(interstitialParam)"

    .line 15
    .line 16
    .line 17
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    return-object p1
    .line 21
    .line 22
.end method

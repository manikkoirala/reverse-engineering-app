.class Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner$1;
.super Lcom/google/android/gms/ads/AdListener;
.source "AdmobBanner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;->onRealRequest(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic o0:Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;


# direct methods
.method constructor <init>(Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner$1;->o0:Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/google/android/gms/ads/AdListener;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public onAdClicked()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner$1;->o0:Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyOnClick()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public onAdClosed()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner$1;->o0:Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyOnClose()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public onAdFailedToLoad(Lcom/google/android/gms/ads/LoadAdError;)V
    .locals 4
    .param p1    # Lcom/google/android/gms/ads/LoadAdError;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-super {p0, p1}, Lcom/google/android/gms/ads/AdListener;->onAdFailedToLoad(Lcom/google/android/gms/ads/LoadAdError;)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner$1;->o0:Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;

    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/google/android/gms/ads/AdError;->getCode()I

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    new-instance v2, Ljava/lang/StringBuilder;

    .line 11
    .line 12
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 13
    .line 14
    .line 15
    const-string v3, "onAdFailedToLoad message="

    .line 16
    .line 17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1}, Lcom/google/android/gms/ads/AdError;->getMessage()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 25
    .line 26
    .line 27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    invoke-virtual {v0, v1, p1}, Lcom/intsig/advertisement/interfaces/BannerRequest;->notifyOnFailed(ILjava/lang/String;)V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public onAdImpression()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner$1;->o0:Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    const-string v2, "onAdImpression"

    .line 5
    .line 6
    invoke-static {v0, v1, v2}, Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;->〇〇808〇(Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;ZLjava/lang/String;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public onAdLoaded()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner$1;->o0:Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;

    .line 2
    .line 3
    invoke-static {v0}, Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;->〇8o8o〇(Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner$1;->o0:Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/advertisement/interfaces/BannerRequest;->notifyOnShowSucceed()V

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner$1;->o0:Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;

    .line 15
    .line 16
    const/4 v1, 0x0

    .line 17
    const-string v2, "banner notify refresh show"

    .line 18
    .line 19
    invoke-static {v0, v1, v2}, Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;->〇O8o08O(Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;ZLjava/lang/String;)V

    .line 20
    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_0
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner$1;->o0:Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;

    .line 24
    .line 25
    iget-object v1, v0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 26
    .line 27
    check-cast v1, Lcom/google/android/gms/ads/AdView;

    .line 28
    .line 29
    invoke-static {v0}, Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;->OO0o〇〇〇〇0(Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;)Lcom/google/android/gms/ads/OnPaidEventListener;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    invoke-virtual {v1, v0}, Lcom/google/android/gms/ads/BaseAdView;->setOnPaidEventListener(Lcom/google/android/gms/ads/OnPaidEventListener;)V

    .line 34
    .line 35
    .line 36
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner$1;->o0:Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;

    .line 37
    .line 38
    invoke-static {v0}, Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;->OO0o〇〇(Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;)V

    .line 39
    .line 40
    .line 41
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner$1;->o0:Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;

    .line 42
    .line 43
    iget-object v0, v0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 44
    .line 45
    check-cast v0, Lcom/google/android/gms/ads/AdView;

    .line 46
    .line 47
    invoke-virtual {v0}, Lcom/google/android/gms/ads/BaseAdView;->getResponseInfo()Lcom/google/android/gms/ads/ResponseInfo;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    if-eqz v0, :cond_1

    .line 52
    .line 53
    iget-object v1, p0, Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner$1;->o0:Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;

    .line 54
    .line 55
    new-instance v2, Ljava/lang/StringBuilder;

    .line 56
    .line 57
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 58
    .line 59
    .line 60
    const-string v3, "ad from:"

    .line 61
    .line 62
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    invoke-virtual {v0}, Lcom/google/android/gms/ads/ResponseInfo;->getMediationAdapterClassName()Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v0

    .line 76
    const/4 v2, 0x1

    .line 77
    invoke-static {v1, v2, v0}, Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;->Oooo8o0〇(Lcom/intsig/advertisement/adapters/sources/admob/AdmobBanner;ZLjava/lang/String;)V

    .line 78
    .line 79
    .line 80
    :cond_1
    :goto_0
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public onAdOpened()V
    .locals 0

    .line 1
    invoke-super {p0}, Lcom/google/android/gms/ads/AdListener;->onAdOpened()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.class public Lcom/intsig/advertisement/adapters/sources/cs/CsRewardVideo;
.super Lcom/intsig/advertisement/interfaces/RewardVideoRequest;
.source "CsRewardVideo.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/intsig/advertisement/interfaces/RewardVideoRequest<",
        "Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/intsig/advertisement/params/RewardVideoParam;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/interfaces/RewardVideoRequest;-><init>(Lcom/intsig/advertisement/params/RewardVideoParam;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static synthetic OO0o〇〇(Lcom/intsig/advertisement/adapters/sources/cs/CsRewardVideo;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/advertisement/interfaces/RewardVideoRequest;->notifyOnReward()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static synthetic OO0o〇〇〇〇0(Lcom/intsig/advertisement/adapters/sources/cs/CsRewardVideo;)Lcom/intsig/advertisement/params/RequestParam;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private checkCache(Landroid/content/Context;Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;)V
    .locals 3

    .line 1
    invoke-virtual {p2}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getVideoLocalPath()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-direct {p0, p2}, Lcom/intsig/advertisement/adapters/sources/cs/CsRewardVideo;->onSucceed(Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;)V

    .line 12
    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 v0, 0x0

    .line 16
    const-string/jumbo v1, "start download video--"

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0, v0, v1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->printLog(ZLjava/lang/String;)V

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mTag:Ljava/lang/String;

    .line 23
    .line 24
    invoke-virtual {p2}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getVideo()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    new-instance v2, Lcom/intsig/advertisement/adapters/sources/cs/CsRewardVideo$2;

    .line 29
    .line 30
    invoke-direct {v2, p0, p2}, Lcom/intsig/advertisement/adapters/sources/cs/CsRewardVideo$2;-><init>(Lcom/intsig/advertisement/adapters/sources/cs/CsRewardVideo;Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;)V

    .line 31
    .line 32
    .line 33
    invoke-static {v0, p1, v1, v2}, Lcom/intsig/advertisement/util/FileDownLoadUtil;->〇o00〇〇Oo(Ljava/lang/String;Landroid/content/Context;Ljava/lang/String;Lcom/intsig/okgo/OkGoUtils$DownloadListener;)V

    .line 34
    .line 35
    .line 36
    :goto_0
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method static bridge synthetic oO80(Lcom/intsig/advertisement/adapters/sources/cs/CsRewardVideo;Landroid/content/Context;Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/advertisement/adapters/sources/cs/CsRewardVideo;->checkCache(Landroid/content/Context;Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private onSucceed(Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;)V
    .locals 2

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 2
    .line 3
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 4
    .line 5
    check-cast v0, Lcom/intsig/advertisement/params/RewardVideoParam;

    .line 6
    .line 7
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getId()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    invoke-virtual {v0, p1}, Lcom/intsig/advertisement/params/RequestParam;->O8〇o(Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    iget-object p1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 15
    .line 16
    check-cast p1, Lcom/intsig/advertisement/params/RewardVideoParam;

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 19
    .line 20
    check-cast v0, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;

    .line 21
    .line 22
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;->getSourceType()I

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    const/4 v1, 0x2

    .line 27
    if-ne v0, v1, :cond_0

    .line 28
    .line 29
    const-string v0, "operation"

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_0
    const-string v0, "ad"

    .line 33
    .line 34
    :goto_0
    const-string v1, "operation_type"

    .line 35
    .line 36
    invoke-virtual {p1, v1, v0}, Lcom/intsig/advertisement/params/RequestParam;->〇〇0o(Ljava/lang/String;Ljava/lang/Object;)V

    .line 37
    .line 38
    .line 39
    invoke-virtual {p0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyOnSucceed()V

    .line 40
    .line 41
    .line 42
    return-void
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method static bridge synthetic 〇80〇808〇O(Lcom/intsig/advertisement/adapters/sources/cs/CsRewardVideo;Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/adapters/sources/cs/CsRewardVideo;->onSucceed(Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method static synthetic 〇8o8o〇(Lcom/intsig/advertisement/adapters/sources/cs/CsRewardVideo;)Lcom/intsig/advertisement/params/RequestParam;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static synthetic 〇O8o08O(Lcom/intsig/advertisement/adapters/sources/cs/CsRewardVideo;)Lcom/intsig/advertisement/params/RequestParam;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public onPlayVideo(Landroid/content/Context;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/advertisement/interfaces/RewardVideoRequest;->onPlayVideo(Landroid/content/Context;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->isActivityFinish(Landroid/content/Context;)Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    const/4 v0, -0x1

    .line 11
    const-string v1, "activity is finish"

    .line 12
    .line 13
    invoke-virtual {p0, v0, v1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyOnShowFailed(ILjava/lang/String;)V

    .line 14
    .line 15
    .line 16
    :cond_0
    new-instance v0, Lcom/intsig/advertisement/view/rewardvideo/RewardVideoHelper;

    .line 17
    .line 18
    invoke-direct {v0}, Lcom/intsig/advertisement/view/rewardvideo/RewardVideoHelper;-><init>()V

    .line 19
    .line 20
    .line 21
    new-instance v1, Lcom/intsig/advertisement/adapters/sources/cs/CsRewardVideo$3;

    .line 22
    .line 23
    invoke-direct {v1, p0}, Lcom/intsig/advertisement/adapters/sources/cs/CsRewardVideo$3;-><init>(Lcom/intsig/advertisement/adapters/sources/cs/CsRewardVideo;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/view/rewardvideo/RewardVideoHelper;->〇o00〇〇Oo(Lcom/intsig/advertisement/listener/OnRewardVideoAdListener;)V

    .line 27
    .line 28
    .line 29
    check-cast p1, Landroid/app/Activity;

    .line 30
    .line 31
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 32
    .line 33
    check-cast v1, Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;

    .line 34
    .line 35
    invoke-virtual {v0, p1, v1}, Lcom/intsig/advertisement/view/rewardvideo/RewardVideoHelper;->〇080(Landroid/app/Activity;Lcom/intsig/advertisement/adapters/sources/cs/CsAdDataBeanN;)V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method protected onRequest(Landroid/content/Context;)V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/advertisement/control/AdConfigManager;->〇o〇:Lcom/intsig/advertisement/control/AdInfoCallback;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 6
    .line 7
    check-cast v1, Lcom/intsig/advertisement/params/RewardVideoParam;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/advertisement/params/RequestParam;->〇〇8O0〇8()Lcom/intsig/advertisement/enums/SourceType;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    iget-object v2, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 14
    .line 15
    check-cast v2, Lcom/intsig/advertisement/params/RewardVideoParam;

    .line 16
    .line 17
    invoke-virtual {v2}, Lcom/intsig/advertisement/params/RequestParam;->〇O8o08O()Lcom/intsig/advertisement/enums/PositionType;

    .line 18
    .line 19
    .line 20
    move-result-object v2

    .line 21
    new-instance v3, Lcom/intsig/advertisement/adapters/sources/cs/CsRewardVideo$1;

    .line 22
    .line 23
    invoke-direct {v3, p0, p1}, Lcom/intsig/advertisement/adapters/sources/cs/CsRewardVideo$1;-><init>(Lcom/intsig/advertisement/adapters/sources/cs/CsRewardVideo;Landroid/content/Context;)V

    .line 24
    .line 25
    .line 26
    invoke-interface {v0, p1, v1, v2, v3}, Lcom/intsig/advertisement/control/AdInfoCallback;->O〇8O8〇008(Landroid/content/Context;Lcom/intsig/advertisement/enums/SourceType;Lcom/intsig/advertisement/enums/PositionType;Lcom/intsig/advertisement/listener/OnAdRequestListener;)V

    .line 27
    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_0
    const/4 p1, -0x1

    .line 31
    const-string v0, "AdInfoCallback is null"

    .line 32
    .line 33
    invoke-virtual {p0, p1, v0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyOnFailed(ILjava/lang/String;)V

    .line 34
    .line 35
    .line 36
    :goto_0
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

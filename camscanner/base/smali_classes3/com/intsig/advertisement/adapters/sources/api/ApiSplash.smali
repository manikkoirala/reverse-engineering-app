.class public Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;
.super Lcom/intsig/advertisement/interfaces/SplashRequest;
.source "ApiSplash.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/intsig/advertisement/interfaces/SplashRequest<",
        "Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;",
        ">;"
    }
.end annotation


# instance fields
.field private mAdClickView:Landroid/view/View;

.field private mediaView:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;


# direct methods
.method public constructor <init>(Lcom/intsig/advertisement/params/RequestParam;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/interfaces/SplashRequest;-><init>(Lcom/intsig/advertisement/params/RequestParam;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static synthetic OO0o〇〇(Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->lambda$setClickArea$5(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static synthetic OO0o〇〇〇〇0(Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->lambda$renderMultiElements$3(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method static synthetic Oooo8o0〇(Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;)Lcom/intsig/advertisement/params/RequestParam;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private addAPiLogView(Landroid/content/Context;Landroid/widget/RelativeLayout;)V
    .locals 4

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->isActivityFinish(Landroid/content/Context;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 9
    .line 10
    check-cast v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getLogoContent()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    if-nez v1, :cond_2

    .line 21
    .line 22
    const-string v1, "http"

    .line 23
    .line 24
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    if-eqz v1, :cond_1

    .line 29
    .line 30
    new-instance v1, Landroid/widget/ImageView;

    .line 31
    .line 32
    invoke-direct {v1, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 33
    .line 34
    .line 35
    const v2, 0x3f666666    # 0.9f

    .line 36
    .line 37
    .line 38
    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 39
    .line 40
    .line 41
    const/16 v2, 0x20

    .line 42
    .line 43
    invoke-static {p1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 44
    .line 45
    .line 46
    move-result v2

    .line 47
    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    .line 48
    .line 49
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 50
    .line 51
    .line 52
    invoke-static {p1}, Lcom/bumptech/glide/Glide;->OoO8(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    .line 53
    .line 54
    .line 55
    move-result-object v3

    .line 56
    invoke-virtual {v3, v0}, Lcom/bumptech/glide/RequestManager;->〇〇808〇(Ljava/lang/String;)Lcom/bumptech/glide/RequestBuilder;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    invoke-virtual {v0, v1}, Lcom/bumptech/glide/RequestBuilder;->Oo〇o(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/ViewTarget;

    .line 61
    .line 62
    .line 63
    invoke-direct {p0, p1, v2, v2}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->getLogLayoutParams(Landroid/content/Context;II)Landroid/widget/RelativeLayout$LayoutParams;

    .line 64
    .line 65
    .line 66
    move-result-object p1

    .line 67
    invoke-virtual {p2, v1, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 68
    .line 69
    .line 70
    goto :goto_0

    .line 71
    :cond_1
    new-instance v1, Lcom/intsig/advertisement/view/AdTagTextView;

    .line 72
    .line 73
    invoke-direct {v1, p1}, Lcom/intsig/advertisement/view/AdTagTextView;-><init>(Landroid/content/Context;)V

    .line 74
    .line 75
    .line 76
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    .line 78
    .line 79
    const-string v0, "#99FFFFFF"

    .line 80
    .line 81
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 82
    .line 83
    .line 84
    move-result v0

    .line 85
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 86
    .line 87
    .line 88
    const/high16 v0, 0x41200000    # 10.0f

    .line 89
    .line 90
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextSize(F)V

    .line 91
    .line 92
    .line 93
    const/16 v0, 0x11

    .line 94
    .line 95
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 96
    .line 97
    .line 98
    const/4 v0, 0x4

    .line 99
    invoke-static {p1, v0}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 100
    .line 101
    .line 102
    move-result v0

    .line 103
    div-int/lit8 v2, v0, 0x2

    .line 104
    .line 105
    invoke-virtual {v1, v0, v2, v0, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 106
    .line 107
    .line 108
    const-string v0, "#66000000"

    .line 109
    .line 110
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 111
    .line 112
    .line 113
    move-result v0

    .line 114
    invoke-virtual {v1, v0}, Lcom/intsig/advertisement/view/AdTagTextView;->setBackGroundColor(I)V

    .line 115
    .line 116
    .line 117
    const/4 v0, 0x2

    .line 118
    invoke-static {p1, v0}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 119
    .line 120
    .line 121
    move-result v0

    .line 122
    invoke-virtual {v1, v0, v0, v0, v0}, Lcom/intsig/advertisement/view/AdTagTextView;->o〇0(IIII)V

    .line 123
    .line 124
    .line 125
    const/4 v0, -0x2

    .line 126
    invoke-direct {p0, p1, v0, v0}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->getLogLayoutParams(Landroid/content/Context;II)Landroid/widget/RelativeLayout$LayoutParams;

    .line 127
    .line 128
    .line 129
    move-result-object p1

    .line 130
    invoke-virtual {p2, v1, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 131
    .line 132
    .line 133
    :cond_2
    :goto_0
    return-void
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

.method private getLogLayoutParams(Landroid/content/Context;II)Landroid/widget/RelativeLayout$LayoutParams;
    .locals 1

    .line 1
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2
    .line 3
    invoke-direct {v0, p2, p3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->getLayoutType()Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 7
    .line 8
    .line 9
    move-result-object p2

    .line 10
    sget-object p3, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;->FULL_LOG_RIGHT_BOTTOM:Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 11
    .line 12
    if-ne p2, p3, :cond_0

    .line 13
    .line 14
    const/16 p2, 0x9

    .line 15
    .line 16
    invoke-virtual {v0, p2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 17
    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    const/16 p2, 0xb

    .line 21
    .line 22
    invoke-virtual {v0, p2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 23
    .line 24
    .line 25
    :goto_0
    const/16 p2, 0xc

    .line 26
    .line 27
    invoke-virtual {v0, p2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 28
    .line 29
    .line 30
    const/4 p2, 0x7

    .line 31
    invoke-static {p1, p2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 32
    .line 33
    .line 34
    move-result p1

    .line 35
    iput p1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 36
    .line 37
    iput p1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 38
    .line 39
    div-int/lit8 p1, p1, 0x2

    .line 40
    .line 41
    iput p1, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 42
    .line 43
    return-object v0
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private initMediaView(Landroid/app/Activity;)V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 2
    .line 3
    invoke-direct {v0, p1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;-><init>(Landroid/content/Context;)V

    .line 4
    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mediaView:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 7
    .line 8
    const/4 p1, 0x1

    .line 9
    invoke-virtual {v0, p1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;->〇oo〇(Z)V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mediaView:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 15
    .line 16
    check-cast v1, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 17
    .line 18
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getUrl()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;->setJumpUrl(Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mediaView:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 26
    .line 27
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 28
    .line 29
    check-cast v1, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 30
    .line 31
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getDp_url()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v1

    .line 35
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;->setJumpDeepLinkUrl(Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mediaView:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 39
    .line 40
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 41
    .line 42
    check-cast v1, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 43
    .line 44
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getClicktrackers()[Ljava/lang/String;

    .line 45
    .line 46
    .line 47
    move-result-object v1

    .line 48
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;->setClickTrackers([Ljava/lang/String;)V

    .line 49
    .line 50
    .line 51
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mediaView:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 52
    .line 53
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 54
    .line 55
    check-cast v1, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 56
    .line 57
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getImptrackers()[Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;->setImpressionTrackers([Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mediaView:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 65
    .line 66
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 67
    .line 68
    check-cast v1, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 69
    .line 70
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getDptrackers()Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/DpLinkTrackers;

    .line 71
    .line 72
    .line 73
    move-result-object v1

    .line 74
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;->setDeepLinkTrackers(Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/DpLinkTrackers;)V

    .line 75
    .line 76
    .line 77
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mediaView:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 78
    .line 79
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 80
    .line 81
    check-cast v1, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 82
    .line 83
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getJumpAlert()I

    .line 84
    .line 85
    .line 86
    move-result v1

    .line 87
    if-ne v1, p1, :cond_0

    .line 88
    .line 89
    goto :goto_0

    .line 90
    :cond_0
    const/4 p1, 0x0

    .line 91
    :goto_0
    invoke-virtual {v0, p1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;->setEnableDpAlert(Z)V

    .line 92
    .line 93
    .line 94
    iget-object p1, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mediaView:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 95
    .line 96
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 97
    .line 98
    check-cast v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 99
    .line 100
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getConstantMap()Ljava/util/HashMap;

    .line 101
    .line 102
    .line 103
    move-result-object v0

    .line 104
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;->setConstantMap(Ljava/util/HashMap;)V

    .line 105
    .line 106
    .line 107
    iget-object p1, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mediaView:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 108
    .line 109
    iget v0, p0, Lcom/intsig/advertisement/interfaces/SplashRequest;->mRequestCode:I

    .line 110
    .line 111
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;->setRequestCodeForResult(I)V

    .line 112
    .line 113
    .line 114
    iget-object p1, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mediaView:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 115
    .line 116
    new-instance v0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash$3;

    .line 117
    .line 118
    invoke-direct {v0, p0}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash$3;-><init>(Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;)V

    .line 119
    .line 120
    .line 121
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;->setAdClickListener(Lcom/intsig/advertisement/adapters/sources/api/sdk/listener/CsAdListener;)V

    .line 122
    .line 123
    .line 124
    return-void
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method private synthetic lambda$renderMultiElements$3(Landroid/view/View;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mediaView:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;->onClick(Landroid/view/View;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private synthetic lambda$setClickArea$4(Landroid/view/View;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mediaView:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;->onClick(Landroid/view/View;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private synthetic lambda$setClickArea$5(Landroid/view/View;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mediaView:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 2
    .line 3
    invoke-virtual {v0, p1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;->onClick(Landroid/view/View;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private synthetic lambda$showSplashAd$0(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/advertisement/interfaces/SplashRequest;->notifyOnSkip()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private synthetic lambda$showSplashAd$1(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/advertisement/interfaces/SplashRequest;->notifyOnSkip()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private synthetic lambda$showSplashAd$2(Landroid/app/Activity;Landroid/widget/RelativeLayout;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/SplashRequest;->mApiLogFl:Landroid/widget/RelativeLayout;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-direct {p0, p1, v0}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->addAPiLogView(Landroid/content/Context;Landroid/widget/RelativeLayout;)V

    .line 6
    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->addAPiLogView(Landroid/content/Context;Landroid/widget/RelativeLayout;)V

    .line 10
    .line 11
    .line 12
    :goto_0
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static synthetic oO80(Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->lambda$showSplashAd$0(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private renderHtml(Landroid/widget/RelativeLayout;)V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/ViewRender;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 4
    .line 5
    check-cast v1, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 6
    .line 7
    iget-object v2, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 8
    .line 9
    check-cast v2, Lcom/intsig/advertisement/params/SplashParam;

    .line 10
    .line 11
    invoke-virtual {v2}, Lcom/intsig/advertisement/params/RequestParam;->〇O8o08O()Lcom/intsig/advertisement/enums/PositionType;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    new-instance v3, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash$2;

    .line 16
    .line 17
    invoke-direct {v3, p0}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash$2;-><init>(Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;)V

    .line 18
    .line 19
    .line 20
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/advertisement/adapters/sources/api/sdk/ViewRender;-><init>(Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;Lcom/intsig/advertisement/enums/PositionType;Lcom/intsig/advertisement/listener/OnAdShowListener;)V

    .line 21
    .line 22
    .line 23
    iget-object v1, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mediaView:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 24
    .line 25
    iput-object v1, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mAdClickView:Landroid/view/View;

    .line 26
    .line 27
    invoke-virtual {v1, v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;->setViewRender(Lcom/intsig/advertisement/adapters/sources/api/sdk/ViewRender;)V

    .line 28
    .line 29
    .line 30
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mediaView:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 31
    .line 32
    invoke-direct {p0, v0}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->setMediaViewAsset(Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;)V

    .line 33
    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mediaView:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 36
    .line 37
    const/4 v1, -0x1

    .line 38
    invoke-virtual {p1, v0, v1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private renderMultiElements(Landroid/app/Activity;Landroid/widget/RelativeLayout;)V
    .locals 9

    .line 1
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    sget v1, Lcom/intsig/advertisement/R$layout;->app_launch_native:I

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    sget v1, Lcom/intsig/advertisement/R$id;->rv_main_view_container:I

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    check-cast v1, Landroidx/cardview/widget/CardView;

    .line 19
    .line 20
    invoke-direct {p0, p1, v1}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->setCardViewLayoutParam(Landroid/content/Context;Landroidx/cardview/widget/CardView;)V

    .line 21
    .line 22
    .line 23
    sget v2, Lcom/intsig/advertisement/R$id;->iv_ad_icon:I

    .line 24
    .line 25
    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 26
    .line 27
    .line 28
    move-result-object v2

    .line 29
    check-cast v2, Lcom/intsig/advertisement/view/RoundConnerImageView;

    .line 30
    .line 31
    sget v3, Lcom/intsig/advertisement/R$id;->tv_title:I

    .line 32
    .line 33
    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 34
    .line 35
    .line 36
    move-result-object v3

    .line 37
    check-cast v3, Landroid/widget/TextView;

    .line 38
    .line 39
    sget v4, Lcom/intsig/advertisement/R$id;->tv_sub_title:I

    .line 40
    .line 41
    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 42
    .line 43
    .line 44
    move-result-object v4

    .line 45
    check-cast v4, Landroid/widget/TextView;

    .line 46
    .line 47
    sget v5, Lcom/intsig/advertisement/R$id;->btn_action:I

    .line 48
    .line 49
    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 50
    .line 51
    .line 52
    move-result-object v5

    .line 53
    check-cast v5, Landroid/widget/Button;

    .line 54
    .line 55
    sget v6, Lcom/intsig/advertisement/R$id;->tv_ad:I

    .line 56
    .line 57
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 58
    .line 59
    .line 60
    move-result-object v6

    .line 61
    check-cast v6, Landroid/widget/TextView;

    .line 62
    .line 63
    const/16 v7, 0x8

    .line 64
    .line 65
    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 66
    .line 67
    .line 68
    iget-object v6, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mediaView:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 69
    .line 70
    invoke-direct {p0, v6}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->setMediaViewAsset(Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;)V

    .line 71
    .line 72
    .line 73
    iget-object v6, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mediaView:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 74
    .line 75
    const/4 v8, -0x1

    .line 76
    invoke-virtual {v1, v6, v8, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 77
    .line 78
    .line 79
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 80
    .line 81
    check-cast v1, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 82
    .line 83
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getTitle()Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object v1

    .line 87
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 88
    .line 89
    .line 90
    move-result v1

    .line 91
    if-eqz v1, :cond_0

    .line 92
    .line 93
    invoke-virtual {v3, v7}, Landroid/view/View;->setVisibility(I)V

    .line 94
    .line 95
    .line 96
    goto :goto_0

    .line 97
    :cond_0
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 98
    .line 99
    check-cast v1, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 100
    .line 101
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getTitle()Ljava/lang/String;

    .line 102
    .line 103
    .line 104
    move-result-object v1

    .line 105
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    .line 107
    .line 108
    :goto_0
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 109
    .line 110
    check-cast v1, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 111
    .line 112
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getDescription()Ljava/lang/String;

    .line 113
    .line 114
    .line 115
    move-result-object v1

    .line 116
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 117
    .line 118
    .line 119
    move-result v1

    .line 120
    if-eqz v1, :cond_1

    .line 121
    .line 122
    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 123
    .line 124
    .line 125
    goto :goto_1

    .line 126
    :cond_1
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 127
    .line 128
    check-cast v1, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 129
    .line 130
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getDescription()Ljava/lang/String;

    .line 131
    .line 132
    .line 133
    move-result-object v1

    .line 134
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    .line 136
    .line 137
    :goto_1
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 138
    .line 139
    check-cast v1, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 140
    .line 141
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getBtn_text()Ljava/lang/String;

    .line 142
    .line 143
    .line 144
    move-result-object v1

    .line 145
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 146
    .line 147
    .line 148
    move-result v1

    .line 149
    if-eqz v1, :cond_2

    .line 150
    .line 151
    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    .line 152
    .line 153
    .line 154
    goto :goto_2

    .line 155
    :cond_2
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 156
    .line 157
    check-cast v1, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 158
    .line 159
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getBtn_text()Ljava/lang/String;

    .line 160
    .line 161
    .line 162
    move-result-object v1

    .line 163
    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    .line 165
    .line 166
    :goto_2
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 167
    .line 168
    check-cast v1, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 169
    .line 170
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getIcon_pic()Ljava/lang/String;

    .line 171
    .line 172
    .line 173
    move-result-object v1

    .line 174
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 175
    .line 176
    .line 177
    move-result v1

    .line 178
    if-nez v1, :cond_3

    .line 179
    .line 180
    new-instance v1, Lcom/bumptech/glide/request/RequestOptions;

    .line 181
    .line 182
    invoke-direct {v1}, Lcom/bumptech/glide/request/RequestOptions;-><init>()V

    .line 183
    .line 184
    .line 185
    invoke-virtual {v1}, Lcom/bumptech/glide/request/BaseRequestOptions;->O8()Lcom/bumptech/glide/request/BaseRequestOptions;

    .line 186
    .line 187
    .line 188
    invoke-static {p1}, Lcom/bumptech/glide/Glide;->〇0〇O0088o(Landroid/app/Activity;)Lcom/bumptech/glide/RequestManager;

    .line 189
    .line 190
    .line 191
    move-result-object p1

    .line 192
    iget-object v3, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 193
    .line 194
    check-cast v3, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 195
    .line 196
    invoke-virtual {v3}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getIcon_pic()Ljava/lang/String;

    .line 197
    .line 198
    .line 199
    move-result-object v3

    .line 200
    invoke-virtual {p1, v3}, Lcom/bumptech/glide/RequestManager;->〇〇808〇(Ljava/lang/String;)Lcom/bumptech/glide/RequestBuilder;

    .line 201
    .line 202
    .line 203
    move-result-object p1

    .line 204
    invoke-virtual {p1, v1}, Lcom/bumptech/glide/RequestBuilder;->〇O(Lcom/bumptech/glide/request/BaseRequestOptions;)Lcom/bumptech/glide/RequestBuilder;

    .line 205
    .line 206
    .line 207
    move-result-object p1

    .line 208
    invoke-virtual {p1, v2}, Lcom/bumptech/glide/RequestBuilder;->Oo〇o(Landroid/widget/ImageView;)Lcom/bumptech/glide/request/target/ViewTarget;

    .line 209
    .line 210
    .line 211
    goto :goto_3

    .line 212
    :cond_3
    invoke-virtual {v2, v7}, Landroid/view/View;->setVisibility(I)V

    .line 213
    .line 214
    .line 215
    :goto_3
    new-instance p1, Lcom/intsig/advertisement/adapters/sources/api/o〇0;

    .line 216
    .line 217
    invoke-direct {p1, p0}, Lcom/intsig/advertisement/adapters/sources/api/o〇0;-><init>(Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;)V

    .line 218
    .line 219
    .line 220
    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 221
    .line 222
    .line 223
    iput-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mAdClickView:Landroid/view/View;

    .line 224
    .line 225
    new-instance p1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 226
    .line 227
    const/4 v1, -0x2

    .line 228
    invoke-direct {p1, v8, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 229
    .line 230
    .line 231
    const/16 v1, 0xd

    .line 232
    .line 233
    invoke-virtual {p1, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 234
    .line 235
    .line 236
    invoke-virtual {p2, v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 237
    .line 238
    .line 239
    return-void
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

.method private renderSingleVideoOrImage(Landroid/widget/RelativeLayout;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mediaView:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 2
    .line 3
    iput-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mAdClickView:Landroid/view/View;

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->getLayoutType()Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    sget-object v1, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;->NORMAL_BOTTOM_LOG:Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 10
    .line 11
    if-eq v0, v1, :cond_0

    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mediaView:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 14
    .line 15
    const/4 v1, 0x1

    .line 16
    invoke-virtual {v0, v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;->setFullScreen(Z)V

    .line 17
    .line 18
    .line 19
    :cond_0
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mediaView:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 20
    .line 21
    invoke-direct {p0, v0}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->setMediaViewAsset(Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;)V

    .line 22
    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mediaView:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 25
    .line 26
    const/4 v1, -0x1

    .line 27
    invoke-virtual {p1, v0, v1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private setCardViewLayoutParam(Landroid/content/Context;Landroidx/cardview/widget/CardView;)V
    .locals 3

    .line 1
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {p1}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    const/16 v2, 0x20

    .line 10
    .line 11
    invoke-static {p1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    sub-int/2addr v1, p1

    .line 16
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 17
    .line 18
    mul-int/lit8 v1, v1, 0x9

    .line 19
    .line 20
    div-int/lit8 v1, v1, 0x10

    .line 21
    .line 22
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 23
    .line 24
    invoke-virtual {p2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private setClickArea(Landroid/content/Context;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 2
    .line 3
    check-cast v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getJumpAlertType()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->setClickTipStyle(Landroid/content/Context;)V

    .line 10
    .line 11
    .line 12
    const/4 p1, 0x1

    .line 13
    const/4 v1, 0x0

    .line 14
    if-ne v0, p1, :cond_0

    .line 15
    .line 16
    iget-object p1, p0, Lcom/intsig/advertisement/interfaces/SplashRequest;->mAdClickTipView:Lcom/intsig/advertisement/view/AdClickTipView;

    .line 17
    .line 18
    invoke-virtual {p1, v1}, Landroid/view/View;->setClickable(Z)V

    .line 19
    .line 20
    .line 21
    iget-object p1, p0, Lcom/intsig/advertisement/interfaces/SplashRequest;->mAdClickTipView:Lcom/intsig/advertisement/view/AdClickTipView;

    .line 22
    .line 23
    new-instance v0, Lcom/intsig/advertisement/adapters/sources/api/O8;

    .line 24
    .line 25
    invoke-direct {v0, p0}, Lcom/intsig/advertisement/adapters/sources/api/O8;-><init>(Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_0
    const/4 p1, 0x2

    .line 33
    if-ne v0, p1, :cond_1

    .line 34
    .line 35
    iget-object p1, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mAdClickView:Landroid/view/View;

    .line 36
    .line 37
    invoke-virtual {p1, v1}, Landroid/view/View;->setClickable(Z)V

    .line 38
    .line 39
    .line 40
    iget-object p1, p0, Lcom/intsig/advertisement/interfaces/SplashRequest;->mAdClickTipView:Lcom/intsig/advertisement/view/AdClickTipView;

    .line 41
    .line 42
    new-instance v0, Lcom/intsig/advertisement/adapters/sources/api/Oo08;

    .line 43
    .line 44
    invoke-direct {v0, p0}, Lcom/intsig/advertisement/adapters/sources/api/Oo08;-><init>(Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;)V

    .line 45
    .line 46
    .line 47
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    .line 49
    .line 50
    goto :goto_0

    .line 51
    :cond_1
    iget-object p1, p0, Lcom/intsig/advertisement/interfaces/SplashRequest;->mAdClickTipView:Lcom/intsig/advertisement/view/AdClickTipView;

    .line 52
    .line 53
    const/16 v0, 0x8

    .line 54
    .line 55
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 56
    .line 57
    .line 58
    :goto_0
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private setClickTipStyle(Landroid/content/Context;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/advertisement/control/AdConfigManager;->〇o〇:Lcom/intsig/advertisement/control/AdInfoCallback;

    .line 2
    .line 3
    if-eqz v0, :cond_1

    .line 4
    .line 5
    invoke-interface {v0, p1}, Lcom/intsig/advertisement/control/AdInfoCallback;->〇O888o0o(Landroid/content/Context;)Z

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    if-nez p1, :cond_1

    .line 10
    .line 11
    iget-object p1, p0, Lcom/intsig/advertisement/interfaces/SplashRequest;->mAdClickTipView:Lcom/intsig/advertisement/view/AdClickTipView;

    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 15
    .line 16
    .line 17
    iget-object p1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 18
    .line 19
    check-cast p1, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 20
    .line 21
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getStyleClickTip()Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    if-nez p1, :cond_0

    .line 26
    .line 27
    invoke-virtual {p0}, Lcom/intsig/advertisement/interfaces/SplashRequest;->getDefaultStyle()Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    :cond_0
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/SplashRequest;->mAdClickTipView:Lcom/intsig/advertisement/view/AdClickTipView;

    .line 32
    .line 33
    invoke-virtual {v0, p1}, Lcom/intsig/advertisement/view/AdClickTipView;->setStyleConfig(Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;)V

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_1
    iget-object p1, p0, Lcom/intsig/advertisement/interfaces/SplashRequest;->mAdClickTipView:Lcom/intsig/advertisement/view/AdClickTipView;

    .line 38
    .line 39
    const/16 v0, 0x8

    .line 40
    .line 41
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 42
    .line 43
    .line 44
    :goto_0
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private setMediaViewAsset(Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->isVideo()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 8
    .line 9
    check-cast v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 10
    .line 11
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getVideo()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 16
    .line 17
    check-cast v1, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 18
    .line 19
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getVideotrackers()Lcom/intsig/advertisement/adapters/sources/cs/VideoTrackers;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-virtual {p1, v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;->setVideoTrackers(Lcom/intsig/advertisement/adapters/sources/cs/VideoTrackers;)V

    .line 24
    .line 25
    .line 26
    invoke-static {v0}, Lcom/intsig/advertisement/util/FileDownLoadUtil;->Oo08(Ljava/lang/String;)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-static {v1}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 31
    .line 32
    .line 33
    move-result v2

    .line 34
    if-eqz v2, :cond_0

    .line 35
    .line 36
    sget-object v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView$MediaType;->video:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView$MediaType;

    .line 37
    .line 38
    invoke-virtual {p1, v1, v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;->O〇O〇oO(Ljava/lang/String;Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView$MediaType;)V

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    sget-object v1, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView$MediaType;->video:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView$MediaType;

    .line 43
    .line 44
    invoke-virtual {p1, v0, v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;->O〇O〇oO(Ljava/lang/String;Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView$MediaType;)V

    .line 45
    .line 46
    .line 47
    :goto_0
    const/4 v0, 0x0

    .line 48
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;->setWebFullScreen(Z)V

    .line 49
    .line 50
    .line 51
    new-instance v0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash$4;

    .line 52
    .line 53
    invoke-direct {v0, p0}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash$4;-><init>(Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;)V

    .line 54
    .line 55
    .line 56
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;->setVideoPlayListener(Lcom/intsig/advertisement/adapters/sources/api/sdk/listener/OnVideoPlayListener;)V

    .line 57
    .line 58
    .line 59
    goto :goto_1

    .line 60
    :cond_1
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 61
    .line 62
    check-cast v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 63
    .line 64
    invoke-static {v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/ApiUtil;->〇080(Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;)Z

    .line 65
    .line 66
    .line 67
    move-result v0

    .line 68
    if-eqz v0, :cond_2

    .line 69
    .line 70
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 71
    .line 72
    check-cast v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 73
    .line 74
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getHtml()Ljava/lang/String;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    sget-object v1, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView$MediaType;->html:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView$MediaType;

    .line 79
    .line 80
    invoke-virtual {p1, v0, v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;->O〇O〇oO(Ljava/lang/String;Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView$MediaType;)V

    .line 81
    .line 82
    .line 83
    goto :goto_1

    .line 84
    :cond_2
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 85
    .line 86
    check-cast v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 87
    .line 88
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getPic()Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object v0

    .line 92
    invoke-virtual {p1, v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;->setAdAsset(Ljava/lang/String;)V

    .line 93
    .line 94
    .line 95
    :goto_1
    return-void
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method public static synthetic 〇80〇808〇O(Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->lambda$setClickArea$4(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static synthetic 〇8o8o〇(Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->lambda$showSplashAd$1(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method static synthetic 〇O00(Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;)Lcom/intsig/advertisement/params/RequestParam;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static synthetic 〇O8o08O(Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;Landroid/app/Activity;Landroid/widget/RelativeLayout;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->lambda$showSplashAd$2(Landroid/app/Activity;Landroid/widget/RelativeLayout;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method static synthetic 〇O〇(Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyOnSucceed()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static synthetic 〇〇808〇(Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;)Lcom/intsig/advertisement/feedback/FeedBackInfo;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mFeedBackInfo:Lcom/intsig/advertisement/feedback/FeedBackInfo;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static synthetic 〇〇8O0〇8(Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;)Lcom/intsig/advertisement/params/RequestParam;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->destroy()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->mediaView:Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/CsAdMediaView;->oO()V

    .line 9
    .line 10
    .line 11
    :cond_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public getInteractType()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 2
    .line 3
    check-cast v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getStyleClickTip()Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/advertisement/interfaces/SplashRequest;->getDefaultStyle()Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->getLayout()I

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    return v0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public getLayoutType()Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 2
    .line 3
    check-cast v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getLayout()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x4

    .line 10
    if-ne v0, v1, :cond_0

    .line 11
    .line 12
    sget-object v0, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;->FULL_NO_TAG_LOG:Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 13
    .line 14
    return-object v0

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 16
    .line 17
    check-cast v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 18
    .line 19
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getLayout()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    const/4 v1, 0x5

    .line 24
    if-ne v0, v1, :cond_1

    .line 25
    .line 26
    sget-object v0, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;->NORMAL_BOTTOM_LOG:Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 27
    .line 28
    return-object v0

    .line 29
    :cond_1
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 30
    .line 31
    check-cast v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 32
    .line 33
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getLayout()I

    .line 34
    .line 35
    .line 36
    move-result v0

    .line 37
    const/4 v1, 0x6

    .line 38
    if-ne v0, v1, :cond_2

    .line 39
    .line 40
    sget-object v0, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;->FULL_LOG_LEFT_TOP:Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 41
    .line 42
    return-object v0

    .line 43
    :cond_2
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 44
    .line 45
    check-cast v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 46
    .line 47
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getLayout()I

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    const/4 v1, 0x7

    .line 52
    if-ne v0, v1, :cond_3

    .line 53
    .line 54
    sget-object v0, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;->FULL_LOG_RIGHT_BOTTOM:Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 55
    .line 56
    return-object v0

    .line 57
    :cond_3
    invoke-super {p0}, Lcom/intsig/advertisement/interfaces/SplashRequest;->getLayoutType()Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 58
    .line 59
    .line 60
    move-result-object v0

    .line 61
    return-object v0
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public getRealPrice()F
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    check-cast v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 6
    .line 7
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getPrice()Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-static {v0}, Lcom/intsig/advertisement/util/CommonUtil;->〇〇808〇(Ljava/lang/String;)Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 18
    .line 19
    check-cast v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 20
    .line 21
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getPrice()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    return v0

    .line 30
    :cond_0
    sget v0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->INVALIDATE_PRICE:F

    .line 31
    .line 32
    return v0
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public getShowTimeSeconds()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 2
    .line 3
    check-cast v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getShowTime()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public isExpandSkipClickArea()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 2
    .line 3
    check-cast v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getSkipClickAreaStyle()I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x1

    .line 10
    if-ne v0, v1, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v1, 0x0

    .line 14
    :goto_0
    return v1
    .line 15
.end method

.method public isPriceFromResponse()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public isSkipTextSkipAd()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public isVideo()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 2
    .line 3
    check-cast v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 4
    .line 5
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getVideo()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    xor-int/lit8 v0, v0, 0x1

    .line 14
    .line 15
    return v0
.end method

.method public notifyForVirImpression()V
    .locals 2

    .line 1
    invoke-super {p0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyForVirImpression()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyOnShowSucceed()V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 12
    .line 13
    check-cast v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 14
    .line 15
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getImptrackers()[Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 20
    .line 21
    check-cast v1, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 22
    .line 23
    invoke-virtual {v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getConstantMap()Ljava/util/HashMap;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-static {v0, v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/trackers/TrackerUtil;->〇080([Ljava/lang/String;Ljava/util/HashMap;)V

    .line 28
    .line 29
    .line 30
    :cond_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method protected onRequest(Landroid/content/Context;)V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/ApiAdRequest;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash$1;

    .line 4
    .line 5
    invoke-direct {v1, p0}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash$1;-><init>(Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {v0, p1, v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/ApiAdRequest;-><init>(Landroid/content/Context;Lcom/intsig/advertisement/listener/OnAdRequestListener;)V

    .line 9
    .line 10
    .line 11
    iget-object p1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 12
    .line 13
    check-cast p1, Lcom/intsig/advertisement/params/SplashParam;

    .line 14
    .line 15
    invoke-virtual {p1}, Lcom/intsig/advertisement/params/RequestParam;->〇O8o08O()Lcom/intsig/advertisement/enums/PositionType;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 20
    .line 21
    check-cast v1, Lcom/intsig/advertisement/params/SplashParam;

    .line 22
    .line 23
    invoke-virtual {v1}, Lcom/intsig/advertisement/params/RequestParam;->〇8o8o〇()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-virtual {v0, p1, v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/ApiAdRequest;->OO0o〇〇〇〇0(Lcom/intsig/advertisement/enums/PositionType;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public showSplashAd(Landroid/app/Activity;Landroid/widget/RelativeLayout;Landroid/widget/TextView;ILandroid/widget/TextView;Lcom/intsig/advertisement/adapters/sources/api/sdk/listener/ResetBootListener;)V
    .locals 0

    .line 1
    invoke-super/range {p0 .. p6}, Lcom/intsig/advertisement/interfaces/SplashRequest;->showSplashAd(Landroid/app/Activity;Landroid/widget/RelativeLayout;Landroid/widget/TextView;ILandroid/widget/TextView;Lcom/intsig/advertisement/adapters/sources/api/sdk/listener/ResetBootListener;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->isExpandSkipClickArea()Z

    .line 5
    .line 6
    .line 7
    move-result p4

    .line 8
    if-eqz p4, :cond_0

    .line 9
    .line 10
    iget-object p3, p0, Lcom/intsig/advertisement/interfaces/SplashRequest;->mSkipContainer:Landroid/widget/RelativeLayout;

    .line 11
    .line 12
    new-instance p4, Lcom/intsig/advertisement/adapters/sources/api/〇080;

    .line 13
    .line 14
    invoke-direct {p4, p0}, Lcom/intsig/advertisement/adapters/sources/api/〇080;-><init>(Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p3, p4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    new-instance p4, Lcom/intsig/advertisement/adapters/sources/api/〇o00〇〇Oo;

    .line 22
    .line 23
    invoke-direct {p4, p0}, Lcom/intsig/advertisement/adapters/sources/api/〇o00〇〇Oo;-><init>(Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p3, p4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 27
    .line 28
    .line 29
    :goto_0
    iget-object p3, p0, Lcom/intsig/advertisement/interfaces/SplashRequest;->mPolicyDesTv:Landroid/widget/TextView;

    .line 30
    .line 31
    iget-object p4, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 32
    .line 33
    check-cast p4, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 34
    .line 35
    invoke-static {p3, p4}, Lcom/intsig/advertisement/view/AdPolicyWrapper;->〇080(Landroid/widget/TextView;Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;)V

    .line 36
    .line 37
    .line 38
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->initMediaView(Landroid/app/Activity;)V

    .line 39
    .line 40
    .line 41
    iget-object p3, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 42
    .line 43
    check-cast p3, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 44
    .line 45
    invoke-static {p3}, Lcom/intsig/advertisement/adapters/sources/api/sdk/ApiUtil;->〇080(Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;)Z

    .line 46
    .line 47
    .line 48
    move-result p3

    .line 49
    if-eqz p3, :cond_1

    .line 50
    .line 51
    invoke-direct {p0, p2}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->renderHtml(Landroid/widget/RelativeLayout;)V

    .line 52
    .line 53
    .line 54
    goto :goto_1

    .line 55
    :cond_1
    iget-object p3, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 56
    .line 57
    check-cast p3, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 58
    .line 59
    invoke-virtual {p3}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getRenderModel()I

    .line 60
    .line 61
    .line 62
    move-result p3

    .line 63
    if-nez p3, :cond_2

    .line 64
    .line 65
    invoke-direct {p0, p2}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->renderSingleVideoOrImage(Landroid/widget/RelativeLayout;)V

    .line 66
    .line 67
    .line 68
    goto :goto_1

    .line 69
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->renderMultiElements(Landroid/app/Activity;Landroid/widget/RelativeLayout;)V

    .line 70
    .line 71
    .line 72
    :goto_1
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;->setClickArea(Landroid/content/Context;)V

    .line 73
    .line 74
    .line 75
    iget-object p3, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 76
    .line 77
    check-cast p3, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 78
    .line 79
    invoke-static {p3}, Lcom/intsig/advertisement/adapters/sources/api/sdk/ApiUtil;->〇080(Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;)Z

    .line 80
    .line 81
    .line 82
    move-result p3

    .line 83
    if-nez p3, :cond_3

    .line 84
    .line 85
    new-instance p3, Lcom/intsig/advertisement/adapters/sources/api/〇o〇;

    .line 86
    .line 87
    invoke-direct {p3, p0, p1, p2}, Lcom/intsig/advertisement/adapters/sources/api/〇o〇;-><init>(Lcom/intsig/advertisement/adapters/sources/api/ApiSplash;Landroid/app/Activity;Landroid/widget/RelativeLayout;)V

    .line 88
    .line 89
    .line 90
    invoke-virtual {p2, p3}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 91
    .line 92
    .line 93
    :cond_3
    return-void
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
.end method

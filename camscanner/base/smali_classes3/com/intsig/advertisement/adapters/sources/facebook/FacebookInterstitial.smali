.class public Lcom/intsig/advertisement/adapters/sources/facebook/FacebookInterstitial;
.super Lcom/intsig/advertisement/interfaces/InterstitialRequest;
.source "FacebookInterstitial.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/intsig/advertisement/interfaces/InterstitialRequest<",
        "Lcom/facebook/ads/InterstitialAd;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/intsig/advertisement/params/InterstitialParam;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/interfaces/InterstitialRequest;-><init>(Lcom/intsig/advertisement/params/InterstitialParam;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static synthetic oO80(Lcom/intsig/advertisement/adapters/sources/facebook/FacebookInterstitial;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyOnSucceed()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static synthetic 〇80〇808〇O(Lcom/intsig/advertisement/adapters/sources/facebook/FacebookInterstitial;ZLjava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->printLog(ZLjava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .line 1
    invoke-super {p0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->destroy()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    check-cast v0, Lcom/facebook/ads/InterstitialAd;

    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/facebook/ads/InterstitialAd;->destroy()V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
.end method

.method protected onRequest(Landroid/content/Context;)V
    .locals 2

    .line 1
    new-instance v0, Lcom/facebook/ads/InterstitialAd;

    .line 2
    .line 3
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    iget-object v1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mRequestParam:Lcom/intsig/advertisement/params/RequestParam;

    .line 8
    .line 9
    check-cast v1, Lcom/intsig/advertisement/params/InterstitialParam;

    .line 10
    .line 11
    invoke-virtual {v1}, Lcom/intsig/advertisement/params/RequestParam;->〇8o8o〇()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-direct {v0, p1, v1}, Lcom/facebook/ads/InterstitialAd;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    iput-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 19
    .line 20
    new-instance p1, Lcom/intsig/advertisement/adapters/sources/facebook/FacebookInterstitial$1;

    .line 21
    .line 22
    invoke-direct {p1, p0}, Lcom/intsig/advertisement/adapters/sources/facebook/FacebookInterstitial$1;-><init>(Lcom/intsig/advertisement/adapters/sources/facebook/FacebookInterstitial;)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 26
    .line 27
    move-object v1, v0

    .line 28
    check-cast v1, Lcom/facebook/ads/InterstitialAd;

    .line 29
    .line 30
    check-cast v0, Lcom/facebook/ads/InterstitialAd;

    .line 31
    .line 32
    invoke-virtual {v0}, Lcom/facebook/ads/InterstitialAd;->buildLoadAdConfig()Lcom/facebook/ads/InterstitialAd$InterstitialAdLoadConfigBuilder;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    invoke-interface {v0, p1}, Lcom/facebook/ads/InterstitialAd$InterstitialAdLoadConfigBuilder;->withAdListener(Lcom/facebook/ads/InterstitialAdListener;)Lcom/facebook/ads/InterstitialAd$InterstitialAdLoadConfigBuilder;

    .line 37
    .line 38
    .line 39
    move-result-object p1

    .line 40
    invoke-interface {p1}, Lcom/facebook/ads/InterstitialAd$InterstitialAdLoadConfigBuilder;->build()Lcom/facebook/ads/InterstitialAd$InterstitialLoadAdConfig;

    .line 41
    .line 42
    .line 43
    move-result-object p1

    .line 44
    invoke-virtual {v1, p1}, Lcom/facebook/ads/InterstitialAd;->loadAd(Lcom/facebook/ads/InterstitialAd$InterstitialLoadAdConfig;)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public onShowInterstitialAd(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/advertisement/interfaces/InterstitialRequest;->onShowInterstitialAd(Landroid/content/Context;)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 5
    .line 6
    const/4 v0, -0x1

    .line 7
    if-eqz p1, :cond_2

    .line 8
    .line 9
    check-cast p1, Lcom/facebook/ads/InterstitialAd;

    .line 10
    .line 11
    invoke-virtual {p1}, Lcom/facebook/ads/InterstitialAd;->isAdLoaded()Z

    .line 12
    .line 13
    .line 14
    move-result p1

    .line 15
    if-nez p1, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    iget-object p1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 19
    .line 20
    check-cast p1, Lcom/facebook/ads/InterstitialAd;

    .line 21
    .line 22
    invoke-virtual {p1}, Lcom/facebook/ads/InterstitialAd;->isAdInvalidated()Z

    .line 23
    .line 24
    .line 25
    move-result p1

    .line 26
    if-eqz p1, :cond_1

    .line 27
    .line 28
    const-string p1, "ad is not validate"

    .line 29
    .line 30
    invoke-virtual {p0, v0, p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyOnShowFailed(ILjava/lang/String;)V

    .line 31
    .line 32
    .line 33
    return-void

    .line 34
    :cond_1
    iget-object p1, p0, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 35
    .line 36
    check-cast p1, Lcom/facebook/ads/InterstitialAd;

    .line 37
    .line 38
    invoke-virtual {p1}, Lcom/facebook/ads/InterstitialAd;->show()Z

    .line 39
    .line 40
    .line 41
    return-void

    .line 42
    :cond_2
    :goto_0
    const-string p1, "ad is not load"

    .line 43
    .line 44
    invoke-virtual {p0, v0, p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyOnShowFailed(ILjava/lang/String;)V

    .line 45
    .line 46
    .line 47
    return-void
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.class Lcom/intsig/advertisement/adapters/sources/applovin/AppLovinNative$1;
.super Lcom/applovin/mediation/nativeAds/MaxNativeAdListener;
.source "AppLovinNative.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/advertisement/adapters/sources/applovin/AppLovinNative;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic o0:Lcom/intsig/advertisement/adapters/sources/applovin/AppLovinNative;


# direct methods
.method constructor <init>(Lcom/intsig/advertisement/adapters/sources/applovin/AppLovinNative;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/adapters/sources/applovin/AppLovinNative$1;->o0:Lcom/intsig/advertisement/adapters/sources/applovin/AppLovinNative;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/applovin/mediation/nativeAds/MaxNativeAdListener;-><init>()V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public onNativeAdClicked(Lcom/applovin/mediation/MaxAd;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lcom/applovin/mediation/nativeAds/MaxNativeAdListener;->onNativeAdClicked(Lcom/applovin/mediation/MaxAd;)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/advertisement/adapters/sources/applovin/AppLovinNative$1;->o0:Lcom/intsig/advertisement/adapters/sources/applovin/AppLovinNative;

    .line 5
    .line 6
    invoke-virtual {p1}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyOnClick()V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public onNativeAdLoadFailed(Ljava/lang/String;Lcom/applovin/mediation/MaxError;)V
    .locals 1

    .line 1
    invoke-super {p0, p1, p2}, Lcom/applovin/mediation/nativeAds/MaxNativeAdListener;->onNativeAdLoadFailed(Ljava/lang/String;Lcom/applovin/mediation/MaxError;)V

    .line 2
    .line 3
    .line 4
    if-eqz p2, :cond_0

    .line 5
    .line 6
    iget-object p1, p0, Lcom/intsig/advertisement/adapters/sources/applovin/AppLovinNative$1;->o0:Lcom/intsig/advertisement/adapters/sources/applovin/AppLovinNative;

    .line 7
    .line 8
    invoke-interface {p2}, Lcom/applovin/mediation/MaxError;->getCode()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-interface {p2}, Lcom/applovin/mediation/MaxError;->getMessage()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object p2

    .line 16
    invoke-virtual {p1, v0, p2}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyOnFailed(ILjava/lang/String;)V

    .line 17
    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    iget-object p1, p0, Lcom/intsig/advertisement/adapters/sources/applovin/AppLovinNative$1;->o0:Lcom/intsig/advertisement/adapters/sources/applovin/AppLovinNative;

    .line 21
    .line 22
    const/4 p2, -0x1

    .line 23
    const-string/jumbo v0, "unknown error!"

    .line 24
    .line 25
    .line 26
    invoke-virtual {p1, p2, v0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyOnFailed(ILjava/lang/String;)V

    .line 27
    .line 28
    .line 29
    :goto_0
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public onNativeAdLoaded(Lcom/applovin/mediation/nativeAds/MaxNativeAdView;Lcom/applovin/mediation/MaxAd;)V
    .locals 1
    .param p1    # Lcom/applovin/mediation/nativeAds/MaxNativeAdView;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    invoke-super {p0, p1, p2}, Lcom/applovin/mediation/nativeAds/MaxNativeAdListener;->onNativeAdLoaded(Lcom/applovin/mediation/nativeAds/MaxNativeAdView;Lcom/applovin/mediation/MaxAd;)V

    .line 2
    .line 3
    .line 4
    if-eqz p2, :cond_1

    .line 5
    .line 6
    invoke-interface {p2}, Lcom/applovin/mediation/MaxAd;->getNativeAd()Lcom/applovin/mediation/nativeAds/MaxNativeAd;

    .line 7
    .line 8
    .line 9
    move-result-object p1

    .line 10
    if-nez p1, :cond_0

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    iget-object p1, p0, Lcom/intsig/advertisement/adapters/sources/applovin/AppLovinNative$1;->o0:Lcom/intsig/advertisement/adapters/sources/applovin/AppLovinNative;

    .line 14
    .line 15
    iput-object p2, p1, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->mData:Ljava/lang/Object;

    .line 16
    .line 17
    invoke-static {p1}, Lcom/intsig/advertisement/adapters/sources/applovin/AppLovinNative;->OO0o〇〇〇〇0(Lcom/intsig/advertisement/adapters/sources/applovin/AppLovinNative;)V

    .line 18
    .line 19
    .line 20
    return-void

    .line 21
    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/intsig/advertisement/adapters/sources/applovin/AppLovinNative$1;->o0:Lcom/intsig/advertisement/adapters/sources/applovin/AppLovinNative;

    .line 22
    .line 23
    const/4 p2, -0x1

    .line 24
    const-string v0, "load ad is null!"

    .line 25
    .line 26
    invoke-virtual {p1, p2, v0}, Lcom/intsig/advertisement/interfaces/RealRequestAbs;->notifyOnFailed(ILjava/lang/String;)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

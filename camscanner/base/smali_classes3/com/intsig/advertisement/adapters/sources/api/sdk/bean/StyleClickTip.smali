.class public final Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;
.super Ljava/lang/Object;
.source "StyleClickTip.kt"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private acceleration:F

.field private angular_speed:I

.field private btn_enable_click:I

.field private is_lon_accele:I

.field private layout:I

.field private ly_bg_color:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private ly_height:I

.field private ly_img_url:Ljava/lang/String;

.field private final ly_opacity:F

.field private ly_width:I

.field private rotate_angle:F

.field private text_color:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private text_line_1:Ljava/lang/String;

.field private text_line_2:Ljava/lang/String;

.field private trigger_distance:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const-string/jumbo v0, "\u70b9\u51fb\u67e5\u770b\u8be6\u60c5"

    .line 5
    .line 6
    .line 7
    iput-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->text_line_1:Ljava/lang/String;

    .line 8
    .line 9
    const-string/jumbo v0, "\u8df3\u8f6c\u8be6\u60c5\u9875\u6216\u7b2c\u4e09\u65b9\u5e94\u7528"

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->text_line_2:Ljava/lang/String;

    .line 13
    .line 14
    const-string v0, "ffffff"

    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->text_color:Ljava/lang/String;

    .line 17
    .line 18
    const/4 v0, 0x2

    .line 19
    iput v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->layout:I

    .line 20
    .line 21
    const/16 v0, 0x124

    .line 22
    .line 23
    iput v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->ly_width:I

    .line 24
    .line 25
    const/16 v0, 0x4e

    .line 26
    .line 27
    iput v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->ly_height:I

    .line 28
    .line 29
    const-string v0, "000000"

    .line 30
    .line 31
    iput-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->ly_bg_color:Ljava/lang/String;

    .line 32
    .line 33
    const/high16 v0, 0x3f000000    # 0.5f

    .line 34
    .line 35
    iput v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->ly_opacity:F

    .line 36
    .line 37
    const/16 v0, 0x32

    .line 38
    .line 39
    iput v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->trigger_distance:I

    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method


# virtual methods
.method public final getAcceleration()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->acceleration:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final getAngular_speed()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->angular_speed:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final getBtn_enable_click()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->btn_enable_click:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final getLayout()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->layout:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final getLy_bg_color()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->ly_bg_color:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final getLy_height()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->ly_height:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final getLy_img_url()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->ly_img_url:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final getLy_opacity()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->ly_opacity:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final getLy_width()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->ly_width:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final getRotate_angle()F
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->rotate_angle:F

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final getText_color()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->text_color:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final getText_line_1()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->text_line_1:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final getText_line_2()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->text_line_2:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final getTrigger_distance()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->trigger_distance:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final is_lon_accele()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->is_lon_accele:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final setAcceleration(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->acceleration:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final setAngular_speed(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->angular_speed:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final setBtn_enable_click(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->btn_enable_click:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final setLayout(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->layout:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final setLy_bg_color(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->ly_bg_color:Ljava/lang/String;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final setLy_height(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->ly_height:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final setLy_img_url(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->ly_img_url:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final setLy_width(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->ly_width:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final setRotate_angle(F)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->rotate_angle:F

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final setText_color(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "<set-?>"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->text_color:Ljava/lang/String;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final setText_line_1(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->text_line_1:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final setText_line_2(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->text_line_2:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final setTrigger_distance(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->trigger_distance:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final set_lon_accele(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/StyleClickTip;->is_lon_accele:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

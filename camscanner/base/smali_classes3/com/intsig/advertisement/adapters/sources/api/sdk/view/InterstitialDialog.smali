.class public Lcom/intsig/advertisement/adapters/sources/api/sdk/view/InterstitialDialog;
.super Lcom/intsig/app/BaseDialogFragment;
.source "InterstitialDialog.java"


# instance fields
.field private O8o08O8O:Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

.field private o〇00O:Landroid/widget/ImageView;

.field private 〇080OO8〇0:Lcom/intsig/advertisement/listener/OnAdShowListener;

.field private 〇08O〇00〇o:Landroid/widget/FrameLayout;

.field private 〇0O:Lcom/intsig/advertisement/enums/PositionType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/app/BaseDialogFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private o00〇88〇08()V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/ViewRender;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/InterstitialDialog;->O8o08O8O:Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 4
    .line 5
    iget-object v2, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/InterstitialDialog;->〇0O:Lcom/intsig/advertisement/enums/PositionType;

    .line 6
    .line 7
    iget-object v3, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/InterstitialDialog;->〇080OO8〇0:Lcom/intsig/advertisement/listener/OnAdShowListener;

    .line 8
    .line 9
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/advertisement/adapters/sources/api/sdk/ViewRender;-><init>(Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;Lcom/intsig/advertisement/enums/PositionType;Lcom/intsig/advertisement/listener/OnAdShowListener;)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    if-eqz v1, :cond_2

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/ViewRender;->〇〇8O0〇8()Z

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    if-eqz v1, :cond_1

    .line 23
    .line 24
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 37
    .line 38
    if-lez v1, :cond_0

    .line 39
    .line 40
    iget-object v2, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/InterstitialDialog;->O8o08O8O:Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 41
    .line 42
    invoke-virtual {v2}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getWidth()I

    .line 43
    .line 44
    .line 45
    move-result v2

    .line 46
    if-lez v2, :cond_0

    .line 47
    .line 48
    iget-object v2, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/InterstitialDialog;->O8o08O8O:Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 49
    .line 50
    invoke-virtual {v2}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getHeight()I

    .line 51
    .line 52
    .line 53
    move-result v2

    .line 54
    mul-int v1, v1, v2

    .line 55
    .line 56
    iget-object v2, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/InterstitialDialog;->O8o08O8O:Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 57
    .line 58
    invoke-virtual {v2}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getWidth()I

    .line 59
    .line 60
    .line 61
    move-result v2

    .line 62
    div-int/2addr v1, v2

    .line 63
    goto :goto_0

    .line 64
    :cond_0
    const/4 v1, -0x2

    .line 65
    :goto_0
    iget-object v2, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/InterstitialDialog;->〇08O〇00〇o:Landroid/widget/FrameLayout;

    .line 66
    .line 67
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 68
    .line 69
    .line 70
    move-result-object v2

    .line 71
    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 72
    .line 73
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 74
    .line 75
    .line 76
    move-result-object v1

    .line 77
    iget-object v2, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/InterstitialDialog;->〇08O〇00〇o:Landroid/widget/FrameLayout;

    .line 78
    .line 79
    const/4 v3, 0x0

    .line 80
    invoke-virtual {v0, v1, v2, v3}, Lcom/intsig/advertisement/adapters/sources/api/sdk/ViewRender;->o800o8O(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/intsig/advertisement/view/NativeViewHolder;)Z

    .line 81
    .line 82
    .line 83
    goto :goto_1

    .line 84
    :cond_1
    new-instance v1, Lcom/intsig/advertisement/view/NativeViewHolder;

    .line 85
    .line 86
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 87
    .line 88
    .line 89
    move-result-object v2

    .line 90
    sget v3, Lcom/intsig/advertisement/R$layout;->single_rv_container:I

    .line 91
    .line 92
    invoke-direct {v1, v2, v3}, Lcom/intsig/advertisement/view/NativeViewHolder;-><init>(Landroid/content/Context;I)V

    .line 93
    .line 94
    .line 95
    const/4 v2, 0x1

    .line 96
    iput-boolean v2, v1, Lcom/intsig/advertisement/view/NativeViewHolder;->〇80〇808〇O:Z

    .line 97
    .line 98
    sget v2, Lcom/intsig/advertisement/R$id;->rv_main_view_container:I

    .line 99
    .line 100
    invoke-virtual {v1, v2}, Lcom/intsig/advertisement/view/NativeViewHolder;->Oo08(I)V

    .line 101
    .line 102
    .line 103
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 104
    .line 105
    .line 106
    move-result-object v2

    .line 107
    iget-object v3, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/InterstitialDialog;->〇08O〇00〇o:Landroid/widget/FrameLayout;

    .line 108
    .line 109
    invoke-virtual {v0, v2, v3, v1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/ViewRender;->o800o8O(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/intsig/advertisement/view/NativeViewHolder;)Z

    .line 110
    .line 111
    .line 112
    :cond_2
    :goto_1
    return-void
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public static synthetic o880(Lcom/intsig/advertisement/adapters/sources/api/sdk/view/InterstitialDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/InterstitialDialog;->〇〇o0〇8(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private synthetic 〇〇o0〇8(Landroid/view/View;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/InterstitialDialog;->〇080OO8〇0:Lcom/intsig/advertisement/listener/OnAdShowListener;

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/InterstitialDialog;->O8o08O8O:Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 6
    .line 7
    invoke-interface {p1, v0}, Lcom/intsig/advertisement/listener/OnAdShowListener;->〇〇888(Ljava/lang/Object;)V

    .line 8
    .line 9
    .line 10
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/app/BaseDialogFragment;->dismiss()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public O0〇0(Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/InterstitialDialog;->O8o08O8O:Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method protected init(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param

    .line 1
    iget-object p1, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/InterstitialDialog;->O8o08O8O:Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 2
    .line 3
    if-nez p1, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Lcom/intsig/app/BaseDialogFragment;->dismiss()V

    .line 6
    .line 7
    .line 8
    return-void

    .line 9
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    const/4 v0, 0x0

    .line 14
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 15
    .line 16
    .line 17
    new-instance p1, Landroid/graphics/drawable/ColorDrawable;

    .line 18
    .line 19
    invoke-direct {p1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {p0, p1}, Lcom/intsig/app/BaseDialogFragment;->oooO888(Landroid/graphics/drawable/Drawable;)V

    .line 23
    .line 24
    .line 25
    invoke-virtual {p0}, Lcom/intsig/app/BaseDialogFragment;->〇O8oOo0()V

    .line 26
    .line 27
    .line 28
    iget-object p1, p0, Lcom/intsig/app/BaseDialogFragment;->o0:Landroid/view/View;

    .line 29
    .line 30
    sget v0, Lcom/intsig/advertisement/R$id;->view_container:I

    .line 31
    .line 32
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    check-cast p1, Landroid/widget/FrameLayout;

    .line 37
    .line 38
    iput-object p1, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/InterstitialDialog;->〇08O〇00〇o:Landroid/widget/FrameLayout;

    .line 39
    .line 40
    iget-object p1, p0, Lcom/intsig/app/BaseDialogFragment;->o0:Landroid/view/View;

    .line 41
    .line 42
    sget v0, Lcom/intsig/advertisement/R$id;->iv_close:I

    .line 43
    .line 44
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    check-cast p1, Landroid/widget/ImageView;

    .line 49
    .line 50
    iput-object p1, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/InterstitialDialog;->o〇00O:Landroid/widget/ImageView;

    .line 51
    .line 52
    invoke-direct {p0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/InterstitialDialog;->o00〇88〇08()V

    .line 53
    .line 54
    .line 55
    iget-object p1, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/InterstitialDialog;->o〇00O:Landroid/widget/ImageView;

    .line 56
    .line 57
    new-instance v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/〇8o8o〇;

    .line 58
    .line 59
    invoke-direct {v0, p0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/〇8o8o〇;-><init>(Lcom/intsig/advertisement/adapters/sources/api/sdk/view/InterstitialDialog;)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    .line 64
    .line 65
    iget-object p1, p0, Lcom/intsig/app/BaseDialogFragment;->o0:Landroid/view/View;

    .line 66
    .line 67
    sget v0, Lcom/intsig/advertisement/R$id;->tv_ad_policy:I

    .line 68
    .line 69
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 70
    .line 71
    .line 72
    move-result-object p1

    .line 73
    check-cast p1, Landroid/widget/TextView;

    .line 74
    .line 75
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/InterstitialDialog;->O8o08O8O:Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;

    .line 76
    .line 77
    invoke-static {p1, v0}, Lcom/intsig/advertisement/view/AdPolicyWrapper;->〇080(Landroid/widget/TextView;Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;)V

    .line 78
    .line 79
    .line 80
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method public o〇0〇o(Lcom/intsig/advertisement/enums/PositionType;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/InterstitialDialog;->〇0O:Lcom/intsig/advertisement/enums/PositionType;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    sget v0, Lcom/intsig/advertisement/R$layout;->layout_api_interstitial_dialog:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇8〇OOoooo(Lcom/intsig/advertisement/listener/OnAdShowListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/adapters/sources/api/sdk/view/InterstitialDialog;->〇080OO8〇0:Lcom/intsig/advertisement/listener/OnAdShowListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.class public final enum Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;
.super Ljava/lang/Enum;
.source "LayoutType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

.field public static final enum FULL_LOG_LEFT_TOP:Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

.field public static final enum FULL_LOG_RIGHT_BOTTOM:Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

.field public static final enum FULL_NO_TAG_LOG:Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

.field public static final enum IMAGE_ONLY:Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

.field public static final enum NORMAL_BOTTOM_LOG:Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

.field public static final enum QUESTIONNAIRE:Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 15

    .line 1
    new-instance v0, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 2
    .line 3
    const-string v1, "IMAGE_ONLY"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    const/4 v3, 0x3

    .line 7
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;-><init>(Ljava/lang/String;II)V

    .line 8
    .line 9
    .line 10
    sput-object v0, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;->IMAGE_ONLY:Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 11
    .line 12
    new-instance v1, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 13
    .line 14
    const-string v4, "FULL_NO_TAG_LOG"

    .line 15
    .line 16
    const/4 v5, 0x1

    .line 17
    const/4 v6, 0x4

    .line 18
    invoke-direct {v1, v4, v5, v6}, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;-><init>(Ljava/lang/String;II)V

    .line 19
    .line 20
    .line 21
    sput-object v1, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;->FULL_NO_TAG_LOG:Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 22
    .line 23
    new-instance v4, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 24
    .line 25
    const-string v7, "NORMAL_BOTTOM_LOG"

    .line 26
    .line 27
    const/4 v8, 0x2

    .line 28
    const/4 v9, 0x5

    .line 29
    invoke-direct {v4, v7, v8, v9}, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;-><init>(Ljava/lang/String;II)V

    .line 30
    .line 31
    .line 32
    sput-object v4, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;->NORMAL_BOTTOM_LOG:Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 33
    .line 34
    new-instance v7, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 35
    .line 36
    const-string v10, "FULL_LOG_LEFT_TOP"

    .line 37
    .line 38
    const/4 v11, 0x6

    .line 39
    invoke-direct {v7, v10, v3, v11}, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;-><init>(Ljava/lang/String;II)V

    .line 40
    .line 41
    .line 42
    sput-object v7, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;->FULL_LOG_LEFT_TOP:Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 43
    .line 44
    new-instance v10, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 45
    .line 46
    const-string v12, "FULL_LOG_RIGHT_BOTTOM"

    .line 47
    .line 48
    const/4 v13, 0x7

    .line 49
    invoke-direct {v10, v12, v6, v13}, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;-><init>(Ljava/lang/String;II)V

    .line 50
    .line 51
    .line 52
    sput-object v10, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;->FULL_LOG_RIGHT_BOTTOM:Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 53
    .line 54
    new-instance v12, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 55
    .line 56
    const-string v13, "QUESTIONNAIRE"

    .line 57
    .line 58
    const/16 v14, 0x8

    .line 59
    .line 60
    invoke-direct {v12, v13, v9, v14}, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;-><init>(Ljava/lang/String;II)V

    .line 61
    .line 62
    .line 63
    sput-object v12, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;->QUESTIONNAIRE:Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 64
    .line 65
    new-array v11, v11, [Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 66
    .line 67
    aput-object v0, v11, v2

    .line 68
    .line 69
    aput-object v1, v11, v5

    .line 70
    .line 71
    aput-object v4, v11, v8

    .line 72
    .line 73
    aput-object v7, v11, v3

    .line 74
    .line 75
    aput-object v10, v11, v6

    .line 76
    .line 77
    aput-object v12, v11, v9

    .line 78
    .line 79
    sput-object v11, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;->$VALUES:[Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 80
    .line 81
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput p3, p0, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;->value:I

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static values()[Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;->$VALUES:[Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/advertisement/adapters/sources/cs/LayoutType;->value:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

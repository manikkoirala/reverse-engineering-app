.class public final Lcom/intsig/advertisement/adapters/sources/api/sdk/ApiUtil;
.super Ljava/lang/Object;
.source "ApiUtil.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080:Lcom/intsig/advertisement/adapters/sources/api/sdk/ApiUtil;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/ApiUtil;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/ApiUtil;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/advertisement/adapters/sources/api/sdk/ApiUtil;->〇080:Lcom/intsig/advertisement/adapters/sources/api/sdk/ApiUtil;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public static final 〇080(Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;)Z
    .locals 1
    .param p0    # Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "data"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getHtml()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object p0

    .line 10
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 11
    .line 12
    .line 13
    move-result p0

    .line 14
    xor-int/lit8 p0, p0, 0x1

    .line 15
    .line 16
    return p0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static final 〇o00〇〇Oo(Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;)Z
    .locals 1
    .param p0    # Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "data"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getRenderModel()I

    .line 7
    .line 8
    .line 9
    move-result p0

    .line 10
    if-nez p0, :cond_0

    .line 11
    .line 12
    const/4 p0, 0x1

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 p0, 0x0

    .line 15
    :goto_0
    return p0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static final 〇o〇(Lcom/intsig/advertisement/feedback/FeedBackInfo;Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;)V
    .locals 1

    .line 1
    if-eqz p0, :cond_1

    .line 2
    .line 3
    if-eqz p1, :cond_1

    .line 4
    .line 5
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getOrigin()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {p0, v0}, Lcom/intsig/advertisement/feedback/FeedBackInfo;->setOriginal(Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getPic()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-nez v0, :cond_0

    .line 21
    .line 22
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getPic()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-virtual {p0, v0}, Lcom/intsig/advertisement/feedback/FeedBackInfo;->setPic(Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getVideo()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    invoke-virtual {p0, v0}, Lcom/intsig/advertisement/feedback/FeedBackInfo;->setPic(Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    :goto_0
    invoke-virtual {p1}, Lcom/intsig/advertisement/adapters/sources/api/sdk/bean/ApiAdBean;->getUrl()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object p1

    .line 41
    invoke-virtual {p0, p1}, Lcom/intsig/advertisement/feedback/FeedBackInfo;->setUrl(Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    :cond_1
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

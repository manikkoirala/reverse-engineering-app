.class public Lcom/intsig/advertisement/adapters/AbsInitHelper;
.super Ljava/lang/Object;
.source "AbsInitHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/advertisement/adapters/AbsInitHelper$InitFunc;,
        Lcom/intsig/advertisement/adapters/AbsInitHelper$CallBack;
    }
.end annotation


# instance fields
.field private O8:Lcom/intsig/advertisement/adapters/AbsInitHelper$InitFunc;

.field private 〇080:Lcom/intsig/advertisement/enums/InitState;

.field private 〇o00〇〇Oo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/advertisement/adapters/AbsInitHelper$CallBack;",
            ">;"
        }
    .end annotation
.end field

.field private 〇o〇:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/advertisement/enums/InitState;->NotInit:Lcom/intsig/advertisement/enums/InitState;

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/advertisement/adapters/AbsInitHelper;->〇080:Lcom/intsig/advertisement/enums/InitState;

    .line 7
    .line 8
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 9
    .line 10
    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/advertisement/adapters/AbsInitHelper;->〇o00〇〇Oo:Ljava/util/List;

    .line 14
    .line 15
    const-string v0, "AbsInitHelper"

    .line 16
    .line 17
    iput-object v0, p0, Lcom/intsig/advertisement/adapters/AbsInitHelper;->〇o〇:Ljava/lang/String;

    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private Oo08(Ljava/lang/Boolean;)V
    .locals 3

    .line 1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    if-ne v0, v1, :cond_0

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/AbsInitHelper;->〇o00〇〇Oo:Ljava/util/List;

    .line 16
    .line 17
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 22
    .line 23
    .line 24
    move-result v1

    .line 25
    if-eqz v1, :cond_0

    .line 26
    .line 27
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    check-cast v1, Lcom/intsig/advertisement/adapters/AbsInitHelper$CallBack;

    .line 32
    .line 33
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    .line 34
    .line 35
    .line 36
    move-result v2

    .line 37
    invoke-interface {v1, v2}, Lcom/intsig/advertisement/adapters/AbsInitHelper$CallBack;->〇080(Z)V

    .line 38
    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_0
    iget-object p1, p0, Lcom/intsig/advertisement/adapters/AbsInitHelper;->〇o00〇〇Oo:Ljava/util/List;

    .line 42
    .line 43
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 44
    .line 45
    .line 46
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public static synthetic 〇080(Lcom/intsig/advertisement/adapters/AbsInitHelper;Ljava/lang/Boolean;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/adapters/AbsInitHelper;->〇o〇(Ljava/lang/Boolean;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private synthetic 〇o〇(Ljava/lang/Boolean;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/adapters/AbsInitHelper;->Oo08(Ljava/lang/Boolean;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public O8(Ljava/lang/Boolean;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/advertisement/enums/InitState;->HasInit:Lcom/intsig/advertisement/enums/InitState;

    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/advertisement/adapters/AbsInitHelper;->〇080:Lcom/intsig/advertisement/enums/InitState;

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    sget-object v0, Lcom/intsig/advertisement/enums/InitState;->NotInit:Lcom/intsig/advertisement/enums/InitState;

    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/advertisement/adapters/AbsInitHelper;->〇080:Lcom/intsig/advertisement/enums/InitState;

    .line 15
    .line 16
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-virtual {v1}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    if-ne v0, v1, :cond_1

    .line 29
    .line 30
    invoke-direct {p0, p1}, Lcom/intsig/advertisement/adapters/AbsInitHelper;->Oo08(Ljava/lang/Boolean;)V

    .line 31
    .line 32
    .line 33
    goto :goto_1

    .line 34
    :cond_1
    new-instance v0, Landroid/os/Handler;

    .line 35
    .line 36
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    .line 37
    .line 38
    .line 39
    move-result-object v1

    .line 40
    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 41
    .line 42
    .line 43
    new-instance v1, L〇80/〇080;

    .line 44
    .line 45
    invoke-direct {v1, p0, p1}, L〇80/〇080;-><init>(Lcom/intsig/advertisement/adapters/AbsInitHelper;Ljava/lang/Boolean;)V

    .line 46
    .line 47
    .line 48
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 49
    .line 50
    .line 51
    :goto_1
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public o〇0(Lcom/intsig/advertisement/enums/InitState;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/adapters/AbsInitHelper;->〇080:Lcom/intsig/advertisement/enums/InitState;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public 〇o00〇〇Oo(Lcom/intsig/advertisement/adapters/AbsInitHelper$InitFunc;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/advertisement/adapters/AbsInitHelper;->O8:Lcom/intsig/advertisement/adapters/AbsInitHelper$InitFunc;

    .line 2
    .line 3
    iput-object p2, p0, Lcom/intsig/advertisement/adapters/AbsInitHelper;->〇o〇:Ljava/lang/String;

    .line 4
    .line 5
    return-void
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public 〇〇888(Lcom/intsig/advertisement/adapters/AbsInitHelper$CallBack;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/AbsInitHelper;->〇080:Lcom/intsig/advertisement/enums/InitState;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/advertisement/enums/InitState;->NotInit:Lcom/intsig/advertisement/enums/InitState;

    .line 4
    .line 5
    if-ne v0, v1, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/AbsInitHelper;->〇o〇:Ljava/lang/String;

    .line 8
    .line 9
    const-string v1, "not init"

    .line 10
    .line 11
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    sget-object v0, Lcom/intsig/advertisement/enums/InitState;->InitIng:Lcom/intsig/advertisement/enums/InitState;

    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/advertisement/adapters/AbsInitHelper;->〇080:Lcom/intsig/advertisement/enums/InitState;

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/AbsInitHelper;->〇o00〇〇Oo:Ljava/util/List;

    .line 19
    .line 20
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 21
    .line 22
    .line 23
    iget-object p1, p0, Lcom/intsig/advertisement/adapters/AbsInitHelper;->O8:Lcom/intsig/advertisement/adapters/AbsInitHelper$InitFunc;

    .line 24
    .line 25
    if-eqz p1, :cond_2

    .line 26
    .line 27
    invoke-interface {p1}, Lcom/intsig/advertisement/adapters/AbsInitHelper$InitFunc;->init()V

    .line 28
    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    sget-object v1, Lcom/intsig/advertisement/enums/InitState;->InitIng:Lcom/intsig/advertisement/enums/InitState;

    .line 32
    .line 33
    if-ne v0, v1, :cond_1

    .line 34
    .line 35
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/AbsInitHelper;->〇o〇:Ljava/lang/String;

    .line 36
    .line 37
    const-string v1, " init ing"

    .line 38
    .line 39
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/AbsInitHelper;->〇o00〇〇Oo:Ljava/util/List;

    .line 43
    .line 44
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 45
    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_1
    iget-object v0, p0, Lcom/intsig/advertisement/adapters/AbsInitHelper;->〇o〇:Ljava/lang/String;

    .line 49
    .line 50
    const-string v1, " has init"

    .line 51
    .line 52
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    const/4 v0, 0x1

    .line 56
    invoke-interface {p1, v0}, Lcom/intsig/advertisement/adapters/AbsInitHelper$CallBack;->〇080(Z)V

    .line 57
    .line 58
    .line 59
    :cond_2
    :goto_0
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

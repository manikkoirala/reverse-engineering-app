.class public final Lcom/intsig/camscanner/searchactivity/SearchReferralOriEntity;
.super Ljava/util/ArrayList;
.source "SearchReferralOriEntity.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/ArrayList<",
        "Lcom/intsig/camscanner/searchactivity/SearchReferralEntity;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/util/ArrayList;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method


# virtual methods
.method public bridge contains(Lcom/intsig/camscanner/searchactivity/SearchReferralEntity;)Z
    .locals 0

    .line 1
    invoke-super {p0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final bridge contains(Ljava/lang/Object;)Z
    .locals 1

    .line 2
    instance-of v0, p1, Lcom/intsig/camscanner/searchactivity/SearchReferralEntity;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    check-cast p1, Lcom/intsig/camscanner/searchactivity/SearchReferralEntity;

    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/searchactivity/SearchReferralOriEntity;->contains(Lcom/intsig/camscanner/searchactivity/SearchReferralEntity;)Z

    move-result p1

    return p1
.end method

.method public bridge getSize()I
    .locals 1

    .line 1
    invoke-super {p0}, Ljava/util/ArrayList;->size()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public bridge indexOf(Lcom/intsig/camscanner/searchactivity/SearchReferralEntity;)I
    .locals 0

    .line 1
    invoke-super {p0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result p1

    return p1
.end method

.method public final bridge indexOf(Ljava/lang/Object;)I
    .locals 1

    .line 2
    instance-of v0, p1, Lcom/intsig/camscanner/searchactivity/SearchReferralEntity;

    if-nez v0, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    check-cast p1, Lcom/intsig/camscanner/searchactivity/SearchReferralEntity;

    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/searchactivity/SearchReferralOriEntity;->indexOf(Lcom/intsig/camscanner/searchactivity/SearchReferralEntity;)I

    move-result p1

    return p1
.end method

.method public bridge lastIndexOf(Lcom/intsig/camscanner/searchactivity/SearchReferralEntity;)I
    .locals 0

    .line 1
    invoke-super {p0, p1}, Ljava/util/ArrayList;->lastIndexOf(Ljava/lang/Object;)I

    move-result p1

    return p1
.end method

.method public final bridge lastIndexOf(Ljava/lang/Object;)I
    .locals 1

    .line 2
    instance-of v0, p1, Lcom/intsig/camscanner/searchactivity/SearchReferralEntity;

    if-nez v0, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    check-cast p1, Lcom/intsig/camscanner/searchactivity/SearchReferralEntity;

    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/searchactivity/SearchReferralOriEntity;->lastIndexOf(Lcom/intsig/camscanner/searchactivity/SearchReferralEntity;)I

    move-result p1

    return p1
.end method

.method public final bridge remove(I)Lcom/intsig/camscanner/searchactivity/SearchReferralEntity;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/searchactivity/SearchReferralOriEntity;->removeAt(I)Lcom/intsig/camscanner/searchactivity/SearchReferralEntity;

    move-result-object p1

    return-object p1
.end method

.method public bridge remove(Lcom/intsig/camscanner/searchactivity/SearchReferralEntity;)Z
    .locals 0

    .line 2
    invoke-super {p0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final bridge remove(Ljava/lang/Object;)Z
    .locals 1

    .line 3
    instance-of v0, p1, Lcom/intsig/camscanner/searchactivity/SearchReferralEntity;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    check-cast p1, Lcom/intsig/camscanner/searchactivity/SearchReferralEntity;

    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/searchactivity/SearchReferralOriEntity;->remove(Lcom/intsig/camscanner/searchactivity/SearchReferralEntity;)Z

    move-result p1

    return p1
.end method

.method public bridge removeAt(I)Lcom/intsig/camscanner/searchactivity/SearchReferralEntity;
    .locals 0

    .line 1
    invoke-super {p0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    check-cast p1, Lcom/intsig/camscanner/searchactivity/SearchReferralEntity;

    .line 6
    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final bridge size()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/searchactivity/SearchReferralOriEntity;->getSize()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    return v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

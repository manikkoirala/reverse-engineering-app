.class abstract Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;
.super Ljava/lang/Object;
.source "AbsSecurityMarkOperation.java"


# instance fields
.field O8:Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;

.field private final Oo08:Lcom/intsig/camscanner/securitymark/contact/SecurityMarkContract$AddSecurityMarkCallback;

.field 〇080:Lcom/intsig/camscanner/securitymark/presenter/SecurityMarkPresenter;

.field protected 〇o00〇〇Oo:Landroidx/fragment/app/FragmentActivity;

.field 〇o〇:Lcom/intsig/camscanner/share/ShareHelper;


# direct methods
.method constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation$1;

    .line 5
    .line 6
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation$1;-><init>(Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;)V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->Oo08:Lcom/intsig/camscanner/securitymark/contact/SecurityMarkContract$AddSecurityMarkCallback;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
.end method


# virtual methods
.method O8()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/share/data_mode/SharePageProperty;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->O8:Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return-object v0

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;->O8()Ljava/util/ArrayList;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method Oo08(Landroid/content/Intent;)V
    .locals 1

    .line 1
    const-string v0, "key_security_data"

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    check-cast p1, Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;

    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->O8:Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;

    .line 10
    .line 11
    if-nez p1, :cond_0

    .line 12
    .line 13
    const-string p1, "AbsSecurityMarkOperation"

    .line 14
    .line 15
    const-string v0, "securityImageShareData == null"

    .line 16
    .line 17
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    return-void

    .line 21
    :cond_0
    invoke-virtual {p1}, Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;->Oo08()Ljava/lang/String;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    if-eqz p1, :cond_1

    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->O8:Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;

    .line 32
    .line 33
    const-string/jumbo v0, "subjectTitle"

    .line 34
    .line 35
    .line 36
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;->oo88o8O(Ljava/lang/String;)V

    .line 37
    .line 38
    .line 39
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->O8:Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;

    .line 40
    .line 41
    invoke-virtual {p1}, Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;->o〇0()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object p1

    .line 45
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 46
    .line 47
    .line 48
    move-result p1

    .line 49
    if-eqz p1, :cond_2

    .line 50
    .line 51
    iget-object p1, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->O8:Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;

    .line 52
    .line 53
    const-string v0, "Doc"

    .line 54
    .line 55
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;->o〇O8〇〇o(Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    :cond_2
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method oO80(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList<",
            "Landroid/content/ContentProviderOperation;",
            ">;)V"
        }
    .end annotation

    .line 1
    const-string v0, "AbsSecurityMarkOperation"

    .line 2
    .line 3
    if-nez p1, :cond_0

    .line 4
    .line 5
    const-string/jumbo p1, "updateDB applicationContext == null"

    .line 6
    .line 7
    .line 8
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    invoke-static {p1, p2}, Lcom/intsig/camscanner/db/dao/DBDaoUtil;->O8(Landroid/content/Context;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 13
    .line 14
    .line 15
    move-result-object p2

    .line 16
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    if-lez v1, :cond_1

    .line 21
    .line 22
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    sget-object v1, Lcom/intsig/camscanner/provider/Documents;->〇080:Ljava/lang/String;

    .line 27
    .line 28
    invoke-virtual {p1, v1, p2}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :catch_0
    move-exception p1

    .line 33
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 34
    .line 35
    .line 36
    goto :goto_0

    .line 37
    :catch_1
    move-exception p1

    .line 38
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 39
    .line 40
    .line 41
    :cond_1
    :goto_0
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method o〇0(Landroidx/fragment/app/FragmentActivity;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method 〇080()V
    .locals 2

    .line 1
    const-string v0, "AbsSecurityMarkOperation"

    .line 2
    .line 3
    const-string v1, "clickComplete"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇o00〇〇Oo()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->〇080:Lcom/intsig/camscanner/securitymark/presenter/SecurityMarkPresenter;

    .line 2
    .line 3
    const-string v1, "AbsSecurityMarkOperation"

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    const-string v0, "securityMarkPresenter == null"

    .line 8
    .line 9
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_0
    const-string v0, "clickShare"

    .line 14
    .line 15
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 16
    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->〇080:Lcom/intsig/camscanner/securitymark/presenter/SecurityMarkPresenter;

    .line 19
    .line 20
    iget-object v1, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->Oo08:Lcom/intsig/camscanner/securitymark/contact/SecurityMarkContract$AddSecurityMarkCallback;

    .line 21
    .line 22
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/securitymark/presenter/SecurityMarkPresenter;->〇〇888(Lcom/intsig/camscanner/securitymark/contact/SecurityMarkContract$AddSecurityMarkCallback;)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method 〇o〇()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->O8:Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x0

    .line 6
    return-object v0

    .line 7
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;->〇o00〇〇Oo()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method 〇〇888(Lcom/intsig/camscanner/securitymark/presenter/SecurityMarkPresenter;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->〇080:Lcom/intsig/camscanner/securitymark/presenter/SecurityMarkPresenter;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

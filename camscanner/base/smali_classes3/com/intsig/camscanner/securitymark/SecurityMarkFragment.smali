.class public Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;
.super Landroidx/fragment/app/Fragment;
.source "SecurityMarkFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/intsig/camscanner/securitymark/contact/SecurityMarkContract$View;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroidx/fragment/app/Fragment;",
        "Landroid/view/View$OnClickListener;",
        "Lcom/intsig/camscanner/securitymark/contact/SecurityMarkContract$View<",
        "Lcom/intsig/camscanner/securitymark/presenter/SecurityMarkPresenter;",
        ">;"
    }
.end annotation


# static fields
.field private static final O0O:Ljava/lang/String; = "SecurityMarkFragment"


# instance fields
.field private O8o08O8O:Landroid/view/View;

.field private OO:Lcom/intsig/app/BaseProgressDialog;

.field protected OO〇00〇8oO:Landroidx/recyclerview/widget/LinearLayoutManager;

.field private o0:Landroid/view/View;

.field protected o8〇OO0〇0o:I

.field protected oOo0:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

.field protected oOo〇8o008:Landroid/widget/TextView;

.field private ooo0〇〇O:Lcom/intsig/camscanner/tools/GuidePopClient;

.field private o〇00O:Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;

.field private 〇080OO8〇0:Lcom/intsig/view/ImageTextButton;

.field private final 〇08O〇00〇o:Lorg/json/JSONObject;

.field private 〇0O:Lcom/intsig/view/ImageTextButton;

.field private 〇8〇oO〇〇8o:Lcom/intsig/camscanner/securitymark/adapter/SecurityMarkAdapter;

.field private 〇OOo8〇0:Lcom/intsig/camscanner/securitymark/presenter/SecurityMarkPresenter;

.field protected 〇〇08O:Landroid/animation/AnimatorSet;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lorg/json/JSONObject;

    .line 5
    .line 6
    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 7
    .line 8
    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇08O〇00〇o:Lorg/json/JSONObject;

    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method static bridge synthetic O0〇0()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->O0O:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private Ooo8o()V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/base/ToolbarThemeGet;->Oo08()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o0:Landroid/view/View;

    .line 8
    .line 9
    const/high16 v1, -0x1000000

    .line 10
    .line 11
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 12
    .line 13
    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o0:Landroid/view/View;

    .line 15
    .line 16
    const v1, 0x7f0a1672    # 1.8355E38f

    .line 17
    .line 18
    .line 19
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    check-cast v0, Landroid/widget/TextView;

    .line 24
    .line 25
    iput-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->oOo〇8o008:Landroid/widget/TextView;

    .line 26
    .line 27
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o0:Landroid/view/View;

    .line 28
    .line 29
    const v1, 0x7f0a0b5b

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    check-cast v0, Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 37
    .line 38
    iput-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->oOo0:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 39
    .line 40
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o0:Landroid/view/View;

    .line 41
    .line 42
    const v1, 0x7f0a15db

    .line 43
    .line 44
    .line 45
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    check-cast v0, Lcom/intsig/view/ImageTextButton;

    .line 50
    .line 51
    iput-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇0O:Lcom/intsig/view/ImageTextButton;

    .line 52
    .line 53
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o0:Landroid/view/View;

    .line 54
    .line 55
    const v1, 0x7f0a1258

    .line 56
    .line 57
    .line 58
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    iput-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->O8o08O8O:Landroid/view/View;

    .line 63
    .line 64
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o0:Landroid/view/View;

    .line 65
    .line 66
    const v1, 0x7f0a1368

    .line 67
    .line 68
    .line 69
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 70
    .line 71
    .line 72
    move-result-object v0

    .line 73
    check-cast v0, Lcom/intsig/view/ImageTextButton;

    .line 74
    .line 75
    iput-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇080OO8〇0:Lcom/intsig/view/ImageTextButton;

    .line 76
    .line 77
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇0O:Lcom/intsig/view/ImageTextButton;

    .line 78
    .line 79
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    .line 81
    .line 82
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->O8o08O8O:Landroid/view/View;

    .line 83
    .line 84
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    .line 86
    .line 87
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇080OO8〇0:Lcom/intsig/view/ImageTextButton;

    .line 88
    .line 89
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    .line 91
    .line 92
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->O8o08O8O:Landroid/view/View;

    .line 93
    .line 94
    const/16 v1, 0x8

    .line 95
    .line 96
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 97
    .line 98
    .line 99
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇080OO8〇0:Lcom/intsig/view/ImageTextButton;

    .line 100
    .line 101
    const/4 v1, 0x0

    .line 102
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 103
    .line 104
    .line 105
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇080OO8〇0:Lcom/intsig/view/ImageTextButton;

    .line 106
    .line 107
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 108
    .line 109
    .line 110
    move-result-object v1

    .line 111
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 112
    .line 113
    .line 114
    move-result-object v1

    .line 115
    const v2, 0x7f060208

    .line 116
    .line 117
    .line 118
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    .line 119
    .line 120
    .line 121
    move-result v1

    .line 122
    invoke-virtual {v0, v1}, Lcom/intsig/view/ImageTextButton;->setTextColor(I)V

    .line 123
    .line 124
    .line 125
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇0O:Lcom/intsig/view/ImageTextButton;

    .line 126
    .line 127
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 128
    .line 129
    .line 130
    move-result-object v1

    .line 131
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 132
    .line 133
    .line 134
    move-result-object v1

    .line 135
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    .line 136
    .line 137
    .line 138
    move-result v1

    .line 139
    invoke-virtual {v0, v1}, Lcom/intsig/view/ImageTextButton;->setTextColor(I)V

    .line 140
    .line 141
    .line 142
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->oo〇88()Z

    .line 143
    .line 144
    .line 145
    move-result v0

    .line 146
    if-eqz v0, :cond_1

    .line 147
    .line 148
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇0O:Lcom/intsig/view/ImageTextButton;

    .line 149
    .line 150
    new-instance v1, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment$1;

    .line 151
    .line 152
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment$1;-><init>(Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;)V

    .line 153
    .line 154
    .line 155
    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 156
    .line 157
    .line 158
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇8〇80o()V

    .line 159
    .line 160
    .line 161
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 162
    .line 163
    .line 164
    move-result-object v0

    .line 165
    const/4 v1, 0x6

    .line 166
    invoke-static {v0, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 167
    .line 168
    .line 169
    move-result v0

    .line 170
    iget-object v1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->oOo0:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 171
    .line 172
    new-instance v2, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment$2;

    .line 173
    .line 174
    invoke-direct {v2, p0, v0}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment$2;-><init>(Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;I)V

    .line 175
    .line 176
    .line 177
    invoke-virtual {v1, v2}, Landroidx/recyclerview/widget/RecyclerView;->addItemDecoration(Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;)V

    .line 178
    .line 179
    .line 180
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 181
    .line 182
    .line 183
    move-result-object v0

    .line 184
    instance-of v1, v0, Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 185
    .line 186
    if-eqz v1, :cond_2

    .line 187
    .line 188
    iget-object v1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->oOo0:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 189
    .line 190
    check-cast v0, Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 191
    .line 192
    invoke-virtual {v0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇o08()Landroidx/appcompat/widget/Toolbar;

    .line 193
    .line 194
    .line 195
    move-result-object v0

    .line 196
    invoke-static {v1, v0}, Lcom/intsig/camscanner/util/RvUtils;->〇080(Landroidx/recyclerview/widget/RecyclerView;Landroid/view/View;)V

    .line 197
    .line 198
    .line 199
    :cond_2
    return-void
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method static bridge synthetic o00〇88〇08(Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇00()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic o880(Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;)Lcom/intsig/camscanner/securitymark/presenter/SecurityMarkPresenter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇OOo8〇0:Lcom/intsig/camscanner/securitymark/presenter/SecurityMarkPresenter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private oOoO8OO〇()V
    .locals 3

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇08O〇00〇o:Lorg/json/JSONObject;

    .line 2
    .line 3
    const-string v1, "from_part"

    .line 4
    .line 5
    iget-object v2, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o〇00O:Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;

    .line 6
    .line 7
    invoke-virtual {v2}, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->〇o〇()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 8
    .line 9
    .line 10
    move-result-object v2

    .line 11
    invoke-virtual {v2}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 16
    .line 17
    .line 18
    goto :goto_0

    .line 19
    :catch_0
    move-exception v0

    .line 20
    sget-object v1, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->O0O:Ljava/lang/String;

    .line 21
    .line 22
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 23
    .line 24
    .line 25
    :goto_0
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method static bridge synthetic oOo〇08〇(Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;)Lcom/intsig/view/ImageTextButton;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇080OO8〇0:Lcom/intsig/view/ImageTextButton;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic oO〇oo(Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;)Lcom/intsig/camscanner/securitymark/adapter/SecurityMarkAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/securitymark/adapter/SecurityMarkAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic oooO888(Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;)Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o〇00O:Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private 〇00()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->O0O:Ljava/lang/String;

    .line 8
    .line 9
    const-string v1, "finishActivity activity == null"

    .line 10
    .line 11
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private 〇0ooOOo(Landroid/content/Intent;)V
    .locals 2

    .line 1
    const-string v0, "key_security_operation"

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    invoke-static {p1}, Lcom/intsig/camscanner/securitymark/SecurityOperationFactory;->〇080(I)Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    iput-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o〇00O:Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;

    .line 13
    .line 14
    if-eqz p1, :cond_0

    .line 15
    .line 16
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->o〇0(Landroidx/fragment/app/FragmentActivity;)V

    .line 21
    .line 22
    .line 23
    iget-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o〇00O:Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;

    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇OOo8〇0:Lcom/intsig/camscanner/securitymark/presenter/SecurityMarkPresenter;

    .line 26
    .line 27
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->〇〇888(Lcom/intsig/camscanner/securitymark/presenter/SecurityMarkPresenter;)V

    .line 28
    .line 29
    .line 30
    :cond_0
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private 〇0〇0()V
    .locals 6

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->O0O:Ljava/lang/String;

    .line 8
    .line 9
    const-string v1, "activity == null"

    .line 10
    .line 11
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    if-nez v0, :cond_1

    .line 20
    .line 21
    sget-object v0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->O0O:Ljava/lang/String;

    .line 22
    .line 23
    const-string v1, "intent == null"

    .line 24
    .line 25
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    return-void

    .line 29
    :cond_1
    const-string v1, "key_show_done_button"

    .line 30
    .line 31
    const/4 v2, 0x1

    .line 32
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 33
    .line 34
    .line 35
    move-result v1

    .line 36
    iget-object v3, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o0:Landroid/view/View;

    .line 37
    .line 38
    const v4, 0x7f0a0805

    .line 39
    .line 40
    .line 41
    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 42
    .line 43
    .line 44
    move-result-object v3

    .line 45
    iget-object v4, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o0:Landroid/view/View;

    .line 46
    .line 47
    const v5, 0x7f0a0cf2

    .line 48
    .line 49
    .line 50
    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 51
    .line 52
    .line 53
    move-result-object v4

    .line 54
    if-eqz v1, :cond_2

    .line 55
    .line 56
    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    .line 58
    .line 59
    const/4 v1, 0x0

    .line 60
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 61
    .line 62
    .line 63
    goto :goto_0

    .line 64
    :cond_2
    const/16 v1, 0x8

    .line 65
    .line 66
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 67
    .line 68
    .line 69
    :goto_0
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇0ooOOo(Landroid/content/Intent;)V

    .line 70
    .line 71
    .line 72
    iget-object v1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o〇00O:Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;

    .line 73
    .line 74
    if-nez v1, :cond_3

    .line 75
    .line 76
    sget-object v0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->O0O:Ljava/lang/String;

    .line 77
    .line 78
    const-string v1, "securityMarkOperation == null"

    .line 79
    .line 80
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    .line 82
    .line 83
    return-void

    .line 84
    :cond_3
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->Oo08(Landroid/content/Intent;)V

    .line 85
    .line 86
    .line 87
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o〇00O:Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;

    .line 88
    .line 89
    invoke-virtual {v0}, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->O8()Ljava/util/List;

    .line 90
    .line 91
    .line 92
    move-result-object v0

    .line 93
    if-nez v0, :cond_4

    .line 94
    .line 95
    sget-object v0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->O0O:Ljava/lang/String;

    .line 96
    .line 97
    const-string v1, "securityMarkOperation.getSharePageProperty() == null"

    .line 98
    .line 99
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    .line 101
    .line 102
    return-void

    .line 103
    :cond_4
    invoke-direct {p0}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->oOoO8OO〇()V

    .line 104
    .line 105
    .line 106
    invoke-virtual {p0}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o〇0()Landroid/content/Context;

    .line 107
    .line 108
    .line 109
    move-result-object v0

    .line 110
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 111
    .line 112
    .line 113
    move-result v0

    .line 114
    shr-int/2addr v0, v2

    .line 115
    iput v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o8〇OO0〇0o:I

    .line 116
    .line 117
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇OOo8〇0:Lcom/intsig/camscanner/securitymark/presenter/SecurityMarkPresenter;

    .line 118
    .line 119
    iget-object v1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o〇00O:Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;

    .line 120
    .line 121
    invoke-virtual {v1}, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->O8()Ljava/util/List;

    .line 122
    .line 123
    .line 124
    move-result-object v1

    .line 125
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/securitymark/presenter/SecurityMarkPresenter;->Oooo8o0〇(Ljava/util/List;)V

    .line 126
    .line 127
    .line 128
    return-void
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method static bridge synthetic 〇80O8o8O〇(Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;)Landroid/view/View;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->O8o08O8O:Landroid/view/View;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private 〇8〇80o()V
    .locals 4

    .line 1
    new-instance v0, Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 2
    .line 3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const/4 v2, 0x1

    .line 8
    const/4 v3, 0x0

    .line 9
    invoke-direct {v0, v1, v2, v3}, Landroidx/recyclerview/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->OO〇00〇8oO:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 13
    .line 14
    iget-object v1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->oOo0:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 15
    .line 16
    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 17
    .line 18
    .line 19
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->oOo0:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 20
    .line 21
    new-instance v1, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment$7;

    .line 22
    .line 23
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment$7;-><init>(Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;)V

    .line 24
    .line 25
    .line 26
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->addOnScrollListener(Landroidx/recyclerview/widget/RecyclerView$OnScrollListener;)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private 〇8〇OOoooo(I)Z
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇o0〇〇()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    new-instance v1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 8
    .line 9
    invoke-direct {v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 10
    .line 11
    .line 12
    sget-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_SECURITY_MARK:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 13
    .line 14
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function(Lcom/intsig/camscanner/purchase/entity/Function;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 15
    .line 16
    .line 17
    move-result-object v1

    .line 18
    iget-object v2, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o〇00O:Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;

    .line 19
    .line 20
    invoke-virtual {v2}, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->〇o〇()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-static {p0, v1, p1}, Lcom/intsig/camscanner/tsapp/purchase/PurchaseSceneAdapter;->o800o8O(Landroidx/fragment/app/Fragment;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;I)V

    .line 29
    .line 30
    .line 31
    :cond_0
    return v0
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method static bridge synthetic 〇O8oOo0(Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;)Lcom/intsig/view/ImageTextButton;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇0O:Lcom/intsig/view/ImageTextButton;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private 〇o08(Ljava/lang/String;)V
    .locals 3

    .line 1
    const-string v0, "CSAddSecurity"

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇08O〇00〇o:Lorg/json/JSONObject;

    .line 4
    .line 5
    invoke-static {v0, p1, v1}, Lcom/intsig/camscanner/log/LogAgentData;->Oo08(Ljava/lang/String;Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->〇8〇0〇o〇O()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    sget-object v0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->O0O:Ljava/lang/String;

    .line 15
    .line 16
    new-instance v1, Ljava/lang/StringBuilder;

    .line 17
    .line 18
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 19
    .line 20
    .line 21
    const-string v2, "printShowLog actionId="

    .line 22
    .line 23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    const-string p1, " mFromPartObject="

    .line 30
    .line 31
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    iget-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇08O〇00〇o:Lorg/json/JSONObject;

    .line 35
    .line 36
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    :cond_0
    return-void
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method static bridge synthetic 〇〇o0〇8(Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇o08(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private 〇〇〇0()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->O0O:Ljava/lang/String;

    .line 8
    .line 9
    const-string/jumbo v1, "showAddMarkDialog activity == null"

    .line 10
    .line 11
    .line 12
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    return-void

    .line 16
    :cond_0
    new-instance v1, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment$5;

    .line 17
    .line 18
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment$5;-><init>(Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;)V

    .line 19
    .line 20
    .line 21
    invoke-static {v0, v1}, Lcom/intsig/camscanner/securitymark/ModifySecurityMarkDialog;->〇O〇(Landroid/content/Context;Lcom/intsig/camscanner/securitymark/ModifySecurityMarkDialog$SecurityMarkChangeListener;)Lcom/intsig/camscanner/securitymark/ModifySecurityMarkDialog;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lcom/intsig/camscanner/securitymark/ModifySecurityMarkDialog;->o800o8O()V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method


# virtual methods
.method public Oo08(I)V
    .locals 2

    .line 1
    iget-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->OO:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-nez p1, :cond_0

    .line 4
    .line 5
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    const/4 v0, 0x0

    .line 10
    invoke-static {p1, v0}, Lcom/intsig/utils/DialogUtils;->〇o〇(Landroid/content/Context;I)Lcom/intsig/app/BaseProgressDialog;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    iput-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->OO:Lcom/intsig/app/BaseProgressDialog;

    .line 15
    .line 16
    const v1, 0x7f130e22

    .line 17
    .line 18
    .line 19
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-virtual {p1, v1}, Lcom/intsig/app/BaseProgressDialog;->o〇O8〇〇o(Ljava/lang/CharSequence;)V

    .line 24
    .line 25
    .line 26
    iget-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->OO:Lcom/intsig/app/BaseProgressDialog;

    .line 27
    .line 28
    invoke-virtual {p1, v0}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 29
    .line 30
    .line 31
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->OO:Lcom/intsig/app/BaseProgressDialog;

    .line 32
    .line 33
    invoke-virtual {p1}, Landroid/app/Dialog;->isShowing()Z

    .line 34
    .line 35
    .line 36
    move-result p1

    .line 37
    if-nez p1, :cond_1

    .line 38
    .line 39
    iget-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->OO:Lcom/intsig/app/BaseProgressDialog;

    .line 40
    .line 41
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V

    .line 42
    .line 43
    .line 44
    :cond_1
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public O〇〇(I)V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public o88O〇8(Ljava/util/List;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/share/data_mode/SharePageProperty;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/securitymark/adapter/SecurityMarkAdapter;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Lcom/intsig/camscanner/securitymark/adapter/SecurityMarkAdapter;

    .line 6
    .line 7
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 8
    .line 9
    .line 10
    move-result-object v1

    .line 11
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/securitymark/adapter/SecurityMarkAdapter;-><init>(Landroid/content/Context;)V

    .line 12
    .line 13
    .line 14
    iput-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/securitymark/adapter/SecurityMarkAdapter;

    .line 15
    .line 16
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/securitymark/adapter/SecurityMarkAdapter;

    .line 17
    .line 18
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/securitymark/adapter/SecurityMarkAdapter;->o800o8O(Ljava/util/List;)V

    .line 19
    .line 20
    .line 21
    iget-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/securitymark/adapter/SecurityMarkAdapter;

    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇OOo8〇0:Lcom/intsig/camscanner/securitymark/presenter/SecurityMarkPresenter;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/camscanner/securitymark/presenter/SecurityMarkPresenter;->oO80()Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/securitymark/adapter/SecurityMarkAdapter;->〇O888o0o(Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;)V

    .line 30
    .line 31
    .line 32
    iget-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->oOo0:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 33
    .line 34
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/securitymark/adapter/SecurityMarkAdapter;

    .line 35
    .line 36
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 37
    .line 38
    .line 39
    iget-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->oOo0:Lcom/intsig/camscanner/topic/view/SmoothScrollRecyclerView;

    .line 40
    .line 41
    const/4 v0, 0x0

    .line 42
    invoke-virtual {p1, v0}, Landroidx/recyclerview/widget/RecyclerView;->scrollToPosition(I)V

    .line 43
    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .line 1
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    sget-object p2, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->O0O:Ljava/lang/String;

    .line 5
    .line 6
    new-instance p3, Ljava/lang/StringBuilder;

    .line 7
    .line 8
    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    const-string v0, "onActivityResult requestCode="

    .line 12
    .line 13
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object p3

    .line 23
    invoke-static {p2, p3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 27
    .line 28
    .line 29
    move-result p3

    .line 30
    const-string v0, "onActivityResult is viper"

    .line 31
    .line 32
    if-eqz p3, :cond_4

    .line 33
    .line 34
    const/16 p3, 0xc9

    .line 35
    .line 36
    if-ne p1, p3, :cond_0

    .line 37
    .line 38
    invoke-virtual {p0}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇〇〇00()V

    .line 39
    .line 40
    .line 41
    invoke-static {p2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_0
    const/16 p2, 0xca

    .line 46
    .line 47
    if-ne p1, p2, :cond_5

    .line 48
    .line 49
    iget-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o〇00O:Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;

    .line 50
    .line 51
    if-nez p1, :cond_1

    .line 52
    .line 53
    return-void

    .line 54
    :cond_1
    iget-object p1, p1, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->O8:Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;

    .line 55
    .line 56
    if-eqz p1, :cond_3

    .line 57
    .line 58
    invoke-virtual {p1}, Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;->〇o00〇〇Oo()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    if-eqz p1, :cond_3

    .line 63
    .line 64
    iget-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o〇00O:Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;

    .line 65
    .line 66
    iget-object p1, p1, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->O8:Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;

    .line 67
    .line 68
    invoke-virtual {p1}, Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;->〇o00〇〇Oo()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 69
    .line 70
    .line 71
    move-result-object p1

    .line 72
    sget-object p2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_PDF_PACKAGE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 73
    .line 74
    if-eq p1, p2, :cond_2

    .line 75
    .line 76
    iget-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o〇00O:Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;

    .line 77
    .line 78
    iget-object p1, p1, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->O8:Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;

    .line 79
    .line 80
    invoke-virtual {p1}, Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;->〇o00〇〇Oo()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 81
    .line 82
    .line 83
    move-result-object p1

    .line 84
    sget-object p2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_PDF_PACKAGE_LOCAL:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 85
    .line 86
    if-ne p1, p2, :cond_3

    .line 87
    .line 88
    :cond_2
    const-string p1, "CSPdfPackage"

    .line 89
    .line 90
    const-string/jumbo p2, "watermark_success"

    .line 91
    .line 92
    .line 93
    invoke-static {p1, p2}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    .line 95
    .line 96
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o〇00O:Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;

    .line 97
    .line 98
    invoke-virtual {p1}, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->〇080()V

    .line 99
    .line 100
    .line 101
    goto :goto_0

    .line 102
    :cond_4
    invoke-static {p2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    :cond_5
    :goto_0
    return-void
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    const v0, 0x7f0a1258

    .line 6
    .line 7
    .line 8
    if-ne p1, v0, :cond_0

    .line 9
    .line 10
    sget-object p1, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->O0O:Ljava/lang/String;

    .line 11
    .line 12
    const-string v0, "add mark"

    .line 13
    .line 14
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇〇〇0()V

    .line 18
    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const v0, 0x7f0a15db

    .line 22
    .line 23
    .line 24
    if-ne p1, v0, :cond_1

    .line 25
    .line 26
    sget-object p1, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->O0O:Ljava/lang/String;

    .line 27
    .line 28
    const-string v0, "modify mark"

    .line 29
    .line 30
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    const-string p1, "fix_security_water"

    .line 34
    .line 35
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇o08(Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    invoke-direct {p0}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇〇〇0()V

    .line 39
    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_1
    const v0, 0x7f0a1368

    .line 43
    .line 44
    .line 45
    if-ne p1, v0, :cond_2

    .line 46
    .line 47
    iget-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/securitymark/adapter/SecurityMarkAdapter;

    .line 48
    .line 49
    invoke-virtual {p1}, Lcom/intsig/camscanner/securitymark/adapter/SecurityMarkAdapter;->OoO8()V

    .line 50
    .line 51
    .line 52
    iget-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->O8o08O8O:Landroid/view/View;

    .line 53
    .line 54
    const/4 v0, 0x0

    .line 55
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 56
    .line 57
    .line 58
    iget-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇080OO8〇0:Lcom/intsig/view/ImageTextButton;

    .line 59
    .line 60
    const/16 v1, 0x8

    .line 61
    .line 62
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 63
    .line 64
    .line 65
    iget-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇0O:Lcom/intsig/view/ImageTextButton;

    .line 66
    .line 67
    const v1, 0x3e99999a    # 0.3f

    .line 68
    .line 69
    .line 70
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 71
    .line 72
    .line 73
    iget-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇0O:Lcom/intsig/view/ImageTextButton;

    .line 74
    .line 75
    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 76
    .line 77
    .line 78
    iget-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇OOo8〇0:Lcom/intsig/camscanner/securitymark/presenter/SecurityMarkPresenter;

    .line 79
    .line 80
    const/4 v0, 0x0

    .line 81
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/securitymark/presenter/SecurityMarkPresenter;->OO0o〇〇(Lcom/intsig/camscanner/securitymark/mode/SecurityMarkEntity;)V

    .line 82
    .line 83
    .line 84
    goto :goto_0

    .line 85
    :cond_2
    const v0, 0x7f0a0805

    .line 86
    .line 87
    .line 88
    if-ne p1, v0, :cond_4

    .line 89
    .line 90
    iget-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/securitymark/adapter/SecurityMarkAdapter;

    .line 91
    .line 92
    invoke-virtual {p1}, Lcom/intsig/camscanner/securitymark/adapter/SecurityMarkAdapter;->〇O00()Z

    .line 93
    .line 94
    .line 95
    move-result p1

    .line 96
    if-eqz p1, :cond_3

    .line 97
    .line 98
    invoke-direct {p0}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇00()V

    .line 99
    .line 100
    .line 101
    goto :goto_0

    .line 102
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o〇0〇o()V

    .line 103
    .line 104
    .line 105
    :cond_4
    :goto_0
    return-void
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1    # Landroid/content/res/Configuration;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/securitymark/adapter/SecurityMarkAdapter;

    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/intsig/camscanner/securitymark/adapter/SecurityMarkAdapter;->getItemCount()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    const/4 v1, 0x0

    .line 17
    invoke-virtual {p1, v1, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyItemChanged(ILjava/lang/Object;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    return-void
    .line 21
    .line 22
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/LayoutInflater;
        .annotation build Landroidx/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroidx/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    const p3, 0x7f0d033c

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 6
    .line 7
    .line 8
    move-result-object p1

    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o0:Landroid/view/View;

    .line 10
    .line 11
    new-instance p1, Lcom/intsig/camscanner/securitymark/presenter/SecurityMarkPresenter;

    .line 12
    .line 13
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/securitymark/presenter/SecurityMarkPresenter;-><init>(Lcom/intsig/camscanner/securitymark/contact/SecurityMarkContract$View;)V

    .line 14
    .line 15
    .line 16
    iput-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇OOo8〇0:Lcom/intsig/camscanner/securitymark/presenter/SecurityMarkPresenter;

    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->Ooo8o()V

    .line 19
    .line 20
    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇0〇0()V

    .line 22
    .line 23
    .line 24
    iget-object p1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o0:Landroid/view/View;

    .line 25
    .line 26
    return-object p1
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public onStart()V
    .locals 3

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onStart()V

    .line 2
    .line 3
    .line 4
    const-string v0, "CSAddSecurity"

    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇08O〇00〇o:Lorg/json/JSONObject;

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇O00(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 9
    .line 10
    .line 11
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->〇8〇0〇o〇O()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    sget-object v0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->O0O:Ljava/lang/String;

    .line 18
    .line 19
    new-instance v1, Ljava/lang/StringBuilder;

    .line 20
    .line 21
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 22
    .line 23
    .line 24
    const-string v2, "printShowLog  mFromPartObject="

    .line 25
    .line 26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    iget-object v2, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇08O〇00〇o:Lorg/json/JSONObject;

    .line 30
    .line 31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    :cond_0
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public o〇0()Landroid/content/Context;
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getContext()Landroid/content/Context;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    :cond_0
    if-nez v0, :cond_1

    .line 12
    .line 13
    invoke-static {}, Lcom/intsig/camscanner/launch/CsApplication;->o0ooO()Lcom/intsig/camscanner/launch/CsApplication;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    :cond_1
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public o〇0〇o()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->O0O:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "clickComplete"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, "done"

    .line 9
    .line 10
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇o08(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    const/16 v0, 0xca

    .line 14
    .line 15
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇8〇OOoooo(I)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    if-nez v0, :cond_0

    .line 20
    .line 21
    return-void

    .line 22
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o〇00O:Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;

    .line 23
    .line 24
    if-nez v0, :cond_1

    .line 25
    .line 26
    return-void

    .line 27
    :cond_1
    iget-object v0, v0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->O8:Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;

    .line 28
    .line 29
    if-eqz v0, :cond_3

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;->〇o00〇〇Oo()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    if-eqz v0, :cond_3

    .line 36
    .line 37
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o〇00O:Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;

    .line 38
    .line 39
    iget-object v0, v0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->O8:Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;

    .line 40
    .line 41
    invoke-virtual {v0}, Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;->〇o00〇〇Oo()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    sget-object v1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_PDF_PACKAGE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 46
    .line 47
    if-eq v0, v1, :cond_2

    .line 48
    .line 49
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o〇00O:Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;

    .line 50
    .line 51
    iget-object v0, v0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->O8:Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;

    .line 52
    .line 53
    invoke-virtual {v0}, Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;->〇o00〇〇Oo()Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 54
    .line 55
    .line 56
    move-result-object v0

    .line 57
    sget-object v1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_PDF_PACKAGE_LOCAL:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 58
    .line 59
    if-ne v0, v1, :cond_3

    .line 60
    .line 61
    :cond_2
    const-string v0, "CSPdfPackage"

    .line 62
    .line 63
    const-string/jumbo v1, "watermark_success"

    .line 64
    .line 65
    .line 66
    invoke-static {v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    .line 68
    .line 69
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o〇00O:Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;

    .line 70
    .line 71
    invoke-virtual {v0}, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->〇080()V

    .line 72
    .line 73
    .line 74
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method protected o〇O8OO()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇〇08O:Landroid/animation/AnimatorSet;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Landroid/animation/AnimatorSet;

    .line 6
    .line 7
    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇〇08O:Landroid/animation/AnimatorSet;

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->oOo〇8o008:Landroid/widget/TextView;

    .line 13
    .line 14
    const/4 v1, 0x2

    .line 15
    new-array v1, v1, [F

    .line 16
    .line 17
    fill-array-data v1, :array_0

    .line 18
    .line 19
    .line 20
    const-string v2, "alpha"

    .line 21
    .line 22
    invoke-static {v0, v2, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    iget-object v1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇〇08O:Landroid/animation/AnimatorSet;

    .line 27
    .line 28
    const-wide/16 v2, 0xfa

    .line 29
    .line 30
    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 31
    .line 32
    .line 33
    iget-object v1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇〇08O:Landroid/animation/AnimatorSet;

    .line 34
    .line 35
    const/4 v2, 0x1

    .line 36
    new-array v2, v2, [Landroid/animation/Animator;

    .line 37
    .line 38
    const/4 v3, 0x0

    .line 39
    aput-object v0, v2, v3

    .line 40
    .line 41
    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 42
    .line 43
    .line 44
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇〇08O:Landroid/animation/AnimatorSet;

    .line 45
    .line 46
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    .line 47
    .line 48
    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 49
    .line 50
    .line 51
    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 52
    .line 53
    .line 54
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇〇08O:Landroid/animation/AnimatorSet;

    .line 55
    .line 56
    const-wide/16 v1, 0x320

    .line 57
    .line 58
    invoke-virtual {v0, v1, v2}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    .line 59
    .line 60
    .line 61
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇〇08O:Landroid/animation/AnimatorSet;

    .line 62
    .line 63
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    .line 64
    .line 65
    .line 66
    move-result v0

    .line 67
    if-nez v0, :cond_1

    .line 68
    .line 69
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇〇08O:Landroid/animation/AnimatorSet;

    .line 70
    .line 71
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 72
    .line 73
    .line 74
    :cond_1
    return-void

    .line 75
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public 〇088O()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->O0O:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "share"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇o08(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇〇〇00()V

    .line 12
    .line 13
    .line 14
    return-void
    .line 15
.end method

.method protected 〇0oO〇oo00()V
    .locals 6

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇〇08O:Landroid/animation/AnimatorSet;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇〇08O:Landroid/animation/AnimatorSet;

    .line 12
    .line 13
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 14
    .line 15
    .line 16
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->OO〇00〇8oO:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 17
    .line 18
    invoke-virtual {v0}, Landroidx/recyclerview/widget/LinearLayoutManager;->findFirstVisibleItemPosition()I

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    iget-object v1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->OO〇00〇8oO:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 23
    .line 24
    invoke-virtual {v1}, Landroidx/recyclerview/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    move v2, v1

    .line 29
    :goto_0
    const/4 v3, 0x1

    .line 30
    if-lt v2, v0, :cond_2

    .line 31
    .line 32
    const/4 v4, 0x2

    .line 33
    new-array v4, v4, [I

    .line 34
    .line 35
    iget-object v5, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->OO〇00〇8oO:Landroidx/recyclerview/widget/LinearLayoutManager;

    .line 36
    .line 37
    invoke-virtual {v5, v2}, Landroidx/recyclerview/widget/LinearLayoutManager;->findViewByPosition(I)Landroid/view/View;

    .line 38
    .line 39
    .line 40
    move-result-object v5

    .line 41
    if-eqz v5, :cond_1

    .line 42
    .line 43
    invoke-virtual {v5, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 44
    .line 45
    .line 46
    aget v4, v4, v3

    .line 47
    .line 48
    iget v5, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o8〇OO0〇0o:I

    .line 49
    .line 50
    if-gt v4, v5, :cond_1

    .line 51
    .line 52
    move v1, v2

    .line 53
    goto :goto_1

    .line 54
    :cond_1
    add-int/lit8 v2, v2, -0x1

    .line 55
    .line 56
    goto :goto_0

    .line 57
    :cond_2
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 58
    .line 59
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 60
    .line 61
    .line 62
    add-int/2addr v1, v3

    .line 63
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 64
    .line 65
    .line 66
    const-string v1, "/"

    .line 67
    .line 68
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    .line 70
    .line 71
    iget-object v1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/securitymark/adapter/SecurityMarkAdapter;

    .line 72
    .line 73
    invoke-virtual {v1}, Lcom/intsig/camscanner/securitymark/adapter/SecurityMarkAdapter;->getItemCount()I

    .line 74
    .line 75
    .line 76
    move-result v1

    .line 77
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object v0

    .line 84
    iget-object v1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->oOo〇8o008:Landroid/widget/TextView;

    .line 85
    .line 86
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 87
    .line 88
    .line 89
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->oOo〇8o008:Landroid/widget/TextView;

    .line 90
    .line 91
    const/high16 v1, 0x3f800000    # 1.0f

    .line 92
    .line 93
    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 94
    .line 95
    .line 96
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->oOo〇8o008:Landroid/widget/TextView;

    .line 97
    .line 98
    const/4 v1, 0x0

    .line 99
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 100
    .line 101
    .line 102
    return-void
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public 〇O〇〇O8()Landroid/app/Activity;
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇o〇()V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->OO:Lcom/intsig/app/BaseProgressDialog;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->OO:Lcom/intsig/app/BaseProgressDialog;

    .line 12
    .line 13
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->dismiss()V

    .line 14
    .line 15
    .line 16
    :cond_0
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public 〇o〇88〇8()V
    .locals 3

    .line 1
    const-string v0, "back"

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇o08(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    sget-object v0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->O0O:Ljava/lang/String;

    .line 13
    .line 14
    const-string v1, "clickToBack activity == null"

    .line 15
    .line 16
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    return-void

    .line 20
    :cond_0
    new-instance v1, Lcom/intsig/app/AlertDialog$Builder;

    .line 21
    .line 22
    invoke-direct {v1, v0}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 23
    .line 24
    .line 25
    const v0, 0x7f131d10

    .line 26
    .line 27
    .line 28
    invoke-virtual {v1, v0}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    const v1, 0x7f130618

    .line 33
    .line 34
    .line 35
    invoke-virtual {v0, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    new-instance v1, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment$4;

    .line 40
    .line 41
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment$4;-><init>(Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;)V

    .line 42
    .line 43
    .line 44
    const v2, 0x7f13057e

    .line 45
    .line 46
    .line 47
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->OoO8(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    new-instance v1, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment$3;

    .line 52
    .line 53
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment$3;-><init>(Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;)V

    .line 54
    .line 55
    .line 56
    const v2, 0x7f130617

    .line 57
    .line 58
    .line 59
    invoke-virtual {v0, v2, v1}, Lcom/intsig/app/AlertDialog$Builder;->〇0000OOO(ILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 68
    .line 69
    .line 70
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public 〇〇O80〇0o(Landroid/app/Activity;Landroid/view/View;)V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->O0O:Ljava/lang/String;

    .line 2
    .line 3
    const-string/jumbo v1, "showDocFragmentGuidPop"

    .line 4
    .line 5
    .line 6
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->ooo0〇〇O:Lcom/intsig/camscanner/tools/GuidePopClient;

    .line 10
    .line 11
    if-nez v0, :cond_0

    .line 12
    .line 13
    invoke-static {p1}, Lcom/intsig/camscanner/tools/GuidePopClient;->oO80(Landroid/app/Activity;)Lcom/intsig/camscanner/tools/GuidePopClient;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    iput-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->ooo0〇〇O:Lcom/intsig/camscanner/tools/GuidePopClient;

    .line 18
    .line 19
    new-instance v1, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment$6;

    .line 20
    .line 21
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment$6;-><init>(Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tools/GuidePopClient;->〇80〇808〇O(Lcom/intsig/camscanner/tools/GuidePopClient$GuidPopClientCallback;)V

    .line 25
    .line 26
    .line 27
    new-instance v0, Lcom/intsig/camscanner/tools/GuidePopClient$GuidPopClientParams;

    .line 28
    .line 29
    invoke-direct {v0}, Lcom/intsig/camscanner/tools/GuidePopClient$GuidPopClientParams;-><init>()V

    .line 30
    .line 31
    .line 32
    sget-object v1, Lcom/intsig/comm/widget/CustomTextView$ArrowDirection;->BOTTOM:Lcom/intsig/comm/widget/CustomTextView$ArrowDirection;

    .line 33
    .line 34
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tools/GuidePopClient$GuidPopClientParams;->〇〇808〇(Lcom/intsig/comm/widget/CustomTextView$ArrowDirection;)V

    .line 35
    .line 36
    .line 37
    const v1, 0x7f130651

    .line 38
    .line 39
    .line 40
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tools/GuidePopClient$GuidPopClientParams;->OoO8(Ljava/lang/CharSequence;)V

    .line 45
    .line 46
    .line 47
    const/4 v1, 0x6

    .line 48
    invoke-static {p1, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 49
    .line 50
    .line 51
    move-result v1

    .line 52
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/tools/GuidePopClient$GuidPopClientParams;->〇O00(I)V

    .line 53
    .line 54
    .line 55
    iget-object v1, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->ooo0〇〇O:Lcom/intsig/camscanner/tools/GuidePopClient;

    .line 56
    .line 57
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/tools/GuidePopClient;->OO0o〇〇〇〇0(Lcom/intsig/camscanner/tools/GuidePopClient$GuidPopClientParams;)V

    .line 58
    .line 59
    .line 60
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->ooo0〇〇O:Lcom/intsig/camscanner/tools/GuidePopClient;

    .line 61
    .line 62
    invoke-virtual {v0, p1, p2}, Lcom/intsig/camscanner/tools/GuidePopClient;->〇8o8o〇(Landroid/content/Context;Landroid/view/View;)V

    .line 63
    .line 64
    .line 65
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

.method public 〇〇〇00()V
    .locals 1

    .line 1
    const/16 v0, 0xc9

    .line 2
    .line 3
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->〇8〇OOoooo(I)Z

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    return-void

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/SecurityMarkFragment;->o〇00O:Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;

    .line 11
    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->〇o00〇〇Oo()V

    .line 13
    .line 14
    .line 15
    return-void
.end method

.class Lcom/intsig/camscanner/securitymark/SecurityOperationFactory$ShareOperation;
.super Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;
.source "SecurityOperationFactory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/securitymark/SecurityOperationFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ShareOperation"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .line 2
    invoke-direct {p0}, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LO〇8/〇〇888;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/securitymark/SecurityOperationFactory$ShareOperation;-><init>()V

    return-void
.end method


# virtual methods
.method public 〇o00〇〇Oo()V
    .locals 4

    .line 1
    const-string v0, "ShareOperation clickComplete"

    .line 2
    .line 3
    const-string v1, "SecurityOperationFactory"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->O8:Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;

    .line 9
    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    const-string v0, "securityImageData == null"

    .line 13
    .line 14
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    return-void

    .line 18
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->〇o〇:Lcom/intsig/camscanner/share/ShareHelper;

    .line 19
    .line 20
    if-nez v0, :cond_1

    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentActivity;

    .line 23
    .line 24
    invoke-static {v0}, Lcom/intsig/camscanner/share/ShareHelper;->ooo8o〇o〇(Landroidx/fragment/app/FragmentActivity;)Lcom/intsig/camscanner/share/ShareHelper;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    iput-object v0, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->〇o〇:Lcom/intsig/camscanner/share/ShareHelper;

    .line 29
    .line 30
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->O8:Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;

    .line 31
    .line 32
    invoke-virtual {v0}, Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;->〇80〇808〇O()Z

    .line 33
    .line 34
    .line 35
    move-result v0

    .line 36
    if-eqz v0, :cond_2

    .line 37
    .line 38
    new-instance v0, Lcom/intsig/camscanner/share/data_mode/SecurityImageShareData;

    .line 39
    .line 40
    iget-object v1, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentActivity;

    .line 41
    .line 42
    iget-object v2, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->〇080:Lcom/intsig/camscanner/securitymark/presenter/SecurityMarkPresenter;

    .line 43
    .line 44
    iget-object v3, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->O8:Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;

    .line 45
    .line 46
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/camscanner/share/data_mode/SecurityImageShareData;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/securitymark/contact/SecurityMarkContract$Presenter;Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;)V

    .line 47
    .line 48
    .line 49
    new-instance v1, Lcom/intsig/camscanner/share/type/ImageShare;

    .line 50
    .line 51
    iget-object v2, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentActivity;

    .line 52
    .line 53
    invoke-direct {v1, v2, v0}, Lcom/intsig/camscanner/share/type/ImageShare;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/share/data_mode/IShareData;)V

    .line 54
    .line 55
    .line 56
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->〇o〇:Lcom/intsig/camscanner/share/ShareHelper;

    .line 57
    .line 58
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 59
    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_2
    new-instance v0, Lcom/intsig/camscanner/share/data_mode/SecurityPdfShareData;

    .line 63
    .line 64
    iget-object v1, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentActivity;

    .line 65
    .line 66
    iget-object v2, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->〇080:Lcom/intsig/camscanner/securitymark/presenter/SecurityMarkPresenter;

    .line 67
    .line 68
    iget-object v3, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->O8:Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;

    .line 69
    .line 70
    invoke-direct {v0, v1, v2, v3}, Lcom/intsig/camscanner/share/data_mode/SecurityPdfShareData;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/securitymark/contact/SecurityMarkContract$Presenter;Lcom/intsig/camscanner/securitymark/mode/SecurityImageData;)V

    .line 71
    .line 72
    .line 73
    new-instance v1, Lcom/intsig/camscanner/share/type/PdfShare;

    .line 74
    .line 75
    iget-object v2, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->〇o00〇〇Oo:Landroidx/fragment/app/FragmentActivity;

    .line 76
    .line 77
    invoke-direct {v1, v2, v0}, Lcom/intsig/camscanner/share/type/PdfShare;-><init>(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/share/data_mode/IShareData;)V

    .line 78
    .line 79
    .line 80
    iget-object v0, p0, Lcom/intsig/camscanner/securitymark/AbsSecurityMarkOperation;->〇o〇:Lcom/intsig/camscanner/share/ShareHelper;

    .line 81
    .line 82
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/share/ShareHelper;->OO0o〇〇(Lcom/intsig/camscanner/share/type/BaseShare;)V

    .line 83
    .line 84
    .line 85
    :goto_0
    return-void
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

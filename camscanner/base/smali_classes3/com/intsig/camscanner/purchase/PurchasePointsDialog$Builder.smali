.class public Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;
.super Ljava/lang/Object;
.source "PurchasePointsDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/purchase/PurchasePointsDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private O8:Z

.field private Oo08:I

.field private oO80:I

.field private o〇0:I

.field private 〇080:Landroid/app/Activity;

.field private 〇80〇808〇O:Z

.field private 〇o00〇〇Oo:Ljava/lang/String;

.field private 〇o〇:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

.field private 〇〇888:Lcom/intsig/camscanner/purchase/PurchasePointsDialog$PurchaseCallback;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->O8:Z

    .line 6
    .line 7
    const/4 v1, -0x1

    .line 8
    iput v1, p0, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->o〇0:I

    .line 9
    .line 10
    iput v0, p0, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->oO80:I

    .line 11
    .line 12
    const/4 v0, 0x1

    .line 13
    iput-boolean v0, p0, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->〇80〇808〇O:Z

    .line 14
    .line 15
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->〇080:Landroid/app/Activity;

    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic O8(Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->〇80〇808〇O:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic Oo08(Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;)Lcom/intsig/camscanner/purchase/PurchasePointsDialog$PurchaseCallback;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->〇〇888:Lcom/intsig/camscanner/purchase/PurchasePointsDialog$PurchaseCallback;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic o〇0(Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->o〇0:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->Oo08:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->〇o00〇〇Oo:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->O8:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic 〇〇888(Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->oO80:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public OO0o〇〇(I)Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->o〇0:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public OO0o〇〇〇〇0(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->〇o00〇〇Oo:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public Oooo8o0〇(I)Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->oO80:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public oO80(I)Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->Oo08:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public 〇80〇808〇O(Z)Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->〇80〇808〇O:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public 〇8o8o〇(Lcom/intsig/camscanner/purchase/PurchasePointsDialog$PurchaseCallback;)Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->〇〇888:Lcom/intsig/camscanner/purchase/PurchasePointsDialog$PurchaseCallback;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public 〇O8o08O(Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->〇o〇:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public 〇〇808〇()Lcom/intsig/camscanner/purchase/PurchasePointsDialog;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/purchase/PurchasePointsDialog;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->〇080:Landroid/app/Activity;

    .line 4
    .line 5
    iget-object v2, p0, Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;->〇o〇:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 6
    .line 7
    invoke-direct {v0, v1, p0, v2}, Lcom/intsig/camscanner/purchase/PurchasePointsDialog;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/PurchasePointsDialog$Builder;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/PurchasePointsDialog;->show()V

    .line 11
    .line 12
    .line 13
    return-object v0
    .line 14
    .line 15
.end method

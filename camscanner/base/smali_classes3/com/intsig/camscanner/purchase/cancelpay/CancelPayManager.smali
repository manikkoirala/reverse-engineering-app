.class public final Lcom/intsig/camscanner/purchase/cancelpay/CancelPayManager;
.super Ljava/lang/Object;
.source "CancelPayManager.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080:Lcom/intsig/camscanner/purchase/cancelpay/CancelPayManager;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field static final synthetic 〇o00〇〇Oo:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final 〇o〇:Lcom/intsig/camscanner/delegate/sp/BaseSharedPreferencesDelegate;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/MutablePropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mCancelPayTimes"

    .line 7
    .line 8
    const-string v3, "getMCancelPayTimes()I"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/purchase/cancelpay/CancelPayManager;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/MutablePropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->Oo08(Lkotlin/jvm/internal/MutablePropertyReference1;)Lkotlin/reflect/KMutableProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/purchase/cancelpay/CancelPayManager;->〇o00〇〇Oo:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/purchase/cancelpay/CancelPayManager;

    .line 25
    .line 26
    invoke-direct {v0}, Lcom/intsig/camscanner/purchase/cancelpay/CancelPayManager;-><init>()V

    .line 27
    .line 28
    .line 29
    sput-object v0, Lcom/intsig/camscanner/purchase/cancelpay/CancelPayManager;->〇080:Lcom/intsig/camscanner/purchase/cancelpay/CancelPayManager;

    .line 30
    .line 31
    const-string v1, "KEY_CANCEL_PAY_TIMES_651"

    .line 32
    .line 33
    const/4 v2, 0x0

    .line 34
    const/4 v3, 0x0

    .line 35
    const/4 v4, 0x0

    .line 36
    const/4 v5, 0x6

    .line 37
    const/4 v6, 0x0

    .line 38
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/delegate/sp/SpDelegateUtilKt;->o〇0(Ljava/lang/String;IZZILjava/lang/Object;)Lcom/intsig/camscanner/delegate/sp/BaseSharedPreferencesDelegate;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    sput-object v0, Lcom/intsig/camscanner/purchase/cancelpay/CancelPayManager;->〇o〇:Lcom/intsig/camscanner/delegate/sp/BaseSharedPreferencesDelegate;

    .line 43
    .line 44
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private final O8(I)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/purchase/cancelpay/CancelPayManager;->〇o〇:Lcom/intsig/camscanner/delegate/sp/BaseSharedPreferencesDelegate;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/purchase/cancelpay/CancelPayManager;->〇o00〇〇Oo:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    invoke-virtual {v0, p0, v1, p1}, Lcom/intsig/camscanner/delegate/sp/BaseSharedPreferencesDelegate;->〇080(Ljava/lang/Object;Lkotlin/reflect/KProperty;Ljava/lang/Object;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static final 〇080()I
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/purchase/cancelpay/CancelPayManager;->〇080:Lcom/intsig/camscanner/purchase/cancelpay/CancelPayManager;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/purchase/cancelpay/CancelPayManager;->〇o00〇〇Oo()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    return v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private final 〇o00〇〇Oo()I
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/purchase/cancelpay/CancelPayManager;->〇o〇:Lcom/intsig/camscanner/delegate/sp/BaseSharedPreferencesDelegate;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/purchase/cancelpay/CancelPayManager;->〇o00〇〇Oo:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/camscanner/delegate/sp/BaseSharedPreferencesDelegate;->〇o00〇〇Oo(Ljava/lang/Object;Lkotlin/reflect/KProperty;)Ljava/lang/Object;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Ljava/lang/Number;

    .line 13
    .line 14
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    return v0
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static final 〇o〇(ZLcom/intsig/comm/purchase/entity/ProductResultItem;)V
    .locals 0

    .line 1
    if-nez p0, :cond_2

    .line 2
    .line 3
    if-eqz p1, :cond_0

    .line 4
    .line 5
    iget-object p0, p1, Lcom/intsig/comm/purchase/entity/ProductResultItem;->propertyId:Ljava/lang/String;

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 p0, 0x0

    .line 9
    :goto_0
    invoke-static {p0}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->o〇〇0〇(Ljava/lang/String;)Z

    .line 10
    .line 11
    .line 12
    move-result p0

    .line 13
    if-nez p0, :cond_1

    .line 14
    .line 15
    goto :goto_1

    .line 16
    :cond_1
    sget-object p0, Lcom/intsig/camscanner/purchase/cancelpay/CancelPayManager;->〇080:Lcom/intsig/camscanner/purchase/cancelpay/CancelPayManager;

    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/cancelpay/CancelPayManager;->〇o00〇〇Oo()I

    .line 19
    .line 20
    .line 21
    move-result p1

    .line 22
    add-int/lit8 p1, p1, 0x1

    .line 23
    .line 24
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/purchase/cancelpay/CancelPayManager;->O8(I)V

    .line 25
    .line 26
    .line 27
    :cond_2
    :goto_1
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

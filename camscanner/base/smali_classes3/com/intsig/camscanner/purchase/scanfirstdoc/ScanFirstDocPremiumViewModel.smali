.class public final Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocPremiumViewModel;
.super Landroidx/lifecycle/AndroidViewModel;
.source "ScanFirstDocPremiumViewModel.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final o0:Landroid/app/Application;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇OOo8〇0:Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Application;)V
    .locals 1
    .param p1    # Landroid/app/Application;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "app"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1}, Landroidx/lifecycle/AndroidViewModel;-><init>(Landroid/app/Application;)V

    .line 7
    .line 8
    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocPremiumViewModel;->o0:Landroid/app/Application;

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;

    .line 12
    .line 13
    invoke-direct {v0, p1}, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;-><init>(Landroid/app/Application;)V

    .line 14
    .line 15
    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocPremiumViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;

    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public final 〇80〇808〇O()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/purchase/scanfirstdoc/entity/IScanFirstDocType;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    new-instance v1, Lcom/intsig/camscanner/purchase/scanfirstdoc/entity/ScanFirstDocDefaultType;

    .line 7
    .line 8
    const/4 v2, 0x0

    .line 9
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/purchase/scanfirstdoc/entity/ScanFirstDocDefaultType;-><init>(I)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 13
    .line 14
    .line 15
    new-instance v1, Lcom/intsig/camscanner/purchase/scanfirstdoc/entity/ScanFirstDocDefaultType;

    .line 16
    .line 17
    const/4 v2, 0x1

    .line 18
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/purchase/scanfirstdoc/entity/ScanFirstDocDefaultType;-><init>(I)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 22
    .line 23
    .line 24
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocPremiumViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;

    .line 25
    .line 26
    const v2, 0x7f1303bc

    .line 27
    .line 28
    .line 29
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;->o〇0(I)Lcom/intsig/camscanner/purchase/scanfirstdoc/entity/ScanFirstDocTitleType;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    .line 35
    .line 36
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocPremiumViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;

    .line 37
    .line 38
    invoke-virtual {v1}, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;->〇o〇()Lcom/intsig/camscanner/purchase/scanfirstdoc/entity/ScanFirstDocFunctionLineType;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    .line 44
    .line 45
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocPremiumViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;

    .line 46
    .line 47
    invoke-virtual {v1}, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;->O8()Lcom/intsig/camscanner/purchase/scanfirstdoc/entity/ScanFirstDocFunctionLineType;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    .line 53
    .line 54
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocPremiumViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;

    .line 55
    .line 56
    const v2, 0x7f130d09

    .line 57
    .line 58
    .line 59
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;->o〇0(I)Lcom/intsig/camscanner/purchase/scanfirstdoc/entity/ScanFirstDocTitleType;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    .line 65
    .line 66
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocPremiumViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;

    .line 67
    .line 68
    invoke-virtual {v1}, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;->〇〇888()Lcom/intsig/camscanner/purchase/scanfirstdoc/entity/ScanFirstDocFunctionLineType;

    .line 69
    .line 70
    .line 71
    move-result-object v1

    .line 72
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    .line 74
    .line 75
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocPremiumViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;

    .line 76
    .line 77
    invoke-virtual {v1}, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;->oO80()Lcom/intsig/camscanner/purchase/scanfirstdoc/entity/ScanFirstDocFunctionLineType;

    .line 78
    .line 79
    .line 80
    move-result-object v1

    .line 81
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    .line 83
    .line 84
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocPremiumViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;

    .line 85
    .line 86
    const v2, 0x7f1309a1

    .line 87
    .line 88
    .line 89
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;->o〇0(I)Lcom/intsig/camscanner/purchase/scanfirstdoc/entity/ScanFirstDocTitleType;

    .line 90
    .line 91
    .line 92
    move-result-object v1

    .line 93
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    .line 95
    .line 96
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocPremiumViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;

    .line 97
    .line 98
    invoke-virtual {v1}, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;->〇080()Lcom/intsig/camscanner/purchase/scanfirstdoc/entity/ScanFirstDocFunctionLineType;

    .line 99
    .line 100
    .line 101
    move-result-object v1

    .line 102
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    .line 104
    .line 105
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocPremiumViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;

    .line 106
    .line 107
    invoke-virtual {v1}, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;->〇o00〇〇Oo()Lcom/intsig/camscanner/purchase/scanfirstdoc/entity/ScanFirstDocFunctionLineType;

    .line 108
    .line 109
    .line 110
    move-result-object v1

    .line 111
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    .line 113
    .line 114
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocPremiumViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;

    .line 115
    .line 116
    const v2, 0x7f130c49

    .line 117
    .line 118
    .line 119
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;->o〇0(I)Lcom/intsig/camscanner/purchase/scanfirstdoc/entity/ScanFirstDocTitleType;

    .line 120
    .line 121
    .line 122
    move-result-object v1

    .line 123
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    .line 125
    .line 126
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocPremiumViewModel;->〇OOo8〇0:Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;

    .line 127
    .line 128
    invoke-virtual {v1}, Lcom/intsig/camscanner/purchase/scanfirstdoc/ScanFirstDocRepo;->Oo08()Lcom/intsig/camscanner/purchase/scanfirstdoc/entity/ScanFirstDocFunctionLineType;

    .line 129
    .line 130
    .line 131
    move-result-object v1

    .line 132
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    .line 134
    .line 135
    return-object v0
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

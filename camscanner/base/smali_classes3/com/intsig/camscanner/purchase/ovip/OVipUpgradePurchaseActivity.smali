.class public final Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;
.super Lcom/intsig/mvp/activity/BaseChangeActivity;
.source "OVipUpgradePurchaseActivity.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final O88O:Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field static final synthetic oOO〇〇:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private O0O:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

.field private o8oOOo:I

.field private final ooo0〇〇O:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇O〇〇O8:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o0O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇〇08O:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->oOO〇〇:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->O88O:Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity$Companion;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;

    .line 7
    .line 8
    invoke-direct {v0, v1, p0}, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;-><init>(Ljava/lang/Class;Landroid/app/Activity;)V

    .line 9
    .line 10
    .line 11
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->ooo0〇〇O:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 12
    .line 13
    const/4 v0, 0x2

    .line 14
    iput v0, p0, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o8oOOo:I

    .line 15
    .line 16
    sget-object v0, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 17
    .line 18
    new-instance v1, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity$mFrom$2;

    .line 19
    .line 20
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity$mFrom$2;-><init>(Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;)V

    .line 21
    .line 22
    .line 23
    invoke-static {v0, v1}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    iput-object v1, p0, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->〇O〇〇O8:Lkotlin/Lazy;

    .line 28
    .line 29
    new-instance v1, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity$mTracker$2;

    .line 30
    .line 31
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity$mTracker$2;-><init>(Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;)V

    .line 32
    .line 33
    .line 34
    invoke-static {v0, v1}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->〇o0O:Lkotlin/Lazy;

    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static synthetic O0〇(Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->OooO〇(Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final O880O〇()V
    .locals 8

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o0Oo()Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;->o8oOOo:Landroidx/appcompat/widget/AppCompatTextView;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->OO0O()I

    .line 12
    .line 13
    .line 14
    move-result v1

    .line 15
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 16
    .line 17
    .line 18
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o0Oo()Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    const/4 v1, 0x0

    .line 23
    if-eqz v0, :cond_1

    .line 24
    .line 25
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;->〇〇08O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_1
    move-object v0, v1

    .line 29
    :goto_0
    const/4 v2, 0x1

    .line 30
    const/4 v3, 0x0

    .line 31
    if-nez v0, :cond_2

    .line 32
    .line 33
    goto :goto_1

    .line 34
    :cond_2
    new-instance v4, Landroid/text/SpannableString;

    .line 35
    .line 36
    const-string/jumbo v5, "\uffe570"

    .line 37
    .line 38
    .line 39
    invoke-direct {v4, v5}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 40
    .line 41
    .line 42
    new-instance v5, Landroid/text/style/AbsoluteSizeSpan;

    .line 43
    .line 44
    sget-object v6, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 45
    .line 46
    invoke-virtual {v6}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 47
    .line 48
    .line 49
    move-result-object v6

    .line 50
    const/16 v7, 0x10

    .line 51
    .line 52
    invoke-static {v6, v7}, Lcom/intsig/utils/DisplayUtil;->〇〇8O0〇8(Landroid/content/Context;I)I

    .line 53
    .line 54
    .line 55
    move-result v6

    .line 56
    invoke-direct {v5, v6}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    .line 57
    .line 58
    .line 59
    const/16 v6, 0x21

    .line 60
    .line 61
    invoke-virtual {v4, v5, v3, v2, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    .line 66
    .line 67
    :goto_1
    new-instance v0, Ljava/text/SimpleDateFormat;

    .line 68
    .line 69
    const-string/jumbo v4, "yyyy-MM-dd"

    .line 70
    .line 71
    .line 72
    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    .line 73
    .line 74
    invoke-direct {v0, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 75
    .line 76
    .line 77
    new-instance v4, Ljava/util/Date;

    .line 78
    .line 79
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OO〇0008O8()J

    .line 80
    .line 81
    .line 82
    move-result-wide v5

    .line 83
    invoke-direct {v4, v5, v6}, Ljava/util/Date;-><init>(J)V

    .line 84
    .line 85
    .line 86
    invoke-virtual {v0, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    .line 87
    .line 88
    .line 89
    move-result-object v0

    .line 90
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o0Oo()Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;

    .line 91
    .line 92
    .line 93
    move-result-object v4

    .line 94
    if-eqz v4, :cond_3

    .line 95
    .line 96
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;->O0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 97
    .line 98
    goto :goto_2

    .line 99
    :cond_3
    move-object v4, v1

    .line 100
    :goto_2
    if-nez v4, :cond_4

    .line 101
    .line 102
    goto :goto_3

    .line 103
    :cond_4
    const/4 v5, 0x2

    .line 104
    new-array v5, v5, [Ljava/lang/Object;

    .line 105
    .line 106
    aput-object v0, v5, v3

    .line 107
    .line 108
    const-string/jumbo v0, "\uffe5258"

    .line 109
    .line 110
    .line 111
    aput-object v0, v5, v2

    .line 112
    .line 113
    const v0, 0x7f131484

    .line 114
    .line 115
    .line 116
    invoke-virtual {p0, v0, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 117
    .line 118
    .line 119
    move-result-object v0

    .line 120
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    .line 122
    .line 123
    :goto_3
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o0Oo()Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;

    .line 124
    .line 125
    .line 126
    move-result-object v0

    .line 127
    if-eqz v0, :cond_5

    .line 128
    .line 129
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;->o8〇OO0〇0o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 130
    .line 131
    :cond_5
    const-string v0, "#FF212121"

    .line 132
    .line 133
    invoke-static {p0, v1, v0}, Lcom/intsig/camscanner/util/StringUtil;->〇〇888(Landroid/content/Context;Landroid/widget/TextView;Ljava/lang/String;)V

    .line 134
    .line 135
    .line 136
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->OO〇〇o0oO()V

    .line 137
    .line 138
    .line 139
    return-void
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private final OO0O()I
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->〇oO88o()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    const v2, 0x7f131482

    .line 7
    .line 8
    .line 9
    if-eq v0, v1, :cond_2

    .line 10
    .line 11
    const/4 v1, 0x2

    .line 12
    if-eq v0, v1, :cond_1

    .line 13
    .line 14
    const/4 v1, 0x3

    .line 15
    if-eq v0, v1, :cond_0

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    const v2, 0x7f131481

    .line 19
    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_1
    const v2, 0x7f131483

    .line 23
    .line 24
    .line 25
    :cond_2
    :goto_0
    return v2
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final OO〇〇o0oO()V
    .locals 7

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->office_member_vip:Lcom/intsig/comm/purchase/entity/QueryProductsResult$OfficeMemberVipInfo;

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$OfficeMemberVipInfo;->up_product_id:Ljava/lang/String;

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    move-object v0, v1

    .line 18
    :goto_0
    const-string v2, ""

    .line 19
    .line 20
    if-nez v0, :cond_1

    .line 21
    .line 22
    move-object v0, v2

    .line 23
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O0oO0()I

    .line 24
    .line 25
    .line 26
    move-result v3

    .line 27
    add-int/lit8 v3, v3, 0x1

    .line 28
    .line 29
    invoke-static {v3}, Lcom/intsig/camscanner/util/PreferenceHelper;->O0o0O0(I)V

    .line 30
    .line 31
    .line 32
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    .line 33
    .line 34
    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 35
    .line 36
    .line 37
    const-string v5, "scheme"

    .line 38
    .line 39
    sget-object v6, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->MAIN_NORMAL:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 40
    .line 41
    invoke-virtual {v6}, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->toTrackerValue()Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v6

    .line 45
    invoke-virtual {v4, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 46
    .line 47
    .line 48
    const-string/jumbo v5, "times"

    .line 49
    .line 50
    .line 51
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v3

    .line 55
    invoke-virtual {v4, v5, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 56
    .line 57
    .line 58
    const-string v3, "product_id"

    .line 59
    .line 60
    invoke-virtual {v4, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 61
    .line 62
    .line 63
    const-string/jumbo v0, "user_type"

    .line 64
    .line 65
    .line 66
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o〇08oO80o()Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object v3

    .line 70
    invoke-virtual {v4, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 71
    .line 72
    .line 73
    const-string v0, "page_type"

    .line 74
    .line 75
    const-string/jumbo v3, "work_function_upgrade"

    .line 76
    .line 77
    .line 78
    invoke-virtual {v4, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 79
    .line 80
    .line 81
    const-string v0, "from"

    .line 82
    .line 83
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o808o8o08()Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 84
    .line 85
    .line 86
    move-result-object v3

    .line 87
    if-eqz v3, :cond_2

    .line 88
    .line 89
    iget-object v3, v3, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 90
    .line 91
    if-eqz v3, :cond_2

    .line 92
    .line 93
    invoke-virtual {v3}, Lcom/intsig/camscanner/purchase/entity/Function;->toTrackerValue()Ljava/lang/String;

    .line 94
    .line 95
    .line 96
    move-result-object v3

    .line 97
    goto :goto_1

    .line 98
    :cond_2
    move-object v3, v1

    .line 99
    :goto_1
    if-nez v3, :cond_3

    .line 100
    .line 101
    move-object v3, v2

    .line 102
    :cond_3
    invoke-virtual {v4, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 103
    .line 104
    .line 105
    const-string v0, "from_part"

    .line 106
    .line 107
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o808o8o08()Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 108
    .line 109
    .line 110
    move-result-object v3

    .line 111
    if-eqz v3, :cond_4

    .line 112
    .line 113
    iget-object v3, v3, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 114
    .line 115
    if-eqz v3, :cond_4

    .line 116
    .line 117
    invoke-virtual {v3}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 118
    .line 119
    .line 120
    move-result-object v1

    .line 121
    :cond_4
    if-nez v1, :cond_5

    .line 122
    .line 123
    goto :goto_2

    .line 124
    :cond_5
    move-object v2, v1

    .line 125
    :goto_2
    invoke-virtual {v4, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 126
    .line 127
    .line 128
    const-string v0, "CSPremiumPop"

    .line 129
    .line 130
    invoke-static {v0, v4}, Lcom/intsig/camscanner/log/LogAgentData;->〇O00(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    .line 132
    .line 133
    goto :goto_3

    .line 134
    :catch_0
    move-exception v0

    .line 135
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 136
    .line 137
    .line 138
    :goto_3
    return-void
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private static final OooO〇(Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    .line 2
    .line 3
    new-instance p0, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v0, "purchase end: "

    .line 9
    .line 10
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p0

    .line 20
    const-string p1, "OVipUpgradePurchaseActivity"

    .line 21
    .line 22
    invoke-static {p1, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    :cond_0
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final O〇080〇o0(Z)V
    .locals 3

    .line 1
    const/4 v0, 0x1

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    const/4 v1, 0x2

    .line 5
    goto :goto_0

    .line 6
    :cond_0
    const/4 v1, 0x1

    .line 7
    :goto_0
    iput v1, p0, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o8oOOo:I

    .line 8
    .line 9
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o0Oo()Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    const/4 v2, 0x0

    .line 14
    if-eqz v1, :cond_1

    .line 15
    .line 16
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;->oOo〇8o008:Landroid/widget/RadioButton;

    .line 17
    .line 18
    goto :goto_1

    .line 19
    :cond_1
    move-object v1, v2

    .line 20
    :goto_1
    if-nez v1, :cond_2

    .line 21
    .line 22
    goto :goto_2

    .line 23
    :cond_2
    invoke-virtual {v1, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 24
    .line 25
    .line 26
    :goto_2
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o0Oo()Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    if-eqz v1, :cond_3

    .line 31
    .line 32
    iget-object v2, v1, Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;->〇0O:Landroid/widget/RadioButton;

    .line 33
    .line 34
    :cond_3
    if-nez v2, :cond_4

    .line 35
    .line 36
    goto :goto_3

    .line 37
    :cond_4
    xor-int/2addr p1, v0

    .line 38
    invoke-virtual {v2, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 39
    .line 40
    .line 41
    :goto_3
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private final O〇0O〇Oo〇o()V
    .locals 7

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->office_member_vip:Lcom/intsig/comm/purchase/entity/QueryProductsResult$OfficeMemberVipInfo;

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$OfficeMemberVipInfo;->up_product_id:Ljava/lang/String;

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    move-object v0, v1

    .line 18
    :goto_0
    const-string v2, ""

    .line 19
    .line 20
    if-nez v0, :cond_1

    .line 21
    .line 22
    move-object v0, v2

    .line 23
    :cond_1
    const/4 v3, 0x6

    .line 24
    new-array v3, v3, [Landroid/util/Pair;

    .line 25
    .line 26
    new-instance v4, Landroid/util/Pair;

    .line 27
    .line 28
    sget-object v5, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->MAIN_NORMAL:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 29
    .line 30
    invoke-virtual {v5}, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->toTrackerValue()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v5

    .line 34
    const-string v6, "scheme"

    .line 35
    .line 36
    invoke-direct {v4, v6, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 37
    .line 38
    .line 39
    const/4 v5, 0x0

    .line 40
    aput-object v4, v3, v5

    .line 41
    .line 42
    new-instance v4, Landroid/util/Pair;

    .line 43
    .line 44
    const-string v5, "product_id"

    .line 45
    .line 46
    invoke-direct {v4, v5, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 47
    .line 48
    .line 49
    const/4 v0, 0x1

    .line 50
    aput-object v4, v3, v0

    .line 51
    .line 52
    new-instance v0, Landroid/util/Pair;

    .line 53
    .line 54
    const-string/jumbo v4, "user_type"

    .line 55
    .line 56
    .line 57
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o〇08oO80o()Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object v5

    .line 61
    invoke-direct {v0, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 62
    .line 63
    .line 64
    const/4 v4, 0x2

    .line 65
    aput-object v0, v3, v4

    .line 66
    .line 67
    new-instance v0, Landroid/util/Pair;

    .line 68
    .line 69
    const-string v4, "page_type"

    .line 70
    .line 71
    const-string/jumbo v5, "work_function_upgrade"

    .line 72
    .line 73
    .line 74
    invoke-direct {v0, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 75
    .line 76
    .line 77
    const/4 v4, 0x3

    .line 78
    aput-object v0, v3, v4

    .line 79
    .line 80
    new-instance v0, Landroid/util/Pair;

    .line 81
    .line 82
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o808o8o08()Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 83
    .line 84
    .line 85
    move-result-object v4

    .line 86
    if-eqz v4, :cond_2

    .line 87
    .line 88
    iget-object v4, v4, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 89
    .line 90
    if-eqz v4, :cond_2

    .line 91
    .line 92
    invoke-virtual {v4}, Lcom/intsig/camscanner/purchase/entity/Function;->toTrackerValue()Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object v4

    .line 96
    goto :goto_1

    .line 97
    :cond_2
    move-object v4, v1

    .line 98
    :goto_1
    if-nez v4, :cond_3

    .line 99
    .line 100
    move-object v4, v2

    .line 101
    :cond_3
    const-string v5, "from"

    .line 102
    .line 103
    invoke-direct {v0, v5, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 104
    .line 105
    .line 106
    const/4 v4, 0x4

    .line 107
    aput-object v0, v3, v4

    .line 108
    .line 109
    new-instance v0, Landroid/util/Pair;

    .line 110
    .line 111
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o808o8o08()Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 112
    .line 113
    .line 114
    move-result-object v4

    .line 115
    if-eqz v4, :cond_4

    .line 116
    .line 117
    iget-object v4, v4, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 118
    .line 119
    if-eqz v4, :cond_4

    .line 120
    .line 121
    invoke-virtual {v4}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 122
    .line 123
    .line 124
    move-result-object v1

    .line 125
    :cond_4
    if-nez v1, :cond_5

    .line 126
    .line 127
    goto :goto_2

    .line 128
    :cond_5
    move-object v2, v1

    .line 129
    :goto_2
    const-string v1, "from_part"

    .line 130
    .line 131
    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 132
    .line 133
    .line 134
    const/4 v1, 0x5

    .line 135
    aput-object v0, v3, v1

    .line 136
    .line 137
    const-string v0, "CSPremiumPop"

    .line 138
    .line 139
    const-string v1, "close"

    .line 140
    .line 141
    invoke-static {v0, v1, v3}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 142
    .line 143
    .line 144
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 145
    .line 146
    .line 147
    return-void
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private final O〇〇O80o8()V
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o0Oo()Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;->〇OOo8〇0:Landroid/widget/CheckBox;

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/4 v0, 0x0

    .line 18
    :goto_0
    if-nez v0, :cond_1

    .line 19
    .line 20
    const v0, 0x7f1311b8

    .line 21
    .line 22
    .line 23
    invoke-static {p0, v0}, Lcom/intsig/utils/ToastUtils;->O8(Landroid/content/Context;I)V

    .line 24
    .line 25
    .line 26
    return-void

    .line 27
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->office_member_vip:Lcom/intsig/comm/purchase/entity/QueryProductsResult$OfficeMemberVipInfo;

    .line 36
    .line 37
    const/4 v2, 0x0

    .line 38
    if-eqz v0, :cond_2

    .line 39
    .line 40
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$OfficeMemberVipInfo;->up_product_id:Ljava/lang/String;

    .line 41
    .line 42
    goto :goto_1

    .line 43
    :cond_2
    move-object v0, v2

    .line 44
    :goto_1
    iget v3, p0, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o8oOOo:I

    .line 45
    .line 46
    new-instance v4, Ljava/lang/StringBuilder;

    .line 47
    .line 48
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 49
    .line 50
    .line 51
    const-string/jumbo v5, "start purchase, productId: "

    .line 52
    .line 53
    .line 54
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 55
    .line 56
    .line 57
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    .line 59
    .line 60
    const-string v5, ", payType: "

    .line 61
    .line 62
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    .line 64
    .line 65
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 66
    .line 67
    .line 68
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object v3

    .line 72
    const-string v4, "OVipUpgradePurchaseActivity"

    .line 73
    .line 74
    invoke-static {v4, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    const/4 v3, 0x1

    .line 78
    if-eqz v0, :cond_3

    .line 79
    .line 80
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 81
    .line 82
    .line 83
    move-result v4

    .line 84
    if-nez v4, :cond_4

    .line 85
    .line 86
    :cond_3
    const/4 v1, 0x1

    .line 87
    :cond_4
    if-eqz v1, :cond_5

    .line 88
    .line 89
    return-void

    .line 90
    :cond_5
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->O0O:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 91
    .line 92
    if-eqz v1, :cond_8

    .line 93
    .line 94
    iget-object v4, p0, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->〇〇08O:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 95
    .line 96
    const-string v5, "mPurchaseTracker"

    .line 97
    .line 98
    if-nez v4, :cond_6

    .line 99
    .line 100
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 101
    .line 102
    .line 103
    move-object v4, v2

    .line 104
    :cond_6
    iput-object v0, v4, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->productId:Ljava/lang/String;

    .line 105
    .line 106
    iget-object v4, p0, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->〇〇08O:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 107
    .line 108
    if-nez v4, :cond_7

    .line 109
    .line 110
    invoke-static {v5}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 111
    .line 112
    .line 113
    goto :goto_2

    .line 114
    :cond_7
    move-object v2, v4

    .line 115
    :goto_2
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->OOO(Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 116
    .line 117
    .line 118
    iget v2, p0, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o8oOOo:I

    .line 119
    .line 120
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->〇80(I)V

    .line 121
    .line 122
    .line 123
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->oO00OOO(I)V

    .line 124
    .line 125
    .line 126
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->O0O8OO088(Ljava/lang/String;)V

    .line 127
    .line 128
    .line 129
    :cond_8
    return-void
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private final o0Oo()Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->ooo0〇〇O:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->oOO〇〇:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;->〇〇888(Landroid/app/Activity;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
.end method

.method private final o808o8o08()Lcom/intsig/camscanner/purchase/track/PurchaseTracker;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->〇o0O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private final o〇08oO80o()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->〇oO88o()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x1

    .line 6
    if-eq v0, v1, :cond_2

    .line 7
    .line 8
    const/4 v1, 0x2

    .line 9
    if-eq v0, v1, :cond_1

    .line 10
    .line 11
    const/4 v1, 0x3

    .line 12
    if-eq v0, v1, :cond_0

    .line 13
    .line 14
    const-string v0, ""

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const-string v0, "other_payfunction"

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_1
    const-string v0, "reach_upper_limit"

    .line 21
    .line 22
    goto :goto_0

    .line 23
    :cond_2
    const-string v0, "click_mepage"

    .line 24
    .line 25
    :goto_0
    return-object v0
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final o〇o08〇()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o808o8o08()Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    new-instance v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 8
    .line 9
    invoke-direct {v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 10
    .line 11
    .line 12
    :cond_0
    sget-object v1, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->CSPremiumPop:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId(Lcom/intsig/camscanner/purchase/track/PurchasePageId;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 15
    .line 16
    .line 17
    sget-object v1, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->MAIN_NORMAL:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 18
    .line 19
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme(Lcom/intsig/camscanner/purchase/track/PurchaseScheme;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 20
    .line 21
    .line 22
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o〇08oO80o()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    iput-object v1, v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->user_type:Ljava/lang/String;

    .line 27
    .line 28
    const-string/jumbo v1, "work_function_upgrade"

    .line 29
    .line 30
    .line 31
    iput-object v1, v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->page_type:Ljava/lang/String;

    .line 32
    .line 33
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->〇〇08O:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 34
    .line 35
    new-instance v1, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 36
    .line 37
    invoke-direct {v1, p0, v0}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 38
    .line 39
    .line 40
    iput-object v1, p0, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->O0O:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 41
    .line 42
    new-instance v0, L〇80〇/〇080;

    .line 43
    .line 44
    invoke-direct {v0}, L〇80〇/〇080;-><init>()V

    .line 45
    .line 46
    .line 47
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->〇O〇80o08O(Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient$PurchaseCallback;)V

    .line 48
    .line 49
    .line 50
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final 〇oO88o()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->〇O〇〇O8:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Number;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
.end method


# virtual methods
.method public dealClickAction(Landroid/view/View;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/mvp/activity/BaseChangeActivity;->dealClickAction(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    if-nez p1, :cond_0

    .line 5
    .line 6
    return-void

    .line 7
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o0Oo()Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const/4 v1, 0x1

    .line 16
    const/4 v2, 0x0

    .line 17
    if-eqz v0, :cond_1

    .line 18
    .line 19
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 20
    .line 21
    if-eqz v0, :cond_1

    .line 22
    .line 23
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    .line 24
    .line 25
    .line 26
    move-result v0

    .line 27
    if-ne p1, v0, :cond_1

    .line 28
    .line 29
    const/4 v0, 0x1

    .line 30
    goto :goto_0

    .line 31
    :cond_1
    const/4 v0, 0x0

    .line 32
    :goto_0
    if-eqz v0, :cond_2

    .line 33
    .line 34
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->O〇0O〇Oo〇o()V

    .line 35
    .line 36
    .line 37
    goto :goto_4

    .line 38
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o0Oo()Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    if-eqz v0, :cond_3

    .line 43
    .line 44
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;->〇8〇oO〇〇8o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 45
    .line 46
    if-eqz v0, :cond_3

    .line 47
    .line 48
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    .line 49
    .line 50
    .line 51
    move-result v0

    .line 52
    if-ne p1, v0, :cond_3

    .line 53
    .line 54
    const/4 v0, 0x1

    .line 55
    goto :goto_1

    .line 56
    :cond_3
    const/4 v0, 0x0

    .line 57
    :goto_1
    if-eqz v0, :cond_4

    .line 58
    .line 59
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->O〇〇O80o8()V

    .line 60
    .line 61
    .line 62
    goto :goto_4

    .line 63
    :cond_4
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o0Oo()Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    if-eqz v0, :cond_5

    .line 68
    .line 69
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;->〇080OO8〇0:Landroid/widget/LinearLayout;

    .line 70
    .line 71
    if-eqz v0, :cond_5

    .line 72
    .line 73
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    .line 74
    .line 75
    .line 76
    move-result v0

    .line 77
    if-ne p1, v0, :cond_5

    .line 78
    .line 79
    const/4 v0, 0x1

    .line 80
    goto :goto_2

    .line 81
    :cond_5
    const/4 v0, 0x0

    .line 82
    :goto_2
    if-eqz v0, :cond_6

    .line 83
    .line 84
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->O〇080〇o0(Z)V

    .line 85
    .line 86
    .line 87
    goto :goto_4

    .line 88
    :cond_6
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o0Oo()Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;

    .line 89
    .line 90
    .line 91
    move-result-object v0

    .line 92
    if-eqz v0, :cond_7

    .line 93
    .line 94
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;->O8o08O8O:Landroid/widget/LinearLayout;

    .line 95
    .line 96
    if-eqz v0, :cond_7

    .line 97
    .line 98
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    .line 99
    .line 100
    .line 101
    move-result v0

    .line 102
    if-ne p1, v0, :cond_7

    .line 103
    .line 104
    goto :goto_3

    .line 105
    :cond_7
    const/4 v1, 0x0

    .line 106
    :goto_3
    if-eqz v1, :cond_8

    .line 107
    .line 108
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->O〇080〇o0(Z)V

    .line 109
    .line 110
    .line 111
    :cond_8
    :goto_4
    return-void
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 4

    .line 1
    const p1, 0x7f0602f5

    .line 2
    .line 3
    .line 4
    invoke-static {p0, p1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 5
    .line 6
    .line 7
    move-result p1

    .line 8
    const/4 v0, 0x1

    .line 9
    invoke-static {p0, v0, v0, p1}, Lcom/intsig/utils/StatusBarUtil;->〇o00〇〇Oo(Landroid/app/Activity;ZZI)V

    .line 10
    .line 11
    .line 12
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o〇o08〇()V

    .line 13
    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->O880O〇()V

    .line 16
    .line 17
    .line 18
    const/4 p1, 0x4

    .line 19
    new-array p1, p1, [Landroid/view/View;

    .line 20
    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o0Oo()Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    const/4 v2, 0x0

    .line 26
    if-eqz v1, :cond_0

    .line 27
    .line 28
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;->〇8〇oO〇〇8o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    move-object v1, v2

    .line 32
    :goto_0
    const/4 v3, 0x0

    .line 33
    aput-object v1, p1, v3

    .line 34
    .line 35
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o0Oo()Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;

    .line 36
    .line 37
    .line 38
    move-result-object v1

    .line 39
    if-eqz v1, :cond_1

    .line 40
    .line 41
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 42
    .line 43
    goto :goto_1

    .line 44
    :cond_1
    move-object v1, v2

    .line 45
    :goto_1
    aput-object v1, p1, v0

    .line 46
    .line 47
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o0Oo()Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    if-eqz v0, :cond_2

    .line 52
    .line 53
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;->〇080OO8〇0:Landroid/widget/LinearLayout;

    .line 54
    .line 55
    goto :goto_2

    .line 56
    :cond_2
    move-object v0, v2

    .line 57
    :goto_2
    const/4 v1, 0x2

    .line 58
    aput-object v0, p1, v1

    .line 59
    .line 60
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/ovip/OVipUpgradePurchaseActivity;->o0Oo()Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    if-eqz v0, :cond_3

    .line 65
    .line 66
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/ActivityOvipUpgradePurchaseBinding;->O8o08O8O:Landroid/widget/LinearLayout;

    .line 67
    .line 68
    :cond_3
    const/4 v0, 0x3

    .line 69
    aput-object v2, p1, v0

    .line 70
    .line 71
    invoke-virtual {p0, p1}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇O8〇8O0oO([Landroid/view/View;)V

    .line 72
    .line 73
    .line 74
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method protected onStop()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/appcompat/app/AppCompatActivity;->onStop()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    const v1, 0x7f01000e

    .line 6
    .line 7
    .line 8
    :try_start_0
    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 9
    .line 10
    .line 11
    goto :goto_0

    .line 12
    :catch_0
    move-exception v0

    .line 13
    const-string v1, "OVipUpgradePurchaseActivity"

    .line 14
    .line 15
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public bridge synthetic onToolbarTitleClick(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->Oo08(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public o〇oo()I
    .locals 1

    .line 1
    const v0, 0x7f0d0093

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇0〇0()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇Oo〇O()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

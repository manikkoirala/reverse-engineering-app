.class public final Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;
.super Lcom/intsig/app/BaseDialogFragment;
.source "CsPdfVipBuySuccessDialogFragment.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment$VipRightInfo;,
        Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final O8o08O8O:Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field static final synthetic 〇080OO8〇0:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final o〇00O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08O〇00〇o:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/DialogCsPdfVipBuySuccessBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->〇080OO8〇0:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->O8o08O8O:Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment$Companion;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/app/BaseDialogFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v6, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/DialogCsPdfVipBuySuccessBinding;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x4

    .line 10
    const/4 v5, 0x0

    .line 11
    move-object v0, v6

    .line 12
    move-object v2, p0

    .line 13
    invoke-direct/range {v0 .. v5}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object v6, p0, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->〇08O〇00〇o:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 17
    .line 18
    sget-object v0, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 19
    .line 20
    new-instance v1, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment$mIsLogin$2;

    .line 21
    .line 22
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment$mIsLogin$2;-><init>(Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;)V

    .line 23
    .line 24
    .line 25
    invoke-static {v0, v1}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->o〇00O:Lkotlin/Lazy;

    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final O0〇0()V
    .locals 4

    .line 1
    const-string v0, "CsPdfVipBuySuccessDialogFragment"

    .line 2
    .line 3
    const-string v1, "clickLogin"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string/jumbo v0, "type"

    .line 9
    .line 10
    .line 11
    const-string v1, "register_page"

    .line 12
    .line 13
    const-string v2, "CSUnionPdfVipPop"

    .line 14
    .line 15
    const-string v3, "login_or_register"

    .line 16
    .line 17
    invoke-static {v2, v3, v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireActivity()Landroidx/fragment/app/FragmentActivity;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    invoke-static {v0}, Lcom/intsig/tsapp/account/util/LoginRouteCenter;->oO80(Landroid/content/Context;)V

    .line 25
    .line 26
    .line 27
    const/4 v0, 0x1

    .line 28
    const/4 v1, 0x0

    .line 29
    const/4 v2, 0x0

    .line 30
    invoke-static {p0, v2, v0, v1}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->o〇0〇o(Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;ZILjava/lang/Object;)V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final Ooo8o()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->〇o〇88〇8()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    const-string v0, "got_page"

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const-string v0, "register_page"

    .line 11
    .line 12
    :goto_0
    const-string v1, "CSUnionPdfVipPop"

    .line 13
    .line 14
    const-string/jumbo v2, "type"

    .line 15
    .line 16
    .line 17
    invoke-static {v1, v2, v0}, Lcom/intsig/camscanner/log/LogAgentData;->Oooo8o0〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final o00〇88〇08()V
    .locals 4

    .line 1
    const-string v0, "CsPdfVipBuySuccessDialogFragment"

    .line 2
    .line 3
    const-string v1, "clickKnow"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string/jumbo v0, "type"

    .line 9
    .line 10
    .line 11
    const-string v1, "got_page"

    .line 12
    .line 13
    const-string v2, "CSUnionPdfVipPop"

    .line 14
    .line 15
    const-string v3, "got_it"

    .line 16
    .line 17
    invoke-static {v2, v3, v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    const/4 v0, 0x1

    .line 21
    const/4 v1, 0x0

    .line 22
    const/4 v2, 0x0

    .line 23
    invoke-static {p0, v2, v0, v1}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->o〇0〇o(Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;ZILjava/lang/Object;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final o880()V
    .locals 4

    .line 1
    const-string v0, "CsPdfVipBuySuccessDialogFragment"

    .line 2
    .line 3
    const-string v1, "clickClose"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string/jumbo v0, "type"

    .line 9
    .line 10
    .line 11
    const-string v1, "register_page"

    .line 12
    .line 13
    const-string v2, "CSUnionPdfVipPop"

    .line 14
    .line 15
    const-string v3, "close"

    .line 16
    .line 17
    invoke-static {v2, v3, v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    const/4 v0, 0x1

    .line 21
    const/4 v1, 0x0

    .line 22
    const/4 v2, 0x0

    .line 23
    invoke-static {p0, v2, v0, v1}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->o〇0〇o(Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;ZILjava/lang/Object;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final oOoO8OO〇()V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->〇088O()Lcom/intsig/camscanner/databinding/DialogCsPdfVipBuySuccessBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogCsPdfVipBuySuccessBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/Group;

    .line 8
    .line 9
    const-string v2, "groupLogin"

    .line 10
    .line 11
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    const/4 v2, 0x0

    .line 15
    invoke-static {v1, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 16
    .line 17
    .line 18
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogCsPdfVipBuySuccessBinding;->o〇00O:Landroidx/constraintlayout/widget/Group;

    .line 19
    .line 20
    const-string v3, "groupUnLogin"

    .line 21
    .line 22
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    const/4 v3, 0x1

    .line 26
    invoke-static {v1, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 27
    .line 28
    .line 29
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogCsPdfVipBuySuccessBinding;->o8〇OO0〇0o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 30
    .line 31
    const/high16 v4, 0x41600000    # 14.0f

    .line 32
    .line 33
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextSize(F)V

    .line 34
    .line 35
    .line 36
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogCsPdfVipBuySuccessBinding;->o8〇OO0〇0o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 37
    .line 38
    const v4, 0x7f1315e0

    .line 39
    .line 40
    .line 41
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    .line 42
    .line 43
    .line 44
    const/4 v1, 0x3

    .line 45
    new-array v1, v1, [Landroid/view/View;

    .line 46
    .line 47
    iget-object v4, v0, Lcom/intsig/camscanner/databinding/DialogCsPdfVipBuySuccessBinding;->o8〇OO0〇0o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 48
    .line 49
    aput-object v4, v1, v2

    .line 50
    .line 51
    iget-object v2, v0, Lcom/intsig/camscanner/databinding/DialogCsPdfVipBuySuccessBinding;->〇8〇oO〇〇8o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 52
    .line 53
    aput-object v2, v1, v3

    .line 54
    .line 55
    const/4 v2, 0x2

    .line 56
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCsPdfVipBuySuccessBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 57
    .line 58
    aput-object v0, v1, v2

    .line 59
    .line 60
    invoke-virtual {p0, v1}, Lcom/intsig/app/BaseDialogFragment;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 61
    .line 62
    .line 63
    :cond_0
    return-void
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method static synthetic o〇0〇o(Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    const/4 p3, 0x1

    .line 2
    and-int/2addr p2, p3

    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x1

    .line 6
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->〇8〇OOoooo(Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
.end method

.method private final o〇O8OO()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->〇088O()Lcom/intsig/camscanner/databinding/DialogCsPdfVipBuySuccessBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogCsPdfVipBuySuccessBinding;->o〇00O:Landroidx/constraintlayout/widget/Group;

    .line 8
    .line 9
    const-string v2, "groupUnLogin"

    .line 10
    .line 11
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    const/4 v2, 0x0

    .line 15
    invoke-static {v1, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 16
    .line 17
    .line 18
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogCsPdfVipBuySuccessBinding;->〇08O〇00〇o:Landroidx/constraintlayout/widget/Group;

    .line 19
    .line 20
    const-string v3, "groupLogin"

    .line 21
    .line 22
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    const/4 v3, 0x1

    .line 26
    invoke-static {v1, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 27
    .line 28
    .line 29
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogCsPdfVipBuySuccessBinding;->〇0O:Lcom/intsig/camscanner/databinding/LayoutCsPdfVipBuySuccessRightItemBinding;

    .line 30
    .line 31
    const-string v4, "layoutCsRight"

    .line 32
    .line 33
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    new-instance v4, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment$VipRightInfo;

    .line 37
    .line 38
    const v5, 0x7f0806cb

    .line 39
    .line 40
    .line 41
    const v6, 0x7f1315e5

    .line 42
    .line 43
    .line 44
    invoke-direct {v4, v5, v6}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment$VipRightInfo;-><init>(II)V

    .line 45
    .line 46
    .line 47
    invoke-direct {p0, v1, v4}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->〇0〇0(Lcom/intsig/camscanner/databinding/LayoutCsPdfVipBuySuccessRightItemBinding;Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment$VipRightInfo;)V

    .line 48
    .line 49
    .line 50
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogCsPdfVipBuySuccessBinding;->oOo〇8o008:Lcom/intsig/camscanner/databinding/LayoutCsPdfVipBuySuccessRightItemBinding;

    .line 51
    .line 52
    const-string v4, "layoutPdfRight"

    .line 53
    .line 54
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    new-instance v4, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment$VipRightInfo;

    .line 58
    .line 59
    const v5, 0x7f080aa3

    .line 60
    .line 61
    .line 62
    const v6, 0x7f1315e6

    .line 63
    .line 64
    .line 65
    invoke-direct {v4, v5, v6}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment$VipRightInfo;-><init>(II)V

    .line 66
    .line 67
    .line 68
    invoke-direct {p0, v1, v4}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->〇0〇0(Lcom/intsig/camscanner/databinding/LayoutCsPdfVipBuySuccessRightItemBinding;Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment$VipRightInfo;)V

    .line 69
    .line 70
    .line 71
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->〇8〇80o()V

    .line 72
    .line 73
    .line 74
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogCsPdfVipBuySuccessBinding;->o8〇OO0〇0o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 75
    .line 76
    const/high16 v4, 0x41880000    # 17.0f

    .line 77
    .line 78
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextSize(F)V

    .line 79
    .line 80
    .line 81
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogCsPdfVipBuySuccessBinding;->o8〇OO0〇0o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 82
    .line 83
    const v4, 0x7f1315de

    .line 84
    .line 85
    .line 86
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    .line 87
    .line 88
    .line 89
    new-array v1, v3, [Landroid/view/View;

    .line 90
    .line 91
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCsPdfVipBuySuccessBinding;->o8〇OO0〇0o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 92
    .line 93
    aput-object v0, v1, v2

    .line 94
    .line 95
    invoke-virtual {p0, v1}, Lcom/intsig/app/BaseDialogFragment;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 96
    .line 97
    .line 98
    :cond_0
    return-void
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private final 〇00()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    instance-of v1, v0, Lcom/intsig/webview/WebViewActivity;

    .line 8
    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    :goto_0
    if-eqz v0, :cond_1

    .line 14
    .line 15
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 16
    .line 17
    .line 18
    :cond_1
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final 〇088O()Lcom/intsig/camscanner/databinding/DialogCsPdfVipBuySuccessBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->〇08O〇00〇o:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->〇080OO8〇0:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/DialogCsPdfVipBuySuccessBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
.end method

.method private final 〇0ooOOo()V
    .locals 2

    .line 1
    const-string v0, "CsPdfVipBuySuccessDialogFragment"

    .line 2
    .line 3
    const-string v1, "initView"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->〇o〇88〇8()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->o〇O8OO()V

    .line 15
    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->oOoO8OO〇()V

    .line 19
    .line 20
    .line 21
    :goto_0
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final 〇0〇0(Lcom/intsig/camscanner/databinding/LayoutCsPdfVipBuySuccessRightItemBinding;Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment$VipRightInfo;)V
    .locals 2

    .line 1
    iget-object v0, p1, Lcom/intsig/camscanner/databinding/LayoutCsPdfVipBuySuccessRightItemBinding;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 2
    .line 3
    invoke-virtual {p2}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment$VipRightInfo;->〇080()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 8
    .line 9
    .line 10
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LayoutCsPdfVipBuySuccessRightItemBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 11
    .line 12
    invoke-virtual {p2}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment$VipRightInfo;->〇o00〇〇Oo()I

    .line 13
    .line 14
    .line 15
    move-result p2

    .line 16
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(I)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final 〇8〇80o()V
    .locals 8

    .line 1
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->OoO8()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object v6

    .line 5
    const/4 v0, 0x1

    .line 6
    new-array v0, v0, [Ljava/lang/Object;

    .line 7
    .line 8
    const/4 v1, 0x0

    .line 9
    aput-object v6, v0, v1

    .line 10
    .line 11
    const v1, 0x7f1315dd

    .line 12
    .line 13
    .line 14
    invoke-virtual {p0, v1, v0}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object v7

    .line 18
    const-string v0, "getString(R.string.cs_636_pdf_03, account)"

    .line 19
    .line 20
    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    const-string v0, "account"

    .line 24
    .line 25
    invoke-static {v6, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    const/4 v2, 0x0

    .line 29
    const/4 v3, 0x0

    .line 30
    const/4 v4, 0x6

    .line 31
    const/4 v5, 0x0

    .line 32
    move-object v0, v7

    .line 33
    move-object v1, v6

    .line 34
    invoke-static/range {v0 .. v5}, Lkotlin/text/StringsKt;->oO00OOO(Ljava/lang/CharSequence;Ljava/lang/String;IZILjava/lang/Object;)I

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    .line 39
    .line 40
    .line 41
    move-result v1

    .line 42
    add-int/2addr v1, v0

    .line 43
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->〇088O()Lcom/intsig/camscanner/databinding/DialogCsPdfVipBuySuccessBinding;

    .line 44
    .line 45
    .line 46
    move-result-object v2

    .line 47
    if-eqz v2, :cond_0

    .line 48
    .line 49
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/DialogCsPdfVipBuySuccessBinding;->OO〇00〇8oO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_0
    const/4 v2, 0x0

    .line 53
    :goto_0
    if-nez v2, :cond_1

    .line 54
    .line 55
    goto :goto_1

    .line 56
    :cond_1
    new-instance v3, Landroid/text/SpannableString;

    .line 57
    .line 58
    invoke-direct {v3, v7}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 59
    .line 60
    .line 61
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    .line 62
    .line 63
    sget-object v5, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 64
    .line 65
    invoke-virtual {v5}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 66
    .line 67
    .line 68
    move-result-object v5

    .line 69
    const v6, 0x7f060226

    .line 70
    .line 71
    .line 72
    invoke-static {v5, v6}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 73
    .line 74
    .line 75
    move-result v5

    .line 76
    invoke-direct {v4, v5}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 77
    .line 78
    .line 79
    const/16 v5, 0x21

    .line 80
    .line 81
    invoke-virtual {v3, v4, v0, v1, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 82
    .line 83
    .line 84
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    .line 86
    .line 87
    :goto_1
    return-void
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private final 〇8〇OOoooo(Z)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/app/BaseDialogFragment;->dismiss()V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->〇00()V

    .line 7
    .line 8
    .line 9
    :cond_0
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final 〇o〇88〇8()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->o〇00O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/Boolean;

    .line 8
    .line 9
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 10
    .line 11
    .line 12
    move-result v0

    .line 13
    return v0
    .line 14
    .line 15
.end method

.method private final 〇〇o0〇8()V
    .locals 4

    .line 1
    const-string v0, "CsPdfVipBuySuccessDialogFragment"

    .line 2
    .line 3
    const-string v1, "clickLater"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string/jumbo v0, "type"

    .line 9
    .line 10
    .line 11
    const-string v1, "register_page"

    .line 12
    .line 13
    const-string v2, "CSUnionPdfVipPop"

    .line 14
    .line 15
    const-string v3, "maybe_later"

    .line 16
    .line 17
    invoke-static {v2, v3, v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    const/4 v0, 0x1

    .line 21
    const/4 v1, 0x0

    .line 22
    const/4 v2, 0x0

    .line 23
    invoke-static {p0, v2, v0, v1}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->o〇0〇o(Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;ZILjava/lang/Object;)V

    .line 24
    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method


# virtual methods
.method public dealClickAction(Landroid/view/View;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/app/BaseDialogFragment;->dealClickAction(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 p1, 0x0

    .line 16
    :goto_0
    if-nez p1, :cond_1

    .line 17
    .line 18
    goto :goto_1

    .line 19
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    const v1, 0x7f0a1548

    .line 24
    .line 25
    .line 26
    if-ne v0, v1, :cond_3

    .line 27
    .line 28
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->〇o〇88〇8()Z

    .line 29
    .line 30
    .line 31
    move-result p1

    .line 32
    new-instance v0, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    const-string/jumbo v1, "tv_know_login, login: "

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object p1

    .line 50
    const-string v0, "CsPdfVipBuySuccessDialogFragment"

    .line 51
    .line 52
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->〇o〇88〇8()Z

    .line 56
    .line 57
    .line 58
    move-result p1

    .line 59
    if-eqz p1, :cond_2

    .line 60
    .line 61
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->o00〇88〇08()V

    .line 62
    .line 63
    .line 64
    goto :goto_3

    .line 65
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->O0〇0()V

    .line 66
    .line 67
    .line 68
    goto :goto_3

    .line 69
    :cond_3
    :goto_1
    if-nez p1, :cond_4

    .line 70
    .line 71
    goto :goto_2

    .line 72
    :cond_4
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 73
    .line 74
    .line 75
    move-result v0

    .line 76
    const v1, 0x7f0a1557

    .line 77
    .line 78
    .line 79
    if-ne v0, v1, :cond_5

    .line 80
    .line 81
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->〇〇o0〇8()V

    .line 82
    .line 83
    .line 84
    goto :goto_3

    .line 85
    :cond_5
    :goto_2
    if-nez p1, :cond_6

    .line 86
    .line 87
    goto :goto_3

    .line 88
    :cond_6
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 89
    .line 90
    .line 91
    move-result p1

    .line 92
    const v0, 0x7f0a087d

    .line 93
    .line 94
    .line 95
    if-ne p1, v0, :cond_7

    .line 96
    .line 97
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->o880()V

    .line 98
    .line 99
    .line 100
    :cond_7
    :goto_3
    return-void
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method protected init(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    const/4 v0, 0x0

    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    if-eqz p1, :cond_0

    .line 13
    .line 14
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    .line 15
    .line 16
    invoke-direct {v1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 17
    .line 18
    .line 19
    invoke-virtual {p1, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 20
    .line 21
    .line 22
    :cond_0
    sget-object p1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 23
    .line 24
    invoke-virtual {p1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    const/16 v1, 0x122

    .line 29
    .line 30
    invoke-static {p1, v1}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 31
    .line 32
    .line 33
    move-result p1

    .line 34
    invoke-virtual {p0, p1}, Lcom/intsig/app/BaseDialogFragment;->oO〇oo(I)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p0, v0}, Landroidx/fragment/app/DialogFragment;->setCancelable(Z)V

    .line 38
    .line 39
    .line 40
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->〇0ooOOo()V

    .line 41
    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public onResume()V
    .locals 0

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/pdfvip/CsPdfVipBuySuccessDialogFragment;->Ooo8o()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d01a1

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.class public final Lcom/intsig/camscanner/purchase/entity/ToRetainGpDataBean;
.super Ljava/lang/Object;
.source "ToRetainGpDataBean.kt"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Landroidx/annotation/Keep;
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lcom/intsig/camscanner/purchase/entity/ToRetainGpDataBean;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field public beginActiveDays:I

.field public bubbleCurrentTime:J

.field public bubbleTime:J

.field public isShowBubble:Z

.field public showTimes:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/purchase/entity/ToRetainGpDataBean$Creator;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/purchase/entity/ToRetainGpDataBean$Creator;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/purchase/entity/ToRetainGpDataBean;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public constructor <init>()V
    .locals 10

    .line 1
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    const/16 v8, 0x1f

    const/4 v9, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lcom/intsig/camscanner/purchase/entity/ToRetainGpDataBean;-><init>(IIZJJILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(IIZJJ)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    iput p1, p0, Lcom/intsig/camscanner/purchase/entity/ToRetainGpDataBean;->beginActiveDays:I

    .line 4
    iput p2, p0, Lcom/intsig/camscanner/purchase/entity/ToRetainGpDataBean;->showTimes:I

    .line 5
    iput-boolean p3, p0, Lcom/intsig/camscanner/purchase/entity/ToRetainGpDataBean;->isShowBubble:Z

    .line 6
    iput-wide p4, p0, Lcom/intsig/camscanner/purchase/entity/ToRetainGpDataBean;->bubbleTime:J

    .line 7
    iput-wide p6, p0, Lcom/intsig/camscanner/purchase/entity/ToRetainGpDataBean;->bubbleCurrentTime:J

    return-void
.end method

.method public synthetic constructor <init>(IIZJJILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 4

    and-int/lit8 p9, p8, 0x1

    const/4 v0, 0x0

    if-eqz p9, :cond_0

    const/4 p9, 0x0

    goto :goto_0

    :cond_0
    move p9, p1

    :goto_0
    and-int/lit8 p1, p8, 0x2

    if-eqz p1, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    move v1, p2

    :goto_1
    and-int/lit8 p1, p8, 0x4

    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    move v0, p3

    :goto_2
    and-int/lit8 p1, p8, 0x8

    const-wide/16 p2, 0x0

    if-eqz p1, :cond_3

    move-wide v2, p2

    goto :goto_3

    :cond_3
    move-wide v2, p4

    :goto_3
    and-int/lit8 p1, p8, 0x10

    if-eqz p1, :cond_4

    move-wide p7, p2

    goto :goto_4

    :cond_4
    move-wide p7, p6

    :goto_4
    move-object p1, p0

    move p2, p9

    move p3, v1

    move p4, v0

    move-wide p5, v2

    .line 8
    invoke-direct/range {p1 .. p8}, Lcom/intsig/camscanner/purchase/entity/ToRetainGpDataBean;-><init>(IIZJJ)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1    # Landroid/os/Parcel;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string p2, "out"

    .line 2
    .line 3
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget p2, p0, Lcom/intsig/camscanner/purchase/entity/ToRetainGpDataBean;->beginActiveDays:I

    .line 7
    .line 8
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 9
    .line 10
    .line 11
    iget p2, p0, Lcom/intsig/camscanner/purchase/entity/ToRetainGpDataBean;->showTimes:I

    .line 12
    .line 13
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 14
    .line 15
    .line 16
    iget-boolean p2, p0, Lcom/intsig/camscanner/purchase/entity/ToRetainGpDataBean;->isShowBubble:Z

    .line 17
    .line 18
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->writeInt(I)V

    .line 19
    .line 20
    .line 21
    iget-wide v0, p0, Lcom/intsig/camscanner/purchase/entity/ToRetainGpDataBean;->bubbleTime:J

    .line 22
    .line 23
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 24
    .line 25
    .line 26
    iget-wide v0, p0, Lcom/intsig/camscanner/purchase/entity/ToRetainGpDataBean;->bubbleCurrentTime:J

    .line 27
    .line 28
    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 29
    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

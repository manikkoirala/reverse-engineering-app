.class public final enum Lcom/intsig/camscanner/purchase/entity/Function;
.super Ljava/lang/Enum;
.source "Function.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/camscanner/purchase/entity/Function;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum ADD_LONG_PIC_WATERMARK:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum ADD_WATERMARK:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum BACK_SCHOOL_GIFT:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum BUBBLE_VIP_EXPIRES:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum BUBBLE_VIP_EXPIRES_WITHIN_FIVE_DAYS:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum CERTIFICATE_CS_LIST:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum CLEANUP_UPGRADE_VIP:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum CLOUD_UPGRADE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum COUNT_MODE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum CS_ADVANCE_OCR:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum CS_CLOUD_ALERT_BUBBLE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum CS_EXCEL_DOCUMENT_DETAIL:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum CS_NEW_COUPON_POP:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum CS_OTHER_DOCUMENT_DETAIL:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum CS_WORD_DOCUMENT_DETAIL:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum DEEP_CLEANUP_UPGRADE_VIP:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum DE_MOIRE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum DOCUMENT_SCAN:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FAKE_FUNCTION:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FIRST_SYNC:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FOLDER:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FOLDER_UPGRADE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_BANK_CARD:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_BATCH_OCR:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_CAPTURE_BOOK:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_CERTIFICATE_BUSINESS:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_CERTIFICATE_CAPTURE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_CERTIFICATE_CHILD:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_CERTIFICATE_FILM:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_CERTIFICATE_MEDICAL:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_CERTIFICATE_NORMAL_ONE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_CERTIFICATE_OCCUPATION:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_CERTIFICATE_OTHER:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_CERTIFICATE_PHOTO:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_CERTIFICATE_RESIDENCE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_CERTIFICATE_STUDENT:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_CERTIFICATE_TRAVEL:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_CHINA_CAR:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_CLOSE_AD:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_DISABILITY:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_DOCUMENT_COMPRESSION:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_DRIVECHINA:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_DRIVE_OVER_SEA:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_ELECTRONIC_SIGNATURE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_EXCEL_OCR:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FOLDER_LIMIT_FOR_SYNC_EXCEPTION:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FOLDER_LIMIT_FOR_USER_CREATE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FUN_AUTO_UPLOAD:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FUN_CHANGE_ICON:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FUN_CLOUD_10G:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FUN_CLOUD_OCR:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FUN_CLOUD_OVERFLOW:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FUN_CLOUD_SEVENTY_PERCENT_HINT:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FUN_COMPOSITE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FUN_GREETCARD_FROM_GALLERY:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FUN_HD_PICTURE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FUN_NO_INK:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FUN_NO_INK_IMAGE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FUN_OCR_CHECK:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FUN_OCR_EXPORT:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FUN_OFFLINE_FOLDER:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FUN_OFFLINE_FOLDER_FREE_DOC:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FUN_SETTING_BUY_1G_CLOUD:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FUN_SHARE_ENCRYPTION_DOC_LINK:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FUN_SHARE_TXT:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FUN_TOPIC:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FUN_TRANSLATE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FUN_VIP_AFTER_EXPIRE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FUN_VIP_BEFORE_EXPIRE_3_1:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FUN_VIP_BEFORE_EXPIRE_7_4:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_FUN_VIP_PAY_FAIL:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_HD_SCAN:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_HOUSE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_HUKOU:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_ID_CARD_MODE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_ID_CHINA:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_ID_OVER_SEA:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_JPG_TO_PDF:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_MAINPAGE_PREMIUM:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_MAIN_PREMIUMPOP_FOR_GP:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_OPERATION_AD:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_PASSPORT:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_PDF_IMPORT:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_PDF_PAGE_NUM:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_PDF_PASSWORD:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_PDF_PICS_LIMIT:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_PDF_WATERMARK_FREE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_PREVIEW_DETECT_IDCARD:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_SAVE_PDF_SIGNATURE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_SAVE_SIGNATURE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_SAVE_SIGNATURE2:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_SCAN_DONE_IDCARD:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_SECURITY_MARK:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_SHARE_LIMIT:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_SINGLE_ID_CARD:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_TAKE_PICTURE_OCR:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_WEB_THANKSGIVING_PURCHASE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FROM_WORD:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum FUNCTION_OCR_GUIDE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum GREETING_CARD:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum ICLOUD_RECOVER:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum ICLOUD_SPACE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum ICON:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum ID_CARD:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum IMAGE_RECOLOR:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum IMAGE_RESTORE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum INITIATION:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum JISGAW_VIDEO_2:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum LOCAL_OCR_NO_RECOGNITION:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum LOCAL_ONLY:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum LONG_PIC_WATERMARK_FREE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum MAGE_RESTORE_WATERMAR:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum MAIN_ICON:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum MAIN_WEEK:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum MARKETING:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum MARKETING_RETURN_GIFT:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum ME_ICON:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum ME_WEB:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum NONE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum NONVIP_EXPANSION:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum OCR:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum PAY_POST_POSITION:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum PAY_POST_POSITION_ACTIVITY:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum PDF:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum PDF_PACKAGE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum PDF_SHARE_LIMIT:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum PDF_STRENGTHEN:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum PDF_TO_EXCEL:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum PDF_TO_PPT:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum PDF_TO_WORD:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum PDF_WATERMARK_FREE_PREVIEW:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum PDF_WATERMARK_FREE_UPGRADE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum PHOTO_IMPORT:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum PICTURE_WATERMARK_FREE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum REMOVE_HANDWRITING_FILTER:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum REMOVE_WATERMARK_FILTER:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum SCANDONE_BACK:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum SCAN_BILL:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum SCAN_BOOK:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum SCAN_FILE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum SCAN_NOTE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum SCREENSHOT_PICTURE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum SCREENSHOT_SAVE_TO_GALLERY:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum SCREENSHOT_TRANSFER_PDF:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum SCREENSHOT_TRANSFER_WORD:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum SELF_SEARCH:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum SHARE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum SMART_ERASE_REMOVE_ANY:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum SMART_ERASE_REMOVE_NOTE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum SMART_ERASE_REMOVE_TEXT:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum SMART_ERASE_REMOVE_WATERMARK:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum SYNC_CLOUD_STORAGE_LIMIT:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum TEXT_RECOGNITION:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum TO_WORD:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum UNSIGN_MARKETING:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum UPGRADE_ALL:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum USER_MARKETING:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum VIP_ICON:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum VIP_UPGRADE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum WORD_COPY:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum WORD_CUT:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum WORD_EXPORT:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum WORD_PREVIEW_NEW_EXPORT:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum WORD_PREVIEW_NEW_MORE_EMAIL:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum WORD_PREVIEW_NEW_MORE_PC_EDIT:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum WORD_PREVIEW_NEW_SAVE_FOR_PDF:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum WORD_TRANSFER_SHARE:Lcom/intsig/camscanner/purchase/entity/Function;

.field public static final enum WORKFLOW_FUNCTION_AUTO_ADJUST:Lcom/intsig/camscanner/purchase/entity/Function;


# instance fields
.field public mFromWhere:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 170

    .line 1
    new-instance v0, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v1, "FROM_FUN_CLOUD_10G"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_CLOUD_10G:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 2
    new-instance v1, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v3, "FROM_FUN_OCR_EXPORT"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_OCR_EXPORT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 3
    new-instance v3, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v5, "FROM_FUN_NO_INK"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_NO_INK:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 4
    new-instance v5, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v7, "pdf_watermark_free_upgrade"

    const-string v8, "PDF_WATERMARK_FREE_UPGRADE"

    const/4 v9, 0x3

    invoke-direct {v5, v8, v9, v7}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/entity/Function;->PDF_WATERMARK_FREE_UPGRADE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 5
    new-instance v7, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v8, "pdf_watermark_free_preview"

    const-string v10, "PDF_WATERMARK_FREE_PREVIEW"

    const/4 v11, 0x4

    invoke-direct {v7, v10, v11, v8}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/entity/Function;->PDF_WATERMARK_FREE_PREVIEW:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 6
    new-instance v8, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v10, "image_restore_watermar"

    const-string v12, "MAGE_RESTORE_WATERMAR"

    const/4 v13, 0x5

    invoke-direct {v8, v12, v13, v10}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v8, Lcom/intsig/camscanner/purchase/entity/Function;->MAGE_RESTORE_WATERMAR:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 7
    new-instance v10, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v12, "pdf_watermark_free"

    const-string v14, "FROM_PDF_WATERMARK_FREE"

    const/4 v15, 0x6

    invoke-direct {v10, v14, v15, v12}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v10, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_PDF_WATERMARK_FREE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 8
    new-instance v12, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v14, "FROM_FUN_NO_INK_IMAGE"

    const/4 v15, 0x7

    invoke-direct {v12, v14, v15}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_NO_INK_IMAGE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 9
    new-instance v14, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v15, "FROM_FUN_AUTO_UPLOAD"

    const/16 v13, 0x8

    invoke-direct {v14, v15, v13}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_AUTO_UPLOAD:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 10
    new-instance v15, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v13, "FROM_FUN_COMPOSITE"

    const/16 v11, 0x9

    invoke-direct {v15, v13, v11}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_COMPOSITE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 11
    new-instance v13, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v11, "FROM_FUN_OCR_CHECK"

    const/16 v9, 0xa

    invoke-direct {v13, v11, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_OCR_CHECK:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 12
    new-instance v11, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_FUN_HD_PICTURE"

    const/16 v6, 0xb

    invoke-direct {v11, v9, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_HD_PICTURE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 13
    new-instance v9, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v6, "FROM_FOLDER_LIMIT_FOR_USER_CREATE"

    const/16 v4, 0xc

    invoke-direct {v9, v6, v4}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FOLDER_LIMIT_FOR_USER_CREATE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 14
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v4, "FROM_FOLDER_LIMIT_FOR_SYNC_EXCEPTION"

    const/16 v2, 0xd

    invoke-direct {v6, v4, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FOLDER_LIMIT_FOR_SYNC_EXCEPTION:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 15
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v2, "FROM_CERTIFICATE_CAPTURE"

    move-object/from16 v16, v6

    const/16 v6, 0xe

    invoke-direct {v4, v2, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_CERTIFICATE_CAPTURE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 16
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v6, "FROM_PREVIEW_DETECT_IDCARD"

    move-object/from16 v17, v4

    const/16 v4, 0xf

    invoke-direct {v2, v6, v4}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_PREVIEW_DETECT_IDCARD:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 17
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v4, "FROM_SCAN_DONE_IDCARD"

    move-object/from16 v18, v2

    const/16 v2, 0x10

    invoke-direct {v6, v4, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_SCAN_DONE_IDCARD:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 18
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v2, "FROM_ID_CHINA"

    move-object/from16 v19, v6

    const/16 v6, 0x11

    invoke-direct {v4, v2, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_ID_CHINA:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 19
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v6, "FROM_HUKOU"

    move-object/from16 v20, v4

    const/16 v4, 0x12

    invoke-direct {v2, v6, v4}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_HUKOU:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 20
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v4, "FROM_PASSPORT"

    move-object/from16 v21, v2

    const/16 v2, 0x13

    invoke-direct {v6, v4, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_PASSPORT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 21
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v2, "FROM_DRIVECHINA"

    move-object/from16 v22, v6

    const/16 v6, 0x14

    invoke-direct {v4, v2, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_DRIVECHINA:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 22
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v6, "china_car"

    move-object/from16 v23, v4

    const-string v4, "FROM_CHINA_CAR"

    move-object/from16 v24, v9

    const/16 v9, 0x15

    invoke-direct {v2, v4, v9, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_CHINA_CAR:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 23
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v6, "FROM_HOUSE"

    const/16 v9, 0x16

    invoke-direct {v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_HOUSE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 24
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_DRIVE_OVER_SEA"

    move-object/from16 v25, v4

    const/16 v4, 0x17

    invoke-direct {v6, v9, v4}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_DRIVE_OVER_SEA:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 25
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v9, 0x18

    move-object/from16 v26, v6

    const-string v6, "residence"

    move-object/from16 v27, v2

    const-string v2, "FROM_CERTIFICATE_RESIDENCE"

    invoke-direct {v4, v2, v9, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_CERTIFICATE_RESIDENCE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 26
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x19

    const-string v9, "normal"

    move-object/from16 v28, v4

    const-string v4, "FROM_CERTIFICATE_NORMAL_ONE"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_CERTIFICATE_NORMAL_ONE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 27
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x1a

    const-string v9, "medical"

    move-object/from16 v29, v2

    const-string v2, "FROM_CERTIFICATE_MEDICAL"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_CERTIFICATE_MEDICAL:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 28
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x1b

    const-string/jumbo v9, "travel"

    move-object/from16 v30, v4

    const-string v4, "FROM_CERTIFICATE_TRAVEL"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_CERTIFICATE_TRAVEL:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 29
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x1c

    const-string v9, "child"

    move-object/from16 v31, v2

    const-string v2, "FROM_CERTIFICATE_CHILD"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_CERTIFICATE_CHILD:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 30
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x1d

    const-string/jumbo v9, "student"

    move-object/from16 v32, v4

    const-string v4, "FROM_CERTIFICATE_STUDENT"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_CERTIFICATE_STUDENT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 31
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x1e

    const-string v9, "occupation"

    move-object/from16 v33, v2

    const-string v2, "FROM_CERTIFICATE_OCCUPATION"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_CERTIFICATE_OCCUPATION:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 32
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x1f

    const-string v9, "firm"

    move-object/from16 v34, v4

    const-string v4, "FROM_CERTIFICATE_FILM"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_CERTIFICATE_FILM:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 33
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x20

    const-string v9, "other_certificate"

    move-object/from16 v35, v2

    const-string v2, "FROM_CERTIFICATE_OTHER"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_CERTIFICATE_OTHER:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 34
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x21

    const-string v9, "business_license"

    move-object/from16 v36, v4

    const-string v4, "FROM_CERTIFICATE_BUSINESS"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_CERTIFICATE_BUSINESS:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 35
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x22

    const-string v9, "disability_certificate"

    move-object/from16 v37, v2

    const-string v2, "FROM_DISABILITY"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_DISABILITY:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 36
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v6, "FROM_ID_OVER_SEA"

    const/16 v9, 0x23

    invoke-direct {v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_ID_OVER_SEA:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 37
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_BANK_CARD"

    move-object/from16 v38, v2

    const/16 v2, 0x24

    invoke-direct {v6, v9, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_BANK_CARD:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 38
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_SINGLE_ID_CARD"

    move-object/from16 v39, v6

    const/16 v6, 0x25

    invoke-direct {v2, v9, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_SINGLE_ID_CARD:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 39
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_FUN_TRANSLATE"

    move-object/from16 v40, v2

    const/16 v2, 0x26

    invoke-direct {v6, v9, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_TRANSLATE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 40
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_FUN_CLOUD_OCR"

    move-object/from16 v41, v6

    const/16 v6, 0x27

    invoke-direct {v2, v9, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_CLOUD_OCR:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 41
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_FUN_SETTING_BUY_1G_CLOUD"

    move-object/from16 v42, v2

    const/16 v2, 0x28

    invoke-direct {v6, v9, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_SETTING_BUY_1G_CLOUD:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 42
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_FUN_GREETCARD_FROM_GALLERY"

    move-object/from16 v43, v6

    const/16 v6, 0x29

    invoke-direct {v2, v9, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_GREETCARD_FROM_GALLERY:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 43
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_FUN_OFFLINE_FOLDER"

    move-object/from16 v44, v2

    const/16 v2, 0x2a

    invoke-direct {v6, v9, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_OFFLINE_FOLDER:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 44
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_FUN_OFFLINE_FOLDER_FREE_DOC"

    move-object/from16 v45, v6

    const/16 v6, 0x2b

    invoke-direct {v2, v9, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_OFFLINE_FOLDER_FREE_DOC:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 45
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_FUN_CHANGE_ICON"

    move-object/from16 v46, v2

    const/16 v2, 0x2c

    invoke-direct {v6, v9, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_CHANGE_ICON:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 46
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_FUN_CLOUD_SEVENTY_PERCENT_HINT"

    move-object/from16 v47, v6

    const/16 v6, 0x2d

    invoke-direct {v2, v9, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_CLOUD_SEVENTY_PERCENT_HINT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 47
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_FUN_CLOUD_OVERFLOW"

    move-object/from16 v48, v2

    const/16 v2, 0x2e

    invoke-direct {v6, v9, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_CLOUD_OVERFLOW:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 48
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_FUN_SHARE_TXT"

    move-object/from16 v49, v6

    const/16 v6, 0x2f

    invoke-direct {v2, v9, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_SHARE_TXT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 49
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_FUN_SHARE_ENCRYPTION_DOC_LINK"

    move-object/from16 v50, v2

    const/16 v2, 0x30

    invoke-direct {v6, v9, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_SHARE_ENCRYPTION_DOC_LINK:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 50
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_FUN_VIP_BEFORE_EXPIRE_7_4"

    move-object/from16 v51, v6

    const/16 v6, 0x31

    invoke-direct {v2, v9, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_VIP_BEFORE_EXPIRE_7_4:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 51
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_FUN_VIP_BEFORE_EXPIRE_3_1"

    move-object/from16 v52, v2

    const/16 v2, 0x32

    invoke-direct {v6, v9, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_VIP_BEFORE_EXPIRE_3_1:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 52
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_FUN_VIP_AFTER_EXPIRE"

    move-object/from16 v53, v6

    const/16 v6, 0x33

    invoke-direct {v2, v9, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_VIP_AFTER_EXPIRE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 53
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_FUN_VIP_PAY_FAIL"

    move-object/from16 v54, v2

    const/16 v2, 0x34

    invoke-direct {v6, v9, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_VIP_PAY_FAIL:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 54
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_CLOSE_AD"

    move-object/from16 v55, v6

    const/16 v6, 0x35

    invoke-direct {v2, v9, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_CLOSE_AD:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 55
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_WEB_THANKSGIVING_PURCHASE"

    move-object/from16 v56, v2

    const/16 v2, 0x36

    invoke-direct {v6, v9, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_WEB_THANKSGIVING_PURCHASE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 56
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_FUN_TOPIC"

    move-object/from16 v57, v6

    const/16 v6, 0x37

    invoke-direct {v2, v9, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_TOPIC:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 57
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_TAKE_PICTURE_OCR"

    move-object/from16 v58, v2

    const/16 v2, 0x38

    invoke-direct {v6, v9, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_TAKE_PICTURE_OCR:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 58
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_MAIN_PREMIUMPOP_FOR_GP"

    move-object/from16 v59, v6

    const/16 v6, 0x39

    invoke-direct {v2, v9, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_MAIN_PREMIUMPOP_FOR_GP:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 59
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_MAINPAGE_PREMIUM"

    move-object/from16 v60, v2

    const/16 v2, 0x3a

    invoke-direct {v6, v9, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_MAINPAGE_PREMIUM:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 60
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_PDF_PASSWORD"

    move-object/from16 v61, v6

    const/16 v6, 0x3b

    invoke-direct {v2, v9, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_PDF_PASSWORD:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 61
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_PDF_PICS_LIMIT"

    move-object/from16 v62, v2

    const/16 v2, 0x3c

    invoke-direct {v6, v9, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_PDF_PICS_LIMIT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 62
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_DOCUMENT_COMPRESSION"

    move-object/from16 v63, v6

    const/16 v6, 0x3d

    invoke-direct {v2, v9, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_DOCUMENT_COMPRESSION:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 63
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "SELF_SEARCH"

    move-object/from16 v64, v2

    const/16 v2, 0x3e

    invoke-direct {v6, v9, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->SELF_SEARCH:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 64
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_WORD"

    move-object/from16 v65, v6

    const/16 v6, 0x3f

    invoke-direct {v2, v9, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_WORD:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 65
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_CERTIFICATE_PHOTO"

    move-object/from16 v66, v2

    const/16 v2, 0x40

    invoke-direct {v6, v9, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_CERTIFICATE_PHOTO:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 66
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_PDF_IMPORT"

    move-object/from16 v67, v6

    const/16 v6, 0x41

    invoke-direct {v2, v9, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_PDF_IMPORT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 67
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_OPERATION_AD"

    move-object/from16 v68, v2

    const/16 v2, 0x42

    invoke-direct {v6, v9, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_OPERATION_AD:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 68
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_CAPTURE_BOOK"

    move-object/from16 v69, v6

    const/16 v6, 0x43

    invoke-direct {v2, v9, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_CAPTURE_BOOK:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 69
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_SECURITY_MARK"

    move-object/from16 v70, v2

    const/16 v2, 0x44

    invoke-direct {v6, v9, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_SECURITY_MARK:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 70
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_SAVE_SIGNATURE"

    move-object/from16 v71, v6

    const/16 v6, 0x45

    invoke-direct {v2, v9, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_SAVE_SIGNATURE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 71
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_SAVE_SIGNATURE2"

    move-object/from16 v72, v2

    const/16 v2, 0x46

    invoke-direct {v6, v9, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_SAVE_SIGNATURE2:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 72
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_SAVE_PDF_SIGNATURE"

    move-object/from16 v73, v6

    const/16 v6, 0x47

    invoke-direct {v2, v9, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_SAVE_PDF_SIGNATURE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 73
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_EXCEL_OCR"

    move-object/from16 v74, v2

    const/16 v2, 0x48

    invoke-direct {v6, v9, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_EXCEL_OCR:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 74
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "FROM_BATCH_OCR"

    move-object/from16 v75, v6

    const/16 v6, 0x49

    invoke-direct {v2, v9, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_BATCH_OCR:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 75
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v9, 0x4a

    move-object/from16 v76, v2

    const-string v2, "folder_upgrade"

    move-object/from16 v77, v4

    const-string v4, "FOLDER_UPGRADE"

    invoke-direct {v6, v4, v9, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->FOLDER_UPGRADE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 76
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v4, 0x4b

    const-string v9, "cloud_upgrade"

    move-object/from16 v78, v6

    const-string v6, "CLOUD_UPGRADE"

    invoke-direct {v2, v6, v4, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->CLOUD_UPGRADE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 77
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v6, "MARKETING"

    const/16 v9, 0x4c

    invoke-direct {v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->MARKETING:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 78
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "MAIN_WEEK"

    move-object/from16 v79, v4

    const/16 v4, 0x4d

    invoke-direct {v6, v9, v4}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->MAIN_WEEK:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 79
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v9, 0x4e

    move-object/from16 v80, v6

    const-string v6, "30+_user_marketing"

    move-object/from16 v81, v2

    const-string v2, "USER_MARKETING"

    invoke-direct {v4, v2, v9, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->USER_MARKETING:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 80
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v6, "LONG_PIC_WATERMARK_FREE"

    const/16 v9, 0x4f

    invoke-direct {v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->LONG_PIC_WATERMARK_FREE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 81
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "ADD_LONG_PIC_WATERMARK"

    move-object/from16 v82, v2

    const/16 v2, 0x50

    invoke-direct {v6, v9, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->ADD_LONG_PIC_WATERMARK:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 82
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v9, 0x51

    move-object/from16 v83, v6

    const-string v6, "pdf_package"

    move-object/from16 v84, v4

    const-string v4, "PDF_PACKAGE"

    invoke-direct {v2, v4, v9, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->PDF_PACKAGE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 83
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x52

    const-string v9, "pdf_to_word"

    move-object/from16 v85, v2

    const-string v2, "PDF_TO_WORD"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->PDF_TO_WORD:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 84
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x53

    const-string v9, "pdf_to_excel"

    move-object/from16 v86, v4

    const-string v4, "PDF_TO_EXCEL"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->PDF_TO_EXCEL:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 85
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x54

    const-string v9, "pdf_to_ppt"

    move-object/from16 v87, v2

    const-string v2, "PDF_TO_PPT"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->PDF_TO_PPT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 86
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v6, "GREETING_CARD"

    const/16 v9, 0x55

    invoke-direct {v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->GREETING_CARD:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 87
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "JISGAW_VIDEO_2"

    move-object/from16 v88, v2

    const/16 v2, 0x56

    invoke-direct {v6, v9, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->JISGAW_VIDEO_2:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 88
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v9, "ME_WEB"

    move-object/from16 v89, v6

    const/16 v6, 0x57

    invoke-direct {v2, v9, v6}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->ME_WEB:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 89
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v9, 0x58

    move-object/from16 v90, v2

    const-string v2, "add_watermark"

    move-object/from16 v91, v4

    const-string v4, "ADD_WATERMARK"

    invoke-direct {v6, v4, v9, v2}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->ADD_WATERMARK:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 90
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v4, 0x59

    const-string v9, "local_only"

    move-object/from16 v92, v6

    const-string v6, "LOCAL_ONLY"

    invoke-direct {v2, v6, v4, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->LOCAL_ONLY:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 91
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x5a

    const-string v9, "folder"

    move-object/from16 v93, v2

    const-string v2, "FOLDER"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->FOLDER:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 92
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x5b

    const-string v9, "icloud_space"

    move-object/from16 v94, v4

    const-string v4, "ICLOUD_SPACE"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->ICLOUD_SPACE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 93
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x5c

    const-string v9, "icloud_recover"

    move-object/from16 v95, v2

    const-string v2, "ICLOUD_RECOVER"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->ICLOUD_RECOVER:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 94
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x5d

    const-string/jumbo v9, "upgrade_all"

    move-object/from16 v96, v4

    const-string v4, "UPGRADE_ALL"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->UPGRADE_ALL:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 95
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x5e

    const-string v9, "add_page_num"

    move-object/from16 v97, v2

    const-string v2, "FROM_PDF_PAGE_NUM"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_PDF_PAGE_NUM:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 96
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x5f

    const-string v9, "cs_advance_ocr"

    move-object/from16 v98, v4

    const-string v4, "CS_ADVANCE_OCR"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->CS_ADVANCE_OCR:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 97
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x60

    const-string v9, "ocr_guide"

    move-object/from16 v99, v2

    const-string v2, "FUNCTION_OCR_GUIDE"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->FUNCTION_OCR_GUIDE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 98
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x61

    const-string v9, "id_card_mode"

    move-object/from16 v100, v4

    const-string v4, "FROM_ID_CARD_MODE"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_ID_CARD_MODE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 99
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x62

    const-string v9, "jpg_to_pdf"

    move-object/from16 v101, v2

    const-string v2, "FROM_JPG_TO_PDF"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_JPG_TO_PDF:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 100
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x63

    const-string v9, "electronic_signature"

    move-object/from16 v102, v4

    const-string v4, "FROM_ELECTRONIC_SIGNATURE"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_ELECTRONIC_SIGNATURE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 101
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x64

    const-string v9, "hd_scan"

    move-object/from16 v103, v2

    const-string v2, "FROM_HD_SCAN"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_HD_SCAN:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 102
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x65

    const-string/jumbo v9, "word_export"

    move-object/from16 v104, v4

    const-string v4, "WORD_EXPORT"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->WORD_EXPORT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 103
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x66

    const-string/jumbo v9, "word_copy"

    move-object/from16 v105, v2

    const-string v2, "WORD_COPY"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->WORD_COPY:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 104
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x67

    const-string/jumbo v9, "word_cut"

    move-object/from16 v106, v4

    const-string v4, "WORD_CUT"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->WORD_CUT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 105
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x68

    const-string v9, "image_restore"

    move-object/from16 v107, v2

    const-string v2, "IMAGE_RESTORE"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->IMAGE_RESTORE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 106
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x69

    const-string v9, "demoire"

    move-object/from16 v108, v4

    const-string v4, "DE_MOIRE"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->DE_MOIRE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 107
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x6a

    const-string v9, "colorize"

    move-object/from16 v109, v2

    const-string v2, "IMAGE_RECOLOR"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->IMAGE_RECOLOR:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 108
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x6b

    const-string v9, "picture_watermark_free"

    move-object/from16 v110, v4

    const-string v4, "PICTURE_WATERMARK_FREE"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->PICTURE_WATERMARK_FREE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 109
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x6c

    const-string v9, "pdf_strengthen"

    move-object/from16 v111, v2

    const-string v2, "PDF_STRENGTHEN"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->PDF_STRENGTHEN:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 110
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x6d

    const-string v9, "pdf_share_limit"

    move-object/from16 v112, v4

    const-string v4, "PDF_SHARE_LIMIT"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->PDF_SHARE_LIMIT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 111
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v6, "CS_NEW_COUPON_POP"

    const/16 v9, 0x6e

    invoke-direct {v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->CS_NEW_COUPON_POP:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 112
    new-instance v6, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v9, 0x6f

    move-object/from16 v113, v4

    const-string v4, "id_card"

    move-object/from16 v114, v2

    const-string v2, "ID_CARD"

    invoke-direct {v6, v2, v9, v4}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->ID_CARD:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 113
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v4, 0x70

    const-string/jumbo v9, "text_recognition"

    move-object/from16 v115, v6

    const-string v6, "TEXT_RECOGNITION"

    invoke-direct {v2, v6, v4, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->TEXT_RECOGNITION:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 114
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x71

    const-string v9, "document_scan"

    move-object/from16 v116, v2

    const-string v2, "DOCUMENT_SCAN"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->DOCUMENT_SCAN:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 115
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x72

    const-string v9, "photo_import"

    move-object/from16 v117, v4

    const-string v4, "PHOTO_IMPORT"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->PHOTO_IMPORT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 116
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x73

    const-string v9, "ocr"

    move-object/from16 v118, v2

    const-string v2, "OCR"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->OCR:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 117
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x74

    const-string v9, "scan_file"

    move-object/from16 v119, v4

    const-string v4, "SCAN_FILE"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->SCAN_FILE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 118
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x75

    const-string/jumbo v9, "to_word"

    move-object/from16 v120, v2

    const-string v2, "TO_WORD"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->TO_WORD:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 119
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x76

    const-string v9, "scan_note"

    move-object/from16 v121, v4

    const-string v4, "SCAN_NOTE"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->SCAN_NOTE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 120
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x77

    const-string v9, "scan_book"

    move-object/from16 v122, v2

    const-string v2, "SCAN_BOOK"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->SCAN_BOOK:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 121
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x78

    const-string v9, "scan_bill"

    move-object/from16 v123, v4

    const-string v4, "SCAN_BILL"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->SCAN_BILL:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 122
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x79

    const-string v9, "pdf"

    move-object/from16 v124, v2

    const-string v2, "PDF"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->PDF:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 123
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x7a

    const-string v9, "initiation"

    move-object/from16 v125, v4

    const-string v4, "INITIATION"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->INITIATION:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 124
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x7b

    const-string v9, "scandone_back"

    move-object/from16 v126, v2

    const-string v2, "SCANDONE_BACK"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->SCANDONE_BACK:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 125
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x7c

    const-string v9, "first_sync"

    move-object/from16 v127, v4

    const-string v4, "FIRST_SYNC"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FIRST_SYNC:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 126
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x7d

    const-string v9, "cs_cloud_alert_bubble"

    move-object/from16 v128, v2

    const-string v2, "CS_CLOUD_ALERT_BUBBLE"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->CS_CLOUD_ALERT_BUBBLE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 127
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x7e

    const-string/jumbo v9, "sync_cloud_storage_limit"

    move-object/from16 v129, v4

    const-string v4, "SYNC_CLOUD_STORAGE_LIMIT"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->SYNC_CLOUD_STORAGE_LIMIT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 128
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x7f

    const-string/jumbo v9, "vip_upgrade"

    move-object/from16 v130, v2

    const-string v2, "VIP_UPGRADE"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->VIP_UPGRADE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 129
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x80

    const-string/jumbo v9, "vip_icon"

    move-object/from16 v131, v4

    const-string v4, "VIP_ICON"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->VIP_ICON:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 130
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x81

    const-string v9, "icon"

    move-object/from16 v132, v2

    const-string v2, "ICON"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->ICON:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 131
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x82

    const-string v9, "bubble_vip_expires"

    move-object/from16 v133, v4

    const-string v4, "BUBBLE_VIP_EXPIRES"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->BUBBLE_VIP_EXPIRES:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 132
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x83

    const-string v9, "cleanup_upgrade_vip"

    move-object/from16 v134, v2

    const-string v2, "CLEANUP_UPGRADE_VIP"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->CLEANUP_UPGRADE_VIP:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 133
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x84

    const-string v9, "deep_cleanup_upgrade_vip"

    move-object/from16 v135, v4

    const-string v4, "DEEP_CLEANUP_UPGRADE_VIP"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->DEEP_CLEANUP_UPGRADE_VIP:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 134
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x85

    const-string v9, "bubble_vip_expires_within_five_days"

    move-object/from16 v136, v2

    const-string v2, "BUBBLE_VIP_EXPIRES_WITHIN_FIVE_DAYS"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->BUBBLE_VIP_EXPIRES_WITHIN_FIVE_DAYS:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 135
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x86

    const-string v9, "local_ocr_no_recognition"

    move-object/from16 v137, v4

    const-string v4, "LOCAL_OCR_NO_RECOGNITION"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->LOCAL_OCR_NO_RECOGNITION:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 136
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x87

    const-string v9, "pay_post_position"

    move-object/from16 v138, v2

    const-string v2, "PAY_POST_POSITION"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->PAY_POST_POSITION:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 137
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x88

    const-string v9, "backSchoolgift"

    move-object/from16 v139, v4

    const-string v4, "BACK_SCHOOL_GIFT"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->BACK_SCHOOL_GIFT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 138
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x89

    const-string v9, "picture"

    move-object/from16 v140, v2

    const-string v2, "SCREENSHOT_PICTURE"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->SCREENSHOT_PICTURE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 139
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x8a

    const-string/jumbo v9, "transfer_word"

    move-object/from16 v141, v4

    const-string v4, "SCREENSHOT_TRANSFER_WORD"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->SCREENSHOT_TRANSFER_WORD:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 140
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x8b

    const-string/jumbo v9, "transfer_pdf"

    move-object/from16 v142, v2

    const-string v2, "SCREENSHOT_TRANSFER_PDF"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->SCREENSHOT_TRANSFER_PDF:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 141
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x8c

    const-string v9, "save_to_gallery"

    move-object/from16 v143, v4

    const-string v4, "SCREENSHOT_SAVE_TO_GALLERY"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->SCREENSHOT_SAVE_TO_GALLERY:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 142
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x8d

    const-string v9, "remove_random"

    move-object/from16 v144, v2

    const-string v2, "SMART_ERASE_REMOVE_ANY"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->SMART_ERASE_REMOVE_ANY:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 143
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x8e

    const-string v9, "remove_text"

    move-object/from16 v145, v4

    const-string v4, "SMART_ERASE_REMOVE_TEXT"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->SMART_ERASE_REMOVE_TEXT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 144
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x8f

    const-string v9, "remove_watermark"

    move-object/from16 v146, v2

    const-string v2, "SMART_ERASE_REMOVE_WATERMARK"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->SMART_ERASE_REMOVE_WATERMARK:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 145
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x90

    const-string v9, "remove_handwriting"

    move-object/from16 v147, v4

    const-string v4, "SMART_ERASE_REMOVE_NOTE"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->SMART_ERASE_REMOVE_NOTE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 146
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x91

    const-string v9, "pay_post_position_activity"

    move-object/from16 v148, v2

    const-string v2, "PAY_POST_POSITION_ACTIVITY"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->PAY_POST_POSITION_ACTIVITY:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 147
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x92

    const-string v9, "fake_function"

    move-object/from16 v149, v4

    const-string v4, "FAKE_FUNCTION"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->FAKE_FUNCTION:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 148
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x93

    const-string v9, "share_limit"

    move-object/from16 v150, v2

    const-string v2, "FROM_SHARE_LIMIT"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_SHARE_LIMIT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 149
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x94

    const-string v9, "share"

    move-object/from16 v151, v4

    const-string v4, "SHARE"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->SHARE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 150
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x95

    const-string v9, "count_mode"

    move-object/from16 v152, v2

    const-string v2, "COUNT_MODE"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->COUNT_MODE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 151
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x96

    const-string v9, "cs_word_document_detail"

    move-object/from16 v153, v4

    const-string v4, "CS_WORD_DOCUMENT_DETAIL"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->CS_WORD_DOCUMENT_DETAIL:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 152
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x97

    const-string v9, "cs_excel_document_detail"

    move-object/from16 v154, v2

    const-string v2, "CS_EXCEL_DOCUMENT_DETAIL"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->CS_EXCEL_DOCUMENT_DETAIL:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 153
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x98

    const-string v9, "cs_other_document_detail"

    move-object/from16 v155, v4

    const-string v4, "CS_OTHER_DOCUMENT_DETAIL"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->CS_OTHER_DOCUMENT_DETAIL:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 154
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x99

    const-string v9, "export"

    move-object/from16 v156, v2

    const-string v2, "WORD_PREVIEW_NEW_EXPORT"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->WORD_PREVIEW_NEW_EXPORT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 155
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x9a

    const-string v9, "save_for_pdf"

    move-object/from16 v157, v4

    const-string v4, "WORD_PREVIEW_NEW_SAVE_FOR_PDF"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->WORD_PREVIEW_NEW_SAVE_FOR_PDF:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 156
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x9b

    const-string v9, "more_email"

    move-object/from16 v158, v2

    const-string v2, "WORD_PREVIEW_NEW_MORE_EMAIL"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->WORD_PREVIEW_NEW_MORE_EMAIL:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 157
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x9c

    const-string v9, "more_pc_edit"

    move-object/from16 v159, v4

    const-string v4, "WORD_PREVIEW_NEW_MORE_PC_EDIT"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->WORD_PREVIEW_NEW_MORE_PC_EDIT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 158
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x9d

    const-string/jumbo v9, "word_transfer_share"

    move-object/from16 v160, v2

    const-string v2, "WORD_TRANSFER_SHARE"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->WORD_TRANSFER_SHARE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 159
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x9e

    const-string v9, "me_icon"

    move-object/from16 v161, v4

    const-string v4, "ME_ICON"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->ME_ICON:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 160
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0x9f

    const-string v9, "main_icon"

    move-object/from16 v162, v2

    const-string v2, "MAIN_ICON"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->MAIN_ICON:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 161
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0xa0

    const-string/jumbo v9, "unsign_marketing"

    move-object/from16 v163, v4

    const-string v4, "UNSIGN_MARKETING"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->UNSIGN_MARKETING:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 162
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0xa1

    const-string v9, ""

    move-object/from16 v164, v2

    const-string v2, "WORKFLOW_FUNCTION_AUTO_ADJUST"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->WORKFLOW_FUNCTION_AUTO_ADJUST:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 163
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0xa2

    const-string v9, "id_mode_optimization"

    move-object/from16 v165, v4

    const-string v4, "CERTIFICATE_CS_LIST"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->CERTIFICATE_CS_LIST:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 164
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0xa3

    const-string v9, "remove_watermark_filter"

    move-object/from16 v166, v2

    const-string v2, "REMOVE_WATERMARK_FILTER"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->REMOVE_WATERMARK_FILTER:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 165
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0xa4

    const-string v9, "remove_handwriting_filter"

    move-object/from16 v167, v4

    const-string v4, "REMOVE_HANDWRITING_FILTER"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->REMOVE_HANDWRITING_FILTER:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 166
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0xa5

    const-string v9, "nonvip_expansion"

    move-object/from16 v168, v2

    const-string v2, "NONVIP_EXPANSION"

    invoke-direct {v4, v2, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->NONVIP_EXPANSION:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 167
    new-instance v2, Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0xa6

    const-string v9, "marketing_return_gift"

    move-object/from16 v169, v4

    const-string v4, "MARKETING_RETURN_GIFT"

    invoke-direct {v2, v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->MARKETING_RETURN_GIFT:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 168
    new-instance v4, Lcom/intsig/camscanner/purchase/entity/Function;

    const-string v6, "NONE"

    const/16 v9, 0xa7

    invoke-direct {v4, v6, v9}, Lcom/intsig/camscanner/purchase/entity/Function;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/intsig/camscanner/purchase/entity/Function;->NONE:Lcom/intsig/camscanner/purchase/entity/Function;

    const/16 v6, 0xa8

    new-array v6, v6, [Lcom/intsig/camscanner/purchase/entity/Function;

    const/4 v9, 0x0

    aput-object v0, v6, v9

    const/4 v0, 0x1

    aput-object v1, v6, v0

    const/4 v0, 0x2

    aput-object v3, v6, v0

    const/4 v0, 0x3

    aput-object v5, v6, v0

    const/4 v0, 0x4

    aput-object v7, v6, v0

    const/4 v0, 0x5

    aput-object v8, v6, v0

    const/4 v0, 0x6

    aput-object v10, v6, v0

    const/4 v0, 0x7

    aput-object v12, v6, v0

    const/16 v0, 0x8

    aput-object v14, v6, v0

    const/16 v0, 0x9

    aput-object v15, v6, v0

    const/16 v0, 0xa

    aput-object v13, v6, v0

    const/16 v0, 0xb

    aput-object v11, v6, v0

    const/16 v0, 0xc

    aput-object v24, v6, v0

    const/16 v0, 0xd

    aput-object v16, v6, v0

    const/16 v0, 0xe

    aput-object v17, v6, v0

    const/16 v0, 0xf

    aput-object v18, v6, v0

    const/16 v0, 0x10

    aput-object v19, v6, v0

    const/16 v0, 0x11

    aput-object v20, v6, v0

    const/16 v0, 0x12

    aput-object v21, v6, v0

    const/16 v0, 0x13

    aput-object v22, v6, v0

    const/16 v0, 0x14

    aput-object v23, v6, v0

    const/16 v0, 0x15

    aput-object v27, v6, v0

    const/16 v0, 0x16

    aput-object v25, v6, v0

    const/16 v0, 0x17

    aput-object v26, v6, v0

    const/16 v0, 0x18

    aput-object v28, v6, v0

    const/16 v0, 0x19

    aput-object v29, v6, v0

    const/16 v0, 0x1a

    aput-object v30, v6, v0

    const/16 v0, 0x1b

    aput-object v31, v6, v0

    const/16 v0, 0x1c

    aput-object v32, v6, v0

    const/16 v0, 0x1d

    aput-object v33, v6, v0

    const/16 v0, 0x1e

    aput-object v34, v6, v0

    const/16 v0, 0x1f

    aput-object v35, v6, v0

    const/16 v0, 0x20

    aput-object v36, v6, v0

    const/16 v0, 0x21

    aput-object v37, v6, v0

    const/16 v0, 0x22

    aput-object v77, v6, v0

    const/16 v0, 0x23

    aput-object v38, v6, v0

    const/16 v0, 0x24

    aput-object v39, v6, v0

    const/16 v0, 0x25

    aput-object v40, v6, v0

    const/16 v0, 0x26

    aput-object v41, v6, v0

    const/16 v0, 0x27

    aput-object v42, v6, v0

    const/16 v0, 0x28

    aput-object v43, v6, v0

    const/16 v0, 0x29

    aput-object v44, v6, v0

    const/16 v0, 0x2a

    aput-object v45, v6, v0

    const/16 v0, 0x2b

    aput-object v46, v6, v0

    const/16 v0, 0x2c

    aput-object v47, v6, v0

    const/16 v0, 0x2d

    aput-object v48, v6, v0

    const/16 v0, 0x2e

    aput-object v49, v6, v0

    const/16 v0, 0x2f

    aput-object v50, v6, v0

    const/16 v0, 0x30

    aput-object v51, v6, v0

    const/16 v0, 0x31

    aput-object v52, v6, v0

    const/16 v0, 0x32

    aput-object v53, v6, v0

    const/16 v0, 0x33

    aput-object v54, v6, v0

    const/16 v0, 0x34

    aput-object v55, v6, v0

    const/16 v0, 0x35

    aput-object v56, v6, v0

    const/16 v0, 0x36

    aput-object v57, v6, v0

    const/16 v0, 0x37

    aput-object v58, v6, v0

    const/16 v0, 0x38

    aput-object v59, v6, v0

    const/16 v0, 0x39

    aput-object v60, v6, v0

    const/16 v0, 0x3a

    aput-object v61, v6, v0

    const/16 v0, 0x3b

    aput-object v62, v6, v0

    const/16 v0, 0x3c

    aput-object v63, v6, v0

    const/16 v0, 0x3d

    aput-object v64, v6, v0

    const/16 v0, 0x3e

    aput-object v65, v6, v0

    const/16 v0, 0x3f

    aput-object v66, v6, v0

    const/16 v0, 0x40

    aput-object v67, v6, v0

    const/16 v0, 0x41

    aput-object v68, v6, v0

    const/16 v0, 0x42

    aput-object v69, v6, v0

    const/16 v0, 0x43

    aput-object v70, v6, v0

    const/16 v0, 0x44

    aput-object v71, v6, v0

    const/16 v0, 0x45

    aput-object v72, v6, v0

    const/16 v0, 0x46

    aput-object v73, v6, v0

    const/16 v0, 0x47

    aput-object v74, v6, v0

    const/16 v0, 0x48

    aput-object v75, v6, v0

    const/16 v0, 0x49

    aput-object v76, v6, v0

    const/16 v0, 0x4a

    aput-object v78, v6, v0

    const/16 v0, 0x4b

    aput-object v81, v6, v0

    const/16 v0, 0x4c

    aput-object v79, v6, v0

    const/16 v0, 0x4d

    aput-object v80, v6, v0

    const/16 v0, 0x4e

    aput-object v84, v6, v0

    const/16 v0, 0x4f

    aput-object v82, v6, v0

    const/16 v0, 0x50

    aput-object v83, v6, v0

    const/16 v0, 0x51

    aput-object v85, v6, v0

    const/16 v0, 0x52

    aput-object v86, v6, v0

    const/16 v0, 0x53

    aput-object v87, v6, v0

    const/16 v0, 0x54

    aput-object v91, v6, v0

    const/16 v0, 0x55

    aput-object v88, v6, v0

    const/16 v0, 0x56

    aput-object v89, v6, v0

    const/16 v0, 0x57

    aput-object v90, v6, v0

    const/16 v0, 0x58

    aput-object v92, v6, v0

    const/16 v0, 0x59

    aput-object v93, v6, v0

    const/16 v0, 0x5a

    aput-object v94, v6, v0

    const/16 v0, 0x5b

    aput-object v95, v6, v0

    const/16 v0, 0x5c

    aput-object v96, v6, v0

    const/16 v0, 0x5d

    aput-object v97, v6, v0

    const/16 v0, 0x5e

    aput-object v98, v6, v0

    const/16 v0, 0x5f

    aput-object v99, v6, v0

    const/16 v0, 0x60

    aput-object v100, v6, v0

    const/16 v0, 0x61

    aput-object v101, v6, v0

    const/16 v0, 0x62

    aput-object v102, v6, v0

    const/16 v0, 0x63

    aput-object v103, v6, v0

    const/16 v0, 0x64

    aput-object v104, v6, v0

    const/16 v0, 0x65

    aput-object v105, v6, v0

    const/16 v0, 0x66

    aput-object v106, v6, v0

    const/16 v0, 0x67

    aput-object v107, v6, v0

    const/16 v0, 0x68

    aput-object v108, v6, v0

    const/16 v0, 0x69

    aput-object v109, v6, v0

    const/16 v0, 0x6a

    aput-object v110, v6, v0

    const/16 v0, 0x6b

    aput-object v111, v6, v0

    const/16 v0, 0x6c

    aput-object v112, v6, v0

    const/16 v0, 0x6d

    aput-object v114, v6, v0

    const/16 v0, 0x6e

    aput-object v113, v6, v0

    const/16 v0, 0x6f

    aput-object v115, v6, v0

    const/16 v0, 0x70

    aput-object v116, v6, v0

    const/16 v0, 0x71

    aput-object v117, v6, v0

    const/16 v0, 0x72

    aput-object v118, v6, v0

    const/16 v0, 0x73

    aput-object v119, v6, v0

    const/16 v0, 0x74

    aput-object v120, v6, v0

    const/16 v0, 0x75

    aput-object v121, v6, v0

    const/16 v0, 0x76

    aput-object v122, v6, v0

    const/16 v0, 0x77

    aput-object v123, v6, v0

    const/16 v0, 0x78

    aput-object v124, v6, v0

    const/16 v0, 0x79

    aput-object v125, v6, v0

    const/16 v0, 0x7a

    aput-object v126, v6, v0

    const/16 v0, 0x7b

    aput-object v127, v6, v0

    const/16 v0, 0x7c

    aput-object v128, v6, v0

    const/16 v0, 0x7d

    aput-object v129, v6, v0

    const/16 v0, 0x7e

    aput-object v130, v6, v0

    const/16 v0, 0x7f

    aput-object v131, v6, v0

    const/16 v0, 0x80

    aput-object v132, v6, v0

    const/16 v0, 0x81

    aput-object v133, v6, v0

    const/16 v0, 0x82

    aput-object v134, v6, v0

    const/16 v0, 0x83

    aput-object v135, v6, v0

    const/16 v0, 0x84

    aput-object v136, v6, v0

    const/16 v0, 0x85

    aput-object v137, v6, v0

    const/16 v0, 0x86

    aput-object v138, v6, v0

    const/16 v0, 0x87

    aput-object v139, v6, v0

    const/16 v0, 0x88

    aput-object v140, v6, v0

    const/16 v0, 0x89

    aput-object v141, v6, v0

    const/16 v0, 0x8a

    aput-object v142, v6, v0

    const/16 v0, 0x8b

    aput-object v143, v6, v0

    const/16 v0, 0x8c

    aput-object v144, v6, v0

    const/16 v0, 0x8d

    aput-object v145, v6, v0

    const/16 v0, 0x8e

    aput-object v146, v6, v0

    const/16 v0, 0x8f

    aput-object v147, v6, v0

    const/16 v0, 0x90

    aput-object v148, v6, v0

    const/16 v0, 0x91

    aput-object v149, v6, v0

    const/16 v0, 0x92

    aput-object v150, v6, v0

    const/16 v0, 0x93

    aput-object v151, v6, v0

    const/16 v0, 0x94

    aput-object v152, v6, v0

    const/16 v0, 0x95

    aput-object v153, v6, v0

    const/16 v0, 0x96

    aput-object v154, v6, v0

    const/16 v0, 0x97

    aput-object v155, v6, v0

    const/16 v0, 0x98

    aput-object v156, v6, v0

    const/16 v0, 0x99

    aput-object v157, v6, v0

    const/16 v0, 0x9a

    aput-object v158, v6, v0

    const/16 v0, 0x9b

    aput-object v159, v6, v0

    const/16 v0, 0x9c

    aput-object v160, v6, v0

    const/16 v0, 0x9d

    aput-object v161, v6, v0

    const/16 v0, 0x9e

    aput-object v162, v6, v0

    const/16 v0, 0x9f

    aput-object v163, v6, v0

    const/16 v0, 0xa0

    aput-object v164, v6, v0

    const/16 v0, 0xa1

    aput-object v165, v6, v0

    const/16 v0, 0xa2

    aput-object v166, v6, v0

    const/16 v0, 0xa3

    aput-object v167, v6, v0

    const/16 v0, 0xa4

    aput-object v168, v6, v0

    const/16 v0, 0xa5

    aput-object v169, v6, v0

    const/16 v0, 0xa6

    aput-object v2, v6, v0

    const/16 v0, 0xa7

    aput-object v4, v6, v0

    .line 169
    sput-object v6, Lcom/intsig/camscanner/purchase/entity/Function;->$VALUES:[Lcom/intsig/camscanner/purchase/entity/Function;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 3
    iput-object p3, p0, Lcom/intsig/camscanner/purchase/entity/Function;->mFromWhere:Ljava/lang/String;

    return-void
.end method

.method public static getFunction(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/entity/Function;
    .locals 2

    .line 1
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    const/4 v1, -0x1

    .line 9
    sparse-switch v0, :sswitch_data_0

    .line 10
    .line 11
    .line 12
    goto :goto_0

    .line 13
    :sswitch_0
    const-string v0, "local_only"

    .line 14
    .line 15
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 16
    .line 17
    .line 18
    move-result p0

    .line 19
    if-nez p0, :cond_0

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    const/4 v1, 0x6

    .line 23
    goto :goto_0

    .line 24
    :sswitch_1
    const-string v0, "pdf_password"

    .line 25
    .line 26
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 27
    .line 28
    .line 29
    move-result p0

    .line 30
    if-nez p0, :cond_1

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_1
    const/4 v1, 0x5

    .line 34
    goto :goto_0

    .line 35
    :sswitch_2
    const-string v0, "add_watermark"

    .line 36
    .line 37
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 38
    .line 39
    .line 40
    move-result p0

    .line 41
    if-nez p0, :cond_2

    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_2
    const/4 v1, 0x4

    .line 45
    goto :goto_0

    .line 46
    :sswitch_3
    const-string v0, "icloud_recover"

    .line 47
    .line 48
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 49
    .line 50
    .line 51
    move-result p0

    .line 52
    if-nez p0, :cond_3

    .line 53
    .line 54
    goto :goto_0

    .line 55
    :cond_3
    const/4 v1, 0x3

    .line 56
    goto :goto_0

    .line 57
    :sswitch_4
    const-string v0, "icloud_space"

    .line 58
    .line 59
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 60
    .line 61
    .line 62
    move-result p0

    .line 63
    if-nez p0, :cond_4

    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_4
    const/4 v1, 0x2

    .line 67
    goto :goto_0

    .line 68
    :sswitch_5
    const-string v0, "folder"

    .line 69
    .line 70
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 71
    .line 72
    .line 73
    move-result p0

    .line 74
    if-nez p0, :cond_5

    .line 75
    .line 76
    goto :goto_0

    .line 77
    :cond_5
    const/4 v1, 0x1

    .line 78
    goto :goto_0

    .line 79
    :sswitch_6
    const-string/jumbo v0, "upgrade_all"

    .line 80
    .line 81
    .line 82
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 83
    .line 84
    .line 85
    move-result p0

    .line 86
    if-nez p0, :cond_6

    .line 87
    .line 88
    goto :goto_0

    .line 89
    :cond_6
    const/4 v1, 0x0

    .line 90
    :goto_0
    packed-switch v1, :pswitch_data_0

    .line 91
    .line 92
    .line 93
    sget-object p0, Lcom/intsig/camscanner/purchase/entity/Function;->NONE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 94
    .line 95
    goto :goto_1

    .line 96
    :pswitch_0
    sget-object p0, Lcom/intsig/camscanner/purchase/entity/Function;->LOCAL_ONLY:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 97
    .line 98
    goto :goto_1

    .line 99
    :pswitch_1
    sget-object p0, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_PDF_PASSWORD:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 100
    .line 101
    goto :goto_1

    .line 102
    :pswitch_2
    sget-object p0, Lcom/intsig/camscanner/purchase/entity/Function;->ADD_WATERMARK:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 103
    .line 104
    goto :goto_1

    .line 105
    :pswitch_3
    sget-object p0, Lcom/intsig/camscanner/purchase/entity/Function;->ICLOUD_RECOVER:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 106
    .line 107
    goto :goto_1

    .line 108
    :pswitch_4
    sget-object p0, Lcom/intsig/camscanner/purchase/entity/Function;->ICLOUD_SPACE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 109
    .line 110
    goto :goto_1

    .line 111
    :pswitch_5
    sget-object p0, Lcom/intsig/camscanner/purchase/entity/Function;->FOLDER:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 112
    .line 113
    goto :goto_1

    .line 114
    :pswitch_6
    sget-object p0, Lcom/intsig/camscanner/purchase/entity/Function;->UPGRADE_ALL:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 115
    .line 116
    :goto_1
    return-object p0

    .line 117
    :sswitch_data_0
    .sparse-switch
        -0x6ded2b02 -> :sswitch_6
        -0x4ba2e392 -> :sswitch_5
        -0x1e8c358d -> :sswitch_4
        0xc215f91 -> :sswitch_3
        0x24e84646 -> :sswitch_2
        0x2a402b48 -> :sswitch_1
        0x4db2e400 -> :sswitch_0
    .end sparse-switch

    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/entity/Function;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/camscanner/purchase/entity/Function;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/camscanner/purchase/entity/Function;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static values()[Lcom/intsig/camscanner/purchase/entity/Function;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/purchase/entity/Function;->$VALUES:[Lcom/intsig/camscanner/purchase/entity/Function;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/intsig/camscanner/purchase/entity/Function;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/camscanner/purchase/entity/Function;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method


# virtual methods
.method public fromCertificateType()Z
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/purchase/entity/Function$1;->〇080:[I

    .line 2
    .line 3
    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    aget v0, v0, v1

    .line 8
    .line 9
    packed-switch v0, :pswitch_data_0

    .line 10
    .line 11
    .line 12
    const/4 v0, 0x0

    .line 13
    return v0

    .line 14
    :pswitch_0
    const/4 v0, 0x1

    .line 15
    return v0

    .line 16
    nop

    .line 17
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public isNotNone()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/purchase/entity/Function;->NONE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 2
    .line 3
    if-eq p0, v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v0, 0x0

    .line 8
    :goto_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public setValue(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/entity/Function;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/entity/Function;->mFromWhere:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public toTrackerValue()Ljava/lang/String;
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/purchase/entity/Function$1;->〇080:[I

    .line 2
    .line 3
    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    aget v0, v0, v1

    .line 8
    .line 9
    const-string v1, "add_signature"

    .line 10
    .line 11
    packed-switch v0, :pswitch_data_0

    .line 12
    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/entity/Function;->mFromWhere:Ljava/lang/String;

    .line 15
    .line 16
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-nez v0, :cond_0

    .line 21
    .line 22
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/entity/Function;->mFromWhere:Ljava/lang/String;

    .line 23
    .line 24
    goto/16 :goto_0

    .line 25
    .line 26
    :pswitch_0
    const-string v1, "selfsearch"

    .line 27
    .line 28
    goto/16 :goto_0

    .line 29
    .line 30
    :pswitch_1
    const-string v1, "cs_new_coupon_pop"

    .line 31
    .line 32
    goto/16 :goto_0

    .line 33
    .line 34
    :pswitch_2
    const-string v1, "greeting_card"

    .line 35
    .line 36
    goto/16 :goto_0

    .line 37
    .line 38
    :pswitch_3
    const-string v1, "document_compression"

    .line 39
    .line 40
    goto/16 :goto_0

    .line 41
    .line 42
    :pswitch_4
    const-string v1, "pdf_pics_limit"

    .line 43
    .line 44
    goto/16 :goto_0

    .line 45
    .line 46
    :pswitch_5
    const-string v1, "add_long_pic_watermark"

    .line 47
    .line 48
    goto/16 :goto_0

    .line 49
    .line 50
    :pswitch_6
    const-string v1, "long_pic_watermark_free"

    .line 51
    .line 52
    goto/16 :goto_0

    .line 53
    .line 54
    :pswitch_7
    const-string v1, "30+_user_marketing"

    .line 55
    .line 56
    goto/16 :goto_0

    .line 57
    .line 58
    :pswitch_8
    const-string v1, "main_week"

    .line 59
    .line 60
    goto/16 :goto_0

    .line 61
    .line 62
    :pswitch_9
    const-string v1, "marketing"

    .line 63
    .line 64
    goto/16 :goto_0

    .line 65
    .line 66
    :pswitch_a
    const-string v1, "book"

    .line 67
    .line 68
    goto/16 :goto_0

    .line 69
    .line 70
    :pswitch_b
    const-string v1, "batch_ocr"

    .line 71
    .line 72
    goto/16 :goto_0

    .line 73
    .line 74
    :pswitch_c
    const-string v1, "excel_ocr"

    .line 75
    .line 76
    goto/16 :goto_0

    .line 77
    .line 78
    :pswitch_d
    const-string v1, "add_security_watermark"

    .line 79
    .line 80
    goto/16 :goto_0

    .line 81
    .line 82
    :pswitch_e
    const-string v1, "TextMainpagePremium"

    .line 83
    .line 84
    goto/16 :goto_0

    .line 85
    .line 86
    :pswitch_f
    const-string v1, "operation_ad"

    .line 87
    .line 88
    goto/16 :goto_0

    .line 89
    .line 90
    :pswitch_10
    const-string v1, "pdf_import"

    .line 91
    .line 92
    goto/16 :goto_0

    .line 93
    .line 94
    :pswitch_11
    const-string v1, "id_photo"

    .line 95
    .line 96
    goto/16 :goto_0

    .line 97
    .line 98
    :pswitch_12
    const-string/jumbo v1, "transfer_word"

    .line 99
    .line 100
    .line 101
    goto/16 :goto_0

    .line 102
    .line 103
    :pswitch_13
    const-string v1, "purchasevipgp"

    .line 104
    .line 105
    goto/16 :goto_0

    .line 106
    .line 107
    :pswitch_14
    const-string v1, "import_formalbum"

    .line 108
    .line 109
    goto/16 :goto_0

    .line 110
    .line 111
    :pswitch_15
    const-string v1, "localonly_freedoc"

    .line 112
    .line 113
    goto/16 :goto_0

    .line 114
    .line 115
    :pswitch_16
    const-string v1, "localonly"

    .line 116
    .line 117
    goto/16 :goto_0

    .line 118
    .line 119
    :pswitch_17
    const-string v1, "add_page_num"

    .line 120
    .line 121
    goto/16 :goto_0

    .line 122
    .line 123
    :pswitch_18
    const-string v1, "pdf_password"

    .line 124
    .line 125
    goto/16 :goto_0

    .line 126
    .line 127
    :pswitch_19
    const-string/jumbo v1, "text_recognition"

    .line 128
    .line 129
    .line 130
    goto/16 :goto_0

    .line 131
    .line 132
    :pswitch_1a
    const-string v1, "qbook_mode"

    .line 133
    .line 134
    goto/16 :goto_0

    .line 135
    .line 136
    :pswitch_1b
    const-string v1, "icon"

    .line 137
    .line 138
    goto/16 :goto_0

    .line 139
    .line 140
    :pswitch_1c
    const-string v1, "proofread"

    .line 141
    .line 142
    goto/16 :goto_0

    .line 143
    .line 144
    :pswitch_1d
    const-string/jumbo v1, "translate"

    .line 145
    .line 146
    .line 147
    goto :goto_0

    .line 148
    :pswitch_1e
    const-string v1, "ocr_advance"

    .line 149
    .line 150
    goto :goto_0

    .line 151
    :pswitch_1f
    const-string v1, "ad_free"

    .line 152
    .line 153
    goto :goto_0

    .line 154
    :pswitch_20
    const-string v1, "folder_limit"

    .line 155
    .line 156
    goto :goto_0

    .line 157
    :pswitch_21
    const-string v1, "add_folder"

    .line 158
    .line 159
    goto :goto_0

    .line 160
    :pswitch_22
    const-string v1, "full_hd"

    .line 161
    .line 162
    goto :goto_0

    .line 163
    :pswitch_23
    const-string v1, "encrypted_link"

    .line 164
    .line 165
    goto :goto_0

    .line 166
    :pswitch_24
    const-string v1, "auto_upload"

    .line 167
    .line 168
    goto :goto_0

    .line 169
    :pswitch_25
    const-string v1, "share_pic"

    .line 170
    .line 171
    goto :goto_0

    .line 172
    :pswitch_26
    const-string v1, "pdf_watermark_free"

    .line 173
    .line 174
    goto :goto_0

    .line 175
    :pswitch_27
    const-string v1, "cloud_space"

    .line 176
    .line 177
    goto :goto_0

    .line 178
    :pswitch_28
    const-string v1, "collage"

    .line 179
    .line 180
    goto :goto_0

    .line 181
    :pswitch_29
    const-string v1, "ocr"

    .line 182
    .line 183
    goto :goto_0

    .line 184
    :pswitch_2a
    const-string v1, "one_page_id"

    .line 185
    .line 186
    goto :goto_0

    .line 187
    :pswitch_2b
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/entity/Function;->mFromWhere:Ljava/lang/String;

    .line 188
    .line 189
    goto :goto_0

    .line 190
    :pswitch_2c
    const-string v1, "bank_card"

    .line 191
    .line 192
    goto :goto_0

    .line 193
    :pswitch_2d
    const-string v1, "oversea_idcard"

    .line 194
    .line 195
    goto :goto_0

    .line 196
    :pswitch_2e
    const-string v1, "oversea_driver"

    .line 197
    .line 198
    goto :goto_0

    .line 199
    :pswitch_2f
    const-string v1, "house"

    .line 200
    .line 201
    goto :goto_0

    .line 202
    :pswitch_30
    const-string v1, "china_driver"

    .line 203
    .line 204
    goto :goto_0

    .line 205
    :pswitch_31
    const-string v1, "passport"

    .line 206
    .line 207
    goto :goto_0

    .line 208
    :pswitch_32
    const-string v1, "household_register"

    .line 209
    .line 210
    goto :goto_0

    .line 211
    :pswitch_33
    const-string v1, "id_card"

    .line 212
    .line 213
    goto :goto_0

    .line 214
    :pswitch_34
    const-string v1, "scandoneidcard"

    .line 215
    .line 216
    goto :goto_0

    .line 217
    :pswitch_35
    const-string v1, "idcard"

    .line 218
    .line 219
    goto :goto_0

    .line 220
    :cond_0
    const-string v1, ""

    .line 221
    .line 222
    :goto_0
    :pswitch_36
    return-object v1

    .line 223
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_35
        :pswitch_35
        :pswitch_34
        :pswitch_33
        :pswitch_32
        :pswitch_31
        :pswitch_30
        :pswitch_2f
        :pswitch_2e
        :pswitch_2d
        :pswitch_2c
        :pswitch_2b
        :pswitch_2b
        :pswitch_2b
        :pswitch_2b
        :pswitch_2b
        :pswitch_2b
        :pswitch_2b
        :pswitch_2b
        :pswitch_2b
        :pswitch_2b
        :pswitch_2a
        :pswitch_29
        :pswitch_29
        :pswitch_28
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_26
        :pswitch_25
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_36
        :pswitch_36
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_2b
        :pswitch_2b
        :pswitch_2b
        :pswitch_2b
        :pswitch_2b
        :pswitch_2b
        :pswitch_2b
        :pswitch_2b
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
.end method

.method public toTypePara()Ljava/lang/String;
    .locals 2
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/purchase/entity/Function$1;->〇080:[I

    .line 2
    .line 3
    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    aget v0, v0, v1

    .line 8
    .line 9
    const/4 v1, 0x6

    .line 10
    if-eq v0, v1, :cond_3

    .line 11
    .line 12
    const/16 v1, 0x16

    .line 13
    .line 14
    if-eq v0, v1, :cond_2

    .line 15
    .line 16
    const/16 v1, 0x9

    .line 17
    .line 18
    if-eq v0, v1, :cond_1

    .line 19
    .line 20
    const/16 v1, 0xa

    .line 21
    .line 22
    if-eq v0, v1, :cond_0

    .line 23
    .line 24
    const/4 v0, 0x0

    .line 25
    return-object v0

    .line 26
    :cond_0
    const-string v0, "id_mode"

    .line 27
    .line 28
    return-object v0

    .line 29
    :cond_1
    const-string v0, "driver_licence"

    .line 30
    .line 31
    return-object v0

    .line 32
    :cond_2
    const-string/jumbo v0, "single_side"

    .line 33
    .line 34
    .line 35
    return-object v0

    .line 36
    :cond_3
    const-string v0, "passport"

    .line 37
    .line 38
    return-object v0
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

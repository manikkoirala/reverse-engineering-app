.class public final enum Lcom/intsig/camscanner/purchase/track/FunctionType;
.super Ljava/lang/Enum;
.source "FunctionType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/camscanner/purchase/track/FunctionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/camscanner/purchase/track/FunctionType;

.field public static final enum AD:Lcom/intsig/camscanner/purchase/track/FunctionType;

.field public static final enum BUY_ONCE:Lcom/intsig/camscanner/purchase/track/FunctionType;

.field public static final enum GUIDE_ALIPAY_TYPE:Lcom/intsig/camscanner/purchase/track/FunctionType;

.field public static final enum GUIDE_PREMIUM_MARKETING:Lcom/intsig/camscanner/purchase/track/FunctionType;

.field public static final enum GUIDE_PREMIUM_TYPE:Lcom/intsig/camscanner/purchase/track/FunctionType;

.field public static final enum GUIDE_WX_TYPE:Lcom/intsig/camscanner/purchase/track/FunctionType;

.field public static final enum MONTH_SUBSCRIPTION:Lcom/intsig/camscanner/purchase/track/FunctionType;

.field public static final enum NONE:Lcom/intsig/camscanner/purchase/track/FunctionType;

.field public static final enum SELF_SEARCH_GUIDE:Lcom/intsig/camscanner/purchase/track/FunctionType;

.field public static final enum YEAR_2_99:Lcom/intsig/camscanner/purchase/track/FunctionType;

.field public static final enum YEAR_DOUBLE_SUBSCRIPTION:Lcom/intsig/camscanner/purchase/track/FunctionType;

.field public static final enum YEAR_SUBSCRIPTION:Lcom/intsig/camscanner/purchase/track/FunctionType;


# instance fields
.field o0:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 16

    .line 1
    new-instance v0, Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 2
    .line 3
    const-string v1, ""

    .line 4
    .line 5
    const-string v2, "NONE"

    .line 6
    .line 7
    const/4 v3, 0x0

    .line 8
    invoke-direct {v0, v2, v3, v1}, Lcom/intsig/camscanner/purchase/track/FunctionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 9
    .line 10
    .line 11
    sput-object v0, Lcom/intsig/camscanner/purchase/track/FunctionType;->NONE:Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 12
    .line 13
    new-instance v1, Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 14
    .line 15
    const-string v2, "month_subscription"

    .line 16
    .line 17
    const-string v4, "MONTH_SUBSCRIPTION"

    .line 18
    .line 19
    const/4 v5, 0x1

    .line 20
    invoke-direct {v1, v4, v5, v2}, Lcom/intsig/camscanner/purchase/track/FunctionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 21
    .line 22
    .line 23
    sput-object v1, Lcom/intsig/camscanner/purchase/track/FunctionType;->MONTH_SUBSCRIPTION:Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 24
    .line 25
    new-instance v2, Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 26
    .line 27
    const-string/jumbo v4, "year_double_subscription"

    .line 28
    .line 29
    .line 30
    const-string v6, "YEAR_DOUBLE_SUBSCRIPTION"

    .line 31
    .line 32
    const/4 v7, 0x2

    .line 33
    invoke-direct {v2, v6, v7, v4}, Lcom/intsig/camscanner/purchase/track/FunctionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 34
    .line 35
    .line 36
    sput-object v2, Lcom/intsig/camscanner/purchase/track/FunctionType;->YEAR_DOUBLE_SUBSCRIPTION:Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 37
    .line 38
    new-instance v4, Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 39
    .line 40
    const-string/jumbo v6, "year_subscription"

    .line 41
    .line 42
    .line 43
    const-string v8, "YEAR_SUBSCRIPTION"

    .line 44
    .line 45
    const/4 v9, 0x3

    .line 46
    invoke-direct {v4, v8, v9, v6}, Lcom/intsig/camscanner/purchase/track/FunctionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 47
    .line 48
    .line 49
    sput-object v4, Lcom/intsig/camscanner/purchase/track/FunctionType;->YEAR_SUBSCRIPTION:Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 50
    .line 51
    new-instance v6, Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 52
    .line 53
    const-string v8, "guide_premium_marketing"

    .line 54
    .line 55
    const-string v10, "GUIDE_PREMIUM_MARKETING"

    .line 56
    .line 57
    const/4 v11, 0x4

    .line 58
    invoke-direct {v6, v10, v11, v8}, Lcom/intsig/camscanner/purchase/track/FunctionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 59
    .line 60
    .line 61
    sput-object v6, Lcom/intsig/camscanner/purchase/track/FunctionType;->GUIDE_PREMIUM_MARKETING:Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 62
    .line 63
    new-instance v8, Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 64
    .line 65
    const-string v10, "guide_premium"

    .line 66
    .line 67
    const-string v12, "GUIDE_PREMIUM_TYPE"

    .line 68
    .line 69
    const/4 v13, 0x5

    .line 70
    invoke-direct {v8, v12, v13, v10}, Lcom/intsig/camscanner/purchase/track/FunctionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 71
    .line 72
    .line 73
    sput-object v8, Lcom/intsig/camscanner/purchase/track/FunctionType;->GUIDE_PREMIUM_TYPE:Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 74
    .line 75
    new-instance v10, Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 76
    .line 77
    const-string v12, "alipay"

    .line 78
    .line 79
    const-string v14, "GUIDE_ALIPAY_TYPE"

    .line 80
    .line 81
    const/4 v15, 0x6

    .line 82
    invoke-direct {v10, v14, v15, v12}, Lcom/intsig/camscanner/purchase/track/FunctionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 83
    .line 84
    .line 85
    sput-object v10, Lcom/intsig/camscanner/purchase/track/FunctionType;->GUIDE_ALIPAY_TYPE:Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 86
    .line 87
    new-instance v12, Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 88
    .line 89
    const-string/jumbo v14, "wx"

    .line 90
    .line 91
    .line 92
    const-string v15, "GUIDE_WX_TYPE"

    .line 93
    .line 94
    const/4 v13, 0x7

    .line 95
    invoke-direct {v12, v15, v13, v14}, Lcom/intsig/camscanner/purchase/track/FunctionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 96
    .line 97
    .line 98
    sput-object v12, Lcom/intsig/camscanner/purchase/track/FunctionType;->GUIDE_WX_TYPE:Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 99
    .line 100
    new-instance v14, Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 101
    .line 102
    const-string/jumbo v15, "year_2_99"

    .line 103
    .line 104
    .line 105
    const-string v13, "YEAR_2_99"

    .line 106
    .line 107
    const/16 v11, 0x8

    .line 108
    .line 109
    invoke-direct {v14, v13, v11, v15}, Lcom/intsig/camscanner/purchase/track/FunctionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 110
    .line 111
    .line 112
    sput-object v14, Lcom/intsig/camscanner/purchase/track/FunctionType;->YEAR_2_99:Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 113
    .line 114
    new-instance v13, Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 115
    .line 116
    const-string v15, "buy_once"

    .line 117
    .line 118
    const-string v11, "BUY_ONCE"

    .line 119
    .line 120
    const/16 v9, 0x9

    .line 121
    .line 122
    invoke-direct {v13, v11, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 123
    .line 124
    .line 125
    sput-object v13, Lcom/intsig/camscanner/purchase/track/FunctionType;->BUY_ONCE:Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 126
    .line 127
    new-instance v11, Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 128
    .line 129
    const-string v15, "ad"

    .line 130
    .line 131
    const-string v9, "AD"

    .line 132
    .line 133
    const/16 v7, 0xa

    .line 134
    .line 135
    invoke-direct {v11, v9, v7, v15}, Lcom/intsig/camscanner/purchase/track/FunctionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 136
    .line 137
    .line 138
    sput-object v11, Lcom/intsig/camscanner/purchase/track/FunctionType;->AD:Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 139
    .line 140
    new-instance v9, Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 141
    .line 142
    const-string v15, "selfsearch_guide"

    .line 143
    .line 144
    const-string v7, "SELF_SEARCH_GUIDE"

    .line 145
    .line 146
    const/16 v5, 0xb

    .line 147
    .line 148
    invoke-direct {v9, v7, v5, v15}, Lcom/intsig/camscanner/purchase/track/FunctionType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    .line 149
    .line 150
    .line 151
    sput-object v9, Lcom/intsig/camscanner/purchase/track/FunctionType;->SELF_SEARCH_GUIDE:Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 152
    .line 153
    const/16 v7, 0xc

    .line 154
    .line 155
    new-array v7, v7, [Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 156
    .line 157
    aput-object v0, v7, v3

    .line 158
    .line 159
    const/4 v0, 0x1

    .line 160
    aput-object v1, v7, v0

    .line 161
    .line 162
    const/4 v0, 0x2

    .line 163
    aput-object v2, v7, v0

    .line 164
    .line 165
    const/4 v0, 0x3

    .line 166
    aput-object v4, v7, v0

    .line 167
    .line 168
    const/4 v0, 0x4

    .line 169
    aput-object v6, v7, v0

    .line 170
    .line 171
    const/4 v0, 0x5

    .line 172
    aput-object v8, v7, v0

    .line 173
    .line 174
    const/4 v0, 0x6

    .line 175
    aput-object v10, v7, v0

    .line 176
    .line 177
    const/4 v0, 0x7

    .line 178
    aput-object v12, v7, v0

    .line 179
    .line 180
    const/16 v0, 0x8

    .line 181
    .line 182
    aput-object v14, v7, v0

    .line 183
    .line 184
    const/16 v0, 0x9

    .line 185
    .line 186
    aput-object v13, v7, v0

    .line 187
    .line 188
    const/16 v0, 0xa

    .line 189
    .line 190
    aput-object v11, v7, v0

    .line 191
    .line 192
    aput-object v9, v7, v5

    .line 193
    .line 194
    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionType;->$VALUES:[Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 195
    .line 196
    return-void
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput-object p3, p0, Lcom/intsig/camscanner/purchase/track/FunctionType;->o0:Ljava/lang/String;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/track/FunctionType;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static values()[Lcom/intsig/camscanner/purchase/track/FunctionType;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionType;->$VALUES:[Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/intsig/camscanner/purchase/track/FunctionType;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method


# virtual methods
.method public getValue()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/track/FunctionType;->o0:Ljava/lang/String;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public setValue(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/track/FunctionType;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/track/FunctionType;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

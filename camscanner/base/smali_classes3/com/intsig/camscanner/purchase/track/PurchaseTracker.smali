.class public Lcom/intsig/camscanner/purchase/track/PurchaseTracker;
.super Ljava/lang/Object;
.source "PurchaseTracker.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final TAG:Ljava/lang/String; = "PurchaseTracker"


# instance fields
.field public act_1:I

.field public buy_note:Ljava/lang/String;

.field public couponId:Ljava/lang/String;

.field public entrance:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public extra_params:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public function:Lcom/intsig/camscanner/purchase/entity/Function;

.field public functionStr:Ljava/lang/String;

.field public guide_type:Ljava/lang/String;

.field public id_type:Ljava/lang/String;

.field public pageId:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

.field public page_status:Ljava/lang/String;

.field public page_type:Ljava/lang/String;

.field public pay_type:Ljava/lang/String;

.field public price_config:Ljava/lang/String;

.field public price_copywriting:Ljava/lang/String;

.field public productId:Ljava/lang/String;

.field public scheme:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

.field public scheme_type:Ljava/lang/String;

.field public self_config:Ljava/lang/String;

.field public showExpire:I

.field public times:I

.field public type:Lcom/intsig/camscanner/purchase/track/FunctionType;

.field public typeString:Ljava/lang/String;

.field public user_data:Ljava/lang/String;

.field public user_status:Ljava/lang/String;

.field public user_type:Ljava/lang/String;

.field public vipType:Lcom/intsig/camscanner/purchase/track/FunctionVipType;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    sget-object v0, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->None:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    iput-object v0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 3
    sget-object v0, Lcom/intsig/camscanner/purchase/entity/Function;->NONE:Lcom/intsig/camscanner/purchase/entity/Function;

    iput-object v0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 4
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionType;->NONE:Lcom/intsig/camscanner/purchase/track/FunctionType;

    iput-object v0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->type:Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 5
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionVipType;->NONE:Lcom/intsig/camscanner/purchase/track/FunctionVipType;

    iput-object v0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->vipType:Lcom/intsig/camscanner/purchase/track/FunctionVipType;

    const-string v0, ""

    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->functionStr:Ljava/lang/String;

    .line 7
    sget-object v1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->NONE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    iput-object v1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 8
    sget-object v1, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->NONE:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    iput-object v1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 9
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->couponId:Ljava/lang/String;

    const/4 v1, 0x0

    .line 10
    iput v1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->showExpire:I

    .line 11
    iput v1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->act_1:I

    const/4 v1, -0x1

    .line 12
    iput v1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->times:I

    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->typeString:Ljava/lang/String;

    .line 14
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->buy_note:Ljava/lang/String;

    .line 15
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->price_copywriting:Ljava/lang/String;

    .line 16
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->page_status:Ljava/lang/String;

    .line 17
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->user_status:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/intsig/camscanner/purchase/entity/Function;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)V
    .locals 2

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    sget-object v0, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->None:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    iput-object v0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 20
    sget-object v0, Lcom/intsig/camscanner/purchase/entity/Function;->NONE:Lcom/intsig/camscanner/purchase/entity/Function;

    iput-object v0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 21
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionType;->NONE:Lcom/intsig/camscanner/purchase/track/FunctionType;

    iput-object v0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->type:Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 22
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionVipType;->NONE:Lcom/intsig/camscanner/purchase/track/FunctionVipType;

    iput-object v0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->vipType:Lcom/intsig/camscanner/purchase/track/FunctionVipType;

    const-string v0, ""

    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->functionStr:Ljava/lang/String;

    .line 24
    sget-object v1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->NONE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    iput-object v1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 25
    sget-object v1, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->NONE:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    iput-object v1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 26
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->couponId:Ljava/lang/String;

    const/4 v1, 0x0

    .line 27
    iput v1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->showExpire:I

    .line 28
    iput v1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->act_1:I

    const/4 v1, -0x1

    .line 29
    iput v1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->times:I

    .line 30
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->typeString:Ljava/lang/String;

    .line 31
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->buy_note:Ljava/lang/String;

    .line 32
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->price_copywriting:Ljava/lang/String;

    .line 33
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->page_status:Ljava/lang/String;

    .line 34
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->user_status:Ljava/lang/String;

    .line 35
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 36
    iput-object p2, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    return-void
.end method


# virtual methods
.method public addExtraParams(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;
    .locals 1

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_2

    .line 6
    .line 7
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    goto :goto_0

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->extra_params:Ljava/util/HashMap;

    .line 15
    .line 16
    if-nez v0, :cond_1

    .line 17
    .line 18
    new-instance v0, Ljava/util/HashMap;

    .line 19
    .line 20
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->extra_params:Ljava/util/HashMap;

    .line 24
    .line 25
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->extra_params:Ljava/util/HashMap;

    .line 26
    .line 27
    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    :cond_2
    :goto_0
    return-object p0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public buyNote(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->buy_note:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public entrance(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public function(Lcom/intsig/camscanner/purchase/entity/Function;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public idType(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->id_type:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public pageId(Lcom/intsig/camscanner/purchase/track/PurchasePageId;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public pageStatus(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->page_status:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public priceCopyWriting(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->price_copywriting:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public productId(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->productId:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public scheme(Lcom/intsig/camscanner/purchase/track/PurchaseScheme;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public setCouponId(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->couponId:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public times(I)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->times:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, "PurchaseTracker print :\n {\n     pageId = "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 12
    .line 13
    invoke-virtual {v1}, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->toTrackerValue()Ljava/lang/String;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    const-string v1, ",\n     function = "

    .line 21
    .line 22
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    .line 24
    .line 25
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 26
    .line 27
    invoke-virtual {v1}, Lcom/intsig/camscanner/purchase/entity/Function;->toTrackerValue()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    .line 33
    .line 34
    const-string v1, ",\n     times = "

    .line 35
    .line 36
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    iget v1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->times:I

    .line 40
    .line 41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 42
    .line 43
    .line 44
    const-string v1, ",\n     entrance = "

    .line 45
    .line 46
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    .line 48
    .line 49
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 50
    .line 51
    invoke-virtual {v1}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    const-string v1, ",\n     scheme = "

    .line 59
    .line 60
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 64
    .line 65
    invoke-virtual {v1}, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->toTrackerValue()Ljava/lang/String;

    .line 66
    .line 67
    .line 68
    move-result-object v1

    .line 69
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    const-string v1, ",\n     showExpired = "

    .line 73
    .line 74
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    .line 76
    .line 77
    iget v1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->showExpire:I

    .line 78
    .line 79
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 80
    .line 81
    .line 82
    const-string v1, ",\n     act_1 = "

    .line 83
    .line 84
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    iget v1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->act_1:I

    .line 88
    .line 89
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 90
    .line 91
    .line 92
    const-string v1, ",\n     buy_note = "

    .line 93
    .line 94
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    .line 96
    .line 97
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->buy_note:Ljava/lang/String;

    .line 98
    .line 99
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    const-string v1, ",\n     price_copywriting = "

    .line 103
    .line 104
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 105
    .line 106
    .line 107
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->price_copywriting:Ljava/lang/String;

    .line 108
    .line 109
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    .line 111
    .line 112
    const-string v1, ",\n     couponId = "

    .line 113
    .line 114
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    .line 116
    .line 117
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->couponId:Ljava/lang/String;

    .line 118
    .line 119
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    .line 121
    .line 122
    const-string v1, "\n}"

    .line 123
    .line 124
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    .line 126
    .line 127
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 128
    .line 129
    .line 130
    move-result-object v0

    .line 131
    return-object v0
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public type(Lcom/intsig/camscanner/purchase/track/FunctionType;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->type:Lcom/intsig/camscanner/purchase/track/FunctionType;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public typeString(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->typeString:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public vipType(Lcom/intsig/camscanner/purchase/track/FunctionVipType;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->vipType:Lcom/intsig/camscanner/purchase/track/FunctionVipType;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

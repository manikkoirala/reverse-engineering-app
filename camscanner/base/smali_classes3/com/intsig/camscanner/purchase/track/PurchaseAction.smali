.class public final enum Lcom/intsig/camscanner/purchase/track/PurchaseAction;
.super Ljava/lang/Enum;
.source "PurchaseAction.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/camscanner/purchase/track/PurchaseAction;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum BUY_FAILED:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum BUY_SUCCESS:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum CANCEL:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum CHINAMOBILE:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum CLOSE_POPUP:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum GOOGLEPLAY:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum LIFETIME_BUY:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum MONTH_BUY:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum MONTH_SUBSCRIPTION:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum MONTH_SUBSCRIPTION_FREE:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum MONTH_SUBSCRIPTION_IN_POP:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum NONE:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum OFF_AUTO_BUY:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum POINTS_PUR:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum PRODUCT_ITEM_CLICK:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum SKIP:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum TURN_AUTO_BUY:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum UPGRADE_PERMIUM:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum VIEW_PREMIUM:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum WEEK_BUY:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum WEEK_SUBSCRIPTION:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum WEEK_SUBSCRIPTION_FREE:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum WEIXIN:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum YEAR_BUY:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum YEAR_DOUBLE_SUBSCRIPTION:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum YEAR_SUBSCRIPTION:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum YEAR_SUBSCRIPTION_FREE:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum YEAR_SUBSCRIPTION_IN_POP:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum YINLIAN:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

.field public static final enum ZHIFUBAO:Lcom/intsig/camscanner/purchase/track/PurchaseAction;


# direct methods
.method static constructor <clinit>()V
    .locals 32

    .line 1
    new-instance v0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 2
    .line 3
    const-string v1, "YEAR_SUBSCRIPTION"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 7
    .line 8
    .line 9
    sput-object v0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->YEAR_SUBSCRIPTION:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 10
    .line 11
    new-instance v1, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 12
    .line 13
    const-string v3, "YEAR_SUBSCRIPTION_IN_POP"

    .line 14
    .line 15
    const/4 v4, 0x1

    .line 16
    invoke-direct {v1, v3, v4}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 17
    .line 18
    .line 19
    sput-object v1, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->YEAR_SUBSCRIPTION_IN_POP:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 20
    .line 21
    new-instance v3, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 22
    .line 23
    const-string v5, "MONTH_SUBSCRIPTION"

    .line 24
    .line 25
    const/4 v6, 0x2

    .line 26
    invoke-direct {v3, v5, v6}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 27
    .line 28
    .line 29
    sput-object v3, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->MONTH_SUBSCRIPTION:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 30
    .line 31
    new-instance v5, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 32
    .line 33
    const-string v7, "MONTH_SUBSCRIPTION_IN_POP"

    .line 34
    .line 35
    const/4 v8, 0x3

    .line 36
    invoke-direct {v5, v7, v8}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 37
    .line 38
    .line 39
    sput-object v5, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->MONTH_SUBSCRIPTION_IN_POP:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 40
    .line 41
    new-instance v7, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 42
    .line 43
    const-string v9, "WEEK_SUBSCRIPTION"

    .line 44
    .line 45
    const/4 v10, 0x4

    .line 46
    invoke-direct {v7, v9, v10}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 47
    .line 48
    .line 49
    sput-object v7, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->WEEK_SUBSCRIPTION:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 50
    .line 51
    new-instance v9, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 52
    .line 53
    const-string v11, "WEEK_SUBSCRIPTION_FREE"

    .line 54
    .line 55
    const/4 v12, 0x5

    .line 56
    invoke-direct {v9, v11, v12}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 57
    .line 58
    .line 59
    sput-object v9, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->WEEK_SUBSCRIPTION_FREE:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 60
    .line 61
    new-instance v11, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 62
    .line 63
    const-string v13, "MONTH_SUBSCRIPTION_FREE"

    .line 64
    .line 65
    const/4 v14, 0x6

    .line 66
    invoke-direct {v11, v13, v14}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 67
    .line 68
    .line 69
    sput-object v11, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->MONTH_SUBSCRIPTION_FREE:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 70
    .line 71
    new-instance v13, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 72
    .line 73
    const-string v15, "YEAR_SUBSCRIPTION_FREE"

    .line 74
    .line 75
    const/4 v14, 0x7

    .line 76
    invoke-direct {v13, v15, v14}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 77
    .line 78
    .line 79
    sput-object v13, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->YEAR_SUBSCRIPTION_FREE:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 80
    .line 81
    new-instance v15, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 82
    .line 83
    const-string v14, "MONTH_BUY"

    .line 84
    .line 85
    const/16 v12, 0x8

    .line 86
    .line 87
    invoke-direct {v15, v14, v12}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 88
    .line 89
    .line 90
    sput-object v15, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->MONTH_BUY:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 91
    .line 92
    new-instance v14, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 93
    .line 94
    const-string v12, "YEAR_BUY"

    .line 95
    .line 96
    const/16 v10, 0x9

    .line 97
    .line 98
    invoke-direct {v14, v12, v10}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 99
    .line 100
    .line 101
    sput-object v14, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->YEAR_BUY:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 102
    .line 103
    new-instance v12, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 104
    .line 105
    const-string v10, "LIFETIME_BUY"

    .line 106
    .line 107
    const/16 v8, 0xa

    .line 108
    .line 109
    invoke-direct {v12, v10, v8}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 110
    .line 111
    .line 112
    sput-object v12, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->LIFETIME_BUY:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 113
    .line 114
    new-instance v10, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 115
    .line 116
    const-string v8, "WEEK_BUY"

    .line 117
    .line 118
    const/16 v6, 0xb

    .line 119
    .line 120
    invoke-direct {v10, v8, v6}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 121
    .line 122
    .line 123
    sput-object v10, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->WEEK_BUY:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 124
    .line 125
    new-instance v8, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 126
    .line 127
    const-string v6, "POINTS_PUR"

    .line 128
    .line 129
    const/16 v4, 0xc

    .line 130
    .line 131
    invoke-direct {v8, v6, v4}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 132
    .line 133
    .line 134
    sput-object v8, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->POINTS_PUR:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 135
    .line 136
    new-instance v6, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 137
    .line 138
    const-string v4, "CANCEL"

    .line 139
    .line 140
    const/16 v2, 0xd

    .line 141
    .line 142
    invoke-direct {v6, v4, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 143
    .line 144
    .line 145
    sput-object v6, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->CANCEL:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 146
    .line 147
    new-instance v4, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 148
    .line 149
    const-string v2, "VIEW_PREMIUM"

    .line 150
    .line 151
    move-object/from16 v16, v6

    .line 152
    .line 153
    const/16 v6, 0xe

    .line 154
    .line 155
    invoke-direct {v4, v2, v6}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 156
    .line 157
    .line 158
    sput-object v4, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->VIEW_PREMIUM:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 159
    .line 160
    new-instance v2, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 161
    .line 162
    const-string v6, "TURN_AUTO_BUY"

    .line 163
    .line 164
    move-object/from16 v17, v4

    .line 165
    .line 166
    const/16 v4, 0xf

    .line 167
    .line 168
    invoke-direct {v2, v6, v4}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 169
    .line 170
    .line 171
    sput-object v2, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->TURN_AUTO_BUY:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 172
    .line 173
    new-instance v6, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 174
    .line 175
    const-string v4, "OFF_AUTO_BUY"

    .line 176
    .line 177
    move-object/from16 v18, v2

    .line 178
    .line 179
    const/16 v2, 0x10

    .line 180
    .line 181
    invoke-direct {v6, v4, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 182
    .line 183
    .line 184
    sput-object v6, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->OFF_AUTO_BUY:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 185
    .line 186
    new-instance v4, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 187
    .line 188
    const-string v2, "UPGRADE_PERMIUM"

    .line 189
    .line 190
    move-object/from16 v19, v6

    .line 191
    .line 192
    const/16 v6, 0x11

    .line 193
    .line 194
    invoke-direct {v4, v2, v6}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 195
    .line 196
    .line 197
    sput-object v4, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->UPGRADE_PERMIUM:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 198
    .line 199
    new-instance v2, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 200
    .line 201
    const-string v6, "CLOSE_POPUP"

    .line 202
    .line 203
    move-object/from16 v20, v4

    .line 204
    .line 205
    const/16 v4, 0x12

    .line 206
    .line 207
    invoke-direct {v2, v6, v4}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 208
    .line 209
    .line 210
    sput-object v2, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->CLOSE_POPUP:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 211
    .line 212
    new-instance v6, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 213
    .line 214
    const-string v4, "ZHIFUBAO"

    .line 215
    .line 216
    move-object/from16 v21, v2

    .line 217
    .line 218
    const/16 v2, 0x13

    .line 219
    .line 220
    invoke-direct {v6, v4, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 221
    .line 222
    .line 223
    sput-object v6, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->ZHIFUBAO:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 224
    .line 225
    new-instance v4, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 226
    .line 227
    const-string v2, "WEIXIN"

    .line 228
    .line 229
    move-object/from16 v22, v6

    .line 230
    .line 231
    const/16 v6, 0x14

    .line 232
    .line 233
    invoke-direct {v4, v2, v6}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 234
    .line 235
    .line 236
    sput-object v4, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->WEIXIN:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 237
    .line 238
    new-instance v2, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 239
    .line 240
    const-string v6, "YINLIAN"

    .line 241
    .line 242
    move-object/from16 v23, v4

    .line 243
    .line 244
    const/16 v4, 0x15

    .line 245
    .line 246
    invoke-direct {v2, v6, v4}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 247
    .line 248
    .line 249
    sput-object v2, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->YINLIAN:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 250
    .line 251
    new-instance v6, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 252
    .line 253
    const-string v4, "GOOGLEPLAY"

    .line 254
    .line 255
    move-object/from16 v24, v2

    .line 256
    .line 257
    const/16 v2, 0x16

    .line 258
    .line 259
    invoke-direct {v6, v4, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 260
    .line 261
    .line 262
    sput-object v6, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->GOOGLEPLAY:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 263
    .line 264
    new-instance v2, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 265
    .line 266
    const-string v4, "CHINAMOBILE"

    .line 267
    .line 268
    move-object/from16 v25, v6

    .line 269
    .line 270
    const/16 v6, 0x17

    .line 271
    .line 272
    invoke-direct {v2, v4, v6}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 273
    .line 274
    .line 275
    sput-object v2, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->CHINAMOBILE:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 276
    .line 277
    new-instance v4, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 278
    .line 279
    const-string v6, "BUY_SUCCESS"

    .line 280
    .line 281
    move-object/from16 v26, v2

    .line 282
    .line 283
    const/16 v2, 0x18

    .line 284
    .line 285
    invoke-direct {v4, v6, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 286
    .line 287
    .line 288
    sput-object v4, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->BUY_SUCCESS:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 289
    .line 290
    new-instance v2, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 291
    .line 292
    const-string v6, "BUY_FAILED"

    .line 293
    .line 294
    move-object/from16 v27, v4

    .line 295
    .line 296
    const/16 v4, 0x19

    .line 297
    .line 298
    invoke-direct {v2, v6, v4}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 299
    .line 300
    .line 301
    sput-object v2, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->BUY_FAILED:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 302
    .line 303
    new-instance v4, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 304
    .line 305
    const-string v6, "PRODUCT_ITEM_CLICK"

    .line 306
    .line 307
    move-object/from16 v28, v2

    .line 308
    .line 309
    const/16 v2, 0x1a

    .line 310
    .line 311
    invoke-direct {v4, v6, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 312
    .line 313
    .line 314
    sput-object v4, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->PRODUCT_ITEM_CLICK:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 315
    .line 316
    new-instance v2, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 317
    .line 318
    const-string v6, "YEAR_DOUBLE_SUBSCRIPTION"

    .line 319
    .line 320
    move-object/from16 v29, v4

    .line 321
    .line 322
    const/16 v4, 0x1b

    .line 323
    .line 324
    invoke-direct {v2, v6, v4}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 325
    .line 326
    .line 327
    sput-object v2, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->YEAR_DOUBLE_SUBSCRIPTION:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 328
    .line 329
    new-instance v4, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 330
    .line 331
    const-string v6, "SKIP"

    .line 332
    .line 333
    move-object/from16 v30, v2

    .line 334
    .line 335
    const/16 v2, 0x1c

    .line 336
    .line 337
    invoke-direct {v4, v6, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 338
    .line 339
    .line 340
    sput-object v4, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->SKIP:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 341
    .line 342
    new-instance v2, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 343
    .line 344
    const-string v6, "NONE"

    .line 345
    .line 346
    move-object/from16 v31, v4

    .line 347
    .line 348
    const/16 v4, 0x1d

    .line 349
    .line 350
    invoke-direct {v2, v6, v4}, Lcom/intsig/camscanner/purchase/track/PurchaseAction;-><init>(Ljava/lang/String;I)V

    .line 351
    .line 352
    .line 353
    sput-object v2, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->NONE:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 354
    .line 355
    const/16 v4, 0x1e

    .line 356
    .line 357
    new-array v4, v4, [Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 358
    .line 359
    const/4 v6, 0x0

    .line 360
    aput-object v0, v4, v6

    .line 361
    .line 362
    const/4 v0, 0x1

    .line 363
    aput-object v1, v4, v0

    .line 364
    .line 365
    const/4 v0, 0x2

    .line 366
    aput-object v3, v4, v0

    .line 367
    .line 368
    const/4 v0, 0x3

    .line 369
    aput-object v5, v4, v0

    .line 370
    .line 371
    const/4 v0, 0x4

    .line 372
    aput-object v7, v4, v0

    .line 373
    .line 374
    const/4 v0, 0x5

    .line 375
    aput-object v9, v4, v0

    .line 376
    .line 377
    const/4 v0, 0x6

    .line 378
    aput-object v11, v4, v0

    .line 379
    .line 380
    const/4 v0, 0x7

    .line 381
    aput-object v13, v4, v0

    .line 382
    .line 383
    const/16 v0, 0x8

    .line 384
    .line 385
    aput-object v15, v4, v0

    .line 386
    .line 387
    const/16 v0, 0x9

    .line 388
    .line 389
    aput-object v14, v4, v0

    .line 390
    .line 391
    const/16 v0, 0xa

    .line 392
    .line 393
    aput-object v12, v4, v0

    .line 394
    .line 395
    const/16 v0, 0xb

    .line 396
    .line 397
    aput-object v10, v4, v0

    .line 398
    .line 399
    const/16 v0, 0xc

    .line 400
    .line 401
    aput-object v8, v4, v0

    .line 402
    .line 403
    const/16 v0, 0xd

    .line 404
    .line 405
    aput-object v16, v4, v0

    .line 406
    .line 407
    const/16 v0, 0xe

    .line 408
    .line 409
    aput-object v17, v4, v0

    .line 410
    .line 411
    const/16 v0, 0xf

    .line 412
    .line 413
    aput-object v18, v4, v0

    .line 414
    .line 415
    const/16 v0, 0x10

    .line 416
    .line 417
    aput-object v19, v4, v0

    .line 418
    .line 419
    const/16 v0, 0x11

    .line 420
    .line 421
    aput-object v20, v4, v0

    .line 422
    .line 423
    const/16 v0, 0x12

    .line 424
    .line 425
    aput-object v21, v4, v0

    .line 426
    .line 427
    const/16 v0, 0x13

    .line 428
    .line 429
    aput-object v22, v4, v0

    .line 430
    .line 431
    const/16 v0, 0x14

    .line 432
    .line 433
    aput-object v23, v4, v0

    .line 434
    .line 435
    const/16 v0, 0x15

    .line 436
    .line 437
    aput-object v24, v4, v0

    .line 438
    .line 439
    const/16 v0, 0x16

    .line 440
    .line 441
    aput-object v25, v4, v0

    .line 442
    .line 443
    const/16 v0, 0x17

    .line 444
    .line 445
    aput-object v26, v4, v0

    .line 446
    .line 447
    const/16 v0, 0x18

    .line 448
    .line 449
    aput-object v27, v4, v0

    .line 450
    .line 451
    const/16 v0, 0x19

    .line 452
    .line 453
    aput-object v28, v4, v0

    .line 454
    .line 455
    const/16 v0, 0x1a

    .line 456
    .line 457
    aput-object v29, v4, v0

    .line 458
    .line 459
    const/16 v0, 0x1b

    .line 460
    .line 461
    aput-object v30, v4, v0

    .line 462
    .line 463
    const/16 v0, 0x1c

    .line 464
    .line 465
    aput-object v31, v4, v0

    .line 466
    .line 467
    const/16 v0, 0x1d

    .line 468
    .line 469
    aput-object v2, v4, v0

    .line 470
    .line 471
    sput-object v4, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->$VALUES:[Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 472
    .line 473
    return-void
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static switchTo(Lcom/intsig/comm/purchase/entity/ProductEnum;ZZ)Lcom/intsig/camscanner/purchase/track/PurchaseAction;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/purchase/track/PurchaseAction$1;->〇080:[I

    .line 2
    .line 3
    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    .line 4
    .line 5
    .line 6
    move-result p0

    .line 7
    aget p0, v0, p0

    .line 8
    .line 9
    packed-switch p0, :pswitch_data_0

    .line 10
    .line 11
    .line 12
    sget-object p0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->NONE:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :pswitch_0
    sget-object p0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->POINTS_PUR:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :pswitch_1
    sget-object p0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->LIFETIME_BUY:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :pswitch_2
    sget-object p0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->WEEK_BUY:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :pswitch_3
    if-eqz p1, :cond_0

    .line 25
    .line 26
    sget-object p0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->YEAR_SUBSCRIPTION_FREE:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    sget-object p0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->YEAR_SUBSCRIPTION_IN_POP:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :pswitch_4
    if-eqz p1, :cond_1

    .line 33
    .line 34
    sget-object p0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->MONTH_SUBSCRIPTION_FREE:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_1
    sget-object p0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->MONTH_SUBSCRIPTION_IN_POP:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 38
    .line 39
    goto :goto_0

    .line 40
    :pswitch_5
    if-eqz p2, :cond_2

    .line 41
    .line 42
    sget-object p0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->YEAR_BUY:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 43
    .line 44
    goto :goto_0

    .line 45
    :cond_2
    if-eqz p1, :cond_3

    .line 46
    .line 47
    sget-object p0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->YEAR_SUBSCRIPTION_FREE:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_3
    sget-object p0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->YEAR_SUBSCRIPTION:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 51
    .line 52
    goto :goto_0

    .line 53
    :pswitch_6
    if-eqz p1, :cond_4

    .line 54
    .line 55
    sget-object p0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->YEAR_SUBSCRIPTION_FREE:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 56
    .line 57
    goto :goto_0

    .line 58
    :cond_4
    sget-object p0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->YEAR_SUBSCRIPTION:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 59
    .line 60
    goto :goto_0

    .line 61
    :pswitch_7
    if-eqz p2, :cond_5

    .line 62
    .line 63
    sget-object p0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->MONTH_BUY:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_5
    if-eqz p1, :cond_6

    .line 67
    .line 68
    sget-object p0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->MONTH_SUBSCRIPTION_FREE:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 69
    .line 70
    goto :goto_0

    .line 71
    :cond_6
    sget-object p0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->MONTH_SUBSCRIPTION:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 72
    .line 73
    goto :goto_0

    .line 74
    :pswitch_8
    if-eqz p1, :cond_7

    .line 75
    .line 76
    sget-object p0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->WEEK_SUBSCRIPTION_FREE:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 77
    .line 78
    goto :goto_0

    .line 79
    :cond_7
    sget-object p0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->WEEK_SUBSCRIPTION:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 80
    .line 81
    goto :goto_0

    .line 82
    :pswitch_9
    sget-object p0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->MONTH_SUBSCRIPTION:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 83
    .line 84
    goto :goto_0

    .line 85
    :pswitch_a
    sget-object p0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->YEAR_SUBSCRIPTION:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 86
    .line 87
    :goto_0
    return-object p0

    .line 88
    nop

    .line 89
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_8
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/track/PurchaseAction;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static values()[Lcom/intsig/camscanner/purchase/track/PurchaseAction;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->$VALUES:[Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/intsig/camscanner/purchase/track/PurchaseAction;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method


# virtual methods
.method public toTrackerValue()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->NONE:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 2
    .line 3
    if-ne p0, v0, :cond_0

    .line 4
    .line 5
    const-string v0, ""

    .line 6
    .line 7
    goto :goto_0

    .line 8
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    :goto_0
    return-object v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.class public final enum Lcom/intsig/camscanner/purchase/track/FunctionEntrance;
.super Ljava/lang/Enum;
.source "FunctionEntrance.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/camscanner/purchase/track/FunctionEntrance;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum APP_LAUNCH:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum APP_SHORTCUT_BATCH:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum APP_SHORTCUT_SCAN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum APP_SHORTCUT_SINGLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum APP_SHORTCUT_TO_WORD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum BASIC_SAVE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum BATCH_OCR_RESULT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CERTIFICATE_CS_LIST:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum COUNTDOWN_POP_24H:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_ACCOUNT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_ADS_REWARD_PRE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_ADVANCED_FOLDER_CERTIFICATE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_ADVANCED_FOLDER_CERTIFICATE_RETAKE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_ADVANCE_OCR_RESULT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_ADVANCE_OCR_SEARCH_BUBBLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_BACKUP_FUNCTION:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_BACKUP_FUNCTION_CLOUD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_BATCH_RESULT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_CHANGE_ICON:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_CLOUD_ALERT_BUBBLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_CLOUD_BUY_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_CLOUD_OVERRUN_BUBBLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_CLOUD_SEVER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_COLLAGE_PREVIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_CROP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_ENHANCE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_FAKE_ENTRANCE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_FIRST_SCAN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_FUNCTION_RECOMMEND:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_GUIDE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_GUIDE_MARKETING:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_HAMBURGER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_HIDDEN_EGG:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_HOME:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_HOME_BANNER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_HOME_BUBBLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_HOME_ICON:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_HOME_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_HOME_POP_USER_RECALL_MARKETING:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_ICLOUD_GUIDE_WEB:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_ID_PHOTO_PREVIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_LEFT_ICON:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_LINK_OVERRUN_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_LIST:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_LIST_ID_PHOTO:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_MAIN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_MAIN_ICON:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_MAIN_MORE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_MAIN_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_MORE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_MY_ACCOUNT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_NEW_COMER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_NEW_ESIGN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_NEW_GIFT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_OCCUPATION_TAG:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_OS_PUSH:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_PASSIVE_POP_GIFT_TEST:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_PASSIVE_SAVE_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_PDF_IMPORT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_PREMIUM_MARKETING:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_REMOVE_WATERMARK_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_RENEW_DISCOUNT_MONTH_WEEK_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_RENEW_DISCOUNT_YEAR_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_RENEW_EDUCATION_MONTH_WEEK_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_RENEW_EDUCATION_YEAR_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_RESUBSCRIBE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_SCAN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_SCANDONE_BANNER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_SCAN_TOOLBOX:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_SCREEN_SHOT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_SEDIMENT_BUBBLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_SHARE_DONE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_SMART_ERASE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_UPGRADE_MARKETING_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_USERTAG_RECOMMAND:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_VIDEO_CLOSE_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_YEAR_DISCOUNT_LANDING:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum CS_YEAR_DISCOUNT_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum EMAIL:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum EMAIL_TO_MYSELF:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum EXCEL_EXPORT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FOLDER_LIMIT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FREE_SIGNATURE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_BACK_SCHOOL_GIFT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_BRIEFCASE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_BUBBLE_VIP_EXPIRES:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_BUBBLE_VIP_EXPIRES_WITHIN_FIVE_DAYS:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_CAPTURE_HD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_CARD_BAG:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_CLEANUP_UPGRADE_VIP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_COLLAGE_BANK_CARD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_COLLAGE_CAR_LICENCE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_COLLAGE_DOC_BANNER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_COLLAGE_DRIVE_CARD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_COLLAGE_ID_CARD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_COLLAGE_STYLE_01:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_COUNT_DOWN_PAGE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_COUPON_AD_NEW_USER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_COUPON_BACKFLOW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_COUPON_FAILED_BUY:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_COUPON_GENERAL_DISCOUNT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_COUPON_GENERAL_PRICE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_COUPON_NEW_USER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_COUPON_OLD_USER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_COUPON_QUIT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_COUPON_VIP_EXPIRE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_CS_CLOUD_SPACE_OVER_LIMIT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_CS_DETAIL:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_CS_DIRECTORY:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_CS_DISCOUNT_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_CS_ERROR_CLOUD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_CS_ERROR_FOLDER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_CS_ESIGNATURES_H5:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_CS_GIFT_CARD_INVITE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_CS_GUIDE_TRIAL_UNSUB:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_CS_LIST:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_CS_MAIN_LEFT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_CS_MAIN_TOOLS:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_CS_OCR:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_CS_RESTORE_SHARE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_CS_SCAN_DONE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_CS_SELECT_PATH:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_CS_SHARE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_CS_SPECIAL_FOR_NEW_USER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_CS_UPGRADE_CLOUD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_CS_WORD_PREVIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_DEEP_CLEANUP_UPGRADE_VIP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_EMAIL_SHARE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_FAMILY_STORAGE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_FIRST_SYNC:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_GROWTH_RECORD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_IDCARD_FOLDER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_IMAGE_RECOLOR:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_IMAGE_RESTORE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_IMPORT_COLLAGE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_INSPIRATION_LIBRARY:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_INVITE_GET_VIP_CN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_INVITE_GET_VIP_GP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_JPG_SHARE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_KEEP_A_WHILE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_LIST_DOC_IDCARD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_LOCAL_OCR_NO_RECOGNITION:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_LONG_PIC_PREVIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_MAIN_DOC_IDCARD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_MAIN_DOC_OCR:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_MAIN_LEFT_RENEW_PREMIUM:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_MAIN_PAGE_PURCHASE_VIP_RECALL:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_MAIN_PURCHASE_POP_MORE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_MAIN_RIGHT_TOP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_MAIN_SHOPPING:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_MATERIALS:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_NOT_VIP_PDF_SHARE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_NO_LOGIN_VIP_EXPIRES_WITHIN_FIVE_DAYS:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_OCR_TRAILPOP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_PAD_SETTING_UPGRADE_VIP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_PART_PAD_H5:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_PDF_PACKAGE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_PDF_PACKAGE_LOCAL:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_PREMIUM_LEVEL:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_PRO_PDF_SHARE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_SCANDONE_VIP_GUIDE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_SETTING_VIP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_SPECIAL_MARKET_NEW_REGISTER_VIP_FREE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_SYNC_CLOUD_STORAGE_LIMIT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_TEMPLATE_DIR_PRESET:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_VIP_EXPIRES_WITHIN_FIVE_DAYS:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_VIP_FUNCTION_INTRODUCE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum FROM_WORK_FILE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum GUIDE_PREMIUM:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum GUIDE_PREMIUM_MARKETING:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum GUIDE_VIDEO_PREMIUM:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum IDCARD_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum IDENTIFY_IDCARD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum LEARN_EFFICIENCY:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum LEFT_PREMIUM_ICON:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum LIFE_EFFICIENCY:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum LOOP_COUNTDOWN_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum MAIN_WEEK:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum MORE_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum NONE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum NOT_ENOUGH_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum NO_WATER_EMAIL_MOD01:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum NO_WATER_EMAIL_MOD02:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum NO_WATER_SHARE_BOTTOM:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum NO_WATER_SHARE_SELECT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum NO_WATER_SHARE_SELECT_IMAGE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum NO_WATER_SHARE_TOP_MOD01:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum NO_WATER_SHARE_TOP_MOD02:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum OCR_VIP_GUIDE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum ONE_YUAN_SCAN_BRACKET:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum OPERATION_TYPE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum OTHER_EXPORT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum PDF_COLLAGE_VIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum PDF_PAGE_VIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum PDF_POP_VIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum PDF_SETTING:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum PDF_SHARE_LIMIT_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum PDF_SHARE_PAGE_VIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum PDF_SHARE_REMIND_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum PDF_VIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum PRINTER_SCAN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum PUSH:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum SAVE_AS_EXCEL:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum SAVE_AS_OTHER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum SAVE_AS_WORD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum SCANDONE_BACK:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum SCANDONE_PREMIUMPAGE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum SEDIMENT_COUNTDOWN_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum TEXT_MESSAGE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum WATERMARK_FREE_REMIND_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum WIDGET_DOC42_SCAN_SINGLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum WIDGET_FUNC11_QR:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum WIDGET_FUNC11_SCAN_BATCH:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum WIDGET_FUNC11_SCAN_SINGLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum WIDGET_FUNC22_QR:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum WIDGET_FUNC22_SCAN_BATCH:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum WIDGET_FUNC22_SCAN_SINGLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum WIDGET_SEARCH42_ID_MODE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum WIDGET_SEARCH42_OCR_MODE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum WIDGET_SEARCH42_SCAN_BATCH:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum WIDGET_SEARCH42_SCAN_SINGLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum WORD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum WORD_EXPORT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum WORD_PREVIEW_NEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum WORD_RECOGNIZE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum WORKFLOWS_SHARE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum WORKFLOWS_TOOLS:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum WORK_EFFICIENCY:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field public static final enum YEAR_DISCOUNT_BUBBLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;


# instance fields
.field o0:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 232

    .line 1
    new-instance v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const-string v1, "no_water_share_top_mod01"

    const-string v2, "NO_WATER_SHARE_TOP_MOD01"

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3, v1}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->NO_WATER_SHARE_TOP_MOD01:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 2
    new-instance v1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const-string v2, "no_water_share_top_mod02"

    const-string v4, "NO_WATER_SHARE_TOP_MOD02"

    const/4 v5, 0x1

    invoke-direct {v1, v4, v5, v2}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->NO_WATER_SHARE_TOP_MOD02:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 3
    new-instance v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const-string v4, "no_water_email_mod01"

    const-string v6, "NO_WATER_EMAIL_MOD01"

    const/4 v7, 0x2

    invoke-direct {v2, v6, v7, v4}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->NO_WATER_EMAIL_MOD01:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 4
    new-instance v4, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const-string v6, "no_water_email_mod02"

    const-string v8, "NO_WATER_EMAIL_MOD02"

    const/4 v9, 0x3

    invoke-direct {v4, v8, v9, v6}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->NO_WATER_EMAIL_MOD02:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 5
    new-instance v6, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const-string v8, "no_water_share_bottom"

    const-string v10, "NO_WATER_SHARE_BOTTOM"

    const/4 v11, 0x4

    invoke-direct {v6, v10, v11, v8}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v6, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->NO_WATER_SHARE_BOTTOM:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 6
    new-instance v8, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const-string v10, "no_water_share_select"

    const-string v12, "NO_WATER_SHARE_SELECT"

    const/4 v13, 0x5

    invoke-direct {v8, v12, v13, v10}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v8, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->NO_WATER_SHARE_SELECT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 7
    new-instance v10, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const-string v12, "no_water_share_select_image"

    const-string v14, "NO_WATER_SHARE_SELECT_IMAGE"

    const/4 v15, 0x6

    invoke-direct {v10, v14, v15, v12}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v10, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->NO_WATER_SHARE_SELECT_IMAGE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 8
    new-instance v12, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const-string v14, "identify_idcard"

    const-string v15, "IDENTIFY_IDCARD"

    const/4 v13, 0x7

    invoke-direct {v12, v15, v13, v14}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v12, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->IDENTIFY_IDCARD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 9
    new-instance v14, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const-string v15, "free_signature"

    const-string v13, "FREE_SIGNATURE"

    const/16 v11, 0x8

    invoke-direct {v14, v13, v11, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v14, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FREE_SIGNATURE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 10
    new-instance v13, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const-string v15, "basic_save"

    const-string v11, "BASIC_SAVE"

    const/16 v9, 0x9

    invoke-direct {v13, v11, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v13, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->BASIC_SAVE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 11
    new-instance v11, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const-string v15, "idcard_pop"

    const-string v9, "IDCARD_POP"

    const/16 v7, 0xa

    invoke-direct {v11, v9, v7, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v11, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->IDCARD_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 12
    new-instance v9, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const-string v15, "scandone_premiumpage"

    const-string v7, "SCANDONE_PREMIUMPAGE"

    const/16 v5, 0xb

    invoke-direct {v9, v7, v5, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v9, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->SCANDONE_PREMIUMPAGE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 13
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const-string v15, "left_premium_icon"

    const-string v5, "LEFT_PREMIUM_ICON"

    const/16 v3, 0xc

    invoke-direct {v7, v5, v3, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->LEFT_PREMIUM_ICON:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 14
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const-string v15, "folder_limit"

    const-string v3, "FOLDER_LIMIT"

    move-object/from16 v16, v7

    const/16 v7, 0xd

    invoke-direct {v5, v3, v7, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FOLDER_LIMIT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 15
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const-string v15, "pdf_pop_view"

    const-string v7, "PDF_POP_VIEW"

    move-object/from16 v17, v5

    const/16 v5, 0xe

    invoke-direct {v3, v7, v5, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->PDF_POP_VIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 16
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const-string v15, "pdf_page_view"

    const-string v5, "PDF_PAGE_VIEW"

    move-object/from16 v18, v3

    const/16 v3, 0xf

    invoke-direct {v7, v5, v3, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->PDF_PAGE_VIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 17
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const-string v15, "pdf_share_page_view"

    const-string v3, "PDF_SHARE_PAGE_VIEW"

    move-object/from16 v19, v7

    const/16 v7, 0x10

    invoke-direct {v5, v3, v7, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->PDF_SHARE_PAGE_VIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 18
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const-string v15, "PDF_VIEW"

    const/16 v7, 0x11

    move-object/from16 v20, v5

    const-string v5, "cs_pdf_preview"

    invoke-direct {v3, v15, v7, v5}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->PDF_VIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 19
    new-instance v15, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const-string v7, "pdf_share_remind_pop"

    move-object/from16 v21, v3

    const-string v3, "PDF_SHARE_REMIND_POP"

    move-object/from16 v22, v9

    const/16 v9, 0x12

    invoke-direct {v15, v3, v9, v7}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v15, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->PDF_SHARE_REMIND_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 20
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const-string v7, "cs_video_close_pop"

    const-string v9, "CS_VIDEO_CLOSE_POP"

    move-object/from16 v23, v15

    const/16 v15, 0x13

    invoke-direct {v3, v9, v15, v7}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_VIDEO_CLOSE_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 21
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x14

    const-string v15, "cs_remove_watermark_pop"

    move-object/from16 v24, v3

    const-string v3, "CS_REMOVE_WATERMARK_POP"

    invoke-direct {v7, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_REMOVE_WATERMARK_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 22
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x15

    const-string v15, "cs_more"

    move-object/from16 v25, v7

    const-string v7, "MORE_POP"

    invoke-direct {v3, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->MORE_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 23
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x16

    const-string v15, "cs_pdf_setting"

    move-object/from16 v26, v3

    const-string v3, "PDF_SETTING"

    invoke-direct {v7, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->PDF_SETTING:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 24
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const-string v9, "PDF_COLLAGE_VIEW"

    const/16 v15, 0x17

    invoke-direct {v3, v9, v15, v5}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->PDF_COLLAGE_VIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 25
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x18

    const-string v15, "cs_batch_ocr_result"

    move-object/from16 v27, v3

    const-string v3, "BATCH_OCR_RESULT"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->BATCH_OCR_RESULT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 26
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x19

    const-string v15, "idcard_folder"

    move-object/from16 v28, v5

    const-string v5, "FROM_IDCARD_FOLDER"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_IDCARD_FOLDER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 27
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x1a

    const-string v15, "newcouponpop"

    move-object/from16 v29, v3

    const-string v3, "FROM_COUPON_NEW_USER"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_COUPON_NEW_USER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 28
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x1b

    const-string v15, "ad_vip_retain_pop"

    move-object/from16 v30, v5

    const-string v5, "FROM_COUPON_AD_NEW_USER"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_COUPON_AD_NEW_USER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 29
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x1c

    const-string v15, "csstart_coupon"

    move-object/from16 v31, v3

    const-string v3, "FROM_COUPON_BACKFLOW"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_COUPON_BACKFLOW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 30
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x1d

    const-string v15, "csquit_coupon"

    move-object/from16 v32, v5

    const-string v5, "FROM_COUPON_QUIT"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_COUPON_QUIT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 31
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x1e

    const-string v15, "failedcouponpop"

    move-object/from16 v33, v3

    const-string v3, "FROM_COUPON_FAILED_BUY"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_COUPON_FAILED_BUY:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 32
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x1f

    const-string/jumbo v15, "vip_old_user_coupon_pop"

    move-object/from16 v34, v5

    const-string v5, "FROM_COUPON_OLD_USER"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_COUPON_OLD_USER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 33
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x20

    const-string/jumbo v15, "vip_expire_coupon_pop"

    move-object/from16 v35, v3

    const-string v3, "FROM_COUPON_VIP_EXPIRE"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_COUPON_VIP_EXPIRE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 34
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x21

    const-string v15, "retrieve"

    move-object/from16 v36, v5

    const-string v5, "FROM_MAIN_PAGE_PURCHASE_VIP_RECALL"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_MAIN_PAGE_PURCHASE_VIP_RECALL:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 35
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x22

    const-string v15, "couponpop1"

    move-object/from16 v37, v3

    const-string v3, "FROM_COUPON_GENERAL_PRICE"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_COUPON_GENERAL_PRICE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 36
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x23

    const-string v15, "couponpop2"

    move-object/from16 v38, v5

    const-string v5, "FROM_COUPON_GENERAL_DISCOUNT"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_COUPON_GENERAL_DISCOUNT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 37
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x24

    const-string v15, "jpg_share"

    move-object/from16 v39, v3

    const-string v3, "FROM_JPG_SHARE"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_JPG_SHARE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 38
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x25

    const-string v15, "pdf_share"

    move-object/from16 v40, v5

    const-string v5, "FROM_NOT_VIP_PDF_SHARE"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_NOT_VIP_PDF_SHARE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 39
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x26

    const-string v15, "pro_pdf_share"

    move-object/from16 v41, v3

    const-string v3, "FROM_PRO_PDF_SHARE"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_PRO_PDF_SHARE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 40
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x27

    const-string v15, "collage_entrance"

    move-object/from16 v42, v5

    const-string v5, "FROM_COLLAGE_STYLE_01"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_COLLAGE_STYLE_01:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 41
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x28

    const-string v15, "count_down_page"

    move-object/from16 v43, v3

    const-string v3, "FROM_COUNT_DOWN_PAGE"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_COUNT_DOWN_PAGE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 42
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x29

    const-string v15, "cs_detail"

    move-object/from16 v44, v5

    const-string v5, "FROM_CS_DETAIL"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_DETAIL:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 43
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x2a

    const-string v15, "cs_scan_done"

    move-object/from16 v45, v3

    const-string v3, "FROM_CS_SCAN_DONE"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_SCAN_DONE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 44
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x2b

    const-string v15, "cs_list"

    move-object/from16 v46, v5

    const-string v5, "FROM_CS_LIST"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_LIST:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 45
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x2c

    const-string v15, "cs_directory"

    move-object/from16 v47, v3

    const-string v3, "FROM_CS_DIRECTORY"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_DIRECTORY:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 46
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x2d

    const-string v15, "cs_ocr_result"

    move-object/from16 v48, v5

    const-string v5, "FROM_CS_OCR"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_OCR:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 47
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x2e

    const-string v15, "ocr_TrailPop"

    move-object/from16 v49, v3

    const-string v3, "FROM_OCR_TRAILPOP"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_OCR_TRAILPOP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 48
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x2f

    const-string v15, "import_collage"

    move-object/from16 v50, v5

    const-string v5, "FROM_IMPORT_COLLAGE"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_IMPORT_COLLAGE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 49
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x30

    const-string v15, "main_doc_idcard"

    move-object/from16 v51, v3

    const-string v3, "FROM_MAIN_DOC_IDCARD"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_MAIN_DOC_IDCARD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 50
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x31

    const-string v15, "main_doc_text_recognition"

    move-object/from16 v52, v5

    const-string v5, "FROM_MAIN_DOC_OCR"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_MAIN_DOC_OCR:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 51
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x32

    const-string v15, "list_doc_idcard"

    move-object/from16 v53, v3

    const-string v3, "FROM_LIST_DOC_IDCARD"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_LIST_DOC_IDCARD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 52
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x33

    const-string/jumbo v15, "special_market_register_vip_free"

    move-object/from16 v54, v5

    const-string v5, "FROM_SPECIAL_MARKET_NEW_REGISTER_VIP_FREE"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_SPECIAL_MARKET_NEW_REGISTER_VIP_FREE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 53
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x34

    const-string v15, "invite_get_vip_cn"

    move-object/from16 v55, v3

    const-string v3, "FROM_INVITE_GET_VIP_CN"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_INVITE_GET_VIP_CN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 54
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x35

    const-string v15, "invite_get_vip_gp"

    move-object/from16 v56, v5

    const-string v5, "FROM_INVITE_GET_VIP_GP"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_INVITE_GET_VIP_GP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 55
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x36

    const-string v15, "main_right_top"

    move-object/from16 v57, v3

    const-string v3, "FROM_MAIN_RIGHT_TOP"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_MAIN_RIGHT_TOP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 56
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x37

    const-string v15, "main_shopping"

    move-object/from16 v58, v5

    const-string v5, "FROM_MAIN_SHOPPING"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_MAIN_SHOPPING:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 57
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x38

    const-string v15, "main_left_renew_premium"

    move-object/from16 v59, v3

    const-string v3, "FROM_MAIN_LEFT_RENEW_PREMIUM"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_MAIN_LEFT_RENEW_PREMIUM:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 58
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x39

    const-string v15, "no_login_vip_expires_within_five_days"

    move-object/from16 v60, v5

    const-string v5, "FROM_NO_LOGIN_VIP_EXPIRES_WITHIN_FIVE_DAYS"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_NO_LOGIN_VIP_EXPIRES_WITHIN_FIVE_DAYS:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 59
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x3a

    const-string v15, "account_vip_expires_within_five_days"

    move-object/from16 v61, v3

    const-string v3, "FROM_VIP_EXPIRES_WITHIN_FIVE_DAYS"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_VIP_EXPIRES_WITHIN_FIVE_DAYS:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 60
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x3b

    const-string v15, "bubble_vip_expires_within_five_days"

    move-object/from16 v62, v5

    const-string v5, "FROM_BUBBLE_VIP_EXPIRES_WITHIN_FIVE_DAYS"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_BUBBLE_VIP_EXPIRES_WITHIN_FIVE_DAYS:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 61
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x3c

    const-string v15, "bubble_vip_expires"

    move-object/from16 v63, v3

    const-string v3, "FROM_BUBBLE_VIP_EXPIRES"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_BUBBLE_VIP_EXPIRES:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 62
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x3d

    const-string v15, "cleanup_upgrade_vip"

    move-object/from16 v64, v5

    const-string v5, "FROM_CLEANUP_UPGRADE_VIP"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CLEANUP_UPGRADE_VIP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 63
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x3e

    const-string v15, "deep_cleanup_upgrade_vip"

    move-object/from16 v65, v3

    const-string v3, "FROM_DEEP_CLEANUP_UPGRADE_VIP"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_DEEP_CLEANUP_UPGRADE_VIP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 64
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x3f

    const-string v15, "pad_setting_upgrade_vip"

    move-object/from16 v66, v5

    const-string v5, "FROM_PAD_SETTING_UPGRADE_VIP"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_PAD_SETTING_UPGRADE_VIP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 65
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x40

    const-string v15, "local_ocr_no_recognition"

    move-object/from16 v67, v3

    const-string v3, "FROM_LOCAL_OCR_NO_RECOGNITION"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_LOCAL_OCR_NO_RECOGNITION:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 66
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x41

    const-string/jumbo v15, "sync_cloud_storage_limit"

    move-object/from16 v68, v5

    const-string v5, "FROM_SYNC_CLOUD_STORAGE_LIMIT"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_SYNC_CLOUD_STORAGE_LIMIT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 67
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x42

    const-string/jumbo v15, "vip_function_introduce"

    move-object/from16 v69, v3

    const-string v3, "FROM_VIP_FUNCTION_INTRODUCE"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_VIP_FUNCTION_INTRODUCE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 68
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x43

    const-string v15, "main_purchase_pop_more"

    move-object/from16 v70, v5

    const-string v5, "FROM_MAIN_PURCHASE_POP_MORE"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_MAIN_PURCHASE_POP_MORE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 69
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x44

    const-string v15, "main_setting_vip"

    move-object/from16 v71, v3

    const-string v3, "FROM_SETTING_VIP"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_SETTING_VIP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 70
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x45

    const-string v15, "cs_scan_hd"

    move-object/from16 v72, v5

    const-string v5, "FROM_CAPTURE_HD"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CAPTURE_HD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 71
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x46

    const-string v15, "collage_id_card"

    move-object/from16 v73, v3

    const-string v3, "FROM_COLLAGE_ID_CARD"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_COLLAGE_ID_CARD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 72
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x47

    const-string v15, "collage_drive_licence"

    move-object/from16 v74, v5

    const-string v5, "FROM_COLLAGE_DRIVE_CARD"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_COLLAGE_DRIVE_CARD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 73
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x48

    const-string v15, "collage_car_licence"

    move-object/from16 v75, v3

    const-string v3, "FROM_COLLAGE_CAR_LICENCE"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_COLLAGE_CAR_LICENCE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 74
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x49

    const-string v15, "collage_bank_card"

    move-object/from16 v76, v5

    const-string v5, "FROM_COLLAGE_BANK_CARD"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_COLLAGE_BANK_CARD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 75
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x4a

    const-string v15, "collage_doc_banner"

    move-object/from16 v77, v3

    const-string v3, "FROM_COLLAGE_DOC_BANNER"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_COLLAGE_DOC_BANNER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 76
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x4b

    const-string v15, "premium_level"

    move-object/from16 v78, v5

    const-string v5, "FROM_PREMIUM_LEVEL"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_PREMIUM_LEVEL:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 77
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x4c

    const-string v15, "cs_main_left"

    move-object/from16 v79, v3

    const-string v3, "FROM_CS_MAIN_LEFT"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_MAIN_LEFT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 78
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x4d

    const-string v15, "cs_cloud_space_over_limit"

    move-object/from16 v80, v5

    const-string v5, "FROM_CS_CLOUD_SPACE_OVER_LIMIT"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_CLOUD_SPACE_OVER_LIMIT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 79
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x4e

    const-string v15, "first_sync"

    move-object/from16 v81, v3

    const-string v3, "FROM_FIRST_SYNC"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_FIRST_SYNC:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 80
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x4f

    const-string v15, "cs_main_tools"

    move-object/from16 v82, v5

    const-string v5, "FROM_CS_MAIN_TOOLS"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_MAIN_TOOLS:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 81
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x50

    const-string v15, "from_scandone_vip_guide"

    move-object/from16 v83, v3

    const-string v3, "FROM_SCANDONE_VIP_GUIDE"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_SCANDONE_VIP_GUIDE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 82
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x51

    const-string v15, "cs_special_for_new_users"

    move-object/from16 v84, v5

    const-string v5, "FROM_CS_SPECIAL_FOR_NEW_USER"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_SPECIAL_FOR_NEW_USER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 83
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x52

    const-string v15, "cs_upgrade_cloud"

    move-object/from16 v85, v3

    const-string v3, "FROM_CS_UPGRADE_CLOUD"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_UPGRADE_CLOUD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 84
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x53

    const-string v15, "cs_discount_pop"

    move-object/from16 v86, v5

    const-string v5, "FROM_CS_DISCOUNT_POP"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_DISCOUNT_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 85
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x54

    const-string v15, "sediment_countdown_pop"

    move-object/from16 v87, v3

    const-string v3, "SEDIMENT_COUNTDOWN_POP"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->SEDIMENT_COUNTDOWN_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 86
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x55

    const-string v15, "24h_countdown_pop"

    move-object/from16 v88, v5

    const-string v5, "COUNTDOWN_POP_24H"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->COUNTDOWN_POP_24H:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 87
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x56

    const-string v15, "loop_countdown_pop"

    move-object/from16 v89, v3

    const-string v3, "LOOP_COUNTDOWN_POP"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->LOOP_COUNTDOWN_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 88
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x57

    const-string v15, "cs_scandone_banner"

    move-object/from16 v90, v5

    const-string v5, "CS_SCANDONE_BANNER"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_SCANDONE_BANNER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 89
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x58

    const-string v15, "cs_keep_a_while"

    move-object/from16 v91, v3

    const-string v3, "FROM_KEEP_A_WHILE"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_KEEP_A_WHILE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 90
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x59

    const-string v15, "cs_select_path"

    move-object/from16 v92, v5

    const-string v5, "FROM_CS_SELECT_PATH"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_SELECT_PATH:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 91
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x5a

    const-string v15, "cs_gift_card_invite"

    move-object/from16 v93, v3

    const-string v3, "FROM_CS_GIFT_CARD_INVITE"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_GIFT_CARD_INVITE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 92
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x5b

    const-string v15, "cs_word_preview"

    move-object/from16 v94, v5

    const-string v5, "FROM_CS_WORD_PREVIEW"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_WORD_PREVIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 93
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x5c

    const-string v15, "cs_long_pic_preview"

    move-object/from16 v95, v3

    const-string v3, "FROM_LONG_PIC_PREVIEW"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_LONG_PIC_PREVIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 94
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x5d

    const-string v15, "cs_pdf_package"

    move-object/from16 v96, v5

    const-string v5, "FROM_PDF_PACKAGE"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_PDF_PACKAGE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 95
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x5e

    const-string v15, "cs_pdf_package"

    move-object/from16 v97, v3

    const-string v3, "FROM_PDF_PACKAGE_LOCAL"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_PDF_PACKAGE_LOCAL:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 96
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x5f

    const-string v15, "cs_share"

    move-object/from16 v98, v5

    const-string v5, "FROM_CS_SHARE"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_SHARE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 97
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x60

    const-string v15, "cs_restore_share"

    move-object/from16 v99, v3

    const-string v3, "FROM_CS_RESTORE_SHARE"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_RESTORE_SHARE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 98
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x61

    const-string v15, "email_share"

    move-object/from16 v100, v5

    const-string v5, "FROM_EMAIL_SHARE"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_EMAIL_SHARE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 99
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x62

    const-string v15, "cs_guide_trial_unsub"

    move-object/from16 v101, v3

    const-string v3, "FROM_CS_GUIDE_TRIAL_UNSUB"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_GUIDE_TRIAL_UNSUB:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 100
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x63

    const-string v15, "cs_esignatures_h5"

    move-object/from16 v102, v5

    const-string v5, "FROM_CS_ESIGNATURES_H5"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_ESIGNATURES_H5:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 101
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x64

    const-string v15, "cs_error_folder"

    move-object/from16 v103, v3

    const-string v3, "FROM_CS_ERROR_FOLDER"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_ERROR_FOLDER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 102
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x65

    const-string v15, "cs_error_cloud"

    move-object/from16 v104, v5

    const-string v5, "FROM_CS_ERROR_CLOUD"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CS_ERROR_CLOUD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 103
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x66

    const-string v15, "cs_main"

    move-object/from16 v105, v3

    const-string v3, "CS_MAIN"

    invoke-direct {v5, v3, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MAIN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 104
    new-instance v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x67

    const-string v15, "cs_hamburger"

    move-object/from16 v106, v5

    const-string v5, "CS_HAMBURGER"

    invoke-direct {v3, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_HAMBURGER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 105
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const-string v9, "OPERATION_TYPE"

    const/16 v15, 0x68

    move-object/from16 v107, v3

    const-string v3, ""

    invoke-direct {v5, v9, v15, v3}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->OPERATION_TYPE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 106
    new-instance v9, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v15, 0x69

    move-object/from16 v108, v5

    const-string v5, "cs_scan"

    move-object/from16 v109, v7

    const-string v7, "CS_SCAN"

    invoke-direct {v9, v7, v15, v5}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v9, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_SCAN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 107
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v7, 0x6a

    const-string v15, "cs_more"

    move-object/from16 v110, v9

    const-string v9, "CS_MORE"

    invoke-direct {v5, v9, v7, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MORE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 108
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x6b

    const-string v15, "cs_collage_preview"

    move-object/from16 v111, v5

    const-string v5, "CS_COLLAGE_PREVIEW"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_COLLAGE_PREVIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 109
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x6c

    const-string v15, "cs_left_icon"

    move-object/from16 v112, v7

    const-string v7, "CS_LEFT_ICON"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_LEFT_ICON:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 110
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x6d

    const-string/jumbo v15, "text_message"

    move-object/from16 v113, v5

    const-string v5, "TEXT_MESSAGE"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->TEXT_MESSAGE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 111
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x6e

    const-string v15, "push"

    move-object/from16 v114, v7

    const-string v7, "PUSH"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->PUSH:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 112
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x6f

    const-string v15, "app_launch"

    move-object/from16 v115, v5

    const-string v5, "APP_LAUNCH"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->APP_LAUNCH:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 113
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x70

    const-string v15, "cs_main_icon"

    move-object/from16 v116, v7

    const-string v7, "CS_MAIN_ICON"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MAIN_ICON:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 114
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x71

    const-string v15, "cs_change_icon"

    move-object/from16 v117, v5

    const-string v5, "CS_CHANGE_ICON"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_CHANGE_ICON:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 115
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x72

    const-string v15, "cs_main_more"

    move-object/from16 v118, v7

    const-string v7, "CS_MAIN_MORE"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MAIN_MORE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 116
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x73

    const-string v15, "not_enough_pop"

    move-object/from16 v119, v5

    const-string v5, "NOT_ENOUGH_POP"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->NOT_ENOUGH_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 117
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x74

    const-string v15, "cs_account"

    move-object/from16 v120, v7

    const-string v7, "CS_ACCOUNT"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_ACCOUNT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 118
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x75

    const-string v15, "cs_crop"

    move-object/from16 v121, v5

    const-string v5, "CS_CROP"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_CROP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 119
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x76

    const-string v15, "cs_enhance"

    move-object/from16 v122, v7

    const-string v7, "CS_ENHANCE"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_ENHANCE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 120
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x77

    const-string v15, "cs_batch_result"

    move-object/from16 v123, v5

    const-string v5, "CS_BATCH_RESULT"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_BATCH_RESULT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 121
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x78

    const-string v15, "scan_toolbox"

    move-object/from16 v124, v7

    const-string v7, "CS_SCAN_TOOLBOX"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_SCAN_TOOLBOX:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 122
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x79

    const-string v15, "cs_sediment_bubble"

    move-object/from16 v125, v5

    const-string v5, "CS_SEDIMENT_BUBBLE"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_SEDIMENT_BUBBLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 123
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x7a

    const-string v15, "cs_renew_discount_month_week_pop"

    move-object/from16 v126, v7

    const-string v7, "CS_RENEW_DISCOUNT_MONTH_WEEK_POP"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_RENEW_DISCOUNT_MONTH_WEEK_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 124
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x7b

    const-string v15, "cs_renew_discount_year_pop"

    move-object/from16 v127, v5

    const-string v5, "CS_RENEW_DISCOUNT_YEAR_POP"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_RENEW_DISCOUNT_YEAR_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 125
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x7c

    const-string v15, "cs_hidden_egg"

    move-object/from16 v128, v7

    const-string v7, "CS_HIDDEN_EGG"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_HIDDEN_EGG:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 126
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x7d

    const-string v15, "cs_renew_education_month_week_pop"

    move-object/from16 v129, v5

    const-string v5, "CS_RENEW_EDUCATION_MONTH_WEEK_POP"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_RENEW_EDUCATION_MONTH_WEEK_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 127
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x7e

    const-string v15, "cs_renew_education_year_pop"

    move-object/from16 v130, v7

    const-string v7, "CS_RENEW_EDUCATION_YEAR_POP"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_RENEW_EDUCATION_YEAR_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 128
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x7f

    const-string v15, "cs_passive_save_pop"

    move-object/from16 v131, v5

    const-string v5, "CS_PASSIVE_SAVE_POP"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_PASSIVE_SAVE_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 129
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x80

    const-string v15, "icloud_guide_web"

    move-object/from16 v132, v7

    const-string v7, "CS_ICLOUD_GUIDE_WEB"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_ICLOUD_GUIDE_WEB:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 130
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x81

    const-string v15, "cs_cloud_buy_pop"

    move-object/from16 v133, v5

    const-string v5, "CS_CLOUD_BUY_POP"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_CLOUD_BUY_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 131
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x82

    const-string v15, "cs_cloud_alert_bubble"

    move-object/from16 v134, v7

    const-string v7, "CS_CLOUD_ALERT_BUBBLE"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_CLOUD_ALERT_BUBBLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 132
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x83

    const-string v15, "cs_cloud_overrun_bubble"

    move-object/from16 v135, v5

    const-string v5, "CS_CLOUD_OVERRUN_BUBBLE"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_CLOUD_OVERRUN_BUBBLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 133
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x84

    const-string v15, "cs_link_overrun_pop"

    move-object/from16 v136, v7

    const-string v7, "CS_LINK_OVERRUN_POP"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_LINK_OVERRUN_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 134
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x85

    const-string v15, "cs_advance_ocr_search_bubble"

    move-object/from16 v137, v5

    const-string v5, "CS_ADVANCE_OCR_SEARCH_BUBBLE"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_ADVANCE_OCR_SEARCH_BUBBLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 135
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x86

    const-string v15, "cs_advance_ocr_result"

    move-object/from16 v138, v7

    const-string v7, "CS_ADVANCE_OCR_RESULT"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_ADVANCE_OCR_RESULT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 136
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x87

    const-string/jumbo v15, "word_recognize"

    move-object/from16 v139, v5

    const-string v5, "WORD_RECOGNIZE"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WORD_RECOGNIZE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 137
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x88

    const-string v15, "cs_occupation_tag"

    move-object/from16 v140, v7

    const-string v7, "CS_OCCUPATION_TAG"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_OCCUPATION_TAG:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 138
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x89

    const-string v15, "cs_usertag_recommand"

    move-object/from16 v141, v5

    const-string v5, "CS_USERTAG_RECOMMAND"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_USERTAG_RECOMMAND:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 139
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x8a

    const-string v15, "cs_guide"

    move-object/from16 v142, v7

    const-string v7, "CS_GUIDE"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_GUIDE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 140
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x8b

    const-string v15, "cs_guide_marketing"

    move-object/from16 v143, v5

    const-string v5, "CS_GUIDE_MARKETING"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_GUIDE_MARKETING:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 141
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x8c

    const-string v15, "main_week"

    move-object/from16 v144, v7

    const-string v7, "MAIN_WEEK"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->MAIN_WEEK:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 142
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x8d

    const-string v15, "guide_premium_marketing"

    move-object/from16 v145, v5

    const-string v5, "GUIDE_PREMIUM_MARKETING"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->GUIDE_PREMIUM_MARKETING:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 143
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x8e

    const-string/jumbo v15, "word"

    move-object/from16 v146, v7

    const-string v7, "WORD"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WORD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 144
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x8f

    const-string v15, "cs_image_restore"

    move-object/from16 v147, v5

    const-string v5, "FROM_IMAGE_RESTORE"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_IMAGE_RESTORE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 145
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x90

    const-string v15, "cs_image_colorize"

    move-object/from16 v148, v7

    const-string v7, "FROM_IMAGE_RECOLOR"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_IMAGE_RECOLOR:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 146
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x91

    const-string v15, "scandone_back"

    move-object/from16 v149, v5

    const-string v5, "SCANDONE_BACK"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->SCANDONE_BACK:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 147
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x92

    const-string v15, "pdf_share_limit_pop"

    move-object/from16 v150, v7

    const-string v7, "PDF_SHARE_LIMIT_POP"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->PDF_SHARE_LIMIT_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 148
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x93

    const-string v15, "ocr_vip_guide"

    move-object/from16 v151, v5

    const-string v5, "OCR_VIP_GUIDE"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->OCR_VIP_GUIDE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 149
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x94

    const-string v15, "cs_home_banner"

    move-object/from16 v152, v7

    const-string v7, "CS_HOME_BANNER"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_HOME_BANNER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 150
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x95

    const-string v15, "cs_home_icon"

    move-object/from16 v153, v5

    const-string v5, "CS_HOME_ICON"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_HOME_ICON:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 151
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x96

    const-string v15, "cs_resubscribe"

    move-object/from16 v154, v7

    const-string v7, "CS_RESUBSCRIBE"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_RESUBSCRIBE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 152
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x97

    const-string v15, "CSFunctionRecommend"

    move-object/from16 v155, v5

    const-string v5, "CS_FUNCTION_RECOMMEND"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_FUNCTION_RECOMMEND:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 153
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x98

    const-string/jumbo v15, "work_efficiency"

    move-object/from16 v156, v7

    const-string v7, "WORK_EFFICIENCY"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WORK_EFFICIENCY:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 154
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x99

    const-string v15, "learn_efficiency"

    move-object/from16 v157, v5

    const-string v5, "LEARN_EFFICIENCY"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->LEARN_EFFICIENCY:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 155
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x9a

    const-string v15, "life_efficiency"

    move-object/from16 v158, v7

    const-string v7, "LIFE_EFFICIENCY"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->LIFE_EFFICIENCY:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 156
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x9b

    const-string v15, "cs_premium_marketing"

    move-object/from16 v159, v5

    const-string v5, "CS_PREMIUM_MARKETING"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_PREMIUM_MARKETING:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 157
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x9c

    const-string/jumbo v15, "watermark_free_remind_pop"

    move-object/from16 v160, v7

    const-string v7, "WATERMARK_FREE_REMIND_POP"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WATERMARK_FREE_REMIND_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 158
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x9d

    const-string v15, "cs_year_discount_landing"

    move-object/from16 v161, v5

    const-string v5, "CS_YEAR_DISCOUNT_LANDING"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_YEAR_DISCOUNT_LANDING:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 159
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x9e

    const-string v15, "cs_year_discount_pop"

    move-object/from16 v162, v7

    const-string v7, "CS_YEAR_DISCOUNT_POP"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_YEAR_DISCOUNT_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 160
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0x9f

    const-string/jumbo v15, "year_discount_bubble"

    move-object/from16 v163, v5

    const-string v5, "YEAR_DISCOUNT_BUBBLE"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->YEAR_DISCOUNT_BUBBLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 161
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xa0

    const-string v15, "cs_home_pop"

    move-object/from16 v164, v7

    const-string v7, "CS_HOME_POP"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_HOME_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 162
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xa1

    const-string v15, "cs_home_pop_user_recall_marketing"

    move-object/from16 v165, v5

    const-string v5, "CS_HOME_POP_USER_RECALL_MARKETING"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_HOME_POP_USER_RECALL_MARKETING:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 163
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xa2

    const-string v15, "cs_os_push"

    move-object/from16 v166, v7

    const-string v7, "CS_OS_PUSH"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_OS_PUSH:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 164
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xa3

    const-string v15, "cs_list"

    move-object/from16 v167, v5

    const-string v5, "CS_LIST"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_LIST:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 165
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xa4

    const-string v15, "cs_upgrade_marketing_pop"

    move-object/from16 v168, v7

    const-string v7, "CS_UPGRADE_MARKETING_POP"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_UPGRADE_MARKETING_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 166
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xa5

    const-string v15, "cs_home_bubble"

    move-object/from16 v169, v5

    const-string v5, "CS_HOME_BUBBLE"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_HOME_BUBBLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 167
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xa6

    const-string v15, "from_card_bag"

    move-object/from16 v170, v7

    const-string v7, "FROM_CARD_BAG"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CARD_BAG:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 168
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xa7

    const-string v15, "from_growth_record"

    move-object/from16 v171, v5

    const-string v5, "FROM_GROWTH_RECORD"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_GROWTH_RECORD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 169
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xa8

    const-string v15, "from_briefcase"

    move-object/from16 v172, v7

    const-string v7, "FROM_BRIEFCASE"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_BRIEFCASE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 170
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xa9

    const-string v15, "from_work_file"

    move-object/from16 v173, v5

    const-string v5, "FROM_WORK_FILE"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_WORK_FILE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 171
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xaa

    const-string v15, "from_inspiration_library"

    move-object/from16 v174, v7

    const-string v7, "FROM_INSPIRATION_LIBRARY"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_INSPIRATION_LIBRARY:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 172
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xab

    const-string v15, "from_family_storage"

    move-object/from16 v175, v5

    const-string v5, "FROM_FAMILY_STORAGE"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_FAMILY_STORAGE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 173
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xac

    const-string v15, "from_materials"

    move-object/from16 v176, v7

    const-string v7, "FROM_MATERIALS"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_MATERIALS:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 174
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xad

    const-string v15, "cs_advanced_folder_certificate"

    move-object/from16 v177, v5

    const-string v5, "CS_ADVANCED_FOLDER_CERTIFICATE"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_ADVANCED_FOLDER_CERTIFICATE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 175
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xae

    const-string v15, "cs_advanced_folder_certificate_retake"

    move-object/from16 v178, v7

    const-string v7, "CS_ADVANCED_FOLDER_CERTIFICATE_RETAKE"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_ADVANCED_FOLDER_CERTIFICATE_RETAKE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 176
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xaf

    const-string v15, "from_template_dir_preset"

    move-object/from16 v179, v5

    const-string v5, "FROM_TEMPLATE_DIR_PRESET"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_TEMPLATE_DIR_PRESET:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 177
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xb0

    const-string v15, "backSchoolgift"

    move-object/from16 v180, v7

    const-string v7, "FROM_BACK_SCHOOL_GIFT"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_BACK_SCHOOL_GIFT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 178
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xb1

    const-string v15, "cs_new_gift"

    move-object/from16 v181, v5

    const-string v5, "CS_NEW_GIFT"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_NEW_GIFT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 179
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xb2

    const-string v15, "cs_first_scan"

    move-object/from16 v182, v7

    const-string v7, "CS_FIRST_SCAN"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_FIRST_SCAN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 180
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xb3

    const-string v15, "cs_new_comer"

    move-object/from16 v183, v5

    const-string v5, "CS_NEW_COMER"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_NEW_COMER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 181
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xb4

    const-string v15, "cs_screen_shot"

    move-object/from16 v184, v7

    const-string v7, "CS_SCREEN_SHOT"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_SCREEN_SHOT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 182
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xb5

    const-string v15, "guide_video_premium"

    move-object/from16 v185, v5

    const-string v5, "GUIDE_VIDEO_PREMIUM"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->GUIDE_VIDEO_PREMIUM:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 183
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xb6

    const-string v15, "guide_premium"

    move-object/from16 v186, v7

    const-string v7, "GUIDE_PREMIUM"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->GUIDE_PREMIUM:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 184
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xb7

    const-string v15, "cs_smart_remove"

    move-object/from16 v187, v5

    const-string v5, "CS_SMART_ERASE"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_SMART_ERASE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 185
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xb8

    const-string v15, "cs_passive_pop_gift_test"

    move-object/from16 v188, v7

    const-string v7, "CS_PASSIVE_POP_GIFT_TEST"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_PASSIVE_POP_GIFT_TEST:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 186
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xb9

    const-string v15, "cs_ads_reward_pre"

    move-object/from16 v189, v5

    const-string v5, "CS_ADS_REWARD_PRE"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_ADS_REWARD_PRE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 187
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xba

    const-string v15, "cs_fake_entrance"

    move-object/from16 v190, v7

    const-string v7, "CS_FAKE_ENTRANCE"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_FAKE_ENTRANCE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 188
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xbb

    const-string v15, "cs_main_pop"

    move-object/from16 v191, v5

    const-string v5, "CS_MAIN_POP"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MAIN_POP:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 189
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xbc

    const-string v15, "cs_my_account"

    move-object/from16 v192, v7

    const-string v7, "CS_MY_ACCOUNT"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MY_ACCOUNT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 190
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xbd

    const-string v15, "cs_new_esign"

    move-object/from16 v193, v5

    const-string v5, "CS_NEW_ESIGN"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_NEW_ESIGN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 191
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xbe

    const-string v15, "cs_id_photo_preview"

    move-object/from16 v194, v7

    const-string v7, "CS_ID_PHOTO_PREVIEW"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_ID_PHOTO_PREVIEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 192
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xbf

    const-string v15, "cs_list_id_photo"

    move-object/from16 v195, v5

    const-string v5, "CS_LIST_ID_PHOTO"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_LIST_ID_PHOTO:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 193
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xc0

    const-string v15, "from_part_pad_h5"

    move-object/from16 v196, v7

    const-string v7, "FROM_PART_PAD_H5"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_PART_PAD_H5:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 194
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xc1

    const-string v15, "cs_share_done"

    move-object/from16 v197, v5

    const-string v5, "CS_SHARE_DONE"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_SHARE_DONE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 195
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xc2

    const-string v15, "save_as_word"

    move-object/from16 v198, v7

    const-string v7, "SAVE_AS_WORD"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->SAVE_AS_WORD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 196
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xc3

    const-string v15, "save_as_excel"

    move-object/from16 v199, v5

    const-string v5, "SAVE_AS_EXCEL"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->SAVE_AS_EXCEL:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 197
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xc4

    const-string v15, "save_as_other"

    move-object/from16 v200, v7

    const-string v7, "SAVE_AS_OTHER"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->SAVE_AS_OTHER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 198
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xc5

    const-string/jumbo v15, "word_export"

    move-object/from16 v201, v5

    const-string v5, "WORD_EXPORT"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WORD_EXPORT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 199
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xc6

    const-string v15, "excel_export"

    move-object/from16 v202, v7

    const-string v7, "EXCEL_EXPORT"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->EXCEL_EXPORT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 200
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xc7

    const-string v15, "other_export"

    move-object/from16 v203, v5

    const-string v5, "OTHER_EXPORT"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->OTHER_EXPORT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 201
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xc8

    const-string v15, "email_to_myself"

    move-object/from16 v204, v7

    const-string v7, "EMAIL_TO_MYSELF"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->EMAIL_TO_MYSELF:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 202
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xc9

    const-string v15, "email"

    move-object/from16 v205, v5

    const-string v5, "EMAIL"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->EMAIL:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 203
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xca

    const-string/jumbo v15, "word_preview_new"

    move-object/from16 v206, v7

    const-string v7, "WORD_PREVIEW_NEW"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WORD_PREVIEW_NEW:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 204
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xcb

    const-string v15, "app_shortcut_scan"

    move-object/from16 v207, v5

    const-string v5, "APP_SHORTCUT_SCAN"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->APP_SHORTCUT_SCAN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 205
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xcc

    const-string v15, "app_shortcut_to_word"

    move-object/from16 v208, v7

    const-string v7, "APP_SHORTCUT_TO_WORD"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->APP_SHORTCUT_TO_WORD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 206
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xcd

    const-string v15, "app_shortcut_batch"

    move-object/from16 v209, v5

    const-string v5, "APP_SHORTCUT_BATCH"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->APP_SHORTCUT_BATCH:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 207
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xce

    const-string v15, "app_shortcut_single"

    move-object/from16 v210, v7

    const-string v7, "APP_SHORTCUT_SINGLE"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->APP_SHORTCUT_SINGLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 208
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xcf

    const-string/jumbo v15, "widget_search42_ocr_mode"

    move-object/from16 v211, v5

    const-string v5, "WIDGET_SEARCH42_OCR_MODE"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WIDGET_SEARCH42_OCR_MODE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 209
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xd0

    const-string/jumbo v15, "widget_search42_id_mode"

    move-object/from16 v212, v7

    const-string v7, "WIDGET_SEARCH42_ID_MODE"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WIDGET_SEARCH42_ID_MODE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 210
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xd1

    const-string/jumbo v15, "widget_search42_scan_batch"

    move-object/from16 v213, v5

    const-string v5, "WIDGET_SEARCH42_SCAN_BATCH"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WIDGET_SEARCH42_SCAN_BATCH:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 211
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xd2

    const-string/jumbo v15, "widget_search42_scan_single"

    move-object/from16 v214, v7

    const-string v7, "WIDGET_SEARCH42_SCAN_SINGLE"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WIDGET_SEARCH42_SCAN_SINGLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 212
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xd3

    const-string/jumbo v15, "widget_func22_qr"

    move-object/from16 v215, v5

    const-string v5, "WIDGET_FUNC22_QR"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WIDGET_FUNC22_QR:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 213
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xd4

    const-string/jumbo v15, "widget_func11_qr"

    move-object/from16 v216, v7

    const-string v7, "WIDGET_FUNC11_QR"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WIDGET_FUNC11_QR:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 214
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xd5

    const-string/jumbo v15, "widget_func22_scan_batch"

    move-object/from16 v217, v5

    const-string v5, "WIDGET_FUNC22_SCAN_BATCH"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WIDGET_FUNC22_SCAN_BATCH:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 215
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xd6

    const-string/jumbo v15, "widget_func11_scan_batch"

    move-object/from16 v218, v7

    const-string v7, "WIDGET_FUNC11_SCAN_BATCH"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WIDGET_FUNC11_SCAN_BATCH:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 216
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xd7

    const-string/jumbo v15, "widget_func22_scan_single"

    move-object/from16 v219, v5

    const-string v5, "WIDGET_FUNC22_SCAN_SINGLE"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WIDGET_FUNC22_SCAN_SINGLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 217
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xd8

    const-string/jumbo v15, "widget_func11_scan_single"

    move-object/from16 v220, v7

    const-string v7, "WIDGET_FUNC11_SCAN_SINGLE"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WIDGET_FUNC11_SCAN_SINGLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 218
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xd9

    const-string/jumbo v15, "widget_doc42_scan_single"

    move-object/from16 v221, v5

    const-string v5, "WIDGET_DOC42_SCAN_SINGLE"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WIDGET_DOC42_SCAN_SINGLE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 219
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xda

    const-string v15, "one_yuan_scan_bracket"

    move-object/from16 v222, v7

    const-string v7, "ONE_YUAN_SCAN_BRACKET"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->ONE_YUAN_SCAN_BRACKET:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 220
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xdb

    const-string/jumbo v15, "workflows_share"

    move-object/from16 v223, v5

    const-string v5, "WORKFLOWS_SHARE"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WORKFLOWS_SHARE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 221
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xdc

    const-string/jumbo v15, "workflows_tools"

    move-object/from16 v224, v7

    const-string v7, "WORKFLOWS_TOOLS"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->WORKFLOWS_TOOLS:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 222
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const-string v9, "PRINTER_SCAN"

    const/16 v15, 0xdd

    invoke-direct {v7, v9, v15, v3}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->PRINTER_SCAN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 223
    new-instance v9, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v15, 0xde

    move-object/from16 v225, v7

    const-string v7, "cs_list_id_mode"

    move-object/from16 v226, v5

    const-string v5, "CERTIFICATE_CS_LIST"

    invoke-direct {v9, v5, v15, v7}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v9, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CERTIFICATE_CS_LIST:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 224
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v7, 0xdf

    const-string v15, "cs_home"

    move-object/from16 v227, v9

    const-string v9, "CS_HOME"

    invoke-direct {v5, v9, v7, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_HOME:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 225
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xe0

    const-string v15, "cs_pdf_import"

    move-object/from16 v228, v5

    const-string v5, "CS_PDF_IMPORT"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_PDF_IMPORT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 226
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xe1

    const-string v15, "cs_backup_function"

    move-object/from16 v229, v7

    const-string v7, "CS_BACKUP_FUNCTION"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_BACKUP_FUNCTION:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 227
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xe2

    const-string v15, "cs_cloud_sever"

    move-object/from16 v230, v5

    const-string v5, "CS_CLOUD_SEVER"

    invoke-direct {v7, v5, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_CLOUD_SEVER:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 228
    new-instance v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v9, 0xe3

    const-string v15, "cs_backup_function_cloud"

    move-object/from16 v231, v7

    const-string v7, "CS_BACKUP_FUNCTION_CLOUD"

    invoke-direct {v5, v7, v9, v15}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_BACKUP_FUNCTION_CLOUD:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 229
    new-instance v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const-string v9, "NONE"

    const/16 v15, 0xe4

    invoke-direct {v7, v9, v15, v3}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->NONE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/16 v3, 0xe5

    new-array v3, v3, [Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    const/4 v9, 0x0

    aput-object v0, v3, v9

    const/4 v0, 0x1

    aput-object v1, v3, v0

    const/4 v0, 0x2

    aput-object v2, v3, v0

    const/4 v0, 0x3

    aput-object v4, v3, v0

    const/4 v0, 0x4

    aput-object v6, v3, v0

    const/4 v0, 0x5

    aput-object v8, v3, v0

    const/4 v0, 0x6

    aput-object v10, v3, v0

    const/4 v0, 0x7

    aput-object v12, v3, v0

    const/16 v0, 0x8

    aput-object v14, v3, v0

    const/16 v0, 0x9

    aput-object v13, v3, v0

    const/16 v0, 0xa

    aput-object v11, v3, v0

    const/16 v0, 0xb

    aput-object v22, v3, v0

    const/16 v0, 0xc

    aput-object v16, v3, v0

    const/16 v0, 0xd

    aput-object v17, v3, v0

    const/16 v0, 0xe

    aput-object v18, v3, v0

    const/16 v0, 0xf

    aput-object v19, v3, v0

    const/16 v0, 0x10

    aput-object v20, v3, v0

    const/16 v0, 0x11

    aput-object v21, v3, v0

    const/16 v0, 0x12

    aput-object v23, v3, v0

    const/16 v0, 0x13

    aput-object v24, v3, v0

    const/16 v0, 0x14

    aput-object v25, v3, v0

    const/16 v0, 0x15

    aput-object v26, v3, v0

    const/16 v0, 0x16

    aput-object v109, v3, v0

    const/16 v0, 0x17

    aput-object v27, v3, v0

    const/16 v0, 0x18

    aput-object v28, v3, v0

    const/16 v0, 0x19

    aput-object v29, v3, v0

    const/16 v0, 0x1a

    aput-object v30, v3, v0

    const/16 v0, 0x1b

    aput-object v31, v3, v0

    const/16 v0, 0x1c

    aput-object v32, v3, v0

    const/16 v0, 0x1d

    aput-object v33, v3, v0

    const/16 v0, 0x1e

    aput-object v34, v3, v0

    const/16 v0, 0x1f

    aput-object v35, v3, v0

    const/16 v0, 0x20

    aput-object v36, v3, v0

    const/16 v0, 0x21

    aput-object v37, v3, v0

    const/16 v0, 0x22

    aput-object v38, v3, v0

    const/16 v0, 0x23

    aput-object v39, v3, v0

    const/16 v0, 0x24

    aput-object v40, v3, v0

    const/16 v0, 0x25

    aput-object v41, v3, v0

    const/16 v0, 0x26

    aput-object v42, v3, v0

    const/16 v0, 0x27

    aput-object v43, v3, v0

    const/16 v0, 0x28

    aput-object v44, v3, v0

    const/16 v0, 0x29

    aput-object v45, v3, v0

    const/16 v0, 0x2a

    aput-object v46, v3, v0

    const/16 v0, 0x2b

    aput-object v47, v3, v0

    const/16 v0, 0x2c

    aput-object v48, v3, v0

    const/16 v0, 0x2d

    aput-object v49, v3, v0

    const/16 v0, 0x2e

    aput-object v50, v3, v0

    const/16 v0, 0x2f

    aput-object v51, v3, v0

    const/16 v0, 0x30

    aput-object v52, v3, v0

    const/16 v0, 0x31

    aput-object v53, v3, v0

    const/16 v0, 0x32

    aput-object v54, v3, v0

    const/16 v0, 0x33

    aput-object v55, v3, v0

    const/16 v0, 0x34

    aput-object v56, v3, v0

    const/16 v0, 0x35

    aput-object v57, v3, v0

    const/16 v0, 0x36

    aput-object v58, v3, v0

    const/16 v0, 0x37

    aput-object v59, v3, v0

    const/16 v0, 0x38

    aput-object v60, v3, v0

    const/16 v0, 0x39

    aput-object v61, v3, v0

    const/16 v0, 0x3a

    aput-object v62, v3, v0

    const/16 v0, 0x3b

    aput-object v63, v3, v0

    const/16 v0, 0x3c

    aput-object v64, v3, v0

    const/16 v0, 0x3d

    aput-object v65, v3, v0

    const/16 v0, 0x3e

    aput-object v66, v3, v0

    const/16 v0, 0x3f

    aput-object v67, v3, v0

    const/16 v0, 0x40

    aput-object v68, v3, v0

    const/16 v0, 0x41

    aput-object v69, v3, v0

    const/16 v0, 0x42

    aput-object v70, v3, v0

    const/16 v0, 0x43

    aput-object v71, v3, v0

    const/16 v0, 0x44

    aput-object v72, v3, v0

    const/16 v0, 0x45

    aput-object v73, v3, v0

    const/16 v0, 0x46

    aput-object v74, v3, v0

    const/16 v0, 0x47

    aput-object v75, v3, v0

    const/16 v0, 0x48

    aput-object v76, v3, v0

    const/16 v0, 0x49

    aput-object v77, v3, v0

    const/16 v0, 0x4a

    aput-object v78, v3, v0

    const/16 v0, 0x4b

    aput-object v79, v3, v0

    const/16 v0, 0x4c

    aput-object v80, v3, v0

    const/16 v0, 0x4d

    aput-object v81, v3, v0

    const/16 v0, 0x4e

    aput-object v82, v3, v0

    const/16 v0, 0x4f

    aput-object v83, v3, v0

    const/16 v0, 0x50

    aput-object v84, v3, v0

    const/16 v0, 0x51

    aput-object v85, v3, v0

    const/16 v0, 0x52

    aput-object v86, v3, v0

    const/16 v0, 0x53

    aput-object v87, v3, v0

    const/16 v0, 0x54

    aput-object v88, v3, v0

    const/16 v0, 0x55

    aput-object v89, v3, v0

    const/16 v0, 0x56

    aput-object v90, v3, v0

    const/16 v0, 0x57

    aput-object v91, v3, v0

    const/16 v0, 0x58

    aput-object v92, v3, v0

    const/16 v0, 0x59

    aput-object v93, v3, v0

    const/16 v0, 0x5a

    aput-object v94, v3, v0

    const/16 v0, 0x5b

    aput-object v95, v3, v0

    const/16 v0, 0x5c

    aput-object v96, v3, v0

    const/16 v0, 0x5d

    aput-object v97, v3, v0

    const/16 v0, 0x5e

    aput-object v98, v3, v0

    const/16 v0, 0x5f

    aput-object v99, v3, v0

    const/16 v0, 0x60

    aput-object v100, v3, v0

    const/16 v0, 0x61

    aput-object v101, v3, v0

    const/16 v0, 0x62

    aput-object v102, v3, v0

    const/16 v0, 0x63

    aput-object v103, v3, v0

    const/16 v0, 0x64

    aput-object v104, v3, v0

    const/16 v0, 0x65

    aput-object v105, v3, v0

    const/16 v0, 0x66

    aput-object v106, v3, v0

    const/16 v0, 0x67

    aput-object v107, v3, v0

    const/16 v0, 0x68

    aput-object v108, v3, v0

    const/16 v0, 0x69

    aput-object v110, v3, v0

    const/16 v0, 0x6a

    aput-object v111, v3, v0

    const/16 v0, 0x6b

    aput-object v112, v3, v0

    const/16 v0, 0x6c

    aput-object v113, v3, v0

    const/16 v0, 0x6d

    aput-object v114, v3, v0

    const/16 v0, 0x6e

    aput-object v115, v3, v0

    const/16 v0, 0x6f

    aput-object v116, v3, v0

    const/16 v0, 0x70

    aput-object v117, v3, v0

    const/16 v0, 0x71

    aput-object v118, v3, v0

    const/16 v0, 0x72

    aput-object v119, v3, v0

    const/16 v0, 0x73

    aput-object v120, v3, v0

    const/16 v0, 0x74

    aput-object v121, v3, v0

    const/16 v0, 0x75

    aput-object v122, v3, v0

    const/16 v0, 0x76

    aput-object v123, v3, v0

    const/16 v0, 0x77

    aput-object v124, v3, v0

    const/16 v0, 0x78

    aput-object v125, v3, v0

    const/16 v0, 0x79

    aput-object v126, v3, v0

    const/16 v0, 0x7a

    aput-object v127, v3, v0

    const/16 v0, 0x7b

    aput-object v128, v3, v0

    const/16 v0, 0x7c

    aput-object v129, v3, v0

    const/16 v0, 0x7d

    aput-object v130, v3, v0

    const/16 v0, 0x7e

    aput-object v131, v3, v0

    const/16 v0, 0x7f

    aput-object v132, v3, v0

    const/16 v0, 0x80

    aput-object v133, v3, v0

    const/16 v0, 0x81

    aput-object v134, v3, v0

    const/16 v0, 0x82

    aput-object v135, v3, v0

    const/16 v0, 0x83

    aput-object v136, v3, v0

    const/16 v0, 0x84

    aput-object v137, v3, v0

    const/16 v0, 0x85

    aput-object v138, v3, v0

    const/16 v0, 0x86

    aput-object v139, v3, v0

    const/16 v0, 0x87

    aput-object v140, v3, v0

    const/16 v0, 0x88

    aput-object v141, v3, v0

    const/16 v0, 0x89

    aput-object v142, v3, v0

    const/16 v0, 0x8a

    aput-object v143, v3, v0

    const/16 v0, 0x8b

    aput-object v144, v3, v0

    const/16 v0, 0x8c

    aput-object v145, v3, v0

    const/16 v0, 0x8d

    aput-object v146, v3, v0

    const/16 v0, 0x8e

    aput-object v147, v3, v0

    const/16 v0, 0x8f

    aput-object v148, v3, v0

    const/16 v0, 0x90

    aput-object v149, v3, v0

    const/16 v0, 0x91

    aput-object v150, v3, v0

    const/16 v0, 0x92

    aput-object v151, v3, v0

    const/16 v0, 0x93

    aput-object v152, v3, v0

    const/16 v0, 0x94

    aput-object v153, v3, v0

    const/16 v0, 0x95

    aput-object v154, v3, v0

    const/16 v0, 0x96

    aput-object v155, v3, v0

    const/16 v0, 0x97

    aput-object v156, v3, v0

    const/16 v0, 0x98

    aput-object v157, v3, v0

    const/16 v0, 0x99

    aput-object v158, v3, v0

    const/16 v0, 0x9a

    aput-object v159, v3, v0

    const/16 v0, 0x9b

    aput-object v160, v3, v0

    const/16 v0, 0x9c

    aput-object v161, v3, v0

    const/16 v0, 0x9d

    aput-object v162, v3, v0

    const/16 v0, 0x9e

    aput-object v163, v3, v0

    const/16 v0, 0x9f

    aput-object v164, v3, v0

    const/16 v0, 0xa0

    aput-object v165, v3, v0

    const/16 v0, 0xa1

    aput-object v166, v3, v0

    const/16 v0, 0xa2

    aput-object v167, v3, v0

    const/16 v0, 0xa3

    aput-object v168, v3, v0

    const/16 v0, 0xa4

    aput-object v169, v3, v0

    const/16 v0, 0xa5

    aput-object v170, v3, v0

    const/16 v0, 0xa6

    aput-object v171, v3, v0

    const/16 v0, 0xa7

    aput-object v172, v3, v0

    const/16 v0, 0xa8

    aput-object v173, v3, v0

    const/16 v0, 0xa9

    aput-object v174, v3, v0

    const/16 v0, 0xaa

    aput-object v175, v3, v0

    const/16 v0, 0xab

    aput-object v176, v3, v0

    const/16 v0, 0xac

    aput-object v177, v3, v0

    const/16 v0, 0xad

    aput-object v178, v3, v0

    const/16 v0, 0xae

    aput-object v179, v3, v0

    const/16 v0, 0xaf

    aput-object v180, v3, v0

    const/16 v0, 0xb0

    aput-object v181, v3, v0

    const/16 v0, 0xb1

    aput-object v182, v3, v0

    const/16 v0, 0xb2

    aput-object v183, v3, v0

    const/16 v0, 0xb3

    aput-object v184, v3, v0

    const/16 v0, 0xb4

    aput-object v185, v3, v0

    const/16 v0, 0xb5

    aput-object v186, v3, v0

    const/16 v0, 0xb6

    aput-object v187, v3, v0

    const/16 v0, 0xb7

    aput-object v188, v3, v0

    const/16 v0, 0xb8

    aput-object v189, v3, v0

    const/16 v0, 0xb9

    aput-object v190, v3, v0

    const/16 v0, 0xba

    aput-object v191, v3, v0

    const/16 v0, 0xbb

    aput-object v192, v3, v0

    const/16 v0, 0xbc

    aput-object v193, v3, v0

    const/16 v0, 0xbd

    aput-object v194, v3, v0

    const/16 v0, 0xbe

    aput-object v195, v3, v0

    const/16 v0, 0xbf

    aput-object v196, v3, v0

    const/16 v0, 0xc0

    aput-object v197, v3, v0

    const/16 v0, 0xc1

    aput-object v198, v3, v0

    const/16 v0, 0xc2

    aput-object v199, v3, v0

    const/16 v0, 0xc3

    aput-object v200, v3, v0

    const/16 v0, 0xc4

    aput-object v201, v3, v0

    const/16 v0, 0xc5

    aput-object v202, v3, v0

    const/16 v0, 0xc6

    aput-object v203, v3, v0

    const/16 v0, 0xc7

    aput-object v204, v3, v0

    const/16 v0, 0xc8

    aput-object v205, v3, v0

    const/16 v0, 0xc9

    aput-object v206, v3, v0

    const/16 v0, 0xca

    aput-object v207, v3, v0

    const/16 v0, 0xcb

    aput-object v208, v3, v0

    const/16 v0, 0xcc

    aput-object v209, v3, v0

    const/16 v0, 0xcd

    aput-object v210, v3, v0

    const/16 v0, 0xce

    aput-object v211, v3, v0

    const/16 v0, 0xcf

    aput-object v212, v3, v0

    const/16 v0, 0xd0

    aput-object v213, v3, v0

    const/16 v0, 0xd1

    aput-object v214, v3, v0

    const/16 v0, 0xd2

    aput-object v215, v3, v0

    const/16 v0, 0xd3

    aput-object v216, v3, v0

    const/16 v0, 0xd4

    aput-object v217, v3, v0

    const/16 v0, 0xd5

    aput-object v218, v3, v0

    const/16 v0, 0xd6

    aput-object v219, v3, v0

    const/16 v0, 0xd7

    aput-object v220, v3, v0

    const/16 v0, 0xd8

    aput-object v221, v3, v0

    const/16 v0, 0xd9

    aput-object v222, v3, v0

    const/16 v0, 0xda

    aput-object v223, v3, v0

    const/16 v0, 0xdb

    aput-object v224, v3, v0

    const/16 v0, 0xdc

    aput-object v226, v3, v0

    const/16 v0, 0xdd

    aput-object v225, v3, v0

    const/16 v0, 0xde

    aput-object v227, v3, v0

    const/16 v0, 0xdf

    aput-object v228, v3, v0

    const/16 v0, 0xe0

    aput-object v229, v3, v0

    const/16 v0, 0xe1

    aput-object v230, v3, v0

    const/16 v0, 0xe2

    aput-object v231, v3, v0

    const/16 v0, 0xe3

    aput-object v5, v3, v0

    const/16 v0, 0xe4

    aput-object v7, v3, v0

    .line 230
    sput-object v3, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->$VALUES:[Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    iput-object p3, p0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->o0:Ljava/lang/String;

    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/track/FunctionEntrance;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static values()[Lcom/intsig/camscanner/purchase/track/FunctionEntrance;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->$VALUES:[Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method


# virtual methods
.method public isNotNone()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->NONE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 2
    .line 3
    if-eq p0, v0, :cond_0

    .line 4
    .line 5
    const/4 v0, 0x1

    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v0, 0x0

    .line 8
    :goto_0
    return v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public needHideVipTips()Z
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MY_ACCOUNT:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 2
    .line 3
    if-eq p0, v0, :cond_1

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MAIN_ICON:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 6
    .line 7
    if-ne p0, v0, :cond_0

    .line 8
    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    goto :goto_1

    .line 12
    :cond_1
    :goto_0
    const/4 v0, 0x1

    .line 13
    :goto_1
    return v0
    .line 14
    .line 15
.end method

.method public setValue(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/track/FunctionEntrance;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public toTrackerValue()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->o0:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

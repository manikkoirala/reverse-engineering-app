.class public final Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;
.super Lcom/intsig/app/BaseBottomSheetDialogFragment;
.source "VipActivityPurchaseDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080OO8〇0:Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field static final synthetic 〇0O:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Lcom/intsig/callback/DialogDismissListener;

.field private OO:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

.field private 〇08O〇00〇o:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

.field private 〇OOo8〇0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$WorldWideMonthlyMembershipActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "mBinding"

    .line 7
    .line 8
    const-string v3, "getMBinding()Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇0O:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇080OO8〇0:Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog$Companion;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/app/BaseBottomSheetDialogFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v6, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x4

    .line 10
    const/4 v5, 0x0

    .line 11
    move-object v0, v6

    .line 12
    move-object v2, p0

    .line 13
    invoke-direct/range {v0 .. v5}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object v6, p0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 17
    .line 18
    const-string v0, ""

    .line 19
    .line 20
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->OO:Ljava/lang/String;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private static final O0〇0(Landroid/content/DialogInterface;)V
    .locals 1

    .line 1
    const-string v0, "null cannot be cast to non-null type com.google.android.material.bottomsheet.BottomSheetDialog"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    check-cast p0, Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->getBehavior()Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    .line 9
    .line 10
    .line 11
    move-result-object p0

    .line 12
    const-string v0, "dialog.behavior"

    .line 13
    .line 14
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    const/4 v0, 0x3

    .line 18
    invoke-virtual {p0, v0}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setState(I)V

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
.end method

.method private final Ooo8o()V
    .locals 12

    .line 1
    const-string v0, "VipActivityPurchaseDialog"

    .line 2
    .line 3
    const-string/jumbo v1, "showTrialRuleDialog"

    .line 4
    .line 5
    .line 6
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    new-instance v0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog$showTrialRuleDialog$trialRuleDialogListener$1;

    .line 10
    .line 11
    invoke-direct {v0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog$showTrialRuleDialog$trialRuleDialogListener$1;-><init>()V

    .line 12
    .line 13
    .line 14
    sget-object v10, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->o8o:Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog$Companion;

    .line 15
    .line 16
    sget-object v1, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseManager;->〇080:Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseManager;

    .line 17
    .line 18
    iget-object v2, p0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->OO:Ljava/lang/String;

    .line 19
    .line 20
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseManager;->O8(Ljava/lang/String;)Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;

    .line 21
    .line 22
    .line 23
    move-result-object v2

    .line 24
    iget-object v3, p0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->OO:Ljava/lang/String;

    .line 25
    .line 26
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇OOo8〇0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$WorldWideMonthlyMembershipActivity;

    .line 27
    .line 28
    const/4 v4, 0x0

    .line 29
    if-eqz v1, :cond_0

    .line 30
    .line 31
    iget-object v1, v1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$WorldWideMonthlyMembershipActivity;->rule_button_text:Ljava/lang/String;

    .line 32
    .line 33
    goto :goto_0

    .line 34
    :cond_0
    move-object v1, v4

    .line 35
    :goto_0
    if-nez v1, :cond_1

    .line 36
    .line 37
    const-string v1, ""

    .line 38
    .line 39
    :cond_1
    move-object v5, v1

    .line 40
    const/4 v6, 0x1

    .line 41
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->o〇00O:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 42
    .line 43
    if-nez v1, :cond_2

    .line 44
    .line 45
    const-string v1, "mPurchaseTracker"

    .line 46
    .line 47
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    move-object v7, v4

    .line 51
    goto :goto_1

    .line 52
    :cond_2
    move-object v7, v1

    .line 53
    :goto_1
    const/4 v8, 0x0

    .line 54
    const/16 v9, 0x20

    .line 55
    .line 56
    const/4 v11, 0x0

    .line 57
    move-object v1, v10

    .line 58
    move-object v4, v5

    .line 59
    move v5, v6

    .line 60
    move-object v6, v7

    .line 61
    move-object v7, v8

    .line 62
    move v8, v9

    .line 63
    move-object v9, v11

    .line 64
    invoke-static/range {v1 .. v9}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog$Companion;->〇o〇(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog$Companion;Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;Ljava/lang/String;Ljava/lang/String;ILcom/intsig/camscanner/purchase/track/PurchaseTracker;Lcom/intsig/camscanner/guide/dropchannel/dialog/TrialRuleDialogListener;ILjava/lang/Object;)Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;

    .line 65
    .line 66
    .line 67
    move-result-object v1

    .line 68
    new-instance v2, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog$showTrialRuleDialog$instance$1;

    .line 69
    .line 70
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog$showTrialRuleDialog$instance$1;-><init>(Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;)V

    .line 71
    .line 72
    .line 73
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->O0O0〇(Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;)Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;

    .line 74
    .line 75
    .line 76
    move-result-object v1

    .line 77
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇Oo〇O(Lcom/intsig/camscanner/guide/dropchannel/dialog/TrialRuleDialogListener;)V

    .line 78
    .line 79
    .line 80
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 81
    .line 82
    .line 83
    move-result-object v0

    .line 84
    invoke-virtual {v10}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog$Companion;->〇080()Ljava/lang/String;

    .line 85
    .line 86
    .line 87
    move-result-object v2

    .line 88
    invoke-virtual {v1, v0, v2}, Landroidx/fragment/app/DialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    return-void
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private final o00〇88〇08()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->OO:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "click purchase, productId: "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "VipActivityPurchaseDialog"

    .line 21
    .line 22
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->OO:Ljava/lang/String;

    .line 26
    .line 27
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    const/4 v1, 0x1

    .line 32
    const/4 v2, 0x0

    .line 33
    if-nez v0, :cond_0

    .line 34
    .line 35
    const/4 v0, 0x1

    .line 36
    goto :goto_0

    .line 37
    :cond_0
    const/4 v0, 0x0

    .line 38
    :goto_0
    if-eqz v0, :cond_1

    .line 39
    .line 40
    return-void

    .line 41
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇〇o0〇8()Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    const/4 v3, 0x0

    .line 46
    if-eqz v0, :cond_3

    .line 47
    .line 48
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;->〇OOo8〇0:Landroid/widget/CheckBox;

    .line 49
    .line 50
    if-eqz v0, :cond_3

    .line 51
    .line 52
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 53
    .line 54
    .line 55
    move-result v0

    .line 56
    if-nez v0, :cond_2

    .line 57
    .line 58
    goto :goto_1

    .line 59
    :cond_2
    const/4 v1, 0x0

    .line 60
    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    goto :goto_2

    .line 65
    :cond_3
    move-object v0, v3

    .line 66
    :goto_2
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇〇o0〇8()Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;

    .line 67
    .line 68
    .line 69
    move-result-object v1

    .line 70
    if-eqz v1, :cond_4

    .line 71
    .line 72
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;->〇OOo8〇0:Landroid/widget/CheckBox;

    .line 73
    .line 74
    if-eqz v1, :cond_4

    .line 75
    .line 76
    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    .line 77
    .line 78
    .line 79
    move-result v1

    .line 80
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 81
    .line 82
    .line 83
    move-result-object v1

    .line 84
    goto :goto_3

    .line 85
    :cond_4
    move-object v1, v3

    .line 86
    :goto_3
    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 87
    .line 88
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 89
    .line 90
    .line 91
    move-result v0

    .line 92
    if-eqz v0, :cond_5

    .line 93
    .line 94
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 95
    .line 96
    .line 97
    move-result v0

    .line 98
    if-nez v0, :cond_5

    .line 99
    .line 100
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    .line 101
    .line 102
    .line 103
    move-result-object v0

    .line 104
    const v1, 0x7f1311b8

    .line 105
    .line 106
    .line 107
    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->O8(Landroid/content/Context;I)V

    .line 108
    .line 109
    .line 110
    return-void

    .line 111
    :cond_5
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->o〇00O:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 112
    .line 113
    if-nez v0, :cond_6

    .line 114
    .line 115
    const-string v0, "mPurchaseTracker"

    .line 116
    .line 117
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 118
    .line 119
    .line 120
    goto :goto_4

    .line 121
    :cond_6
    move-object v3, v0

    .line 122
    :goto_4
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->OO:Ljava/lang/String;

    .line 123
    .line 124
    iput-object v0, v3, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->productId:Ljava/lang/String;

    .line 125
    .line 126
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->o〇0()Z

    .line 127
    .line 128
    .line 129
    move-result v0

    .line 130
    if-eqz v0, :cond_7

    .line 131
    .line 132
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->Ooo8o()V

    .line 133
    .line 134
    .line 135
    return-void

    .line 136
    :cond_7
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 137
    .line 138
    if-eqz v0, :cond_8

    .line 139
    .line 140
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->OO:Ljava/lang/String;

    .line 141
    .line 142
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->O0O8OO088(Ljava/lang/String;)V

    .line 143
    .line 144
    .line 145
    :cond_8
    return-void
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private final o880()V
    .locals 4

    .line 1
    const-string v0, "VipActivityPurchaseDialog"

    .line 2
    .line 3
    const-string v1, "click cancel"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 v0, 0x3

    .line 9
    new-array v0, v0, [Landroid/util/Pair;

    .line 10
    .line 11
    new-instance v1, Landroid/util/Pair;

    .line 12
    .line 13
    sget-object v2, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->PAY_POST_POSITION_ACTIVITY:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 14
    .line 15
    invoke-virtual {v2}, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->toTrackerValue()Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v2

    .line 19
    const-string v3, "scheme"

    .line 20
    .line 21
    invoke-direct {v1, v3, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 22
    .line 23
    .line 24
    const/4 v2, 0x0

    .line 25
    aput-object v1, v0, v2

    .line 26
    .line 27
    new-instance v1, Landroid/util/Pair;

    .line 28
    .line 29
    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_SCAN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 30
    .line 31
    invoke-virtual {v2}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    const-string v3, "from_part"

    .line 36
    .line 37
    invoke-direct {v1, v3, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 38
    .line 39
    .line 40
    const/4 v2, 0x1

    .line 41
    aput-object v1, v0, v2

    .line 42
    .line 43
    new-instance v1, Landroid/util/Pair;

    .line 44
    .line 45
    sget-object v2, Lcom/intsig/camscanner/purchase/entity/Function;->PAY_POST_POSITION_ACTIVITY:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 46
    .line 47
    invoke-virtual {v2}, Lcom/intsig/camscanner/purchase/entity/Function;->toTrackerValue()Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v2

    .line 51
    const-string v3, "from"

    .line 52
    .line 53
    invoke-direct {v1, v3, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 54
    .line 55
    .line 56
    const/4 v2, 0x2

    .line 57
    aput-object v1, v0, v2

    .line 58
    .line 59
    const-string v1, "CSPremiumPop"

    .line 60
    .line 61
    const-string v2, "abandon"

    .line 62
    .line 63
    invoke-static {v1, v2, v0}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 64
    .line 65
    .line 66
    invoke-virtual {p0}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->dismissAllowingStateLoss()V

    .line 67
    .line 68
    .line 69
    return-void
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private final oOoO8OO〇()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇8〇OOoooo()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->o〇0〇o()V

    .line 5
    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇088O()V

    .line 8
    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇8〇80o()V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇o〇88〇8()V

    .line 14
    .line 15
    .line 16
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->o〇O8OO()V

    .line 17
    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->oO〇oo()V

    .line 20
    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static synthetic oOo〇08〇(Landroid/content/DialogInterface;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->O0〇0(Landroid/content/DialogInterface;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final oO〇oo()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇〇o0〇8()Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 12
    .line 13
    .line 14
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇〇o0〇8()Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    if-eqz v0, :cond_1

    .line 19
    .line 20
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;->OO〇00〇8oO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 21
    .line 22
    if-eqz v0, :cond_1

    .line 23
    .line 24
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 25
    .line 26
    .line 27
    :cond_1
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final oooO888()Z
    .locals 6

    .line 1
    const-string v0, "checkData"

    .line 2
    .line 3
    const-string v1, "VipActivityPurchaseDialog"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->worldwide_monthly_membership_activity:Lcom/intsig/comm/purchase/entity/QueryProductsResult$WorldWideMonthlyMembershipActivity;

    .line 17
    .line 18
    const/4 v2, 0x0

    .line 19
    if-nez v0, :cond_0

    .line 20
    .line 21
    const-string v0, "checkData, data == null"

    .line 22
    .line 23
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    return v2

    .line 27
    :cond_0
    iget-object v3, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$WorldWideMonthlyMembershipActivity;->product_id:Ljava/lang/String;

    .line 28
    .line 29
    const/4 v4, 0x1

    .line 30
    if-eqz v3, :cond_2

    .line 31
    .line 32
    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    .line 33
    .line 34
    .line 35
    move-result v5

    .line 36
    if-nez v5, :cond_1

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_1
    const/4 v5, 0x0

    .line 40
    goto :goto_1

    .line 41
    :cond_2
    :goto_0
    const/4 v5, 0x1

    .line 42
    :goto_1
    if-eqz v5, :cond_3

    .line 43
    .line 44
    const-string v0, "checkData, product_id isNullOrEmpty"

    .line 45
    .line 46
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    return v2

    .line 50
    :cond_3
    iget-object v5, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$WorldWideMonthlyMembershipActivity;->product_description_656:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductDescription656;

    .line 51
    .line 52
    if-nez v5, :cond_4

    .line 53
    .line 54
    const-string v0, "checkData, product_description_656 is null"

    .line 55
    .line 56
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    return v2

    .line 60
    :cond_4
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇OOo8〇0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$WorldWideMonthlyMembershipActivity;

    .line 61
    .line 62
    const-string v0, "productId"

    .line 63
    .line 64
    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    iput-object v3, p0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->OO:Ljava/lang/String;

    .line 68
    .line 69
    return v4
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private final o〇0〇o()V
    .locals 14

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇OOo8〇0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$WorldWideMonthlyMembershipActivity;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    iget-object v2, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$WorldWideMonthlyMembershipActivity;->product_description_656:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductDescription656;

    .line 7
    .line 8
    if-eqz v2, :cond_0

    .line 9
    .line 10
    iget-object v2, v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductDescription656;->pop_description_click:Ljava/lang/String;

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    move-object v2, v1

    .line 14
    :goto_0
    const-string v3, ""

    .line 15
    .line 16
    if-nez v2, :cond_1

    .line 17
    .line 18
    move-object v2, v3

    .line 19
    :cond_1
    if-eqz v0, :cond_2

    .line 20
    .line 21
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$WorldWideMonthlyMembershipActivity;->product_description_656:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductDescription656;

    .line 22
    .line 23
    if-eqz v0, :cond_2

    .line 24
    .line 25
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductDescription656;->pop_description:Ljava/lang/String;

    .line 26
    .line 27
    goto :goto_1

    .line 28
    :cond_2
    move-object v0, v1

    .line 29
    :goto_1
    if-nez v0, :cond_3

    .line 30
    .line 31
    move-object v0, v3

    .line 32
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇〇o0〇8()Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;

    .line 33
    .line 34
    .line 35
    move-result-object v4

    .line 36
    if-eqz v4, :cond_4

    .line 37
    .line 38
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;->〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 39
    .line 40
    goto :goto_2

    .line 41
    :cond_4
    move-object v4, v1

    .line 42
    :goto_2
    const/4 v5, 0x1

    .line 43
    const/4 v6, 0x0

    .line 44
    if-nez v4, :cond_5

    .line 45
    .line 46
    goto :goto_5

    .line 47
    :cond_5
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 48
    .line 49
    .line 50
    move-result v7

    .line 51
    if-nez v7, :cond_6

    .line 52
    .line 53
    const/4 v7, 0x1

    .line 54
    goto :goto_3

    .line 55
    :cond_6
    const/4 v7, 0x0

    .line 56
    :goto_3
    if-eqz v7, :cond_7

    .line 57
    .line 58
    move-object v8, v3

    .line 59
    goto :goto_4

    .line 60
    :cond_7
    move-object v8, v0

    .line 61
    :goto_4
    const/4 v9, 0x0

    .line 62
    const/4 v10, 0x0

    .line 63
    const/4 v11, 0x0

    .line 64
    const/4 v12, 0x7

    .line 65
    const/4 v13, 0x0

    .line 66
    invoke-static/range {v8 .. v13}, Lcom/intsig/utils/html/HtmlUtilKt;->〇o00〇〇Oo(Ljava/lang/String;ILcom/intsig/utils/html/HtmlParser$TagHandler;Landroid/text/Html$ImageGetter;ILjava/lang/Object;)Ljava/lang/CharSequence;

    .line 67
    .line 68
    .line 69
    move-result-object v7

    .line 70
    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    .line 72
    .line 73
    :goto_5
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 74
    .line 75
    .line 76
    move-result v4

    .line 77
    if-nez v4, :cond_8

    .line 78
    .line 79
    const/4 v4, 0x1

    .line 80
    goto :goto_6

    .line 81
    :cond_8
    const/4 v4, 0x0

    .line 82
    :goto_6
    if-eqz v4, :cond_9

    .line 83
    .line 84
    move-object v7, v3

    .line 85
    goto :goto_7

    .line 86
    :cond_9
    move-object v7, v0

    .line 87
    :goto_7
    const/4 v8, 0x0

    .line 88
    const/4 v9, 0x0

    .line 89
    const/4 v10, 0x0

    .line 90
    const/4 v11, 0x7

    .line 91
    const/4 v12, 0x0

    .line 92
    invoke-static/range {v7 .. v12}, Lcom/intsig/utils/html/HtmlUtilKt;->〇o00〇〇Oo(Ljava/lang/String;ILcom/intsig/utils/html/HtmlParser$TagHandler;Landroid/text/Html$ImageGetter;ILjava/lang/Object;)Ljava/lang/CharSequence;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    .line 97
    .line 98
    .line 99
    move-result v3

    .line 100
    if-nez v3, :cond_a

    .line 101
    .line 102
    goto :goto_8

    .line 103
    :cond_a
    const/4 v5, 0x0

    .line 104
    :goto_8
    if-nez v5, :cond_10

    .line 105
    .line 106
    instance-of v3, v0, Landroid/text/Spannable;

    .line 107
    .line 108
    if-nez v3, :cond_b

    .line 109
    .line 110
    goto :goto_a

    .line 111
    :cond_b
    const/4 v6, 0x0

    .line 112
    const/4 v7, 0x0

    .line 113
    const/4 v8, 0x6

    .line 114
    const/4 v9, 0x0

    .line 115
    move-object v4, v0

    .line 116
    move-object v5, v2

    .line 117
    invoke-static/range {v4 .. v9}, Lkotlin/text/StringsKt;->oO00OOO(Ljava/lang/CharSequence;Ljava/lang/String;IZILjava/lang/Object;)I

    .line 118
    .line 119
    .line 120
    move-result v3

    .line 121
    if-gez v3, :cond_e

    .line 122
    .line 123
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇〇o0〇8()Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;

    .line 124
    .line 125
    .line 126
    move-result-object v2

    .line 127
    if-eqz v2, :cond_c

    .line 128
    .line 129
    iget-object v1, v2, Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;->〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 130
    .line 131
    :cond_c
    if-nez v1, :cond_d

    .line 132
    .line 133
    goto :goto_9

    .line 134
    :cond_d
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    .line 136
    .line 137
    :goto_9
    return-void

    .line 138
    :cond_e
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    .line 139
    .line 140
    .line 141
    move-result v1

    .line 142
    add-int/2addr v1, v3

    .line 143
    move-object v2, v0

    .line 144
    check-cast v2, Landroid/text/Spannable;

    .line 145
    .line 146
    new-instance v4, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog$initGiftDesc$2;

    .line 147
    .line 148
    invoke-direct {v4, p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog$initGiftDesc$2;-><init>(Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;)V

    .line 149
    .line 150
    .line 151
    const/16 v5, 0x11

    .line 152
    .line 153
    invoke-interface {v2, v4, v3, v1, v5}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 154
    .line 155
    .line 156
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇〇o0〇8()Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;

    .line 157
    .line 158
    .line 159
    move-result-object v1

    .line 160
    if-eqz v1, :cond_f

    .line 161
    .line 162
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;->〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 163
    .line 164
    if-eqz v1, :cond_f

    .line 165
    .line 166
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    .line 168
    .line 169
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    .line 170
    .line 171
    .line 172
    move-result-object v0

    .line 173
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 174
    .line 175
    .line 176
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 177
    .line 178
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 179
    .line 180
    .line 181
    move-result-object v0

    .line 182
    const v2, 0x7f0602f5

    .line 183
    .line 184
    .line 185
    invoke-static {v0, v2}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 186
    .line 187
    .line 188
    move-result v0

    .line 189
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setHighlightColor(I)V

    .line 190
    .line 191
    .line 192
    :cond_f
    return-void

    .line 193
    :cond_10
    :goto_a
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇〇o0〇8()Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;

    .line 194
    .line 195
    .line 196
    move-result-object v2

    .line 197
    if-eqz v2, :cond_11

    .line 198
    .line 199
    iget-object v1, v2, Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;->〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 200
    .line 201
    :cond_11
    if-nez v1, :cond_12

    .line 202
    .line 203
    goto :goto_b

    .line 204
    :cond_12
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 205
    .line 206
    .line 207
    :goto_b
    return-void
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private final o〇O8OO()V
    .locals 5

    .line 1
    new-instance v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->CSPremiumPop:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 7
    .line 8
    iput-object v1, v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 9
    .line 10
    sget-object v1, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->PAY_POST_POSITION_ACTIVITY:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 11
    .line 12
    iput-object v1, v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 13
    .line 14
    sget-object v1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_SCAN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 15
    .line 16
    iput-object v1, v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 17
    .line 18
    sget-object v1, Lcom/intsig/camscanner/purchase/entity/Function;->PAY_POST_POSITION_ACTIVITY:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 19
    .line 20
    iput-object v1, v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 21
    .line 22
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->o〇00O:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 23
    .line 24
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    if-eqz v0, :cond_2

    .line 29
    .line 30
    new-instance v1, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 31
    .line 32
    iget-object v2, p0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->o〇00O:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 33
    .line 34
    const/4 v3, 0x0

    .line 35
    const-string v4, "mPurchaseTracker"

    .line 36
    .line 37
    if-nez v2, :cond_0

    .line 38
    .line 39
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 40
    .line 41
    .line 42
    move-object v2, v3

    .line 43
    :cond_0
    invoke-direct {v1, v0, v2}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 44
    .line 45
    .line 46
    iput-object v1, p0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 47
    .line 48
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->o〇00O:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 49
    .line 50
    if-nez v0, :cond_1

    .line 51
    .line 52
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 53
    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_1
    move-object v3, v0

    .line 57
    :goto_0
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->OOO(Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 58
    .line 59
    .line 60
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 61
    .line 62
    if-eqz v0, :cond_2

    .line 63
    .line 64
    new-instance v1, Lo088O8800/〇o00〇〇Oo;

    .line 65
    .line 66
    invoke-direct {v1, p0}, Lo088O8800/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;)V

    .line 67
    .line 68
    .line 69
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->〇O〇80o08O(Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient$PurchaseCallback;)V

    .line 70
    .line 71
    .line 72
    :cond_2
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private final 〇088O()V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇OOo8〇0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$WorldWideMonthlyMembershipActivity;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$WorldWideMonthlyMembershipActivity;->product_description_656:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductDescription656;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductDescription656;->price_description:Ljava/lang/String;

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    move-object v0, v1

    .line 14
    :goto_0
    const-string v2, ""

    .line 15
    .line 16
    if-nez v0, :cond_1

    .line 17
    .line 18
    move-object v0, v2

    .line 19
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇〇o0〇8()Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;

    .line 20
    .line 21
    .line 22
    move-result-object v3

    .line 23
    if-eqz v3, :cond_2

    .line 24
    .line 25
    iget-object v1, v3, Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;->oOo〇8o008:Landroidx/appcompat/widget/AppCompatTextView;

    .line 26
    .line 27
    :cond_2
    if-nez v1, :cond_3

    .line 28
    .line 29
    goto :goto_3

    .line 30
    :cond_3
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 31
    .line 32
    .line 33
    move-result v3

    .line 34
    if-nez v3, :cond_4

    .line 35
    .line 36
    const/4 v3, 0x1

    .line 37
    goto :goto_1

    .line 38
    :cond_4
    const/4 v3, 0x0

    .line 39
    :goto_1
    if-eqz v3, :cond_5

    .line 40
    .line 41
    move-object v4, v2

    .line 42
    goto :goto_2

    .line 43
    :cond_5
    move-object v4, v0

    .line 44
    :goto_2
    const/4 v5, 0x0

    .line 45
    const/4 v6, 0x0

    .line 46
    const/4 v7, 0x0

    .line 47
    const/4 v8, 0x7

    .line 48
    const/4 v9, 0x0

    .line 49
    invoke-static/range {v4 .. v9}, Lcom/intsig/utils/html/HtmlUtilKt;->〇o00〇〇Oo(Ljava/lang/String;ILcom/intsig/utils/html/HtmlParser$TagHandler;Landroid/text/Html$ImageGetter;ILjava/lang/Object;)Ljava/lang/CharSequence;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    .line 55
    .line 56
    :goto_3
    return-void
    .line 57
    .line 58
.end method

.method public static final 〇0ooOOo()Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇080OO8〇0:Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog$Companion;->〇080()Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private static final 〇0〇0(Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V
    .locals 1

    .line 1
    const-string/jumbo p1, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    new-instance p1, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v0, "buy end: "

    .line 13
    .line 14
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    const-string v0, "VipActivityPurchaseDialog"

    .line 25
    .line 26
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    if-eqz p2, :cond_0

    .line 30
    .line 31
    invoke-virtual {p0}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->dismissAllowingStateLoss()V

    .line 32
    .line 33
    .line 34
    :cond_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private final 〇8〇80o()V
    .locals 8

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇OOo8〇0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$WorldWideMonthlyMembershipActivity;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-eqz v0, :cond_0

    .line 5
    .line 6
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$WorldWideMonthlyMembershipActivity;->product_description_656:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductDescription656;

    .line 7
    .line 8
    if-eqz v0, :cond_0

    .line 9
    .line 10
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductDescription656;->button_description:Ljava/lang/String;

    .line 11
    .line 12
    goto :goto_0

    .line 13
    :cond_0
    move-object v0, v1

    .line 14
    :goto_0
    if-nez v0, :cond_1

    .line 15
    .line 16
    const-string v0, ""

    .line 17
    .line 18
    :cond_1
    move-object v2, v0

    .line 19
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    const/4 v3, 0x0

    .line 24
    const/4 v4, 0x1

    .line 25
    if-nez v0, :cond_2

    .line 26
    .line 27
    const/4 v0, 0x1

    .line 28
    goto :goto_1

    .line 29
    :cond_2
    const/4 v0, 0x0

    .line 30
    :goto_1
    if-ne v0, v4, :cond_5

    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇〇o0〇8()Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    if-eqz v0, :cond_3

    .line 37
    .line 38
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;->OO〇00〇8oO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 39
    .line 40
    :cond_3
    if-nez v1, :cond_4

    .line 41
    .line 42
    goto :goto_2

    .line 43
    :cond_4
    new-array v0, v4, [Ljava/lang/Object;

    .line 44
    .line 45
    const-string v2, "7"

    .line 46
    .line 47
    aput-object v2, v0, v3

    .line 48
    .line 49
    const v2, 0x7f131501

    .line 50
    .line 51
    .line 52
    invoke-virtual {p0, v2, v0}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v0

    .line 56
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    .line 58
    .line 59
    goto :goto_2

    .line 60
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇〇o0〇8()Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;

    .line 61
    .line 62
    .line 63
    move-result-object v0

    .line 64
    if-eqz v0, :cond_6

    .line 65
    .line 66
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;->OO〇00〇8oO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 67
    .line 68
    :cond_6
    if-nez v1, :cond_7

    .line 69
    .line 70
    goto :goto_2

    .line 71
    :cond_7
    const/4 v3, 0x0

    .line 72
    const/4 v4, 0x0

    .line 73
    const/4 v5, 0x0

    .line 74
    const/4 v6, 0x7

    .line 75
    const/4 v7, 0x0

    .line 76
    invoke-static/range {v2 .. v7}, Lcom/intsig/utils/html/HtmlUtilKt;->〇o00〇〇Oo(Ljava/lang/String;ILcom/intsig/utils/html/HtmlParser$TagHandler;Landroid/text/Html$ImageGetter;ILjava/lang/Object;)Ljava/lang/CharSequence;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    .line 82
    .line 83
    :goto_2
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private final 〇8〇OOoooo()V
    .locals 9

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇〇o0〇8()Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_2

    .line 7
    .line 8
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 9
    .line 10
    if-eqz v0, :cond_2

    .line 11
    .line 12
    const v2, 0x7f080b0e

    .line 13
    .line 14
    .line 15
    invoke-virtual {v0, v2}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    instance-of v2, v0, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 23
    .line 24
    if-eqz v2, :cond_0

    .line 25
    .line 26
    check-cast v0, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    move-object v0, v1

    .line 30
    :goto_0
    if-nez v0, :cond_1

    .line 31
    .line 32
    goto :goto_1

    .line 33
    :cond_1
    const-string v2, "1125:481"

    .line 34
    .line 35
    iput-object v2, v0, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;->dimensionRatio:Ljava/lang/String;

    .line 36
    .line 37
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇OOo8〇0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$WorldWideMonthlyMembershipActivity;

    .line 38
    .line 39
    if-eqz v0, :cond_3

    .line 40
    .line 41
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$WorldWideMonthlyMembershipActivity;->product_description_656:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductDescription656;

    .line 42
    .line 43
    if-eqz v0, :cond_3

    .line 44
    .line 45
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductDescription656;->title:Ljava/lang/String;

    .line 46
    .line 47
    goto :goto_2

    .line 48
    :cond_3
    move-object v0, v1

    .line 49
    :goto_2
    if-nez v0, :cond_4

    .line 50
    .line 51
    const-string v0, ""

    .line 52
    .line 53
    :cond_4
    move-object v2, v0

    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    .line 55
    .line 56
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 57
    .line 58
    .line 59
    const-string v3, "initBannerTitle title = "

    .line 60
    .line 61
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    .line 63
    .line 64
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    .line 66
    .line 67
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    const-string v3, "VipActivityPurchaseDialog"

    .line 72
    .line 73
    invoke-static {v3, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    .line 77
    .line 78
    .line 79
    move-result v0

    .line 80
    const/4 v3, 0x1

    .line 81
    const/4 v8, 0x0

    .line 82
    if-nez v0, :cond_5

    .line 83
    .line 84
    const/4 v0, 0x1

    .line 85
    goto :goto_3

    .line 86
    :cond_5
    const/4 v0, 0x0

    .line 87
    :goto_3
    if-ne v0, v3, :cond_6

    .line 88
    .line 89
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇〇o0〇8()Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;

    .line 90
    .line 91
    .line 92
    move-result-object v0

    .line 93
    if-eqz v0, :cond_9

    .line 94
    .line 95
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;->〇8〇oO〇〇8o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 96
    .line 97
    if-eqz v0, :cond_9

    .line 98
    .line 99
    const v1, 0x7f1314fa

    .line 100
    .line 101
    .line 102
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 103
    .line 104
    .line 105
    goto :goto_4

    .line 106
    :cond_6
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇〇o0〇8()Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;

    .line 107
    .line 108
    .line 109
    move-result-object v0

    .line 110
    if-eqz v0, :cond_7

    .line 111
    .line 112
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;->〇8〇oO〇〇8o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 113
    .line 114
    :cond_7
    if-nez v1, :cond_8

    .line 115
    .line 116
    goto :goto_4

    .line 117
    :cond_8
    const/4 v3, 0x0

    .line 118
    const/4 v4, 0x0

    .line 119
    const/4 v5, 0x0

    .line 120
    const/4 v6, 0x7

    .line 121
    const/4 v7, 0x0

    .line 122
    invoke-static/range {v2 .. v7}, Lcom/intsig/utils/html/HtmlUtilKt;->〇o00〇〇Oo(Ljava/lang/String;ILcom/intsig/utils/html/HtmlParser$TagHandler;Landroid/text/Html$ImageGetter;ILjava/lang/Object;)Ljava/lang/CharSequence;

    .line 123
    .line 124
    .line 125
    move-result-object v0

    .line 126
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    .line 128
    .line 129
    :cond_9
    :goto_4
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇〇o0〇8()Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;

    .line 130
    .line 131
    .line 132
    move-result-object v0

    .line 133
    if-eqz v0, :cond_a

    .line 134
    .line 135
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;->o8〇OO0〇0o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 136
    .line 137
    if-eqz v0, :cond_a

    .line 138
    .line 139
    invoke-static {v0, v8}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 140
    .line 141
    .line 142
    :cond_a
    return-void
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇0〇0(Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private final 〇o〇88〇8()V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/app/AppSwitch;->o〇0()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_1

    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇〇o0〇8()Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;->OO:Landroidx/constraintlayout/widget/Group;

    .line 14
    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    const/4 v1, 0x0

    .line 18
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 19
    .line 20
    .line 21
    :cond_0
    return-void

    .line 22
    :cond_1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-static {v0}, Lcom/intsig/camscanner/util/DarkModeUtils;->〇080(Landroid/content/Context;)Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-eqz v0, :cond_2

    .line 31
    .line 32
    const-string v0, "#FFCECECE"

    .line 33
    .line 34
    goto :goto_0

    .line 35
    :cond_2
    const-string v0, "#FF5A5A5A"

    .line 36
    .line 37
    :goto_0
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->requireContext()Landroid/content/Context;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇〇o0〇8()Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;

    .line 42
    .line 43
    .line 44
    move-result-object v2

    .line 45
    if-eqz v2, :cond_3

    .line 46
    .line 47
    iget-object v2, v2, Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;->oOo0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 48
    .line 49
    goto :goto_1

    .line 50
    :cond_3
    const/4 v2, 0x0

    .line 51
    :goto_1
    invoke-static {v1, v2, v0}, Lcom/intsig/camscanner/util/StringUtil;->〇〇888(Landroid/content/Context;Landroid/widget/TextView;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇〇o0〇8()Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    if-eqz v0, :cond_4

    .line 59
    .line 60
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;->OO:Landroidx/constraintlayout/widget/Group;

    .line 61
    .line 62
    if-eqz v0, :cond_4

    .line 63
    .line 64
    const/4 v1, 0x1

    .line 65
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 66
    .line 67
    .line 68
    :cond_4
    return-void
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private final 〇〇o0〇8()Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->o0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->〇0O:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/FragmentVipActivityPurchaseDialogBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
.end method


# virtual methods
.method public getTheme()I
    .locals 1

    .line 1
    const v0, 0x7f140193

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method protected init(Landroid/os/Bundle;)V
    .locals 10

    .line 1
    const-string p1, "VipActivityPurchaseDialog"

    .line 2
    .line 3
    const-string v0, "init"

    .line 4
    .line 5
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 p1, 0x0

    .line 9
    invoke-virtual {p0, p1}, Landroidx/fragment/app/DialogFragment;->setCancelable(Z)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    new-instance v1, Lo088O8800/〇080;

    .line 19
    .line 20
    invoke-direct {v1}, Lo088O8800/〇080;-><init>()V

    .line 21
    .line 22
    .line 23
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 24
    .line 25
    .line 26
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->oooO888()Z

    .line 27
    .line 28
    .line 29
    move-result v0

    .line 30
    if-nez v0, :cond_1

    .line 31
    .line 32
    invoke-virtual {p0}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->dismissAllowingStateLoss()V

    .line 33
    .line 34
    .line 35
    return-void

    .line 36
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->oOoO8OO〇()V

    .line 37
    .line 38
    .line 39
    sget-object v0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseManager;->〇080:Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseManager;

    .line 40
    .line 41
    const/4 v1, 0x1

    .line 42
    const/4 v2, 0x0

    .line 43
    invoke-static {v0, p1, v1, v2}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseManager;->o〇0(Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseManager;ZILjava/lang/Object;)V

    .line 44
    .line 45
    .line 46
    const-string v3, "CSPremiumPop"

    .line 47
    .line 48
    const-string v4, "scheme"

    .line 49
    .line 50
    sget-object p1, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->PAY_POST_POSITION_ACTIVITY:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 51
    .line 52
    invoke-virtual {p1}, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->toTrackerValue()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v5

    .line 56
    const-string v6, "from_part"

    .line 57
    .line 58
    sget-object p1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_SCAN:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 59
    .line 60
    invoke-virtual {p1}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v7

    .line 64
    const-string v8, "from"

    .line 65
    .line 66
    sget-object p1, Lcom/intsig/camscanner/purchase/entity/Function;->PAY_POST_POSITION_ACTIVITY:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 67
    .line 68
    invoke-virtual {p1}, Lcom/intsig/camscanner/purchase/entity/Function;->toTrackerValue()Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object v9

    .line 72
    invoke-static/range {v3 .. v9}, Lcom/intsig/camscanner/log/LogAgentData;->〇O〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    return-void
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method protected isDefaultExpanded()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/utils/ClickLimit;->〇o〇()Lcom/intsig/utils/ClickLimit;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0, p1}, Lcom/intsig/utils/ClickLimit;->〇080(Landroid/view/View;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    return-void

    .line 12
    :cond_0
    if-eqz p1, :cond_1

    .line 13
    .line 14
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 15
    .line 16
    .line 17
    move-result p1

    .line 18
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    goto :goto_0

    .line 23
    :cond_1
    const/4 p1, 0x0

    .line 24
    :goto_0
    if-nez p1, :cond_2

    .line 25
    .line 26
    goto :goto_1

    .line 27
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    const v1, 0x7f0a12be

    .line 32
    .line 33
    .line 34
    if-ne v0, v1, :cond_3

    .line 35
    .line 36
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->o880()V

    .line 37
    .line 38
    .line 39
    goto :goto_2

    .line 40
    :cond_3
    :goto_1
    if-nez p1, :cond_4

    .line 41
    .line 42
    goto :goto_2

    .line 43
    :cond_4
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 44
    .line 45
    .line 46
    move-result p1

    .line 47
    const v0, 0x7f0a16ea

    .line 48
    .line 49
    .line 50
    if-ne p1, v0, :cond_5

    .line 51
    .line 52
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->o00〇88〇08()V

    .line 53
    .line 54
    .line 55
    :cond_5
    :goto_2
    return-void
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method protected provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d035b

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public setDialogDismissListener(Lcom/intsig/callback/DialogDismissListener;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseDialog;->O8o08O8O:Lcom/intsig/callback/DialogDismissListener;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

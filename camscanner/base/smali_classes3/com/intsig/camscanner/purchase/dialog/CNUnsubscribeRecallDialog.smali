.class public final Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;
.super Lcom/intsig/app/BaseDialogFragment;
.source "CNUnsubscribeRecallDialog.kt"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog$Companion;,
        Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog$FunctionViewHolder;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final O0O:Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field static final synthetic o8oOOo:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private O8o08O8O:J

.field private final OO〇00〇8oO:[Ljava/lang/Integer;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o8〇OO0〇0o:Lcom/intsig/camscanner/Client/ProgressDialogClient;

.field private final oOo0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOo〇8o008:Z

.field private ooo0〇〇O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:I

.field private 〇080OO8〇0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecall;

.field private final 〇08O〇00〇o:J

.field private 〇0O:Z

.field private 〇8〇oO〇〇8o:I

.field private 〇〇08O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "binding"

    .line 7
    .line 8
    const-string v3, "getBinding()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->o8oOOo:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->O0O:Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog$Companion;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public constructor <init>()V
    .locals 8

    .line 1
    invoke-direct {p0}, Lcom/intsig/app/BaseDialogFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    const-wide/32 v0, 0x5265c00

    .line 5
    .line 6
    .line 7
    iput-wide v0, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇08O〇00〇o:J

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 10
    .line 11
    const-class v3, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 12
    .line 13
    const/4 v5, 0x0

    .line 14
    const/4 v6, 0x4

    .line 15
    const/4 v7, 0x0

    .line 16
    move-object v2, v0

    .line 17
    move-object v4, p0

    .line 18
    invoke-direct/range {v2 .. v7}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 19
    .line 20
    .line 21
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->oOo0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 22
    .line 23
    const/4 v0, 0x4

    .line 24
    new-array v0, v0, [Ljava/lang/Integer;

    .line 25
    .line 26
    const v1, 0x7f080a5a

    .line 27
    .line 28
    .line 29
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    const/4 v2, 0x0

    .line 34
    aput-object v1, v0, v2

    .line 35
    .line 36
    const v1, 0x7f080a5b

    .line 37
    .line 38
    .line 39
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    const/4 v2, 0x1

    .line 44
    aput-object v1, v0, v2

    .line 45
    .line 46
    const v1, 0x7f080a59

    .line 47
    .line 48
    .line 49
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 50
    .line 51
    .line 52
    move-result-object v1

    .line 53
    const/4 v2, 0x2

    .line 54
    aput-object v1, v0, v2

    .line 55
    .line 56
    const v1, 0x7f080a58

    .line 57
    .line 58
    .line 59
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 60
    .line 61
    .line 62
    move-result-object v1

    .line 63
    const/4 v2, 0x3

    .line 64
    aput-object v1, v0, v2

    .line 65
    .line 66
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->OO〇00〇8oO:[Ljava/lang/Integer;

    .line 67
    .line 68
    const-string v0, ""

    .line 69
    .line 70
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->ooo0〇〇O:Ljava/lang/String;

    .line 71
    .line 72
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇〇08O:Ljava/lang/String;

    .line 73
    .line 74
    return-void
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public static final synthetic O0〇0(Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8O0880(Ljava/util/ArrayList;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final O8〇8〇O80(Z)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇0oO〇oo00()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const v1, 0x7f080d8f

    .line 6
    .line 7
    .line 8
    const v2, 0x7f08060d

    .line 9
    .line 10
    .line 11
    if-eqz v0, :cond_1

    .line 12
    .line 13
    if-eqz p1, :cond_0

    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 16
    .line 17
    .line 18
    move-result-object p1

    .line 19
    if-eqz p1, :cond_3

    .line 20
    .line 21
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->oOo0:Landroid/widget/CheckBox;

    .line 22
    .line 23
    if-eqz p1, :cond_3

    .line 24
    .line 25
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 26
    .line 27
    .line 28
    goto :goto_0

    .line 29
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 30
    .line 31
    .line 32
    move-result-object p1

    .line 33
    if-eqz p1, :cond_3

    .line 34
    .line 35
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->oOo0:Landroid/widget/CheckBox;

    .line 36
    .line 37
    if-eqz p1, :cond_3

    .line 38
    .line 39
    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 40
    .line 41
    .line 42
    goto :goto_0

    .line 43
    :cond_1
    if-eqz p1, :cond_2

    .line 44
    .line 45
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 46
    .line 47
    .line 48
    move-result-object p1

    .line 49
    if-eqz p1, :cond_3

    .line 50
    .line 51
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->oOo〇8o008:Landroid/widget/CheckBox;

    .line 52
    .line 53
    if-eqz p1, :cond_3

    .line 54
    .line 55
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 56
    .line 57
    .line 58
    goto :goto_0

    .line 59
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 60
    .line 61
    .line 62
    move-result-object p1

    .line 63
    if-eqz p1, :cond_3

    .line 64
    .line 65
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->oOo〇8o008:Landroid/widget/CheckBox;

    .line 66
    .line 67
    if-eqz p1, :cond_3

    .line 68
    .line 69
    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 70
    .line 71
    .line 72
    :cond_3
    :goto_0
    return-void
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method private final Ooo8o()V
    .locals 5

    .line 1
    const-string v0, "CNUnsubscribeRecallDialog"

    .line 2
    .line 3
    const-string v1, "initRecyclerView()"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const/4 v1, 0x0

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->o8oOOo:Landroidx/recyclerview/widget/RecyclerView;

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_0
    move-object v0, v1

    .line 19
    :goto_0
    if-nez v0, :cond_1

    .line 20
    .line 21
    goto :goto_1

    .line 22
    :cond_1
    new-instance v2, Landroidx/recyclerview/widget/GridLayoutManager;

    .line 23
    .line 24
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 25
    .line 26
    .line 27
    move-result-object v3

    .line 28
    const/4 v4, 0x4

    .line 29
    invoke-direct {v2, v3, v4}, Landroidx/recyclerview/widget/GridLayoutManager;-><init>(Landroid/content/Context;I)V

    .line 30
    .line 31
    .line 32
    invoke-virtual {v0, v2}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 33
    .line 34
    .line 35
    :goto_1
    new-instance v0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog$initRecyclerView$adapter$1;

    .line 36
    .line 37
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog$initRecyclerView$adapter$1;-><init>(Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇0ooOOo()Ljava/util/ArrayList;

    .line 41
    .line 42
    .line 43
    move-result-object v2

    .line 44
    invoke-virtual {v0, v2}, Lcom/intsig/adapter/BaseRecyclerViewAdapter;->O〇8O8〇008(Ljava/util/List;)V

    .line 45
    .line 46
    .line 47
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 48
    .line 49
    .line 50
    move-result-object v2

    .line 51
    if-eqz v2, :cond_2

    .line 52
    .line 53
    iget-object v1, v2, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->o8oOOo:Landroidx/recyclerview/widget/RecyclerView;

    .line 54
    .line 55
    :cond_2
    if-nez v1, :cond_3

    .line 56
    .line 57
    goto :goto_2

    .line 58
    :cond_3
    invoke-virtual {v1, v0}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 59
    .line 60
    .line 61
    :goto_2
    return-void
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public static synthetic o00〇88〇08(Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;Lcom/intsig/view/countdown/CountdownView;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇〇〇0(Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;Lcom/intsig/view/countdown/CountdownView;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static synthetic o880(Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇〇〇00(Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static final synthetic o〇0〇o(Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇O0o〇〇o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final 〇088O()V
    .locals 8

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇0oO〇oo00()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_5

    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    const/4 v2, 0x1

    .line 13
    if-eqz v0, :cond_0

    .line 14
    .line 15
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->〇〇08O:Landroid/widget/RelativeLayout;

    .line 16
    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    invoke-static {v0, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 20
    .line 21
    .line 22
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    if-eqz v0, :cond_1

    .line 27
    .line 28
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->〇〇08O:Landroid/widget/RelativeLayout;

    .line 29
    .line 30
    if-eqz v0, :cond_1

    .line 31
    .line 32
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 33
    .line 34
    .line 35
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 36
    .line 37
    .line 38
    move-result-object v0

    .line 39
    if-eqz v0, :cond_2

    .line 40
    .line 41
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->o8o:Landroid/widget/TextView;

    .line 42
    .line 43
    if-eqz v0, :cond_2

    .line 44
    .line 45
    const/4 v3, 0x0

    .line 46
    invoke-static {v0, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 47
    .line 48
    .line 49
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    if-eqz v0, :cond_3

    .line 54
    .line 55
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->〇o0O:Landroid/widget/TextView;

    .line 56
    .line 57
    if-eqz v0, :cond_3

    .line 58
    .line 59
    invoke-static {v0, v2}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 60
    .line 61
    .line 62
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    if-eqz v0, :cond_4

    .line 67
    .line 68
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->oo8ooo8O:Landroid/widget/TextView;

    .line 69
    .line 70
    :cond_4
    move-object v2, v1

    .line 71
    const v3, 0x3f666666    # 0.9f

    .line 72
    .line 73
    .line 74
    const-wide/16 v4, 0x7d0

    .line 75
    .line 76
    const/4 v6, -0x1

    .line 77
    const/4 v7, 0x0

    .line 78
    invoke-static/range {v2 .. v7}, Lcom/intsig/utils/AnimateUtils;->〇〇888(Landroid/view/View;FJILandroid/animation/Animator$AnimatorListener;)V

    .line 79
    .line 80
    .line 81
    goto :goto_3

    .line 82
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    if-eqz v0, :cond_6

    .line 87
    .line 88
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->〇O〇〇O8:Landroid/widget/ScrollView;

    .line 89
    .line 90
    if-eqz v0, :cond_6

    .line 91
    .line 92
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    goto :goto_0

    .line 97
    :cond_6
    move-object v0, v1

    .line 98
    :goto_0
    if-nez v0, :cond_7

    .line 99
    .line 100
    goto :goto_1

    .line 101
    :cond_7
    const/4 v2, -0x1

    .line 102
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 103
    .line 104
    :goto_1
    if-nez v0, :cond_8

    .line 105
    .line 106
    goto :goto_2

    .line 107
    :cond_8
    const/4 v2, -0x2

    .line 108
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 109
    .line 110
    :goto_2
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 111
    .line 112
    .line 113
    move-result-object v2

    .line 114
    if-eqz v2, :cond_9

    .line 115
    .line 116
    iget-object v1, v2, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->〇O〇〇O8:Landroid/widget/ScrollView;

    .line 117
    .line 118
    :cond_9
    if-nez v1, :cond_a

    .line 119
    .line 120
    goto :goto_3

    .line 121
    :cond_a
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 122
    .line 123
    .line 124
    :goto_3
    return-void
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private final 〇08O()V
    .locals 3

    .line 1
    const-string v0, "CNUnsubscribeRecallDialog"

    .line 2
    .line 3
    const-string v1, "queryAddCouponLooper"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Lcom/intsig/tianshu/addcoupon/AddCouponRequest;

    .line 9
    .line 10
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇080OO8〇0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecall;

    .line 11
    .line 12
    if-eqz v1, :cond_0

    .line 13
    .line 14
    iget v1, v1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecall;->coupon_type:I

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_0
    const/16 v1, 0x14

    .line 18
    .line 19
    :goto_0
    const/4 v2, 0x0

    .line 20
    invoke-direct {v0, v1, v2}, Lcom/intsig/tianshu/addcoupon/AddCouponRequest;-><init>(II)V

    .line 21
    .line 22
    .line 23
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->O8()Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    invoke-virtual {v0, v1}, Lcom/intsig/tianshu/addcoupon/AddCouponRequest;->OO0o〇〇(Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-static {}, Lcom/intsig/utils/LanguageUtil;->〇〇888()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    invoke-virtual {v0, v1}, Lcom/intsig/tianshu/addcoupon/AddCouponRequest;->〇〇808〇(Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    invoke-static {}, Lcom/intsig/utils/LanguageUtil;->O8()Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    invoke-virtual {v0, v1}, Lcom/intsig/tianshu/addcoupon/AddCouponRequest;->Oooo8o0〇(Ljava/lang/String;)V

    .line 42
    .line 43
    .line 44
    sget-object v1, Lcom/intsig/camscanner/app/AppSwitch;->〇O〇:Ljava/lang/String;

    .line 45
    .line 46
    invoke-virtual {v0, v1}, Lcom/intsig/tianshu/addcoupon/AddCouponRequest;->〇O00(Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    sget-object v1, Lcom/intsig/camscanner/guide/GuideGrayInterval;->Companion:Lcom/intsig/camscanner/guide/GuideGrayInterval$Companion;

    .line 50
    .line 51
    invoke-virtual {v1}, Lcom/intsig/camscanner/guide/GuideGrayInterval$Companion;->O8()I

    .line 52
    .line 53
    .line 54
    move-result v1

    .line 55
    invoke-virtual {v0, v1}, Lcom/intsig/tianshu/addcoupon/AddCouponRequest;->〇O〇(I)V

    .line 56
    .line 57
    .line 58
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v1

    .line 62
    new-instance v2, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog$queryAddCouponLooper$1;

    .line 63
    .line 64
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog$queryAddCouponLooper$1;-><init>(Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;)V

    .line 65
    .line 66
    .line 67
    invoke-static {v1, v0, v2}, Lcom/intsig/tianshu/TianShuAPI;->〇〇888(Ljava/lang/String;Lcom/intsig/tianshu/addcoupon/AddCouponRequest;Lcom/intsig/okgo/callback/CustomStringCallback;)V

    .line 68
    .line 69
    .line 70
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private final 〇0oO〇oo00()Z
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->〇80〇808〇O(Landroid/content/Context;)I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    const/16 v2, 0x780

    .line 13
    .line 14
    if-gt v0, v2, :cond_0

    .line 15
    .line 16
    const/4 v0, 0x1

    .line 17
    const/4 v1, 0x1

    .line 18
    :cond_0
    return v1
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final 〇8O0880(Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/comm/purchase/entity/Coupon;",
            ">;)V"
        }
    .end annotation

    .line 1
    new-instance v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 4
    .line 5
    .line 6
    sget-object v1, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->CSPremiumPop:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId(Lcom/intsig/camscanner/purchase/track/PurchasePageId;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    sget-object v1, Lcom/intsig/camscanner/purchase/entity/Function;->MARKETING:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 13
    .line 14
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function(Lcom/intsig/camscanner/purchase/entity/Function;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    sget-object v1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_RESUBSCRIBE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 19
    .line 20
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    sget-object v1, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->MAIN_MARKETING_RESUBSCRIBE:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 25
    .line 26
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme(Lcom/intsig/camscanner/purchase/track/PurchaseScheme;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    if-lez v1, :cond_4

    .line 35
    .line 36
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    new-instance v2, Ljava/lang/StringBuilder;

    .line 41
    .line 42
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 43
    .line 44
    .line 45
    const-string v3, "queryCouponList recallCoupon.size = "

    .line 46
    .line 47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v1

    .line 57
    const-string v2, "CNUnsubscribeRecallDialog"

    .line 58
    .line 59
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    .line 63
    .line 64
    .line 65
    move-result v1

    .line 66
    const/4 v3, 0x0

    .line 67
    const/4 v4, 0x0

    .line 68
    :goto_0
    if-ge v4, v1, :cond_2

    .line 69
    .line 70
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 71
    .line 72
    .line 73
    move-result-object v5

    .line 74
    const-string v6, "recallCoupon[i]"

    .line 75
    .line 76
    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    check-cast v5, Lcom/intsig/comm/purchase/entity/Coupon;

    .line 80
    .line 81
    iget-object v6, v5, Lcom/intsig/comm/purchase/entity/Coupon;->product_class:[Ljava/lang/String;

    .line 82
    .line 83
    aget-object v6, v6, v3

    .line 84
    .line 85
    const-string v7, "ms"

    .line 86
    .line 87
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 88
    .line 89
    .line 90
    move-result v6

    .line 91
    const-string v7, "couponTemp.coupon"

    .line 92
    .line 93
    if-eqz v6, :cond_0

    .line 94
    .line 95
    iget-object v6, v5, Lcom/intsig/comm/purchase/entity/Coupon;->coupon:Ljava/lang/String;

    .line 96
    .line 97
    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    .line 99
    .line 100
    iput-object v6, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->ooo0〇〇O:Ljava/lang/String;

    .line 101
    .line 102
    new-instance v8, Ljava/lang/StringBuilder;

    .line 103
    .line 104
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 105
    .line 106
    .line 107
    const-string v9, "print msCoupon "

    .line 108
    .line 109
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    .line 111
    .line 112
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    .line 114
    .line 115
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 116
    .line 117
    .line 118
    move-result-object v6

    .line 119
    invoke-static {v2, v6}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    .line 121
    .line 122
    :cond_0
    iget-object v6, v5, Lcom/intsig/comm/purchase/entity/Coupon;->product_class:[Ljava/lang/String;

    .line 123
    .line 124
    aget-object v6, v6, v3

    .line 125
    .line 126
    const-string/jumbo v8, "ys"

    .line 127
    .line 128
    .line 129
    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    .line 130
    .line 131
    .line 132
    move-result v6

    .line 133
    if-eqz v6, :cond_1

    .line 134
    .line 135
    iget-object v5, v5, Lcom/intsig/comm/purchase/entity/Coupon;->coupon:Ljava/lang/String;

    .line 136
    .line 137
    invoke-static {v5, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 138
    .line 139
    .line 140
    iput-object v5, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇〇08O:Ljava/lang/String;

    .line 141
    .line 142
    new-instance v6, Ljava/lang/StringBuilder;

    .line 143
    .line 144
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 145
    .line 146
    .line 147
    const-string v7, "print ysCoupon "

    .line 148
    .line 149
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    .line 151
    .line 152
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    .line 154
    .line 155
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 156
    .line 157
    .line 158
    move-result-object v5

    .line 159
    invoke-static {v2, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    .line 161
    .line 162
    :cond_1
    add-int/lit8 v4, v4, 0x1

    .line 163
    .line 164
    goto :goto_0

    .line 165
    :cond_2
    iget p1, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->o〇00O:I

    .line 166
    .line 167
    if-nez p1, :cond_3

    .line 168
    .line 169
    iget-object p1, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇〇08O:Ljava/lang/String;

    .line 170
    .line 171
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->setCouponId(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 172
    .line 173
    .line 174
    goto :goto_1

    .line 175
    :cond_3
    iget-object p1, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->ooo0〇〇O:Ljava/lang/String;

    .line 176
    .line 177
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->setCouponId(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 178
    .line 179
    .line 180
    :goto_1
    sget-object p1, Lcom/intsig/camscanner/purchase/dialog/LocalBottomServerPurchaseDialog;->oOo0:Lcom/intsig/camscanner/purchase/dialog/LocalBottomServerPurchaseDialog$Companion;

    .line 181
    .line 182
    iget v1, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->o〇00O:I

    .line 183
    .line 184
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/purchase/dialog/LocalBottomServerPurchaseDialog$Companion;->〇080(Lcom/intsig/camscanner/purchase/track/PurchaseTracker;I)Lcom/intsig/camscanner/purchase/dialog/LocalBottomServerPurchaseDialog;

    .line 185
    .line 186
    .line 187
    move-result-object p1

    .line 188
    new-instance v0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog$openPayDialog$1;

    .line 189
    .line 190
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog$openPayDialog$1;-><init>(Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;)V

    .line 191
    .line 192
    .line 193
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/purchase/dialog/LocalBottomServerPurchaseDialog;->〇O8〇8000(Lcom/intsig/camscanner/purchase/dialog/LocalBottomServerPurchaseDialog$OnFinishClickListener;)V

    .line 194
    .line 195
    .line 196
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 197
    .line 198
    .line 199
    move-result-object v0

    .line 200
    if-eqz v0, :cond_4

    .line 201
    .line 202
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 203
    .line 204
    .line 205
    move-result-object v0

    .line 206
    if-eqz v0, :cond_4

    .line 207
    .line 208
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/purchase/dialog/LocalBottomServerPurchaseDialog;->〇8O0880(Landroidx/fragment/app/FragmentManager;)V

    .line 209
    .line 210
    .line 211
    :cond_4
    return-void
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method private final 〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->oOo0:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->o8oOOo:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
.end method

.method public static final synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇08O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final 〇O0o〇〇o()V
    .locals 3

    .line 1
    const-string v0, "CNUnsubscribeRecallDialog"

    .line 2
    .line 3
    const-string v1, "queryCouponList"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Lcom/intsig/camscanner/tsapp/purchase/CouponManager;

    .line 9
    .line 10
    invoke-direct {v0}, Lcom/intsig/camscanner/tsapp/purchase/CouponManager;-><init>()V

    .line 11
    .line 12
    .line 13
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 14
    .line 15
    invoke-virtual {v1}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    new-instance v2, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog$queryCouponList$1;

    .line 20
    .line 21
    invoke-direct {v2, v0, p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog$queryCouponList$1;-><init>(Lcom/intsig/camscanner/tsapp/purchase/CouponManager;Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;)V

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/tsapp/purchase/CouponManager;->O8(Landroid/content/Context;Lcom/intsig/okgo/callback/CustomStringCallback;)V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static final 〇O8〇8000()Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->O0O:Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog$Companion;->〇080()Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private final 〇Oo〇O()V
    .locals 6

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇8()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    const-wide/16 v2, 0x0

    .line 6
    .line 7
    cmp-long v4, v0, v2

    .line 8
    .line 9
    if-nez v4, :cond_0

    .line 10
    .line 11
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 12
    .line 13
    .line 14
    move-result-wide v0

    .line 15
    iget-wide v2, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->O8o08O8O:J

    .line 16
    .line 17
    sub-long v2, v0, v2

    .line 18
    .line 19
    const/16 v4, 0x3e8

    .line 20
    .line 21
    int-to-long v4, v4

    .line 22
    div-long/2addr v2, v4

    .line 23
    const/16 v4, 0x3c

    .line 24
    .line 25
    int-to-long v4, v4

    .line 26
    div-long/2addr v2, v4

    .line 27
    new-instance v4, Ljava/lang/StringBuilder;

    .line 28
    .line 29
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 30
    .line 31
    .line 32
    const-string v5, "difMinute"

    .line 33
    .line 34
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    .line 36
    .line 37
    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 38
    .line 39
    .line 40
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v2

    .line 44
    const-string v3, "CNUnsubscribeRecallDialog"

    .line 45
    .line 46
    invoke-static {v3, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    invoke-static {v0, v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->oo〇0〇08(J)V

    .line 50
    .line 51
    .line 52
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 53
    .line 54
    .line 55
    move-result v0

    .line 56
    new-instance v1, Lcom/intsig/camscanner/eventbus/AccountInfoUpdatedEvent;

    .line 57
    .line 58
    invoke-direct {v1}, Lcom/intsig/camscanner/eventbus/AccountInfoUpdatedEvent;-><init>()V

    .line 59
    .line 60
    .line 61
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/eventbus/AccountInfoUpdatedEvent;->〇o00〇〇Oo(Z)V

    .line 62
    .line 63
    .line 64
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/eventbus/AccountInfoUpdatedEvent;->〇080(Ljava/lang/String;)V

    .line 65
    .line 66
    .line 67
    invoke-static {v1}, Lcom/intsig/camscanner/eventbus/CsEventBus;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 68
    .line 69
    .line 70
    :cond_0
    return-void
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private final 〇o08()V
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "StringFormatInvalid"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->o8o:Landroid/widget/TextView;

    .line 9
    .line 10
    move-object v2, v0

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    move-object v2, v1

    .line 13
    :goto_0
    const v3, 0x3f666666    # 0.9f

    .line 14
    .line 15
    .line 16
    const-wide/16 v4, 0x7d0

    .line 17
    .line 18
    const/4 v6, -0x1

    .line 19
    const/4 v7, 0x0

    .line 20
    invoke-static/range {v2 .. v7}, Lcom/intsig/utils/AnimateUtils;->〇〇888(Landroid/view/View;FJILandroid/animation/Animator$AnimatorListener;)V

    .line 21
    .line 22
    .line 23
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇8()J

    .line 24
    .line 25
    .line 26
    move-result-wide v2

    .line 27
    const-wide/16 v4, 0x0

    .line 28
    .line 29
    cmp-long v0, v2, v4

    .line 30
    .line 31
    if-eqz v0, :cond_1

    .line 32
    .line 33
    iget-wide v2, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->O8o08O8O:J

    .line 34
    .line 35
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇8()J

    .line 36
    .line 37
    .line 38
    move-result-wide v6

    .line 39
    sub-long/2addr v2, v6

    .line 40
    iget-wide v6, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇08O〇00〇o:J

    .line 41
    .line 42
    sub-long/2addr v6, v2

    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    .line 44
    .line 45
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 46
    .line 47
    .line 48
    const-string v8, "remainTime"

    .line 49
    .line 50
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    const-string v6, "CNUnsubscribeRecallDialog"

    .line 61
    .line 62
    invoke-static {v6, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    if-eqz v0, :cond_2

    .line 70
    .line 71
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->OO〇00〇8oO:Lcom/intsig/view/countdown/CountdownView;

    .line 72
    .line 73
    if-eqz v0, :cond_2

    .line 74
    .line 75
    iget-wide v6, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇08O〇00〇o:J

    .line 76
    .line 77
    sub-long/2addr v6, v2

    .line 78
    invoke-virtual {v0, v6, v7}, Lcom/intsig/view/countdown/CountdownView;->oO80(J)V

    .line 79
    .line 80
    .line 81
    goto :goto_1

    .line 82
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    if-eqz v0, :cond_2

    .line 87
    .line 88
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->OO〇00〇8oO:Lcom/intsig/view/countdown/CountdownView;

    .line 89
    .line 90
    if-eqz v0, :cond_2

    .line 91
    .line 92
    iget-wide v2, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇08O〇00〇o:J

    .line 93
    .line 94
    invoke-virtual {v0, v2, v3}, Lcom/intsig/view/countdown/CountdownView;->oO80(J)V

    .line 95
    .line 96
    .line 97
    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇088O()V

    .line 98
    .line 99
    .line 100
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 101
    .line 102
    .line 103
    move-result-object v0

    .line 104
    if-eqz v0, :cond_3

    .line 105
    .line 106
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 107
    .line 108
    if-eqz v0, :cond_3

    .line 109
    .line 110
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->〇8〇oO〇〇8o:Landroid/widget/RelativeLayout;

    .line 111
    .line 112
    if-eqz v0, :cond_3

    .line 113
    .line 114
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 115
    .line 116
    .line 117
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 118
    .line 119
    .line 120
    move-result-object v0

    .line 121
    if-eqz v0, :cond_4

    .line 122
    .line 123
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 124
    .line 125
    if-eqz v0, :cond_4

    .line 126
    .line 127
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->o8〇OO0〇0o:Landroid/widget/RelativeLayout;

    .line 128
    .line 129
    if-eqz v0, :cond_4

    .line 130
    .line 131
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 132
    .line 133
    .line 134
    :cond_4
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 135
    .line 136
    .line 137
    move-result-object v0

    .line 138
    if-eqz v0, :cond_5

    .line 139
    .line 140
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->o8o:Landroid/widget/TextView;

    .line 141
    .line 142
    if-eqz v0, :cond_5

    .line 143
    .line 144
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    .line 146
    .line 147
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 148
    .line 149
    .line 150
    move-result-object v0

    .line 151
    if-eqz v0, :cond_6

    .line 152
    .line 153
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 154
    .line 155
    if-eqz v0, :cond_6

    .line 156
    .line 157
    new-instance v2, Lcom/intsig/camscanner/purchase/dialog/oO80;

    .line 158
    .line 159
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/purchase/dialog/oO80;-><init>(Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;)V

    .line 160
    .line 161
    .line 162
    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    .line 164
    .line 165
    :cond_6
    invoke-static {}, Lcom/intsig/utils/PreferenceUtil;->oO80()Lcom/intsig/utils/PreferenceUtil;

    .line 166
    .line 167
    .line 168
    move-result-object v0

    .line 169
    const-string/jumbo v2, "tkreds3sdvv22ccsx3xd3"

    .line 170
    .line 171
    .line 172
    invoke-virtual {v0, v2, v4, v5}, Lcom/intsig/utils/PreferenceUtil;->OO0o〇〇〇〇0(Ljava/lang/String;J)J

    .line 173
    .line 174
    .line 175
    move-result-wide v2

    .line 176
    const/16 v0, 0x3e8

    .line 177
    .line 178
    int-to-long v4, v0

    .line 179
    mul-long v2, v2, v4

    .line 180
    .line 181
    invoke-static {v2, v3}, Lcom/intsig/utils/DateTimeUtil;->〇o00〇〇Oo(J)I

    .line 182
    .line 183
    .line 184
    move-result v0

    .line 185
    const/4 v2, 0x0

    .line 186
    const/4 v3, 0x1

    .line 187
    if-lt v0, v3, :cond_9

    .line 188
    .line 189
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 190
    .line 191
    .line 192
    move-result-object v0

    .line 193
    if-eqz v0, :cond_7

    .line 194
    .line 195
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 196
    .line 197
    goto :goto_2

    .line 198
    :cond_7
    move-object v0, v1

    .line 199
    :goto_2
    if-nez v0, :cond_8

    .line 200
    .line 201
    goto :goto_4

    .line 202
    :cond_8
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OOo0O()Ljava/lang/String;

    .line 203
    .line 204
    .line 205
    move-result-object v4

    .line 206
    new-instance v5, Ljava/lang/StringBuilder;

    .line 207
    .line 208
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 209
    .line 210
    .line 211
    const-string/jumbo v6, "\u5df2\u4e8e"

    .line 212
    .line 213
    .line 214
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    .line 216
    .line 217
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 218
    .line 219
    .line 220
    const-string/jumbo v4, "\u5931\u6548"

    .line 221
    .line 222
    .line 223
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    .line 225
    .line 226
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 227
    .line 228
    .line 229
    move-result-object v4

    .line 230
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    .line 232
    .line 233
    goto :goto_4

    .line 234
    :cond_9
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 235
    .line 236
    .line 237
    move-result-object v0

    .line 238
    if-eqz v0, :cond_a

    .line 239
    .line 240
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 241
    .line 242
    goto :goto_3

    .line 243
    :cond_a
    move-object v0, v1

    .line 244
    :goto_3
    if-nez v0, :cond_b

    .line 245
    .line 246
    goto :goto_4

    .line 247
    :cond_b
    new-array v4, v3, [Ljava/lang/Object;

    .line 248
    .line 249
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇8o()Ljava/lang/String;

    .line 250
    .line 251
    .line 252
    move-result-object v5

    .line 253
    aput-object v5, v4, v2

    .line 254
    .line 255
    const v5, 0x7f130ce9

    .line 256
    .line 257
    .line 258
    invoke-virtual {p0, v5, v4}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 259
    .line 260
    .line 261
    move-result-object v4

    .line 262
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 263
    .line 264
    .line 265
    :goto_4
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 266
    .line 267
    .line 268
    move-result-object v0

    .line 269
    if-eqz v0, :cond_c

    .line 270
    .line 271
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->〇08〇o0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 272
    .line 273
    goto :goto_5

    .line 274
    :cond_c
    move-object v0, v1

    .line 275
    :goto_5
    if-nez v0, :cond_d

    .line 276
    .line 277
    goto :goto_7

    .line 278
    :cond_d
    iget-object v4, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇080OO8〇0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecall;

    .line 279
    .line 280
    if-eqz v4, :cond_e

    .line 281
    .line 282
    iget v4, v4, Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecall;->discount_amount:I

    .line 283
    .line 284
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 285
    .line 286
    .line 287
    move-result-object v4

    .line 288
    goto :goto_6

    .line 289
    :cond_e
    move-object v4, v1

    .line 290
    :goto_6
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 291
    .line 292
    .line 293
    move-result-object v4

    .line 294
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 295
    .line 296
    .line 297
    :goto_7
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 298
    .line 299
    .line 300
    move-result-object v0

    .line 301
    if-eqz v0, :cond_f

    .line 302
    .line 303
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 304
    .line 305
    if-eqz v0, :cond_f

    .line 306
    .line 307
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->〇O〇〇O8:Landroidx/appcompat/widget/AppCompatTextView;

    .line 308
    .line 309
    goto :goto_8

    .line 310
    :cond_f
    move-object v0, v1

    .line 311
    :goto_8
    if-nez v0, :cond_10

    .line 312
    .line 313
    goto :goto_a

    .line 314
    :cond_10
    iget-object v4, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇080OO8〇0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecall;

    .line 315
    .line 316
    if-eqz v4, :cond_11

    .line 317
    .line 318
    iget v4, v4, Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecall;->coupon_price:I

    .line 319
    .line 320
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 321
    .line 322
    .line 323
    move-result-object v4

    .line 324
    goto :goto_9

    .line 325
    :cond_11
    move-object v4, v1

    .line 326
    :goto_9
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    .line 327
    .line 328
    .line 329
    move-result-object v4

    .line 330
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 331
    .line 332
    .line 333
    :goto_a
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 334
    .line 335
    .line 336
    move-result-object v0

    .line 337
    if-eqz v0, :cond_12

    .line 338
    .line 339
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 340
    .line 341
    if-eqz v0, :cond_12

    .line 342
    .line 343
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->ooo0〇〇O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 344
    .line 345
    goto :goto_b

    .line 346
    :cond_12
    move-object v0, v1

    .line 347
    :goto_b
    if-nez v0, :cond_13

    .line 348
    .line 349
    goto :goto_d

    .line 350
    :cond_13
    iget-object v4, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇080OO8〇0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecall;

    .line 351
    .line 352
    if-eqz v4, :cond_14

    .line 353
    .line 354
    iget-object v4, v4, Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecall;->month_price:Ljava/lang/String;

    .line 355
    .line 356
    goto :goto_c

    .line 357
    :cond_14
    move-object v4, v1

    .line 358
    :goto_c
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 359
    .line 360
    .line 361
    :goto_d
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 362
    .line 363
    .line 364
    move-result-object v0

    .line 365
    if-eqz v0, :cond_15

    .line 366
    .line 367
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 368
    .line 369
    goto :goto_e

    .line 370
    :cond_15
    move-object v0, v1

    .line 371
    :goto_e
    if-nez v0, :cond_16

    .line 372
    .line 373
    goto :goto_10

    .line 374
    :cond_16
    const v4, 0x7f130ce8

    .line 375
    .line 376
    .line 377
    invoke-virtual {p0, v4}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 378
    .line 379
    .line 380
    move-result-object v4

    .line 381
    iget-object v5, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇080OO8〇0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecall;

    .line 382
    .line 383
    if-eqz v5, :cond_17

    .line 384
    .line 385
    iget v5, v5, Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecall;->discount_amount:I

    .line 386
    .line 387
    goto :goto_f

    .line 388
    :cond_17
    const/16 v5, -0x64

    .line 389
    .line 390
    :goto_f
    new-instance v6, Ljava/lang/StringBuilder;

    .line 391
    .line 392
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 393
    .line 394
    .line 395
    const-string v7, "* "

    .line 396
    .line 397
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 398
    .line 399
    .line 400
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 401
    .line 402
    .line 403
    const-string v4, " -"

    .line 404
    .line 405
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 406
    .line 407
    .line 408
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 409
    .line 410
    .line 411
    const-string/jumbo v4, "\u5143"

    .line 412
    .line 413
    .line 414
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 415
    .line 416
    .line 417
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 418
    .line 419
    .line 420
    move-result-object v4

    .line 421
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 422
    .line 423
    .line 424
    :goto_10
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 425
    .line 426
    .line 427
    move-result-object v0

    .line 428
    if-eqz v0, :cond_18

    .line 429
    .line 430
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->OO〇00〇8oO:Lcom/intsig/view/countdown/CountdownView;

    .line 431
    .line 432
    if-eqz v0, :cond_18

    .line 433
    .line 434
    new-instance v4, Lcom/intsig/camscanner/purchase/dialog/〇80〇808〇O;

    .line 435
    .line 436
    invoke-direct {v4, p0}, Lcom/intsig/camscanner/purchase/dialog/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;)V

    .line 437
    .line 438
    .line 439
    invoke-virtual {v0, v4}, Lcom/intsig/view/countdown/CountdownView;->setOnCountdownEndListener(Lcom/intsig/view/countdown/CountdownView$OnCountdownEndListener;)V

    .line 440
    .line 441
    .line 442
    :cond_18
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇080OO8〇0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecall;

    .line 443
    .line 444
    if-eqz v0, :cond_19

    .line 445
    .line 446
    iget v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecall;->isCompliant:I

    .line 447
    .line 448
    if-ne v0, v3, :cond_19

    .line 449
    .line 450
    const/4 v0, 0x1

    .line 451
    goto :goto_11

    .line 452
    :cond_19
    const/4 v0, 0x0

    .line 453
    :goto_11
    if-eqz v0, :cond_1f

    .line 454
    .line 455
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇0oO〇oo00()Z

    .line 456
    .line 457
    .line 458
    move-result v0

    .line 459
    const-string v4, "#B7781B"

    .line 460
    .line 461
    if-eqz v0, :cond_1c

    .line 462
    .line 463
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 464
    .line 465
    .line 466
    move-result-object v0

    .line 467
    if-eqz v0, :cond_1a

    .line 468
    .line 469
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->〇8〇oO〇〇8o:Landroid/widget/LinearLayout;

    .line 470
    .line 471
    if-eqz v0, :cond_1a

    .line 472
    .line 473
    invoke-static {v0, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 474
    .line 475
    .line 476
    :cond_1a
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 477
    .line 478
    .line 479
    move-result-object v0

    .line 480
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 481
    .line 482
    .line 483
    move-result-object v3

    .line 484
    if-eqz v3, :cond_1b

    .line 485
    .line 486
    iget-object v1, v3, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->oOO〇〇:Landroidx/appcompat/widget/AppCompatTextView;

    .line 487
    .line 488
    :cond_1b
    invoke-static {v0, v1, v4}, Lcom/intsig/camscanner/util/StringUtil;->〇〇888(Landroid/content/Context;Landroid/widget/TextView;Ljava/lang/String;)V

    .line 489
    .line 490
    .line 491
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->O8〇8〇O80(Z)V

    .line 492
    .line 493
    .line 494
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 495
    .line 496
    .line 497
    move-result-object v0

    .line 498
    if-eqz v0, :cond_1f

    .line 499
    .line 500
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->oOo0:Landroid/widget/CheckBox;

    .line 501
    .line 502
    if-eqz v0, :cond_1f

    .line 503
    .line 504
    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 505
    .line 506
    .line 507
    goto :goto_12

    .line 508
    :cond_1c
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 509
    .line 510
    .line 511
    move-result-object v0

    .line 512
    if-eqz v0, :cond_1d

    .line 513
    .line 514
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->o8〇OO0〇0o:Landroid/widget/LinearLayout;

    .line 515
    .line 516
    if-eqz v0, :cond_1d

    .line 517
    .line 518
    invoke-static {v0, v3}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 519
    .line 520
    .line 521
    :cond_1d
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 522
    .line 523
    .line 524
    move-result-object v0

    .line 525
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 526
    .line 527
    .line 528
    move-result-object v3

    .line 529
    if-eqz v3, :cond_1e

    .line 530
    .line 531
    iget-object v1, v3, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->O88O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 532
    .line 533
    :cond_1e
    invoke-static {v0, v1, v4}, Lcom/intsig/camscanner/util/StringUtil;->〇〇888(Landroid/content/Context;Landroid/widget/TextView;Ljava/lang/String;)V

    .line 534
    .line 535
    .line 536
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->O8〇8〇O80(Z)V

    .line 537
    .line 538
    .line 539
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 540
    .line 541
    .line 542
    move-result-object v0

    .line 543
    if-eqz v0, :cond_1f

    .line 544
    .line 545
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->oOo〇8o008:Landroid/widget/CheckBox;

    .line 546
    .line 547
    if-eqz v0, :cond_1f

    .line 548
    .line 549
    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 550
    .line 551
    .line 552
    :cond_1f
    :goto_12
    return-void
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
.end method

.method private final 〇〇O80〇0o()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->o〇00O:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    iget-boolean v0, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇0O:Z

    .line 7
    .line 8
    if-eqz v0, :cond_1

    .line 9
    .line 10
    return v1

    .line 11
    :cond_0
    iget-boolean v0, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->oOo〇8o008:Z

    .line 12
    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    return v1

    .line 16
    :cond_1
    const/4 v0, 0x0

    .line 17
    return v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static final synthetic 〇〇o0〇8(Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;)Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecall;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇080OO8〇0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecall;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private static final 〇〇〇0(Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;Lcom/intsig/view/countdown/CountdownView;)V
    .locals 3

    .line 1
    const-string/jumbo p1, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    const/4 v0, 0x0

    .line 12
    if-eqz p1, :cond_0

    .line 13
    .line 14
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->o8o:Landroid/widget/TextView;

    .line 15
    .line 16
    if-eqz p1, :cond_0

    .line 17
    .line 18
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 19
    .line 20
    .line 21
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    if-eqz p1, :cond_1

    .line 26
    .line 27
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->o8o:Landroid/widget/TextView;

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_1
    move-object p1, v0

    .line 31
    :goto_0
    const v1, 0x7f130c7f

    .line 32
    .line 33
    .line 34
    if-nez p1, :cond_2

    .line 35
    .line 36
    goto :goto_1

    .line 37
    :cond_2
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v2

    .line 41
    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 42
    .line 43
    .line 44
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    if-eqz p1, :cond_3

    .line 49
    .line 50
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->oo8ooo8O:Landroid/widget/TextView;

    .line 51
    .line 52
    goto :goto_2

    .line 53
    :cond_3
    move-object p1, v0

    .line 54
    :goto_2
    if-nez p1, :cond_4

    .line 55
    .line 56
    goto :goto_3

    .line 57
    :cond_4
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    .line 63
    .line 64
    :goto_3
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 65
    .line 66
    .line 67
    move-result-object p1

    .line 68
    if-eqz p1, :cond_5

    .line 69
    .line 70
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->〇〇08O:Landroid/widget/RelativeLayout;

    .line 71
    .line 72
    if-eqz p1, :cond_5

    .line 73
    .line 74
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    .line 76
    .line 77
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 78
    .line 79
    .line 80
    move-result-object p1

    .line 81
    const-string v0, "#EBEBEB"

    .line 82
    .line 83
    if-eqz p1, :cond_6

    .line 84
    .line 85
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->o8o:Landroid/widget/TextView;

    .line 86
    .line 87
    if-eqz p1, :cond_6

    .line 88
    .line 89
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 90
    .line 91
    .line 92
    move-result v1

    .line 93
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 94
    .line 95
    .line 96
    :cond_6
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 97
    .line 98
    .line 99
    move-result-object p0

    .line 100
    if-eqz p0, :cond_7

    .line 101
    .line 102
    iget-object p0, p0, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->oo8ooo8O:Landroid/widget/TextView;

    .line 103
    .line 104
    if-eqz p0, :cond_7

    .line 105
    .line 106
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 107
    .line 108
    .line 109
    move-result p1

    .line 110
    invoke-virtual {p0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 111
    .line 112
    .line 113
    :cond_7
    sget-object p0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 114
    .line 115
    invoke-static {p0}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇OO0O(Ljava/lang/Boolean;)V

    .line 116
    .line 117
    .line 118
    return-void
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

.method private static final 〇〇〇00(Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;Landroid/view/View;)V
    .locals 2

    .line 1
    const-string/jumbo p1, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/app/BaseDialogFragment;->dismiss()V

    .line 8
    .line 9
    .line 10
    const/4 p0, 0x2

    .line 11
    new-array p0, p0, [Landroid/util/Pair;

    .line 12
    .line 13
    new-instance p1, Landroid/util/Pair;

    .line 14
    .line 15
    const-string v0, "from_part"

    .line 16
    .line 17
    const-string v1, "cs_resubscribe"

    .line 18
    .line 19
    invoke-direct {p1, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 20
    .line 21
    .line 22
    const/4 v0, 0x0

    .line 23
    aput-object p1, p0, v0

    .line 24
    .line 25
    new-instance p1, Landroid/util/Pair;

    .line 26
    .line 27
    sget-object v0, Lcom/intsig/camscanner/purchase/entity/Function;->MARKETING:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 28
    .line 29
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/entity/Function;->toTrackerValue()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    const-string v1, "from"

    .line 34
    .line 35
    invoke-direct {p1, v1, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 36
    .line 37
    .line 38
    const/4 v0, 0x1

    .line 39
    aput-object p1, p0, v0

    .line 40
    .line 41
    const-string p1, "CSPremiumPop"

    .line 42
    .line 43
    const-string v0, "cancel"

    .line 44
    .line 45
    invoke-static {p1, v0, p0}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 46
    .line 47
    .line 48
    return-void
    .line 49
.end method


# virtual methods
.method public final O0O0〇(I)V
    .locals 14

    .line 1
    const-string v0, "* "

    .line 2
    .line 3
    const v1, 0x7f130ce8

    .line 4
    .line 5
    .line 6
    const/4 v2, 0x0

    .line 7
    const-string v3, "#FFFFFF"

    .line 8
    .line 9
    const v4, 0x7f080f4d

    .line 10
    .line 11
    .line 12
    const v5, 0x7f080f4c

    .line 13
    .line 14
    .line 15
    const v6, 0x7f080f4e

    .line 16
    .line 17
    .line 18
    const v7, 0x7f080f4f

    .line 19
    .line 20
    .line 21
    const/4 v8, 0x1

    .line 22
    const v9, 0x7f0810f9

    .line 23
    .line 24
    .line 25
    const v10, 0x7f0810f7

    .line 26
    .line 27
    .line 28
    const/4 v11, 0x0

    .line 29
    const-string v12, "#9C9C9C"

    .line 30
    .line 31
    const-string v13, "#BB6E00"

    .line 32
    .line 33
    if-nez p1, :cond_12

    .line 34
    .line 35
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    if-eqz p1, :cond_0

    .line 40
    .line 41
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 42
    .line 43
    if-eqz p1, :cond_0

    .line 44
    .line 45
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->〇8〇oO〇〇8o:Landroid/widget/RelativeLayout;

    .line 46
    .line 47
    if-eqz p1, :cond_0

    .line 48
    .line 49
    invoke-virtual {p1, v10}, Landroid/view/View;->setBackgroundResource(I)V

    .line 50
    .line 51
    .line 52
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 53
    .line 54
    .line 55
    move-result-object p1

    .line 56
    if-eqz p1, :cond_1

    .line 57
    .line 58
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 59
    .line 60
    if-eqz p1, :cond_1

    .line 61
    .line 62
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->o8〇OO0〇0o:Landroid/widget/RelativeLayout;

    .line 63
    .line 64
    if-eqz p1, :cond_1

    .line 65
    .line 66
    invoke-virtual {p1, v9}, Landroid/view/View;->setBackgroundResource(I)V

    .line 67
    .line 68
    .line 69
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 70
    .line 71
    .line 72
    move-result-object p1

    .line 73
    if-eqz p1, :cond_2

    .line 74
    .line 75
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 76
    .line 77
    if-eqz p1, :cond_2

    .line 78
    .line 79
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->〇O〇〇O8:Landroidx/appcompat/widget/AppCompatTextView;

    .line 80
    .line 81
    if-eqz p1, :cond_2

    .line 82
    .line 83
    invoke-static {v13}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 84
    .line 85
    .line 86
    move-result v9

    .line 87
    invoke-virtual {p1, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 88
    .line 89
    .line 90
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 91
    .line 92
    .line 93
    move-result-object p1

    .line 94
    if-eqz p1, :cond_3

    .line 95
    .line 96
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 97
    .line 98
    if-eqz p1, :cond_3

    .line 99
    .line 100
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->O0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 101
    .line 102
    if-eqz p1, :cond_3

    .line 103
    .line 104
    invoke-static {v12}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 105
    .line 106
    .line 107
    move-result v9

    .line 108
    invoke-virtual {p1, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 109
    .line 110
    .line 111
    :cond_3
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 112
    .line 113
    .line 114
    move-result-object p1

    .line 115
    if-eqz p1, :cond_4

    .line 116
    .line 117
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 118
    .line 119
    if-eqz p1, :cond_4

    .line 120
    .line 121
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->〇o0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 122
    .line 123
    if-eqz p1, :cond_4

    .line 124
    .line 125
    invoke-static {v13}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 126
    .line 127
    .line 128
    move-result v9

    .line 129
    invoke-virtual {p1, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 130
    .line 131
    .line 132
    :cond_4
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 133
    .line 134
    .line 135
    move-result-object p1

    .line 136
    if-eqz p1, :cond_5

    .line 137
    .line 138
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 139
    .line 140
    if-eqz p1, :cond_5

    .line 141
    .line 142
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->o8oOOo:Landroidx/appcompat/widget/AppCompatTextView;

    .line 143
    .line 144
    if-eqz p1, :cond_5

    .line 145
    .line 146
    invoke-static {v12}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 147
    .line 148
    .line 149
    move-result v9

    .line 150
    invoke-virtual {p1, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 151
    .line 152
    .line 153
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 154
    .line 155
    .line 156
    move-result-object p1

    .line 157
    if-eqz p1, :cond_6

    .line 158
    .line 159
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 160
    .line 161
    if-eqz p1, :cond_6

    .line 162
    .line 163
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 164
    .line 165
    if-eqz p1, :cond_6

    .line 166
    .line 167
    invoke-static {p1, v8}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 168
    .line 169
    .line 170
    :cond_6
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 171
    .line 172
    .line 173
    move-result-object p1

    .line 174
    if-eqz p1, :cond_7

    .line 175
    .line 176
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 177
    .line 178
    if-eqz p1, :cond_7

    .line 179
    .line 180
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 181
    .line 182
    if-eqz p1, :cond_7

    .line 183
    .line 184
    invoke-static {p1, v11}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 185
    .line 186
    .line 187
    :cond_7
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 188
    .line 189
    .line 190
    move-result-object p1

    .line 191
    if-eqz p1, :cond_8

    .line 192
    .line 193
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 194
    .line 195
    if-eqz p1, :cond_8

    .line 196
    .line 197
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 198
    .line 199
    if-eqz p1, :cond_8

    .line 200
    .line 201
    invoke-virtual {p1, v7}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 202
    .line 203
    .line 204
    :cond_8
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 205
    .line 206
    .line 207
    move-result-object p1

    .line 208
    if-eqz p1, :cond_9

    .line 209
    .line 210
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 211
    .line 212
    if-eqz p1, :cond_9

    .line 213
    .line 214
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 215
    .line 216
    if-eqz p1, :cond_9

    .line 217
    .line 218
    invoke-virtual {p1, v6}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 219
    .line 220
    .line 221
    :cond_9
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 222
    .line 223
    .line 224
    move-result-object p1

    .line 225
    if-eqz p1, :cond_a

    .line 226
    .line 227
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 228
    .line 229
    if-eqz p1, :cond_a

    .line 230
    .line 231
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 232
    .line 233
    if-eqz p1, :cond_a

    .line 234
    .line 235
    invoke-virtual {p1, v5}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 236
    .line 237
    .line 238
    :cond_a
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 239
    .line 240
    .line 241
    move-result-object p1

    .line 242
    if-eqz p1, :cond_b

    .line 243
    .line 244
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 245
    .line 246
    if-eqz p1, :cond_b

    .line 247
    .line 248
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 249
    .line 250
    if-eqz p1, :cond_b

    .line 251
    .line 252
    invoke-virtual {p1, v4}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 253
    .line 254
    .line 255
    :cond_b
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 256
    .line 257
    .line 258
    move-result-object p1

    .line 259
    if-eqz p1, :cond_c

    .line 260
    .line 261
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 262
    .line 263
    if-eqz p1, :cond_c

    .line 264
    .line 265
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->O88O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 266
    .line 267
    if-eqz p1, :cond_c

    .line 268
    .line 269
    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 270
    .line 271
    .line 272
    move-result v3

    .line 273
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 274
    .line 275
    .line 276
    :cond_c
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 277
    .line 278
    .line 279
    move-result-object p1

    .line 280
    if-eqz p1, :cond_d

    .line 281
    .line 282
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 283
    .line 284
    if-eqz p1, :cond_d

    .line 285
    .line 286
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->〇〇08O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 287
    .line 288
    if-eqz p1, :cond_d

    .line 289
    .line 290
    invoke-static {v13}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 291
    .line 292
    .line 293
    move-result v3

    .line 294
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 295
    .line 296
    .line 297
    :cond_d
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 298
    .line 299
    .line 300
    move-result-object p1

    .line 301
    if-eqz p1, :cond_e

    .line 302
    .line 303
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 304
    .line 305
    if-eqz p1, :cond_e

    .line 306
    .line 307
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->ooo0〇〇O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 308
    .line 309
    if-eqz p1, :cond_e

    .line 310
    .line 311
    const-string v3, "#5A5A5A"

    .line 312
    .line 313
    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 314
    .line 315
    .line 316
    move-result v3

    .line 317
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 318
    .line 319
    .line 320
    :cond_e
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 321
    .line 322
    .line 323
    move-result-object p1

    .line 324
    if-eqz p1, :cond_f

    .line 325
    .line 326
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 327
    .line 328
    :cond_f
    if-nez v2, :cond_10

    .line 329
    .line 330
    goto/16 :goto_0

    .line 331
    .line 332
    :cond_10
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 333
    .line 334
    .line 335
    move-result-object p1

    .line 336
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇080OO8〇0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecall;

    .line 337
    .line 338
    if-eqz v1, :cond_11

    .line 339
    .line 340
    iget v11, v1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecall;->discount_amount:I

    .line 341
    .line 342
    :cond_11
    new-instance v1, Ljava/lang/StringBuilder;

    .line 343
    .line 344
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 345
    .line 346
    .line 347
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 348
    .line 349
    .line 350
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 351
    .line 352
    .line 353
    const-string p1, " -"

    .line 354
    .line 355
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 356
    .line 357
    .line 358
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 359
    .line 360
    .line 361
    const-string/jumbo p1, "\u5143"

    .line 362
    .line 363
    .line 364
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 365
    .line 366
    .line 367
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 368
    .line 369
    .line 370
    move-result-object p1

    .line 371
    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 372
    .line 373
    .line 374
    goto/16 :goto_0

    .line 375
    .line 376
    :cond_12
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 377
    .line 378
    .line 379
    move-result-object p1

    .line 380
    if-eqz p1, :cond_13

    .line 381
    .line 382
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 383
    .line 384
    if-eqz p1, :cond_13

    .line 385
    .line 386
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->o8〇OO0〇0o:Landroid/widget/RelativeLayout;

    .line 387
    .line 388
    if-eqz p1, :cond_13

    .line 389
    .line 390
    invoke-virtual {p1, v10}, Landroid/view/View;->setBackgroundResource(I)V

    .line 391
    .line 392
    .line 393
    :cond_13
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 394
    .line 395
    .line 396
    move-result-object p1

    .line 397
    if-eqz p1, :cond_14

    .line 398
    .line 399
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 400
    .line 401
    if-eqz p1, :cond_14

    .line 402
    .line 403
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->〇8〇oO〇〇8o:Landroid/widget/RelativeLayout;

    .line 404
    .line 405
    if-eqz p1, :cond_14

    .line 406
    .line 407
    invoke-virtual {p1, v9}, Landroid/view/View;->setBackgroundResource(I)V

    .line 408
    .line 409
    .line 410
    :cond_14
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 411
    .line 412
    .line 413
    move-result-object p1

    .line 414
    if-eqz p1, :cond_15

    .line 415
    .line 416
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 417
    .line 418
    if-eqz p1, :cond_15

    .line 419
    .line 420
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->O0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 421
    .line 422
    if-eqz p1, :cond_15

    .line 423
    .line 424
    invoke-static {v13}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 425
    .line 426
    .line 427
    move-result v9

    .line 428
    invoke-virtual {p1, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 429
    .line 430
    .line 431
    :cond_15
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 432
    .line 433
    .line 434
    move-result-object p1

    .line 435
    if-eqz p1, :cond_16

    .line 436
    .line 437
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 438
    .line 439
    if-eqz p1, :cond_16

    .line 440
    .line 441
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->〇O〇〇O8:Landroidx/appcompat/widget/AppCompatTextView;

    .line 442
    .line 443
    if-eqz p1, :cond_16

    .line 444
    .line 445
    invoke-static {v12}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 446
    .line 447
    .line 448
    move-result v9

    .line 449
    invoke-virtual {p1, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 450
    .line 451
    .line 452
    :cond_16
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 453
    .line 454
    .line 455
    move-result-object p1

    .line 456
    if-eqz p1, :cond_17

    .line 457
    .line 458
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 459
    .line 460
    if-eqz p1, :cond_17

    .line 461
    .line 462
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->o8oOOo:Landroidx/appcompat/widget/AppCompatTextView;

    .line 463
    .line 464
    if-eqz p1, :cond_17

    .line 465
    .line 466
    invoke-static {v13}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 467
    .line 468
    .line 469
    move-result v9

    .line 470
    invoke-virtual {p1, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 471
    .line 472
    .line 473
    :cond_17
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 474
    .line 475
    .line 476
    move-result-object p1

    .line 477
    if-eqz p1, :cond_18

    .line 478
    .line 479
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 480
    .line 481
    if-eqz p1, :cond_18

    .line 482
    .line 483
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->〇o0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 484
    .line 485
    if-eqz p1, :cond_18

    .line 486
    .line 487
    invoke-static {v12}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 488
    .line 489
    .line 490
    move-result v9

    .line 491
    invoke-virtual {p1, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 492
    .line 493
    .line 494
    :cond_18
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 495
    .line 496
    .line 497
    move-result-object p1

    .line 498
    if-eqz p1, :cond_19

    .line 499
    .line 500
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 501
    .line 502
    if-eqz p1, :cond_19

    .line 503
    .line 504
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 505
    .line 506
    if-eqz p1, :cond_19

    .line 507
    .line 508
    invoke-static {p1, v8}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 509
    .line 510
    .line 511
    :cond_19
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 512
    .line 513
    .line 514
    move-result-object p1

    .line 515
    if-eqz p1, :cond_1a

    .line 516
    .line 517
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 518
    .line 519
    if-eqz p1, :cond_1a

    .line 520
    .line 521
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->OO:Landroidx/appcompat/widget/AppCompatImageView;

    .line 522
    .line 523
    if-eqz p1, :cond_1a

    .line 524
    .line 525
    invoke-static {p1, v11}, Lcom/intsig/camscanner/util/ViewExtKt;->o〇O8〇〇o(Landroid/view/View;Z)V

    .line 526
    .line 527
    .line 528
    :cond_1a
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 529
    .line 530
    .line 531
    move-result-object p1

    .line 532
    if-eqz p1, :cond_1b

    .line 533
    .line 534
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 535
    .line 536
    if-eqz p1, :cond_1b

    .line 537
    .line 538
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->〇08O〇00〇o:Landroidx/appcompat/widget/AppCompatImageView;

    .line 539
    .line 540
    if-eqz p1, :cond_1b

    .line 541
    .line 542
    invoke-virtual {p1, v7}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 543
    .line 544
    .line 545
    :cond_1b
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 546
    .line 547
    .line 548
    move-result-object p1

    .line 549
    if-eqz p1, :cond_1c

    .line 550
    .line 551
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 552
    .line 553
    if-eqz p1, :cond_1c

    .line 554
    .line 555
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->O8o08O8O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 556
    .line 557
    if-eqz p1, :cond_1c

    .line 558
    .line 559
    invoke-virtual {p1, v6}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 560
    .line 561
    .line 562
    :cond_1c
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 563
    .line 564
    .line 565
    move-result-object p1

    .line 566
    if-eqz p1, :cond_1d

    .line 567
    .line 568
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 569
    .line 570
    if-eqz p1, :cond_1d

    .line 571
    .line 572
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatImageView;

    .line 573
    .line 574
    if-eqz p1, :cond_1d

    .line 575
    .line 576
    invoke-virtual {p1, v5}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 577
    .line 578
    .line 579
    :cond_1d
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 580
    .line 581
    .line 582
    move-result-object p1

    .line 583
    if-eqz p1, :cond_1e

    .line 584
    .line 585
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 586
    .line 587
    if-eqz p1, :cond_1e

    .line 588
    .line 589
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->〇080OO8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 590
    .line 591
    if-eqz p1, :cond_1e

    .line 592
    .line 593
    invoke-virtual {p1, v4}, Landroidx/appcompat/widget/AppCompatImageView;->setImageResource(I)V

    .line 594
    .line 595
    .line 596
    :cond_1e
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 597
    .line 598
    .line 599
    move-result-object p1

    .line 600
    if-eqz p1, :cond_1f

    .line 601
    .line 602
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 603
    .line 604
    if-eqz p1, :cond_1f

    .line 605
    .line 606
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->O88O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 607
    .line 608
    if-eqz p1, :cond_1f

    .line 609
    .line 610
    invoke-static {v13}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 611
    .line 612
    .line 613
    move-result v4

    .line 614
    invoke-virtual {p1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 615
    .line 616
    .line 617
    :cond_1f
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 618
    .line 619
    .line 620
    move-result-object p1

    .line 621
    if-eqz p1, :cond_20

    .line 622
    .line 623
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 624
    .line 625
    if-eqz p1, :cond_20

    .line 626
    .line 627
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->〇〇08O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 628
    .line 629
    if-eqz p1, :cond_20

    .line 630
    .line 631
    invoke-static {v3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 632
    .line 633
    .line 634
    move-result v3

    .line 635
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 636
    .line 637
    .line 638
    :cond_20
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 639
    .line 640
    .line 641
    move-result-object p1

    .line 642
    if-eqz p1, :cond_21

    .line 643
    .line 644
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->ooo0〇〇O:Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;

    .line 645
    .line 646
    if-eqz p1, :cond_21

    .line 647
    .line 648
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/LlRecallPriceYsMonthSelectBinding;->ooo0〇〇O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 649
    .line 650
    if-eqz p1, :cond_21

    .line 651
    .line 652
    invoke-static {v12}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 653
    .line 654
    .line 655
    move-result v3

    .line 656
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 657
    .line 658
    .line 659
    :cond_21
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇80o()Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;

    .line 660
    .line 661
    .line 662
    move-result-object p1

    .line 663
    if-eqz p1, :cond_22

    .line 664
    .line 665
    iget-object v2, p1, Lcom/intsig/camscanner/databinding/DialogCnUnsubscribeRecallLayoutBinding;->o〇00O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 666
    .line 667
    :cond_22
    if-nez v2, :cond_23

    .line 668
    .line 669
    goto :goto_0

    .line 670
    :cond_23
    invoke-virtual {p0, v1}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    .line 671
    .line 672
    .line 673
    move-result-object p1

    .line 674
    new-instance v1, Ljava/lang/StringBuilder;

    .line 675
    .line 676
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 677
    .line 678
    .line 679
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 680
    .line 681
    .line 682
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 683
    .line 684
    .line 685
    const-string p1, " -9\u5143"

    .line 686
    .line 687
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 688
    .line 689
    .line 690
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 691
    .line 692
    .line 693
    move-result-object p1

    .line 694
    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 695
    .line 696
    .line 697
    :goto_0
    return-void
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
.end method

.method public dealClickAction(Landroid/view/View;)V
    .locals 5

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/app/BaseDialogFragment;->dealClickAction(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    if-eqz p1, :cond_0

    .line 5
    .line 6
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const/4 p1, 0x0

    .line 16
    :goto_0
    const-string v0, "CNUnsubscribeRecallDialog"

    .line 17
    .line 18
    const/4 v1, 0x0

    .line 19
    if-nez p1, :cond_1

    .line 20
    .line 21
    goto :goto_1

    .line 22
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 23
    .line 24
    .line 25
    move-result v2

    .line 26
    const v3, 0x7f0a1001

    .line 27
    .line 28
    .line 29
    if-ne v2, v3, :cond_2

    .line 30
    .line 31
    const-string p1, "click year"

    .line 32
    .line 33
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    iput v1, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->o〇00O:I

    .line 37
    .line 38
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->O0O0〇(I)V

    .line 39
    .line 40
    .line 41
    iget-boolean p1, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇0O:Z

    .line 42
    .line 43
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->O8〇8〇O80(Z)V

    .line 44
    .line 45
    .line 46
    goto :goto_7

    .line 47
    :cond_2
    :goto_1
    const/4 v2, 0x1

    .line 48
    if-nez p1, :cond_3

    .line 49
    .line 50
    goto :goto_2

    .line 51
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 52
    .line 53
    .line 54
    move-result v3

    .line 55
    const v4, 0x7f0a0fc1

    .line 56
    .line 57
    .line 58
    if-ne v3, v4, :cond_4

    .line 59
    .line 60
    const-string p1, "click month"

    .line 61
    .line 62
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    iput v2, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->o〇00O:I

    .line 66
    .line 67
    invoke-virtual {p0, v2}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->O0O0〇(I)V

    .line 68
    .line 69
    .line 70
    iget-boolean p1, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->oOo〇8o008:Z

    .line 71
    .line 72
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->O8〇8〇O80(Z)V

    .line 73
    .line 74
    .line 75
    goto :goto_7

    .line 76
    :cond_4
    :goto_2
    if-nez p1, :cond_5

    .line 77
    .line 78
    goto :goto_4

    .line 79
    :cond_5
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 80
    .line 81
    .line 82
    move-result v0

    .line 83
    const v3, 0x7f0a16ea

    .line 84
    .line 85
    .line 86
    if-ne v0, v3, :cond_6

    .line 87
    .line 88
    :goto_3
    const/4 p1, 0x1

    .line 89
    goto :goto_6

    .line 90
    :cond_6
    :goto_4
    if-nez p1, :cond_7

    .line 91
    .line 92
    goto :goto_5

    .line 93
    :cond_7
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 94
    .line 95
    .line 96
    move-result p1

    .line 97
    const v0, 0x7f0a0fda

    .line 98
    .line 99
    .line 100
    if-ne p1, v0, :cond_8

    .line 101
    .line 102
    goto :goto_3

    .line 103
    :cond_8
    :goto_5
    const/4 p1, 0x0

    .line 104
    :goto_6
    if-eqz p1, :cond_b

    .line 105
    .line 106
    iget-object p1, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇080OO8〇0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecall;

    .line 107
    .line 108
    if-eqz p1, :cond_9

    .line 109
    .line 110
    iget p1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecall;->isCompliant:I

    .line 111
    .line 112
    if-ne p1, v2, :cond_9

    .line 113
    .line 114
    const/4 v1, 0x1

    .line 115
    :cond_9
    if-eqz v1, :cond_a

    .line 116
    .line 117
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇〇O80〇0o()Z

    .line 118
    .line 119
    .line 120
    move-result p1

    .line 121
    if-nez p1, :cond_a

    .line 122
    .line 123
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 124
    .line 125
    .line 126
    move-result-object p1

    .line 127
    const-string/jumbo v0, "\u8bf7\u5148\u9605\u8bfb\u5e76\u52fe\u9009\u534f\u8bae"

    .line 128
    .line 129
    .line 130
    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->o〇0(Landroid/content/Context;Ljava/lang/String;)V

    .line 131
    .line 132
    .line 133
    goto :goto_7

    .line 134
    :cond_a
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇o〇88〇8()V

    .line 135
    .line 136
    .line 137
    :cond_b
    :goto_7
    return-void
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method protected init(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    const-string p1, "CNUnsubscribeRecallDialog"

    .line 2
    .line 3
    const-string v0, "init>>>"

    .line 4
    .line 5
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const/4 p1, 0x0

    .line 9
    invoke-virtual {p0, p1}, Landroidx/fragment/app/DialogFragment;->setShowsDialog(Z)V

    .line 10
    .line 11
    .line 12
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 13
    .line 14
    .line 15
    move-result-wide v0

    .line 16
    iput-wide v0, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->O8o08O8O:J

    .line 17
    .line 18
    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    .line 19
    .line 20
    invoke-static {p1}, Lcom/intsig/camscanner/util/PreferenceHelper;->OOOoooooO(Ljava/lang/Boolean;)V

    .line 21
    .line 22
    .line 23
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇8oOO88()Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecall;

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇080OO8〇0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecall;

    .line 28
    .line 29
    invoke-virtual {p0}, Lcom/intsig/app/BaseDialogFragment;->〇O8oOo0()V

    .line 30
    .line 31
    .line 32
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->Ooo8o()V

    .line 33
    .line 34
    .line 35
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇o08()V

    .line 36
    .line 37
    .line 38
    return-void
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public final oOoO8OO〇()Lcom/intsig/camscanner/Client/ProgressDialogClient;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->o8〇OO0〇0o:Lcom/intsig/camscanner/Client/ProgressDialogClient;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 1
    if-eqz p2, :cond_1

    .line 2
    .line 3
    iget p1, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->o〇00O:I

    .line 4
    .line 5
    const/4 p2, 0x1

    .line 6
    if-nez p1, :cond_0

    .line 7
    .line 8
    iput-boolean p2, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇0O:Z

    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    iput-boolean p2, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->oOo〇8o008:Z

    .line 12
    .line 13
    :goto_0
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->O8〇8〇O80(Z)V

    .line 14
    .line 15
    .line 16
    goto :goto_2

    .line 17
    :cond_1
    iget p1, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->o〇00O:I

    .line 18
    .line 19
    const/4 p2, 0x0

    .line 20
    if-nez p1, :cond_2

    .line 21
    .line 22
    iput-boolean p2, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇0O:Z

    .line 23
    .line 24
    goto :goto_1

    .line 25
    :cond_2
    iput-boolean p2, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->oOo〇8o008:Z

    .line 26
    .line 27
    :goto_1
    invoke-direct {p0, p2}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->O8〇8〇O80(Z)V

    .line 28
    .line 29
    .line 30
    :goto_2
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public onDestroyView()V
    .locals 0

    .line 1
    invoke-super {p0}, Lcom/intsig/app/BaseDialogFragment;->onDestroyView()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇Oo〇O()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public onStart()V
    .locals 8

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/DialogFragment;->onStart()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->CSPremiumPop:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 5
    .line 6
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->toTrackerValue()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    const-string v2, "from_part"

    .line 11
    .line 12
    const-string v3, "cs_resubscribe"

    .line 13
    .line 14
    const-string v4, "from"

    .line 15
    .line 16
    sget-object v0, Lcom/intsig/camscanner/purchase/entity/Function;->MARKETING:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 17
    .line 18
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/entity/Function;->toTrackerValue()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v5

    .line 22
    const-string v6, "schema"

    .line 23
    .line 24
    sget-object v0, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->MAIN_NORMAL:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 25
    .line 26
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->toTrackerValue()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v7

    .line 30
    invoke-static/range {v1 .. v7}, Lcom/intsig/camscanner/log/LogAgentData;->〇O〇(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public final o〇O8OO(I)I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->OO〇00〇8oO:[Ljava/lang/Integer;

    .line 2
    .line 3
    aget-object p1, v0, p1

    .line 4
    .line 5
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 6
    .line 7
    .line 8
    move-result p1

    .line 9
    return p1
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final o〇oo(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇oO〇〇8o:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d0198

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final 〇0ooOOo()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    const v1, 0x7f1301fd

    .line 7
    .line 8
    .line 9
    invoke-static {v1}, Lcom/intsig/util/AppStringUtils;->〇080(I)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 14
    .line 15
    .line 16
    const v1, 0x7f130afd

    .line 17
    .line 18
    .line 19
    invoke-static {v1}, Lcom/intsig/util/AppStringUtils;->〇080(I)Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v1

    .line 23
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 24
    .line 25
    .line 26
    const v1, 0x7f130c68

    .line 27
    .line 28
    .line 29
    invoke-static {v1}, Lcom/intsig/util/AppStringUtils;->〇080(I)Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    .line 35
    .line 36
    const v1, 0x7f130201

    .line 37
    .line 38
    .line 39
    invoke-static {v1}, Lcom/intsig/util/AppStringUtils;->〇080(I)Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    .line 45
    .line 46
    return-object v0
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public final 〇0〇0()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇8〇oO〇〇8o:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final 〇o〇88〇8()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->o8〇OO0〇0o:Lcom/intsig/camscanner/Client/ProgressDialogClient;

    .line 2
    .line 3
    if-nez v0, :cond_1

    .line 4
    .line 5
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    if-eqz v1, :cond_0

    .line 14
    .line 15
    const v2, 0x7f130e22

    .line 16
    .line 17
    .line 18
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    const/4 v1, 0x0

    .line 24
    :goto_0
    invoke-static {v0, v1}, Lcom/intsig/camscanner/Client/ProgressDialogClient;->〇o00〇〇Oo(Landroid/app/Activity;Ljava/lang/String;)Lcom/intsig/camscanner/Client/ProgressDialogClient;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->o8〇OO0〇0o:Lcom/intsig/camscanner/Client/ProgressDialogClient;

    .line 29
    .line 30
    :cond_1
    const-string v0, "CNUnsubscribeRecallDialog"

    .line 31
    .line 32
    const-string v1, "checkCouponState isGetCoupon"

    .line 33
    .line 34
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/CNUnsubscribeRecallDialog;->〇O0o〇〇o()V

    .line 38
    .line 39
    .line 40
    return-void
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

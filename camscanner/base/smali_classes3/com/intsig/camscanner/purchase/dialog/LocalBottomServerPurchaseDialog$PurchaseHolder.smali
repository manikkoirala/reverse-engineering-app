.class public final Lcom/intsig/camscanner/purchase/dialog/LocalBottomServerPurchaseDialog$PurchaseHolder;
.super Lcom/intsig/adapter/BaseViewHolder;
.source "LocalBottomServerPurchaseDialog.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/purchase/dialog/LocalBottomServerPurchaseDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PurchaseHolder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/intsig/adapter/BaseViewHolder<",
        "Lcom/intsig/comm/purchase/entity/PayItem;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final OO:Landroid/widget/ImageView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇00O:Landroid/widget/RadioButton;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08O〇00〇o:Landroid/widget/TextView;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "itemView"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1}, Lcom/intsig/adapter/BaseViewHolder;-><init>(Landroid/view/View;)V

    .line 7
    .line 8
    .line 9
    const v0, 0x7f0a0931

    .line 10
    .line 11
    .line 12
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    const-string v1, "itemView.findViewById(R.id.iv_icon)"

    .line 17
    .line 18
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    check-cast v0, Landroid/widget/ImageView;

    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/dialog/LocalBottomServerPurchaseDialog$PurchaseHolder;->OO:Landroid/widget/ImageView;

    .line 24
    .line 25
    const v0, 0x7f0a187b

    .line 26
    .line 27
    .line 28
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 29
    .line 30
    .line 31
    move-result-object v0

    .line 32
    const-string v1, "itemView.findViewById(R.id.tv_title)"

    .line 33
    .line 34
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 35
    .line 36
    .line 37
    check-cast v0, Landroid/widget/TextView;

    .line 38
    .line 39
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/dialog/LocalBottomServerPurchaseDialog$PurchaseHolder;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 40
    .line 41
    const v0, 0x7f0a0ed0

    .line 42
    .line 43
    .line 44
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    const-string v0, "itemView.findViewById(R.id.radio_btn)"

    .line 49
    .line 50
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 51
    .line 52
    .line 53
    check-cast p1, Landroid/widget/RadioButton;

    .line 54
    .line 55
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/dialog/LocalBottomServerPurchaseDialog$PurchaseHolder;->o〇00O:Landroid/widget/RadioButton;

    .line 56
    .line 57
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method


# virtual methods
.method public bridge synthetic O〇8O8〇008(Ljava/lang/Object;I)V
    .locals 0

    .line 1
    check-cast p1, Lcom/intsig/comm/purchase/entity/PayItem;

    .line 2
    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/purchase/dialog/LocalBottomServerPurchaseDialog$PurchaseHolder;->〇0000OOO(Lcom/intsig/comm/purchase/entity/PayItem;I)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public 〇0000OOO(Lcom/intsig/comm/purchase/entity/PayItem;I)V
    .locals 1
    .param p1    # Lcom/intsig/comm/purchase/entity/PayItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string p2, "data"

    .line 2
    .line 3
    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-object p2, p0, Lcom/intsig/camscanner/purchase/dialog/LocalBottomServerPurchaseDialog$PurchaseHolder;->OO:Landroid/widget/ImageView;

    .line 7
    .line 8
    invoke-virtual {p1}, Lcom/intsig/comm/purchase/entity/PayItem;->O8()I

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 13
    .line 14
    .line 15
    iget-object p2, p0, Lcom/intsig/camscanner/purchase/dialog/LocalBottomServerPurchaseDialog$PurchaseHolder;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 16
    .line 17
    invoke-virtual {p1}, Lcom/intsig/comm/purchase/entity/PayItem;->〇o00〇〇Oo()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 22
    .line 23
    .line 24
    iget-object p2, p0, Lcom/intsig/camscanner/purchase/dialog/LocalBottomServerPurchaseDialog$PurchaseHolder;->o〇00O:Landroid/widget/RadioButton;

    .line 25
    .line 26
    invoke-virtual {p1}, Lcom/intsig/comm/purchase/entity/PayItem;->oO80()Z

    .line 27
    .line 28
    .line 29
    move-result p1

    .line 30
    invoke-virtual {p2, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

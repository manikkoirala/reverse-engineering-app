.class public final Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;
.super Lcom/intsig/app/BaseDialogFragment;
.source "GpDropCnlDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$Companion;,
        Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$UserAndComment;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field static final synthetic ooo0〇〇O:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public static final 〇8〇oO〇〇8o:Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇〇08O:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$UserAndComment;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

.field private OO〇00〇8oO:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;

.field private final o8〇OO0〇0o:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOo0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;

.field private oOo〇8o008:Z

.field private o〇00O:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

.field private 〇080OO8〇0:I

.field private final 〇08O〇00〇o:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0O:Z


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v1, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v2, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v3, "mBinding"

    .line 7
    .line 8
    const-string v4, "getMBinding()Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;"

    .line 9
    .line 10
    const-class v5, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;

    .line 11
    .line 12
    const/4 v6, 0x0

    .line 13
    invoke-direct {v2, v5, v3, v4, v6}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v2

    .line 20
    aput-object v2, v1, v6

    .line 21
    .line 22
    sput-object v1, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->ooo0〇〇O:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v1, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$Companion;

    .line 25
    .line 26
    const/4 v2, 0x0

    .line 27
    invoke-direct {v1, v2}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v1, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$Companion;

    .line 31
    .line 32
    const/4 v1, 0x3

    .line 33
    new-array v1, v1, [Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$UserAndComment;

    .line 34
    .line 35
    new-instance v2, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$UserAndComment;

    .line 36
    .line 37
    const v3, 0x7f131e18

    .line 38
    .line 39
    .line 40
    invoke-static {v3}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 41
    .line 42
    .line 43
    move-result-object v3

    .line 44
    const-string v4, "Tina_1986"

    .line 45
    .line 46
    invoke-direct {v2, v4, v3}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$UserAndComment;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    aput-object v2, v1, v6

    .line 50
    .line 51
    new-instance v2, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$UserAndComment;

    .line 52
    .line 53
    const v3, 0x7f131e19

    .line 54
    .line 55
    .line 56
    invoke-static {v3}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v3

    .line 60
    const-string v4, "Joe3325"

    .line 61
    .line 62
    invoke-direct {v2, v4, v3}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$UserAndComment;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    aput-object v2, v1, v0

    .line 66
    .line 67
    new-instance v0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$UserAndComment;

    .line 68
    .line 69
    const v2, 0x7f131e1a

    .line 70
    .line 71
    .line 72
    invoke-static {v2}, Lcom/intsig/utils/ext/StringExtKt;->O8(I)Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v2

    .line 76
    const-string v3, "M.-Alma"

    .line 77
    .line 78
    invoke-direct {v0, v3, v2}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$UserAndComment;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    const/4 v2, 0x2

    .line 82
    aput-object v0, v1, v2

    .line 83
    .line 84
    invoke-static {v1}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 85
    .line 86
    .line 87
    move-result-object v0

    .line 88
    sput-object v0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇〇08O:Ljava/util/List;

    .line 89
    .line 90
    return-void
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/app/BaseDialogFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v6, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 5
    .line 6
    const-class v1, Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;

    .line 7
    .line 8
    const/4 v3, 0x0

    .line 9
    const/4 v4, 0x4

    .line 10
    const/4 v5, 0x0

    .line 11
    move-object v0, v6

    .line 12
    move-object v2, p0

    .line 13
    invoke-direct/range {v0 .. v5}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;-><init>(Ljava/lang/Class;Landroidx/fragment/app/Fragment;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 14
    .line 15
    .line 16
    iput-object v6, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇08O〇00〇o:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 17
    .line 18
    const/4 v0, 0x1

    .line 19
    iput-boolean v0, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->oOo〇8o008:Z

    .line 20
    .line 21
    new-instance v0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$mHandler$2;

    .line 22
    .line 23
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$mHandler$2;-><init>(Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;)V

    .line 24
    .line 25
    .line 26
    invoke-static {v0}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->o8〇OO0〇0o:Lkotlin/Lazy;

    .line 31
    .line 32
    return-void
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static final synthetic O0〇0()Ljava/util/List;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇〇08O:Ljava/util/List;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private final Ooo8o()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lcom/intsig/utils/DisplayUtil;->OO0o〇〇〇〇0(Landroid/content/Context;)I

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    const/16 v2, 0x28

    .line 14
    .line 15
    invoke-static {v1, v2}, Lcom/intsig/utils/DisplayUtil;->〇o〇(Landroid/content/Context;I)I

    .line 16
    .line 17
    .line 18
    move-result v1

    .line 19
    sub-int/2addr v0, v1

    .line 20
    iput v0, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇080OO8〇0:I

    .line 21
    .line 22
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 27
    .line 28
    .line 29
    move-result-object v0

    .line 30
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->ad_premium_pop:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPop;

    .line 31
    .line 32
    const/4 v1, 0x0

    .line 33
    if-eqz v0, :cond_0

    .line 34
    .line 35
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPop;->price_style_0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceStyle;

    .line 36
    .line 37
    if-eqz v0, :cond_0

    .line 38
    .line 39
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceStyle;->month:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;

    .line 40
    .line 41
    goto :goto_0

    .line 42
    :cond_0
    move-object v0, v1

    .line 43
    :goto_0
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->OO〇00〇8oO:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;

    .line 44
    .line 45
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 46
    .line 47
    .line 48
    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 50
    .line 51
    .line 52
    move-result-object v0

    .line 53
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->ad_premium_pop:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPop;

    .line 54
    .line 55
    if-eqz v0, :cond_1

    .line 56
    .line 57
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPop;->price_style_0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceStyle;

    .line 58
    .line 59
    if-eqz v0, :cond_1

    .line 60
    .line 61
    iget-object v1, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceStyle;->year:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;

    .line 62
    .line 63
    :cond_1
    iput-object v1, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->oOo0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;

    .line 64
    .line 65
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public static synthetic o00〇88〇08(Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇0oO〇oo00(Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public static synthetic o880(Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇〇〇00(Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private final oOoO8OO〇()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇o〇88〇8()Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    new-instance v1, Lcom/intsig/camscanner/purchase/dialog/O〇8O8〇008;

    .line 12
    .line 13
    invoke-direct {v1, p0, v0}, Lcom/intsig/camscanner/purchase/dialog/O〇8O8〇008;-><init>(Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;Landroidx/appcompat/widget/AppCompatImageView;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static final synthetic o〇0〇o(Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;)Landroid/os/Handler;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇8〇80o()Landroid/os/Handler;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final o〇O8OO()Landroidx/viewpager/widget/PagerAdapter;
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$getPagerAdapter$1;

    .line 2
    .line 3
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$getPagerAdapter$1;-><init>(Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;)V

    .line 4
    .line 5
    .line 6
    return-object v0
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private final 〇088O()Z
    .locals 4

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->ad_premium_pop:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPop;

    .line 10
    .line 11
    const/4 v1, 0x0

    .line 12
    const-string v2, "GpDropCnlDialog"

    .line 13
    .line 14
    if-nez v0, :cond_0

    .line 15
    .line 16
    const-string v0, "ad_premium_pop is null"

    .line 17
    .line 18
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    return v1

    .line 22
    :cond_0
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPop;->price_style_0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceStyle;

    .line 23
    .line 24
    if-nez v0, :cond_1

    .line 25
    .line 26
    const-string v0, "price_style_0 is null"

    .line 27
    .line 28
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    return v1

    .line 32
    :cond_1
    iget-object v3, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceStyle;->month:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;

    .line 33
    .line 34
    if-eqz v3, :cond_3

    .line 35
    .line 36
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceStyle;->year:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;

    .line 37
    .line 38
    if-nez v0, :cond_2

    .line 39
    .line 40
    goto :goto_0

    .line 41
    :cond_2
    const/4 v0, 0x1

    .line 42
    return v0

    .line 43
    :cond_3
    :goto_0
    const-string v0, "month or year can not be null"

    .line 44
    .line 45
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    return v1
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final 〇08O(Z)V
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    const/4 v1, 0x0

    .line 3
    if-ne p1, v0, :cond_2

    .line 4
    .line 5
    iget-object p1, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->oOo0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;

    .line 6
    .line 7
    if-eqz p1, :cond_5

    .line 8
    .line 9
    iget-object p1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;->button_title:Ljava/lang/String;

    .line 10
    .line 11
    if-eqz p1, :cond_5

    .line 12
    .line 13
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇o〇88〇8()Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    if-eqz v0, :cond_0

    .line 18
    .line 19
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;->o8〇OO0〇0o:Landroid/widget/TextView;

    .line 20
    .line 21
    :cond_0
    if-nez v1, :cond_1

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_1
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 25
    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_2
    if-nez p1, :cond_5

    .line 29
    .line 30
    iget-object p1, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->OO〇00〇8oO:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;

    .line 31
    .line 32
    if-eqz p1, :cond_5

    .line 33
    .line 34
    iget-object p1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;->button_title:Ljava/lang/String;

    .line 35
    .line 36
    if-eqz p1, :cond_5

    .line 37
    .line 38
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇o〇88〇8()Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    if-eqz v0, :cond_3

    .line 43
    .line 44
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;->o8〇OO0〇0o:Landroid/widget/TextView;

    .line 45
    .line 46
    :cond_3
    if-nez v1, :cond_4

    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_4
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 50
    .line 51
    .line 52
    :cond_5
    :goto_0
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private static final 〇0oO〇oo00(Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    .line 1
    const-string/jumbo p1, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    const-string p1, "event"

    .line 8
    .line 9
    invoke-static {p2, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    .line 13
    .line 14
    .line 15
    move-result p1

    .line 16
    const/4 p2, 0x1

    .line 17
    if-eqz p1, :cond_1

    .line 18
    .line 19
    if-eq p1, p2, :cond_0

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇8〇80o()Landroid/os/Handler;

    .line 23
    .line 24
    .line 25
    move-result-object p1

    .line 26
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇8〇80o()Landroid/os/Handler;

    .line 27
    .line 28
    .line 29
    move-result-object p0

    .line 30
    invoke-virtual {p0, p2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    .line 31
    .line 32
    .line 33
    move-result-object p0

    .line 34
    const-wide/16 v0, 0x1b58

    .line 35
    .line 36
    invoke-virtual {p1, p0, v0, v1}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 37
    .line 38
    .line 39
    goto :goto_0

    .line 40
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇8〇80o()Landroid/os/Handler;

    .line 41
    .line 42
    .line 43
    move-result-object p0

    .line 44
    invoke-virtual {p0, p2}, Landroid/os/Handler;->removeMessages(I)V

    .line 45
    .line 46
    .line 47
    :goto_0
    const/4 p0, 0x0

    .line 48
    return p0
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private static final 〇0ooOOo(Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;Landroidx/appcompat/widget/AppCompatImageView;)V
    .locals 1

    .line 1
    const-string/jumbo v0, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    const-string v0, "$this_run"

    .line 8
    .line 9
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    invoke-static {p0}, Lcom/intsig/camscanner/mainmenu/CsLifecycleUtil;->〇080(Landroidx/lifecycle/LifecycleOwner;)Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eqz v0, :cond_0

    .line 17
    .line 18
    return-void

    .line 19
    :cond_0
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    .line 20
    .line 21
    .line 22
    move-result-object p0

    .line 23
    if-eqz p0, :cond_1

    .line 24
    .line 25
    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    .line 26
    .line 27
    .line 28
    move-result-object p0

    .line 29
    if-eqz p0, :cond_1

    .line 30
    .line 31
    invoke-static {p0, p1}, Lcom/intsig/utils/SystemUiUtil;->O8(Landroid/view/Window;Landroid/view/View;)V

    .line 32
    .line 33
    .line 34
    :cond_1
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final 〇0〇0(Z)Ljava/lang/String;
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->oOo0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;

    .line 5
    .line 6
    if-eqz p1, :cond_1

    .line 7
    .line 8
    goto :goto_0

    .line 9
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->OO〇00〇8oO:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;

    .line 10
    .line 11
    if-eqz p1, :cond_1

    .line 12
    .line 13
    :goto_0
    iget-object v0, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;->product_id:Ljava/lang/String;

    .line 14
    .line 15
    :cond_1
    return-object v0
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final 〇8O0880(Z)V
    .locals 8

    .line 1
    const/16 v0, 0x8

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const/4 v2, 0x2

    .line 5
    const-string v3, ""

    .line 6
    .line 7
    const/4 v4, 0x0

    .line 8
    const/4 v5, 0x1

    .line 9
    if-ne p1, v5, :cond_9

    .line 10
    .line 11
    iget-object p1, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->oOo0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;

    .line 12
    .line 13
    if-eqz p1, :cond_0

    .line 14
    .line 15
    iget-object p1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;->description:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPriceStr;

    .line 16
    .line 17
    if-eqz p1, :cond_0

    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇o〇88〇8()Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;

    .line 20
    .line 21
    .line 22
    move-result-object v6

    .line 23
    if-eqz v6, :cond_0

    .line 24
    .line 25
    iget-object v6, v6, Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;->ooo0〇〇O:Landroid/widget/TextView;

    .line 26
    .line 27
    if-eqz v6, :cond_0

    .line 28
    .line 29
    iget v7, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇080OO8〇0:I

    .line 30
    .line 31
    invoke-static {v6, p1, v7}, Lcom/intsig/camscanner/guide/GuideTextViewUtils;->〇o00〇〇Oo(Landroid/widget/TextView;Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPriceStr;I)V

    .line 32
    .line 33
    .line 34
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    move-object p1, v4

    .line 38
    :goto_0
    if-nez p1, :cond_13

    .line 39
    .line 40
    iget-object p1, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->oOo0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;

    .line 41
    .line 42
    if-eqz p1, :cond_1

    .line 43
    .line 44
    iget-object p1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;->free_days:Ljava/lang/String;

    .line 45
    .line 46
    goto :goto_1

    .line 47
    :cond_1
    move-object p1, v4

    .line 48
    :goto_1
    if-nez p1, :cond_2

    .line 49
    .line 50
    move-object p1, v3

    .line 51
    goto :goto_2

    .line 52
    :cond_2
    const-string v6, "mYearItem?.free_days ?: \"\""

    .line 53
    .line 54
    invoke-static {p1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    :goto_2
    iget-object v6, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->oOo0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;

    .line 58
    .line 59
    if-eqz v6, :cond_3

    .line 60
    .line 61
    iget-object v6, v6, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;->full_price:Ljava/lang/String;

    .line 62
    .line 63
    goto :goto_3

    .line 64
    :cond_3
    move-object v6, v4

    .line 65
    :goto_3
    if-nez v6, :cond_4

    .line 66
    .line 67
    goto :goto_4

    .line 68
    :cond_4
    const-string v3, "mYearItem?.full_price ?: \"\""

    .line 69
    .line 70
    invoke-static {v6, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    move-object v3, v6

    .line 74
    :goto_4
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇o〇88〇8()Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;

    .line 75
    .line 76
    .line 77
    move-result-object v6

    .line 78
    if-eqz v6, :cond_5

    .line 79
    .line 80
    iget-object v6, v6, Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;->ooo0〇〇O:Landroid/widget/TextView;

    .line 81
    .line 82
    goto :goto_5

    .line 83
    :cond_5
    move-object v6, v4

    .line 84
    :goto_5
    if-nez v6, :cond_6

    .line 85
    .line 86
    goto :goto_6

    .line 87
    :cond_6
    new-array v2, v2, [Ljava/lang/Object;

    .line 88
    .line 89
    aput-object p1, v2, v1

    .line 90
    .line 91
    aput-object v3, v2, v5

    .line 92
    .line 93
    const p1, 0x7f1310c4

    .line 94
    .line 95
    .line 96
    invoke-virtual {p0, p1, v2}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object p1

    .line 100
    invoke-virtual {v6, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    .line 102
    .line 103
    :goto_6
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇o〇88〇8()Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;

    .line 104
    .line 105
    .line 106
    move-result-object p1

    .line 107
    if-eqz p1, :cond_7

    .line 108
    .line 109
    iget-object v4, p1, Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;->〇〇08O:Landroid/widget/TextView;

    .line 110
    .line 111
    :cond_7
    if-nez v4, :cond_8

    .line 112
    .line 113
    goto/16 :goto_e

    .line 114
    .line 115
    :cond_8
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 116
    .line 117
    .line 118
    goto/16 :goto_e

    .line 119
    .line 120
    :cond_9
    if-nez p1, :cond_13

    .line 121
    .line 122
    iget-object p1, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->OO〇00〇8oO:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;

    .line 123
    .line 124
    if-eqz p1, :cond_a

    .line 125
    .line 126
    iget-object p1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;->description:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPriceStr;

    .line 127
    .line 128
    if-eqz p1, :cond_a

    .line 129
    .line 130
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇o〇88〇8()Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;

    .line 131
    .line 132
    .line 133
    move-result-object v6

    .line 134
    if-eqz v6, :cond_a

    .line 135
    .line 136
    iget-object v6, v6, Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;->ooo0〇〇O:Landroid/widget/TextView;

    .line 137
    .line 138
    if-eqz v6, :cond_a

    .line 139
    .line 140
    iget v7, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇080OO8〇0:I

    .line 141
    .line 142
    invoke-static {v6, p1, v7}, Lcom/intsig/camscanner/guide/GuideTextViewUtils;->〇o00〇〇Oo(Landroid/widget/TextView;Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPriceStr;I)V

    .line 143
    .line 144
    .line 145
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 146
    .line 147
    goto :goto_7

    .line 148
    :cond_a
    move-object p1, v4

    .line 149
    :goto_7
    if-nez p1, :cond_13

    .line 150
    .line 151
    iget-object p1, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->OO〇00〇8oO:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;

    .line 152
    .line 153
    if-eqz p1, :cond_b

    .line 154
    .line 155
    iget-object p1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;->first_mo_price:Ljava/lang/String;

    .line 156
    .line 157
    goto :goto_8

    .line 158
    :cond_b
    move-object p1, v4

    .line 159
    :goto_8
    if-nez p1, :cond_c

    .line 160
    .line 161
    move-object p1, v3

    .line 162
    goto :goto_9

    .line 163
    :cond_c
    const-string v6, "mMonthItem?.first_mo_price ?: \"\""

    .line 164
    .line 165
    invoke-static {p1, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    .line 167
    .line 168
    :goto_9
    iget-object v6, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->OO〇00〇8oO:Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;

    .line 169
    .line 170
    if-eqz v6, :cond_d

    .line 171
    .line 172
    iget-object v6, v6, Lcom/intsig/comm/purchase/entity/QueryProductsResult$AdPremiumPopPriceItem;->full_price:Ljava/lang/String;

    .line 173
    .line 174
    goto :goto_a

    .line 175
    :cond_d
    move-object v6, v4

    .line 176
    :goto_a
    if-nez v6, :cond_e

    .line 177
    .line 178
    goto :goto_b

    .line 179
    :cond_e
    const-string v3, "mMonthItem?.full_price ?: \"\""

    .line 180
    .line 181
    invoke-static {v6, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    .line 183
    .line 184
    move-object v3, v6

    .line 185
    :goto_b
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇o〇88〇8()Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;

    .line 186
    .line 187
    .line 188
    move-result-object v6

    .line 189
    if-eqz v6, :cond_f

    .line 190
    .line 191
    iget-object v6, v6, Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;->ooo0〇〇O:Landroid/widget/TextView;

    .line 192
    .line 193
    goto :goto_c

    .line 194
    :cond_f
    move-object v6, v4

    .line 195
    :goto_c
    if-nez v6, :cond_10

    .line 196
    .line 197
    goto :goto_d

    .line 198
    :cond_10
    new-array v2, v2, [Ljava/lang/Object;

    .line 199
    .line 200
    aput-object p1, v2, v1

    .line 201
    .line 202
    aput-object v3, v2, v5

    .line 203
    .line 204
    const p1, 0x7f131117

    .line 205
    .line 206
    .line 207
    invoke-virtual {p0, p1, v2}, Landroidx/fragment/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 208
    .line 209
    .line 210
    move-result-object p1

    .line 211
    invoke-virtual {v6, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 212
    .line 213
    .line 214
    :goto_d
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇o〇88〇8()Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;

    .line 215
    .line 216
    .line 217
    move-result-object p1

    .line 218
    if-eqz p1, :cond_11

    .line 219
    .line 220
    iget-object v4, p1, Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;->〇〇08O:Landroid/widget/TextView;

    .line 221
    .line 222
    :cond_11
    if-nez v4, :cond_12

    .line 223
    .line 224
    goto :goto_e

    .line 225
    :cond_12
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 226
    .line 227
    .line 228
    :cond_13
    :goto_e
    return-void
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
.end method

.method private final 〇8〇80o()Landroid/os/Handler;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->o8〇OO0〇0o:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Landroid/os/Handler;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public static final synthetic 〇8〇OOoooo(Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;)Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇o〇88〇8()Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static final 〇O8〇8000()Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$Companion;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$Companion;->O8()Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private final 〇o08()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->MAIN_WEEK:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->toTrackerValue()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    sget-object v1, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$Companion;

    .line 8
    .line 9
    invoke-virtual {v1}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$Companion;->〇o00〇〇Oo()I

    .line 10
    .line 11
    .line 12
    move-result v1

    .line 13
    new-instance v2, Ljava/lang/StringBuilder;

    .line 14
    .line 15
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 16
    .line 17
    .line 18
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    const-string v0, "_"

    .line 22
    .line 23
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    .line 25
    .line 26
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 27
    .line 28
    .line 29
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    new-instance v1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 34
    .line 35
    invoke-direct {v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 36
    .line 37
    .line 38
    sget-object v2, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->CSPremiumPop:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 39
    .line 40
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId(Lcom/intsig/camscanner/purchase/track/PurchasePageId;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 41
    .line 42
    .line 43
    move-result-object v1

    .line 44
    sget-object v2, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->MAIN_WEEK_TIMES:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 45
    .line 46
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->setValue(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme(Lcom/intsig/camscanner/purchase/track/PurchaseScheme;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 51
    .line 52
    .line 53
    move-result-object v0

    .line 54
    sget-object v1, Lcom/intsig/camscanner/purchase/entity/Function;->SELF_SEARCH:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 55
    .line 56
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function(Lcom/intsig/camscanner/purchase/entity/Function;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 57
    .line 58
    .line 59
    move-result-object v0

    .line 60
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->o〇00O:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 61
    .line 62
    new-instance v0, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 63
    .line 64
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 65
    .line 66
    .line 67
    move-result-object v1

    .line 68
    iget-object v2, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->o〇00O:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 69
    .line 70
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 71
    .line 72
    .line 73
    new-instance v1, Lcom/intsig/camscanner/purchase/dialog/〇00;

    .line 74
    .line 75
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/purchase/dialog/〇00;-><init>(Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;)V

    .line 76
    .line 77
    .line 78
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->〇O〇80o08O(Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient$PurchaseCallback;)V

    .line 79
    .line 80
    .line 81
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->O8o08O8O:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 82
    .line 83
    return-void
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private final 〇o〇88〇8()Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇08O〇00〇o:Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->ooo0〇〇O:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/FragmentViewBinding;->〇〇888(Landroidx/fragment/app/Fragment;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
.end method

.method private final 〇〇O80〇0o()V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇o〇88〇8()Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_1

    .line 6
    .line 7
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;->〇o0O:Landroidx/viewpager/widget/ViewPager;

    .line 8
    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    const/4 v1, 0x3

    .line 12
    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setOffscreenPageLimit(I)V

    .line 13
    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->o〇O8OO()Landroidx/viewpager/widget/PagerAdapter;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    invoke-virtual {v0, v1}, Landroidx/viewpager/widget/ViewPager;->setAdapter(Landroidx/viewpager/widget/PagerAdapter;)V

    .line 20
    .line 21
    .line 22
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇o〇88〇8()Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    if-eqz v1, :cond_0

    .line 27
    .line 28
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;->〇O〇〇O8:Lcom/intsig/camscanner/guide/IndicatorView;

    .line 29
    .line 30
    if-eqz v1, :cond_0

    .line 31
    .line 32
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/guide/IndicatorView;->setViewPager(Landroidx/viewpager/widget/ViewPager;)V

    .line 33
    .line 34
    .line 35
    :cond_0
    new-instance v1, Lcom/intsig/camscanner/purchase/dialog/o〇O8〇〇o;

    .line 36
    .line 37
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/purchase/dialog/o〇O8〇〇o;-><init>(Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;)V

    .line 38
    .line 39
    .line 40
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 41
    .line 42
    .line 43
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇8〇80o()Landroid/os/Handler;

    .line 44
    .line 45
    .line 46
    move-result-object v0

    .line 47
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇8〇80o()Landroid/os/Handler;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    const/4 v2, 0x1

    .line 52
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    const-wide/16 v2, 0x1b58

    .line 57
    .line 58
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 59
    .line 60
    .line 61
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 62
    .line 63
    .line 64
    move-result-object v0

    .line 65
    if-eqz v0, :cond_1

    .line 66
    .line 67
    invoke-virtual {v0}, Landroidx/activity/ComponentActivity;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    .line 68
    .line 69
    .line 70
    move-result-object v0

    .line 71
    if-eqz v0, :cond_1

    .line 72
    .line 73
    new-instance v1, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$initViewPager$1$2;

    .line 74
    .line 75
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$initViewPager$1$2;-><init>(Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;)V

    .line 76
    .line 77
    .line 78
    invoke-virtual {v0, v1}, Landroidx/lifecycle/Lifecycle;->addObserver(Landroidx/lifecycle/LifecycleObserver;)V

    .line 79
    .line 80
    .line 81
    :cond_1
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public static synthetic 〇〇o0〇8(Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;Landroidx/appcompat/widget/AppCompatImageView;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇0ooOOo(Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;Landroidx/appcompat/widget/AppCompatImageView;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final 〇〇〇0()V
    .locals 7

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇o〇88〇8()Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;->〇0O:Landroid/widget/RelativeLayout;

    .line 8
    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    const v2, 0x3f666666    # 0.9f

    .line 12
    .line 13
    .line 14
    const-wide/16 v3, 0x7d0

    .line 15
    .line 16
    const/4 v5, -0x1

    .line 17
    const/4 v6, 0x0

    .line 18
    invoke-static/range {v1 .. v6}, Lcom/intsig/utils/AnimateUtils;->〇〇888(Landroid/view/View;FJILandroid/animation/Animator$AnimatorListener;)V

    .line 19
    .line 20
    .line 21
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇o〇88〇8()Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    if-eqz v0, :cond_1

    .line 26
    .line 27
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;->〇8〇oO〇〇8o:Landroid/widget/TextView;

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_1
    const/4 v0, 0x0

    .line 31
    :goto_0
    if-nez v0, :cond_2

    .line 32
    .line 33
    goto :goto_1

    .line 34
    :cond_2
    invoke-static {}, Landroid/text/method/ScrollingMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 39
    .line 40
    .line 41
    :goto_1
    iget-boolean v0, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->oOo〇8o008:Z

    .line 42
    .line 43
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇08O(Z)V

    .line 44
    .line 45
    .line 46
    iget-boolean v0, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->oOo〇8o008:Z

    .line 47
    .line 48
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇8O0880(Z)V

    .line 49
    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private static final 〇〇〇00(Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V
    .locals 0

    .line 1
    const-string/jumbo p1, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    if-eqz p2, :cond_0

    .line 8
    .line 9
    const-string p1, "GpDropCnlDialog"

    .line 10
    .line 11
    const-string p2, "purchase success"

    .line 12
    .line 13
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/app/BaseDialogFragment;->dismiss()V

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method


# virtual methods
.method public dealClickAction(Landroid/view/View;)V
    .locals 6

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/app/BaseDialogFragment;->dealClickAction(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    if-nez p1, :cond_0

    .line 5
    .line 6
    return-void

    .line 7
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇o〇88〇8()Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    const/4 v1, 0x0

    .line 12
    if-eqz v0, :cond_1

    .line 13
    .line 14
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_1
    move-object v0, v1

    .line 18
    :goto_0
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    const-string v2, "GpDropCnlDialog"

    .line 23
    .line 24
    if-eqz v0, :cond_2

    .line 25
    .line 26
    const-string p1, "close"

    .line 27
    .line 28
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-virtual {p0}, Lcom/intsig/app/BaseDialogFragment;->dismiss()V

    .line 32
    .line 33
    .line 34
    goto/16 :goto_4

    .line 35
    .line 36
    :cond_2
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇o〇88〇8()Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    if-eqz v0, :cond_3

    .line 41
    .line 42
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;->OO:Landroid/widget/ImageView;

    .line 43
    .line 44
    goto :goto_1

    .line 45
    :cond_3
    move-object v0, v1

    .line 46
    :goto_1
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 47
    .line 48
    .line 49
    move-result v0

    .line 50
    const/4 v3, 0x0

    .line 51
    const/4 v4, 0x1

    .line 52
    if-eqz v0, :cond_8

    .line 53
    .line 54
    iget-boolean p1, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->oOo〇8o008:Z

    .line 55
    .line 56
    xor-int/2addr p1, v4

    .line 57
    iput-boolean p1, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->oOo〇8o008:Z

    .line 58
    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    .line 60
    .line 61
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 62
    .line 63
    .line 64
    const-string v1, "click free trial enabled  isFreeTrailEnabled="

    .line 65
    .line 66
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    .line 68
    .line 69
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 70
    .line 71
    .line 72
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object p1

    .line 76
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    .line 78
    .line 79
    iget-boolean p1, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->oOo〇8o008:Z

    .line 80
    .line 81
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇08O(Z)V

    .line 82
    .line 83
    .line 84
    iget-boolean p1, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->oOo〇8o008:Z

    .line 85
    .line 86
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇8O0880(Z)V

    .line 87
    .line 88
    .line 89
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇o〇88〇8()Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;

    .line 90
    .line 91
    .line 92
    move-result-object p1

    .line 93
    if-eqz p1, :cond_5

    .line 94
    .line 95
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;->OO:Landroid/widget/ImageView;

    .line 96
    .line 97
    if-eqz p1, :cond_5

    .line 98
    .line 99
    iget-boolean v0, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->oOo〇8o008:Z

    .line 100
    .line 101
    if-eqz v0, :cond_4

    .line 102
    .line 103
    const v0, 0x7f080b6b

    .line 104
    .line 105
    .line 106
    goto :goto_2

    .line 107
    :cond_4
    const v0, 0x7f080b68

    .line 108
    .line 109
    .line 110
    :goto_2
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 111
    .line 112
    .line 113
    :cond_5
    iget-boolean p1, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->oOo〇8o008:Z

    .line 114
    .line 115
    if-eqz p1, :cond_6

    .line 116
    .line 117
    const-string p1, "button_on"

    .line 118
    .line 119
    goto :goto_3

    .line 120
    :cond_6
    const-string p1, "button_off"

    .line 121
    .line 122
    :goto_3
    sget-object v0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$Companion;

    .line 123
    .line 124
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog$Companion;->〇o00〇〇Oo()I

    .line 125
    .line 126
    .line 127
    move-result v0

    .line 128
    new-instance v1, Ljava/lang/StringBuilder;

    .line 129
    .line 130
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 131
    .line 132
    .line 133
    const-string v2, "main_week_"

    .line 134
    .line 135
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 136
    .line 137
    .line 138
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 139
    .line 140
    .line 141
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 142
    .line 143
    .line 144
    move-result-object v0

    .line 145
    const/4 v1, 0x3

    .line 146
    new-array v1, v1, [Landroid/util/Pair;

    .line 147
    .line 148
    new-instance v2, Landroid/util/Pair;

    .line 149
    .line 150
    const-string v5, "scheme"

    .line 151
    .line 152
    invoke-direct {v2, v5, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 153
    .line 154
    .line 155
    aput-object v2, v1, v3

    .line 156
    .line 157
    new-instance v0, Landroid/util/Pair;

    .line 158
    .line 159
    const-string v2, "from"

    .line 160
    .line 161
    const-string v3, "selfsearch"

    .line 162
    .line 163
    invoke-direct {v0, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 164
    .line 165
    .line 166
    aput-object v0, v1, v4

    .line 167
    .line 168
    new-instance v0, Landroid/util/Pair;

    .line 169
    .line 170
    iget-boolean v2, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->oOo〇8o008:Z

    .line 171
    .line 172
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇0〇0(Z)Ljava/lang/String;

    .line 173
    .line 174
    .line 175
    move-result-object v2

    .line 176
    if-nez v2, :cond_7

    .line 177
    .line 178
    const-string v2, ""

    .line 179
    .line 180
    :cond_7
    const-string v3, "product_id"

    .line 181
    .line 182
    invoke-direct {v0, v3, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 183
    .line 184
    .line 185
    const/4 v2, 0x2

    .line 186
    aput-object v0, v1, v2

    .line 187
    .line 188
    const-string v0, "CSPremiumPop"

    .line 189
    .line 190
    invoke-static {v0, p1, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 191
    .line 192
    .line 193
    goto :goto_4

    .line 194
    :cond_8
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇o〇88〇8()Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;

    .line 195
    .line 196
    .line 197
    move-result-object v0

    .line 198
    if-eqz v0, :cond_9

    .line 199
    .line 200
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;->〇0O:Landroid/widget/RelativeLayout;

    .line 201
    .line 202
    :cond_9
    invoke-static {p1, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 203
    .line 204
    .line 205
    move-result p1

    .line 206
    if-eqz p1, :cond_d

    .line 207
    .line 208
    const-string p1, "go purchase"

    .line 209
    .line 210
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    .line 212
    .line 213
    iget-boolean p1, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->oOo〇8o008:Z

    .line 214
    .line 215
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇0〇0(Z)Ljava/lang/String;

    .line 216
    .line 217
    .line 218
    move-result-object p1

    .line 219
    if-eqz p1, :cond_a

    .line 220
    .line 221
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 222
    .line 223
    .line 224
    move-result v0

    .line 225
    if-nez v0, :cond_b

    .line 226
    .line 227
    :cond_a
    const/4 v3, 0x1

    .line 228
    :cond_b
    if-eqz v3, :cond_c

    .line 229
    .line 230
    const-string p1, "id is null or empty"

    .line 231
    .line 232
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    .line 234
    .line 235
    return-void

    .line 236
    :cond_c
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->O8o08O8O:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 237
    .line 238
    if-eqz v0, :cond_d

    .line 239
    .line 240
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->O0O8OO088(Ljava/lang/String;)V

    .line 241
    .line 242
    .line 243
    :cond_d
    :goto_4
    return-void
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
.end method

.method protected init(Landroid/os/Bundle;)V
    .locals 4

    .line 1
    const-string p1, "init"

    .line 2
    .line 3
    const-string v0, "GpDropCnlDialog"

    .line 4
    .line 5
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/app/BaseDialogFragment;->〇O8oOo0()V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    const/4 v1, 0x0

    .line 16
    if-eqz p1, :cond_0

    .line 17
    .line 18
    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    goto :goto_0

    .line 23
    :cond_0
    move-object p1, v1

    .line 24
    :goto_0
    const/4 v2, 0x1

    .line 25
    invoke-static {p1, v2}, Lcom/intsig/utils/SystemUiUtil;->o〇0(Landroid/view/Window;Z)V

    .line 26
    .line 27
    .line 28
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇088O()Z

    .line 29
    .line 30
    .line 31
    move-result p1

    .line 32
    iput-boolean p1, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇0O:Z

    .line 33
    .line 34
    if-nez p1, :cond_1

    .line 35
    .line 36
    const-string p1, "param not ok"

    .line 37
    .line 38
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {p0}, Lcom/intsig/app/BaseDialogFragment;->dismiss()V

    .line 42
    .line 43
    .line 44
    return-void

    .line 45
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->Ooo8o()V

    .line 46
    .line 47
    .line 48
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇o08()V

    .line 49
    .line 50
    .line 51
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->oOoO8OO〇()V

    .line 52
    .line 53
    .line 54
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇〇O80〇0o()V

    .line 55
    .line 56
    .line 57
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇〇〇0()V

    .line 58
    .line 59
    .line 60
    const/4 p1, 0x3

    .line 61
    new-array p1, p1, [Landroid/view/View;

    .line 62
    .line 63
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇o〇88〇8()Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;

    .line 64
    .line 65
    .line 66
    move-result-object v0

    .line 67
    if-eqz v0, :cond_2

    .line 68
    .line 69
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 70
    .line 71
    goto :goto_1

    .line 72
    :cond_2
    move-object v0, v1

    .line 73
    :goto_1
    const/4 v3, 0x0

    .line 74
    aput-object v0, p1, v3

    .line 75
    .line 76
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇o〇88〇8()Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    if-eqz v0, :cond_3

    .line 81
    .line 82
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;->OO:Landroid/widget/ImageView;

    .line 83
    .line 84
    goto :goto_2

    .line 85
    :cond_3
    move-object v0, v1

    .line 86
    :goto_2
    aput-object v0, p1, v2

    .line 87
    .line 88
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇o〇88〇8()Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;

    .line 89
    .line 90
    .line 91
    move-result-object v0

    .line 92
    if-eqz v0, :cond_4

    .line 93
    .line 94
    iget-object v1, v0, Lcom/intsig/camscanner/databinding/DialogGpDropCnlBinding;->〇0O:Landroid/widget/RelativeLayout;

    .line 95
    .line 96
    :cond_4
    const/4 v0, 0x2

    .line 97
    aput-object v1, p1, v0

    .line 98
    .line 99
    invoke-virtual {p0, p1}, Lcom/intsig/app/BaseDialogFragment;->setSomeOnClickListeners([Landroid/view/View;)V

    .line 100
    .line 101
    .line 102
    return-void
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method public onStart()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/DialogFragment;->onStart()V

    .line 2
    .line 3
    .line 4
    iget-boolean v0, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->〇0O:Z

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/dialog/GpDropCnlDialog;->o〇00O:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 9
    .line 10
    invoke-static {v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTrackerUtil;->oO80(Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
.end method

.method public provideLayoutResourceId()I
    .locals 1

    .line 1
    const v0, 0x7f0d01c7

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

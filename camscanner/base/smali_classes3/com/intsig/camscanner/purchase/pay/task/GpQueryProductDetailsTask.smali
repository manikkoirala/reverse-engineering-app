.class public final Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;
.super Ljava/lang/Object;
.source "GpQueryProductDetailsTask.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final O8:Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final 〇080:Lkotlinx/coroutines/CoroutineScope;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o00〇〇Oo:Lkotlinx/coroutines/Job;

.field private 〇o〇:Lcom/intsig/pay/base/model/ProductDetailsData;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;->O8:Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    invoke-static {}, Lkotlinx/coroutines/CoroutineScopeKt;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineScope;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;->〇080:Lkotlinx/coroutines/CoroutineScope;

    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private final Oo08(Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lkotlin/Unit;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    invoke-interface {p1}, Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;->〇080()Lkotlinx/coroutines/flow/Flow;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    new-instance v0, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask$startWatch$2;

    .line 6
    .line 7
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask$startWatch$2;-><init>(Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;)V

    .line 8
    .line 9
    .line 10
    invoke-interface {p1, v0, p2}, Lkotlinx/coroutines/flow/Flow;->〇080(Lkotlinx/coroutines/flow/FlowCollector;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->O8()Ljava/lang/Object;

    .line 15
    .line 16
    .line 17
    move-result-object p2

    .line 18
    if-ne p1, p2, :cond_0

    .line 19
    .line 20
    return-object p1

    .line 21
    :cond_0
    sget-object p1, Lkotlin/Unit;->〇080:Lkotlin/Unit;

    .line 22
    .line 23
    return-object p1
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static final synthetic 〇080(Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;)Lkotlinx/coroutines/Job;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;->〇o00〇〇Oo:Lkotlinx/coroutines/Job;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;Lcom/intsig/pay/base/model/ProductDetailsData;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;->〇o〇:Lcom/intsig/pay/base/model/ProductDetailsData;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;->Oo08(Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method


# virtual methods
.method public final O8(Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;Ljava/util/ArrayList;ILkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 16
    .param p2    # Ljava/util/ArrayList;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p4    # Lkotlin/coroutines/Continuation;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;I",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lcom/intsig/pay/base/model/ProductDetailsData;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    move-object/from16 v0, p1

    .line 4
    .line 5
    move-object/from16 v2, p4

    .line 6
    .line 7
    instance-of v3, v2, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask$chain$1;

    .line 8
    .line 9
    if-eqz v3, :cond_0

    .line 10
    .line 11
    move-object v3, v2

    .line 12
    check-cast v3, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask$chain$1;

    .line 13
    .line 14
    iget v4, v3, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask$chain$1;->〇08O〇00〇o:I

    .line 15
    .line 16
    const/high16 v5, -0x80000000

    .line 17
    .line 18
    and-int v6, v4, v5

    .line 19
    .line 20
    if-eqz v6, :cond_0

    .line 21
    .line 22
    sub-int/2addr v4, v5

    .line 23
    iput v4, v3, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask$chain$1;->〇08O〇00〇o:I

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    new-instance v3, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask$chain$1;

    .line 27
    .line 28
    invoke-direct {v3, v1, v2}, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask$chain$1;-><init>(Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;Lkotlin/coroutines/Continuation;)V

    .line 29
    .line 30
    .line 31
    :goto_0
    iget-object v2, v3, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 32
    .line 33
    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->O8()Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object v4

    .line 37
    iget v5, v3, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask$chain$1;->〇08O〇00〇o:I

    .line 38
    .line 39
    const/4 v6, 0x2

    .line 40
    const-string v7, "PurchaseHelper-GpQueryProductDetailsTask"

    .line 41
    .line 42
    const/4 v8, 0x1

    .line 43
    const/4 v9, 0x0

    .line 44
    if-eqz v5, :cond_3

    .line 45
    .line 46
    if-eq v5, v8, :cond_2

    .line 47
    .line 48
    if-ne v5, v6, :cond_1

    .line 49
    .line 50
    iget-object v0, v3, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask$chain$1;->o0:Ljava/lang/Object;

    .line 51
    .line 52
    move-object v3, v0

    .line 53
    check-cast v3, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;

    .line 54
    .line 55
    :try_start_0
    invoke-static {v2}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    .line 57
    .line 58
    goto/16 :goto_3

    .line 59
    .line 60
    :catchall_0
    move-exception v0

    .line 61
    goto/16 :goto_5

    .line 62
    .line 63
    :catch_0
    move-exception v0

    .line 64
    goto/16 :goto_4

    .line 65
    .line 66
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 67
    .line 68
    const-string v2, "call to \'resume\' before \'invoke\' with coroutine"

    .line 69
    .line 70
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    throw v0

    .line 74
    :cond_2
    iget-object v0, v3, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask$chain$1;->o0:Ljava/lang/Object;

    .line 75
    .line 76
    move-object v5, v0

    .line 77
    check-cast v5, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;

    .line 78
    .line 79
    :try_start_1
    invoke-static {v2}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 80
    .line 81
    .line 82
    goto :goto_2

    .line 83
    :catchall_1
    move-exception v0

    .line 84
    move-object v3, v5

    .line 85
    goto/16 :goto_5

    .line 86
    .line 87
    :catch_1
    move-exception v0

    .line 88
    move-object v3, v5

    .line 89
    goto/16 :goto_4

    .line 90
    .line 91
    :cond_3
    invoke-static {v2}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 92
    .line 93
    .line 94
    const-string/jumbo v2, "start chain"

    .line 95
    .line 96
    .line 97
    invoke-static {v7, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    .line 99
    .line 100
    if-nez v0, :cond_5

    .line 101
    .line 102
    :try_start_2
    const-string/jumbo v0, "start chain payClient = null"

    .line 103
    .line 104
    .line 105
    invoke-static {v7, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 106
    .line 107
    .line 108
    iget-object v0, v1, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;->〇o00〇〇Oo:Lkotlinx/coroutines/Job;

    .line 109
    .line 110
    if-eqz v0, :cond_4

    .line 111
    .line 112
    invoke-static {v0, v9, v8, v9}, Lkotlinx/coroutines/Job$DefaultImpls;->〇080(Lkotlinx/coroutines/Job;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V

    .line 113
    .line 114
    .line 115
    :cond_4
    iget-object v0, v1, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;->〇080:Lkotlinx/coroutines/CoroutineScope;

    .line 116
    .line 117
    :goto_1
    invoke-static {v0, v9, v8, v9}, Lkotlinx/coroutines/CoroutineScopeKt;->O8(Lkotlinx/coroutines/CoroutineScope;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V

    .line 118
    .line 119
    .line 120
    return-object v9

    .line 121
    :cond_5
    :try_start_3
    iget-object v10, v1, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;->〇080:Lkotlinx/coroutines/CoroutineScope;

    .line 122
    .line 123
    const/4 v11, 0x0

    .line 124
    const/4 v12, 0x0

    .line 125
    new-instance v13, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask$chain$2;

    .line 126
    .line 127
    invoke-direct {v13, v1, v0, v9}, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask$chain$2;-><init>(Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;Lkotlin/coroutines/Continuation;)V

    .line 128
    .line 129
    .line 130
    const/4 v14, 0x3

    .line 131
    const/4 v15, 0x0

    .line 132
    invoke-static/range {v10 .. v15}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 133
    .line 134
    .line 135
    move-result-object v2

    .line 136
    iput-object v2, v1, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;->〇o00〇〇Oo:Lkotlinx/coroutines/Job;

    .line 137
    .line 138
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o〇()Lkotlinx/coroutines/MainCoroutineDispatcher;

    .line 139
    .line 140
    .line 141
    move-result-object v2

    .line 142
    new-instance v5, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask$chain$3;

    .line 143
    .line 144
    move-object/from16 v10, p2

    .line 145
    .line 146
    move/from16 v11, p3

    .line 147
    .line 148
    invoke-direct {v5, v0, v10, v11, v9}, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask$chain$3;-><init>(Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;Ljava/util/ArrayList;ILkotlin/coroutines/Continuation;)V

    .line 149
    .line 150
    .line 151
    iput-object v1, v3, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask$chain$1;->o0:Ljava/lang/Object;

    .line 152
    .line 153
    iput v8, v3, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask$chain$1;->〇08O〇00〇o:I

    .line 154
    .line 155
    invoke-static {v2, v5, v3}, Lkotlinx/coroutines/BuildersKt;->Oo08(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 156
    .line 157
    .line 158
    move-result-object v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 159
    if-ne v0, v4, :cond_6

    .line 160
    .line 161
    return-object v4

    .line 162
    :cond_6
    move-object v5, v1

    .line 163
    :goto_2
    :try_start_4
    iget-object v0, v5, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;->〇o00〇〇Oo:Lkotlinx/coroutines/Job;

    .line 164
    .line 165
    if-eqz v0, :cond_7

    .line 166
    .line 167
    iput-object v5, v3, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask$chain$1;->o0:Ljava/lang/Object;

    .line 168
    .line 169
    iput v6, v3, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask$chain$1;->〇08O〇00〇o:I

    .line 170
    .line 171
    invoke-interface {v0, v3}, Lkotlinx/coroutines/Job;->〇〇〇0〇〇0(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 172
    .line 173
    .line 174
    move-result-object v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 175
    if-ne v0, v4, :cond_7

    .line 176
    .line 177
    return-object v4

    .line 178
    :cond_7
    move-object v3, v5

    .line 179
    :goto_3
    :try_start_5
    const-string/jumbo v0, "watchJob end"

    .line 180
    .line 181
    .line 182
    invoke-static {v7, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    .line 184
    .line 185
    iget-object v0, v3, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;->〇o〇:Lcom/intsig/pay/base/model/ProductDetailsData;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 186
    .line 187
    iget-object v2, v3, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;->〇o00〇〇Oo:Lkotlinx/coroutines/Job;

    .line 188
    .line 189
    if-eqz v2, :cond_8

    .line 190
    .line 191
    invoke-static {v2, v9, v8, v9}, Lkotlinx/coroutines/Job$DefaultImpls;->〇080(Lkotlinx/coroutines/Job;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V

    .line 192
    .line 193
    .line 194
    :cond_8
    iget-object v2, v3, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;->〇080:Lkotlinx/coroutines/CoroutineScope;

    .line 195
    .line 196
    invoke-static {v2, v9, v8, v9}, Lkotlinx/coroutines/CoroutineScopeKt;->O8(Lkotlinx/coroutines/CoroutineScope;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V

    .line 197
    .line 198
    .line 199
    return-object v0

    .line 200
    :catchall_2
    move-exception v0

    .line 201
    move-object v3, v1

    .line 202
    goto :goto_5

    .line 203
    :catch_2
    move-exception v0

    .line 204
    move-object v3, v1

    .line 205
    :goto_4
    :try_start_6
    invoke-static {v7, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 206
    .line 207
    .line 208
    iget-object v0, v3, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;->〇o00〇〇Oo:Lkotlinx/coroutines/Job;

    .line 209
    .line 210
    if-eqz v0, :cond_9

    .line 211
    .line 212
    invoke-static {v0, v9, v8, v9}, Lkotlinx/coroutines/Job$DefaultImpls;->〇080(Lkotlinx/coroutines/Job;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V

    .line 213
    .line 214
    .line 215
    :cond_9
    iget-object v0, v3, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;->〇080:Lkotlinx/coroutines/CoroutineScope;

    .line 216
    .line 217
    goto :goto_1

    .line 218
    :goto_5
    iget-object v2, v3, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;->〇o00〇〇Oo:Lkotlinx/coroutines/Job;

    .line 219
    .line 220
    if-eqz v2, :cond_a

    .line 221
    .line 222
    invoke-static {v2, v9, v8, v9}, Lkotlinx/coroutines/Job$DefaultImpls;->〇080(Lkotlinx/coroutines/Job;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V

    .line 223
    .line 224
    .line 225
    :cond_a
    iget-object v2, v3, Lcom/intsig/camscanner/purchase/pay/task/GpQueryProductDetailsTask;->〇080:Lkotlinx/coroutines/CoroutineScope;

    .line 226
    .line 227
    invoke-static {v2, v9, v8, v9}, Lkotlinx/coroutines/CoroutineScopeKt;->O8(Lkotlinx/coroutines/CoroutineScope;Ljava/util/concurrent/CancellationException;ILjava/lang/Object;)V

    .line 228
    .line 229
    .line 230
    throw v0
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
.end method

.class public final Lcom/intsig/camscanner/purchase/pay/task/HmsCreateOrderTask;
.super Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;
.source "HmsCreateOrderTask.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/purchase/pay/task/HmsCreateOrderTask$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇o〇:Lcom/intsig/camscanner/purchase/pay/task/HmsCreateOrderTask$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/purchase/pay/task/HmsCreateOrderTask$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/purchase/pay/task/HmsCreateOrderTask$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/purchase/pay/task/HmsCreateOrderTask;->〇o〇:Lcom/intsig/camscanner/purchase/pay/task/HmsCreateOrderTask$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method


# virtual methods
.method public 〇080(Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 22
    .param p1    # Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/coroutines/Continuation;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    const-string v0, "PurchaseHelper-HmsCreateOrderTask"

    .line 2
    .line 3
    const-string/jumbo v1, "start create_order"

    .line 4
    .line 5
    .line 6
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇〇888()Lcom/intsig/comm/purchase/entity/ProductResultItem;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/ProductResultItem;->propertyId:Ljava/lang/String;

    .line 14
    .line 15
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇〇888()Lcom/intsig/comm/purchase/entity/ProductResultItem;

    .line 16
    .line 17
    .line 18
    move-result-object v1

    .line 19
    iget-object v1, v1, Lcom/intsig/comm/purchase/entity/ProductResultItem;->product_id:Ljava/lang/String;

    .line 20
    .line 21
    sget-object v2, Lcom/intsig/pay/base/utils/PayTypeUtils;->〇080:Lcom/intsig/pay/base/utils/PayTypeUtils$Companion;

    .line 22
    .line 23
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇〇888()Lcom/intsig/comm/purchase/entity/ProductResultItem;

    .line 24
    .line 25
    .line 26
    move-result-object v3

    .line 27
    iget v3, v3, Lcom/intsig/comm/purchase/entity/ProductResultItem;->consume:I

    .line 28
    .line 29
    invoke-virtual {v2, v3}, Lcom/intsig/pay/base/utils/PayTypeUtils$Companion;->〇〇888(I)Z

    .line 30
    .line 31
    .line 32
    move-result v2

    .line 33
    xor-int/lit8 v2, v2, 0x1

    .line 34
    .line 35
    sget-object v3, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 36
    .line 37
    invoke-virtual {v3}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 38
    .line 39
    .line 40
    move-result-object v3

    .line 41
    invoke-static {v3, v1, v0, v2}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->Oooo8o0〇(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    .line 42
    .line 43
    .line 44
    move-result-object v0

    .line 45
    new-instance v15, Lcom/intsig/pay/base/model/PayOrderRequest;

    .line 46
    .line 47
    move-object v1, v15

    .line 48
    const/4 v2, 0x0

    .line 49
    const/4 v3, 0x0

    .line 50
    const/4 v4, 0x0

    .line 51
    const/4 v5, 0x0

    .line 52
    const/4 v6, 0x0

    .line 53
    const/4 v7, 0x0

    .line 54
    const/4 v8, 0x0

    .line 55
    const/4 v9, 0x0

    .line 56
    const/4 v10, 0x0

    .line 57
    const/4 v11, 0x0

    .line 58
    const/4 v12, 0x0

    .line 59
    const/4 v13, 0x0

    .line 60
    const/4 v14, 0x0

    .line 61
    const/16 v16, 0x0

    .line 62
    .line 63
    move-object/from16 v21, v15

    .line 64
    .line 65
    move-object/from16 v15, v16

    .line 66
    .line 67
    const/16 v17, 0x0

    .line 68
    .line 69
    const/16 v18, 0x0

    .line 70
    .line 71
    const v19, 0x1ffff

    .line 72
    .line 73
    .line 74
    const/16 v20, 0x0

    .line 75
    .line 76
    invoke-direct/range {v1 .. v20}, Lcom/intsig/pay/base/model/PayOrderRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 77
    .line 78
    .line 79
    move-object/from16 v1, p1

    .line 80
    .line 81
    move-object/from16 v2, v21

    .line 82
    .line 83
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->OO0o〇〇(Lcom/intsig/pay/base/model/PayOrderRequest;)V

    .line 84
    .line 85
    .line 86
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->O8()Lcom/intsig/pay/base/model/PayOrderRequest;

    .line 87
    .line 88
    .line 89
    move-result-object v2

    .line 90
    if-eqz v2, :cond_0

    .line 91
    .line 92
    invoke-virtual {v2, v0}, Lcom/intsig/pay/base/model/PayOrderRequest;->setDeveloperPayload(Ljava/lang/String;)V

    .line 93
    .line 94
    .line 95
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇〇888()Lcom/intsig/comm/purchase/entity/ProductResultItem;

    .line 96
    .line 97
    .line 98
    move-result-object v0

    .line 99
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/ProductResultItem;->product_id:Ljava/lang/String;

    .line 100
    .line 101
    invoke-virtual {v2, v0}, Lcom/intsig/pay/base/model/PayOrderRequest;->setPay_param(Ljava/lang/String;)V

    .line 102
    .line 103
    .line 104
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇8o8o〇()Ljava/lang/String;

    .line 105
    .line 106
    .line 107
    move-result-object v0

    .line 108
    invoke-virtual {v2, v0}, Lcom/intsig/pay/base/model/PayOrderRequest;->setAccountId(Ljava/lang/String;)V

    .line 109
    .line 110
    .line 111
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇〇888()Lcom/intsig/comm/purchase/entity/ProductResultItem;

    .line 112
    .line 113
    .line 114
    move-result-object v0

    .line 115
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/ProductResultItem;->propertyId:Ljava/lang/String;

    .line 116
    .line 117
    invoke-static {v0}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->〇00〇8(Ljava/lang/String;)Ljava/lang/String;

    .line 118
    .line 119
    .line 120
    move-result-object v0

    .line 121
    invoke-virtual {v2, v0}, Lcom/intsig/pay/base/model/PayOrderRequest;->setProfileId(Ljava/lang/String;)V

    .line 122
    .line 123
    .line 124
    :cond_0
    invoke-virtual/range {p0 .. p2}, Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;->〇o00〇〇Oo(Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 125
    .line 126
    .line 127
    move-result-object v0

    .line 128
    return-object v0
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

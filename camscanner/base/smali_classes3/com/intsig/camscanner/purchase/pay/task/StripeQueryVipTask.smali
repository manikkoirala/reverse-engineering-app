.class public final Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask;
.super Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;
.source "StripeQueryVipTask.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final O8:Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private 〇o〇:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask;->O8:Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private final o〇0(Ljava/lang/String;)Z
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_0

    .line 3
    .line 4
    const/4 v1, 0x2

    .line 5
    const/4 v2, 0x0

    .line 6
    const-string v3, "CamScanner_NADVIP"

    .line 7
    .line 8
    invoke-static {p1, v3, v0, v1, v2}, Lkotlin/text/StringsKt;->〇00〇8(Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Z

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    const/4 v1, 0x1

    .line 13
    if-ne p1, v1, :cond_0

    .line 14
    .line 15
    const/4 v0, 0x1

    .line 16
    :cond_0
    return v0
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final 〇〇888(Lcom/intsig/comm/purchase/entity/ProductResultItem;Ljava/lang/String;Lcom/intsig/camscanner/https/entity/CSQueryProperty;)Z
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/pay/base/utils/PayTypeUtils;->〇080:Lcom/intsig/pay/base/utils/PayTypeUtils$Companion;

    .line 2
    .line 3
    iget v1, p1, Lcom/intsig/comm/purchase/entity/ProductResultItem;->consume:I

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/pay/base/utils/PayTypeUtils$Companion;->〇〇888(I)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    const/4 v1, 0x1

    .line 10
    const-string v2, "PurchaseHelper-StripeQueryVipTask"

    .line 11
    .line 12
    if-nez v0, :cond_1

    .line 13
    .line 14
    invoke-static {p2}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->o〇〇0〇(Ljava/lang/String;)Z

    .line 15
    .line 16
    .line 17
    move-result p2

    .line 18
    if-eqz p2, :cond_0

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const-string p1, "query property, consume product"

    .line 22
    .line 23
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    return v1

    .line 27
    :cond_1
    :goto_0
    iget-object p1, p1, Lcom/intsig/comm/purchase/entity/ProductResultItem;->product_id:Ljava/lang/String;

    .line 28
    .line 29
    new-instance p2, Ljava/lang/StringBuilder;

    .line 30
    .line 31
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    .line 33
    .line 34
    const-string v0, "query property, vip productId = "

    .line 35
    .line 36
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    .line 38
    .line 39
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object p2

    .line 46
    invoke-static {v2, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    invoke-static {p3}, Lcom/intsig/camscanner/https/account/UserPropertyAPI;->〇080(Lcom/intsig/camscanner/https/entity/CSQueryProperty;)[J

    .line 50
    .line 51
    .line 52
    move-result-object p2

    .line 53
    sget-object p3, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 54
    .line 55
    invoke-virtual {p3}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 56
    .line 57
    .line 58
    move-result-object p3

    .line 59
    invoke-static {p3}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o8o〇〇0O(Landroid/content/Context;)Z

    .line 60
    .line 61
    .line 62
    move-result p3

    .line 63
    invoke-static {p2, p3}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇o0O([JZ)Z

    .line 64
    .line 65
    .line 66
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask;->o〇0(Ljava/lang/String;)Z

    .line 67
    .line 68
    .line 69
    move-result p1

    .line 70
    if-eqz p1, :cond_2

    .line 71
    .line 72
    const-string p1, "query property, isBuyFreeAd"

    .line 73
    .line 74
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    invoke-static {}, Lcom/intsig/comm/account_data/AccountPreference;->Oo8Oo00oo()Z

    .line 78
    .line 79
    .line 80
    move-result p1

    .line 81
    if-eqz p1, :cond_3

    .line 82
    .line 83
    return v1

    .line 84
    :cond_2
    const-string p1, "query property, isBuy normal Vip"

    .line 85
    .line 86
    invoke-static {v2, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    .line 88
    .line 89
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->Ooo8()Z

    .line 90
    .line 91
    .line 92
    move-result p1

    .line 93
    if-nez p1, :cond_4

    .line 94
    .line 95
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇008〇oo()Z

    .line 96
    .line 97
    .line 98
    move-result p1

    .line 99
    if-eqz p1, :cond_3

    .line 100
    .line 101
    goto :goto_1

    .line 102
    :cond_3
    const/4 p1, 0x0

    .line 103
    return p1

    .line 104
    :cond_4
    :goto_1
    return v1
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method


# virtual methods
.method public 〇080(Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 16
    .param p1    # Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/coroutines/Continuation;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    move-object/from16 v0, p2

    .line 4
    .line 5
    const-string v2, "PurchaseHelper-PayApi"

    .line 6
    .line 7
    instance-of v3, v0, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$chain$1;

    .line 8
    .line 9
    if-eqz v3, :cond_0

    .line 10
    .line 11
    move-object v3, v0

    .line 12
    check-cast v3, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$chain$1;

    .line 13
    .line 14
    iget v4, v3, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$chain$1;->〇0O:I

    .line 15
    .line 16
    const/high16 v5, -0x80000000

    .line 17
    .line 18
    and-int v6, v4, v5

    .line 19
    .line 20
    if-eqz v6, :cond_0

    .line 21
    .line 22
    sub-int/2addr v4, v5

    .line 23
    iput v4, v3, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$chain$1;->〇0O:I

    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_0
    new-instance v3, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$chain$1;

    .line 27
    .line 28
    invoke-direct {v3, v1, v0}, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$chain$1;-><init>(Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask;Lkotlin/coroutines/Continuation;)V

    .line 29
    .line 30
    .line 31
    :goto_0
    iget-object v0, v3, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$chain$1;->O8o08O8O:Ljava/lang/Object;

    .line 32
    .line 33
    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->O8()Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object v4

    .line 37
    iget v5, v3, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$chain$1;->〇0O:I

    .line 38
    .line 39
    const-string v6, "propertyId"

    .line 40
    .line 41
    const-string v8, "PurchaseHelper-StripeQueryVipTask"

    .line 42
    .line 43
    const/4 v9, 0x1

    .line 44
    if-eqz v5, :cond_2

    .line 45
    .line 46
    if-ne v5, v9, :cond_1

    .line 47
    .line 48
    iget-object v5, v3, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$chain$1;->o〇00O:Ljava/lang/Object;

    .line 49
    .line 50
    check-cast v5, Ljava/lang/String;

    .line 51
    .line 52
    iget-object v10, v3, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$chain$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 53
    .line 54
    check-cast v10, Ljava/util/HashMap;

    .line 55
    .line 56
    iget-object v11, v3, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$chain$1;->OO:Ljava/lang/Object;

    .line 57
    .line 58
    check-cast v11, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;

    .line 59
    .line 60
    iget-object v12, v3, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 61
    .line 62
    check-cast v12, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;

    .line 63
    .line 64
    iget-object v13, v3, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$chain$1;->o0:Ljava/lang/Object;

    .line 65
    .line 66
    check-cast v13, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask;

    .line 67
    .line 68
    invoke-static {v0}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 69
    .line 70
    .line 71
    move-object v7, v5

    .line 72
    move-object v5, v4

    .line 73
    move-object v4, v3

    .line 74
    move-object v3, v11

    .line 75
    move-object v11, v10

    .line 76
    move-object v10, v13

    .line 77
    goto/16 :goto_9

    .line 78
    .line 79
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 80
    .line 81
    const-string v2, "call to \'resume\' before \'invoke\' with coroutine"

    .line 82
    .line 83
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    throw v0

    .line 87
    :cond_2
    invoke-static {v0}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 88
    .line 89
    .line 90
    const-string/jumbo v0, "start stripe query vip"

    .line 91
    .line 92
    .line 93
    invoke-static {v8, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    .line 95
    .line 96
    invoke-static {}, Lcom/intsig/huaweipaylib/HuaweiPayConfig;->〇o00〇〇Oo()Z

    .line 97
    .line 98
    .line 99
    move-result v0

    .line 100
    if-eqz v0, :cond_3

    .line 101
    .line 102
    sget-object v0, Lcom/intsig/camscanner/app/AppSwitch;->〇O〇:Ljava/lang/String;

    .line 103
    .line 104
    goto :goto_1

    .line 105
    :cond_3
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 106
    .line 107
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 108
    .line 109
    .line 110
    move-result-object v0

    .line 111
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O〇oO〇oo8o(Landroid/content/Context;)Z

    .line 112
    .line 113
    .line 114
    move-result v0

    .line 115
    if-eqz v0, :cond_4

    .line 116
    .line 117
    const-string v0, "Android_Edu"

    .line 118
    .line 119
    goto :goto_1

    .line 120
    :cond_4
    sget-object v0, Lcom/intsig/camscanner/launch/CsApplication;->〇08O〇00〇o:Lcom/intsig/camscanner/launch/CsApplication$Companion;

    .line 121
    .line 122
    invoke-virtual {v0}, Lcom/intsig/camscanner/launch/CsApplication$Companion;->〇O888o0o()Z

    .line 123
    .line 124
    .line 125
    move-result v0

    .line 126
    if-eqz v0, :cond_5

    .line 127
    .line 128
    const-string v0, "Android_License"

    .line 129
    .line 130
    goto :goto_1

    .line 131
    :cond_5
    const-string v0, ""

    .line 132
    .line 133
    :goto_1
    new-instance v5, Ljava/util/HashMap;

    .line 134
    .line 135
    const/16 v10, 0xc

    .line 136
    .line 137
    invoke-direct {v5, v10}, Ljava/util/HashMap;-><init>(I)V

    .line 138
    .line 139
    .line 140
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->OOo0O()Ljava/lang/String;

    .line 141
    .line 142
    .line 143
    move-result-object v10

    .line 144
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0o()Ljava/lang/String;

    .line 145
    .line 146
    .line 147
    move-result-object v11

    .line 148
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇〇888()Ljava/lang/String;

    .line 149
    .line 150
    .line 151
    move-result-object v12

    .line 152
    if-eqz v10, :cond_7

    .line 153
    .line 154
    invoke-interface {v10}, Ljava/lang/CharSequence;->length()I

    .line 155
    .line 156
    .line 157
    move-result v13

    .line 158
    if-nez v13, :cond_6

    .line 159
    .line 160
    goto :goto_2

    .line 161
    :cond_6
    const/4 v13, 0x0

    .line 162
    goto :goto_3

    .line 163
    :cond_7
    :goto_2
    const/4 v13, 0x1

    .line 164
    :goto_3
    const-string v14, "cs_ept_d"

    .line 165
    .line 166
    if-eqz v13, :cond_8

    .line 167
    .line 168
    invoke-interface {v5, v14, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    .line 170
    .line 171
    goto :goto_4

    .line 172
    :cond_8
    sget-object v13, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 173
    .line 174
    invoke-virtual {v13}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 175
    .line 176
    .line 177
    move-result-object v13

    .line 178
    invoke-static {v13}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o08oOO(Landroid/content/Context;)Z

    .line 179
    .line 180
    .line 181
    move-result v13

    .line 182
    const-string/jumbo v15, "user_id"

    .line 183
    .line 184
    .line 185
    const-string/jumbo v7, "token"

    .line 186
    .line 187
    .line 188
    if-eqz v13, :cond_9

    .line 189
    .line 190
    invoke-static {v10, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 191
    .line 192
    .line 193
    invoke-interface {v5, v7, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    .line 195
    .line 196
    const-string/jumbo v7, "userId"

    .line 197
    .line 198
    .line 199
    invoke-static {v11, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 200
    .line 201
    .line 202
    invoke-interface {v5, v15, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    .line 204
    .line 205
    invoke-interface {v5, v14, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    .line 207
    .line 208
    goto :goto_4

    .line 209
    :cond_9
    invoke-static {v10, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 210
    .line 211
    .line 212
    invoke-interface {v5, v7, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    .line 214
    .line 215
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0o()Ljava/lang/String;

    .line 216
    .line 217
    .line 218
    move-result-object v7

    .line 219
    const-string v10, "getSyncAccountUID()"

    .line 220
    .line 221
    invoke-static {v7, v10}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 222
    .line 223
    .line 224
    invoke-interface {v5, v15, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 225
    .line 226
    .line 227
    invoke-interface {v5, v14, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    .line 229
    .line 230
    :goto_4
    if-eqz v0, :cond_b

    .line 231
    .line 232
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 233
    .line 234
    .line 235
    move-result v7

    .line 236
    if-nez v7, :cond_a

    .line 237
    .line 238
    goto :goto_5

    .line 239
    :cond_a
    const/4 v7, 0x0

    .line 240
    goto :goto_6

    .line 241
    :cond_b
    :goto_5
    const/4 v7, 0x1

    .line 242
    :goto_6
    if-nez v7, :cond_c

    .line 243
    .line 244
    const-string v7, "payVersion"

    .line 245
    .line 246
    invoke-static {v0, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 247
    .line 248
    .line 249
    const-string v7, "app_type"

    .line 250
    .line 251
    invoke-interface {v5, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    .line 253
    .line 254
    :cond_c
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 255
    .line 256
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 257
    .line 258
    .line 259
    move-result-object v0

    .line 260
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o0O0(Landroid/content/Context;)Ljava/lang/String;

    .line 261
    .line 262
    .line 263
    move-result-object v0

    .line 264
    const-string v7, "getClientApp(ApplicationHelper.sContext)"

    .line 265
    .line 266
    invoke-static {v0, v7}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 267
    .line 268
    .line 269
    const-string v7, "client_app"

    .line 270
    .line 271
    invoke-interface {v5, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    .line 273
    .line 274
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇〇888()Lcom/intsig/comm/purchase/entity/ProductResultItem;

    .line 275
    .line 276
    .line 277
    move-result-object v0

    .line 278
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/ProductResultItem;->propertyId:Ljava/lang/String;

    .line 279
    .line 280
    if-eqz v0, :cond_e

    .line 281
    .line 282
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 283
    .line 284
    .line 285
    move-result v7

    .line 286
    if-nez v7, :cond_d

    .line 287
    .line 288
    goto :goto_7

    .line 289
    :cond_d
    const/4 v7, 0x0

    .line 290
    goto :goto_8

    .line 291
    :cond_e
    :goto_7
    const/4 v7, 0x1

    .line 292
    :goto_8
    if-nez v7, :cond_10

    .line 293
    .line 294
    const-string v7, "CamScanner_VIP"

    .line 295
    .line 296
    invoke-static {v7, v0, v9}, Lkotlin/text/StringsKt;->〇0〇O0088o(Ljava/lang/String;Ljava/lang/String;Z)Z

    .line 297
    .line 298
    .line 299
    move-result v7

    .line 300
    if-eqz v7, :cond_f

    .line 301
    .line 302
    new-instance v0, Ljava/lang/StringBuilder;

    .line 303
    .line 304
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 305
    .line 306
    .line 307
    const-string v7, "cs_vip"

    .line 308
    .line 309
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 310
    .line 311
    .line 312
    const-string/jumbo v7, "|removead"

    .line 313
    .line 314
    .line 315
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 316
    .line 317
    .line 318
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 319
    .line 320
    .line 321
    move-result-object v0

    .line 322
    :cond_f
    invoke-static {v0, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 323
    .line 324
    .line 325
    const-string v7, "property_id"

    .line 326
    .line 327
    invoke-interface {v5, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 328
    .line 329
    .line 330
    :cond_10
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->O880oOO08()I

    .line 331
    .line 332
    .line 333
    move-result v7

    .line 334
    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 335
    .line 336
    .line 337
    move-result-object v7

    .line 338
    const-string/jumbo v10, "time_zone"

    .line 339
    .line 340
    .line 341
    invoke-interface {v5, v10, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 342
    .line 343
    .line 344
    invoke-static {}, Lcom/intsig/utils/LanguageUtil;->O8()Ljava/lang/String;

    .line 345
    .line 346
    .line 347
    move-result-object v7

    .line 348
    const-string v10, "getLocalCountry()"

    .line 349
    .line 350
    invoke-static {v7, v10}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 351
    .line 352
    .line 353
    const-string v10, "country"

    .line 354
    .line 355
    invoke-interface {v5, v10, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    .line 357
    .line 358
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O〇0()Ljava/lang/String;

    .line 359
    .line 360
    .line 361
    move-result-object v7

    .line 362
    const-string v10, "getLocalLang()"

    .line 363
    .line 364
    invoke-static {v7, v10}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 365
    .line 366
    .line 367
    const-string v10, "language"

    .line 368
    .line 369
    invoke-interface {v5, v10, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 370
    .line 371
    .line 372
    const/4 v7, 0x0

    .line 373
    iput v7, v1, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask;->〇o〇:I

    .line 374
    .line 375
    move-object/from16 v12, p1

    .line 376
    .line 377
    move-object v7, v0

    .line 378
    move-object v10, v1

    .line 379
    move-object v11, v5

    .line 380
    move-object v5, v4

    .line 381
    move-object v4, v3

    .line 382
    move-object v3, v12

    .line 383
    :cond_11
    :goto_9
    iget v0, v10, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask;->〇o〇:I

    .line 384
    .line 385
    const/16 v13, 0xa

    .line 386
    .line 387
    if-gt v0, v13, :cond_14

    .line 388
    .line 389
    sget-object v0, Lcom/intsig/camscanner/purchase/pay/task/PayApi;->〇080:Lcom/intsig/camscanner/purchase/pay/task/PayApi;

    .line 390
    .line 391
    const-string v13, "/query_property"

    .line 392
    .line 393
    invoke-virtual {v0, v13}, Lcom/intsig/camscanner/purchase/pay/task/PayApi;->〇o00〇〇Oo(Ljava/lang/String;)Ljava/lang/String;

    .line 394
    .line 395
    .line 396
    move-result-object v0

    .line 397
    const/4 v13, 0x0

    .line 398
    :try_start_0
    invoke-static {v0}, Lcom/lzy/okgo/OkGo;->get(Ljava/lang/String;)Lcom/lzy/okgo/request/GetRequest;

    .line 399
    .line 400
    .line 401
    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 402
    const/4 v14, 0x0

    .line 403
    :try_start_1
    new-array v15, v14, [Z

    .line 404
    .line 405
    invoke-virtual {v0, v11, v15}, Lcom/lzy/okgo/request/base/Request;->params(Ljava/util/Map;[Z)Lcom/lzy/okgo/request/base/Request;

    .line 406
    .line 407
    .line 408
    move-result-object v0

    .line 409
    check-cast v0, Lcom/lzy/okgo/request/GetRequest;

    .line 410
    .line 411
    invoke-virtual {v0}, Lcom/lzy/okgo/request/base/Request;->execute()Lokhttp3/Response;

    .line 412
    .line 413
    .line 414
    move-result-object v0

    .line 415
    const-string v15, "response"

    .line 416
    .line 417
    invoke-static {v0, v15}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 418
    .line 419
    .line 420
    :try_start_2
    invoke-virtual {v0}, Lokhttp3/Response;->Oo08()Lokhttp3/ResponseBody;

    .line 421
    .line 422
    .line 423
    move-result-object v15

    .line 424
    invoke-virtual {v0}, Lokhttp3/Response;->〇oo〇()Z

    .line 425
    .line 426
    .line 427
    move-result v0

    .line 428
    if-eqz v0, :cond_12

    .line 429
    .line 430
    if-eqz v15, :cond_12

    .line 431
    .line 432
    invoke-virtual {v15}, Lokhttp3/ResponseBody;->charStream()Ljava/io/Reader;

    .line 433
    .line 434
    .line 435
    move-result-object v0

    .line 436
    new-instance v15, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$chain$lambda$0$$inlined$getRequest$default$1;

    .line 437
    .line 438
    invoke-direct {v15}, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$chain$lambda$0$$inlined$getRequest$default$1;-><init>()V

    .line 439
    .line 440
    .line 441
    invoke-virtual {v15}, Lcom/google/gson/reflect/TypeToken;->getType()Ljava/lang/reflect/Type;

    .line 442
    .line 443
    .line 444
    move-result-object v15

    .line 445
    invoke-static {v0, v15}, Lcom/intsig/okgo/utils/GsonUtils;->〇o〇(Ljava/io/Reader;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    .line 446
    .line 447
    .line 448
    move-result-object v13
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 449
    goto :goto_b

    .line 450
    :catch_0
    move-exception v0

    .line 451
    :try_start_3
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    .line 452
    .line 453
    .line 454
    goto :goto_b

    .line 455
    :catch_1
    move-exception v0

    .line 456
    goto :goto_a

    .line 457
    :catch_2
    move-exception v0

    .line 458
    const/4 v14, 0x0

    .line 459
    :goto_a
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 460
    .line 461
    .line 462
    :cond_12
    :goto_b
    check-cast v13, Lcom/intsig/camscanner/https/entity/CSQueryProperty;

    .line 463
    .line 464
    if-eqz v13, :cond_13

    .line 465
    .line 466
    :try_start_4
    invoke-static {v13}, Lcom/intsig/okgo/utils/GsonUtils;->Oo08(Ljava/lang/Object;)Ljava/lang/String;

    .line 467
    .line 468
    .line 469
    move-result-object v0

    .line 470
    new-instance v15, Ljava/lang/StringBuilder;

    .line 471
    .line 472
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 473
    .line 474
    .line 475
    const-string v14, "queryProperty propertyId: "

    .line 476
    .line 477
    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 478
    .line 479
    .line 480
    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 481
    .line 482
    .line 483
    const-string v14, "  | result: "

    .line 484
    .line 485
    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 486
    .line 487
    .line 488
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 489
    .line 490
    .line 491
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 492
    .line 493
    .line 494
    move-result-object v0

    .line 495
    invoke-static {v8, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_3

    .line 496
    .line 497
    .line 498
    goto :goto_c

    .line 499
    :catch_3
    move-exception v0

    .line 500
    const-string v14, "queryProperty result"

    .line 501
    .line 502
    invoke-static {v8, v14, v0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 503
    .line 504
    .line 505
    :goto_c
    const/16 v0, 0xc8

    .line 506
    .line 507
    iput v0, v13, Lcom/intsig/camscanner/https/entity/CSQueryProperty;->errorCode:I

    .line 508
    .line 509
    invoke-virtual {v3}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇〇888()Lcom/intsig/comm/purchase/entity/ProductResultItem;

    .line 510
    .line 511
    .line 512
    move-result-object v0

    .line 513
    invoke-static {v7, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 514
    .line 515
    .line 516
    invoke-direct {v10, v0, v7, v13}, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask;->〇〇888(Lcom/intsig/comm/purchase/entity/ProductResultItem;Ljava/lang/String;Lcom/intsig/camscanner/https/entity/CSQueryProperty;)Z

    .line 517
    .line 518
    .line 519
    move-result v0

    .line 520
    if-eqz v0, :cond_13

    .line 521
    .line 522
    const-string v0, "query property success"

    .line 523
    .line 524
    invoke-static {v8, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    .line 526
    .line 527
    const v11, 0xc35a

    .line 528
    .line 529
    .line 530
    const/4 v13, 0x0

    .line 531
    const/4 v14, 0x4

    .line 532
    const/4 v15, 0x0

    .line 533
    invoke-static/range {v10 .. v15}, Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;->O8(Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;ILcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;Lcom/intsig/pay/base/model/PayOrderResponse;ILjava/lang/Object;)Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;

    .line 534
    .line 535
    .line 536
    move-result-object v0

    .line 537
    return-object v0

    .line 538
    :cond_13
    iget v0, v10, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask;->〇o〇:I

    .line 539
    .line 540
    new-instance v13, Ljava/lang/StringBuilder;

    .line 541
    .line 542
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 543
    .line 544
    .line 545
    const-string v14, "query property, retry delay  = "

    .line 546
    .line 547
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 548
    .line 549
    .line 550
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 551
    .line 552
    .line 553
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 554
    .line 555
    .line 556
    move-result-object v0

    .line 557
    invoke-static {v8, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    .line 559
    .line 560
    iget v0, v10, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask;->〇o〇:I

    .line 561
    .line 562
    add-int/2addr v0, v9

    .line 563
    iput v0, v10, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask;->〇o〇:I

    .line 564
    .line 565
    iput-object v10, v4, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$chain$1;->o0:Ljava/lang/Object;

    .line 566
    .line 567
    iput-object v12, v4, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 568
    .line 569
    iput-object v3, v4, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$chain$1;->OO:Ljava/lang/Object;

    .line 570
    .line 571
    iput-object v11, v4, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$chain$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 572
    .line 573
    iput-object v7, v4, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$chain$1;->o〇00O:Ljava/lang/Object;

    .line 574
    .line 575
    iput v9, v4, Lcom/intsig/camscanner/purchase/pay/task/StripeQueryVipTask$chain$1;->〇0O:I

    .line 576
    .line 577
    const-wide/16 v13, 0xbb8

    .line 578
    .line 579
    invoke-static {v13, v14, v4}, Lkotlinx/coroutines/DelayKt;->〇080(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 580
    .line 581
    .line 582
    move-result-object v0

    .line 583
    if-ne v0, v5, :cond_11

    .line 584
    .line 585
    return-object v5

    .line 586
    :cond_14
    const v11, 0xc35b

    .line 587
    .line 588
    .line 589
    const/4 v13, 0x0

    .line 590
    const/4 v14, 0x4

    .line 591
    const/4 v15, 0x0

    .line 592
    invoke-static/range {v10 .. v15}, Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;->O8(Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;ILcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;Lcom/intsig/pay/base/model/PayOrderResponse;ILjava/lang/Object;)Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;

    .line 593
    .line 594
    .line 595
    move-result-object v0

    .line 596
    return-object v0
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
.end method

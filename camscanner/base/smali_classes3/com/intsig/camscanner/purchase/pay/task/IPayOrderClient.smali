.class public interface abstract Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;
.super Ljava/lang/Object;
.source "IPayOrderClient.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# virtual methods
.method public abstract O8(Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;I)V
    .param p1    # Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract Oo08(ILjava/lang/String;Lcom/intsig/pay/base/model/PayOrderResponse;)Z
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/pay/base/model/PayOrderResponse;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract getAppId()Ljava/lang/String;
.end method

.method public abstract oO80(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract o〇0(Lcom/intsig/pay/base/model/PayOrderRequest;I)V
    .param p1    # Lcom/intsig/pay/base/model/PayOrderRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
.end method

.method public abstract 〇080()Lkotlinx/coroutines/flow/Flow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lkotlinx/coroutines/flow/Flow<",
            "Lcom/intsig/camscanner/purchase/pay/task/entity/OrderResponse;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end method

.method public abstract 〇80〇808〇O(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract 〇o00〇〇Oo(Ljava/util/ArrayList;I)V
    .param p1    # Ljava/util/ArrayList;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation
.end method

.method public abstract 〇o〇(I)V
.end method

.method public abstract 〇〇888(ILjava/lang/String;Ljava/lang/String;)V
.end method

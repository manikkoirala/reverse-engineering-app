.class public final Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;
.super Ljava/lang/Object;
.source "PayResponse.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O8:Ljava/lang/String;

.field private final Oo08:Ljava/lang/String;

.field private final 〇080:I

.field private final 〇o00〇〇Oo:Lcom/intsig/comm/purchase/entity/ProductResultItem;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o〇:Lcom/intsig/pay/base/model/PayOrderResponse;


# direct methods
.method public constructor <init>(ILcom/intsig/comm/purchase/entity/ProductResultItem;Lcom/intsig/pay/base/model/PayOrderResponse;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2    # Lcom/intsig/comm/purchase/entity/ProductResultItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "product"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;->〇080:I

    .line 3
    iput-object p2, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;->〇o00〇〇Oo:Lcom/intsig/comm/purchase/entity/ProductResultItem;

    .line 4
    iput-object p3, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;->〇o〇:Lcom/intsig/pay/base/model/PayOrderResponse;

    .line 5
    iput-object p4, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;->O8:Ljava/lang/String;

    .line 6
    iput-object p5, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;->Oo08:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(ILcom/intsig/comm/purchase/entity/ProductResultItem;Lcom/intsig/pay/base/model/PayOrderResponse;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p7, p6, 0x8

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    move-object v5, v0

    goto :goto_0

    :cond_0
    move-object v5, p4

    :goto_0
    and-int/lit8 p4, p6, 0x10

    if-eqz p4, :cond_1

    move-object v6, v0

    goto :goto_1

    :cond_1
    move-object v6, p5

    :goto_1
    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    .line 7
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;-><init>(ILcom/intsig/comm/purchase/entity/ProductResultItem;Lcom/intsig/pay/base/model/PayOrderResponse;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final 〇080()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;->〇080:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

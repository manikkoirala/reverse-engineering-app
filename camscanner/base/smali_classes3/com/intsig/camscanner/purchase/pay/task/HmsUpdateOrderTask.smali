.class public final Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask;
.super Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;
.source "HmsUpdateOrderTask.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final O8:Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private 〇o〇:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask;->O8:Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private final o〇0(ILjava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/intsig/camscanner/purchase/track/PurchaseTracker;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    const-string p5, "PurchaseHelper-HmsUpdateOrderTask"

    .line 2
    .line 3
    new-instance v0, Ljava/util/HashMap;

    .line 4
    .line 5
    const/16 v1, 0xb

    .line 6
    .line 7
    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 8
    .line 9
    .line 10
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->OOo0O()Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    const/4 v2, 0x0

    .line 15
    const/4 v3, 0x1

    .line 16
    if-eqz v1, :cond_1

    .line 17
    .line 18
    invoke-static {v1}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 19
    .line 20
    .line 21
    move-result v4

    .line 22
    if-eqz v4, :cond_0

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_0
    const/4 v4, 0x0

    .line 26
    goto :goto_1

    .line 27
    :cond_1
    :goto_0
    const/4 v4, 0x1

    .line 28
    :goto_1
    if-eqz v4, :cond_2

    .line 29
    .line 30
    const-string v1, "cs_ept_d"

    .line 31
    .line 32
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇〇888()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v4

    .line 36
    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    .line 38
    .line 39
    goto :goto_2

    .line 40
    :cond_2
    const-string/jumbo v4, "token"

    .line 41
    .line 42
    .line 43
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    invoke-interface {v0, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    .line 48
    .line 49
    :goto_2
    if-eqz p4, :cond_b

    .line 50
    .line 51
    iget-object v1, p4, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 52
    .line 53
    invoke-virtual {v1}, Lcom/intsig/camscanner/purchase/entity/Function;->toTrackerValue()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v1

    .line 57
    if-eqz v1, :cond_4

    .line 58
    .line 59
    invoke-static {v1}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 60
    .line 61
    .line 62
    move-result v4

    .line 63
    if-eqz v4, :cond_3

    .line 64
    .line 65
    goto :goto_3

    .line 66
    :cond_3
    const/4 v4, 0x0

    .line 67
    goto :goto_4

    .line 68
    :cond_4
    :goto_3
    const/4 v4, 0x1

    .line 69
    :goto_4
    if-nez v4, :cond_5

    .line 70
    .line 71
    const-string v4, "payFrom"

    .line 72
    .line 73
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    const-string v4, "pay_from"

    .line 77
    .line 78
    invoke-interface {v0, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    .line 80
    .line 81
    :cond_5
    iget-object v1, p4, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 82
    .line 83
    invoke-virtual {v1}, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->toTrackerValue()Ljava/lang/String;

    .line 84
    .line 85
    .line 86
    move-result-object v1

    .line 87
    if-eqz v1, :cond_7

    .line 88
    .line 89
    invoke-static {v1}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 90
    .line 91
    .line 92
    move-result v4

    .line 93
    if-eqz v4, :cond_6

    .line 94
    .line 95
    goto :goto_5

    .line 96
    :cond_6
    const/4 v4, 0x0

    .line 97
    goto :goto_6

    .line 98
    :cond_7
    :goto_5
    const/4 v4, 0x1

    .line 99
    :goto_6
    if-nez v4, :cond_8

    .line 100
    .line 101
    const-string v4, "payScheme"

    .line 102
    .line 103
    invoke-static {v1, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 104
    .line 105
    .line 106
    const-string v4, "pay_scheme"

    .line 107
    .line 108
    invoke-interface {v0, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    .line 110
    .line 111
    :cond_8
    iget-object p4, p4, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 112
    .line 113
    invoke-virtual {p4}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 114
    .line 115
    .line 116
    move-result-object p4

    .line 117
    if-eqz p4, :cond_a

    .line 118
    .line 119
    invoke-static {p4}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 120
    .line 121
    .line 122
    move-result v1

    .line 123
    if-eqz v1, :cond_9

    .line 124
    .line 125
    goto :goto_7

    .line 126
    :cond_9
    const/4 v1, 0x0

    .line 127
    goto :goto_8

    .line 128
    :cond_a
    :goto_7
    const/4 v1, 0x1

    .line 129
    :goto_8
    if-nez v1, :cond_b

    .line 130
    .line 131
    const-string v1, "payFromPart"

    .line 132
    .line 133
    invoke-static {p4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 134
    .line 135
    .line 136
    const-string v1, "pay_from_part"

    .line 137
    .line 138
    invoke-interface {v0, v1, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    .line 140
    .line 141
    :cond_b
    invoke-static {}, Lcom/intsig/utils/LanguageUtil;->O8()Ljava/lang/String;

    .line 142
    .line 143
    .line 144
    move-result-object p4

    .line 145
    const-string v1, "getLocalCountry()"

    .line 146
    .line 147
    invoke-static {p4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 148
    .line 149
    .line 150
    const-string v1, "country"

    .line 151
    .line 152
    invoke-interface {v0, v1, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    .line 154
    .line 155
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O〇0()Ljava/lang/String;

    .line 156
    .line 157
    .line 158
    move-result-object p4

    .line 159
    const-string v1, "getLocalLang()"

    .line 160
    .line 161
    invoke-static {p4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 162
    .line 163
    .line 164
    const-string v1, "language"

    .line 165
    .line 166
    invoke-interface {v0, v1, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 167
    .line 168
    .line 169
    sget-object p4, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 170
    .line 171
    invoke-virtual {p4}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 172
    .line 173
    .line 174
    move-result-object p4

    .line 175
    invoke-static {p4}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o0O0(Landroid/content/Context;)Ljava/lang/String;

    .line 176
    .line 177
    .line 178
    move-result-object p4

    .line 179
    const-string v1, "getClientApp(ApplicationHelper.sContext)"

    .line 180
    .line 181
    invoke-static {p4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 182
    .line 183
    .line 184
    const-string v1, "client_app"

    .line 185
    .line 186
    invoke-interface {v0, v1, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    .line 188
    .line 189
    new-instance p4, Lorg/json/JSONObject;

    .line 190
    .line 191
    invoke-direct {p4}, Lorg/json/JSONObject;-><init>()V

    .line 192
    .line 193
    .line 194
    :try_start_0
    const-string v1, "inAppPurchaseData"

    .line 195
    .line 196
    invoke-virtual {p4, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 197
    .line 198
    .line 199
    const-string v1, "inAppDataSignature"

    .line 200
    .line 201
    invoke-virtual {p4, v1, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 202
    .line 203
    .line 204
    goto :goto_9

    .line 205
    :catch_0
    move-exception v1

    .line 206
    invoke-static {p5, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 207
    .line 208
    .line 209
    :goto_9
    invoke-virtual {p4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    .line 210
    .line 211
    .line 212
    move-result-object p4

    .line 213
    const-string v1, "bodyObject.toString()"

    .line 214
    .line 215
    invoke-static {p4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 216
    .line 217
    .line 218
    new-instance v1, Ljava/lang/StringBuilder;

    .line 219
    .line 220
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 221
    .line 222
    .line 223
    const-string v4, "inAppPurchaseData = "

    .line 224
    .line 225
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    .line 227
    .line 228
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    .line 230
    .line 231
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 232
    .line 233
    .line 234
    move-result-object p2

    .line 235
    invoke-static {p5, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    .line 237
    .line 238
    new-instance p2, Ljava/lang/StringBuilder;

    .line 239
    .line 240
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 241
    .line 242
    .line 243
    const-string v1, "inAppDataSignature = "

    .line 244
    .line 245
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 246
    .line 247
    .line 248
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    .line 250
    .line 251
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 252
    .line 253
    .line 254
    move-result-object p2

    .line 255
    invoke-static {p5, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    .line 257
    .line 258
    new-instance p2, Ljava/lang/StringBuilder;

    .line 259
    .line 260
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 261
    .line 262
    .line 263
    const-string p3, "RequestBody = "

    .line 264
    .line 265
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    .line 267
    .line 268
    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    .line 270
    .line 271
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 272
    .line 273
    .line 274
    move-result-object p2

    .line 275
    invoke-static {p5, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    .line 277
    .line 278
    sget-object p2, Lcom/intsig/pay/base/utils/PayTypeUtils;->〇080:Lcom/intsig/pay/base/utils/PayTypeUtils$Companion;

    .line 279
    .line 280
    invoke-virtual {p2, p1}, Lcom/intsig/pay/base/utils/PayTypeUtils$Companion;->〇〇888(I)Z

    .line 281
    .line 282
    .line 283
    move-result p1

    .line 284
    if-eqz p1, :cond_c

    .line 285
    .line 286
    const-string p1, "/update_vip_huawei"

    .line 287
    .line 288
    goto :goto_a

    .line 289
    :cond_c
    const-string p1, "/pay/huawei"

    .line 290
    .line 291
    :goto_a
    sget-object p2, Lcom/intsig/camscanner/purchase/pay/task/PayApi;->〇080:Lcom/intsig/camscanner/purchase/pay/task/PayApi;

    .line 292
    .line 293
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/purchase/pay/task/PayApi;->〇o00〇〇Oo(Ljava/lang/String;)Ljava/lang/String;

    .line 294
    .line 295
    .line 296
    move-result-object p2

    .line 297
    const/4 p3, 0x0

    .line 298
    :try_start_1
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    .line 299
    .line 300
    .line 301
    move-result p5

    .line 302
    if-nez p5, :cond_d

    .line 303
    .line 304
    invoke-static {p2}, Lcom/lzy/okgo/OkGo;->post(Ljava/lang/String;)Lcom/lzy/okgo/request/PostRequest;

    .line 305
    .line 306
    .line 307
    move-result-object p2

    .line 308
    invoke-virtual {p2, v3}, Lcom/lzy/okgo/request/base/BodyRequest;->isSpliceUrl(Z)Lcom/lzy/okgo/request/base/BodyRequest;

    .line 309
    .line 310
    .line 311
    move-result-object p2

    .line 312
    check-cast p2, Lcom/lzy/okgo/request/PostRequest;

    .line 313
    .line 314
    new-array p5, v2, [Z

    .line 315
    .line 316
    invoke-virtual {p2, v0, p5}, Lcom/lzy/okgo/request/base/Request;->params(Ljava/util/Map;[Z)Lcom/lzy/okgo/request/base/Request;

    .line 317
    .line 318
    .line 319
    move-result-object p2

    .line 320
    check-cast p2, Lcom/lzy/okgo/request/PostRequest;

    .line 321
    .line 322
    sget-object p5, Lokhttp3/RequestBody;->Companion:Lokhttp3/RequestBody$Companion;

    .line 323
    .line 324
    invoke-static {p5, p4, p3, v3, p3}, Lokhttp3/RequestBody$Companion;->〇80〇808〇O(Lokhttp3/RequestBody$Companion;Ljava/lang/String;Lokhttp3/MediaType;ILjava/lang/Object;)Lokhttp3/RequestBody;

    .line 325
    .line 326
    .line 327
    move-result-object p4

    .line 328
    invoke-virtual {p2, p4}, Lcom/lzy/okgo/request/base/BodyRequest;->upRequestBody(Lokhttp3/RequestBody;)Lcom/lzy/okgo/request/base/BodyRequest;

    .line 329
    .line 330
    .line 331
    move-result-object p2

    .line 332
    check-cast p2, Lcom/lzy/okgo/request/PostRequest;

    .line 333
    .line 334
    invoke-virtual {p2}, Lcom/lzy/okgo/request/base/Request;->execute()Lokhttp3/Response;

    .line 335
    .line 336
    .line 337
    move-result-object p2

    .line 338
    goto :goto_b

    .line 339
    :cond_d
    invoke-static {p2}, Lcom/lzy/okgo/OkGo;->post(Ljava/lang/String;)Lcom/lzy/okgo/request/PostRequest;

    .line 340
    .line 341
    .line 342
    move-result-object p2

    .line 343
    sget-object p5, Lokhttp3/RequestBody;->Companion:Lokhttp3/RequestBody$Companion;

    .line 344
    .line 345
    invoke-static {p5, p4, p3, v3, p3}, Lokhttp3/RequestBody$Companion;->〇80〇808〇O(Lokhttp3/RequestBody$Companion;Ljava/lang/String;Lokhttp3/MediaType;ILjava/lang/Object;)Lokhttp3/RequestBody;

    .line 346
    .line 347
    .line 348
    move-result-object p4

    .line 349
    invoke-virtual {p2, p4}, Lcom/lzy/okgo/request/base/BodyRequest;->upRequestBody(Lokhttp3/RequestBody;)Lcom/lzy/okgo/request/base/BodyRequest;

    .line 350
    .line 351
    .line 352
    move-result-object p2

    .line 353
    check-cast p2, Lcom/lzy/okgo/request/PostRequest;

    .line 354
    .line 355
    invoke-virtual {p2}, Lcom/lzy/okgo/request/base/Request;->execute()Lokhttp3/Response;

    .line 356
    .line 357
    .line 358
    move-result-object p2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 359
    :goto_b
    move-object p3, p2

    .line 360
    goto :goto_c

    .line 361
    :catch_1
    move-exception p2

    .line 362
    const-string p4, "PurchaseHelper-PayApi"

    .line 363
    .line 364
    invoke-static {p4, p2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 365
    .line 366
    .line 367
    :goto_c
    const/16 p2, 0x194

    .line 368
    .line 369
    if-nez p3, :cond_e

    .line 370
    .line 371
    invoke-static {p2}, Lkotlin/coroutines/jvm/internal/Boxing;->〇o〇(I)Ljava/lang/Integer;

    .line 372
    .line 373
    .line 374
    move-result-object p1

    .line 375
    return-object p1

    .line 376
    :cond_e
    invoke-virtual {p3}, Lokhttp3/Response;->〇oo〇()Z

    .line 377
    .line 378
    .line 379
    move-result p4

    .line 380
    if-eqz p4, :cond_f

    .line 381
    .line 382
    invoke-static {v2}, Lkotlin/coroutines/jvm/internal/Boxing;->〇o〇(I)Ljava/lang/Integer;

    .line 383
    .line 384
    .line 385
    move-result-object p1

    .line 386
    return-object p1

    .line 387
    :cond_f
    sget-object p4, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 388
    .line 389
    invoke-virtual {p4}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 390
    .line 391
    .line 392
    move-result-object p4

    .line 393
    invoke-virtual {p3}, Lokhttp3/Response;->O〇8O8〇008()Ljava/lang/String;

    .line 394
    .line 395
    .line 396
    move-result-object p3

    .line 397
    new-instance p5, Ljava/lang/StringBuilder;

    .line 398
    .line 399
    invoke-direct {p5}, Ljava/lang/StringBuilder;-><init>()V

    .line 400
    .line 401
    .line 402
    const-string/jumbo v0, "updateVipProperty "

    .line 403
    .line 404
    .line 405
    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 406
    .line 407
    .line 408
    invoke-virtual {p5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 409
    .line 410
    .line 411
    const-string p1, " Exception= "

    .line 412
    .line 413
    invoke-virtual {p5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 414
    .line 415
    .line 416
    invoke-virtual {p5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 417
    .line 418
    .line 419
    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 420
    .line 421
    .line 422
    move-result-object p1

    .line 423
    invoke-static {p4, p1}, Lcom/intsig/camscanner/log/LogTrackerUserData;->〇80〇808〇O(Landroid/content/Context;Ljava/lang/String;)V

    .line 424
    .line 425
    .line 426
    invoke-static {p2}, Lkotlin/coroutines/jvm/internal/Boxing;->〇o〇(I)Ljava/lang/Integer;

    .line 427
    .line 428
    .line 429
    move-result-object p1

    .line 430
    return-object p1
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
.end method


# virtual methods
.method public 〇080(Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 24
    .param p1    # Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/coroutines/Continuation;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    move-object/from16 v6, p0

    .line 2
    .line 3
    move-object/from16 v0, p2

    .line 4
    .line 5
    instance-of v1, v0, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;

    .line 6
    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    move-object v1, v0

    .line 10
    check-cast v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;

    .line 11
    .line 12
    iget v2, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->oOo〇8o008:I

    .line 13
    .line 14
    const/high16 v3, -0x80000000

    .line 15
    .line 16
    and-int v4, v2, v3

    .line 17
    .line 18
    if-eqz v4, :cond_0

    .line 19
    .line 20
    sub-int/2addr v2, v3

    .line 21
    iput v2, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->oOo〇8o008:I

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    new-instance v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;

    .line 25
    .line 26
    invoke-direct {v1, v6, v0}, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;-><init>(Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask;Lkotlin/coroutines/Continuation;)V

    .line 27
    .line 28
    .line 29
    :goto_0
    iget-object v0, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->〇080OO8〇0:Ljava/lang/Object;

    .line 30
    .line 31
    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->O8()Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    iget v3, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->oOo〇8o008:I

    .line 36
    .line 37
    const/4 v4, 0x4

    .line 38
    const/4 v5, 0x3

    .line 39
    const/4 v7, 0x2

    .line 40
    const/4 v8, 0x5

    .line 41
    const-string v9, "PurchaseHelper-HmsUpdateOrderTask"

    .line 42
    .line 43
    const/4 v10, 0x1

    .line 44
    if-eqz v3, :cond_6

    .line 45
    .line 46
    if-eq v3, v10, :cond_5

    .line 47
    .line 48
    if-eq v3, v7, :cond_4

    .line 49
    .line 50
    if-eq v3, v5, :cond_3

    .line 51
    .line 52
    if-eq v3, v4, :cond_2

    .line 53
    .line 54
    if-ne v3, v8, :cond_1

    .line 55
    .line 56
    iget v3, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->O8o08O8O:I

    .line 57
    .line 58
    iget-object v12, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->o〇00O:Ljava/lang/Object;

    .line 59
    .line 60
    check-cast v12, Ljava/lang/String;

    .line 61
    .line 62
    iget-object v13, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 63
    .line 64
    check-cast v13, Ljava/lang/String;

    .line 65
    .line 66
    iget-object v14, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->OO:Ljava/lang/Object;

    .line 67
    .line 68
    check-cast v14, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;

    .line 69
    .line 70
    iget-object v15, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 71
    .line 72
    check-cast v15, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;

    .line 73
    .line 74
    iget-object v5, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->o0:Ljava/lang/Object;

    .line 75
    .line 76
    check-cast v5, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask;

    .line 77
    .line 78
    invoke-static {v0}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 79
    .line 80
    .line 81
    move-object v0, v14

    .line 82
    move-object/from16 v22, v2

    .line 83
    .line 84
    move-object v2, v1

    .line 85
    move-object v1, v12

    .line 86
    move v12, v3

    .line 87
    move-object/from16 v3, v22

    .line 88
    .line 89
    move-object/from16 v23, v13

    .line 90
    .line 91
    move-object v13, v5

    .line 92
    move-object/from16 v5, v23

    .line 93
    .line 94
    goto/16 :goto_a

    .line 95
    .line 96
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 97
    .line 98
    const-string v1, "call to \'resume\' before \'invoke\' with coroutine"

    .line 99
    .line 100
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 101
    .line 102
    .line 103
    throw v0

    .line 104
    :cond_2
    iget v3, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->O8o08O8O:I

    .line 105
    .line 106
    iget-object v5, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->o〇00O:Ljava/lang/Object;

    .line 107
    .line 108
    check-cast v5, Ljava/lang/String;

    .line 109
    .line 110
    iget-object v12, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 111
    .line 112
    check-cast v12, Ljava/lang/String;

    .line 113
    .line 114
    iget-object v13, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->OO:Ljava/lang/Object;

    .line 115
    .line 116
    check-cast v13, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;

    .line 117
    .line 118
    iget-object v14, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 119
    .line 120
    check-cast v14, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;

    .line 121
    .line 122
    iget-object v15, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->o0:Ljava/lang/Object;

    .line 123
    .line 124
    check-cast v15, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask;

    .line 125
    .line 126
    invoke-static {v0}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 127
    .line 128
    .line 129
    const/4 v0, 0x4

    .line 130
    move-object/from16 v22, v2

    .line 131
    .line 132
    move-object v2, v1

    .line 133
    move-object v1, v5

    .line 134
    move-object v5, v12

    .line 135
    :goto_1
    move v12, v3

    .line 136
    move-object/from16 v3, v22

    .line 137
    .line 138
    goto/16 :goto_b

    .line 139
    .line 140
    :cond_3
    invoke-static {v0}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 141
    .line 142
    .line 143
    goto/16 :goto_d

    .line 144
    .line 145
    :cond_4
    iget-object v3, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 146
    .line 147
    check-cast v3, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;

    .line 148
    .line 149
    iget-object v4, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->o0:Ljava/lang/Object;

    .line 150
    .line 151
    check-cast v4, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask;

    .line 152
    .line 153
    invoke-static {v0}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 154
    .line 155
    .line 156
    const/4 v0, 0x0

    .line 157
    goto/16 :goto_c

    .line 158
    .line 159
    :cond_5
    iget v3, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->O8o08O8O:I

    .line 160
    .line 161
    iget-object v5, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->o〇00O:Ljava/lang/Object;

    .line 162
    .line 163
    check-cast v5, Ljava/lang/String;

    .line 164
    .line 165
    iget-object v12, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 166
    .line 167
    check-cast v12, Ljava/lang/String;

    .line 168
    .line 169
    iget-object v13, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->OO:Ljava/lang/Object;

    .line 170
    .line 171
    check-cast v13, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;

    .line 172
    .line 173
    iget-object v14, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 174
    .line 175
    check-cast v14, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;

    .line 176
    .line 177
    iget-object v15, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->o0:Ljava/lang/Object;

    .line 178
    .line 179
    check-cast v15, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask;

    .line 180
    .line 181
    invoke-static {v0}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 182
    .line 183
    .line 184
    move-object v7, v12

    .line 185
    move-object v11, v15

    .line 186
    move-object v12, v5

    .line 187
    move-object v5, v13

    .line 188
    move-object v13, v14

    .line 189
    goto/16 :goto_9

    .line 190
    .line 191
    :cond_6
    invoke-static {v0}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 192
    .line 193
    .line 194
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->Oo08()Lcom/intsig/pay/base/model/PayOrderResponse;

    .line 195
    .line 196
    .line 197
    move-result-object v0

    .line 198
    if-eqz v0, :cond_7

    .line 199
    .line 200
    invoke-virtual {v0}, Lcom/intsig/pay/base/model/PayOrderResponse;->getInAppPurchaseData()Ljava/lang/String;

    .line 201
    .line 202
    .line 203
    move-result-object v0

    .line 204
    goto :goto_2

    .line 205
    :cond_7
    const/4 v0, 0x0

    .line 206
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->Oo08()Lcom/intsig/pay/base/model/PayOrderResponse;

    .line 207
    .line 208
    .line 209
    move-result-object v3

    .line 210
    if-eqz v3, :cond_8

    .line 211
    .line 212
    invoke-virtual {v3}, Lcom/intsig/pay/base/model/PayOrderResponse;->getInAppDataSignature()Ljava/lang/String;

    .line 213
    .line 214
    .line 215
    move-result-object v3

    .line 216
    goto :goto_3

    .line 217
    :cond_8
    const/4 v3, 0x0

    .line 218
    :goto_3
    const/4 v5, 0x0

    .line 219
    if-eqz v0, :cond_a

    .line 220
    .line 221
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 222
    .line 223
    .line 224
    move-result v12

    .line 225
    if-eqz v12, :cond_9

    .line 226
    .line 227
    goto :goto_4

    .line 228
    :cond_9
    const/4 v12, 0x0

    .line 229
    goto :goto_5

    .line 230
    :cond_a
    :goto_4
    const/4 v12, 0x1

    .line 231
    :goto_5
    if-nez v12, :cond_19

    .line 232
    .line 233
    if-eqz v3, :cond_c

    .line 234
    .line 235
    invoke-static {v3}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 236
    .line 237
    .line 238
    move-result v12

    .line 239
    if-eqz v12, :cond_b

    .line 240
    .line 241
    goto :goto_6

    .line 242
    :cond_b
    const/4 v12, 0x0

    .line 243
    goto :goto_7

    .line 244
    :cond_c
    :goto_6
    const/4 v12, 0x1

    .line 245
    :goto_7
    if-eqz v12, :cond_d

    .line 246
    .line 247
    goto/16 :goto_e

    .line 248
    .line 249
    :cond_d
    iput v5, v6, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask;->〇o〇:I

    .line 250
    .line 251
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇〇888()Lcom/intsig/comm/purchase/entity/ProductResultItem;

    .line 252
    .line 253
    .line 254
    move-result-object v5

    .line 255
    iget-object v5, v5, Lcom/intsig/comm/purchase/entity/ProductResultItem;->propertyId:Ljava/lang/String;

    .line 256
    .line 257
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇〇888()Lcom/intsig/comm/purchase/entity/ProductResultItem;

    .line 258
    .line 259
    .line 260
    move-result-object v12

    .line 261
    iget v12, v12, Lcom/intsig/comm/purchase/entity/ProductResultItem;->consume:I

    .line 262
    .line 263
    new-instance v13, Ljava/lang/StringBuilder;

    .line 264
    .line 265
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 266
    .line 267
    .line 268
    const-string v14, "product type = "

    .line 269
    .line 270
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    .line 272
    .line 273
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 274
    .line 275
    .line 276
    const-string v14, ", propertyId = "

    .line 277
    .line 278
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    .line 280
    .line 281
    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 282
    .line 283
    .line 284
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 285
    .line 286
    .line 287
    move-result-object v5

    .line 288
    invoke-static {v9, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    .line 290
    .line 291
    move-object/from16 v15, p1

    .line 292
    .line 293
    move-object v5, v3

    .line 294
    move-object v13, v6

    .line 295
    move-object v3, v2

    .line 296
    move-object v2, v1

    .line 297
    move-object v1, v0

    .line 298
    move-object v0, v15

    .line 299
    :goto_8
    iget v14, v13, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask;->〇o〇:I

    .line 300
    .line 301
    const/16 v7, 0xa

    .line 302
    .line 303
    if-gt v14, v7, :cond_18

    .line 304
    .line 305
    new-instance v7, Ljava/lang/StringBuilder;

    .line 306
    .line 307
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 308
    .line 309
    .line 310
    const-string v11, "UPDATE_VIP_PROPERTY retry count = "

    .line 311
    .line 312
    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 313
    .line 314
    .line 315
    invoke-virtual {v7, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 316
    .line 317
    .line 318
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 319
    .line 320
    .line 321
    move-result-object v7

    .line 322
    invoke-static {v9, v7}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    .line 324
    .line 325
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->oO80()Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 326
    .line 327
    .line 328
    move-result-object v20

    .line 329
    iput-object v13, v2, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->o0:Ljava/lang/Object;

    .line 330
    .line 331
    iput-object v15, v2, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 332
    .line 333
    iput-object v0, v2, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->OO:Ljava/lang/Object;

    .line 334
    .line 335
    iput-object v5, v2, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 336
    .line 337
    iput-object v1, v2, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->o〇00O:Ljava/lang/Object;

    .line 338
    .line 339
    iput v12, v2, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->O8o08O8O:I

    .line 340
    .line 341
    iput v10, v2, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->oOo〇8o008:I

    .line 342
    .line 343
    move-object/from16 v16, v13

    .line 344
    .line 345
    move/from16 v17, v12

    .line 346
    .line 347
    move-object/from16 v18, v1

    .line 348
    .line 349
    move-object/from16 v19, v5

    .line 350
    .line 351
    move-object/from16 v21, v2

    .line 352
    .line 353
    invoke-direct/range {v16 .. v21}, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask;->o〇0(ILjava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 354
    .line 355
    .line 356
    move-result-object v7

    .line 357
    if-ne v7, v3, :cond_e

    .line 358
    .line 359
    return-object v3

    .line 360
    :cond_e
    move-object v11, v13

    .line 361
    move-object v13, v15

    .line 362
    move-object/from16 v22, v5

    .line 363
    .line 364
    move-object v5, v0

    .line 365
    move-object v0, v7

    .line 366
    move-object/from16 v7, v22

    .line 367
    .line 368
    move/from16 v23, v12

    .line 369
    .line 370
    move-object v12, v1

    .line 371
    move-object v1, v2

    .line 372
    move-object v2, v3

    .line 373
    move/from16 v3, v23

    .line 374
    .line 375
    :goto_9
    check-cast v0, Ljava/lang/Number;

    .line 376
    .line 377
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 378
    .line 379
    .line 380
    move-result v0

    .line 381
    if-eqz v0, :cond_14

    .line 382
    .line 383
    const/16 v14, 0x1f6

    .line 384
    .line 385
    move-object v15, v5

    .line 386
    if-eq v0, v14, :cond_10

    .line 387
    .line 388
    iget v14, v11, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask;->〇o〇:I

    .line 389
    .line 390
    new-instance v4, Ljava/lang/StringBuilder;

    .line 391
    .line 392
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 393
    .line 394
    .line 395
    const-string v5, "UPDATE_VIP_PROPERTY failed code = "

    .line 396
    .line 397
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 398
    .line 399
    .line 400
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 401
    .line 402
    .line 403
    const-string v0, ", retry delay  = "

    .line 404
    .line 405
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 406
    .line 407
    .line 408
    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 409
    .line 410
    .line 411
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 412
    .line 413
    .line 414
    move-result-object v0

    .line 415
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 416
    .line 417
    .line 418
    iget v0, v11, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask;->〇o〇:I

    .line 419
    .line 420
    add-int/2addr v0, v10

    .line 421
    iput v0, v11, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask;->〇o〇:I

    .line 422
    .line 423
    iput-object v11, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->o0:Ljava/lang/Object;

    .line 424
    .line 425
    iput-object v13, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 426
    .line 427
    move-object v4, v15

    .line 428
    iput-object v4, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->OO:Ljava/lang/Object;

    .line 429
    .line 430
    iput-object v7, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 431
    .line 432
    iput-object v12, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->o〇00O:Ljava/lang/Object;

    .line 433
    .line 434
    iput v3, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->O8o08O8O:I

    .line 435
    .line 436
    iput v8, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->oOo〇8o008:I

    .line 437
    .line 438
    const-wide/16 v14, 0xbb8

    .line 439
    .line 440
    invoke-static {v14, v15, v1}, Lkotlinx/coroutines/DelayKt;->〇080(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 441
    .line 442
    .line 443
    move-result-object v0

    .line 444
    if-ne v0, v2, :cond_f

    .line 445
    .line 446
    return-object v2

    .line 447
    :cond_f
    move-object v0, v4

    .line 448
    move-object v5, v7

    .line 449
    move-object v15, v13

    .line 450
    move-object v13, v11

    .line 451
    move-object/from16 v22, v2

    .line 452
    .line 453
    move-object v2, v1

    .line 454
    move-object v1, v12

    .line 455
    move v12, v3

    .line 456
    move-object/from16 v3, v22

    .line 457
    .line 458
    :goto_a
    const/4 v4, 0x4

    .line 459
    const/4 v7, 0x2

    .line 460
    goto/16 :goto_8

    .line 461
    .line 462
    :cond_10
    move-object v4, v15

    .line 463
    iget v5, v11, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask;->〇o〇:I

    .line 464
    .line 465
    add-int/2addr v5, v10

    .line 466
    iput v5, v11, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask;->〇o〇:I

    .line 467
    .line 468
    if-le v5, v8, :cond_12

    .line 469
    .line 470
    const/16 v1, 0xb

    .line 471
    .line 472
    iput v1, v11, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask;->〇o〇:I

    .line 473
    .line 474
    new-instance v1, Ljava/lang/StringBuilder;

    .line 475
    .line 476
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 477
    .line 478
    .line 479
    const-string v2, "UPDATE_VIP_PROPERTY failed\uff0ccode = "

    .line 480
    .line 481
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 482
    .line 483
    .line 484
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 485
    .line 486
    .line 487
    const-string v0, " consumePurchase"

    .line 488
    .line 489
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 490
    .line 491
    .line 492
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 493
    .line 494
    .line 495
    move-result-object v0

    .line 496
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    .line 498
    .line 499
    invoke-virtual {v4}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇o〇()Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;

    .line 500
    .line 501
    .line 502
    move-result-object v0

    .line 503
    if-eqz v0, :cond_11

    .line 504
    .line 505
    invoke-virtual {v4}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇〇888()Lcom/intsig/comm/purchase/entity/ProductResultItem;

    .line 506
    .line 507
    .line 508
    move-result-object v1

    .line 509
    iget v1, v1, Lcom/intsig/comm/purchase/entity/ProductResultItem;->consume:I

    .line 510
    .line 511
    invoke-interface {v0, v1, v12, v7}, Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;->〇〇888(ILjava/lang/String;Ljava/lang/String;)V

    .line 512
    .line 513
    .line 514
    :cond_11
    const v12, 0x9c4e

    .line 515
    .line 516
    .line 517
    const/4 v14, 0x0

    .line 518
    const/4 v15, 0x4

    .line 519
    const/16 v16, 0x0

    .line 520
    .line 521
    invoke-static/range {v11 .. v16}, Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;->O8(Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;ILcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;Lcom/intsig/pay/base/model/PayOrderResponse;ILjava/lang/Object;)Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;

    .line 522
    .line 523
    .line 524
    move-result-object v0

    .line 525
    return-object v0

    .line 526
    :cond_12
    iput-object v11, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->o0:Ljava/lang/Object;

    .line 527
    .line 528
    iput-object v13, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 529
    .line 530
    iput-object v4, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->OO:Ljava/lang/Object;

    .line 531
    .line 532
    iput-object v7, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 533
    .line 534
    iput-object v12, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->o〇00O:Ljava/lang/Object;

    .line 535
    .line 536
    iput v3, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->O8o08O8O:I

    .line 537
    .line 538
    const/4 v0, 0x4

    .line 539
    iput v0, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->oOo〇8o008:I

    .line 540
    .line 541
    const-wide/16 v14, 0xbb8

    .line 542
    .line 543
    invoke-static {v14, v15, v1}, Lkotlinx/coroutines/DelayKt;->〇080(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 544
    .line 545
    .line 546
    move-result-object v5

    .line 547
    if-ne v5, v2, :cond_13

    .line 548
    .line 549
    return-object v2

    .line 550
    :cond_13
    move-object v5, v7

    .line 551
    move-object v15, v11

    .line 552
    move-object v14, v13

    .line 553
    move-object v13, v4

    .line 554
    move-object/from16 v22, v2

    .line 555
    .line 556
    move-object v2, v1

    .line 557
    move-object v1, v12

    .line 558
    goto/16 :goto_1

    .line 559
    .line 560
    :goto_b
    move-object v0, v13

    .line 561
    move-object v13, v15

    .line 562
    const/4 v4, 0x4

    .line 563
    const/4 v7, 0x2

    .line 564
    move-object v15, v14

    .line 565
    goto/16 :goto_8

    .line 566
    .line 567
    :cond_14
    move-object v4, v5

    .line 568
    iget v0, v11, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask;->〇o〇:I

    .line 569
    .line 570
    new-instance v3, Ljava/lang/StringBuilder;

    .line 571
    .line 572
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 573
    .line 574
    .line 575
    const-string v5, "UPDATE_VIP_PROPERTY success! = "

    .line 576
    .line 577
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 578
    .line 579
    .line 580
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 581
    .line 582
    .line 583
    const-string/jumbo v0, "\uff0cthen delay 2000"

    .line 584
    .line 585
    .line 586
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 587
    .line 588
    .line 589
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 590
    .line 591
    .line 592
    move-result-object v0

    .line 593
    invoke-static {v9, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    .line 595
    .line 596
    invoke-virtual {v4}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇o〇()Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;

    .line 597
    .line 598
    .line 599
    move-result-object v0

    .line 600
    if-eqz v0, :cond_15

    .line 601
    .line 602
    invoke-virtual {v4}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇〇888()Lcom/intsig/comm/purchase/entity/ProductResultItem;

    .line 603
    .line 604
    .line 605
    move-result-object v3

    .line 606
    iget v3, v3, Lcom/intsig/comm/purchase/entity/ProductResultItem;->consume:I

    .line 607
    .line 608
    invoke-interface {v0, v3, v12, v7}, Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;->〇〇888(ILjava/lang/String;Ljava/lang/String;)V

    .line 609
    .line 610
    .line 611
    :cond_15
    iput-object v11, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->o0:Ljava/lang/Object;

    .line 612
    .line 613
    iput-object v13, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 614
    .line 615
    const/4 v0, 0x0

    .line 616
    iput-object v0, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->OO:Ljava/lang/Object;

    .line 617
    .line 618
    iput-object v0, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 619
    .line 620
    iput-object v0, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->o〇00O:Ljava/lang/Object;

    .line 621
    .line 622
    const/4 v3, 0x2

    .line 623
    iput v3, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->oOo〇8o008:I

    .line 624
    .line 625
    const-wide/16 v3, 0x7d0

    .line 626
    .line 627
    invoke-static {v3, v4, v1}, Lkotlinx/coroutines/DelayKt;->〇080(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 628
    .line 629
    .line 630
    move-result-object v3

    .line 631
    if-ne v3, v2, :cond_16

    .line 632
    .line 633
    return-object v2

    .line 634
    :cond_16
    move-object v4, v11

    .line 635
    move-object v3, v13

    .line 636
    :goto_c
    iput-object v0, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->o0:Ljava/lang/Object;

    .line 637
    .line 638
    iput-object v0, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 639
    .line 640
    const/4 v0, 0x3

    .line 641
    iput v0, v1, Lcom/intsig/camscanner/purchase/pay/task/HmsUpdateOrderTask$chain$1;->oOo〇8o008:I

    .line 642
    .line 643
    invoke-virtual {v4, v3, v1}, Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;->〇o00〇〇Oo(Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 644
    .line 645
    .line 646
    move-result-object v0

    .line 647
    if-ne v0, v2, :cond_17

    .line 648
    .line 649
    return-object v2

    .line 650
    :cond_17
    :goto_d
    return-object v0

    .line 651
    :cond_18
    const v14, 0x9c4c

    .line 652
    .line 653
    .line 654
    const/16 v16, 0x0

    .line 655
    .line 656
    const/16 v17, 0x4

    .line 657
    .line 658
    const/16 v18, 0x0

    .line 659
    .line 660
    invoke-static/range {v13 .. v18}, Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;->O8(Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;ILcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;Lcom/intsig/pay/base/model/PayOrderResponse;ILjava/lang/Object;)Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;

    .line 661
    .line 662
    .line 663
    move-result-object v0

    .line 664
    return-object v0

    .line 665
    :cond_19
    :goto_e
    const v1, 0x9c4b

    .line 666
    .line 667
    .line 668
    const/4 v3, 0x0

    .line 669
    const/4 v4, 0x4

    .line 670
    const/4 v5, 0x0

    .line 671
    move-object/from16 v0, p0

    .line 672
    .line 673
    move-object/from16 v2, p1

    .line 674
    .line 675
    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;->O8(Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;ILcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;Lcom/intsig/pay/base/model/PayOrderResponse;ILjava/lang/Object;)Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;

    .line 676
    .line 677
    .line 678
    move-result-object v0

    .line 679
    return-object v0
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
.end method

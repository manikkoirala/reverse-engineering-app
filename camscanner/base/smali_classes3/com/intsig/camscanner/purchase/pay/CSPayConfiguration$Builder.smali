.class public final Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;
.super Ljava/lang/Object;
.source "CSPayConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Builder"
.end annotation


# instance fields
.field private O8:I

.field private Oo08:Ljava/lang/String;

.field private oO80:Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;

.field private o〇0:Ljava/lang/String;

.field private 〇080:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

.field private 〇o00〇〇Oo:Ljava/lang/String;

.field private 〇o〇:I

.field private 〇〇888:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;->〇〇888:Z

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method static bridge synthetic O8(Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;->〇o〇:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic Oo08(Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;->O8:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic oO80(Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;->〇〇888:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic o〇0(Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;->〇o00〇〇Oo:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;->o〇0:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;)Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;->oO80:Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;->Oo08:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic 〇〇888(Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;->〇080:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public OO0o〇〇(I)Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;->O8:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public OO0o〇〇〇〇0(Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;)Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;->oO80:Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public Oooo8o0〇(Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;->〇080:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public 〇80〇808〇O()Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration;
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, p0, v1}, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration;-><init>(Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;LOO8〇O8/〇080;)V

    .line 5
    .line 6
    .line 7
    return-object v0
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇8o8o〇(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;->Oo08:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public 〇O8o08O(I)Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;->〇o〇:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public 〇〇808〇(Z)Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;->〇〇888:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

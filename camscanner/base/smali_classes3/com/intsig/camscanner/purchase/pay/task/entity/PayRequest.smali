.class public final Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;
.super Ljava/lang/Object;
.source "PayRequest.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O8:Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;

.field private OO0o〇〇〇〇0:Ljava/lang/String;

.field private final Oo08:Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;

.field private oO80:Lcom/intsig/pay/base/model/PayOrderRequest;

.field private final o〇0:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

.field private final 〇080:I

.field private 〇80〇808〇O:Lcom/intsig/pay/base/model/PayOrderResponse;

.field private 〇8o8o〇:Ljava/lang/String;

.field private final 〇o00〇〇Oo:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇o〇:Lcom/intsig/comm/purchase/entity/ProductResultItem;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇〇888:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Lcom/intsig/comm/purchase/entity/ProductResultItem;Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;Ljava/lang/String;Lcom/intsig/pay/base/model/PayOrderRequest;Lcom/intsig/pay/base/model/PayOrderResponse;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p3    # Lcom/intsig/comm/purchase/entity/ProductResultItem;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string/jumbo v0, "userId"

    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "product"

    invoke-static {p3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇080:I

    .line 3
    iput-object p2, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇o00〇〇Oo:Ljava/lang/String;

    .line 4
    iput-object p3, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇o〇:Lcom/intsig/comm/purchase/entity/ProductResultItem;

    .line 5
    iput-object p4, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->O8:Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;

    .line 6
    iput-object p5, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->Oo08:Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;

    .line 7
    iput-object p6, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->o〇0:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 8
    iput-object p7, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇〇888:Ljava/lang/String;

    .line 9
    iput-object p8, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->oO80:Lcom/intsig/pay/base/model/PayOrderRequest;

    .line 10
    iput-object p9, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇80〇808〇O:Lcom/intsig/pay/base/model/PayOrderResponse;

    .line 11
    iput-object p10, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 12
    iput-object p11, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇8o8o〇:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(ILjava/lang/String;Lcom/intsig/comm/purchase/entity/ProductResultItem;Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;Ljava/lang/String;Lcom/intsig/pay/base/model/PayOrderRequest;Lcom/intsig/pay/base/model/PayOrderResponse;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 15

    move/from16 v0, p12

    and-int/lit8 v1, v0, 0x10

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move-object v8, v2

    goto :goto_0

    :cond_0
    move-object/from16 v8, p5

    :goto_0
    and-int/lit8 v1, v0, 0x20

    if-eqz v1, :cond_1

    move-object v9, v2

    goto :goto_1

    :cond_1
    move-object/from16 v9, p6

    :goto_1
    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_2

    move-object v10, v2

    goto :goto_2

    :cond_2
    move-object/from16 v10, p7

    :goto_2
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_3

    move-object v11, v2

    goto :goto_3

    :cond_3
    move-object/from16 v11, p8

    :goto_3
    and-int/lit16 v1, v0, 0x100

    if-eqz v1, :cond_4

    move-object v12, v2

    goto :goto_4

    :cond_4
    move-object/from16 v12, p9

    :goto_4
    and-int/lit16 v1, v0, 0x200

    if-eqz v1, :cond_5

    move-object v13, v2

    goto :goto_5

    :cond_5
    move-object/from16 v13, p10

    :goto_5
    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_6

    move-object v14, v2

    goto :goto_6

    :cond_6
    move-object/from16 v14, p11

    :goto_6
    move-object v3, p0

    move/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    .line 13
    invoke-direct/range {v3 .. v14}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;-><init>(ILjava/lang/String;Lcom/intsig/comm/purchase/entity/ProductResultItem;Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;Ljava/lang/String;Lcom/intsig/pay/base/model/PayOrderRequest;Lcom/intsig/pay/base/model/PayOrderResponse;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final O8()Lcom/intsig/pay/base/model/PayOrderRequest;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->oO80:Lcom/intsig/pay/base/model/PayOrderRequest;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final OO0o〇〇(Lcom/intsig/pay/base/model/PayOrderRequest;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->oO80:Lcom/intsig/pay/base/model/PayOrderRequest;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final OO0o〇〇〇〇0()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇〇888:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final Oo08()Lcom/intsig/pay/base/model/PayOrderResponse;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇80〇808〇O:Lcom/intsig/pay/base/model/PayOrderResponse;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final Oooo8o0〇(Lcom/intsig/pay/base/model/PayOrderResponse;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇80〇808〇O:Lcom/intsig/pay/base/model/PayOrderResponse;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final oO80()Lcom/intsig/camscanner/purchase/track/PurchaseTracker;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->o〇0:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final o〇0()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇080:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final 〇080()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final 〇80〇808〇O()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇8o8o〇:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final 〇8o8o〇()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇o00〇〇Oo:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final 〇O8o08O(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->OO0o〇〇〇〇0:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final 〇O〇(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇〇888:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final 〇o00〇〇Oo()Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->Oo08:Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final 〇o〇()Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->O8:Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final 〇〇808〇(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇8o8o〇:Ljava/lang/String;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final 〇〇888()Lcom/intsig/comm/purchase/entity/ProductResultItem;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇o〇:Lcom/intsig/comm/purchase/entity/ProductResultItem;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

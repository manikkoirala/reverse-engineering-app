.class public final Lcom/intsig/camscanner/purchase/pay/task/entity/OrderResponse;
.super Ljava/lang/Object;
.source "OrderResponse.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private final O8:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/pay/base/model/PayOrderResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final Oo08:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/pay/base/model/ProductDetailsData;",
            ">;"
        }
    .end annotation
.end field

.field private final 〇080:I

.field private final 〇o00〇〇Oo:I

.field private final 〇o〇:Lcom/intsig/pay/base/model/PayOrderResponse;


# direct methods
.method public constructor <init>(IILcom/intsig/pay/base/model/PayOrderResponse;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Lcom/intsig/pay/base/model/PayOrderResponse;",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/pay/base/model/PayOrderResponse;",
            ">;",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/pay/base/model/ProductDetailsData;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/OrderResponse;->〇080:I

    .line 3
    iput p2, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/OrderResponse;->〇o00〇〇Oo:I

    .line 4
    iput-object p3, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/OrderResponse;->〇o〇:Lcom/intsig/pay/base/model/PayOrderResponse;

    .line 5
    iput-object p4, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/OrderResponse;->O8:Ljava/util/ArrayList;

    .line 6
    iput-object p5, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/OrderResponse;->Oo08:Ljava/util/ArrayList;

    return-void
.end method

.method public synthetic constructor <init>(IILcom/intsig/pay/base/model/PayOrderResponse;Ljava/util/ArrayList;Ljava/util/ArrayList;ILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p7, p6, 0x4

    const/4 v0, 0x0

    if-eqz p7, :cond_0

    move-object v4, v0

    goto :goto_0

    :cond_0
    move-object v4, p3

    :goto_0
    and-int/lit8 p3, p6, 0x8

    if-eqz p3, :cond_1

    move-object v5, v0

    goto :goto_1

    :cond_1
    move-object v5, p4

    :goto_1
    and-int/lit8 p3, p6, 0x10

    if-eqz p3, :cond_2

    move-object v6, v0

    goto :goto_2

    :cond_2
    move-object v6, p5

    :goto_2
    move-object v1, p0

    move v2, p1

    move v3, p2

    .line 7
    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/purchase/pay/task/entity/OrderResponse;-><init>(IILcom/intsig/pay/base/model/PayOrderResponse;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    return-void
.end method


# virtual methods
.method public final O8()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/pay/base/model/PayOrderResponse;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/OrderResponse;->O8:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final Oo08()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/OrderResponse;->〇080:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final 〇080()Lcom/intsig/pay/base/model/PayOrderResponse;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/OrderResponse;->〇o〇:Lcom/intsig/pay/base/model/PayOrderResponse;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final 〇o00〇〇Oo()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/pay/base/model/ProductDetailsData;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/OrderResponse;->Oo08:Ljava/util/ArrayList;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final 〇o〇()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/purchase/pay/task/entity/OrderResponse;->〇o00〇〇Oo:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

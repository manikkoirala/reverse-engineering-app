.class public final Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask;
.super Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;
.source "CreateOrderTask.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final O8:Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private 〇o〇:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask;->O8:Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private final oO80(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lcom/intsig/comm/purchase/entity/CreateOrderResult;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    const-string v0, "PurchaseHelper-PayApi"

    .line 2
    .line 3
    instance-of v1, p2, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$createOrder$1;

    .line 4
    .line 5
    if-eqz v1, :cond_0

    .line 6
    .line 7
    move-object v1, p2

    .line 8
    check-cast v1, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$createOrder$1;

    .line 9
    .line 10
    iget v2, v1, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$createOrder$1;->o〇00O:I

    .line 11
    .line 12
    const/high16 v3, -0x80000000

    .line 13
    .line 14
    and-int v4, v2, v3

    .line 15
    .line 16
    if-eqz v4, :cond_0

    .line 17
    .line 18
    sub-int/2addr v2, v3

    .line 19
    iput v2, v1, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$createOrder$1;->o〇00O:I

    .line 20
    .line 21
    goto :goto_0

    .line 22
    :cond_0
    new-instance v1, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$createOrder$1;

    .line 23
    .line 24
    invoke-direct {v1, p0, p2}, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$createOrder$1;-><init>(Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask;Lkotlin/coroutines/Continuation;)V

    .line 25
    .line 26
    .line 27
    :goto_0
    iget-object p2, v1, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$createOrder$1;->OO:Ljava/lang/Object;

    .line 28
    .line 29
    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->O8()Ljava/lang/Object;

    .line 30
    .line 31
    .line 32
    move-result-object v2

    .line 33
    iget v3, v1, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$createOrder$1;->o〇00O:I

    .line 34
    .line 35
    const/4 v4, 0x2

    .line 36
    const-string v5, "PurchaseHelper-CreateOrderTask"

    .line 37
    .line 38
    const/4 v6, 0x1

    .line 39
    const/4 v7, 0x0

    .line 40
    if-eqz v3, :cond_3

    .line 41
    .line 42
    if-eq v3, v6, :cond_2

    .line 43
    .line 44
    if-ne v3, v4, :cond_1

    .line 45
    .line 46
    invoke-static {p2}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 47
    .line 48
    .line 49
    goto/16 :goto_3

    .line 50
    .line 51
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 52
    .line 53
    const-string p2, "call to \'resume\' before \'invoke\' with coroutine"

    .line 54
    .line 55
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 56
    .line 57
    .line 58
    throw p1

    .line 59
    :cond_2
    iget-object p1, v1, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$createOrder$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 60
    .line 61
    check-cast p1, Ljava/lang/String;

    .line 62
    .line 63
    iget-object v0, v1, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$createOrder$1;->o0:Ljava/lang/Object;

    .line 64
    .line 65
    check-cast v0, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask;

    .line 66
    .line 67
    invoke-static {p2}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 68
    .line 69
    .line 70
    goto/16 :goto_2

    .line 71
    .line 72
    :cond_3
    invoke-static {p2}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 73
    .line 74
    .line 75
    sget-object p2, Lcom/intsig/camscanner/purchase/pay/task/PayApi;->〇080:Lcom/intsig/camscanner/purchase/pay/task/PayApi;

    .line 76
    .line 77
    const-string v3, "/pay/create_order"

    .line 78
    .line 79
    invoke-virtual {p2, v3}, Lcom/intsig/camscanner/purchase/pay/task/PayApi;->〇o00〇〇Oo(Ljava/lang/String;)Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object p2

    .line 83
    :try_start_0
    invoke-static {p2}, Lcom/lzy/okgo/OkGo;->post(Ljava/lang/String;)Lcom/lzy/okgo/request/PostRequest;

    .line 84
    .line 85
    .line 86
    move-result-object p2

    .line 87
    sget-object v3, Lokhttp3/RequestBody;->Companion:Lokhttp3/RequestBody$Companion;

    .line 88
    .line 89
    invoke-static {v3, p1, v7, v6, v7}, Lokhttp3/RequestBody$Companion;->〇80〇808〇O(Lokhttp3/RequestBody$Companion;Ljava/lang/String;Lokhttp3/MediaType;ILjava/lang/Object;)Lokhttp3/RequestBody;

    .line 90
    .line 91
    .line 92
    move-result-object v3

    .line 93
    invoke-virtual {p2, v3}, Lcom/lzy/okgo/request/base/BodyRequest;->upRequestBody(Lokhttp3/RequestBody;)Lcom/lzy/okgo/request/base/BodyRequest;

    .line 94
    .line 95
    .line 96
    move-result-object p2

    .line 97
    check-cast p2, Lcom/lzy/okgo/request/PostRequest;

    .line 98
    .line 99
    invoke-virtual {p2}, Lcom/lzy/okgo/request/base/Request;->execute()Lokhttp3/Response;

    .line 100
    .line 101
    .line 102
    move-result-object p2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    goto :goto_1

    .line 104
    :catch_0
    move-exception p2

    .line 105
    invoke-static {v0, p2}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 106
    .line 107
    .line 108
    move-object p2, v7

    .line 109
    :goto_1
    if-nez p2, :cond_4

    .line 110
    .line 111
    const-string p1, "create_order failed"

    .line 112
    .line 113
    invoke-static {v5, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    .line 115
    .line 116
    return-object v7

    .line 117
    :cond_4
    invoke-virtual {p2}, Lokhttp3/Response;->〇oo〇()Z

    .line 118
    .line 119
    .line 120
    move-result v3

    .line 121
    if-nez v3, :cond_8

    .line 122
    .line 123
    invoke-virtual {p2}, Lokhttp3/Response;->〇O8o08O()I

    .line 124
    .line 125
    .line 126
    move-result v0

    .line 127
    invoke-virtual {p2}, Lokhttp3/Response;->oO()Lokhttp3/Request;

    .line 128
    .line 129
    .line 130
    move-result-object v3

    .line 131
    invoke-virtual {p2}, Lokhttp3/Response;->O〇8O8〇008()Ljava/lang/String;

    .line 132
    .line 133
    .line 134
    move-result-object v8

    .line 135
    new-instance v9, Ljava/lang/StringBuilder;

    .line 136
    .line 137
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 138
    .line 139
    .line 140
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 141
    .line 142
    .line 143
    const-string v3, " \t"

    .line 144
    .line 145
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    .line 147
    .line 148
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    .line 150
    .line 151
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 152
    .line 153
    .line 154
    move-result-object v3

    .line 155
    new-instance v8, Ljava/lang/StringBuilder;

    .line 156
    .line 157
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 158
    .line 159
    .line 160
    const-string v9, "errorMsg = "

    .line 161
    .line 162
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    .line 164
    .line 165
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    .line 167
    .line 168
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 169
    .line 170
    .line 171
    move-result-object v3

    .line 172
    invoke-static {v5, v3}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    .line 174
    .line 175
    const/16 v3, 0x196

    .line 176
    .line 177
    if-ne v0, v3, :cond_7

    .line 178
    .line 179
    invoke-virtual {p2}, Lokhttp3/Response;->o800o8O()Lokhttp3/Headers;

    .line 180
    .line 181
    .line 182
    move-result-object p2

    .line 183
    const-string v0, "X-IS-Error-Code"

    .line 184
    .line 185
    invoke-virtual {p2, v0}, Lokhttp3/Headers;->〇080(Ljava/lang/String;)Ljava/lang/String;

    .line 186
    .line 187
    .line 188
    move-result-object p2

    .line 189
    const-string v0, "10010002"

    .line 190
    .line 191
    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 192
    .line 193
    .line 194
    move-result p2

    .line 195
    if-eqz p2, :cond_7

    .line 196
    .line 197
    iget-boolean p2, p0, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask;->〇o〇:Z

    .line 198
    .line 199
    if-nez p2, :cond_7

    .line 200
    .line 201
    iput-boolean v6, p0, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask;->〇o〇:Z

    .line 202
    .line 203
    sget-object p2, Lcom/intsig/camscanner/message/ApiChangeReqWrapper;->〇080:Lcom/intsig/camscanner/message/ApiChangeReqWrapper;

    .line 204
    .line 205
    iput-object p0, v1, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$createOrder$1;->o0:Ljava/lang/Object;

    .line 206
    .line 207
    iput-object p1, v1, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$createOrder$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 208
    .line 209
    iput v6, v1, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$createOrder$1;->o〇00O:I

    .line 210
    .line 211
    invoke-virtual {p2, v1}, Lcom/intsig/camscanner/message/ApiChangeReqWrapper;->o〇0(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 212
    .line 213
    .line 214
    move-result-object p2

    .line 215
    if-ne p2, v2, :cond_5

    .line 216
    .line 217
    return-object v2

    .line 218
    :cond_5
    move-object v0, p0

    .line 219
    :goto_2
    check-cast p2, Ljava/lang/Boolean;

    .line 220
    .line 221
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    .line 222
    .line 223
    .line 224
    move-result p2

    .line 225
    new-instance v3, Ljava/lang/StringBuilder;

    .line 226
    .line 227
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 228
    .line 229
    .line 230
    const-string v6, "apis error occur, isApisOk = "

    .line 231
    .line 232
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 233
    .line 234
    .line 235
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 236
    .line 237
    .line 238
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 239
    .line 240
    .line 241
    move-result-object v3

    .line 242
    invoke-static {v5, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    .line 244
    .line 245
    if-eqz p2, :cond_7

    .line 246
    .line 247
    iput-object v7, v1, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$createOrder$1;->o0:Ljava/lang/Object;

    .line 248
    .line 249
    iput-object v7, v1, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$createOrder$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 250
    .line 251
    iput v4, v1, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$createOrder$1;->o〇00O:I

    .line 252
    .line 253
    invoke-direct {v0, p1, v1}, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask;->oO80(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 254
    .line 255
    .line 256
    move-result-object p2

    .line 257
    if-ne p2, v2, :cond_6

    .line 258
    .line 259
    return-object v2

    .line 260
    :cond_6
    :goto_3
    return-object p2

    .line 261
    :cond_7
    return-object v7

    .line 262
    :cond_8
    sget-object p1, Lcom/intsig/camscanner/purchase/pay/task/PayApi;->〇080:Lcom/intsig/camscanner/purchase/pay/task/PayApi;

    .line 263
    .line 264
    :try_start_1
    invoke-virtual {p2}, Lokhttp3/Response;->Oo08()Lokhttp3/ResponseBody;

    .line 265
    .line 266
    .line 267
    move-result-object p1

    .line 268
    invoke-virtual {p2}, Lokhttp3/Response;->〇oo〇()Z

    .line 269
    .line 270
    .line 271
    move-result p2

    .line 272
    if-eqz p2, :cond_9

    .line 273
    .line 274
    if-eqz p1, :cond_9

    .line 275
    .line 276
    invoke-virtual {p1}, Lokhttp3/ResponseBody;->charStream()Ljava/io/Reader;

    .line 277
    .line 278
    .line 279
    move-result-object p1

    .line 280
    new-instance p2, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$createOrder$$inlined$parseResponse$1;

    .line 281
    .line 282
    invoke-direct {p2}, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$createOrder$$inlined$parseResponse$1;-><init>()V

    .line 283
    .line 284
    .line 285
    invoke-virtual {p2}, Lcom/google/gson/reflect/TypeToken;->getType()Ljava/lang/reflect/Type;

    .line 286
    .line 287
    .line 288
    move-result-object p2

    .line 289
    invoke-static {p1, p2}, Lcom/intsig/okgo/utils/GsonUtils;->〇o〇(Ljava/io/Reader;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    .line 290
    .line 291
    .line 292
    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 293
    move-object v7, p1

    .line 294
    goto :goto_4

    .line 295
    :catch_1
    move-exception p1

    .line 296
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 297
    .line 298
    .line 299
    :cond_9
    :goto_4
    return-object v7
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

.method public static final synthetic o〇0(Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask;->oO80(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private final 〇80〇808〇O(Ljava/lang/String;ILjava/lang/String;Lcom/intsig/comm/purchase/entity/CreateOrderParam;Lcom/intsig/comm/purchase/entity/CreateOrderResult;)Lcom/intsig/comm/purchase/entity/SignOrderParam;
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/comm/purchase/entity/SignOrderParam;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/comm/purchase/entity/SignOrderParam;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p5, Lcom/intsig/comm/purchase/entity/CreateOrderResult;->client_id:Ljava/lang/String;

    .line 7
    .line 8
    iput-object v1, v0, Lcom/intsig/comm/purchase/entity/SignOrderParam;->client_id:Ljava/lang/String;

    .line 9
    .line 10
    iget-object v1, p5, Lcom/intsig/comm/purchase/entity/CreateOrderResult;->sign:Ljava/lang/String;

    .line 11
    .line 12
    iput-object v1, v0, Lcom/intsig/comm/purchase/entity/SignOrderParam;->sign:Ljava/lang/String;

    .line 13
    .line 14
    new-instance v1, Lcom/intsig/comm/purchase/entity/SignOrderParam$Payload;

    .line 15
    .line 16
    invoke-direct {v1}, Lcom/intsig/comm/purchase/entity/SignOrderParam$Payload;-><init>()V

    .line 17
    .line 18
    .line 19
    iput-object p1, v1, Lcom/intsig/comm/purchase/entity/SignOrderParam$Payload;->user_id:Ljava/lang/String;

    .line 20
    .line 21
    iget-object p1, p5, Lcom/intsig/comm/purchase/entity/CreateOrderResult;->uniq_id:Ljava/lang/String;

    .line 22
    .line 23
    iput-object p1, v1, Lcom/intsig/comm/purchase/entity/SignOrderParam$Payload;->uniq_id:Ljava/lang/String;

    .line 24
    .line 25
    iget-wide v2, p5, Lcom/intsig/comm/purchase/entity/CreateOrderResult;->total_amount:D

    .line 26
    .line 27
    iput-wide v2, v1, Lcom/intsig/comm/purchase/entity/SignOrderParam$Payload;->total_amount:D

    .line 28
    .line 29
    iget-object p1, p5, Lcom/intsig/comm/purchase/entity/CreateOrderResult;->currency:Ljava/lang/String;

    .line 30
    .line 31
    iput-object p1, v1, Lcom/intsig/comm/purchase/entity/SignOrderParam$Payload;->currency:Ljava/lang/String;

    .line 32
    .line 33
    iget-object p1, p5, Lcom/intsig/comm/purchase/entity/CreateOrderResult;->notify_url:Ljava/lang/String;

    .line 34
    .line 35
    iput-object p1, v1, Lcom/intsig/comm/purchase/entity/SignOrderParam$Payload;->notify_url:Ljava/lang/String;

    .line 36
    .line 37
    iget-object p1, p5, Lcom/intsig/comm/purchase/entity/CreateOrderResult;->attach_data:Ljava/lang/String;

    .line 38
    .line 39
    iput-object p1, v1, Lcom/intsig/comm/purchase/entity/SignOrderParam$Payload;->attach_data:Ljava/lang/String;

    .line 40
    .line 41
    iget-object p1, p4, Lcom/intsig/comm/purchase/entity/CreateOrderParam;->productName:Ljava/lang/String;

    .line 42
    .line 43
    iput-object p1, v1, Lcom/intsig/comm/purchase/entity/SignOrderParam$Payload;->product_name:Ljava/lang/String;

    .line 44
    .line 45
    iget-object p1, p4, Lcom/intsig/comm/purchase/entity/CreateOrderParam;->productDesc:Ljava/lang/String;

    .line 46
    .line 47
    iput-object p1, v1, Lcom/intsig/comm/purchase/entity/SignOrderParam$Payload;->product_desc:Ljava/lang/String;

    .line 48
    .line 49
    iget-object p1, p4, Lcom/intsig/comm/purchase/entity/CreateOrderParam;->returnUrl:Ljava/lang/String;

    .line 50
    .line 51
    iput-object p1, v1, Lcom/intsig/comm/purchase/entity/SignOrderParam$Payload;->return_url:Ljava/lang/String;

    .line 52
    .line 53
    iput-object v1, v0, Lcom/intsig/comm/purchase/entity/SignOrderParam;->payload:Lcom/intsig/comm/purchase/entity/SignOrderParam$Payload;

    .line 54
    .line 55
    iput-object p3, v0, Lcom/intsig/comm/purchase/entity/SignOrderParam;->app_id:Ljava/lang/String;

    .line 56
    .line 57
    iput p2, v0, Lcom/intsig/comm/purchase/entity/SignOrderParam;->payway:I

    .line 58
    .line 59
    return-object v0
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
.end method

.method private final 〇〇888(Lcom/intsig/comm/purchase/entity/CreateOrderParam;)Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/comm/purchase/entity/CreateOrderParam;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/HashMap;

    .line 2
    .line 3
    const/16 v1, 0x18

    .line 4
    .line 5
    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 6
    .line 7
    .line 8
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/CreateOrderParam;->token:Ljava/lang/String;

    .line 9
    .line 10
    const-string/jumbo v2, "token"

    .line 11
    .line 12
    .line 13
    invoke-static {v0, v2, v1}, Lcom/intsig/utils/CollectionUtil;->〇080(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    const-string v1, "cs_ept_d"

    .line 17
    .line 18
    iget-object v2, p1, Lcom/intsig/comm/purchase/entity/CreateOrderParam;->cs_ept_d:Ljava/lang/String;

    .line 19
    .line 20
    invoke-static {v0, v1, v2}, Lcom/intsig/utils/CollectionUtil;->〇080(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    const-string v1, "client_app"

    .line 24
    .line 25
    iget-object v2, p1, Lcom/intsig/comm/purchase/entity/CreateOrderParam;->client_app:Ljava/lang/String;

    .line 26
    .line 27
    invoke-static {v0, v1, v2}, Lcom/intsig/utils/CollectionUtil;->〇080(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    const-string/jumbo v1, "vendor"

    .line 31
    .line 32
    .line 33
    iget-object v2, p1, Lcom/intsig/comm/purchase/entity/CreateOrderParam;->vendor:Ljava/lang/String;

    .line 34
    .line 35
    invoke-static {v0, v1, v2}, Lcom/intsig/utils/CollectionUtil;->〇080(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    const-string v1, "language"

    .line 39
    .line 40
    iget-object v2, p1, Lcom/intsig/comm/purchase/entity/CreateOrderParam;->language:Ljava/lang/String;

    .line 41
    .line 42
    invoke-static {v0, v1, v2}, Lcom/intsig/utils/CollectionUtil;->〇080(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    .line 44
    .line 45
    const-string v1, "currency"

    .line 46
    .line 47
    iget-object v2, p1, Lcom/intsig/comm/purchase/entity/CreateOrderParam;->currency:Ljava/lang/String;

    .line 48
    .line 49
    invoke-static {v0, v1, v2}, Lcom/intsig/utils/CollectionUtil;->〇080(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    const-string v1, "pay_from"

    .line 53
    .line 54
    iget-object v2, p1, Lcom/intsig/comm/purchase/entity/CreateOrderParam;->pay_from:Ljava/lang/String;

    .line 55
    .line 56
    invoke-static {v0, v1, v2}, Lcom/intsig/utils/CollectionUtil;->〇080(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    const-string v1, "pay_from_part"

    .line 60
    .line 61
    iget-object v2, p1, Lcom/intsig/comm/purchase/entity/CreateOrderParam;->pay_from_part:Ljava/lang/String;

    .line 62
    .line 63
    invoke-static {v0, v1, v2}, Lcom/intsig/utils/CollectionUtil;->〇080(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    .line 65
    .line 66
    const-string v1, "pay_scheme"

    .line 67
    .line 68
    iget-object v2, p1, Lcom/intsig/comm/purchase/entity/CreateOrderParam;->pay_scheme:Ljava/lang/String;

    .line 69
    .line 70
    invoke-static {v0, v1, v2}, Lcom/intsig/utils/CollectionUtil;->〇080(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    .line 72
    .line 73
    const-string v1, "app_package"

    .line 74
    .line 75
    iget-object v2, p1, Lcom/intsig/comm/purchase/entity/CreateOrderParam;->app_package:Ljava/lang/String;

    .line 76
    .line 77
    invoke-static {v0, v1, v2}, Lcom/intsig/utils/CollectionUtil;->〇080(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    .line 79
    .line 80
    const-string v1, "country"

    .line 81
    .line 82
    iget-object v2, p1, Lcom/intsig/comm/purchase/entity/CreateOrderParam;->country:Ljava/lang/String;

    .line 83
    .line 84
    invoke-static {v0, v1, v2}, Lcom/intsig/utils/CollectionUtil;->〇080(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    const-string v1, "productName"

    .line 88
    .line 89
    iget-object v2, p1, Lcom/intsig/comm/purchase/entity/CreateOrderParam;->productName:Ljava/lang/String;

    .line 90
    .line 91
    invoke-static {v0, v1, v2}, Lcom/intsig/utils/CollectionUtil;->〇080(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    const-string v1, "productDesc"

    .line 95
    .line 96
    iget-object v2, p1, Lcom/intsig/comm/purchase/entity/CreateOrderParam;->productDesc:Ljava/lang/String;

    .line 97
    .line 98
    invoke-static {v0, v1, v2}, Lcom/intsig/utils/CollectionUtil;->〇080(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    .line 100
    .line 101
    const-string v1, "alipay"

    .line 102
    .line 103
    iget-object v2, p1, Lcom/intsig/comm/purchase/entity/CreateOrderParam;->alipay:Ljava/lang/String;

    .line 104
    .line 105
    invoke-static {v0, v1, v2}, Lcom/intsig/utils/CollectionUtil;->〇080(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    .line 107
    .line 108
    const-string v1, "google_play"

    .line 109
    .line 110
    iget-object v2, p1, Lcom/intsig/comm/purchase/entity/CreateOrderParam;->google_play:Ljava/lang/String;

    .line 111
    .line 112
    invoke-static {v0, v1, v2}, Lcom/intsig/utils/CollectionUtil;->〇080(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    .line 114
    .line 115
    const-string v1, "coupon"

    .line 116
    .line 117
    iget-object v2, p1, Lcom/intsig/comm/purchase/entity/CreateOrderParam;->coupon:Ljava/lang/String;

    .line 118
    .line 119
    invoke-static {v0, v1, v2}, Lcom/intsig/utils/CollectionUtil;->〇080(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    .line 121
    .line 122
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/CreateOrderParam;->payload:Lcom/intsig/comm/purchase/entity/CreateOrderParam$PayLoad;

    .line 123
    .line 124
    invoke-static {v1}, Lcom/intsig/okgo/utils/GsonUtils;->Oo08(Ljava/lang/Object;)Ljava/lang/String;

    .line 125
    .line 126
    .line 127
    move-result-object v1

    .line 128
    const-string v2, "payload"

    .line 129
    .line 130
    invoke-static {v0, v2, v1}, Lcom/intsig/utils/CollectionUtil;->〇080(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    .line 132
    .line 133
    const-string v1, "returnUrl"

    .line 134
    .line 135
    iget-object p1, p1, Lcom/intsig/comm/purchase/entity/CreateOrderParam;->returnUrl:Ljava/lang/String;

    .line 136
    .line 137
    invoke-static {v0, v1, p1}, Lcom/intsig/utils/CollectionUtil;->〇080(Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    .line 139
    .line 140
    return-object v0
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method


# virtual methods
.method public 〇080(Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 36
    .param p1    # Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/coroutines/Continuation;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    move-object/from16 v7, p0

    .line 2
    .line 3
    move-object/from16 v3, p1

    .line 4
    .line 5
    move-object/from16 v0, p2

    .line 6
    .line 7
    const-string v1, "PurchaseHelper-PayApi"

    .line 8
    .line 9
    instance-of v2, v0, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$chain$1;

    .line 10
    .line 11
    if-eqz v2, :cond_0

    .line 12
    .line 13
    move-object v2, v0

    .line 14
    check-cast v2, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$chain$1;

    .line 15
    .line 16
    iget v4, v2, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$chain$1;->〇080OO8〇0:I

    .line 17
    .line 18
    const/high16 v5, -0x80000000

    .line 19
    .line 20
    and-int v6, v4, v5

    .line 21
    .line 22
    if-eqz v6, :cond_0

    .line 23
    .line 24
    sub-int/2addr v4, v5

    .line 25
    iput v4, v2, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$chain$1;->〇080OO8〇0:I

    .line 26
    .line 27
    goto :goto_0

    .line 28
    :cond_0
    new-instance v2, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$chain$1;

    .line 29
    .line 30
    invoke-direct {v2, v7, v0}, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$chain$1;-><init>(Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask;Lkotlin/coroutines/Continuation;)V

    .line 31
    .line 32
    .line 33
    :goto_0
    iget-object v0, v2, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$chain$1;->o〇00O:Ljava/lang/Object;

    .line 34
    .line 35
    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->O8()Ljava/lang/Object;

    .line 36
    .line 37
    .line 38
    move-result-object v4

    .line 39
    iget v5, v2, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$chain$1;->〇080OO8〇0:I

    .line 40
    .line 41
    const/4 v6, 0x0

    .line 42
    const/4 v8, 0x2

    .line 43
    const/4 v9, 0x1

    .line 44
    const-string v10, "PurchaseHelper-CreateOrderTask"

    .line 45
    .line 46
    const/4 v11, 0x0

    .line 47
    if-eqz v5, :cond_3

    .line 48
    .line 49
    if-eq v5, v9, :cond_2

    .line 50
    .line 51
    if-ne v5, v8, :cond_1

    .line 52
    .line 53
    invoke-static {v0}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 54
    .line 55
    .line 56
    goto/16 :goto_e

    .line 57
    .line 58
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 59
    .line 60
    const-string v1, "call to \'resume\' before \'invoke\' with coroutine"

    .line 61
    .line 62
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    throw v0

    .line 66
    :cond_2
    iget-object v3, v2, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$chain$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 67
    .line 68
    check-cast v3, Lcom/intsig/comm/purchase/entity/CreateOrderParam;

    .line 69
    .line 70
    iget-object v5, v2, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$chain$1;->OO:Ljava/lang/Object;

    .line 71
    .line 72
    check-cast v5, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;

    .line 73
    .line 74
    iget-object v12, v2, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 75
    .line 76
    check-cast v12, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;

    .line 77
    .line 78
    iget-object v13, v2, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$chain$1;->o0:Ljava/lang/Object;

    .line 79
    .line 80
    check-cast v13, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask;

    .line 81
    .line 82
    invoke-static {v0}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 83
    .line 84
    .line 85
    move-object v14, v12

    .line 86
    move-object v12, v13

    .line 87
    move-object/from16 v35, v5

    .line 88
    .line 89
    move-object v5, v3

    .line 90
    move-object/from16 v3, v35

    .line 91
    .line 92
    goto/16 :goto_8

    .line 93
    .line 94
    :cond_3
    invoke-static {v0}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 95
    .line 96
    .line 97
    const-string/jumbo v0, "start create_order"

    .line 98
    .line 99
    .line 100
    invoke-static {v10, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    .line 102
    .line 103
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇o00〇〇Oo()Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;

    .line 104
    .line 105
    .line 106
    move-result-object v0

    .line 107
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇o00〇〇Oo()Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;

    .line 108
    .line 109
    .line 110
    move-result-object v5

    .line 111
    if-eqz v5, :cond_4

    .line 112
    .line 113
    invoke-virtual {v5}, Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;->〇o〇()Ljava/lang/String;

    .line 114
    .line 115
    .line 116
    move-result-object v5

    .line 117
    goto :goto_1

    .line 118
    :cond_4
    move-object v5, v11

    .line 119
    :goto_1
    new-instance v12, Ljava/lang/StringBuilder;

    .line 120
    .line 121
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 122
    .line 123
    .line 124
    const-string v13, "create_order, uuid: "

    .line 125
    .line 126
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    .line 128
    .line 129
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 130
    .line 131
    .line 132
    const-string v0, " - "

    .line 133
    .line 134
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    .line 136
    .line 137
    invoke-virtual {v12, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    .line 139
    .line 140
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 141
    .line 142
    .line 143
    move-result-object v0

    .line 144
    invoke-static {v10, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    .line 146
    .line 147
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇〇888()Lcom/intsig/comm/purchase/entity/ProductResultItem;

    .line 148
    .line 149
    .line 150
    move-result-object v0

    .line 151
    iget-object v12, v0, Lcom/intsig/comm/purchase/entity/ProductResultItem;->product_id:Ljava/lang/String;

    .line 152
    .line 153
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->oO80()Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 154
    .line 155
    .line 156
    move-result-object v13

    .line 157
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇〇888()Lcom/intsig/comm/purchase/entity/ProductResultItem;

    .line 158
    .line 159
    .line 160
    move-result-object v0

    .line 161
    iget-object v14, v0, Lcom/intsig/comm/purchase/entity/ProductResultItem;->product_title:Ljava/lang/String;

    .line 162
    .line 163
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇〇888()Lcom/intsig/comm/purchase/entity/ProductResultItem;

    .line 164
    .line 165
    .line 166
    move-result-object v0

    .line 167
    iget-object v15, v0, Lcom/intsig/comm/purchase/entity/ProductResultItem;->propertyId:Ljava/lang/String;

    .line 168
    .line 169
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇o00〇〇Oo()Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;

    .line 170
    .line 171
    .line 172
    move-result-object v0

    .line 173
    if-eqz v0, :cond_5

    .line 174
    .line 175
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;->〇o00〇〇Oo()I

    .line 176
    .line 177
    .line 178
    move-result v0

    .line 179
    move/from16 v16, v0

    .line 180
    .line 181
    goto :goto_2

    .line 182
    :cond_5
    const/16 v16, 0x0

    .line 183
    .line 184
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇o00〇〇Oo()Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;

    .line 185
    .line 186
    .line 187
    move-result-object v0

    .line 188
    if-eqz v0, :cond_6

    .line 189
    .line 190
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;->〇080()Ljava/lang/String;

    .line 191
    .line 192
    .line 193
    move-result-object v0

    .line 194
    move-object/from16 v17, v0

    .line 195
    .line 196
    goto :goto_3

    .line 197
    :cond_6
    move-object/from16 v17, v11

    .line 198
    .line 199
    :goto_3
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇o00〇〇Oo()Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;

    .line 200
    .line 201
    .line 202
    move-result-object v0

    .line 203
    if-eqz v0, :cond_7

    .line 204
    .line 205
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;->〇o〇()Ljava/lang/String;

    .line 206
    .line 207
    .line 208
    move-result-object v0

    .line 209
    move-object/from16 v18, v0

    .line 210
    .line 211
    goto :goto_4

    .line 212
    :cond_7
    move-object/from16 v18, v11

    .line 213
    .line 214
    :goto_4
    invoke-static/range {v12 .. v18}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->〇O888o0o(Ljava/lang/String;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lcom/intsig/comm/purchase/entity/CreateOrderParam;

    .line 215
    .line 216
    .line 217
    move-result-object v5

    .line 218
    if-nez v5, :cond_8

    .line 219
    .line 220
    const-string v0, "createOrderParam is empty"

    .line 221
    .line 222
    invoke-static {v10, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    .line 224
    .line 225
    const/16 v2, 0x271b

    .line 226
    .line 227
    const/4 v4, 0x0

    .line 228
    const/4 v5, 0x4

    .line 229
    const/4 v6, 0x0

    .line 230
    move-object/from16 v1, p0

    .line 231
    .line 232
    move-object/from16 v3, p1

    .line 233
    .line 234
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;->O8(Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;ILcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;Lcom/intsig/pay/base/model/PayOrderResponse;ILjava/lang/Object;)Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;

    .line 235
    .line 236
    .line 237
    move-result-object v0

    .line 238
    return-object v0

    .line 239
    :cond_8
    invoke-static {v5}, Lcom/intsig/okgo/utils/GsonUtils;->Oo08(Ljava/lang/Object;)Ljava/lang/String;

    .line 240
    .line 241
    .line 242
    move-result-object v0

    .line 243
    new-instance v12, Ljava/lang/StringBuilder;

    .line 244
    .line 245
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 246
    .line 247
    .line 248
    const-string v13, "create_order, params: "

    .line 249
    .line 250
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    .line 252
    .line 253
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    .line 255
    .line 256
    const-string v0, ")"

    .line 257
    .line 258
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    .line 260
    .line 261
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 262
    .line 263
    .line 264
    move-result-object v0

    .line 265
    invoke-static {v10, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    .line 267
    .line 268
    invoke-direct {v7, v5}, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask;->〇〇888(Lcom/intsig/comm/purchase/entity/CreateOrderParam;)Ljava/util/Map;

    .line 269
    .line 270
    .line 271
    move-result-object v0

    .line 272
    invoke-static {v0}, Lcom/intsig/tianshu/TianShuAPI;->Oo(Ljava/util/Map;)Ljava/lang/String;

    .line 273
    .line 274
    .line 275
    move-result-object v0

    .line 276
    const-string v12, "createOrderSign"

    .line 277
    .line 278
    invoke-static {v0, v12}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 279
    .line 280
    .line 281
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 282
    .line 283
    .line 284
    move-result v12

    .line 285
    if-eqz v12, :cond_9

    .line 286
    .line 287
    const-string/jumbo v0, "sign is empty"

    .line 288
    .line 289
    .line 290
    invoke-static {v10, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 291
    .line 292
    .line 293
    const/16 v2, 0x271b

    .line 294
    .line 295
    const/4 v4, 0x0

    .line 296
    const/4 v5, 0x4

    .line 297
    const/4 v6, 0x0

    .line 298
    move-object/from16 v1, p0

    .line 299
    .line 300
    move-object/from16 v3, p1

    .line 301
    .line 302
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;->O8(Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;ILcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;Lcom/intsig/pay/base/model/PayOrderResponse;ILjava/lang/Object;)Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;

    .line 303
    .line 304
    .line 305
    move-result-object v0

    .line 306
    return-object v0

    .line 307
    :cond_9
    iput-object v0, v5, Lcom/intsig/comm/purchase/entity/CreateOrderParam;->sign:Ljava/lang/String;

    .line 308
    .line 309
    :try_start_0
    invoke-virtual {v5}, Lcom/intsig/tianshu/base/BaseJsonObj;->toJSONObject()Lorg/json/JSONObject;

    .line 310
    .line 311
    .line 312
    move-result-object v0

    .line 313
    const-string v12, "payload"

    .line 314
    .line 315
    iget-object v13, v5, Lcom/intsig/comm/purchase/entity/CreateOrderParam;->payload:Lcom/intsig/comm/purchase/entity/CreateOrderParam$PayLoad;

    .line 316
    .line 317
    invoke-static {v13}, Lcom/intsig/okgo/utils/GsonUtils;->Oo08(Ljava/lang/Object;)Ljava/lang/String;

    .line 318
    .line 319
    .line 320
    move-result-object v13

    .line 321
    invoke-virtual {v0, v12, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 322
    .line 323
    .line 324
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    .line 325
    .line 326
    .line 327
    move-result-object v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 328
    goto :goto_5

    .line 329
    :catch_0
    move-exception v0

    .line 330
    invoke-static {v10, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 331
    .line 332
    .line 333
    move-object v0, v11

    .line 334
    :goto_5
    new-instance v12, Ljava/lang/StringBuilder;

    .line 335
    .line 336
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 337
    .line 338
    .line 339
    const-string v13, " pay/create_order params:"

    .line 340
    .line 341
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 342
    .line 343
    .line 344
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 345
    .line 346
    .line 347
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 348
    .line 349
    .line 350
    move-result-object v12

    .line 351
    invoke-static {v10, v12}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    .line 353
    .line 354
    if-eqz v0, :cond_b

    .line 355
    .line 356
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 357
    .line 358
    .line 359
    move-result v12

    .line 360
    if-eqz v12, :cond_a

    .line 361
    .line 362
    goto :goto_6

    .line 363
    :cond_a
    const/4 v12, 0x0

    .line 364
    goto :goto_7

    .line 365
    :cond_b
    :goto_6
    const/4 v12, 0x1

    .line 366
    :goto_7
    if-eqz v12, :cond_c

    .line 367
    .line 368
    const/16 v2, 0x271b

    .line 369
    .line 370
    const/4 v4, 0x0

    .line 371
    const/4 v5, 0x4

    .line 372
    const/4 v6, 0x0

    .line 373
    move-object/from16 v1, p0

    .line 374
    .line 375
    move-object/from16 v3, p1

    .line 376
    .line 377
    invoke-static/range {v1 .. v6}, Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;->O8(Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;ILcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;Lcom/intsig/pay/base/model/PayOrderResponse;ILjava/lang/Object;)Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;

    .line 378
    .line 379
    .line 380
    move-result-object v0

    .line 381
    return-object v0

    .line 382
    :cond_c
    iput-object v7, v2, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$chain$1;->o0:Ljava/lang/Object;

    .line 383
    .line 384
    iput-object v3, v2, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 385
    .line 386
    iput-object v3, v2, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$chain$1;->OO:Ljava/lang/Object;

    .line 387
    .line 388
    iput-object v5, v2, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$chain$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 389
    .line 390
    iput v9, v2, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$chain$1;->〇080OO8〇0:I

    .line 391
    .line 392
    invoke-direct {v7, v0, v2}, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask;->oO80(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 393
    .line 394
    .line 395
    move-result-object v0

    .line 396
    if-ne v0, v4, :cond_d

    .line 397
    .line 398
    return-object v4

    .line 399
    :cond_d
    move-object v14, v3

    .line 400
    move-object v12, v7

    .line 401
    :goto_8
    move-object v13, v0

    .line 402
    check-cast v13, Lcom/intsig/comm/purchase/entity/CreateOrderResult;

    .line 403
    .line 404
    if-nez v13, :cond_e

    .line 405
    .line 406
    const/16 v13, 0x271b

    .line 407
    .line 408
    const/4 v15, 0x0

    .line 409
    const/16 v16, 0x4

    .line 410
    .line 411
    const/16 v17, 0x0

    .line 412
    .line 413
    invoke-static/range {v12 .. v17}, Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;->O8(Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;ILcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;Lcom/intsig/pay/base/model/PayOrderResponse;ILjava/lang/Object;)Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;

    .line 414
    .line 415
    .line 416
    move-result-object v0

    .line 417
    return-object v0

    .line 418
    :cond_e
    iget-object v0, v13, Lcom/intsig/comm/purchase/entity/CreateOrderResult;->uniq_id:Ljava/lang/String;

    .line 419
    .line 420
    invoke-virtual {v3, v0}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇O〇(Ljava/lang/String;)V

    .line 421
    .line 422
    .line 423
    invoke-virtual {v3}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇o〇()Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;

    .line 424
    .line 425
    .line 426
    move-result-object v0

    .line 427
    if-eqz v0, :cond_f

    .line 428
    .line 429
    invoke-interface {v0}, Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;->getAppId()Ljava/lang/String;

    .line 430
    .line 431
    .line 432
    move-result-object v0

    .line 433
    move-object v15, v0

    .line 434
    goto :goto_9

    .line 435
    :cond_f
    move-object v15, v11

    .line 436
    :goto_9
    if-nez v15, :cond_10

    .line 437
    .line 438
    const-string v0, "create_order failed, appId = null"

    .line 439
    .line 440
    invoke-static {v10, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    .line 442
    .line 443
    const/16 v13, 0x271b

    .line 444
    .line 445
    const/4 v15, 0x0

    .line 446
    const/16 v16, 0x4

    .line 447
    .line 448
    const/16 v17, 0x0

    .line 449
    .line 450
    invoke-static/range {v12 .. v17}, Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;->O8(Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;ILcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;Lcom/intsig/pay/base/model/PayOrderResponse;ILjava/lang/Object;)Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;

    .line 451
    .line 452
    .line 453
    move-result-object v0

    .line 454
    return-object v0

    .line 455
    :cond_10
    const-string v0, "intsig-"

    .line 456
    .line 457
    invoke-static {v15, v0, v6, v8, v11}, Lkotlin/text/StringsKt;->Oo8Oo00oo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    .line 458
    .line 459
    .line 460
    move-result v0

    .line 461
    if-eqz v0, :cond_11

    .line 462
    .line 463
    const-string v16, "intsig-"

    .line 464
    .line 465
    const-string v17, ""

    .line 466
    .line 467
    const/16 v18, 0x0

    .line 468
    .line 469
    const/16 v19, 0x4

    .line 470
    .line 471
    const/16 v20, 0x0

    .line 472
    .line 473
    invoke-static/range {v15 .. v20}, Lkotlin/text/StringsKt;->〇oOO8O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILjava/lang/Object;)Ljava/lang/String;

    .line 474
    .line 475
    .line 476
    move-result-object v0

    .line 477
    move-object/from16 v18, v0

    .line 478
    .line 479
    goto :goto_a

    .line 480
    :cond_11
    move-object/from16 v18, v15

    .line 481
    .line 482
    :goto_a
    invoke-virtual {v3}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇8o8o〇()Ljava/lang/String;

    .line 483
    .line 484
    .line 485
    move-result-object v16

    .line 486
    invoke-virtual {v3}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇〇888()Lcom/intsig/comm/purchase/entity/ProductResultItem;

    .line 487
    .line 488
    .line 489
    move-result-object v0

    .line 490
    iget v0, v0, Lcom/intsig/comm/purchase/entity/ProductResultItem;->payway:I

    .line 491
    .line 492
    const-string v6, "createOrderParam"

    .line 493
    .line 494
    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 495
    .line 496
    .line 497
    move-object v15, v12

    .line 498
    move/from16 v17, v0

    .line 499
    .line 500
    move-object/from16 v19, v5

    .line 501
    .line 502
    move-object/from16 v20, v13

    .line 503
    .line 504
    invoke-direct/range {v15 .. v20}, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask;->〇80〇808〇O(Ljava/lang/String;ILjava/lang/String;Lcom/intsig/comm/purchase/entity/CreateOrderParam;Lcom/intsig/comm/purchase/entity/CreateOrderResult;)Lcom/intsig/comm/purchase/entity/SignOrderParam;

    .line 505
    .line 506
    .line 507
    move-result-object v0

    .line 508
    invoke-static {v0}, Lcom/intsig/okgo/utils/GsonUtils;->Oo08(Ljava/lang/Object;)Ljava/lang/String;

    .line 509
    .line 510
    .line 511
    move-result-object v0

    .line 512
    new-instance v5, Ljava/lang/StringBuilder;

    .line 513
    .line 514
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 515
    .line 516
    .line 517
    const-string/jumbo v6, "start get_sign params = "

    .line 518
    .line 519
    .line 520
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 521
    .line 522
    .line 523
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 524
    .line 525
    .line 526
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 527
    .line 528
    .line 529
    move-result-object v5

    .line 530
    invoke-static {v10, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 531
    .line 532
    .line 533
    sget-object v5, Lcom/intsig/camscanner/purchase/pay/task/PayApi;->〇080:Lcom/intsig/camscanner/purchase/pay/task/PayApi;

    .line 534
    .line 535
    const-string/jumbo v5, "signBody"

    .line 536
    .line 537
    .line 538
    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 539
    .line 540
    .line 541
    sget-object v5, Lcom/intsig/camscanner/purchase/pay/task/PayApi;->〇080:Lcom/intsig/camscanner/purchase/pay/task/PayApi;

    .line 542
    .line 543
    const-string v6, "/pay/get_signed_order"

    .line 544
    .line 545
    invoke-virtual {v5, v6}, Lcom/intsig/camscanner/purchase/pay/task/PayApi;->〇080(Ljava/lang/String;)Ljava/lang/String;

    .line 546
    .line 547
    .line 548
    move-result-object v5

    .line 549
    :try_start_1
    invoke-static {v5}, Lcom/lzy/okgo/OkGo;->post(Ljava/lang/String;)Lcom/lzy/okgo/request/PostRequest;

    .line 550
    .line 551
    .line 552
    move-result-object v5

    .line 553
    sget-object v6, Lokhttp3/RequestBody;->Companion:Lokhttp3/RequestBody$Companion;

    .line 554
    .line 555
    invoke-static {v6, v0, v11, v9, v11}, Lokhttp3/RequestBody$Companion;->〇80〇808〇O(Lokhttp3/RequestBody$Companion;Ljava/lang/String;Lokhttp3/MediaType;ILjava/lang/Object;)Lokhttp3/RequestBody;

    .line 556
    .line 557
    .line 558
    move-result-object v0

    .line 559
    invoke-virtual {v5, v0}, Lcom/lzy/okgo/request/base/BodyRequest;->upRequestBody(Lokhttp3/RequestBody;)Lcom/lzy/okgo/request/base/BodyRequest;

    .line 560
    .line 561
    .line 562
    move-result-object v0

    .line 563
    check-cast v0, Lcom/lzy/okgo/request/PostRequest;

    .line 564
    .line 565
    invoke-virtual {v0}, Lcom/lzy/okgo/request/base/Request;->execute()Lokhttp3/Response;

    .line 566
    .line 567
    .line 568
    move-result-object v0

    .line 569
    const-string v5, "response"

    .line 570
    .line 571
    invoke-static {v0, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 572
    .line 573
    .line 574
    :try_start_2
    invoke-virtual {v0}, Lokhttp3/Response;->Oo08()Lokhttp3/ResponseBody;

    .line 575
    .line 576
    .line 577
    move-result-object v5

    .line 578
    invoke-virtual {v0}, Lokhttp3/Response;->〇oo〇()Z

    .line 579
    .line 580
    .line 581
    move-result v0

    .line 582
    if-eqz v0, :cond_12

    .line 583
    .line 584
    if-eqz v5, :cond_12

    .line 585
    .line 586
    invoke-virtual {v5}, Lokhttp3/ResponseBody;->charStream()Ljava/io/Reader;

    .line 587
    .line 588
    .line 589
    move-result-object v0

    .line 590
    new-instance v5, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$chain$lambda$1$$inlined$postRequestParse$1;

    .line 591
    .line 592
    invoke-direct {v5}, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$chain$lambda$1$$inlined$postRequestParse$1;-><init>()V

    .line 593
    .line 594
    .line 595
    invoke-virtual {v5}, Lcom/google/gson/reflect/TypeToken;->getType()Ljava/lang/reflect/Type;

    .line 596
    .line 597
    .line 598
    move-result-object v5

    .line 599
    invoke-static {v0, v5}, Lcom/intsig/okgo/utils/GsonUtils;->〇o〇(Ljava/io/Reader;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    .line 600
    .line 601
    .line 602
    move-result-object v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    .line 603
    goto :goto_c

    .line 604
    :catch_1
    move-exception v0

    .line 605
    :try_start_3
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    .line 606
    .line 607
    .line 608
    goto :goto_b

    .line 609
    :catch_2
    move-exception v0

    .line 610
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 611
    .line 612
    .line 613
    :cond_12
    :goto_b
    move-object v0, v11

    .line 614
    :goto_c
    check-cast v0, Lcom/intsig/comm/purchase/entity/SignOrderResult;

    .line 615
    .line 616
    if-eqz v0, :cond_13

    .line 617
    .line 618
    iget-object v1, v0, Lcom/intsig/comm/purchase/entity/SignOrderResult;->data:Lcom/intsig/comm/purchase/entity/SignOrderResult$Data;

    .line 619
    .line 620
    goto :goto_d

    .line 621
    :cond_13
    move-object v1, v11

    .line 622
    :goto_d
    if-eqz v0, :cond_17

    .line 623
    .line 624
    iget v0, v0, Lcom/intsig/comm/purchase/entity/SignOrderResult;->ret:I

    .line 625
    .line 626
    if-nez v0, :cond_17

    .line 627
    .line 628
    if-nez v1, :cond_14

    .line 629
    .line 630
    goto/16 :goto_f

    .line 631
    .line 632
    :cond_14
    new-instance v0, Lcom/intsig/pay/base/model/PayOrderRequest;

    .line 633
    .line 634
    move-object v15, v0

    .line 635
    const/16 v16, 0x0

    .line 636
    .line 637
    const/16 v17, 0x0

    .line 638
    .line 639
    const/16 v18, 0x0

    .line 640
    .line 641
    const/16 v19, 0x0

    .line 642
    .line 643
    const/16 v20, 0x0

    .line 644
    .line 645
    const/16 v21, 0x0

    .line 646
    .line 647
    const/16 v22, 0x0

    .line 648
    .line 649
    const/16 v23, 0x0

    .line 650
    .line 651
    const/16 v24, 0x0

    .line 652
    .line 653
    const/16 v25, 0x0

    .line 654
    .line 655
    const/16 v26, 0x0

    .line 656
    .line 657
    const/16 v27, 0x0

    .line 658
    .line 659
    const/16 v28, 0x0

    .line 660
    .line 661
    const/16 v29, 0x0

    .line 662
    .line 663
    const/16 v30, 0x0

    .line 664
    .line 665
    const/16 v31, 0x0

    .line 666
    .line 667
    const/16 v32, 0x0

    .line 668
    .line 669
    const v33, 0x1ffff

    .line 670
    .line 671
    .line 672
    const/16 v34, 0x0

    .line 673
    .line 674
    invoke-direct/range {v15 .. v34}, Lcom/intsig/pay/base/model/PayOrderRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 675
    .line 676
    .line 677
    invoke-virtual {v3, v0}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->OO0o〇〇(Lcom/intsig/pay/base/model/PayOrderRequest;)V

    .line 678
    .line 679
    .line 680
    invoke-virtual {v3}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->O8()Lcom/intsig/pay/base/model/PayOrderRequest;

    .line 681
    .line 682
    .line 683
    move-result-object v0

    .line 684
    if-eqz v0, :cond_15

    .line 685
    .line 686
    iget-object v3, v1, Lcom/intsig/comm/purchase/entity/SignOrderResult$Data;->out_trade_no:Ljava/lang/String;

    .line 687
    .line 688
    invoke-virtual {v0, v3}, Lcom/intsig/pay/base/model/PayOrderRequest;->setOut_trade_no(Ljava/lang/String;)V

    .line 689
    .line 690
    .line 691
    iget-object v3, v1, Lcom/intsig/comm/purchase/entity/SignOrderResult$Data;->notify_token:Ljava/lang/String;

    .line 692
    .line 693
    invoke-virtual {v0, v3}, Lcom/intsig/pay/base/model/PayOrderRequest;->setNotify_token(Ljava/lang/String;)V

    .line 694
    .line 695
    .line 696
    iget-object v3, v13, Lcom/intsig/comm/purchase/entity/CreateOrderResult;->uniq_id:Ljava/lang/String;

    .line 697
    .line 698
    invoke-virtual {v0, v3}, Lcom/intsig/pay/base/model/PayOrderRequest;->setUniq_id(Ljava/lang/String;)V

    .line 699
    .line 700
    .line 701
    iget-object v3, v1, Lcom/intsig/comm/purchase/entity/SignOrderResult$Data;->noncestr:Ljava/lang/String;

    .line 702
    .line 703
    invoke-virtual {v0, v3}, Lcom/intsig/pay/base/model/PayOrderRequest;->setNoncestr(Ljava/lang/String;)V

    .line 704
    .line 705
    .line 706
    iget-object v3, v1, Lcom/intsig/comm/purchase/entity/SignOrderResult$Data;->packageValue:Ljava/lang/String;

    .line 707
    .line 708
    invoke-virtual {v0, v3}, Lcom/intsig/pay/base/model/PayOrderRequest;->setPackageValue(Ljava/lang/String;)V

    .line 709
    .line 710
    .line 711
    iget-object v3, v1, Lcom/intsig/comm/purchase/entity/SignOrderResult$Data;->partnerid:Ljava/lang/String;

    .line 712
    .line 713
    invoke-virtual {v0, v3}, Lcom/intsig/pay/base/model/PayOrderRequest;->setPartnerid(Ljava/lang/String;)V

    .line 714
    .line 715
    .line 716
    iget-object v3, v1, Lcom/intsig/comm/purchase/entity/SignOrderResult$Data;->prepay_id:Ljava/lang/String;

    .line 717
    .line 718
    invoke-virtual {v0, v3}, Lcom/intsig/pay/base/model/PayOrderRequest;->setPrepay_id(Ljava/lang/String;)V

    .line 719
    .line 720
    .line 721
    iget-object v3, v1, Lcom/intsig/comm/purchase/entity/SignOrderResult$Data;->timestamp:Ljava/lang/String;

    .line 722
    .line 723
    invoke-virtual {v0, v3}, Lcom/intsig/pay/base/model/PayOrderRequest;->setTimestamp(Ljava/lang/String;)V

    .line 724
    .line 725
    .line 726
    iget-object v3, v1, Lcom/intsig/comm/purchase/entity/SignOrderResult$Data;->sign:Ljava/lang/String;

    .line 727
    .line 728
    invoke-virtual {v0, v3}, Lcom/intsig/pay/base/model/PayOrderRequest;->setSign(Ljava/lang/String;)V

    .line 729
    .line 730
    .line 731
    iget-object v1, v1, Lcom/intsig/comm/purchase/entity/SignOrderResult$Data;->pay_param:Ljava/lang/String;

    .line 732
    .line 733
    invoke-virtual {v0, v1}, Lcom/intsig/pay/base/model/PayOrderRequest;->setPay_param(Ljava/lang/String;)V

    .line 734
    .line 735
    .line 736
    :cond_15
    iput-object v11, v2, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$chain$1;->o0:Ljava/lang/Object;

    .line 737
    .line 738
    iput-object v11, v2, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 739
    .line 740
    iput-object v11, v2, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$chain$1;->OO:Ljava/lang/Object;

    .line 741
    .line 742
    iput-object v11, v2, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$chain$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 743
    .line 744
    iput v8, v2, Lcom/intsig/camscanner/purchase/pay/task/CreateOrderTask$chain$1;->〇080OO8〇0:I

    .line 745
    .line 746
    invoke-virtual {v12, v14, v2}, Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;->〇o00〇〇Oo(Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 747
    .line 748
    .line 749
    move-result-object v0

    .line 750
    if-ne v0, v4, :cond_16

    .line 751
    .line 752
    return-object v4

    .line 753
    :cond_16
    :goto_e
    return-object v0

    .line 754
    :cond_17
    :goto_f
    const-string v0, "get_sign failed"

    .line 755
    .line 756
    invoke-static {v10, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 757
    .line 758
    .line 759
    const/16 v13, 0x271b

    .line 760
    .line 761
    const/4 v15, 0x0

    .line 762
    const/16 v16, 0x4

    .line 763
    .line 764
    const/16 v17, 0x0

    .line 765
    .line 766
    invoke-static/range {v12 .. v17}, Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;->O8(Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;ILcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;Lcom/intsig/pay/base/model/PayOrderResponse;ILjava/lang/Object;)Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;

    .line 767
    .line 768
    .line 769
    move-result-object v0

    .line 770
    return-object v0
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
.end method

.class public final Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;
.super Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;
.source "GPUpdateOrderTask.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final Oo08:Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8:Z

.field private 〇o〇:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;->Oo08:Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private final oO80(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/intsig/camscanner/purchase/track/PurchaseTracker;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    move-object/from16 v1, p0

    .line 2
    .line 3
    move-object/from16 v2, p2

    .line 4
    .line 5
    move-object/from16 v3, p3

    .line 6
    .line 7
    move-object/from16 v4, p4

    .line 8
    .line 9
    move-object/from16 v5, p5

    .line 10
    .line 11
    move-object/from16 v0, p6

    .line 12
    .line 13
    instance-of v6, v0, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;

    .line 14
    .line 15
    if-eqz v6, :cond_0

    .line 16
    .line 17
    move-object v6, v0

    .line 18
    check-cast v6, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;

    .line 19
    .line 20
    iget v7, v6, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;->oOo〇8o008:I

    .line 21
    .line 22
    const/high16 v8, -0x80000000

    .line 23
    .line 24
    and-int v9, v7, v8

    .line 25
    .line 26
    if-eqz v9, :cond_0

    .line 27
    .line 28
    sub-int/2addr v7, v8

    .line 29
    iput v7, v6, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;->oOo〇8o008:I

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_0
    new-instance v6, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;

    .line 33
    .line 34
    invoke-direct {v6, v1, v0}, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;-><init>(Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;Lkotlin/coroutines/Continuation;)V

    .line 35
    .line 36
    .line 37
    :goto_0
    move-object v13, v6

    .line 38
    iget-object v0, v13, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;->〇080OO8〇0:Ljava/lang/Object;

    .line 39
    .line 40
    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->O8()Ljava/lang/Object;

    .line 41
    .line 42
    .line 43
    move-result-object v6

    .line 44
    iget v7, v13, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;->oOo〇8o008:I

    .line 45
    .line 46
    const/16 v8, 0x194

    .line 47
    .line 48
    const/4 v9, 0x2

    .line 49
    const-string v10, "PurchaseHelper-GPUpdateOrderTask"

    .line 50
    .line 51
    const/4 v11, 0x0

    .line 52
    const/4 v12, 0x1

    .line 53
    if-eqz v7, :cond_3

    .line 54
    .line 55
    if-eq v7, v12, :cond_2

    .line 56
    .line 57
    if-ne v7, v9, :cond_1

    .line 58
    .line 59
    invoke-static {v0}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 60
    .line 61
    .line 62
    goto/16 :goto_f

    .line 63
    .line 64
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 65
    .line 66
    const-string v2, "call to \'resume\' before \'invoke\' with coroutine"

    .line 67
    .line 68
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 69
    .line 70
    .line 71
    throw v0

    .line 72
    :cond_2
    iget-object v2, v13, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;->O8o08O8O:Ljava/lang/Object;

    .line 73
    .line 74
    check-cast v2, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 75
    .line 76
    iget-object v3, v13, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;->o〇00O:Ljava/lang/Object;

    .line 77
    .line 78
    check-cast v3, Ljava/lang/String;

    .line 79
    .line 80
    iget-object v4, v13, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 81
    .line 82
    check-cast v4, Ljava/lang/String;

    .line 83
    .line 84
    iget-object v5, v13, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;->OO:Ljava/lang/Object;

    .line 85
    .line 86
    check-cast v5, Ljava/lang/String;

    .line 87
    .line 88
    iget-object v7, v13, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 89
    .line 90
    check-cast v7, Ljava/lang/String;

    .line 91
    .line 92
    iget-object v12, v13, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;->o0:Ljava/lang/Object;

    .line 93
    .line 94
    check-cast v12, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;

    .line 95
    .line 96
    invoke-static {v0}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 97
    .line 98
    .line 99
    move-object v9, v5

    .line 100
    move-object/from16 v16, v12

    .line 101
    .line 102
    move-object v12, v2

    .line 103
    move-object v2, v7

    .line 104
    move-object/from16 v7, v16

    .line 105
    .line 106
    move-object/from16 v17, v4

    .line 107
    .line 108
    move-object v4, v3

    .line 109
    move-object/from16 v3, v17

    .line 110
    .line 111
    goto/16 :goto_e

    .line 112
    .line 113
    :cond_3
    invoke-static {v0}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 114
    .line 115
    .line 116
    new-instance v7, Ljava/util/HashMap;

    .line 117
    .line 118
    const/16 v0, 0x14

    .line 119
    .line 120
    invoke-direct {v7, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 121
    .line 122
    .line 123
    const-string v0, "referer"

    .line 124
    .line 125
    const-string v14, "google_play_v2"

    .line 126
    .line 127
    invoke-interface {v7, v0, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    .line 129
    .line 130
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O0o()Ljava/lang/String;

    .line 131
    .line 132
    .line 133
    move-result-object v0

    .line 134
    const/4 v14, 0x0

    .line 135
    if-eqz v0, :cond_5

    .line 136
    .line 137
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 138
    .line 139
    .line 140
    move-result v15

    .line 141
    if-eqz v15, :cond_4

    .line 142
    .line 143
    goto :goto_1

    .line 144
    :cond_4
    const/4 v15, 0x0

    .line 145
    goto :goto_2

    .line 146
    :cond_5
    :goto_1
    const/4 v15, 0x1

    .line 147
    :goto_2
    if-nez v15, :cond_6

    .line 148
    .line 149
    const-string/jumbo v15, "userId"

    .line 150
    .line 151
    .line 152
    invoke-static {v0, v15}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 153
    .line 154
    .line 155
    const-string/jumbo v15, "user_id"

    .line 156
    .line 157
    .line 158
    invoke-interface {v7, v15, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    .line 160
    .line 161
    :cond_6
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->OOo0O()Ljava/lang/String;

    .line 162
    .line 163
    .line 164
    move-result-object v0

    .line 165
    if-eqz v0, :cond_8

    .line 166
    .line 167
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 168
    .line 169
    .line 170
    move-result v15

    .line 171
    if-eqz v15, :cond_7

    .line 172
    .line 173
    goto :goto_3

    .line 174
    :cond_7
    const/4 v15, 0x0

    .line 175
    goto :goto_4

    .line 176
    :cond_8
    :goto_3
    const/4 v15, 0x1

    .line 177
    :goto_4
    if-nez v15, :cond_9

    .line 178
    .line 179
    const-string/jumbo v15, "token"

    .line 180
    .line 181
    .line 182
    invoke-static {v0, v15}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 183
    .line 184
    .line 185
    invoke-interface {v7, v15, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    .line 187
    .line 188
    :cond_9
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇〇888()Ljava/lang/String;

    .line 189
    .line 190
    .line 191
    move-result-object v0

    .line 192
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 193
    .line 194
    .line 195
    move-result v15

    .line 196
    if-nez v15, :cond_a

    .line 197
    .line 198
    const-string v15, "cs_ept_d"

    .line 199
    .line 200
    invoke-interface {v7, v15, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    .line 202
    .line 203
    :cond_a
    if-eqz v5, :cond_15

    .line 204
    .line 205
    iget-object v0, v5, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 206
    .line 207
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/entity/Function;->toTrackerValue()Ljava/lang/String;

    .line 208
    .line 209
    .line 210
    move-result-object v0

    .line 211
    if-eqz v0, :cond_c

    .line 212
    .line 213
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 214
    .line 215
    .line 216
    move-result v15

    .line 217
    if-eqz v15, :cond_b

    .line 218
    .line 219
    goto :goto_5

    .line 220
    :cond_b
    const/4 v15, 0x0

    .line 221
    goto :goto_6

    .line 222
    :cond_c
    :goto_5
    const/4 v15, 0x1

    .line 223
    :goto_6
    if-nez v15, :cond_d

    .line 224
    .line 225
    const-string v15, "payFrom"

    .line 226
    .line 227
    invoke-static {v0, v15}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 228
    .line 229
    .line 230
    const-string v15, "pay_from"

    .line 231
    .line 232
    invoke-interface {v7, v15, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 233
    .line 234
    .line 235
    :cond_d
    iget-object v0, v5, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 236
    .line 237
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->toTrackerValue()Ljava/lang/String;

    .line 238
    .line 239
    .line 240
    move-result-object v0

    .line 241
    if-eqz v0, :cond_f

    .line 242
    .line 243
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 244
    .line 245
    .line 246
    move-result v15

    .line 247
    if-eqz v15, :cond_e

    .line 248
    .line 249
    goto :goto_7

    .line 250
    :cond_e
    const/4 v15, 0x0

    .line 251
    goto :goto_8

    .line 252
    :cond_f
    :goto_7
    const/4 v15, 0x1

    .line 253
    :goto_8
    if-nez v15, :cond_10

    .line 254
    .line 255
    const-string v15, "payScheme"

    .line 256
    .line 257
    invoke-static {v0, v15}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 258
    .line 259
    .line 260
    const-string v15, "pay_scheme"

    .line 261
    .line 262
    invoke-interface {v7, v15, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 263
    .line 264
    .line 265
    :cond_10
    iget-object v0, v5, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 266
    .line 267
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 268
    .line 269
    .line 270
    move-result-object v0

    .line 271
    if-eqz v0, :cond_12

    .line 272
    .line 273
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 274
    .line 275
    .line 276
    move-result v15

    .line 277
    if-eqz v15, :cond_11

    .line 278
    .line 279
    goto :goto_9

    .line 280
    :cond_11
    const/4 v15, 0x0

    .line 281
    goto :goto_a

    .line 282
    :cond_12
    :goto_9
    const/4 v15, 0x1

    .line 283
    :goto_a
    if-nez v15, :cond_13

    .line 284
    .line 285
    const-string v15, "payFromPart"

    .line 286
    .line 287
    invoke-static {v0, v15}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 288
    .line 289
    .line 290
    const-string v15, "pay_from_part"

    .line 291
    .line 292
    invoke-interface {v7, v15, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 293
    .line 294
    .line 295
    :cond_13
    iget-object v0, v5, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 296
    .line 297
    sget-object v15, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->CSGuidePremium:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 298
    .line 299
    if-ne v0, v15, :cond_14

    .line 300
    .line 301
    const-string v0, "1"

    .line 302
    .line 303
    goto :goto_b

    .line 304
    :cond_14
    const-string v0, "0"

    .line 305
    .line 306
    :goto_b
    const-string v15, "guide"

    .line 307
    .line 308
    invoke-interface {v7, v15, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    .line 310
    .line 311
    :cond_15
    invoke-static {}, Lcom/intsig/utils/LanguageUtil;->O8()Ljava/lang/String;

    .line 312
    .line 313
    .line 314
    move-result-object v0

    .line 315
    const-string v15, "getLocalCountry()"

    .line 316
    .line 317
    invoke-static {v0, v15}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 318
    .line 319
    .line 320
    const-string v15, "country"

    .line 321
    .line 322
    invoke-interface {v7, v15, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 323
    .line 324
    .line 325
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->O〇0()Ljava/lang/String;

    .line 326
    .line 327
    .line 328
    move-result-object v0

    .line 329
    const-string v15, "getLocalLang()"

    .line 330
    .line 331
    invoke-static {v0, v15}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 332
    .line 333
    .line 334
    const-string v15, "language"

    .line 335
    .line 336
    invoke-interface {v7, v15, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 337
    .line 338
    .line 339
    invoke-static {}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->OO8oO0o〇()Ljava/lang/String;

    .line 340
    .line 341
    .line 342
    move-result-object v0

    .line 343
    const-string v15, "getClient()"

    .line 344
    .line 345
    invoke-static {v0, v15}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 346
    .line 347
    .line 348
    const-string v15, "client"

    .line 349
    .line 350
    invoke-interface {v7, v15, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 351
    .line 352
    .line 353
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 354
    .line 355
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 356
    .line 357
    .line 358
    move-result-object v15

    .line 359
    invoke-static {v15}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇0(Landroid/content/Context;)Ljava/lang/String;

    .line 360
    .line 361
    .line 362
    move-result-object v15

    .line 363
    const-string v9, "getClientId(ApplicationHelper.sContext)"

    .line 364
    .line 365
    invoke-static {v15, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 366
    .line 367
    .line 368
    const-string v9, "clientId"

    .line 369
    .line 370
    invoke-interface {v7, v9, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 371
    .line 372
    .line 373
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 374
    .line 375
    .line 376
    move-result-object v0

    .line 377
    invoke-static {v0}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o0O0(Landroid/content/Context;)Ljava/lang/String;

    .line 378
    .line 379
    .line 380
    move-result-object v0

    .line 381
    const-string v9, "getClientApp(ApplicationHelper.sContext)"

    .line 382
    .line 383
    invoke-static {v0, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 384
    .line 385
    .line 386
    const-string v9, "client_app"

    .line 387
    .line 388
    invoke-interface {v7, v9, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389
    .line 390
    .line 391
    const-string/jumbo v0, "signature"

    .line 392
    .line 393
    .line 394
    invoke-interface {v7, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 395
    .line 396
    .line 397
    const-string v0, "gp_version"

    .line 398
    .line 399
    const-string v9, "3"

    .line 400
    .line 401
    invoke-interface {v7, v0, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402
    .line 403
    .line 404
    new-instance v9, Lorg/json/JSONObject;

    .line 405
    .line 406
    invoke-direct {v9}, Lorg/json/JSONObject;-><init>()V

    .line 407
    .line 408
    .line 409
    :try_start_0
    const-string v0, "receipt"

    .line 410
    .line 411
    invoke-virtual {v9, v0, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 412
    .line 413
    .line 414
    invoke-static {v2, v4}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->〇O00(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 415
    .line 416
    .line 417
    move-result-object v0

    .line 418
    const-string v15, "developerPayload"

    .line 419
    .line 420
    invoke-virtual {v9, v15, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 421
    .line 422
    .line 423
    goto :goto_c

    .line 424
    :catch_0
    move-exception v0

    .line 425
    invoke-static {v10, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 426
    .line 427
    .line 428
    :goto_c
    invoke-virtual {v9}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    .line 429
    .line 430
    .line 431
    move-result-object v0

    .line 432
    const-string v9, "bodyJson.toString()"

    .line 433
    .line 434
    invoke-static {v0, v9}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 435
    .line 436
    .line 437
    new-instance v9, Ljava/lang/StringBuilder;

    .line 438
    .line 439
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 440
    .line 441
    .line 442
    const-string v15, "RequestBody = "

    .line 443
    .line 444
    invoke-virtual {v9, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 445
    .line 446
    .line 447
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 448
    .line 449
    .line 450
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 451
    .line 452
    .line 453
    move-result-object v9

    .line 454
    invoke-static {v10, v9}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 455
    .line 456
    .line 457
    new-instance v9, Ljava/lang/StringBuilder;

    .line 458
    .line 459
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 460
    .line 461
    .line 462
    const-string v15, "inAppDataSignature = "

    .line 463
    .line 464
    invoke-virtual {v9, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 465
    .line 466
    .line 467
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 468
    .line 469
    .line 470
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 471
    .line 472
    .line 473
    move-result-object v9

    .line 474
    invoke-static {v10, v9}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    .line 476
    .line 477
    sget-object v9, Lcom/intsig/camscanner/purchase/pay/task/PayApi;->〇080:Lcom/intsig/camscanner/purchase/pay/task/PayApi;

    .line 478
    .line 479
    const-string v15, "/update_vip_property"

    .line 480
    .line 481
    invoke-virtual {v9, v15}, Lcom/intsig/camscanner/purchase/pay/task/PayApi;->〇o00〇〇Oo(Ljava/lang/String;)Ljava/lang/String;

    .line 482
    .line 483
    .line 484
    move-result-object v9

    .line 485
    :try_start_1
    invoke-interface {v7}, Ljava/util/Map;->isEmpty()Z

    .line 486
    .line 487
    .line 488
    move-result v15

    .line 489
    if-nez v15, :cond_16

    .line 490
    .line 491
    invoke-static {v9}, Lcom/lzy/okgo/OkGo;->post(Ljava/lang/String;)Lcom/lzy/okgo/request/PostRequest;

    .line 492
    .line 493
    .line 494
    move-result-object v9

    .line 495
    invoke-virtual {v9, v12}, Lcom/lzy/okgo/request/base/BodyRequest;->isSpliceUrl(Z)Lcom/lzy/okgo/request/base/BodyRequest;

    .line 496
    .line 497
    .line 498
    move-result-object v9

    .line 499
    check-cast v9, Lcom/lzy/okgo/request/PostRequest;

    .line 500
    .line 501
    new-array v15, v14, [Z

    .line 502
    .line 503
    invoke-virtual {v9, v7, v15}, Lcom/lzy/okgo/request/base/Request;->params(Ljava/util/Map;[Z)Lcom/lzy/okgo/request/base/Request;

    .line 504
    .line 505
    .line 506
    move-result-object v7

    .line 507
    check-cast v7, Lcom/lzy/okgo/request/PostRequest;

    .line 508
    .line 509
    sget-object v9, Lokhttp3/RequestBody;->Companion:Lokhttp3/RequestBody$Companion;

    .line 510
    .line 511
    invoke-static {v9, v0, v11, v12, v11}, Lokhttp3/RequestBody$Companion;->〇80〇808〇O(Lokhttp3/RequestBody$Companion;Ljava/lang/String;Lokhttp3/MediaType;ILjava/lang/Object;)Lokhttp3/RequestBody;

    .line 512
    .line 513
    .line 514
    move-result-object v0

    .line 515
    invoke-virtual {v7, v0}, Lcom/lzy/okgo/request/base/BodyRequest;->upRequestBody(Lokhttp3/RequestBody;)Lcom/lzy/okgo/request/base/BodyRequest;

    .line 516
    .line 517
    .line 518
    move-result-object v0

    .line 519
    check-cast v0, Lcom/lzy/okgo/request/PostRequest;

    .line 520
    .line 521
    invoke-virtual {v0}, Lcom/lzy/okgo/request/base/Request;->execute()Lokhttp3/Response;

    .line 522
    .line 523
    .line 524
    move-result-object v0

    .line 525
    goto :goto_d

    .line 526
    :cond_16
    invoke-static {v9}, Lcom/lzy/okgo/OkGo;->post(Ljava/lang/String;)Lcom/lzy/okgo/request/PostRequest;

    .line 527
    .line 528
    .line 529
    move-result-object v7

    .line 530
    sget-object v9, Lokhttp3/RequestBody;->Companion:Lokhttp3/RequestBody$Companion;

    .line 531
    .line 532
    invoke-static {v9, v0, v11, v12, v11}, Lokhttp3/RequestBody$Companion;->〇80〇808〇O(Lokhttp3/RequestBody$Companion;Ljava/lang/String;Lokhttp3/MediaType;ILjava/lang/Object;)Lokhttp3/RequestBody;

    .line 533
    .line 534
    .line 535
    move-result-object v0

    .line 536
    invoke-virtual {v7, v0}, Lcom/lzy/okgo/request/base/BodyRequest;->upRequestBody(Lokhttp3/RequestBody;)Lcom/lzy/okgo/request/base/BodyRequest;

    .line 537
    .line 538
    .line 539
    move-result-object v0

    .line 540
    check-cast v0, Lcom/lzy/okgo/request/PostRequest;

    .line 541
    .line 542
    invoke-virtual {v0}, Lcom/lzy/okgo/request/base/Request;->execute()Lokhttp3/Response;

    .line 543
    .line 544
    .line 545
    move-result-object v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 546
    goto :goto_d

    .line 547
    :catch_1
    move-exception v0

    .line 548
    const-string v7, "PurchaseHelper-PayApi"

    .line 549
    .line 550
    invoke-static {v7, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 551
    .line 552
    .line 553
    move-object v0, v11

    .line 554
    :goto_d
    if-nez v0, :cond_17

    .line 555
    .line 556
    invoke-static {v8}, Lkotlin/coroutines/jvm/internal/Boxing;->〇o〇(I)Ljava/lang/Integer;

    .line 557
    .line 558
    .line 559
    move-result-object v0

    .line 560
    return-object v0

    .line 561
    :cond_17
    invoke-virtual {v0}, Lokhttp3/Response;->〇oo〇()Z

    .line 562
    .line 563
    .line 564
    move-result v7

    .line 565
    if-eqz v7, :cond_18

    .line 566
    .line 567
    invoke-static {v14}, Lkotlin/coroutines/jvm/internal/Boxing;->〇o〇(I)Ljava/lang/Integer;

    .line 568
    .line 569
    .line 570
    move-result-object v0

    .line 571
    return-object v0

    .line 572
    :cond_18
    sget-object v7, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 573
    .line 574
    invoke-virtual {v7}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 575
    .line 576
    .line 577
    move-result-object v7

    .line 578
    invoke-virtual {v0}, Lokhttp3/Response;->O〇8O8〇008()Ljava/lang/String;

    .line 579
    .line 580
    .line 581
    move-result-object v9

    .line 582
    new-instance v14, Ljava/lang/StringBuilder;

    .line 583
    .line 584
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    .line 585
    .line 586
    .line 587
    const-string/jumbo v15, "updateVipProperty Exception= "

    .line 588
    .line 589
    .line 590
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 591
    .line 592
    .line 593
    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 594
    .line 595
    .line 596
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 597
    .line 598
    .line 599
    move-result-object v9

    .line 600
    invoke-static {v7, v9}, Lcom/intsig/camscanner/log/LogTrackerUserData;->〇80〇808〇O(Landroid/content/Context;Ljava/lang/String;)V

    .line 601
    .line 602
    .line 603
    invoke-virtual {v0}, Lokhttp3/Response;->〇O8o08O()I

    .line 604
    .line 605
    .line 606
    move-result v7

    .line 607
    const/16 v9, 0x196

    .line 608
    .line 609
    if-ne v7, v9, :cond_1c

    .line 610
    .line 611
    invoke-virtual {v0}, Lokhttp3/Response;->o800o8O()Lokhttp3/Headers;

    .line 612
    .line 613
    .line 614
    move-result-object v0

    .line 615
    const-string v7, "X-IS-Error-Code"

    .line 616
    .line 617
    invoke-virtual {v0, v7}, Lokhttp3/Headers;->〇080(Ljava/lang/String;)Ljava/lang/String;

    .line 618
    .line 619
    .line 620
    move-result-object v0

    .line 621
    const-string v7, "502"

    .line 622
    .line 623
    invoke-static {v7, v0}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 624
    .line 625
    .line 626
    move-result v7

    .line 627
    if-eqz v7, :cond_19

    .line 628
    .line 629
    const/16 v0, 0x1f6

    .line 630
    .line 631
    invoke-static {v0}, Lkotlin/coroutines/jvm/internal/Boxing;->〇o〇(I)Ljava/lang/Integer;

    .line 632
    .line 633
    .line 634
    move-result-object v0

    .line 635
    return-object v0

    .line 636
    :cond_19
    const-string v7, "10010002"

    .line 637
    .line 638
    invoke-static {v7, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    .line 639
    .line 640
    .line 641
    move-result v0

    .line 642
    if-eqz v0, :cond_1c

    .line 643
    .line 644
    iget-boolean v0, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;->O8:Z

    .line 645
    .line 646
    if-nez v0, :cond_1c

    .line 647
    .line 648
    iput-boolean v12, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;->O8:Z

    .line 649
    .line 650
    sget-object v0, Lcom/intsig/camscanner/message/ApiChangeReqWrapper;->〇080:Lcom/intsig/camscanner/message/ApiChangeReqWrapper;

    .line 651
    .line 652
    iput-object v1, v13, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;->o0:Ljava/lang/Object;

    .line 653
    .line 654
    move-object/from16 v7, p1

    .line 655
    .line 656
    iput-object v7, v13, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 657
    .line 658
    iput-object v2, v13, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;->OO:Ljava/lang/Object;

    .line 659
    .line 660
    iput-object v3, v13, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 661
    .line 662
    iput-object v4, v13, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;->o〇00O:Ljava/lang/Object;

    .line 663
    .line 664
    iput-object v5, v13, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;->O8o08O8O:Ljava/lang/Object;

    .line 665
    .line 666
    iput v12, v13, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;->oOo〇8o008:I

    .line 667
    .line 668
    invoke-virtual {v0, v13}, Lcom/intsig/camscanner/message/ApiChangeReqWrapper;->o〇0(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 669
    .line 670
    .line 671
    move-result-object v0

    .line 672
    if-ne v0, v6, :cond_1a

    .line 673
    .line 674
    return-object v6

    .line 675
    :cond_1a
    move-object v9, v2

    .line 676
    move-object v12, v5

    .line 677
    move-object v2, v7

    .line 678
    move-object v7, v1

    .line 679
    :goto_e
    check-cast v0, Ljava/lang/Boolean;

    .line 680
    .line 681
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    .line 682
    .line 683
    .line 684
    move-result v0

    .line 685
    new-instance v5, Ljava/lang/StringBuilder;

    .line 686
    .line 687
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 688
    .line 689
    .line 690
    const-string v14, "apis error occur, isApisOk = "

    .line 691
    .line 692
    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 693
    .line 694
    .line 695
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 696
    .line 697
    .line 698
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 699
    .line 700
    .line 701
    move-result-object v5

    .line 702
    invoke-static {v10, v5}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    .line 704
    .line 705
    if-eqz v0, :cond_1c

    .line 706
    .line 707
    iput-object v11, v13, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;->o0:Ljava/lang/Object;

    .line 708
    .line 709
    iput-object v11, v13, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 710
    .line 711
    iput-object v11, v13, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;->OO:Ljava/lang/Object;

    .line 712
    .line 713
    iput-object v11, v13, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 714
    .line 715
    iput-object v11, v13, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;->o〇00O:Ljava/lang/Object;

    .line 716
    .line 717
    iput-object v11, v13, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;->O8o08O8O:Ljava/lang/Object;

    .line 718
    .line 719
    const/4 v5, 0x2

    .line 720
    iput v5, v13, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$updatePayOrderVip$1;->oOo〇8o008:I

    .line 721
    .line 722
    move-object v8, v2

    .line 723
    move-object v10, v3

    .line 724
    move-object v11, v4

    .line 725
    invoke-direct/range {v7 .. v13}, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;->oO80(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 726
    .line 727
    .line 728
    move-result-object v0

    .line 729
    if-ne v0, v6, :cond_1b

    .line 730
    .line 731
    return-object v6

    .line 732
    :cond_1b
    :goto_f
    return-object v0

    .line 733
    :cond_1c
    invoke-static {v8}, Lkotlin/coroutines/jvm/internal/Boxing;->〇o〇(I)Ljava/lang/Integer;

    .line 734
    .line 735
    .line 736
    move-result-object v0

    .line 737
    return-object v0
.end method

.method public static final synthetic o〇0(Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-direct/range {p0 .. p6}, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;->oO80(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
.end method

.method private final 〇〇888(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    const-string/jumbo p6, "sign"

    .line 2
    .line 3
    .line 4
    const-string v0, "PurchaseHelper-GPUpdateOrderTask"

    .line 5
    .line 6
    new-instance v1, Lorg/json/JSONObject;

    .line 7
    .line 8
    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 9
    .line 10
    .line 11
    :try_start_0
    const-string v2, "client_app"

    .line 12
    .line 13
    sget-object v3, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 14
    .line 15
    invoke-virtual {v3}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 16
    .line 17
    .line 18
    move-result-object v4

    .line 19
    invoke-static {v4}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->o0O0(Landroid/content/Context;)Ljava/lang/String;

    .line 20
    .line 21
    .line 22
    move-result-object v4

    .line 23
    invoke-virtual {v1, v2, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 24
    .line 25
    .line 26
    const-string v2, "property"

    .line 27
    .line 28
    invoke-virtual {v1, v2, p5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 29
    .line 30
    .line 31
    invoke-virtual {v1, p6, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 32
    .line 33
    .line 34
    const-string p5, "receipt"

    .line 35
    .line 36
    invoke-virtual {v1, p5, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 37
    .line 38
    .line 39
    const-string/jumbo p5, "user_id"

    .line 40
    .line 41
    .line 42
    invoke-virtual {v3}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 43
    .line 44
    .line 45
    move-result-object v2

    .line 46
    invoke-static {v2}, Lcom/intsig/camscanner/tsapp/sync/SyncUtil;->〇o〇8(Landroid/content/Context;)Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v2

    .line 50
    invoke-virtual {v1, p5, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 51
    .line 52
    .line 53
    invoke-static {p2, p4}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->〇O00(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object p4

    .line 57
    const-string p5, "developerPayload"

    .line 58
    .line 59
    invoke-virtual {v1, p5, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    .line 61
    .line 62
    goto :goto_0

    .line 63
    :catch_0
    move-exception p4

    .line 64
    invoke-static {v0, p4}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 65
    .line 66
    .line 67
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object p4

    .line 71
    const-string p5, "bodyJson.toString()"

    .line 72
    .line 73
    invoke-static {p4, p5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 74
    .line 75
    .line 76
    invoke-static {p4}, Lcom/intsig/tianshu/TianShuAPI;->OO88〇OOO(Ljava/lang/String;)Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object p5

    .line 80
    new-instance v1, Ljava/util/HashMap;

    .line 81
    .line 82
    const/4 v2, 0x4

    .line 83
    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 84
    .line 85
    .line 86
    invoke-static {p5, p6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 87
    .line 88
    .line 89
    invoke-interface {v1, p6, p5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    .line 91
    .line 92
    const-string p6, "seqid"

    .line 93
    .line 94
    invoke-interface {v1, p6, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    .line 96
    .line 97
    const-string p6, "gp_version"

    .line 98
    .line 99
    const-string v2, "3"

    .line 100
    .line 101
    invoke-interface {v1, p6, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    .line 103
    .line 104
    invoke-static {}, Lcom/intsig/tianshu/TianShuAPI;->OOo0O()Ljava/lang/String;

    .line 105
    .line 106
    .line 107
    move-result-object p6

    .line 108
    const/4 v2, 0x0

    .line 109
    const/4 v3, 0x1

    .line 110
    if-eqz p6, :cond_1

    .line 111
    .line 112
    invoke-static {p6}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 113
    .line 114
    .line 115
    move-result v4

    .line 116
    if-eqz v4, :cond_0

    .line 117
    .line 118
    goto :goto_1

    .line 119
    :cond_0
    const/4 v4, 0x0

    .line 120
    goto :goto_2

    .line 121
    :cond_1
    :goto_1
    const/4 v4, 0x1

    .line 122
    :goto_2
    if-nez v4, :cond_2

    .line 123
    .line 124
    const-string/jumbo v4, "token"

    .line 125
    .line 126
    .line 127
    invoke-static {p6, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    .line 129
    .line 130
    invoke-interface {v1, v4, p6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    .line 132
    .line 133
    :cond_2
    invoke-static {}, Lcom/intsig/utils/ApplicationHelper;->〇〇888()Ljava/lang/String;

    .line 134
    .line 135
    .line 136
    move-result-object p6

    .line 137
    invoke-static {p6}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 138
    .line 139
    .line 140
    move-result v4

    .line 141
    xor-int/2addr v4, v3

    .line 142
    if-eqz v4, :cond_3

    .line 143
    .line 144
    const-string v4, "cs_ept_d"

    .line 145
    .line 146
    invoke-interface {v1, v4, p6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    .line 148
    .line 149
    :cond_3
    new-instance p6, Ljava/lang/StringBuilder;

    .line 150
    .line 151
    invoke-direct {p6}, Ljava/lang/StringBuilder;-><init>()V

    .line 152
    .line 153
    .line 154
    const-string v4, "RequestBody = "

    .line 155
    .line 156
    invoke-virtual {p6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    .line 158
    .line 159
    invoke-virtual {p6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    .line 161
    .line 162
    invoke-virtual {p6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 163
    .line 164
    .line 165
    move-result-object p2

    .line 166
    invoke-static {v0, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    .line 168
    .line 169
    new-instance p2, Ljava/lang/StringBuilder;

    .line 170
    .line 171
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 172
    .line 173
    .line 174
    const-string p6, "inAppDataSignature = "

    .line 175
    .line 176
    invoke-virtual {p2, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 177
    .line 178
    .line 179
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    .line 181
    .line 182
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 183
    .line 184
    .line 185
    move-result-object p2

    .line 186
    invoke-static {v0, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    .line 188
    .line 189
    new-instance p2, Ljava/lang/StringBuilder;

    .line 190
    .line 191
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 192
    .line 193
    .line 194
    const-string p3, "pay/googleplay?sign= "

    .line 195
    .line 196
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    .line 198
    .line 199
    invoke-virtual {p2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    .line 201
    .line 202
    const-string p3, "  seqid(MD5\uff09="

    .line 203
    .line 204
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    .line 206
    .line 207
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    .line 209
    .line 210
    const-string p1, " body = "

    .line 211
    .line 212
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 213
    .line 214
    .line 215
    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 216
    .line 217
    .line 218
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 219
    .line 220
    .line 221
    move-result-object p1

    .line 222
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    .line 224
    .line 225
    sget-object p1, Lcom/intsig/camscanner/purchase/pay/task/PayApi;->〇080:Lcom/intsig/camscanner/purchase/pay/task/PayApi;

    .line 226
    .line 227
    const-string p2, "/pay/googleplay"

    .line 228
    .line 229
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/purchase/pay/task/PayApi;->〇o00〇〇Oo(Ljava/lang/String;)Ljava/lang/String;

    .line 230
    .line 231
    .line 232
    move-result-object p1

    .line 233
    const/4 p2, 0x0

    .line 234
    :try_start_1
    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    .line 235
    .line 236
    .line 237
    move-result p3

    .line 238
    if-nez p3, :cond_4

    .line 239
    .line 240
    invoke-static {p1}, Lcom/lzy/okgo/OkGo;->post(Ljava/lang/String;)Lcom/lzy/okgo/request/PostRequest;

    .line 241
    .line 242
    .line 243
    move-result-object p1

    .line 244
    invoke-virtual {p1, v3}, Lcom/lzy/okgo/request/base/BodyRequest;->isSpliceUrl(Z)Lcom/lzy/okgo/request/base/BodyRequest;

    .line 245
    .line 246
    .line 247
    move-result-object p1

    .line 248
    check-cast p1, Lcom/lzy/okgo/request/PostRequest;

    .line 249
    .line 250
    new-array p3, v2, [Z

    .line 251
    .line 252
    invoke-virtual {p1, v1, p3}, Lcom/lzy/okgo/request/base/Request;->params(Ljava/util/Map;[Z)Lcom/lzy/okgo/request/base/Request;

    .line 253
    .line 254
    .line 255
    move-result-object p1

    .line 256
    check-cast p1, Lcom/lzy/okgo/request/PostRequest;

    .line 257
    .line 258
    sget-object p3, Lokhttp3/RequestBody;->Companion:Lokhttp3/RequestBody$Companion;

    .line 259
    .line 260
    invoke-static {p3, p4, p2, v3, p2}, Lokhttp3/RequestBody$Companion;->〇80〇808〇O(Lokhttp3/RequestBody$Companion;Ljava/lang/String;Lokhttp3/MediaType;ILjava/lang/Object;)Lokhttp3/RequestBody;

    .line 261
    .line 262
    .line 263
    move-result-object p3

    .line 264
    invoke-virtual {p1, p3}, Lcom/lzy/okgo/request/base/BodyRequest;->upRequestBody(Lokhttp3/RequestBody;)Lcom/lzy/okgo/request/base/BodyRequest;

    .line 265
    .line 266
    .line 267
    move-result-object p1

    .line 268
    check-cast p1, Lcom/lzy/okgo/request/PostRequest;

    .line 269
    .line 270
    invoke-virtual {p1}, Lcom/lzy/okgo/request/base/Request;->execute()Lokhttp3/Response;

    .line 271
    .line 272
    .line 273
    move-result-object p1

    .line 274
    goto :goto_3

    .line 275
    :cond_4
    invoke-static {p1}, Lcom/lzy/okgo/OkGo;->post(Ljava/lang/String;)Lcom/lzy/okgo/request/PostRequest;

    .line 276
    .line 277
    .line 278
    move-result-object p1

    .line 279
    sget-object p3, Lokhttp3/RequestBody;->Companion:Lokhttp3/RequestBody$Companion;

    .line 280
    .line 281
    invoke-static {p3, p4, p2, v3, p2}, Lokhttp3/RequestBody$Companion;->〇80〇808〇O(Lokhttp3/RequestBody$Companion;Ljava/lang/String;Lokhttp3/MediaType;ILjava/lang/Object;)Lokhttp3/RequestBody;

    .line 282
    .line 283
    .line 284
    move-result-object p3

    .line 285
    invoke-virtual {p1, p3}, Lcom/lzy/okgo/request/base/BodyRequest;->upRequestBody(Lokhttp3/RequestBody;)Lcom/lzy/okgo/request/base/BodyRequest;

    .line 286
    .line 287
    .line 288
    move-result-object p1

    .line 289
    check-cast p1, Lcom/lzy/okgo/request/PostRequest;

    .line 290
    .line 291
    invoke-virtual {p1}, Lcom/lzy/okgo/request/base/Request;->execute()Lokhttp3/Response;

    .line 292
    .line 293
    .line 294
    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 295
    :goto_3
    move-object p2, p1

    .line 296
    goto :goto_4

    .line 297
    :catch_1
    move-exception p1

    .line 298
    const-string p3, "PurchaseHelper-PayApi"

    .line 299
    .line 300
    invoke-static {p3, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 301
    .line 302
    .line 303
    :goto_4
    const/16 p1, 0x194

    .line 304
    .line 305
    if-nez p2, :cond_5

    .line 306
    .line 307
    invoke-static {p1}, Lkotlin/coroutines/jvm/internal/Boxing;->〇o〇(I)Ljava/lang/Integer;

    .line 308
    .line 309
    .line 310
    move-result-object p1

    .line 311
    return-object p1

    .line 312
    :cond_5
    invoke-virtual {p2}, Lokhttp3/Response;->〇oo〇()Z

    .line 313
    .line 314
    .line 315
    move-result p3

    .line 316
    if-eqz p3, :cond_6

    .line 317
    .line 318
    invoke-static {v2}, Lkotlin/coroutines/jvm/internal/Boxing;->〇o〇(I)Ljava/lang/Integer;

    .line 319
    .line 320
    .line 321
    move-result-object p1

    .line 322
    return-object p1

    .line 323
    :cond_6
    sget-object p3, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 324
    .line 325
    invoke-virtual {p3}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 326
    .line 327
    .line 328
    move-result-object p3

    .line 329
    invoke-virtual {p2}, Lokhttp3/Response;->O〇8O8〇008()Ljava/lang/String;

    .line 330
    .line 331
    .line 332
    move-result-object p2

    .line 333
    new-instance p4, Ljava/lang/StringBuilder;

    .line 334
    .line 335
    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    .line 336
    .line 337
    .line 338
    const-string/jumbo p5, "updateVipProperty Exception= "

    .line 339
    .line 340
    .line 341
    invoke-virtual {p4, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 342
    .line 343
    .line 344
    invoke-virtual {p4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 345
    .line 346
    .line 347
    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 348
    .line 349
    .line 350
    move-result-object p2

    .line 351
    invoke-static {p3, p2}, Lcom/intsig/camscanner/log/LogTrackerUserData;->〇80〇808〇O(Landroid/content/Context;Ljava/lang/String;)V

    .line 352
    .line 353
    .line 354
    invoke-static {p1}, Lkotlin/coroutines/jvm/internal/Boxing;->〇o〇(I)Ljava/lang/Integer;

    .line 355
    .line 356
    .line 357
    move-result-object p1

    .line 358
    return-object p1
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
.end method


# virtual methods
.method public 〇080(Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 23
    .param p1    # Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lkotlin/coroutines/Continuation;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    move-object/from16 v6, p0

    .line 2
    .line 3
    move-object/from16 v0, p2

    .line 4
    .line 5
    instance-of v1, v0, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;

    .line 6
    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    move-object v1, v0

    .line 10
    check-cast v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;

    .line 11
    .line 12
    iget v2, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->OO〇00〇8oO:I

    .line 13
    .line 14
    const/high16 v3, -0x80000000

    .line 15
    .line 16
    and-int v4, v2, v3

    .line 17
    .line 18
    if-eqz v4, :cond_0

    .line 19
    .line 20
    sub-int/2addr v2, v3

    .line 21
    iput v2, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->OO〇00〇8oO:I

    .line 22
    .line 23
    goto :goto_0

    .line 24
    :cond_0
    new-instance v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;

    .line 25
    .line 26
    invoke-direct {v1, v6, v0}, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;-><init>(Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;Lkotlin/coroutines/Continuation;)V

    .line 27
    .line 28
    .line 29
    :goto_0
    iget-object v0, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->oOo〇8o008:Ljava/lang/Object;

    .line 30
    .line 31
    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->O8()Ljava/lang/Object;

    .line 32
    .line 33
    .line 34
    move-result-object v2

    .line 35
    iget v3, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->OO〇00〇8oO:I

    .line 36
    .line 37
    const/4 v4, 0x1

    .line 38
    const-string v5, "PurchaseHelper-GPUpdateOrderTask"

    .line 39
    .line 40
    packed-switch v3, :pswitch_data_0

    .line 41
    .line 42
    .line 43
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 44
    .line 45
    const-string v1, "call to \'resume\' before \'invoke\' with coroutine"

    .line 46
    .line 47
    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 48
    .line 49
    .line 50
    throw v0

    .line 51
    :pswitch_0
    iget v3, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇0O:I

    .line 52
    .line 53
    iget-object v8, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇080OO8〇0:Ljava/lang/Object;

    .line 54
    .line 55
    check-cast v8, Ljava/lang/String;

    .line 56
    .line 57
    iget-object v9, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->O8o08O8O:Ljava/lang/Object;

    .line 58
    .line 59
    check-cast v9, Ljava/lang/String;

    .line 60
    .line 61
    iget-object v10, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->o〇00O:Ljava/lang/Object;

    .line 62
    .line 63
    check-cast v10, Ljava/lang/String;

    .line 64
    .line 65
    iget-object v11, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 66
    .line 67
    check-cast v11, Ljava/lang/String;

    .line 68
    .line 69
    iget-object v12, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->OO:Ljava/lang/Object;

    .line 70
    .line 71
    check-cast v12, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;

    .line 72
    .line 73
    iget-object v13, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 74
    .line 75
    check-cast v13, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;

    .line 76
    .line 77
    iget-object v14, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->o0:Ljava/lang/Object;

    .line 78
    .line 79
    check-cast v14, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;

    .line 80
    .line 81
    invoke-static {v0}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 82
    .line 83
    .line 84
    move-object v4, v5

    .line 85
    move-object v0, v12

    .line 86
    move-object v12, v14

    .line 87
    goto :goto_1

    .line 88
    :pswitch_1
    iget v3, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇0O:I

    .line 89
    .line 90
    iget-object v8, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇080OO8〇0:Ljava/lang/Object;

    .line 91
    .line 92
    check-cast v8, Ljava/lang/String;

    .line 93
    .line 94
    iget-object v9, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->O8o08O8O:Ljava/lang/Object;

    .line 95
    .line 96
    check-cast v9, Ljava/lang/String;

    .line 97
    .line 98
    iget-object v10, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->o〇00O:Ljava/lang/Object;

    .line 99
    .line 100
    check-cast v10, Ljava/lang/String;

    .line 101
    .line 102
    iget-object v11, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 103
    .line 104
    check-cast v11, Ljava/lang/String;

    .line 105
    .line 106
    iget-object v12, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->OO:Ljava/lang/Object;

    .line 107
    .line 108
    check-cast v12, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;

    .line 109
    .line 110
    iget-object v13, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 111
    .line 112
    check-cast v13, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;

    .line 113
    .line 114
    iget-object v14, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->o0:Ljava/lang/Object;

    .line 115
    .line 116
    check-cast v14, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;

    .line 117
    .line 118
    invoke-static {v0}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 119
    .line 120
    .line 121
    move-object v4, v5

    .line 122
    move-object v0, v12

    .line 123
    move-object v12, v14

    .line 124
    const/4 v6, 0x1

    .line 125
    :goto_1
    move-object v14, v13

    .line 126
    goto/16 :goto_d

    .line 127
    .line 128
    :pswitch_2
    invoke-static {v0}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 129
    .line 130
    .line 131
    goto/16 :goto_f

    .line 132
    .line 133
    :pswitch_3
    iget-object v3, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 134
    .line 135
    check-cast v3, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;

    .line 136
    .line 137
    iget-object v4, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->o0:Ljava/lang/Object;

    .line 138
    .line 139
    check-cast v4, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;

    .line 140
    .line 141
    invoke-static {v0}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 142
    .line 143
    .line 144
    const/4 v0, 0x0

    .line 145
    goto/16 :goto_e

    .line 146
    .line 147
    :pswitch_4
    iget v3, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇0O:I

    .line 148
    .line 149
    iget-object v8, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇080OO8〇0:Ljava/lang/Object;

    .line 150
    .line 151
    check-cast v8, Ljava/lang/String;

    .line 152
    .line 153
    iget-object v9, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->O8o08O8O:Ljava/lang/Object;

    .line 154
    .line 155
    check-cast v9, Ljava/lang/String;

    .line 156
    .line 157
    iget-object v10, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->o〇00O:Ljava/lang/Object;

    .line 158
    .line 159
    check-cast v10, Ljava/lang/String;

    .line 160
    .line 161
    iget-object v11, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 162
    .line 163
    check-cast v11, Ljava/lang/String;

    .line 164
    .line 165
    iget-object v12, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->OO:Ljava/lang/Object;

    .line 166
    .line 167
    check-cast v12, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;

    .line 168
    .line 169
    iget-object v13, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 170
    .line 171
    check-cast v13, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;

    .line 172
    .line 173
    iget-object v14, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->o0:Ljava/lang/Object;

    .line 174
    .line 175
    check-cast v14, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;

    .line 176
    .line 177
    invoke-static {v0}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 178
    .line 179
    .line 180
    goto/16 :goto_9

    .line 181
    .line 182
    :pswitch_5
    iget v3, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇0O:I

    .line 183
    .line 184
    iget-object v8, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇080OO8〇0:Ljava/lang/Object;

    .line 185
    .line 186
    check-cast v8, Ljava/lang/String;

    .line 187
    .line 188
    iget-object v9, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->O8o08O8O:Ljava/lang/Object;

    .line 189
    .line 190
    check-cast v9, Ljava/lang/String;

    .line 191
    .line 192
    iget-object v10, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->o〇00O:Ljava/lang/Object;

    .line 193
    .line 194
    check-cast v10, Ljava/lang/String;

    .line 195
    .line 196
    iget-object v11, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 197
    .line 198
    check-cast v11, Ljava/lang/String;

    .line 199
    .line 200
    iget-object v12, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->OO:Ljava/lang/Object;

    .line 201
    .line 202
    check-cast v12, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;

    .line 203
    .line 204
    iget-object v13, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 205
    .line 206
    check-cast v13, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;

    .line 207
    .line 208
    iget-object v14, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->o0:Ljava/lang/Object;

    .line 209
    .line 210
    check-cast v14, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;

    .line 211
    .line 212
    invoke-static {v0}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 213
    .line 214
    .line 215
    goto/16 :goto_b

    .line 216
    .line 217
    :pswitch_6
    invoke-static {v0}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 218
    .line 219
    .line 220
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->Oo08()Lcom/intsig/pay/base/model/PayOrderResponse;

    .line 221
    .line 222
    .line 223
    move-result-object v0

    .line 224
    if-eqz v0, :cond_1

    .line 225
    .line 226
    invoke-virtual {v0}, Lcom/intsig/pay/base/model/PayOrderResponse;->getInAppPurchaseData()Ljava/lang/String;

    .line 227
    .line 228
    .line 229
    move-result-object v0

    .line 230
    goto :goto_2

    .line 231
    :cond_1
    const/4 v0, 0x0

    .line 232
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->Oo08()Lcom/intsig/pay/base/model/PayOrderResponse;

    .line 233
    .line 234
    .line 235
    move-result-object v3

    .line 236
    if-eqz v3, :cond_2

    .line 237
    .line 238
    invoke-virtual {v3}, Lcom/intsig/pay/base/model/PayOrderResponse;->getInAppDataSignature()Ljava/lang/String;

    .line 239
    .line 240
    .line 241
    move-result-object v3

    .line 242
    goto :goto_3

    .line 243
    :cond_2
    const/4 v3, 0x0

    .line 244
    :goto_3
    const/4 v8, 0x0

    .line 245
    if-eqz v0, :cond_4

    .line 246
    .line 247
    invoke-static {v0}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 248
    .line 249
    .line 250
    move-result v9

    .line 251
    if-eqz v9, :cond_3

    .line 252
    .line 253
    goto :goto_4

    .line 254
    :cond_3
    const/4 v9, 0x0

    .line 255
    goto :goto_5

    .line 256
    :cond_4
    :goto_4
    const/4 v9, 0x1

    .line 257
    :goto_5
    if-nez v9, :cond_15

    .line 258
    .line 259
    if-eqz v3, :cond_6

    .line 260
    .line 261
    invoke-static {v3}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 262
    .line 263
    .line 264
    move-result v9

    .line 265
    if-eqz v9, :cond_5

    .line 266
    .line 267
    goto :goto_6

    .line 268
    :cond_5
    const/4 v9, 0x0

    .line 269
    goto :goto_7

    .line 270
    :cond_6
    :goto_6
    const/4 v9, 0x1

    .line 271
    :goto_7
    if-eqz v9, :cond_7

    .line 272
    .line 273
    goto/16 :goto_10

    .line 274
    .line 275
    :cond_7
    iput v8, v6, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;->〇o〇:I

    .line 276
    .line 277
    invoke-static {}, Lcom/intsig/tianshu/UUID;->〇o00〇〇Oo()Ljava/lang/String;

    .line 278
    .line 279
    .line 280
    move-result-object v8

    .line 281
    invoke-static {v8}, Lcom/intsig/tianshu/TianShuAPI;->oO0〇〇O8o(Ljava/lang/String;)Ljava/lang/String;

    .line 282
    .line 283
    .line 284
    move-result-object v8

    .line 285
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇〇888()Lcom/intsig/comm/purchase/entity/ProductResultItem;

    .line 286
    .line 287
    .line 288
    move-result-object v9

    .line 289
    iget-object v9, v9, Lcom/intsig/comm/purchase/entity/ProductResultItem;->propertyId:Ljava/lang/String;

    .line 290
    .line 291
    invoke-virtual/range {p1 .. p1}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇〇888()Lcom/intsig/comm/purchase/entity/ProductResultItem;

    .line 292
    .line 293
    .line 294
    move-result-object v10

    .line 295
    iget v10, v10, Lcom/intsig/comm/purchase/entity/ProductResultItem;->consume:I

    .line 296
    .line 297
    new-instance v11, Ljava/lang/StringBuilder;

    .line 298
    .line 299
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 300
    .line 301
    .line 302
    const-string v12, "product type = "

    .line 303
    .line 304
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 305
    .line 306
    .line 307
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 308
    .line 309
    .line 310
    const-string v12, ", propertyId = "

    .line 311
    .line 312
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 313
    .line 314
    .line 315
    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 316
    .line 317
    .line 318
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 319
    .line 320
    .line 321
    move-result-object v11

    .line 322
    invoke-static {v5, v11}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    .line 324
    .line 325
    move-object/from16 v14, p1

    .line 326
    .line 327
    move-object v11, v3

    .line 328
    move-object v12, v6

    .line 329
    move v3, v10

    .line 330
    move-object v10, v0

    .line 331
    move-object v0, v14

    .line 332
    move-object/from16 v22, v9

    .line 333
    .line 334
    move-object v9, v8

    .line 335
    move-object/from16 v8, v22

    .line 336
    .line 337
    :goto_8
    iget v13, v12, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;->〇o〇:I

    .line 338
    .line 339
    const/16 v15, 0xa

    .line 340
    .line 341
    if-gt v13, v15, :cond_14

    .line 342
    .line 343
    new-instance v15, Ljava/lang/StringBuilder;

    .line 344
    .line 345
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 346
    .line 347
    .line 348
    const-string v7, "UPDATE_VIP_PROPERTY retry count = "

    .line 349
    .line 350
    invoke-virtual {v15, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 351
    .line 352
    .line 353
    invoke-virtual {v15, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 354
    .line 355
    .line 356
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 357
    .line 358
    .line 359
    move-result-object v7

    .line 360
    invoke-static {v5, v7}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    .line 362
    .line 363
    sget-object v7, Lcom/intsig/pay/base/utils/PayTypeUtils;->〇080:Lcom/intsig/pay/base/utils/PayTypeUtils$Companion;

    .line 364
    .line 365
    invoke-virtual {v7, v3}, Lcom/intsig/pay/base/utils/PayTypeUtils$Companion;->〇〇888(I)Z

    .line 366
    .line 367
    .line 368
    move-result v7

    .line 369
    const-string v13, "seqidMd5"

    .line 370
    .line 371
    if-nez v7, :cond_a

    .line 372
    .line 373
    invoke-static {v8}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->o〇〇0〇(Ljava/lang/String;)Z

    .line 374
    .line 375
    .line 376
    move-result v7

    .line 377
    if-eqz v7, :cond_8

    .line 378
    .line 379
    goto :goto_a

    .line 380
    :cond_8
    invoke-static {v9, v13}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 381
    .line 382
    .line 383
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 384
    .line 385
    .line 386
    move-result-object v19

    .line 387
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇〇888()Lcom/intsig/comm/purchase/entity/ProductResultItem;

    .line 388
    .line 389
    .line 390
    move-result-object v7

    .line 391
    iget-object v7, v7, Lcom/intsig/comm/purchase/entity/ProductResultItem;->propertyId:Ljava/lang/String;

    .line 392
    .line 393
    iput-object v12, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->o0:Ljava/lang/Object;

    .line 394
    .line 395
    iput-object v14, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 396
    .line 397
    iput-object v0, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->OO:Ljava/lang/Object;

    .line 398
    .line 399
    iput-object v11, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 400
    .line 401
    iput-object v10, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->o〇00O:Ljava/lang/Object;

    .line 402
    .line 403
    iput-object v9, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->O8o08O8O:Ljava/lang/Object;

    .line 404
    .line 405
    iput-object v8, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇080OO8〇0:Ljava/lang/Object;

    .line 406
    .line 407
    iput v3, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇0O:I

    .line 408
    .line 409
    const/4 v13, 0x2

    .line 410
    iput v13, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->OO〇00〇8oO:I

    .line 411
    .line 412
    move-object v15, v12

    .line 413
    move-object/from16 v16, v9

    .line 414
    .line 415
    move-object/from16 v17, v10

    .line 416
    .line 417
    move-object/from16 v18, v11

    .line 418
    .line 419
    move-object/from16 v20, v7

    .line 420
    .line 421
    move-object/from16 v21, v1

    .line 422
    .line 423
    invoke-direct/range {v15 .. v21}, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;->〇〇888(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 424
    .line 425
    .line 426
    move-result-object v7

    .line 427
    if-ne v7, v2, :cond_9

    .line 428
    .line 429
    return-object v2

    .line 430
    :cond_9
    move-object v13, v14

    .line 431
    move-object v14, v12

    .line 432
    move-object v12, v0

    .line 433
    move-object v0, v7

    .line 434
    :goto_9
    check-cast v0, Ljava/lang/Number;

    .line 435
    .line 436
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 437
    .line 438
    .line 439
    move-result v0

    .line 440
    goto :goto_c

    .line 441
    :cond_a
    :goto_a
    invoke-static {v9, v13}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 442
    .line 443
    .line 444
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->OO0o〇〇〇〇0()Ljava/lang/String;

    .line 445
    .line 446
    .line 447
    move-result-object v19

    .line 448
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->oO80()Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 449
    .line 450
    .line 451
    move-result-object v20

    .line 452
    iput-object v12, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->o0:Ljava/lang/Object;

    .line 453
    .line 454
    iput-object v14, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 455
    .line 456
    iput-object v0, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->OO:Ljava/lang/Object;

    .line 457
    .line 458
    iput-object v11, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 459
    .line 460
    iput-object v10, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->o〇00O:Ljava/lang/Object;

    .line 461
    .line 462
    iput-object v9, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->O8o08O8O:Ljava/lang/Object;

    .line 463
    .line 464
    iput-object v8, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇080OO8〇0:Ljava/lang/Object;

    .line 465
    .line 466
    iput v3, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇0O:I

    .line 467
    .line 468
    iput v4, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->OO〇00〇8oO:I

    .line 469
    .line 470
    move-object v15, v12

    .line 471
    move-object/from16 v16, v9

    .line 472
    .line 473
    move-object/from16 v17, v10

    .line 474
    .line 475
    move-object/from16 v18, v11

    .line 476
    .line 477
    move-object/from16 v21, v1

    .line 478
    .line 479
    invoke-direct/range {v15 .. v21}, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;->oO80(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 480
    .line 481
    .line 482
    move-result-object v7

    .line 483
    if-ne v7, v2, :cond_b

    .line 484
    .line 485
    return-object v2

    .line 486
    :cond_b
    move-object v13, v14

    .line 487
    move-object v14, v12

    .line 488
    move-object v12, v0

    .line 489
    move-object v0, v7

    .line 490
    :goto_b
    check-cast v0, Ljava/lang/Number;

    .line 491
    .line 492
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    .line 493
    .line 494
    .line 495
    move-result v0

    .line 496
    :goto_c
    move-object v7, v14

    .line 497
    move-object/from16 v22, v10

    .line 498
    .line 499
    move-object v10, v9

    .line 500
    move-object v9, v13

    .line 501
    move-object v13, v12

    .line 502
    move-object v12, v11

    .line 503
    move-object/from16 v11, v22

    .line 504
    .line 505
    if-eqz v0, :cond_10

    .line 506
    .line 507
    const/16 v14, 0x1f6

    .line 508
    .line 509
    move-object/from16 v16, v5

    .line 510
    .line 511
    if-eq v0, v14, :cond_c

    .line 512
    .line 513
    iget v14, v7, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;->〇o〇:I

    .line 514
    .line 515
    new-instance v15, Ljava/lang/StringBuilder;

    .line 516
    .line 517
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 518
    .line 519
    .line 520
    const-string v4, "UPDATE_VIP_PROPERTY failed code = "

    .line 521
    .line 522
    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 523
    .line 524
    .line 525
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 526
    .line 527
    .line 528
    const-string v0, ", retry delay  = "

    .line 529
    .line 530
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 531
    .line 532
    .line 533
    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 534
    .line 535
    .line 536
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 537
    .line 538
    .line 539
    move-result-object v0

    .line 540
    move-object/from16 v4, v16

    .line 541
    .line 542
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    .line 544
    .line 545
    iget v0, v7, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;->〇o〇:I

    .line 546
    .line 547
    const/4 v5, 0x1

    .line 548
    add-int/2addr v0, v5

    .line 549
    iput v0, v7, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;->〇o〇:I

    .line 550
    .line 551
    iput-object v7, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->o0:Ljava/lang/Object;

    .line 552
    .line 553
    iput-object v9, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 554
    .line 555
    iput-object v13, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->OO:Ljava/lang/Object;

    .line 556
    .line 557
    iput-object v12, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 558
    .line 559
    iput-object v11, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->o〇00O:Ljava/lang/Object;

    .line 560
    .line 561
    iput-object v10, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->O8o08O8O:Ljava/lang/Object;

    .line 562
    .line 563
    iput-object v8, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇080OO8〇0:Ljava/lang/Object;

    .line 564
    .line 565
    iput v3, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇0O:I

    .line 566
    .line 567
    const/4 v0, 0x6

    .line 568
    iput v0, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->OO〇00〇8oO:I

    .line 569
    .line 570
    const-wide/16 v5, 0xbb8

    .line 571
    .line 572
    invoke-static {v5, v6, v1}, Lkotlinx/coroutines/DelayKt;->〇080(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 573
    .line 574
    .line 575
    move-result-object v0

    .line 576
    if-ne v0, v2, :cond_f

    .line 577
    .line 578
    return-object v2

    .line 579
    :cond_c
    move-object/from16 v4, v16

    .line 580
    .line 581
    iget v5, v7, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;->〇o〇:I

    .line 582
    .line 583
    const/4 v6, 0x1

    .line 584
    add-int/2addr v5, v6

    .line 585
    iput v5, v7, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;->〇o〇:I

    .line 586
    .line 587
    const/4 v14, 0x5

    .line 588
    if-le v5, v14, :cond_e

    .line 589
    .line 590
    const/16 v1, 0xb

    .line 591
    .line 592
    iput v1, v7, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;->〇o〇:I

    .line 593
    .line 594
    new-instance v1, Ljava/lang/StringBuilder;

    .line 595
    .line 596
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 597
    .line 598
    .line 599
    const-string v2, "UPDATE_VIP_PROPERTY failed\uff0ccode = "

    .line 600
    .line 601
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 602
    .line 603
    .line 604
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 605
    .line 606
    .line 607
    const-string v0, " consumePurchase"

    .line 608
    .line 609
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 610
    .line 611
    .line 612
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 613
    .line 614
    .line 615
    move-result-object v0

    .line 616
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    .line 618
    .line 619
    invoke-virtual {v13}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇o〇()Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;

    .line 620
    .line 621
    .line 622
    move-result-object v0

    .line 623
    if-eqz v0, :cond_d

    .line 624
    .line 625
    invoke-virtual {v13}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇〇888()Lcom/intsig/comm/purchase/entity/ProductResultItem;

    .line 626
    .line 627
    .line 628
    move-result-object v1

    .line 629
    iget v1, v1, Lcom/intsig/comm/purchase/entity/ProductResultItem;->consume:I

    .line 630
    .line 631
    invoke-interface {v0, v1, v11, v12}, Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;->〇〇888(ILjava/lang/String;Ljava/lang/String;)V

    .line 632
    .line 633
    .line 634
    :cond_d
    const v8, 0x9c4e

    .line 635
    .line 636
    .line 637
    const/4 v10, 0x0

    .line 638
    const/4 v11, 0x4

    .line 639
    const/4 v12, 0x0

    .line 640
    invoke-static/range {v7 .. v12}, Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;->O8(Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;ILcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;Lcom/intsig/pay/base/model/PayOrderResponse;ILjava/lang/Object;)Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;

    .line 641
    .line 642
    .line 643
    move-result-object v0

    .line 644
    return-object v0

    .line 645
    :cond_e
    iput-object v7, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->o0:Ljava/lang/Object;

    .line 646
    .line 647
    iput-object v9, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 648
    .line 649
    iput-object v13, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->OO:Ljava/lang/Object;

    .line 650
    .line 651
    iput-object v12, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 652
    .line 653
    iput-object v11, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->o〇00O:Ljava/lang/Object;

    .line 654
    .line 655
    iput-object v10, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->O8o08O8O:Ljava/lang/Object;

    .line 656
    .line 657
    iput-object v8, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇080OO8〇0:Ljava/lang/Object;

    .line 658
    .line 659
    iput v3, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇0O:I

    .line 660
    .line 661
    iput v14, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->OO〇00〇8oO:I

    .line 662
    .line 663
    const-wide/16 v14, 0xbb8

    .line 664
    .line 665
    invoke-static {v14, v15, v1}, Lkotlinx/coroutines/DelayKt;->〇080(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 666
    .line 667
    .line 668
    move-result-object v0

    .line 669
    if-ne v0, v2, :cond_f

    .line 670
    .line 671
    return-object v2

    .line 672
    :cond_f
    move-object v14, v9

    .line 673
    move-object v9, v10

    .line 674
    move-object v10, v11

    .line 675
    move-object v11, v12

    .line 676
    move-object v0, v13

    .line 677
    move-object v12, v7

    .line 678
    :goto_d
    move-object/from16 v6, p0

    .line 679
    .line 680
    move-object v5, v4

    .line 681
    const/4 v4, 0x1

    .line 682
    goto/16 :goto_8

    .line 683
    .line 684
    :cond_10
    move-object v4, v5

    .line 685
    iget v0, v7, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask;->〇o〇:I

    .line 686
    .line 687
    new-instance v3, Ljava/lang/StringBuilder;

    .line 688
    .line 689
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 690
    .line 691
    .line 692
    const-string v5, "UPDATE_VIP_PROPERTY success! = "

    .line 693
    .line 694
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 695
    .line 696
    .line 697
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 698
    .line 699
    .line 700
    const-string/jumbo v0, "\uff0cthen delay 2000"

    .line 701
    .line 702
    .line 703
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 704
    .line 705
    .line 706
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 707
    .line 708
    .line 709
    move-result-object v0

    .line 710
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    .line 712
    .line 713
    invoke-virtual {v13}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇o〇()Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;

    .line 714
    .line 715
    .line 716
    move-result-object v0

    .line 717
    if-eqz v0, :cond_11

    .line 718
    .line 719
    invoke-virtual {v13}, Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;->〇〇888()Lcom/intsig/comm/purchase/entity/ProductResultItem;

    .line 720
    .line 721
    .line 722
    move-result-object v3

    .line 723
    iget v3, v3, Lcom/intsig/comm/purchase/entity/ProductResultItem;->consume:I

    .line 724
    .line 725
    invoke-interface {v0, v3, v11, v12}, Lcom/intsig/camscanner/purchase/pay/task/IPayOrderClient;->〇〇888(ILjava/lang/String;Ljava/lang/String;)V

    .line 726
    .line 727
    .line 728
    :cond_11
    iput-object v7, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->o0:Ljava/lang/Object;

    .line 729
    .line 730
    iput-object v9, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 731
    .line 732
    const/4 v0, 0x0

    .line 733
    iput-object v0, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->OO:Ljava/lang/Object;

    .line 734
    .line 735
    iput-object v0, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇08O〇00〇o:Ljava/lang/Object;

    .line 736
    .line 737
    iput-object v0, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->o〇00O:Ljava/lang/Object;

    .line 738
    .line 739
    iput-object v0, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->O8o08O8O:Ljava/lang/Object;

    .line 740
    .line 741
    iput-object v0, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇080OO8〇0:Ljava/lang/Object;

    .line 742
    .line 743
    const/4 v3, 0x3

    .line 744
    iput v3, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->OO〇00〇8oO:I

    .line 745
    .line 746
    const-wide/16 v3, 0x7d0

    .line 747
    .line 748
    invoke-static {v3, v4, v1}, Lkotlinx/coroutines/DelayKt;->〇080(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 749
    .line 750
    .line 751
    move-result-object v3

    .line 752
    if-ne v3, v2, :cond_12

    .line 753
    .line 754
    return-object v2

    .line 755
    :cond_12
    move-object v4, v7

    .line 756
    move-object v3, v9

    .line 757
    :goto_e
    iput-object v0, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->o0:Ljava/lang/Object;

    .line 758
    .line 759
    iput-object v0, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->〇OOo8〇0:Ljava/lang/Object;

    .line 760
    .line 761
    const/4 v0, 0x4

    .line 762
    iput v0, v1, Lcom/intsig/camscanner/purchase/pay/task/GPUpdateOrderTask$chain$1;->OO〇00〇8oO:I

    .line 763
    .line 764
    invoke-virtual {v4, v3, v1}, Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;->〇o00〇〇Oo(Lcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 765
    .line 766
    .line 767
    move-result-object v0

    .line 768
    if-ne v0, v2, :cond_13

    .line 769
    .line 770
    return-object v2

    .line 771
    :cond_13
    :goto_f
    return-object v0

    .line 772
    :cond_14
    const v13, 0x9c4c

    .line 773
    .line 774
    .line 775
    const/4 v15, 0x0

    .line 776
    const/16 v16, 0x4

    .line 777
    .line 778
    const/16 v17, 0x0

    .line 779
    .line 780
    invoke-static/range {v12 .. v17}, Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;->O8(Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;ILcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;Lcom/intsig/pay/base/model/PayOrderResponse;ILjava/lang/Object;)Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;

    .line 781
    .line 782
    .line 783
    move-result-object v0

    .line 784
    return-object v0

    .line 785
    :cond_15
    :goto_10
    const v1, 0x9c4b

    .line 786
    .line 787
    .line 788
    const/4 v3, 0x0

    .line 789
    const/4 v4, 0x4

    .line 790
    const/4 v5, 0x0

    .line 791
    move-object/from16 v0, p0

    .line 792
    .line 793
    move-object/from16 v2, p1

    .line 794
    .line 795
    invoke-static/range {v0 .. v5}, Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;->O8(Lcom/intsig/camscanner/purchase/pay/task/PayInterceptor;ILcom/intsig/camscanner/purchase/pay/task/entity/PayRequest;Lcom/intsig/pay/base/model/PayOrderResponse;ILjava/lang/Object;)Lcom/intsig/camscanner/purchase/pay/task/entity/PayResponse;

    .line 796
    .line 797
    .line 798
    move-result-object v0

    .line 799
    return-object v0

    .line 800
    nop

    .line 801
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
.end method

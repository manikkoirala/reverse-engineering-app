.class public Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration;
.super Ljava/lang/Object;
.source "CSPayConfiguration.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;
    }
.end annotation


# instance fields
.field private O8:I

.field private Oo08:Ljava/lang/String;

.field private oO80:Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;

.field private o〇0:Ljava/lang/String;

.field private 〇080:I

.field private 〇o00〇〇Oo:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

.field private 〇o〇:Ljava/lang/String;

.field private 〇〇888:Z


# direct methods
.method private constructor <init>(Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    invoke-static {p1}, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;->〇〇888(Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration;->〇o00〇〇Oo:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 4
    invoke-static {p1}, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;->o〇0(Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration;->〇o〇:Ljava/lang/String;

    .line 5
    invoke-static {p1}, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;->Oo08(Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;)I

    move-result v0

    iput v0, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration;->O8:I

    .line 6
    invoke-static {p1}, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;->O8(Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;)I

    move-result v0

    iput v0, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration;->〇080:I

    .line 7
    invoke-static {p1}, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;->〇o〇(Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration;->Oo08:Ljava/lang/String;

    .line 8
    invoke-static {p1}, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;->〇080(Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration;->o〇0:Ljava/lang/String;

    .line 9
    invoke-static {p1}, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;->oO80(Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration;->〇〇888:Z

    .line 10
    invoke-static {p1}, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;->〇o00〇〇Oo(Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;)Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;

    move-result-object p1

    iput-object p1, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration;->oO80:Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;

    return-void
.end method

.method synthetic constructor <init>(Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;LOO8〇O8/〇080;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration;-><init>(Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration$Builder;)V

    return-void
.end method


# virtual methods
.method public O8()Lcom/intsig/camscanner/purchase/track/PurchaseTracker;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration;->〇o00〇〇Oo:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public Oo08()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration;->〇〇888:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇080()Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration;->oO80:Lcom/intsig/camscanner/purchase/pay/task/entity/CreateOrderExtra;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇o00〇〇Oo()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration;->Oo08:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇o〇()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/purchase/pay/CSPayConfiguration;->〇080:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.class Lcom/intsig/camscanner/purchase/activity/AccountPurchaseActivity$LogAgentHelper;
.super Ljava/lang/Object;
.source "AccountPurchaseActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/purchase/activity/AccountPurchaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LogAgentHelper"
.end annotation


# direct methods
.method private static O8(JLcom/intsig/camscanner/purchase/track/PurchaseTracker;)V
    .locals 2

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    sub-long/2addr v0, p0

    .line 6
    long-to-float p0, v0

    .line 7
    const/high16 p1, 0x447a0000    # 1000.0f

    .line 8
    .line 9
    div-float/2addr p0, p1

    .line 10
    invoke-static {p0}, Lcom/intsig/utils/DecimalFormatUtil;->〇080(F)Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object p0

    .line 14
    iget-object p1, p2, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->toTrackerValue()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    const-string/jumbo p2, "stay_time"

    .line 21
    .line 22
    .line 23
    const-string v0, "close_time"

    .line 24
    .line 25
    invoke-static {p1, p2, v0, p0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private static Oo08(JLcom/intsig/camscanner/purchase/track/PurchaseTracker;)V
    .locals 2

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    sub-long/2addr v0, p0

    .line 6
    long-to-float p0, v0

    .line 7
    const/high16 p1, 0x447a0000    # 1000.0f

    .line 8
    .line 9
    div-float/2addr p0, p1

    .line 10
    invoke-static {p0}, Lcom/intsig/utils/DecimalFormatUtil;->〇080(F)Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object p0

    .line 14
    iget-object p1, p2, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 15
    .line 16
    invoke-virtual {p1}, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->toTrackerValue()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p1

    .line 20
    const-string/jumbo p2, "stay_time"

    .line 21
    .line 22
    .line 23
    const-string v0, "buy_intent_time"

    .line 24
    .line 25
    invoke-static {p1, p2, v0, p0}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private static o〇0(Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V
    .locals 3

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->toTrackerValue()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    const-string v0, "scheme"

    .line 8
    .line 9
    const-string v1, "retain_pop"

    .line 10
    .line 11
    const-string/jumbo v2, "stay_time"

    .line 12
    .line 13
    .line 14
    invoke-static {p0, v2, v0, v1}, Lcom/intsig/camscanner/log/LogAgentData;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic 〇080(JLcom/intsig/camscanner/purchase/track/PurchaseTracker;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseActivity$LogAgentHelper;->O8(JLcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method static bridge synthetic 〇o00〇〇Oo(JLcom/intsig/camscanner/purchase/track/PurchaseTracker;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseActivity$LogAgentHelper;->Oo08(JLcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseActivity$LogAgentHelper;->o〇0(Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

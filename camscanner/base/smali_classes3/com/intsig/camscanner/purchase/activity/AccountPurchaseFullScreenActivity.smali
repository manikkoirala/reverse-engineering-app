.class public Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;
.super Lcom/intsig/mvp/activity/BaseChangeActivity;
.source "AccountPurchaseFullScreenActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity$LogAgentHelper;,
        Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity$NewResStyle;,
        Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity$OldResStyle;,
        Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity$ForeverPriceStyle;,
        Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity$FullPriceStyle;
    }
.end annotation


# instance fields
.field private O0O:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

.field private O88O:I

.field private Oo80:Lcom/intsig/camscanner/purchase/ToRetainGpCommonDialog;

.field private o8o:Lcom/intsig/camscanner/purchase/activity/IPurchaseViewStyle;

.field private o8oOOo:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

.field private oOO〇〇:Lcom/intsig/camscanner/purchase/activity/IPurchaseViewStyle;

.field private oo8ooo8O:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/purchase/entity/Function;",
            ">;"
        }
    .end annotation
.end field

.field private ooo0〇〇O:Lcom/intsig/camscanner/purchase/entity/Function;

.field private o〇oO:J

.field private 〇08〇o0O:Lcom/intsig/camscanner/purchase/dialog/GPRedeemCallDialog;

.field private 〇O〇〇O8:Z

.field private 〇o0O:Z

.field private 〇〇08O:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

.field private 〇〇o〇:Lcom/intsig/camscanner/purchase/ToRetainGpDialog;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇O〇〇O8:Z

    .line 6
    .line 7
    new-instance v0, Ljava/util/ArrayList;

    .line 8
    .line 9
    const/4 v1, 0x4

    .line 10
    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 11
    .line 12
    .line 13
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->oo8ooo8O:Ljava/util/List;

    .line 14
    .line 15
    return-void
.end method

.method public static synthetic O0〇(Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->O88(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private synthetic O88(Z)V
    .locals 2

    .line 1
    const-string v0, "AccountPurchaseFullScreenActivity"

    .line 2
    .line 3
    :try_start_0
    invoke-virtual {p0}, Landroid/app/Activity;->isFinishing()Z

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    if-eqz v1, :cond_0

    .line 8
    .line 9
    const-string/jumbo p1, "this activity is finish"

    .line 10
    .line 11
    .line 12
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 13
    .line 14
    .line 15
    return-void

    .line 16
    :cond_0
    if-eqz p1, :cond_1

    .line 17
    .line 18
    iget-object p1, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->o8o:Lcom/intsig/camscanner/purchase/activity/IPurchaseViewStyle;

    .line 19
    .line 20
    if-eqz p1, :cond_2

    .line 21
    .line 22
    invoke-interface {p1}, Lcom/intsig/camscanner/purchase/activity/IPurchaseViewStyle;->〇o〇()V

    .line 23
    .line 24
    .line 25
    goto :goto_0

    .line 26
    :cond_1
    const-string p1, "callback false"

    .line 27
    .line 28
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 29
    .line 30
    .line 31
    goto :goto_0

    .line 32
    :catch_0
    move-exception p1

    .line 33
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 34
    .line 35
    .line 36
    :cond_2
    :goto_0
    return-void
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method static synthetic O880O〇(Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;)Lcom/intsig/mvp/activity/BaseChangeActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic OO0O(Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->o〇oO:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static synthetic OO〇〇o0oO(Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;)Lcom/intsig/mvp/activity/BaseChangeActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static synthetic OooO〇(Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;)Lcom/intsig/mvp/activity/BaseChangeActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic O〇080〇o0(Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;)Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->O0O:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static synthetic O〇0O〇Oo〇o(Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇oOO80o(Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method static bridge synthetic O〇〇O80o8(Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇o0O:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private o0O0O〇〇〇0()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->oo8ooo8O:Ljava/util/List;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->ooo0〇〇O:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 4
    .line 5
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method static bridge synthetic o0Oo(Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇O〇〇O8:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic o808o8o08(Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;)Lcom/intsig/camscanner/purchase/track/FunctionEntrance;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇〇08O:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic o〇08oO80o(Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->o8oOOo:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic o〇o08〇(Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->o0O0O〇〇〇0()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private 〇0o88Oo〇(Lcom/intsig/app/BaseDialogFragment;Ljava/lang/String;)Z
    .locals 2

    .line 1
    :try_start_0
    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentManager;->beginTransaction()Landroidx/fragment/app/FragmentTransaction;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0, p1, p2}, Landroidx/fragment/app/FragmentTransaction;->add(Landroidx/fragment/app/Fragment;Ljava/lang/String;)Landroidx/fragment/app/FragmentTransaction;

    .line 10
    .line 11
    .line 12
    const p1, 0x7f010012

    .line 13
    .line 14
    .line 15
    const p2, 0x7f010013

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0, p1, p2}, Landroidx/fragment/app/FragmentTransaction;->setCustomAnimations(II)Landroidx/fragment/app/FragmentTransaction;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v0}, Landroidx/fragment/app/FragmentTransaction;->commitNowAllowingStateLoss()V

    .line 22
    .line 23
    .line 24
    invoke-static {}, Lcom/intsig/utils/PreferenceUtil;->oO80()Lcom/intsig/utils/PreferenceUtil;

    .line 25
    .line 26
    .line 27
    move-result-object p1

    .line 28
    const-string p2, "CS_REDEEM_RECALL_SHOW_TIME"

    .line 29
    .line 30
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 31
    .line 32
    .line 33
    move-result-wide v0

    .line 34
    invoke-virtual {p1, p2, v0, v1}, Lcom/intsig/utils/PreferenceUtil;->OoO8(Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 35
    .line 36
    .line 37
    const/4 p1, 0x1

    .line 38
    return p1

    .line 39
    :catch_0
    move-exception p1

    .line 40
    const-string p2, "AccountPurchaseFullScreenActivity"

    .line 41
    .line 42
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 43
    .line 44
    .line 45
    const/4 p1, 0x0

    .line 46
    return p1
    .line 47
    .line 48
    .line 49
.end method

.method private 〇OoO0o0()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->oo8ooo8O:Ljava/util/List;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_SETTING_BUY_1G_CLOUD:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 4
    .line 5
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->oo8ooo8O:Ljava/util/List;

    .line 9
    .line 10
    sget-object v1, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_CLOUD_OCR:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 11
    .line 12
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 13
    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->oo8ooo8O:Ljava/util/List;

    .line 16
    .line 17
    sget-object v1, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_TAKE_PICTURE_OCR:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 18
    .line 19
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 20
    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->oo8ooo8O:Ljava/util/List;

    .line 23
    .line 24
    sget-object v1, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_TRANSLATE:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 25
    .line 26
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 27
    .line 28
    .line 29
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->oo8ooo8O:Ljava/util/List;

    .line 30
    .line 31
    sget-object v1, Lcom/intsig/camscanner/purchase/entity/Function;->FROM_FUN_GREETCARD_FROM_GALLERY:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 32
    .line 33
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 34
    .line 35
    .line 36
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 37
    .line 38
    .line 39
    move-result-object v0

    .line 40
    if-nez v0, :cond_0

    .line 41
    .line 42
    return-void

    .line 43
    :cond_0
    sget-object v1, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->CSPremiumPop:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 44
    .line 45
    const-string v2, "extra_vip_item_pos"

    .line 46
    .line 47
    invoke-virtual {v0, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    check-cast v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 52
    .line 53
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->o8oOOo:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 54
    .line 55
    if-nez v0, :cond_1

    .line 56
    .line 57
    new-instance v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 58
    .line 59
    invoke-direct {v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 60
    .line 61
    .line 62
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->o8oOOo:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 63
    .line 64
    :cond_1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->o8oOOo:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 65
    .line 66
    iget-object v2, v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->function:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 67
    .line 68
    iput-object v2, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->ooo0〇〇O:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 69
    .line 70
    iget-object v2, v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 71
    .line 72
    iput-object v2, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇〇08O:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 73
    .line 74
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId(Lcom/intsig/camscanner/purchase/track/PurchasePageId;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    sget-object v1, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->MAIN_NORMAL:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 79
    .line 80
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme(Lcom/intsig/camscanner/purchase/track/PurchaseScheme;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 81
    .line 82
    .line 83
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->o8oOOo:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 84
    .line 85
    new-instance v1, Ljava/lang/StringBuilder;

    .line 86
    .line 87
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 88
    .line 89
    .line 90
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇OO8ooO8〇()I

    .line 91
    .line 92
    .line 93
    move-result v2

    .line 94
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 95
    .line 96
    .line 97
    const-string v2, ""

    .line 98
    .line 99
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 100
    .line 101
    .line 102
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 103
    .line 104
    .line 105
    move-result-object v1

    .line 106
    iput-object v1, v0, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->price_copywriting:Ljava/lang/String;

    .line 107
    .line 108
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->o8oOOo:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 109
    .line 110
    invoke-static {v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTrackerUtil;->oO80(Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 111
    .line 112
    .line 113
    new-instance v0, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 114
    .line 115
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->o8oOOo:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 116
    .line 117
    invoke-direct {v0, p0, v1}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 118
    .line 119
    .line 120
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->O0O:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 121
    .line 122
    new-instance v1, Lcom/intsig/camscanner/purchase/activity/〇〇888;

    .line 123
    .line 124
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/purchase/activity/〇〇888;-><init>(Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;)V

    .line 125
    .line 126
    .line 127
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->O000(Lcom/intsig/camscanner/purchase/OnProductLoadListener;)V

    .line 128
    .line 129
    .line 130
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->O0O:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 131
    .line 132
    new-instance v1, Lcom/intsig/camscanner/purchase/activity/oO80;

    .line 133
    .line 134
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/purchase/activity/oO80;-><init>(Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;)V

    .line 135
    .line 136
    .line 137
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->〇O〇80o08O(Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient$PurchaseCallback;)V

    .line 138
    .line 139
    .line 140
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->O0O:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 141
    .line 142
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->o〇〇0〇()Z

    .line 143
    .line 144
    .line 145
    move-result v0

    .line 146
    iput-boolean v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇O〇〇O8:Z

    .line 147
    .line 148
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 149
    .line 150
    .line 151
    move-result-object v0

    .line 152
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 153
    .line 154
    .line 155
    move-result-object v0

    .line 156
    iget v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->price_copywriting:I

    .line 157
    .line 158
    const/4 v1, 0x1

    .line 159
    if-ne v0, v1, :cond_2

    .line 160
    .line 161
    goto :goto_0

    .line 162
    :cond_2
    const/4 v1, 0x0

    .line 163
    :goto_0
    iput-boolean v1, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇o0O:Z

    .line 164
    .line 165
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->O8O()Lcom/intsig/camscanner/purchase/activity/IPurchaseViewStyle;

    .line 166
    .line 167
    .line 168
    move-result-object v0

    .line 169
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->o8o:Lcom/intsig/camscanner/purchase/activity/IPurchaseViewStyle;

    .line 170
    .line 171
    if-eqz v0, :cond_3

    .line 172
    .line 173
    invoke-interface {v0}, Lcom/intsig/camscanner/purchase/activity/IPurchaseViewStyle;->〇080()V

    .line 174
    .line 175
    .line 176
    :cond_3
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇ooO〇000()Lcom/intsig/camscanner/purchase/activity/IPurchaseViewStyle;

    .line 177
    .line 178
    .line 179
    move-result-object v0

    .line 180
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->oOO〇〇:Lcom/intsig/camscanner/purchase/activity/IPurchaseViewStyle;

    .line 181
    .line 182
    invoke-interface {v0}, Lcom/intsig/camscanner/purchase/activity/IPurchaseViewStyle;->〇080()V

    .line 183
    .line 184
    .line 185
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 186
    .line 187
    .line 188
    move-result-object v0

    .line 189
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇〇808〇()Z

    .line 190
    .line 191
    .line 192
    move-result v0

    .line 193
    if-eqz v0, :cond_4

    .line 194
    .line 195
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->o8o:Lcom/intsig/camscanner/purchase/activity/IPurchaseViewStyle;

    .line 196
    .line 197
    if-eqz v0, :cond_4

    .line 198
    .line 199
    invoke-interface {v0}, Lcom/intsig/camscanner/purchase/activity/IPurchaseViewStyle;->〇o〇()V

    .line 200
    .line 201
    .line 202
    :cond_4
    return-void
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method static bridge synthetic 〇oO88o(Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;)Lcom/intsig/camscanner/purchase/entity/Function;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->ooo0〇〇O:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private synthetic 〇oOO80o(Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->o8()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, -0x1

    .line 6
    if-nez v0, :cond_4

    .line 7
    .line 8
    sget-boolean v0, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->〇080:Z

    .line 9
    .line 10
    if-eqz v0, :cond_0

    .line 11
    .line 12
    goto :goto_2

    .line 13
    :cond_0
    iget-object v0, p1, Lcom/intsig/comm/purchase/entity/ProductResultItem;->propertyId:Ljava/lang/String;

    .line 14
    .line 15
    invoke-static {v0}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->〇00(Ljava/lang/String;)Z

    .line 16
    .line 17
    .line 18
    move-result v0

    .line 19
    const/4 v2, 0x1

    .line 20
    invoke-static {p2, v0, v2}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->〇oOO8O8(ZZZ)Z

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    if-eqz v0, :cond_1

    .line 25
    .line 26
    iget-object p1, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->o8oOOo:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 27
    .line 28
    invoke-static {p0, p1}, Lcom/intsig/camscanner/purchase/activity/GPRedeemActivity;->startActivity(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 29
    .line 30
    .line 31
    iget-object p1, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->o8oOOo:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 32
    .line 33
    invoke-static {p1}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity$LogAgentHelper;->〇o〇(Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 34
    .line 35
    .line 36
    new-instance p1, Ljava/lang/StringBuilder;

    .line 37
    .line 38
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 39
    .line 40
    .line 41
    const-string p2, "PreferenceHelper.getCountDownPopupSkipInterval() = "

    .line 42
    .line 43
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 44
    .line 45
    .line 46
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->Oo〇o()I

    .line 47
    .line 48
    .line 49
    move-result p2

    .line 50
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 51
    .line 52
    .line 53
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object p1

    .line 57
    const-string p2, "AccountPurchaseFullScreenActivity"

    .line 58
    .line 59
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇00()V

    .line 63
    .line 64
    .line 65
    goto :goto_1

    .line 66
    :cond_1
    iget-object p1, p1, Lcom/intsig/comm/purchase/entity/ProductResultItem;->propertyId:Ljava/lang/String;

    .line 67
    .line 68
    invoke-static {p1}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->〇00(Ljava/lang/String;)Z

    .line 69
    .line 70
    .line 71
    move-result p1

    .line 72
    invoke-static {p2, p1}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->〇0000OOO(ZZ)Z

    .line 73
    .line 74
    .line 75
    move-result p1

    .line 76
    if-eqz p1, :cond_2

    .line 77
    .line 78
    invoke-static {p0}, Lcom/intsig/camscanner/purchase/utils/PurchaseUtil;->〇o(Landroid/app/Activity;)V

    .line 79
    .line 80
    .line 81
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇00()V

    .line 82
    .line 83
    .line 84
    goto :goto_1

    .line 85
    :cond_2
    if-eqz p2, :cond_3

    .line 86
    .line 87
    goto :goto_0

    .line 88
    :cond_3
    const/4 v1, 0x0

    .line 89
    :goto_0
    invoke-virtual {p0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 90
    .line 91
    .line 92
    :goto_1
    return-void

    .line 93
    :cond_4
    :goto_2
    if-eqz p2, :cond_5

    .line 94
    .line 95
    invoke-virtual {p0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 96
    .line 97
    .line 98
    :cond_5
    return-void
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

.method private 〇ooO8Ooo〇()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/ads/reward/AdRewardedManager;->〇080:Lcom/intsig/camscanner/ads/reward/AdRewardedManager;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->o8oOOo:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/ads/reward/AdRewardedManager;->〇8o8o〇(Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    if-eqz v1, :cond_0

    .line 10
    .line 11
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->o8oOOo:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/ads/reward/AdRewardedManager;->〇O8o08O(Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 14
    .line 15
    .line 16
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇00()V

    .line 17
    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇Oo〇O()Z

    .line 21
    .line 22
    .line 23
    move-result v0

    .line 24
    if-nez v0, :cond_1

    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇00()V

    .line 27
    .line 28
    .line 29
    :cond_1
    :goto_0
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method


# virtual methods
.method O8O()Lcom/intsig/camscanner/purchase/activity/IPurchaseViewStyle;
    .locals 3
    .annotation build Landroidx/annotation/Nullable;
    .end annotation

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 4
    .line 5
    .line 6
    const-string v1, " purchase style = "

    .line 7
    .line 8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 9
    .line 10
    .line 11
    iget v1, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->O88O:I

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const-string v1, "AccountPurchaseFullScreenActivity"

    .line 21
    .line 22
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    iget v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->O88O:I

    .line 26
    .line 27
    const/4 v1, 0x1

    .line 28
    const/4 v2, 0x0

    .line 29
    if-ne v0, v1, :cond_0

    .line 30
    .line 31
    new-instance v0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity$ForeverPriceStyle;

    .line 32
    .line 33
    invoke-direct {v0, p0, v2}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity$ForeverPriceStyle;-><init>(Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;Lcom/intsig/camscanner/purchase/activity/〇80〇808〇O;)V

    .line 34
    .line 35
    .line 36
    return-object v0

    .line 37
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity$FullPriceStyle;

    .line 38
    .line 39
    invoke-direct {v0, p0, v2}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity$FullPriceStyle;-><init>(Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;Lcom/intsig/camscanner/purchase/activity/OO0o〇〇〇〇0;)V

    .line 40
    .line 41
    .line 42
    return-object v0
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public OoO〇OOo8o()Ljava/lang/String;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->ooo0〇〇O:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 2
    .line 3
    const-string v1, ""

    .line 4
    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    return-object v1

    .line 8
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/entity/Function;->fromCertificateType()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    const v2, 0x7f13038f

    .line 13
    .line 14
    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    return-object v0

    .line 22
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity$1;->〇080:[I

    .line 23
    .line 24
    iget-object v3, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->ooo0〇〇O:Lcom/intsig/camscanner/purchase/entity/Function;

    .line 25
    .line 26
    invoke-virtual {v3}, Ljava/lang/Enum;->ordinal()I

    .line 27
    .line 28
    .line 29
    move-result v3

    .line 30
    aget v0, v0, v3

    .line 31
    .line 32
    packed-switch v0, :pswitch_data_0

    .line 33
    .line 34
    .line 35
    goto :goto_0

    .line 36
    :pswitch_0
    const v0, 0x7f13020a

    .line 37
    .line 38
    .line 39
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v1

    .line 43
    goto :goto_0

    .line 44
    :pswitch_1
    const v0, 0x7f1301c2

    .line 45
    .line 46
    .line 47
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 48
    .line 49
    .line 50
    move-result-object v1

    .line 51
    goto :goto_0

    .line 52
    :pswitch_2
    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v1

    .line 56
    goto :goto_0

    .line 57
    :pswitch_3
    const v0, 0x7f130397

    .line 58
    .line 59
    .line 60
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    goto :goto_0

    .line 65
    :pswitch_4
    const v0, 0x7f130392

    .line 66
    .line 67
    .line 68
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 69
    .line 70
    .line 71
    move-result-object v1

    .line 72
    :goto_0
    return-object v1

    .line 73
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public dealClickAction(Landroid/view/View;)V
    .locals 2

    .line 1
    const-string v0, "onClick"

    .line 2
    .line 3
    const-string v1, "AccountPurchaseFullScreenActivity"

    .line 4
    .line 5
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 9
    .line 10
    .line 11
    move-result p1

    .line 12
    const v0, 0x7f0a087d

    .line 13
    .line 14
    .line 15
    if-eq p1, v0, :cond_0

    .line 16
    .line 17
    const v0, 0x7f0a0eb2

    .line 18
    .line 19
    .line 20
    if-ne p1, v0, :cond_1

    .line 21
    .line 22
    :cond_0
    const-string p1, "onClick iv_close"

    .line 23
    .line 24
    invoke-static {v1, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    iget-object p1, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->o8oOOo:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 28
    .line 29
    sget-object v0, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->CANCEL:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 30
    .line 31
    invoke-static {p1, v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTrackerUtil;->〇080(Lcom/intsig/camscanner/purchase/track/PurchaseTracker;Lcom/intsig/camscanner/purchase/track/PurchaseAction;)V

    .line 32
    .line 33
    .line 34
    const/4 p1, 0x0

    .line 35
    invoke-virtual {p0, p1}, Landroid/app/Activity;->setResult(I)V

    .line 36
    .line 37
    .line 38
    iget-wide v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->o〇oO:J

    .line 39
    .line 40
    iget-object p1, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->o8oOOo:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 41
    .line 42
    invoke-static {v0, v1, p1}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity$LogAgentHelper;->〇080(JLcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 43
    .line 44
    .line 45
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇ooO8Ooo〇()V

    .line 46
    .line 47
    .line 48
    :cond_1
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇OoO0o0()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .line 1
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->O0O:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0, p1, p2, p3}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->〇8〇0〇o〇O(IILandroid/content/Intent;)V

    .line 9
    .line 10
    .line 11
    :cond_0
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/app/AppUtil;->O08000()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇00()V

    .line 8
    .line 9
    .line 10
    :cond_0
    invoke-super {p0, p1}, Lcom/intsig/mvp/activity/BaseChangeActivity;->onCreate(Landroid/os/Bundle;)V

    .line 11
    .line 12
    .line 13
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 14
    .line 15
    .line 16
    move-result-wide v0

    .line 17
    iput-wide v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->o〇oO:J

    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
.end method

.method protected onDestroy()V
    .locals 0

    .line 1
    invoke-super {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->onDestroy()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Lcom/intsig/mvp/activity/BaseChangeActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    return p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method protected onStop()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/appcompat/app/AppCompatActivity;->onStop()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    const v1, 0x7f01000e

    .line 6
    .line 7
    .line 8
    :try_start_0
    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 9
    .line 10
    .line 11
    goto :goto_0

    .line 12
    :catch_0
    move-exception v0

    .line 13
    const-string v1, "AccountPurchaseFullScreenActivity"

    .line 14
    .line 15
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 16
    .line 17
    .line 18
    :goto_0
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public bridge synthetic onToolbarTitleClick(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->Oo08(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public o〇oo()I
    .locals 1

    .line 1
    const v0, 0x7f0d00ab

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇00()V
    .locals 3

    .line 1
    invoke-super {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;->〇00()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->o8oOOo:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 5
    .line 6
    sget-object v1, Lcom/intsig/camscanner/purchase/track/PurchaseAction;->CANCEL:Lcom/intsig/camscanner/purchase/track/PurchaseAction;

    .line 7
    .line 8
    invoke-static {v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTrackerUtil;->〇080(Lcom/intsig/camscanner/purchase/track/PurchaseTracker;Lcom/intsig/camscanner/purchase/track/PurchaseAction;)V

    .line 9
    .line 10
    .line 11
    iget-wide v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->o〇oO:J

    .line 12
    .line 13
    iget-object v2, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->o8oOOo:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 14
    .line 15
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity$LogAgentHelper;->〇080(JLcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public 〇0ooOOo(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    const-string v0, "EXTRA_VIP_STYLE_TYPE"

    .line 2
    .line 3
    invoke-virtual {p1, v0}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;)I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    iput p1, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->O88O:I

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public 〇0〇0()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇Oo〇O()Z
    .locals 4

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/purchase/looperdialog/DialogActiveDayManager;->〇080()V

    .line 2
    .line 3
    .line 4
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->o8()Z

    .line 5
    .line 6
    .line 7
    move-result v0

    .line 8
    const-string v1, "cs_main_normal"

    .line 9
    .line 10
    if-eqz v0, :cond_1

    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇08〇o0O:Lcom/intsig/camscanner/purchase/dialog/GPRedeemCallDialog;

    .line 13
    .line 14
    if-nez v0, :cond_0

    .line 15
    .line 16
    new-instance v0, Lcom/intsig/camscanner/purchase/dialog/GPRedeemCallDialog;

    .line 17
    .line 18
    invoke-direct {v0}, Lcom/intsig/camscanner/purchase/dialog/GPRedeemCallDialog;-><init>()V

    .line 19
    .line 20
    .line 21
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇08〇o0O:Lcom/intsig/camscanner/purchase/dialog/GPRedeemCallDialog;

    .line 22
    .line 23
    new-instance v0, Landroid/os/Bundle;

    .line 24
    .line 25
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 26
    .line 27
    .line 28
    const-string/jumbo v2, "webGuideDialogKey"

    .line 29
    .line 30
    .line 31
    const-string v3, "1"

    .line 32
    .line 33
    invoke-virtual {v0, v2, v3}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    const-string v2, "fromPartKey"

    .line 37
    .line 38
    invoke-virtual {v0, v2, v1}, Landroid/os/BaseBundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇08〇o0O:Lcom/intsig/camscanner/purchase/dialog/GPRedeemCallDialog;

    .line 42
    .line 43
    invoke-virtual {v1, v0}, Landroidx/fragment/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 44
    .line 45
    .line 46
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇08〇o0O:Lcom/intsig/camscanner/purchase/dialog/GPRedeemCallDialog;

    .line 47
    .line 48
    new-instance v1, Lcom/intsig/camscanner/purchase/activity/o〇0;

    .line 49
    .line 50
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/purchase/activity/o〇0;-><init>(Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;)V

    .line 51
    .line 52
    .line 53
    invoke-virtual {v0, v1}, Lcom/intsig/app/BaseDialogFragment;->setDialogDismissListener(Lcom/intsig/callback/DialogDismissListener;)V

    .line 54
    .line 55
    .line 56
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇08〇o0O:Lcom/intsig/camscanner/purchase/dialog/GPRedeemCallDialog;

    .line 57
    .line 58
    const-string v1, "GPRenewalDialog"

    .line 59
    .line 60
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇0o88Oo〇(Lcom/intsig/app/BaseDialogFragment;Ljava/lang/String;)Z

    .line 61
    .line 62
    .line 63
    move-result v0

    .line 64
    return v0

    .line 65
    :cond_1
    const-string v0, "AccountPurchaseFullScreenActivity"

    .line 66
    .line 67
    invoke-static {v0}, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->〇〇0o(Ljava/lang/String;)Z

    .line 68
    .line 69
    .line 70
    move-result v0

    .line 71
    const/4 v2, 0x0

    .line 72
    if-eqz v0, :cond_3

    .line 73
    .line 74
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇〇o〇:Lcom/intsig/camscanner/purchase/ToRetainGpDialog;

    .line 75
    .line 76
    if-nez v0, :cond_2

    .line 77
    .line 78
    invoke-static {v1}, Lcom/intsig/camscanner/purchase/ToRetainGpDialog;->〇o〇88〇8(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/ToRetainGpDialog;

    .line 79
    .line 80
    .line 81
    move-result-object v0

    .line 82
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇〇o〇:Lcom/intsig/camscanner/purchase/ToRetainGpDialog;

    .line 83
    .line 84
    new-instance v1, Lcom/intsig/camscanner/purchase/activity/o〇0;

    .line 85
    .line 86
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/purchase/activity/o〇0;-><init>(Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;)V

    .line 87
    .line 88
    .line 89
    invoke-virtual {v0, v1}, Lcom/intsig/app/BaseDialogFragment;->setDialogDismissListener(Lcom/intsig/callback/DialogDismissListener;)V

    .line 90
    .line 91
    .line 92
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇〇o〇:Lcom/intsig/camscanner/purchase/ToRetainGpDialog;

    .line 93
    .line 94
    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    .line 95
    .line 96
    .line 97
    move-result v0

    .line 98
    if-nez v0, :cond_5

    .line 99
    .line 100
    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 101
    .line 102
    .line 103
    move-result-object v0

    .line 104
    const-string v1, "ToRetainGpDialog"

    .line 105
    .line 106
    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    .line 107
    .line 108
    .line 109
    move-result-object v0

    .line 110
    if-nez v0, :cond_5

    .line 111
    .line 112
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇〇o〇:Lcom/intsig/camscanner/purchase/ToRetainGpDialog;

    .line 113
    .line 114
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇0o88Oo〇(Lcom/intsig/app/BaseDialogFragment;Ljava/lang/String;)Z

    .line 115
    .line 116
    .line 117
    move-result v0

    .line 118
    return v0

    .line 119
    :cond_3
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductHelper;->〇〇〇0〇〇0()Z

    .line 120
    .line 121
    .line 122
    move-result v0

    .line 123
    if-eqz v0, :cond_5

    .line 124
    .line 125
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->Oo80:Lcom/intsig/camscanner/purchase/ToRetainGpCommonDialog;

    .line 126
    .line 127
    if-nez v0, :cond_4

    .line 128
    .line 129
    invoke-static {}, Lcom/intsig/camscanner/purchase/ToRetainGpCommonDialog;->〇〇o0〇8()Lcom/intsig/camscanner/purchase/ToRetainGpCommonDialog;

    .line 130
    .line 131
    .line 132
    move-result-object v0

    .line 133
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->Oo80:Lcom/intsig/camscanner/purchase/ToRetainGpCommonDialog;

    .line 134
    .line 135
    invoke-virtual {v0, v2}, Landroidx/fragment/app/DialogFragment;->setCancelable(Z)V

    .line 136
    .line 137
    .line 138
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->Oo80:Lcom/intsig/camscanner/purchase/ToRetainGpCommonDialog;

    .line 139
    .line 140
    new-instance v1, Lcom/intsig/camscanner/purchase/activity/o〇0;

    .line 141
    .line 142
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/purchase/activity/o〇0;-><init>(Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;)V

    .line 143
    .line 144
    .line 145
    invoke-virtual {v0, v1}, Lcom/intsig/app/BaseDialogFragment;->setDialogDismissListener(Lcom/intsig/callback/DialogDismissListener;)V

    .line 146
    .line 147
    .line 148
    :cond_4
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->Oo80:Lcom/intsig/camscanner/purchase/ToRetainGpCommonDialog;

    .line 149
    .line 150
    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->isAdded()Z

    .line 151
    .line 152
    .line 153
    move-result v0

    .line 154
    if-nez v0, :cond_5

    .line 155
    .line 156
    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 157
    .line 158
    .line 159
    move-result-object v0

    .line 160
    const-string v1, "ToRetainGpCommonDialog"

    .line 161
    .line 162
    invoke-virtual {v0, v1}, Landroidx/fragment/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroidx/fragment/app/Fragment;

    .line 163
    .line 164
    .line 165
    move-result-object v0

    .line 166
    if-nez v0, :cond_5

    .line 167
    .line 168
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->Oo80:Lcom/intsig/camscanner/purchase/ToRetainGpCommonDialog;

    .line 169
    .line 170
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;->〇0o88Oo〇(Lcom/intsig/app/BaseDialogFragment;Ljava/lang/String;)Z

    .line 171
    .line 172
    .line 173
    move-result v0

    .line 174
    return v0

    .line 175
    :cond_5
    return v2
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method 〇ooO〇000()Lcom/intsig/camscanner/purchase/activity/IPurchaseViewStyle;
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iget v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->content_style:I

    .line 10
    .line 11
    new-instance v1, Ljava/lang/StringBuilder;

    .line 12
    .line 13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 14
    .line 15
    .line 16
    const-string v2, "newBannerType = "

    .line 17
    .line 18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    const-string v2, "AccountPurchaseFullScreenActivity"

    .line 29
    .line 30
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    const/4 v1, 0x1

    .line 34
    const/4 v2, 0x0

    .line 35
    if-ne v0, v1, :cond_0

    .line 36
    .line 37
    new-instance v0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity$NewResStyle;

    .line 38
    .line 39
    invoke-direct {v0, p0, v2}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity$NewResStyle;-><init>(Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;Lcom/intsig/camscanner/purchase/activity/〇8o8o〇;)V

    .line 40
    .line 41
    .line 42
    return-object v0

    .line 43
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity$OldResStyle;

    .line 44
    .line 45
    invoke-direct {v0, p0, v2}, Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity$OldResStyle;-><init>(Lcom/intsig/camscanner/purchase/activity/AccountPurchaseFullScreenActivity;Lcom/intsig/camscanner/purchase/activity/〇O00;)V

    .line 46
    .line 47
    .line 48
    return-object v0
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

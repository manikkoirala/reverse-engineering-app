.class public final Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;
.super Landroidx/viewpager/widget/ViewPager;
.source "AutoScrollViewPager.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$Direction;,
        Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$SlideBorderMode;,
        Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$MyHandler;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private O0O:Z

.field private O8o08O8O:Z

.field private OO:J

.field private OO〇00〇8oO:F

.field private final o0:I

.field private o8〇OO0〇0o:F

.field private oOo0:Z

.field private oOo〇8o008:Z

.field private ooo0〇〇O:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference<",
            "Landroid/os/Handler;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o〇00O:Z

.field private 〇080OO8〇0:Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$SlideBorderMode;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇08O〇00〇o:Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$Direction;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇0O:Z

.field private 〇8〇oO〇〇8o:Lcom/intsig/camscanner/purchase/wediget/CustomDurationScroller;

.field private final 〇OOo8〇0:I

.field private 〇〇08O:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0, p1, p2}, Landroidx/viewpager/widget/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 7
    .line 8
    .line 9
    const/16 p1, 0x5dc

    .line 10
    .line 11
    iput p1, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->o0:I

    .line 12
    .line 13
    int-to-long p1, p1

    .line 14
    iput-wide p1, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->OO:J

    .line 15
    .line 16
    sget-object p1, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$Direction;->RIGHT:Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$Direction;

    .line 17
    .line 18
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->〇08O〇00〇o:Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$Direction;

    .line 19
    .line 20
    const/4 p1, 0x1

    .line 21
    iput-boolean p1, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->o〇00O:Z

    .line 22
    .line 23
    iput-boolean p1, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->O8o08O8O:Z

    .line 24
    .line 25
    sget-object p2, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$SlideBorderMode;->NONE:Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$SlideBorderMode;

    .line 26
    .line 27
    iput-object p2, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->〇080OO8〇0:Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$SlideBorderMode;

    .line 28
    .line 29
    iput-boolean p1, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->〇0O:Z

    .line 30
    .line 31
    iput-boolean p1, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->O0O:Z

    .line 32
    .line 33
    new-instance p1, Ljava/lang/ref/WeakReference;

    .line 34
    .line 35
    new-instance p2, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$MyHandler;

    .line 36
    .line 37
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$MyHandler;-><init>(Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;)V

    .line 38
    .line 39
    .line 40
    invoke-direct {p1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 41
    .line 42
    .line 43
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->ooo0〇〇O:Ljava/lang/ref/WeakReference;

    .line 44
    .line 45
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->〇〇888()V

    .line 46
    .line 47
    .line 48
    return-void
    .line 49
.end method

.method private final o〇0(J)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->ooo0〇〇O:Ljava/lang/ref/WeakReference;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Landroid/os/Handler;

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    iget v1, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->〇OOo8〇0:I

    .line 12
    .line 13
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 14
    .line 15
    .line 16
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->ooo0〇〇O:Ljava/lang/ref/WeakReference;

    .line 17
    .line 18
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    check-cast v0, Landroid/os/Handler;

    .line 23
    .line 24
    if-eqz v0, :cond_1

    .line 25
    .line 26
    iget v1, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->〇OOo8〇0:I

    .line 27
    .line 28
    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 29
    .line 30
    .line 31
    :cond_1
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public static final synthetic 〇080(Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->OO:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static final synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->〇OOo8〇0:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static final synthetic 〇o〇(Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;J)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->o〇0(J)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final 〇〇888()V
    .locals 5

    .line 1
    const-class v0, Landroidx/viewpager/widget/ViewPager;

    .line 2
    .line 3
    :try_start_0
    const-string v1, "mScroller"

    .line 4
    .line 5
    invoke-virtual {v0, v1}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    const/4 v2, 0x1

    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    .line 11
    .line 12
    .line 13
    const-string v3, "sInterpolator"

    .line 14
    .line 15
    invoke-virtual {v0, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    .line 16
    .line 17
    .line 18
    move-result-object v0

    .line 19
    invoke-virtual {v0, v2}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    .line 20
    .line 21
    .line 22
    new-instance v2, Lcom/intsig/camscanner/purchase/wediget/CustomDurationScroller;

    .line 23
    .line 24
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 25
    .line 26
    .line 27
    move-result-object v3

    .line 28
    const-string v4, "context"

    .line 29
    .line 30
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    const/4 v4, 0x0

    .line 34
    invoke-virtual {v0, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    const-string v4, "null cannot be cast to non-null type android.view.animation.Interpolator"

    .line 39
    .line 40
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    .line 42
    .line 43
    check-cast v0, Landroid/view/animation/Interpolator;

    .line 44
    .line 45
    invoke-direct {v2, v3, v0}, Lcom/intsig/camscanner/purchase/wediget/CustomDurationScroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    .line 46
    .line 47
    .line 48
    iput-object v2, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/purchase/wediget/CustomDurationScroller;

    .line 49
    .line 50
    invoke-virtual {v1, p0, v2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    .line 52
    .line 53
    goto :goto_0

    .line 54
    :catch_0
    move-exception v0

    .line 55
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    .line 56
    .line 57
    .line 58
    :goto_0
    return-void
.end method


# virtual methods
.method public final O8()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->ooo0〇〇O:Ljava/lang/ref/WeakReference;

    .line 2
    .line 3
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    iget-boolean v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->〇〇08O:Z

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    new-instance v0, Ljava/lang/ref/WeakReference;

    .line 14
    .line 15
    new-instance v1, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$MyHandler;

    .line 16
    .line 17
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$MyHandler;-><init>(Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;)V

    .line 18
    .line 19
    .line 20
    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 21
    .line 22
    .line 23
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->ooo0〇〇O:Ljava/lang/ref/WeakReference;

    .line 24
    .line 25
    :cond_0
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public final Oo08()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Landroidx/viewpager/widget/ViewPager;->getAdapter()Landroidx/viewpager/widget/PagerAdapter;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_4

    .line 6
    .line 7
    invoke-virtual {p0}, Landroidx/viewpager/widget/ViewPager;->getAdapter()Landroidx/viewpager/widget/PagerAdapter;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 12
    .line 13
    .line 14
    invoke-virtual {v0}, Landroidx/viewpager/widget/PagerAdapter;->getCount()I

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    const/4 v1, 0x1

    .line 19
    if-gt v0, v1, :cond_0

    .line 20
    .line 21
    goto :goto_1

    .line 22
    :cond_0
    invoke-virtual {p0}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 23
    .line 24
    .line 25
    move-result v0

    .line 26
    invoke-virtual {p0}, Landroidx/viewpager/widget/ViewPager;->getAdapter()Landroidx/viewpager/widget/PagerAdapter;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 31
    .line 32
    .line 33
    invoke-virtual {v2}, Landroidx/viewpager/widget/PagerAdapter;->getCount()I

    .line 34
    .line 35
    .line 36
    move-result v2

    .line 37
    iget-object v3, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->〇08O〇00〇o:Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$Direction;

    .line 38
    .line 39
    sget-object v4, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$Direction;->LEFT:Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$Direction;

    .line 40
    .line 41
    if-ne v3, v4, :cond_1

    .line 42
    .line 43
    add-int/lit8 v0, v0, -0x1

    .line 44
    .line 45
    goto :goto_0

    .line 46
    :cond_1
    add-int/2addr v0, v1

    .line 47
    :goto_0
    if-gez v0, :cond_2

    .line 48
    .line 49
    iget-boolean v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->o〇00O:Z

    .line 50
    .line 51
    if-eqz v0, :cond_4

    .line 52
    .line 53
    sub-int/2addr v2, v1

    .line 54
    iget-boolean v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->〇0O:Z

    .line 55
    .line 56
    invoke-virtual {p0, v2, v0}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(IZ)V

    .line 57
    .line 58
    .line 59
    goto :goto_1

    .line 60
    :cond_2
    if-ne v0, v2, :cond_3

    .line 61
    .line 62
    iget-boolean v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->o〇00O:Z

    .line 63
    .line 64
    if-eqz v0, :cond_4

    .line 65
    .line 66
    const/4 v0, 0x0

    .line 67
    iget-boolean v1, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->〇0O:Z

    .line 68
    .line 69
    invoke-virtual {p0, v0, v1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(IZ)V

    .line 70
    .line 71
    .line 72
    goto :goto_1

    .line 73
    :cond_3
    invoke-virtual {p0, v0, v1}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(IZ)V

    .line 74
    .line 75
    .line 76
    :cond_4
    :goto_1
    return-void
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public final getDirection()Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$Direction;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->〇08O〇00〇o:Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$Direction;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final getInterval()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->OO:J

    .line 2
    .line 3
    return-wide v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final getSlideBorderMode()Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$SlideBorderMode;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->〇080OO8〇0:Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$SlideBorderMode;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final oO80()V
    .locals 2

    .line 1
    const/4 v0, 0x1

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->oOo〇8o008:Z

    .line 3
    .line 4
    iget-wide v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->OO:J

    .line 5
    .line 6
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->o〇0(J)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/viewpager/widget/ViewPager;->onAttachedToWindow()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->O8()V

    .line 5
    .line 6
    .line 7
    iget-boolean v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->O0O:Z

    .line 8
    .line 9
    if-eqz v0, :cond_0

    .line 10
    .line 11
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->oO80()V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-void
    .line 15
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .line 1
    invoke-super {p0}, Landroidx/viewpager/widget/ViewPager;->onDetachedFromWindow()V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->〇80〇808〇O()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1    # Landroid/view/MotionEvent;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "ev"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iget-boolean v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->O8o08O8O:Z

    .line 7
    .line 8
    const/4 v1, 0x1

    .line 9
    if-eqz v0, :cond_1

    .line 10
    .line 11
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-nez v0, :cond_0

    .line 16
    .line 17
    iget-boolean v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->oOo〇8o008:Z

    .line 18
    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    iput-boolean v1, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->oOo0:Z

    .line 22
    .line 23
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->〇80〇808〇O()V

    .line 24
    .line 25
    .line 26
    goto :goto_0

    .line 27
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 28
    .line 29
    .line 30
    move-result v0

    .line 31
    if-ne v0, v1, :cond_1

    .line 32
    .line 33
    iget-boolean v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->oOo0:Z

    .line 34
    .line 35
    if-eqz v0, :cond_1

    .line 36
    .line 37
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->oO80()V

    .line 38
    .line 39
    .line 40
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->〇080OO8〇0:Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$SlideBorderMode;

    .line 41
    .line 42
    sget-object v2, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$SlideBorderMode;->TO_PARENT:Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$SlideBorderMode;

    .line 43
    .line 44
    if-eq v0, v2, :cond_2

    .line 45
    .line 46
    sget-object v3, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$SlideBorderMode;->CYCLE:Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$SlideBorderMode;

    .line 47
    .line 48
    if-ne v0, v3, :cond_9

    .line 49
    .line 50
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    .line 51
    .line 52
    .line 53
    move-result v0

    .line 54
    iput v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->OO〇00〇8oO:F

    .line 55
    .line 56
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    .line 57
    .line 58
    .line 59
    move-result v0

    .line 60
    if-nez v0, :cond_3

    .line 61
    .line 62
    iget v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->OO〇00〇8oO:F

    .line 63
    .line 64
    iput v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->o8〇OO0〇0o:F

    .line 65
    .line 66
    :cond_3
    invoke-virtual {p0}, Landroidx/viewpager/widget/ViewPager;->getCurrentItem()I

    .line 67
    .line 68
    .line 69
    move-result v0

    .line 70
    invoke-virtual {p0}, Landroidx/viewpager/widget/ViewPager;->getAdapter()Landroidx/viewpager/widget/PagerAdapter;

    .line 71
    .line 72
    .line 73
    move-result-object v3

    .line 74
    const/4 v4, 0x0

    .line 75
    if-eqz v3, :cond_4

    .line 76
    .line 77
    invoke-virtual {v3}, Landroidx/viewpager/widget/PagerAdapter;->getCount()I

    .line 78
    .line 79
    .line 80
    move-result v3

    .line 81
    goto :goto_1

    .line 82
    :cond_4
    const/4 v3, 0x0

    .line 83
    :goto_1
    if-nez v0, :cond_5

    .line 84
    .line 85
    iget v5, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->o8〇OO0〇0o:F

    .line 86
    .line 87
    iget v6, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->OO〇00〇8oO:F

    .line 88
    .line 89
    cmpg-float v5, v5, v6

    .line 90
    .line 91
    if-lez v5, :cond_6

    .line 92
    .line 93
    :cond_5
    add-int/lit8 v5, v3, -0x1

    .line 94
    .line 95
    if-ne v0, v5, :cond_9

    .line 96
    .line 97
    iget v5, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->o8〇OO0〇0o:F

    .line 98
    .line 99
    iget v6, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->OO〇00〇8oO:F

    .line 100
    .line 101
    cmpl-float v5, v5, v6

    .line 102
    .line 103
    if-ltz v5, :cond_9

    .line 104
    .line 105
    :cond_6
    iget-object v5, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->〇080OO8〇0:Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$SlideBorderMode;

    .line 106
    .line 107
    if-ne v5, v2, :cond_7

    .line 108
    .line 109
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 110
    .line 111
    .line 112
    move-result-object v0

    .line 113
    invoke-interface {v0, v4}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 114
    .line 115
    .line 116
    goto :goto_2

    .line 117
    :cond_7
    if-le v3, v1, :cond_8

    .line 118
    .line 119
    sub-int/2addr v3, v0

    .line 120
    sub-int/2addr v3, v1

    .line 121
    iget-boolean v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->〇0O:Z

    .line 122
    .line 123
    invoke-virtual {p0, v3, v0}, Landroidx/viewpager/widget/ViewPager;->setCurrentItem(IZ)V

    .line 124
    .line 125
    .line 126
    :cond_8
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 127
    .line 128
    .line 129
    move-result-object v0

    .line 130
    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 131
    .line 132
    .line 133
    :goto_2
    invoke-super {p0, p1}, Landroidx/viewpager/widget/ViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 134
    .line 135
    .line 136
    move-result p1

    .line 137
    return p1

    .line 138
    :cond_9
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 139
    .line 140
    .line 141
    move-result-object v0

    .line 142
    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 143
    .line 144
    .line 145
    invoke-super {p0, p1}, Landroidx/viewpager/widget/ViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 146
    .line 147
    .line 148
    move-result p1

    .line 149
    return p1
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method public final setBorderAnimation(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->〇0O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final setCycle(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->o〇00O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final setDirection(Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$Direction;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$Direction;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "direction"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->〇08O〇00〇o:Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$Direction;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final setEnableAutoScroll(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->O0O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final setInterval(J)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->OO:J

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final setNeedResetHandler(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->〇〇08O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final setScrollDurationFactor(D)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/purchase/wediget/CustomDurationScroller;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0, p1, p2}, Lcom/intsig/camscanner/purchase/wediget/CustomDurationScroller;->〇080(D)V

    .line 6
    .line 7
    .line 8
    :cond_0
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final setSlideBorderMode(Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$SlideBorderMode;)V
    .locals 1
    .param p1    # Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$SlideBorderMode;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string/jumbo v0, "slideBorderMode"

    .line 2
    .line 3
    .line 4
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->〇080OO8〇0:Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager$SlideBorderMode;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final setStopScrollWhenTouch(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->O8o08O8O:Z

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final 〇80〇808〇O()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->oOo〇8o008:Z

    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->ooo0〇〇O:Ljava/lang/ref/WeakReference;

    .line 5
    .line 6
    invoke-virtual {v0}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    check-cast v0, Landroid/os/Handler;

    .line 11
    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    iget v1, p0, Lcom/intsig/camscanner/purchase/wediget/AutoScrollViewPager;->〇OOo8〇0:I

    .line 15
    .line 16
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 17
    .line 18
    .line 19
    :cond_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

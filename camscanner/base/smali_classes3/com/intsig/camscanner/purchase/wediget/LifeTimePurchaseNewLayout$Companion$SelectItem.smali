.class public final enum Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;
.super Ljava/lang/Enum;
.source "LifeTimePurchaseNewLayout.kt"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SelectItem"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;

.field public static final enum LEFT:Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;

.field public static final enum MIDDLE:Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;

.field public static final enum RIGHT:Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;


# direct methods
.method private static final synthetic $values()[Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;
    .locals 3

    .line 1
    const/4 v0, 0x3

    .line 2
    new-array v0, v0, [Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;

    .line 3
    .line 4
    const/4 v1, 0x0

    .line 5
    sget-object v2, Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;->LEFT:Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;

    .line 6
    .line 7
    aput-object v2, v0, v1

    .line 8
    .line 9
    const/4 v1, 0x1

    .line 10
    sget-object v2, Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;->MIDDLE:Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;

    .line 11
    .line 12
    aput-object v2, v0, v1

    .line 13
    .line 14
    const/4 v1, 0x2

    .line 15
    sget-object v2, Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;->RIGHT:Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;

    .line 16
    .line 17
    aput-object v2, v0, v1

    .line 18
    .line 19
    return-object v0
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;

    .line 2
    .line 3
    const-string v1, "LEFT"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;-><init>(Ljava/lang/String;I)V

    .line 7
    .line 8
    .line 9
    sput-object v0, Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;->LEFT:Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;

    .line 10
    .line 11
    new-instance v0, Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;

    .line 12
    .line 13
    const-string v1, "MIDDLE"

    .line 14
    .line 15
    const/4 v2, 0x1

    .line 16
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;-><init>(Ljava/lang/String;I)V

    .line 17
    .line 18
    .line 19
    sput-object v0, Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;->MIDDLE:Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;

    .line 20
    .line 21
    new-instance v0, Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;

    .line 22
    .line 23
    const-string v1, "RIGHT"

    .line 24
    .line 25
    const/4 v2, 0x2

    .line 26
    invoke-direct {v0, v1, v2}, Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;-><init>(Ljava/lang/String;I)V

    .line 27
    .line 28
    .line 29
    sput-object v0, Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;->RIGHT:Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;

    .line 30
    .line 31
    invoke-static {}, Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;->$values()[Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    sput-object v0, Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;->$VALUES:[Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;

    .line 36
    .line 37
    return-void
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static values()[Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;->$VALUES:[Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;

    .line 2
    .line 3
    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/camscanner/purchase/wediget/LifeTimePurchaseNewLayout$Companion$SelectItem;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

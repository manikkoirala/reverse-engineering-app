.class public Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;
.super Ljava/lang/Object;
.source "UsePointsDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/intsig/camscanner/purchase/UsePointsDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private O8:Lcom/intsig/camscanner/purchase/UsePointsDialog$UseCallback;

.field private Oo08:Z

.field private 〇080:Landroid/content/Context;

.field private 〇o00〇〇Oo:Ljava/lang/String;

.field private 〇o〇:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x1

    .line 5
    iput-boolean v0, p0, Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;->Oo08:Z

    .line 6
    .line 7
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;->〇080:Landroid/content/Context;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic O8(Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;)Lcom/intsig/camscanner/purchase/UsePointsDialog$UseCallback;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;->O8:Lcom/intsig/camscanner/purchase/UsePointsDialog$UseCallback;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic 〇080(Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;)I
    .locals 0

    .line 1
    iget p0, p0, Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;->〇o〇:I

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;->〇o00〇〇Oo:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method static bridge synthetic 〇o〇(Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;->Oo08:Z

    .line 2
    .line 3
    return p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public Oo08(I)Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;->〇o〇:I

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public oO80(Lcom/intsig/camscanner/purchase/UsePointsDialog$UseCallback;)Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;->O8:Lcom/intsig/camscanner/purchase/UsePointsDialog$UseCallback;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public o〇0(Z)Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;->Oo08:Z

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public 〇80〇808〇O()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/purchase/UsePointsDialog;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;->〇080:Landroid/content/Context;

    .line 4
    .line 5
    invoke-direct {v0, v1, p0}, Lcom/intsig/camscanner/purchase/UsePointsDialog;-><init>(Landroid/content/Context;Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/UsePointsDialog;->show()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇〇888(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/UsePointsDialog$Builder;->〇o00〇〇Oo:Ljava/lang/String;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

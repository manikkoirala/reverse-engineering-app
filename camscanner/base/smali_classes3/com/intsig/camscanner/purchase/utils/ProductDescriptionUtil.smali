.class public final Lcom/intsig/camscanner/purchase/utils/ProductDescriptionUtil;
.super Ljava/lang/Object;
.source "ProductDescriptionUtil.kt"


# annotations
.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇080:Lcom/intsig/camscanner/purchase/utils/ProductDescriptionUtil;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lcom/intsig/camscanner/purchase/utils/ProductDescriptionUtil;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/purchase/utils/ProductDescriptionUtil;-><init>()V

    .line 4
    .line 5
    .line 6
    sput-object v0, Lcom/intsig/camscanner/purchase/utils/ProductDescriptionUtil;->〇080:Lcom/intsig/camscanner/purchase/utils/ProductDescriptionUtil;

    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private constructor <init>()V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method


# virtual methods
.method public final 〇080(Ljava/lang/String;)Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;
    .locals 7

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x1

    .line 3
    if-eqz p1, :cond_1

    .line 4
    .line 5
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    .line 6
    .line 7
    .line 8
    move-result v2

    .line 9
    if-nez v2, :cond_0

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v2, 0x0

    .line 13
    goto :goto_1

    .line 14
    :cond_1
    :goto_0
    const/4 v2, 0x1

    .line 15
    :goto_1
    const/4 v3, 0x0

    .line 16
    if-eqz v2, :cond_2

    .line 17
    .line 18
    return-object v3

    .line 19
    :cond_2
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 20
    .line 21
    .line 22
    move-result-object v2

    .line 23
    invoke-virtual {v2}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 24
    .line 25
    .line 26
    move-result-object v2

    .line 27
    iget-object v2, v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->product_description:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductDescription;

    .line 28
    .line 29
    if-eqz v2, :cond_3

    .line 30
    .line 31
    iget-object v2, v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductDescription;->trial_rule_list:Ljava/util/List;

    .line 32
    .line 33
    goto :goto_2

    .line 34
    :cond_3
    move-object v2, v3

    .line 35
    :goto_2
    move-object v4, v2

    .line 36
    check-cast v4, Ljava/util/Collection;

    .line 37
    .line 38
    if-eqz v4, :cond_5

    .line 39
    .line 40
    invoke-interface {v4}, Ljava/util/Collection;->isEmpty()Z

    .line 41
    .line 42
    .line 43
    move-result v4

    .line 44
    if-eqz v4, :cond_4

    .line 45
    .line 46
    goto :goto_3

    .line 47
    :cond_4
    const/4 v4, 0x0

    .line 48
    goto :goto_4

    .line 49
    :cond_5
    :goto_3
    const/4 v4, 0x1

    .line 50
    :goto_4
    if-eqz v4, :cond_6

    .line 51
    .line 52
    return-object v3

    .line 53
    :cond_6
    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 54
    .line 55
    .line 56
    move-result v4

    .line 57
    const/4 v5, 0x0

    .line 58
    :goto_5
    if-ge v5, v4, :cond_b

    .line 59
    .line 60
    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 61
    .line 62
    .line 63
    move-result-object v6

    .line 64
    check-cast v6, Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;

    .line 65
    .line 66
    iget-object v6, v6, Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;->product_id:Ljava/lang/String;

    .line 67
    .line 68
    if-eqz v6, :cond_8

    .line 69
    .line 70
    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    .line 71
    .line 72
    .line 73
    move-result v6

    .line 74
    if-nez v6, :cond_7

    .line 75
    .line 76
    goto :goto_6

    .line 77
    :cond_7
    const/4 v6, 0x0

    .line 78
    goto :goto_7

    .line 79
    :cond_8
    :goto_6
    const/4 v6, 0x1

    .line 80
    :goto_7
    if-eqz v6, :cond_9

    .line 81
    .line 82
    goto :goto_8

    .line 83
    :cond_9
    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 84
    .line 85
    .line 86
    move-result-object v6

    .line 87
    check-cast v6, Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;

    .line 88
    .line 89
    iget-object v6, v6, Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;->product_id:Ljava/lang/String;

    .line 90
    .line 91
    invoke-static {v6, p1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 92
    .line 93
    .line 94
    move-result v6

    .line 95
    if-eqz v6, :cond_a

    .line 96
    .line 97
    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    .line 98
    .line 99
    .line 100
    move-result-object p1

    .line 101
    check-cast p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;

    .line 102
    .line 103
    return-object p1

    .line 104
    :cond_a
    :goto_8
    add-int/lit8 v5, v5, 0x1

    .line 105
    .line 106
    goto :goto_5

    .line 107
    :cond_b
    return-object v3
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

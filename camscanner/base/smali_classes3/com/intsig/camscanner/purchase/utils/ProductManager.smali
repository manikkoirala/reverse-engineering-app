.class public Lcom/intsig/camscanner/purchase/utils/ProductManager;
.super Ljava/lang/Object;
.source "ProductManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/purchase/utils/ProductManager$ProductManagerHolder;
    }
.end annotation


# instance fields
.field private O8:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/ref/WeakReference<",
            "Lcom/intsig/camscanner/purchase/OnProductLoadListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private volatile 〇080:Lcom/intsig/comm/purchase/entity/QueryProductsResult;

.field private 〇o00〇〇Oo:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap<",
            "Lcom/intsig/comm/purchase/entity/ProductEnum;",
            "Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;",
            ">;"
        }
    .end annotation
.end field

.field private 〇o〇:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lcom/intsig/comm/purchase/entity/ProductResultItem;",
            "Lcom/intsig/comm/purchase/entity/ProductResultItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 2

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/intsig/comm/purchase/entity/ProductEnum;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 4
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o〇:Ljava/util/Map;

    .line 5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->O8:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(LoO8/o〇O8〇〇o;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;-><init>()V

    return-void
.end method

.method private OO0o〇〇(Lcom/intsig/comm/purchase/entity/QueryProductsResult;)V
    .locals 5

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    const-string p1, "ProductManager"

    .line 4
    .line 5
    const-string v0, "queryProductsResult is null"

    .line 6
    .line 7
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 12
    .line 13
    if-nez v0, :cond_1

    .line 14
    .line 15
    new-instance v0, Ljava/util/EnumMap;

    .line 16
    .line 17
    const-class v1, Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 18
    .line 19
    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 20
    .line 21
    .line 22
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 23
    .line 24
    goto :goto_0

    .line 25
    :cond_1
    invoke-virtual {v0}, Ljava/util/AbstractMap;->isEmpty()Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-nez v0, :cond_2

    .line 30
    .line 31
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 32
    .line 33
    invoke-virtual {v0}, Ljava/util/EnumMap;->clear()V

    .line 34
    .line 35
    .line 36
    :cond_2
    :goto_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇8o8o〇(Lcom/intsig/comm/purchase/entity/QueryProductsResult;)V

    .line 37
    .line 38
    .line 39
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 40
    .line 41
    sget-object v1, Lcom/intsig/comm/purchase/entity/ProductEnum;->MONTH:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 42
    .line 43
    iget-object v2, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->month:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 44
    .line 45
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    .line 47
    .line 48
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 49
    .line 50
    sget-object v1, Lcom/intsig/comm/purchase/entity/ProductEnum;->YEAR:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 51
    .line 52
    iget-object v2, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->year:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 53
    .line 54
    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    .line 56
    .line 57
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 58
    .line 59
    sget-object v2, Lcom/intsig/comm/purchase/entity/ProductEnum;->WEEK:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 60
    .line 61
    iget-object v3, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->week:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 62
    .line 63
    invoke-virtual {v0, v2, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    .line 65
    .line 66
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 67
    .line 68
    sget-object v2, Lcom/intsig/comm/purchase/entity/ProductEnum;->MS:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 69
    .line 70
    iget-object v3, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->ms:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 71
    .line 72
    invoke-virtual {v0, v2, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    .line 74
    .line 75
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 76
    .line 77
    sget-object v2, Lcom/intsig/comm/purchase/entity/ProductEnum;->YS:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 78
    .line 79
    iget-object v3, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->ys:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 80
    .line 81
    invoke-virtual {v0, v2, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    .line 83
    .line 84
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 85
    .line 86
    sget-object v2, Lcom/intsig/comm/purchase/entity/ProductEnum;->WS:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 87
    .line 88
    iget-object v3, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->ws:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 89
    .line 90
    invoke-virtual {v0, v2, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    .line 92
    .line 93
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 94
    .line 95
    sget-object v2, Lcom/intsig/comm/purchase/entity/ProductEnum;->WS_DISCOUNT:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 96
    .line 97
    iget-object v3, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->ws_discount:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 98
    .line 99
    invoke-virtual {v0, v2, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    .line 101
    .line 102
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 103
    .line 104
    sget-object v2, Lcom/intsig/comm/purchase/entity/ProductEnum;->POINT:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 105
    .line 106
    iget-object v3, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->point:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 107
    .line 108
    invoke-virtual {v0, v2, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    .line 110
    .line 111
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 112
    .line 113
    sget-object v2, Lcom/intsig/comm/purchase/entity/ProductEnum;->LIFE_TIME:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 114
    .line 115
    iget-object v3, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->lifetime:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 116
    .line 117
    invoke-virtual {v0, v2, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    .line 119
    .line 120
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 121
    .line 122
    sget-object v2, Lcom/intsig/comm/purchase/entity/ProductEnum;->YEAR_24H:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 123
    .line 124
    iget-object v3, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->year_24h:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 125
    .line 126
    invoke-virtual {v0, v2, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    .line 128
    .line 129
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 130
    .line 131
    sget-object v2, Lcom/intsig/comm/purchase/entity/ProductEnum;->YEAR_48H:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 132
    .line 133
    iget-object v3, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->year_48h:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 134
    .line 135
    invoke-virtual {v0, v2, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    .line 137
    .line 138
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 139
    .line 140
    sget-object v2, Lcom/intsig/comm/purchase/entity/ProductEnum;->YEAR_GUIDE:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 141
    .line 142
    iget-object v3, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->year_guide:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 143
    .line 144
    invoke-virtual {v0, v2, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    .line 146
    .line 147
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 148
    .line 149
    sget-object v2, Lcom/intsig/comm/purchase/entity/ProductEnum;->YEAR_RECALL:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 150
    .line 151
    iget-object v3, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->year_recall:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 152
    .line 153
    invoke-virtual {v0, v2, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    .line 155
    .line 156
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 157
    .line 158
    sget-object v2, Lcom/intsig/comm/purchase/entity/ProductEnum;->YEAR_48HDISCOUNT:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 159
    .line 160
    iget-object v3, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->year_48hdiscount:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 161
    .line 162
    invoke-virtual {v0, v2, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    .line 164
    .line 165
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 166
    .line 167
    sget-object v2, Lcom/intsig/comm/purchase/entity/ProductEnum;->EXTRA_GUIDE_CN:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 168
    .line 169
    iget-object v3, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->extra_guide_cn:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 170
    .line 171
    invoke-virtual {v0, v2, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    .line 173
    .line 174
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 175
    .line 176
    sget-object v2, Lcom/intsig/comm/purchase/entity/ProductEnum;->YEAR_GUIDE_CN:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 177
    .line 178
    iget-object v3, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->year_guide_cn:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 179
    .line 180
    invoke-virtual {v0, v2, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    .line 182
    .line 183
    iget-object v0, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->extra_guide:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 184
    .line 185
    const/4 v2, 0x1

    .line 186
    if-eqz v0, :cond_4

    .line 187
    .line 188
    iget v3, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;->consume:I

    .line 189
    .line 190
    if-nez v3, :cond_3

    .line 191
    .line 192
    iget-object v3, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 193
    .line 194
    sget-object v4, Lcom/intsig/comm/purchase/entity/ProductEnum;->EXTRA_GUIDE:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 195
    .line 196
    invoke-virtual {v3, v4, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    .line 198
    .line 199
    goto :goto_1

    .line 200
    :cond_3
    if-ne v3, v2, :cond_4

    .line 201
    .line 202
    iget-object v3, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 203
    .line 204
    sget-object v4, Lcom/intsig/comm/purchase/entity/ProductEnum;->EXTRA_GUIDE2:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 205
    .line 206
    invoke-virtual {v3, v4, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    .line 208
    .line 209
    :cond_4
    :goto_1
    iget-object v0, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->guide:Lcom/intsig/comm/purchase/entity/QueryProductsResult$Guide;

    .line 210
    .line 211
    if-eqz v0, :cond_5

    .line 212
    .line 213
    iget-object v3, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 214
    .line 215
    sget-object v4, Lcom/intsig/comm/purchase/entity/ProductEnum;->Guide_YEAR:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 216
    .line 217
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$Guide;->year:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 218
    .line 219
    invoke-virtual {v3, v4, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    .line 221
    .line 222
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 223
    .line 224
    sget-object v3, Lcom/intsig/comm/purchase/entity/ProductEnum;->COUNT_DOWN_YEAR:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 225
    .line 226
    iget-object v4, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->guide:Lcom/intsig/comm/purchase/entity/QueryProductsResult$Guide;

    .line 227
    .line 228
    iget-object v4, v4, Lcom/intsig/comm/purchase/entity/QueryProductsResult$Guide;->count_down_year:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 229
    .line 230
    invoke-virtual {v0, v3, v4}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    .line 232
    .line 233
    :cond_5
    invoke-static {}, Lcom/intsig/utils/PreferenceUtil;->oO80()Lcom/intsig/utils/PreferenceUtil;

    .line 234
    .line 235
    .line 236
    move-result-object v0

    .line 237
    const-string v3, "EXTRA_TRIAL_YEAR"

    .line 238
    .line 239
    invoke-virtual {p1}, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->isTrialYear()Z

    .line 240
    .line 241
    .line 242
    move-result v4

    .line 243
    invoke-virtual {v0, v3, v4}, Lcom/intsig/utils/PreferenceUtil;->〇O00(Ljava/lang/String;Z)V

    .line 244
    .line 245
    .line 246
    invoke-static {}, Lcom/intsig/utils/PreferenceUtil;->oO80()Lcom/intsig/utils/PreferenceUtil;

    .line 247
    .line 248
    .line 249
    move-result-object v0

    .line 250
    const-string v3, "EXTRA_SHOW_GUIDE"

    .line 251
    .line 252
    invoke-virtual {p1}, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->isShowGuideGp()Z

    .line 253
    .line 254
    .line 255
    move-result v4

    .line 256
    invoke-virtual {v0, v3, v4}, Lcom/intsig/utils/PreferenceUtil;->〇O00(Ljava/lang/String;Z)V

    .line 257
    .line 258
    .line 259
    iget-object v0, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->year:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 260
    .line 261
    invoke-virtual {v1, v0}, Lcom/intsig/comm/purchase/entity/ProductEnum;->checkData(Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;)Z

    .line 262
    .line 263
    .line 264
    move-result v0

    .line 265
    if-eqz v0, :cond_6

    .line 266
    .line 267
    invoke-static {}, Lcom/intsig/utils/PreferenceUtil;->oO80()Lcom/intsig/utils/PreferenceUtil;

    .line 268
    .line 269
    .line 270
    move-result-object v0

    .line 271
    iget-object v1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->year:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 272
    .line 273
    iget-object v1, v1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;->price_info:Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;

    .line 274
    .line 275
    iget v1, v1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$PriceInfo;->activity:I

    .line 276
    .line 277
    const-string v3, "CS_ACTIVITY_PRODUCT"

    .line 278
    .line 279
    invoke-virtual {v0, v3, v1}, Lcom/intsig/utils/PreferenceUtil;->〇0〇O0088o(Ljava/lang/String;I)V

    .line 280
    .line 281
    .line 282
    :cond_6
    iget v0, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->price_copywriting:I

    .line 283
    .line 284
    const/4 v1, 0x0

    .line 285
    if-ne v0, v2, :cond_7

    .line 286
    .line 287
    const/4 v0, 0x1

    .line 288
    goto :goto_2

    .line 289
    :cond_7
    const/4 v0, 0x0

    .line 290
    :goto_2
    iget v3, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->guide_price_style:I

    .line 291
    .line 292
    invoke-static {v3}, Lcom/intsig/camscanner/util/PreferenceHelper;->Oo8o(I)V

    .line 293
    .line 294
    .line 295
    if-eqz v0, :cond_8

    .line 296
    .line 297
    const/4 v0, 0x1

    .line 298
    goto :goto_3

    .line 299
    :cond_8
    iget v0, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->price_full_screen:I

    .line 300
    .line 301
    :goto_3
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇0O0〇8〇(I)V

    .line 302
    .line 303
    .line 304
    iget-object v0, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->vip_popup:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPopup;

    .line 305
    .line 306
    if-eqz v0, :cond_9

    .line 307
    .line 308
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 309
    .line 310
    sget-object v3, Lcom/intsig/comm/purchase/entity/ProductEnum;->YEAR_IN_POP:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 311
    .line 312
    iget-object v4, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPopup;->year:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 313
    .line 314
    invoke-virtual {v1, v3, v4}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 315
    .line 316
    .line 317
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 318
    .line 319
    sget-object v3, Lcom/intsig/comm/purchase/entity/ProductEnum;->MONTH_IN_POP:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 320
    .line 321
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPopup;->month:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 322
    .line 323
    invoke-virtual {v1, v3, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    .line 325
    .line 326
    invoke-static {v2}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO0〇(Z)V

    .line 327
    .line 328
    .line 329
    goto :goto_4

    .line 330
    :cond_9
    invoke-static {v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO0〇(Z)V

    .line 331
    .line 332
    .line 333
    :goto_4
    iget-object v0, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->expire_price:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ExpirePrice;

    .line 334
    .line 335
    if-eqz v0, :cond_a

    .line 336
    .line 337
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 338
    .line 339
    sget-object v2, Lcom/intsig/comm/purchase/entity/ProductEnum;->EXPIRE_PRICE_MONTH:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 340
    .line 341
    iget-object v3, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ExpirePrice;->month:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 342
    .line 343
    invoke-virtual {v1, v2, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 344
    .line 345
    .line 346
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 347
    .line 348
    sget-object v2, Lcom/intsig/comm/purchase/entity/ProductEnum;->EXPIRE_PRICE_YEAR:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 349
    .line 350
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ExpirePrice;->year:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 351
    .line 352
    invoke-virtual {v1, v2, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 353
    .line 354
    .line 355
    :cond_a
    iget-object v0, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->recall_price:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ExpirePrice;

    .line 356
    .line 357
    if-eqz v0, :cond_b

    .line 358
    .line 359
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 360
    .line 361
    sget-object v2, Lcom/intsig/comm/purchase/entity/ProductEnum;->RECALL_PRICE_MONTH:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 362
    .line 363
    iget-object v3, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ExpirePrice;->month:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 364
    .line 365
    invoke-virtual {v1, v2, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    .line 367
    .line 368
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 369
    .line 370
    sget-object v2, Lcom/intsig/comm/purchase/entity/ProductEnum;->RECALL_PRICE_YEAR:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 371
    .line 372
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ExpirePrice;->year:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 373
    .line 374
    invoke-virtual {v1, v2, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 375
    .line 376
    .line 377
    :cond_b
    iget-object v0, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->vip_price_recall:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPriceRecall;

    .line 378
    .line 379
    if-eqz v0, :cond_d

    .line 380
    .line 381
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 382
    .line 383
    sget-object v2, Lcom/intsig/comm/purchase/entity/ProductEnum;->VIP_REDEEM_YEAR:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 384
    .line 385
    iget-object v3, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPriceRecall;->year:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 386
    .line 387
    invoke-virtual {v1, v2, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 388
    .line 389
    .line 390
    iget-object v1, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPriceRecall;->month:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 391
    .line 392
    if-eqz v1, :cond_c

    .line 393
    .line 394
    iget-object v2, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 395
    .line 396
    sget-object v3, Lcom/intsig/comm/purchase/entity/ProductEnum;->VIP_REDEEM_MONTH:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 397
    .line 398
    invoke-virtual {v2, v3, v1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 399
    .line 400
    .line 401
    :cond_c
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPriceRecall;->pic_url:Ljava/lang/String;

    .line 402
    .line 403
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇O00(Ljava/lang/String;)V

    .line 404
    .line 405
    .line 406
    :cond_d
    iget-object v0, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->me_price_recall:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPriceRecall;

    .line 407
    .line 408
    if-eqz v0, :cond_f

    .line 409
    .line 410
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 411
    .line 412
    sget-object v2, Lcom/intsig/comm/purchase/entity/ProductEnum;->WEB_ME_VIP_REDEEM_YEAR:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 413
    .line 414
    iget-object v3, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPriceRecall;->year:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 415
    .line 416
    invoke-virtual {v1, v2, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 417
    .line 418
    .line 419
    iget-object v1, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPriceRecall;->month:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 420
    .line 421
    if-eqz v1, :cond_e

    .line 422
    .line 423
    iget-object v2, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 424
    .line 425
    sget-object v3, Lcom/intsig/comm/purchase/entity/ProductEnum;->WEB_ME_VIP_REDEEM_MONTH:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 426
    .line 427
    invoke-virtual {v2, v3, v1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 428
    .line 429
    .line 430
    :cond_e
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPriceRecall;->pic_url:Ljava/lang/String;

    .line 431
    .line 432
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇O00(Ljava/lang/String;)V

    .line 433
    .line 434
    .line 435
    :cond_f
    iget-object v0, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->capacity_overrun_popup:Lcom/intsig/comm/purchase/entity/QueryProductsResult$CloudOverrun;

    .line 436
    .line 437
    if-eqz v0, :cond_10

    .line 438
    .line 439
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 440
    .line 441
    sget-object v2, Lcom/intsig/comm/purchase/entity/ProductEnum;->CLOUD_OVERRUN_MONTH:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 442
    .line 443
    iget-object v3, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$CloudOverrun;->product1:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 444
    .line 445
    invoke-virtual {v1, v2, v3}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 446
    .line 447
    .line 448
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 449
    .line 450
    sget-object v2, Lcom/intsig/comm/purchase/entity/ProductEnum;->CLOUD_OVERRUN_YEAR:Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 451
    .line 452
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$CloudOverrun;->product2:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;

    .line 453
    .line 454
    invoke-virtual {v1, v2, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 455
    .line 456
    .line 457
    :cond_10
    iget v0, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->recurring_user_pop_538_ab:I

    .line 458
    .line 459
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇O0〇O(I)V

    .line 460
    .line 461
    .line 462
    iget-object v0, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->countdown_popup:Lcom/intsig/comm/purchase/entity/QueryProductsResult$CountDownPopup;

    .line 463
    .line 464
    invoke-static {v0}, Lcom/intsig/okgo/utils/GsonUtils;->Oo08(Ljava/lang/Object;)Ljava/lang/String;

    .line 465
    .line 466
    .line 467
    move-result-object v0

    .line 468
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->o000oo(Ljava/lang/String;)V

    .line 469
    .line 470
    .line 471
    iget-object v0, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->renew_recall:Lcom/intsig/comm/purchase/entity/QueryProductsResult$RenewRecall;

    .line 472
    .line 473
    invoke-static {v0}, Lcom/intsig/okgo/utils/GsonUtils;->Oo08(Ljava/lang/Object;)Ljava/lang/String;

    .line 474
    .line 475
    .line 476
    move-result-object v0

    .line 477
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇〇O08〇oO(Ljava/lang/String;)V

    .line 478
    .line 479
    .line 480
    iget-object v0, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->user_attendance_week:Lcom/intsig/comm/purchase/entity/QueryProductsResult$UserAttendanceWeek;

    .line 481
    .line 482
    invoke-static {v0}, Lcom/intsig/okgo/utils/GsonUtils;->Oo08(Ljava/lang/Object;)Ljava/lang/String;

    .line 483
    .line 484
    .line 485
    move-result-object v0

    .line 486
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->oO0〇o800(Ljava/lang/String;)V

    .line 487
    .line 488
    .line 489
    sget-object v0, Lcom/intsig/camscanner/edu/EduBenefitHelper;->〇080:Lcom/intsig/camscanner/edu/EduBenefitHelper;

    .line 490
    .line 491
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/edu/EduBenefitHelper;->OO0o〇〇〇〇0(Lcom/intsig/comm/purchase/entity/QueryProductsResult;)V

    .line 492
    .line 493
    .line 494
    invoke-virtual {v0}, Lcom/intsig/camscanner/edu/EduBenefitHelper;->〇o〇()V

    .line 495
    .line 496
    .line 497
    invoke-static {}, Lcom/intsig/camscanner/purchase/scandone/vip_month/ScanDoneVipMonthManager;->OoO8()V

    .line 498
    .line 499
    .line 500
    invoke-static {}, Lcom/intsig/camscanner/purchase/manager/RejoinBenefitManager;->OO0o〇〇〇〇0()V

    .line 501
    .line 502
    .line 503
    return-void
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
.end method

.method public static o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager$ProductManagerHolder;->〇080()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    return-object v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/purchase/utils/ProductManager;Lcom/intsig/camscanner/purchase/OnProductLoadListener;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇O〇(Lcom/intsig/camscanner/purchase/OnProductLoadListener;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private 〇8o8o〇(Lcom/intsig/comm/purchase/entity/QueryProductsResult;)V
    .locals 1

    .line 1
    iget-object p1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->product_list:Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductResult;

    .line 2
    .line 3
    if-nez p1, :cond_0

    .line 4
    .line 5
    return-void

    .line 6
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o〇:Ljava/util/Map;

    .line 7
    .line 8
    if-nez v0, :cond_1

    .line 9
    .line 10
    new-instance v0, Ljava/util/HashMap;

    .line 11
    .line 12
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 13
    .line 14
    .line 15
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o〇:Ljava/util/Map;

    .line 16
    .line 17
    goto :goto_0

    .line 18
    :cond_1
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    .line 19
    .line 20
    .line 21
    move-result v0

    .line 22
    if-nez v0, :cond_2

    .line 23
    .line 24
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o〇:Ljava/util/Map;

    .line 25
    .line 26
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 27
    .line 28
    .line 29
    :cond_2
    :goto_0
    iget-object v0, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductResult;->product_alipay:Ljava/util/List;

    .line 30
    .line 31
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇O8o08O(Ljava/util/List;)V

    .line 32
    .line 33
    .line 34
    iget-object v0, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductResult;->product_wechat:Ljava/util/List;

    .line 35
    .line 36
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇O8o08O(Ljava/util/List;)V

    .line 37
    .line 38
    .line 39
    iget-object v0, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductResult;->product_google:Ljava/util/List;

    .line 40
    .line 41
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇O8o08O(Ljava/util/List;)V

    .line 42
    .line 43
    .line 44
    iget-object v0, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductResult;->product_huawei:Ljava/util/List;

    .line 45
    .line 46
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇O8o08O(Ljava/util/List;)V

    .line 47
    .line 48
    .line 49
    iget-object p1, p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductResult;->product_stripe:Ljava/util/List;

    .line 50
    .line 51
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇O8o08O(Ljava/util/List;)V

    .line 52
    .line 53
    .line 54
    return-void
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private 〇O00(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 8
    .line 9
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    const v1, 0x7f07059d

    .line 14
    .line 15
    .line 16
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    sget-object v1, Lcom/intsig/utils/ApplicationHelper;->〇OOo8〇0:Landroid/content/Context;

    .line 21
    .line 22
    invoke-static {v1}, Lcom/bumptech/glide/Glide;->OoO8(Landroid/content/Context;)Lcom/bumptech/glide/RequestManager;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    invoke-virtual {v1, p1}, Lcom/bumptech/glide/RequestManager;->〇〇808〇(Ljava/lang/String;)Lcom/bumptech/glide/RequestBuilder;

    .line 27
    .line 28
    .line 29
    move-result-object p1

    .line 30
    invoke-virtual {p1, v0, v0}, Lcom/bumptech/glide/RequestBuilder;->oO〇(II)Lcom/bumptech/glide/request/target/Target;

    .line 31
    .line 32
    .line 33
    :cond_0
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private 〇O8o08O(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductResultObj;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-static {p1}, Lcom/intsig/utils/ListUtils;->〇o〇(Ljava/util/List;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    return-void

    .line 8
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 9
    .line 10
    .line 11
    move-result-object p1

    .line 12
    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 13
    .line 14
    .line 15
    move-result v0

    .line 16
    if-eqz v0, :cond_4

    .line 17
    .line 18
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    check-cast v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductResultObj;

    .line 23
    .line 24
    iget-object v1, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductResultObj;->product_id:Ljava/util/List;

    .line 25
    .line 26
    invoke-static {v1}, Lcom/intsig/utils/ListUtils;->〇o〇(Ljava/util/List;)Z

    .line 27
    .line 28
    .line 29
    move-result v2

    .line 30
    if-eqz v2, :cond_2

    .line 31
    .line 32
    goto :goto_0

    .line 33
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 34
    .line 35
    .line 36
    move-result-object v1

    .line 37
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 38
    .line 39
    .line 40
    move-result v2

    .line 41
    if-eqz v2, :cond_1

    .line 42
    .line 43
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 44
    .line 45
    .line 46
    move-result-object v2

    .line 47
    check-cast v2, Ljava/lang/String;

    .line 48
    .line 49
    new-instance v9, Lcom/intsig/comm/purchase/entity/ProductResultItem;

    .line 50
    .line 51
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 52
    .line 53
    .line 54
    move-result v3

    .line 55
    if-nez v3, :cond_3

    .line 56
    .line 57
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    .line 58
    .line 59
    .line 60
    move-result-object v2

    .line 61
    :cond_3
    move-object v4, v2

    .line 62
    iget v5, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductResultObj;->payway:I

    .line 63
    .line 64
    iget v6, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductResultObj;->consume:I

    .line 65
    .line 66
    iget-object v7, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductResultObj;->propertyId:Ljava/lang/String;

    .line 67
    .line 68
    iget-object v8, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductResultObj;->product_title:Ljava/lang/String;

    .line 69
    .line 70
    move-object v3, v9

    .line 71
    invoke-direct/range {v3 .. v8}, Lcom/intsig/comm/purchase/entity/ProductResultItem;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    iget-object v2, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o〇:Ljava/util/Map;

    .line 75
    .line 76
    invoke-interface {v2, v9, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    .line 78
    .line 79
    goto :goto_1

    .line 80
    :cond_4
    return-void
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method private synthetic 〇O〇(Lcom/intsig/camscanner/purchase/OnProductLoadListener;Z)V
    .locals 2

    .line 1
    if-eqz p2, :cond_0

    .line 2
    .line 3
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇〇8O0〇8()V

    .line 4
    .line 5
    .line 6
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->O8:Ljava/util/List;

    .line 7
    .line 8
    invoke-static {v0}, Lcom/intsig/utils/ListUtils;->〇o〇(Ljava/util/List;)Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-nez v0, :cond_2

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->O8:Ljava/util/List;

    .line 15
    .line 16
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    if-eqz v1, :cond_2

    .line 25
    .line 26
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 31
    .line 32
    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    check-cast v1, Lcom/intsig/camscanner/purchase/OnProductLoadListener;

    .line 37
    .line 38
    if-eqz v1, :cond_1

    .line 39
    .line 40
    invoke-interface {v1, p2}, Lcom/intsig/camscanner/purchase/OnProductLoadListener;->〇080(Z)V

    .line 41
    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_2
    if-eqz p1, :cond_3

    .line 45
    .line 46
    invoke-interface {p1, p2}, Lcom/intsig/camscanner/purchase/OnProductLoadListener;->〇080(Z)V

    .line 47
    .line 48
    .line 49
    :cond_3
    return-void
.end method


# virtual methods
.method public O8()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lcom/intsig/comm/purchase/entity/ProductResultItem;",
            "Lcom/intsig/comm/purchase/entity/ProductResultItem;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o〇:Ljava/util/Map;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Ljava/util/HashMap;

    .line 6
    .line 7
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 8
    .line 9
    .line 10
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o〇:Ljava/util/Map;

    .line 11
    .line 12
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o〇:Ljava/util/Map;

    .line 13
    .line 14
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-nez v0, :cond_1

    .line 19
    .line 20
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o〇:Ljava/util/Map;

    .line 21
    .line 22
    return-object v0

    .line 23
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 24
    .line 25
    .line 26
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o〇:Ljava/util/Map;

    .line 27
    .line 28
    return-object v0
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public OO0o〇〇〇〇0()Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPopup;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->vip_popup:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPopup;

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPopup;

    .line 10
    .line 11
    invoke-direct {v0}, Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPopup;-><init>()V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-object v0
    .line 15
.end method

.method public Oo08(Lcom/intsig/comm/purchase/entity/ProductEnum;)Lcom/intsig/comm/purchase/entity/QueryProductsResult$ExpirePrice;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    invoke-virtual {p1}, Lcom/intsig/comm/purchase/entity/ProductEnum;->getDesc()Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object p1

    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    .line 14
    .line 15
    .line 16
    move-result-object p1

    .line 17
    invoke-virtual {p1, v0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    check-cast p1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$ExpirePrice;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 22
    .line 23
    return-object p1

    .line 24
    :catch_0
    move-exception p1

    .line 25
    const-string v0, "ProductManager"

    .line 26
    .line 27
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 28
    .line 29
    .line 30
    const/4 p1, 0x0

    .line 31
    return-object p1
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public Oooo8o0〇(Landroid/content/Context;Z)V
    .locals 2

    .line 1
    const/4 v0, 0x0

    .line 2
    const/4 v1, 0x0

    .line 3
    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇0〇O0088o(Landroid/content/Context;ZZLcom/intsig/camscanner/purchase/OnProductLoadListener;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;
    .locals 1
    .annotation build Landroidx/annotation/NonNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇080:Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇080:Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 6
    .line 7
    return-object v0

    .line 8
    :cond_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇〇8O0〇8()V

    .line 9
    .line 10
    .line 11
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇080:Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 12
    .line 13
    if-eqz v0, :cond_1

    .line 14
    .line 15
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇080:Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 16
    .line 17
    return-object v0

    .line 18
    :cond_1
    new-instance v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 19
    .line 20
    invoke-direct {v0}, Lcom/intsig/comm/purchase/entity/QueryProductsResult;-><init>()V

    .line 21
    .line 22
    .line 23
    return-object v0
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public 〇0〇O0088o(Landroid/content/Context;ZZLcom/intsig/camscanner/purchase/OnProductLoadListener;)V
    .locals 1

    .line 1
    new-instance v0, LoO8/〇oo〇;

    .line 2
    .line 3
    invoke-direct {v0, p0, p4}, LoO8/〇oo〇;-><init>(Lcom/intsig/camscanner/purchase/utils/ProductManager;Lcom/intsig/camscanner/purchase/OnProductLoadListener;)V

    .line 4
    .line 5
    .line 6
    invoke-static {p1, p2, p3, v0}, Lcom/intsig/camscanner/purchase/utils/PurchaseApiUtil;->O8(Landroid/content/Context;ZZLcom/intsig/camscanner/purchase/OnProductLoadListener;)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
.end method

.method public 〇80〇808〇O()Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPopup;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->vip_popup_guide:Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPopup;

    .line 6
    .line 7
    if-nez v0, :cond_0

    .line 8
    .line 9
    new-instance v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPopup;

    .line 10
    .line 11
    invoke-direct {v0}, Lcom/intsig/comm/purchase/entity/QueryProductsResult$VipPopup;-><init>()V

    .line 12
    .line 13
    .line 14
    :cond_0
    return-object v0
    .line 15
.end method

.method public 〇o00〇〇Oo(Lcom/intsig/camscanner/purchase/OnProductLoadListener;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->O8:Ljava/util/List;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/ref/WeakReference;

    .line 4
    .line 5
    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 6
    .line 7
    .line 8
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public 〇o〇()V
    .locals 2

    .line 1
    const-string v0, "ProductManager"

    .line 2
    .line 3
    const-string v1, "clear Product Info"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-string v0, ""

    .line 9
    .line 10
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->o〇〇8(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 14
    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    invoke-virtual {v0}, Ljava/util/AbstractMap;->isEmpty()Z

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-nez v0, :cond_0

    .line 22
    .line 23
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 24
    .line 25
    invoke-virtual {v0}, Ljava/util/EnumMap;->clear()V

    .line 26
    .line 27
    .line 28
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇080:Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 29
    .line 30
    if-eqz v0, :cond_1

    .line 31
    .line 32
    const/4 v0, 0x0

    .line 33
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇080:Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 34
    .line 35
    :cond_1
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public 〇〇808〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Ljava/util/AbstractMap;->isEmpty()Z

    .line 6
    .line 7
    .line 8
    move-result v0

    .line 9
    if-nez v0, :cond_0

    .line 10
    .line 11
    const/4 v0, 0x1

    .line 12
    goto :goto_0

    .line 13
    :cond_0
    const/4 v0, 0x0

    .line 14
    :goto_0
    return v0
    .line 15
.end method

.method public 〇〇888()Ljava/util/EnumMap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumMap<",
            "Lcom/intsig/comm/purchase/entity/ProductEnum;",
            "Lcom/intsig/comm/purchase/entity/QueryProductsResult$ProductItem;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    new-instance v0, Ljava/util/EnumMap;

    .line 6
    .line 7
    const-class v1, Lcom/intsig/comm/purchase/entity/ProductEnum;

    .line 8
    .line 9
    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 10
    .line 11
    .line 12
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 13
    .line 14
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 15
    .line 16
    invoke-virtual {v0}, Ljava/util/AbstractMap;->isEmpty()Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-nez v0, :cond_1

    .line 21
    .line 22
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 23
    .line 24
    return-object v0

    .line 25
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 26
    .line 27
    .line 28
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇o00〇〇Oo:Ljava/util/EnumMap;

    .line 29
    .line 30
    return-object v0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public 〇〇8O0〇8()V
    .locals 7

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇OO〇00〇0O()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v2

    .line 9
    new-instance v3, Ljava/lang/StringBuilder;

    .line 10
    .line 11
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 12
    .line 13
    .line 14
    const-string v4, "queryProductsResult = "

    .line 15
    .line 16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 17
    .line 18
    .line 19
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 20
    .line 21
    .line 22
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 23
    .line 24
    .line 25
    move-result-object v3

    .line 26
    const-string v4, "ProductManager"

    .line 27
    .line 28
    invoke-static {v4, v3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 32
    .line 33
    .line 34
    move-result v3

    .line 35
    if-eqz v3, :cond_0

    .line 36
    .line 37
    return-void

    .line 38
    :cond_0
    :try_start_0
    const-class v3, Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 39
    .line 40
    invoke-static {v2, v3}, Lcom/intsig/okgo/utils/GsonUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    .line 41
    .line 42
    .line 43
    move-result-object v2

    .line 44
    check-cast v2, Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 45
    .line 46
    iput-object v2, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇080:Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 47
    .line 48
    iget-object v2, p0, Lcom/intsig/camscanner/purchase/utils/ProductManager;->〇080:Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 49
    .line 50
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->OO0o〇〇(Lcom/intsig/comm/purchase/entity/QueryProductsResult;)V

    .line 51
    .line 52
    .line 53
    new-instance v2, Ljava/lang/StringBuilder;

    .line 54
    .line 55
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 56
    .line 57
    .line 58
    const-string v3, "current Thread is"

    .line 59
    .line 60
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    .line 64
    .line 65
    .line 66
    move-result-object v3

    .line 67
    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    .line 68
    .line 69
    .line 70
    move-result-object v3

    .line 71
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    .line 73
    .line 74
    const-string v3, ", parse QueryProductsResult json cost time = "

    .line 75
    .line 76
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 77
    .line 78
    .line 79
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 80
    .line 81
    .line 82
    move-result-wide v5

    .line 83
    sub-long/2addr v5, v0

    .line 84
    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇o00〇〇Oo(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    .line 93
    .line 94
    goto :goto_0

    .line 95
    :catch_0
    move-exception v0

    .line 96
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 97
    .line 98
    .line 99
    move-result-object v0

    .line 100
    invoke-static {v4, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    .line 102
    .line 103
    :goto_0
    return-void
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

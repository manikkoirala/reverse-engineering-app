.class public abstract Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;
.super Lcom/intsig/app/BaseBottomSheetDialogFragment;
.source "BaseVipMonthPromotionDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final oOo0:Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Z

.field private OO:I

.field private o0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$MonthlyPromotionPopCnPriceInfoStyle;

.field private oOo〇8o008:Z

.field private o〇00O:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

.field private 〇080OO8〇0:Z

.field private 〇08O〇00〇o:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

.field private final 〇0O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇OOo8〇0:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->oOo0:Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/app/BaseBottomSheetDialogFragment;-><init>()V

    .line 2
    .line 3
    .line 4
    const-string v0, ""

    .line 5
    .line 6
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇OOo8〇0:Ljava/lang/String;

    .line 7
    .line 8
    const/4 v0, 0x1

    .line 9
    iput v0, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->OO:I

    .line 10
    .line 11
    sget-object v0, Lkotlin/LazyThreadSafetyMode;->NONE:Lkotlin/LazyThreadSafetyMode;

    .line 12
    .line 13
    new-instance v1, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog$mFromPart$2;

    .line 14
    .line 15
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog$mFromPart$2;-><init>(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;)V

    .line 16
    .line 17
    .line 18
    invoke-static {v0, v1}, Lkotlin/LazyKt;->〇080(Lkotlin/LazyThreadSafetyMode;Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 19
    .line 20
    .line 21
    move-result-object v0

    .line 22
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇0O:Lkotlin/Lazy;

    .line 23
    .line 24
    return-void
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final O08〇()V
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇OOo8〇0:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "clickPurchase, productId: "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-virtual {p0, v1}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->logD(Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 24
    .line 25
    .line 26
    move-result v1

    .line 27
    const/4 v2, 0x0

    .line 28
    const/4 v3, 0x1

    .line 29
    if-nez v1, :cond_0

    .line 30
    .line 31
    const/4 v1, 0x1

    .line 32
    goto :goto_0

    .line 33
    :cond_0
    const/4 v1, 0x0

    .line 34
    :goto_0
    if-eqz v1, :cond_1

    .line 35
    .line 36
    return-void

    .line 37
    :cond_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇Oo〇O()V

    .line 38
    .line 39
    .line 40
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇O8〇8000()Z

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    if-nez v1, :cond_2

    .line 45
    .line 46
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇O8〇8O0oO()V

    .line 47
    .line 48
    .line 49
    return-void

    .line 50
    :cond_2
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->o〇00O:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 51
    .line 52
    if-nez v1, :cond_3

    .line 53
    .line 54
    const-string v1, "mPurchaseTracker"

    .line 55
    .line 56
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 57
    .line 58
    .line 59
    const/4 v1, 0x0

    .line 60
    :cond_3
    iput-object v0, v1, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->productId:Ljava/lang/String;

    .line 61
    .line 62
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->o0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$MonthlyPromotionPopCnPriceInfoStyle;

    .line 63
    .line 64
    if-eqz v1, :cond_4

    .line 65
    .line 66
    iget v1, v1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$MonthlyPromotionPopCnPriceInfoStyle;->is_timeline_agree:I

    .line 67
    .line 68
    if-ne v3, v1, :cond_4

    .line 69
    .line 70
    const/4 v2, 0x1

    .line 71
    :cond_4
    if-eqz v2, :cond_5

    .line 72
    .line 73
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇〇()V

    .line 74
    .line 75
    .line 76
    goto :goto_1

    .line 77
    :cond_5
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 78
    .line 79
    if-eqz v1, :cond_6

    .line 80
    .line 81
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->oO00OOO(I)V

    .line 82
    .line 83
    .line 84
    iget v2, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->OO:I

    .line 85
    .line 86
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->〇80(I)V

    .line 87
    .line 88
    .line 89
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->O0O8OO088(Ljava/lang/String;)V

    .line 90
    .line 91
    .line 92
    :cond_6
    :goto_1
    return-void
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private static final O0O0〇(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    const-string/jumbo p1, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    if-nez p1, :cond_0

    .line 12
    .line 13
    const-string p1, "close"

    .line 14
    .line 15
    const-string p2, "blank"

    .line 16
    .line 17
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇o〇88〇8(Ljava/lang/String;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    :cond_0
    const/4 p0, 0x0

    .line 21
    return p0
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private static final O0〇0(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;Landroid/widget/CompoundButton;Z)V
    .locals 1

    .line 1
    const-string/jumbo p1, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    iget-boolean p1, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->oOo〇8o008:Z

    .line 8
    .line 9
    if-eqz p1, :cond_0

    .line 10
    .line 11
    const/4 p1, 0x0

    .line 12
    iput-boolean p1, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->oOo〇8o008:Z

    .line 13
    .line 14
    goto :goto_0

    .line 15
    :cond_0
    const-string p1, "agree"

    .line 16
    .line 17
    const/4 p2, 0x2

    .line 18
    const/4 v0, 0x0

    .line 19
    invoke-static {p0, p1, v0, p2, v0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇8〇80o(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    .line 20
    .line 21
    .line 22
    :goto_0
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public static final synthetic o00〇88〇08(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->O08〇()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static final synthetic o880(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇08O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static synthetic oOo〇08〇(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->O0O0〇(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;Landroid/view/View;Landroid/view/MotionEvent;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public static synthetic oO〇oo(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇〇O80〇0o(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public static final synthetic oooO888(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->logD(Ljava/lang/String;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final o〇0〇o()V
    .locals 2

    .line 1
    const-string v0, "close"

    .line 2
    .line 3
    const-string v1, "give_up"

    .line 4
    .line 5
    invoke-virtual {p0, v0, v1}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇o〇88〇8(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->dismissAllowingStateLoss()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private final 〇08O()V
    .locals 2

    .line 1
    const-string v0, "onPrivacyAgree"

    .line 2
    .line 3
    invoke-virtual {p0, v0}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->logD(Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    iput-boolean v0, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->oOo〇8o008:Z

    .line 8
    .line 9
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->o〇O8OO()Landroid/widget/CheckBox;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    if-nez v1, :cond_0

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    invoke-virtual {v1, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 17
    .line 18
    .line 19
    :goto_0
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static synthetic 〇8〇80o(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V
    .locals 0

    .line 1
    if-nez p4, :cond_1

    .line 2
    .line 3
    and-int/lit8 p3, p3, 0x2

    .line 4
    .line 5
    if-eqz p3, :cond_0

    .line 6
    .line 7
    const-string p2, ""

    .line 8
    .line 9
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇o〇88〇8(Ljava/lang/String;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    return-void

    .line 13
    :cond_1
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    .line 14
    .line 15
    const-string p1, "Super calls with default arguments not supported in this target, function: clickFunction"

    .line 16
    .line 17
    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    throw p0
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
.end method

.method private final 〇8〇OOoooo()Z
    .locals 4

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇o08()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->o0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$MonthlyPromotionPopCnPriceInfoStyle;

    .line 5
    .line 6
    const/4 v1, 0x0

    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$MonthlyPromotionPopCnPriceInfoStyle;->product_id:Ljava/lang/String;

    .line 10
    .line 11
    goto :goto_0

    .line 12
    :cond_0
    move-object v0, v1

    .line 13
    :goto_0
    const/4 v2, 0x0

    .line 14
    const/4 v3, 0x1

    .line 15
    if-eqz v0, :cond_2

    .line 16
    .line 17
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    if-nez v0, :cond_1

    .line 22
    .line 23
    goto :goto_1

    .line 24
    :cond_1
    const/4 v0, 0x0

    .line 25
    goto :goto_2

    .line 26
    :cond_2
    :goto_1
    const/4 v0, 0x1

    .line 27
    :goto_2
    if-eqz v0, :cond_3

    .line 28
    .line 29
    const-string v0, "checkData, product_id isNullOrEmpty"

    .line 30
    .line 31
    invoke-virtual {p0, v0}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->logD(Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    return v2

    .line 35
    :cond_3
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->o0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$MonthlyPromotionPopCnPriceInfoStyle;

    .line 36
    .line 37
    if-eqz v0, :cond_4

    .line 38
    .line 39
    iget-object v1, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$MonthlyPromotionPopCnPriceInfoStyle;->product_id:Ljava/lang/String;

    .line 40
    .line 41
    :cond_4
    if-nez v1, :cond_5

    .line 42
    .line 43
    const-string v1, ""

    .line 44
    .line 45
    :cond_5
    iput-object v1, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇OOo8〇0:Ljava/lang/String;

    .line 46
    .line 47
    return v3
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static synthetic 〇O8oOo0(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;Landroid/widget/CompoundButton;Z)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->O0〇0(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;Landroid/widget/CompoundButton;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private final 〇O8〇8000()Z
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->o〇O8OO()Landroid/widget/CheckBox;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    const/4 v2, 0x1

    .line 13
    if-ne v2, v0, :cond_0

    .line 14
    .line 15
    const/4 v1, 0x1

    .line 16
    :cond_0
    return v1
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final 〇O8〇8O0oO()V
    .locals 3

    .line 1
    const-string/jumbo v0, "showPrivacyDialog"

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->logD(Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    sget-object v0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/VipMonthPrivacyDialog;->OO:Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/VipMonthPrivacyDialog$Companion;

    .line 8
    .line 9
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/VipMonthPrivacyDialog$Companion;->〇080()Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/VipMonthPrivacyDialog;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    new-instance v1, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog$showPrivacyDialog$1$1;

    .line 14
    .line 15
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog$showPrivacyDialog$1$1;-><init>(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/VipMonthPrivacyDialog;->oOo〇08〇(Lkotlin/jvm/functions/Function1;)V

    .line 19
    .line 20
    .line 21
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 22
    .line 23
    .line 24
    move-result-object v1

    .line 25
    const-string v2, "childFragmentManager"

    .line 26
    .line 27
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    const-string v2, "VipMonthPrivacyDialog"

    .line 31
    .line 32
    invoke-virtual {v0, v1, v2}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    return-void
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final 〇〇()V
    .locals 12

    .line 1
    const-string/jumbo v0, "showTimeLineDialog"

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, v0}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->logD(Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    new-instance v0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog$showTimeLineDialog$trialRuleDialogListener$1;

    .line 8
    .line 9
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog$showTimeLineDialog$trialRuleDialogListener$1;-><init>(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;)V

    .line 10
    .line 11
    .line 12
    sget-object v10, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->o8o:Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog$Companion;

    .line 13
    .line 14
    sget-object v1, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseManager;->〇080:Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseManager;

    .line 15
    .line 16
    iget-object v2, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇OOo8〇0:Ljava/lang/String;

    .line 17
    .line 18
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/purchase/vipactivity/VipActivityPurchaseManager;->O8(Ljava/lang/String;)Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;

    .line 19
    .line 20
    .line 21
    move-result-object v2

    .line 22
    iget-object v3, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇OOo8〇0:Ljava/lang/String;

    .line 23
    .line 24
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->o0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$MonthlyPromotionPopCnPriceInfoStyle;

    .line 25
    .line 26
    const/4 v4, 0x0

    .line 27
    if-eqz v1, :cond_0

    .line 28
    .line 29
    iget-object v1, v1, Lcom/intsig/comm/purchase/entity/QueryProductsResult$MonthlyPromotionPopCnPriceInfoStyle;->timeline_pop_button_description:Ljava/lang/String;

    .line 30
    .line 31
    goto :goto_0

    .line 32
    :cond_0
    move-object v1, v4

    .line 33
    :goto_0
    if-nez v1, :cond_1

    .line 34
    .line 35
    const-string v1, ""

    .line 36
    .line 37
    :cond_1
    move-object v5, v1

    .line 38
    iget v6, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->OO:I

    .line 39
    .line 40
    iget-object v1, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->o〇00O:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 41
    .line 42
    if-nez v1, :cond_2

    .line 43
    .line 44
    const-string v1, "mPurchaseTracker"

    .line 45
    .line 46
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    move-object v7, v4

    .line 50
    goto :goto_1

    .line 51
    :cond_2
    move-object v7, v1

    .line 52
    :goto_1
    const/4 v8, 0x0

    .line 53
    const/16 v9, 0x20

    .line 54
    .line 55
    const/4 v11, 0x0

    .line 56
    move-object v1, v10

    .line 57
    move-object v4, v5

    .line 58
    move v5, v6

    .line 59
    move-object v6, v7

    .line 60
    move-object v7, v8

    .line 61
    move v8, v9

    .line 62
    move-object v9, v11

    .line 63
    invoke-static/range {v1 .. v9}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog$Companion;->〇o〇(Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog$Companion;Lcom/intsig/comm/purchase/entity/QueryProductsResult$TrialRules;Ljava/lang/String;Ljava/lang/String;ILcom/intsig/camscanner/purchase/track/PurchaseTracker;Lcom/intsig/camscanner/guide/dropchannel/dialog/TrialRuleDialogListener;ILjava/lang/Object;)Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;

    .line 64
    .line 65
    .line 66
    move-result-object v1

    .line 67
    new-instance v2, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog$showTimeLineDialog$instance$1;

    .line 68
    .line 69
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog$showTimeLineDialog$instance$1;-><init>(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;)V

    .line 70
    .line 71
    .line 72
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->O0O0〇(Lcom/intsig/camscanner/guide/GuideGpPurchaseStyleFragment$OnLastGuidePageListener;)Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;

    .line 73
    .line 74
    .line 75
    move-result-object v1

    .line 76
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇Oo〇O(Lcom/intsig/camscanner/guide/dropchannel/dialog/TrialRuleDialogListener;)V

    .line 77
    .line 78
    .line 79
    const/4 v0, 0x1

    .line 80
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇8O0880(Z)V

    .line 81
    .line 82
    .line 83
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->〇O0o〇〇o(Z)V

    .line 84
    .line 85
    .line 86
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇8O0880()Z

    .line 87
    .line 88
    .line 89
    move-result v0

    .line 90
    invoke-virtual {v1, v0}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog;->O8〇8〇O80(Z)V

    .line 91
    .line 92
    .line 93
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getChildFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 94
    .line 95
    .line 96
    move-result-object v0

    .line 97
    invoke-virtual {v10}, Lcom/intsig/camscanner/guide/dropchannel/dialog/DropCnlTrialRuleDialog$Companion;->〇080()Ljava/lang/String;

    .line 98
    .line 99
    .line 100
    move-result-object v2

    .line 101
    invoke-virtual {v1, v0, v2}, Landroidx/fragment/app/DialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V

    .line 102
    .line 103
    .line 104
    return-void
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private static final 〇〇O80〇0o(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;Lcom/intsig/comm/purchase/entity/ProductResultItem;Z)V
    .locals 1

    .line 1
    const-string/jumbo p1, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    new-instance p1, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v0, "buy end: "

    .line 13
    .line 14
    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    invoke-virtual {p0, p1}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->logD(Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p0, p2}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇O0o〇〇o(Z)V

    .line 28
    .line 29
    .line 30
    return-void
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private final 〇〇o0〇8()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->o〇O8OO()Landroid/widget/CheckBox;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    new-instance v1, LO08〇oO8〇/〇o00〇〇Oo;

    .line 8
    .line 9
    invoke-direct {v1, p0}, LO08〇oO8〇/〇o00〇〇Oo;-><init>(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;)V

    .line 10
    .line 11
    .line 12
    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 13
    .line 14
    .line 15
    :cond_0
    return-void
.end method

.method private final 〇〇〇0()V
    .locals 5

    .line 1
    sget-object v0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/VipMonthPromotionManager;->〇080:Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/VipMonthPromotionManager;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/VipMonthPromotionManager;->〇O8o08O()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    new-instance v1, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v2, "marketing_cs_scan_gift_"

    .line 13
    .line 14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v0

    .line 24
    const-string v1, "cs_more"

    .line 25
    .line 26
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇0〇0()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 31
    .line 32
    .line 33
    move-result v1

    .line 34
    if-eqz v1, :cond_0

    .line 35
    .line 36
    sget-object v1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_MORE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    sget-object v1, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_LIST:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 40
    .line 41
    :goto_0
    new-instance v2, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 42
    .line 43
    invoke-direct {v2}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;-><init>()V

    .line 44
    .line 45
    .line 46
    sget-object v3, Lcom/intsig/camscanner/purchase/track/PurchasePageId;->CSPremiumPop:Lcom/intsig/camscanner/purchase/track/PurchasePageId;

    .line 47
    .line 48
    invoke-virtual {v2, v3}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->pageId(Lcom/intsig/camscanner/purchase/track/PurchasePageId;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 49
    .line 50
    .line 51
    move-result-object v2

    .line 52
    sget-object v3, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->FAKE_SCHEME:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 53
    .line 54
    invoke-virtual {v3, v0}, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->setValue(Ljava/lang/String;)Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 55
    .line 56
    .line 57
    move-result-object v0

    .line 58
    invoke-virtual {v2, v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme(Lcom/intsig/camscanner/purchase/track/PurchaseScheme;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 59
    .line 60
    .line 61
    move-result-object v0

    .line 62
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->o〇oo()I

    .line 67
    .line 68
    .line 69
    move-result v1

    .line 70
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 71
    .line 72
    .line 73
    move-result-object v1

    .line 74
    const-string v2, "pop_style"

    .line 75
    .line 76
    invoke-virtual {v0, v2, v1}, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->addExtraParams(Ljava/lang/String;Ljava/lang/String;)Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 77
    .line 78
    .line 79
    move-result-object v0

    .line 80
    const-string v1, "PurchaseTracker()\n      \u2026\", popStyle().toString())"

    .line 81
    .line 82
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 83
    .line 84
    .line 85
    iput-object v0, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->o〇00O:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 86
    .line 87
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    if-eqz v0, :cond_5

    .line 92
    .line 93
    new-instance v1, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 94
    .line 95
    iget-object v2, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->o〇00O:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 96
    .line 97
    const/4 v3, 0x0

    .line 98
    const-string v4, "mPurchaseTracker"

    .line 99
    .line 100
    if-nez v2, :cond_1

    .line 101
    .line 102
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 103
    .line 104
    .line 105
    move-object v2, v3

    .line 106
    :cond_1
    invoke-direct {v1, v0, v2}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;-><init>(Landroid/app/Activity;Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 107
    .line 108
    .line 109
    iput-object v1, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 110
    .line 111
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->o〇00O:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 112
    .line 113
    if-nez v0, :cond_2

    .line 114
    .line 115
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 116
    .line 117
    .line 118
    goto :goto_1

    .line 119
    :cond_2
    move-object v3, v0

    .line 120
    :goto_1
    invoke-virtual {v1, v3}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->OOO(Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 121
    .line 122
    .line 123
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 124
    .line 125
    if-nez v0, :cond_3

    .line 126
    .line 127
    goto :goto_2

    .line 128
    :cond_3
    const/4 v1, 0x1

    .line 129
    iput-boolean v1, v0, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->〇oo〇:Z

    .line 130
    .line 131
    :goto_2
    if-nez v0, :cond_4

    .line 132
    .line 133
    goto :goto_3

    .line 134
    :cond_4
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇8O0880()Z

    .line 135
    .line 136
    .line 137
    move-result v1

    .line 138
    iput-boolean v1, v0, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->o800o8O:Z

    .line 139
    .line 140
    :goto_3
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇08O〇00〇o:Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;

    .line 141
    .line 142
    if-eqz v0, :cond_5

    .line 143
    .line 144
    new-instance v1, LO08〇oO8〇/〇o〇;

    .line 145
    .line 146
    invoke-direct {v1, p0}, LO08〇oO8〇/〇o〇;-><init>(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;)V

    .line 147
    .line 148
    .line 149
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient;->〇O〇80o08O(Lcom/intsig/camscanner/purchase/utils/CSPurchaseClient$PurchaseCallback;)V

    .line 150
    .line 151
    .line 152
    :cond_5
    return-void
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private final 〇〇〇00()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->o0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$MonthlyPromotionPopCnPriceInfoStyle;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    iget v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$MonthlyPromotionPopCnPriceInfoStyle;->pay_way:I

    .line 6
    .line 7
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 v0, 0x0

    .line 13
    :goto_0
    const/4 v1, 0x0

    .line 14
    const/4 v2, 0x1

    .line 15
    if-nez v0, :cond_1

    .line 16
    .line 17
    goto :goto_1

    .line 18
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 19
    .line 20
    .line 21
    move-result v3

    .line 22
    if-nez v3, :cond_2

    .line 23
    .line 24
    iput v2, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->OO:I

    .line 25
    .line 26
    iput-boolean v1, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->O8o08O8O:Z

    .line 27
    .line 28
    goto :goto_4

    .line 29
    :cond_2
    :goto_1
    const/4 v3, 0x2

    .line 30
    if-nez v0, :cond_3

    .line 31
    .line 32
    goto :goto_2

    .line 33
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 34
    .line 35
    .line 36
    move-result v4

    .line 37
    if-ne v4, v2, :cond_4

    .line 38
    .line 39
    iput v3, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->OO:I

    .line 40
    .line 41
    iput-boolean v1, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->O8o08O8O:Z

    .line 42
    .line 43
    goto :goto_4

    .line 44
    :cond_4
    :goto_2
    if-nez v0, :cond_5

    .line 45
    .line 46
    goto :goto_3

    .line 47
    :cond_5
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 48
    .line 49
    .line 50
    move-result v0

    .line 51
    if-ne v0, v3, :cond_6

    .line 52
    .line 53
    iput v3, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->OO:I

    .line 54
    .line 55
    iput-boolean v2, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->O8o08O8O:Z

    .line 56
    .line 57
    goto :goto_4

    .line 58
    :cond_6
    :goto_3
    iput v2, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->OO:I

    .line 59
    .line 60
    iput-boolean v1, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->O8o08O8O:Z

    .line 61
    .line 62
    :goto_4
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->oOoO8OO〇()Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;

    .line 63
    .line 64
    .line 65
    move-result-object v0

    .line 66
    if-eqz v0, :cond_8

    .line 67
    .line 68
    iget-boolean v2, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->O8o08O8O:Z

    .line 69
    .line 70
    if-nez v2, :cond_7

    .line 71
    .line 72
    const/16 v1, 0x8

    .line 73
    .line 74
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 75
    .line 76
    .line 77
    return-void

    .line 78
    :cond_7
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 79
    .line 80
    .line 81
    iget v1, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->OO:I

    .line 82
    .line 83
    new-instance v2, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog$initPayType$1$1;

    .line 84
    .line 85
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog$initPayType$1$1;-><init>(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;)V

    .line 86
    .line 87
    .line 88
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;->O8(ILcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView$IPaymentChooseCallback;)V

    .line 89
    .line 90
    .line 91
    :cond_8
    return-void
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method


# virtual methods
.method protected O8〇8〇O80(Z)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->dismissAllowingStateLoss()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method protected final Ooo8o()I
    .locals 1

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->o〇0()Lcom/intsig/camscanner/purchase/utils/ProductManager;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/purchase/utils/ProductManager;->oO80()Lcom/intsig/comm/purchase/entity/QueryProductsResult;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    iget-object v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult;->monthly_promotion_pop_cn:Lcom/intsig/comm/purchase/entity/QueryProductsResult$MonthlyPromotionPopCn;

    .line 10
    .line 11
    if-eqz v0, :cond_0

    .line 12
    .line 13
    iget v0, v0, Lcom/intsig/comm/purchase/entity/QueryProductsResult$MonthlyPromotionPopCn;->scan_level:I

    .line 14
    .line 15
    goto :goto_0

    .line 16
    :cond_0
    const/4 v0, 0x1

    .line 17
    :goto_0
    return v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method protected final O〇8〇008(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->OO:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method protected dealClickAction(Landroid/view/View;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->dealClickAction(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    move-object p1, v0

    .line 17
    :goto_0
    if-nez p1, :cond_1

    .line 18
    .line 19
    goto :goto_1

    .line 20
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    const v2, 0x7f0a12be

    .line 25
    .line 26
    .line 27
    if-ne v1, v2, :cond_2

    .line 28
    .line 29
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->o〇0〇o()V

    .line 30
    .line 31
    .line 32
    goto :goto_3

    .line 33
    :cond_2
    :goto_1
    if-nez p1, :cond_3

    .line 34
    .line 35
    goto :goto_2

    .line 36
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    const v2, 0x7f0a087d

    .line 41
    .line 42
    .line 43
    if-ne v1, v2, :cond_4

    .line 44
    .line 45
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇088O()V

    .line 46
    .line 47
    .line 48
    goto :goto_3

    .line 49
    :cond_4
    :goto_2
    if-nez p1, :cond_5

    .line 50
    .line 51
    goto :goto_3

    .line 52
    :cond_5
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 53
    .line 54
    .line 55
    move-result p1

    .line 56
    const v1, 0x7f0a16ea

    .line 57
    .line 58
    .line 59
    if-ne p1, v1, :cond_6

    .line 60
    .line 61
    const-string p1, "instant_discounts"

    .line 62
    .line 63
    const/4 v1, 0x2

    .line 64
    invoke-static {p0, p1, v0, v1, v0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇8〇80o(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Object;)V

    .line 65
    .line 66
    .line 67
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->O08〇()V

    .line 68
    .line 69
    .line 70
    :cond_6
    :goto_3
    return-void
    .line 71
.end method

.method protected getNavigationBarColor()I
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/utils/ApplicationHelper;->o0:Lcom/intsig/utils/ApplicationHelper;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/utils/ApplicationHelper;->Oo08()Landroid/content/Context;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    const v1, 0x7f0602e0

    .line 8
    .line 9
    .line 10
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 11
    .line 12
    .line 13
    move-result v0

    .line 14
    return v0
    .line 15
.end method

.method public getTheme()I
    .locals 1

    .line 1
    const v0, 0x7f140193

    .line 2
    .line 3
    .line 4
    return v0
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method protected init(Landroid/os/Bundle;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇8〇OOoooo()Z

    .line 2
    .line 3
    .line 4
    move-result p1

    .line 5
    if-nez p1, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->dismissAllowingStateLoss()V

    .line 8
    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇〇〇0()V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇〇〇00()V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇0oO〇oo00()V

    .line 18
    .line 19
    .line 20
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇〇o0〇8()V

    .line 21
    .line 22
    .line 23
    sget-object p1, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/VipMonthPromotionManager;->〇080:Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/VipMonthPromotionManager;

    .line 24
    .line 25
    invoke-virtual {p1}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/VipMonthPromotionManager;->OO0o〇〇()V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method protected isDefaultExpanded()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method protected o88()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method protected abstract oOoO8OO〇()Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;
.end method

.method public onDestroyView()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/DialogFragment;->onDestroyView()V

    .line 2
    .line 3
    .line 4
    iget-boolean v0, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇080OO8〇0:Z

    .line 5
    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    const-string v0, "cs_list"

    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇0〇0()Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v1

    .line 14
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 15
    .line 16
    .line 17
    move-result v0

    .line 18
    if-eqz v0, :cond_0

    .line 19
    .line 20
    sget-object v0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/VipMonthPromotionManager;->〇080:Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/VipMonthPromotionManager;

    .line 21
    .line 22
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 23
    .line 24
    .line 25
    move-result-object v1

    .line 26
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/VipMonthPromotionManager;->〇o〇(Landroidx/fragment/app/FragmentActivity;)V

    .line 27
    .line 28
    .line 29
    :cond_0
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public onResume()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->o〇00O:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 5
    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    const-string v0, "mPurchaseTracker"

    .line 9
    .line 10
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    const/4 v0, 0x0

    .line 14
    :cond_0
    invoke-static {v0}, Lcom/intsig/camscanner/purchase/track/PurchaseTrackerUtil;->oO80(Lcom/intsig/camscanner/purchase/track/PurchaseTracker;)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .line 1
    const-string/jumbo v0, "view"

    .line 2
    .line 3
    .line 4
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    invoke-super {p0, p1, p2}, Lcom/intsig/app/BaseBottomSheetDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p0}, Landroidx/fragment/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    instance-of p2, p1, Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    .line 15
    .line 16
    if-eqz p2, :cond_0

    .line 17
    .line 18
    check-cast p1, Lcom/google/android/material/bottomsheet/BottomSheetDialog;

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_0
    const/4 p1, 0x0

    .line 22
    :goto_0
    if-nez p1, :cond_1

    .line 23
    .line 24
    return-void

    .line 25
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/material/bottomsheet/BottomSheetDialog;->getBehavior()Lcom/google/android/material/bottomsheet/BottomSheetBehavior;

    .line 26
    .line 27
    .line 28
    move-result-object p2

    .line 29
    const/4 v0, 0x1

    .line 30
    invoke-virtual {p2, v0}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->setSkipCollapsed(Z)V

    .line 31
    .line 32
    .line 33
    new-instance v0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog$onViewCreated$1$1;

    .line 34
    .line 35
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog$onViewCreated$1$1;-><init>(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {p2, v0}, Lcom/google/android/material/bottomsheet/BottomSheetBehavior;->addBottomSheetCallback(Lcom/google/android/material/bottomsheet/BottomSheetBehavior$BottomSheetCallback;)V

    .line 39
    .line 40
    .line 41
    const p2, 0x7f0a11fc

    .line 42
    .line 43
    .line 44
    invoke-virtual {p1, p2}, Landroidx/appcompat/app/AppCompatDialog;->findViewById(I)Landroid/view/View;

    .line 45
    .line 46
    .line 47
    move-result-object p1

    .line 48
    if-eqz p1, :cond_2

    .line 49
    .line 50
    new-instance p2, LO08〇oO8〇/〇080;

    .line 51
    .line 52
    invoke-direct {p2, p0}, LO08〇oO8〇/〇080;-><init>(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;)V

    .line 53
    .line 54
    .line 55
    invoke-virtual {p1, p2}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 56
    .line 57
    .line 58
    :cond_2
    return-void
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

.method protected abstract o〇O8OO()Landroid/widget/CheckBox;
.end method

.method public abstract o〇oo()I
.end method

.method protected 〇088O()V
    .locals 1

    .line 1
    const-string v0, "close"

    .line 2
    .line 3
    invoke-virtual {p0, v0, v0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇o〇88〇8(Ljava/lang/String;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public abstract 〇0oO〇oo00()V
.end method

.method protected final 〇0ooOOo()Lcom/intsig/comm/purchase/entity/QueryProductsResult$MonthlyPromotionPopCnPriceInfoStyle;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->o0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$MonthlyPromotionPopCnPriceInfoStyle;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method protected final 〇0〇0()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇0O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/lang/String;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method protected 〇8O0880()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method protected final 〇O0o〇〇o(Z)V
    .locals 1

    .line 1
    iput-boolean p1, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇080OO8〇0:Z

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->O8〇8〇O80(Z)V

    .line 4
    .line 5
    .line 6
    const-string p1, "close"

    .line 7
    .line 8
    const-string v0, "other"

    .line 9
    .line 10
    invoke-virtual {p0, p1, v0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->〇o〇88〇8(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method protected 〇Oo〇O()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public abstract 〇o08()V
.end method

.method protected final 〇o〇88〇8(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string/jumbo v0, "type"

    .line 2
    .line 3
    .line 4
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    const-string v1, "closeType"

    .line 8
    .line 9
    invoke-static {p2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    const/4 v1, 0x5

    .line 13
    new-array v1, v1, [Landroid/util/Pair;

    .line 14
    .line 15
    iget-object v2, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->o〇00O:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 16
    .line 17
    const/4 v3, 0x0

    .line 18
    const-string v4, "mPurchaseTracker"

    .line 19
    .line 20
    if-nez v2, :cond_0

    .line 21
    .line 22
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 23
    .line 24
    .line 25
    move-object v2, v3

    .line 26
    :cond_0
    iget-object v2, v2, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->scheme:Lcom/intsig/camscanner/purchase/track/PurchaseScheme;

    .line 27
    .line 28
    invoke-virtual {v2}, Lcom/intsig/camscanner/purchase/track/PurchaseScheme;->toTrackerValue()Ljava/lang/String;

    .line 29
    .line 30
    .line 31
    move-result-object v2

    .line 32
    const-string v5, "schema"

    .line 33
    .line 34
    invoke-static {v5, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    const/4 v5, 0x0

    .line 39
    aput-object v2, v1, v5

    .line 40
    .line 41
    iget-object v2, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->o〇00O:Lcom/intsig/camscanner/purchase/track/PurchaseTracker;

    .line 42
    .line 43
    if-nez v2, :cond_1

    .line 44
    .line 45
    invoke-static {v4}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 46
    .line 47
    .line 48
    goto :goto_0

    .line 49
    :cond_1
    move-object v3, v2

    .line 50
    :goto_0
    iget-object v2, v3, Lcom/intsig/camscanner/purchase/track/PurchaseTracker;->entrance:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 51
    .line 52
    invoke-virtual {v2}, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->toTrackerValue()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v2

    .line 56
    const-string v3, "from_part"

    .line 57
    .line 58
    invoke-static {v3, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    .line 59
    .line 60
    .line 61
    move-result-object v2

    .line 62
    const/4 v3, 0x1

    .line 63
    aput-object v2, v1, v3

    .line 64
    .line 65
    const/4 v2, 0x2

    .line 66
    invoke-static {v0, p1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    .line 67
    .line 68
    .line 69
    move-result-object p1

    .line 70
    aput-object p1, v1, v2

    .line 71
    .line 72
    invoke-virtual {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->o〇oo()I

    .line 73
    .line 74
    .line 75
    move-result p1

    .line 76
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    .line 77
    .line 78
    .line 79
    move-result-object p1

    .line 80
    const-string v0, "pop_style"

    .line 81
    .line 82
    invoke-static {v0, p1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    .line 83
    .line 84
    .line 85
    move-result-object p1

    .line 86
    const/4 v0, 0x3

    .line 87
    aput-object p1, v1, v0

    .line 88
    .line 89
    const-string p1, "close_type"

    .line 90
    .line 91
    invoke-static {p1, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    .line 92
    .line 93
    .line 94
    move-result-object p1

    .line 95
    const/4 p2, 0x4

    .line 96
    aput-object p1, v1, p2

    .line 97
    .line 98
    const-string p1, "CSPremiumPop"

    .line 99
    .line 100
    const-string p2, "click_function"

    .line 101
    .line 102
    invoke-static {p1, p2, v1}, Lcom/intsig/camscanner/log/LogAgentData;->〇〇888(Ljava/lang/String;Ljava/lang/String;[Landroid/util/Pair;)V

    .line 103
    .line 104
    .line 105
    return-void
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

.method protected final 〇〇〇O〇(Lcom/intsig/comm/purchase/entity/QueryProductsResult$MonthlyPromotionPopCnPriceInfoStyle;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/BaseVipMonthPromotionDialog;->o0:Lcom/intsig/comm/purchase/entity/QueryProductsResult$MonthlyPromotionPopCnPriceInfoStyle;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

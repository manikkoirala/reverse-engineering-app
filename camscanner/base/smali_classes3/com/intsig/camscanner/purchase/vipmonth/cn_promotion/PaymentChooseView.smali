.class public final Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;
.super Landroid/widget/LinearLayout;
.source "PaymentChooseView.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView$IPaymentChooseCallback;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private OO:Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView$IPaymentChooseCallback;

.field private final o0:Lcom/intsig/camscanner/databinding/LayoutPaymentChooseViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇OOo8〇0:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    const-string v0, "context"

    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 3
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p2, 0x1

    .line 4
    iput p2, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;->〇OOo8〇0:I

    .line 5
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    invoke-static {p1, p0}, Lcom/intsig/camscanner/databinding/LayoutPaymentChooseViewBinding;->〇080(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Lcom/intsig/camscanner/databinding/LayoutPaymentChooseViewBinding;

    move-result-object p1

    const-string p2, "inflate(LayoutInflater.from(context), this)"

    invoke-static {p1, p2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;->o0:Lcom/intsig/camscanner/databinding/LayoutPaymentChooseViewBinding;

    const/4 p1, 0x0

    .line 6
    invoke-virtual {p0, p1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;->Oo08()V

    return-void
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;IILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 0

    and-int/lit8 p5, p4, 0x2

    if-eqz p5, :cond_0

    const/4 p2, 0x0

    :cond_0
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_1

    const/4 p3, 0x0

    .line 2
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private final OO0o〇〇〇〇0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;->o0:Lcom/intsig/camscanner/databinding/LayoutPaymentChooseViewBinding;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutPaymentChooseViewBinding;->〇08O〇00〇o:Landroid/widget/RadioButton;

    .line 4
    .line 5
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;->oO80()Z

    .line 6
    .line 7
    .line 8
    move-result v1

    .line 9
    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 10
    .line 11
    .line 12
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;->o0:Lcom/intsig/camscanner/databinding/LayoutPaymentChooseViewBinding;

    .line 13
    .line 14
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutPaymentChooseViewBinding;->o〇00O:Landroid/widget/RadioButton;

    .line 15
    .line 16
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;->〇80〇808〇O()Z

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final Oo08()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;->o0:Lcom/intsig/camscanner/databinding/LayoutPaymentChooseViewBinding;

    .line 2
    .line 3
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutPaymentChooseViewBinding;->〇OOo8〇0:Landroidx/appcompat/widget/LinearLayoutCompat;

    .line 4
    .line 5
    new-instance v1, LO08〇oO8〇/O8;

    .line 6
    .line 7
    invoke-direct {v1, p0}, LO08〇oO8〇/O8;-><init>(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 11
    .line 12
    .line 13
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;->o0:Lcom/intsig/camscanner/databinding/LayoutPaymentChooseViewBinding;

    .line 14
    .line 15
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/LayoutPaymentChooseViewBinding;->OO:Landroidx/appcompat/widget/LinearLayoutCompat;

    .line 16
    .line 17
    new-instance v1, LO08〇oO8〇/Oo08;

    .line 18
    .line 19
    invoke-direct {v1, p0}, LO08〇oO8〇/Oo08;-><init>(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private static synthetic getMCurPayType$annotations()V
    .locals 0

    .line 1
    return-void
    .line 2
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private final oO80()Z
    .locals 2

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;->〇OOo8〇0:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    if-ne v1, v0, :cond_0

    .line 5
    .line 6
    goto :goto_0

    .line 7
    :cond_0
    const/4 v1, 0x0

    .line 8
    :goto_0
    return v1
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private static final o〇0(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string/jumbo p1, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    const/4 p1, 0x1

    .line 8
    iput p1, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;->〇OOo8〇0:I

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;->〇o〇()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static synthetic 〇080(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;->〇〇888(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final 〇80〇808〇O()Z
    .locals 2

    .line 1
    const/4 v0, 0x2

    .line 2
    iget v1, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;->〇OOo8〇0:I

    .line 3
    .line 4
    if-ne v0, v1, :cond_0

    .line 5
    .line 6
    const/4 v0, 0x1

    .line 7
    goto :goto_0

    .line 8
    :cond_0
    const/4 v0, 0x0

    .line 9
    :goto_0
    return v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public static synthetic 〇o00〇〇Oo(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;->o〇0(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final 〇o〇()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;->OO0o〇〇〇〇0()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;->OO:Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView$IPaymentChooseCallback;

    .line 5
    .line 6
    if-eqz v0, :cond_0

    .line 7
    .line 8
    iget v1, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;->〇OOo8〇0:I

    .line 9
    .line 10
    invoke-interface {v0, v1}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView$IPaymentChooseCallback;->〇080(I)V

    .line 11
    .line 12
    .line 13
    :cond_0
    return-void
    .line 14
    .line 15
.end method

.method private static final 〇〇888(Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string/jumbo p1, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    const/4 p1, 0x2

    .line 8
    iput p1, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;->〇OOo8〇0:I

    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;->〇o〇()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method


# virtual methods
.method public final O8(ILcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView$IPaymentChooseCallback;)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;->〇OOo8〇0:I

    .line 2
    .line 3
    iput-object p2, p0, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;->OO:Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView$IPaymentChooseCallback;

    .line 4
    .line 5
    invoke-direct {p0}, Lcom/intsig/camscanner/purchase/vipmonth/cn_promotion/PaymentChooseView;->OO0o〇〇〇〇0()V

    .line 6
    .line 7
    .line 8
    return-void
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

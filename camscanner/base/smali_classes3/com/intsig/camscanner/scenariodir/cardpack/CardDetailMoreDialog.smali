.class public final Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailMoreDialog;
.super Lcom/intsig/camscanner/view/CsBottomItemsDialog;
.source "CardDetailMoreDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailMoreDialog$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final o8oOOo:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailMoreDialog$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final 〇O〇〇O8:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final O0O:I

.field private final ooo0〇〇O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇8〇oO〇〇8o:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇〇08O:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailMoreDialog$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailMoreDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailMoreDialog;->o8oOOo:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailMoreDialog$Companion;

    .line 8
    .line 9
    const-class v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailMoreDialog;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "CardDetailMoreDialog::class.java.simpleName"

    .line 16
    .line 17
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    sput-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailMoreDialog;->〇O〇〇O8:Ljava/lang/String;

    .line 21
    .line 22
    return-void
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public constructor <init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Ljava/lang/String;ZI)V
    .locals 8
    .param p1    # Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "activity"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const-string/jumbo v0, "ti"

    .line 7
    .line 8
    .line 9
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    const/4 v3, 0x0

    .line 13
    const/4 v4, 0x0

    .line 14
    const/4 v5, 0x0

    .line 15
    const/16 v6, 0xe

    .line 16
    .line 17
    const/4 v7, 0x0

    .line 18
    move-object v1, p0

    .line 19
    move-object v2, p1

    .line 20
    invoke-direct/range {v1 .. v7}, Lcom/intsig/camscanner/view/CsBottomItemsDialog;-><init>(Landroid/content/Context;IIIILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 21
    .line 22
    .line 23
    iput-object p1, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailMoreDialog;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;

    .line 24
    .line 25
    iput-object p2, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailMoreDialog;->ooo0〇〇O:Ljava/lang/String;

    .line 26
    .line 27
    iput-boolean p3, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailMoreDialog;->〇〇08O:Z

    .line 28
    .line 29
    iput p4, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailMoreDialog;->O0O:I

    .line 30
    .line 31
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
.end method


# virtual methods
.method public OO0o〇〇(II)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/CsBottomItemsDialog;->dismiss()V

    .line 2
    .line 3
    .line 4
    iget-object p2, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailMoreDialog;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;

    .line 5
    .line 6
    iget-boolean v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailMoreDialog;->〇〇08O:Z

    .line 7
    .line 8
    invoke-virtual {p2, p1, v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O8oO0(IZ)V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public final getActivity()Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailMoreDialog;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇80〇808〇O()Ljava/lang/String;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailMoreDialog;->ooo0〇〇O:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇〇888()Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lcom/intsig/menu/MenuTypeItem;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    .line 2
    .line 3
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 4
    .line 5
    .line 6
    iget-object v1, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailMoreDialog;->〇8〇oO〇〇8o:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O08o()Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    invoke-virtual {v1}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;->Oooo8o0〇()Landroidx/lifecycle/MutableLiveData;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-virtual {v1}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    check-cast v1, Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;

    .line 21
    .line 22
    const/4 v2, 0x0

    .line 23
    if-eqz v1, :cond_0

    .line 24
    .line 25
    invoke-virtual {v1}, Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;->〇o00〇〇Oo()Z

    .line 26
    .line 27
    .line 28
    move-result v3

    .line 29
    goto :goto_0

    .line 30
    :cond_0
    const/4 v3, 0x0

    .line 31
    :goto_0
    const/4 v4, 0x1

    .line 32
    if-eqz v1, :cond_2

    .line 33
    .line 34
    invoke-virtual {v1}, Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;->〇80〇808〇O()Z

    .line 35
    .line 36
    .line 37
    move-result v5

    .line 38
    if-nez v5, :cond_1

    .line 39
    .line 40
    invoke-virtual {v1}, Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;->oO80()Z

    .line 41
    .line 42
    .line 43
    move-result v1

    .line 44
    if-eqz v1, :cond_2

    .line 45
    .line 46
    :cond_1
    const/4 v1, 0x1

    .line 47
    goto :goto_1

    .line 48
    :cond_2
    const/4 v1, 0x0

    .line 49
    :goto_1
    new-instance v5, Lcom/intsig/menu/MenuItem;

    .line 50
    .line 51
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/CsBottomItemsDialog;->Oo08()Landroid/content/Context;

    .line 52
    .line 53
    .line 54
    move-result-object v6

    .line 55
    const v7, 0x7f13033e

    .line 56
    .line 57
    .line 58
    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v6

    .line 62
    const v7, 0x7f080749

    .line 63
    .line 64
    .line 65
    invoke-direct {v5, v2, v6, v7}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 66
    .line 67
    .line 68
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    .line 70
    .line 71
    new-instance v5, Lcom/intsig/menu/MenuItem;

    .line 72
    .line 73
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/CsBottomItemsDialog;->Oo08()Landroid/content/Context;

    .line 74
    .line 75
    .line 76
    move-result-object v6

    .line 77
    const v7, 0x7f131c8a

    .line 78
    .line 79
    .line 80
    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 81
    .line 82
    .line 83
    move-result-object v6

    .line 84
    const v7, 0x7f0806b9

    .line 85
    .line 86
    .line 87
    invoke-direct {v5, v4, v6, v7}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 88
    .line 89
    .line 90
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    .line 92
    .line 93
    new-instance v4, Lcom/intsig/menu/MenuItem;

    .line 94
    .line 95
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/CsBottomItemsDialog;->Oo08()Landroid/content/Context;

    .line 96
    .line 97
    .line 98
    move-result-object v5

    .line 99
    const v6, 0x7f131214

    .line 100
    .line 101
    .line 102
    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 103
    .line 104
    .line 105
    move-result-object v5

    .line 106
    const v6, 0x7f080757

    .line 107
    .line 108
    .line 109
    const/4 v7, 0x2

    .line 110
    invoke-direct {v4, v7, v5, v6}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 111
    .line 112
    .line 113
    if-eqz v1, :cond_3

    .line 114
    .line 115
    invoke-virtual {v4, v2}, Lcom/intsig/menu/MenuItem;->〇〇808〇(Z)V

    .line 116
    .line 117
    .line 118
    goto :goto_2

    .line 119
    :cond_3
    iget-boolean v2, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailMoreDialog;->〇〇08O:Z

    .line 120
    .line 121
    invoke-virtual {v4, v2}, Lcom/intsig/menu/MenuItem;->〇〇808〇(Z)V

    .line 122
    .line 123
    .line 124
    :goto_2
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    .line 126
    .line 127
    sget-object v2, Lcom/intsig/camscanner/scenariodir/util/CertificateUtil;->〇080:Lcom/intsig/camscanner/scenariodir/util/CertificateUtil;

    .line 128
    .line 129
    iget v4, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailMoreDialog;->O0O:I

    .line 130
    .line 131
    invoke-virtual {v2, v4}, Lcom/intsig/camscanner/scenariodir/util/CertificateUtil;->Oooo8o0〇(I)Z

    .line 132
    .line 133
    .line 134
    move-result v2

    .line 135
    if-nez v2, :cond_4

    .line 136
    .line 137
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/folder/PreferenceFolderHelper;->o〇0()Z

    .line 138
    .line 139
    .line 140
    move-result v2

    .line 141
    if-nez v2, :cond_4

    .line 142
    .line 143
    if-nez v1, :cond_4

    .line 144
    .line 145
    new-instance v2, Lcom/intsig/menu/MenuItem;

    .line 146
    .line 147
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/CsBottomItemsDialog;->Oo08()Landroid/content/Context;

    .line 148
    .line 149
    .line 150
    move-result-object v4

    .line 151
    const v5, 0x7f13122d

    .line 152
    .line 153
    .line 154
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 155
    .line 156
    .line 157
    move-result-object v4

    .line 158
    const v5, 0x7f080bd4

    .line 159
    .line 160
    .line 161
    const/4 v6, 0x3

    .line 162
    invoke-direct {v2, v6, v4, v5}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;I)V

    .line 163
    .line 164
    .line 165
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    .line 167
    .line 168
    :cond_4
    if-nez v1, :cond_5

    .line 169
    .line 170
    if-nez v3, :cond_5

    .line 171
    .line 172
    new-instance v1, Lcom/intsig/menu/MenuItem;

    .line 173
    .line 174
    const/4 v8, 0x4

    .line 175
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/CsBottomItemsDialog;->Oo08()Landroid/content/Context;

    .line 176
    .line 177
    .line 178
    move-result-object v2

    .line 179
    const v3, 0x7f131213

    .line 180
    .line 181
    .line 182
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 183
    .line 184
    .line 185
    move-result-object v9

    .line 186
    const v10, 0x7f0806f2

    .line 187
    .line 188
    .line 189
    const/4 v11, 0x0

    .line 190
    const/4 v12, -0x1

    .line 191
    invoke-virtual {p0}, Lcom/intsig/camscanner/view/CsBottomItemsDialog;->Oo08()Landroid/content/Context;

    .line 192
    .line 193
    .line 194
    move-result-object v2

    .line 195
    const v3, 0x7f0601f2

    .line 196
    .line 197
    .line 198
    invoke-static {v2, v3}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 199
    .line 200
    .line 201
    move-result v13

    .line 202
    move-object v7, v1

    .line 203
    invoke-direct/range {v7 .. v13}, Lcom/intsig/menu/MenuItem;-><init>(ILjava/lang/String;IZII)V

    .line 204
    .line 205
    .line 206
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207
    .line 208
    .line 209
    :cond_5
    return-object v0
    .line 210
    .line 211
.end method

.class public final Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;
.super Lcom/intsig/mvp/activity/BaseChangeActivity;
.source "CardDetailActivity.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$CardDetailMoveCopyEvent;,
        Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field static final synthetic oO〇8O8oOo:[Lkotlin/reflect/KProperty;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lkotlin/reflect/KProperty<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final 〇0O〇O00O:Ljava/lang/String;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field public static final 〇〇〇0o〇〇0:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O0O:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

.field private O88O:Lcom/intsig/camscanner/datastruct/DocItem;

.field private final Oo0〇Ooo:Landroidx/activity/result/ActivityResultLauncher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/activity/result/ActivityResultLauncher<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final Oo80:Landroidx/activity/result/ActivityResultLauncher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/activity/result/ActivityResultLauncher<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private Ooo08:Ljava/lang/String;

.field private final O〇08oOOO0:Landroidx/activity/result/ActivityResultLauncher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/activity/result/ActivityResultLauncher<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private O〇o88o08〇:Ljava/lang/String;

.field private final o8o:Landroid/view/View$OnClickListener;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private o8oOOo:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

.field private final o8〇OO:Landroidx/activity/result/ActivityResultLauncher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/activity/result/ActivityResultLauncher<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private oOO〇〇:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailAdapter;

.field private final oo8ooo8O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private ooO:Ljava/lang/String;

.field private final ooo0〇〇O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final o〇oO:Landroidx/activity/result/ActivityResultLauncher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/activity/result/ActivityResultLauncher<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇00O0:Landroidx/activity/result/ActivityResultLauncher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/activity/result/ActivityResultLauncher<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇08〇o0O:Landroidx/activity/result/ActivityResultLauncher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/activity/result/ActivityResultLauncher<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇OO8ooO8〇:Ljava/lang/String;

.field private 〇OO〇00〇0O:Ljava/lang/String;

.field private final 〇O〇〇O8:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private 〇o0O:Z

.field private final 〇〇08O:Lkotlin/Lazy;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final 〇〇o〇:Landroidx/activity/result/ActivityResultLauncher;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/activity/result/ActivityResultLauncher<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 1
    const/4 v0, 0x1

    .line 2
    new-array v0, v0, [Lkotlin/reflect/KProperty;

    .line 3
    .line 4
    new-instance v1, Lkotlin/jvm/internal/PropertyReference1Impl;

    .line 5
    .line 6
    const-string v2, "_mViewBinding"

    .line 7
    .line 8
    const-string v3, "get_mViewBinding()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;"

    .line 9
    .line 10
    const-class v4, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;

    .line 11
    .line 12
    const/4 v5, 0x0

    .line 13
    invoke-direct {v1, v4, v2, v3, v5}, Lkotlin/jvm/internal/PropertyReference1Impl;-><init>(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;I)V

    .line 14
    .line 15
    .line 16
    invoke-static {v1}, Lkotlin/jvm/internal/Reflection;->oO80(Lkotlin/jvm/internal/PropertyReference1;)Lkotlin/reflect/KProperty1;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    aput-object v1, v0, v5

    .line 21
    .line 22
    sput-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oO〇8O8oOo:[Lkotlin/reflect/KProperty;

    .line 23
    .line 24
    new-instance v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$Companion;

    .line 25
    .line 26
    const/4 v1, 0x0

    .line 27
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 28
    .line 29
    .line 30
    sput-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇〇〇0o〇〇0:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$Companion;

    .line 31
    .line 32
    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    const-string v1, "CardDetailActivity::class.java.simpleName"

    .line 37
    .line 38
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    .line 40
    .line 41
    sput-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 42
    .line 43
    return-void
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public constructor <init>()V
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/intsig/mvp/activity/BaseChangeActivity;-><init>()V

    .line 2
    .line 3
    .line 4
    new-instance v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$special$$inlined$viewModels$default$1;

    .line 5
    .line 6
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$special$$inlined$viewModels$default$1;-><init>(Landroidx/activity/ComponentActivity;)V

    .line 7
    .line 8
    .line 9
    new-instance v1, Landroidx/lifecycle/ViewModelLazy;

    .line 10
    .line 11
    const-class v2, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailViewModel;

    .line 12
    .line 13
    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->〇o00〇〇Oo(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    .line 14
    .line 15
    .line 16
    move-result-object v2

    .line 17
    new-instance v3, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$special$$inlined$viewModels$default$2;

    .line 18
    .line 19
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$special$$inlined$viewModels$default$2;-><init>(Landroidx/activity/ComponentActivity;)V

    .line 20
    .line 21
    .line 22
    new-instance v4, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$special$$inlined$viewModels$default$3;

    .line 23
    .line 24
    const/4 v5, 0x0

    .line 25
    invoke-direct {v4, v5, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$special$$inlined$viewModels$default$3;-><init>(Lkotlin/jvm/functions/Function0;Landroidx/activity/ComponentActivity;)V

    .line 26
    .line 27
    .line 28
    invoke-direct {v1, v2, v3, v0, v4}, Landroidx/lifecycle/ViewModelLazy;-><init>(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    .line 29
    .line 30
    .line 31
    iput-object v1, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->ooo0〇〇O:Lkotlin/Lazy;

    .line 32
    .line 33
    new-instance v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$special$$inlined$viewModels$default$4;

    .line 34
    .line 35
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$special$$inlined$viewModels$default$4;-><init>(Landroidx/activity/ComponentActivity;)V

    .line 36
    .line 37
    .line 38
    new-instance v1, Landroidx/lifecycle/ViewModelLazy;

    .line 39
    .line 40
    const-class v2, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 41
    .line 42
    invoke-static {v2}, Lkotlin/jvm/internal/Reflection;->〇o00〇〇Oo(Ljava/lang/Class;)Lkotlin/reflect/KClass;

    .line 43
    .line 44
    .line 45
    move-result-object v2

    .line 46
    new-instance v3, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$special$$inlined$viewModels$default$5;

    .line 47
    .line 48
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$special$$inlined$viewModels$default$5;-><init>(Landroidx/activity/ComponentActivity;)V

    .line 49
    .line 50
    .line 51
    new-instance v4, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$special$$inlined$viewModels$default$6;

    .line 52
    .line 53
    invoke-direct {v4, v5, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$special$$inlined$viewModels$default$6;-><init>(Lkotlin/jvm/functions/Function0;Landroidx/activity/ComponentActivity;)V

    .line 54
    .line 55
    .line 56
    invoke-direct {v1, v2, v3, v0, v4}, Landroidx/lifecycle/ViewModelLazy;-><init>(Lkotlin/reflect/KClass;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function0;)V

    .line 57
    .line 58
    .line 59
    iput-object v1, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇〇08O:Lkotlin/Lazy;

    .line 60
    .line 61
    new-instance v0, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 62
    .line 63
    const-class v1, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 64
    .line 65
    invoke-direct {v0, v1, p0}, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;-><init>(Ljava/lang/Class;Landroid/app/Activity;)V

    .line 66
    .line 67
    .line 68
    iput-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇O〇〇O8:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 69
    .line 70
    new-instance v0, LO〇O800oo/〇00;

    .line 71
    .line 72
    invoke-direct {v0, p0}, LO〇O800oo/〇00;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 73
    .line 74
    .line 75
    iput-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o8o:Landroid/view/View$OnClickListener;

    .line 76
    .line 77
    new-instance v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$certParamList$2;

    .line 78
    .line 79
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$certParamList$2;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 80
    .line 81
    .line 82
    invoke-static {v0}, Lkotlin/LazyKt;->〇o00〇〇Oo(Lkotlin/jvm/functions/Function0;)Lkotlin/Lazy;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    iput-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oo8ooo8O:Lkotlin/Lazy;

    .line 87
    .line 88
    new-instance v0, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;

    .line 89
    .line 90
    invoke-direct {v0}, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;-><init>()V

    .line 91
    .line 92
    .line 93
    new-instance v1, LO〇O800oo/O〇8O8〇008;

    .line 94
    .line 95
    invoke-direct {v1, p0}, LO〇O800oo/O〇8O8〇008;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 96
    .line 97
    .line 98
    invoke-virtual {p0, v0, v1}, Landroidx/activity/ComponentActivity;->registerForActivityResult(Landroidx/activity/result/contract/ActivityResultContract;Landroidx/activity/result/ActivityResultCallback;)Landroidx/activity/result/ActivityResultLauncher;

    .line 99
    .line 100
    .line 101
    move-result-object v0

    .line 102
    const-string v1, "registerForActivityResul\u2026activityResult)\n        }"

    .line 103
    .line 104
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 105
    .line 106
    .line 107
    iput-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o〇oO:Landroidx/activity/result/ActivityResultLauncher;

    .line 108
    .line 109
    new-instance v0, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;

    .line 110
    .line 111
    invoke-direct {v0}, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;-><init>()V

    .line 112
    .line 113
    .line 114
    new-instance v2, LO〇O800oo/O8ooOoo〇;

    .line 115
    .line 116
    invoke-direct {v2}, LO〇O800oo/O8ooOoo〇;-><init>()V

    .line 117
    .line 118
    .line 119
    invoke-virtual {p0, v0, v2}, Landroidx/activity/ComponentActivity;->registerForActivityResult(Landroidx/activity/result/contract/ActivityResultContract;Landroidx/activity/result/ActivityResultCallback;)Landroidx/activity/result/ActivityResultLauncher;

    .line 120
    .line 121
    .line 122
    move-result-object v0

    .line 123
    const-string v2, "registerForActivityResul\u2026result == $it\")\n        }"

    .line 124
    .line 125
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 126
    .line 127
    .line 128
    iput-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇08〇o0O:Landroidx/activity/result/ActivityResultLauncher;

    .line 129
    .line 130
    new-instance v0, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;

    .line 131
    .line 132
    invoke-direct {v0}, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;-><init>()V

    .line 133
    .line 134
    .line 135
    new-instance v2, LO〇O800oo/〇oOO8O8;

    .line 136
    .line 137
    invoke-direct {v2, p0}, LO〇O800oo/〇oOO8O8;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 138
    .line 139
    .line 140
    invoke-virtual {p0, v0, v2}, Landroidx/activity/ComponentActivity;->registerForActivityResult(Landroidx/activity/result/contract/ActivityResultContract;Landroidx/activity/result/ActivityResultCallback;)Landroidx/activity/result/ActivityResultLauncher;

    .line 141
    .line 142
    .line 143
    move-result-object v0

    .line 144
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    .line 146
    .line 147
    iput-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇〇o〇:Landroidx/activity/result/ActivityResultLauncher;

    .line 148
    .line 149
    new-instance v0, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;

    .line 150
    .line 151
    invoke-direct {v0}, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;-><init>()V

    .line 152
    .line 153
    .line 154
    new-instance v2, LO〇O800oo/〇〇888;

    .line 155
    .line 156
    invoke-direct {v2, p0}, LO〇O800oo/〇〇888;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 157
    .line 158
    .line 159
    invoke-virtual {p0, v0, v2}, Landroidx/activity/ComponentActivity;->registerForActivityResult(Landroidx/activity/result/contract/ActivityResultContract;Landroidx/activity/result/ActivityResultCallback;)Landroidx/activity/result/ActivityResultLauncher;

    .line 160
    .line 161
    .line 162
    move-result-object v0

    .line 163
    const-string v2, "registerForActivityResul\u2026eImportResult()\n        }"

    .line 164
    .line 165
    invoke-static {v0, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 166
    .line 167
    .line 168
    iput-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->Oo80:Landroidx/activity/result/ActivityResultLauncher;

    .line 169
    .line 170
    new-instance v0, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;

    .line 171
    .line 172
    invoke-direct {v0}, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;-><init>()V

    .line 173
    .line 174
    .line 175
    new-instance v2, LO〇O800oo/oO80;

    .line 176
    .line 177
    invoke-direct {v2, p0}, LO〇O800oo/oO80;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 178
    .line 179
    .line 180
    invoke-virtual {p0, v0, v2}, Landroidx/activity/ComponentActivity;->registerForActivityResult(Landroidx/activity/result/contract/ActivityResultContract;Landroidx/activity/result/ActivityResultCallback;)Landroidx/activity/result/ActivityResultLauncher;

    .line 181
    .line 182
    .line 183
    move-result-object v0

    .line 184
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 185
    .line 186
    .line 187
    iput-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇00O0:Landroidx/activity/result/ActivityResultLauncher;

    .line 188
    .line 189
    new-instance v0, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;

    .line 190
    .line 191
    invoke-direct {v0}, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;-><init>()V

    .line 192
    .line 193
    .line 194
    new-instance v1, LO〇O800oo/〇80〇808〇O;

    .line 195
    .line 196
    invoke-direct {v1, p0}, LO〇O800oo/〇80〇808〇O;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 197
    .line 198
    .line 199
    invoke-virtual {p0, v0, v1}, Landroidx/activity/ComponentActivity;->registerForActivityResult(Landroidx/activity/result/contract/ActivityResultContract;Landroidx/activity/result/ActivityResultCallback;)Landroidx/activity/result/ActivityResultLauncher;

    .line 200
    .line 201
    .line 202
    move-result-object v0

    .line 203
    const-string v1, "registerForActivityResul\u2026tyResult, true)\n        }"

    .line 204
    .line 205
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 206
    .line 207
    .line 208
    iput-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O〇08oOOO0:Landroidx/activity/result/ActivityResultLauncher;

    .line 209
    .line 210
    new-instance v0, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;

    .line 211
    .line 212
    invoke-direct {v0}, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;-><init>()V

    .line 213
    .line 214
    .line 215
    new-instance v1, LO〇O800oo/OO0o〇〇〇〇0;

    .line 216
    .line 217
    invoke-direct {v1, p0}, LO〇O800oo/OO0o〇〇〇〇0;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 218
    .line 219
    .line 220
    invoke-virtual {p0, v0, v1}, Landroidx/activity/ComponentActivity;->registerForActivityResult(Landroidx/activity/result/contract/ActivityResultContract;Landroidx/activity/result/ActivityResultCallback;)Landroidx/activity/result/ActivityResultLauncher;

    .line 221
    .line 222
    .line 223
    move-result-object v0

    .line 224
    const-string v1, "registerForActivityResul\u2026hotoFilePath())\n        }"

    .line 225
    .line 226
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 227
    .line 228
    .line 229
    iput-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o8〇OO:Landroidx/activity/result/ActivityResultLauncher;

    .line 230
    .line 231
    new-instance v0, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;

    .line 232
    .line 233
    invoke-direct {v0}, Landroidx/activity/result/contract/ActivityResultContracts$StartActivityForResult;-><init>()V

    .line 234
    .line 235
    .line 236
    new-instance v1, LO〇O800oo/〇8o8o〇;

    .line 237
    .line 238
    invoke-direct {v1, p0}, LO〇O800oo/〇8o8o〇;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 239
    .line 240
    .line 241
    invoke-virtual {p0, v0, v1}, Landroidx/activity/ComponentActivity;->registerForActivityResult(Landroidx/activity/result/contract/ActivityResultContract;Landroidx/activity/result/ActivityResultCallback;)Landroidx/activity/result/ActivityResultLauncher;

    .line 242
    .line 243
    .line 244
    move-result-object v0

    .line 245
    const-string v1, "registerForActivityResul\u2026        }\n        }\n    }"

    .line 246
    .line 247
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 248
    .line 249
    .line 250
    iput-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->Oo0〇Ooo:Landroidx/activity/result/ActivityResultLauncher;

    .line 251
    .line 252
    return-void
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
.end method

.method private static final O008o8oo(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Ljava/util/ArrayList;)V
    .locals 2

    .line 1
    const-string/jumbo v0, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    const-string v0, "$pageIdList"

    .line 8
    .line 9
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 13
    .line 14
    const-string v1, "continue batch reedit"

    .line 15
    .line 16
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->Oo0O〇8800()Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->OOO8o〇〇(Ljava/util/Collection;)[J

    .line 24
    .line 25
    .line 26
    move-result-object p1

    .line 27
    iput-object p1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o8〇OO0〇0o:[J

    .line 28
    .line 29
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 30
    .line 31
    const/4 v1, 0x1

    .line 32
    invoke-static {p1, v1, v0}, Lcom/intsig/camscanner/batch/BatchImageReeditActivity;->o808o8o08(Landroid/content/Context;ZLcom/intsig/camscanner/datastruct/ParcelDocInfo;)Landroid/content/Intent;

    .line 33
    .line 34
    .line 35
    move-result-object p1

    .line 36
    iget-object p0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇08〇o0O:Landroidx/activity/result/ActivityResultLauncher;

    .line 37
    .line 38
    invoke-virtual {p0, p1}, Landroidx/activity/result/ActivityResultLauncher;->launch(Ljava/lang/Object;)V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final O008oO0(Ljava/util/List;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lcom/intsig/camscanner/scenariodir/data/DetailValue;",
            ">;)Z"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇00〇〇〇o〇8()Ljava/util/ArrayList;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    const/4 v1, 0x0

    .line 10
    const/4 v2, 0x0

    .line 11
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 12
    .line 13
    .line 14
    move-result v3

    .line 15
    if-eqz v3, :cond_5

    .line 16
    .line 17
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object v3

    .line 21
    check-cast v3, Lcom/intsig/camscanner/scenariodir/data/DetailValue;

    .line 22
    .line 23
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    .line 24
    .line 25
    .line 26
    move-result-object v4

    .line 27
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    .line 28
    .line 29
    .line 30
    move-result v5

    .line 31
    if-eqz v5, :cond_0

    .line 32
    .line 33
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object v5

    .line 37
    check-cast v5, Lcom/intsig/camscanner/scenariodir/data/DetailValue;

    .line 38
    .line 39
    invoke-virtual {v5}, Lcom/intsig/camscanner/scenariodir/data/DetailValue;->getKey()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v6

    .line 43
    invoke-virtual {v3}, Lcom/intsig/camscanner/scenariodir/data/DetailValue;->getKey()Ljava/lang/String;

    .line 44
    .line 45
    .line 46
    move-result-object v7

    .line 47
    invoke-static {v6, v7}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 48
    .line 49
    .line 50
    move-result v6

    .line 51
    if-eqz v6, :cond_1

    .line 52
    .line 53
    invoke-virtual {v5}, Lcom/intsig/camscanner/scenariodir/data/DetailValue;->getValue()Ljava/lang/String;

    .line 54
    .line 55
    .line 56
    move-result-object v4

    .line 57
    const/4 v5, 0x1

    .line 58
    if-eqz v4, :cond_3

    .line 59
    .line 60
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    .line 61
    .line 62
    .line 63
    move-result v6

    .line 64
    if-nez v6, :cond_2

    .line 65
    .line 66
    goto :goto_1

    .line 67
    :cond_2
    const/4 v6, 0x0

    .line 68
    goto :goto_2

    .line 69
    :cond_3
    :goto_1
    const/4 v6, 0x1

    .line 70
    :goto_2
    if-eqz v6, :cond_4

    .line 71
    .line 72
    const-string v4, ""

    .line 73
    .line 74
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/scenariodir/data/DetailValue;->setValue(Ljava/lang/String;)V

    .line 75
    .line 76
    .line 77
    goto :goto_0

    .line 78
    :cond_4
    invoke-virtual {v3, v4}, Lcom/intsig/camscanner/scenariodir/data/DetailValue;->setValue(Ljava/lang/String;)V

    .line 79
    .line 80
    .line 81
    const/4 v2, 0x1

    .line 82
    goto :goto_0

    .line 83
    :cond_5
    return v2
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method public static synthetic O00OoO〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oO0o(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final varargs O00〇o00(Ljava/lang/String;[Landroid/net/Uri;)Z
    .locals 7

    .line 1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    const/4 v1, 0x0

    .line 6
    if-nez v0, :cond_3

    .line 7
    .line 8
    array-length v0, p2

    .line 9
    const/4 v2, 0x1

    .line 10
    if-nez v0, :cond_0

    .line 11
    .line 12
    const/4 v0, 0x1

    .line 13
    goto :goto_0

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 15
    :goto_0
    if-eqz v0, :cond_1

    .line 16
    .line 17
    goto :goto_2

    .line 18
    :cond_1
    array-length v0, p2

    .line 19
    const/4 v3, 0x0

    .line 20
    :goto_1
    if-ge v3, v0, :cond_3

    .line 21
    .line 22
    aget-object v4, p2, v3

    .line 23
    .line 24
    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v4

    .line 28
    const-string/jumbo v5, "uri.toString()"

    .line 29
    .line 30
    .line 31
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 32
    .line 33
    .line 34
    const/4 v5, 0x2

    .line 35
    const/4 v6, 0x0

    .line 36
    invoke-static {p1, v4, v1, v5, v6}, Lkotlin/text/StringsKt;->Oo8Oo00oo(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZILjava/lang/Object;)Z

    .line 37
    .line 38
    .line 39
    move-result v4

    .line 40
    if-eqz v4, :cond_2

    .line 41
    .line 42
    return v2

    .line 43
    :cond_2
    add-int/lit8 v3, v3, 0x1

    .line 44
    .line 45
    goto :goto_1

    .line 46
    :cond_3
    :goto_2
    return v1
    .line 47
    .line 48
    .line 49
.end method

.method public static final varargs synthetic O088O(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Ljava/lang/String;[Landroid/net/Uri;)Z
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O00〇o00(Ljava/lang/String;[Landroid/net/Uri;)Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public static final synthetic O08〇oO8〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇8o00(Ljava/util/ArrayList;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static synthetic O0Oo〇8(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p2, p2, 0x1

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    :cond_0
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o08〇808(Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
.end method

.method public static final synthetic O0o0(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)Lcom/intsig/camscanner/capture/certificates/model/CertificateMoreCapture;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0〇o8〇()Lcom/intsig/camscanner/capture/certificates/model/CertificateMoreCapture;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final O0o0〇8o()V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    new-instance v1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$showCaptureAppendOrNewDialog$1;

    .line 4
    .line 5
    invoke-direct {v1, p0, v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$showCaptureAppendOrNewDialog$1;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Lcom/intsig/mvp/activity/BaseChangeActivity;)V

    .line 6
    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/intsig/camscanner/view/CsBottomItemsDialog;->show()V

    .line 9
    .line 10
    .line 11
    return-void
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private final O0oO(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    new-instance v1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$getDocPageIdList$2;

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-direct {v1, p0, v2}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$getDocPageIdList$2;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Lkotlin/coroutines/Continuation;)V

    .line 9
    .line 10
    .line 11
    invoke-static {v0, v1, p1}, Lkotlinx/coroutines/BuildersKt;->Oo08(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    return-object p1
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static synthetic O0〇(Landroidx/activity/result/ActivityResult;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o088〇〇(Landroidx/activity/result/ActivityResult;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final O0〇8〇()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O88O:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "mDocItem"

    .line 6
    .line 7
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->o8()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    return v0
.end method

.method private static final O0〇O80ooo(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V
    .locals 1

    .line 1
    const-string/jumbo v0, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇O〇〇〇()V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final O0〇o8o〇〇(Ljava/lang/String;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string/jumbo v2, "tempPhoto path = "

    .line 9
    .line 10
    .line 11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    iput-object p1, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O〇o88o08〇:Ljava/lang/String;

    .line 25
    .line 26
    return-void
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private final O80(I)I
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "clickCertificateCamera cardType:"

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    sget-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->ID_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    const/4 v1, 0x0

    .line 30
    const/4 v2, 0x1

    .line 31
    if-ne p1, v0, :cond_0

    .line 32
    .line 33
    :goto_0
    const/4 v0, 0x1

    .line 34
    goto :goto_1

    .line 35
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_ID_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 36
    .line 37
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 38
    .line 39
    .line 40
    move-result v0

    .line 41
    if-ne p1, v0, :cond_1

    .line 42
    .line 43
    goto :goto_0

    .line 44
    :cond_1
    const/4 v0, 0x0

    .line 45
    :goto_1
    if-eqz v0, :cond_2

    .line 46
    .line 47
    const/16 p1, 0x3e9

    .line 48
    .line 49
    goto/16 :goto_6

    .line 50
    .line 51
    :cond_2
    sget-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FAMILY_REGISTER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 52
    .line 53
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 54
    .line 55
    .line 56
    move-result v0

    .line 57
    if-ne p1, v0, :cond_3

    .line 58
    .line 59
    const/16 p1, 0x3eb

    .line 60
    .line 61
    goto/16 :goto_6

    .line 62
    .line 63
    :cond_3
    sget-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->PASSPORT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 64
    .line 65
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 66
    .line 67
    .line 68
    move-result v0

    .line 69
    if-ne p1, v0, :cond_4

    .line 70
    .line 71
    :goto_2
    const/4 v0, 0x1

    .line 72
    goto :goto_3

    .line 73
    :cond_4
    sget-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_PASSPORT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 74
    .line 75
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 76
    .line 77
    .line 78
    move-result v0

    .line 79
    if-ne p1, v0, :cond_5

    .line 80
    .line 81
    goto :goto_2

    .line 82
    :cond_5
    const/4 v0, 0x0

    .line 83
    :goto_3
    if-eqz v0, :cond_6

    .line 84
    .line 85
    const/16 p1, 0x3ea

    .line 86
    .line 87
    goto :goto_6

    .line 88
    :cond_6
    sget-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->DRIVE_LICENSE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 89
    .line 90
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 91
    .line 92
    .line 93
    move-result v0

    .line 94
    if-ne p1, v0, :cond_7

    .line 95
    .line 96
    const/16 p1, 0x3ec

    .line 97
    .line 98
    goto :goto_6

    .line 99
    :cond_7
    sget-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->VEHICLE_LICENSE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 100
    .line 101
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 102
    .line 103
    .line 104
    move-result v0

    .line 105
    if-ne p1, v0, :cond_8

    .line 106
    .line 107
    const/16 p1, 0x3ed

    .line 108
    .line 109
    goto :goto_6

    .line 110
    :cond_8
    sget-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->BANK_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 111
    .line 112
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 113
    .line 114
    .line 115
    move-result v0

    .line 116
    if-ne p1, v0, :cond_9

    .line 117
    .line 118
    :goto_4
    const/4 v1, 0x1

    .line 119
    goto :goto_5

    .line 120
    :cond_9
    sget-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_BANK_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 121
    .line 122
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 123
    .line 124
    .line 125
    move-result v0

    .line 126
    if-ne p1, v0, :cond_a

    .line 127
    .line 128
    goto :goto_4

    .line 129
    :cond_a
    :goto_5
    if-eqz v1, :cond_b

    .line 130
    .line 131
    const/16 p1, 0x3f1

    .line 132
    .line 133
    goto :goto_6

    .line 134
    :cond_b
    sget-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->BUSINESS_LICENSE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 135
    .line 136
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 137
    .line 138
    .line 139
    move-result v0

    .line 140
    if-ne p1, v0, :cond_c

    .line 141
    .line 142
    const/16 p1, 0x3ef

    .line 143
    .line 144
    goto :goto_6

    .line 145
    :cond_c
    sget-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->HOUSE_PROPERTY:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 146
    .line 147
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 148
    .line 149
    .line 150
    move-result v0

    .line 151
    if-ne p1, v0, :cond_d

    .line 152
    .line 153
    const/16 p1, 0x3ee

    .line 154
    .line 155
    goto :goto_6

    .line 156
    :cond_d
    const/16 p1, 0x3f3

    .line 157
    .line 158
    :goto_6
    return p1
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method public static final synthetic O80OO(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o8o0(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final O80〇()V
    .locals 6

    .line 1
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 6
    .line 7
    .line 8
    move-result-object v1

    .line 9
    const/4 v2, 0x0

    .line 10
    new-instance v3, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$onMultiPageImportResult$1;

    .line 11
    .line 12
    const/4 v4, 0x0

    .line 13
    invoke-direct {v3, p0, v4}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$onMultiPageImportResult$1;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Lkotlin/coroutines/Continuation;)V

    .line 14
    .line 15
    .line 16
    const/4 v4, 0x2

    .line 17
    const/4 v5, 0x0

    .line 18
    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final O80〇〇o()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o00o0O〇〇o()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O8〇()Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailViewModel;

    .line 5
    .line 6
    .line 7
    move-result-object v0

    .line 8
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailViewModel;->〇O00()Landroidx/lifecycle/MutableLiveData;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    new-instance v1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$subscribeUi$1;

    .line 13
    .line 14
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$subscribeUi$1;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 15
    .line 16
    .line 17
    new-instance v2, LO〇O800oo/〇O8o08O;

    .line 18
    .line 19
    invoke-direct {v2, v1}, LO〇O800oo/〇O8o08O;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 20
    .line 21
    .line 22
    invoke-virtual {v0, p0, v2}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 23
    .line 24
    .line 25
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O8〇()Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailViewModel;

    .line 26
    .line 27
    .line 28
    move-result-object v0

    .line 29
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailViewModel;->oo88o8O()Landroidx/lifecycle/MutableLiveData;

    .line 30
    .line 31
    .line 32
    move-result-object v0

    .line 33
    new-instance v1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$subscribeUi$2;

    .line 34
    .line 35
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$subscribeUi$2;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 36
    .line 37
    .line 38
    new-instance v2, LO〇O800oo/OO0o〇〇;

    .line 39
    .line 40
    invoke-direct {v2, v1}, LO〇O800oo/OO0o〇〇;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 41
    .line 42
    .line 43
    invoke-virtual {v0, p0, v2}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 44
    .line 45
    .line 46
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 47
    .line 48
    .line 49
    move-result-object v0

    .line 50
    new-instance v1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$subscribeUi$3;

    .line 51
    .line 52
    const/4 v2, 0x0

    .line 53
    invoke-direct {v1, p0, v2}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$subscribeUi$3;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Lkotlin/coroutines/Continuation;)V

    .line 54
    .line 55
    .line 56
    invoke-virtual {v0, v1}, Landroidx/lifecycle/LifecycleCoroutineScope;->launchWhenStarted(Lkotlin/jvm/functions/Function2;)Lkotlinx/coroutines/Job;

    .line 57
    .line 58
    .line 59
    return-void
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public static synthetic O88(Lkotlin/jvm/functions/Function0;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->ooooo0O(Lkotlin/jvm/functions/Function0;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static synthetic O880O〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Lcom/chad/library/adapter/base/BaseQuickAdapter;Landroid/view/View;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2, p3}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O8o(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Lcom/chad/library/adapter/base/BaseQuickAdapter;Landroid/view/View;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
.end method

.method private final O888Oo(Lkotlin/jvm/functions/Function0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O08o()Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;->Oooo8o0〇()Landroidx/lifecycle/MutableLiveData;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;

    .line 14
    .line 15
    new-instance v1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$checkGo2Camera$1;

    .line 16
    .line 17
    invoke-direct {v1, p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$checkGo2Camera$1;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Lkotlin/jvm/functions/Function0;)V

    .line 18
    .line 19
    .line 20
    invoke-static {v0, v1}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker;->〇o00〇〇Oo(Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;Lkotlin/jvm/functions/Function0;)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public static synthetic O8O(Lkotlin/jvm/functions/Function0;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8ooOO(Lkotlin/jvm/functions/Function0;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private static final O8o(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Lcom/chad/library/adapter/base/BaseQuickAdapter;Landroid/view/View;I)V
    .locals 6

    .line 1
    const-string/jumbo v0, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    const-string v0, "adapter"

    .line 8
    .line 9
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    const-string v0, "<anonymous parameter 1>"

    .line 13
    .line 14
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    invoke-virtual {p1, p3}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->getItem(I)Ljava/lang/Object;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    instance-of p2, p1, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 22
    .line 23
    if-eqz p2, :cond_0

    .line 24
    .line 25
    check-cast p1, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 26
    .line 27
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageImageItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/pagelist/model/PageItem;

    .line 28
    .line 29
    .line 30
    move-result-object p2

    .line 31
    iget-wide v2, p2, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇080:J

    .line 32
    .line 33
    invoke-virtual {p1}, Lcom/intsig/camscanner/pagelist/model/PageImageItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/pagelist/model/PageItem;

    .line 34
    .line 35
    .line 36
    move-result-object p1

    .line 37
    iget-wide v4, p1, Lcom/intsig/camscanner/pagelist/model/PageItem;->OoO8:J

    .line 38
    .line 39
    move-object v0, p0

    .line 40
    move v1, p3

    .line 41
    invoke-direct/range {v0 .. v5}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8Oo8〇8(IJJ)V

    .line 42
    .line 43
    .line 44
    :cond_0
    return-void
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
.end method

.method private static final O8o0〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroidx/activity/result/ActivityResult;)V
    .locals 3

    .line 1
    const-string/jumbo v0, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    const-string v0, "activityResult"

    .line 8
    .line 9
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    const/4 v0, 0x2

    .line 13
    const/4 v1, 0x0

    .line 14
    const/4 v2, 0x0

    .line 15
    invoke-static {p0, p1, v2, v0, v1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o〇Oo(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroidx/activity/result/ActivityResult;ZILjava/lang/Object;)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final O8〇()Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailViewModel;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->ooo0〇〇O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailViewModel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public static final synthetic O8〇o0〇〇8(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;JLkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇〇0(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private final OO00〇0o〇〇()V
    .locals 5

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->O8o08O8O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 6
    .line 7
    new-instance v1, Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 8
    .line 9
    invoke-direct {v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v2, "#DBE2F2"

    .line 13
    .line 14
    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 15
    .line 16
    .line 17
    move-result v2

    .line 18
    invoke-virtual {v1, v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇00(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    const-string v2, "#F0F5FE"

    .line 23
    .line 24
    invoke-static {v2}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    .line 25
    .line 26
    .line 27
    move-result v2

    .line 28
    invoke-virtual {v1, v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->〇oo〇(I)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 29
    .line 30
    .line 31
    move-result-object v1

    .line 32
    sget-object v2, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    .line 33
    .line 34
    invoke-virtual {v1, v2}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->o〇O8〇〇o(Landroid/graphics/drawable/GradientDrawable$Orientation;)Lcom/intsig/utils/GradientDrawableBuilder$Builder;

    .line 35
    .line 36
    .line 37
    move-result-object v1

    .line 38
    invoke-virtual {v1}, Lcom/intsig/utils/GradientDrawableBuilder$Builder;->OoO8()Landroid/graphics/drawable/GradientDrawable;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 43
    .line 44
    .line 45
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 46
    .line 47
    const/16 v1, 0x1d

    .line 48
    .line 49
    if-lt v0, v1, :cond_0

    .line 50
    .line 51
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 52
    .line 53
    .line 54
    move-result-object v1

    .line 55
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->O8o08O8O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 56
    .line 57
    const/4 v2, 0x0

    .line 58
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setForceDarkAllowed(Z)V

    .line 59
    .line 60
    .line 61
    :cond_0
    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    .line 62
    .line 63
    .line 64
    move-result-object v1

    .line 65
    invoke-static {v1}, Lcom/intsig/camscanner/util/DarkModeUtils;->〇080(Landroid/content/Context;)Z

    .line 66
    .line 67
    .line 68
    move-result v1

    .line 69
    const/16 v2, 0x17

    .line 70
    .line 71
    if-eqz v1, :cond_1

    .line 72
    .line 73
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 74
    .line 75
    .line 76
    move-result-object v1

    .line 77
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->O8o08O8O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 78
    .line 79
    const/4 v3, 0x0

    .line 80
    invoke-virtual {v1, v3}, Landroid/view/View;->setElevation(F)V

    .line 81
    .line 82
    .line 83
    if-lt v0, v2, :cond_2

    .line 84
    .line 85
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 86
    .line 87
    .line 88
    move-result-object v0

    .line 89
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->o8o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 90
    .line 91
    const/4 v1, -0x1

    .line 92
    invoke-static {v1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    .line 93
    .line 94
    .line 95
    move-result-object v1

    .line 96
    invoke-static {v0, v1}, Landroidx/core/widget/TextViewCompat;->setCompoundDrawableTintList(Landroid/widget/TextView;Landroid/content/res/ColorStateList;)V

    .line 97
    .line 98
    .line 99
    goto :goto_0

    .line 100
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 101
    .line 102
    .line 103
    move-result-object v1

    .line 104
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->O8o08O8O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 105
    .line 106
    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    .line 107
    .line 108
    .line 109
    move-result-object v3

    .line 110
    const/high16 v4, 0x40a00000    # 5.0f

    .line 111
    .line 112
    invoke-static {v3, v4}, Lcom/intsig/utils/DisplayUtil;->〇o00〇〇Oo(Landroid/content/Context;F)F

    .line 113
    .line 114
    .line 115
    move-result v3

    .line 116
    invoke-virtual {v1, v3}, Landroid/view/View;->setElevation(F)V

    .line 117
    .line 118
    .line 119
    if-lt v0, v2, :cond_2

    .line 120
    .line 121
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 122
    .line 123
    .line 124
    move-result-object v0

    .line 125
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->o8o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 126
    .line 127
    const/4 v1, 0x0

    .line 128
    invoke-static {v0, v1}, Landroidx/core/widget/TextViewCompat;->setCompoundDrawableTintList(Landroid/widget/TextView;Landroid/content/res/ColorStateList;)V

    .line 129
    .line 130
    .line 131
    :cond_2
    :goto_0
    return-void
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public static synthetic OO0O(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroidx/activity/result/ActivityResult;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O8o0〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroidx/activity/result/ActivityResult;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static final synthetic OO0o(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O〇8(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final OO0o88()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 2
    .line 3
    const-string/jumbo v1, "showManualFillInGuide"

    .line 4
    .line 5
    .line 6
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    new-instance v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$showManualFillInGuide$1;

    .line 10
    .line 11
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$showManualFillInGuide$1;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0〇8o〇(Lkotlin/jvm/functions/Function1;)V

    .line 15
    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->Oo0〇Ooo:Landroid/widget/TextView;

    .line 22
    .line 23
    const v1, 0x7f13127f

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 31
    .line 32
    .line 33
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->〇〇〇0o〇〇0:Landroid/widget/TextView;

    .line 38
    .line 39
    const v1, 0x7f131297

    .line 40
    .line 41
    .line 42
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final OO0〇O()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O88O:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "mDocItem"

    .line 6
    .line 7
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->OO0o〇〇〇〇0()Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    if-nez v0, :cond_1

    .line 16
    .line 17
    const/4 v0, 0x0

    .line 18
    goto :goto_0

    .line 19
    :cond_1
    const/4 v0, 0x1

    .line 20
    :goto_0
    return v0
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static final synthetic OO8〇O8(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)I
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇o〇OoO8()I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final OOO0o〇(Landroidx/activity/result/ActivityResult;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Landroidx/activity/result/ActivityResult;->getData()Landroid/content/Intent;

    .line 2
    .line 3
    .line 4
    move-result-object p1

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    goto :goto_0

    .line 12
    :cond_0
    const/4 p1, 0x0

    .line 13
    :goto_0
    if-nez p1, :cond_1

    .line 14
    .line 15
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 16
    .line 17
    const-string v0, "onImagePageViewActivityResult data == null"

    .line 18
    .line 19
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    goto :goto_1

    .line 23
    :cond_1
    const-string v0, "finish activity"

    .line 24
    .line 25
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    .line 26
    .line 27
    .line 28
    move-result v0

    .line 29
    if-eqz v0, :cond_2

    .line 30
    .line 31
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 32
    .line 33
    const-string v0, "onImagePageViewActivityResult no doc, finish"

    .line 34
    .line 35
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o88O8()V

    .line 39
    .line 40
    .line 41
    return-void

    .line 42
    :cond_2
    const-string v0, "firstpage"

    .line 43
    .line 44
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    .line 45
    .line 46
    .line 47
    move-result p1

    .line 48
    if-eqz p1, :cond_3

    .line 49
    .line 50
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 51
    .line 52
    const-string v0, "is firstPage == firstpage"

    .line 53
    .line 54
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    :cond_3
    :goto_1
    return-void
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private final OOOo〇(Landroidx/activity/result/ActivityResult;)V
    .locals 19

    .line 1
    invoke-virtual/range {p1 .. p1}, Landroidx/activity/result/ActivityResult;->getData()Landroid/content/Intent;

    .line 2
    .line 3
    .line 4
    move-result-object v2

    .line 5
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 6
    .line 7
    new-instance v1, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v3, "data "

    .line 13
    .line 14
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object v1

    .line 24
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    if-nez v2, :cond_0

    .line 28
    .line 29
    return-void

    .line 30
    :cond_0
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v1

    .line 34
    const-string v3, "com.intsig.camscanner.NEW_PAGE_MULTIPLE"

    .line 35
    .line 36
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 37
    .line 38
    .line 39
    move-result v1

    .line 40
    if-eqz v1, :cond_1

    .line 41
    .line 42
    const-string v1, "do nothing"

    .line 43
    .line 44
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 49
    .line 50
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oo0O()J

    .line 51
    .line 52
    .line 53
    move-result-wide v3

    .line 54
    invoke-static {v0, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 55
    .line 56
    .line 57
    move-result-object v3

    .line 58
    const-string/jumbo v0, "withAppendedId(Documents\u2026ment.CONTENT_URI, mDocId)"

    .line 59
    .line 60
    .line 61
    invoke-static {v3, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    .line 63
    .line 64
    const-string v0, "image_sync_id"

    .line 65
    .line 66
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 67
    .line 68
    .line 69
    move-result-object v4

    .line 70
    const-string v0, "issaveready"

    .line 71
    .line 72
    const/4 v1, 0x1

    .line 73
    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 74
    .line 75
    .line 76
    move-result v5

    .line 77
    const-string v0, "extra_ocr_result"

    .line 78
    .line 79
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 80
    .line 81
    .line 82
    move-result-object v6

    .line 83
    const-string v0, "extra_ocr_user_result"

    .line 84
    .line 85
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 86
    .line 87
    .line 88
    move-result-object v7

    .line 89
    const-string v0, "extra_ocr_file"

    .line 90
    .line 91
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 92
    .line 93
    .line 94
    move-result-object v8

    .line 95
    const-string v0, "extra_ocr_paragraph"

    .line 96
    .line 97
    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 98
    .line 99
    .line 100
    move-result-object v9

    .line 101
    const-string v0, "extra_ocr_time"

    .line 102
    .line 103
    const-wide/16 v10, 0x0

    .line 104
    .line 105
    invoke-virtual {v2, v0, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 106
    .line 107
    .line 108
    move-result-wide v10

    .line 109
    const-string v0, "extra_ocr_mode"

    .line 110
    .line 111
    const/4 v1, 0x0

    .line 112
    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 113
    .line 114
    .line 115
    move-result v12

    .line 116
    invoke-static/range {p0 .. p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 117
    .line 118
    .line 119
    move-result-object v14

    .line 120
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 121
    .line 122
    .line 123
    move-result-object v15

    .line 124
    const/16 v16, 0x0

    .line 125
    .line 126
    new-instance v17, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$onImportPageResult$1;

    .line 127
    .line 128
    const/4 v13, 0x0

    .line 129
    move-object/from16 v0, v17

    .line 130
    .line 131
    move-object/from16 v1, p0

    .line 132
    .line 133
    invoke-direct/range {v0 .. v13}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$onImportPageResult$1;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroid/content/Intent;Landroid/net/Uri;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JILkotlin/coroutines/Continuation;)V

    .line 134
    .line 135
    .line 136
    const/4 v0, 0x2

    .line 137
    const/16 v18, 0x0

    .line 138
    .line 139
    move-object v13, v14

    .line 140
    move-object v14, v15

    .line 141
    move-object/from16 v15, v16

    .line 142
    .line 143
    move-object/from16 v16, v17

    .line 144
    .line 145
    move/from16 v17, v0

    .line 146
    .line 147
    invoke-static/range {v13 .. v18}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 148
    .line 149
    .line 150
    :goto_0
    invoke-static {}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->OoO8()Lcom/intsig/camscanner/background_batch/client/BackScanClient;

    .line 151
    .line 152
    .line 153
    move-result-object v0

    .line 154
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oo0O()J

    .line 155
    .line 156
    .line 157
    move-result-wide v1

    .line 158
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/background_batch/client/BackScanClient;->O〇O〇oO(J)Lcom/intsig/camscanner/background_batch/client/BackScanClient;

    .line 159
    .line 160
    .line 161
    invoke-direct/range {p0 .. p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇〇80o〇o0()V

    .line 162
    .line 163
    .line 164
    return-void
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method static synthetic OOO〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;ZZILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p3, p3, 0x2

    .line 2
    .line 3
    if-eqz p3, :cond_0

    .line 4
    .line 5
    const/4 p2, 0x0

    .line 6
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oO8o〇o〇8(ZZ)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
.end method

.method public static final synthetic OOo00(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static final synthetic OO〇000(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Lcom/intsig/camscanner/datastruct/DocItem;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O88O:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final OO〇80oO〇()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O88O:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "mDocItem"

    .line 6
    .line 7
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->O000()Z

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    return v0
.end method

.method public static synthetic OO〇〇o0oO(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇OoOO〇(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final Oo0O〇8800()Lcom/intsig/camscanner/datastruct/ParcelDocInfo;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oo0O()J

    .line 7
    .line 8
    .line 9
    move-result-wide v1

    .line 10
    iput-wide v1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 11
    .line 12
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇o〇OoO8()I

    .line 13
    .line 14
    .line 15
    move-result v1

    .line 16
    iput v1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->oOo0:I

    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o〇oO08〇o0()Ljava/lang/String;

    .line 19
    .line 20
    .line 21
    move-result-object v1

    .line 22
    iput-object v1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO:Ljava/lang/String;

    .line 23
    .line 24
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->OO〇80oO〇()Z

    .line 25
    .line 26
    .line 27
    move-result v1

    .line 28
    iput-boolean v1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇08O〇00〇o:Z

    .line 29
    .line 30
    return-object v0
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private static final OoOO〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroidx/activity/result/ActivityResult;)V
    .locals 1

    .line 1
    const-string/jumbo v0, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    const-string v0, "activityResult"

    .line 8
    .line 9
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oo〇O0o〇()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o00oooo(Landroidx/activity/result/ActivityResult;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    return-void
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static synthetic OoO〇OOo8o(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O〇〇O(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static synthetic OooO〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0o(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private static final OooO〇080(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroidx/activity/result/ActivityResult;)V
    .locals 1

    .line 1
    const-string/jumbo v0, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    const-string v0, "activityResult"

    .line 8
    .line 9
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    const/4 v0, 0x1

    .line 13
    invoke-direct {p0, p1, v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇88(Landroidx/activity/result/ActivityResult;Z)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private static final Oo〇〇〇〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V
    .locals 3

    .line 1
    const-string/jumbo v0, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O8〇()Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailViewModel;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oo0O()J

    .line 12
    .line 13
    .line 14
    move-result-wide v1

    .line 15
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailViewModel;->oo〇(J)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static final synthetic O〇00O(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)Lcom/intsig/camscanner/datastruct/ParcelDocInfo;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->Oo0O〇8800()Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static synthetic O〇080〇o0(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O008o8oo(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Ljava/util/ArrayList;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private static final O〇0888o(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroidx/activity/result/ActivityResult;)V
    .locals 4

    .line 1
    const-string/jumbo v0, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    invoke-virtual {p1}, Landroidx/activity/result/ActivityResult;->getResultCode()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    const/4 v1, -0x1

    .line 12
    if-ne v0, v1, :cond_8

    .line 13
    .line 14
    invoke-virtual {p1}, Landroidx/activity/result/ActivityResult;->getData()Landroid/content/Intent;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    const/4 v1, 0x0

    .line 19
    if-eqz v0, :cond_0

    .line 20
    .line 21
    const-string v2, "key_card_detail_certificate_info"

    .line 22
    .line 23
    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    check-cast v0, Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;

    .line 28
    .line 29
    goto :goto_0

    .line 30
    :cond_0
    move-object v0, v1

    .line 31
    :goto_0
    invoke-virtual {p1}, Landroidx/activity/result/ActivityResult;->getData()Landroid/content/Intent;

    .line 32
    .line 33
    .line 34
    move-result-object p1

    .line 35
    const/4 v2, 0x0

    .line 36
    if-eqz p1, :cond_1

    .line 37
    .line 38
    const-string v3, "key_card_detail_new_doc_type"

    .line 39
    .line 40
    invoke-virtual {p1, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    .line 41
    .line 42
    .line 43
    move-result v2

    .line 44
    :cond_1
    const/4 p1, 0x1

    .line 45
    if-eqz v0, :cond_2

    .line 46
    .line 47
    invoke-direct {p0, v0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8O(Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;Z)V

    .line 48
    .line 49
    .line 50
    :cond_2
    if-eqz v2, :cond_4

    .line 51
    .line 52
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O88O:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 53
    .line 54
    if-nez v0, :cond_3

    .line 55
    .line 56
    const-string v0, "mDocItem"

    .line 57
    .line 58
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    goto :goto_1

    .line 62
    :cond_3
    move-object v1, v0

    .line 63
    :goto_1
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/datastruct/DocItem;->O0O8OO088(I)V

    .line 64
    .line 65
    .line 66
    :cond_4
    if-nez v2, :cond_7

    .line 67
    .line 68
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇o〇OoO8()I

    .line 69
    .line 70
    .line 71
    move-result v0

    .line 72
    sget-object v1, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_OTHER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 73
    .line 74
    invoke-virtual {v1}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 75
    .line 76
    .line 77
    move-result v1

    .line 78
    if-ne v0, v1, :cond_5

    .line 79
    .line 80
    goto :goto_2

    .line 81
    :cond_5
    if-nez v2, :cond_6

    .line 82
    .line 83
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇o〇OoO8()I

    .line 84
    .line 85
    .line 86
    move-result v0

    .line 87
    sget-object v1, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_ID_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 88
    .line 89
    invoke-virtual {v1}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 90
    .line 91
    .line 92
    move-result v1

    .line 93
    if-eq v0, v1, :cond_6

    .line 94
    .line 95
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇o〇OoO8()I

    .line 96
    .line 97
    .line 98
    move-result v0

    .line 99
    sget-object v1, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_PASSPORT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 100
    .line 101
    invoke-virtual {v1}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 102
    .line 103
    .line 104
    move-result v1

    .line 105
    if-eq v0, v1, :cond_6

    .line 106
    .line 107
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇o〇OoO8()I

    .line 108
    .line 109
    .line 110
    move-result v0

    .line 111
    sget-object v1, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_BANK_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 112
    .line 113
    invoke-virtual {v1}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 114
    .line 115
    .line 116
    move-result v1

    .line 117
    if-ne v0, v1, :cond_8

    .line 118
    .line 119
    :cond_6
    invoke-static {p1}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇80o(Z)V

    .line 120
    .line 121
    .line 122
    invoke-direct {p0, p1, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oO8o〇o〇8(ZZ)V

    .line 123
    .line 124
    .line 125
    goto :goto_3

    .line 126
    :cond_7
    :goto_2
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o008()V

    .line 127
    .line 128
    .line 129
    :cond_8
    :goto_3
    return-void
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

.method public static synthetic O〇0O〇Oo〇o(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroidx/activity/result/ActivityResult;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->OooO〇080(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroidx/activity/result/ActivityResult;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static final synthetic O〇0o8o8〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;I)Lcom/intsig/camscanner/datastruct/ParcelDocInfo;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o〇o8〇〇O(I)Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static final synthetic O〇0o8〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)Landroidx/activity/result/ActivityResultLauncher;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o〇oO:Landroidx/activity/result/ActivityResultLauncher;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final O〇8(Z)V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/utils/PreferenceUtil;->oO80()Lcom/intsig/utils/PreferenceUtil;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "KEY_USE_SYS_CAMERA"

    .line 6
    .line 7
    const/4 v2, 0x0

    .line 8
    invoke-virtual {v0, v1, v2}, Lcom/intsig/utils/PreferenceUtil;->Oo08(Ljava/lang/String;Z)Z

    .line 9
    .line 10
    .line 11
    move-result v0

    .line 12
    if-eqz v0, :cond_0

    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oo〇O0o〇()Ljava/lang/String;

    .line 15
    .line 16
    .line 17
    move-result-object p1

    .line 18
    :try_start_0
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 19
    .line 20
    invoke-static {v0, p1}, Lcom/intsig/camscanner/app/IntentUtil;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o8〇OO:Landroidx/activity/result/ActivityResultLauncher;

    .line 25
    .line 26
    invoke-virtual {v0, p1}, Landroidx/activity/result/ActivityResultLauncher;->launch(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 27
    .line 28
    .line 29
    goto :goto_0

    .line 30
    :catch_0
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 31
    .line 32
    const-string/jumbo v0, "start system camera failed"

    .line 33
    .line 34
    .line 35
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    .line 37
    .line 38
    goto :goto_0

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 40
    .line 41
    const v1, 0x7f01004a

    .line 42
    .line 43
    .line 44
    const v2, 0x7f010049

    .line 45
    .line 46
    .line 47
    invoke-static {v0, v1, v2}, Landroidx/core/app/ActivityOptionsCompat;->makeCustomAnimation(Landroid/content/Context;II)Landroidx/core/app/ActivityOptionsCompat;

    .line 48
    .line 49
    .line 50
    move-result-object v0

    .line 51
    const-string v1, "makeCustomAnimation(mAct\u2026anim.slide_from_left_out)"

    .line 52
    .line 53
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 54
    .line 55
    .line 56
    if-eqz p1, :cond_1

    .line 57
    .line 58
    iget-object p1, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O〇08oOOO0:Landroidx/activity/result/ActivityResultLauncher;

    .line 59
    .line 60
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oooO8〇00()Landroid/content/Intent;

    .line 61
    .line 62
    .line 63
    move-result-object v1

    .line 64
    invoke-virtual {p1, v1, v0}, Landroidx/activity/result/ActivityResultLauncher;->launch(Ljava/lang/Object;Landroidx/core/app/ActivityOptionsCompat;)V

    .line 65
    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_1
    iget-object p1, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇00O0:Landroidx/activity/result/ActivityResultLauncher;

    .line 69
    .line 70
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O〇O800oo()Landroid/content/Intent;

    .line 71
    .line 72
    .line 73
    move-result-object v1

    .line 74
    invoke-virtual {p1, v1, v0}, Landroidx/activity/result/ActivityResultLauncher;->launch(Ljava/lang/Object;Landroidx/core/app/ActivityOptionsCompat;)V

    .line 75
    .line 76
    .line 77
    :goto_0
    return-void
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method private final O〇88()V
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O88O:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    const-string v2, "mDocItem"

    .line 5
    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    move-object v0, v1

    .line 12
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->Oo8Oo00oo()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    if-nez v0, :cond_1

    .line 17
    .line 18
    const-string v0, "cs_main"

    .line 19
    .line 20
    goto :goto_0

    .line 21
    :cond_1
    const-string v0, "cs_directory"

    .line 22
    .line 23
    :goto_0
    move-object v8, v0

    .line 24
    sget-object v0, Lcom/intsig/camscanner/data/dao/FolderDao;->〇080:Lcom/intsig/camscanner/data/dao/FolderDao;

    .line 25
    .line 26
    iget-object v3, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O88O:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 27
    .line 28
    if-nez v3, :cond_2

    .line 29
    .line 30
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 31
    .line 32
    .line 33
    move-object v3, v1

    .line 34
    :cond_2
    invoke-virtual {v3}, Lcom/intsig/camscanner/datastruct/DocItem;->Oo8Oo00oo()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v3

    .line 38
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/data/dao/FolderDao;->〇o〇(Ljava/lang/String;)Lcom/intsig/camscanner/datastruct/FolderItem;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    invoke-virtual {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O08o()Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 43
    .line 44
    .line 45
    move-result-object v3

    .line 46
    invoke-virtual {v3}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;->Oooo8o0〇()Landroidx/lifecycle/MutableLiveData;

    .line 47
    .line 48
    .line 49
    move-result-object v3

    .line 50
    invoke-virtual {v3}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 51
    .line 52
    .line 53
    move-result-object v3

    .line 54
    check-cast v3, Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;

    .line 55
    .line 56
    if-eqz v3, :cond_4

    .line 57
    .line 58
    if-nez v0, :cond_3

    .line 59
    .line 60
    goto :goto_1

    .line 61
    :cond_3
    iget v3, v3, Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;->〇o〇:I

    .line 62
    .line 63
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/datastruct/FolderItem;->o0O0(I)V

    .line 64
    .line 65
    .line 66
    :cond_4
    :goto_1
    sget-object v3, Lcom/intsig/camscanner/scenariodir/DocManualOperations;->〇080:Lcom/intsig/camscanner/scenariodir/DocManualOperations;

    .line 67
    .line 68
    iget-object v4, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O88O:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 69
    .line 70
    if-nez v4, :cond_5

    .line 71
    .line 72
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 73
    .line 74
    .line 75
    move-object v4, v1

    .line 76
    :cond_5
    invoke-static {v4}, Lkotlin/collections/SetsKt;->〇o〇(Ljava/lang/Object;)Ljava/util/Set;

    .line 77
    .line 78
    .line 79
    move-result-object v4

    .line 80
    invoke-virtual {v3, v0, v4}, Lcom/intsig/camscanner/scenariodir/DocManualOperations;->〇80〇808〇O(Lcom/intsig/camscanner/datastruct/FolderItem;Ljava/util/Set;)Z

    .line 81
    .line 82
    .line 83
    move-result v7

    .line 84
    iget-object v4, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 85
    .line 86
    const-string v0, "mActivity"

    .line 87
    .line 88
    invoke-static {v4, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 89
    .line 90
    .line 91
    const/4 v0, 0x1

    .line 92
    new-array v5, v0, [Lcom/intsig/camscanner/datastruct/DocItem;

    .line 93
    .line 94
    iget-object v6, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O88O:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 95
    .line 96
    if-nez v6, :cond_6

    .line 97
    .line 98
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 99
    .line 100
    .line 101
    goto :goto_2

    .line 102
    :cond_6
    move-object v1, v6

    .line 103
    :goto_2
    const/4 v2, 0x0

    .line 104
    aput-object v1, v5, v2

    .line 105
    .line 106
    invoke-static {v5}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 107
    .line 108
    .line 109
    move-result-object v5

    .line 110
    new-array v0, v0, [Ljava/lang/Long;

    .line 111
    .line 112
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oo0O()J

    .line 113
    .line 114
    .line 115
    move-result-wide v9

    .line 116
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 117
    .line 118
    .line 119
    move-result-object v1

    .line 120
    aput-object v1, v0, v2

    .line 121
    .line 122
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 123
    .line 124
    .line 125
    move-result-object v6

    .line 126
    invoke-virtual/range {v3 .. v8}, Lcom/intsig/camscanner/scenariodir/DocManualOperations;->〇oo〇(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;Ljava/util/ArrayList;ZLjava/lang/String;)V

    .line 127
    .line 128
    .line 129
    return-void
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private final O〇8O0O80〇(IZ)V
    .locals 12

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "append new doc, docType == "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    sget-object v3, Lcom/intsig/camscanner/capture/CaptureMode;->CERTIFICATE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 24
    .line 25
    sget-object v4, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->FROM_CARD_BAG:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 26
    .line 27
    sget-object v5, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_CERTIFICATION:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 28
    .line 29
    const/4 v6, 0x0

    .line 30
    const/16 v8, 0x3e8

    .line 31
    .line 32
    const/16 v10, 0x8

    .line 33
    .line 34
    const/4 v11, 0x0

    .line 35
    move-object v2, p0

    .line 36
    move v7, p1

    .line 37
    move v9, p2

    .line 38
    invoke-static/range {v2 .. v11}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇〇O(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ZIIZILjava/lang/Object;)V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final O〇O800oo()Landroid/content/Intent;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O0〇8〇()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oo0O()J

    .line 8
    .line 9
    .line 10
    move-result-wide v2

    .line 11
    invoke-static {v0, v1, v2, v3}, Lcom/intsig/camscanner/capture/util/CaptureActivityRouterUtil;->〇o00〇〇Oo(Landroid/content/Context;IJ)Landroid/content/Intent;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "extra_back_animaiton"

    .line 16
    .line 17
    const/4 v2, 0x1

    .line 18
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 19
    .line 20
    .line 21
    const-string v1, "extra_show_capture_mode_tips"

    .line 22
    .line 23
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 24
    .line 25
    .line 26
    const-string/jumbo v1, "support_mode"

    .line 27
    .line 28
    .line 29
    sget-object v2, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_NORMAL_MULTI:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 30
    .line 31
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 32
    .line 33
    .line 34
    const-string v1, "extra_folder_id"

    .line 35
    .line 36
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o〇oO08〇o0()Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v2

    .line 40
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 41
    .line 42
    .line 43
    const-string v1, "doc_title"

    .line 44
    .line 45
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oooo800〇〇()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v2

    .line 49
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 50
    .line 51
    .line 52
    const-string v1, "intent"

    .line 53
    .line 54
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    return-object v0
    .line 58
.end method

.method private static final O〇O〇88O8O(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroidx/activity/result/ActivityResult;)V
    .locals 1

    .line 1
    const-string/jumbo v0, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    const-string v0, "activityResult"

    .line 8
    .line 9
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->OOOo〇(Landroidx/activity/result/ActivityResult;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static final synthetic O〇o8(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)Z
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->OO〇80oO〇()Z

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final O〇oo8O80(Lkotlin/jvm/functions/Function0;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oo0O()J

    .line 4
    .line 5
    .line 6
    move-result-wide v1

    .line 7
    new-instance v3, LO〇O800oo/〇O888o0o;

    .line 8
    .line 9
    invoke-direct {v3, p1}, LO〇O800oo/〇O888o0o;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 10
    .line 11
    .line 12
    new-instance v4, LO〇O800oo/oo88o8O;

    .line 13
    .line 14
    invoke-direct {v4, p1}, LO〇O800oo/oo88o8O;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 15
    .line 16
    .line 17
    invoke-static {v0, v1, v2, v3, v4}, Lcom/intsig/camscanner/control/DataChecker;->〇80〇808〇O(Landroid/app/Activity;JLcom/intsig/camscanner/control/DataChecker$ActionListener;Lcom/intsig/camscanner/app/DbWaitingListener;)Z

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
.end method

.method private static final O〇〇O(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static synthetic O〇〇O80o8(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroidx/activity/result/ActivityResult;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇〇08〇0oo0(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroidx/activity/result/ActivityResult;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static final synthetic O〇〇o8O(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroid/content/Intent;Landroid/net/Uri;Ljava/lang/String;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8oo0oO0(Landroid/content/Intent;Landroid/net/Uri;Ljava/lang/String;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
.end method

.method private final o008()V
    .locals 6

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->O0O:Landroid/widget/ImageView;

    .line 6
    .line 7
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 8
    .line 9
    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->Oo0〇Ooo:Landroid/widget/TextView;

    .line 15
    .line 16
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 17
    .line 18
    .line 19
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇o〇OoO8()I

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    invoke-static {v0}, Lcom/intsig/camscanner/scenariodir/util/CertificateUtil;->oO80(I)Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    if-nez v0, :cond_0

    .line 28
    .line 29
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/folder/PreferenceFolderHelper;->o〇0()Z

    .line 30
    .line 31
    .line 32
    move-result v1

    .line 33
    if-nez v1, :cond_0

    .line 34
    .line 35
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oO800o()V

    .line 36
    .line 37
    .line 38
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 39
    .line 40
    const-string/jumbo v1, "unknown certificate type"

    .line 41
    .line 42
    .line 43
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    .line 45
    .line 46
    return-void

    .line 47
    :cond_0
    const-string v1, "mViewBinding.ivCardType"

    .line 48
    .line 49
    const/4 v2, 0x1

    .line 50
    const/4 v3, 0x0

    .line 51
    if-eqz v0, :cond_4

    .line 52
    .line 53
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 54
    .line 55
    .line 56
    move-result v4

    .line 57
    if-eqz v4, :cond_4

    .line 58
    .line 59
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 60
    .line 61
    .line 62
    move-result-object v4

    .line 63
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->〇〇08O:Landroid/widget/ImageView;

    .line 64
    .line 65
    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    .line 67
    .line 68
    invoke-virtual {v4, v3}, Landroid/view/View;->setVisibility(I)V

    .line 69
    .line 70
    .line 71
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 72
    .line 73
    .line 74
    move-result-object v1

    .line 75
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->〇〇08O:Landroid/widget/ImageView;

    .line 76
    .line 77
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getIconId()I

    .line 78
    .line 79
    .line 80
    move-result v4

    .line 81
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 82
    .line 83
    .line 84
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 85
    .line 86
    .line 87
    move-result v1

    .line 88
    invoke-direct {p0, v1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oO8o〇08〇(I)Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object v1

    .line 92
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 93
    .line 94
    .line 95
    move-result-object v4

    .line 96
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->〇08〇o0O:Landroid/widget/TextView;

    .line 97
    .line 98
    if-eqz v1, :cond_2

    .line 99
    .line 100
    invoke-static {v1}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 101
    .line 102
    .line 103
    move-result v5

    .line 104
    if-eqz v5, :cond_1

    .line 105
    .line 106
    goto :goto_0

    .line 107
    :cond_1
    const/4 v5, 0x0

    .line 108
    goto :goto_1

    .line 109
    :cond_2
    :goto_0
    const/4 v5, 0x1

    .line 110
    :goto_1
    if-eqz v5, :cond_3

    .line 111
    .line 112
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getNameId()I

    .line 113
    .line 114
    .line 115
    move-result v1

    .line 116
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 117
    .line 118
    .line 119
    move-result-object v1

    .line 120
    :cond_3
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    .line 122
    .line 123
    goto :goto_2

    .line 124
    :cond_4
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 125
    .line 126
    .line 127
    move-result-object v4

    .line 128
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->〇〇08O:Landroid/widget/ImageView;

    .line 129
    .line 130
    invoke-static {v4, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 131
    .line 132
    .line 133
    const/16 v1, 0x8

    .line 134
    .line 135
    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 136
    .line 137
    .line 138
    :goto_2
    const-string v1, ""

    .line 139
    .line 140
    invoke-direct {p0, v1, v3, v2}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇〇8088(Ljava/lang/String;ZZ)V

    .line 141
    .line 142
    .line 143
    iget-object v1, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O88O:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 144
    .line 145
    const/4 v4, 0x0

    .line 146
    if-nez v1, :cond_5

    .line 147
    .line 148
    const-string v1, "mDocItem"

    .line 149
    .line 150
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 151
    .line 152
    .line 153
    move-object v1, v4

    .line 154
    :cond_5
    invoke-virtual {v1}, Lcom/intsig/camscanner/datastruct/DocItem;->OO0o〇〇〇〇0()Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;

    .line 155
    .line 156
    .line 157
    move-result-object v1

    .line 158
    if-eqz v1, :cond_6

    .line 159
    .line 160
    invoke-direct {p0, v1, v3}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8O(Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;Z)V

    .line 161
    .line 162
    .line 163
    goto :goto_5

    .line 164
    :cond_6
    if-eqz v0, :cond_7

    .line 165
    .line 166
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getNeedManualFillIn()Z

    .line 167
    .line 168
    .line 169
    move-result v0

    .line 170
    if-nez v0, :cond_7

    .line 171
    .line 172
    goto :goto_3

    .line 173
    :cond_7
    const/4 v2, 0x0

    .line 174
    :goto_3
    if-nez v2, :cond_8

    .line 175
    .line 176
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->OO0o88()V

    .line 177
    .line 178
    .line 179
    goto :goto_5

    .line 180
    :cond_8
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 181
    .line 182
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->ooo008()Z

    .line 183
    .line 184
    .line 185
    move-result v1

    .line 186
    new-instance v2, Ljava/lang/StringBuilder;

    .line 187
    .line 188
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 189
    .line 190
    .line 191
    const-string v3, "autoOCR == "

    .line 192
    .line 193
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    .line 195
    .line 196
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 197
    .line 198
    .line 199
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 200
    .line 201
    .line 202
    move-result-object v1

    .line 203
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    .line 205
    .line 206
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->ooo008()Z

    .line 207
    .line 208
    .line 209
    move-result v0

    .line 210
    if-eqz v0, :cond_a

    .line 211
    .line 212
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oOO〇〇:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailAdapter;

    .line 213
    .line 214
    if-nez v0, :cond_9

    .line 215
    .line 216
    const-string v0, "mAdapter"

    .line 217
    .line 218
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 219
    .line 220
    .line 221
    goto :goto_4

    .line 222
    :cond_9
    move-object v4, v0

    .line 223
    :goto_4
    new-instance v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$updateCardView$1;

    .line 224
    .line 225
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$updateCardView$1;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 226
    .line 227
    .line 228
    invoke-virtual {v4, v0}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->registerAdapterDataObserver(Landroidx/recyclerview/widget/RecyclerView$AdapterDataObserver;)V

    .line 229
    .line 230
    .line 231
    :cond_a
    :goto_5
    return-void
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
.end method

.method private final o00o0O〇〇o()V
    .locals 4

    .line 1
    new-instance v0, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 2
    .line 3
    const-string v1, "page_list_load_doc"

    .line 4
    .line 5
    invoke-direct {v0, p0, v1}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;-><init>(Landroidx/lifecycle/LifecycleOwner;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    const-wide/16 v1, 0x5dc

    .line 9
    .line 10
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->〇80〇808〇O(J)V

    .line 11
    .line 12
    .line 13
    new-instance v3, LO〇O800oo/〇0〇O0088o;

    .line 14
    .line 15
    invoke-direct {v3, p0}, LO〇O800oo/〇0〇O0088o;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0, v3}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->OO0o〇〇(Ljava/lang/Runnable;)V

    .line 19
    .line 20
    .line 21
    iput-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O0O:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 22
    .line 23
    new-instance v0, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 24
    .line 25
    const-string v3, "page_list_load_page"

    .line 26
    .line 27
    invoke-direct {v0, p0, v3}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;-><init>(Landroidx/lifecycle/LifecycleOwner;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->〇80〇808〇O(J)V

    .line 31
    .line 32
    .line 33
    new-instance v1, LO〇O800oo/OoO8;

    .line 34
    .line 35
    invoke-direct {v1, p0}, LO〇O800oo/OoO8;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 36
    .line 37
    .line 38
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;->OO0o〇〇(Ljava/lang/Runnable;)V

    .line 39
    .line 40
    .line 41
    iput-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o8oOOo:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 42
    .line 43
    new-instance v0, Landroidx/lifecycle/ViewModelProvider;

    .line 44
    .line 45
    invoke-static {}, Lcom/intsig/camscanner/multiimageedit/factory/NewInstanceFactoryImpl;->〇080()Landroidx/lifecycle/ViewModelProvider$NewInstanceFactory;

    .line 46
    .line 47
    .line 48
    move-result-object v1

    .line 49
    const-string v2, "getInstance()"

    .line 50
    .line 51
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 52
    .line 53
    .line 54
    invoke-direct {v0, p0, v1}, Landroidx/lifecycle/ViewModelProvider;-><init>(Landroidx/lifecycle/ViewModelStoreOwner;Landroidx/lifecycle/ViewModelProvider$Factory;)V

    .line 55
    .line 56
    .line 57
    const-class v1, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel;

    .line 58
    .line 59
    invoke-virtual {v0, v1}, Landroidx/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroidx/lifecycle/ViewModel;

    .line 60
    .line 61
    .line 62
    move-result-object v0

    .line 63
    check-cast v0, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel;

    .line 64
    .line 65
    invoke-virtual {v0}, Lcom/intsig/camscanner/databaseManager/DatabaseCallbackViewModel;->〇80〇808〇O()Landroidx/lifecycle/MutableLiveData;

    .line 66
    .line 67
    .line 68
    move-result-object v0

    .line 69
    new-instance v1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$initDatabaseCallbackViewModel$3;

    .line 70
    .line 71
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$initDatabaseCallbackViewModel$3;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 72
    .line 73
    .line 74
    new-instance v2, LO〇O800oo/o800o8O;

    .line 75
    .line 76
    invoke-direct {v2, v1}, LO〇O800oo/o800o8O;-><init>(Lkotlin/jvm/functions/Function1;)V

    .line 77
    .line 78
    .line 79
    invoke-virtual {v0, p0, v2}, Landroidx/lifecycle/LiveData;->observe(Landroidx/lifecycle/LifecycleOwner;Landroidx/lifecycle/Observer;)V

    .line 80
    .line 81
    .line 82
    return-void
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private final o00oooo(Landroidx/activity/result/ActivityResult;Ljava/lang/String;)V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 2
    .line 3
    invoke-virtual {p1}, Landroidx/activity/result/ActivityResult;->getResultCode()I

    .line 4
    .line 5
    .line 6
    move-result p1

    .line 7
    new-instance v1, Ljava/lang/StringBuilder;

    .line 8
    .line 9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 10
    .line 11
    .line 12
    const-string v2, "onSystemCameraResult: "

    .line 13
    .line 14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 18
    .line 19
    .line 20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    .line 26
    .line 27
    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oo0O()J

    .line 32
    .line 33
    .line 34
    move-result-wide v1

    .line 35
    invoke-static {p1, v1, v2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇o00〇〇Oo(Landroid/content/Context;J)Z

    .line 36
    .line 37
    .line 38
    move-result p1

    .line 39
    if-eqz p1, :cond_5

    .line 40
    .line 41
    if-eqz p2, :cond_1

    .line 42
    .line 43
    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    .line 44
    .line 45
    .line 46
    move-result p1

    .line 47
    if-nez p1, :cond_0

    .line 48
    .line 49
    goto :goto_0

    .line 50
    :cond_0
    const/4 p1, 0x0

    .line 51
    goto :goto_1

    .line 52
    :cond_1
    :goto_0
    const/4 p1, 0x1

    .line 53
    :goto_1
    const v1, 0x7f130085

    .line 54
    .line 55
    .line 56
    if-eqz p1, :cond_2

    .line 57
    .line 58
    const-string p1, "mTmpPhotoPath == null"

    .line 59
    .line 60
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    .line 62
    .line 63
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 64
    .line 65
    invoke-static {p1, v1}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 66
    .line 67
    .line 68
    goto :goto_2

    .line 69
    :cond_2
    new-instance p1, Ljava/io/File;

    .line 70
    .line 71
    invoke-direct {p1, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 72
    .line 73
    .line 74
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    .line 75
    .line 76
    .line 77
    move-result p2

    .line 78
    if-eqz p2, :cond_4

    .line 79
    .line 80
    new-instance p2, Ljava/io/File;

    .line 81
    .line 82
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->〇〇808〇()Ljava/lang/String;

    .line 83
    .line 84
    .line 85
    move-result-object v2

    .line 86
    const-string v3, ".jpg"

    .line 87
    .line 88
    invoke-static {v2, v3}, Lcom/intsig/camscanner/util/SDStorageManager;->〇O8o08O(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object v2

    .line 92
    invoke-direct {p2, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 93
    .line 94
    .line 95
    :try_start_0
    invoke-static {p1, p2}, Lcom/intsig/utils/FileUtil;->Oo08(Ljava/io/File;Ljava/io/File;)V

    .line 96
    .line 97
    .line 98
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    .line 99
    .line 100
    .line 101
    move-result p1

    .line 102
    if-eqz p1, :cond_3

    .line 103
    .line 104
    invoke-static {p2}, Lcom/intsig/utils/FileUtil;->〇〇8O0〇8(Ljava/io/File;)Landroid/net/Uri;

    .line 105
    .line 106
    .line 107
    move-result-object p1

    .line 108
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇o88〇O(Landroid/net/Uri;)V

    .line 109
    .line 110
    .line 111
    goto :goto_2

    .line 112
    :cond_3
    const-string p1, "copyFile fail"

    .line 113
    .line 114
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    .line 116
    .line 117
    goto :goto_2

    .line 118
    :catch_0
    move-exception p1

    .line 119
    iget-object p2, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 120
    .line 121
    invoke-static {p2, v1}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 122
    .line 123
    .line 124
    sget-object p2, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 125
    .line 126
    invoke-static {p2, p1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 127
    .line 128
    .line 129
    goto :goto_2

    .line 130
    :cond_4
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 131
    .line 132
    invoke-static {p1, v1}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 133
    .line 134
    .line 135
    const-string/jumbo p1, "tempFile is not exists"

    .line 136
    .line 137
    .line 138
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    .line 140
    .line 141
    :cond_5
    :goto_2
    return-void
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

.method public static final synthetic o088O8800()Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private static final o088〇〇(Landroidx/activity/result/ActivityResult;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "batch result == "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object p0

    .line 20
    invoke-static {v0, p0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public static synthetic o0O0O〇〇〇0(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->Oo〇〇〇〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static final synthetic o0OO(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;IZ)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O〇8O0O80〇(IZ)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public static synthetic o0Oo(Lkotlin/jvm/functions/Function0;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oo8(Lkotlin/jvm/functions/Function0;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private static final o0o〇〇〇8o(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroidx/activity/result/ActivityResult;)V
    .locals 1

    .line 1
    const-string/jumbo v0, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    const-string v0, "activityResult"

    .line 8
    .line 9
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->OOO0o〇(Landroidx/activity/result/ActivityResult;)V

    .line 13
    .line 14
    .line 15
    return-void
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private static final o0〇(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final o0〇OO008O()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "User Operation: import photo"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 9
    .line 10
    const/16 v1, 0x3ea

    .line 11
    .line 12
    const/4 v2, 0x1

    .line 13
    invoke-static {v0, v1, v2}, Lcom/intsig/camscanner/app/IntentUtil;->〇oo〇(Landroid/app/Activity;IZ)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static final synthetic o0〇〇00〇o(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o8〇O〇0O0〇(Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static synthetic o808o8o08(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroidx/activity/result/ActivityResult;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O〇O〇88O8O(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroidx/activity/result/ActivityResult;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final o88O8()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "finishWhenDocNotExist"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-object v1, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 9
    .line 10
    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    .line 11
    .line 12
    .line 13
    move-result v1

    .line 14
    if-eqz v1, :cond_0

    .line 15
    .line 16
    const-string v1, "activity is finishing, return"

    .line 17
    .line 18
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    return-void

    .line 22
    :cond_0
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 23
    .line 24
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 25
    .line 26
    .line 27
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static final synthetic o88o88(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)Lcom/intsig/mvp/activity/BaseChangeActivity;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static final synthetic o88oo〇O(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)[Ljava/lang/String;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o8oo0OOO()[Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static final synthetic o8O〇008(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O0oO(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final o8o0(Z)V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/ipo/IPOCheck;->o800o8O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o08〇808(Z)V

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailShowPersonalInfoDialog;->o〇00O:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailShowPersonalInfoDialog$Companion;

    .line 12
    .line 13
    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 14
    .line 15
    .line 16
    move-result-object v0

    .line 17
    const-string/jumbo v1, "supportFragmentManager"

    .line 18
    .line 19
    .line 20
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    const/4 v1, 0x2

    .line 24
    const/4 v2, 0x0

    .line 25
    invoke-static {p1, v0, v2, v1, v2}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailShowPersonalInfoDialog$Companion;->〇o〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailShowPersonalInfoDialog$Companion;Landroidx/fragment/app/FragmentManager;Ljava/lang/Runnable;ILjava/lang/Object;)V

    .line 26
    .line 27
    .line 28
    :goto_0
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public static final synthetic o8o0o8(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)I
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O0〇8〇()I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final o8o8〇o()V
    .locals 9

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-wide/16 v1, -0x1

    .line 6
    .line 7
    if-eqz v0, :cond_0

    .line 8
    .line 9
    const-string v3, "INTENT_KEY_DOC_ID"

    .line 10
    .line 11
    invoke-virtual {v0, v3, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    .line 12
    .line 13
    .line 14
    move-result-wide v1

    .line 15
    :cond_0
    move-wide v4, v1

    .line 16
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 17
    .line 18
    .line 19
    move-result-object v0

    .line 20
    const/4 v1, 0x0

    .line 21
    if-eqz v0, :cond_1

    .line 22
    .line 23
    const-string v2, "INTENT_KEY_FROM_WHERE"

    .line 24
    .line 25
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    :cond_1
    iput-boolean v1, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇o0O:Z

    .line 30
    .line 31
    invoke-static {v4, v5}, Lcom/intsig/camscanner/app/DBUtil;->o〇8oOO88(J)Lcom/intsig/camscanner/datastruct/DocItem;

    .line 32
    .line 33
    .line 34
    move-result-object v0

    .line 35
    sget-object v1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 36
    .line 37
    new-instance v2, Ljava/lang/StringBuilder;

    .line 38
    .line 39
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 40
    .line 41
    .line 42
    const-string v3, "docItem == "

    .line 43
    .line 44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 45
    .line 46
    .line 47
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 48
    .line 49
    .line 50
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 51
    .line 52
    .line 53
    move-result-object v2

    .line 54
    invoke-static {v1, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    const-wide/16 v2, 0x0

    .line 58
    .line 59
    cmp-long v6, v4, v2

    .line 60
    .line 61
    if-ltz v6, :cond_3

    .line 62
    .line 63
    if-nez v0, :cond_2

    .line 64
    .line 65
    goto :goto_0

    .line 66
    :cond_2
    iput-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O88O:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 67
    .line 68
    invoke-virtual {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O08o()Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 69
    .line 70
    .line 71
    move-result-object v3

    .line 72
    const/4 v6, 0x0

    .line 73
    const/4 v7, 0x2

    .line 74
    const/4 v8, 0x0

    .line 75
    invoke-static/range {v3 .. v8}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;->〇00(Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;JZILjava/lang/Object;)V

    .line 76
    .line 77
    .line 78
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇〇80o〇o0()V

    .line 79
    .line 80
    .line 81
    return-void

    .line 82
    :cond_3
    :goto_0
    const-string v0, "finish as docItem == null"

    .line 83
    .line 84
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 88
    .line 89
    .line 90
    return-void
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private final o8oo0OOO()[Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "EXTRA_QUERY_STRING"

    .line 6
    .line 7
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private final o8〇8oooO〇()V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇080:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇O888o0o()V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇o〇OoO8()I

    .line 7
    .line 8
    .line 9
    move-result v0

    .line 10
    invoke-static {v0}, Lcom/intsig/camscanner/scenariodir/util/CertificateUtil;->oO80(I)Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 11
    .line 12
    .line 13
    move-result-object v0

    .line 14
    const/4 v1, 0x0

    .line 15
    if-eqz v0, :cond_0

    .line 16
    .line 17
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getNameId()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    goto :goto_0

    .line 26
    :cond_0
    move-object v0, v1

    .line 27
    :goto_0
    if-eqz v0, :cond_1

    .line 28
    .line 29
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 30
    .line 31
    .line 32
    move-result v0

    .line 33
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    goto :goto_1

    .line 38
    :cond_1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oooo800〇〇()Ljava/lang/String;

    .line 39
    .line 40
    .line 41
    move-result-object v0

    .line 42
    :goto_1
    if-nez v0, :cond_2

    .line 43
    .line 44
    const-string v0, ""

    .line 45
    .line 46
    :cond_2
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/folder/PreferenceFolderHelper;->o〇0()Z

    .line 47
    .line 48
    .line 49
    move-result v2

    .line 50
    const/4 v3, 0x1

    .line 51
    if-nez v2, :cond_8

    .line 52
    .line 53
    iget-object v2, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O88O:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 54
    .line 55
    if-nez v2, :cond_3

    .line 56
    .line 57
    const-string v2, "mDocItem"

    .line 58
    .line 59
    invoke-static {v2}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 60
    .line 61
    .line 62
    move-object v2, v1

    .line 63
    :cond_3
    invoke-virtual {v2}, Lcom/intsig/camscanner/datastruct/DocItem;->OO0o〇〇〇〇0()Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;

    .line 64
    .line 65
    .line 66
    move-result-object v2

    .line 67
    if-eqz v2, :cond_4

    .line 68
    .line 69
    invoke-virtual {v2}, Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;->getCert_detail()Ljava/util/List;

    .line 70
    .line 71
    .line 72
    move-result-object v1

    .line 73
    :cond_4
    check-cast v1, Ljava/util/Collection;

    .line 74
    .line 75
    const/4 v2, 0x0

    .line 76
    if-eqz v1, :cond_6

    .line 77
    .line 78
    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    .line 79
    .line 80
    .line 81
    move-result v1

    .line 82
    if-eqz v1, :cond_5

    .line 83
    .line 84
    goto :goto_2

    .line 85
    :cond_5
    const/4 v1, 0x0

    .line 86
    goto :goto_3

    .line 87
    :cond_6
    :goto_2
    const/4 v1, 0x1

    .line 88
    :goto_3
    if-nez v1, :cond_7

    .line 89
    .line 90
    goto :goto_4

    .line 91
    :cond_7
    const/4 v3, 0x0

    .line 92
    :cond_8
    :goto_4
    new-instance v1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailMoreDialog;

    .line 93
    .line 94
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇o〇OoO8()I

    .line 95
    .line 96
    .line 97
    move-result v2

    .line 98
    invoke-direct {v1, p0, v0, v3, v2}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailMoreDialog;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Ljava/lang/String;ZI)V

    .line 99
    .line 100
    .line 101
    invoke-virtual {v1}, Lcom/intsig/camscanner/view/CsBottomItemsDialog;->show()V

    .line 102
    .line 103
    .line 104
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 105
    .line 106
    const-string v1, "more dialog show"

    .line 107
    .line 108
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    .line 110
    .line 111
    return-void
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private final o8〇O〇0O0〇(Z)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O08o()Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;->Oooo8o0〇()Landroidx/lifecycle/MutableLiveData;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;

    .line 14
    .line 15
    new-instance v1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$goRetry$1;

    .line 16
    .line 17
    invoke-direct {v1, p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$goRetry$1;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Z)V

    .line 18
    .line 19
    .line 20
    invoke-static {v0, v1}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker;->〇o00〇〇Oo(Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;Lkotlin/jvm/functions/Function0;)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private static final oO0o(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroid/view/View;)V
    .locals 2

    .line 1
    const-string/jumbo v0, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    .line 8
    .line 9
    .line 10
    move-result-object p1

    .line 11
    const-string v0, "null cannot be cast to non-null type kotlin.String"

    .line 12
    .line 13
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->o〇0(Ljava/lang/Object;Ljava/lang/String;)V

    .line 14
    .line 15
    .line 16
    check-cast p1, Ljava/lang/String;

    .line 17
    .line 18
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 19
    .line 20
    const/4 v1, 0x0

    .line 21
    invoke-static {v0, v1, p1}, Lcom/intsig/camscanner/app/AppUtil;->〇O00(Landroid/content/Context;Ljava/lang/String;Ljava/lang/CharSequence;)Z

    .line 22
    .line 23
    .line 24
    iget-object p1, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 25
    .line 26
    const v0, 0x7f13097e

    .line 27
    .line 28
    .line 29
    invoke-static {p1, v0}, Lcom/intsig/utils/ToastUtils;->OO0o〇〇〇〇0(Landroid/content/Context;I)V

    .line 30
    .line 31
    .line 32
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇080:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;

    .line 33
    .line 34
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0o〇o()Ljava/lang/String;

    .line 35
    .line 36
    .line 37
    move-result-object v0

    .line 38
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->OO0〇O()I

    .line 39
    .line 40
    .line 41
    move-result p0

    .line 42
    invoke-virtual {p1, v0, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇O〇(Ljava/lang/String;I)V

    .line 43
    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static final synthetic oO8(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailViewModel;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O8〇()Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final oO800o()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 2
    .line 3
    const-string/jumbo v1, "showCardTypeError"

    .line 4
    .line 5
    .line 6
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    new-instance v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$showCardTypeError$1;

    .line 10
    .line 11
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$showCardTypeError$1;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0〇8o〇(Lkotlin/jvm/functions/Function1;)V

    .line 15
    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->Oo0〇Ooo:Landroid/widget/TextView;

    .line 22
    .line 23
    const v1, 0x7f131078

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 31
    .line 32
    .line 33
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->〇〇〇0o〇〇0:Landroid/widget/TextView;

    .line 38
    .line 39
    const v1, 0x7f13122c

    .line 40
    .line 41
    .line 42
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final oO88〇0O8O(Ljava/util/ArrayList;Lkotlin/jvm/functions/Function0;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;",
            "Lkotlin/jvm/functions/Function0<",
            "Lkotlin/Unit;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    new-instance v1, LO〇O800oo/Oooo8o0〇;

    .line 4
    .line 5
    invoke-direct {v1, p2}, LO〇O800oo/Oooo8o0〇;-><init>(Lkotlin/jvm/functions/Function0;)V

    .line 6
    .line 7
    .line 8
    const/4 p2, 0x0

    .line 9
    invoke-static {v0, p1, p2, v1}, Lcom/intsig/camscanner/control/DataChecker;->〇O00(Landroid/app/Activity;Ljava/util/ArrayList;Ljava/lang/String;Lcom/intsig/camscanner/control/DataChecker$ActionListener;)Z

    .line 10
    .line 11
    .line 12
    return-void
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final oO8o〇08〇(I)Ljava/lang/String;
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_OTHER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->getType()I

    .line 4
    .line 5
    .line 6
    move-result v0

    .line 7
    const/4 v1, 0x0

    .line 8
    if-eq p1, v0, :cond_0

    .line 9
    .line 10
    return-object v1

    .line 11
    :cond_0
    iget-object p1, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O88O:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 12
    .line 13
    const-string v0, "mDocItem"

    .line 14
    .line 15
    if-nez p1, :cond_1

    .line 16
    .line 17
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 18
    .line 19
    .line 20
    move-object p1, v1

    .line 21
    :cond_1
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->OO0o〇〇〇〇0()Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;

    .line 22
    .line 23
    .line 24
    move-result-object p1

    .line 25
    if-eqz p1, :cond_2

    .line 26
    .line 27
    invoke-virtual {p1}, Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;->getCert_detail()Ljava/util/List;

    .line 28
    .line 29
    .line 30
    move-result-object p1

    .line 31
    goto :goto_0

    .line 32
    :cond_2
    move-object p1, v1

    .line 33
    :goto_0
    check-cast p1, Ljava/util/Collection;

    .line 34
    .line 35
    if-eqz p1, :cond_4

    .line 36
    .line 37
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    .line 38
    .line 39
    .line 40
    move-result p1

    .line 41
    if-eqz p1, :cond_3

    .line 42
    .line 43
    goto :goto_1

    .line 44
    :cond_3
    const/4 p1, 0x0

    .line 45
    goto :goto_2

    .line 46
    :cond_4
    :goto_1
    const/4 p1, 0x1

    .line 47
    :goto_2
    if-eqz p1, :cond_5

    .line 48
    .line 49
    return-object v1

    .line 50
    :cond_5
    iget-object p1, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O88O:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 51
    .line 52
    if-nez p1, :cond_6

    .line 53
    .line 54
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 55
    .line 56
    .line 57
    move-object p1, v1

    .line 58
    :cond_6
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->OO0o〇〇〇〇0()Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;

    .line 59
    .line 60
    .line 61
    move-result-object p1

    .line 62
    if-eqz p1, :cond_8

    .line 63
    .line 64
    invoke-virtual {p1}, Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;->getCert_detail()Ljava/util/List;

    .line 65
    .line 66
    .line 67
    move-result-object p1

    .line 68
    if-eqz p1, :cond_8

    .line 69
    .line 70
    check-cast p1, Ljava/lang/Iterable;

    .line 71
    .line 72
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 73
    .line 74
    .line 75
    move-result-object p1

    .line 76
    :cond_7
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    .line 77
    .line 78
    .line 79
    move-result v0

    .line 80
    if-eqz v0, :cond_8

    .line 81
    .line 82
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 83
    .line 84
    .line 85
    move-result-object v0

    .line 86
    check-cast v0, Lcom/intsig/camscanner/scenariodir/data/DetailValue;

    .line 87
    .line 88
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/data/DetailValue;->getKey()Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object v2

    .line 92
    const-string v3, "foregin_card_user_card_name"

    .line 93
    .line 94
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 95
    .line 96
    .line 97
    move-result v2

    .line 98
    if-eqz v2, :cond_7

    .line 99
    .line 100
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/data/DetailValue;->getValue()Ljava/lang/String;

    .line 101
    .line 102
    .line 103
    move-result-object p1

    .line 104
    return-object p1

    .line 105
    :cond_8
    return-object v1
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method private final oO8o〇o〇8(ZZ)V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NotifyDataSetChanged"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string/jumbo v2, "showEyeOpen = "

    .line 9
    .line 10
    .line 11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 12
    .line 13
    .line 14
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 15
    .line 16
    .line 17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 18
    .line 19
    .line 20
    move-result-object v1

    .line 21
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    .line 23
    .line 24
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->o〇oO:Landroid/widget/TextView;

    .line 29
    .line 30
    const-string v1, "mViewBinding.tvCardOwner"

    .line 31
    .line 32
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 33
    .line 34
    .line 35
    iget-object v1, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->Ooo08:Ljava/lang/String;

    .line 36
    .line 37
    const/4 v2, 0x1

    .line 38
    const/4 v3, 0x0

    .line 39
    if-eqz v1, :cond_1

    .line 40
    .line 41
    invoke-static {v1}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 42
    .line 43
    .line 44
    move-result v1

    .line 45
    if-eqz v1, :cond_0

    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_0
    const/4 v1, 0x0

    .line 49
    goto :goto_1

    .line 50
    :cond_1
    :goto_0
    const/4 v1, 0x1

    .line 51
    :goto_1
    if-eqz v1, :cond_2

    .line 52
    .line 53
    if-nez p2, :cond_2

    .line 54
    .line 55
    const/4 p2, 0x1

    .line 56
    goto :goto_2

    .line 57
    :cond_2
    const/4 p2, 0x0

    .line 58
    :goto_2
    const/16 v1, 0x8

    .line 59
    .line 60
    if-eqz p2, :cond_3

    .line 61
    .line 62
    const/16 p2, 0x8

    .line 63
    .line 64
    goto :goto_3

    .line 65
    :cond_3
    const/4 p2, 0x0

    .line 66
    :goto_3
    invoke-virtual {v0, p2}, Landroid/view/View;->setVisibility(I)V

    .line 67
    .line 68
    .line 69
    if-eqz p1, :cond_4

    .line 70
    .line 71
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 72
    .line 73
    .line 74
    move-result-object p2

    .line 75
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->O0O:Landroid/widget/ImageView;

    .line 76
    .line 77
    const v0, 0x7f080791

    .line 78
    .line 79
    .line 80
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 81
    .line 82
    .line 83
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 84
    .line 85
    .line 86
    move-result-object p2

    .line 87
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->〇OO8ooO8〇:Landroidx/appcompat/widget/AppCompatTextView;

    .line 88
    .line 89
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇OO8ooO8〇:Ljava/lang/String;

    .line 90
    .line 91
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    .line 93
    .line 94
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 95
    .line 96
    .line 97
    move-result-object p2

    .line 98
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->ooO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 99
    .line 100
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->ooO:Ljava/lang/String;

    .line 101
    .line 102
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    .line 104
    .line 105
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 106
    .line 107
    .line 108
    move-result-object p2

    .line 109
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->〇OO〇00〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 110
    .line 111
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇OO〇00〇0O:Ljava/lang/String;

    .line 112
    .line 113
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    .line 115
    .line 116
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 117
    .line 118
    .line 119
    move-result-object p2

    .line 120
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->o〇oO:Landroid/widget/TextView;

    .line 121
    .line 122
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->Ooo08:Ljava/lang/String;

    .line 123
    .line 124
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    .line 126
    .line 127
    goto :goto_4

    .line 128
    :cond_4
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 129
    .line 130
    .line 131
    move-result-object p2

    .line 132
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->O0O:Landroid/widget/ImageView;

    .line 133
    .line 134
    const v0, 0x7f080790

    .line 135
    .line 136
    .line 137
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 138
    .line 139
    .line 140
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 141
    .line 142
    .line 143
    move-result-object p2

    .line 144
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->〇OO8ooO8〇:Landroidx/appcompat/widget/AppCompatTextView;

    .line 145
    .line 146
    const-string v0, "******"

    .line 147
    .line 148
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    .line 150
    .line 151
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 152
    .line 153
    .line 154
    move-result-object p2

    .line 155
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->ooO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 156
    .line 157
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    .line 159
    .line 160
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 161
    .line 162
    .line 163
    move-result-object p2

    .line 164
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->〇OO〇00〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 165
    .line 166
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    .line 168
    .line 169
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 170
    .line 171
    .line 172
    move-result-object p2

    .line 173
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->o〇oO:Landroid/widget/TextView;

    .line 174
    .line 175
    const-string v0, "**"

    .line 176
    .line 177
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    .line 179
    .line 180
    :goto_4
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 181
    .line 182
    .line 183
    move-result-object p2

    .line 184
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->〇〇o〇:Landroid/widget/TextView;

    .line 185
    .line 186
    const-string v0, "mViewBinding.tvCopy1"

    .line 187
    .line 188
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 189
    .line 190
    .line 191
    if-eqz p1, :cond_6

    .line 192
    .line 193
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 194
    .line 195
    .line 196
    move-result-object v0

    .line 197
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->〇OO8ooO8〇:Landroidx/appcompat/widget/AppCompatTextView;

    .line 198
    .line 199
    const-string v4, "mViewBinding.tvItemValue1"

    .line 200
    .line 201
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 202
    .line 203
    .line 204
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 205
    .line 206
    .line 207
    move-result v0

    .line 208
    if-nez v0, :cond_5

    .line 209
    .line 210
    const/4 v0, 0x1

    .line 211
    goto :goto_5

    .line 212
    :cond_5
    const/4 v0, 0x0

    .line 213
    :goto_5
    if-eqz v0, :cond_6

    .line 214
    .line 215
    const/4 v0, 0x1

    .line 216
    goto :goto_6

    .line 217
    :cond_6
    const/4 v0, 0x0

    .line 218
    :goto_6
    if-eqz v0, :cond_7

    .line 219
    .line 220
    const/4 v0, 0x0

    .line 221
    goto :goto_7

    .line 222
    :cond_7
    const/16 v0, 0x8

    .line 223
    .line 224
    :goto_7
    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 225
    .line 226
    .line 227
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 228
    .line 229
    .line 230
    move-result-object p2

    .line 231
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->Oo80:Landroid/widget/TextView;

    .line 232
    .line 233
    const-string v0, "mViewBinding.tvCopy2"

    .line 234
    .line 235
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 236
    .line 237
    .line 238
    if-eqz p1, :cond_9

    .line 239
    .line 240
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 241
    .line 242
    .line 243
    move-result-object v0

    .line 244
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->ooO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 245
    .line 246
    const-string v4, "mViewBinding.tvItemValue2"

    .line 247
    .line 248
    invoke-static {v0, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 249
    .line 250
    .line 251
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    .line 252
    .line 253
    .line 254
    move-result v0

    .line 255
    if-nez v0, :cond_8

    .line 256
    .line 257
    const/4 v0, 0x1

    .line 258
    goto :goto_8

    .line 259
    :cond_8
    const/4 v0, 0x0

    .line 260
    :goto_8
    if-eqz v0, :cond_9

    .line 261
    .line 262
    const/4 v0, 0x1

    .line 263
    goto :goto_9

    .line 264
    :cond_9
    const/4 v0, 0x0

    .line 265
    :goto_9
    if-eqz v0, :cond_a

    .line 266
    .line 267
    const/4 v0, 0x0

    .line 268
    goto :goto_a

    .line 269
    :cond_a
    const/16 v0, 0x8

    .line 270
    .line 271
    :goto_a
    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 272
    .line 273
    .line 274
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 275
    .line 276
    .line 277
    move-result-object p2

    .line 278
    iget-object p2, p2, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->O〇o88o08〇:Landroid/widget/TextView;

    .line 279
    .line 280
    const-string v0, "mViewBinding.tvCopy3"

    .line 281
    .line 282
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 283
    .line 284
    .line 285
    if-eqz p1, :cond_c

    .line 286
    .line 287
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 288
    .line 289
    .line 290
    move-result-object p1

    .line 291
    iget-object p1, p1, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->〇OO〇00〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 292
    .line 293
    const-string v0, "mViewBinding.tvItemValue3"

    .line 294
    .line 295
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 296
    .line 297
    .line 298
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    .line 299
    .line 300
    .line 301
    move-result p1

    .line 302
    if-nez p1, :cond_b

    .line 303
    .line 304
    const/4 p1, 0x1

    .line 305
    goto :goto_b

    .line 306
    :cond_b
    const/4 p1, 0x0

    .line 307
    :goto_b
    if-eqz p1, :cond_c

    .line 308
    .line 309
    goto :goto_c

    .line 310
    :cond_c
    const/4 v2, 0x0

    .line 311
    :goto_c
    if-eqz v2, :cond_d

    .line 312
    .line 313
    goto :goto_d

    .line 314
    :cond_d
    const/16 v3, 0x8

    .line 315
    .line 316
    :goto_d
    invoke-virtual {p2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 317
    .line 318
    .line 319
    iget-object p1, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oOO〇〇:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailAdapter;

    .line 320
    .line 321
    if-nez p1, :cond_e

    .line 322
    .line 323
    const-string p1, "mAdapter"

    .line 324
    .line 325
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 326
    .line 327
    .line 328
    const/4 p1, 0x0

    .line 329
    :cond_e
    invoke-virtual {p1}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->notifyDataSetChanged()V

    .line 330
    .line 331
    .line 332
    return-void
    .line 333
    .line 334
    .line 335
.end method

.method public static final synthetic oOO8oo0(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Lkotlin/jvm/functions/Function0;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O〇oo8O80(Lkotlin/jvm/functions/Function0;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final oOOO0()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇o〇OoO8()I

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-gez v0, :cond_0

    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 8
    .line 9
    const v1, 0x7f131044

    .line 10
    .line 11
    .line 12
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    invoke-static {v0, v1}, Lcom/intsig/utils/ToastUtils;->〇〇808〇(Landroid/content/Context;Ljava/lang/String;)V

    .line 17
    .line 18
    .line 19
    return-void

    .line 20
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$go2CompositeCertificate$1;

    .line 21
    .line 22
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$go2CompositeCertificate$1;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 23
    .line 24
    .line 25
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O〇oo8O80(Lkotlin/jvm/functions/Function0;)V

    .line 26
    .line 27
    .line 28
    return-void
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final oOO〇0o8〇(Lcom/intsig/utils/CsResult;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/intsig/utils/CsResult<",
            "Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;",
            ">;)V"
        }
    .end annotation

    .line 1
    const/4 v1, 0x0

    .line 2
    new-instance v2, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$onCardOcrResult$1;

    .line 3
    .line 4
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$onCardOcrResult$1;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 5
    .line 6
    .line 7
    new-instance v3, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$onCardOcrResult$2;

    .line 8
    .line 9
    invoke-direct {v3, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$onCardOcrResult$2;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 10
    .line 11
    .line 12
    new-instance v4, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$onCardOcrResult$3;

    .line 13
    .line 14
    invoke-direct {v4, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$onCardOcrResult$3;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 15
    .line 16
    .line 17
    const/4 v5, 0x1

    .line 18
    const/4 v6, 0x0

    .line 19
    move-object v0, p1

    .line 20
    invoke-static/range {v0 .. v6}, Lcom/intsig/utils/CsResultKt;->〇o00〇〇Oo(Lcom/intsig/utils/CsResult;Lkotlin/jvm/functions/Function0;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function1;Lkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public static final synthetic oO〇O0O(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)Lcom/intsig/camscanner/datastruct/DocItem;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O88O:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final oo0O()J
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O88O:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "mDocItem"

    .line 6
    .line 7
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 12
    .line 13
    .line 14
    move-result-wide v0

    .line 15
    return-wide v0
.end method

.method private static final oo8(Lkotlin/jvm/functions/Function0;I)V
    .locals 1

    .line 1
    const-string p1, "$afterCheck"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 7
    .line 8
    const-string v0, "ActionListener  doc all checked"

    .line 9
    .line 10
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-interface {p0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final oo88()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "capture2ReplacePicture"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    new-instance v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$capture2ReplacePicture$1;

    .line 9
    .line 10
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$capture2ReplacePicture$1;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 11
    .line 12
    .line 13
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O888Oo(Lkotlin/jvm/functions/Function0;)V

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static final synthetic oo8〇〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O0o0〇8o()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final ooo008()Z
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/utils/PreferenceUtil;->oO80()Lcom/intsig/utils/PreferenceUtil;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const v1, 0x7f131d57

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 9
    .line 10
    .line 11
    move-result-object v1

    .line 12
    const/4 v2, 0x0

    .line 13
    invoke-virtual {v0, v1, v2}, Lcom/intsig/utils/PreferenceUtil;->Oo08(Ljava/lang/String;Z)Z

    .line 14
    .line 15
    .line 16
    move-result v0

    .line 17
    return v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final ooo0〇080()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 2
    .line 3
    const-string/jumbo v1, "showPictureError"

    .line 4
    .line 5
    .line 6
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    new-instance v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$showPictureError$1;

    .line 10
    .line 11
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$showPictureError$1;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0〇8o〇(Lkotlin/jvm/functions/Function1;)V

    .line 15
    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->Oo0〇Ooo:Landroid/widget/TextView;

    .line 22
    .line 23
    const v1, 0x7f131078

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 31
    .line 32
    .line 33
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->〇〇〇0o〇〇0:Landroid/widget/TextView;

    .line 38
    .line 39
    const v1, 0x7f13122b

    .line 40
    .line 41
    .line 42
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final oooO8〇00()Landroid/content/Intent;
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O0〇8〇()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oo0O()J

    .line 8
    .line 9
    .line 10
    move-result-wide v2

    .line 11
    invoke-static {v0, v1, v2, v3}, Lcom/intsig/camscanner/capture/util/CaptureActivityRouterUtil;->〇o00〇〇Oo(Landroid/content/Context;IJ)Landroid/content/Intent;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    const-string v1, "extra_back_animaiton"

    .line 16
    .line 17
    const/4 v2, 0x1

    .line 18
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 19
    .line 20
    .line 21
    const-string v1, "extra_show_capture_mode_tips"

    .line 22
    .line 23
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 24
    .line 25
    .line 26
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇o〇OoO8()I

    .line 27
    .line 28
    .line 29
    move-result v1

    .line 30
    const/16 v2, 0xbbb

    .line 31
    .line 32
    if-ne v1, v2, :cond_0

    .line 33
    .line 34
    sget-object v1, Lcom/intsig/camscanner/capture/CaptureMode;->NORMAL_MULTI:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 35
    .line 36
    goto :goto_0

    .line 37
    :cond_0
    sget-object v1, Lcom/intsig/camscanner/capture/CaptureMode;->CERTIFICATE:Lcom/intsig/camscanner/capture/CaptureMode;

    .line 38
    .line 39
    :goto_0
    const-string v3, "capture_mode"

    .line 40
    .line 41
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 42
    .line 43
    .line 44
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇o〇OoO8()I

    .line 45
    .line 46
    .line 47
    move-result v1

    .line 48
    if-ne v1, v2, :cond_1

    .line 49
    .line 50
    sget-object v1, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_NORMAL_MULTI:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 51
    .line 52
    goto :goto_1

    .line 53
    :cond_1
    sget-object v1, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ONLY_CERTIFICATION:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 54
    .line 55
    :goto_1
    const-string/jumbo v2, "support_mode"

    .line 56
    .line 57
    .line 58
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 59
    .line 60
    .line 61
    const-string v1, "extra_folder_id"

    .line 62
    .line 63
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o〇oO08〇o0()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object v2

    .line 67
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 68
    .line 69
    .line 70
    const-string v1, "doc_title"

    .line 71
    .line 72
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oooo800〇〇()Ljava/lang/String;

    .line 73
    .line 74
    .line 75
    move-result-object v2

    .line 76
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 77
    .line 78
    .line 79
    const-string v1, "extra_entrance"

    .line 80
    .line 81
    sget-object v2, Lcom/intsig/camscanner/purchase/track/FunctionEntrance;->CS_ADVANCED_FOLDER_CERTIFICATE_RETAKE:Lcom/intsig/camscanner/purchase/track/FunctionEntrance;

    .line 82
    .line 83
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 84
    .line 85
    .line 86
    const-string v1, "EXTRA_DOC_TYPE"

    .line 87
    .line 88
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇o〇OoO8()I

    .line 89
    .line 90
    .line 91
    move-result v2

    .line 92
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 93
    .line 94
    .line 95
    const-string v1, "intent"

    .line 96
    .line 97
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 98
    .line 99
    .line 100
    return-object v0
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private final oooo800〇〇()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O88O:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "mDocItem"

    .line 6
    .line 7
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->o〇8oOO88()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    return-object v0
.end method

.method private static final ooooo0O(Lkotlin/jvm/functions/Function0;)V
    .locals 2

    .line 1
    const-string v0, "$afterCheck"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 7
    .line 8
    const-string v1, "DbWaitingListener doc all checked"

    .line 9
    .line 10
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-interface {p0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 14
    .line 15
    .line 16
    return-void
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final oo〇O0o〇()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O〇o88o08〇:Ljava/lang/String;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->ooo〇8oO()Ljava/lang/String;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O0〇o8o〇〇(Ljava/lang/String;)V

    .line 10
    .line 11
    .line 12
    const-string v1, "getTempPhotoPathForSyste\u2026toFilePath = it\n        }"

    .line 13
    .line 14
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    :cond_0
    return-object v0
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static synthetic o〇08oO80o(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroidx/activity/result/ActivityResult;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o0o〇〇〇8o(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroidx/activity/result/ActivityResult;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final o〇O80o8OO()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇O〇〇O8:Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;

    .line 2
    .line 3
    sget-object v1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oO〇8O8oOo:[Lkotlin/reflect/KProperty;

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    aget-object v1, v1, v2

    .line 7
    .line 8
    invoke-virtual {v0, p0, v1}, Lcom/intsig/viewbinding/viewbind/ActivityViewBinding;->〇〇888(Landroid/app/Activity;Lkotlin/reflect/KProperty;)Landroidx/viewbinding/ViewBinding;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    check-cast v0, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 13
    .line 14
    return-object v0
    .line 15
.end method

.method static synthetic o〇Oo(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroidx/activity/result/ActivityResult;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p3, p3, 0x2

    .line 2
    .line 3
    if-eqz p3, :cond_0

    .line 4
    .line 5
    const/4 p2, 0x0

    .line 6
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇88(Landroidx/activity/result/ActivityResult;Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
.end method

.method public static final synthetic o〇OoO0(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oo88()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static synthetic o〇o08〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroidx/activity/result/ActivityResult;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O〇0888o(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroidx/activity/result/ActivityResult;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static final synthetic o〇o0oOO8(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;I)I
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O80(I)I

    .line 2
    .line 3
    .line 4
    move-result p0

    .line 5
    return p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final o〇o8〇〇O(I)Lcom/intsig/camscanner/datastruct/ParcelDocInfo;
    .locals 3

    .line 1
    new-instance v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;

    .line 2
    .line 3
    invoke-direct {v0}, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;-><init>()V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oo0O()J

    .line 7
    .line 8
    .line 9
    move-result-wide v1

    .line 10
    iput-wide v1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->o0:J

    .line 11
    .line 12
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o〇oO08〇o0()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    iput-object v1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->OO:Ljava/lang/String;

    .line 17
    .line 18
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->OO〇80oO〇()Z

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    iput-boolean v1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->〇08O〇00〇o:Z

    .line 23
    .line 24
    iput p1, v0, Lcom/intsig/camscanner/datastruct/ParcelDocInfo;->oOo0:I

    .line 25
    .line 26
    return-object v0
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private final o〇oO08〇o0()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O88O:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "mDocItem"

    .line 6
    .line 7
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->Oo8Oo00oo()Ljava/lang/String;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    return-object v0
.end method

.method private final o〇〇8〇〇()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇080:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0o〇o()Ljava/lang/String;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->OO0〇O()I

    .line 8
    .line 9
    .line 10
    move-result v2

    .line 11
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇O00(Ljava/lang/String;I)V

    .line 12
    .line 13
    .line 14
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 15
    .line 16
    const-string v1, "go2SaveToGallery"

    .line 17
    .line 18
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 19
    .line 20
    .line 21
    const/4 v0, 0x0

    .line 22
    invoke-static {v0}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇0080o(Z)V

    .line 23
    .line 24
    .line 25
    new-instance v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$go2SaveDocToGallery$1;

    .line 26
    .line 27
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$go2SaveDocToGallery$1;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 28
    .line 29
    .line 30
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O〇oo8O80(Lkotlin/jvm/functions/Function0;)V

    .line 31
    .line 32
    .line 33
    return-void
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private static final 〇00o80oo(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-interface {p0}, Landroid/content/DialogInterface;->dismiss()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static final synthetic 〇00o〇O8(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Lkotlin/jvm/functions/Function1;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0〇8o〇(Lkotlin/jvm/functions/Function1;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final 〇00〇〇〇o〇8()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/camscanner/scenariodir/data/DetailValue;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oo8ooo8O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Ljava/util/ArrayList;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private final 〇0888()V
    .locals 3

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/ipo/IPOCheck;->o800o8O()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O〇00o08()V

    .line 8
    .line 9
    .line 10
    goto :goto_0

    .line 11
    :cond_0
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailShowPersonalInfoDialog;->o〇00O:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailShowPersonalInfoDialog$Companion;

    .line 12
    .line 13
    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    const-string/jumbo v2, "supportFragmentManager"

    .line 18
    .line 19
    .line 20
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    new-instance v2, LO〇O800oo/〇oo〇;

    .line 24
    .line 25
    invoke-direct {v2, p0}, LO〇O800oo/〇oo〇;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailShowPersonalInfoDialog$Companion;->〇o00〇〇Oo(Landroidx/fragment/app/FragmentManager;Ljava/lang/Runnable;)V

    .line 29
    .line 30
    .line 31
    :goto_0
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static final synthetic 〇0O8Oo(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o8oOOo:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private static final 〇0o(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V
    .locals 3

    .line 1
    const-string/jumbo v0, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O8〇()Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailViewModel;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oo0O()J

    .line 12
    .line 13
    .line 14
    move-result-wide v1

    .line 15
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailViewModel;->O8ooOoo〇(J)V

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static final synthetic 〇0o0oO〇〇0(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0888()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final 〇0o88O()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O08o()Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;->Oooo8o0〇()Landroidx/lifecycle/MutableLiveData;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    invoke-virtual {v0}, Landroidx/lifecycle/LiveData;->getValue()Ljava/lang/Object;

    .line 10
    .line 11
    .line 12
    move-result-object v0

    .line 13
    check-cast v0, Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;

    .line 14
    .line 15
    new-instance v1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$go2BatchDoc$1;

    .line 16
    .line 17
    invoke-direct {v1, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$go2BatchDoc$1;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 18
    .line 19
    .line 20
    invoke-static {v0, v1}, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker;->〇o00〇〇Oo(Lcom/intsig/camscanner/data/dao/ShareDirDao$PermissionAndCreator;Lkotlin/jvm/functions/Function0;)V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static synthetic 〇0o88Oo〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇oO〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final 〇0oO(Landroid/content/Intent;)V
    .locals 3

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 4
    .line 5
    const-string v0, "onGalleryImportResult data == null"

    .line 6
    .line 7
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    return-void

    .line 11
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    if-eqz v0, :cond_1

    .line 16
    .line 17
    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    .line 18
    .line 19
    .line 20
    move-result-object p1

    .line 21
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oo0O()J

    .line 22
    .line 23
    .line 24
    move-result-wide v1

    .line 25
    invoke-static {p1, v1, v2}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇o00〇〇Oo(Landroid/content/Context;J)Z

    .line 26
    .line 27
    .line 28
    move-result p1

    .line 29
    if-eqz p1, :cond_5

    .line 30
    .line 31
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇o88〇O(Landroid/net/Uri;)V

    .line 32
    .line 33
    .line 34
    goto :goto_2

    .line 35
    :cond_1
    invoke-static {p1}, Lcom/intsig/camscanner/app/IntentUtil;->〇O8o08O(Landroid/content/Intent;)Ljava/util/ArrayList;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    if-eqz p1, :cond_3

    .line 40
    .line 41
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    .line 42
    .line 43
    .line 44
    move-result v0

    .line 45
    if-eqz v0, :cond_2

    .line 46
    .line 47
    goto :goto_0

    .line 48
    :cond_2
    const/4 v0, 0x0

    .line 49
    goto :goto_1

    .line 50
    :cond_3
    :goto_0
    const/4 v0, 0x1

    .line 51
    :goto_1
    if-eqz v0, :cond_4

    .line 52
    .line 53
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 54
    .line 55
    const-string/jumbo v0, "uris are null"

    .line 56
    .line 57
    .line 58
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .line 60
    .line 61
    goto :goto_2

    .line 62
    :cond_4
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8o80O(Ljava/util/ArrayList;)V

    .line 63
    .line 64
    .line 65
    :cond_5
    :goto_2
    return-void
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private final 〇0o〇o()Ljava/lang/String;
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/logagent/ScenarioLogDirAgent;->〇080:Lcom/intsig/camscanner/scenariodir/logagent/ScenarioLogDirAgent;

    .line 2
    .line 3
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇o〇OoO8()I

    .line 4
    .line 5
    .line 6
    move-result v1

    .line 7
    invoke-virtual {v0, v1}, Lcom/intsig/camscanner/scenariodir/logagent/ScenarioLogDirAgent;->〇O00(I)Ljava/lang/String;

    .line 8
    .line 9
    .line 10
    move-result-object v0

    .line 11
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method static synthetic 〇0〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    const/4 p3, 0x1

    .line 2
    and-int/2addr p2, p3

    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x1

    .line 6
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o8〇O〇0O0〇(Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
.end method

.method private final 〇0〇8o〇(Lkotlin/jvm/functions/Function1;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lkotlin/jvm/functions/Function1<",
            "-",
            "Landroid/view/View;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->O8o08O8O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 6
    .line 7
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    .line 8
    .line 9
    .line 10
    move-result v0

    .line 11
    const/4 v1, 0x0

    .line 12
    const/4 v2, 0x0

    .line 13
    :goto_0
    if-ge v2, v0, :cond_1

    .line 14
    .line 15
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 16
    .line 17
    .line 18
    move-result-object v3

    .line 19
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->O8o08O8O:Landroidx/constraintlayout/widget/ConstraintLayout;

    .line 20
    .line 21
    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 22
    .line 23
    .line 24
    move-result-object v3

    .line 25
    const-string v4, "childView"

    .line 26
    .line 27
    invoke-static {v3, v4}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 28
    .line 29
    .line 30
    invoke-interface {p1, v3}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    .line 32
    .line 33
    move-result-object v4

    .line 34
    check-cast v4, Ljava/lang/Boolean;

    .line 35
    .line 36
    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    .line 37
    .line 38
    .line 39
    move-result v4

    .line 40
    if-eqz v4, :cond_0

    .line 41
    .line 42
    const/4 v4, 0x0

    .line 43
    goto :goto_1

    .line 44
    :cond_0
    const/16 v4, 0x8

    .line 45
    .line 46
    :goto_1
    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 47
    .line 48
    .line 49
    add-int/lit8 v2, v2, 0x1

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_1
    return-void
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private final 〇0〇o8〇()Lcom/intsig/camscanner/capture/certificates/model/CertificateMoreCapture;
    .locals 6

    .line 1
    invoke-static {}, Lcom/intsig/camscanner/capture/certificates/util/CertificateMoreDataUtil;->〇080()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const-string v1, "certificateChildModels"

    .line 6
    .line 7
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    check-cast v0, Ljava/lang/Iterable;

    .line 11
    .line 12
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 17
    .line 18
    .line 19
    move-result v1

    .line 20
    const/4 v2, 0x0

    .line 21
    if-eqz v1, :cond_4

    .line 22
    .line 23
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v1

    .line 27
    check-cast v1, Lcom/intsig/camscanner/capture/certificates/data/CertificateMoreModel;

    .line 28
    .line 29
    iget-object v1, v1, Lcom/intsig/camscanner/capture/certificates/data/CertificateMoreModel;->〇OOo8〇0:Ljava/util/List;

    .line 30
    .line 31
    const-string v3, "certificateMoreModel.modelList"

    .line 32
    .line 33
    invoke-static {v1, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    .line 35
    .line 36
    check-cast v1, Ljava/lang/Iterable;

    .line 37
    .line 38
    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 39
    .line 40
    .line 41
    move-result-object v1

    .line 42
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 43
    .line 44
    .line 45
    move-result v3

    .line 46
    if-eqz v3, :cond_3

    .line 47
    .line 48
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 49
    .line 50
    .line 51
    move-result-object v3

    .line 52
    move-object v4, v3

    .line 53
    check-cast v4, Lcom/intsig/camscanner/capture/certificates/data/CertificateMoreItemModel;

    .line 54
    .line 55
    iget v4, v4, Lcom/intsig/camscanner/capture/certificates/data/CertificateMoreItemModel;->OO:I

    .line 56
    .line 57
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇o〇OoO8()I

    .line 58
    .line 59
    .line 60
    move-result v5

    .line 61
    if-ne v4, v5, :cond_2

    .line 62
    .line 63
    const/4 v4, 0x1

    .line 64
    goto :goto_0

    .line 65
    :cond_2
    const/4 v4, 0x0

    .line 66
    :goto_0
    if-eqz v4, :cond_1

    .line 67
    .line 68
    move-object v2, v3

    .line 69
    :cond_3
    check-cast v2, Lcom/intsig/camscanner/capture/certificates/data/CertificateMoreItemModel;

    .line 70
    .line 71
    if-eqz v2, :cond_0

    .line 72
    .line 73
    new-instance v0, Lcom/intsig/camscanner/capture/certificates/model/CertificateMoreCapture;

    .line 74
    .line 75
    invoke-direct {v0, v2}, Lcom/intsig/camscanner/capture/certificates/model/CertificateMoreCapture;-><init>(Lcom/intsig/camscanner/capture/certificates/data/CertificateMoreItemModel;)V

    .line 76
    .line 77
    .line 78
    return-object v0

    .line 79
    :cond_4
    return-object v2
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public static final synthetic 〇80O(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇OO0oO()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static final synthetic 〇80〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oooo800〇〇()Ljava/lang/String;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final 〇88(Landroidx/activity/result/ActivityResult;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->OOOo〇(Landroidx/activity/result/ActivityResult;)V

    .line 2
    .line 3
    .line 4
    if-eqz p2, :cond_1

    .line 5
    .line 6
    invoke-virtual {p1}, Landroidx/activity/result/ActivityResult;->getResultCode()I

    .line 7
    .line 8
    .line 9
    move-result p1

    .line 10
    const/4 p2, -0x1

    .line 11
    if-ne p1, p2, :cond_1

    .line 12
    .line 13
    iget-object p1, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oOO〇〇:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailAdapter;

    .line 14
    .line 15
    if-nez p1, :cond_0

    .line 16
    .line 17
    const-string p1, "mAdapter"

    .line 18
    .line 19
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 20
    .line 21
    .line 22
    const/4 p1, 0x0

    .line 23
    :cond_0
    new-instance p2, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$onCsCameraResult$1;

    .line 24
    .line 25
    invoke-direct {p2, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$onCsCameraResult$1;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 26
    .line 27
    .line 28
    invoke-virtual {p1, p2}, Landroidx/recyclerview/widget/RecyclerView$Adapter;->registerAdapterDataObserver(Landroidx/recyclerview/widget/RecyclerView$AdapterDataObserver;)V

    .line 29
    .line 30
    .line 31
    :cond_1
    return-void
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final 〇8O(Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;Z)V
    .locals 11

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "inflate from net: "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    const/4 v1, 0x0

    .line 24
    if-eqz p1, :cond_0

    .line 25
    .line 26
    invoke-virtual {p1}, Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;->getCloud_cert_cate_code()Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v2

    .line 30
    goto :goto_0

    .line 31
    :cond_0
    move-object v2, v1

    .line 32
    :goto_0
    if-eqz v2, :cond_2

    .line 33
    .line 34
    invoke-static {v2}, Lkotlin/text/StringsKt;->〇O8o08O(Ljava/lang/String;)Ljava/lang/Integer;

    .line 35
    .line 36
    .line 37
    move-result-object v2

    .line 38
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇o〇OoO8()I

    .line 39
    .line 40
    .line 41
    move-result v3

    .line 42
    if-nez v2, :cond_1

    .line 43
    .line 44
    goto :goto_1

    .line 45
    :cond_1
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    .line 46
    .line 47
    .line 48
    move-result v2

    .line 49
    if-eq v2, v3, :cond_2

    .line 50
    .line 51
    :goto_1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oO800o()V

    .line 52
    .line 53
    .line 54
    return-void

    .line 55
    :cond_2
    if-eqz p1, :cond_3

    .line 56
    .line 57
    invoke-virtual {p1}, Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;->getCert_detail()Ljava/util/List;

    .line 58
    .line 59
    .line 60
    move-result-object v2

    .line 61
    goto :goto_2

    .line 62
    :cond_3
    move-object v2, v1

    .line 63
    :goto_2
    move-object v3, v2

    .line 64
    check-cast v3, Ljava/util/Collection;

    .line 65
    .line 66
    const/4 v4, 0x0

    .line 67
    if-eqz v3, :cond_5

    .line 68
    .line 69
    invoke-interface {v3}, Ljava/util/Collection;->isEmpty()Z

    .line 70
    .line 71
    .line 72
    move-result v3

    .line 73
    if-eqz v3, :cond_4

    .line 74
    .line 75
    goto :goto_3

    .line 76
    :cond_4
    const/4 v3, 0x0

    .line 77
    goto :goto_4

    .line 78
    :cond_5
    :goto_3
    const/4 v3, 0x1

    .line 79
    :goto_4
    if-eqz v3, :cond_6

    .line 80
    .line 81
    const-string p1, "cert_detail == null"

    .line 82
    .line 83
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    .line 85
    .line 86
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->ooo0〇080()V

    .line 87
    .line 88
    .line 89
    return-void

    .line 90
    :cond_6
    invoke-direct {p0, v2}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O008oO0(Ljava/util/List;)Z

    .line 91
    .line 92
    .line 93
    move-result v2

    .line 94
    if-nez v2, :cond_7

    .line 95
    .line 96
    const-string p1, "card ocr result is empty"

    .line 97
    .line 98
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    .line 100
    .line 101
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->ooo0〇080()V

    .line 102
    .line 103
    .line 104
    return-void

    .line 105
    :cond_7
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇00〇〇〇o〇8()Ljava/util/ArrayList;

    .line 106
    .line 107
    .line 108
    move-result-object v0

    .line 109
    invoke-virtual {p1, v0}, Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;->setCert_detail(Ljava/util/List;)V

    .line 110
    .line 111
    .line 112
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O88O:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 113
    .line 114
    if-nez v0, :cond_8

    .line 115
    .line 116
    const-string v0, "mDocItem"

    .line 117
    .line 118
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 119
    .line 120
    .line 121
    move-object v0, v1

    .line 122
    :cond_8
    invoke-virtual {v0, p1}, Lcom/intsig/camscanner/datastruct/DocItem;->Ooo(Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;)V

    .line 123
    .line 124
    .line 125
    invoke-virtual {p1}, Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;->getCert_title()Ljava/lang/String;

    .line 126
    .line 127
    .line 128
    move-result-object v0

    .line 129
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇80()Z

    .line 130
    .line 131
    .line 132
    move-result v2

    .line 133
    invoke-direct {p0, v0, v2, v4}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇〇8088(Ljava/lang/String;ZZ)V

    .line 134
    .line 135
    .line 136
    if-eqz p2, :cond_9

    .line 137
    .line 138
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 139
    .line 140
    .line 141
    move-result-object v5

    .line 142
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 143
    .line 144
    .line 145
    move-result-object v6

    .line 146
    const/4 v7, 0x0

    .line 147
    new-instance v8, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$checkWhenInflateCardInfoParamsView$1;

    .line 148
    .line 149
    invoke-direct {v8, p0, p1, v1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$checkWhenInflateCardInfoParamsView$1;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;Lkotlin/coroutines/Continuation;)V

    .line 150
    .line 151
    .line 152
    const/4 v9, 0x2

    .line 153
    const/4 v10, 0x0

    .line 154
    invoke-static/range {v5 .. v10}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 155
    .line 156
    .line 157
    :cond_9
    return-void
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

.method private final 〇8Oo8〇8(IJJ)V
    .locals 14

    .line 1
    move-object v9, p0

    .line 2
    iget-object v0, v9, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oOO〇〇:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailAdapter;

    .line 3
    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    const-string v0, "mAdapter"

    .line 7
    .line 8
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    const/4 v0, 0x0

    .line 12
    :cond_0
    const v1, 0x7f0a0e43

    .line 13
    .line 14
    .line 15
    move v4, p1

    .line 16
    invoke-virtual {v0, p1, v1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->o〇O(II)Landroid/view/View;

    .line 17
    .line 18
    .line 19
    move-result-object v7

    .line 20
    if-nez v7, :cond_1

    .line 21
    .line 22
    return-void

    .line 23
    :cond_1
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 24
    .line 25
    .line 26
    move-result-object v10

    .line 27
    const/4 v11, 0x0

    .line 28
    const/4 v12, 0x0

    .line 29
    new-instance v13, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$viewPages$1;

    .line 30
    .line 31
    const/4 v8, 0x0

    .line 32
    move-object v0, v13

    .line 33
    move-wide/from16 v1, p4

    .line 34
    .line 35
    move-object v3, p0

    .line 36
    move v4, p1

    .line 37
    move-wide/from16 v5, p2

    .line 38
    .line 39
    invoke-direct/range {v0 .. v8}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$viewPages$1;-><init>(JLcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;IJLandroid/view/View;Lkotlin/coroutines/Continuation;)V

    .line 40
    .line 41
    .line 42
    const/4 v4, 0x3

    .line 43
    const/4 v5, 0x0

    .line 44
    move-object v0, v10

    .line 45
    move-object v1, v11

    .line 46
    move-object v2, v12

    .line 47
    move-object v3, v13

    .line 48
    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 49
    .line 50
    .line 51
    return-void
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public static final synthetic 〇8o0o0(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O0O:Lcom/intsig/camscanner/databaseManager/LifecycleDataChangerManager;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final 〇8o80O(Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .line 1
    if-nez p1, :cond_0

    .line 2
    .line 3
    return-void

    .line 4
    :cond_0
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oo0O()J

    .line 7
    .line 8
    .line 9
    move-result-wide v2

    .line 10
    const-wide/16 v4, -0x1

    .line 11
    .line 12
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o〇oO08〇o0()Ljava/lang/String;

    .line 13
    .line 14
    .line 15
    move-result-object v6

    .line 16
    const/4 v7, 0x0

    .line 17
    const/4 v8, 0x0

    .line 18
    const/4 v9, 0x0

    .line 19
    move-object v1, p1

    .line 20
    invoke-static/range {v0 .. v9}, Lcom/intsig/camscanner/BatchModeActivity;->o88o88(Landroid/content/Context;Ljava/util/ArrayList;JJLjava/lang/String;Ljava/lang/String;ZZ)Landroid/content/Intent;

    .line 21
    .line 22
    .line 23
    move-result-object p1

    .line 24
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->Oo80:Landroidx/activity/result/ActivityResultLauncher;

    .line 25
    .line 26
    invoke-virtual {v0, p1}, Landroidx/activity/result/ActivityResultLauncher;->launch(Ljava/lang/Object;)V

    .line 27
    .line 28
    .line 29
    return-void
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private final 〇8oo0oO0(Landroid/content/Intent;Landroid/net/Uri;Ljava/lang/String;Z)V
    .locals 9

    .line 1
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    if-nez v0, :cond_0

    .line 6
    .line 7
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 8
    .line 9
    const-string p2, "data.getData() == null"

    .line 10
    .line 11
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 16
    .line 17
    .line 18
    move-result-wide v1

    .line 19
    new-instance v8, Lcom/intsig/camscanner/datastruct/PageProperty;

    .line 20
    .line 21
    invoke-direct {v8}, Lcom/intsig/camscanner/datastruct/PageProperty;-><init>()V

    .line 22
    .line 23
    .line 24
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v0

    .line 28
    const-string v3, "raw_path"

    .line 29
    .line 30
    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 31
    .line 32
    .line 33
    move-result-object v3

    .line 34
    const-string v4, "extra_thumb_path"

    .line 35
    .line 36
    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 37
    .line 38
    .line 39
    move-result-object v4

    .line 40
    invoke-static {v4}, Lcom/intsig/utils/FileUtil;->〇0000OOO(Ljava/lang/String;)Z

    .line 41
    .line 42
    .line 43
    move-result v5

    .line 44
    if-nez v5, :cond_1

    .line 45
    .line 46
    invoke-static {v0}, Lcom/intsig/camscanner/bitmap/BitmapUtils;->〇o(Ljava/lang/String;)Ljava/lang/String;

    .line 47
    .line 48
    .line 49
    move-result-object v4

    .line 50
    :cond_1
    iput-object v3, v8, Lcom/intsig/camscanner/datastruct/PageProperty;->OO:Ljava/lang/String;

    .line 51
    .line 52
    iput-object v0, v8, Lcom/intsig/camscanner/datastruct/PageProperty;->〇OOo8〇0:Ljava/lang/String;

    .line 53
    .line 54
    iput-object v4, v8, Lcom/intsig/camscanner/datastruct/PageProperty;->〇08O〇00〇o:Ljava/lang/String;

    .line 55
    .line 56
    invoke-static {p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    .line 57
    .line 58
    .line 59
    move-result-wide v3

    .line 60
    iput-wide v3, v8, Lcom/intsig/camscanner/datastruct/PageProperty;->o0:J

    .line 61
    .line 62
    invoke-static {p1, v8}, Lcom/intsig/camscanner/app/DBUtil;->〇o0O0O8(Landroid/content/Intent;Lcom/intsig/camscanner/datastruct/PageProperty;)Lcom/intsig/camscanner/datastruct/PageProperty;

    .line 63
    .line 64
    .line 65
    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    .line 66
    .line 67
    .line 68
    move-result-object p1

    .line 69
    iget-wide v3, v8, Lcom/intsig/camscanner/datastruct/PageProperty;->o0:J

    .line 70
    .line 71
    invoke-static {p1, v3, v4}, Lcom/intsig/camscanner/db/dao/DocumentDao;->〇〇8O0〇8(Landroid/content/Context;J)I

    .line 72
    .line 73
    .line 74
    move-result p1

    .line 75
    add-int/lit8 p1, p1, 0x1

    .line 76
    .line 77
    iput p1, v8, Lcom/intsig/camscanner/datastruct/PageProperty;->o〇00O:I

    .line 78
    .line 79
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O8〇()Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailViewModel;

    .line 80
    .line 81
    .line 82
    move-result-object v3

    .line 83
    iget v7, v8, Lcom/intsig/camscanner/datastruct/PageProperty;->o〇00O:I

    .line 84
    .line 85
    move-object v4, p2

    .line 86
    move-object v5, p3

    .line 87
    move v6, p4

    .line 88
    invoke-virtual/range {v3 .. v8}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailViewModel;->Oooo8o0〇(Landroid/net/Uri;Ljava/lang/String;ZILcom/intsig/camscanner/datastruct/PageProperty;)V

    .line 89
    .line 90
    .line 91
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 92
    .line 93
    .line 94
    move-result-wide p1

    .line 95
    sub-long/2addr p1, v1

    .line 96
    sget-object p3, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 97
    .line 98
    new-instance p4, Ljava/lang/StringBuilder;

    .line 99
    .line 100
    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    .line 101
    .line 102
    .line 103
    const-string v0, "appendOnePage consume "

    .line 104
    .line 105
    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 106
    .line 107
    .line 108
    invoke-virtual {p4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 112
    .line 113
    .line 114
    move-result-object p1

    .line 115
    invoke-static {p3, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    .line 117
    .line 118
    return-void
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
.end method

.method public static final synthetic 〇8oo8888(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o0〇OO008O()V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private static final 〇8ooOO(Lkotlin/jvm/functions/Function0;I)V
    .locals 0

    .line 1
    const-string p1, "$afterCheck"

    .line 2
    .line 3
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0}, Lkotlin/jvm/functions/Function0;->invoke()Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final 〇8ooo()V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 2
    .line 3
    const-string v1, "checkInitFillInPromptDialog"

    .line 4
    .line 5
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    iget-boolean v1, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇o0O:Z

    .line 9
    .line 10
    if-nez v1, :cond_0

    .line 11
    .line 12
    const-string v1, "from existed doc click, not from capture scene"

    .line 13
    .line 14
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    return-void

    .line 18
    :cond_0
    invoke-static {}, Lcom/intsig/camscanner/mainmenu/folder/PreferenceFolderHelper;->o〇0()Z

    .line 19
    .line 20
    .line 21
    move-result v1

    .line 22
    if-nez v1, :cond_1

    .line 23
    .line 24
    const-string v1, "not in this gray"

    .line 25
    .line 26
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    return-void

    .line 30
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/dialog/CardDetailFillPromptDialog;->OO:Lcom/intsig/camscanner/scenariodir/dialog/CardDetailFillPromptDialog$Companion;

    .line 31
    .line 32
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/dialog/CardDetailFillPromptDialog$Companion;->〇o00〇〇Oo()Lcom/intsig/camscanner/scenariodir/dialog/CardDetailFillPromptDialog;

    .line 33
    .line 34
    .line 35
    move-result-object v1

    .line 36
    new-instance v2, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$checkInitFillInPromptDialog$dialog$1$1;

    .line 37
    .line 38
    invoke-direct {v2, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$checkInitFillInPromptDialog$dialog$1$1;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 39
    .line 40
    .line 41
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/scenariodir/dialog/CardDetailFillPromptDialog;->〇〇o0〇8(Lcom/intsig/camscanner/scenariodir/dialog/CardDetailFillPromptDialog$CardDetailFillCallback;)V

    .line 42
    .line 43
    .line 44
    :try_start_0
    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 45
    .line 46
    .line 47
    move-result-object v2

    .line 48
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/dialog/CardDetailFillPromptDialog$Companion;->〇080()Ljava/lang/String;

    .line 49
    .line 50
    .line 51
    move-result-object v0

    .line 52
    invoke-virtual {v1, v2, v0}, Landroidx/fragment/app/DialogFragment;->show(Landroidx/fragment/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    .line 54
    .line 55
    goto :goto_0

    .line 56
    :catch_0
    move-exception v0

    .line 57
    sget-object v1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 58
    .line 59
    invoke-static {v1, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 60
    .line 61
    .line 62
    :goto_0
    return-void
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public static final synthetic 〇8oo〇〇oO(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Lcom/intsig/utils/CsResult;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oOO〇0o8〇(Lcom/intsig/utils/CsResult;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private static final 〇8〇0O〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V
    .locals 1

    .line 1
    const-string/jumbo v0, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O〇00o08()V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final 〇8〇8o00(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    .line 2
    .line 3
    .line 4
    move-result v0

    .line 5
    if-eqz v0, :cond_0

    .line 6
    .line 7
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 8
    .line 9
    const-string v0, "pageIds isEmpty"

    .line 10
    .line 11
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    return-void

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 16
    .line 17
    new-instance v1, LO〇O800oo/o〇O8〇〇o;

    .line 18
    .line 19
    invoke-direct {v1, p0, p1}, LO〇O800oo/o〇O8〇〇o;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Ljava/util/ArrayList;)V

    .line 20
    .line 21
    .line 22
    invoke-static {v0, v1}, Lcom/intsig/camscanner/batch/BatchImageReeditActivity;->〇ooO〇000(Landroid/content/Context;Ljava/lang/Runnable;)V

    .line 23
    .line 24
    .line 25
    return-void
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method private final 〇8〇o〇OoO8()I
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O88O:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "mDocItem"

    .line 6
    .line 7
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->〇o()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    return v0
.end method

.method public static final synthetic 〇8〇〇8o(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailAdapter;
    .locals 0

    .line 1
    iget-object p0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oOO〇〇:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailAdapter;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private final 〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o〇O80o8OO()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->Oo08(Ljava/lang/Object;)V

    .line 6
    .line 7
    .line 8
    return-object v0
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method private final 〇O8〇〇o8〇()V
    .locals 10

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    const v1, 0x7f060092

    .line 4
    .line 5
    .line 6
    invoke-static {v0, v1}, Landroidx/core/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    .line 7
    .line 8
    .line 9
    move-result v1

    .line 10
    const/4 v2, 0x0

    .line 11
    invoke-static {v0, v2, v2, v1}, Lcom/intsig/utils/StatusBarUtil;->〇o00〇〇Oo(Landroid/app/Activity;ZZI)V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->oOO〇〇:Landroidx/appcompat/widget/Toolbar;

    .line 19
    .line 20
    invoke-virtual {p0, v0}, Landroidx/appcompat/app/AppCompatActivity;->setSupportActionBar(Landroidx/appcompat/widget/Toolbar;)V

    .line 21
    .line 22
    .line 23
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getSupportActionBar()Landroidx/appcompat/app/ActionBar;

    .line 24
    .line 25
    .line 26
    move-result-object v0

    .line 27
    if-eqz v0, :cond_0

    .line 28
    .line 29
    const/4 v1, 0x1

    .line 30
    invoke-virtual {v0, v1}, Landroidx/appcompat/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 31
    .line 32
    .line 33
    :cond_0
    invoke-virtual {p0}, Landroidx/appcompat/app/AppCompatActivity;->getSupportActionBar()Landroidx/appcompat/app/ActionBar;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    if-eqz v0, :cond_1

    .line 38
    .line 39
    invoke-virtual {v0, v2}, Landroidx/appcompat/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 40
    .line 41
    .line 42
    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    .line 43
    .line 44
    .line 45
    move-result-object v0

    .line 46
    const-string/jumbo v1, "window"

    .line 47
    .line 48
    .line 49
    invoke-static {v0, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    const/high16 v1, 0xc000000

    .line 53
    .line 54
    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 55
    .line 56
    .line 57
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    .line 58
    .line 59
    .line 60
    move-result-object v1

    .line 61
    const/16 v3, 0x500

    .line 62
    .line 63
    invoke-virtual {v1, v3}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 64
    .line 65
    .line 66
    const/high16 v1, -0x80000000

    .line 67
    .line 68
    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 69
    .line 70
    .line 71
    invoke-virtual {v0, v2}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 72
    .line 73
    .line 74
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 75
    .line 76
    .line 77
    move-result-object v0

    .line 78
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->o8o:Landroidx/appcompat/widget/AppCompatTextView;

    .line 79
    .line 80
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 81
    .line 82
    .line 83
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 84
    .line 85
    .line 86
    move-result-object v0

    .line 87
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->〇00O0:Landroid/widget/TextView;

    .line 88
    .line 89
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    .line 91
    .line 92
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 93
    .line 94
    .line 95
    move-result-object v0

    .line 96
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->oo8ooo8O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 97
    .line 98
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    .line 100
    .line 101
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 102
    .line 103
    .line 104
    move-result-object v0

    .line 105
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->oO〇8O8oOo:Landroidx/appcompat/widget/AppCompatTextView;

    .line 106
    .line 107
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    .line 109
    .line 110
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 111
    .line 112
    .line 113
    move-result-object v0

    .line 114
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->o8oOOo:Landroidx/appcompat/widget/AppCompatImageView;

    .line 115
    .line 116
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    .line 118
    .line 119
    new-instance v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailAdapter;

    .line 120
    .line 121
    invoke-direct {v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailAdapter;-><init>()V

    .line 122
    .line 123
    .line 124
    iput-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oOO〇〇:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailAdapter;

    .line 125
    .line 126
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 127
    .line 128
    .line 129
    move-result-object v0

    .line 130
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->O88O:Landroidx/recyclerview/widget/RecyclerView;

    .line 131
    .line 132
    new-instance v9, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/FooterLinearLayoutManager;

    .line 133
    .line 134
    iget-object v2, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 135
    .line 136
    const-string v1, "mActivity"

    .line 137
    .line 138
    invoke-static {v2, v1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    .line 140
    .line 141
    const/4 v3, 0x1

    .line 142
    const/4 v4, 0x0

    .line 143
    sget-object v5, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$initView$1$1;->o0:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$initView$1$1;

    .line 144
    .line 145
    const/4 v6, 0x0

    .line 146
    const/16 v7, 0x10

    .line 147
    .line 148
    const/4 v8, 0x0

    .line 149
    move-object v1, v9

    .line 150
    invoke-direct/range {v1 .. v8}, Lcom/intsig/camscanner/pagelist/newpagelist/adapter/FooterLinearLayoutManager;-><init>(Landroid/content/Context;IZLkotlin/jvm/functions/Function1;ZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 151
    .line 152
    .line 153
    invoke-virtual {v0, v9}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 154
    .line 155
    .line 156
    iget-object v1, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oOO〇〇:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailAdapter;

    .line 157
    .line 158
    const/4 v2, 0x0

    .line 159
    const-string v3, "mAdapter"

    .line 160
    .line 161
    if-nez v1, :cond_2

    .line 162
    .line 163
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 164
    .line 165
    .line 166
    move-object v1, v2

    .line 167
    :cond_2
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 168
    .line 169
    .line 170
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oOO〇〇:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailAdapter;

    .line 171
    .line 172
    if-nez v0, :cond_3

    .line 173
    .line 174
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 175
    .line 176
    .line 177
    goto :goto_0

    .line 178
    :cond_3
    move-object v2, v0

    .line 179
    :goto_0
    new-instance v0, LO〇O800oo/〇〇808〇;

    .line 180
    .line 181
    invoke-direct {v0, p0}, LO〇O800oo/〇〇808〇;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 182
    .line 183
    .line 184
    invoke-virtual {v2, v0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O880oOO08(Lcom/chad/library/adapter/base/listener/OnItemClickListener;)V

    .line 185
    .line 186
    .line 187
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->OO00〇0o〇〇()V

    .line 188
    .line 189
    .line 190
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o008()V

    .line 191
    .line 192
    .line 193
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8ooo()V

    .line 194
    .line 195
    .line 196
    return-void
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private final 〇OO0oO()V
    .locals 2

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 2
    .line 3
    const-string/jumbo v1, "showNetError"

    .line 4
    .line 5
    .line 6
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    new-instance v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$showNetError$1;

    .line 10
    .line 11
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$showNetError$1;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0〇8o〇(Lkotlin/jvm/functions/Function1;)V

    .line 15
    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->Oo0〇Ooo:Landroid/widget/TextView;

    .line 22
    .line 23
    const v1, 0x7f130026

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 27
    .line 28
    .line 29
    move-result-object v1

    .line 30
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 31
    .line 32
    .line 33
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 34
    .line 35
    .line 36
    move-result-object v0

    .line 37
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->〇〇〇0o〇〇0:Landroid/widget/TextView;

    .line 38
    .line 39
    const v1, 0x7f130f55

    .line 40
    .line 41
    .line 42
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 43
    .line 44
    .line 45
    move-result-object v1

    .line 46
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    .line 48
    .line 49
    return-void
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public static synthetic 〇OoO0o0(Landroid/content/DialogInterface;I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇00o80oo(Landroid/content/DialogInterface;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private static final 〇OoOO〇(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 1

    .line 1
    const-string v0, "$tmp0"

    .line 2
    .line 3
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-interface {p0, p1}, Lkotlin/jvm/functions/Function1;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final 〇O〇〇〇()V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    new-array v2, v1, [Ljava/lang/Long;

    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oo0O()J

    .line 7
    .line 8
    .line 9
    move-result-wide v3

    .line 10
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 11
    .line 12
    .line 13
    move-result-object v3

    .line 14
    const/4 v4, 0x0

    .line 15
    aput-object v3, v2, v4

    .line 16
    .line 17
    invoke-static {v2}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 18
    .line 19
    .line 20
    move-result-object v2

    .line 21
    new-instance v3, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$go2ShareDoc$1;

    .line 22
    .line 23
    invoke-direct {v3}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$go2ShareDoc$1;-><init>()V

    .line 24
    .line 25
    .line 26
    new-instance v4, LO〇O800oo/〇〇8O0〇8;

    .line 27
    .line 28
    invoke-direct {v4, p0}, LO〇O800oo/〇〇8O0〇8;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 29
    .line 30
    .line 31
    invoke-static {v0, v2, v1, v3, v4}, Lcom/intsig/camscanner/share/ShareHelper;->Oo〇(Landroidx/fragment/app/FragmentActivity;Ljava/util/ArrayList;ZLcom/intsig/camscanner/share/listener/SharePreviousInterceptor;Lcom/intsig/camscanner/share/listener/ShareBackListener;)V

    .line 32
    .line 33
    .line 34
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final 〇o88〇O(Landroid/net/Uri;)V
    .locals 4

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "go2ImageScan uri = "

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 17
    .line 18
    .line 19
    move-result-object v1

    .line 20
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    .line 22
    .line 23
    if-nez p1, :cond_0

    .line 24
    .line 25
    return-void

    .line 26
    :cond_0
    new-instance v0, Landroid/content/Intent;

    .line 27
    .line 28
    iget-object v1, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 29
    .line 30
    const-class v2, Lcom/intsig/camscanner/ImageScannerActivity;

    .line 31
    .line 32
    const-string v3, "com.intsig.camscanner.NEW_PAGE"

    .line 33
    .line 34
    invoke-direct {v0, v3, p1, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 35
    .line 36
    .line 37
    const-string p1, "scanner_image_src"

    .line 38
    .line 39
    const/4 v1, 0x1

    .line 40
    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 41
    .line 42
    .line 43
    iget-object p1, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇〇o〇:Landroidx/activity/result/ActivityResultLauncher;

    .line 44
    .line 45
    invoke-virtual {p1, v0}, Landroidx/activity/result/ActivityResultLauncher;->launch(Ljava/lang/Object;)V

    .line 46
    .line 47
    .line 48
    return-void
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method static synthetic 〇o8〇8(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p2, p2, 0x1

    .line 2
    .line 3
    if-eqz p2, :cond_0

    .line 4
    .line 5
    const/4 p1, 0x0

    .line 6
    :cond_0
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o8o0(Z)V

    .line 7
    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
.end method

.method public static synthetic 〇oO88o(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroidx/activity/result/ActivityResult;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->OoOO〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroidx/activity/result/ActivityResult;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static synthetic 〇oOO80o(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O0〇O80ooo(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method private static final 〇oO〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V
    .locals 2

    .line 1
    const-string/jumbo v0, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 8
    .line 9
    new-instance v1, LO〇O800oo/〇O00;

    .line 10
    .line 11
    invoke-direct {v1, p0}, LO〇O800oo/〇O00;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 12
    .line 13
    .line 14
    invoke-static {v0, v1}, Lcom/intsig/camscanner/share/ShareSuccessDialog;->〇〇o0〇8(Landroidx/fragment/app/FragmentActivity;Lcom/intsig/camscanner/share/ShareSuccessDialog$ShareContinue;)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static synthetic 〇ooO8Ooo〇(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o0〇(Lkotlin/jvm/functions/Function1;Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static synthetic 〇ooO〇000(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇0O〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static final synthetic 〇o〇88(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)J
    .locals 2

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oo0O()J

    .line 2
    .line 3
    .line 4
    move-result-wide v0

    .line 5
    return-wide v0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static final synthetic 〇o〇OO80oO(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8O(Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;Z)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method private final 〇〇0(JLkotlin/coroutines/Continuation;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lkotlin/coroutines/Continuation<",
            "-",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    instance-of v0, p3, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$checkDocExist$1;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    move-object v0, p3

    .line 6
    check-cast v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$checkDocExist$1;

    .line 7
    .line 8
    iget v1, v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$checkDocExist$1;->OO:I

    .line 9
    .line 10
    const/high16 v2, -0x80000000

    .line 11
    .line 12
    and-int v3, v1, v2

    .line 13
    .line 14
    if-eqz v3, :cond_0

    .line 15
    .line 16
    sub-int/2addr v1, v2

    .line 17
    iput v1, v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$checkDocExist$1;->OO:I

    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    new-instance v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$checkDocExist$1;

    .line 21
    .line 22
    invoke-direct {v0, p0, p3}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$checkDocExist$1;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Lkotlin/coroutines/Continuation;)V

    .line 23
    .line 24
    .line 25
    :goto_0
    iget-object p3, v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$checkDocExist$1;->o0:Ljava/lang/Object;

    .line 26
    .line 27
    invoke-static {}, Lkotlin/coroutines/intrinsics/IntrinsicsKt;->O8()Ljava/lang/Object;

    .line 28
    .line 29
    .line 30
    move-result-object v1

    .line 31
    iget v2, v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$checkDocExist$1;->OO:I

    .line 32
    .line 33
    const/4 v3, 0x1

    .line 34
    if-eqz v2, :cond_2

    .line 35
    .line 36
    if-ne v2, v3, :cond_1

    .line 37
    .line 38
    invoke-static {p3}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 39
    .line 40
    .line 41
    goto :goto_1

    .line 42
    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    .line 43
    .line 44
    const-string p2, "call to \'resume\' before \'invoke\' with coroutine"

    .line 45
    .line 46
    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    .line 47
    .line 48
    .line 49
    throw p1

    .line 50
    :cond_2
    invoke-static {p3}, Lkotlin/ResultKt;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 51
    .line 52
    .line 53
    invoke-static {}, Lkotlinx/coroutines/Dispatchers;->〇o00〇〇Oo()Lkotlinx/coroutines/CoroutineDispatcher;

    .line 54
    .line 55
    .line 56
    move-result-object p3

    .line 57
    new-instance v2, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$checkDocExist$docExist$1;

    .line 58
    .line 59
    const/4 v4, 0x0

    .line 60
    invoke-direct {v2, p0, p1, p2, v4}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$checkDocExist$docExist$1;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;JLkotlin/coroutines/Continuation;)V

    .line 61
    .line 62
    .line 63
    iput v3, v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$checkDocExist$1;->OO:I

    .line 64
    .line 65
    invoke-static {p3, v2, v0}, Lkotlinx/coroutines/BuildersKt;->Oo08(Lkotlin/coroutines/CoroutineContext;Lkotlin/jvm/functions/Function2;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;

    .line 66
    .line 67
    .line 68
    move-result-object p3

    .line 69
    if-ne p3, v1, :cond_3

    .line 70
    .line 71
    return-object v1

    .line 72
    :cond_3
    :goto_1
    check-cast p3, Ljava/lang/Boolean;

    .line 73
    .line 74
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    .line 75
    .line 76
    .line 77
    move-result p1

    .line 78
    if-nez p1, :cond_4

    .line 79
    .line 80
    sget-object p2, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 81
    .line 82
    const-string p3, "doc not exist"

    .line 83
    .line 84
    invoke-static {p2, p3}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    .line 86
    .line 87
    :cond_4
    invoke-static {p1}, Lkotlin/coroutines/jvm/internal/Boxing;->〇080(Z)Ljava/lang/Boolean;

    .line 88
    .line 89
    .line 90
    move-result-object p1

    .line 91
    return-object p1
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

.method private static final 〇〇08〇0oo0(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Landroidx/activity/result/ActivityResult;)V
    .locals 0

    .line 1
    const-string/jumbo p1, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O80〇()V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final 〇〇8088(Ljava/lang/String;ZZ)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NotifyDataSetChanged"
        }
    .end annotation

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 2
    .line 3
    const-string/jumbo v1, "showCardParamsView"

    .line 4
    .line 5
    .line 6
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 7
    .line 8
    .line 9
    new-instance v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$showCardParamsView$1;

    .line 10
    .line 11
    invoke-direct {v0, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$showCardParamsView$1;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0, v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0〇8o〇(Lkotlin/jvm/functions/Function1;)V

    .line 15
    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇00〇〇〇o〇8()Ljava/util/ArrayList;

    .line 18
    .line 19
    .line 20
    move-result-object v0

    .line 21
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 22
    .line 23
    .line 24
    move-result-object v0

    .line 25
    const/4 v1, 0x0

    .line 26
    const/4 v2, 0x0

    .line 27
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 28
    .line 29
    .line 30
    move-result v3

    .line 31
    if-eqz v3, :cond_7

    .line 32
    .line 33
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object v3

    .line 37
    check-cast v3, Lcom/intsig/camscanner/scenariodir/data/DetailValue;

    .line 38
    .line 39
    invoke-virtual {v3}, Lcom/intsig/camscanner/scenariodir/data/DetailValue;->getKey()Ljava/lang/String;

    .line 40
    .line 41
    .line 42
    move-result-object v4

    .line 43
    const-string v5, "foregin_card_user_card_name"

    .line 44
    .line 45
    invoke-static {v4, v5}, Lkotlin/jvm/internal/Intrinsics;->〇o〇(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 46
    .line 47
    .line 48
    move-result v4

    .line 49
    if-eqz v4, :cond_0

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_0
    invoke-virtual {v3}, Lcom/intsig/camscanner/scenariodir/data/DetailValue;->getDescription()Ljava/lang/String;

    .line 53
    .line 54
    .line 55
    move-result-object v4

    .line 56
    invoke-virtual {v3}, Lcom/intsig/camscanner/scenariodir/data/DetailValue;->getValue()Ljava/lang/String;

    .line 57
    .line 58
    .line 59
    move-result-object v5

    .line 60
    const/4 v6, 0x1

    .line 61
    if-eqz v5, :cond_2

    .line 62
    .line 63
    invoke-static {v5}, Lkotlin/text/StringsKt;->〇O888o0o(Ljava/lang/CharSequence;)Z

    .line 64
    .line 65
    .line 66
    move-result v5

    .line 67
    if-eqz v5, :cond_1

    .line 68
    .line 69
    goto :goto_1

    .line 70
    :cond_1
    const/4 v5, 0x0

    .line 71
    goto :goto_2

    .line 72
    :cond_2
    :goto_1
    const/4 v5, 0x1

    .line 73
    :goto_2
    if-eqz v5, :cond_3

    .line 74
    .line 75
    const-string v3, "-"

    .line 76
    .line 77
    goto :goto_3

    .line 78
    :cond_3
    invoke-virtual {v3}, Lcom/intsig/camscanner/scenariodir/data/DetailValue;->getValue()Ljava/lang/String;

    .line 79
    .line 80
    .line 81
    move-result-object v3

    .line 82
    :goto_3
    if-eqz v2, :cond_6

    .line 83
    .line 84
    if-eq v2, v6, :cond_5

    .line 85
    .line 86
    const/4 v5, 0x2

    .line 87
    if-eq v2, v5, :cond_4

    .line 88
    .line 89
    goto/16 :goto_4

    .line 90
    .line 91
    :cond_4
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 92
    .line 93
    .line 94
    move-result-object v5

    .line 95
    iget-object v5, v5, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->OO〇00〇8oO:Landroidx/constraintlayout/widget/Group;

    .line 96
    .line 97
    const-string v6, "mViewBinding.groupLine3"

    .line 98
    .line 99
    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 100
    .line 101
    .line 102
    invoke-virtual {v5, v1}, Landroid/view/View;->setVisibility(I)V

    .line 103
    .line 104
    .line 105
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 106
    .line 107
    .line 108
    move-result-object v5

    .line 109
    iget-object v5, v5, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->Ooo08:Landroidx/appcompat/widget/AppCompatTextView;

    .line 110
    .line 111
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    .line 113
    .line 114
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 115
    .line 116
    .line 117
    move-result-object v4

    .line 118
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->〇OO〇00〇0O:Landroidx/appcompat/widget/AppCompatTextView;

    .line 119
    .line 120
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    .line 122
    .line 123
    iput-object v3, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇OO〇00〇0O:Ljava/lang/String;

    .line 124
    .line 125
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 126
    .line 127
    .line 128
    move-result-object v4

    .line 129
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->O〇o88o08〇:Landroid/widget/TextView;

    .line 130
    .line 131
    invoke-virtual {v4, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 132
    .line 133
    .line 134
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 135
    .line 136
    .line 137
    move-result-object v3

    .line 138
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->O〇o88o08〇:Landroid/widget/TextView;

    .line 139
    .line 140
    iget-object v4, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o8o:Landroid/view/View$OnClickListener;

    .line 141
    .line 142
    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    .line 144
    .line 145
    goto :goto_4

    .line 146
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 147
    .line 148
    .line 149
    move-result-object v5

    .line 150
    iget-object v5, v5, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->oOo0:Landroidx/constraintlayout/widget/Group;

    .line 151
    .line 152
    const-string v6, "mViewBinding.groupLine2"

    .line 153
    .line 154
    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 155
    .line 156
    .line 157
    invoke-virtual {v5, v1}, Landroid/view/View;->setVisibility(I)V

    .line 158
    .line 159
    .line 160
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 161
    .line 162
    .line 163
    move-result-object v5

    .line 164
    iget-object v5, v5, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->o8〇OO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 165
    .line 166
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 167
    .line 168
    .line 169
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 170
    .line 171
    .line 172
    move-result-object v4

    .line 173
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->ooO:Landroidx/appcompat/widget/AppCompatTextView;

    .line 174
    .line 175
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    .line 177
    .line 178
    iput-object v3, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->ooO:Ljava/lang/String;

    .line 179
    .line 180
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 181
    .line 182
    .line 183
    move-result-object v4

    .line 184
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->Oo80:Landroid/widget/TextView;

    .line 185
    .line 186
    invoke-virtual {v4, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 187
    .line 188
    .line 189
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 190
    .line 191
    .line 192
    move-result-object v3

    .line 193
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->Oo80:Landroid/widget/TextView;

    .line 194
    .line 195
    iget-object v4, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o8o:Landroid/view/View$OnClickListener;

    .line 196
    .line 197
    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 198
    .line 199
    .line 200
    goto :goto_4

    .line 201
    :cond_6
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 202
    .line 203
    .line 204
    move-result-object v5

    .line 205
    iget-object v5, v5, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->oOo〇8o008:Landroidx/constraintlayout/widget/Group;

    .line 206
    .line 207
    const-string v6, "mViewBinding.groupLine1"

    .line 208
    .line 209
    invoke-static {v5, v6}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 210
    .line 211
    .line 212
    invoke-virtual {v5, v1}, Landroid/view/View;->setVisibility(I)V

    .line 213
    .line 214
    .line 215
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 216
    .line 217
    .line 218
    move-result-object v5

    .line 219
    iget-object v5, v5, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->O〇08oOOO0:Landroidx/appcompat/widget/AppCompatTextView;

    .line 220
    .line 221
    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 222
    .line 223
    .line 224
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 225
    .line 226
    .line 227
    move-result-object v4

    .line 228
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->〇OO8ooO8〇:Landroidx/appcompat/widget/AppCompatTextView;

    .line 229
    .line 230
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    .line 232
    .line 233
    iput-object v3, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇OO8ooO8〇:Ljava/lang/String;

    .line 234
    .line 235
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 236
    .line 237
    .line 238
    move-result-object v4

    .line 239
    iget-object v4, v4, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->〇〇o〇:Landroid/widget/TextView;

    .line 240
    .line 241
    invoke-virtual {v4, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 242
    .line 243
    .line 244
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇〇8〇8()Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;

    .line 245
    .line 246
    .line 247
    move-result-object v3

    .line 248
    iget-object v3, v3, Lcom/intsig/camscanner/databinding/ActivityScenariodirCardDetailBinding;->〇〇o〇:Landroid/widget/TextView;

    .line 249
    .line 250
    iget-object v4, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o8o:Landroid/view/View$OnClickListener;

    .line 251
    .line 252
    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 253
    .line 254
    .line 255
    :goto_4
    add-int/lit8 v2, v2, 0x1

    .line 256
    .line 257
    goto/16 :goto_0

    .line 258
    .line 259
    :cond_7
    iput-object p1, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->Ooo08:Ljava/lang/String;

    .line 260
    .line 261
    invoke-direct {p0, p2, p3}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oO8o〇o〇8(ZZ)V

    .line 262
    .line 263
    .line 264
    return-void
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
.end method

.method private final 〇〇80o〇o0()V
    .locals 6

    .line 1
    invoke-static {p0}, Landroidx/lifecycle/LifecycleOwnerKt;->getLifecycleScope(Landroidx/lifecycle/LifecycleOwner;)Landroidx/lifecycle/LifecycleCoroutineScope;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    const/4 v1, 0x0

    .line 6
    const/4 v2, 0x0

    .line 7
    new-instance v3, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$recordToRecentDocs$1;

    .line 8
    .line 9
    const/4 v4, 0x0

    .line 10
    invoke-direct {v3, p0, v4}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$recordToRecentDocs$1;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Lkotlin/coroutines/Continuation;)V

    .line 11
    .line 12
    .line 13
    const/4 v4, 0x3

    .line 14
    const/4 v5, 0x0

    .line 15
    invoke-static/range {v0 .. v5}, Lkotlinx/coroutines/BuildersKt;->O8(Lkotlinx/coroutines/CoroutineScope;Lkotlin/coroutines/CoroutineContext;Lkotlinx/coroutines/CoroutineStart;Lkotlin/jvm/functions/Function2;ILjava/lang/Object;)Lkotlinx/coroutines/Job;

    .line 16
    .line 17
    .line 18
    return-void
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method private final 〇〇8o0OOOo()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oOO〇〇:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailAdapter;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    const-string v0, "mAdapter"

    .line 6
    .line 7
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 8
    .line 9
    .line 10
    const/4 v0, 0x0

    .line 11
    :cond_0
    invoke-virtual {v0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O8〇o()Ljava/util/List;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    new-instance v1, Ljava/util/ArrayList;

    .line 16
    .line 17
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 18
    .line 19
    .line 20
    check-cast v0, Ljava/lang/Iterable;

    .line 21
    .line 22
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    .line 23
    .line 24
    .line 25
    move-result-object v0

    .line 26
    const/4 v2, 0x0

    .line 27
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    .line 28
    .line 29
    .line 30
    move-result v3

    .line 31
    if-eqz v3, :cond_3

    .line 32
    .line 33
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 34
    .line 35
    .line 36
    move-result-object v3

    .line 37
    add-int/lit8 v4, v2, 0x1

    .line 38
    .line 39
    if-gez v2, :cond_1

    .line 40
    .line 41
    invoke-static {}, Lkotlin/collections/CollectionsKt;->〇〇8O0〇8()V

    .line 42
    .line 43
    .line 44
    :cond_1
    check-cast v3, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;

    .line 45
    .line 46
    instance-of v2, v3, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 47
    .line 48
    if-eqz v2, :cond_2

    .line 49
    .line 50
    check-cast v3, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 51
    .line 52
    invoke-virtual {v3}, Lcom/intsig/camscanner/pagelist/model/PageImageItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/pagelist/model/PageItem;

    .line 53
    .line 54
    .line 55
    move-result-object v2

    .line 56
    iget-wide v2, v2, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇080:J

    .line 57
    .line 58
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 59
    .line 60
    .line 61
    move-result-object v2

    .line 62
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    .line 64
    .line 65
    :cond_2
    move v2, v4

    .line 66
    goto :goto_0

    .line 67
    :cond_3
    return-object v1
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method public static synthetic 〇〇O(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ZIIZILjava/lang/Object;)V
    .locals 9

    .line 1
    and-int/lit8 v0, p8, 0x4

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    sget-object v0, Lcom/intsig/camscanner/capture/SupportCaptureModeOption;->VALUE_SUPPORT_MODE_ALL:Lcom/intsig/camscanner/capture/SupportCaptureModeOption;

    .line 6
    .line 7
    move-object v4, v0

    .line 8
    goto :goto_0

    .line 9
    :cond_0
    move-object v4, p3

    .line 10
    :goto_0
    and-int/lit8 v0, p8, 0x8

    .line 11
    .line 12
    if-eqz v0, :cond_1

    .line 13
    .line 14
    const/4 v0, 0x0

    .line 15
    const/4 v5, 0x0

    .line 16
    goto :goto_1

    .line 17
    :cond_1
    move v5, p4

    .line 18
    :goto_1
    and-int/lit8 v0, p8, 0x10

    .line 19
    .line 20
    const/4 v1, -0x1

    .line 21
    if-eqz v0, :cond_2

    .line 22
    .line 23
    const/4 v6, -0x1

    .line 24
    goto :goto_2

    .line 25
    :cond_2
    move v6, p5

    .line 26
    :goto_2
    and-int/lit8 v0, p8, 0x20

    .line 27
    .line 28
    if-eqz v0, :cond_3

    .line 29
    .line 30
    const/4 v7, -0x1

    .line 31
    goto :goto_3

    .line 32
    :cond_3
    move v7, p6

    .line 33
    :goto_3
    move-object v1, p0

    .line 34
    move-object v2, p1

    .line 35
    move-object v3, p2

    .line 36
    move/from16 v8, p7

    .line 37
    .line 38
    invoke-virtual/range {v1 .. v8}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇80O80O〇0(Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ZIIZ)V

    .line 39
    .line 40
    .line 41
    return-void
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
.end method

.method public static final synthetic 〇〇〇OOO〇〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)Ljava/util/List;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇〇8o0OOOo()Ljava/util/List;

    .line 2
    .line 3
    .line 4
    move-result-object p0

    .line 5
    return-object p0
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method


# virtual methods
.method public final O08o()Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇〇08O:Lkotlin/Lazy;

    .line 2
    .line 3
    invoke-interface {v0}, Lkotlin/Lazy;->getValue()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, Lcom/intsig/camscanner/mainmenu/ShareRoleChecker$PermissionAndCreatorViewModel;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final O8oO0(IZ)V
    .locals 11

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O88O:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    if-nez v0, :cond_0

    .line 5
    .line 6
    const-string v0, "mDocItem"

    .line 7
    .line 8
    invoke-static {v0}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    move-object v0, v1

    .line 12
    :cond_0
    invoke-virtual {v0}, Lcom/intsig/camscanner/datastruct/DocItem;->O8〇o()J

    .line 13
    .line 14
    .line 15
    move-result-wide v2

    .line 16
    const-wide/16 v4, 0x0

    .line 17
    .line 18
    cmp-long v0, v2, v4

    .line 19
    .line 20
    if-gtz v0, :cond_1

    .line 21
    .line 22
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 23
    .line 24
    const-string p2, "onMenuItemClick illegal docId, return"

    .line 25
    .line 26
    invoke-static {p1, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    return-void

    .line 30
    :cond_1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 31
    .line 32
    new-instance v2, Ljava/lang/StringBuilder;

    .line 33
    .line 34
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 35
    .line 36
    .line 37
    const-string v3, "click menuId == "

    .line 38
    .line 39
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 40
    .line 41
    .line 42
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 43
    .line 44
    .line 45
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 46
    .line 47
    .line 48
    move-result-object v2

    .line 49
    invoke-static {v0, v2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .line 51
    .line 52
    if-eqz p1, :cond_7

    .line 53
    .line 54
    const/4 v2, 0x1

    .line 55
    if-eq p1, v2, :cond_6

    .line 56
    .line 57
    const/4 v3, 0x2

    .line 58
    if-eq p1, v3, :cond_4

    .line 59
    .line 60
    const/4 p2, 0x3

    .line 61
    const/4 v0, 0x0

    .line 62
    if-eq p1, p2, :cond_3

    .line 63
    .line 64
    const/4 p2, 0x4

    .line 65
    if-eq p1, p2, :cond_2

    .line 66
    .line 67
    goto :goto_0

    .line 68
    :cond_2
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇080:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;

    .line 69
    .line 70
    invoke-virtual {p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇o〇()V

    .line 71
    .line 72
    .line 73
    sget-object v3, Lcom/intsig/camscanner/scenariodir/DocManualOperations;->〇080:Lcom/intsig/camscanner/scenariodir/DocManualOperations;

    .line 74
    .line 75
    iget-object v4, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 76
    .line 77
    const-string p1, "mActivity"

    .line 78
    .line 79
    invoke-static {v4, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    .line 81
    .line 82
    new-array p1, v2, [Ljava/lang/Long;

    .line 83
    .line 84
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oo0O()J

    .line 85
    .line 86
    .line 87
    move-result-wide v1

    .line 88
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 89
    .line 90
    .line 91
    move-result-object p2

    .line 92
    aput-object p2, p1, v0

    .line 93
    .line 94
    invoke-static {p1}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 95
    .line 96
    .line 97
    move-result-object v5

    .line 98
    const/4 v6, 0x0

    .line 99
    const/4 v7, 0x0

    .line 100
    new-instance v8, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$onMenuItemClick$1;

    .line 101
    .line 102
    invoke-direct {v8, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$onMenuItemClick$1;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 103
    .line 104
    .line 105
    const/16 v9, 0x8

    .line 106
    .line 107
    const/4 v10, 0x0

    .line 108
    invoke-static/range {v3 .. v10}, Lcom/intsig/camscanner/scenariodir/DocManualOperations;->oo88o8O(Lcom/intsig/camscanner/scenariodir/DocManualOperations;Landroid/app/Activity;Ljava/util/ArrayList;ZZLkotlin/jvm/functions/Function0;ILjava/lang/Object;)V

    .line 109
    .line 110
    .line 111
    goto :goto_0

    .line 112
    :cond_3
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇080:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;

    .line 113
    .line 114
    invoke-virtual {p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->o〇0()V

    .line 115
    .line 116
    .line 117
    invoke-static {p0, v0, v2, v1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇o8〇8(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;ZILjava/lang/Object;)V

    .line 118
    .line 119
    .line 120
    goto :goto_0

    .line 121
    :cond_4
    new-instance p1, Ljava/lang/StringBuilder;

    .line 122
    .line 123
    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    .line 124
    .line 125
    .line 126
    const-string v1, "click edit enabled == "

    .line 127
    .line 128
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    .line 130
    .line 131
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 132
    .line 133
    .line 134
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 135
    .line 136
    .line 137
    move-result-object p1

    .line 138
    invoke-static {v0, p1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    .line 140
    .line 141
    if-nez p2, :cond_5

    .line 142
    .line 143
    return-void

    .line 144
    :cond_5
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0888()V

    .line 145
    .line 146
    .line 147
    goto :goto_0

    .line 148
    :cond_6
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇080:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;

    .line 149
    .line 150
    invoke-virtual {p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇〇8O0〇8()V

    .line 151
    .line 152
    .line 153
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O〇88()V

    .line 154
    .line 155
    .line 156
    goto :goto_0

    .line 157
    :cond_7
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇080:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;

    .line 158
    .line 159
    invoke-virtual {p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->OoO8()V

    .line 160
    .line 161
    .line 162
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o〇〇8〇〇()V

    .line 163
    .line 164
    .line 165
    :goto_0
    return-void
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
.end method

.method public final O〇00o08()V
    .locals 8

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇080:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;

    .line 2
    .line 3
    invoke-virtual {v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->Oo08()V

    .line 4
    .line 5
    .line 6
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->Oo0〇Ooo:Landroidx/activity/result/ActivityResultLauncher;

    .line 7
    .line 8
    sget-object v1, Lcom/intsig/camscanner/scenariodir/cardpack/EditCardDetailInfoViewModel;->〇〇o〇:Lcom/intsig/camscanner/scenariodir/cardpack/EditCardDetailInfoViewModel$Companion;

    .line 9
    .line 10
    iget-object v2, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 11
    .line 12
    const-string v3, "mActivity"

    .line 13
    .line 14
    invoke-static {v2, v3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 15
    .line 16
    .line 17
    iget-object v3, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O88O:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 18
    .line 19
    if-nez v3, :cond_0

    .line 20
    .line 21
    const-string v3, "mDocItem"

    .line 22
    .line 23
    invoke-static {v3}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 24
    .line 25
    .line 26
    const/4 v3, 0x0

    .line 27
    :cond_0
    invoke-virtual {v3}, Lcom/intsig/camscanner/datastruct/DocItem;->OO0o〇〇〇〇0()Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;

    .line 28
    .line 29
    .line 30
    move-result-object v3

    .line 31
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇00〇〇〇o〇8()Ljava/util/ArrayList;

    .line 32
    .line 33
    .line 34
    move-result-object v4

    .line 35
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇8〇o〇OoO8()I

    .line 36
    .line 37
    .line 38
    move-result v5

    .line 39
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oo0O()J

    .line 40
    .line 41
    .line 42
    move-result-wide v6

    .line 43
    invoke-virtual/range {v1 .. v7}, Lcom/intsig/camscanner/scenariodir/cardpack/EditCardDetailInfoViewModel$Companion;->〇080(Landroidx/appcompat/app/AppCompatActivity;Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;Ljava/util/ArrayList;IJ)Landroid/content/Intent;

    .line 44
    .line 45
    .line 46
    move-result-object v1

    .line 47
    invoke-virtual {v0, v1}, Landroidx/activity/result/ActivityResultLauncher;->launch(Ljava/lang/Object;)V

    .line 48
    .line 49
    .line 50
    return-void
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public dealClickAction(Landroid/view/View;)V
    .locals 6

    .line 1
    invoke-super {p0, p1}, Lcom/intsig/mvp/activity/BaseChangeActivity;->dealClickAction(Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    if-eqz p1, :cond_0

    .line 6
    .line 7
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    .line 8
    .line 9
    .line 10
    move-result p1

    .line 11
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    goto :goto_0

    .line 16
    :cond_0
    move-object p1, v0

    .line 17
    :goto_0
    if-nez p1, :cond_1

    .line 18
    .line 19
    goto :goto_1

    .line 20
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 21
    .line 22
    .line 23
    move-result v1

    .line 24
    const v2, 0x7f0a1271

    .line 25
    .line 26
    .line 27
    if-ne v1, v2, :cond_2

    .line 28
    .line 29
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇080:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;

    .line 30
    .line 31
    invoke-virtual {p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->oO80()V

    .line 32
    .line 33
    .line 34
    new-instance p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$dealClickAction$1;

    .line 35
    .line 36
    invoke-direct {p1, p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$dealClickAction$1;-><init>(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;)V

    .line 37
    .line 38
    .line 39
    invoke-direct {p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O888Oo(Lkotlin/jvm/functions/Function0;)V

    .line 40
    .line 41
    .line 42
    goto/16 :goto_b

    .line 43
    .line 44
    :cond_2
    :goto_1
    if-nez p1, :cond_3

    .line 45
    .line 46
    goto :goto_2

    .line 47
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 48
    .line 49
    .line 50
    move-result v1

    .line 51
    const v2, 0x7f0a1291

    .line 52
    .line 53
    .line 54
    if-ne v1, v2, :cond_4

    .line 55
    .line 56
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇080:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;

    .line 57
    .line 58
    invoke-virtual {p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->O8()V

    .line 59
    .line 60
    .line 61
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0o88O()V

    .line 62
    .line 63
    .line 64
    goto/16 :goto_b

    .line 65
    .line 66
    :cond_4
    :goto_2
    if-nez p1, :cond_5

    .line 67
    .line 68
    goto :goto_3

    .line 69
    :cond_5
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 70
    .line 71
    .line 72
    move-result v1

    .line 73
    const v2, 0x7f0a17b7

    .line 74
    .line 75
    .line 76
    if-ne v1, v2, :cond_6

    .line 77
    .line 78
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇080:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;

    .line 79
    .line 80
    invoke-virtual {p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇〇888()V

    .line 81
    .line 82
    .line 83
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇O〇〇〇()V

    .line 84
    .line 85
    .line 86
    goto/16 :goto_b

    .line 87
    .line 88
    :cond_6
    :goto_3
    if-nez p1, :cond_7

    .line 89
    .line 90
    goto :goto_4

    .line 91
    :cond_7
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 92
    .line 93
    .line 94
    move-result v1

    .line 95
    const v2, 0x7f0a14a8

    .line 96
    .line 97
    .line 98
    if-ne v1, v2, :cond_8

    .line 99
    .line 100
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇080:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;

    .line 101
    .line 102
    invoke-virtual {p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇80〇808〇O()V

    .line 103
    .line 104
    .line 105
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0o〇o()Ljava/lang/String;

    .line 106
    .line 107
    .line 108
    move-result-object v0

    .line 109
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->OO0〇O()I

    .line 110
    .line 111
    .line 112
    move-result v1

    .line 113
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->oo88o8O(Ljava/lang/String;I)V

    .line 114
    .line 115
    .line 116
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oOOO0()V

    .line 117
    .line 118
    .line 119
    goto/16 :goto_b

    .line 120
    .line 121
    :cond_8
    :goto_4
    const/4 v1, 0x1

    .line 122
    const/4 v2, 0x0

    .line 123
    if-nez p1, :cond_9

    .line 124
    .line 125
    goto/16 :goto_9

    .line 126
    .line 127
    :cond_9
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 128
    .line 129
    .line 130
    move-result v3

    .line 131
    const v4, 0x7f0a0942

    .line 132
    .line 133
    .line 134
    if-ne v3, v4, :cond_14

    .line 135
    .line 136
    iget-object p1, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oOO〇〇:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailAdapter;

    .line 137
    .line 138
    if-nez p1, :cond_a

    .line 139
    .line 140
    const-string p1, "mAdapter"

    .line 141
    .line 142
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 143
    .line 144
    .line 145
    move-object p1, v0

    .line 146
    :cond_a
    invoke-virtual {p1}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O8〇o()Ljava/util/List;

    .line 147
    .line 148
    .line 149
    move-result-object p1

    .line 150
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    .line 151
    .line 152
    .line 153
    move-result p1

    .line 154
    if-eqz p1, :cond_b

    .line 155
    .line 156
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 157
    .line 158
    const-string/jumbo v0, "toggle visibility but no image"

    .line 159
    .line 160
    .line 161
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    .line 163
    .line 164
    return-void

    .line 165
    :cond_b
    iget-object p1, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O88O:Lcom/intsig/camscanner/datastruct/DocItem;

    .line 166
    .line 167
    if-nez p1, :cond_c

    .line 168
    .line 169
    const-string p1, "mDocItem"

    .line 170
    .line 171
    invoke-static {p1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 172
    .line 173
    .line 174
    move-object p1, v0

    .line 175
    :cond_c
    invoke-virtual {p1}, Lcom/intsig/camscanner/datastruct/DocItem;->OO0o〇〇〇〇0()Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;

    .line 176
    .line 177
    .line 178
    move-result-object p1

    .line 179
    if-eqz p1, :cond_d

    .line 180
    .line 181
    invoke-virtual {p1}, Lcom/intsig/camscanner/scenariodir/data/CertificateInfo;->getCert_detail()Ljava/util/List;

    .line 182
    .line 183
    .line 184
    move-result-object p1

    .line 185
    goto :goto_5

    .line 186
    :cond_d
    move-object p1, v0

    .line 187
    :goto_5
    check-cast p1, Ljava/util/Collection;

    .line 188
    .line 189
    if-eqz p1, :cond_f

    .line 190
    .line 191
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    .line 192
    .line 193
    .line 194
    move-result p1

    .line 195
    if-eqz p1, :cond_e

    .line 196
    .line 197
    goto :goto_6

    .line 198
    :cond_e
    const/4 p1, 0x0

    .line 199
    goto :goto_7

    .line 200
    :cond_f
    :goto_6
    const/4 p1, 0x1

    .line 201
    :goto_7
    const/4 v3, 0x2

    .line 202
    if-eqz p1, :cond_12

    .line 203
    .line 204
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇080:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;

    .line 205
    .line 206
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0o〇o()Ljava/lang/String;

    .line 207
    .line 208
    .line 209
    move-result-object v4

    .line 210
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->OO0〇O()I

    .line 211
    .line 212
    .line 213
    move-result v5

    .line 214
    invoke-virtual {p1, v4, v5}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->o800o8O(Ljava/lang/String;I)V

    .line 215
    .line 216
    .line 217
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->ooo008()Z

    .line 218
    .line 219
    .line 220
    move-result p1

    .line 221
    if-eqz p1, :cond_11

    .line 222
    .line 223
    invoke-static {}, Lcom/intsig/camscanner/ipo/IPOCheck;->o800o8O()Z

    .line 224
    .line 225
    .line 226
    move-result p1

    .line 227
    if-nez p1, :cond_10

    .line 228
    .line 229
    goto :goto_8

    .line 230
    :cond_10
    invoke-static {p0, v2, v1, v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O0Oo〇8(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;ZILjava/lang/Object;)V

    .line 231
    .line 232
    .line 233
    goto :goto_b

    .line 234
    :cond_11
    :goto_8
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailShowPersonalInfoDialog;->o〇00O:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailShowPersonalInfoDialog$Companion;

    .line 235
    .line 236
    invoke-virtual {p0}, Landroidx/fragment/app/FragmentActivity;->getSupportFragmentManager()Landroidx/fragment/app/FragmentManager;

    .line 237
    .line 238
    .line 239
    move-result-object v1

    .line 240
    const-string/jumbo v2, "supportFragmentManager"

    .line 241
    .line 242
    .line 243
    invoke-static {v1, v2}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullExpressionValue(Ljava/lang/Object;Ljava/lang/String;)V

    .line 244
    .line 245
    .line 246
    invoke-static {p1, v1, v0, v3, v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailShowPersonalInfoDialog$Companion;->〇o〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailShowPersonalInfoDialog$Companion;Landroidx/fragment/app/FragmentManager;Ljava/lang/Runnable;ILjava/lang/Object;)V

    .line 247
    .line 248
    .line 249
    goto :goto_b

    .line 250
    :cond_12
    invoke-static {v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇0o8(Z)V

    .line 251
    .line 252
    .line 253
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇80()Z

    .line 254
    .line 255
    .line 256
    move-result p1

    .line 257
    if-eqz p1, :cond_13

    .line 258
    .line 259
    invoke-static {v2}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇80o(Z)V

    .line 260
    .line 261
    .line 262
    invoke-static {p0, v2, v2, v3, v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->OOO〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;ZZILjava/lang/Object;)V

    .line 263
    .line 264
    .line 265
    goto :goto_b

    .line 266
    :cond_13
    invoke-static {v1}, Lcom/intsig/camscanner/util/PreferenceHelper;->〇80o(Z)V

    .line 267
    .line 268
    .line 269
    invoke-static {p0, v1, v2, v3, v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->OOO〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;ZZILjava/lang/Object;)V

    .line 270
    .line 271
    .line 272
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇080:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;

    .line 273
    .line 274
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0o〇o()Ljava/lang/String;

    .line 275
    .line 276
    .line 277
    move-result-object v0

    .line 278
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->OO0〇O()I

    .line 279
    .line 280
    .line 281
    move-result v1

    .line 282
    invoke-virtual {p1, v0, v1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->o800o8O(Ljava/lang/String;I)V

    .line 283
    .line 284
    .line 285
    goto :goto_b

    .line 286
    :cond_14
    :goto_9
    if-nez p1, :cond_15

    .line 287
    .line 288
    goto :goto_a

    .line 289
    :cond_15
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 290
    .line 291
    .line 292
    move-result v3

    .line 293
    const v4, 0x7f0a1747

    .line 294
    .line 295
    .line 296
    if-ne v3, v4, :cond_16

    .line 297
    .line 298
    invoke-static {p0, v2, v1, v0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0〇(Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;ZILjava/lang/Object;)V

    .line 299
    .line 300
    .line 301
    goto :goto_b

    .line 302
    :cond_16
    :goto_a
    if-nez p1, :cond_17

    .line 303
    .line 304
    goto :goto_b

    .line 305
    :cond_17
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    .line 306
    .line 307
    .line 308
    move-result p1

    .line 309
    const v0, 0x7f0a09ca

    .line 310
    .line 311
    .line 312
    if-ne p1, v0, :cond_18

    .line 313
    .line 314
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o8〇8oooO〇()V

    .line 315
    .line 316
    .line 317
    :cond_18
    :goto_b
    return-void
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
.end method

.method public initialize(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 2
    .line 3
    const-string v0, "initialize"

    .line 4
    .line 5
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 6
    .line 7
    .line 8
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o8o8〇o()V

    .line 9
    .line 10
    .line 11
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇O8〇〇o8〇()V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->O80〇〇o()V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public final o08〇808(Z)V
    .locals 5

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oOO〇〇:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailAdapter;

    .line 2
    .line 3
    const-string v1, "mAdapter"

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    if-nez v0, :cond_0

    .line 7
    .line 8
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 9
    .line 10
    .line 11
    move-object v0, v2

    .line 12
    :cond_0
    invoke-virtual {v0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O8〇o()Ljava/util/List;

    .line 13
    .line 14
    .line 15
    move-result-object v0

    .line 16
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    .line 17
    .line 18
    .line 19
    move-result v0

    .line 20
    if-eqz v0, :cond_1

    .line 21
    .line 22
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 23
    .line 24
    const-string v0, "adapter data is empty"

    .line 25
    .line 26
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    .line 28
    .line 29
    return-void

    .line 30
    :cond_1
    invoke-static {}, Lcom/intsig/camscanner/movecopyactivity/action/OtherMoveInActionKt;->〇080()Lcom/intsig/camscanner/launch/CsApplication;

    .line 31
    .line 32
    .line 33
    move-result-object v0

    .line 34
    invoke-static {v0}, Lcom/intsig/camscanner/util/Util;->O0(Landroid/content/Context;)Z

    .line 35
    .line 36
    .line 37
    move-result v0

    .line 38
    if-nez v0, :cond_2

    .line 39
    .line 40
    new-instance p1, Lcom/intsig/app/AlertDialog$Builder;

    .line 41
    .line 42
    iget-object v0, p0, Lcom/intsig/mvp/activity/BaseChangeActivity;->o8〇OO0〇0o:Lcom/intsig/mvp/activity/BaseChangeActivity;

    .line 43
    .line 44
    invoke-direct {p1, v0}, Lcom/intsig/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 45
    .line 46
    .line 47
    const v0, 0x7f131055

    .line 48
    .line 49
    .line 50
    invoke-virtual {p1, v0}, Lcom/intsig/app/AlertDialog$Builder;->Oo8Oo00oo(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 51
    .line 52
    .line 53
    move-result-object p1

    .line 54
    const v0, 0x7f131053

    .line 55
    .line 56
    .line 57
    invoke-virtual {p1, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇O〇(I)Lcom/intsig/app/AlertDialog$Builder;

    .line 58
    .line 59
    .line 60
    move-result-object p1

    .line 61
    new-instance v0, LO〇O800oo/o〇0;

    .line 62
    .line 63
    invoke-direct {v0}, LO〇O800oo/o〇0;-><init>()V

    .line 64
    .line 65
    .line 66
    const v1, 0x7f130019

    .line 67
    .line 68
    .line 69
    const v2, 0x7f0601ee

    .line 70
    .line 71
    .line 72
    invoke-virtual {p1, v1, v2, v0}, Lcom/intsig/app/AlertDialog$Builder;->〇oOO8O8(IILandroid/content/DialogInterface$OnClickListener;)Lcom/intsig/app/AlertDialog$Builder;

    .line 73
    .line 74
    .line 75
    move-result-object p1

    .line 76
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog$Builder;->〇080()Lcom/intsig/app/AlertDialog;

    .line 77
    .line 78
    .line 79
    move-result-object p1

    .line 80
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->show()V

    .line 81
    .line 82
    .line 83
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇OO0oO()V

    .line 84
    .line 85
    .line 86
    return-void

    .line 87
    :cond_2
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oOO〇〇:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailAdapter;

    .line 88
    .line 89
    if-nez v0, :cond_3

    .line 90
    .line 91
    invoke-static {v1}, Lkotlin/jvm/internal/Intrinsics;->〇oo〇(Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    move-object v0, v2

    .line 95
    :cond_3
    invoke-virtual {v0}, Lcom/chad/library/adapter/base/BaseQuickAdapter;->O8〇o()Ljava/util/List;

    .line 96
    .line 97
    .line 98
    move-result-object v0

    .line 99
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->〇8〇0〇o〇O(Ljava/util/List;)Ljava/lang/Object;

    .line 100
    .line 101
    .line 102
    move-result-object v0

    .line 103
    check-cast v0, Lcom/intsig/camscanner/pagelist/model/PageTypeItem;

    .line 104
    .line 105
    instance-of v1, v0, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 106
    .line 107
    if-eqz v1, :cond_4

    .line 108
    .line 109
    check-cast v0, Lcom/intsig/camscanner/pagelist/model/PageImageItem;

    .line 110
    .line 111
    goto :goto_0

    .line 112
    :cond_4
    move-object v0, v2

    .line 113
    :goto_0
    if-eqz v0, :cond_5

    .line 114
    .line 115
    invoke-virtual {v0}, Lcom/intsig/camscanner/pagelist/model/PageImageItem;->〇o00〇〇Oo()Lcom/intsig/camscanner/pagelist/model/PageItem;

    .line 116
    .line 117
    .line 118
    move-result-object v2

    .line 119
    :cond_5
    if-nez v2, :cond_6

    .line 120
    .line 121
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 122
    .line 123
    const-string/jumbo v0, "startCertOcr first page is not image"

    .line 124
    .line 125
    .line 126
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    .line 128
    .line 129
    return-void

    .line 130
    :cond_6
    const/4 v0, 0x1

    .line 131
    new-array v0, v0, [Ljava/lang/Long;

    .line 132
    .line 133
    iget-wide v3, v2, Lcom/intsig/camscanner/pagelist/model/PageItem;->〇080:J

    .line 134
    .line 135
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    .line 136
    .line 137
    .line 138
    move-result-object v1

    .line 139
    const/4 v3, 0x0

    .line 140
    aput-object v1, v0, v3

    .line 141
    .line 142
    invoke-static {v0}, Lkotlin/collections/CollectionsKt;->Oo08([Ljava/lang/Object;)Ljava/util/ArrayList;

    .line 143
    .line 144
    .line 145
    move-result-object v0

    .line 146
    new-instance v1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$startCertOcr$2;

    .line 147
    .line 148
    invoke-direct {v1, v2, p0, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$startCertOcr$2;-><init>(Lcom/intsig/camscanner/pagelist/model/PageItem;Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;Z)V

    .line 149
    .line 150
    .line 151
    invoke-direct {p0, v0, v1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oO88〇0O8O(Ljava/util/ArrayList;Lkotlin/jvm/functions/Function0;)V

    .line 152
    .line 153
    .line 154
    return-void
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 2
    .line 3
    new-instance v1, Ljava/lang/StringBuilder;

    .line 4
    .line 5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 6
    .line 7
    .line 8
    const-string v2, "onActivityResult requestCode="

    .line 9
    .line 10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 14
    .line 15
    .line 16
    const-string v2, " resultCode="

    .line 17
    .line 18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 19
    .line 20
    .line 21
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 22
    .line 23
    .line 24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 25
    .line 26
    .line 27
    move-result-object v1

    .line 28
    invoke-static {v0, v1}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    .line 30
    .line 31
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 32
    .line 33
    .line 34
    const/16 v1, 0x81

    .line 35
    .line 36
    const/4 v2, -0x1

    .line 37
    if-eq p1, v1, :cond_2

    .line 38
    .line 39
    const/16 v1, 0x82

    .line 40
    .line 41
    if-eq p1, v1, :cond_2

    .line 42
    .line 43
    const/16 v0, 0x3e8

    .line 44
    .line 45
    if-eq p1, v0, :cond_1

    .line 46
    .line 47
    const/16 p2, 0x3ea

    .line 48
    .line 49
    if-eq p1, p2, :cond_0

    .line 50
    .line 51
    goto :goto_0

    .line 52
    :cond_0
    invoke-direct {p0, p3}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0oO(Landroid/content/Intent;)V

    .line 53
    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_1
    if-ne p2, v2, :cond_3

    .line 57
    .line 58
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 59
    .line 60
    .line 61
    goto :goto_0

    .line 62
    :cond_2
    if-ne p2, v2, :cond_3

    .line 63
    .line 64
    if-eqz p3, :cond_3

    .line 65
    .line 66
    const-string/jumbo p1, "targetDirSyncId"

    .line 67
    .line 68
    .line 69
    invoke-virtual {p3, p1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    .line 70
    .line 71
    .line 72
    move-result-object p1

    .line 73
    if-eqz p1, :cond_3

    .line 74
    .line 75
    new-instance p2, Ljava/lang/StringBuilder;

    .line 76
    .line 77
    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    .line 78
    .line 79
    .line 80
    const-string p3, "move copy back, dirSyncId == "

    .line 81
    .line 82
    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    .line 84
    .line 85
    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    .line 87
    .line 88
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 89
    .line 90
    .line 91
    move-result-object p2

    .line 92
    invoke-static {v0, p2}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    .line 94
    .line 95
    new-instance p2, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$CardDetailMoveCopyEvent;

    .line 96
    .line 97
    invoke-direct {p2, p1}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity$CardDetailMoveCopyEvent;-><init>(Ljava/lang/String;)V

    .line 98
    .line 99
    .line 100
    invoke-static {p2}, Lcom/intsig/camscanner/eventbus/CsEventBus;->〇o00〇〇Oo(Ljava/lang/Object;)V

    .line 101
    .line 102
    .line 103
    :cond_3
    :goto_0
    return-void
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroidx/activity/ComponentActivity;->onNewIntent(Landroid/content/Intent;)V

    .line 2
    .line 3
    .line 4
    invoke-virtual {p0, p1}, Landroid/app/Activity;->setIntent(Landroid/content/Intent;)V

    .line 5
    .line 6
    .line 7
    sget-object p1, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0O〇O00O:Ljava/lang/String;

    .line 8
    .line 9
    const-string v0, "onNewIntent"

    .line 10
    .line 11
    invoke-static {p1, v0}, Lcom/intsig/log/LogUtils;->〇080(Ljava/lang/String;Ljava/lang/String;)V

    .line 12
    .line 13
    .line 14
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o8o8〇o()V

    .line 15
    .line 16
    .line 17
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o008()V

    .line 18
    .line 19
    .line 20
    return-void
    .line 21
    .line 22
.end method

.method protected onResume()V
    .locals 3

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/FragmentActivity;->onResume()V

    .line 2
    .line 3
    .line 4
    sget-object v0, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇080:Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;

    .line 5
    .line 6
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->〇0o〇o()Ljava/lang/String;

    .line 7
    .line 8
    .line 9
    move-result-object v1

    .line 10
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->OO0〇O()I

    .line 11
    .line 12
    .line 13
    move-result v2

    .line 14
    invoke-virtual {v0, v1, v2}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailLogAgent;->〇0〇O0088o(Ljava/lang/String;I)V

    .line 15
    .line 16
    .line 17
    return-void
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public bridge synthetic onToolbarTitleClick(Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/mvp/activity/〇o〇;->Oo08(Lcom/intsig/mvp/activity/IToolbar;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public 〇0〇0()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final 〇80O80O〇0(Lcom/intsig/camscanner/capture/CaptureMode;Lcom/intsig/camscanner/purchase/track/FunctionEntrance;Lcom/intsig/camscanner/capture/SupportCaptureModeOption;ZIIZ)V
    .locals 2
    .param p2    # Lcom/intsig/camscanner/purchase/track/FunctionEntrance;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "functionEntrance"

    .line 2
    .line 3
    invoke-static {p2, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    new-instance v0, Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 7
    .line 8
    invoke-direct {v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;-><init>()V

    .line 9
    .line 10
    .line 11
    invoke-virtual {v0, p0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇o(Landroid/app/Activity;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 12
    .line 13
    .line 14
    move-result-object v0

    .line 15
    invoke-virtual {v0, p2}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇O8o08O(Lcom/intsig/camscanner/purchase/track/FunctionEntrance;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 16
    .line 17
    .line 18
    move-result-object p2

    .line 19
    invoke-static {}, Lcom/intsig/camscanner/util/PreferenceHelper;->O08O0〇O()J

    .line 20
    .line 21
    .line 22
    move-result-wide v0

    .line 23
    invoke-virtual {p2, v0, v1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇80〇808〇O(J)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 24
    .line 25
    .line 26
    move-result-object p2

    .line 27
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->o〇oO08〇o0()Ljava/lang/String;

    .line 28
    .line 29
    .line 30
    move-result-object v0

    .line 31
    invoke-virtual {p2, v0}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇00(Ljava/lang/String;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 32
    .line 33
    .line 34
    move-result-object p2

    .line 35
    invoke-virtual {p2, p1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇〇888(Lcom/intsig/camscanner/capture/CaptureMode;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 36
    .line 37
    .line 38
    move-result-object p1

    .line 39
    invoke-virtual {p1, p3}, Lcom/intsig/camscanner/app/StartCameraBuilder;->O8〇o(Lcom/intsig/camscanner/capture/SupportCaptureModeOption;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 40
    .line 41
    .line 42
    move-result-object p1

    .line 43
    invoke-virtual {p1, p4}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇0〇O0088o(Z)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 44
    .line 45
    .line 46
    move-result-object p1

    .line 47
    const/4 p2, 0x1

    .line 48
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/app/StartCameraBuilder;->〇8o8o〇(Z)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 49
    .line 50
    .line 51
    move-result-object p1

    .line 52
    const/16 p2, 0x66

    .line 53
    .line 54
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/app/StartCameraBuilder;->O〇8O8〇008(I)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 55
    .line 56
    .line 57
    move-result-object p1

    .line 58
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/cardpack/CardDetailActivity;->oo〇O0o〇()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object p2

    .line 62
    invoke-virtual {p1, p2}, Lcom/intsig/camscanner/app/StartCameraBuilder;->oo〇(Ljava/lang/String;)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 63
    .line 64
    .line 65
    move-result-object p1

    .line 66
    invoke-virtual {p1, p5}, Lcom/intsig/camscanner/app/StartCameraBuilder;->OO0o〇〇〇〇0(I)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 67
    .line 68
    .line 69
    move-result-object p1

    .line 70
    invoke-virtual {p1, p7}, Lcom/intsig/camscanner/app/StartCameraBuilder;->o〇O8〇〇o(Z)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 71
    .line 72
    .line 73
    move-result-object p1

    .line 74
    invoke-virtual {p1, p6}, Lcom/intsig/camscanner/app/StartCameraBuilder;->O8ooOoo〇(I)Lcom/intsig/camscanner/app/StartCameraBuilder;

    .line 75
    .line 76
    .line 77
    move-result-object p1

    .line 78
    invoke-virtual {p1}, Lcom/intsig/camscanner/app/StartCameraBuilder;->OO0o〇〇()V

    .line 79
    .line 80
    .line 81
    return-void
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
    .line 212
    .line 213
    .line 214
    .line 215
    .line 216
    .line 217
    .line 218
    .line 219
    .line 220
    .line 221
    .line 222
    .line 223
    .line 224
    .line 225
    .line 226
    .line 227
    .line 228
    .line 229
    .line 230
    .line 231
    .line 232
    .line 233
    .line 234
    .line 235
    .line 236
    .line 237
    .line 238
    .line 239
    .line 240
    .line 241
    .line 242
    .line 243
    .line 244
    .line 245
    .line 246
    .line 247
    .line 248
    .line 249
    .line 250
    .line 251
    .line 252
    .line 253
    .line 254
    .line 255
    .line 256
    .line 257
    .line 258
    .line 259
    .line 260
    .line 261
    .line 262
    .line 263
    .line 264
    .line 265
    .line 266
    .line 267
    .line 268
    .line 269
    .line 270
    .line 271
    .line 272
    .line 273
    .line 274
    .line 275
    .line 276
    .line 277
    .line 278
    .line 279
    .line 280
    .line 281
    .line 282
    .line 283
    .line 284
    .line 285
    .line 286
    .line 287
    .line 288
    .line 289
    .line 290
    .line 291
    .line 292
    .line 293
    .line 294
    .line 295
    .line 296
    .line 297
    .line 298
    .line 299
    .line 300
    .line 301
    .line 302
    .line 303
    .line 304
    .line 305
    .line 306
    .line 307
    .line 308
    .line 309
    .line 310
    .line 311
    .line 312
    .line 313
    .line 314
    .line 315
    .line 316
    .line 317
    .line 318
    .line 319
    .line 320
    .line 321
    .line 322
    .line 323
    .line 324
    .line 325
    .line 326
    .line 327
    .line 328
    .line 329
    .line 330
    .line 331
    .line 332
    .line 333
    .line 334
    .line 335
    .line 336
    .line 337
    .line 338
    .line 339
    .line 340
    .line 341
    .line 342
    .line 343
    .line 344
    .line 345
    .line 346
    .line 347
    .line 348
    .line 349
    .line 350
    .line 351
    .line 352
    .line 353
    .line 354
    .line 355
    .line 356
    .line 357
    .line 358
    .line 359
    .line 360
    .line 361
    .line 362
    .line 363
    .line 364
    .line 365
    .line 366
    .line 367
    .line 368
    .line 369
    .line 370
    .line 371
    .line 372
    .line 373
    .line 374
    .line 375
    .line 376
    .line 377
    .line 378
    .line 379
    .line 380
    .line 381
    .line 382
    .line 383
    .line 384
    .line 385
    .line 386
    .line 387
    .line 388
    .line 389
    .line 390
    .line 391
    .line 392
    .line 393
    .line 394
    .line 395
    .line 396
    .line 397
    .line 398
    .line 399
    .line 400
    .line 401
    .line 402
    .line 403
    .line 404
    .line 405
    .line 406
    .line 407
    .line 408
    .line 409
    .line 410
    .line 411
    .line 412
    .line 413
    .line 414
    .line 415
    .line 416
    .line 417
    .line 418
    .line 419
    .line 420
    .line 421
    .line 422
    .line 423
    .line 424
    .line 425
    .line 426
    .line 427
    .line 428
    .line 429
    .line 430
    .line 431
    .line 432
    .line 433
    .line 434
    .line 435
    .line 436
    .line 437
    .line 438
    .line 439
    .line 440
    .line 441
    .line 442
    .line 443
    .line 444
    .line 445
    .line 446
    .line 447
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
.end method

.class public final Lcom/intsig/camscanner/scenariodir/dialog/AddOverseaCertificateDialog;
.super Lcom/intsig/camscanner/scenariodir/dialog/AbsAddCertificateDialog;
.source "AddOverseaCertificateDialog.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/scenariodir/dialog/AddOverseaCertificateDialog$Companion;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field public static final 〇0O:Lcom/intsig/camscanner/scenariodir/dialog/AddOverseaCertificateDialog$Companion;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private O8o08O8O:Z

.field private o〇00O:Lcom/intsig/camscanner/databinding/DialogAddOverseaCertificateBinding;

.field private 〇080OO8〇0:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lcom/intsig/camscanner/scenariodir/dialog/AddOverseaCertificateDialog$Companion;

    .line 2
    .line 3
    const/4 v1, 0x0

    .line 4
    invoke-direct {v0, v1}, Lcom/intsig/camscanner/scenariodir/dialog/AddOverseaCertificateDialog$Companion;-><init>(Lkotlin/jvm/internal/DefaultConstructorMarker;)V

    .line 5
    .line 6
    .line 7
    sput-object v0, Lcom/intsig/camscanner/scenariodir/dialog/AddOverseaCertificateDialog;->〇0O:Lcom/intsig/camscanner/scenariodir/dialog/AddOverseaCertificateDialog$Companion;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/dialog/AbsAddCertificateDialog;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, -0x1

    .line 5
    iput v0, p0, Lcom/intsig/camscanner/scenariodir/dialog/AddOverseaCertificateDialog;->〇080OO8〇0:I

    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public static synthetic O0〇0(Lcom/intsig/camscanner/scenariodir/dialog/AddOverseaCertificateDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lcom/intsig/camscanner/scenariodir/dialog/AddOverseaCertificateDialog;->〇088O(Lcom/intsig/camscanner/scenariodir/dialog/AddOverseaCertificateDialog;Landroid/view/View;)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final o〇0〇o()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/dialog/AddOverseaCertificateDialog;->o〇00O:Lcom/intsig/camscanner/databinding/DialogAddOverseaCertificateBinding;

    .line 2
    .line 3
    if-eqz v0, :cond_2

    .line 4
    .line 5
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogAddOverseaCertificateBinding;->OO:Landroidx/recyclerview/widget/RecyclerView;

    .line 6
    .line 7
    if-eqz v0, :cond_2

    .line 8
    .line 9
    new-instance v1, Lcom/intsig/recycleviewLayoutmanager/TrycatchLinearLayoutManager;

    .line 10
    .line 11
    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 12
    .line 13
    .line 14
    move-result-object v2

    .line 15
    invoke-direct {v1, v2}, Lcom/intsig/recycleviewLayoutmanager/TrycatchLinearLayoutManager;-><init>(Landroid/content/Context;)V

    .line 16
    .line 17
    .line 18
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setLayoutManager(Landroidx/recyclerview/widget/RecyclerView$LayoutManager;)V

    .line 19
    .line 20
    .line 21
    new-instance v1, Lcom/intsig/camscanner/scenariodir/adapter/CertificateSelectAdapter;

    .line 22
    .line 23
    invoke-direct {v1}, Lcom/intsig/camscanner/scenariodir/adapter/CertificateSelectAdapter;-><init>()V

    .line 24
    .line 25
    .line 26
    invoke-virtual {p0, v1}, Lcom/intsig/camscanner/scenariodir/dialog/AbsAddCertificateDialog;->o00〇88〇08(Lcom/intsig/camscanner/scenariodir/adapter/CertificateSelectAdapter;)V

    .line 27
    .line 28
    .line 29
    invoke-virtual {p0}, Lcom/intsig/camscanner/scenariodir/dialog/AbsAddCertificateDialog;->oO〇oo()Lcom/intsig/camscanner/scenariodir/adapter/CertificateSelectAdapter;

    .line 30
    .line 31
    .line 32
    move-result-object v1

    .line 33
    if-nez v1, :cond_0

    .line 34
    .line 35
    goto :goto_0

    .line 36
    :cond_0
    iget v2, p0, Lcom/intsig/camscanner/scenariodir/dialog/AddOverseaCertificateDialog;->〇080OO8〇0:I

    .line 37
    .line 38
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/scenariodir/adapter/CertificateSelectAdapter;->O0o(I)V

    .line 39
    .line 40
    .line 41
    :goto_0
    invoke-virtual {p0}, Lcom/intsig/camscanner/scenariodir/dialog/AbsAddCertificateDialog;->oO〇oo()Lcom/intsig/camscanner/scenariodir/adapter/CertificateSelectAdapter;

    .line 42
    .line 43
    .line 44
    move-result-object v1

    .line 45
    if-nez v1, :cond_1

    .line 46
    .line 47
    goto :goto_1

    .line 48
    :cond_1
    iget-boolean v2, p0, Lcom/intsig/camscanner/scenariodir/dialog/AddOverseaCertificateDialog;->O8o08O8O:Z

    .line 49
    .line 50
    invoke-virtual {v1, v2}, Lcom/intsig/camscanner/scenariodir/adapter/CertificateSelectAdapter;->O0o〇O0〇(Z)V

    .line 51
    .line 52
    .line 53
    :goto_1
    invoke-virtual {p0}, Lcom/intsig/camscanner/scenariodir/dialog/AbsAddCertificateDialog;->oO〇oo()Lcom/intsig/camscanner/scenariodir/adapter/CertificateSelectAdapter;

    .line 54
    .line 55
    .line 56
    move-result-object v1

    .line 57
    invoke-virtual {v0, v1}, Landroidx/recyclerview/widget/RecyclerView;->setAdapter(Landroidx/recyclerview/widget/RecyclerView$Adapter;)V

    .line 58
    .line 59
    .line 60
    :cond_2
    invoke-virtual {p0}, Lcom/intsig/camscanner/scenariodir/dialog/AbsAddCertificateDialog;->oOo〇08〇()V

    .line 61
    .line 62
    .line 63
    iget-object v0, p0, Lcom/intsig/camscanner/scenariodir/dialog/AddOverseaCertificateDialog;->o〇00O:Lcom/intsig/camscanner/databinding/DialogAddOverseaCertificateBinding;

    .line 64
    .line 65
    if-eqz v0, :cond_3

    .line 66
    .line 67
    iget-object v0, v0, Lcom/intsig/camscanner/databinding/DialogAddOverseaCertificateBinding;->〇OOo8〇0:Landroidx/appcompat/widget/AppCompatImageView;

    .line 68
    .line 69
    if-eqz v0, :cond_3

    .line 70
    .line 71
    new-instance v1, LoooO8〇00/o〇0;

    .line 72
    .line 73
    invoke-direct {v1, p0}, LoooO8〇00/o〇0;-><init>(Lcom/intsig/camscanner/scenariodir/dialog/AddOverseaCertificateDialog;)V

    .line 74
    .line 75
    .line 76
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    .line 78
    .line 79
    :cond_3
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/FragmentActivity;

    .line 80
    .line 81
    .line 82
    move-result-object v0

    .line 83
    if-eqz v0, :cond_7

    .line 84
    .line 85
    iget-object v1, p0, Lcom/intsig/camscanner/scenariodir/dialog/AddOverseaCertificateDialog;->o〇00O:Lcom/intsig/camscanner/databinding/DialogAddOverseaCertificateBinding;

    .line 86
    .line 87
    if-eqz v1, :cond_4

    .line 88
    .line 89
    iget-object v1, v1, Lcom/intsig/camscanner/databinding/DialogAddOverseaCertificateBinding;->〇08O〇00〇o:Landroid/widget/TextView;

    .line 90
    .line 91
    goto :goto_2

    .line 92
    :cond_4
    const/4 v1, 0x0

    .line 93
    :goto_2
    if-nez v1, :cond_5

    .line 94
    .line 95
    goto :goto_4

    .line 96
    :cond_5
    iget-boolean v2, p0, Lcom/intsig/camscanner/scenariodir/dialog/AddOverseaCertificateDialog;->O8o08O8O:Z

    .line 97
    .line 98
    if-eqz v2, :cond_6

    .line 99
    .line 100
    const v2, 0x7f13127a

    .line 101
    .line 102
    .line 103
    goto :goto_3

    .line 104
    :cond_6
    const v2, 0x7f131025

    .line 105
    .line 106
    .line 107
    :goto_3
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 108
    .line 109
    .line 110
    move-result-object v0

    .line 111
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    .line 113
    .line 114
    :cond_7
    :goto_4
    invoke-virtual {p0}, Lcom/intsig/camscanner/scenariodir/dialog/AbsAddCertificateDialog;->〇〇o0〇8()V

    .line 115
    .line 116
    .line 117
    return-void
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
    .line 124
    .line 125
    .line 126
    .line 127
    .line 128
    .line 129
    .line 130
    .line 131
    .line 132
    .line 133
    .line 134
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method

.method private static final 〇088O(Lcom/intsig/camscanner/scenariodir/dialog/AddOverseaCertificateDialog;Landroid/view/View;)V
    .locals 0

    .line 1
    const-string/jumbo p1, "this$0"

    .line 2
    .line 3
    .line 4
    invoke-static {p0, p1}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    invoke-virtual {p0}, Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;->dismiss()V

    .line 8
    .line 9
    .line 10
    return-void
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final 〇8〇OOoooo()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/intsig/camscanner/scenariodir/dialog/AbsAddCertificateDialog;->oooO888()Ljava/util/ArrayList;

    .line 2
    .line 3
    .line 4
    move-result-object v0

    .line 5
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 6
    .line 7
    .line 8
    invoke-virtual {p0}, Lcom/intsig/camscanner/scenariodir/dialog/AbsAddCertificateDialog;->oooO888()Ljava/util/ArrayList;

    .line 9
    .line 10
    .line 11
    move-result-object v0

    .line 12
    invoke-static {}, Lcom/intsig/camscanner/scenariodir/util/CertificateUtil;->〇80〇808〇O()Ljava/util/List;

    .line 13
    .line 14
    .line 15
    move-result-object v1

    .line 16
    check-cast v1, Ljava/util/Collection;

    .line 17
    .line 18
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 19
    .line 20
    .line 21
    return-void
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method


# virtual methods
.method public final init()V
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/dialog/AddOverseaCertificateDialog;->〇8〇OOoooo()V

    .line 2
    .line 3
    .line 4
    invoke-direct {p0}, Lcom/intsig/camscanner/scenariodir/dialog/AddOverseaCertificateDialog;->o〇0〇o()V

    .line 5
    .line 6
    .line 7
    return-void
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 2
    .line 3
    .line 4
    const p1, 0x7f140192

    .line 5
    .line 6
    .line 7
    const/4 v0, 0x0

    .line 8
    invoke-virtual {p0, v0, p1}, Landroidx/fragment/app/DialogFragment;->setStyle(II)V

    .line 9
    .line 10
    .line 11
    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    .line 12
    .line 13
    .line 14
    move-result-object p1

    .line 15
    if-eqz p1, :cond_0

    .line 16
    .line 17
    const-string v1, "extra_key_need_highlight"

    .line 18
    .line 19
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    .line 20
    .line 21
    .line 22
    move-result v0

    .line 23
    iput-boolean v0, p0, Lcom/intsig/camscanner/scenariodir/dialog/AddOverseaCertificateDialog;->O8o08O8O:Z

    .line 24
    .line 25
    const-string v0, "extra_key_current_select_card_type"

    .line 26
    .line 27
    const/4 v1, -0x1

    .line 28
    invoke-virtual {p1, v0, v1}, Landroid/os/BaseBundle;->getInt(Ljava/lang/String;I)I

    .line 29
    .line 30
    .line 31
    move-result p1

    .line 32
    iput p1, p0, Lcom/intsig/camscanner/scenariodir/dialog/AddOverseaCertificateDialog;->〇080OO8〇0:I

    .line 33
    .line 34
    :cond_0
    return-void
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1    # Landroid/view/LayoutInflater;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string p3, "inflater"

    .line 2
    .line 3
    invoke-static {p1, p3}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    const p3, 0x7f0d0172

    .line 7
    .line 8
    .line 9
    const/4 v0, 0x0

    .line 10
    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 11
    .line 12
    .line 13
    move-result-object p1

    .line 14
    invoke-static {p1}, Lcom/intsig/camscanner/databinding/DialogAddOverseaCertificateBinding;->bind(Landroid/view/View;)Lcom/intsig/camscanner/databinding/DialogAddOverseaCertificateBinding;

    .line 15
    .line 16
    .line 17
    move-result-object p2

    .line 18
    iput-object p2, p0, Lcom/intsig/camscanner/scenariodir/dialog/AddOverseaCertificateDialog;->o〇00O:Lcom/intsig/camscanner/databinding/DialogAddOverseaCertificateBinding;

    .line 19
    .line 20
    return-object p1
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
    .line 72
    .line 73
    .line 74
    .line 75
    .line 76
    .line 77
    .line 78
    .line 79
    .line 80
    .line 81
    .line 82
    .line 83
    .line 84
    .line 85
    .line 86
    .line 87
    .line 88
    .line 89
    .line 90
    .line 91
    .line 92
    .line 93
    .line 94
    .line 95
    .line 96
    .line 97
    .line 98
    .line 99
    .line 100
    .line 101
    .line 102
    .line 103
    .line 104
    .line 105
    .line 106
    .line 107
    .line 108
    .line 109
    .line 110
    .line 111
    .line 112
    .line 113
    .line 114
    .line 115
    .line 116
    .line 117
    .line 118
    .line 119
    .line 120
    .line 121
    .line 122
    .line 123
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string/jumbo v0, "view"

    .line 2
    .line 3
    .line 4
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 5
    .line 6
    .line 7
    invoke-super {p0, p1, p2}, Landroidx/fragment/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 8
    .line 9
    .line 10
    invoke-virtual {p0}, Lcom/intsig/camscanner/scenariodir/dialog/AddOverseaCertificateDialog;->init()V

    .line 11
    .line 12
    .line 13
    return-void
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

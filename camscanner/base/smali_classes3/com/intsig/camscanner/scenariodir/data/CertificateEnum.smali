.class public final enum Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;
.super Ljava/lang/Enum;
.source "CertificateEnum.kt"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;",
        ">;"
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum ADMISSION_LETTER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum AIRCREW:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum ARCHITECT_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum AUCTIONEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum AVIATION_INTELLIGENCE_OFFICER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum AVIATION_SECURITY_OFFICER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum BANKBOOK:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum BANKING_PROFESSIONAL:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum BANK_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum BIRTH_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum BIRTH_CERTIFICATE_NEW:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum BUSINESS:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum BUSINESS_LICENSE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum CERTIFICATE_COMPLETION:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum CERTIFIED_PERSONNEL:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum CET_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum CHEMICAL_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum CHILD_HEALTH_CARE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum CHILD_HEALTH_HANDBOOK:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum CITIZEN_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum CIVIL_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum COMMUNICATION_PERSONNEL:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum COMPERE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum COMPUTER_RANK:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum COMPUTER_SOFTWARE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum CONSTRUCTOR_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum COUNTRY_DOCTOR:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum CPA:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum CPA_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum CPV:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum CREW:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum CULTURAL_RELICS_PROTECTION:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum DEATH_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum DEGREE_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum DISABILITY_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum DIVORCE_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum DOCTOR:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum DRIVE_LICENSE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum ECONOMIC_PROFESSIONAL:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum EIA:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum ELECTRICAL_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum ENTRY_EXIT_PERSONNEL:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum ENVIRONMENTAL_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum EQUIPMENT_SUPERVISOR:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum FAMILY_REGISTER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum FIREFIGHTER_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum FOREIGN_BANK_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum FOREIGN_ID_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum FOREIGN_OTHER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum FOREIGN_PASSPORT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum HIGHWAY_WATERWAY_ENGINEERIN:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum HK_MACAO_PASSPORT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum HOUSE_PROPERTY:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum ID_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum JOURNALIST:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum LAND_REGISTRATION_AGENTS:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum LAWYER_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum LAWYER_LICENSE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum LICENSED_VETERINARIAN:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum MARINE_SURVEYOR:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum MARRIAGE_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum MECHANICAL_ENGINEERS:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum MEDICAL_INSURANCE_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum MEDICAL_PROFESSIONAL:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum MEDICAL_RECORD_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum METALLURGICAL_ENGINEE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum METROLOGIST:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum MILITARY_OFFICE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum MINERAL_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum MOTOR_VEHICLE_TESTING:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum NATURAL_GAS_ENGINEE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum NAVIGATORS:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum NOTARY_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum NUCLEAR_OPERATOR_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum NUCLEAR_SAFETY_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum NUCLEAR_SAFETY_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum NURSE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum ORGAN_TRANSPLANTATION_DOCTOR:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum PASSPORT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum PATENT_AGENT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum PERFORMANCE_BROKERS:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum PHARMACIST:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum PROFESSIONAL:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum PROJECT_CONSULTANCY:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum PUBLIC_EQUIPMENT_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum PUBLISHING_INDUSTRY:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum QS_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum REAL_ESTATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum REAL_ESTATE_BROKERS:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum REGISTERED_SURVEYOR:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum REGISTRATION_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum RESIDENCE_PERMIT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum SAFETY_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum SECURITIES_CERTIFICATION:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum SOCIAL_SECURITY_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum SOCIAL_WORKER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum SPECIAL_EQUIPMENT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum STATISTICAL_PROFESSION:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum STRUCTURAL_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum STUDENT_ID:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum SUPERVISING_ENGINEER_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum TAIWAN_PASSPORT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum TAX_ACCOUNTANT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum TEACHER_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum TOUR_GUIDE_TEST:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum TRANSLATORS_QUALIFICATION:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum UNDERGRADUATE_DIPLOMA:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum URBANRURAL_PLANNER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum VACCINATION_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum VEHICLE_LICENSE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum VILLAGE_VETERINARIAN:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

.field public static final enum WATER_CONSERVANCY:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;


# instance fields
.field private iconId:I

.field private final nameId:I

.field private final needManualFillIn:Z

.field private final type:I


# direct methods
.method private static final synthetic $values()[Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;
    .locals 3

    .line 1
    const/16 v0, 0x70

    .line 2
    .line 3
    new-array v0, v0, [Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->ID_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 7
    .line 8
    aput-object v2, v0, v1

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FAMILY_REGISTER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 12
    .line 13
    aput-object v2, v0, v1

    .line 14
    .line 15
    const/4 v1, 0x2

    .line 16
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->PASSPORT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 17
    .line 18
    aput-object v2, v0, v1

    .line 19
    .line 20
    const/4 v1, 0x3

    .line 21
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->DRIVE_LICENSE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 22
    .line 23
    aput-object v2, v0, v1

    .line 24
    .line 25
    const/4 v1, 0x4

    .line 26
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->VEHICLE_LICENSE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 27
    .line 28
    aput-object v2, v0, v1

    .line 29
    .line 30
    const/4 v1, 0x5

    .line 31
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->BANK_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 32
    .line 33
    aput-object v2, v0, v1

    .line 34
    .line 35
    const/4 v1, 0x6

    .line 36
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->BUSINESS_LICENSE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 37
    .line 38
    aput-object v2, v0, v1

    .line 39
    .line 40
    const/4 v1, 0x7

    .line 41
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->HOUSE_PROPERTY:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 42
    .line 43
    aput-object v2, v0, v1

    .line 44
    .line 45
    const/16 v1, 0x8

    .line 46
    .line 47
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->BUSINESS:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 48
    .line 49
    aput-object v2, v0, v1

    .line 50
    .line 51
    const/16 v1, 0x9

    .line 52
    .line 53
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->RESIDENCE_PERMIT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 54
    .line 55
    aput-object v2, v0, v1

    .line 56
    .line 57
    const/16 v1, 0xa

    .line 58
    .line 59
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->MARRIAGE_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 60
    .line 61
    aput-object v2, v0, v1

    .line 62
    .line 63
    const/16 v1, 0xb

    .line 64
    .line 65
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->DIVORCE_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 66
    .line 67
    aput-object v2, v0, v1

    .line 68
    .line 69
    const/16 v1, 0xc

    .line 70
    .line 71
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->SOCIAL_SECURITY_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 72
    .line 73
    aput-object v2, v0, v1

    .line 74
    .line 75
    const/16 v1, 0xd

    .line 76
    .line 77
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CITIZEN_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 78
    .line 79
    aput-object v2, v0, v1

    .line 80
    .line 81
    const/16 v1, 0xe

    .line 82
    .line 83
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->MILITARY_OFFICE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 84
    .line 85
    aput-object v2, v0, v1

    .line 86
    .line 87
    const/16 v1, 0xf

    .line 88
    .line 89
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->DISABILITY_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 90
    .line 91
    aput-object v2, v0, v1

    .line 92
    .line 93
    const/16 v1, 0x10

    .line 94
    .line 95
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->BANKBOOK:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 96
    .line 97
    aput-object v2, v0, v1

    .line 98
    .line 99
    const/16 v1, 0x11

    .line 100
    .line 101
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->MEDICAL_INSURANCE_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 102
    .line 103
    aput-object v2, v0, v1

    .line 104
    .line 105
    const/16 v1, 0x12

    .line 106
    .line 107
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->MEDICAL_RECORD_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 108
    .line 109
    aput-object v2, v0, v1

    .line 110
    .line 111
    const/16 v1, 0x13

    .line 112
    .line 113
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->TAIWAN_PASSPORT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 114
    .line 115
    aput-object v2, v0, v1

    .line 116
    .line 117
    const/16 v1, 0x14

    .line 118
    .line 119
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->HK_MACAO_PASSPORT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 120
    .line 121
    aput-object v2, v0, v1

    .line 122
    .line 123
    const/16 v1, 0x15

    .line 124
    .line 125
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->STUDENT_ID:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 126
    .line 127
    aput-object v2, v0, v1

    .line 128
    .line 129
    const/16 v1, 0x16

    .line 130
    .line 131
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->UNDERGRADUATE_DIPLOMA:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 132
    .line 133
    aput-object v2, v0, v1

    .line 134
    .line 135
    const/16 v1, 0x17

    .line 136
    .line 137
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->DEGREE_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 138
    .line 139
    aput-object v2, v0, v1

    .line 140
    .line 141
    const/16 v1, 0x18

    .line 142
    .line 143
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CERTIFICATE_COMPLETION:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 144
    .line 145
    aput-object v2, v0, v1

    .line 146
    .line 147
    const/16 v1, 0x19

    .line 148
    .line 149
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CET_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 150
    .line 151
    aput-object v2, v0, v1

    .line 152
    .line 153
    const/16 v1, 0x1a

    .line 154
    .line 155
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->COMPUTER_RANK:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 156
    .line 157
    aput-object v2, v0, v1

    .line 158
    .line 159
    const/16 v1, 0x1b

    .line 160
    .line 161
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->REGISTRATION_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 162
    .line 163
    aput-object v2, v0, v1

    .line 164
    .line 165
    const/16 v1, 0x1c

    .line 166
    .line 167
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->ADMISSION_LETTER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 168
    .line 169
    aput-object v2, v0, v1

    .line 170
    .line 171
    const/16 v1, 0x1d

    .line 172
    .line 173
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->BIRTH_CERTIFICATE_NEW:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 174
    .line 175
    aput-object v2, v0, v1

    .line 176
    .line 177
    const/16 v1, 0x1e

    .line 178
    .line 179
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->BIRTH_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 180
    .line 181
    aput-object v2, v0, v1

    .line 182
    .line 183
    const/16 v1, 0x1f

    .line 184
    .line 185
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->VACCINATION_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 186
    .line 187
    aput-object v2, v0, v1

    .line 188
    .line 189
    const/16 v1, 0x20

    .line 190
    .line 191
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CHILD_HEALTH_HANDBOOK:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 192
    .line 193
    aput-object v2, v0, v1

    .line 194
    .line 195
    const/16 v1, 0x21

    .line 196
    .line 197
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->TEACHER_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 198
    .line 199
    aput-object v2, v0, v1

    .line 200
    .line 201
    const/16 v1, 0x22

    .line 202
    .line 203
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->TOUR_GUIDE_TEST:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 204
    .line 205
    aput-object v2, v0, v1

    .line 206
    .line 207
    const/16 v1, 0x23

    .line 208
    .line 209
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->DOCTOR:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 210
    .line 211
    aput-object v2, v0, v1

    .line 212
    .line 213
    const/16 v1, 0x24

    .line 214
    .line 215
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FIREFIGHTER_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 216
    .line 217
    aput-object v2, v0, v1

    .line 218
    .line 219
    const/16 v1, 0x25

    .line 220
    .line 221
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->LAWYER_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 222
    .line 223
    aput-object v2, v0, v1

    .line 224
    .line 225
    const/16 v1, 0x26

    .line 226
    .line 227
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->LAWYER_LICENSE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 228
    .line 229
    aput-object v2, v0, v1

    .line 230
    .line 231
    const/16 v1, 0x27

    .line 232
    .line 233
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->NOTARY_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 234
    .line 235
    aput-object v2, v0, v1

    .line 236
    .line 237
    const/16 v1, 0x28

    .line 238
    .line 239
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CPA_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 240
    .line 241
    aput-object v2, v0, v1

    .line 242
    .line 243
    const/16 v1, 0x29

    .line 244
    .line 245
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->NUCLEAR_SAFETY_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 246
    .line 247
    aput-object v2, v0, v1

    .line 248
    .line 249
    const/16 v1, 0x2a

    .line 250
    .line 251
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->NUCLEAR_OPERATOR_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 252
    .line 253
    aput-object v2, v0, v1

    .line 254
    .line 255
    const/16 v1, 0x2b

    .line 256
    .line 257
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->NUCLEAR_SAFETY_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 258
    .line 259
    aput-object v2, v0, v1

    .line 260
    .line 261
    const/16 v1, 0x2c

    .line 262
    .line 263
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->ARCHITECT_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 264
    .line 265
    aput-object v2, v0, v1

    .line 266
    .line 267
    const/16 v1, 0x2d

    .line 268
    .line 269
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->SUPERVISING_ENGINEER_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 270
    .line 271
    aput-object v2, v0, v1

    .line 272
    .line 273
    const/16 v1, 0x2e

    .line 274
    .line 275
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->REAL_ESTATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 276
    .line 277
    aput-object v2, v0, v1

    .line 278
    .line 279
    const/16 v1, 0x2f

    .line 280
    .line 281
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->QS_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 282
    .line 283
    aput-object v2, v0, v1

    .line 284
    .line 285
    const/16 v1, 0x30

    .line 286
    .line 287
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->URBANRURAL_PLANNER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 288
    .line 289
    aput-object v2, v0, v1

    .line 290
    .line 291
    const/16 v1, 0x31

    .line 292
    .line 293
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CONSTRUCTOR_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 294
    .line 295
    aput-object v2, v0, v1

    .line 296
    .line 297
    const/16 v1, 0x32

    .line 298
    .line 299
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->STRUCTURAL_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 300
    .line 301
    aput-object v2, v0, v1

    .line 302
    .line 303
    const/16 v1, 0x33

    .line 304
    .line 305
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CIVIL_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 306
    .line 307
    aput-object v2, v0, v1

    .line 308
    .line 309
    const/16 v1, 0x34

    .line 310
    .line 311
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CHEMICAL_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 312
    .line 313
    aput-object v2, v0, v1

    .line 314
    .line 315
    const/16 v1, 0x35

    .line 316
    .line 317
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->ELECTRICAL_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 318
    .line 319
    aput-object v2, v0, v1

    .line 320
    .line 321
    const/16 v1, 0x36

    .line 322
    .line 323
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->PUBLIC_EQUIPMENT_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 324
    .line 325
    aput-object v2, v0, v1

    .line 326
    .line 327
    const/16 v1, 0x37

    .line 328
    .line 329
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->ENVIRONMENTAL_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 330
    .line 331
    aput-object v2, v0, v1

    .line 332
    .line 333
    const/16 v1, 0x38

    .line 334
    .line 335
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->NATURAL_GAS_ENGINEE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 336
    .line 337
    aput-object v2, v0, v1

    .line 338
    .line 339
    const/16 v1, 0x39

    .line 340
    .line 341
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->METALLURGICAL_ENGINEE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 342
    .line 343
    aput-object v2, v0, v1

    .line 344
    .line 345
    const/16 v1, 0x3a

    .line 346
    .line 347
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->MINERAL_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 348
    .line 349
    aput-object v2, v0, v1

    .line 350
    .line 351
    const/16 v1, 0x3b

    .line 352
    .line 353
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->MECHANICAL_ENGINEERS:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 354
    .line 355
    aput-object v2, v0, v1

    .line 356
    .line 357
    const/16 v1, 0x3c

    .line 358
    .line 359
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->MARINE_SURVEYOR:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 360
    .line 361
    aput-object v2, v0, v1

    .line 362
    .line 363
    const/16 v1, 0x3d

    .line 364
    .line 365
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CREW:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 366
    .line 367
    aput-object v2, v0, v1

    .line 368
    .line 369
    const/16 v1, 0x3e

    .line 370
    .line 371
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->LICENSED_VETERINARIAN:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 372
    .line 373
    aput-object v2, v0, v1

    .line 374
    .line 375
    const/16 v1, 0x3f

    .line 376
    .line 377
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->VILLAGE_VETERINARIAN:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 378
    .line 379
    aput-object v2, v0, v1

    .line 380
    .line 381
    const/16 v1, 0x40

    .line 382
    .line 383
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->AUCTIONEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 384
    .line 385
    aput-object v2, v0, v1

    .line 386
    .line 387
    const/16 v1, 0x41

    .line 388
    .line 389
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->PERFORMANCE_BROKERS:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 390
    .line 391
    aput-object v2, v0, v1

    .line 392
    .line 393
    const/16 v1, 0x42

    .line 394
    .line 395
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->COUNTRY_DOCTOR:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 396
    .line 397
    aput-object v2, v0, v1

    .line 398
    .line 399
    const/16 v1, 0x43

    .line 400
    .line 401
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->ORGAN_TRANSPLANTATION_DOCTOR:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 402
    .line 403
    aput-object v2, v0, v1

    .line 404
    .line 405
    const/16 v1, 0x44

    .line 406
    .line 407
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->NURSE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 408
    .line 409
    aput-object v2, v0, v1

    .line 410
    .line 411
    const/16 v1, 0x45

    .line 412
    .line 413
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CHILD_HEALTH_CARE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 414
    .line 415
    aput-object v2, v0, v1

    .line 416
    .line 417
    const/16 v1, 0x46

    .line 418
    .line 419
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->ENTRY_EXIT_PERSONNEL:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 420
    .line 421
    aput-object v2, v0, v1

    .line 422
    .line 423
    const/16 v1, 0x47

    .line 424
    .line 425
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->EQUIPMENT_SUPERVISOR:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 426
    .line 427
    aput-object v2, v0, v1

    .line 428
    .line 429
    const/16 v1, 0x48

    .line 430
    .line 431
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->METROLOGIST:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 432
    .line 433
    aput-object v2, v0, v1

    .line 434
    .line 435
    const/16 v1, 0x49

    .line 436
    .line 437
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->COMPERE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 438
    .line 439
    aput-object v2, v0, v1

    .line 440
    .line 441
    const/16 v1, 0x4a

    .line 442
    .line 443
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->JOURNALIST:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 444
    .line 445
    aput-object v2, v0, v1

    .line 446
    .line 447
    const/16 v1, 0x4b

    .line 448
    .line 449
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->SAFETY_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 450
    .line 451
    aput-object v2, v0, v1

    .line 452
    .line 453
    const/16 v1, 0x4c

    .line 454
    .line 455
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->PHARMACIST:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 456
    .line 457
    aput-object v2, v0, v1

    .line 458
    .line 459
    const/16 v1, 0x4d

    .line 460
    .line 461
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->PATENT_AGENT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 462
    .line 463
    aput-object v2, v0, v1

    .line 464
    .line 465
    const/16 v1, 0x4e

    .line 466
    .line 467
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->REGISTERED_SURVEYOR:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 468
    .line 469
    aput-object v2, v0, v1

    .line 470
    .line 471
    const/16 v1, 0x4f

    .line 472
    .line 473
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->AIRCREW:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 474
    .line 475
    aput-object v2, v0, v1

    .line 476
    .line 477
    const/16 v1, 0x50

    .line 478
    .line 479
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->NAVIGATORS:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 480
    .line 481
    aput-object v2, v0, v1

    .line 482
    .line 483
    const/16 v1, 0x51

    .line 484
    .line 485
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->AVIATION_SECURITY_OFFICER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 486
    .line 487
    aput-object v2, v0, v1

    .line 488
    .line 489
    const/16 v1, 0x52

    .line 490
    .line 491
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->AVIATION_INTELLIGENCE_OFFICER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 492
    .line 493
    aput-object v2, v0, v1

    .line 494
    .line 495
    const/16 v1, 0x53

    .line 496
    .line 497
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->SPECIAL_EQUIPMENT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 498
    .line 499
    aput-object v2, v0, v1

    .line 500
    .line 501
    const/16 v1, 0x54

    .line 502
    .line 503
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->PROJECT_CONSULTANCY:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 504
    .line 505
    aput-object v2, v0, v1

    .line 506
    .line 507
    const/16 v1, 0x55

    .line 508
    .line 509
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->COMMUNICATION_PERSONNEL:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 510
    .line 511
    aput-object v2, v0, v1

    .line 512
    .line 513
    const/16 v1, 0x56

    .line 514
    .line 515
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->COMPUTER_SOFTWARE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 516
    .line 517
    aput-object v2, v0, v1

    .line 518
    .line 519
    const/16 v1, 0x57

    .line 520
    .line 521
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->SOCIAL_WORKER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 522
    .line 523
    aput-object v2, v0, v1

    .line 524
    .line 525
    const/16 v1, 0x58

    .line 526
    .line 527
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CPA:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 528
    .line 529
    aput-object v2, v0, v1

    .line 530
    .line 531
    const/16 v1, 0x59

    .line 532
    .line 533
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CPV:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 534
    .line 535
    aput-object v2, v0, v1

    .line 536
    .line 537
    const/16 v1, 0x5a

    .line 538
    .line 539
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->ECONOMIC_PROFESSIONAL:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 540
    .line 541
    aput-object v2, v0, v1

    .line 542
    .line 543
    const/16 v1, 0x5b

    .line 544
    .line 545
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->LAND_REGISTRATION_AGENTS:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 546
    .line 547
    aput-object v2, v0, v1

    .line 548
    .line 549
    const/16 v1, 0x5c

    .line 550
    .line 551
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->EIA:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 552
    .line 553
    aput-object v2, v0, v1

    .line 554
    .line 555
    const/16 v1, 0x5d

    .line 556
    .line 557
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->REAL_ESTATE_BROKERS:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 558
    .line 559
    aput-object v2, v0, v1

    .line 560
    .line 561
    const/16 v1, 0x5e

    .line 562
    .line 563
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->MOTOR_VEHICLE_TESTING:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 564
    .line 565
    aput-object v2, v0, v1

    .line 566
    .line 567
    const/16 v1, 0x5f

    .line 568
    .line 569
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->HIGHWAY_WATERWAY_ENGINEERIN:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 570
    .line 571
    aput-object v2, v0, v1

    .line 572
    .line 573
    const/16 v1, 0x60

    .line 574
    .line 575
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->WATER_CONSERVANCY:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 576
    .line 577
    aput-object v2, v0, v1

    .line 578
    .line 579
    const/16 v1, 0x61

    .line 580
    .line 581
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->MEDICAL_PROFESSIONAL:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 582
    .line 583
    aput-object v2, v0, v1

    .line 584
    .line 585
    const/16 v1, 0x62

    .line 586
    .line 587
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->PROFESSIONAL:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 588
    .line 589
    aput-object v2, v0, v1

    .line 590
    .line 591
    const/16 v1, 0x63

    .line 592
    .line 593
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->TAX_ACCOUNTANT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 594
    .line 595
    aput-object v2, v0, v1

    .line 596
    .line 597
    const/16 v1, 0x64

    .line 598
    .line 599
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CERTIFIED_PERSONNEL:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 600
    .line 601
    aput-object v2, v0, v1

    .line 602
    .line 603
    const/16 v1, 0x65

    .line 604
    .line 605
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->PUBLISHING_INDUSTRY:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 606
    .line 607
    aput-object v2, v0, v1

    .line 608
    .line 609
    const/16 v1, 0x66

    .line 610
    .line 611
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->STATISTICAL_PROFESSION:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 612
    .line 613
    aput-object v2, v0, v1

    .line 614
    .line 615
    const/16 v1, 0x67

    .line 616
    .line 617
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->BANKING_PROFESSIONAL:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 618
    .line 619
    aput-object v2, v0, v1

    .line 620
    .line 621
    const/16 v1, 0x68

    .line 622
    .line 623
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->SECURITIES_CERTIFICATION:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 624
    .line 625
    aput-object v2, v0, v1

    .line 626
    .line 627
    const/16 v1, 0x69

    .line 628
    .line 629
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CULTURAL_RELICS_PROTECTION:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 630
    .line 631
    aput-object v2, v0, v1

    .line 632
    .line 633
    const/16 v1, 0x6a

    .line 634
    .line 635
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->TRANSLATORS_QUALIFICATION:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 636
    .line 637
    aput-object v2, v0, v1

    .line 638
    .line 639
    const/16 v1, 0x6b

    .line 640
    .line 641
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->DEATH_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 642
    .line 643
    aput-object v2, v0, v1

    .line 644
    .line 645
    const/16 v1, 0x6c

    .line 646
    .line 647
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_ID_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 648
    .line 649
    aput-object v2, v0, v1

    .line 650
    .line 651
    const/16 v1, 0x6d

    .line 652
    .line 653
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_BANK_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 654
    .line 655
    aput-object v2, v0, v1

    .line 656
    .line 657
    const/16 v1, 0x6e

    .line 658
    .line 659
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_PASSPORT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 660
    .line 661
    aput-object v2, v0, v1

    .line 662
    .line 663
    const/16 v1, 0x6f

    .line 664
    .line 665
    sget-object v2, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_OTHER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 666
    .line 667
    aput-object v2, v0, v1

    .line 668
    .line 669
    return-object v0
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
    .line 825
    .line 826
    .line 827
    .line 828
    .line 829
    .line 830
    .line 831
    .line 832
    .line 833
    .line 834
    .line 835
    .line 836
    .line 837
    .line 838
    .line 839
    .line 840
    .line 841
    .line 842
    .line 843
    .line 844
    .line 845
    .line 846
    .line 847
    .line 848
    .line 849
    .line 850
    .line 851
    .line 852
    .line 853
    .line 854
    .line 855
    .line 856
    .line 857
    .line 858
    .line 859
    .line 860
    .line 861
    .line 862
    .line 863
    .line 864
    .line 865
    .line 866
    .line 867
    .line 868
    .line 869
    .line 870
    .line 871
    .line 872
    .line 873
    .line 874
    .line 875
    .line 876
    .line 877
    .line 878
    .line 879
    .line 880
    .line 881
    .line 882
    .line 883
    .line 884
    .line 885
    .line 886
    .line 887
    .line 888
    .line 889
    .line 890
    .line 891
    .line 892
    .line 893
    .line 894
    .line 895
    .line 896
    .line 897
    .line 898
    .line 899
    .line 900
    .line 901
    .line 902
    .line 903
    .line 904
    .line 905
    .line 906
    .line 907
    .line 908
    .line 909
    .line 910
    .line 911
    .line 912
    .line 913
    .line 914
    .line 915
    .line 916
    .line 917
    .line 918
    .line 919
    .line 920
    .line 921
    .line 922
    .line 923
    .line 924
    .line 925
    .line 926
    .line 927
    .line 928
    .line 929
    .line 930
    .line 931
    .line 932
    .line 933
    .line 934
    .line 935
    .line 936
    .line 937
    .line 938
    .line 939
    .line 940
    .line 941
    .line 942
    .line 943
    .line 944
    .line 945
    .line 946
    .line 947
    .line 948
    .line 949
    .line 950
    .line 951
    .line 952
    .line 953
    .line 954
    .line 955
    .line 956
    .line 957
    .line 958
    .line 959
    .line 960
    .line 961
    .line 962
    .line 963
    .line 964
    .line 965
    .line 966
    .line 967
    .line 968
    .line 969
    .line 970
    .line 971
    .line 972
    .line 973
    .line 974
    .line 975
    .line 976
    .line 977
    .line 978
    .line 979
    .line 980
    .line 981
    .line 982
    .line 983
    .line 984
    .line 985
    .line 986
    .line 987
    .line 988
    .line 989
    .line 990
    .line 991
    .line 992
    .line 993
    .line 994
    .line 995
    .line 996
    .line 997
    .line 998
    .line 999
    .line 1000
    .line 1001
    .line 1002
    .line 1003
    .line 1004
    .line 1005
    .line 1006
    .line 1007
    .line 1008
    .line 1009
    .line 1010
    .line 1011
    .line 1012
    .line 1013
    .line 1014
    .line 1015
    .line 1016
    .line 1017
    .line 1018
    .line 1019
    .line 1020
    .line 1021
    .line 1022
    .line 1023
    .line 1024
    .line 1025
    .line 1026
    .line 1027
    .line 1028
    .line 1029
    .line 1030
    .line 1031
    .line 1032
    .line 1033
    .line 1034
    .line 1035
    .line 1036
    .line 1037
    .line 1038
    .line 1039
    .line 1040
    .line 1041
    .line 1042
    .line 1043
    .line 1044
    .line 1045
    .line 1046
    .line 1047
    .line 1048
    .line 1049
    .line 1050
    .line 1051
    .line 1052
    .line 1053
    .line 1054
    .line 1055
    .line 1056
    .line 1057
    .line 1058
    .line 1059
    .line 1060
    .line 1061
    .line 1062
    .line 1063
    .line 1064
    .line 1065
    .line 1066
    .line 1067
    .line 1068
    .line 1069
    .line 1070
    .line 1071
    .line 1072
    .line 1073
    .line 1074
    .line 1075
    .line 1076
    .line 1077
    .line 1078
    .line 1079
    .line 1080
    .line 1081
    .line 1082
    .line 1083
    .line 1084
    .line 1085
    .line 1086
    .line 1087
    .line 1088
    .line 1089
    .line 1090
    .line 1091
    .line 1092
    .line 1093
    .line 1094
    .line 1095
    .line 1096
    .line 1097
    .line 1098
    .line 1099
    .line 1100
    .line 1101
    .line 1102
    .line 1103
    .line 1104
    .line 1105
    .line 1106
    .line 1107
    .line 1108
    .line 1109
    .line 1110
    .line 1111
    .line 1112
    .line 1113
    .line 1114
    .line 1115
    .line 1116
    .line 1117
    .line 1118
    .line 1119
    .line 1120
    .line 1121
    .line 1122
    .line 1123
    .line 1124
    .line 1125
    .line 1126
    .line 1127
    .line 1128
    .line 1129
    .line 1130
    .line 1131
    .line 1132
    .line 1133
    .line 1134
    .line 1135
    .line 1136
    .line 1137
    .line 1138
    .line 1139
    .line 1140
    .line 1141
    .line 1142
    .line 1143
    .line 1144
    .line 1145
    .line 1146
    .line 1147
    .line 1148
    .line 1149
    .line 1150
    .line 1151
    .line 1152
    .line 1153
    .line 1154
    .line 1155
    .line 1156
    .line 1157
    .line 1158
    .line 1159
    .line 1160
    .line 1161
    .line 1162
    .line 1163
    .line 1164
    .line 1165
    .line 1166
    .line 1167
    .line 1168
    .line 1169
    .line 1170
    .line 1171
    .line 1172
    .line 1173
    .line 1174
    .line 1175
    .line 1176
    .line 1177
    .line 1178
    .line 1179
    .line 1180
    .line 1181
    .line 1182
    .line 1183
    .line 1184
    .line 1185
    .line 1186
    .line 1187
    .line 1188
    .line 1189
    .line 1190
    .line 1191
    .line 1192
    .line 1193
    .line 1194
    .line 1195
    .line 1196
    .line 1197
    .line 1198
    .line 1199
    .line 1200
    .line 1201
    .line 1202
    .line 1203
    .line 1204
    .line 1205
    .line 1206
    .line 1207
    .line 1208
    .line 1209
    .line 1210
    .line 1211
    .line 1212
    .line 1213
    .line 1214
    .line 1215
    .line 1216
    .line 1217
    .line 1218
    .line 1219
    .line 1220
    .line 1221
    .line 1222
    .line 1223
    .line 1224
    .line 1225
    .line 1226
    .line 1227
    .line 1228
    .line 1229
    .line 1230
    .line 1231
    .line 1232
    .line 1233
    .line 1234
    .line 1235
    .line 1236
    .line 1237
    .line 1238
    .line 1239
    .line 1240
    .line 1241
    .line 1242
    .line 1243
    .line 1244
    .line 1245
    .line 1246
    .line 1247
    .line 1248
    .line 1249
    .line 1250
    .line 1251
    .line 1252
    .line 1253
    .line 1254
    .line 1255
    .line 1256
    .line 1257
    .line 1258
    .line 1259
    .line 1260
    .line 1261
    .line 1262
    .line 1263
    .line 1264
    .line 1265
    .line 1266
    .line 1267
    .line 1268
    .line 1269
    .line 1270
    .line 1271
    .line 1272
    .line 1273
    .line 1274
    .line 1275
    .line 1276
    .line 1277
    .line 1278
    .line 1279
    .line 1280
    .line 1281
    .line 1282
    .line 1283
    .line 1284
    .line 1285
    .line 1286
    .line 1287
    .line 1288
    .line 1289
    .line 1290
    .line 1291
    .line 1292
    .line 1293
    .line 1294
    .line 1295
    .line 1296
    .line 1297
    .line 1298
    .line 1299
    .line 1300
    .line 1301
    .line 1302
    .line 1303
    .line 1304
    .line 1305
    .line 1306
    .line 1307
    .line 1308
    .line 1309
    .line 1310
    .line 1311
    .line 1312
    .line 1313
    .line 1314
    .line 1315
    .line 1316
    .line 1317
    .line 1318
    .line 1319
    .line 1320
    .line 1321
    .line 1322
    .line 1323
    .line 1324
    .line 1325
    .line 1326
    .line 1327
    .line 1328
    .line 1329
    .line 1330
    .line 1331
    .line 1332
    .line 1333
    .line 1334
    .line 1335
    .line 1336
    .line 1337
    .line 1338
    .line 1339
    .line 1340
    .line 1341
    .line 1342
    .line 1343
    .line 1344
    .line 1345
    .line 1346
    .line 1347
    .line 1348
    .line 1349
    .line 1350
    .line 1351
    .line 1352
    .line 1353
    .line 1354
    .line 1355
    .line 1356
    .line 1357
    .line 1358
    .line 1359
    .line 1360
    .line 1361
    .line 1362
    .line 1363
    .line 1364
    .line 1365
    .line 1366
    .line 1367
    .line 1368
    .line 1369
    .line 1370
    .line 1371
    .line 1372
    .line 1373
    .line 1374
    .line 1375
    .line 1376
    .line 1377
    .line 1378
    .line 1379
    .line 1380
    .line 1381
    .line 1382
    .line 1383
    .line 1384
    .line 1385
    .line 1386
    .line 1387
    .line 1388
    .line 1389
    .line 1390
    .line 1391
    .line 1392
    .line 1393
    .line 1394
    .line 1395
    .line 1396
    .line 1397
    .line 1398
    .line 1399
    .line 1400
    .line 1401
    .line 1402
    .line 1403
    .line 1404
    .line 1405
    .line 1406
    .line 1407
    .line 1408
    .line 1409
    .line 1410
    .line 1411
    .line 1412
    .line 1413
    .line 1414
    .line 1415
    .line 1416
    .line 1417
    .line 1418
    .line 1419
    .line 1420
    .line 1421
    .line 1422
    .line 1423
    .line 1424
    .line 1425
    .line 1426
    .line 1427
    .line 1428
    .line 1429
    .line 1430
    .line 1431
    .line 1432
    .line 1433
    .line 1434
    .line 1435
    .line 1436
    .line 1437
    .line 1438
    .line 1439
    .line 1440
    .line 1441
    .line 1442
    .line 1443
    .line 1444
    .line 1445
    .line 1446
    .line 1447
    .line 1448
    .line 1449
    .line 1450
    .line 1451
    .line 1452
    .line 1453
    .line 1454
    .line 1455
    .line 1456
    .line 1457
    .line 1458
    .line 1459
    .line 1460
    .line 1461
    .line 1462
    .line 1463
    .line 1464
    .line 1465
    .line 1466
    .line 1467
    .line 1468
    .line 1469
    .line 1470
    .line 1471
    .line 1472
    .line 1473
    .line 1474
    .line 1475
    .line 1476
    .line 1477
    .line 1478
    .line 1479
    .line 1480
    .line 1481
    .line 1482
    .line 1483
    .line 1484
    .line 1485
    .line 1486
    .line 1487
    .line 1488
    .line 1489
    .line 1490
    .line 1491
    .line 1492
    .line 1493
    .line 1494
    .line 1495
    .line 1496
    .line 1497
    .line 1498
    .line 1499
    .line 1500
    .line 1501
    .line 1502
    .line 1503
    .line 1504
    .line 1505
    .line 1506
    .line 1507
    .line 1508
    .line 1509
    .line 1510
    .line 1511
    .line 1512
    .line 1513
    .line 1514
    .line 1515
    .line 1516
    .line 1517
    .line 1518
    .line 1519
    .line 1520
    .line 1521
    .line 1522
    .line 1523
    .line 1524
    .line 1525
    .line 1526
    .line 1527
    .line 1528
    .line 1529
    .line 1530
    .line 1531
    .line 1532
    .line 1533
    .line 1534
    .line 1535
    .line 1536
    .line 1537
    .line 1538
.end method

.method static constructor <clinit>()V
    .locals 19

    .line 1
    new-instance v9, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v1, "ID_CARD"

    const/4 v2, 0x0

    const/4 v3, 0x2

    const v4, 0x7f130696

    const v5, 0x7f080e55

    const/4 v6, 0x0

    const/16 v7, 0x8

    const/4 v8, 0x0

    move-object v0, v9

    invoke-direct/range {v0 .. v8}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v9, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->ID_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 2
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "FAMILY_REGISTER"

    const/4 v12, 0x1

    const/4 v13, 0x3

    const v14, 0x7f13103f

    const v15, 0x7f080e4b

    const/16 v16, 0x0

    const/16 v17, 0x8

    const/16 v18, 0x0

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FAMILY_REGISTER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 3
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "PASSPORT"

    const/4 v4, 0x5

    const v5, 0x7f130107

    const v6, 0x7f080e65

    const/4 v7, 0x0

    const/16 v8, 0x8

    const/4 v9, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->PASSPORT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 4
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "DRIVE_LICENSE"

    const/4 v12, 0x3

    const/16 v13, 0x71

    const v14, 0x7f130f86

    const v15, 0x7f080e46

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->DRIVE_LICENSE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 5
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "VEHICLE_LICENSE"

    const/4 v3, 0x4

    const/16 v4, 0x72

    const v5, 0x7f130f89

    const v6, 0x7f080e7d

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->VEHICLE_LICENSE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 6
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "BANK_CARD"

    const/4 v12, 0x5

    const/16 v13, 0xd

    const v14, 0x7f131a03

    const/4 v15, 0x0

    const/16 v17, 0xc

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->BANK_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 7
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "BUSINESS_LICENSE"

    const/4 v3, 0x6

    const/16 v4, 0x10

    const v5, 0x7f131a06

    const/4 v6, 0x0

    const/16 v8, 0xc

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->BUSINESS_LICENSE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 8
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "HOUSE_PROPERTY"

    const/4 v12, 0x7

    const/16 v13, 0x9

    const v14, 0x7f13070d

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->HOUSE_PROPERTY:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 9
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "BUSINESS"

    const/16 v3, 0x8

    const/4 v4, 0x6

    const v5, 0x7f1300f1

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->BUSINESS:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 10
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "RESIDENCE_PERMIT"

    const/16 v12, 0x9

    const/16 v13, 0x11

    const v14, 0x7f130717

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->RESIDENCE_PERMIT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 11
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "MARRIAGE_CARD"

    const/16 v3, 0xa

    const/16 v4, 0x12

    const v5, 0x7f1306f0

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->MARRIAGE_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 12
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "DIVORCE_CARD"

    const/16 v12, 0xb

    const/16 v13, 0x13

    const v14, 0x7f1306c9

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->DIVORCE_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 13
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "SOCIAL_SECURITY_CARD"

    const/16 v3, 0xc

    const/16 v4, 0x14

    const v5, 0x7f13071a

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->SOCIAL_SECURITY_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 14
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "CITIZEN_CARD"

    const/16 v12, 0xd

    const/16 v13, 0x15

    const v14, 0x7f1306b7

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CITIZEN_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 15
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "MILITARY_OFFICE"

    const/16 v3, 0xe

    const/16 v4, 0x16

    const v5, 0x7f1306f8

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->MILITARY_OFFICE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 16
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "DISABILITY_CARD"

    const/16 v12, 0xf

    const/16 v13, 0x75

    const v14, 0x7f1308c5

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->DISABILITY_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 17
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "BANKBOOK"

    const/16 v3, 0x10

    const/16 v4, 0x17

    const v5, 0x7f1306a2

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->BANKBOOK:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 18
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "MEDICAL_INSURANCE_CARD"

    const/16 v12, 0x11

    const/16 v13, 0x18

    const v14, 0x7f1306f2

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->MEDICAL_INSURANCE_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 19
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "MEDICAL_RECORD_CARD"

    const/16 v3, 0x12

    const/16 v4, 0x19

    const v5, 0x7f1306f4

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->MEDICAL_RECORD_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 20
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "TAIWAN_PASSPORT"

    const/16 v12, 0x13

    const/16 v13, 0x1a

    const v14, 0x7f130722

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->TAIWAN_PASSPORT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 21
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "HK_MACAO_PASSPORT"

    const/16 v3, 0x14

    const/16 v4, 0x1b

    const v5, 0x7f1306e7

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->HK_MACAO_PASSPORT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 22
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "STUDENT_ID"

    const/16 v12, 0x15

    const/16 v13, 0x1c

    const v14, 0x7f13071f

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->STUDENT_ID:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 23
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "UNDERGRADUATE_DIPLOMA"

    const/16 v3, 0x16

    const/16 v4, 0x1d

    const v5, 0x7f130726

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->UNDERGRADUATE_DIPLOMA:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 24
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "DEGREE_CERTIFICATE"

    const/16 v12, 0x17

    const/16 v13, 0x1e

    const v14, 0x7f1306c8

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->DEGREE_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 25
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "CERTIFICATE_COMPLETION"

    const/16 v3, 0x18

    const/16 v4, 0x1f

    const v5, 0x7f1306af

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CERTIFICATE_COMPLETION:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 26
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "CET_CERTIFICATE"

    const/16 v12, 0x19

    const/16 v13, 0x20

    const v14, 0x7f1306b2

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CET_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 27
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "COMPUTER_RANK"

    const/16 v3, 0x1a

    const/16 v4, 0x21

    const v5, 0x7f1306bd

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->COMPUTER_RANK:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 28
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "REGISTRATION_CERTIFICATE"

    const/16 v12, 0x1b

    const/16 v13, 0x22

    const v14, 0x7f130715

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->REGISTRATION_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 29
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "ADMISSION_LETTER"

    const/16 v3, 0x1c

    const/16 v4, 0x23

    const v5, 0x7f13069b

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->ADMISSION_LETTER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 30
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "BIRTH_CERTIFICATE_NEW"

    const/16 v12, 0x1d

    const/16 v13, 0x76

    const v14, 0x7f1308bf

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->BIRTH_CERTIFICATE_NEW:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 31
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "BIRTH_CERTIFICATE"

    const/16 v3, 0x1e

    const/16 v4, 0x24

    const v5, 0x7f1306ac

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->BIRTH_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 32
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "VACCINATION_CERTIFICATE"

    const/16 v12, 0x1f

    const/16 v13, 0x25

    const v14, 0x7f13072a

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->VACCINATION_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 33
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "CHILD_HEALTH_HANDBOOK"

    const/16 v3, 0x20

    const/16 v4, 0x26

    const v5, 0x7f1306b6

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CHILD_HEALTH_HANDBOOK:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 34
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "TEACHER_CERTIFICATE"

    const/16 v12, 0x21

    const/16 v13, 0x27

    const v14, 0x7f130724

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->TEACHER_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 35
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "TOUR_GUIDE_TEST"

    const/16 v3, 0x22

    const/16 v4, 0x28

    const v5, 0x7f130e3b

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->TOUR_GUIDE_TEST:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 36
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "DOCTOR"

    const/16 v12, 0x23

    const/16 v13, 0x29

    const v14, 0x7f1306ca

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->DOCTOR:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 37
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "FIREFIGHTER_CERTIFICATE"

    const/16 v3, 0x24

    const/16 v4, 0x2a

    const v5, 0x7f1306e2

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FIREFIGHTER_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 38
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "LAWYER_CERTIFICATE"

    const/16 v12, 0x25

    const/16 v13, 0x2b

    const v14, 0x7f1306ed

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->LAWYER_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 39
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "LAWYER_LICENSE"

    const/16 v3, 0x26

    const/16 v4, 0x74

    const v5, 0x7f1308d4

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->LAWYER_LICENSE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 40
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "NOTARY_CERTIFICATE"

    const/16 v12, 0x27

    const/16 v13, 0x2c

    const v14, 0x7f1306ff

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->NOTARY_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 41
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "CPA_CERTIFICATE"

    const/16 v3, 0x28

    const/16 v4, 0x2d

    const v5, 0x7f1306c2

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CPA_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 42
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "NUCLEAR_SAFETY_CERTIFICATE"

    const/16 v12, 0x29

    const/16 v13, 0x2e

    const v14, 0x7f130701

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->NUCLEAR_SAFETY_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 43
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "NUCLEAR_OPERATOR_CERTIFICATE"

    const/16 v3, 0x2a

    const/16 v4, 0x2f

    const v5, 0x7f130700

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->NUCLEAR_OPERATOR_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 44
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "NUCLEAR_SAFETY_ENGINEER"

    const/16 v12, 0x2b

    const/16 v13, 0x30

    const v14, 0x7f130702

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->NUCLEAR_SAFETY_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 45
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "ARCHITECT_CERTIFICATE"

    const/16 v3, 0x2c

    const/16 v4, 0x31

    const v5, 0x7f13069d

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->ARCHITECT_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 46
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "SUPERVISING_ENGINEER_CERTIFICATE"

    const/16 v12, 0x2d

    const/16 v13, 0x32

    const v14, 0x7f130721

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->SUPERVISING_ENGINEER_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 47
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "REAL_ESTATE"

    const/16 v3, 0x2e

    const/16 v4, 0x33

    const v5, 0x7f130711

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->REAL_ESTATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 48
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "QS_ENGINEER"

    const/16 v12, 0x2f

    const/16 v13, 0x34

    const v14, 0x7f130697

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->QS_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 49
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "URBANRURAL_PLANNER"

    const/16 v3, 0x30

    const/16 v4, 0x35

    const v5, 0x7f130698

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->URBANRURAL_PLANNER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 50
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "CONSTRUCTOR_CERTIFICATE"

    const/16 v12, 0x31

    const/16 v13, 0x36

    const v14, 0x7f1306bf

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CONSTRUCTOR_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 51
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "STRUCTURAL_ENGINEER"

    const/16 v3, 0x32

    const/16 v4, 0x37

    const v5, 0x7f13071e

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->STRUCTURAL_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 52
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "CIVIL_ENGINEER"

    const/16 v12, 0x33

    const/16 v13, 0x38

    const v14, 0x7f1306b8

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CIVIL_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 53
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "CHEMICAL_ENGINEER"

    const/16 v3, 0x34

    const/16 v4, 0x39

    const v5, 0x7f1306b3

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CHEMICAL_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 54
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "ELECTRICAL_ENGINEER"

    const/16 v12, 0x35

    const/16 v13, 0x3a

    const v14, 0x7f1306cd

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->ELECTRICAL_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 55
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "PUBLIC_EQUIPMENT_ENGINEER"

    const/16 v3, 0x36

    const/16 v4, 0x3b

    const v5, 0x7f13070f

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->PUBLIC_EQUIPMENT_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 56
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "ENVIRONMENTAL_ENGINEER"

    const/16 v12, 0x37

    const/16 v13, 0x3c

    const v14, 0x7f1306cf

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->ENVIRONMENTAL_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 57
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "NATURAL_GAS_ENGINEE"

    const/16 v3, 0x38

    const/16 v4, 0x3d

    const v5, 0x7f1306fc

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->NATURAL_GAS_ENGINEE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 58
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "METALLURGICAL_ENGINEE"

    const/16 v12, 0x39

    const/16 v13, 0x3e

    const v14, 0x7f1306f6

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->METALLURGICAL_ENGINEE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 59
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "MINERAL_ENGINEER"

    const/16 v3, 0x3a

    const/16 v4, 0x3f

    const v5, 0x7f1306f9

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->MINERAL_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 60
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "MECHANICAL_ENGINEERS"

    const/16 v12, 0x3b

    const/16 v13, 0x40

    const v14, 0x7f1306f1

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->MECHANICAL_ENGINEERS:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 61
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "MARINE_SURVEYOR"

    const/16 v3, 0x3c

    const/16 v4, 0x41

    const v5, 0x7f1306ef

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->MARINE_SURVEYOR:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 62
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "CREW"

    const/16 v12, 0x3d

    const/16 v13, 0x42

    const v14, 0x7f1306c4

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CREW:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 63
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "LICENSED_VETERINARIAN"

    const/16 v3, 0x3e

    const/16 v4, 0x43

    const v5, 0x7f1306ee

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->LICENSED_VETERINARIAN:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 64
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "VILLAGE_VETERINARIAN"

    const/16 v12, 0x3f

    const/16 v13, 0x44

    const v14, 0x7f13072c

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->VILLAGE_VETERINARIAN:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 65
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "AUCTIONEER"

    const/16 v3, 0x40

    const/16 v4, 0x45

    const v5, 0x7f13069e

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->AUCTIONEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 66
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "PERFORMANCE_BROKERS"

    const/16 v12, 0x41

    const/16 v13, 0x46

    const v14, 0x7f13070b

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->PERFORMANCE_BROKERS:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 67
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "COUNTRY_DOCTOR"

    const/16 v3, 0x42

    const/16 v4, 0x47

    const v5, 0x7f1306c0

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->COUNTRY_DOCTOR:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 68
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "ORGAN_TRANSPLANTATION_DOCTOR"

    const/16 v12, 0x43

    const/16 v13, 0x48

    const v14, 0x7f130708

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->ORGAN_TRANSPLANTATION_DOCTOR:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 69
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "NURSE"

    const/16 v3, 0x44

    const/16 v4, 0x49

    const v5, 0x7f130703

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->NURSE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 70
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "CHILD_HEALTH_CARE"

    const/16 v12, 0x45

    const/16 v13, 0x4a

    const v14, 0x7f1306b5

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CHILD_HEALTH_CARE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 71
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "ENTRY_EXIT_PERSONNEL"

    const/16 v3, 0x46

    const/16 v4, 0x4b

    const v5, 0x7f1306ce

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->ENTRY_EXIT_PERSONNEL:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 72
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "EQUIPMENT_SUPERVISOR"

    const/16 v12, 0x47

    const/16 v13, 0x4c

    const v14, 0x7f1306d0

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->EQUIPMENT_SUPERVISOR:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 73
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "METROLOGIST"

    const/16 v3, 0x48

    const/16 v4, 0x4d

    const v5, 0x7f1306f7

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->METROLOGIST:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 74
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "COMPERE"

    const/16 v12, 0x49

    const/16 v13, 0x4e

    const v14, 0x7f1306bc

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->COMPERE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 75
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "JOURNALIST"

    const/16 v3, 0x4a

    const/16 v4, 0x4f

    const v5, 0x7f1306eb

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->JOURNALIST:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 76
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "SAFETY_ENGINEER"

    const/16 v12, 0x4b

    const/16 v13, 0x50

    const v14, 0x7f130718

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->SAFETY_ENGINEER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 77
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "PHARMACIST"

    const/16 v3, 0x4c

    const/16 v4, 0x51

    const v5, 0x7f13070c

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->PHARMACIST:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 78
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "PATENT_AGENT"

    const/16 v12, 0x4d

    const/16 v13, 0x52

    const v14, 0x7f130709

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->PATENT_AGENT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 79
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "REGISTERED_SURVEYOR"

    const/16 v3, 0x4e

    const/16 v4, 0x53

    const v5, 0x7f130714

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->REGISTERED_SURVEYOR:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 80
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "AIRCREW"

    const/16 v12, 0x4f

    const/16 v13, 0x54

    const v14, 0x7f13069c

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->AIRCREW:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 81
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "NAVIGATORS"

    const/16 v3, 0x50

    const/16 v4, 0x55

    const v5, 0x7f1306fd

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->NAVIGATORS:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 82
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "AVIATION_SECURITY_OFFICER"

    const/16 v12, 0x51

    const/16 v13, 0x56

    const v14, 0x7f1306a1

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->AVIATION_SECURITY_OFFICER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 83
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "AVIATION_INTELLIGENCE_OFFICER"

    const/16 v3, 0x52

    const/16 v4, 0x57

    const v5, 0x7f1306a0

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->AVIATION_INTELLIGENCE_OFFICER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 84
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "SPECIAL_EQUIPMENT"

    const/16 v12, 0x53

    const/16 v13, 0x58

    const v14, 0x7f13071c

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->SPECIAL_EQUIPMENT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 85
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "PROJECT_CONSULTANCY"

    const/16 v3, 0x54

    const/16 v4, 0x59

    const v5, 0x7f13070e

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->PROJECT_CONSULTANCY:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 86
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "COMMUNICATION_PERSONNEL"

    const/16 v12, 0x55

    const/16 v13, 0x5a

    const v14, 0x7f1306bb

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->COMMUNICATION_PERSONNEL:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 87
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "COMPUTER_SOFTWARE"

    const/16 v3, 0x56

    const/16 v4, 0x5b

    const v5, 0x7f1306be

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->COMPUTER_SOFTWARE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 88
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "SOCIAL_WORKER"

    const/16 v12, 0x57

    const/16 v13, 0x5c

    const v14, 0x7f13071b

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->SOCIAL_WORKER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 89
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "CPA"

    const/16 v3, 0x58

    const/16 v4, 0x5d

    const v5, 0x7f1306c1

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CPA:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 90
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "CPV"

    const/16 v12, 0x59

    const/16 v13, 0x5e

    const v14, 0x7f1306c3

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CPV:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 91
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "ECONOMIC_PROFESSIONAL"

    const/16 v3, 0x5a

    const/16 v4, 0x5f

    const v5, 0x7f1306cb

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->ECONOMIC_PROFESSIONAL:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 92
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "LAND_REGISTRATION_AGENTS"

    const/16 v12, 0x5b

    const/16 v13, 0x60

    const v14, 0x7f1306ec

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->LAND_REGISTRATION_AGENTS:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 93
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "EIA"

    const/16 v3, 0x5c

    const/16 v4, 0x61

    const v5, 0x7f1306cc

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->EIA:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 94
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "REAL_ESTATE_BROKERS"

    const/16 v12, 0x5d

    const/16 v13, 0x62

    const v14, 0x7f130712

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->REAL_ESTATE_BROKERS:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 95
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "MOTOR_VEHICLE_TESTING"

    const/16 v3, 0x5e

    const/16 v4, 0x63

    const v5, 0x7f1306fb

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->MOTOR_VEHICLE_TESTING:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 96
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "HIGHWAY_WATERWAY_ENGINEERIN"

    const/16 v12, 0x5f

    const/16 v13, 0x64

    const v14, 0x7f1306e6

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->HIGHWAY_WATERWAY_ENGINEERIN:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 97
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "WATER_CONSERVANCY"

    const/16 v3, 0x60

    const/16 v4, 0x65

    const v5, 0x7f13072e

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->WATER_CONSERVANCY:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 98
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "MEDICAL_PROFESSIONAL"

    const/16 v12, 0x61

    const/16 v13, 0x66

    const v14, 0x7f1306f3

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->MEDICAL_PROFESSIONAL:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 99
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "PROFESSIONAL"

    const/16 v3, 0x62

    const/16 v4, 0x67

    const v5, 0x7f13069f

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->PROFESSIONAL:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 100
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "TAX_ACCOUNTANT"

    const/16 v12, 0x63

    const/16 v13, 0x68

    const v14, 0x7f130723

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->TAX_ACCOUNTANT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 101
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "CERTIFIED_PERSONNEL"

    const/16 v3, 0x64

    const/16 v4, 0x69

    const v5, 0x7f1306b1

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CERTIFIED_PERSONNEL:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 102
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "PUBLISHING_INDUSTRY"

    const/16 v12, 0x65

    const/16 v13, 0x6a

    const v14, 0x7f130710

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->PUBLISHING_INDUSTRY:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 103
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "STATISTICAL_PROFESSION"

    const/16 v3, 0x66

    const/16 v4, 0x6b

    const v5, 0x7f13071d

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->STATISTICAL_PROFESSION:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 104
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "BANKING_PROFESSIONAL"

    const/16 v12, 0x67

    const/16 v13, 0x6c

    const v14, 0x7f1306a3

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->BANKING_PROFESSIONAL:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 105
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "SECURITIES_CERTIFICATION"

    const/16 v3, 0x68

    const/16 v4, 0x6d

    const v5, 0x7f130719

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->SECURITIES_CERTIFICATION:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 106
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "CULTURAL_RELICS_PROTECTION"

    const/16 v12, 0x69

    const/16 v13, 0x6e

    const v14, 0x7f1306c6

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->CULTURAL_RELICS_PROTECTION:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 107
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "TRANSLATORS_QUALIFICATION"

    const/16 v3, 0x6a

    const/16 v4, 0x6f

    const v5, 0x7f130725

    move-object v1, v0

    invoke-direct/range {v1 .. v9}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->TRANSLATORS_QUALIFICATION:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 108
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v11, "DEATH_CERTIFICATE"

    const/16 v12, 0x6b

    const/16 v13, 0x70

    const v14, 0x7f1306c7

    move-object v10, v0

    invoke-direct/range {v10 .. v18}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->DEATH_CERTIFICATE:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 109
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "FOREIGN_ID_CARD"

    const/16 v3, 0x6c

    const/16 v4, 0xbb8

    const v5, 0x7f130696

    const v6, 0x7f080e55

    const/4 v7, 0x1

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZ)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_ID_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 110
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v9, "FOREIGN_BANK_CARD"

    const/16 v10, 0x6d

    const/16 v11, 0xbb9

    const v12, 0x7f131a03

    const v13, 0x7f080ddb

    const/4 v14, 0x1

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZ)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_BANK_CARD:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 111
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v2, "FOREIGN_PASSPORT"

    const/16 v3, 0x6e

    const/16 v4, 0xbba

    const v5, 0x7f130107

    const v6, 0x7f080e65

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZ)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_PASSPORT:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 112
    new-instance v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    const-string v9, "FOREIGN_OTHER"

    const/16 v10, 0x6f

    const/16 v11, 0xbbb

    const v12, 0x7f131296

    const v13, 0x7f080e4e

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZ)V

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->FOREIGN_OTHER:Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    invoke-static {}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->$values()[Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    move-result-object v0

    sput-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->$VALUES:[Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIZ)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    iput p3, p0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->type:I

    iput p4, p0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->nameId:I

    iput p5, p0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->iconId:I

    .line 3
    iput-boolean p6, p0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->needManualFillIn:Z

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IIIIZILkotlin/jvm/internal/DefaultConstructorMarker;)V
    .locals 7

    and-int/lit8 p8, p7, 0x4

    if-eqz p8, :cond_0

    const p5, 0x7f080e62

    const v5, 0x7f080e62

    goto :goto_0

    :cond_0
    move v5, p5

    :goto_0
    and-int/lit8 p5, p7, 0x8

    if-eqz p5, :cond_1

    const/4 p6, 0x0

    const/4 v6, 0x0

    goto :goto_1

    :cond_1
    move v6, p6

    :goto_1
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    .line 4
    invoke-direct/range {v0 .. v6}, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;-><init>(Ljava/lang/String;IIIIZ)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;
    .locals 1

    .line 1
    const-class v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static values()[Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;
    .locals 1

    .line 1
    sget-object v0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->$VALUES:[Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 2
    .line 3
    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method


# virtual methods
.method public final getIconId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->iconId:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final getNameId()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->nameId:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final getNeedManualFillIn()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->needManualFillIn:Z

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final getType()I
    .locals 1

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->type:I

    .line 2
    .line 3
    return v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public final setIconId(I)V
    .locals 0

    .line 1
    iput p1, p0, Lcom/intsig/camscanner/scenariodir/data/CertificateEnum;->iconId:I

    .line 2
    .line 3
    return-void
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

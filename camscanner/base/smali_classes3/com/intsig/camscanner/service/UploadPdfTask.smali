.class public Lcom/intsig/camscanner/service/UploadPdfTask;
.super Landroid/os/AsyncTask;
.source "UploadPdfTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/service/UploadPdfTask$PdfCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Long;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private O8:[I

.field private Oo08:J

.field private oO80:Z

.field private o〇0:Lcom/intsig/camscanner/service/UploadPdfTask$PdfCallback;

.field private 〇080:I

.field private 〇o00〇〇Oo:Landroid/content/Context;

.field private 〇o〇:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/webstorage/UploadFile;",
            ">;"
        }
    .end annotation
.end field

.field private 〇〇888:Lcom/intsig/app/BaseProgressDialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;J[ILcom/intsig/camscanner/service/UploadPdfTask$PdfCallback;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/webstorage/UploadFile;",
            ">;J[I",
            "Lcom/intsig/camscanner/service/UploadPdfTask$PdfCallback;",
            "Z)V"
        }
    .end annotation

    .line 7
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 8
    iput-object p1, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->〇o00〇〇Oo:Landroid/content/Context;

    const/4 p1, 0x0

    .line 9
    iput p1, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->〇080:I

    .line 10
    iput-wide p3, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->Oo08:J

    .line 11
    iput-object p5, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->O8:[I

    .line 12
    iput-object p2, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->〇o〇:Ljava/util/ArrayList;

    .line 13
    iput-object p6, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->o〇0:Lcom/intsig/camscanner/service/UploadPdfTask$PdfCallback;

    .line 14
    iput-boolean p7, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->oO80:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/intsig/camscanner/service/UploadPdfTask$PdfCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/webstorage/UploadFile;",
            ">;",
            "Lcom/intsig/camscanner/service/UploadPdfTask$PdfCallback;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->oO80:Z

    .line 3
    iput-object p1, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->〇o00〇〇Oo:Landroid/content/Context;

    const/4 p1, 0x1

    .line 4
    iput p1, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->〇080:I

    .line 5
    iput-object p2, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->〇o〇:Ljava/util/ArrayList;

    .line 6
    iput-object p3, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->o〇0:Lcom/intsig/camscanner/service/UploadPdfTask$PdfCallback;

    return-void
.end method

.method private 〇o00〇〇Oo()V
    .locals 13

    .line 1
    iget v0, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->〇080:I

    .line 2
    .line 3
    const/4 v1, 0x1

    .line 4
    const-string v2, "UploadPdfTask"

    .line 5
    .line 6
    const/4 v3, 0x0

    .line 7
    if-ne v0, v1, :cond_0

    .line 8
    .line 9
    :goto_0
    iget-object v0, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->〇o〇:Ljava/util/ArrayList;

    .line 10
    .line 11
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    .line 12
    .line 13
    .line 14
    move-result v0

    .line 15
    if-ge v3, v0, :cond_1

    .line 16
    .line 17
    iget-object v0, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->〇o〇:Ljava/util/ArrayList;

    .line 18
    .line 19
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 20
    .line 21
    .line 22
    move-result-object v0

    .line 23
    check-cast v0, Lcom/intsig/webstorage/UploadFile;

    .line 24
    .line 25
    invoke-virtual {v0}, Lcom/intsig/webstorage/UploadFile;->〇080()J

    .line 26
    .line 27
    .line 28
    move-result-wide v4

    .line 29
    const/4 v6, 0x0

    .line 30
    iget-object v7, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->〇o00〇〇Oo:Landroid/content/Context;

    .line 31
    .line 32
    const/4 v8, 0x0

    .line 33
    const/4 v9, 0x3

    .line 34
    const/4 v10, 0x0

    .line 35
    const/4 v11, 0x0

    .line 36
    const/4 v12, 0x0

    .line 37
    invoke-static/range {v4 .. v12}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->createPdf(J[ILandroid/content/Context;Ljava/lang/String;ILcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;ZLcom/intsig/utils/ImageScaleListener;)Ljava/lang/String;

    .line 38
    .line 39
    .line 40
    move-result-object v1

    .line 41
    iput-object v1, v0, Lcom/intsig/webstorage/UploadFile;->〇OOo8〇0:Ljava/lang/String;

    .line 42
    .line 43
    iget-object v1, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->〇o〇:Ljava/util/ArrayList;

    .line 44
    .line 45
    invoke-virtual {v1, v3, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 46
    .line 47
    .line 48
    new-instance v1, Ljava/lang/StringBuilder;

    .line 49
    .line 50
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 51
    .line 52
    .line 53
    const-string v4, "path = "

    .line 54
    .line 55
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    iget-object v4, v0, Lcom/intsig/webstorage/UploadFile;->〇OOo8〇0:Ljava/lang/String;

    .line 59
    .line 60
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    .line 62
    .line 63
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 64
    .line 65
    .line 66
    move-result-object v1

    .line 67
    invoke-static {v2, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    .line 69
    .line 70
    new-instance v1, Ljava/lang/StringBuilder;

    .line 71
    .line 72
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 73
    .line 74
    .line 75
    const-string v4, "id = "

    .line 76
    .line 77
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    .line 79
    .line 80
    invoke-virtual {v0}, Lcom/intsig/webstorage/UploadFile;->〇080()J

    .line 81
    .line 82
    .line 83
    move-result-wide v4

    .line 84
    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 85
    .line 86
    .line 87
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 88
    .line 89
    .line 90
    move-result-object v0

    .line 91
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    .line 93
    .line 94
    add-int/lit8 v3, v3, 0x1

    .line 95
    .line 96
    goto :goto_0

    .line 97
    :cond_0
    if-nez v0, :cond_1

    .line 98
    .line 99
    const-string v0, "Create Pdf"

    .line 100
    .line 101
    invoke-static {v2, v0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    .line 103
    .line 104
    iget-wide v4, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->Oo08:J

    .line 105
    .line 106
    iget-object v6, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->O8:[I

    .line 107
    .line 108
    iget-object v7, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->〇o00〇〇Oo:Landroid/content/Context;

    .line 109
    .line 110
    const/4 v8, 0x0

    .line 111
    const/4 v9, 0x3

    .line 112
    const/4 v10, 0x0

    .line 113
    iget-boolean v11, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->oO80:Z

    .line 114
    .line 115
    invoke-static/range {v4 .. v11}, Lcom/intsig/camscanner/pdfengine/PDF_Util;->createPdf(J[ILandroid/content/Context;Ljava/lang/String;ILcom/intsig/camscanner/pdfengine/PDF_Util$OnPdfCreateListener;Z)Ljava/lang/String;

    .line 116
    .line 117
    .line 118
    move-result-object v0

    .line 119
    iget-object v1, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->〇o〇:Ljava/util/ArrayList;

    .line 120
    .line 121
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 122
    .line 123
    .line 124
    move-result-object v1

    .line 125
    check-cast v1, Lcom/intsig/webstorage/UploadFile;

    .line 126
    .line 127
    iput-object v0, v1, Lcom/intsig/webstorage/UploadFile;->〇OOo8〇0:Ljava/lang/String;

    .line 128
    .line 129
    iget-object v0, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->〇o〇:Ljava/util/ArrayList;

    .line 130
    .line 131
    invoke-virtual {v0, v3, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 132
    .line 133
    .line 134
    :cond_1
    return-void
    .line 135
    .line 136
    .line 137
    .line 138
    .line 139
    .line 140
    .line 141
    .line 142
    .line 143
    .line 144
    .line 145
    .line 146
    .line 147
    .line 148
    .line 149
    .line 150
    .line 151
    .line 152
    .line 153
    .line 154
    .line 155
    .line 156
    .line 157
    .line 158
    .line 159
    .line 160
    .line 161
    .line 162
    .line 163
    .line 164
    .line 165
    .line 166
    .line 167
    .line 168
    .line 169
    .line 170
    .line 171
    .line 172
    .line 173
    .line 174
    .line 175
    .line 176
    .line 177
    .line 178
    .line 179
    .line 180
    .line 181
    .line 182
    .line 183
    .line 184
    .line 185
    .line 186
    .line 187
    .line 188
    .line 189
    .line 190
    .line 191
    .line 192
    .line 193
    .line 194
    .line 195
    .line 196
    .line 197
    .line 198
    .line 199
    .line 200
    .line 201
    .line 202
    .line 203
    .line 204
    .line 205
    .line 206
    .line 207
    .line 208
    .line 209
    .line 210
    .line 211
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Long;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/service/UploadPdfTask;->〇080([Ljava/lang/Long;)Ljava/lang/Void;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/lang/Void;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/service/UploadPdfTask;->〇o〇(Ljava/lang/Void;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method protected onPreExecute()V
    .locals 3

    .line 1
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->〇o00〇〇Oo:Landroid/content/Context;

    .line 5
    .line 6
    const v1, 0x7f131d02

    .line 7
    .line 8
    .line 9
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    const/4 v2, 0x0

    .line 14
    invoke-static {v0, v1, v2, v2}, Lcom/intsig/camscanner/app/AppUtil;->OOO〇O0(Landroid/content/Context;Ljava/lang/String;ZI)Lcom/intsig/app/BaseProgressDialog;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    iput-object v0, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->〇〇888:Lcom/intsig/app/BaseProgressDialog;

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method protected varargs 〇080([Ljava/lang/Long;)Ljava/lang/Void;
    .locals 0

    .line 1
    invoke-direct {p0}, Lcom/intsig/camscanner/service/UploadPdfTask;->〇o00〇〇Oo()V

    .line 2
    .line 3
    .line 4
    const/4 p1, 0x0

    .line 5
    return-object p1
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method protected 〇o〇(Ljava/lang/Void;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->o〇0:Lcom/intsig/camscanner/service/UploadPdfTask$PdfCallback;

    .line 5
    .line 6
    if-eqz p1, :cond_0

    .line 7
    .line 8
    iget-object v0, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->〇o〇:Ljava/util/ArrayList;

    .line 9
    .line 10
    invoke-interface {p1, v0}, Lcom/intsig/camscanner/service/UploadPdfTask$PdfCallback;->〇080(Ljava/util/ArrayList;)V

    .line 11
    .line 12
    .line 13
    :cond_0
    :try_start_0
    iget-object p1, p0, Lcom/intsig/camscanner/service/UploadPdfTask;->〇〇888:Lcom/intsig/app/BaseProgressDialog;

    .line 14
    .line 15
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 16
    .line 17
    .line 18
    goto :goto_0

    .line 19
    :catch_0
    move-exception p1

    .line 20
    const-string v0, "UploadPdfTask"

    .line 21
    .line 22
    const-string v1, "Exception"

    .line 23
    .line 24
    invoke-static {v0, v1, p1}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 25
    .line 26
    .line 27
    :goto_0
    return-void
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

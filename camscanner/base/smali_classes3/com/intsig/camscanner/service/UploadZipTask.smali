.class public Lcom/intsig/camscanner/service/UploadZipTask;
.super Landroid/os/AsyncTask;
.source "UploadZipTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/camscanner/service/UploadZipTask$ZipOutInfo;,
        Lcom/intsig/camscanner/service/UploadZipTask$ZipCallback;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Long;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private O8:Lcom/intsig/app/BaseProgressDialog;

.field private Oo08:I

.field private oO80:Z

.field private o〇0:[I

.field private 〇080:Landroid/content/Context;

.field private 〇o00〇〇Oo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "Lcom/intsig/webstorage/UploadFile;",
            ">;"
        }
    .end annotation
.end field

.field private 〇o〇:Lcom/intsig/camscanner/service/UploadZipTask$ZipCallback;

.field private 〇〇888:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/intsig/camscanner/service/UploadZipTask$ZipCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/webstorage/UploadFile;",
            ">;",
            "Lcom/intsig/camscanner/service/UploadZipTask$ZipCallback;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/intsig/camscanner/service/UploadZipTask;->oO80:Z

    .line 3
    iput-object p1, p0, Lcom/intsig/camscanner/service/UploadZipTask;->〇080:Landroid/content/Context;

    .line 4
    iput-object p2, p0, Lcom/intsig/camscanner/service/UploadZipTask;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 5
    iput-object p3, p0, Lcom/intsig/camscanner/service/UploadZipTask;->〇o〇:Lcom/intsig/camscanner/service/UploadZipTask$ZipCallback;

    const/4 p1, 0x1

    .line 6
    iput p1, p0, Lcom/intsig/camscanner/service/UploadZipTask;->Oo08:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;[ILcom/intsig/camscanner/service/UploadZipTask$ZipCallback;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList<",
            "Lcom/intsig/webstorage/UploadFile;",
            ">;[I",
            "Lcom/intsig/camscanner/service/UploadZipTask$ZipCallback;",
            "Z)V"
        }
    .end annotation

    .line 7
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    .line 8
    iput-boolean v0, p0, Lcom/intsig/camscanner/service/UploadZipTask;->oO80:Z

    .line 9
    iput-object p1, p0, Lcom/intsig/camscanner/service/UploadZipTask;->〇080:Landroid/content/Context;

    .line 10
    iput-object p2, p0, Lcom/intsig/camscanner/service/UploadZipTask;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 11
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/intsig/webstorage/UploadFile;

    invoke-virtual {p1}, Lcom/intsig/webstorage/UploadFile;->〇080()J

    move-result-wide p1

    iput-wide p1, p0, Lcom/intsig/camscanner/service/UploadZipTask;->〇〇888:J

    .line 12
    iput-object p4, p0, Lcom/intsig/camscanner/service/UploadZipTask;->〇o〇:Lcom/intsig/camscanner/service/UploadZipTask$ZipCallback;

    .line 13
    iput v0, p0, Lcom/intsig/camscanner/service/UploadZipTask;->Oo08:I

    .line 14
    iput-object p3, p0, Lcom/intsig/camscanner/service/UploadZipTask;->o〇0:[I

    .line 15
    iput-boolean p5, p0, Lcom/intsig/camscanner/service/UploadZipTask;->oO80:Z

    return-void
.end method

.method public static O8(Landroid/content/Context;J[ILcom/intsig/camscanner/service/UploadZipTask$ZipOutInfo;)I
    .locals 21

    .line 1
    move-wide/from16 v0, p1

    .line 2
    .line 3
    move-object/from16 v2, p3

    .line 4
    .line 5
    move-object/from16 v3, p4

    .line 6
    .line 7
    const-string/jumbo v4, "zipOneDoc close zip file error: zos = null"

    .line 8
    .line 9
    .line 10
    const-string/jumbo v5, "title"

    .line 11
    .line 12
    .line 13
    const-string v6, "pages"

    .line 14
    .line 15
    filled-new-array {v5, v6}, [Ljava/lang/String;

    .line 16
    .line 17
    .line 18
    move-result-object v9

    .line 19
    const-string v5, "_id"

    .line 20
    .line 21
    const-string v6, "_data"

    .line 22
    .line 23
    filled-new-array {v5, v6}, [Ljava/lang/String;

    .line 24
    .line 25
    .line 26
    move-result-object v5

    .line 27
    invoke-static/range {p1 .. p2}, Lcom/intsig/camscanner/provider/Documents$Image;->〇080(J)Landroid/net/Uri;

    .line 28
    .line 29
    .line 30
    move-result-object v6

    .line 31
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 32
    .line 33
    .line 34
    move-result-object v7

    .line 35
    sget-object v8, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 36
    .line 37
    invoke-static {v8, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 38
    .line 39
    .line 40
    move-result-object v8

    .line 41
    const/4 v10, 0x0

    .line 42
    const/4 v11, 0x0

    .line 43
    const/4 v12, 0x0

    .line 44
    invoke-virtual/range {v7 .. v12}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 45
    .line 46
    .line 47
    move-result-object v7

    .line 48
    const/4 v8, 0x1

    .line 49
    const/4 v15, 0x0

    .line 50
    if-eqz v7, :cond_1

    .line 51
    .line 52
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 53
    .line 54
    .line 55
    move-result v10

    .line 56
    if-eqz v10, :cond_0

    .line 57
    .line 58
    invoke-interface {v7, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v10

    .line 62
    invoke-static {v10}, Lcom/intsig/camscanner/app/AppUtil;->Ooo8〇〇(Ljava/lang/String;)Ljava/lang/String;

    .line 63
    .line 64
    .line 65
    move-result-object v11

    .line 66
    invoke-interface {v7, v8}, Landroid/database/Cursor;->getInt(I)I

    .line 67
    .line 68
    .line 69
    move-result v12

    .line 70
    const/4 v13, 0x0

    .line 71
    goto :goto_0

    .line 72
    :cond_0
    const/4 v10, 0x0

    .line 73
    const/4 v11, 0x0

    .line 74
    const/4 v12, 0x0

    .line 75
    const/4 v13, 0x1

    .line 76
    :goto_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 77
    .line 78
    .line 79
    move-object v7, v10

    .line 80
    move-object v14, v11

    .line 81
    move/from16 v16, v13

    .line 82
    .line 83
    move v13, v12

    .line 84
    goto :goto_1

    .line 85
    :cond_1
    const/4 v7, 0x0

    .line 86
    const/4 v13, 0x0

    .line 87
    const/4 v14, 0x0

    .line 88
    const/16 v16, 0x1

    .line 89
    .line 90
    :goto_1
    if-eqz v16, :cond_2

    .line 91
    .line 92
    return v16

    .line 93
    :cond_2
    new-instance v10, Ljava/lang/StringBuilder;

    .line 94
    .line 95
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 96
    .line 97
    .line 98
    const-string v11, "doc title = "

    .line 99
    .line 100
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    .line 102
    .line 103
    invoke-virtual {v10, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 104
    .line 105
    .line 106
    const-string v11, " pageNum = "

    .line 107
    .line 108
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    .line 110
    .line 111
    invoke-virtual {v10, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 112
    .line 113
    .line 114
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 115
    .line 116
    .line 117
    move-result-object v10

    .line 118
    const-string v12, "UploadZipTask"

    .line 119
    .line 120
    invoke-static {v12, v10}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    .line 122
    .line 123
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 124
    .line 125
    .line 126
    move-result-object v10

    .line 127
    const/16 v17, 0x0

    .line 128
    .line 129
    const/16 v18, 0x0

    .line 130
    .line 131
    const-string v19, "page_num ASC"

    .line 132
    .line 133
    move-object v11, v6

    .line 134
    move-object v6, v12

    .line 135
    move-object v12, v5

    .line 136
    move v5, v13

    .line 137
    move-object/from16 v13, v17

    .line 138
    .line 139
    move-object v9, v14

    .line 140
    move-object/from16 v14, v18

    .line 141
    .line 142
    move-object/from16 v15, v19

    .line 143
    .line 144
    invoke-virtual/range {v10 .. v15}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 145
    .line 146
    .line 147
    move-result-object v10

    .line 148
    if-eqz v10, :cond_f

    .line 149
    .line 150
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    .line 151
    .line 152
    .line 153
    move-result v11

    .line 154
    if-eqz v11, :cond_e

    .line 155
    .line 156
    if-nez v2, :cond_4

    .line 157
    .line 158
    new-array v2, v5, [I

    .line 159
    .line 160
    const/4 v15, 0x0

    .line 161
    :goto_2
    if-ge v15, v5, :cond_3

    .line 162
    .line 163
    aput v15, v2, v15

    .line 164
    .line 165
    add-int/lit8 v15, v15, 0x1

    .line 166
    .line 167
    goto :goto_2

    .line 168
    :cond_3
    const/4 v5, 0x0

    .line 169
    goto :goto_3

    .line 170
    :cond_4
    array-length v11, v2

    .line 171
    const/4 v12, 0x3

    .line 172
    if-le v11, v5, :cond_5

    .line 173
    .line 174
    return v12

    .line 175
    :cond_5
    array-length v11, v2

    .line 176
    sub-int/2addr v11, v8

    .line 177
    aget v11, v2, v11

    .line 178
    .line 179
    if-ge v11, v5, :cond_d

    .line 180
    .line 181
    const/4 v5, 0x0

    .line 182
    aget v11, v2, v5

    .line 183
    .line 184
    if-gez v11, :cond_6

    .line 185
    .line 186
    goto/16 :goto_d

    .line 187
    .line 188
    :cond_6
    :goto_3
    new-instance v11, Ljava/lang/StringBuilder;

    .line 189
    .line 190
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 191
    .line 192
    .line 193
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->〇o()Ljava/lang/String;

    .line 194
    .line 195
    .line 196
    move-result-object v12

    .line 197
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 198
    .line 199
    .line 200
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 201
    .line 202
    .line 203
    move-result-wide v12

    .line 204
    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 205
    .line 206
    .line 207
    const-string v12, "/"

    .line 208
    .line 209
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 210
    .line 211
    .line 212
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 213
    .line 214
    .line 215
    move-result-object v11

    .line 216
    new-instance v12, Ljava/io/File;

    .line 217
    .line 218
    invoke-direct {v12, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 219
    .line 220
    .line 221
    invoke-virtual {v12}, Ljava/io/File;->exists()Z

    .line 222
    .line 223
    .line 224
    move-result v13

    .line 225
    if-nez v13, :cond_7

    .line 226
    .line 227
    invoke-virtual {v12}, Ljava/io/File;->mkdirs()Z

    .line 228
    .line 229
    .line 230
    move-result v12

    .line 231
    new-instance v13, Ljava/lang/StringBuilder;

    .line 232
    .line 233
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 234
    .line 235
    .line 236
    const-string v14, "mkdirs = "

    .line 237
    .line 238
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 239
    .line 240
    .line 241
    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 242
    .line 243
    .line 244
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 245
    .line 246
    .line 247
    move-result-object v12

    .line 248
    invoke-static {v6, v12}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    .line 250
    .line 251
    :cond_7
    new-instance v12, Ljava/lang/StringBuilder;

    .line 252
    .line 253
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 254
    .line 255
    .line 256
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    .line 258
    .line 259
    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    .line 261
    .line 262
    const-string v11, ".zip"

    .line 263
    .line 264
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 265
    .line 266
    .line 267
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 268
    .line 269
    .line 270
    move-result-object v11

    .line 271
    new-instance v12, Ljava/lang/StringBuilder;

    .line 272
    .line 273
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    .line 274
    .line 275
    .line 276
    const-string/jumbo v13, "zipPath = "

    .line 277
    .line 278
    .line 279
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 280
    .line 281
    .line 282
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 283
    .line 284
    .line 285
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 286
    .line 287
    .line 288
    move-result-object v12

    .line 289
    invoke-static {v6, v12}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 290
    .line 291
    .line 292
    :try_start_0
    new-instance v13, Ljava/io/BufferedOutputStream;

    .line 293
    .line 294
    new-instance v14, Ljava/io/FileOutputStream;

    .line 295
    .line 296
    new-instance v15, Ljava/io/File;

    .line 297
    .line 298
    invoke-direct {v15, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 299
    .line 300
    .line 301
    invoke-direct {v14, v15}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 302
    .line 303
    .line 304
    invoke-direct {v13, v14}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 305
    .line 306
    .line 307
    new-instance v14, Ljava/util/zip/ZipOutputStream;

    .line 308
    .line 309
    invoke-direct {v14, v13}, Ljava/util/zip/ZipOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 310
    .line 311
    .line 312
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 313
    .line 314
    .line 315
    move-result-wide v17

    .line 316
    new-instance v13, Ljava/lang/StringBuilder;

    .line 317
    .line 318
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 319
    .line 320
    .line 321
    const-string v15, "image num: "

    .line 322
    .line 323
    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 324
    .line 325
    .line 326
    array-length v15, v2

    .line 327
    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 328
    .line 329
    .line 330
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 331
    .line 332
    .line 333
    move-result-object v13

    .line 334
    invoke-virtual {v14, v13}, Ljava/util/zip/ZipOutputStream;->setComment(Ljava/lang/String;)V

    .line 335
    .line 336
    .line 337
    invoke-virtual {v14, v5}, Ljava/util/zip/ZipOutputStream;->setLevel(I)V

    .line 338
    .line 339
    .line 340
    array-length v13, v2

    .line 341
    const/4 v15, 0x0

    .line 342
    :goto_4
    if-ge v15, v13, :cond_9

    .line 343
    .line 344
    aget v5, v2, v15

    .line 345
    .line 346
    invoke-interface {v10, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 347
    .line 348
    .line 349
    move-result v19

    .line 350
    if-eqz v19, :cond_8

    .line 351
    .line 352
    invoke-interface {v10, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 353
    .line 354
    .line 355
    move-result-object v12

    .line 356
    new-instance v8, Ljava/lang/StringBuilder;

    .line 357
    .line 358
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 359
    .line 360
    .line 361
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 362
    .line 363
    .line 364
    move-object/from16 v20, v2

    .line 365
    .line 366
    const-string v2, "_"

    .line 367
    .line 368
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 369
    .line 370
    .line 371
    add-int/lit8 v5, v5, 0x1

    .line 372
    .line 373
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 374
    .line 375
    .line 376
    const-string v2, ".jpg"

    .line 377
    .line 378
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 379
    .line 380
    .line 381
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 382
    .line 383
    .line 384
    move-result-object v2

    .line 385
    invoke-static {v14, v12, v2}, Lcom/intsig/camscanner/service/UploadZipTask;->Oo08(Ljava/util/zip/ZipOutputStream;Ljava/lang/String;Ljava/lang/String;)Z

    .line 386
    .line 387
    .line 388
    goto :goto_5

    .line 389
    :cond_8
    move-object/from16 v20, v2

    .line 390
    .line 391
    :goto_5
    add-int/lit8 v15, v15, 0x1

    .line 392
    .line 393
    move-object/from16 v2, v20

    .line 394
    .line 395
    const/4 v8, 0x1

    .line 396
    goto :goto_4

    .line 397
    :cond_9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 398
    .line 399
    .line 400
    move-result-wide v8

    .line 401
    sub-long v8, v8, v17

    .line 402
    .line 403
    new-instance v2, Ljava/lang/StringBuilder;

    .line 404
    .line 405
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 406
    .line 407
    .line 408
    const-string v5, "consume time = "

    .line 409
    .line 410
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 411
    .line 412
    .line 413
    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 414
    .line 415
    .line 416
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 417
    .line 418
    .line 419
    move-result-object v2

    .line 420
    invoke-static {v6, v2}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 421
    .line 422
    .line 423
    if-eqz v3, :cond_a

    .line 424
    .line 425
    iput-wide v0, v3, Lcom/intsig/camscanner/service/UploadZipTask$ZipOutInfo;->〇o〇:J

    .line 426
    .line 427
    iput-object v7, v3, Lcom/intsig/camscanner/service/UploadZipTask$ZipOutInfo;->〇o00〇〇Oo:Ljava/lang/String;

    .line 428
    .line 429
    iput-object v11, v3, Lcom/intsig/camscanner/service/UploadZipTask$ZipOutInfo;->〇080:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 430
    .line 431
    :cond_a
    :try_start_2
    invoke-virtual {v14}, Ljava/util/zip/ZipOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 432
    .line 433
    .line 434
    goto :goto_9

    .line 435
    :catch_0
    move-exception v0

    .line 436
    move-object v1, v0

    .line 437
    new-instance v0, Ljava/io/File;

    .line 438
    .line 439
    invoke-direct {v0, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 440
    .line 441
    .line 442
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 443
    .line 444
    .line 445
    invoke-static {v6, v1}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 446
    .line 447
    .line 448
    goto :goto_8

    .line 449
    :catchall_0
    move-exception v0

    .line 450
    move-object v1, v0

    .line 451
    move-object v9, v14

    .line 452
    goto :goto_a

    .line 453
    :catch_1
    move-exception v0

    .line 454
    move-object v9, v14

    .line 455
    goto :goto_6

    .line 456
    :catchall_1
    move-exception v0

    .line 457
    move-object v1, v0

    .line 458
    const/4 v9, 0x0

    .line 459
    goto :goto_a

    .line 460
    :catch_2
    move-exception v0

    .line 461
    const/4 v9, 0x0

    .line 462
    :goto_6
    :try_start_3
    invoke-static {v6, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 463
    .line 464
    .line 465
    if-eqz v9, :cond_b

    .line 466
    .line 467
    :try_start_4
    invoke-virtual {v9}, Ljava/util/zip/ZipOutputStream;->close()V

    .line 468
    .line 469
    .line 470
    goto :goto_9

    .line 471
    :catch_3
    move-exception v0

    .line 472
    goto :goto_7

    .line 473
    :cond_b
    invoke-static {v6, v4}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 474
    .line 475
    .line 476
    goto :goto_8

    .line 477
    :goto_7
    new-instance v1, Ljava/io/File;

    .line 478
    .line 479
    invoke-direct {v1, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 480
    .line 481
    .line 482
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 483
    .line 484
    .line 485
    invoke-static {v6, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 486
    .line 487
    .line 488
    :goto_8
    const/16 v16, 0x4

    .line 489
    .line 490
    :goto_9
    move/from16 v8, v16

    .line 491
    .line 492
    goto :goto_e

    .line 493
    :catchall_2
    move-exception v0

    .line 494
    move-object v1, v0

    .line 495
    :goto_a
    if-eqz v9, :cond_c

    .line 496
    .line 497
    :try_start_5
    invoke-virtual {v9}, Ljava/util/zip/ZipOutputStream;->close()V

    .line 498
    .line 499
    .line 500
    goto :goto_c

    .line 501
    :catch_4
    move-exception v0

    .line 502
    goto :goto_b

    .line 503
    :cond_c
    invoke-static {v6, v4}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 504
    .line 505
    .line 506
    goto :goto_c

    .line 507
    :goto_b
    new-instance v2, Ljava/io/File;

    .line 508
    .line 509
    invoke-direct {v2, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 510
    .line 511
    .line 512
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 513
    .line 514
    .line 515
    invoke-static {v6, v0}, Lcom/intsig/log/LogUtils;->Oo08(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 516
    .line 517
    .line 518
    :goto_c
    throw v1

    .line 519
    :cond_d
    :goto_d
    return v12

    .line 520
    :cond_e
    const/4 v0, 0x2

    .line 521
    const/4 v8, 0x2

    .line 522
    :goto_e
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 523
    .line 524
    .line 525
    goto :goto_f

    .line 526
    :cond_f
    const/4 v8, 0x1

    .line 527
    :goto_f
    return v8
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
.end method

.method private static Oo08(Ljava/util/zip/ZipOutputStream;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .line 1
    const/4 v0, 0x0

    .line 2
    if-eqz p1, :cond_3

    .line 3
    .line 4
    if-nez p0, :cond_0

    .line 5
    .line 6
    goto/16 :goto_6

    .line 7
    .line 8
    :cond_0
    new-instance v1, Ljava/io/File;

    .line 9
    .line 10
    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 11
    .line 12
    .line 13
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    .line 14
    .line 15
    .line 16
    move-result v2

    .line 17
    const-string v3, "UploadZipTask"

    .line 18
    .line 19
    if-eqz v2, :cond_2

    .line 20
    .line 21
    invoke-virtual {v1}, Ljava/io/File;->isFile()Z

    .line 22
    .line 23
    .line 24
    move-result v2

    .line 25
    if-eqz v2, :cond_2

    .line 26
    .line 27
    new-instance p1, Ljava/util/zip/ZipEntry;

    .line 28
    .line 29
    invoke-direct {p1, p2}, Ljava/util/zip/ZipEntry;-><init>(Ljava/lang/String;)V

    .line 30
    .line 31
    .line 32
    const/4 p2, 0x0

    .line 33
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    .line 34
    .line 35
    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/util/zip/ZipException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 36
    .line 37
    .line 38
    :try_start_1
    invoke-virtual {p0, p1}, Ljava/util/zip/ZipOutputStream;->putNextEntry(Ljava/util/zip/ZipEntry;)V

    .line 39
    .line 40
    .line 41
    const/16 p1, 0x1000

    .line 42
    .line 43
    new-array p1, p1, [B

    .line 44
    .line 45
    :goto_0
    invoke-virtual {v2, p1}, Ljava/io/FileInputStream;->read([B)I

    .line 46
    .line 47
    .line 48
    move-result p2

    .line 49
    const/4 v1, -0x1

    .line 50
    if-eq p2, v1, :cond_1

    .line 51
    .line 52
    invoke-virtual {p0, p1, v0, p2}, Ljava/util/zip/ZipOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/util/zip/ZipException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 53
    .line 54
    .line 55
    goto :goto_0

    .line 56
    :cond_1
    const/4 p1, 0x1

    .line 57
    const/4 v0, 0x1

    .line 58
    goto :goto_5

    .line 59
    :catch_0
    move-exception p1

    .line 60
    move-object p2, v2

    .line 61
    goto :goto_1

    .line 62
    :catch_1
    move-exception p1

    .line 63
    move-object p2, v2

    .line 64
    goto :goto_2

    .line 65
    :catch_2
    move-exception p1

    .line 66
    move-object p2, v2

    .line 67
    goto :goto_3

    .line 68
    :catch_3
    move-exception p1

    .line 69
    :goto_1
    const-string v1, "IOException"

    .line 70
    .line 71
    invoke-static {v3, v1, p1}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 72
    .line 73
    .line 74
    goto :goto_4

    .line 75
    :catch_4
    move-exception p1

    .line 76
    :goto_2
    const-string v1, "FileNotFoundException"

    .line 77
    .line 78
    invoke-static {v3, v1, p1}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 79
    .line 80
    .line 81
    goto :goto_4

    .line 82
    :catch_5
    move-exception p1

    .line 83
    :goto_3
    const-string v1, "ZipException"

    .line 84
    .line 85
    invoke-static {v3, v1, p1}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 86
    .line 87
    .line 88
    :goto_4
    move-object v2, p2

    .line 89
    :goto_5
    :try_start_2
    invoke-virtual {p0}, Ljava/util/zip/ZipOutputStream;->closeEntry()V

    .line 90
    .line 91
    .line 92
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6

    .line 93
    .line 94
    .line 95
    goto :goto_6

    .line 96
    :catch_6
    move-exception p0

    .line 97
    const-string p1, "Exception"

    .line 98
    .line 99
    invoke-static {v3, p1, p0}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 100
    .line 101
    .line 102
    goto :goto_6

    .line 103
    :cond_2
    new-instance p0, Ljava/lang/StringBuilder;

    .line 104
    .line 105
    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    .line 106
    .line 107
    .line 108
    const-string p2, "file may be not exist filePath="

    .line 109
    .line 110
    invoke-virtual {p0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    .line 112
    .line 113
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    .line 115
    .line 116
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 117
    .line 118
    .line 119
    move-result-object p0

    .line 120
    invoke-static {v3, p0}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    .line 122
    .line 123
    :cond_3
    :goto_6
    return v0
.end method

.method private 〇o00〇〇Oo(Landroid/net/Uri;)Ljava/lang/String;
    .locals 7

    .line 1
    iget-object v0, p0, Lcom/intsig/camscanner/service/UploadZipTask;->〇080:Landroid/content/Context;

    .line 2
    .line 3
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 4
    .line 5
    .line 6
    move-result-object v1

    .line 7
    const-string/jumbo v0, "title"

    .line 8
    .line 9
    .line 10
    filled-new-array {v0}, [Ljava/lang/String;

    .line 11
    .line 12
    .line 13
    move-result-object v3

    .line 14
    const/4 v4, 0x0

    .line 15
    const/4 v5, 0x0

    .line 16
    const/4 v6, 0x0

    .line 17
    move-object v2, p1

    .line 18
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 19
    .line 20
    .line 21
    move-result-object p1

    .line 22
    const/4 v0, 0x0

    .line 23
    if-eqz p1, :cond_1

    .line 24
    .line 25
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 26
    .line 27
    .line 28
    move-result v1

    .line 29
    if-eqz v1, :cond_0

    .line 30
    .line 31
    const/4 v0, 0x0

    .line 32
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 33
    .line 34
    .line 35
    move-result-object v0

    .line 36
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 37
    .line 38
    .line 39
    :cond_1
    return-object v0
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Long;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/service/UploadZipTask;->〇080([Ljava/lang/Long;)Ljava/lang/Void;

    .line 4
    .line 5
    .line 6
    move-result-object p1

    .line 7
    return-object p1
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/lang/Void;

    .line 2
    .line 3
    invoke-virtual {p0, p1}, Lcom/intsig/camscanner/service/UploadZipTask;->〇o〇(Ljava/lang/Void;)V

    .line 4
    .line 5
    .line 6
    return-void
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method protected onPreExecute()V
    .locals 3

    .line 1
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 2
    .line 3
    .line 4
    iget-object v0, p0, Lcom/intsig/camscanner/service/UploadZipTask;->〇080:Landroid/content/Context;

    .line 5
    .line 6
    const v1, 0x7f131d02

    .line 7
    .line 8
    .line 9
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    .line 10
    .line 11
    .line 12
    move-result-object v1

    .line 13
    const/4 v2, 0x0

    .line 14
    invoke-static {v0, v1, v2, v2}, Lcom/intsig/camscanner/app/AppUtil;->OOO〇O0(Landroid/content/Context;Ljava/lang/String;ZI)Lcom/intsig/app/BaseProgressDialog;

    .line 15
    .line 16
    .line 17
    move-result-object v0

    .line 18
    iput-object v0, p0, Lcom/intsig/camscanner/service/UploadZipTask;->O8:Lcom/intsig/app/BaseProgressDialog;

    .line 19
    .line 20
    invoke-virtual {v0}, Lcom/intsig/app/AlertDialog;->show()V

    .line 21
    .line 22
    .line 23
    return-void
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method protected varargs 〇080([Ljava/lang/Long;)Ljava/lang/Void;
    .locals 19

    .line 1
    move-object/from16 v0, p0

    .line 2
    .line 3
    iget v1, v0, Lcom/intsig/camscanner/service/UploadZipTask;->Oo08:I

    .line 4
    .line 5
    const/4 v2, 0x0

    .line 6
    const-string v3, "UploadZipTask"

    .line 7
    .line 8
    const/4 v4, 0x1

    .line 9
    if-ne v1, v4, :cond_2

    .line 10
    .line 11
    iget-object v1, v0, Lcom/intsig/camscanner/service/UploadZipTask;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 12
    .line 13
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    .line 14
    .line 15
    .line 16
    move-result-object v1

    .line 17
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    .line 18
    .line 19
    .line 20
    move-result v4

    .line 21
    if-eqz v4, :cond_a

    .line 22
    .line 23
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 24
    .line 25
    .line 26
    move-result-object v4

    .line 27
    check-cast v4, Lcom/intsig/webstorage/UploadFile;

    .line 28
    .line 29
    new-instance v5, Lcom/intsig/camscanner/service/UploadZipTask$ZipOutInfo;

    .line 30
    .line 31
    invoke-direct {v5}, Lcom/intsig/camscanner/service/UploadZipTask$ZipOutInfo;-><init>()V

    .line 32
    .line 33
    .line 34
    iget-object v6, v0, Lcom/intsig/camscanner/service/UploadZipTask;->〇080:Landroid/content/Context;

    .line 35
    .line 36
    invoke-virtual {v4}, Lcom/intsig/webstorage/UploadFile;->〇080()J

    .line 37
    .line 38
    .line 39
    move-result-wide v7

    .line 40
    invoke-static {v6, v7, v8, v2, v5}, Lcom/intsig/camscanner/service/UploadZipTask;->O8(Landroid/content/Context;J[ILcom/intsig/camscanner/service/UploadZipTask$ZipOutInfo;)I

    .line 41
    .line 42
    .line 43
    move-result v6

    .line 44
    new-instance v7, Ljava/lang/StringBuilder;

    .line 45
    .line 46
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 47
    .line 48
    .line 49
    const-string/jumbo v8, "wrapOneDoc res = "

    .line 50
    .line 51
    .line 52
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    .line 54
    .line 55
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 56
    .line 57
    .line 58
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 59
    .line 60
    .line 61
    move-result-object v7

    .line 62
    invoke-static {v3, v7}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    .line 64
    .line 65
    iget-object v5, v5, Lcom/intsig/camscanner/service/UploadZipTask$ZipOutInfo;->〇080:Ljava/lang/String;

    .line 66
    .line 67
    iput-object v5, v4, Lcom/intsig/webstorage/UploadFile;->〇OOo8〇0:Ljava/lang/String;

    .line 68
    .line 69
    if-nez v6, :cond_1

    .line 70
    .line 71
    goto :goto_0

    .line 72
    :cond_1
    iget-object v4, v0, Lcom/intsig/camscanner/service/UploadZipTask;->〇o〇:Lcom/intsig/camscanner/service/UploadZipTask$ZipCallback;

    .line 73
    .line 74
    if-eqz v4, :cond_0

    .line 75
    .line 76
    invoke-interface {v4, v6}, Lcom/intsig/camscanner/service/UploadZipTask$ZipCallback;->〇o00〇〇Oo(I)V

    .line 77
    .line 78
    .line 79
    goto :goto_0

    .line 80
    :cond_2
    iget-object v1, v0, Lcom/intsig/camscanner/service/UploadZipTask;->o〇0:[I

    .line 81
    .line 82
    array-length v1, v1

    .line 83
    const/4 v5, 0x0

    .line 84
    if-ne v1, v4, :cond_8

    .line 85
    .line 86
    const-string v1, "_data"

    .line 87
    .line 88
    const-string v6, "page_num"

    .line 89
    .line 90
    const-string v7, "_id"

    .line 91
    .line 92
    filled-new-array {v7, v1, v6}, [Ljava/lang/String;

    .line 93
    .line 94
    .line 95
    move-result-object v10

    .line 96
    iget-wide v6, v0, Lcom/intsig/camscanner/service/UploadZipTask;->〇〇888:J

    .line 97
    .line 98
    invoke-static {v6, v7}, Lcom/intsig/camscanner/provider/Documents$Image;->〇080(J)Landroid/net/Uri;

    .line 99
    .line 100
    .line 101
    move-result-object v9

    .line 102
    iget-object v1, v0, Lcom/intsig/camscanner/service/UploadZipTask;->〇080:Landroid/content/Context;

    .line 103
    .line 104
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    .line 105
    .line 106
    .line 107
    move-result-object v8

    .line 108
    const/4 v11, 0x0

    .line 109
    const/4 v12, 0x0

    .line 110
    const-string v13, "page_num ASC"

    .line 111
    .line 112
    invoke-virtual/range {v8 .. v13}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .line 113
    .line 114
    .line 115
    move-result-object v1

    .line 116
    if-nez v1, :cond_3

    .line 117
    .line 118
    const-string v1, "cursor == null"

    .line 119
    .line 120
    invoke-static {v3, v1}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 121
    .line 122
    .line 123
    goto/16 :goto_2

    .line 124
    .line 125
    :cond_3
    iget-object v6, v0, Lcom/intsig/camscanner/service/UploadZipTask;->o〇0:[I

    .line 126
    .line 127
    aget v6, v6, v5

    .line 128
    .line 129
    invoke-interface {v1, v6}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 130
    .line 131
    .line 132
    move-result v6

    .line 133
    if-eqz v6, :cond_7

    .line 134
    .line 135
    new-instance v6, Ljava/lang/StringBuilder;

    .line 136
    .line 137
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 138
    .line 139
    .line 140
    const-string v7, "mPages[0] = "

    .line 141
    .line 142
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    .line 144
    .line 145
    iget-object v7, v0, Lcom/intsig/camscanner/service/UploadZipTask;->o〇0:[I

    .line 146
    .line 147
    aget v7, v7, v5

    .line 148
    .line 149
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 150
    .line 151
    .line 152
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 153
    .line 154
    .line 155
    move-result-object v6

    .line 156
    invoke-static {v3, v6}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    .line 158
    .line 159
    new-instance v6, Ljava/lang/StringBuilder;

    .line 160
    .line 161
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 162
    .line 163
    .line 164
    const-string v7, "count = "

    .line 165
    .line 166
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 167
    .line 168
    .line 169
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    .line 170
    .line 171
    .line 172
    move-result v7

    .line 173
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 174
    .line 175
    .line 176
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 177
    .line 178
    .line 179
    move-result-object v6

    .line 180
    invoke-static {v3, v6}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    .line 182
    .line 183
    new-instance v6, Ljava/util/ArrayList;

    .line 184
    .line 185
    invoke-direct {v6, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 186
    .line 187
    .line 188
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    .line 189
    .line 190
    .line 191
    move-result-object v4

    .line 192
    const/4 v7, 0x2

    .line 193
    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    .line 194
    .line 195
    .line 196
    move-result v7

    .line 197
    new-instance v8, Ljava/lang/StringBuilder;

    .line 198
    .line 199
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 200
    .line 201
    .line 202
    const-string v9, "path = "

    .line 203
    .line 204
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    .line 206
    .line 207
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 208
    .line 209
    .line 210
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 211
    .line 212
    .line 213
    move-result-object v8

    .line 214
    invoke-static {v3, v8}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    .line 216
    .line 217
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    .line 218
    .line 219
    .line 220
    move-result v8

    .line 221
    if-nez v8, :cond_6

    .line 222
    .line 223
    sget-object v8, Lcom/intsig/camscanner/provider/Documents$Document;->〇080:Landroid/net/Uri;

    .line 224
    .line 225
    iget-wide v9, v0, Lcom/intsig/camscanner/service/UploadZipTask;->〇〇888:J

    .line 226
    .line 227
    invoke-static {v8, v9, v10}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    .line 228
    .line 229
    .line 230
    move-result-object v8

    .line 231
    invoke-direct {v0, v8}, Lcom/intsig/camscanner/service/UploadZipTask;->〇o00〇〇Oo(Landroid/net/Uri;)Ljava/lang/String;

    .line 232
    .line 233
    .line 234
    move-result-object v8

    .line 235
    invoke-static {v8}, Lcom/intsig/camscanner/app/AppUtil;->Ooo8〇〇(Ljava/lang/String;)Ljava/lang/String;

    .line 236
    .line 237
    .line 238
    move-result-object v8

    .line 239
    new-instance v9, Ljava/lang/StringBuilder;

    .line 240
    .line 241
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 242
    .line 243
    .line 244
    invoke-static {}, Lcom/intsig/camscanner/util/SDStorageManager;->〇o()Ljava/lang/String;

    .line 245
    .line 246
    .line 247
    move-result-object v10

    .line 248
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    .line 250
    .line 251
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    .line 252
    .line 253
    .line 254
    move-result-wide v10

    .line 255
    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 256
    .line 257
    .line 258
    const-string v10, "/"

    .line 259
    .line 260
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 261
    .line 262
    .line 263
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 264
    .line 265
    .line 266
    move-result-object v9

    .line 267
    new-instance v10, Ljava/io/File;

    .line 268
    .line 269
    invoke-direct {v10, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 270
    .line 271
    .line 272
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    .line 273
    .line 274
    .line 275
    move-result v11

    .line 276
    if-nez v11, :cond_4

    .line 277
    .line 278
    invoke-virtual {v10}, Ljava/io/File;->mkdirs()Z

    .line 279
    .line 280
    .line 281
    move-result v10

    .line 282
    new-instance v11, Ljava/lang/StringBuilder;

    .line 283
    .line 284
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 285
    .line 286
    .line 287
    const-string v12, "mkdirs = "

    .line 288
    .line 289
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 290
    .line 291
    .line 292
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 293
    .line 294
    .line 295
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 296
    .line 297
    .line 298
    move-result-object v10

    .line 299
    invoke-static {v3, v10}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    .line 301
    .line 302
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    .line 303
    .line 304
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 305
    .line 306
    .line 307
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 308
    .line 309
    .line 310
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    .line 312
    .line 313
    const-string v10, ".jpg"

    .line 314
    .line 315
    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 316
    .line 317
    .line 318
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 319
    .line 320
    .line 321
    move-result-object v3

    .line 322
    iget-boolean v11, v0, Lcom/intsig/camscanner/service/UploadZipTask;->oO80:Z

    .line 323
    .line 324
    const-string v12, "_"

    .line 325
    .line 326
    if-eqz v11, :cond_5

    .line 327
    .line 328
    new-instance v3, Ljava/lang/StringBuilder;

    .line 329
    .line 330
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 331
    .line 332
    .line 333
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 334
    .line 335
    .line 336
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 337
    .line 338
    .line 339
    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 340
    .line 341
    .line 342
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 343
    .line 344
    .line 345
    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    .line 347
    .line 348
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 349
    .line 350
    .line 351
    move-result-object v3

    .line 352
    :cond_5
    invoke-static {v4, v3}, Lcom/intsig/utils/FileUtil;->oO80(Ljava/lang/String;Ljava/lang/String;)Z

    .line 353
    .line 354
    .line 355
    new-instance v4, Lcom/intsig/webstorage/UploadFile;

    .line 356
    .line 357
    invoke-interface {v1, v5}, Landroid/database/Cursor;->getLong(I)J

    .line 358
    .line 359
    .line 360
    move-result-wide v14

    .line 361
    new-instance v5, Ljava/lang/StringBuilder;

    .line 362
    .line 363
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 364
    .line 365
    .line 366
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 367
    .line 368
    .line 369
    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 370
    .line 371
    .line 372
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 373
    .line 374
    .line 375
    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 376
    .line 377
    .line 378
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 379
    .line 380
    .line 381
    move-result-object v17

    .line 382
    const/16 v18, 0x1

    .line 383
    .line 384
    move-object v13, v4

    .line 385
    move-object/from16 v16, v3

    .line 386
    .line 387
    invoke-direct/range {v13 .. v18}, Lcom/intsig/webstorage/UploadFile;-><init>(JLjava/lang/String;Ljava/lang/String;I)V

    .line 388
    .line 389
    .line 390
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 391
    .line 392
    .line 393
    :cond_6
    iget-object v3, v0, Lcom/intsig/camscanner/service/UploadZipTask;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 394
    .line 395
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 396
    .line 397
    .line 398
    iput-object v6, v0, Lcom/intsig/camscanner/service/UploadZipTask;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 399
    .line 400
    goto :goto_1

    .line 401
    :cond_7
    const-string v4, "moveToPosition failed"

    .line 402
    .line 403
    invoke-static {v3, v4}, Lcom/intsig/log/LogUtils;->〇o〇(Ljava/lang/String;Ljava/lang/String;)V

    .line 404
    .line 405
    .line 406
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 407
    .line 408
    .line 409
    goto :goto_2

    .line 410
    :cond_8
    new-instance v1, Lcom/intsig/camscanner/service/UploadZipTask$ZipOutInfo;

    .line 411
    .line 412
    invoke-direct {v1}, Lcom/intsig/camscanner/service/UploadZipTask$ZipOutInfo;-><init>()V

    .line 413
    .line 414
    .line 415
    iget-object v3, v0, Lcom/intsig/camscanner/service/UploadZipTask;->〇080:Landroid/content/Context;

    .line 416
    .line 417
    iget-wide v6, v0, Lcom/intsig/camscanner/service/UploadZipTask;->〇〇888:J

    .line 418
    .line 419
    iget-object v4, v0, Lcom/intsig/camscanner/service/UploadZipTask;->o〇0:[I

    .line 420
    .line 421
    invoke-static {v3, v6, v7, v4, v1}, Lcom/intsig/camscanner/service/UploadZipTask;->O8(Landroid/content/Context;J[ILcom/intsig/camscanner/service/UploadZipTask$ZipOutInfo;)I

    .line 422
    .line 423
    .line 424
    move-result v3

    .line 425
    if-nez v3, :cond_9

    .line 426
    .line 427
    iget-object v3, v0, Lcom/intsig/camscanner/service/UploadZipTask;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 428
    .line 429
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    .line 430
    .line 431
    .line 432
    move-result-object v3

    .line 433
    check-cast v3, Lcom/intsig/webstorage/UploadFile;

    .line 434
    .line 435
    iget-object v1, v1, Lcom/intsig/camscanner/service/UploadZipTask$ZipOutInfo;->〇080:Ljava/lang/String;

    .line 436
    .line 437
    iput-object v1, v3, Lcom/intsig/webstorage/UploadFile;->〇OOo8〇0:Ljava/lang/String;

    .line 438
    .line 439
    goto :goto_2

    .line 440
    :cond_9
    iget-object v1, v0, Lcom/intsig/camscanner/service/UploadZipTask;->〇o〇:Lcom/intsig/camscanner/service/UploadZipTask$ZipCallback;

    .line 441
    .line 442
    if-eqz v1, :cond_a

    .line 443
    .line 444
    invoke-interface {v1, v3}, Lcom/intsig/camscanner/service/UploadZipTask$ZipCallback;->〇o00〇〇Oo(I)V

    .line 445
    .line 446
    .line 447
    :cond_a
    :goto_2
    return-object v2
    .line 448
    .line 449
    .line 450
    .line 451
    .line 452
    .line 453
    .line 454
    .line 455
    .line 456
    .line 457
    .line 458
    .line 459
    .line 460
    .line 461
    .line 462
    .line 463
    .line 464
    .line 465
    .line 466
    .line 467
    .line 468
    .line 469
    .line 470
    .line 471
    .line 472
    .line 473
    .line 474
    .line 475
    .line 476
    .line 477
    .line 478
    .line 479
    .line 480
    .line 481
    .line 482
    .line 483
    .line 484
    .line 485
    .line 486
    .line 487
    .line 488
    .line 489
    .line 490
    .line 491
    .line 492
    .line 493
    .line 494
    .line 495
    .line 496
    .line 497
    .line 498
    .line 499
    .line 500
    .line 501
    .line 502
    .line 503
    .line 504
    .line 505
    .line 506
    .line 507
    .line 508
    .line 509
    .line 510
    .line 511
    .line 512
    .line 513
    .line 514
    .line 515
    .line 516
    .line 517
    .line 518
    .line 519
    .line 520
    .line 521
    .line 522
    .line 523
    .line 524
    .line 525
    .line 526
    .line 527
    .line 528
    .line 529
    .line 530
    .line 531
    .line 532
    .line 533
    .line 534
    .line 535
    .line 536
    .line 537
    .line 538
    .line 539
    .line 540
    .line 541
    .line 542
    .line 543
    .line 544
    .line 545
    .line 546
    .line 547
    .line 548
    .line 549
    .line 550
    .line 551
    .line 552
    .line 553
    .line 554
    .line 555
    .line 556
    .line 557
    .line 558
    .line 559
    .line 560
    .line 561
    .line 562
    .line 563
    .line 564
    .line 565
    .line 566
    .line 567
    .line 568
    .line 569
    .line 570
    .line 571
    .line 572
    .line 573
    .line 574
    .line 575
    .line 576
    .line 577
    .line 578
    .line 579
    .line 580
    .line 581
    .line 582
    .line 583
    .line 584
    .line 585
    .line 586
    .line 587
    .line 588
    .line 589
    .line 590
    .line 591
    .line 592
    .line 593
    .line 594
    .line 595
    .line 596
    .line 597
    .line 598
    .line 599
    .line 600
    .line 601
    .line 602
    .line 603
    .line 604
    .line 605
    .line 606
    .line 607
    .line 608
    .line 609
    .line 610
    .line 611
    .line 612
    .line 613
    .line 614
    .line 615
    .line 616
    .line 617
    .line 618
    .line 619
    .line 620
    .line 621
    .line 622
    .line 623
    .line 624
    .line 625
    .line 626
    .line 627
    .line 628
    .line 629
    .line 630
    .line 631
    .line 632
    .line 633
    .line 634
    .line 635
    .line 636
    .line 637
    .line 638
    .line 639
    .line 640
    .line 641
    .line 642
    .line 643
    .line 644
    .line 645
    .line 646
    .line 647
    .line 648
    .line 649
    .line 650
    .line 651
    .line 652
    .line 653
    .line 654
    .line 655
    .line 656
    .line 657
    .line 658
    .line 659
    .line 660
    .line 661
    .line 662
    .line 663
    .line 664
    .line 665
    .line 666
    .line 667
    .line 668
    .line 669
    .line 670
    .line 671
    .line 672
    .line 673
    .line 674
    .line 675
    .line 676
    .line 677
    .line 678
    .line 679
    .line 680
    .line 681
    .line 682
    .line 683
    .line 684
    .line 685
    .line 686
    .line 687
    .line 688
    .line 689
    .line 690
    .line 691
    .line 692
    .line 693
    .line 694
    .line 695
    .line 696
    .line 697
    .line 698
    .line 699
    .line 700
    .line 701
    .line 702
    .line 703
    .line 704
    .line 705
    .line 706
    .line 707
    .line 708
    .line 709
    .line 710
    .line 711
    .line 712
    .line 713
    .line 714
    .line 715
    .line 716
    .line 717
    .line 718
    .line 719
    .line 720
    .line 721
    .line 722
    .line 723
    .line 724
    .line 725
    .line 726
    .line 727
    .line 728
    .line 729
    .line 730
    .line 731
    .line 732
    .line 733
    .line 734
    .line 735
    .line 736
    .line 737
    .line 738
    .line 739
    .line 740
    .line 741
    .line 742
    .line 743
    .line 744
    .line 745
    .line 746
    .line 747
    .line 748
    .line 749
    .line 750
    .line 751
    .line 752
    .line 753
    .line 754
    .line 755
    .line 756
    .line 757
    .line 758
    .line 759
    .line 760
    .line 761
    .line 762
    .line 763
    .line 764
    .line 765
    .line 766
    .line 767
    .line 768
    .line 769
    .line 770
    .line 771
    .line 772
    .line 773
    .line 774
    .line 775
    .line 776
    .line 777
    .line 778
    .line 779
    .line 780
    .line 781
    .line 782
    .line 783
    .line 784
    .line 785
    .line 786
    .line 787
    .line 788
    .line 789
    .line 790
    .line 791
    .line 792
    .line 793
    .line 794
    .line 795
    .line 796
    .line 797
    .line 798
    .line 799
    .line 800
    .line 801
    .line 802
    .line 803
    .line 804
    .line 805
    .line 806
    .line 807
    .line 808
    .line 809
    .line 810
    .line 811
    .line 812
    .line 813
    .line 814
    .line 815
    .line 816
    .line 817
    .line 818
    .line 819
    .line 820
    .line 821
    .line 822
    .line 823
    .line 824
.end method

.method protected 〇o〇(Ljava/lang/Void;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 2
    .line 3
    .line 4
    iget-object p1, p0, Lcom/intsig/camscanner/service/UploadZipTask;->〇o〇:Lcom/intsig/camscanner/service/UploadZipTask$ZipCallback;

    .line 5
    .line 6
    if-eqz p1, :cond_1

    .line 7
    .line 8
    iget v0, p0, Lcom/intsig/camscanner/service/UploadZipTask;->Oo08:I

    .line 9
    .line 10
    const/4 v1, 0x1

    .line 11
    const/4 v2, 0x0

    .line 12
    if-ne v0, v1, :cond_0

    .line 13
    .line 14
    iget-object v0, p0, Lcom/intsig/camscanner/service/UploadZipTask;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 15
    .line 16
    invoke-interface {p1, v0, v2}, Lcom/intsig/camscanner/service/UploadZipTask$ZipCallback;->〇080(Ljava/util/ArrayList;I)V

    .line 17
    .line 18
    .line 19
    goto :goto_0

    .line 20
    :cond_0
    iget-object v0, p0, Lcom/intsig/camscanner/service/UploadZipTask;->〇o00〇〇Oo:Ljava/util/ArrayList;

    .line 21
    .line 22
    invoke-interface {p1, v0, v2}, Lcom/intsig/camscanner/service/UploadZipTask$ZipCallback;->〇080(Ljava/util/ArrayList;I)V

    .line 23
    .line 24
    .line 25
    :cond_1
    :goto_0
    :try_start_0
    iget-object p1, p0, Lcom/intsig/camscanner/service/UploadZipTask;->O8:Lcom/intsig/app/BaseProgressDialog;

    .line 26
    .line 27
    invoke-virtual {p1}, Lcom/intsig/app/AlertDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 28
    .line 29
    .line 30
    goto :goto_1

    .line 31
    :catch_0
    move-exception p1

    .line 32
    const-string v0, "UploadZipTask"

    .line 33
    .line 34
    const-string v1, "Exception"

    .line 35
    .line 36
    invoke-static {v0, v1, p1}, Lcom/intsig/log/LogUtils;->O8(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 37
    .line 38
    .line 39
    :goto_1
    return-void
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
    .line 59
    .line 60
    .line 61
    .line 62
    .line 63
    .line 64
    .line 65
    .line 66
    .line 67
    .line 68
    .line 69
    .line 70
    .line 71
.end method

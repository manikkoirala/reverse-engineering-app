.class public final Lcom/intsig/android/camerax/CameraXPreview;
.super Lcom/google/android/camera/PreviewImpl;
.source "CameraXPreview.kt"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/intsig/android/camerax/CameraXPreview$WhenMappings;
    }
.end annotation

.annotation runtime Lkotlin/Metadata;
.end annotation


# instance fields
.field private 〇080:Landroidx/camera/view/PreviewView;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;I)V
    .locals 2
    .param p1    # Landroid/view/ViewGroup;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    .line 1
    const-string v0, "parent"

    .line 2
    .line 3
    invoke-static {p1, v0}, Lkotlin/jvm/internal/Intrinsics;->checkNotNullParameter(Ljava/lang/Object;Ljava/lang/String;)V

    .line 4
    .line 5
    .line 6
    invoke-direct {p0}, Lcom/google/android/camera/PreviewImpl;-><init>()V

    .line 7
    .line 8
    .line 9
    new-instance v0, Landroidx/camera/view/PreviewView;

    .line 10
    .line 11
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    .line 12
    .line 13
    .line 14
    move-result-object v1

    .line 15
    invoke-direct {v0, v1}, Landroidx/camera/view/PreviewView;-><init>(Landroid/content/Context;)V

    .line 16
    .line 17
    .line 18
    iput-object v0, p0, Lcom/intsig/android/camerax/CameraXPreview;->〇080:Landroidx/camera/view/PreviewView;

    .line 19
    .line 20
    new-instance v0, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;

    .line 21
    .line 22
    const/4 v1, -0x1

    .line 23
    invoke-direct {v0, v1, v1}, Landroidx/constraintlayout/widget/ConstraintLayout$LayoutParams;-><init>(II)V

    .line 24
    .line 25
    .line 26
    iget-object v1, p0, Lcom/intsig/android/camerax/CameraXPreview;->〇080:Landroidx/camera/view/PreviewView;

    .line 27
    .line 28
    if-nez v1, :cond_0

    .line 29
    .line 30
    goto :goto_0

    .line 31
    :cond_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 32
    .line 33
    .line 34
    :goto_0
    invoke-direct {p0, p2}, Lcom/intsig/android/camerax/CameraXPreview;->OO0o〇〇〇〇0(I)V

    .line 35
    .line 36
    .line 37
    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 38
    .line 39
    .line 40
    iget-object p2, p0, Lcom/intsig/android/camerax/CameraXPreview;->〇080:Landroidx/camera/view/PreviewView;

    .line 41
    .line 42
    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 43
    .line 44
    .line 45
    return-void
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method private final OO0o〇〇〇〇0(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/android/camerax/CameraXPreview;->〇080:Landroidx/camera/view/PreviewView;

    .line 2
    .line 3
    if-nez v0, :cond_0

    .line 4
    .line 5
    goto :goto_1

    .line 6
    :cond_0
    sget-object v1, Lcom/google/android/camera/data/PreviewMode;->O8:Lcom/google/android/camera/data/PreviewMode$Companion;

    .line 7
    .line 8
    invoke-virtual {v1}, Lcom/google/android/camera/data/PreviewMode$Companion;->〇o〇()I

    .line 9
    .line 10
    .line 11
    move-result v1

    .line 12
    if-ne p1, v1, :cond_1

    .line 13
    .line 14
    sget-object p1, Landroidx/camera/view/PreviewView$ImplementationMode;->COMPATIBLE:Landroidx/camera/view/PreviewView$ImplementationMode;

    .line 15
    .line 16
    goto :goto_0

    .line 17
    :cond_1
    sget-object p1, Landroidx/camera/view/PreviewView$ImplementationMode;->PERFORMANCE:Landroidx/camera/view/PreviewView$ImplementationMode;

    .line 18
    .line 19
    :goto_0
    invoke-virtual {v0, p1}, Landroidx/camera/view/PreviewView;->setImplementationMode(Landroidx/camera/view/PreviewView$ImplementationMode;)V

    .line 20
    .line 21
    .line 22
    :goto_1
    return-void
.end method


# virtual methods
.method public O8()I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/android/camerax/CameraXPreview;->〇080:Landroidx/camera/view/PreviewView;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroidx/camera/view/PreviewView;->getImplementationMode()Landroidx/camera/view/PreviewView$ImplementationMode;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    if-nez v0, :cond_1

    .line 12
    .line 13
    const/4 v0, -0x1

    .line 14
    goto :goto_1

    .line 15
    :cond_1
    sget-object v1, Lcom/intsig/android/camerax/CameraXPreview$WhenMappings;->〇080:[I

    .line 16
    .line 17
    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    .line 18
    .line 19
    .line 20
    move-result v0

    .line 21
    aget v0, v1, v0

    .line 22
    .line 23
    :goto_1
    const/4 v1, 0x1

    .line 24
    if-eq v0, v1, :cond_3

    .line 25
    .line 26
    const/4 v1, 0x2

    .line 27
    if-eq v0, v1, :cond_2

    .line 28
    .line 29
    sget-object v0, Lcom/google/android/camera/data/PreviewMode;->O8:Lcom/google/android/camera/data/PreviewMode$Companion;

    .line 30
    .line 31
    invoke-virtual {v0}, Lcom/google/android/camera/data/PreviewMode$Companion;->〇o00〇〇Oo()I

    .line 32
    .line 33
    .line 34
    move-result v0

    .line 35
    goto :goto_2

    .line 36
    :cond_2
    sget-object v0, Lcom/google/android/camera/data/PreviewMode;->O8:Lcom/google/android/camera/data/PreviewMode$Companion;

    .line 37
    .line 38
    invoke-virtual {v0}, Lcom/google/android/camera/data/PreviewMode$Companion;->〇o00〇〇Oo()I

    .line 39
    .line 40
    .line 41
    move-result v0

    .line 42
    goto :goto_2

    .line 43
    :cond_3
    sget-object v0, Lcom/google/android/camera/data/PreviewMode;->O8:Lcom/google/android/camera/data/PreviewMode$Companion;

    .line 44
    .line 45
    invoke-virtual {v0}, Lcom/google/android/camera/data/PreviewMode$Companion;->〇o〇()I

    .line 46
    .line 47
    .line 48
    move-result v0

    .line 49
    :goto_2
    return v0
    .line 50
    .line 51
    .line 52
    .line 53
    .line 54
    .line 55
    .line 56
    .line 57
    .line 58
.end method

.method public Oo08()Landroid/view/View;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/intsig/android/camerax/CameraXPreview;->〇080:Landroidx/camera/view/PreviewView;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    const/4 v1, 0x0

    .line 6
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    .line 7
    .line 8
    .line 9
    move-result-object v0

    .line 10
    goto :goto_0

    .line 11
    :cond_0
    const/4 v0, 0x0

    .line 12
    :goto_0
    return-object v0
    .line 13
    .line 14
    .line 15
.end method

.method public 〇o00〇〇Oo()Landroid/graphics/Bitmap;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/android/camerax/CameraXPreview;->〇080:Landroidx/camera/view/PreviewView;

    .line 2
    .line 3
    if-eqz v0, :cond_0

    .line 4
    .line 5
    invoke-virtual {v0}, Landroidx/camera/view/PreviewView;->getBitmap()Landroid/graphics/Bitmap;

    .line 6
    .line 7
    .line 8
    move-result-object v0

    .line 9
    goto :goto_0

    .line 10
    :cond_0
    const/4 v0, 0x0

    .line 11
    :goto_0
    return-object v0
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇o〇()Landroid/view/View;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/intsig/android/camerax/CameraXPreview;->〇080:Landroidx/camera/view/PreviewView;

    .line 2
    .line 3
    return-object v0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public 〇〇888()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    .line 2
    return v0
    .line 3
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.class public final enum Lcom/dropbox/core/v2/teamlog/EventTypeArg;
.super Ljava/lang/Enum;
.source "EventTypeArg.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dropbox/core/v2/teamlog/EventTypeArg$Serializer;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/dropbox/core/v2/teamlog/EventTypeArg;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum ACCOUNT_CAPTURE_CHANGE_AVAILABILITY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum ACCOUNT_CAPTURE_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum ACCOUNT_CAPTURE_MIGRATE_ACCOUNT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum ACCOUNT_CAPTURE_NOTIFICATION_EMAILS_SENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum ACCOUNT_CAPTURE_RELINQUISH_ACCOUNT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum ACCOUNT_LOCK_OR_UNLOCKED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum ALLOW_DOWNLOAD_DISABLED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum ALLOW_DOWNLOAD_ENABLED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum APP_LINK_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum APP_LINK_USER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum APP_UNLINK_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum APP_UNLINK_USER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum BINDER_ADD_PAGE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum BINDER_ADD_SECTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum BINDER_REMOVE_PAGE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum BINDER_REMOVE_SECTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum BINDER_RENAME_PAGE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum BINDER_RENAME_SECTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum BINDER_REORDER_PAGE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum BINDER_REORDER_SECTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum CAMERA_UPLOADS_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum CHANGED_ENTERPRISE_ADMIN_ROLE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum CHANGED_ENTERPRISE_CONNECTED_TEAM_STATUS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum COLLECTION_SHARE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum CREATE_FOLDER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum CREATE_TEAM_INVITE_LINK:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DATA_PLACEMENT_RESTRICTION_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DATA_PLACEMENT_RESTRICTION_SATISFY_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DELETE_TEAM_INVITE_LINK:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DEVICE_APPROVALS_ADD_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DEVICE_APPROVALS_CHANGE_DESKTOP_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DEVICE_APPROVALS_CHANGE_MOBILE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DEVICE_APPROVALS_CHANGE_OVERAGE_ACTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DEVICE_APPROVALS_CHANGE_UNLINK_ACTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DEVICE_APPROVALS_REMOVE_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DEVICE_CHANGE_IP_DESKTOP:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DEVICE_CHANGE_IP_MOBILE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DEVICE_CHANGE_IP_WEB:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DEVICE_DELETE_ON_UNLINK_FAIL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DEVICE_DELETE_ON_UNLINK_SUCCESS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DEVICE_LINK_FAIL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DEVICE_LINK_SUCCESS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DEVICE_MANAGEMENT_DISABLED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DEVICE_MANAGEMENT_ENABLED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DEVICE_UNLINK:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DIRECTORY_RESTRICTIONS_ADD_MEMBERS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DIRECTORY_RESTRICTIONS_REMOVE_MEMBERS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DISABLED_DOMAIN_INVITES:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DOMAIN_INVITES_APPROVE_REQUEST_TO_JOIN_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DOMAIN_INVITES_DECLINE_REQUEST_TO_JOIN_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DOMAIN_INVITES_EMAIL_EXISTING_USERS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DOMAIN_INVITES_REQUEST_TO_JOIN_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_NO:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_YES:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DOMAIN_VERIFICATION_ADD_DOMAIN_FAIL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DOMAIN_VERIFICATION_ADD_DOMAIN_SUCCESS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum DOMAIN_VERIFICATION_REMOVE_DOMAIN:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum EMM_ADD_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum EMM_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum EMM_CREATE_EXCEPTIONS_REPORT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum EMM_CREATE_USAGE_REPORT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum EMM_ERROR:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum EMM_REFRESH_AUTH_TOKEN:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum EMM_REMOVE_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum ENABLED_DOMAIN_INVITES:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum ENDED_ENTERPRISE_ADMIN_SESSION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum ENDED_ENTERPRISE_ADMIN_SESSION_DEPRECATED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum ENTERPRISE_SETTINGS_LOCKING:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum EXPORT_MEMBERS_REPORT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum EXPORT_MEMBERS_REPORT_FAIL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum EXTENDED_VERSION_HISTORY_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_ADD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_ADD_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_CHANGE_COMMENT_SUBSCRIPTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_COMMENTS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_COPY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_DELETE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_DELETE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_EDIT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_EDIT_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_GET_COPY_REFERENCE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_LIKE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_LOCKING_LOCK_STATUS_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_LOCKING_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_MOVE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_PERMANENTLY_DELETE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_PREVIEW:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_RENAME:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_REQUESTS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_REQUESTS_EMAILS_ENABLED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_REQUESTS_EMAILS_RESTRICTED_TO_TEAM_ONLY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_REQUEST_CHANGE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_REQUEST_CLOSE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_REQUEST_CREATE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_REQUEST_DELETE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_REQUEST_RECEIVE_FILE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_RESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_RESTORE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_REVERT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_ROLLBACK_CHANGES:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_SAVE_COPY_REFERENCE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_TRANSFERS_FILE_ADD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_TRANSFERS_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_TRANSFERS_TRANSFER_DELETE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_TRANSFERS_TRANSFER_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_TRANSFERS_TRANSFER_SEND:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_TRANSFERS_TRANSFER_VIEW:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_UNLIKE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FILE_UNRESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FOLDER_OVERVIEW_DESCRIPTION_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FOLDER_OVERVIEW_ITEM_PINNED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum FOLDER_OVERVIEW_ITEM_UNPINNED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum GOOGLE_SSO_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum GROUP_ADD_EXTERNAL_ID:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum GROUP_ADD_MEMBER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum GROUP_CHANGE_EXTERNAL_ID:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum GROUP_CHANGE_MANAGEMENT_TYPE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum GROUP_CHANGE_MEMBER_ROLE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum GROUP_CREATE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum GROUP_DELETE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum GROUP_DESCRIPTION_UPDATED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum GROUP_JOIN_POLICY_UPDATED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum GROUP_MOVED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum GROUP_REMOVE_EXTERNAL_ID:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum GROUP_REMOVE_MEMBER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum GROUP_RENAME:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum GROUP_USER_MANAGEMENT_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum GUEST_ADMIN_CHANGE_STATUS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum GUEST_ADMIN_SIGNED_IN_VIA_TRUSTED_TEAMS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum GUEST_ADMIN_SIGNED_OUT_VIA_TRUSTED_TEAMS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum INTEGRATION_CONNECTED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum INTEGRATION_DISCONNECTED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum INTEGRATION_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum LEGAL_HOLDS_ACTIVATE_A_HOLD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum LEGAL_HOLDS_ADD_MEMBERS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum LEGAL_HOLDS_CHANGE_HOLD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum LEGAL_HOLDS_CHANGE_HOLD_NAME:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum LEGAL_HOLDS_EXPORT_A_HOLD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum LEGAL_HOLDS_EXPORT_CANCELLED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum LEGAL_HOLDS_EXPORT_DOWNLOADED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum LEGAL_HOLDS_EXPORT_REMOVED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum LEGAL_HOLDS_RELEASE_A_HOLD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum LEGAL_HOLDS_REMOVE_MEMBERS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum LEGAL_HOLDS_REPORT_A_HOLD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum LOGIN_FAIL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum LOGIN_SUCCESS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum LOGOUT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_ADD_EXTERNAL_ID:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_ADD_NAME:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_CHANGE_ADMIN_ROLE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_CHANGE_EMAIL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_CHANGE_EXTERNAL_ID:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_CHANGE_MEMBERSHIP_TYPE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_CHANGE_NAME:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_CHANGE_STATUS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_DELETE_MANUAL_CONTACTS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_DELETE_PROFILE_PHOTO:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_PERMANENTLY_DELETE_ACCOUNT_CONTENTS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_REMOVE_EXTERNAL_ID:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_REQUESTS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_SEND_INVITE_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_SET_PROFILE_PHOTO:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_SPACE_LIMITS_ADD_CUSTOM_QUOTA:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_SPACE_LIMITS_ADD_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_SPACE_LIMITS_CHANGE_CAPS_TYPE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_SPACE_LIMITS_CHANGE_CUSTOM_QUOTA:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_SPACE_LIMITS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_SPACE_LIMITS_CHANGE_STATUS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_SPACE_LIMITS_REMOVE_CUSTOM_QUOTA:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_SPACE_LIMITS_REMOVE_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_SUGGEST:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_SUGGESTIONS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MEMBER_TRANSFER_ACCOUNT_CONTENTS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum MICROSOFT_OFFICE_ADDIN_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum NETWORK_CONTROL_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum NOTE_ACL_INVITE_ONLY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum NOTE_ACL_LINK:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum NOTE_ACL_TEAM_LINK:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum NOTE_SHARED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum NOTE_SHARE_RECEIVE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum NO_EXPIRATION_LINK_GEN_CREATE_REPORT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum NO_EXPIRATION_LINK_GEN_REPORT_FAILED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum NO_PASSWORD_LINK_GEN_CREATE_REPORT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum NO_PASSWORD_LINK_GEN_REPORT_FAILED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum NO_PASSWORD_LINK_VIEW_CREATE_REPORT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum NO_PASSWORD_LINK_VIEW_REPORT_FAILED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum OPEN_NOTE_SHARED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum OTHER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum OUTDATED_LINK_VIEW_CREATE_REPORT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum OUTDATED_LINK_VIEW_REPORT_FAILED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_ADMIN_EXPORT_START:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_CHANGE_DEPLOYMENT_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_CHANGE_MEMBER_LINK_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_CHANGE_MEMBER_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_CONTENT_ADD_MEMBER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_CONTENT_ADD_TO_FOLDER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_CONTENT_ARCHIVE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_CONTENT_CREATE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_CONTENT_PERMANENTLY_DELETE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_CONTENT_REMOVE_FROM_FOLDER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_CONTENT_REMOVE_MEMBER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_CONTENT_RENAME:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_CONTENT_RESTORE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_DEFAULT_FOLDER_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_DESKTOP_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_DOC_ADD_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_DOC_CHANGE_MEMBER_ROLE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_DOC_CHANGE_SHARING_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_DOC_CHANGE_SUBSCRIPTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_DOC_DELETED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_DOC_DELETE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_DOC_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_DOC_EDIT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_DOC_EDIT_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_DOC_FOLLOWED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_DOC_MENTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_DOC_OWNERSHIP_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_DOC_REQUEST_ACCESS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_DOC_RESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_DOC_REVERT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_DOC_SLACK_SHARE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_DOC_TEAM_INVITE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_DOC_TRASHED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_DOC_UNRESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_DOC_UNTRASHED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_DOC_VIEW:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_ENABLED_USERS_GROUP_ADDITION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_ENABLED_USERS_GROUP_REMOVAL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_EXTERNAL_VIEW_ALLOW:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_EXTERNAL_VIEW_DEFAULT_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_EXTERNAL_VIEW_FORBID:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_FOLDER_CHANGE_SUBSCRIPTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_FOLDER_DELETED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_FOLDER_FOLLOWED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_FOLDER_TEAM_INVITE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_PUBLISHED_LINK_CHANGE_PERMISSION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_PUBLISHED_LINK_CREATE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_PUBLISHED_LINK_DISABLED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PAPER_PUBLISHED_LINK_VIEW:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PASSWORD_CHANGE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PASSWORD_RESET:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PASSWORD_RESET_ALL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PASSWORD_STRENGTH_REQUIREMENTS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PENDING_SECONDARY_EMAIL_ADDED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum PERMANENT_DELETE_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum RESELLER_SUPPORT_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum RESELLER_SUPPORT_SESSION_END:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum RESELLER_SUPPORT_SESSION_START:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum REWIND_FOLDER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum REWIND_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SECONDARY_EMAIL_DELETED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SECONDARY_EMAIL_VERIFIED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SECONDARY_MAILS_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SF_ADD_GROUP:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SF_ALLOW_NON_MEMBERS_TO_VIEW_SHARED_LINKS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SF_EXTERNAL_INVITE_WARN:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SF_FB_INVITE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SF_FB_INVITE_CHANGE_ROLE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SF_FB_UNINVITE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SF_INVITE_GROUP:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SF_TEAM_GRANT_ACCESS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SF_TEAM_INVITE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SF_TEAM_INVITE_CHANGE_ROLE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SF_TEAM_JOIN:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SF_TEAM_JOIN_FROM_OOB_LINK:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SF_TEAM_UNINVITE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_CONTENT_ADD_INVITEES:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_CONTENT_ADD_LINK_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_CONTENT_ADD_LINK_PASSWORD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_CONTENT_ADD_MEMBER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_CONTENT_CHANGE_DOWNLOADS_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_CONTENT_CHANGE_INVITEE_ROLE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_CONTENT_CHANGE_LINK_AUDIENCE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_CONTENT_CHANGE_LINK_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_CONTENT_CHANGE_LINK_PASSWORD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_CONTENT_CHANGE_MEMBER_ROLE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_CONTENT_CHANGE_VIEWER_INFO_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_CONTENT_CLAIM_INVITATION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_CONTENT_COPY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_CONTENT_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_CONTENT_RELINQUISH_MEMBERSHIP:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_CONTENT_REMOVE_INVITEES:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_CONTENT_REMOVE_LINK_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_CONTENT_REMOVE_LINK_PASSWORD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_CONTENT_REMOVE_MEMBER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_CONTENT_REQUEST_ACCESS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_CONTENT_RESTORE_INVITEES:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_CONTENT_RESTORE_MEMBER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_CONTENT_UNSHARE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_CONTENT_VIEW:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_FOLDER_CHANGE_LINK_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_FOLDER_CHANGE_MEMBERS_INHERITANCE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_FOLDER_CHANGE_MEMBERS_MANAGEMENT_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_FOLDER_CHANGE_MEMBERS_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_FOLDER_CREATE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_FOLDER_DECLINE_INVITATION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_FOLDER_MOUNT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_FOLDER_NEST:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_FOLDER_TRANSFER_OWNERSHIP:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_FOLDER_UNMOUNT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_LINK_ADD_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_LINK_CHANGE_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_LINK_CHANGE_VISIBILITY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_LINK_COPY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_LINK_CREATE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_LINK_DISABLE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_LINK_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_LINK_REMOVE_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_LINK_SETTINGS_ADD_EXPIRATION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_LINK_SETTINGS_ADD_PASSWORD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_LINK_SETTINGS_ALLOW_DOWNLOAD_DISABLED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_LINK_SETTINGS_ALLOW_DOWNLOAD_ENABLED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_LINK_SETTINGS_CHANGE_AUDIENCE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_LINK_SETTINGS_CHANGE_EXPIRATION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_LINK_SETTINGS_CHANGE_PASSWORD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_LINK_SETTINGS_REMOVE_EXPIRATION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_LINK_SETTINGS_REMOVE_PASSWORD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_LINK_SHARE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_LINK_VIEW:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARED_NOTE_OPENED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARING_CHANGE_FOLDER_JOIN_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARING_CHANGE_LINK_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHARING_CHANGE_MEMBER_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHMODEL_GROUP_SHARE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_ACCESS_GRANTED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_ADD_MEMBER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_ARCHIVED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_CHANGE_DOWNLOAD_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_CHANGE_ENABLED_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_CHANGE_EXTERNAL_SHARING_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_CREATED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_DELETE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_EDITED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_EDIT_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_FILE_ADDED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_FILE_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_FILE_REMOVED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_FILE_VIEW:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_PERMANENTLY_DELETED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_POST_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_REMOVE_MEMBER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_RENAMED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_REQUEST_ACCESS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_RESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_RESTORED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_TRASHED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_TRASHED_DEPRECATED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_UNRESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_UNTRASHED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_UNTRASHED_DEPRECATED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SHOWCASE_VIEW:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SIGN_IN_AS_SESSION_END:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SIGN_IN_AS_SESSION_START:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SMARTER_SMART_SYNC_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SMART_SYNC_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SMART_SYNC_CREATE_ADMIN_PRIVILEGE_REPORT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SMART_SYNC_NOT_OPT_OUT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SMART_SYNC_OPT_OUT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SSO_ADD_CERT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SSO_ADD_LOGIN_URL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SSO_ADD_LOGOUT_URL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SSO_CHANGE_CERT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SSO_CHANGE_LOGIN_URL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SSO_CHANGE_LOGOUT_URL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SSO_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SSO_CHANGE_SAML_IDENTITY_MODE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SSO_ERROR:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SSO_REMOVE_CERT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SSO_REMOVE_LOGIN_URL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum SSO_REMOVE_LOGOUT_URL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum STARTED_ENTERPRISE_ADMIN_SESSION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_ACTIVITY_CREATE_REPORT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_ACTIVITY_CREATE_REPORT_FAIL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_EXTENSIONS_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_FOLDER_CHANGE_STATUS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_FOLDER_CREATE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_FOLDER_DOWNGRADE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_FOLDER_PERMANENTLY_DELETE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_FOLDER_RENAME:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_MERGE_FROM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_MERGE_REQUEST_ACCEPTED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_MERGE_REQUEST_ACCEPTED_SHOWN_TO_PRIMARY_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_MERGE_REQUEST_ACCEPTED_SHOWN_TO_SECONDARY_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_MERGE_REQUEST_AUTO_CANCELED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_MERGE_REQUEST_CANCELED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_MERGE_REQUEST_CANCELED_SHOWN_TO_PRIMARY_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_MERGE_REQUEST_CANCELED_SHOWN_TO_SECONDARY_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_MERGE_REQUEST_EXPIRED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_MERGE_REQUEST_EXPIRED_SHOWN_TO_PRIMARY_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_MERGE_REQUEST_EXPIRED_SHOWN_TO_SECONDARY_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_MERGE_REQUEST_REJECTED_SHOWN_TO_PRIMARY_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_MERGE_REQUEST_REJECTED_SHOWN_TO_SECONDARY_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_MERGE_REQUEST_REMINDER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_MERGE_REQUEST_REMINDER_SHOWN_TO_PRIMARY_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_MERGE_REQUEST_REMINDER_SHOWN_TO_SECONDARY_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_MERGE_REQUEST_REVOKED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_MERGE_REQUEST_SENT_SHOWN_TO_PRIMARY_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_MERGE_REQUEST_SENT_SHOWN_TO_SECONDARY_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_MERGE_TO:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_PROFILE_ADD_LOGO:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_PROFILE_CHANGE_DEFAULT_LANGUAGE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_PROFILE_CHANGE_LOGO:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_PROFILE_CHANGE_NAME:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_PROFILE_REMOVE_LOGO:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_SELECTIVE_SYNC_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_SELECTIVE_SYNC_SETTINGS_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TEAM_SHARING_WHITELIST_SUBJECTS_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TFA_ADD_BACKUP_PHONE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TFA_ADD_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TFA_ADD_SECURITY_KEY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TFA_CHANGE_BACKUP_PHONE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TFA_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TFA_CHANGE_STATUS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TFA_REMOVE_BACKUP_PHONE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TFA_REMOVE_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TFA_REMOVE_SECURITY_KEY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TFA_RESET:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum TWO_ACCOUNT_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum VIEWER_INFO_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum WATERMARKING_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum WEB_SESSIONS_CHANGE_ACTIVE_SESSION_LIMIT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum WEB_SESSIONS_CHANGE_FIXED_LENGTH_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

.field public static final enum WEB_SESSIONS_CHANGE_IDLE_LENGTH_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;


# direct methods
.method static constructor <clinit>()V
    .locals 427

    .line 1
    new-instance v0, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v1, "APP_LINK_TEAM"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->APP_LINK_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 2
    new-instance v1, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v3, "APP_LINK_USER"

    const/4 v4, 0x1

    invoke-direct {v1, v3, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->APP_LINK_USER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 3
    new-instance v3, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v5, "APP_UNLINK_TEAM"

    const/4 v6, 0x2

    invoke-direct {v3, v5, v6}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->APP_UNLINK_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 4
    new-instance v5, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v7, "APP_UNLINK_USER"

    const/4 v8, 0x3

    invoke-direct {v5, v7, v8}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->APP_UNLINK_USER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 5
    new-instance v7, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v9, "INTEGRATION_CONNECTED"

    const/4 v10, 0x4

    invoke-direct {v7, v9, v10}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->INTEGRATION_CONNECTED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 6
    new-instance v9, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v11, "INTEGRATION_DISCONNECTED"

    const/4 v12, 0x5

    invoke-direct {v9, v11, v12}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->INTEGRATION_DISCONNECTED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 7
    new-instance v11, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v13, "FILE_ADD_COMMENT"

    const/4 v14, 0x6

    invoke-direct {v11, v13, v14}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_ADD_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 8
    new-instance v13, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v15, "FILE_CHANGE_COMMENT_SUBSCRIPTION"

    const/4 v14, 0x7

    invoke-direct {v13, v15, v14}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_CHANGE_COMMENT_SUBSCRIPTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 9
    new-instance v15, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v14, "FILE_DELETE_COMMENT"

    const/16 v12, 0x8

    invoke-direct {v15, v14, v12}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v15, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_DELETE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 10
    new-instance v14, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v12, "FILE_EDIT_COMMENT"

    const/16 v10, 0x9

    invoke-direct {v14, v12, v10}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_EDIT_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 11
    new-instance v12, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v10, "FILE_LIKE_COMMENT"

    const/16 v8, 0xa

    invoke-direct {v12, v10, v8}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_LIKE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 12
    new-instance v10, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v8, "FILE_RESOLVE_COMMENT"

    const/16 v6, 0xb

    invoke-direct {v10, v8, v6}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_RESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 13
    new-instance v8, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_UNLIKE_COMMENT"

    const/16 v4, 0xc

    invoke-direct {v8, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_UNLIKE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 14
    new-instance v6, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v4, "FILE_UNRESOLVE_COMMENT"

    const/16 v2, 0xd

    invoke-direct {v6, v4, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_UNRESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 15
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v2, "DEVICE_CHANGE_IP_DESKTOP"

    move-object/from16 v16, v6

    const/16 v6, 0xe

    invoke-direct {v4, v2, v6}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DEVICE_CHANGE_IP_DESKTOP:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 16
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "DEVICE_CHANGE_IP_MOBILE"

    move-object/from16 v17, v4

    const/16 v4, 0xf

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DEVICE_CHANGE_IP_MOBILE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 17
    new-instance v6, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v4, "DEVICE_CHANGE_IP_WEB"

    move-object/from16 v18, v2

    const/16 v2, 0x10

    invoke-direct {v6, v4, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DEVICE_CHANGE_IP_WEB:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 18
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v2, "DEVICE_DELETE_ON_UNLINK_FAIL"

    move-object/from16 v19, v6

    const/16 v6, 0x11

    invoke-direct {v4, v2, v6}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DEVICE_DELETE_ON_UNLINK_FAIL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 19
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "DEVICE_DELETE_ON_UNLINK_SUCCESS"

    move-object/from16 v20, v4

    const/16 v4, 0x12

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DEVICE_DELETE_ON_UNLINK_SUCCESS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 20
    new-instance v6, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v4, "DEVICE_LINK_FAIL"

    move-object/from16 v21, v2

    const/16 v2, 0x13

    invoke-direct {v6, v4, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DEVICE_LINK_FAIL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 21
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v2, "DEVICE_LINK_SUCCESS"

    move-object/from16 v22, v6

    const/16 v6, 0x14

    invoke-direct {v4, v2, v6}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DEVICE_LINK_SUCCESS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 22
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "DEVICE_MANAGEMENT_DISABLED"

    move-object/from16 v23, v4

    const/16 v4, 0x15

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DEVICE_MANAGEMENT_DISABLED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 23
    new-instance v6, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v4, "DEVICE_MANAGEMENT_ENABLED"

    move-object/from16 v24, v2

    const/16 v2, 0x16

    invoke-direct {v6, v4, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DEVICE_MANAGEMENT_ENABLED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 24
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v4, "DEVICE_UNLINK"

    move-object/from16 v25, v6

    const/16 v6, 0x17

    invoke-direct {v2, v4, v6}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DEVICE_UNLINK:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 25
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "EMM_REFRESH_AUTH_TOKEN"

    move-object/from16 v26, v2

    const/16 v2, 0x18

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->EMM_REFRESH_AUTH_TOKEN:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 26
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "ACCOUNT_CAPTURE_CHANGE_AVAILABILITY"

    move-object/from16 v27, v4

    const/16 v4, 0x19

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->ACCOUNT_CAPTURE_CHANGE_AVAILABILITY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 27
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "ACCOUNT_CAPTURE_MIGRATE_ACCOUNT"

    move-object/from16 v28, v2

    const/16 v2, 0x1a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->ACCOUNT_CAPTURE_MIGRATE_ACCOUNT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 28
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "ACCOUNT_CAPTURE_NOTIFICATION_EMAILS_SENT"

    move-object/from16 v29, v4

    const/16 v4, 0x1b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->ACCOUNT_CAPTURE_NOTIFICATION_EMAILS_SENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 29
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "ACCOUNT_CAPTURE_RELINQUISH_ACCOUNT"

    move-object/from16 v30, v2

    const/16 v2, 0x1c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->ACCOUNT_CAPTURE_RELINQUISH_ACCOUNT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 30
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "DISABLED_DOMAIN_INVITES"

    move-object/from16 v31, v4

    const/16 v4, 0x1d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DISABLED_DOMAIN_INVITES:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 31
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "DOMAIN_INVITES_APPROVE_REQUEST_TO_JOIN_TEAM"

    move-object/from16 v32, v2

    const/16 v2, 0x1e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DOMAIN_INVITES_APPROVE_REQUEST_TO_JOIN_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 32
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "DOMAIN_INVITES_DECLINE_REQUEST_TO_JOIN_TEAM"

    move-object/from16 v33, v4

    const/16 v4, 0x1f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DOMAIN_INVITES_DECLINE_REQUEST_TO_JOIN_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 33
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "DOMAIN_INVITES_EMAIL_EXISTING_USERS"

    move-object/from16 v34, v2

    const/16 v2, 0x20

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DOMAIN_INVITES_EMAIL_EXISTING_USERS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 34
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "DOMAIN_INVITES_REQUEST_TO_JOIN_TEAM"

    move-object/from16 v35, v4

    const/16 v4, 0x21

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DOMAIN_INVITES_REQUEST_TO_JOIN_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 35
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_NO"

    move-object/from16 v36, v2

    const/16 v2, 0x22

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_NO:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 36
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_YES"

    move-object/from16 v37, v4

    const/16 v4, 0x23

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DOMAIN_INVITES_SET_INVITE_NEW_USER_PREF_TO_YES:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 37
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "DOMAIN_VERIFICATION_ADD_DOMAIN_FAIL"

    move-object/from16 v38, v2

    const/16 v2, 0x24

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DOMAIN_VERIFICATION_ADD_DOMAIN_FAIL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 38
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "DOMAIN_VERIFICATION_ADD_DOMAIN_SUCCESS"

    move-object/from16 v39, v4

    const/16 v4, 0x25

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DOMAIN_VERIFICATION_ADD_DOMAIN_SUCCESS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 39
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "DOMAIN_VERIFICATION_REMOVE_DOMAIN"

    move-object/from16 v40, v2

    const/16 v2, 0x26

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DOMAIN_VERIFICATION_REMOVE_DOMAIN:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 40
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "ENABLED_DOMAIN_INVITES"

    move-object/from16 v41, v4

    const/16 v4, 0x27

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->ENABLED_DOMAIN_INVITES:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 41
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "CREATE_FOLDER"

    move-object/from16 v42, v2

    const/16 v2, 0x28

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->CREATE_FOLDER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 42
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_ADD"

    move-object/from16 v43, v4

    const/16 v4, 0x29

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_ADD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 43
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_COPY"

    move-object/from16 v44, v2

    const/16 v2, 0x2a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_COPY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 44
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_DELETE"

    move-object/from16 v45, v4

    const/16 v4, 0x2b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_DELETE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 45
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_DOWNLOAD"

    move-object/from16 v46, v2

    const/16 v2, 0x2c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 46
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_EDIT"

    move-object/from16 v47, v4

    const/16 v4, 0x2d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_EDIT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 47
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_GET_COPY_REFERENCE"

    move-object/from16 v48, v2

    const/16 v2, 0x2e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_GET_COPY_REFERENCE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 48
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_LOCKING_LOCK_STATUS_CHANGED"

    move-object/from16 v49, v4

    const/16 v4, 0x2f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_LOCKING_LOCK_STATUS_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 49
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_MOVE"

    move-object/from16 v50, v2

    const/16 v2, 0x30

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_MOVE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 50
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_PERMANENTLY_DELETE"

    move-object/from16 v51, v4

    const/16 v4, 0x31

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_PERMANENTLY_DELETE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 51
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_PREVIEW"

    move-object/from16 v52, v2

    const/16 v2, 0x32

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_PREVIEW:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 52
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_RENAME"

    move-object/from16 v53, v4

    const/16 v4, 0x33

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_RENAME:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 53
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_RESTORE"

    move-object/from16 v54, v2

    const/16 v2, 0x34

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_RESTORE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 54
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_REVERT"

    move-object/from16 v55, v4

    const/16 v4, 0x35

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_REVERT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 55
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_ROLLBACK_CHANGES"

    move-object/from16 v56, v2

    const/16 v2, 0x36

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_ROLLBACK_CHANGES:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 56
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_SAVE_COPY_REFERENCE"

    move-object/from16 v57, v4

    const/16 v4, 0x37

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_SAVE_COPY_REFERENCE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 57
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FOLDER_OVERVIEW_DESCRIPTION_CHANGED"

    move-object/from16 v58, v2

    const/16 v2, 0x38

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FOLDER_OVERVIEW_DESCRIPTION_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 58
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FOLDER_OVERVIEW_ITEM_PINNED"

    move-object/from16 v59, v4

    const/16 v4, 0x39

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FOLDER_OVERVIEW_ITEM_PINNED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 59
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FOLDER_OVERVIEW_ITEM_UNPINNED"

    move-object/from16 v60, v2

    const/16 v2, 0x3a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FOLDER_OVERVIEW_ITEM_UNPINNED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 60
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "REWIND_FOLDER"

    move-object/from16 v61, v4

    const/16 v4, 0x3b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->REWIND_FOLDER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 61
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_REQUEST_CHANGE"

    move-object/from16 v62, v2

    const/16 v2, 0x3c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_REQUEST_CHANGE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 62
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_REQUEST_CLOSE"

    move-object/from16 v63, v4

    const/16 v4, 0x3d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_REQUEST_CLOSE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 63
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_REQUEST_CREATE"

    move-object/from16 v64, v2

    const/16 v2, 0x3e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_REQUEST_CREATE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 64
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_REQUEST_DELETE"

    move-object/from16 v65, v4

    const/16 v4, 0x3f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_REQUEST_DELETE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 65
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_REQUEST_RECEIVE_FILE"

    move-object/from16 v66, v2

    const/16 v2, 0x40

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_REQUEST_RECEIVE_FILE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 66
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "GROUP_ADD_EXTERNAL_ID"

    move-object/from16 v67, v4

    const/16 v4, 0x41

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->GROUP_ADD_EXTERNAL_ID:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 67
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "GROUP_ADD_MEMBER"

    move-object/from16 v68, v2

    const/16 v2, 0x42

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->GROUP_ADD_MEMBER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 68
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "GROUP_CHANGE_EXTERNAL_ID"

    move-object/from16 v69, v4

    const/16 v4, 0x43

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->GROUP_CHANGE_EXTERNAL_ID:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 69
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "GROUP_CHANGE_MANAGEMENT_TYPE"

    move-object/from16 v70, v2

    const/16 v2, 0x44

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->GROUP_CHANGE_MANAGEMENT_TYPE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 70
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "GROUP_CHANGE_MEMBER_ROLE"

    move-object/from16 v71, v4

    const/16 v4, 0x45

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->GROUP_CHANGE_MEMBER_ROLE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 71
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "GROUP_CREATE"

    move-object/from16 v72, v2

    const/16 v2, 0x46

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->GROUP_CREATE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 72
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "GROUP_DELETE"

    move-object/from16 v73, v4

    const/16 v4, 0x47

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->GROUP_DELETE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 73
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "GROUP_DESCRIPTION_UPDATED"

    move-object/from16 v74, v2

    const/16 v2, 0x48

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->GROUP_DESCRIPTION_UPDATED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 74
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "GROUP_JOIN_POLICY_UPDATED"

    move-object/from16 v75, v4

    const/16 v4, 0x49

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->GROUP_JOIN_POLICY_UPDATED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 75
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "GROUP_MOVED"

    move-object/from16 v76, v2

    const/16 v2, 0x4a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->GROUP_MOVED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 76
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "GROUP_REMOVE_EXTERNAL_ID"

    move-object/from16 v77, v4

    const/16 v4, 0x4b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->GROUP_REMOVE_EXTERNAL_ID:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 77
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "GROUP_REMOVE_MEMBER"

    move-object/from16 v78, v2

    const/16 v2, 0x4c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->GROUP_REMOVE_MEMBER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 78
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "GROUP_RENAME"

    move-object/from16 v79, v4

    const/16 v4, 0x4d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->GROUP_RENAME:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 79
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "LEGAL_HOLDS_ACTIVATE_A_HOLD"

    move-object/from16 v80, v2

    const/16 v2, 0x4e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->LEGAL_HOLDS_ACTIVATE_A_HOLD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 80
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "LEGAL_HOLDS_ADD_MEMBERS"

    move-object/from16 v81, v4

    const/16 v4, 0x4f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->LEGAL_HOLDS_ADD_MEMBERS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 81
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "LEGAL_HOLDS_CHANGE_HOLD_DETAILS"

    move-object/from16 v82, v2

    const/16 v2, 0x50

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->LEGAL_HOLDS_CHANGE_HOLD_DETAILS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 82
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "LEGAL_HOLDS_CHANGE_HOLD_NAME"

    move-object/from16 v83, v4

    const/16 v4, 0x51

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->LEGAL_HOLDS_CHANGE_HOLD_NAME:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 83
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "LEGAL_HOLDS_EXPORT_A_HOLD"

    move-object/from16 v84, v2

    const/16 v2, 0x52

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->LEGAL_HOLDS_EXPORT_A_HOLD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 84
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "LEGAL_HOLDS_EXPORT_CANCELLED"

    move-object/from16 v85, v4

    const/16 v4, 0x53

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->LEGAL_HOLDS_EXPORT_CANCELLED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 85
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "LEGAL_HOLDS_EXPORT_DOWNLOADED"

    move-object/from16 v86, v2

    const/16 v2, 0x54

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->LEGAL_HOLDS_EXPORT_DOWNLOADED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 86
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "LEGAL_HOLDS_EXPORT_REMOVED"

    move-object/from16 v87, v4

    const/16 v4, 0x55

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->LEGAL_HOLDS_EXPORT_REMOVED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 87
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "LEGAL_HOLDS_RELEASE_A_HOLD"

    move-object/from16 v88, v2

    const/16 v2, 0x56

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->LEGAL_HOLDS_RELEASE_A_HOLD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 88
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "LEGAL_HOLDS_REMOVE_MEMBERS"

    move-object/from16 v89, v4

    const/16 v4, 0x57

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->LEGAL_HOLDS_REMOVE_MEMBERS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 89
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "LEGAL_HOLDS_REPORT_A_HOLD"

    move-object/from16 v90, v2

    const/16 v2, 0x58

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->LEGAL_HOLDS_REPORT_A_HOLD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 90
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "ACCOUNT_LOCK_OR_UNLOCKED"

    move-object/from16 v91, v4

    const/16 v4, 0x59

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->ACCOUNT_LOCK_OR_UNLOCKED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 91
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "EMM_ERROR"

    move-object/from16 v92, v2

    const/16 v2, 0x5a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->EMM_ERROR:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 92
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "GUEST_ADMIN_SIGNED_IN_VIA_TRUSTED_TEAMS"

    move-object/from16 v93, v4

    const/16 v4, 0x5b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->GUEST_ADMIN_SIGNED_IN_VIA_TRUSTED_TEAMS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 93
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "GUEST_ADMIN_SIGNED_OUT_VIA_TRUSTED_TEAMS"

    move-object/from16 v94, v2

    const/16 v2, 0x5c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->GUEST_ADMIN_SIGNED_OUT_VIA_TRUSTED_TEAMS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 94
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "LOGIN_FAIL"

    move-object/from16 v95, v4

    const/16 v4, 0x5d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->LOGIN_FAIL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 95
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "LOGIN_SUCCESS"

    move-object/from16 v96, v2

    const/16 v2, 0x5e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->LOGIN_SUCCESS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 96
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "LOGOUT"

    move-object/from16 v97, v4

    const/16 v4, 0x5f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->LOGOUT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 97
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "RESELLER_SUPPORT_SESSION_END"

    move-object/from16 v98, v2

    const/16 v2, 0x60

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->RESELLER_SUPPORT_SESSION_END:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 98
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "RESELLER_SUPPORT_SESSION_START"

    move-object/from16 v99, v4

    const/16 v4, 0x61

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->RESELLER_SUPPORT_SESSION_START:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 99
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SIGN_IN_AS_SESSION_END"

    move-object/from16 v100, v2

    const/16 v2, 0x62

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SIGN_IN_AS_SESSION_END:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 100
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SIGN_IN_AS_SESSION_START"

    move-object/from16 v101, v4

    const/16 v4, 0x63

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SIGN_IN_AS_SESSION_START:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 101
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SSO_ERROR"

    move-object/from16 v102, v2

    const/16 v2, 0x64

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SSO_ERROR:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 102
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "CREATE_TEAM_INVITE_LINK"

    move-object/from16 v103, v4

    const/16 v4, 0x65

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->CREATE_TEAM_INVITE_LINK:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 103
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "DELETE_TEAM_INVITE_LINK"

    move-object/from16 v104, v2

    const/16 v2, 0x66

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DELETE_TEAM_INVITE_LINK:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 104
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_ADD_EXTERNAL_ID"

    move-object/from16 v105, v4

    const/16 v4, 0x67

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_ADD_EXTERNAL_ID:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 105
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_ADD_NAME"

    move-object/from16 v106, v2

    const/16 v2, 0x68

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_ADD_NAME:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 106
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_CHANGE_ADMIN_ROLE"

    move-object/from16 v107, v4

    const/16 v4, 0x69

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_CHANGE_ADMIN_ROLE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 107
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_CHANGE_EMAIL"

    move-object/from16 v108, v2

    const/16 v2, 0x6a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_CHANGE_EMAIL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 108
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_CHANGE_EXTERNAL_ID"

    move-object/from16 v109, v4

    const/16 v4, 0x6b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_CHANGE_EXTERNAL_ID:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 109
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_CHANGE_MEMBERSHIP_TYPE"

    move-object/from16 v110, v2

    const/16 v2, 0x6c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_CHANGE_MEMBERSHIP_TYPE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 110
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_CHANGE_NAME"

    move-object/from16 v111, v4

    const/16 v4, 0x6d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_CHANGE_NAME:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 111
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_CHANGE_STATUS"

    move-object/from16 v112, v2

    const/16 v2, 0x6e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_CHANGE_STATUS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 112
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_DELETE_MANUAL_CONTACTS"

    move-object/from16 v113, v4

    const/16 v4, 0x6f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_DELETE_MANUAL_CONTACTS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 113
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_DELETE_PROFILE_PHOTO"

    move-object/from16 v114, v2

    const/16 v2, 0x70

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_DELETE_PROFILE_PHOTO:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 114
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_PERMANENTLY_DELETE_ACCOUNT_CONTENTS"

    move-object/from16 v115, v4

    const/16 v4, 0x71

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_PERMANENTLY_DELETE_ACCOUNT_CONTENTS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 115
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_REMOVE_EXTERNAL_ID"

    move-object/from16 v116, v2

    const/16 v2, 0x72

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_REMOVE_EXTERNAL_ID:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 116
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_SET_PROFILE_PHOTO"

    move-object/from16 v117, v4

    const/16 v4, 0x73

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_SET_PROFILE_PHOTO:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 117
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_SPACE_LIMITS_ADD_CUSTOM_QUOTA"

    move-object/from16 v118, v2

    const/16 v2, 0x74

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_SPACE_LIMITS_ADD_CUSTOM_QUOTA:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 118
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_SPACE_LIMITS_CHANGE_CUSTOM_QUOTA"

    move-object/from16 v119, v4

    const/16 v4, 0x75

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_SPACE_LIMITS_CHANGE_CUSTOM_QUOTA:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 119
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_SPACE_LIMITS_CHANGE_STATUS"

    move-object/from16 v120, v2

    const/16 v2, 0x76

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_SPACE_LIMITS_CHANGE_STATUS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 120
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_SPACE_LIMITS_REMOVE_CUSTOM_QUOTA"

    move-object/from16 v121, v4

    const/16 v4, 0x77

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_SPACE_LIMITS_REMOVE_CUSTOM_QUOTA:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 121
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_SUGGEST"

    move-object/from16 v122, v2

    const/16 v2, 0x78

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_SUGGEST:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 122
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_TRANSFER_ACCOUNT_CONTENTS"

    move-object/from16 v123, v4

    const/16 v4, 0x79

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_TRANSFER_ACCOUNT_CONTENTS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 123
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PENDING_SECONDARY_EMAIL_ADDED"

    move-object/from16 v124, v2

    const/16 v2, 0x7a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PENDING_SECONDARY_EMAIL_ADDED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 124
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SECONDARY_EMAIL_DELETED"

    move-object/from16 v125, v4

    const/16 v4, 0x7b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SECONDARY_EMAIL_DELETED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 125
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SECONDARY_EMAIL_VERIFIED"

    move-object/from16 v126, v2

    const/16 v2, 0x7c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SECONDARY_EMAIL_VERIFIED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 126
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SECONDARY_MAILS_POLICY_CHANGED"

    move-object/from16 v127, v4

    const/16 v4, 0x7d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SECONDARY_MAILS_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 127
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "BINDER_ADD_PAGE"

    move-object/from16 v128, v2

    const/16 v2, 0x7e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->BINDER_ADD_PAGE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 128
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "BINDER_ADD_SECTION"

    move-object/from16 v129, v4

    const/16 v4, 0x7f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->BINDER_ADD_SECTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 129
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "BINDER_REMOVE_PAGE"

    move-object/from16 v130, v2

    const/16 v2, 0x80

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->BINDER_REMOVE_PAGE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 130
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "BINDER_REMOVE_SECTION"

    move-object/from16 v131, v4

    const/16 v4, 0x81

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->BINDER_REMOVE_SECTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 131
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "BINDER_RENAME_PAGE"

    move-object/from16 v132, v2

    const/16 v2, 0x82

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->BINDER_RENAME_PAGE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 132
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "BINDER_RENAME_SECTION"

    move-object/from16 v133, v4

    const/16 v4, 0x83

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->BINDER_RENAME_SECTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 133
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "BINDER_REORDER_PAGE"

    move-object/from16 v134, v2

    const/16 v2, 0x84

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->BINDER_REORDER_PAGE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 134
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "BINDER_REORDER_SECTION"

    move-object/from16 v135, v4

    const/16 v4, 0x85

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->BINDER_REORDER_SECTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 135
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_CONTENT_ADD_MEMBER"

    move-object/from16 v136, v2

    const/16 v2, 0x86

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_CONTENT_ADD_MEMBER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 136
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_CONTENT_ADD_TO_FOLDER"

    move-object/from16 v137, v4

    const/16 v4, 0x87

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_CONTENT_ADD_TO_FOLDER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 137
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_CONTENT_ARCHIVE"

    move-object/from16 v138, v2

    const/16 v2, 0x88

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_CONTENT_ARCHIVE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 138
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_CONTENT_CREATE"

    move-object/from16 v139, v4

    const/16 v4, 0x89

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_CONTENT_CREATE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 139
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_CONTENT_PERMANENTLY_DELETE"

    move-object/from16 v140, v2

    const/16 v2, 0x8a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_CONTENT_PERMANENTLY_DELETE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 140
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_CONTENT_REMOVE_FROM_FOLDER"

    move-object/from16 v141, v4

    const/16 v4, 0x8b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_CONTENT_REMOVE_FROM_FOLDER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 141
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_CONTENT_REMOVE_MEMBER"

    move-object/from16 v142, v2

    const/16 v2, 0x8c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_CONTENT_REMOVE_MEMBER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 142
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_CONTENT_RENAME"

    move-object/from16 v143, v4

    const/16 v4, 0x8d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_CONTENT_RENAME:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 143
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_CONTENT_RESTORE"

    move-object/from16 v144, v2

    const/16 v2, 0x8e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_CONTENT_RESTORE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 144
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_DOC_ADD_COMMENT"

    move-object/from16 v145, v4

    const/16 v4, 0x8f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_DOC_ADD_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 145
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_DOC_CHANGE_MEMBER_ROLE"

    move-object/from16 v146, v2

    const/16 v2, 0x90

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_DOC_CHANGE_MEMBER_ROLE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 146
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_DOC_CHANGE_SHARING_POLICY"

    move-object/from16 v147, v4

    const/16 v4, 0x91

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_DOC_CHANGE_SHARING_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 147
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_DOC_CHANGE_SUBSCRIPTION"

    move-object/from16 v148, v2

    const/16 v2, 0x92

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_DOC_CHANGE_SUBSCRIPTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 148
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_DOC_DELETED"

    move-object/from16 v149, v4

    const/16 v4, 0x93

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_DOC_DELETED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 149
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_DOC_DELETE_COMMENT"

    move-object/from16 v150, v2

    const/16 v2, 0x94

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_DOC_DELETE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 150
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_DOC_DOWNLOAD"

    move-object/from16 v151, v4

    const/16 v4, 0x95

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_DOC_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 151
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_DOC_EDIT"

    move-object/from16 v152, v2

    const/16 v2, 0x96

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_DOC_EDIT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 152
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_DOC_EDIT_COMMENT"

    move-object/from16 v153, v4

    const/16 v4, 0x97

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_DOC_EDIT_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 153
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_DOC_FOLLOWED"

    move-object/from16 v154, v2

    const/16 v2, 0x98

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_DOC_FOLLOWED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 154
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_DOC_MENTION"

    move-object/from16 v155, v4

    const/16 v4, 0x99

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_DOC_MENTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 155
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_DOC_OWNERSHIP_CHANGED"

    move-object/from16 v156, v2

    const/16 v2, 0x9a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_DOC_OWNERSHIP_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 156
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_DOC_REQUEST_ACCESS"

    move-object/from16 v157, v4

    const/16 v4, 0x9b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_DOC_REQUEST_ACCESS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 157
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_DOC_RESOLVE_COMMENT"

    move-object/from16 v158, v2

    const/16 v2, 0x9c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_DOC_RESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 158
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_DOC_REVERT"

    move-object/from16 v159, v4

    const/16 v4, 0x9d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_DOC_REVERT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 159
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_DOC_SLACK_SHARE"

    move-object/from16 v160, v2

    const/16 v2, 0x9e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_DOC_SLACK_SHARE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 160
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_DOC_TEAM_INVITE"

    move-object/from16 v161, v4

    const/16 v4, 0x9f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_DOC_TEAM_INVITE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 161
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_DOC_TRASHED"

    move-object/from16 v162, v2

    const/16 v2, 0xa0

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_DOC_TRASHED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 162
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_DOC_UNRESOLVE_COMMENT"

    move-object/from16 v163, v4

    const/16 v4, 0xa1

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_DOC_UNRESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 163
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_DOC_UNTRASHED"

    move-object/from16 v164, v2

    const/16 v2, 0xa2

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_DOC_UNTRASHED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 164
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_DOC_VIEW"

    move-object/from16 v165, v4

    const/16 v4, 0xa3

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_DOC_VIEW:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 165
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_EXTERNAL_VIEW_ALLOW"

    move-object/from16 v166, v2

    const/16 v2, 0xa4

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_EXTERNAL_VIEW_ALLOW:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 166
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_EXTERNAL_VIEW_DEFAULT_TEAM"

    move-object/from16 v167, v4

    const/16 v4, 0xa5

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_EXTERNAL_VIEW_DEFAULT_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 167
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_EXTERNAL_VIEW_FORBID"

    move-object/from16 v168, v2

    const/16 v2, 0xa6

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_EXTERNAL_VIEW_FORBID:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 168
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_FOLDER_CHANGE_SUBSCRIPTION"

    move-object/from16 v169, v4

    const/16 v4, 0xa7

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_FOLDER_CHANGE_SUBSCRIPTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 169
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_FOLDER_DELETED"

    move-object/from16 v170, v2

    const/16 v2, 0xa8

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_FOLDER_DELETED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 170
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_FOLDER_FOLLOWED"

    move-object/from16 v171, v4

    const/16 v4, 0xa9

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_FOLDER_FOLLOWED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 171
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_FOLDER_TEAM_INVITE"

    move-object/from16 v172, v2

    const/16 v2, 0xaa

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_FOLDER_TEAM_INVITE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 172
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_PUBLISHED_LINK_CHANGE_PERMISSION"

    move-object/from16 v173, v4

    const/16 v4, 0xab

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_PUBLISHED_LINK_CHANGE_PERMISSION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 173
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_PUBLISHED_LINK_CREATE"

    move-object/from16 v174, v2

    const/16 v2, 0xac

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_PUBLISHED_LINK_CREATE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 174
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_PUBLISHED_LINK_DISABLED"

    move-object/from16 v175, v4

    const/16 v4, 0xad

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_PUBLISHED_LINK_DISABLED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 175
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_PUBLISHED_LINK_VIEW"

    move-object/from16 v176, v2

    const/16 v2, 0xae

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_PUBLISHED_LINK_VIEW:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 176
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PASSWORD_CHANGE"

    move-object/from16 v177, v4

    const/16 v4, 0xaf

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PASSWORD_CHANGE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 177
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PASSWORD_RESET"

    move-object/from16 v178, v2

    const/16 v2, 0xb0

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PASSWORD_RESET:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 178
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PASSWORD_RESET_ALL"

    move-object/from16 v179, v4

    const/16 v4, 0xb1

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PASSWORD_RESET_ALL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 179
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "EMM_CREATE_EXCEPTIONS_REPORT"

    move-object/from16 v180, v2

    const/16 v2, 0xb2

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->EMM_CREATE_EXCEPTIONS_REPORT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 180
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "EMM_CREATE_USAGE_REPORT"

    move-object/from16 v181, v4

    const/16 v4, 0xb3

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->EMM_CREATE_USAGE_REPORT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 181
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "EXPORT_MEMBERS_REPORT"

    move-object/from16 v182, v2

    const/16 v2, 0xb4

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->EXPORT_MEMBERS_REPORT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 182
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "EXPORT_MEMBERS_REPORT_FAIL"

    move-object/from16 v183, v4

    const/16 v4, 0xb5

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->EXPORT_MEMBERS_REPORT_FAIL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 183
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "NO_EXPIRATION_LINK_GEN_CREATE_REPORT"

    move-object/from16 v184, v2

    const/16 v2, 0xb6

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->NO_EXPIRATION_LINK_GEN_CREATE_REPORT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 184
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "NO_EXPIRATION_LINK_GEN_REPORT_FAILED"

    move-object/from16 v185, v4

    const/16 v4, 0xb7

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->NO_EXPIRATION_LINK_GEN_REPORT_FAILED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 185
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "NO_PASSWORD_LINK_GEN_CREATE_REPORT"

    move-object/from16 v186, v2

    const/16 v2, 0xb8

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->NO_PASSWORD_LINK_GEN_CREATE_REPORT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 186
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "NO_PASSWORD_LINK_GEN_REPORT_FAILED"

    move-object/from16 v187, v4

    const/16 v4, 0xb9

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->NO_PASSWORD_LINK_GEN_REPORT_FAILED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 187
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "NO_PASSWORD_LINK_VIEW_CREATE_REPORT"

    move-object/from16 v188, v2

    const/16 v2, 0xba

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->NO_PASSWORD_LINK_VIEW_CREATE_REPORT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 188
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "NO_PASSWORD_LINK_VIEW_REPORT_FAILED"

    move-object/from16 v189, v4

    const/16 v4, 0xbb

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->NO_PASSWORD_LINK_VIEW_REPORT_FAILED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 189
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "OUTDATED_LINK_VIEW_CREATE_REPORT"

    move-object/from16 v190, v2

    const/16 v2, 0xbc

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->OUTDATED_LINK_VIEW_CREATE_REPORT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 190
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "OUTDATED_LINK_VIEW_REPORT_FAILED"

    move-object/from16 v191, v4

    const/16 v4, 0xbd

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->OUTDATED_LINK_VIEW_REPORT_FAILED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 191
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_ADMIN_EXPORT_START"

    move-object/from16 v192, v2

    const/16 v2, 0xbe

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_ADMIN_EXPORT_START:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 192
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SMART_SYNC_CREATE_ADMIN_PRIVILEGE_REPORT"

    move-object/from16 v193, v4

    const/16 v4, 0xbf

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SMART_SYNC_CREATE_ADMIN_PRIVILEGE_REPORT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 193
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_ACTIVITY_CREATE_REPORT"

    move-object/from16 v194, v2

    const/16 v2, 0xc0

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_ACTIVITY_CREATE_REPORT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 194
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_ACTIVITY_CREATE_REPORT_FAIL"

    move-object/from16 v195, v4

    const/16 v4, 0xc1

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_ACTIVITY_CREATE_REPORT_FAIL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 195
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "COLLECTION_SHARE"

    move-object/from16 v196, v2

    const/16 v2, 0xc2

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->COLLECTION_SHARE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 196
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_TRANSFERS_FILE_ADD"

    move-object/from16 v197, v4

    const/16 v4, 0xc3

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_TRANSFERS_FILE_ADD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 197
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_TRANSFERS_TRANSFER_DELETE"

    move-object/from16 v198, v2

    const/16 v2, 0xc4

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_TRANSFERS_TRANSFER_DELETE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 198
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_TRANSFERS_TRANSFER_DOWNLOAD"

    move-object/from16 v199, v4

    const/16 v4, 0xc5

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_TRANSFERS_TRANSFER_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 199
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_TRANSFERS_TRANSFER_SEND"

    move-object/from16 v200, v2

    const/16 v2, 0xc6

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_TRANSFERS_TRANSFER_SEND:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 200
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_TRANSFERS_TRANSFER_VIEW"

    move-object/from16 v201, v4

    const/16 v4, 0xc7

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_TRANSFERS_TRANSFER_VIEW:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 201
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "NOTE_ACL_INVITE_ONLY"

    move-object/from16 v202, v2

    const/16 v2, 0xc8

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->NOTE_ACL_INVITE_ONLY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 202
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "NOTE_ACL_LINK"

    move-object/from16 v203, v4

    const/16 v4, 0xc9

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->NOTE_ACL_LINK:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 203
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "NOTE_ACL_TEAM_LINK"

    move-object/from16 v204, v2

    const/16 v2, 0xca

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->NOTE_ACL_TEAM_LINK:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 204
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "NOTE_SHARED"

    move-object/from16 v205, v4

    const/16 v4, 0xcb

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->NOTE_SHARED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 205
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "NOTE_SHARE_RECEIVE"

    move-object/from16 v206, v2

    const/16 v2, 0xcc

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->NOTE_SHARE_RECEIVE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 206
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "OPEN_NOTE_SHARED"

    move-object/from16 v207, v4

    const/16 v4, 0xcd

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->OPEN_NOTE_SHARED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 207
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SF_ADD_GROUP"

    move-object/from16 v208, v2

    const/16 v2, 0xce

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SF_ADD_GROUP:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 208
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SF_ALLOW_NON_MEMBERS_TO_VIEW_SHARED_LINKS"

    move-object/from16 v209, v4

    const/16 v4, 0xcf

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SF_ALLOW_NON_MEMBERS_TO_VIEW_SHARED_LINKS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 209
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SF_EXTERNAL_INVITE_WARN"

    move-object/from16 v210, v2

    const/16 v2, 0xd0

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SF_EXTERNAL_INVITE_WARN:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 210
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SF_FB_INVITE"

    move-object/from16 v211, v4

    const/16 v4, 0xd1

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SF_FB_INVITE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 211
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SF_FB_INVITE_CHANGE_ROLE"

    move-object/from16 v212, v2

    const/16 v2, 0xd2

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SF_FB_INVITE_CHANGE_ROLE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 212
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SF_FB_UNINVITE"

    move-object/from16 v213, v4

    const/16 v4, 0xd3

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SF_FB_UNINVITE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 213
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SF_INVITE_GROUP"

    move-object/from16 v214, v2

    const/16 v2, 0xd4

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SF_INVITE_GROUP:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 214
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SF_TEAM_GRANT_ACCESS"

    move-object/from16 v215, v4

    const/16 v4, 0xd5

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SF_TEAM_GRANT_ACCESS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 215
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SF_TEAM_INVITE"

    move-object/from16 v216, v2

    const/16 v2, 0xd6

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SF_TEAM_INVITE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 216
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SF_TEAM_INVITE_CHANGE_ROLE"

    move-object/from16 v217, v4

    const/16 v4, 0xd7

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SF_TEAM_INVITE_CHANGE_ROLE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 217
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SF_TEAM_JOIN"

    move-object/from16 v218, v2

    const/16 v2, 0xd8

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SF_TEAM_JOIN:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 218
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SF_TEAM_JOIN_FROM_OOB_LINK"

    move-object/from16 v219, v4

    const/16 v4, 0xd9

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SF_TEAM_JOIN_FROM_OOB_LINK:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 219
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SF_TEAM_UNINVITE"

    move-object/from16 v220, v2

    const/16 v2, 0xda

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SF_TEAM_UNINVITE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 220
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_CONTENT_ADD_INVITEES"

    move-object/from16 v221, v4

    const/16 v4, 0xdb

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_CONTENT_ADD_INVITEES:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 221
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_CONTENT_ADD_LINK_EXPIRY"

    move-object/from16 v222, v2

    const/16 v2, 0xdc

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_CONTENT_ADD_LINK_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 222
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_CONTENT_ADD_LINK_PASSWORD"

    move-object/from16 v223, v4

    const/16 v4, 0xdd

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_CONTENT_ADD_LINK_PASSWORD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 223
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_CONTENT_ADD_MEMBER"

    move-object/from16 v224, v2

    const/16 v2, 0xde

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_CONTENT_ADD_MEMBER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 224
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_CONTENT_CHANGE_DOWNLOADS_POLICY"

    move-object/from16 v225, v4

    const/16 v4, 0xdf

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_CONTENT_CHANGE_DOWNLOADS_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 225
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_CONTENT_CHANGE_INVITEE_ROLE"

    move-object/from16 v226, v2

    const/16 v2, 0xe0

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_CONTENT_CHANGE_INVITEE_ROLE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 226
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_CONTENT_CHANGE_LINK_AUDIENCE"

    move-object/from16 v227, v4

    const/16 v4, 0xe1

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_CONTENT_CHANGE_LINK_AUDIENCE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 227
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_CONTENT_CHANGE_LINK_EXPIRY"

    move-object/from16 v228, v2

    const/16 v2, 0xe2

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_CONTENT_CHANGE_LINK_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 228
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_CONTENT_CHANGE_LINK_PASSWORD"

    move-object/from16 v229, v4

    const/16 v4, 0xe3

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_CONTENT_CHANGE_LINK_PASSWORD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 229
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_CONTENT_CHANGE_MEMBER_ROLE"

    move-object/from16 v230, v2

    const/16 v2, 0xe4

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_CONTENT_CHANGE_MEMBER_ROLE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 230
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_CONTENT_CHANGE_VIEWER_INFO_POLICY"

    move-object/from16 v231, v4

    const/16 v4, 0xe5

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_CONTENT_CHANGE_VIEWER_INFO_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 231
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_CONTENT_CLAIM_INVITATION"

    move-object/from16 v232, v2

    const/16 v2, 0xe6

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_CONTENT_CLAIM_INVITATION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 232
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_CONTENT_COPY"

    move-object/from16 v233, v4

    const/16 v4, 0xe7

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_CONTENT_COPY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 233
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_CONTENT_DOWNLOAD"

    move-object/from16 v234, v2

    const/16 v2, 0xe8

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_CONTENT_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 234
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_CONTENT_RELINQUISH_MEMBERSHIP"

    move-object/from16 v235, v4

    const/16 v4, 0xe9

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_CONTENT_RELINQUISH_MEMBERSHIP:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 235
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_CONTENT_REMOVE_INVITEES"

    move-object/from16 v236, v2

    const/16 v2, 0xea

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_CONTENT_REMOVE_INVITEES:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 236
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_CONTENT_REMOVE_LINK_EXPIRY"

    move-object/from16 v237, v4

    const/16 v4, 0xeb

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_CONTENT_REMOVE_LINK_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 237
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_CONTENT_REMOVE_LINK_PASSWORD"

    move-object/from16 v238, v2

    const/16 v2, 0xec

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_CONTENT_REMOVE_LINK_PASSWORD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 238
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_CONTENT_REMOVE_MEMBER"

    move-object/from16 v239, v4

    const/16 v4, 0xed

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_CONTENT_REMOVE_MEMBER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 239
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_CONTENT_REQUEST_ACCESS"

    move-object/from16 v240, v2

    const/16 v2, 0xee

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_CONTENT_REQUEST_ACCESS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 240
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_CONTENT_RESTORE_INVITEES"

    move-object/from16 v241, v4

    const/16 v4, 0xef

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_CONTENT_RESTORE_INVITEES:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 241
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_CONTENT_RESTORE_MEMBER"

    move-object/from16 v242, v2

    const/16 v2, 0xf0

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_CONTENT_RESTORE_MEMBER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 242
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_CONTENT_UNSHARE"

    move-object/from16 v243, v4

    const/16 v4, 0xf1

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_CONTENT_UNSHARE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 243
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_CONTENT_VIEW"

    move-object/from16 v244, v2

    const/16 v2, 0xf2

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_CONTENT_VIEW:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 244
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_FOLDER_CHANGE_LINK_POLICY"

    move-object/from16 v245, v4

    const/16 v4, 0xf3

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_FOLDER_CHANGE_LINK_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 245
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_FOLDER_CHANGE_MEMBERS_INHERITANCE_POLICY"

    move-object/from16 v246, v2

    const/16 v2, 0xf4

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_FOLDER_CHANGE_MEMBERS_INHERITANCE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 246
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_FOLDER_CHANGE_MEMBERS_MANAGEMENT_POLICY"

    move-object/from16 v247, v4

    const/16 v4, 0xf5

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_FOLDER_CHANGE_MEMBERS_MANAGEMENT_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 247
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_FOLDER_CHANGE_MEMBERS_POLICY"

    move-object/from16 v248, v2

    const/16 v2, 0xf6

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_FOLDER_CHANGE_MEMBERS_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 248
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_FOLDER_CREATE"

    move-object/from16 v249, v4

    const/16 v4, 0xf7

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_FOLDER_CREATE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 249
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_FOLDER_DECLINE_INVITATION"

    move-object/from16 v250, v2

    const/16 v2, 0xf8

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_FOLDER_DECLINE_INVITATION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 250
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_FOLDER_MOUNT"

    move-object/from16 v251, v4

    const/16 v4, 0xf9

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_FOLDER_MOUNT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 251
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_FOLDER_NEST"

    move-object/from16 v252, v2

    const/16 v2, 0xfa

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_FOLDER_NEST:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 252
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_FOLDER_TRANSFER_OWNERSHIP"

    move-object/from16 v253, v4

    const/16 v4, 0xfb

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_FOLDER_TRANSFER_OWNERSHIP:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 253
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_FOLDER_UNMOUNT"

    move-object/from16 v254, v2

    const/16 v2, 0xfc

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_FOLDER_UNMOUNT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 254
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_LINK_ADD_EXPIRY"

    move-object/from16 v255, v4

    const/16 v4, 0xfd

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_LINK_ADD_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 255
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_LINK_CHANGE_EXPIRY"

    move-object/16 v256, v2

    const/16 v2, 0xfe

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_LINK_CHANGE_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 256
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_LINK_CHANGE_VISIBILITY"

    move-object/16 v257, v4

    const/16 v4, 0xff

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_LINK_CHANGE_VISIBILITY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 257
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_LINK_COPY"

    move-object/16 v258, v2

    const/16 v2, 0x100

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_LINK_COPY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 258
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_LINK_CREATE"

    move-object/16 v259, v4

    const/16 v4, 0x101

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_LINK_CREATE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 259
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_LINK_DISABLE"

    move-object/16 v260, v2

    const/16 v2, 0x102

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_LINK_DISABLE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 260
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_LINK_DOWNLOAD"

    move-object/16 v261, v4

    const/16 v4, 0x103

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_LINK_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 261
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_LINK_REMOVE_EXPIRY"

    move-object/16 v262, v2

    const/16 v2, 0x104

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_LINK_REMOVE_EXPIRY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 262
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_LINK_SETTINGS_ADD_EXPIRATION"

    move-object/16 v263, v4

    const/16 v4, 0x105

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_LINK_SETTINGS_ADD_EXPIRATION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 263
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_LINK_SETTINGS_ADD_PASSWORD"

    move-object/16 v264, v2

    const/16 v2, 0x106

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_LINK_SETTINGS_ADD_PASSWORD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 264
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_LINK_SETTINGS_ALLOW_DOWNLOAD_DISABLED"

    move-object/16 v265, v4

    const/16 v4, 0x107

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_LINK_SETTINGS_ALLOW_DOWNLOAD_DISABLED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 265
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_LINK_SETTINGS_ALLOW_DOWNLOAD_ENABLED"

    move-object/16 v266, v2

    const/16 v2, 0x108

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_LINK_SETTINGS_ALLOW_DOWNLOAD_ENABLED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 266
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_LINK_SETTINGS_CHANGE_AUDIENCE"

    move-object/16 v267, v4

    const/16 v4, 0x109

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_LINK_SETTINGS_CHANGE_AUDIENCE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 267
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_LINK_SETTINGS_CHANGE_EXPIRATION"

    move-object/16 v268, v2

    const/16 v2, 0x10a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_LINK_SETTINGS_CHANGE_EXPIRATION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 268
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_LINK_SETTINGS_CHANGE_PASSWORD"

    move-object/16 v269, v4

    const/16 v4, 0x10b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_LINK_SETTINGS_CHANGE_PASSWORD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 269
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_LINK_SETTINGS_REMOVE_EXPIRATION"

    move-object/16 v270, v2

    const/16 v2, 0x10c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_LINK_SETTINGS_REMOVE_EXPIRATION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 270
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_LINK_SETTINGS_REMOVE_PASSWORD"

    move-object/16 v271, v4

    const/16 v4, 0x10d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_LINK_SETTINGS_REMOVE_PASSWORD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 271
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_LINK_SHARE"

    move-object/16 v272, v2

    const/16 v2, 0x10e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_LINK_SHARE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 272
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_LINK_VIEW"

    move-object/16 v273, v4

    const/16 v4, 0x10f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_LINK_VIEW:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 273
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARED_NOTE_OPENED"

    move-object/16 v274, v2

    const/16 v2, 0x110

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARED_NOTE_OPENED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 274
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHMODEL_GROUP_SHARE"

    move-object/16 v275, v4

    const/16 v4, 0x111

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHMODEL_GROUP_SHARE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 275
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_ACCESS_GRANTED"

    move-object/16 v276, v2

    const/16 v2, 0x112

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_ACCESS_GRANTED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 276
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_ADD_MEMBER"

    move-object/16 v277, v4

    const/16 v4, 0x113

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_ADD_MEMBER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 277
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_ARCHIVED"

    move-object/16 v278, v2

    const/16 v2, 0x114

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_ARCHIVED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 278
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_CREATED"

    move-object/16 v279, v4

    const/16 v4, 0x115

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_CREATED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 279
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_DELETE_COMMENT"

    move-object/16 v280, v2

    const/16 v2, 0x116

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_DELETE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 280
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_EDITED"

    move-object/16 v281, v4

    const/16 v4, 0x117

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_EDITED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 281
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_EDIT_COMMENT"

    move-object/16 v282, v2

    const/16 v2, 0x118

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_EDIT_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 282
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_FILE_ADDED"

    move-object/16 v283, v4

    const/16 v4, 0x119

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_FILE_ADDED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 283
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_FILE_DOWNLOAD"

    move-object/16 v284, v2

    const/16 v2, 0x11a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_FILE_DOWNLOAD:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 284
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_FILE_REMOVED"

    move-object/16 v285, v4

    const/16 v4, 0x11b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_FILE_REMOVED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 285
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_FILE_VIEW"

    move-object/16 v286, v2

    const/16 v2, 0x11c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_FILE_VIEW:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 286
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_PERMANENTLY_DELETED"

    move-object/16 v287, v4

    const/16 v4, 0x11d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_PERMANENTLY_DELETED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 287
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_POST_COMMENT"

    move-object/16 v288, v2

    const/16 v2, 0x11e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_POST_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 288
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_REMOVE_MEMBER"

    move-object/16 v289, v4

    const/16 v4, 0x11f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_REMOVE_MEMBER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 289
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_RENAMED"

    move-object/16 v290, v2

    const/16 v2, 0x120

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_RENAMED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 290
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_REQUEST_ACCESS"

    move-object/16 v291, v4

    const/16 v4, 0x121

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_REQUEST_ACCESS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 291
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_RESOLVE_COMMENT"

    move-object/16 v292, v2

    const/16 v2, 0x122

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_RESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 292
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_RESTORED"

    move-object/16 v293, v4

    const/16 v4, 0x123

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_RESTORED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 293
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_TRASHED"

    move-object/16 v294, v2

    const/16 v2, 0x124

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_TRASHED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 294
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_TRASHED_DEPRECATED"

    move-object/16 v295, v4

    const/16 v4, 0x125

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_TRASHED_DEPRECATED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 295
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_UNRESOLVE_COMMENT"

    move-object/16 v296, v2

    const/16 v2, 0x126

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_UNRESOLVE_COMMENT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 296
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_UNTRASHED"

    move-object/16 v297, v4

    const/16 v4, 0x127

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_UNTRASHED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 297
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_UNTRASHED_DEPRECATED"

    move-object/16 v298, v2

    const/16 v2, 0x128

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_UNTRASHED_DEPRECATED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 298
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_VIEW"

    move-object/16 v299, v4

    const/16 v4, 0x129

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_VIEW:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 299
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SSO_ADD_CERT"

    move-object/16 v300, v2

    const/16 v2, 0x12a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SSO_ADD_CERT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 300
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SSO_ADD_LOGIN_URL"

    move-object/16 v301, v4

    const/16 v4, 0x12b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SSO_ADD_LOGIN_URL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 301
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SSO_ADD_LOGOUT_URL"

    move-object/16 v302, v2

    const/16 v2, 0x12c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SSO_ADD_LOGOUT_URL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 302
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SSO_CHANGE_CERT"

    move-object/16 v303, v4

    const/16 v4, 0x12d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SSO_CHANGE_CERT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 303
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SSO_CHANGE_LOGIN_URL"

    move-object/16 v304, v2

    const/16 v2, 0x12e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SSO_CHANGE_LOGIN_URL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 304
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SSO_CHANGE_LOGOUT_URL"

    move-object/16 v305, v4

    const/16 v4, 0x12f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SSO_CHANGE_LOGOUT_URL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 305
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SSO_CHANGE_SAML_IDENTITY_MODE"

    move-object/16 v306, v2

    const/16 v2, 0x130

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SSO_CHANGE_SAML_IDENTITY_MODE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 306
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SSO_REMOVE_CERT"

    move-object/16 v307, v4

    const/16 v4, 0x131

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SSO_REMOVE_CERT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 307
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SSO_REMOVE_LOGIN_URL"

    move-object/16 v308, v2

    const/16 v2, 0x132

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SSO_REMOVE_LOGIN_URL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 308
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SSO_REMOVE_LOGOUT_URL"

    move-object/16 v309, v4

    const/16 v4, 0x133

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SSO_REMOVE_LOGOUT_URL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 309
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_FOLDER_CHANGE_STATUS"

    move-object/16 v310, v2

    const/16 v2, 0x134

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_FOLDER_CHANGE_STATUS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 310
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_FOLDER_CREATE"

    move-object/16 v311, v4

    const/16 v4, 0x135

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_FOLDER_CREATE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 311
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_FOLDER_DOWNGRADE"

    move-object/16 v312, v2

    const/16 v2, 0x136

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_FOLDER_DOWNGRADE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 312
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_FOLDER_PERMANENTLY_DELETE"

    move-object/16 v313, v4

    const/16 v4, 0x137

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_FOLDER_PERMANENTLY_DELETE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 313
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_FOLDER_RENAME"

    move-object/16 v314, v2

    const/16 v2, 0x138

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_FOLDER_RENAME:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 314
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_SELECTIVE_SYNC_SETTINGS_CHANGED"

    move-object/16 v315, v4

    const/16 v4, 0x139

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_SELECTIVE_SYNC_SETTINGS_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 315
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "ACCOUNT_CAPTURE_CHANGE_POLICY"

    move-object/16 v316, v2

    const/16 v2, 0x13a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->ACCOUNT_CAPTURE_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 316
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "ALLOW_DOWNLOAD_DISABLED"

    move-object/16 v317, v4

    const/16 v4, 0x13b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->ALLOW_DOWNLOAD_DISABLED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 317
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "ALLOW_DOWNLOAD_ENABLED"

    move-object/16 v318, v2

    const/16 v2, 0x13c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->ALLOW_DOWNLOAD_ENABLED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 318
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "CAMERA_UPLOADS_POLICY_CHANGED"

    move-object/16 v319, v4

    const/16 v4, 0x13d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->CAMERA_UPLOADS_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 319
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "DATA_PLACEMENT_RESTRICTION_CHANGE_POLICY"

    move-object/16 v320, v2

    const/16 v2, 0x13e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DATA_PLACEMENT_RESTRICTION_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 320
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "DATA_PLACEMENT_RESTRICTION_SATISFY_POLICY"

    move-object/16 v321, v4

    const/16 v4, 0x13f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DATA_PLACEMENT_RESTRICTION_SATISFY_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 321
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "DEVICE_APPROVALS_ADD_EXCEPTION"

    move-object/16 v322, v2

    const/16 v2, 0x140

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DEVICE_APPROVALS_ADD_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 322
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "DEVICE_APPROVALS_CHANGE_DESKTOP_POLICY"

    move-object/16 v323, v4

    const/16 v4, 0x141

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DEVICE_APPROVALS_CHANGE_DESKTOP_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 323
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "DEVICE_APPROVALS_CHANGE_MOBILE_POLICY"

    move-object/16 v324, v2

    const/16 v2, 0x142

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DEVICE_APPROVALS_CHANGE_MOBILE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 324
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "DEVICE_APPROVALS_CHANGE_OVERAGE_ACTION"

    move-object/16 v325, v4

    const/16 v4, 0x143

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DEVICE_APPROVALS_CHANGE_OVERAGE_ACTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 325
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "DEVICE_APPROVALS_CHANGE_UNLINK_ACTION"

    move-object/16 v326, v2

    const/16 v2, 0x144

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DEVICE_APPROVALS_CHANGE_UNLINK_ACTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 326
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "DEVICE_APPROVALS_REMOVE_EXCEPTION"

    move-object/16 v327, v4

    const/16 v4, 0x145

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DEVICE_APPROVALS_REMOVE_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 327
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "DIRECTORY_RESTRICTIONS_ADD_MEMBERS"

    move-object/16 v328, v2

    const/16 v2, 0x146

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DIRECTORY_RESTRICTIONS_ADD_MEMBERS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 328
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "DIRECTORY_RESTRICTIONS_REMOVE_MEMBERS"

    move-object/16 v329, v4

    const/16 v4, 0x147

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->DIRECTORY_RESTRICTIONS_REMOVE_MEMBERS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 329
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "EMM_ADD_EXCEPTION"

    move-object/16 v330, v2

    const/16 v2, 0x148

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->EMM_ADD_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 330
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "EMM_CHANGE_POLICY"

    move-object/16 v331, v4

    const/16 v4, 0x149

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->EMM_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 331
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "EMM_REMOVE_EXCEPTION"

    move-object/16 v332, v2

    const/16 v2, 0x14a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->EMM_REMOVE_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 332
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "EXTENDED_VERSION_HISTORY_CHANGE_POLICY"

    move-object/16 v333, v4

    const/16 v4, 0x14b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->EXTENDED_VERSION_HISTORY_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 333
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_COMMENTS_CHANGE_POLICY"

    move-object/16 v334, v2

    const/16 v2, 0x14c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_COMMENTS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 334
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_LOCKING_POLICY_CHANGED"

    move-object/16 v335, v4

    const/16 v4, 0x14d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_LOCKING_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 335
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_REQUESTS_CHANGE_POLICY"

    move-object/16 v336, v2

    const/16 v2, 0x14e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_REQUESTS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 336
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_REQUESTS_EMAILS_ENABLED"

    move-object/16 v337, v4

    const/16 v4, 0x14f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_REQUESTS_EMAILS_ENABLED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 337
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_REQUESTS_EMAILS_RESTRICTED_TO_TEAM_ONLY"

    move-object/16 v338, v2

    const/16 v2, 0x150

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_REQUESTS_EMAILS_RESTRICTED_TO_TEAM_ONLY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 338
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "FILE_TRANSFERS_POLICY_CHANGED"

    move-object/16 v339, v4

    const/16 v4, 0x151

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->FILE_TRANSFERS_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 339
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "GOOGLE_SSO_CHANGE_POLICY"

    move-object/16 v340, v2

    const/16 v2, 0x152

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->GOOGLE_SSO_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 340
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "GROUP_USER_MANAGEMENT_CHANGE_POLICY"

    move-object/16 v341, v4

    const/16 v4, 0x153

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->GROUP_USER_MANAGEMENT_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 341
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "INTEGRATION_POLICY_CHANGED"

    move-object/16 v342, v2

    const/16 v2, 0x154

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->INTEGRATION_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 342
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_REQUESTS_CHANGE_POLICY"

    move-object/16 v343, v4

    const/16 v4, 0x155

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_REQUESTS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 343
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_SEND_INVITE_POLICY_CHANGED"

    move-object/16 v344, v2

    const/16 v2, 0x156

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_SEND_INVITE_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 344
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_SPACE_LIMITS_ADD_EXCEPTION"

    move-object/16 v345, v4

    const/16 v4, 0x157

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_SPACE_LIMITS_ADD_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 345
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_SPACE_LIMITS_CHANGE_CAPS_TYPE_POLICY"

    move-object/16 v346, v2

    const/16 v2, 0x158

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_SPACE_LIMITS_CHANGE_CAPS_TYPE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 346
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_SPACE_LIMITS_CHANGE_POLICY"

    move-object/16 v347, v4

    const/16 v4, 0x159

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_SPACE_LIMITS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 347
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_SPACE_LIMITS_REMOVE_EXCEPTION"

    move-object/16 v348, v2

    const/16 v2, 0x15a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_SPACE_LIMITS_REMOVE_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 348
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MEMBER_SUGGESTIONS_CHANGE_POLICY"

    move-object/16 v349, v4

    const/16 v4, 0x15b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MEMBER_SUGGESTIONS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 349
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "MICROSOFT_OFFICE_ADDIN_CHANGE_POLICY"

    move-object/16 v350, v2

    const/16 v2, 0x15c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->MICROSOFT_OFFICE_ADDIN_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 350
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "NETWORK_CONTROL_CHANGE_POLICY"

    move-object/16 v351, v4

    const/16 v4, 0x15d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->NETWORK_CONTROL_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 351
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_CHANGE_DEPLOYMENT_POLICY"

    move-object/16 v352, v2

    const/16 v2, 0x15e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_CHANGE_DEPLOYMENT_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 352
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_CHANGE_MEMBER_LINK_POLICY"

    move-object/16 v353, v4

    const/16 v4, 0x15f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_CHANGE_MEMBER_LINK_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 353
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_CHANGE_MEMBER_POLICY"

    move-object/16 v354, v2

    const/16 v2, 0x160

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_CHANGE_MEMBER_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 354
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_CHANGE_POLICY"

    move-object/16 v355, v4

    const/16 v4, 0x161

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 355
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_DEFAULT_FOLDER_POLICY_CHANGED"

    move-object/16 v356, v2

    const/16 v2, 0x162

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_DEFAULT_FOLDER_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 356
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_DESKTOP_POLICY_CHANGED"

    move-object/16 v357, v4

    const/16 v4, 0x163

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_DESKTOP_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 357
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_ENABLED_USERS_GROUP_ADDITION"

    move-object/16 v358, v2

    const/16 v2, 0x164

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_ENABLED_USERS_GROUP_ADDITION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 358
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PAPER_ENABLED_USERS_GROUP_REMOVAL"

    move-object/16 v359, v4

    const/16 v4, 0x165

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PAPER_ENABLED_USERS_GROUP_REMOVAL:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 359
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PASSWORD_STRENGTH_REQUIREMENTS_CHANGE_POLICY"

    move-object/16 v360, v2

    const/16 v2, 0x166

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PASSWORD_STRENGTH_REQUIREMENTS_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 360
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "PERMANENT_DELETE_CHANGE_POLICY"

    move-object/16 v361, v4

    const/16 v4, 0x167

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->PERMANENT_DELETE_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 361
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "RESELLER_SUPPORT_CHANGE_POLICY"

    move-object/16 v362, v2

    const/16 v2, 0x168

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->RESELLER_SUPPORT_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 362
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "REWIND_POLICY_CHANGED"

    move-object/16 v363, v4

    const/16 v4, 0x169

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->REWIND_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 363
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARING_CHANGE_FOLDER_JOIN_POLICY"

    move-object/16 v364, v2

    const/16 v2, 0x16a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARING_CHANGE_FOLDER_JOIN_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 364
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARING_CHANGE_LINK_POLICY"

    move-object/16 v365, v4

    const/16 v4, 0x16b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARING_CHANGE_LINK_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 365
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHARING_CHANGE_MEMBER_POLICY"

    move-object/16 v366, v2

    const/16 v2, 0x16c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHARING_CHANGE_MEMBER_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 366
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_CHANGE_DOWNLOAD_POLICY"

    move-object/16 v367, v4

    const/16 v4, 0x16d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_CHANGE_DOWNLOAD_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 367
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_CHANGE_ENABLED_POLICY"

    move-object/16 v368, v2

    const/16 v2, 0x16e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_CHANGE_ENABLED_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 368
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SHOWCASE_CHANGE_EXTERNAL_SHARING_POLICY"

    move-object/16 v369, v4

    const/16 v4, 0x16f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SHOWCASE_CHANGE_EXTERNAL_SHARING_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 369
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SMARTER_SMART_SYNC_POLICY_CHANGED"

    move-object/16 v370, v2

    const/16 v2, 0x170

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SMARTER_SMART_SYNC_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 370
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SMART_SYNC_CHANGE_POLICY"

    move-object/16 v371, v4

    const/16 v4, 0x171

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SMART_SYNC_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 371
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SMART_SYNC_NOT_OPT_OUT"

    move-object/16 v372, v2

    const/16 v2, 0x172

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SMART_SYNC_NOT_OPT_OUT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 372
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SMART_SYNC_OPT_OUT"

    move-object/16 v373, v4

    const/16 v4, 0x173

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SMART_SYNC_OPT_OUT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 373
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "SSO_CHANGE_POLICY"

    move-object/16 v374, v2

    const/16 v2, 0x174

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->SSO_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 374
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_EXTENSIONS_POLICY_CHANGED"

    move-object/16 v375, v4

    const/16 v4, 0x175

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_EXTENSIONS_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 375
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_SELECTIVE_SYNC_POLICY_CHANGED"

    move-object/16 v376, v2

    const/16 v2, 0x176

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_SELECTIVE_SYNC_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 376
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_SHARING_WHITELIST_SUBJECTS_CHANGED"

    move-object/16 v377, v4

    const/16 v4, 0x177

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_SHARING_WHITELIST_SUBJECTS_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 377
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TFA_ADD_EXCEPTION"

    move-object/16 v378, v2

    const/16 v2, 0x178

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TFA_ADD_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 378
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TFA_CHANGE_POLICY"

    move-object/16 v379, v4

    const/16 v4, 0x179

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TFA_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 379
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TFA_REMOVE_EXCEPTION"

    move-object/16 v380, v2

    const/16 v2, 0x17a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TFA_REMOVE_EXCEPTION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 380
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TWO_ACCOUNT_CHANGE_POLICY"

    move-object/16 v381, v4

    const/16 v4, 0x17b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TWO_ACCOUNT_CHANGE_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 381
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "VIEWER_INFO_POLICY_CHANGED"

    move-object/16 v382, v2

    const/16 v2, 0x17c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->VIEWER_INFO_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 382
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "WATERMARKING_POLICY_CHANGED"

    move-object/16 v383, v4

    const/16 v4, 0x17d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->WATERMARKING_POLICY_CHANGED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 383
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "WEB_SESSIONS_CHANGE_ACTIVE_SESSION_LIMIT"

    move-object/16 v384, v2

    const/16 v2, 0x17e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->WEB_SESSIONS_CHANGE_ACTIVE_SESSION_LIMIT:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 384
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "WEB_SESSIONS_CHANGE_FIXED_LENGTH_POLICY"

    move-object/16 v385, v4

    const/16 v4, 0x17f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->WEB_SESSIONS_CHANGE_FIXED_LENGTH_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 385
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "WEB_SESSIONS_CHANGE_IDLE_LENGTH_POLICY"

    move-object/16 v386, v2

    const/16 v2, 0x180

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->WEB_SESSIONS_CHANGE_IDLE_LENGTH_POLICY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 386
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_MERGE_FROM"

    move-object/16 v387, v4

    const/16 v4, 0x181

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_MERGE_FROM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 387
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_MERGE_TO"

    move-object/16 v388, v2

    const/16 v2, 0x182

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_MERGE_TO:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 388
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_PROFILE_ADD_LOGO"

    move-object/16 v389, v4

    const/16 v4, 0x183

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_PROFILE_ADD_LOGO:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 389
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_PROFILE_CHANGE_DEFAULT_LANGUAGE"

    move-object/16 v390, v2

    const/16 v2, 0x184

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_PROFILE_CHANGE_DEFAULT_LANGUAGE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 390
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_PROFILE_CHANGE_LOGO"

    move-object/16 v391, v4

    const/16 v4, 0x185

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_PROFILE_CHANGE_LOGO:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 391
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_PROFILE_CHANGE_NAME"

    move-object/16 v392, v2

    const/16 v2, 0x186

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_PROFILE_CHANGE_NAME:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 392
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_PROFILE_REMOVE_LOGO"

    move-object/16 v393, v4

    const/16 v4, 0x187

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_PROFILE_REMOVE_LOGO:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 393
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TFA_ADD_BACKUP_PHONE"

    move-object/16 v394, v2

    const/16 v2, 0x188

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TFA_ADD_BACKUP_PHONE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 394
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TFA_ADD_SECURITY_KEY"

    move-object/16 v395, v4

    const/16 v4, 0x189

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TFA_ADD_SECURITY_KEY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 395
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TFA_CHANGE_BACKUP_PHONE"

    move-object/16 v396, v2

    const/16 v2, 0x18a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TFA_CHANGE_BACKUP_PHONE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 396
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TFA_CHANGE_STATUS"

    move-object/16 v397, v4

    const/16 v4, 0x18b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TFA_CHANGE_STATUS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 397
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TFA_REMOVE_BACKUP_PHONE"

    move-object/16 v398, v2

    const/16 v2, 0x18c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TFA_REMOVE_BACKUP_PHONE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 398
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TFA_REMOVE_SECURITY_KEY"

    move-object/16 v399, v4

    const/16 v4, 0x18d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TFA_REMOVE_SECURITY_KEY:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 399
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TFA_RESET"

    move-object/16 v400, v2

    const/16 v2, 0x18e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TFA_RESET:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 400
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "CHANGED_ENTERPRISE_ADMIN_ROLE"

    move-object/16 v401, v4

    const/16 v4, 0x18f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->CHANGED_ENTERPRISE_ADMIN_ROLE:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 401
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "CHANGED_ENTERPRISE_CONNECTED_TEAM_STATUS"

    move-object/16 v402, v2

    const/16 v2, 0x190

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->CHANGED_ENTERPRISE_CONNECTED_TEAM_STATUS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 402
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "ENDED_ENTERPRISE_ADMIN_SESSION"

    move-object/16 v403, v4

    const/16 v4, 0x191

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->ENDED_ENTERPRISE_ADMIN_SESSION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 403
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "ENDED_ENTERPRISE_ADMIN_SESSION_DEPRECATED"

    move-object/16 v404, v2

    const/16 v2, 0x192

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->ENDED_ENTERPRISE_ADMIN_SESSION_DEPRECATED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 404
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "ENTERPRISE_SETTINGS_LOCKING"

    move-object/16 v405, v4

    const/16 v4, 0x193

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->ENTERPRISE_SETTINGS_LOCKING:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 405
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "GUEST_ADMIN_CHANGE_STATUS"

    move-object/16 v406, v2

    const/16 v2, 0x194

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->GUEST_ADMIN_CHANGE_STATUS:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 406
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "STARTED_ENTERPRISE_ADMIN_SESSION"

    move-object/16 v407, v4

    const/16 v4, 0x195

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->STARTED_ENTERPRISE_ADMIN_SESSION:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 407
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_MERGE_REQUEST_ACCEPTED"

    move-object/16 v408, v2

    const/16 v2, 0x196

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_MERGE_REQUEST_ACCEPTED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 408
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_MERGE_REQUEST_ACCEPTED_SHOWN_TO_PRIMARY_TEAM"

    move-object/16 v409, v4

    const/16 v4, 0x197

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_MERGE_REQUEST_ACCEPTED_SHOWN_TO_PRIMARY_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 409
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_MERGE_REQUEST_ACCEPTED_SHOWN_TO_SECONDARY_TEAM"

    move-object/16 v410, v2

    const/16 v2, 0x198

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_MERGE_REQUEST_ACCEPTED_SHOWN_TO_SECONDARY_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 410
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_MERGE_REQUEST_AUTO_CANCELED"

    move-object/16 v411, v4

    const/16 v4, 0x199

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_MERGE_REQUEST_AUTO_CANCELED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 411
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_MERGE_REQUEST_CANCELED"

    move-object/16 v412, v2

    const/16 v2, 0x19a

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_MERGE_REQUEST_CANCELED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 412
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_MERGE_REQUEST_CANCELED_SHOWN_TO_PRIMARY_TEAM"

    move-object/16 v413, v4

    const/16 v4, 0x19b

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_MERGE_REQUEST_CANCELED_SHOWN_TO_PRIMARY_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 413
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_MERGE_REQUEST_CANCELED_SHOWN_TO_SECONDARY_TEAM"

    move-object/16 v414, v2

    const/16 v2, 0x19c

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_MERGE_REQUEST_CANCELED_SHOWN_TO_SECONDARY_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 414
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_MERGE_REQUEST_EXPIRED"

    move-object/16 v415, v4

    const/16 v4, 0x19d

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_MERGE_REQUEST_EXPIRED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 415
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_MERGE_REQUEST_EXPIRED_SHOWN_TO_PRIMARY_TEAM"

    move-object/16 v416, v2

    const/16 v2, 0x19e

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_MERGE_REQUEST_EXPIRED_SHOWN_TO_PRIMARY_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 416
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_MERGE_REQUEST_EXPIRED_SHOWN_TO_SECONDARY_TEAM"

    move-object/16 v417, v4

    const/16 v4, 0x19f

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_MERGE_REQUEST_EXPIRED_SHOWN_TO_SECONDARY_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 417
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_MERGE_REQUEST_REJECTED_SHOWN_TO_PRIMARY_TEAM"

    move-object/16 v418, v2

    const/16 v2, 0x1a0

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_MERGE_REQUEST_REJECTED_SHOWN_TO_PRIMARY_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 418
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_MERGE_REQUEST_REJECTED_SHOWN_TO_SECONDARY_TEAM"

    move-object/16 v419, v4

    const/16 v4, 0x1a1

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_MERGE_REQUEST_REJECTED_SHOWN_TO_SECONDARY_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 419
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_MERGE_REQUEST_REMINDER"

    move-object/16 v420, v2

    const/16 v2, 0x1a2

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_MERGE_REQUEST_REMINDER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 420
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_MERGE_REQUEST_REMINDER_SHOWN_TO_PRIMARY_TEAM"

    move-object/16 v421, v4

    const/16 v4, 0x1a3

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_MERGE_REQUEST_REMINDER_SHOWN_TO_PRIMARY_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 421
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_MERGE_REQUEST_REMINDER_SHOWN_TO_SECONDARY_TEAM"

    move-object/16 v422, v2

    const/16 v2, 0x1a4

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_MERGE_REQUEST_REMINDER_SHOWN_TO_SECONDARY_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 422
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_MERGE_REQUEST_REVOKED"

    move-object/16 v423, v4

    const/16 v4, 0x1a5

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_MERGE_REQUEST_REVOKED:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 423
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_MERGE_REQUEST_SENT_SHOWN_TO_PRIMARY_TEAM"

    move-object/16 v424, v2

    const/16 v2, 0x1a6

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_MERGE_REQUEST_SENT_SHOWN_TO_PRIMARY_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 424
    new-instance v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "TEAM_MERGE_REQUEST_SENT_SHOWN_TO_SECONDARY_TEAM"

    move-object/16 v425, v4

    const/16 v4, 0x1a7

    invoke-direct {v2, v6, v4}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->TEAM_MERGE_REQUEST_SENT_SHOWN_TO_SECONDARY_TEAM:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 425
    new-instance v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const-string v6, "OTHER"

    move-object/16 v426, v2

    const/16 v2, 0x1a8

    invoke-direct {v4, v6, v2}, Lcom/dropbox/core/v2/teamlog/EventTypeArg;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->OTHER:Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const/16 v2, 0x1a9

    new-array v2, v2, [Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    const/4 v6, 0x0

    aput-object v0, v2, v6

    const/4 v0, 0x1

    aput-object v1, v2, v0

    const/4 v0, 0x2

    aput-object v3, v2, v0

    const/4 v0, 0x3

    aput-object v5, v2, v0

    const/4 v0, 0x4

    aput-object v7, v2, v0

    const/4 v0, 0x5

    aput-object v9, v2, v0

    const/4 v0, 0x6

    aput-object v11, v2, v0

    const/4 v0, 0x7

    aput-object v13, v2, v0

    const/16 v0, 0x8

    aput-object v15, v2, v0

    const/16 v0, 0x9

    aput-object v14, v2, v0

    const/16 v0, 0xa

    aput-object v12, v2, v0

    const/16 v0, 0xb

    aput-object v10, v2, v0

    const/16 v0, 0xc

    aput-object v8, v2, v0

    const/16 v0, 0xd

    aput-object v16, v2, v0

    const/16 v0, 0xe

    aput-object v17, v2, v0

    const/16 v0, 0xf

    aput-object v18, v2, v0

    const/16 v0, 0x10

    aput-object v19, v2, v0

    const/16 v0, 0x11

    aput-object v20, v2, v0

    const/16 v0, 0x12

    aput-object v21, v2, v0

    const/16 v0, 0x13

    aput-object v22, v2, v0

    const/16 v0, 0x14

    aput-object v23, v2, v0

    const/16 v0, 0x15

    aput-object v24, v2, v0

    const/16 v0, 0x16

    aput-object v25, v2, v0

    const/16 v0, 0x17

    aput-object v26, v2, v0

    const/16 v0, 0x18

    aput-object v27, v2, v0

    const/16 v0, 0x19

    aput-object v28, v2, v0

    const/16 v0, 0x1a

    aput-object v29, v2, v0

    const/16 v0, 0x1b

    aput-object v30, v2, v0

    const/16 v0, 0x1c

    aput-object v31, v2, v0

    const/16 v0, 0x1d

    aput-object v32, v2, v0

    const/16 v0, 0x1e

    aput-object v33, v2, v0

    const/16 v0, 0x1f

    aput-object v34, v2, v0

    const/16 v0, 0x20

    aput-object v35, v2, v0

    const/16 v0, 0x21

    aput-object v36, v2, v0

    const/16 v0, 0x22

    aput-object v37, v2, v0

    const/16 v0, 0x23

    aput-object v38, v2, v0

    const/16 v0, 0x24

    aput-object v39, v2, v0

    const/16 v0, 0x25

    aput-object v40, v2, v0

    const/16 v0, 0x26

    aput-object v41, v2, v0

    const/16 v0, 0x27

    aput-object v42, v2, v0

    const/16 v0, 0x28

    aput-object v43, v2, v0

    const/16 v0, 0x29

    aput-object v44, v2, v0

    const/16 v0, 0x2a

    aput-object v45, v2, v0

    const/16 v0, 0x2b

    aput-object v46, v2, v0

    const/16 v0, 0x2c

    aput-object v47, v2, v0

    const/16 v0, 0x2d

    aput-object v48, v2, v0

    const/16 v0, 0x2e

    aput-object v49, v2, v0

    const/16 v0, 0x2f

    aput-object v50, v2, v0

    const/16 v0, 0x30

    aput-object v51, v2, v0

    const/16 v0, 0x31

    aput-object v52, v2, v0

    const/16 v0, 0x32

    aput-object v53, v2, v0

    const/16 v0, 0x33

    aput-object v54, v2, v0

    const/16 v0, 0x34

    aput-object v55, v2, v0

    const/16 v0, 0x35

    aput-object v56, v2, v0

    const/16 v0, 0x36

    aput-object v57, v2, v0

    const/16 v0, 0x37

    aput-object v58, v2, v0

    const/16 v0, 0x38

    aput-object v59, v2, v0

    const/16 v0, 0x39

    aput-object v60, v2, v0

    const/16 v0, 0x3a

    aput-object v61, v2, v0

    const/16 v0, 0x3b

    aput-object v62, v2, v0

    const/16 v0, 0x3c

    aput-object v63, v2, v0

    const/16 v0, 0x3d

    aput-object v64, v2, v0

    const/16 v0, 0x3e

    aput-object v65, v2, v0

    const/16 v0, 0x3f

    aput-object v66, v2, v0

    const/16 v0, 0x40

    aput-object v67, v2, v0

    const/16 v0, 0x41

    aput-object v68, v2, v0

    const/16 v0, 0x42

    aput-object v69, v2, v0

    const/16 v0, 0x43

    aput-object v70, v2, v0

    const/16 v0, 0x44

    aput-object v71, v2, v0

    const/16 v0, 0x45

    aput-object v72, v2, v0

    const/16 v0, 0x46

    aput-object v73, v2, v0

    const/16 v0, 0x47

    aput-object v74, v2, v0

    const/16 v0, 0x48

    aput-object v75, v2, v0

    const/16 v0, 0x49

    aput-object v76, v2, v0

    const/16 v0, 0x4a

    aput-object v77, v2, v0

    const/16 v0, 0x4b

    aput-object v78, v2, v0

    const/16 v0, 0x4c

    aput-object v79, v2, v0

    const/16 v0, 0x4d

    aput-object v80, v2, v0

    const/16 v0, 0x4e

    aput-object v81, v2, v0

    const/16 v0, 0x4f

    aput-object v82, v2, v0

    const/16 v0, 0x50

    aput-object v83, v2, v0

    const/16 v0, 0x51

    aput-object v84, v2, v0

    const/16 v0, 0x52

    aput-object v85, v2, v0

    const/16 v0, 0x53

    aput-object v86, v2, v0

    const/16 v0, 0x54

    aput-object v87, v2, v0

    const/16 v0, 0x55

    aput-object v88, v2, v0

    const/16 v0, 0x56

    aput-object v89, v2, v0

    const/16 v0, 0x57

    aput-object v90, v2, v0

    const/16 v0, 0x58

    aput-object v91, v2, v0

    const/16 v0, 0x59

    aput-object v92, v2, v0

    const/16 v0, 0x5a

    aput-object v93, v2, v0

    const/16 v0, 0x5b

    aput-object v94, v2, v0

    const/16 v0, 0x5c

    aput-object v95, v2, v0

    const/16 v0, 0x5d

    aput-object v96, v2, v0

    const/16 v0, 0x5e

    aput-object v97, v2, v0

    const/16 v0, 0x5f

    aput-object v98, v2, v0

    const/16 v0, 0x60

    aput-object v99, v2, v0

    const/16 v0, 0x61

    aput-object v100, v2, v0

    const/16 v0, 0x62

    aput-object v101, v2, v0

    const/16 v0, 0x63

    aput-object v102, v2, v0

    const/16 v0, 0x64

    aput-object v103, v2, v0

    const/16 v0, 0x65

    aput-object v104, v2, v0

    const/16 v0, 0x66

    aput-object v105, v2, v0

    const/16 v0, 0x67

    aput-object v106, v2, v0

    const/16 v0, 0x68

    aput-object v107, v2, v0

    const/16 v0, 0x69

    aput-object v108, v2, v0

    const/16 v0, 0x6a

    aput-object v109, v2, v0

    const/16 v0, 0x6b

    aput-object v110, v2, v0

    const/16 v0, 0x6c

    aput-object v111, v2, v0

    const/16 v0, 0x6d

    aput-object v112, v2, v0

    const/16 v0, 0x6e

    aput-object v113, v2, v0

    const/16 v0, 0x6f

    aput-object v114, v2, v0

    const/16 v0, 0x70

    aput-object v115, v2, v0

    const/16 v0, 0x71

    aput-object v116, v2, v0

    const/16 v0, 0x72

    aput-object v117, v2, v0

    const/16 v0, 0x73

    aput-object v118, v2, v0

    const/16 v0, 0x74

    aput-object v119, v2, v0

    const/16 v0, 0x75

    aput-object v120, v2, v0

    const/16 v0, 0x76

    aput-object v121, v2, v0

    const/16 v0, 0x77

    aput-object v122, v2, v0

    const/16 v0, 0x78

    aput-object v123, v2, v0

    const/16 v0, 0x79

    aput-object v124, v2, v0

    const/16 v0, 0x7a

    aput-object v125, v2, v0

    const/16 v0, 0x7b

    aput-object v126, v2, v0

    const/16 v0, 0x7c

    aput-object v127, v2, v0

    const/16 v0, 0x7d

    aput-object v128, v2, v0

    const/16 v0, 0x7e

    aput-object v129, v2, v0

    const/16 v0, 0x7f

    aput-object v130, v2, v0

    const/16 v0, 0x80

    aput-object v131, v2, v0

    const/16 v0, 0x81

    aput-object v132, v2, v0

    const/16 v0, 0x82

    aput-object v133, v2, v0

    const/16 v0, 0x83

    aput-object v134, v2, v0

    const/16 v0, 0x84

    aput-object v135, v2, v0

    const/16 v0, 0x85

    aput-object v136, v2, v0

    const/16 v0, 0x86

    aput-object v137, v2, v0

    const/16 v0, 0x87

    aput-object v138, v2, v0

    const/16 v0, 0x88

    aput-object v139, v2, v0

    const/16 v0, 0x89

    aput-object v140, v2, v0

    const/16 v0, 0x8a

    aput-object v141, v2, v0

    const/16 v0, 0x8b

    aput-object v142, v2, v0

    const/16 v0, 0x8c

    aput-object v143, v2, v0

    const/16 v0, 0x8d

    aput-object v144, v2, v0

    const/16 v0, 0x8e

    aput-object v145, v2, v0

    const/16 v0, 0x8f

    aput-object v146, v2, v0

    const/16 v0, 0x90

    aput-object v147, v2, v0

    const/16 v0, 0x91

    aput-object v148, v2, v0

    const/16 v0, 0x92

    aput-object v149, v2, v0

    const/16 v0, 0x93

    aput-object v150, v2, v0

    const/16 v0, 0x94

    aput-object v151, v2, v0

    const/16 v0, 0x95

    aput-object v152, v2, v0

    const/16 v0, 0x96

    aput-object v153, v2, v0

    const/16 v0, 0x97

    aput-object v154, v2, v0

    const/16 v0, 0x98

    aput-object v155, v2, v0

    const/16 v0, 0x99

    aput-object v156, v2, v0

    const/16 v0, 0x9a

    aput-object v157, v2, v0

    const/16 v0, 0x9b

    aput-object v158, v2, v0

    const/16 v0, 0x9c

    aput-object v159, v2, v0

    const/16 v0, 0x9d

    aput-object v160, v2, v0

    const/16 v0, 0x9e

    aput-object v161, v2, v0

    const/16 v0, 0x9f

    aput-object v162, v2, v0

    const/16 v0, 0xa0

    aput-object v163, v2, v0

    const/16 v0, 0xa1

    aput-object v164, v2, v0

    const/16 v0, 0xa2

    aput-object v165, v2, v0

    const/16 v0, 0xa3

    aput-object v166, v2, v0

    const/16 v0, 0xa4

    aput-object v167, v2, v0

    const/16 v0, 0xa5

    aput-object v168, v2, v0

    const/16 v0, 0xa6

    aput-object v169, v2, v0

    const/16 v0, 0xa7

    aput-object v170, v2, v0

    const/16 v0, 0xa8

    aput-object v171, v2, v0

    const/16 v0, 0xa9

    aput-object v172, v2, v0

    const/16 v0, 0xaa

    aput-object v173, v2, v0

    const/16 v0, 0xab

    aput-object v174, v2, v0

    const/16 v0, 0xac

    aput-object v175, v2, v0

    const/16 v0, 0xad

    aput-object v176, v2, v0

    const/16 v0, 0xae

    aput-object v177, v2, v0

    const/16 v0, 0xaf

    aput-object v178, v2, v0

    const/16 v0, 0xb0

    aput-object v179, v2, v0

    const/16 v0, 0xb1

    aput-object v180, v2, v0

    const/16 v0, 0xb2

    aput-object v181, v2, v0

    const/16 v0, 0xb3

    aput-object v182, v2, v0

    const/16 v0, 0xb4

    aput-object v183, v2, v0

    const/16 v0, 0xb5

    aput-object v184, v2, v0

    const/16 v0, 0xb6

    aput-object v185, v2, v0

    const/16 v0, 0xb7

    aput-object v186, v2, v0

    const/16 v0, 0xb8

    aput-object v187, v2, v0

    const/16 v0, 0xb9

    aput-object v188, v2, v0

    const/16 v0, 0xba

    aput-object v189, v2, v0

    const/16 v0, 0xbb

    aput-object v190, v2, v0

    const/16 v0, 0xbc

    aput-object v191, v2, v0

    const/16 v0, 0xbd

    aput-object v192, v2, v0

    const/16 v0, 0xbe

    aput-object v193, v2, v0

    const/16 v0, 0xbf

    aput-object v194, v2, v0

    const/16 v0, 0xc0

    aput-object v195, v2, v0

    const/16 v0, 0xc1

    aput-object v196, v2, v0

    const/16 v0, 0xc2

    aput-object v197, v2, v0

    const/16 v0, 0xc3

    aput-object v198, v2, v0

    const/16 v0, 0xc4

    aput-object v199, v2, v0

    const/16 v0, 0xc5

    aput-object v200, v2, v0

    const/16 v0, 0xc6

    aput-object v201, v2, v0

    const/16 v0, 0xc7

    aput-object v202, v2, v0

    const/16 v0, 0xc8

    aput-object v203, v2, v0

    const/16 v0, 0xc9

    aput-object v204, v2, v0

    const/16 v0, 0xca

    aput-object v205, v2, v0

    const/16 v0, 0xcb

    aput-object v206, v2, v0

    const/16 v0, 0xcc

    aput-object v207, v2, v0

    const/16 v0, 0xcd

    aput-object v208, v2, v0

    const/16 v0, 0xce

    aput-object v209, v2, v0

    const/16 v0, 0xcf

    aput-object v210, v2, v0

    const/16 v0, 0xd0

    aput-object v211, v2, v0

    const/16 v0, 0xd1

    aput-object v212, v2, v0

    const/16 v0, 0xd2

    aput-object v213, v2, v0

    const/16 v0, 0xd3

    aput-object v214, v2, v0

    const/16 v0, 0xd4

    aput-object v215, v2, v0

    const/16 v0, 0xd5

    aput-object v216, v2, v0

    const/16 v0, 0xd6

    aput-object v217, v2, v0

    const/16 v0, 0xd7

    aput-object v218, v2, v0

    const/16 v0, 0xd8

    aput-object v219, v2, v0

    const/16 v0, 0xd9

    aput-object v220, v2, v0

    const/16 v0, 0xda

    aput-object v221, v2, v0

    const/16 v0, 0xdb

    aput-object v222, v2, v0

    const/16 v0, 0xdc

    aput-object v223, v2, v0

    const/16 v0, 0xdd

    aput-object v224, v2, v0

    const/16 v0, 0xde

    aput-object v225, v2, v0

    const/16 v0, 0xdf

    aput-object v226, v2, v0

    const/16 v0, 0xe0

    aput-object v227, v2, v0

    const/16 v0, 0xe1

    aput-object v228, v2, v0

    const/16 v0, 0xe2

    aput-object v229, v2, v0

    const/16 v0, 0xe3

    aput-object v230, v2, v0

    const/16 v0, 0xe4

    aput-object v231, v2, v0

    const/16 v0, 0xe5

    aput-object v232, v2, v0

    const/16 v0, 0xe6

    aput-object v233, v2, v0

    const/16 v0, 0xe7

    move-object/from16 v1, v234

    aput-object v1, v2, v0

    const/16 v0, 0xe8

    move-object/from16 v1, v235

    aput-object v1, v2, v0

    const/16 v0, 0xe9

    move-object/from16 v1, v236

    aput-object v1, v2, v0

    const/16 v0, 0xea

    move-object/from16 v1, v237

    aput-object v1, v2, v0

    const/16 v0, 0xeb

    move-object/from16 v1, v238

    aput-object v1, v2, v0

    const/16 v0, 0xec

    move-object/from16 v1, v239

    aput-object v1, v2, v0

    const/16 v0, 0xed

    move-object/from16 v1, v240

    aput-object v1, v2, v0

    const/16 v0, 0xee

    move-object/from16 v1, v241

    aput-object v1, v2, v0

    const/16 v0, 0xef

    move-object/from16 v1, v242

    aput-object v1, v2, v0

    const/16 v0, 0xf0

    move-object/from16 v1, v243

    aput-object v1, v2, v0

    const/16 v0, 0xf1

    move-object/from16 v1, v244

    aput-object v1, v2, v0

    const/16 v0, 0xf2

    move-object/from16 v1, v245

    aput-object v1, v2, v0

    const/16 v0, 0xf3

    move-object/from16 v1, v246

    aput-object v1, v2, v0

    const/16 v0, 0xf4

    move-object/from16 v1, v247

    aput-object v1, v2, v0

    const/16 v0, 0xf5

    move-object/from16 v1, v248

    aput-object v1, v2, v0

    const/16 v0, 0xf6

    move-object/from16 v1, v249

    aput-object v1, v2, v0

    const/16 v0, 0xf7

    move-object/from16 v1, v250

    aput-object v1, v2, v0

    const/16 v0, 0xf8

    move-object/from16 v1, v251

    aput-object v1, v2, v0

    const/16 v0, 0xf9

    move-object/from16 v1, v252

    aput-object v1, v2, v0

    const/16 v0, 0xfa

    move-object/from16 v1, v253

    aput-object v1, v2, v0

    const/16 v0, 0xfb

    move-object/from16 v1, v254

    aput-object v1, v2, v0

    const/16 v0, 0xfc

    move-object/from16 v1, v255

    aput-object v1, v2, v0

    const/16 v0, 0xfd

    move-object/from16 v1, v256

    aput-object v1, v2, v0

    const/16 v0, 0xfe

    move-object/from16 v1, v257

    aput-object v1, v2, v0

    const/16 v0, 0xff

    move-object/from16 v1, v258

    aput-object v1, v2, v0

    const/16 v0, 0x100

    move-object/from16 v1, v259

    aput-object v1, v2, v0

    const/16 v0, 0x101

    move-object/from16 v1, v260

    aput-object v1, v2, v0

    const/16 v0, 0x102

    move-object/from16 v1, v261

    aput-object v1, v2, v0

    const/16 v0, 0x103

    move-object/from16 v1, v262

    aput-object v1, v2, v0

    const/16 v0, 0x104

    move-object/from16 v1, v263

    aput-object v1, v2, v0

    const/16 v0, 0x105

    move-object/from16 v1, v264

    aput-object v1, v2, v0

    const/16 v0, 0x106

    move-object/from16 v1, v265

    aput-object v1, v2, v0

    const/16 v0, 0x107

    move-object/from16 v1, v266

    aput-object v1, v2, v0

    const/16 v0, 0x108

    move-object/from16 v1, v267

    aput-object v1, v2, v0

    const/16 v0, 0x109

    move-object/from16 v1, v268

    aput-object v1, v2, v0

    const/16 v0, 0x10a

    move-object/from16 v1, v269

    aput-object v1, v2, v0

    const/16 v0, 0x10b

    move-object/from16 v1, v270

    aput-object v1, v2, v0

    const/16 v0, 0x10c

    move-object/from16 v1, v271

    aput-object v1, v2, v0

    const/16 v0, 0x10d

    move-object/from16 v1, v272

    aput-object v1, v2, v0

    const/16 v0, 0x10e

    move-object/from16 v1, v273

    aput-object v1, v2, v0

    const/16 v0, 0x10f

    move-object/from16 v1, v274

    aput-object v1, v2, v0

    const/16 v0, 0x110

    move-object/from16 v1, v275

    aput-object v1, v2, v0

    const/16 v0, 0x111

    move-object/from16 v1, v276

    aput-object v1, v2, v0

    const/16 v0, 0x112

    move-object/from16 v1, v277

    aput-object v1, v2, v0

    const/16 v0, 0x113

    move-object/from16 v1, v278

    aput-object v1, v2, v0

    const/16 v0, 0x114

    move-object/from16 v1, v279

    aput-object v1, v2, v0

    const/16 v0, 0x115

    move-object/from16 v1, v280

    aput-object v1, v2, v0

    const/16 v0, 0x116

    move-object/from16 v1, v281

    aput-object v1, v2, v0

    const/16 v0, 0x117

    move-object/from16 v1, v282

    aput-object v1, v2, v0

    const/16 v0, 0x118

    move-object/from16 v1, v283

    aput-object v1, v2, v0

    const/16 v0, 0x119

    move-object/from16 v1, v284

    aput-object v1, v2, v0

    const/16 v0, 0x11a

    move-object/from16 v1, v285

    aput-object v1, v2, v0

    const/16 v0, 0x11b

    move-object/from16 v1, v286

    aput-object v1, v2, v0

    const/16 v0, 0x11c

    move-object/from16 v1, v287

    aput-object v1, v2, v0

    const/16 v0, 0x11d

    move-object/from16 v1, v288

    aput-object v1, v2, v0

    const/16 v0, 0x11e

    move-object/from16 v1, v289

    aput-object v1, v2, v0

    const/16 v0, 0x11f

    move-object/from16 v1, v290

    aput-object v1, v2, v0

    const/16 v0, 0x120

    move-object/from16 v1, v291

    aput-object v1, v2, v0

    const/16 v0, 0x121

    move-object/from16 v1, v292

    aput-object v1, v2, v0

    const/16 v0, 0x122

    move-object/from16 v1, v293

    aput-object v1, v2, v0

    const/16 v0, 0x123

    move-object/from16 v1, v294

    aput-object v1, v2, v0

    const/16 v0, 0x124

    move-object/from16 v1, v295

    aput-object v1, v2, v0

    const/16 v0, 0x125

    move-object/from16 v1, v296

    aput-object v1, v2, v0

    const/16 v0, 0x126

    move-object/from16 v1, v297

    aput-object v1, v2, v0

    const/16 v0, 0x127

    move-object/from16 v1, v298

    aput-object v1, v2, v0

    const/16 v0, 0x128

    move-object/from16 v1, v299

    aput-object v1, v2, v0

    const/16 v0, 0x129

    move-object/from16 v1, v300

    aput-object v1, v2, v0

    const/16 v0, 0x12a

    move-object/from16 v1, v301

    aput-object v1, v2, v0

    const/16 v0, 0x12b

    move-object/from16 v1, v302

    aput-object v1, v2, v0

    const/16 v0, 0x12c

    move-object/from16 v1, v303

    aput-object v1, v2, v0

    const/16 v0, 0x12d

    move-object/from16 v1, v304

    aput-object v1, v2, v0

    const/16 v0, 0x12e

    move-object/from16 v1, v305

    aput-object v1, v2, v0

    const/16 v0, 0x12f

    move-object/from16 v1, v306

    aput-object v1, v2, v0

    const/16 v0, 0x130

    move-object/from16 v1, v307

    aput-object v1, v2, v0

    const/16 v0, 0x131

    move-object/from16 v1, v308

    aput-object v1, v2, v0

    const/16 v0, 0x132

    move-object/from16 v1, v309

    aput-object v1, v2, v0

    const/16 v0, 0x133

    move-object/from16 v1, v310

    aput-object v1, v2, v0

    const/16 v0, 0x134

    move-object/from16 v1, v311

    aput-object v1, v2, v0

    const/16 v0, 0x135

    move-object/from16 v1, v312

    aput-object v1, v2, v0

    const/16 v0, 0x136

    move-object/from16 v1, v313

    aput-object v1, v2, v0

    const/16 v0, 0x137

    move-object/from16 v1, v314

    aput-object v1, v2, v0

    const/16 v0, 0x138

    move-object/from16 v1, v315

    aput-object v1, v2, v0

    const/16 v0, 0x139

    move-object/from16 v1, v316

    aput-object v1, v2, v0

    const/16 v0, 0x13a

    move-object/from16 v1, v317

    aput-object v1, v2, v0

    const/16 v0, 0x13b

    move-object/from16 v1, v318

    aput-object v1, v2, v0

    const/16 v0, 0x13c

    move-object/from16 v1, v319

    aput-object v1, v2, v0

    const/16 v0, 0x13d

    move-object/from16 v1, v320

    aput-object v1, v2, v0

    const/16 v0, 0x13e

    move-object/from16 v1, v321

    aput-object v1, v2, v0

    const/16 v0, 0x13f

    move-object/from16 v1, v322

    aput-object v1, v2, v0

    const/16 v0, 0x140

    move-object/from16 v1, v323

    aput-object v1, v2, v0

    const/16 v0, 0x141

    move-object/from16 v1, v324

    aput-object v1, v2, v0

    const/16 v0, 0x142

    move-object/from16 v1, v325

    aput-object v1, v2, v0

    const/16 v0, 0x143

    move-object/from16 v1, v326

    aput-object v1, v2, v0

    const/16 v0, 0x144

    move-object/from16 v1, v327

    aput-object v1, v2, v0

    const/16 v0, 0x145

    move-object/from16 v1, v328

    aput-object v1, v2, v0

    const/16 v0, 0x146

    move-object/from16 v1, v329

    aput-object v1, v2, v0

    const/16 v0, 0x147

    move-object/from16 v1, v330

    aput-object v1, v2, v0

    const/16 v0, 0x148

    move-object/from16 v1, v331

    aput-object v1, v2, v0

    const/16 v0, 0x149

    move-object/from16 v1, v332

    aput-object v1, v2, v0

    const/16 v0, 0x14a

    move-object/from16 v1, v333

    aput-object v1, v2, v0

    const/16 v0, 0x14b

    move-object/from16 v1, v334

    aput-object v1, v2, v0

    const/16 v0, 0x14c

    move-object/from16 v1, v335

    aput-object v1, v2, v0

    const/16 v0, 0x14d

    move-object/from16 v1, v336

    aput-object v1, v2, v0

    const/16 v0, 0x14e

    move-object/from16 v1, v337

    aput-object v1, v2, v0

    const/16 v0, 0x14f

    move-object/from16 v1, v338

    aput-object v1, v2, v0

    const/16 v0, 0x150

    move-object/from16 v1, v339

    aput-object v1, v2, v0

    const/16 v0, 0x151

    move-object/from16 v1, v340

    aput-object v1, v2, v0

    const/16 v0, 0x152

    move-object/from16 v1, v341

    aput-object v1, v2, v0

    const/16 v0, 0x153

    move-object/from16 v1, v342

    aput-object v1, v2, v0

    const/16 v0, 0x154

    move-object/from16 v1, v343

    aput-object v1, v2, v0

    const/16 v0, 0x155

    move-object/from16 v1, v344

    aput-object v1, v2, v0

    const/16 v0, 0x156

    move-object/from16 v1, v345

    aput-object v1, v2, v0

    const/16 v0, 0x157

    move-object/from16 v1, v346

    aput-object v1, v2, v0

    const/16 v0, 0x158

    move-object/from16 v1, v347

    aput-object v1, v2, v0

    const/16 v0, 0x159

    move-object/from16 v1, v348

    aput-object v1, v2, v0

    const/16 v0, 0x15a

    move-object/from16 v1, v349

    aput-object v1, v2, v0

    const/16 v0, 0x15b

    move-object/from16 v1, v350

    aput-object v1, v2, v0

    const/16 v0, 0x15c

    move-object/from16 v1, v351

    aput-object v1, v2, v0

    const/16 v0, 0x15d

    move-object/from16 v1, v352

    aput-object v1, v2, v0

    const/16 v0, 0x15e

    move-object/from16 v1, v353

    aput-object v1, v2, v0

    const/16 v0, 0x15f

    move-object/from16 v1, v354

    aput-object v1, v2, v0

    const/16 v0, 0x160

    move-object/from16 v1, v355

    aput-object v1, v2, v0

    const/16 v0, 0x161

    move-object/from16 v1, v356

    aput-object v1, v2, v0

    const/16 v0, 0x162

    move-object/from16 v1, v357

    aput-object v1, v2, v0

    const/16 v0, 0x163

    move-object/from16 v1, v358

    aput-object v1, v2, v0

    const/16 v0, 0x164

    move-object/from16 v1, v359

    aput-object v1, v2, v0

    const/16 v0, 0x165

    move-object/from16 v1, v360

    aput-object v1, v2, v0

    const/16 v0, 0x166

    move-object/from16 v1, v361

    aput-object v1, v2, v0

    const/16 v0, 0x167

    move-object/from16 v1, v362

    aput-object v1, v2, v0

    const/16 v0, 0x168

    move-object/from16 v1, v363

    aput-object v1, v2, v0

    const/16 v0, 0x169

    move-object/from16 v1, v364

    aput-object v1, v2, v0

    const/16 v0, 0x16a

    move-object/from16 v1, v365

    aput-object v1, v2, v0

    const/16 v0, 0x16b

    move-object/from16 v1, v366

    aput-object v1, v2, v0

    const/16 v0, 0x16c

    move-object/from16 v1, v367

    aput-object v1, v2, v0

    const/16 v0, 0x16d

    move-object/from16 v1, v368

    aput-object v1, v2, v0

    const/16 v0, 0x16e

    move-object/from16 v1, v369

    aput-object v1, v2, v0

    const/16 v0, 0x16f

    move-object/from16 v1, v370

    aput-object v1, v2, v0

    const/16 v0, 0x170

    move-object/from16 v1, v371

    aput-object v1, v2, v0

    const/16 v0, 0x171

    move-object/from16 v1, v372

    aput-object v1, v2, v0

    const/16 v0, 0x172

    move-object/from16 v1, v373

    aput-object v1, v2, v0

    const/16 v0, 0x173

    move-object/from16 v1, v374

    aput-object v1, v2, v0

    const/16 v0, 0x174

    move-object/from16 v1, v375

    aput-object v1, v2, v0

    const/16 v0, 0x175

    move-object/from16 v1, v376

    aput-object v1, v2, v0

    const/16 v0, 0x176

    move-object/from16 v1, v377

    aput-object v1, v2, v0

    const/16 v0, 0x177

    move-object/from16 v1, v378

    aput-object v1, v2, v0

    const/16 v0, 0x178

    move-object/from16 v1, v379

    aput-object v1, v2, v0

    const/16 v0, 0x179

    move-object/from16 v1, v380

    aput-object v1, v2, v0

    const/16 v0, 0x17a

    move-object/from16 v1, v381

    aput-object v1, v2, v0

    const/16 v0, 0x17b

    move-object/from16 v1, v382

    aput-object v1, v2, v0

    const/16 v0, 0x17c

    move-object/from16 v1, v383

    aput-object v1, v2, v0

    const/16 v0, 0x17d

    move-object/from16 v1, v384

    aput-object v1, v2, v0

    const/16 v0, 0x17e

    move-object/from16 v1, v385

    aput-object v1, v2, v0

    const/16 v0, 0x17f

    move-object/from16 v1, v386

    aput-object v1, v2, v0

    const/16 v0, 0x180

    move-object/from16 v1, v387

    aput-object v1, v2, v0

    const/16 v0, 0x181

    move-object/from16 v1, v388

    aput-object v1, v2, v0

    const/16 v0, 0x182

    move-object/from16 v1, v389

    aput-object v1, v2, v0

    const/16 v0, 0x183

    move-object/from16 v1, v390

    aput-object v1, v2, v0

    const/16 v0, 0x184

    move-object/from16 v1, v391

    aput-object v1, v2, v0

    const/16 v0, 0x185

    move-object/from16 v1, v392

    aput-object v1, v2, v0

    const/16 v0, 0x186

    move-object/from16 v1, v393

    aput-object v1, v2, v0

    const/16 v0, 0x187

    move-object/from16 v1, v394

    aput-object v1, v2, v0

    const/16 v0, 0x188

    move-object/from16 v1, v395

    aput-object v1, v2, v0

    const/16 v0, 0x189

    move-object/from16 v1, v396

    aput-object v1, v2, v0

    const/16 v0, 0x18a

    move-object/from16 v1, v397

    aput-object v1, v2, v0

    const/16 v0, 0x18b

    move-object/from16 v1, v398

    aput-object v1, v2, v0

    const/16 v0, 0x18c

    move-object/from16 v1, v399

    aput-object v1, v2, v0

    const/16 v0, 0x18d

    move-object/from16 v1, v400

    aput-object v1, v2, v0

    const/16 v0, 0x18e

    move-object/from16 v1, v401

    aput-object v1, v2, v0

    const/16 v0, 0x18f

    move-object/from16 v1, v402

    aput-object v1, v2, v0

    const/16 v0, 0x190

    move-object/from16 v1, v403

    aput-object v1, v2, v0

    const/16 v0, 0x191

    move-object/from16 v1, v404

    aput-object v1, v2, v0

    const/16 v0, 0x192

    move-object/from16 v1, v405

    aput-object v1, v2, v0

    const/16 v0, 0x193

    move-object/from16 v1, v406

    aput-object v1, v2, v0

    const/16 v0, 0x194

    move-object/from16 v1, v407

    aput-object v1, v2, v0

    const/16 v0, 0x195

    move-object/from16 v1, v408

    aput-object v1, v2, v0

    const/16 v0, 0x196

    move-object/from16 v1, v409

    aput-object v1, v2, v0

    const/16 v0, 0x197

    move-object/from16 v1, v410

    aput-object v1, v2, v0

    const/16 v0, 0x198

    move-object/from16 v1, v411

    aput-object v1, v2, v0

    const/16 v0, 0x199

    move-object/from16 v1, v412

    aput-object v1, v2, v0

    const/16 v0, 0x19a

    move-object/from16 v1, v413

    aput-object v1, v2, v0

    const/16 v0, 0x19b

    move-object/from16 v1, v414

    aput-object v1, v2, v0

    const/16 v0, 0x19c

    move-object/from16 v1, v415

    aput-object v1, v2, v0

    const/16 v0, 0x19d

    move-object/from16 v1, v416

    aput-object v1, v2, v0

    const/16 v0, 0x19e

    move-object/from16 v1, v417

    aput-object v1, v2, v0

    const/16 v0, 0x19f

    move-object/from16 v1, v418

    aput-object v1, v2, v0

    const/16 v0, 0x1a0

    move-object/from16 v1, v419

    aput-object v1, v2, v0

    const/16 v0, 0x1a1

    move-object/from16 v1, v420

    aput-object v1, v2, v0

    const/16 v0, 0x1a2

    move-object/from16 v1, v421

    aput-object v1, v2, v0

    const/16 v0, 0x1a3

    move-object/from16 v1, v422

    aput-object v1, v2, v0

    const/16 v0, 0x1a4

    move-object/from16 v1, v423

    aput-object v1, v2, v0

    const/16 v0, 0x1a5

    move-object/from16 v1, v424

    aput-object v1, v2, v0

    const/16 v0, 0x1a6

    move-object/from16 v1, v425

    aput-object v1, v2, v0

    const/16 v0, 0x1a7

    move-object/from16 v1, v426

    aput-object v1, v2, v0

    const/16 v0, 0x1a8

    aput-object v4, v2, v0

    .line 426
    sput-object v2, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->$VALUES:[Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2
    .line 3
    .line 4
    return-void
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
    .line 23
    .line 24
    .line 25
    .line 26
    .line 27
    .line 28
    .line 29
    .line 30
    .line 31
    .line 32
    .line 33
    .line 34
    .line 35
    .line 36
    .line 37
    .line 38
    .line 39
    .line 40
    .line 41
    .line 42
    .line 43
    .line 44
    .line 45
    .line 46
    .line 47
    .line 48
    .line 49
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dropbox/core/v2/teamlog/EventTypeArg;
    .locals 1

    .line 1
    const-class v0, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 2
    .line 3
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    .line 4
    .line 5
    .line 6
    move-result-object p0

    .line 7
    check-cast p0, Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 8
    .line 9
    return-object p0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public static values()[Lcom/dropbox/core/v2/teamlog/EventTypeArg;
    .locals 1

    .line 1
    sget-object v0, Lcom/dropbox/core/v2/teamlog/EventTypeArg;->$VALUES:[Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 2
    .line 3
    invoke-virtual {v0}, [Lcom/dropbox/core/v2/teamlog/EventTypeArg;->clone()Ljava/lang/Object;

    .line 4
    .line 5
    .line 6
    move-result-object v0

    .line 7
    check-cast v0, [Lcom/dropbox/core/v2/teamlog/EventTypeArg;

    .line 8
    .line 9
    return-object v0
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

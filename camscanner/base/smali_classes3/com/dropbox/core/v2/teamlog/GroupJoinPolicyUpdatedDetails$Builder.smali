.class public Lcom/dropbox/core/v2/teamlog/GroupJoinPolicyUpdatedDetails$Builder;
.super Ljava/lang/Object;
.source "GroupJoinPolicyUpdatedDetails.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dropbox/core/v2/teamlog/GroupJoinPolicyUpdatedDetails;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field protected isCompanyManaged:Ljava/lang/Boolean;

.field protected joinPolicy:Lcom/dropbox/core/v2/teamlog/GroupJoinPolicy;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    .line 3
    .line 4
    const/4 v0, 0x0

    .line 5
    iput-object v0, p0, Lcom/dropbox/core/v2/teamlog/GroupJoinPolicyUpdatedDetails$Builder;->isCompanyManaged:Ljava/lang/Boolean;

    .line 6
    .line 7
    iput-object v0, p0, Lcom/dropbox/core/v2/teamlog/GroupJoinPolicyUpdatedDetails$Builder;->joinPolicy:Lcom/dropbox/core/v2/teamlog/GroupJoinPolicy;

    .line 8
    .line 9
    return-void
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method


# virtual methods
.method public build()Lcom/dropbox/core/v2/teamlog/GroupJoinPolicyUpdatedDetails;
    .locals 3

    .line 1
    new-instance v0, Lcom/dropbox/core/v2/teamlog/GroupJoinPolicyUpdatedDetails;

    .line 2
    .line 3
    iget-object v1, p0, Lcom/dropbox/core/v2/teamlog/GroupJoinPolicyUpdatedDetails$Builder;->isCompanyManaged:Ljava/lang/Boolean;

    .line 4
    .line 5
    iget-object v2, p0, Lcom/dropbox/core/v2/teamlog/GroupJoinPolicyUpdatedDetails$Builder;->joinPolicy:Lcom/dropbox/core/v2/teamlog/GroupJoinPolicy;

    .line 6
    .line 7
    invoke-direct {v0, v1, v2}, Lcom/dropbox/core/v2/teamlog/GroupJoinPolicyUpdatedDetails;-><init>(Ljava/lang/Boolean;Lcom/dropbox/core/v2/teamlog/GroupJoinPolicy;)V

    .line 8
    .line 9
    .line 10
    return-object v0
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
.end method

.method public withIsCompanyManaged(Ljava/lang/Boolean;)Lcom/dropbox/core/v2/teamlog/GroupJoinPolicyUpdatedDetails$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/dropbox/core/v2/teamlog/GroupJoinPolicyUpdatedDetails$Builder;->isCompanyManaged:Ljava/lang/Boolean;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method

.method public withJoinPolicy(Lcom/dropbox/core/v2/teamlog/GroupJoinPolicy;)Lcom/dropbox/core/v2/teamlog/GroupJoinPolicyUpdatedDetails$Builder;
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/dropbox/core/v2/teamlog/GroupJoinPolicyUpdatedDetails$Builder;->joinPolicy:Lcom/dropbox/core/v2/teamlog/GroupJoinPolicy;

    .line 2
    .line 3
    return-object p0
    .line 4
    .line 5
    .line 6
    .line 7
    .line 8
    .line 9
    .line 10
    .line 11
    .line 12
    .line 13
    .line 14
    .line 15
    .line 16
    .line 17
    .line 18
    .line 19
    .line 20
    .line 21
    .line 22
.end method
